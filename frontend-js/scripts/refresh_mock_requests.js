var testFolder = './testFiles/apiCalls/';
var apiBaseDir = 'http://localhost:8080/minerva/api/';
var fs = require('fs');
var path = require('path');
var request = require('request');

var Promise = require('bluebird');

function listFiles(dir, filelist) {
  var fs = fs || require('fs'),
    files = fs.readdirSync(dir);
  filelist = filelist || [];
  files.forEach(function (file) {
    if (fs.statSync(dir + '/' + file).isDirectory()) {
      filelist = listFiles(dir + '/' + file, filelist);
    } else {
      filelist.push(dir + "/" + file);
    }
  });
  return filelist;
}

function extractUrl(file) {
  var url = apiBaseDir + path.dirname(file);
  url = url.replace(testFolder, "") + "/";
  url = url.replace("/all/", "/*/");
  url = url.replace(/api\/\//g, "api/");
  //don't change "." character if it's part of a number
  if (!/\.[0-9]+/.test(url)) {
    url = url.replace(".", ":");
  }
  return url;
}

function extractLogin(file) {
  var filename = file.replace(path.dirname(file), "").substr(1);
  if (filename.endsWith("MOCK_TOKEN_ID&")) {
    return "anonymous";
  } else if (filename.endsWith("ADMIN_TOKEN_ID&")) {
    return "admin";
  } else if (filename.endsWith("NO_ACCESS_USER_TOKEN_ID&")) {
    // noinspection SpellCheckingInspection
    return "noaccessuser";
  } else if (filename.indexOf("TOKEN_ID&") >= 0) {
    throw new Error("Unknown token in file: " + filename);
  }
  return undefined;
}

function extractMethod(file) {
  var filename = file.replace(path.dirname(file), "").substr(1);
  var method = filename.split("=")[0];
  var result;
  if (method.indexOf("_") >= 0) {
    result = method.split("_")[0];
  } else {
    result = "GET";
  }
  return result;
}

function setParam(params, key, value) {
  if (key.indexOf(".") > 0) {
    var topKey = key.split(".")[0];
    if (params[topKey] === undefined) {
      params[topKey] = {};
    }
    setParam(params[topKey], key.substr(topKey.length + 1), value)
  } else {
    if (value.indexOf("ALIAS.") >= 0) {
      value = value.replace("ALIAS.", "ALIAS:")
    }
    if (value.indexOf("REACTION.") >= 0) {
      value = value.replace("REACTION.", "REACTION:")
    }
    params[key] = value;
  }
}

function extractParams(file) {
  var filename = file.replace(path.dirname(file), "").substr(1);
  var params = {};
  var method = extractMethod(file);
  if (filename.indexOf(method) === 0) {
    filename = filename.substr(method.length + 1);
  }
  var tokens = filename.split("&");
  for (var i = 0; i < tokens.length; i++) {
    var token = tokens[i];
    var key = token.split("=")[0];
    var value = token.split("=")[1];

    if (key !== "" && key !== "token") {
      setParam(params, key, value)
    }
  }
  return params;
}

function prepareQueries(filelist) {
  var result = [];
  for (var i = 0; i < filelist.length; i++) {
    var file = filelist[i];
    result.push({
      file: file,
      url: extractUrl(file),
      method: extractMethod(file),
      login: extractLogin(file),
      params: extractParams(file)
    })
  }

  return result;
}

function getAuthToken(login) {
  var params = {
    method: "POST",
    url: apiBaseDir + "doLogin",
    form: {"login": login}
  };

  if (login === "anonymous") {
    //we don't need to login when accessing with anonymous account
    params.url = apiBaseDir + "configuration/";
    params.method = "GET";
  } else if (login === "admin") {
    params.form.password = "admin";
  } else if (login === "noaccessuser") {
    params.form.password = "noaccessuser";
  } else if (login !== undefined) {
    throw new Error("Unknown user: " + login);
  }
  return new Promise(function (resolve, reject) {
    request(params, function (error, response) {
      if (error) {
        reject(new Error(error.message));
      } else if (response.statusCode !== 200) {
        reject(new Error(params.url + " rejected with status code: " + response.statusCode));
      } else {
        resolve(response.headers['set-cookie']);
      }
    });
  }).then(function (value) {
    return Promise.resolve(value[0]);
  });
}

function executeQuery(query) {
  var url = query.url + "?";
  if (query.method === "GET") {
    for (var key in query.params) {
      if (query.params.hasOwnProperty(key)) {
        url += key + "=" + query.params[key] + "&";
      }
    }
  }
  return getAuthToken(query.login).then(function (token) {
    var params = {
      method: query.method,
      url: url,
      headers: {'Cookie': token}
    };

    return new Promise(function (resolve, reject) {
      request(params, function (error, response, body) {
        if (error) {
          reject(new Error(error.message));
        } else if (response.statusCode !== 200) {
          reject(new Error(url + " rejected with status code: " + response.statusCode));
        } else {
          // for some reason sometimes result is an object not a string
          if (typeof body === 'string' || body instanceof String) {
            resolve(body);
          } else {
            resolve(JSON.stringify(body));
          }
        }
      });
    });
  }).then(function (content) {
    var data = fs.readFileSync(query.file, 'utf8');
    if (data !== content) {
      console.log("Query response changed: " + url);
      console.log("Query response changed: " + url);
      fs.writeFileSync(query.file, content);

      if (data.length < 1000 && content.length < 1000) {
        console.log("\n\n");
        console.log(data);
        console.log(content);
        console.log("\n\n");
      }
    }
  });
}

function executeQueries(queries) {
  var promises = [];
  // for (var i = 0; i < 3; i++) {
  for (var i = 0; i < queries.length; i++) {
    var query = queries[i];
    if (query.method !== "GET" || query.url.indexOf(":downloadModel") >= 0 || query.url.indexOf("/doLogout") >= 0) {
      console.log("Ignoring " + query.method + " query: " + query.url);
    } else {
      // console.log("Exec " + query.method + " query: " + query.url);
      // console.log(query.params);
      promises.push(executeQuery(query));
    }
  }
  return Promise.all(promises);
}


var files = listFiles(testFolder);

var queries = prepareQueries(files);
return executeQueries(queries).catch(function (error) {
  console.log(error);
  process.exit(1);
});
