var process = require('process');
var fs = require('fs');

var CleanCSS = require('clean-css');

var files = [
  {source: "node_modules/vanilla-cookieconsent/dist/cookieconsent.css"},
  {source: "node_modules/@fortawesome/fontawesome-free/css/all.css", targetBase: "../../minerva/resources/css/"},
  {source: "node_modules/spectrum-colorpicker/spectrum.css"},
  {source: "node_modules/bootstrap/dist/css/bootstrap.css"},
  {source: "node_modules/jquery-ui/themes/base/jquery.ui.all.css", targetBase: "../../minerva/resources/jquery-ui/"},
  {source: "node_modules/datatables.net-dt/css/jquery.dataTables.css", targetBase: "../../minerva/resources/datatables/css/"},
  {source: "node_modules/datatables.net-rowreorder-dt/css/rowReorder.dataTables.css"},
  {source: "node_modules/jstree/dist/themes/default/style.css", targetBase: "../../minerva/resources/jstree/"},
  {source: "node_modules/pileup/style/pileup.css"},
  {source: "node_modules/openlayers/dist/ol.css"},
  {source: "node_modules/dual-listbox/dist/dual-listbox.css"},
  {source: "node_modules/multi-checkbox-list/dist/multi-checkbox-list.css"},
  {source: "src/main/css/global.css", targetBase: "../../minerva/resources/"}
];

var inputs = [];

for (var i = 0; i < files.length; i++) {
  var file = files[i];
  var output = new CleanCSS({rebase: false}).minify([file.source]);
  if (output.errors.length > 0) {
    console.log("Problem with css: ", output.errors);
    process.exit(-1);
  }
  if (output.warnings.length > 0) {
    console.log("Problem with css: ", output.warnings);
    process.exit(-2);
  }
  file.content = output.styles;
  var tmp = file.source.split("/");
  file.filename = tmp[tmp.length - 1];

  var input = {};
  var base = "./";
  if (file.targetBase !== undefined) {
    base = file.targetBase;
  }
  input[base + file.filename] = {styles: file.content};
  inputs.push(input);
}


return new CleanCSS({returnPromise: true, rebaseTo: '.'}).minify(inputs).then(function (output) {
  if (output.errors.length > 0) {
    console.log("Problem with css: ", output.errors);
    process.exit(-1);
  }
  if (output.warnings.length > 0) {
    console.log("Problem with css: ", output.warnings);
    process.exit(-2);
  }
  fs.writeFile("dist/minerva.css", output.styles, function (err) {
    if (err) {
      console.log("Problem with writing to file", err);
      process.exit(-3);
    }
  });
}).catch(function (err) {
  console.log(err);
  process.exit(-4);
});
