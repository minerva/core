var fs = require('fs');

var tomcatDirectory = process.env.CATALINA_HOME;
fs.createReadStream('dist/minerva.js').pipe(fs.createWriteStream(tomcatDirectory + '/webapps/minerva/resources/js/minerva.js'));
fs.createReadStream('dist/minerva.js.map').pipe(fs.createWriteStream(tomcatDirectory + '/webapps/minerva/resources/js/minerva.js.map'));

fs.createReadStream('dist/BrowserCheck.js').pipe(fs.createWriteStream(tomcatDirectory + '/webapps/minerva/resources/js/BrowserCheck.js'));
fs.createReadStream('dist/BrowserCheck.js.map').pipe(fs.createWriteStream(tomcatDirectory + '/webapps/minerva/resources/js/BrowserCheck.js.map'));

fs.createReadStream('dist/minerva.css').pipe(fs.createWriteStream(tomcatDirectory + '/webapps/minerva/resources/css/minerva.css'));
