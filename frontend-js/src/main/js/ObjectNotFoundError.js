"use strict";

/* exported logger */

// noinspection JSUnusedLocalSymbols
var logger = require('./logger');

/**
 *
 * @param {string} message
 * @constructor
 * @extends Error
 */
function ObjectNotFoundError(message) {
  this.message = message;
  this.stack = (new Error(message)).stack;
}

ObjectNotFoundError.prototype = Object.create(Error.prototype);
ObjectNotFoundError.prototype.constructor = ObjectNotFoundError;

module.exports = ObjectNotFoundError;
