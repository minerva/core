"use strict";

/**
 * @typedef {Object} Logger
 * @property {function(Object):void} warn
 * @property {function(Object):void} info
 * @property {function(Object):void} debug
 * @property {function(Object):void} error
 * @property {function(Object):void} fatal
 */

/**
 * @type Logger
 */
if (typeof global.it === 'function') {
  var log4js = require('log4js');
  logger = log4js.getLogger();
} else {
  var logger = new function () {
    var levels = ["DEBUG", "INFO", "WARN", "ERROR", "FATAL"];
    var level = "DEBUG";
    // noinspection JSUnusedGlobalSymbols
    return {
      checkLevel: function (minLevel) {
        var index = levels.indexOf(level);
        var minLevelIndex = levels.indexOf(minLevel);
        return index <= minLevelIndex;
      },

      debug: function (message) {
        if (this.checkLevel("DEBUG")) {
          console.log(message)
        }
      },
      info: function (message) {
        if (this.checkLevel("INFO")) {
          console.log(message)
        }
      },
      warn: function (message) {
        if (this.checkLevel("WARN")) {
          console.log(message)
        }
      },
      error: function (message) {
        if (this.checkLevel("ERROR")) {
          console.log(message)
        }
      },
      fatal: function (message) {
        if (this.checkLevel("FATAL")) {
          console.log(message)
        }
      }
    }
  };
}
module.exports = logger;
