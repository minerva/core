"use strict";

/* exported logger */
var $ = require('jquery');

var Promise = require("bluebird");

var CustomMapOptions = require('./map/CustomMapOptions');
var AbstractGuiElement = require('./gui/AbstractGuiElement');

var ConfigurationAdminPanel = require('./gui/admin/ConfigurationAdminPanel');
var PluginAdminPanel = require('./gui/admin/PluginAdminPanel');
var GenomeAdminPanel = require('./gui/admin/GenomeAdminPanel');
var MapsAdminPanel = require('./gui/admin/MapsAdminPanel');
var UsersAdminPanel = require('./gui/admin/UsersAdminPanel');

// noinspection JSUnusedLocalSymbols
var logger = require('./logger');
var request = require('request');
var semver = require('semver');
var GuiUtils = require('./gui/leftPanel/GuiUtils');
var NetworkError = require('./NetworkError');
var GuiConnector = require('./GuiConnector');

/**
 * Default constructor.
 *
 * @param {CustomMapOptions} options
 *           object representing all parameters needed for map creation
 * @constructor
 */
function Admin(options) {
  var self = this;
  if (!(options instanceof CustomMapOptions)) {
    options = new CustomMapOptions(options);
  }

  self.setElement(options.getElement());
  self.setConfiguration(options.getConfiguration());
  self.setServerConnector(options.getServerConnector());

  self._createGui();
}

Admin.prototype = Object.create(AbstractGuiElement.prototype);
Admin.prototype.constructor = AbstractGuiElement;

/**
 *
 * @private
 */
Admin.prototype._createGui = function () {
  var self = this;
  self.getElement().innerHTML = "";
  self.getGuiUtils().initTabContent(self);

  var panels = [{
    name: "PROJECTS",
    panelClass: MapsAdminPanel
  }, {
    name: "USERS",
    panelClass: UsersAdminPanel
  }, {
    name: "CONFIGURATION",
    panelClass: ConfigurationAdminPanel
  }, {
    name: "GENOMES",
    panelClass: GenomeAdminPanel
  }, {
    name: "PLUGINS",
    panelClass: PluginAdminPanel
  }];

  for (var i = 0; i < panels.length; i++) {
    self.getGuiUtils().addTab(self, panels[i]);
  }
  var navElement = $(this.getElement()).find("> .parentTabs > .nav-tabs")[0];

  var navLi = $.parseHTML("<li style='flex-grow:1;margin-top:5px;text-align:right'><a target='_api_docs' href='" + self.getServerConnector().getServerBaseUrl() + "docs/'>API docs</a></li>");
  $(navElement).append(navLi);

  self.addLogoutButton($(this.getElement()).find("> .parentTabs > .nav-tabs")[0]);
};

/**
 *
 * @param {HTMLElement} navElement
 */
Admin.prototype.addLogoutButton = function (navElement) {
  var self = this;
  var logoutLink = self.getGuiUtils().createLogoutLink();

  var navLi = document.createElement("li");
  navLi.appendChild(logoutLink);
  $(navLi).css("text-align", "right");
  $(navLi).css("margin-top", "5px");
  $(navLi).css("margin-right", "5px");
  $(navLi).css("margin-left", "15px");

  navElement.appendChild(navLi);
};

/**
 *
 * @param {HTMLElement} element
 */
Admin.prototype.setElement = function (element) {
  this._element = element;
};
/**
 *
 * @returns {HTMLElement}
 */
Admin.prototype.getElement = function () {
  return this._element;
};

/**
 *
 * @returns {Promise}
 */
Admin.prototype.init = function () {
  var self = this;
  var promises = [];
  for (var i = 0; i < self._panels.length; i++) {
    promises.push(self._panels[i].init());
  }
  return Promise.all(promises).then(function () {
    $(window).trigger('resize');
  }).then(function () {
    $("div.dataTables_filter").wrap("<form>");
    $("div.dataTables_filter").closest("form").attr("autocomplete", "off");
    return self.checkAvailableVersion();
  });
};

/**
 *
 * @param {Configuration} configuration
 */
Admin.prototype.setConfiguration = function (configuration) {
  this._configuration = configuration;
};

/**
 *
 * @returns {Configuration}
 */
Admin.prototype.getConfiguration = function () {
  return this._configuration;
};

/**
 *
 * @returns {Promise}
 */
Admin.prototype.destroy = function () {
  var self = this;
  var promises = [];
  for (var i = 0; i < self._panels.length; i++) {
    promises.push(self._panels[i].destroy());
  }
  return Promise.all(promises);
};

/**
 *
 * @return {Promise<T>}
 */
Admin.prototype.checkAvailableVersion = function () {
  var self = this;

  var localVersion = semver.coerce(self.getConfiguration().getVersion());
  if (localVersion === null) {
    GuiConnector.warn("Minerva was not deployed properly. Please contact system administrator.");
  } else {
    return self.getLatestPublishedVersion().then(function (publishedVersion) {
      if (semver.gt(publishedVersion, localVersion) > 0) {
        GuiConnector.warn("New minerva version (" + publishedVersion + ") was released. Please upgrade your minerva instance or contact system administrator to do so.");
      }
    }).catch(function (error) {
      if (error instanceof NetworkError && error.statusCode) {
        logger.warn("Problem with accessing info about latest minerva version", error);
      } else {
        throw error;
      }
    });
  }
};

/**
 *
 * @return {Promise}
 */
Admin.prototype.getLatestPublishedVersion = function () {
  return new Promise(function (resolve, reject) {
    var url = "https://minerva-net.lcsb.uni.lu/proxy/?url=https://webdav-r3lab.uni.lu/public/minerva/";
    request(url, function (error, response, body) {
      if (error) {
        reject(new NetworkError(error.message, {
          content: body,
          url: url
        }));
      } else if (response.statusCode !== 200) {
        reject(new NetworkError(url + " rejected with status code: " + response.statusCode, {
          content: body,
          url: url,
          statusCode: response.statusCode
        }));
      } else {
        var content;
        // for some reason sometimes result is an object not a string
        if (typeof body === 'string' || body instanceof String) {
          content = body;
        } else {
          content = JSON.stringify(body);
        }
        var re = /href="[\w.]+/g;
        var result = content.match(re);
        for (var i = result.length - 1; i >= 0; i--) {
          result[i] = result[i].replace("href=\"", "");
          if (!semver.valid(result[i])) {
            logger.warn("Invalid version: " + result[i]);
            result.splice(i, 1);
          }
        }
        result.sort(semver.compare);
        resolve(result[result.length - 1]);
      }
    });
  });
};

/**
 *
 * @param {ServerConnector} serverConnector
 */
Admin.prototype.setServerConnector = function (serverConnector) {
  this._serverConnector = serverConnector;
};

/**
 *
 * @returns {ServerConnector}
 */
Admin.prototype.getServerConnector = function () {
  return this._serverConnector;
};

/**
 *
 * @returns {GuiUtils}
 */
Admin.prototype.getGuiUtils = function () {
  var self = this;
  if (self._guiUtils === undefined) {
    self._guiUtils = new GuiUtils(self.getConfiguration());
  }
  return this._guiUtils;
};


module.exports = Admin;
