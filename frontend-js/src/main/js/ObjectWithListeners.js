"use strict";

var Promise = require("bluebird");

var logger = require('./logger');

/**
 * An abstract class that allows to register listeners and call them in case of
 * events.
 * @constructor
 */
function ObjectWithListeners() {
  this._validListeners = [];
  this._validPropertyListeners = [];
}

/**
 * @callback ListenerCallback
 * @param {Object} event
 * @param {string} event.type
 * @param {ObjectWithListeners} event.object
 * @param {Object} event.arg
 */

/**
 * Adds a listener function to the object.
 *
 * @param {string} type
 *          string defining type of the listener
 * @param {ListenerCallback} fun
 *          function that should be thrown when type event occurs
 */
ObjectWithListeners.prototype.addListener = function (type, fun) {
  if (this._validListeners[type] === undefined) {
    throw new Error("Unknown listener type: " + type);
  }

  if (typeof fun !== "function") {
    throw new Error("Second parameter must be a function but \"" + typeof (fun) + "\" found.");
  }

  this._validListeners[type].push(fun);
};

/**
 * Adds a property change listener function to the object.
 *
 * @param {string} name
 *          string defining property name
 * @param {function} fun
 *          function that should be thrown when firePropertyChangeListener is
 *          called
 */
ObjectWithListeners.prototype.addPropertyChangeListener = function (name, fun) {
  if (this._validPropertyListeners[name] === undefined) {
    throw new Error("Unknown property type: " + name);
  }

  if (typeof fun !== "function") {
    throw new Error("Second parameter must be a function but \"" + typeof (fun) + "\" found.");
  }

  this._validPropertyListeners[name].push(fun);
};

/**
 * Register new type of listener.
 *
 * @param type
 *          string identifying new type of listener
 */
ObjectWithListeners.prototype.registerListenerType = function (type) {
  if (this._validListeners[type] !== undefined) {
    throw new Error("Listener type already registered: " + type);
  }
  this._validListeners[type] = [];
};

/**
 * Register new property for listening.
 *
 * @param name
 *          string identifying property
 */
ObjectWithListeners.prototype.registerPropertyType = function (name) {
  if (this._validPropertyListeners[name] !== undefined) {
    throw new Error("Property already registered: " + name);
  }
  this._validPropertyListeners[name] = [];
};

/**
 * Removes listener from the object.
 *
 * @param type
 *          type of the listener
 * @param fun
 *          function that was call when event occurred that should be removed
 */
ObjectWithListeners.prototype.removeListener = function (type, fun) {
  if (this._validListeners[type] === undefined) {
    throw new Error("Unknown listener type: " + type);
  }

  if (typeof (fun) !== "function") {
    throw new Error("Second parameter must be a function but \"" + typeof (fun) + "\" found.");
  }

  var listenerList = this._validListeners[type];

  var index = listenerList.indexOf(fun);
  if (index > -1) {
    listenerList.splice(index, 1);
  } else {
    logger.warn("Cannot remove listener. It doesn't exist", type, fun);
  }
};

/**
 * Removes property listener from the object.
 *
 * @param name
 *          name of the property
 * @param fun
 *          function that was call when event occurred that should be removed
 */
ObjectWithListeners.prototype.removePropertyListener = function (name, fun) {
  if (this._validPropertyListeners[name] === undefined) {
    throw new Error("Unknown property: " + name);
  }

  if (typeof (fun) !== "function") {
    throw new Error("Second parameter must be a function but \"" + typeof (fun) + "\" found.");
  }

  var listenerList = this._validPropertyListeners[name];

  var index = listenerList.indexOf(fun);
  if (index > -1) {
    listenerList.splice(index, 1);
  } else {
    logger.warn("Cannot remove listener. It doesn't exist", name, fun);
  }
};

/**
 * Fires listeners of a given type.
 *
 * @param {string} type
 *          type of the listener (string)
 * @param {Object} [arg]
 *          argument data passed to the handler
 *
 * @returns {PromiseLike}
 */
ObjectWithListeners.prototype.callListeners = function (type, arg) {
  if (this._validListeners[type] === undefined) {
    throw new Error("Unknown listener type: " + type);
  }
  var listenerList = this._validListeners[type];
  var promises = [];
  if (listenerList.length > 0) {
    for (var i in listenerList) {
      if (listenerList.hasOwnProperty(i)) {
        var e = {
          type: type,
          object: this,
          arg: arg
        };
        promises.push(listenerList[i](e));
      }
    }
  }
  return Promise.all(promises);
};

/**
 *
 * @param {string} type
 * @returns {function[]}
 */
ObjectWithListeners.prototype.getListeners = function (type) {
  if (this._validListeners[type] === undefined) {
    throw new Error("Unknown listener type: " + type);
  }
  return this._validListeners[type];
};

/**
 * Fires property change listeners for a given property name.
 *
 * @param propertyName
 *          name of the property
 * @param oldValue
 *          old value of the property
 * @param newValue
 *          new value of the property
 */
ObjectWithListeners.prototype.firePropertyChangeListener = function (propertyName, oldValue, newValue) {
  var self = this;
  if (this._validPropertyListeners[propertyName] === undefined) {
    throw new Error("Unknown property type: " + propertyName);
  }
  var listenerList = this._validPropertyListeners[propertyName];
  var promises = [];
  if (listenerList.length > 0) {
    for (var i in listenerList) {
      var e = {
        propertyName: propertyName,
        object: self,
        oldVal: oldValue,
        newVal: newValue
      };
      promises.push(listenerList[i](e));
    }
  }
  return Promise.all(promises);
};

module.exports = ObjectWithListeners;
