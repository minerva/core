'use strict';

var $ = require('jquery');
//this is needed here because of: https://stackoverflow.com/a/20457891/1127920
require('bootstrap');
var functions = require('./Functions');

var AbstractDbOverlay = require('./map/overlay/AbstractDbOverlay');
var Admin = require('./Admin');
var DbOverlayCollection = require('./map/overlay/DbOverlayCollection');
var ConfigurationType = require('./ConfigurationType');
var CustomMap = require('./map/CustomMap');
var CustomMapOptions = require('./map/CustomMapOptions');
var Export = require('./Export');
var GuiUtils = require('./gui/leftPanel/GuiUtils');
var PluginManager = require('./plugin/PluginManager');
var Point = require('./map/canvas/Point');
var SearchDbOverlay = require('./map/overlay/SearchDbOverlay');

var LeftPanel = require('./gui/leftPanel/LeftPanel');
var TopMenu = require('./gui/topMenu/TopMenu');
var Legend = require('./gui/Legend');
var MapContextMenu = require('./gui/MapContextMenu');

var GuiConnector = require('./GuiConnector');
var ServerConnector = require('./ServerConnector');
var SecurityError = require('./SecurityError');
var ValidationError = require('./ValidationError');
var NetworkError = require('./NetworkError');

var HttpStatus = require('http-status-codes');

var MolArt = require('./map/structure/MolArt');

var Promise = require("bluebird");

var logger = require('./logger');
var Functions = require('./Functions');

var cookieConsent = require('./CookieSetup');


var customMap, leftPanel, topMenu, legend, mapContextMenu;

/**
 *
 * @param {CustomMapOptions} params
 */
function processUrlGetParams(params) {
  var project = params.getProject();
  var sessionData = ServerConnector.getSessionData(project);

  var modelId;
  if (GuiConnector.getParams["submap"] !== undefined) {
    modelId = parseInt(GuiConnector.getParams["submap"]);
  } else if (project.getModels().length > 0) {
    modelId = project.getModels()[0].getId()
  } else {
    return Promise.reject(new ValidationError("You cannot browse the project because it does not contain any map. " +
      "Please re-upload the project."));
  }

  var model = project.getModelById(modelId);
  if (model !== null) {

    if (GuiConnector.getParams["x"] !== undefined && GuiConnector.getParams["y"] !== undefined) {
      var point = new Point(GuiConnector.getParams["x"], GuiConnector.getParams["y"]);
      sessionData.setCenter(model, point);
    }
    if (GuiConnector.getParams["zoom"] !== undefined) {
      sessionData.setZoomLevel(model, GuiConnector.getParams["zoom"]);
    }
  } else {
    logger.warn("Model with given ID does not exist: " + modelId);
  }

  if (GuiConnector.getParams["background"] !== undefined) {
    sessionData.setSelectedBackgroundOverlay(GuiConnector.getParams["background"]);
  }

  if (GuiConnector.getParams["overlays"] !== undefined) {
    var overlays = GuiConnector.getParams["overlays"].split(",");
    var data = [];
    for (var i = 0; i < overlays.length; i++) {
      if (Functions.isInt(parseInt(overlays[i]))) {
        data.push(overlays[i]);
      } else {
        GuiConnector.warn("Invalid overlay id: " + overlays[i]);
      }
    }
    sessionData.setVisibleOverlays(data);
  }

  if (GuiConnector.getParams["comments"] === "on") {
    sessionData.setShowComments(true);
  }
  var query;
  if (GuiConnector.getParams["search"] !== undefined) {
    query = SearchDbOverlay.prototype.encodeQuery(AbstractDbOverlay.QueryType.SEARCH_BY_QUERY,
      GuiConnector.getParams["search"],
      GuiConnector.getParams["search.perfect"] === "true");
    sessionData.setSearchQuery(query);
  }
  if (GuiConnector.getParams["drugSearch"] !== undefined) {
    query = SearchDbOverlay.prototype.encodeQuery(AbstractDbOverlay.QueryType.SEARCH_BY_QUERY,
      GuiConnector.getParams["drugSearch"]);
    sessionData.setDrugQuery(query);
  }
  if (GuiConnector.getParams["chemicalSearch"] !== undefined) {
    query = SearchDbOverlay.prototype.encodeQuery(AbstractDbOverlay.QueryType.SEARCH_BY_QUERY,
      GuiConnector.getParams["chemicalSearch"]);
    sessionData.setChemicalQuery(query);
  }
  if (GuiConnector.getParams["searchCoordinates"] !== undefined) {
    var array = GuiConnector.getParams["searchCoordinates"].split(",");
    var x = parseInt(array[0]);
    var y = parseInt(array[1]);
    var searchModelId = parseInt(array[2]);
    var searchZoom = parseInt(array[3]);
    query = SearchDbOverlay.prototype.encodeQuery(AbstractDbOverlay.QueryType.SEARCH_BY_COORDINATES,
      searchModelId, new Point(x, y), searchZoom);
    sessionData.setSearchQuery(query);
  }
}

/**
 *
 * @param {User} user
 * @param {string} termsOfUseUrl
 */
function requestConsent(user, termsOfUseUrl) {
  if (termsOfUseUrl === "") {
    return;
  }
  var dialog = document.createElement("div");
  var dialogContent = document.createElement("div");
  dialogContent.appendChild(functions.createElement({
    type: "span",
    content: "I agree to the minerva <a href='" + termsOfUseUrl + "' target='_blank'>Terms of Service</a>.",
    xss: false
  }));
  var checkbox = functions.createElement({type: "input", inputType: "checkbox", style: "float: left"});
  var okButton = functions.createElement({
    type: "button",
    content: "OK",
    className: "ui-button ui-corner-all ui-widget",
    onclick: function () {
      user.setTermsOfUseConsent($(checkbox).is(':checked'));
      return ServerConnector.updateUser(user).then(function () {
        $(dialog).dialog("close");
      }).catch(GuiConnector.alert);
    }
  });
  var cancelButton = functions.createElement({
    type: "button",
    content: "I disagree",
    className: "ui-button ui-corner-all ui-widget",
    onclick: function () {
      return ServerConnector.logout().catch(GuiConnector.alert);
    }
  });
  $(okButton).prop("disabled", true);
  $(checkbox).change(function () {
    $(okButton).prop("disabled", !$(checkbox).is(':checked'));
  });
  dialogContent.appendChild(checkbox);
  dialogContent.appendChild(okButton);
  dialogContent.appendChild(cancelButton);

  dialog.appendChild(dialogContent);
  document.body.appendChild(dialog);
  $(dialog).dialog({
    dialogClass: 'minerva-terms-of-use-dialog',
    classes: {
      "ui-dialog": "ui-state-error"
    },
    modal: true,
    closeOnEscape: false,
    title: "Terms of Service"
  }).siblings('.ui-dialog-titlebar').css("background", "red");
  $(".ui-dialog-titlebar-close", $(dialog).dialog().siblings('.ui-dialog-titlebar')).hide();
  $(dialog).dialog("open");
}

/**
 *
 * @param {HTMLElement} element
 */
function createDivStructure(element) {
  var tableDiv = functions.createElement({
    type: "div",
    className: "minerva-root"
  });
  element.appendChild(tableDiv);
  var leftPanelDiv = functions.createElement({
    type: "div",
    className: "minerva-left-panel"
  });
  tableDiv.appendChild(leftPanelDiv);
  var middlePanelDiv = functions.createElement({
    type: "div",
    className: "minerva-middle-panel"
  });
  tableDiv.appendChild(middlePanelDiv);

  var middlePanelContainerDiv = functions.createElement({type: "div"});
  middlePanelDiv.appendChild(middlePanelContainerDiv);

  var splitBar = functions.createElement({
    type: "div",
    className: "minerva-plugin-split-bar",
    content: "&nbsp"
  });
  tableDiv.appendChild(splitBar);

  var rightPanelDiv = functions.createElement({
    type: "div",
    className: "minerva-plugin"
  });
  tableDiv.appendChild(rightPanelDiv);

  var menuDiv = functions.createElement({
    type: "div",
    className: "minerva-top-menu"
  });
  middlePanelContainerDiv.appendChild(menuDiv);

  var mapDiv = functions.createElement({
    type: "div",
    className: "minerva-map"
  });
  middlePanelContainerDiv.appendChild(mapDiv);

  var legendDiv = functions.createElement({
    type: "div",
    name: "legendDiv",
    className: "minerva-legend",
    style: "display:none"
  });
  middlePanelContainerDiv.appendChild(legendDiv);

  var contextMenu = functions.createElement({
    type: "ul",
    name: "contextMenu"
  });
  element.appendChild(contextMenu);
}

/**
 *
 * @param {CustomMapOptions} params
 */
function initGlobals(params) {
  if (global.ServerConnector === undefined) {
    global.ServerConnector = ServerConnector;
    global.$ = global.jQuery = $;
    if (params.isDebug()) {
      logger.level = "debug";
    } else {
      logger.level = "info";
    }
    GuiConnector.init();
    if (GuiConnector.getParams['debug'] !== undefined) {
      logger.level = "debug";
    }
  } else {
    logger.warn("global ServerConnector found");
  }

}


/**
 *
 * @param {CustomMapOptions} params
 * @returns {Promise<Project>}
 */
function getProject(params) {
  if (params.getProject() !== undefined) {
    return Promise.resolve(params.getProject());
  } else {
    return ServerConnector.getProject(params.getProjectId());
  }
}

/**
 *
 * @param params
 * @returns {*}
 */
function modifyParamsForTouchTable(params) {
  if (params.bigLogo === undefined) {
    // noinspection UnnecessaryLocalVariableJS
    var isTouchTable = ((navigator.appVersion.indexOf("Win") > -1) && ('ontouchstart' in document.documentElement));
    params.bigLogo = isTouchTable;
  }
  return params;
}

/**
 *
 * @param {CustomMap} customMap
 * @param {PluginManager} pluginManager
 */
function assignSplitBarHandler(customMap, pluginManager) {
  var splitBar = $('.minerva-plugin-split-bar');
  var rightPanelDiv = $('.minerva-plugin');
  var mouseDownHandler = function (e) {
    e.preventDefault();
    //TODO should use global size (but element size)
    var x = $("body").width() - e.pageX;
    $(rightPanelDiv).css("flex", "0 0 " + x + "px");
    customMap.getMapCanvas().triggerListeners('resize');
    return pluginManager.callListeners("onResize");
  };
  $(splitBar).mousedown(function (e) {
    e.preventDefault();
    $(document).mousemove(mouseDownHandler);
  });
  $(document).mouseup(function () {
    $(document).unbind('mousemove', mouseDownHandler);
  });
}

/**
 *
 * @param {CustomMap} customMap
 */
function addUrlChangeListenersToCustomMap(customMap) {
  customMap.addListener("onSubmapOpen", function (event) {
    GuiConnector.setUrlParam("submap", event.arg.mapId.toString());
  });
  customMap.addListener("onSubmapClose", function (event) {
    GuiConnector.setUrlParam("submap", undefined);
  });
  var onCenterChangedHandler = function (event) {
    if (event.object.getId() !== customMap.getId()) {
      GuiConnector.setUrlParam("submap", event.object.getId().toString());
    } else {
      GuiConnector.setUrlParam("submap", "");
    }
    GuiConnector.setUrlParam("x", Math.round(event.arg.x).toString());
    // noinspection JSSuspiciousNameCombination
    GuiConnector.setUrlParam("y", Math.round(event.arg.y).toString());
    GuiConnector.setUrlParam("zoom", event.object.getZoom().toString());
  };

  var onZoomChangedHandler = function (event) {
    if (event.object.getId() !== customMap.getId()) {
      GuiConnector.setUrlParam("submap", event.object.getId().toString());
    } else {
      GuiConnector.setUrlParam("submap", "");
    }
    GuiConnector.setUrlParam("zoom", event.arg.toString());
  };

  customMap.addListener("onCenterChanged", onCenterChangedHandler);
  customMap.addListener("onZoomChanged", onZoomChangedHandler);

  customMap.getSubmaps().forEach(function (submap) {
    submap.addListener("onCenterChanged", onCenterChangedHandler);
    submap.addListener("onZoomChanged", onZoomChangedHandler);
  });

  customMap.getOverlayByName("search").addListener("onSearch", function (e) {
    GuiConnector.setUrlParam("search", undefined);
    GuiConnector.setUrlParam("searchCoordinates", undefined);
    if (e.arg.type === AbstractDbOverlay.QueryType.SEARCH_BY_COORDINATES) {
      // noinspection JSSuspiciousNameCombination
      GuiConnector.setUrlParam("searchCoordinates", Math.round(e.arg.coordinates.x) + "," + Math.round(e.arg.coordinates.y) + "," +
        e.arg.modelId + "," + e.arg.zoom);
    } else if (e.arg.type === AbstractDbOverlay.QueryType.SEARCH_BY_QUERY) {
      GuiConnector.setUrlParam("search", e.arg.query);
      GuiConnector.setUrlParam("search.perfect", e.arg.perfect);
    }
  });

  customMap.getOverlayByName("drug").addListener("onSearch", function (e) {
    if (e.arg.type === AbstractDbOverlay.QueryType.SEARCH_BY_QUERY) {
      GuiConnector.setUrlParam("drugSearch", e.arg.query);
    }
  });

  customMap.getOverlayByName("chemical").addListener("onSearch", function (e) {
    if (e.arg.type === AbstractDbOverlay.QueryType.SEARCH_BY_QUERY) {
      GuiConnector.setUrlParam("chemicalSearch", e.arg.query);
    }
  });


  var onOverlaysChangedHandler = function () {
    return customMap.getVisibleDataOverlays().then(function (dataOverlays) {
      var ids = [];
      for (var i = 0; i < dataOverlays.length; i++) {
        ids.push(dataOverlays[i].getId());
      }
      GuiConnector.setUrlParam("overlays", ids.join(","));
    })
  };

  customMap.addListener("onShowOverlay", onOverlaysChangedHandler);
  customMap.addListener("onHideOverlay", onOverlaysChangedHandler);

  customMap.addListener("onBackgroundOverlayChange", function (event) {
    GuiConnector.setUrlParam("background", event.arg);
  });


}

/**
 *
 * @param {TopMenu} topMenu
 */
function addUrlChangeListenersToTopMenu(topMenu) {
  topMenu.addListener("onShowCommentsToggle", function (event) {
    if (event.arg) {
      GuiConnector.setUrlParam("comments", "on");
    } else {
      GuiConnector.setUrlParam("comments", "");
    }
  });
}

/**
 *
 * @param {CustomMapOptions|*} params
 * @returns {Promise}
 */
function create(params) {
  params = modifyParamsForTouchTable(params);
  if (!(params instanceof CustomMapOptions)) {
    params = new CustomMapOptions(params);
  }
  params.setServerConnector(ServerConnector);
  initGlobals(params);
  params.getElement().innerHTML = "<div style='position:relative; width:100%;height:100%'>"
    + "<img src='resources/images/icons/ajax-loader.gif' style='position:absolute;top:0;bottom:0;left:0;right:0;margin:auto;'/>" + "</div>";

  // make sure that we are logged in
  return ServerConnector.createSession().then(function () {
    if (GuiConnector.getParams["oauthLogin"] === "success") {
      GuiConnector.setUrlParam("oauthLogin", null);
      ServerConnector.getSessionData(null).setLogin(null);
      return ServerConnector.isSessionValid();
    } else if (GuiConnector.getParams["oauthLogin"] === "failure") {
      GuiConnector.setUrlParam("oauthLogin", null);
      GuiConnector.warn("could not login using oauth");
    }
  }).then(function () {
    return ServerConnector.getConfiguration();
  }).then(function (configuration) {
    params.setConfiguration(configuration);
    var css = configuration.getOption(ConfigurationType.CUSTOM_CSS).getValue();
    if (css !== undefined && css !== null && css !== "") {
      var styleTag = $("<style/>");
      styleTag[0].innerHTML = css;
      $('head').append(styleTag);
    }
    return getProject(params);
  }).then(function (project) {
    if (project === null) {
      var message = "Project with given id doesn't exist.";
      message += "<p>Please go to <a href='" + ServerConnector.getServerBaseUrl() + "'>default map</a>";
      return Promise.reject(new ValidationError(message));
    }
    params.setProject(project);
    return processUrlGetParams(params);
  }).then(function () {

    var element = params.getElement();
    params.getElement().innerHTML = "";
    createDivStructure(element);
    params.setElement(functions.getElementByClassName(element, "minerva-map"));

    customMap = new CustomMap(params);


    new DbOverlayCollection({
      map: customMap
    });

    leftPanel = new LeftPanel({
      element: functions.getElementByClassName(element, "minerva-left-panel"),
      customMap: customMap,
      configuration: params.getConfiguration()
    });

    var pluginManager = new PluginManager({
      element: functions.getElementByClassName(element, "minerva-plugin"),
      customMap: customMap,
      project: params.getProject(),
      configuration: params.getConfiguration()
    });
    leftPanel.setPluginManager(pluginManager);
    assignSplitBarHandler(customMap, pluginManager);

    topMenu = new TopMenu({
      element: functions.getElementByClassName(element, "minerva-top-menu"),
      customMap: customMap,
      configuration: params.getConfiguration(),
      project: params.getProject()
    });

    legend = new Legend({
      element: functions.getElementByName(element, "legendDiv"),
      customMap: customMap
    });

    mapContextMenu = new MapContextMenu({
      element: functions.getElementByName(element, "contextMenu"),
      customMap: customMap,
      molArt: new MolArt(element, customMap)
    });
    customMap.setContextMenu(mapContextMenu);

    topMenu.setLegend(legend);
    topMenu.setLeftPanel(leftPanel);

    return customMap.init();
  }).then(function () {
    return leftPanel.init();
  }).then(function () {
    return legend.init();
  }).then(function () {
    return topMenu.init();
  }).then(function () {
    return mapContextMenu.init();
  }).then(function () {
    addUrlChangeListenersToCustomMap(customMap);
    addUrlChangeListenersToTopMenu(topMenu);

    var submapId = GuiConnector.getParams["submap"];
    if (submapId !== undefined && customMap.getSubmapById(submapId) !== null) {
      return customMap.openSubmap(submapId);
    }
  }).then(function () {
    return ServerConnector.getPluginsData();
  }).then(function (allPlugins) {
      var promises = [], i;
      for (i = 0; i < params.getPlugins().length; i++) {
        promises.push(leftPanel.getPluginManager().addPlugin(params.getPlugins()[i]))
      }
      var hashes = [];
      if (GuiConnector.getParams['plugins'] !== undefined) {
        hashes = GuiConnector.getParams['plugins'].split(",");
      }
      for (i = 0; i < allPlugins.length; i++) {
        if (hashes.indexOf(allPlugins[i].getHash()) < 0) {
          if (allPlugins[i].isDefault()) {
            hashes.push(allPlugins[i].getHash());
          }
        }
      }
      var loadedUrls = [];
      for (i = 0; i < hashes.length; i++) {
        if (hashes[i] !== '') {
          promises.push(ServerConnector.getPluginData(hashes[i]).then(function (plugin) {
            if (plugin !== null) {
              if (loadedUrls.indexOf(plugin.getUrls()[0]) < 0) {
                loadedUrls.push(plugin.getUrls()[0]);
                return leftPanel.getPluginManager().addPlugin({url: plugin.getUrls()[0]});
              }
            } else {
              GuiConnector.warn("Plugin list contains invalid object.");
            }
          }));
        }
      }

      return Promise.all(promises);
    }
  ).then(function () {
    return ServerConnector.getLoggedUser();
  }).then(function (user) {
    if (user.getLogin() !== "anonymous" && !user.isTermsOfUseConsent()) {
      requestConsent(user, params.getConfiguration().getOption(ConfigurationType.TERMS_OF_USE).getValue());
    }
    var result = {
      destroy: function () {
        return leftPanel.destroy().then(function () {
          customMap.destroy();
          return topMenu.destroy();
        }).then(function () {
          return GuiConnector.destroy();
        });
      },
      getProject: function () {
        return customMap.getProject();
      }
    };

    if (params.isDebug()) {
      result.leftPanel = leftPanel;
      result.customMap = customMap;
    }
    cookieConsent.showCookieConsent();
    return result;
  });
}

/**
 *
 * @returns {Promise}
 */
function createFooter() {
  return ServerConnector.getConfiguration().then(function (configuration) {
    var leftLogoLink = configuration.getOption(ConfigurationType.LEFT_LOGO_LINK).getValue();
    var leftLogoText = configuration.getOption(ConfigurationType.LEFT_LOGO_TEXT).getValue();
    var leftLogoImg = configuration.getOption(ConfigurationType.LEFT_LOGO_IMG).getValue();

    var rightLogoLink = configuration.getOption(ConfigurationType.RIGHT_LOGO_LINK).getValue();
    var rightLogoText = configuration.getOption(ConfigurationType.RIGHT_LOGO_TEXT).getValue();
    var rightLogoImg = configuration.getOption(ConfigurationType.RIGHT_LOGO_IMG).getValue();
    return functions.createElement({
      type: "div",
      className: "minerva-footer-table",
      content: '<table width="100%" border="0" cellspacing="0" cellpadding="0">' +
        '<tr>' +

        '<td align="left"><a href="' + leftLogoLink + '" title="' + leftLogoText + '" target="_blank">' +
        '<img src="' + leftLogoImg + '" class="minerva-logo" alt="' + leftLogoText + '"/>' +
        '</a></td>' +

        '<td align="center" class="minerva-footer-text">MiNERVA version ' + configuration.getVersion() + ';<br/>' +
        'build ' + configuration.getBuildDate() + ';<br/>' +
        'git: ' + configuration.getGitHash() + '</td>' +

        '<td align="right"><a href="' + rightLogoLink + '" title="' + rightLogoText + '" target="_blank">' +
        '<img src="' + rightLogoImg + '" class="minerva-logo" alt="' + rightLogoText + '"/>' +
        '</a></td>' +
        '</tr>\n' +
        '</table>', xss: false
    });
  });
}

/**
 *
 * @param {Configuration} configuration
 * @param {Map} ouathProviders
 * @return {HTMLElement}
 */
function createLoginDiv(configuration, ouathProviders) {
  var loggedIn = false;

  var result = functions.createElement({type: "center"});
  var resultDiv = functions.createElement({type: "div", className: "minerva-login-form", style: "text-align:left"});
  result.appendChild(resultDiv);
  resultDiv.appendChild(functions.createElement({
    type: "div",
    className: "minerva-login-form-title",
    content: "AUTHORIZATION FORM"
  }));
  resultDiv.appendChild(functions.createElement({
    type: "div",
    className: "minerva-login-caps-lock-warning",
    content: "Warning: Caps lock is on"
  }));
  GuiConnector.addListener("onCapsLockChange", function (e) {
    if (e.arg) {
      $(".minerva-login-caps-lock-warning").css("display", "block");
    } else {
      $(".minerva-login-caps-lock-warning").css("display", "none");
    }
  });
  var guiUtils = new GuiUtils();
  var table = functions.createElement({
    type: "div",
    className: "loginDataPanelGrid",
    style: "display: table;border-spacing:5px"
  });
  table.appendChild(guiUtils.createTableRow([
    functions.createElement({type: "label", className: "minerva-login-form-label", content: "LOGIN:"}),
    functions.createElement({type: "input", className: "minerva-input-text", id: "username"})]
  ));

  table.appendChild(guiUtils.createTableRow([
    functions.createElement({type: "label", className: "minerva-label", content: "PASSWORD:"}),
    functions.createElement({
      type: "input",
      inputType: "password",
      className: "minerva-input-password",
      id: "password"
    })]
  ));

  var div = $("<div>" +
    "<button class='minerva-label' id='login'>LOGIN</button>" +
    "<button class='minerva-reset-password-button'>RESET PASSWORD</button>" +
    "</div>");
  table.appendChild(guiUtils.createTableRow([
    functions.createElement({type: "label", className: "minerva-label"}),
    div[0]
  ]));

  $("button.minerva-reset-password-button", div).on('click', function () {
    var content = $('<div/>').html("<div>" +
      "<div class='display'>" +
      "<label style='margin-right: 20px'>Login:</label>" +
      "<input class='minerva-login'/>" +
      "</div>" +
      "<div class='minerva-menu-row'>" +
      "<button class='minerva-send-button'><span class='ui-icon ui-icon-mail-closed'></span>&nbsp;SEND</button>" +
      "<button class='minerva-cancel-button'><span class='ui-icon ui-icon-cancel'></span>&nbsp;CANCEL</button>" +
      "</div>" +
      "</div>");
    $(content).dialog({
      dialogClass: 'minerva-reset-password-dialog',
      title: "Reset password",
      width: 320,
      height: 120,
      modal: true,
      close: function () {
        $(content).dialog("destroy");
        dataTable.destroy();
      }
    });


    $(".minerva-send-button", content).on("click", function () {
      GuiConnector.showProcessing();
      return ServerConnector.requestResetPassword({login: $(".minerva-login", content).val()}).then(function () {
        GuiConnector.info("Request sent successfully");
      }).catch(function (error) {
        if (error instanceof NetworkError) {
          var content = JSON.parse(error.content);
          if (error.statusCode === HttpStatus.BAD_REQUEST) {
            GuiConnector.showErrorDialog("Cannot reset password", content.reason)
          } else {
            GuiConnector.showErrorDialog("Cannot reset password", content.reason + "<br/>Please contact system administrator.")
          }
        } else {
          GuiConnector.alert(error);
        }
      }).finally(function () {
        $(content).dialog("destroy");
        GuiConnector.hideProcessing();
      });
    });
    $(".minerva-cancel-button", content).on("click", function () {
      $(content).dialog("destroy");
    });
  });

  for (var key in ouathProviders) {
    if (ouathProviders.hasOwnProperty(key)) {
      var p = key.toLowerCase();
      /**
       * @var {string} r
       */
      var r = ouathProviders[key];

      (function (provider, redirect) {
        div = $("<div>" +
          "<button class='minerva-oauth-button oauth-" + provider + "' id='oauth-" + provider + "'>" +
          "<i class='minerva-oauth-button oauth-" + provider + "'/>" +
          " Sign in with " + key + "</button>" +
          "</div>");
        $(".minerva-oauth-button", div).on("click", function () {
          if (redirect.startsWith("/")) {
            redirect = redirect.substring(1);
          }
          window.location.href = ServerConnector.getServerBaseUrl() + redirect;
        });

        table.appendChild(guiUtils.createTableRow([
          functions.createElement({type: "label", className: "minerva-label"}),
          div[0]
        ]));

      })(p, r);
    }
  }


  resultDiv.appendChild(table);
  resultDiv.appendChild(functions.createElement({type: "br"}));
  resultDiv.appendChild(functions.createElement({type: "br"}));
  resultDiv.appendChild(functions.createElement({
    type: "a",
    href: "javascript:;",
    id: "go_to_map_button",
    className: "adminLink",
    content: '<i class="fa fa-chevron-right"></i> BACK TO MAP',
    xss: false
  }));
  $("#go_to_map_button", resultDiv).hide();

  resultDiv.appendChild(functions.createElement({type: "br"}));
  var requestAccountStyle = "";
  var signUpStyle = "display: none";
  if (configuration.getOption(ConfigurationType.REQUEST_ACCOUNT_EMAIL).getValue() === "" ||
    configuration.getOption(ConfigurationType.REQUEST_ACCOUNT_EMAIL).getValue() === undefined ||
    configuration.getOption(ConfigurationType.ALLOW_AUTO_REGISTER).getValue().toLowerCase() === "true"
  ) {
    requestAccountStyle = "display: none";
  }
  if (configuration.getOption(ConfigurationType.ALLOW_AUTO_REGISTER).getValue().toLowerCase() === "true") {
    signUpStyle = "";
  }

  resultDiv.appendChild(functions.createElement({
    type: "a",
    href: "javascript:;",
    id: "register_button",
    className: "adminLink",
    style: requestAccountStyle,
    content: '<i class="fa fa-chevron-right"></i> REQUEST AN ACCOUNT</a>',
    xss: false
  }));

  resultDiv.appendChild(functions.createElement({
    type: "a",
    href: "javascript:;",
    id: "signup_button",
    className: "adminLink",
    style: signUpStyle,
    content: '<i class="fa fa-chevron-right"></i> SIGN UP</a>',
    xss: false
  }));

  var fromPage = GuiConnector.getParams["from"];

  $("#username", result).keypress(function (event) {
    if (event.which === 13) {
      $("#password", result).focus();
    }
  });
  $("#password", result).keypress(function (event) {
    if (event.which === 13) {
      $("#login", result).click();
    }
  });

  $("#login", result).on("click", function login() {
    GuiConnector.showProcessing();
    var loginString = document.getElementById('username').value;
    var passwordString = document.getElementById('password').value;
    return ServerConnector.login(loginString, passwordString).then(function () {
      loggedIn = true;
      if (fromPage !== undefined) {
        window.location.href = fromPage;
      } else {
        window.location.href = ServerConnector.getServerBaseUrl();
      }
    }).catch(function (error) {
      if (error.constructor.name === "InvalidCredentialsError") {
        GuiConnector.alert("invalid credentials");
      } else {
        GuiConnector.alert(error);
      }
    }).finally(function () {
      GuiConnector.hideProcessing();
    });
  });
  $('#go_to_map_button', result).click(function () {
    return ServerConnector.getProjectId().then(function (projectId) {
      window.location.href = ServerConnector.getServerBaseUrl() + 'index.xhtml?id=' + projectId;
    });
  });
  $('#register_button', result).click(function () {
    var email, content, title;
    return ServerConnector.getConfigurationParam(ConfigurationType.REQUEST_ACCOUNT_EMAIL).then(function (result) {
      email = result;
      return ServerConnector.getConfigurationParam(ConfigurationType.REQUEST_ACCOUNT_DEFAULT_TITLE);
    }).then(function (result) {
      title = result;
      return ServerConnector.getConfigurationParam(ConfigurationType.REQUEST_ACCOUNT_DEFAULT_CONTENT);
    }).then(function (result) {
      content = encodeURIComponent(result);
      document.location.href = 'mailto:' + email + '?subject=' + title + '&body=' + content;
    });
  });

  $("#signup_button", result).on('click', function () {
    var content = $('<div/>').html("<div>" +
      "<div class='minerva-menu-row'>" +
      "<label style='margin-right: 20px; width: 120px'>Email:</label>" +
      "<input class='minerva-email'/>" +
      "</div>" +
      "<div class='minerva-menu-row'>" +
      "<label style='margin-right: 20px; width: 120px'>Password:</label>" +
      "<input class='minerva-password' type='password'/>" +
      "</div>" +
      "<div class='minerva-menu-row'>" +
      "<label style='margin-right: 20px; width: 120px'>Name:</label>" +
      "<input class='minerva-name'/>" +
      "</div>" +
      "<div class='minerva-menu-row'>" +
      "<label style='margin-right: 20px; width: 120px'>Surname:</label>" +
      "<input class='minerva-surname'/>" +
      "</div>" +
      "<div class='minerva-menu-row'>" +
      "<button class='minerva-send-button'><span class='ui-icon ui-icon-mail-closed'></span>&nbsp;SIGNUP</button>" +
      "<button class='minerva-cancel-button'><span class='ui-icon ui-icon-cancel'></span>&nbsp;CANCEL</button>" +
      "</div>" +
      "</div>");
    $(content).dialog({
      dialogClass: 'minerva-sign-up-dialog',
      title: "Sign up",
      width: 400,
      height: 200,
      modal: true,
      close: function () {
        $(content).dialog("destroy");
      }
    });


    $(".minerva-send-button", content).on("click", function () {
      GuiConnector.showProcessing();
      ServerConnector.signUp({
        "email": $('.minerva-email', content).val(),
        "name": $('.minerva-name', content).val(),
        "surname": $('.minerva-surname', content).val(),
        "password": $('.minerva-password', content).val()
      }).then(function () {
        GuiConnector.info("We have sent a confirmation email to the provided address.");
        $(content).dialog("destroy");
      }).catch(function (error) {
        if (error instanceof NetworkError && error.statusCode === HttpStatus.BAD_REQUEST) {
          var message = error.content;
          if (error.content !== undefined) {
            message = error.content.reason;
          }
          GuiConnector.showErrorDialog("Error", message);
        } else {
          GuiConnector.alert(error);
        }
      }).finally(function () {
        GuiConnector.hideProcessing();
      })
    });
    $(".minerva-cancel-button", content).on("click", function () {
      $(content).dialog("destroy");
    });
  });


  if (GuiConnector.getParams["resetPasswordToken"] !== undefined) {
    var content = $('<div/>').html("<div>" +
      "<div class='display'>" +
      "<label style='width: 120px'>Password:</label>" +
      "<input type='password' class='minerva-password' autocomplete='new-password'/>" +
      "</div>" +
      "<div class='display'>" +
      "<label style='width: 120px'>Repeat password:</label>" +
      "<input type='password' class='minerva-password-2' autocomplete='new-password'/>" +
      "</div>" +
      "<div class='minerva-menu-row'>" +
      "<button class='minerva-save-button' disabled='disabled'><span class='ui-icon ui-icon-disk'></span>&nbsp;SAVE</button>" +
      "<button class='minerva-cancel-button'><span class='ui-icon ui-icon-cancel'></span>&nbsp;CANCEL</button>" +
      "</div>" +
      "</div>");
    $(content).dialog({
      dialogClass: 'minerva-reset-password-dialog',
      title: "Reset password",
      width: 400,
      height: 150,
      modal: true,
      close: function () {
        $(content).dialog("destroy");
      }
    });

    $('.minerva-password, .minerva-password-2', content).on('keyup', function () {
      if ($('.minerva-password', content).val() === $('.minerva-password-2', content).val()) {
        if ($('.minerva-password', content).val() !== "") {
          $(".minerva-save-button", content).prop('disabled', false);
        } else {
          $(".minerva-save-button", content).prop('disabled', true);
        }
      } else {
        $(".minerva-save-button", content).prop('disabled', true);
      }
    });


    $(".minerva-save-button", content).on("click", function () {
      GuiConnector.showProcessing();
      return ServerConnector.resetPassword({
        password: $(".minerva-password", content).val(),
        token: GuiConnector.getParams["resetPasswordToken"]
      }).then(function () {
        GuiConnector.info("Password reset successfully");
      }).catch(function (error) {
        if (error instanceof NetworkError) {
          var content = JSON.parse(error.content);
          if (error.statusCode === HttpStatus.NOT_FOUND) {
            GuiConnector.showErrorDialog("Cannot reset password", content.reason)
          } else {
            GuiConnector.showErrorDialog("Cannot reset password", content.reason + "<br/>Please contact system administrator.")
          }
        } else {
          GuiConnector.alert(error);
        }
      }).finally(function () {
        $(content).dialog("destroy");
        GuiConnector.hideProcessing();
      });
    });

  }

  return result;
}

/**
 *
 * @param {CustomMapOptions|*} params
 * @returns {Promise}
 */
function createLogin(params) {
  params = modifyParamsForTouchTable(params);
  if (!(params instanceof CustomMapOptions)) {
    params = new CustomMapOptions(params);
  }

  initGlobals(params);
  var configuration;
  // make sure that we are logged in
  return new Promise.resolve().then(function () {
    if (GuiConnector.getParams["confirmEmailToken"] !== undefined) {
      return ServerConnector.confirmEmail(GuiConnector.getParams["login"], GuiConnector.getParams["confirmEmailToken"]).then(function (message) {
        GuiConnector.info(message);
      }).catch(function (e) {
        var response = e.content;
        if (response !== undefined) {
          GuiConnector.showErrorDialog("Cannot confirm email.", JSON.parse(response).reason);
        } else {
          GuiConnector.showErrorDialog(e);
        }
      });
    }
  }).then(function () {
    return ServerConnector.createSession();
  }).then(function () {
    return ServerConnector.getConfiguration();
  }).then(function (result) {
    configuration = result;
    return ServerConnector.getOauthProviders();
  }).then(function (oauthProviders) {
    var loginDiv = createLoginDiv(configuration, oauthProviders);
    params.getElement().appendChild(loginDiv);
    return createFooter();
  }).then(function (footer) {
    params.getElement().appendChild(footer);
    cookieConsent.showCookieConsent();
  }).then(function () {
    return ServerConnector.getProject().then(function () {
      $("#go_to_map_button").show();
    }).catch(function (e) {
      if (e instanceof SecurityError) {
      } else {
        throw e;
      }
    })
  })
}

/**
 *
 * @param {CustomMapOptions|*} params
 * @returns {Promise}
 */
function createExport(params) {
  params = modifyParamsForTouchTable(params);
  if (!(params instanceof CustomMapOptions)) {
    params = new CustomMapOptions(params);
  }
  initGlobals(params);
  params.getElement().style.display = "table";
  params.getElement().innerHTML = "<div style='vertical-align:middle;display:table-cell;text-align: center'>"
    + "<img src='resources/images/icons/ajax-loader.gif'/>" + "</div>";

  var result;
  // make sure that we are logged in
  return ServerConnector.getConfiguration().then(function (configuration) {
    params.setConfiguration(configuration);
    params.setServerConnector(ServerConnector);
    return getProject(params);
  }).then(function (project) {
    params.setProject(project);
    result = new Export(params);
    return result.init();
  }).then(function () {
    cookieConsent.showCookieConsent();
    return result;
  });
}

/**
 *
 * @param {CustomMapOptions|*} params
 * @returns {Promise}
 */
function createAdmin(params) {
  params = modifyParamsForTouchTable(params);
  if (!(params instanceof CustomMapOptions)) {
    params = new CustomMapOptions(params);
  }
  params.setServerConnector(ServerConnector);
  initGlobals(params);
  params.getElement().style.display = "table";
  params.getElement().innerHTML = "<div style='vertical-align:middle;display:table-cell;text-align: center'>"
    + "<img src='resources/images/icons/ajax-loader.gif'/>" + "</div>";

  var result;
  // make sure that we are logged in
  return ServerConnector.createSession().then(function () {
    if (ServerConnector.getSessionData(null).getLogin() === "anonymous" ||
      ServerConnector.getSessionData(null).getLogin() === null ||
      ServerConnector.getSessionData(null).getLogin() === undefined) {
      window.location.href = ServerConnector.getServerBaseUrl() + "login.xhtml?from=" + encodeURI(window.location.href);
      return Promise.resolve();
    }
    return ServerConnector.getConfiguration();
  }).then(function (configuration) {
    if (configuration !== undefined) {
      params.setConfiguration(configuration);
      result = new Admin(params);
      return createFooter().then(function (footer) {
        params.getElement().appendChild(footer);
        return result.init();
      }).then(function () {
        cookieConsent.showCookieConsent();
        return result;
      });
    }
  });
}

var minerva = {
  create: create,
  createLogin: createLogin,
  createExport: createExport,
  createAdmin: createAdmin,
  ServerConnector: ServerConnector,
  GuiConnector: GuiConnector,
  $: $
};

module.exports = minerva;
