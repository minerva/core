"use strict";

var MinervaPluginProxy = require('./MinervaPluginProxy');
var ObjectWithListeners = require('../ObjectWithListeners');

var Promise = require("bluebird");

var logger = require('../logger');
var Functions = require('../Functions');
var GuiConnector = require('../GuiConnector');
var PluginData = require('../map/data/PluginData');

var md5 = require('md5');

var pluginId = 0;

/**
 * @typedef {Object} PluginOptions
 * @property {CustomMap} map
 * @property {Configuration} configuration
 * @property {ServerConnector} serverConnector
 * @property {HTMLElement} element
 * @property {string} [url]
 */

/**
 * @typedef {Object} UserPluginObject
 * @property {function(Object):void} register
 * @property {function():void} unregister
 * @property {function(Object):void} [notifyError]
 * @property {function():string} getName
 * @property {function():string} getVersion
 * @property {function():(number|string)|number|string} minWidth
 * @property {function():(number|string)|number|string} defaultWidth
 */

/**
 *
 * @param {PluginOptions} options
 * @constructor
 */
function Plugin(options) {
  ObjectWithListeners.call(this);
  var self = this;
  /**
   *
   * @type {string}
   * @private
   */
  self._hash = '';
  self.setOptions(options);
  self.registerListenerType("onUnload");
  self.registerListenerType("onResize");
}

Plugin.prototype = Object.create(ObjectWithListeners.prototype);
Plugin.prototype.constructor = ObjectWithListeners;

/**
 *
 * @param {PluginOptions} options
 */
Plugin.prototype.setOptions = function (options) {
  this._options = options;
};

/**
 *
 * @returns {PluginOptions}
 */
Plugin.prototype.getOptions = function () {
  return this._options;
};

/**
 *
 * @param loadedPluginData
 */
Plugin.prototype.setLoadedPluginData = function (loadedPluginData) {
  this._loadedPluginData = loadedPluginData;
};

/**
 *
 * @returns {?null|UserPluginObject}
 */
Plugin.prototype.getLoadedPluginData = function () {
  return this._loadedPluginData;
};

/**
 *
 * @param {MinervaPluginProxy} minervaPluginProxy
 */
Plugin.prototype.setMinervaPluginProxy = function (minervaPluginProxy) {
  this._minervaPluginProxy = minervaPluginProxy;
};

/**
 *
 * @returns {MinervaPluginProxy}
 */
Plugin.prototype.getMinervaPluginProxy = function () {
  return this._minervaPluginProxy;
};

/**
 *
 * @returns {string}
 */
Plugin.prototype.getPluginId = function () {
  if (this.getMinervaPluginProxy() === undefined) {
    logger.warn("Plugin is not loaded properly");
    return undefined;
  }
  return this.getMinervaPluginProxy().pluginId;
};

/**
 *
 * @returns {Promise|PromiseLike}
 */
Plugin.prototype.load = function () {
  var self = this;
  var options = self.getOptions();

  var hash;
  var error = false;
  var registerPromise = null;

  return options.serverConnector.sendRequest({
    url: options.url,
    description: "Loading plugin: " + options.url,
    method: "GET"
  }).then(function (content) {
    hash = md5(content);
    var pluginData = undefined;
    try {
      // noinspection JSUnusedLocalSymbols
      var minervaDefine = function (pluginFunction) {
        try {
          if (typeof pluginFunction === "function") {
            pluginData = pluginFunction();
          } else {
            pluginData = pluginFunction;
          }

          var minervaPluginProxy = new MinervaPluginProxy({
            hash: hash,
            map: options.map,
            configuration: options.configuration,
            element: options.element,
            plugin: self,
            pluginId: "plugin" + (pluginId++),
            serverConnector: options.serverConnector
          });
          self.setLoadedPluginData(pluginData);
          self.setMinervaPluginProxy(minervaPluginProxy);
          registerPromise = new Promise(function (resolve) {
            resolve(pluginData.register(minervaPluginProxy));
          });
        } catch (e) {
          error = e;
        }
      };
      content += "//# sourceURL=" + options.url;
      eval(content);
    } catch (e) {
      error = e;
    }
    return options.serverConnector.registerPlugin(new PluginData({
      hash: hash,
      urls: [options.url],
      name: self.getName(),
      version: self.getVersion()
    }));
  }).then(function () {
    self._hash = hash;
    if (error) {
      return Promise.reject(error);
    }
    return registerPromise;
  });

};

/**
 *
 * @returns {number}
 */
Plugin.prototype.getMinWidth = function () {
  var value;
  var data = this.getLoadedPluginData();
  if (data === undefined || data === null) { //this can happen when plugin is added to list of plugins but data was not loaded yet
    logger.warn("[" + this.getName() + "] Plugin data not available. ");
  } else {
    if (data.minWidth !== undefined) {
      if (typeof data.minWidth === "function") {
        value = parseInt(data.minWidth());
      } else {
        value = parseInt(data.minWidth);
      }
    }
  }
  return value;
};

/**
 *
 * @returns {number}
 */
Plugin.prototype.getDefaultWidth = function () {
  var value;
  var data = this.getLoadedPluginData();
  if (data.defaultWidth !== undefined) {
    if (typeof data.defaultWidth === "function") {
      value = parseInt(data.defaultWidth());
    } else {
      value = parseInt(data.defaultWidth);
    }
  }
  return value;
};

/**
 *
 * @returns {Promise}
 */
Plugin.prototype.unload = function () {
  var self = this;
  var validPlugin = true;
  if (self.getLoadedPluginData() === undefined) {
    validPlugin = false;
    logger.warn("Plugin wasn't loaded properly");
  }
  return Promise.resolve().then(function () {
    if (validPlugin) {
      return self.getLoadedPluginData().unregister();
    }
  }).catch(function (error) {
    GuiConnector.warn("Plugin crashed on upload.");
    if (typeof self.getLoadedPluginData().notifyError === "function") {
      self.getLoadedPluginData().notifyError({error: error});
    }
  }).then(function () {
    if (validPlugin) {
      var removedListeners = self.getMinervaPluginProxy().project.map.removeAllListeners();
      if (removedListeners.length > 0) {
        logger.warn("'" + self.getLoadedPluginData().getName() + "' plugin didn't remove all registered listeners");
      }
    }
    return self.callListeners("onUnload");
  }).then(function () {
    return self.destroy();
  });
};

/**
 *
 * @returns {string}
 */
Plugin.prototype.getName = function () {
  if (this.getLoadedPluginData() === undefined) {
    return "";
  }
  return this.getLoadedPluginData().getName();
};

/**
 *
 * @returns {string}
 */
Plugin.prototype.getHash = function () {
  return this._hash;
};

/**
 *
 * @returns {string}
 */
Plugin.prototype.getVersion = function () {
  if (this.getLoadedPluginData() === undefined) {
    return "";
  }
  return this.getLoadedPluginData().getVersion();
};

/**
 *
 * @returns {Promise}
 */
Plugin.prototype.destroy = function () {
  return Promise.resolve();
};

module.exports = Plugin;
