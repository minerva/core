"use strict";

var $ = require('jquery');
var AbstractGuiElement = require('../gui/AbstractGuiElement');
var Plugin = require('./Plugin');
var GuiUtils = require('../gui/leftPanel/GuiUtils');
var GuiConnector = require('../GuiConnector');
var InvalidArgumentError = require('../InvalidArgumentError');
var isUrl = require('is-url');

var Promise = require("bluebird");

// noinspection JSUnusedLocalSymbols
var logger = require('../logger');
var Functions = require('../Functions');

/**
 *
 * @param options
 * @param {HTMLElement} options.element
 * @param {CustomMap} options.customMap
 * @param {Configuration} options.configuration
 * @param {Project} options.project
 * @param {ServerConnector} [options.serverConnector]
 * @constructor
 * @extends {AbstractGuiElement}
 */
function PluginManager(options) {
  AbstractGuiElement.call(this, options);
  var self = this;
  /**
   * @type Plugin[]
   */

  self._plugins = [];

  self.registerListenerType("onResize");
  self.addListener("onResize", function () {
    var promises = [];
    for (var i = 0; i < self._plugins.length; i++) {
      promises.push(self._plugins[i].callListeners("onResize"));
    }
    return Promise.resolve(promises);
  });
}

PluginManager.prototype = Object.create(AbstractGuiElement.prototype);
PluginManager.prototype.constructor = AbstractGuiElement;

// noinspection JSUnusedGlobalSymbols
/**
 *
 * @returns {Plugin[]}
 */
PluginManager.prototype.getPlugins = function () {
  return this._plugins;
};

/**
 *
 * @returns {GuiUtils}
 */
PluginManager.prototype.getGuiUtils = function () {
  if (this._guiUtils === undefined) {
    this._guiUtils = new GuiUtils(this.getConfiguration());
  }
  return this._guiUtils;
};

/**
 *
 * @param {Plugin|{url:string}} options
 * @returns {Promise|PromiseLike}
 */
PluginManager.prototype.addPlugin = function (options) {
  var self = this;
  var oldLinkHeight = $(".nav-tabs", self.getElement()).height();
  $(self.getElement()).show();
  if (self._panels === undefined) {
    self.getGuiUtils().initTabContent(self);
    self._adjustHeight = function () {
      var tabs = $(".parentTabs > .tab-content > .tab-pane", self.getElement());
      if (tabs.length > 0) {
        tabs.css('height', 'calc( 100vh - ' + $(".parentTabs > .tab-content", self.getElement()).offset().top + 'px )');
      }
    };
    GuiConnector.addWindowResizeEvent(self._adjustHeight);
  }
  var element = Functions.createElement({type: "div"});

  self.getGuiUtils().addTab(self, {name: "PLUGIN", content: element});

  var plugin;
  return Promise.resolve().then(function () {
    if (options instanceof Plugin) {
      plugin = options;
      plugin.setOptions({
        element: element,
        configuration: self.getConfiguration(),
        map: self.getMap(),
        serverConnector: self.getServerConnector()
      });
      self._plugins.push(plugin);
    } else {
      plugin = new Plugin({
        url: options.url,
        element: element,
        configuration: self.getConfiguration(),
        map: self.getMap(),
        serverConnector: self.getServerConnector()
      });
      plugin.addListener("onUnload", function () {
        self.getGuiUtils().removeTab(self, element);
      });
      self._plugins.push(plugin);
      if (!self.isValidUrl(options.url)) {
        return Promise.reject(new InvalidArgumentError("url: '" + options.url + "' is invalid"));
      }
    }
    return plugin.load();
  }).then(function () {
      $("a[href='#" + element.parentNode.id + "']", $(self.getElement()))[0].innerHTML = plugin.getName();
      if ($(self.getElement()).width() < plugin.getDefaultWidth()) {
        $(self.getElement()).css('flex', "0 0 " + plugin.getDefaultWidth() + "px");
      }
      self._adjustHeight();
      return self.adjustMinWidth();
    }
  ).then(function () {
    var newLinkHeight = $(".nav-tabs", self.getElement()).height();
    if (self._plugins.length === 1) {
      return self.getMap().getMapCanvas().triggerListeners("resize");
    } else if (oldLinkHeight !== newLinkHeight) {
      return self.getMap().getMapCanvas().triggerListeners("resize");
    }
  }).then(function () {
    GuiConnector.setUrlParam("plugins", self._getLoadedPluginsAsHashList());
    return plugin;
  }).catch(function (e) {
    return self.removePlugin(plugin).then(function () {
      GuiConnector.alert("Problem with loading plugin:<br/>" + e.message);
    });
  });
}
;

/**
 *
 * @param {string} url
 * @returns {boolean}
 */
PluginManager.prototype.isValidUrl = function (url) {
  return isUrl(url);
};

/**
 *
 * @return {string}
 * @private
 */
PluginManager.prototype._getLoadedPluginsAsHashList = function () {
  var self = this;
  var hashes = '';
  for (var i = 0; i < self._plugins.length; i++) {
    if (hashes !== '') {
      hashes += ",";
    }
    hashes += self._plugins[i].getHash();
  }
  if (hashes === '') {
    hashes = undefined;
  }
  return hashes;
};

/**
 *
 * @returns {Promise|PromiseLike}
 */
PluginManager.prototype.adjustMinWidth = function () {
  var self = this;
  var oldVal = self.getElement().style.minWidth;
  var minWidth = 150;
  var i;
  for (i = 0; i < self._plugins.length; i++) {
    var plugin = self._plugins[i];

    var value = plugin.getMinWidth();
    if (value > minWidth) {
      minWidth = value;
    }
  }
  var newVal = minWidth + "px";
  self.getElement().style.minWidth = newVal;
  if (newVal !== oldVal) {
    return self.callListeners("onResize");
  } else {
    return Promise.resolve();
  }
};

/**
 *
 * @param {Plugin} plugin
 * @returns {Promise}
 */
PluginManager.prototype.removePlugin = function (plugin) {
  var self = this;
  var found = false;
  for (var i = 0; i < self._plugins.length; i++) {
    if (plugin === self._plugins[i]) {
      self._plugins.splice(i, 1);
      found = true;
    }
  }
  if (!found) {
    return Promise.reject(new Error("Plugin not registered"));
  }
  return plugin.unload().then(function () {
    GuiConnector.setUrlParam("plugins", self._getLoadedPluginsAsHashList());
    if (self._plugins.length === 0) {
      $(self.getElement()).hide();
      return self.getMap().getMapCanvas().triggerListeners("resize");
    } else {
      self._adjustHeight();
      return self.adjustMinWidth();
    }
  });
};

/**
 *
 * @returns {Promise}
 */
PluginManager.prototype.destroy = function () {
  var self = this;
  var promises = [];
  var plugins = self._plugins;
  for (var i = plugins.length - 1; i >= 0; i--) {
    promises.push(self.removePlugin(plugins[i]));
  }
  return Promise.all(promises);
};


module.exports = PluginManager;
