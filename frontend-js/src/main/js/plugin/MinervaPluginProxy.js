"use strict";

var $ = require('jquery');

var Annotation = require('../map/data/Annotation');
var Chemical = require('../map/data/Chemical');
var Drug = require('../map/data/Drug');
var IdentifiedElement = require('../map/data/IdentifiedElement');
var UserDbOverlay = require('../map/overlay/UserDbOverlay');
var SearchDbOverlay = require('../map/overlay/SearchDbOverlay');
var LayoutAlias = require('../map/data/LayoutAlias');
var DataOverlay = require('../map/data/DataOverlay');
var Configuration = require('../Configuration');

var Bounds = require('../map/canvas/Bounds');
var Point = require('../map/canvas/Point');

var GuiConnector = require('../GuiConnector');

var TextEncoder = require('text-encoding').TextEncoder;

// noinspection JSUnusedLocalSymbols
var logger = require('../logger');

var Promise = require("bluebird");

/**
 * @typedef {Object} PluginElementParam
 * @property {IdentifiedElement} element
 * @property {string} [icon]
 * @property {Object} [options]
 * @property {string} options.color
 * @property {number} options.opacity
 * @property {string} options.lineColor
 * @property {number} options.lineWeight
 * @property {number} options.lineOpacity
 */

/**
 * @typedef {Object} PluginProjectData
 *
 * @property {function(*=): Promise<any>} getBioEntityById
 * @property {function(): *} getAllBioEntities
 * @property {function(*=): *} getReactionsWithElement
 * @property {function(): string} getProjectId
 * @property {function(): string} getName
 * @property {function(): string} getVersion
 * @property {function(): *} getDisease
 * @property {function(): *} getOrganism
 * @property {function(): *[]} getModels
 * @property {function(): *[]} getOverviewImages
 * @property {function(): DataOverlay[]} getDataOverlays
 */

/**
 * @typedef {Object} PluginProjectMap
 *
 * @property {function(): (Promise<DataOverlay[]>|*)} getVisibleDataOverlays
 * @property {function(number|DataOverlay): (Promise<DataOverlay[]>|*)} showDataOverlay
 * @property {function(number|DataOverlay): (Promise<DataOverlay[]>|*)} hideDataOverlay
 * @property {function({id: number}): void} showOverviewImage
 * @property {function(): void} hideOverviewImage
 * @property {function(*)} addListener
 * @property {function(*)} removeListener
 * @property {function(): Array} removeAllListeners
 * @property {(function(*=): Promise<Array|never>)} getHighlightedBioEntities
 * @property {(function(*=): Promise<any|never>)} hideBioEntity
 * @property {(function({modelId: number, x: number, y: number}): Promise|PromiseLike)} setCenter
 * @property {(function(*): Point)} getCenter
 * @property {(function(*): Bounds)} getBounds
 * @property {(function(*): void)} fitBounds
 * @property {(function({modelId: number, zoom: number}): Promise|PromiseLike)} setZoom
 * @property {(function(*): number)} getZoom
 * @property {(function(*): PromiseLike)} openMap
 * @property {function({dbOverlayName: string, [query]:string, [perfect]: boolean, [fitBounds]:boolean, [coordinates]:Point, [zoom]:number}, [modelId]:number): Promise|PromiseLike} triggerSearch
 * @property {function(): Promise|PromiseLike} clear
 */


/**
 *
 * @param {CustomMap} customMap
 * @param {string} dbOverlayName
 * @returns {AbstractDbOverlay}
 */
function getOverlayByName(customMap, dbOverlayName) {
  var dbOverlay = customMap.getOverlayByName(dbOverlayName);
  if (dbOverlay === null) {
    var validOverlays = "";
    var overlays = customMap.getDbOverlays();
    for (var overlay in overlays) {
      if (overlays.hasOwnProperty(overlay)) {
        validOverlays += overlay.getName() + ", ";
      }
    }
    throw new Error("Invalid DbOverlay: " + dbOverlayName + ". Valid DbOverlays: " + validOverlays);
  } else {
    return dbOverlay;
  }
}

/**
 *
 * @param {CustomMap} customMap
 * @param {IdentifiedElement[]} identifiedElements
 * @returns {Promise}
 */
function getFullElements(customMap, identifiedElements) {
  var result = [];
  return Promise.each(
    identifiedElements,
    function (item) {
      if (item.length === undefined) {
        if (item instanceof IdentifiedElement) {
          return customMap.getSubmapById(item.getModelId()).getModel().getByIdentifiedElement(item, true).then(
            function (fullElement) {
              result.push(fullElement);
            });
        } else if (item instanceof Drug) {
          result.push(new Drug(item));
        } else if (item instanceof Chemical) {
          result.push(new Chemical(item));
        } else {
          return Promise.reject(new Error("Unknown object type: " + item.constructor.name));
        }
      } else {
        return getFullElements(customMap, item).then(function (resultRow) {
          result.push(resultRow);
        });
      }
    }).then(function () {
    return result;
  });
}

/**
 *
 * @param {IdentifiedElement[]} elementIdentifiers
 * @param {CustomMap} customMap
 * @returns {Promise}
 */
function getElements(elementIdentifiers, customMap) {
  var identifiedElements = [];

  var elementsByModelId = [];
  for (var i = 0; i < elementIdentifiers.length; i++) {
    var identifiedElement = new IdentifiedElement(elementIdentifiers[i]);
    if (elementsByModelId[identifiedElement.getModelId()] === undefined) {
      elementsByModelId[identifiedElement.getModelId()] = [];
    }
    elementsByModelId[identifiedElement.getModelId()].push(identifiedElement);
    identifiedElements.push(identifiedElement);
  }

  var modelScopePromises = [];
  for (var key in elementsByModelId) {
    if (elementsByModelId.hasOwnProperty(key)) {
      var model = customMap.getProject().getModelById(parseInt(key));
      modelScopePromises.push(model.getByIdentifiedElements(elementsByModelId[key], true));
    }
  }
  // first promise fetch all data
  return Promise.all(modelScopePromises).then(function () {
    // this promise return result in the right order
    var elementPromises = [];
    for (var i = 0; i < identifiedElements.length; i++) {
      var element = identifiedElements[i];
      var model = customMap.getProject().getModelById(element.getModelId());
      var promise = model.getByIdentifiedElement(element, true);
      elementPromises.push(promise);
    }
    return Promise.all(elementPromises);
  });
}


/**
 *
 * @param {Object} options
 * @param {CustomMap} options.map
 * @returns {PluginProjectData}
 */
function createProjectData(options) {
  var map = options.map;
  return {
    getBioEntityById: function (param) {
      var isArray = true;
      if (param.length === undefined) {
        param = [param];
        isArray = false;
      }
      return getElements(param, map).then(function (result) {
        if (!isArray) {
          return result[0];
        } else {
          return result;
        }
      });
    },
    getDataOverlays: function () {
      return map.getProject().getDataOverlays();
    },
    /**
     *
     * @param {number} overlayId
     * @return {PromiseLike|Promise}
     */
    removeDataOverlay: function (overlayId) {
      return map.getServerConnector().removeOverlay({overlayId: overlayId, projectId: map.getProject().getProjectId()});
    },
    /**
     *
     * @param {string} param.name
     * @param {string} param.content
     * @param {string} param.description
     * @param {string|null} [param.type]
     * @param {string} [param.filename]
     *
     *
     *
     *
     * @return {*}
     */
    addDataOverlay: function (param) {
      if (param.type === undefined || param.type === null) {
        param.type = LayoutAlias.GENERIC;
      }
      if (param.filename === undefined || param.filename === "") {
        param.filename = "unknown.txt";
      }
      var overlay = new DataOverlay({
        name: param.name,
        description: param.description,
        filename: param.filename,
        type: param.type
      });

      GuiConnector.showProcessing();
      return map.getServerConnector().uploadFile({
        filename: param.filename,
        content: new TextEncoder().encode(param.content)
      }).then(function (file) {
        return map.getServerConnector().addOverlay({
          fileId: file.id,
          overlay: overlay,
          projectId: map.getProject().getProjectId()
        });
      }).finally(GuiConnector.hideProcessing);
    },

    /**
     * Export part of the map that includes bioEntities into a known format.
     *
     * @param {Object} param
     * @param {string} param.handlerClass format defined in {Configuration#getModelConverters}
     * @param {number} param.modelId id of the model
     * @param {Array} param.bioEntities
     * @returns {Promise}
     */
    exportToMapFormat: function (param) {
      var exportParams = {
        projectId: map.getProject().getProjectId(),
        modelId: param.modelId,
        handlerClass: param.handlerClass,
        elementIds: [],
        reactionIds: []
      };
      for (var i = 0; i < param.bioEntities.length; i++) {
        var ie = new IdentifiedElement(param.bioEntities[i]);
        if (ie.getType() === "ALIAS") {
          exportParams.elementIds.push(ie.getId());
        } else if (ie.getType() === "REACTION") {
          exportParams.reactionIds.push(ie.getId());
        } else {
          return Promise.reject(new Error("Unsupported bioentity type: " + ie.getType()));
        }
        if (ie.getModelId() !== param.modelId) {
          return Promise.reject(new Error("Export of bioEntities from few maps is not supported. ModelId: " + param.modelId + "; element modelId: " + ie.getModelId()));
        }
      }
      return options.map.getServerConnector().getPartOfModelInExportFormat(exportParams);
    },
    getAllBioEntities: function () {
      var models = map.getProject().getModels();
      var result = [];
      var promises = [];
      for (var i = 0; i < models.length; i++) {
        promises.push(models[i].getAliases({
          type: map.getConfiguration().getSimpleElementTypeNames(),
          complete: true
        }));
        promises.push(models[i].getReactions());
      }
      return Promise.all(promises).then(function (bioEntitiesLists) {
        for (var i = 0; i < bioEntitiesLists.length; i++) {
          for (var j = 0; j < bioEntitiesLists[i].length; j++) {
            result.push(bioEntitiesLists[i][j]);
          }
        }
        return result;
      });
    },
    getReactionsWithElement: function (param) {
      if (param.length === undefined) {
        param = [param];
      }
      return getReactionsForElements(param, map);
    },
    getProjectId: function () {
      return map.getProject().getProjectId();
    },
    getName: function () {
      return map.getProject().getName();
    },
    getVersion: function () {
      return map.getProject().getVersion();
    },
    getDisease: function () {
      if (map.getProject().getDisease() !== undefined) {
        return new Annotation(map.getProject().getDisease());
      } else {
        return null;
      }
    },
    getOrganism: function () {
      if (map.getProject().getOrganism() !== undefined) {
        return new Annotation(map.getProject().getOrganism());
      } else {
        return null;
      }
    },
    getModels: function () {
      var result = [{modelId: map.getId()}];
      for (var i = 0; i < map.getSubmaps().length; i++) {
        var subModel = map.getSubmaps()[i].getModel();
        result.push({
          modelId: subModel.getId(),
          name: subModel.getName(),
          width: subModel.getWidth(),
          height: subModel.getHeight()
        });
      }
      return result;
    },
    getOverviewImages: function () {
      var result = [];
      for (var i = 0; i < map.getProject().getOverviewImages().length; i++) {
        var image = map.getProject().getOverviewImages()[i];
        result.push({
          id: image.idObject,
          filename: image.filename,
          width: image.width,
          height: image.height
        });
      }
      return result;
    }
  };
}

/**
 *
 * @param {IdentifiedElement[]} elementIdentifiers
 * @param {CustomMap} customMap
 * @returns {Promise}
 */
function getReactionsForElements(elementIdentifiers, customMap) {
  var elementsByModelId = [];
  for (var i = 0; i < elementIdentifiers.length; i++) {
    var identifiedElement = new IdentifiedElement(elementIdentifiers[i]);
    if (elementsByModelId[identifiedElement.getModelId()] === undefined) {
      elementsByModelId[identifiedElement.getModelId()] = [];
    }
    elementsByModelId[identifiedElement.getModelId()].push(identifiedElement);
  }

  var modelScopePromises = [];
  for (var key in elementsByModelId) {
    if (elementsByModelId.hasOwnProperty(key)) {
      var model = customMap.getProject().getModelById(parseInt(key));
      var promise = model.getReactionsForElements(elementsByModelId[key], true);
      modelScopePromises.push(promise);
    }
  }

  // first promise fetch all data
  return Promise.all(modelScopePromises).then(function (reactionResult) {
    var result = [];
    for (var i = 0; i < reactionResult.length; i++) {
      result = result.concat(reactionResult[i]);
    }
    return result;
  });
}

/**
 *
 * @param {Object} options
 * @param {Object} options.params
 * @param {string} [options.filteredType]
 * @param {boolean} [options.isDefault]
 * @returns {Array}
 */
function createMarkerElements(options) {
  var params = options.params;
  var filteredType = options.filteredType;
  var isDefault = options.isDefault;

  var markerElements = [];
  if (params.length === undefined) {
    params = [params];
  }
  for (var i = 0; i < params.length; i++) {
    var elementParam = params[i];
    if (elementParam.type === undefined && isDefault) {
      markerElements.push({
        element: elementParam.element
      });
    } else if (elementParam.type === filteredType) {
      markerElements.push({
        element: elementParam.element,
        options: elementParam.options
      });
    } else if (elementParam.type !== "ICON" && elementParam.type !== "SURFACE") {
      throw new Error("Unknown type:" + elementParam.type);
    }
  }
  return markerElements;
}

/**
 *
 * @param {Object} param
 * @returns {function(*): *}
 */
function createWrapperFunctionForDialogGuiUpdate(param) {
  /**
   *
   * @param {AbstractCustomMap} e.object
   * @returns {Promise}
   */
  return function (e) {
    var submap = e.object;
    var div = $("[name^='submap-div-']", submap.getElement())[0];
    var x = div.getBoundingClientRect().left - document.body.getBoundingClientRect().left;
    var y = div.getBoundingClientRect().top - document.body.getBoundingClientRect().top;
    var width = div.getBoundingClientRect().width;
    var height = div.getBoundingClientRect().height;
    return param.callback({
      modelId: submap.getId(),
      position: new Point(x, y),
      size: {width: width, height: height}
    });
  }
}

/**
 *
 * @param {Object} options
 * @param {CustomMap} options.map
 * @param {Plugin} options.plugin
 * @param {string} options.pluginId
 * @returns {PluginProjectMap}
 */
function createProjectMap(options) {
  var map = options.map;
  var pluginId = options.pluginId;

  map.registerDbOverlay(new UserDbOverlay({name: pluginId, map: map}));

  var listenersData = [];

  return {
    /**
     *
     * @param {Object} params
     * @param {string} params.dbOverlayName
     * @param {string} [params.query]
     * @param {boolean} [params.perfect]
     * @param {boolean} [params.fitBounds]
     * @param {Point} [params.coordinates]
     * @param {number} [params.modelId]
     * @param {number} [params.zoom]
     *
     *
     * @return {Promise|PromiseLike}
     */
    triggerSearch: function (params) {
      var dbOverlay = getOverlayByName(map, params.dbOverlayName);
      if (params.query !== undefined) {
        return dbOverlay.searchByQuery(params.query, params.perfect, params.fitBounds);
      } else if (dbOverlay instanceof SearchDbOverlay) {
        var zoom = params.zoom;
        if (zoom === undefined) {
          zoom = map.getSubmapById(params.modelId).getZoom();
        }
        return dbOverlay.searchByCoordinates({
          coordinates: new Point(params.coordinates),
          modelId: params.modelId,
          fitBounds: params.fitBounds,
          zoom: zoom
        });
      } else {
        return Promise.reject(new Error("Don't know how to handle your query"));
      }
    },
    /**
     *
     * @return {PromiseLike|Promise}
     */
    clear: function () {
      map.getServerConnector().getSessionData(map.getProject()).setShowComments(false);
      return Promise.all([map.clearDbOverlays(), map.refreshComments()]);
    },

    getVisibleDataOverlays: function () {
      return map.getVisibleDataOverlays();
    },
    /**
     *
     * @param {number|DataOverlay} param
     * @return {PromiseLike|Promise}
     */
    showDataOverlay: function (param) {
      return map.openDataOverlay(param);
    },
    /**
     *
     * @param {number|DataOverlay} param
     * @return {PromiseLike|Promise}
     */
    hideDataOverlay: function (param) {
      return map.hideDataOverlay(param);
    },
    /**
     *
     * @param {Object} param
     * @param {number} param.id
     */
    showOverviewImage: function (param) {
      map.getOverviewDialog().showOverview(param.id);
    },
    /**
     *
     */
    hideOverviewImage: function () {
      map.getOverviewDialog().close();
    },

    /**
     *
     * @param {Object} param
     * @param {string} param.type
     * @param {Object} [param.object]
     * @param {function} param.callback
     * @param {string} [param.dbOverlayName]
     */
    addListener: function (param) {
      var objects = [];
      var listenerWrapper = null;
      var listenerType = param.type;
      if (param.dbOverlayName !== undefined) {
        objects.push(getOverlayByName(map, param.dbOverlayName));
        listenerWrapper = function (e) {
          return getFullElements(map, e.arg.identifiedElements).then(function (result) {
            return param.callback(result);
          });
        };
      } else if (param.object === "plugin") {
        objects.push(options.plugin);
        listenerWrapper = function () {
          return param.callback();
        };
      } else if (param.object === "overlay") {
        objects.push(map);
        if (param.type === "onShow") {
          listenerType = "onShowOverlay";
        } else if (param.type === "onHide") {
          listenerType = "onHideOverlay";
        } else {
          throw new Error("Unknown listener type: " + param.type);
        }
        listenerWrapper = function (e) {
          return param.callback(e.arg);
        };
      } else if (param.object === "map") {
        objects.push(map);
        if (param.type === "onZoomChanged") {
          listenerWrapper = function (e) {
            return param.callback({modelId: e.object.getId(), zoom: e.arg});
          };
        } else if (param.type === "onCenterChanged") {
          listenerWrapper = function (e) {
            return param.callback({modelId: e.object.getId(), center: e.arg});
          };
        } else {
          throw new Error("Unknown listener type: " + param.type);
        }
        objects = objects.concat(map.getSubmaps());
      } else if (param.object === "map-dialog") {
        objects = objects.concat(map.getSubmaps());
        if (param.type === "onDrag" || param.type === "onDragStart" || param.type === "onDragStop" ||
          param.type === "onResize" || param.type === "onResizeStart" || param.type === "onResizeStop" ||
          param.type === "onOpen") {
          listenerWrapper = createWrapperFunctionForDialogGuiUpdate(param);
        } else if (param.type === "onFocus") {
          listenerWrapper = function (e) {
            var submap = e.object;
            return param.callback({modelId: submap.getId()});
          };
        } else if (param.type === "onClose") {
          listenerWrapper = function (e) {
            var submap = e.object;
            return param.callback({modelId: submap.getId()});
          };
        } else {
          throw new Error("Unknown listener type: " + param.type);
        }
      } else {
        throw new Error("Invalid argument");
      }

      var wrapper = function (e) {
        try {
          return Promise.resolve(listenerWrapper(e)).catch(function (error) {
            GuiConnector.warn("Plugin " + options.plugin.getName() + " crashed");
            logger.warn(error);
            if (typeof options.plugin.getLoadedPluginData().notifyError === "function") {
              options.plugin.getLoadedPluginData().notifyError({listener: param, data: e, error: error});
            }
          });
        } catch (error) {
          GuiConnector.warn("Plugin " + options.plugin.getName() + " crashed");
          logger.warn(error);
          if (typeof options.plugin.getLoadedPluginData().notifyError === "function") {
            options.plugin.getLoadedPluginData().notifyError({listener: param, data: e, error: error});
          }
        }
      };

      for (var i = 0; i < objects.length; i++) {
        var object = objects[i];
        object.addListener(listenerType, wrapper);
        listenersData.push({listener: param.callback, wrapper: wrapper, object: object, type: listenerType});
      }
    },
    /**
     *
     * @param {Object} param
     * @param {string} param.type
     * @param {function} param.callback
     * @param {string} param.dbOverlayName
     */
    removeListener: function (param) {
      var dbOverlay = getOverlayByName(map, param.dbOverlayName);
      var indexToBeRemoved = -1;

      for (var i = 0; i < listenersData.length; i++) {
        var listenerData = listenersData[i];
        if (listenerData.listener === param.callback &&
          listenerData.object === dbOverlay &&
          listenerData.type === param.type) {
          indexToBeRemoved = i;
        }
      }
      if (indexToBeRemoved === -1) {
        throw new Error("Listener doesn't exist");
      }
      var listenerWrapper = listenersData[indexToBeRemoved].wrapper;

      dbOverlay.removeListener(param.type, listenerWrapper);

      listenersData.splice(indexToBeRemoved, 1);
    },
    removeAllListeners: function () {
      var removedListeners = [];
      for (var i = 0; i < listenersData.length; i++) {
        var listenerData = listenersData[i];
        var listenerWrapper = listenersData[i].wrapper;
        listenerData.object.removeListener(listenerData.type, listenerWrapper);
        removedListeners.push(listenerData.listener);
      }
      listenersData = [];
      return removedListeners;
    },
    getHighlightedBioEntities: function (dbOverlayName) {
      if (dbOverlayName === undefined) {
        dbOverlayName = pluginId;
      }
      var dbOverlay = getOverlayByName(map, dbOverlayName);
      var identifiedElements;
      return dbOverlay.getIdentifiedElements().then(function (result) {
        identifiedElements = result;
        return getFullElements(map, identifiedElements);
      }).then(function (fullElements) {
        var result = [];
        for (var i = 0; i < identifiedElements.length; i++) {
          var type;
          if (identifiedElements[i].getIcon() !== undefined) {
            type = "ICON";
          } else {
            type = "SURFACE";
          }
          var row = {
            element: fullElements[i],
            type: type,
            options: {
              icon: identifiedElements[i].getIcon(),
              color: identifiedElements[i].getColor(),
              opacity: identifiedElements[i].getOpacity()
            }
          };
          result.push(row);
        }
        return result;
      });
    },
    showBioEntity: function (params) {
      var iconElements = createMarkerElements({
        params: params,
        filteredType: "ICON",
        isDefault: true
      });
      var surfaceElements = createMarkerElements({
        params: params,
        filteredType: "SURFACE",
        isDefault: false
      });
      var promise = Promise.resolve();
      if (iconElements.length > 0) {
        promise = map.getOverlayByName(pluginId).addMarker(iconElements);
      }
      return promise.then(function () {
        if (surfaceElements.length > 0) {
          return map.getOverlayByName(pluginId).addSurface(surfaceElements);
        } else {
          return Promise.resolve();
        }
      });
    },
    hideBioEntity: function (params) {
      var iconElements = createMarkerElements({
        params: params,
        filteredType: "ICON",
        isDefault: true
      });
      var surfaceElements = createMarkerElements({
        params: params,
        filteredType: "SURFACE",
        isDefault: false
      });
      return map.getOverlayByName(pluginId).removeMarker(iconElements).then(function () {
        return map.getOverlayByName(pluginId).removeSurface(surfaceElements);
      });
    },

    /**
     *
     * @param {Object} params
     * @param {number} params.modelId
     * @param {number} params.x
     * @param {number} params.y
     * @return {Promise|PromiseLike}
     */
    setCenter: function (params) {
      var submap = map.getSubmapById(params.modelId);
      if (submap === null) {
        throw new Error("Unknown modelId: " + params.modelId);
      }
      return submap.setCenter(new Point(params.x, params.y));
    },
    getCenter: function (params) {
      var submap = map.getSubmapById(params.modelId);
      if (submap === null) {
        throw new Error("Unknown modelId: " + params.modelId);
      }
      return submap.getCenter();
    },
    getBounds: function (params) {
      var submap = map.getSubmapById(params.modelId);
      if (submap === null) {
        throw new Error("Unknown modelId: " + params.modelId);
      }
      return submap.getBounds();
    },
    fitBounds: function (params) {
      var submap = map.getSubmapById(params.modelId);
      if (submap === null) {
        throw new Error("Unknown modelId: " + params.modelId);
      }
      var p1 = new Point(params.x1, params.y1);
      var p2 = new Point(params.x2, params.y2);
      return submap.getMapCanvas().fitBounds(new Bounds(p1, p2));
    },

    /**
     *
     * @param {Object} params
     * @param {number} params.modelId
     * @param {number} params.zoom
     * @return {Promise|PromiseLike}
     */
    setZoom: function (params) {
      var submap = map.getSubmapById(params.modelId);
      if (submap === null) {
        throw new Error("Unknown modelId: " + params.modelId);
      }
      return submap.setZoom(params.zoom);
    },
    getZoom: function (params) {
      var submap = map.getSubmapById(params.modelId);
      if (submap === null) {
        throw new Error("Unknown modelId: " + params.modelId);
      }
      return submap.getZoom();
    },
    openMap: function (params) {
      return map.openSubmap(params.id);
    }
  }
}

/**
 *
 * @param {Object} options
 * @param {CustomMap} options.map
 * @param {Plugin} options.plugin
 * @param {string} options.pluginId
 * @returns {{data: PluginProjectData, map: PluginProjectMap}}
 */
function createProject(options) {
  return {
    data: createProjectData(options),
    map: createProjectMap(options)
  };
}

/**
 *
 * @param {Object} options
 * @param {Configuration} options.configuration
 * @returns {{options: ConfigurationOption[],  overlayTypes: string[],  imageConverters: ImageConverter[],  modelConverters: ModelConverter[],  elementTypes: BioEntityType[],  reactionTypes: BioEntityType[],  miriamTypes: MiriamType[],  mapTypes: MapType[],  modificationStateTypes: ModificationStateType[],  privilegeTypes: PrivilegeType[],  annotators: Annotator[]}}
 * */
function createConfiguration(options) {
  var configuration = new Configuration(options.configuration);
  return {
    options: configuration.getOptions(),
    overlayTypes: configuration.getOverlayTypes(),
    imageConverters: configuration.getImageConverters(),
    modelConverters: configuration.getModelConverters(),
    elementTypes: configuration.getElementTypes(),
    reactionTypes: configuration.getReactionTypes(),
    miriamTypes: configuration.getMiriamTypes(),
    mapTypes: configuration.getMapTypes(),
    modificationStateTypes: configuration.getModificationStateTypes(),
    privilegeTypes: configuration.getPrivilegeTypes(),
    annotators: configuration.getAnnotators()
  };
}

/**
 *
 * @param {Object} options
 * @param {string} options.hash
 * @param {ServerConnector} options.serverConnector
 * @returns {{setGlobalParam: function(*=, *=): *, getGlobalParam: function(*=): *, setUserParam: function(*=, *=): *, getUserParam: function(*=): *}}
 */
function createPluginData(options) {
  return {
    setGlobalParam: function (key, value) {
      return options.serverConnector.setPluginGlobalParam({hash: options.hash, key: key, value: value});
    },
    getGlobalParam: function (key) {
      return options.serverConnector.getPluginGlobalParam({hash: options.hash, key: key});
    },
    setUserParam: function (key, value) {
      return options.serverConnector.setPluginUserParam({hash: options.hash, key: key, value: value});
    },
    getUserParam: function (key) {
      return options.serverConnector.getPluginUserParam({hash: options.hash, key: key});
    }
  };
}

/**
 *
 * @param {Object} options
 * @param {CustomMap} options.map
 * @param {Plugin} options.plugin
 * @param {string} options.pluginId
 * @param {string} options.hash
 * @param {HTMLElement} options.element
 * @param {Configuration} options.configuration
 * @param {ServerConnector} options.serverConnector
 * @returns {{pluginId: (string|string), element: HTMLElement, project: {data: PluginProjectData, map: PluginProjectMap}, configuration: {options: ConfigurationOption[], overlayTypes: string[], imageConverters: ImageConverter[], modelConverters: ModelConverter[], elementTypes: BioEntityType[], reactionTypes: BioEntityType[], miriamTypes: MiriamType[], mapTypes: MapType[], modificationStateTypes: ModificationStateType[], privilegeTypes: PrivilegeType[], annotators: Annotator[]}, pluginData: {setGlobalParam: (function(*=, *=): *), getGlobalParam: (function(*=): *), setUserParam: (function(*=, *=): *), getUserParam: (function(*=): *)}}}
 * @constructor
 */
function MinervaPluginProxy(options) {
  this.pluginId = options.pluginId;
  this.element = options.element;
  this.project = createProject(options);
  this.configuration = createConfiguration(options);
  this.pluginData = createPluginData(options);

}

module.exports = MinervaPluginProxy;
