"use strict";

/* exported logger */

var logger = require('./logger');

/**
 *
 * @param {string} message
*  @param {Error} [error]
 * @constructor
 * @extends Error
 */
function ValidationError(message, error) {
  this.message = message;
  if (error) {
    this.stack = error.stack;
  } else {
    this.stack = (new Error(message)).stack;
  }
}


ValidationError.prototype = Object.create(Error.prototype);
ValidationError.prototype.constructor = ValidationError;

module.exports = ValidationError;
