function getBrowserInfo() {
  var browser = {};
  browser.name = "Unknown";
  browser.version = "Unknown";

  if (typeof navigator !== 'undefined') {
    var ua = navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    if (msie >= 0) {
      browser.name = "IE";
      // IE 10 or older => return version number
      browser.version = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident >= 0) {
      browser.name = "IE";
      // IE 11 => return version number
      var rv = ua.indexOf('rv:');
      browser.version = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge >= 0) {
      browser.name = "IE";
      // Edge (IE 12+) => return version number
      browser.version = parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }
  }
  return browser;
}

function verifyBrowser() {
  var browser = getBrowserInfo();
  if (browser.name === "IE") {
    var message = "This web page might not work in this browser.\n"
      + "\n" + "Please, use supported browsers: Chrome, Firefox or Safari.";
    alert(message);
  }
}

module.exports = verifyBrowser;
