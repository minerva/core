"use strict";

/* exported logger */

var logger = require('./logger');

/**
 *
 * @param {string} message
 * @constructor
 * @extends Error
 */
function MinervaNetError(message) {
  this.message = message;
  this.stack = (new Error(message)).stack;
}

MinervaNetError.prototype = Object.create(Error.prototype);
MinervaNetError.prototype.constructor = MinervaNetError;

module.exports = MinervaNetError;
