"use strict";

/* exported logger */

var Promise = require("bluebird");

// noinspection JSUnusedLocalSymbols
var logger = require('./logger');

var request = require('request');

var HttpStatus = require('http-status-codes');

var Alias = require('./map/data/Alias');
var Annotation = require('./map/data/Annotation');
var Chemical = require('./map/data/Chemical');
var Comment = require('./map/data/Comment');
var Configuration = require('./Configuration');
var ConfigurationOption = require('./ConfigurationOption');
var Drug = require('./map/data/Drug');
var ConfigurationType = require('./ConfigurationType');
var IdentifiedElement = require('./map/data/IdentifiedElement');
var InvalidCredentialsError = require('./InvalidCredentialsError');
var LayoutAlias = require('./map/data/LayoutAlias');
var License = require('./map/data/License');
var DataOverlay = require('./map/data/DataOverlay');
var LayoutReaction = require('./map/data/LayoutReaction');
var MapModel = require('./map/data/MapModel');
var Mesh = require('./map/data/Mesh');
var NetworkError = require('./NetworkError');
var ObjectExistsError = require('./ObjectExistsError');
var ObjectNotFoundError = require('./ObjectNotFoundError');
var PluginData = require('./map/data/PluginData');
var Project = require('./map/data/Project');
var ProjectStatistics = require('./map/data/ProjectStatistics');
var Reaction = require('./map/data/Reaction');
var ReferenceGenome = require('./map/data/ReferenceGenome');
var SbmlFunction = require('./map/data/SbmlFunction');
var SbmlParameter = require('./map/data/SbmlParameter');
var SecurityError = require('./SecurityError');
var SessionData = require('./SessionData');
var Taxonomy = require('./map/data/Taxonomy');
var User = require('./map/data/User');
var ValidationError = require('./ValidationError');
var PrivilegeType = require('./map/data/PrivilegeType');

var GuiConnector = require('./GuiConnector');

var ObjectWithListeners = require('./ObjectWithListeners');

var Point = require('./map/canvas/Point');
var ZipEntry = require('./gui/admin/ZipEntry');
var Background = require("./map/data/Background");
var MinervaNetError = require("./MinervaNetError");

/**
 * This object contains methods that will communicate with server.
 */
var ServerConnector = new ObjectWithListeners();
ServerConnector.init = function () {
  var self = this;

  self._configurationParam = [];
  self._projects = [];

  /**
   @name ServerConnector#_projectsById
   @type Project[]
   */
  self._projectsById = [];

  self._users = [];

  /**
   *
   * @type {Object.<string,User>}
   * @private
   */
  self._usersByLogin = [];

  self._customMap = null;
  self._sessionData = undefined;
  /**
   *
   * @type {Configuration}
   * @private
   */
  self._configuration = undefined;
  self._licenses = undefined;
  self._loggedUser = undefined;
  self._serverBaseUrl = undefined;

  var i;
  var listeners = self.getListeners("onDataLoadStart");
  for (i = 0; i < listeners.length; i++) {
    self.removeListener("onDataLoadStart", listeners[i]);
  }

  listeners = self.getListeners("onDataLoadStop");
  for (i = 0; i < listeners.length; i++) {
    self.removeListener("onDataLoadStop", listeners[i]);
  }

  listeners = self.getListeners("onAddDataOverlay");
  for (i = 0; i < listeners.length; i++) {
    self.removeListener("onAddDataOverlay", listeners[i]);
  }
  listeners = self.getListeners("onRemoveDataOverlay");
  for (i = 0; i < listeners.length; i++) {
    self.removeListener("onRemoveDataOverlay", listeners[i]);
  }

  listeners = self.getListeners("onRemoveBackground");
  for (i = 0; i < listeners.length; i++) {
    self.removeListener("onRemoveBackground", listeners[i]);
  }

  self.MAX_NUMBER_OF_IDS_IN_GET_QUERY = 100;
};
ServerConnector.registerListenerType("onDataLoadStart");
ServerConnector.registerListenerType("onDataLoadStop");
ServerConnector.registerListenerType("onAddDataOverlay");
ServerConnector.registerListenerType("onRemoveDataOverlay");
ServerConnector.registerListenerType("onRemoveBackground");

ServerConnector.init();

ServerConnector.getMinOverlayColorInt = function () {
  var self = this;
  return self.getLoggedUser().then(function (user) {
    var userColor = user.getMinColor();
    return self.returnUserOrSystemColor(userColor, self.getConfigurationParam(ConfigurationType.MIN_COLOR_VAL));
  });
};

ServerConnector.checkIfUserLogoutFromDifferentTab = function () {
  var self = this;
  var sessionLogin = self.getSessionData(null).getLogin();
  var tabLogin = self._currentTabLogin;
  if (tabLogin === undefined) {
    self._currentTabLogin = ServerConnector.getSessionData(null).getLogin();
    tabLogin = self._currentTabLogin;
  }
  if (tabLogin !== sessionLogin) {
    setTimeout(function () {
      var sessionLogin = self.getSessionData(null).getLogin();
      var tabLogin = self._currentTabLogin;
      if (tabLogin !== sessionLogin) {
        logger.warn("User was log out in different tab. Reloading page");
        window.location.reload(false);
      }
    }, 100);
  }
};

setInterval(function () {
  ServerConnector.checkIfUserLogoutFromDifferentTab();
}, 1000);

ServerConnector.returnUserOrSystemColor = function (userColor, systemPromisedColor) {
  return systemPromisedColor.then(function (systemColor) {
    var color = userColor;
    if (userColor === null || userColor === undefined || userColor === "") {
      color = systemColor;
    }
    color = parseInt(color, 16);
    /* jslint bitwise: true */
    color = (color & 0xFFFFFF);
    return color;
  });
};

ServerConnector.getSimpleOverlayColorInt = function () {
  var self = this;
  return self.getLoggedUser().then(function (user) {
    var userColor = user.getSimpleColor();
    return self.returnUserOrSystemColor(userColor, self.getConfigurationParam(ConfigurationType.SIMPLE_COLOR_VAL));
  });
};

ServerConnector.getMaxOverlayColorInt = function () {
  var self = this;
  return self.getLoggedUser().then(function (user) {
    var userColor = user.getMaxColor();
    return self.returnUserOrSystemColor(userColor, self.getConfigurationParam(ConfigurationType.MAX_COLOR_VAL));
  });
};

ServerConnector.getNeutralOverlayColorInt = function () {
  var self = this;
  return self.getLoggedUser().then(function (user) {
    var userColor = user.getNeutralColor();
    return self.returnUserOrSystemColor(userColor, self.getConfigurationParam(ConfigurationType.NEUTRAL_COLOR_VAL));
  });
};

/**
 *
 * @param {string} url
 * @param {string} [description]
 * @returns {Promise<string>}
 */
ServerConnector.sendGetRequest = function (url, description) {
  return this.sendRequest({
    url: url,
    description: description,
    method: "GET"
  });
};

/**
 *
 * @return {Promise<boolean>}
 */
ServerConnector.isSessionValid = function () {
  var self = this;
  return self._sendRequest({method: "GET", url: this.isSessionValidUrl()}).then(function (content) {
    var sessionLogin = self.getSessionData(null).getLogin();
    var serverSideLogin = JSON.parse(content).login;

    //this is workaround for the problem with Firefox Private Window issue
    //in FF new tab is opened with existing cookies but localStorage is cleared
    if (sessionLogin === undefined || sessionLogin === null) {
      self.getSessionData(null).setLogin(serverSideLogin);
    }
    return serverSideLogin === self.getSessionData(null).getLogin();
  }).catch(function (error) {
    if (error instanceof NetworkError) {
      if (error.statusCode === HttpStatus.FORBIDDEN || error.statusCode === HttpStatus.UNAUTHORIZED) {
        return self.getSessionData(null).getLogin() === "anonymous";
      }
    }
    return Promise.reject(error);
  });
};

/**
 *
 * @param params
 * @returns {PromiseLike|Promise}
 */
ServerConnector.sendRequest = function (params) {
  var self = this;
  if (arguments.length > 1) {
    return Promise.reject(new Error("Only two arguments are supported"));
  }

  var description = params.url;
  if (params.description !== undefined) {
    description = params.description;
    params.description = undefined;
  }

  var content;
  return self.callListeners("onDataLoadStart", description).then(function () {
    return self._sendRequest(params);
  }).then(function (result) {
    content = result;
    return self.callListeners("onDataLoadStop", description);
  }, function (error) {
    return self.callListeners("onDataLoadStop", description).then(function () {
      return Promise.reject(error);
    });
  }).then(function () {
    return content;
  }, function (error) {
    return self.isSessionValid().then(function (isValid) {
      if (!isValid) {
        self.getSessionData().setToken(undefined);
        var login = self.getSessionData().getLogin();
        self.login().then(function () {
          if (login === "anonymous" || login === null || login === undefined) {
            window.location.reload(false);
          } else {
            ServerConnector.getSessionData(null).setLogin(null);
            window.location.href = ServerConnector.getServerBaseUrl() + "login.xhtml?from=" + encodeURI(window.location.href);
          }
        });
      }
    }).then(function () {
      return Promise.reject(error);
    });
  });

};

ServerConnector._sendRequest = function (params) {
  return new Promise(function (resolve, reject) {
    request(params, function (error, response, body) {
      if (error) {
        reject(new NetworkError(error.message, {
          content: body,
          url: params.url
        }));
      } else if (response.statusCode !== 200) {
        reject(new NetworkError(params.url + " rejected with status code: " + response.statusCode, {
          content: body,
          url: params.url,
          statusCode: response.statusCode
        }));
      } else {
        // for some reason sometimes result is an object not a string
        if (typeof body === 'string' || body instanceof String) {
          resolve(body);
        } else {
          resolve(JSON.stringify(body));
        }
      }
    });
  });
};

/**
 *
 * @param {string} url
 * @param {Object} params
 * @returns {Promise}
 */
ServerConnector.sendPostRequest = function (url, params) {
  var formParams = {};
  for (var key in params) {
    if (params.hasOwnProperty(key)) {
      formParams[key] = this.objectToRequestString(params[key]);
      if (formParams[key] === undefined) {
        formParams[key] = params[key];
      }
    }
  }
  return this.sendRequest({
    method: "POST",
    url: url,
    form: formParams
  });
};

ServerConnector.sendJsonPostRequest = function (url, json) {
  return this.sendRequest({
    method: "POST",
    url: url,
    json: json
  });
};

ServerConnector.sendDeleteRequest = function (url, json) {
  return this.sendRequest({
    method: "DELETE",
    url: url,
    json: json
  });
};

/**
 *
 * @param {string} url
 * @param {Object} json
 * @returns {Promise}
 */
ServerConnector.sendPatchRequest = function (url, json) {
  return this.sendRequest({
    method: "PATCH",
    url: url,
    json: json
  });
};

/**
 *
 * @returns {string}
 */
ServerConnector.getApiBaseUrl = function () {
  return this.getServerBaseUrl() + "api/";
};

ServerConnector.getNewApiBaseUrl = function () {
  return this.getServerBaseUrl() + "new_api/";
};

/**
 *
 * @returns {string}
 */
ServerConnector.getNewApiBaseUrl = function () {
  return this.getServerBaseUrl() + "new_api/";
};

ServerConnector.getServerBaseUrl = function () {
  if (this._serverBaseUrl === undefined) {
    var url = "" + window.location.href;
    if (url.indexOf("?") >= 0) {
      url = url.substr(0, url.indexOf("?"));
    }
    if (!url.endsWith("/")) {
      url = url.substr(0, url.lastIndexOf("/") + 1);
    }
    this._serverBaseUrl = url;
  }
  return this._serverBaseUrl;
};

/**
 *
 * @param {Object} object
 * @returns {string}
 */
ServerConnector.objectToRequestString = function (object) {
  var value;
  if (object instanceof Point) {
    value = this.pointToString(object);
  } else if (Object.prototype.toString.call(object) === '[object Array]') {
    var arrayObject = object[0];
    if (arrayObject instanceof ZipEntry) {
      value = object;
    } else {
      value = this.idsToString(object);
    }
  } else if (object === null) {
    value = undefined;
  } else if (typeof object === 'string' || object instanceof String || !isNaN(object)) {
    value = object.toString();
  } else {
    value = undefined;
  }
  return value;
};

ServerConnector.createGetParams = function (params, prefix) {
  var sorted = [], key;

  for (key in params) {
    if (params.hasOwnProperty(key)) {
      sorted.push(key);
    }
  }
  sorted.sort();

  var result = "";
  for (var i = 0; i < sorted.length; i++) {
    key = sorted[i];
    var value = params[key];
    if (prefix !== undefined) {
      key = prefix + "." + key;
    }

    var serializedValue = this.objectToRequestString(value);
    if (serializedValue === undefined) {
      result += this.createGetParams(value, key);
    } else if (serializedValue !== undefined && serializedValue !== "") {
      result += key + "=" + serializedValue + "&";
    }
  }
  return result;
};

/**
 *
 * @param {Object} paramObj
 * @param {string} [paramObj.type]
 * @param {string} [paramObj.url]
 * @param {Object} [paramObj.params]
 *
 * @returns {string}
 */
ServerConnector.getApiUrl = function (paramObj) {
  var type = paramObj.type;
  var params = this.createGetParams(paramObj.params);

  var result = paramObj.url;
  if (result === undefined) {
    result = this.getApiBaseUrl() + type;
  }
  if (params !== "") {
    result += "?" + params;
  }
  return result;
};

ServerConnector.getNewApiUrl = function (paramObj) {
  var type = paramObj.type;
  var params = this.createGetParams(paramObj.params);

  var result = paramObj.url;
  if (result === undefined) {
    result = this.getNewApiBaseUrl() + type;
  }
  if (params !== "") {
    result += "?" + params;
  }
  return result;
};

/**
 *
 * @param [queryParams]
 * @param [filterParams]
 * @return {string}
 */
ServerConnector.getProjectsUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    type: "projects/",
    params: filterParams
  });
};

/**
 *
 * @param [queryParams]
 * @param [filterParams]
 * @return {string}
 */
ServerConnector.getNewApiProjectsUrl = function (queryParams, filterParams) {
  return this.getNewApiUrl({
    type: "projects/",
    params: filterParams
  });
};

ServerConnector.getPluginsUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    type: "plugins/",
    params: filterParams
  });
};

/**
 *
 * @param {Object} queryParams
 * @param {string} queryParams.hash
 * @param [filterParams]
 * @returns {string}
 */
ServerConnector.getPluginUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getPluginsUrl(queryParams) + queryParams.hash + "/",
    params: filterParams
  });
};

/**
 *
 * @param {Object} queryParams
 * @param {string} queryParams.hash
 * @param {string} queryParams.key
 * @param [filterParams]
 * @returns {string}
 */
ServerConnector.getPluginGlobalParamUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getPluginUrl(queryParams) + "data/global/" + queryParams.key + "/",
    params: filterParams
  });
};

ServerConnector.getPluginUserParamUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getPluginsUrl(queryParams) + queryParams.hash + "/data/users/" + queryParams.key + "/",
    params: filterParams
  });
};

ServerConnector.getProjectUrl = function (queryParams, filterParams) {
  var id = this.getIdOrAsterisk(queryParams.projectId);
  return this.getApiUrl({
    url: this.getProjectsUrl(queryParams) + id + "/",
    params: filterParams
  });
};

ServerConnector.getNewApiProjectUrl = function (queryParams, filterParams) {
  var id = this.getIdOrAsterisk(queryParams.projectId);
  return this.getNewApiUrl({
    url: this.getNewApiProjectsUrl(queryParams) + id + "/",
    params: filterParams
  });
};

ServerConnector.getMoveProjectUrl = function (queryParams, filterParams) {
  var id = this.getIdOrAsterisk(queryParams.projectId);
  return this.getNewApiBaseUrl() + 'projects/' + id + ':move';
};

ServerConnector.getArchiveProjectUrl = function (queryParams, filterParams) {
  var id = this.getIdOrAsterisk(queryParams.projectId);
  return this.getApiUrl({
    url: this.getProjectsUrl(queryParams) + id + ":archive",
    params: filterParams
  });
};

ServerConnector.getReviveProjectUrl = function (queryParams, filterParams) {
  var id = this.getIdOrAsterisk(queryParams.projectId);
  return this.getApiUrl({
    url: this.getProjectsUrl(queryParams) + id + ":revive",
    params: filterParams
  });
};

/**
 *
 * @param queryParams
 * @param filterParams
 * @return {string}
 */
ServerConnector.getGrantProjectPrivilegesUrl = function (queryParams, filterParams) {
  var id = this.getIdOrAsterisk(queryParams.projectId);
  return this.getApiUrl({
    url: this.getProjectsUrl(queryParams) + id + ":grantPrivileges",
    params: filterParams
  });
};

/**
 *
 * @param queryParams
 * @param filterParams
 * @return {string}
 */
ServerConnector.getRevokeProjectPrivilegesUrl = function (queryParams, filterParams) {
  var id = this.getIdOrAsterisk(queryParams.projectId);
  return this.getApiUrl({
    url: this.getProjectsUrl(queryParams) + id + ":revokePrivileges",
    params: filterParams
  });
};

ServerConnector.getProjectStatisticsUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getProjectUrl(queryParams) + "statistics/",
    params: filterParams
  });
};

ServerConnector.getSubmapConnectionsUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getProjectUrl(queryParams) + "submapConnections",
    params: filterParams
  });
};


ServerConnector.getPublicationsUrl = function (queryParams, filterParams) {
  filterParams.start = filterParams.start || 0;
  filterParams.length = filterParams.length || 10;

  return this.getApiUrl({
    url: this.getModelsUrl(queryParams) + "publications/",
    params: filterParams
  });
};
ServerConnector.getProjectLogsUrl = function (queryParams, filterParams) {
  filterParams.start = filterParams.start || 0;
  filterParams.length = filterParams.length || 10;

  return this.getApiUrl({
    url: this.getProjectUrl(queryParams) + "logs/",
    params: filterParams
  });
};

ServerConnector.getProjectJobsUrl = function (queryParams, filterParams) {
  filterParams.page = filterParams.page || 0;
  filterParams.size = filterParams.size || 10;

  return this.getNewApiUrl({
    url: this.getNewApiProjectUrl(queryParams) + "job/",
    params: filterParams
  });
};

ServerConnector.getMeshUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    type: "mesh/" + queryParams.id,
    params: filterParams
  });
};

/**
 *
 * @param {Object} queryParams
 * @param {number} queryParams.id
 * @param [filterParams]
 * @return {string}
 */
ServerConnector.getTaxonomyUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    type: "taxonomy/" + queryParams.id,
    params: filterParams
  });
};

ServerConnector.getSbmlFunctionUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getModelsUrl(queryParams) + "functions/" + queryParams.functionId,
    params: filterParams
  });
};

ServerConnector.getSbmlParameterUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getModelsUrl(queryParams) + "parameters/" + queryParams.parameterId,
    params: filterParams
  });
};

/**
 *
 * @param {Object} queryParams
 * @param {string} [queryParams.version]
 * @param {string} [queryParams.organism]
 * @param {string} [queryParams.type]
 * @param {string} [queryParams.genomeId]
 * @param {Object} filterParams
 *
 * @returns {string}
 */
ServerConnector.getReferenceGenomeUrl = function (queryParams, filterParams) {
  if (queryParams.genomeId !== undefined) {
    return this.getApiUrl({
      type: "genomics/" + queryParams.genomeId + "/",
      params: filterParams
    });
  } else {
    var version = this.getIdOrAsterisk(queryParams.version);

    return this.getApiUrl({
      type: "genomics/taxonomies/" + queryParams.organism + "/genomeTypes/" + queryParams.type + "/versions/" + version
        + "/",
      params: filterParams
    });
  }
};

/**
 *
 * @param {Object} queryParams
 * @param {string} queryParams.genomeId
 * @param {string} queryParams.mappingId
 * @param {Object} [filterParams]
 *
 * @returns {string}
 */
ServerConnector.getReferenceGenomeGeneMappingUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    type: "genomics/" + queryParams.genomeId + "/geneMapping/" + queryParams.mappingId + "/",
    params: filterParams
  });
};

/**
 *
 * @param {Object} queryParams
 * @param {string} queryParams.genomeId
 * @param {Object} [filterParams]
 *
 * @returns {string}
 */
ServerConnector.getReferenceGenomeGeneMappingsUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    type: "genomics/" + queryParams.genomeId + "/geneMapping/",
    params: filterParams
  });
};

/**
 *
 * @param {Object} queryParams
 * @param {string} queryParams.version
 * @param {Annotation} queryParams.organism
 * @param {string} queryParams.type
 * @param {Object} filterParams
 *
 * @returns {string}
 */
ServerConnector.getAvailableGenomeUrlsUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    type: "genomics/taxonomies/" + queryParams.organism.getResource() + "/genomeTypes/" + queryParams.type + "/versions/" + queryParams.version
      + ":getAvailableRemoteUrls",
    params: filterParams
  });
};

/**
 *
 * @param {Object} queryParams
 * @param {Annotation} queryParams.organism
 * @param {Object} [filterParams]
 *
 * @returns {string}
 */
ServerConnector.getReferenceGenomeTypesUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    type: "genomics/taxonomies/" + queryParams.organism.getResource() + "/genomeTypes/",
    params: filterParams
  });
};

/**
 *
 * @param {Object} queryParams
 * @param {Annotation} queryParams.organism
 * @param {string} queryParams.type
 * @param {Object} [filterParams]
 *
 * @returns {string}
 */
ServerConnector.getReferenceGenomeVersionsUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    type: "genomics/taxonomies/" + queryParams.organism.getResource() + "/genomeTypes/" + queryParams.type + "/versions/",
    params: filterParams
  });
};

/**
 *
 * @param {Object} [queryParams]
 * @param {Object} [filterParams]
 *
 * @returns {string}
 */
ServerConnector.getReferenceGenomeOrganismsUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    type: "genomics/taxonomies/",
    params: filterParams
  });
};

ServerConnector.loginUrl = function () {
  return this.getApiUrl({
    type: "doLogin"
  });
};

ServerConnector.logoutUrl = function () {
  return this.getApiUrl({
    type: "doLogout"
  });
};

ServerConnector.isSessionValidUrl = function () {
  return this.getApiUrl({
    url: this.getUsersUrl() + "isSessionValid"
  });
};

ServerConnector.getSuggestedQueryListUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getBioEntitiesUrl(queryParams) + "suggestedQueryList/",
    params: filterParams
  });
};
ServerConnector.getChemicalSuggestedQueryListUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getProjectUrl(queryParams) + "chemicals/suggestedQueryList",
    params: filterParams
  });
};
ServerConnector.getDrugSuggestedQueryListUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getProjectUrl(queryParams) + "drugs/suggestedQueryList",
    params: filterParams
  });
};

ServerConnector.addCommentUrl = function (queryParams) {
  return this.getApiUrl({
    url: this.getCommentsUrl(queryParams)
  });
};

ServerConnector.addOverlayUrl = function (queryParams) {
  return this.getApiUrl({
    url: this.getOverlaysUrl(queryParams)
  });
};

ServerConnector.updateOverlayUrl = function (queryParams) {
  return this.getApiUrl({
    url: this.getOverlayByIdUrl(queryParams)
  });
};

ServerConnector.updateBackgroundUrl = function (queryParams) {
  return this.getApiUrl({
    url: this.getBackgroundByIdUrl(queryParams)
  });
};

ServerConnector.updateModelUrl = function (queryParams) {
  return this.getApiUrl({
    url: this.getModelsUrl(queryParams)
  });
};

ServerConnector.deleteOverlayUrl = function (queryParams) {
  return this.getApiUrl({
    url: this.getOverlayByIdUrl(queryParams)
  });
};

ServerConnector.deleteBackgroundUrl = function (queryParams) {
  return this.getApiUrl({
    url: this.getBackgroundByIdUrl(queryParams)
  });
};

ServerConnector.deleteCommentUrl = function (queryParams) {
  return this.getApiUrl({
    url: this.getProjectUrl(queryParams) + "comments/" + queryParams.commentId + "/"
  });
};

ServerConnector.getOverlaysUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getProjectUrl(queryParams) + "overlays/",
    params: filterParams
  });
};

ServerConnector.getBackgroundsUrl = function (queryParams) {
  return this.getApiUrl({
    url: this.getProjectUrl(queryParams) + "backgrounds/"
  });
};

ServerConnector.getCommentsUrl = function (queryParams, filterParams) {
  var modelId = this.getIdOrAsterisk(queryParams.modelId);
  var url = this.getProjectUrl(queryParams) + "comments/models/" + modelId + "/";
  if (queryParams.elementType !== undefined) {
    if (queryParams.elementType === "ALIAS") {
      url += "bioEntities/elements/" + queryParams.elementId;
    } else if (queryParams.elementType === "REACTION") {
      url += "bioEntities/reactions/" + queryParams.elementId;
    } else if (queryParams.elementType === "POINT") {
      url += "points/" + queryParams.coordinates;
    } else {
      throw new Error("Unknown element type: " + queryParams.elementType);
    }
  }
  return this.getApiUrl({
    url: url,
    params: filterParams
  });
};

ServerConnector.getOverlayByIdUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getOverlaysUrl(queryParams) + queryParams.overlayId + "/",
    params: filterParams
  });
};

ServerConnector.getBackgroundByIdUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getBackgroundsUrl(queryParams) + queryParams.backgroundId + "/",
    params: filterParams
  });
};

ServerConnector.getOverlayElementsUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getModelsUrl(queryParams) + "bioEntities/",
    params: filterParams
  });

};

ServerConnector.getFullOverlayElementUrl = function (queryParams, filterParams) {

  return this.getApiUrl({
    url: this.getAliasesUrl(queryParams) + queryParams.id + "/",
    params: filterParams
  });

};

ServerConnector.getSubmitErrorToMinervaNetUrl = function () {
  return this.getApiBaseUrl() + 'minervanet/submitError';
};

ServerConnector.getIsRegisteredInMinervaNetUrl = function () {
  return this.getApiBaseUrl() + 'minervanet/isMachineRegistered';
};

ServerConnector.getRegisterMachineUrl = function () {
  return this.getApiBaseUrl() + 'minervanet/registerMachine';
};

ServerConnector.getUnregisterMachineUrl = function () {
  return this.getApiBaseUrl() + 'minervanet/unregisterMachine';
};

ServerConnector.getStacktraceUrl = function () {
  return this.getApiBaseUrl() + 'stacktrace/';
};

/**
 *
 * @param {string} queryParams.projectId
 * @param {object} queryParams
 * @return {string}
 */
ServerConnector.getRegisterProjectUrl = function (queryParams) {
  return this.getApiBaseUrl() + 'minervanet/projects/' + queryParams.projectId + ':registerProject';
};

/**
 *
 * @param {string} queryParams.projectId
 * @param {object} queryParams
 * @return {string}
 */
ServerConnector.getUnregisterProjectUrl = function (queryParams) {
  return this.getApiBaseUrl() + 'minervanet/projects/' + queryParams.projectId + ':unregisterProject';
};

ServerConnector.idsToString = function (ids) {
  var result = "";
  if (ids !== undefined) {
    ids.sort(function (a, b) {
      if (typeof a === "string") {
        return a.localeCompare(b);
      } else {
        return a - b;
      }
    });
    for (var i = 0; i < ids.length; i++) {
      if (result !== "") {
        if (ids[i - 1] !== ids[i]) {
          result = result + "," + ids[i];
        } // we ignore duplicates
      } else {
        result = ids[i];
      }
    }
  }
  return result;
};

ServerConnector.pointToString = function (point) {
  return point.x.toFixed(2) + "," + point.y.toFixed(2);
};

/**
 *
 * @param {Object} queryParams
 * @param {string} queryParams.projectId
 * @param {number} [queryParams.modelId]
 * @param {number} [queryParams.overlayId]
 * @return {string}
 */
ServerConnector.getModelsUrl = function (queryParams) {
  var modelId = this.getIdOrAsterisk(queryParams.modelId);
  var overlayId = queryParams.overlayId;
  var url = this.getProjectUrl(queryParams);
  if (overlayId !== undefined) {
    url = this.getOverlayByIdUrl(queryParams);
  }

  return this.getApiUrl({
    url: url + "models/" + modelId + "/"
  });
};

ServerConnector.getBioEntitiesUrl = function (queryParams) {
  return this.getApiUrl({
    url: this.getModelsUrl(queryParams) + "bioEntities/"
  });
};

ServerConnector.getIdOrAsterisk = function (id) {
  if (id === undefined || id === "" || id === null) {
    return "*";
  } else {
    return id;
  }
};

ServerConnector.getReactionsUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getBioEntitiesUrl(queryParams) + "reactions/",
    params: filterParams
  });
};

ServerConnector.getAliasesUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getBioEntitiesUrl(queryParams) + "elements/",
    params: filterParams
  });
};

ServerConnector.getConfigurationUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    type: "configuration/",
    params: filterParams
  });
};

ServerConnector.getLicensesUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    type: "licenses/",
    params: filterParams
  });
};

ServerConnector.getConfigurationOptionUrl = function (queryParams, filterParams) {
  var self = this;
  return self.getApiUrl({
    url: self.getConfigurationUrl() + "options/" + queryParams.type,
    params: filterParams
  });
};

ServerConnector.getOauthProvidersUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    type: "oauth/providers/",
    params: filterParams
  });
};

ServerConnector.getSearchUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getModelsUrl(queryParams) + "bioEntities:search",
    params: filterParams
  });
};

ServerConnector.getSearchDrugsUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getProjectUrl(queryParams) + "drugs:search",
    params: filterParams
  });
};

ServerConnector.getSearchChemicalsUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getProjectUrl(queryParams) + "chemicals:search",
    params: filterParams
  });
};

ServerConnector.getOverlaySourceUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getOverlaysUrl(queryParams) + queryParams.overlayId + ":downloadSource",
    params: filterParams
  });
};

ServerConnector.getImageUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getProjectUrl(queryParams) + "models/" + queryParams.modelId + ":downloadImage",
    params: filterParams
  });
};

ServerConnector.getModelPartUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getProjectUrl(queryParams) + "models/" + queryParams.modelId + ":downloadModel",
    params: filterParams
  });
};

ServerConnector.getModelPartWarningsUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getProjectUrl(queryParams) + "models/" + queryParams.modelId + ":downloadModelWarnings",
    params: filterParams
  });
};

ServerConnector.getProjectSourceUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    type: "projects/" + queryParams.projectId + ":downloadSource",
    params: filterParams
  });
};


ServerConnector.getFilesUrl = function () {
  return this.getApiUrl({
    type: "files/"
  });
};

ServerConnector.getCreateFileUrl = function () {
  return this.getApiUrl({
    url: this.getFilesUrl()
  });
};
ServerConnector.getFileUrl = function (queryParams) {
  return this.getApiUrl({
    url: this.getFilesUrl() + queryParams.id
  });
};
ServerConnector.getUploadFileUrl = function (queryParams) {
  return this.getApiUrl({
    url: this.getFileUrl(queryParams) + ":uploadContent"
  });
};


ServerConnector.getUsersUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    type: "users/",
    params: filterParams
  });
};

/**
 *
 * @param {Object} [queryParams]
 * @param {Object} [filterParams]
 *
 * @returns {string}
 */
ServerConnector.getReferenceGenomesUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    type: "genomics/",
    params: filterParams
  });
};


ServerConnector.getUserUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getUsersUrl() + queryParams.login,
    params: filterParams
  });
};

/**
 *
 * @param {string} queryParams.login
 * @param {string} queryParams.token
 * @returns {string}
 */
ServerConnector.getConfirmEmailUrl = function (queryParams) {
  return this.getApiUrl({
    url: this.getUserUrl({login: queryParams.login}) + ":confirmEmail",
    params: {}
  });
};

ServerConnector.getUpdateUserPrivilegesUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getUserUrl(queryParams) + ":updatePrivileges",
    params: filterParams
  });
};
ServerConnector.getUpdateUserPreferencesUrl = function (queryParams, filterParams) {
  return this.getApiUrl({
    url: this.getUserUrl(queryParams) + ":updatePreferences",
    params: filterParams
  });
};

/**
 *
 * @return {(PromiseLike<Configuration> | Promise<Configuration>)}
 */
ServerConnector.getConfiguration = function () {
  var self = this;
  if (this._configuration === undefined) {
    return self.sendGetRequest(self.getConfigurationUrl()).then(function (content) {
      self._configuration = new Configuration(JSON.parse(content));
      return Promise.resolve(self._configuration);
    });
  } else {
    return Promise.resolve(self._configuration);
  }
};

ServerConnector.getOauthProviders = function () {
  var self = this;
  return self.sendGetRequest(self.getOauthProvidersUrl()).then(function (content) {
    return Promise.resolve(JSON.parse(content));
  });
}

/**
 *
 * @returns {Promise}
 */
ServerConnector.extendSession = function () {
  return this.sendGetRequest(this.getConfigurationUrl())
};

ServerConnector.getConfigurationParam = function (paramId) {
  if (paramId === undefined) {
    return Promise.reject(new Error("Unknown param type"));
  }
  var self = this;
  return self.getConfiguration().then(function (configuration) {
    var option = configuration.getOption(paramId);
    if (option.getValue !== undefined) {
      return option.getValue();
    } else {
      return option;
    }
  });
};

/**
 *
 * @param {ConfigurationOption} option
 * @return {Promise}
 */
ServerConnector.updateConfigurationOption = function (option) {
  var self = this;
  var queryParams = {
    type: option.getType()
  };
  var filterParams = {
    option: {
      type: option.getType(),
      value: option.getValue()
    }
  };
  var configuration;
  var content;
  return self.getConfiguration().then(function (result) {
    configuration = result;
    return self.sendPatchRequest(self.getConfigurationOptionUrl(queryParams), filterParams);
  }).then(function (result) {
    content = result;
    var conf = JSON.parse(content);
    return configuration.setOption(conf.type, new ConfigurationOption(conf));
  }).then(function () {
    return content;
  }).catch(function (error) {
    return self._processUpdateError(error);
  });
};

ServerConnector.getModels = function (projectId) {
  var queryParams = {};
  var filterParams = {};
  var self = this;
  return self.getProjectId(projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendGetRequest(self.getModelsUrl(queryParams, filterParams));
  }).then(function (content) {
    var models = [];
    var parsedJson = JSON.parse(content);
    for (var i = 0; i < parsedJson.length; i++) {
      models.push(new MapModel(parsedJson[i]));
    }
    return models;
  });
};

/**
 *
 * @param {string} [projectId]
 * @return {Promise<project>| PromiseLike<Project>}
 */
ServerConnector.getProject = function (projectId) {
  var queryParams = {};
  var filterParams = {};
  var project;
  var self = this;
  return self.getProjectId(projectId).then(function (result) {
    projectId = result;
    queryParams.projectId = result;
    return self.sendGetRequest(self.getProjectUrl(queryParams, filterParams));
  }).catch(function (error) {
    return self.processNetworkError(error);
  }).then(function (content) {
    if (content === null) {
      return null;
    } else {
      var promises = [];
      var downloadedProject = new Project(content);
      if (self._projectsById[projectId] instanceof Project) {
        promises.push(self._projectsById[projectId].update(downloadedProject));
      } else {
        self._projectsById[projectId] = downloadedProject;
      }
      project = self._projectsById[projectId];

      return Promise.all(promises).then(function () {
        return self.getModels(projectId);
      }).then(function (models) {
        project.setModels(models);
        return self.getOverlays({
          projectId: projectId,
          publicOverlay: true
        });
      }).then(function (overlays) {
        project.addOrUpdateDataOverlays(overlays);
        return self.getBackgrounds({
          projectId: projectId,
          publicOverlay: true
        });
      }).then(function (backgrounds) {
        project.addOrUpdateBackgrounds(backgrounds);
        return self.getLoggedUser();
      }).then(function (user) {
        return self.getOverlays({
          projectId: projectId,
          creator: user.getLogin(),
          publicOverlay: false
        });
      }).then(function (overlays) {
        project.addOrUpdateDataOverlays(overlays);
        return project;
      });
    }
  });
};

/**
 *
 * @param {Project} project
 * @returns {PromiseLike|Promise}
 */
ServerConnector.updateProject = function (project) {
  var self = this;
  var queryParams = {
    projectId: project.getProjectId()
  };
  var filterParams = {
    project: {
      owner: project.getOwner(),
      name: project.getName(),
      version: project.getVersion(),
      notifyEmail: project.getNotifyEmail(),
      organism: self.serialize(project.getOrganism()),
      disease: self.serialize(project.getDisease()),
      license: {
        id: project.getLicenseId()
      },
      customLicenseName: project.getCustomLicenseName(),
      customLicenseUrl: project.getCustomLicenseUrl()
    }
  };
  if (project.getOrganism() === undefined) {
    filterParams.project.organism = {};
  }
  if (project.getDisease() === undefined) {
    filterParams.project.disease = {};
  }

  return self.sendPatchRequest(self.getProjectUrl(queryParams), filterParams).then(function (content) {
    var downloadedProject = new Project(content);
    return project.update(downloadedProject);
  }).then(function () {
    return project;
  }).catch(function (error) {
    return self.getProject(project.getProjectId()).then(function (project) {
      return self._processUpdateError(error);
    });
  });
};

/**
 *
 * @param {string} data.oldProjectId
 * @param {string} data.newProjectId
 * @return {Promise}
 */
ServerConnector.moveProject = function (data) {
  var self = this;
  var queryParams = {
    projectId: data.oldProjectId
  };

  var filterParams = {
    projectId: data.newProjectId
  };

  return self.sendJsonPostRequest(self.getMoveProjectUrl(queryParams), filterParams)
    .catch(function (error) {
      return self._processUpdateError(error);
    });
};

ServerConnector.removeProject = function (projectId) {
  var self = this;
  var queryParams = {
    projectId: projectId
  };
  return self.sendDeleteRequest(self.getProjectUrl(queryParams)).then(function (content) {
    return self._updateProjectFromResponse(content);
  }).then(null, function (error) {
    return self._processUpdateError(error);
  });
};

/**
 *
 * @param {string} content
 * @returns {Project}
 * @private
 */
ServerConnector._updateProjectFromResponse = function (content) {
  var self = this;
  var project = new Project(content);
  if (self._projectsById[project.getProjectId()] !== undefined) {
    self._projectsById[project.getProjectId()].update(project);
  } else {
    self._projectsById[project.getProjectId()] = project;
  }
  return self._projectsById[project.getProjectId()];
}

ServerConnector.archiveProject = function (projectId) {
  var self = this;
  var queryParams = {
    projectId: projectId
  };
  return self.sendJsonPostRequest(self.getArchiveProjectUrl(queryParams), {}).then(function (content) {
    return self._updateProjectFromResponse(content);
  }).then(null, function (error) {
    return self._processUpdateError(error);
  });
};

ServerConnector.reviveProject = function (projectId) {
  var self = this;
  var queryParams = {
    projectId: projectId
  };
  return self.sendJsonPostRequest(self.getReviveProjectUrl(queryParams), {}).then(function (content) {
    return self._updateProjectFromResponse(content);
  }).then(null, function (error) {
    return self._processUpdateError(error);
  });
};

ServerConnector.addProject = function (options) {
  var self = this;
  var queryParams = {
    projectId: options.projectId
  };
  return self.sendPostRequest(self.getProjectUrl(queryParams), options).then(function (content) {
    var project = new Project(content);
    if (self._projectsById[project.getProjectId()] !== undefined) {
      self._projectsById[project.getProjectId()].update(project);
    } else {
      self._projectsById[project.getProjectId()] = project;
    }
    return project;
  }).catch(function (error) {
    return self.processNetworkError(error);
  });
};

/**
 *
 * @param {Error} error
 * @return {Promise}
 * @private
 */
ServerConnector._processUpdateError = function (error) {
  if (error instanceof NetworkError) {
    if (error.statusCode === HttpStatus.FORBIDDEN) {
      return Promise.reject(new SecurityError("Access denied."));
    } else {
      return Promise.reject(error);
    }
  } else {
    return Promise.reject(error);
  }
};

ServerConnector.serialize = function (object) {
  var result = {};
  if (object instanceof Annotation) {
    result.type = object.getType();
    result.resource = object.getResource();
  } else if (object === undefined) {
    result = undefined;
  } else {
    throw new Error("Unhandled object type: " + (typeof object));
  }
  return result;
};

/**
 *
 * @param {boolean} [reload=false]
 * @return {Promise<Project[]>}
 */
ServerConnector.getProjects = function (reload) {
  var self = this;
  if (self._projects.length > 0 && !reload) {
    return Promise.resolve(self._projects);
  } else {
    return self.sendGetRequest(self.getProjectsUrl()).then(function (content) {
      var parsedData = JSON.parse(content);
      self._projects.length = 0;
      for (var i = 0; i < parsedData.length; i++) {
        var project = new Project(JSON.stringify(parsedData[i]));
        if (self._projectsById[project.getProjectId()] instanceof Project) {
          self._projectsById[project.getProjectId()].update(project);
        } else {
          self._projectsById[project.getProjectId()] = project;
        }
        project = self._projectsById[project.getProjectId()];
        self._projects.push(project);
      }
      return self._projects;
    });
  }
};

/**
 *
 * @param {string} [projectId]
 * @returns {PromiseLike<ProjectStatistics>}
 */
ServerConnector.getProjectStatistics = function (projectId) {
  var queryParams = {};
  var filterParams = {};
  var self = this;
  var content;
  return self.getProjectId(projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendGetRequest(self.getProjectStatisticsUrl(queryParams, filterParams));
  }).then(function (result) {
    content = JSON.parse(result);
    return self.getConfiguration();
  }).then(function (configuration) {
    return new ProjectStatistics(content, configuration);
  });
};

/**
 *
 * @returns {Promise<User>|PromiseLike<User>}
 */
ServerConnector.getLoggedUser = function () {
  var self = this;
  if (self._loggedUser !== undefined && self._loggedUser !== null) {
    return Promise.resolve(self._loggedUser);
  } else {
    if (self.getSessionData().getLogin() === null) {
      logger.warn("User login is null...");
      window.location.reload(false);
    }
    //this prevents double calls
    self._loggedUser = self.getUser(self.getSessionData().getLogin()).then(function (user) {
      if (user !== null) {
        self._loggedUser = user;
        return self._loggedUser;
      } else {
        return Promise.reject(new Error("User " + self.getSessionData().getLogin() + " doesn't exist"));
      }
    });
    return self._loggedUser;
  }
};

/**
 *
 * @returns {Promise<PluginData[]>}
 */
ServerConnector.getPluginsData = function () {
  var self = this;
  return self.sendGetRequest(self.getPluginsUrl()).then(function (content) {
    var data = JSON.parse(content);
    var result = [];
    for (var i = 0; i < data.length; i++) {
      result.push(new PluginData(data[i]));
    }
    return result;
  });
};

/**
 *
 * @param {string} hash
 * @returns {Promise<PluginData>}
 */
ServerConnector.getPluginData = function (hash) {
  var self = this;
  return self.sendGetRequest(self.getPluginUrl({hash: hash})).then(function (content) {
    var data = JSON.parse(content);
    return new PluginData(data);
  }).catch(self.processNetworkError);
};

/**
 *
 * @param {Object} params
 * @param {string} params.hash
 *
 * @returns {Promise<PluginData[]>}
 */
ServerConnector.removePlugin = function (params) {
  var self = this;
  return self.sendDeleteRequest(self.getPluginUrl(params));
};

/**
 *
 * @param {Object} params
 * @param {string} params.hash
 * @param {string} [params.newHash]
 * @param {boolean} [params.isDefault]
 * @param {boolean} [params.version]
 * @param {boolean} [params.name]
 *
 * @returns {Promise<PluginData[]>}
 */
ServerConnector.updatePlugin = function (params) {
  var self = this;
  var content = {
    plugin: {
      hash: params.newHash ? params.newHash : params.hash,
      isDefault: params.isDefault,
      version: params.version,
      name: params.name
    }
  };
  return self.sendPatchRequest(self.getPluginUrl(params), content);
};

/**
 *
 * @param {string} login
 * @returns {Promise}
 */
ServerConnector.getUser = function (login) {
  var self = this;
  var queryParams = {
    login: login
  };
  var filterParams = {
    columns: ["id", "login", "name", "surname", "orcidId", "email", "minColor", "maxColor", "neutralColor", "simpleColor", "removed", "privileges", "preferences", "termsOfUseConsent", "connectedToLdap", "active", "confirmed"]
  };

  var configuration;
  return self.getConfiguration().then(function (result) {
    configuration = result;
    return self.sendGetRequest(self.getUserUrl(queryParams, filterParams));
  }).then(function (content) {
    var obj = JSON.parse(content);
    var user = new User(obj);
    if (self._usersByLogin[user.getLogin()] instanceof User) {
      self._usersByLogin[user.getLogin()].update(user);
    } else {
      self._usersByLogin[user.getLogin()] = user;
    }
    return self._usersByLogin[user.getLogin()];
  }).then(null, function (error) {
    return self.processNetworkError(error);
  });
};

/**
 *
 * @param {User} user
 * @returns {Promise}
 */
ServerConnector.updateUser = function (user) {
  var self = this;
  var queryParams = {
    login: user.getLogin()
  };
  var filterParams = {
    user: {
      name: user.getName(),
      surname: user.getSurname(),
      password: user.getPassword(),
      email: user.getEmail(),
      orcidId: user.getOrcidId(),
      termsOfUseConsent: user.isTermsOfUseConsent(),
      connectedToLdap: user.isConnectedToLdap(),
      active: user.isActive()
    }
  };
  return self.sendPatchRequest(self.getUserUrl(queryParams), filterParams)
    .then(function (content) {
      var obj = JSON.parse(content);
      var updatedUser = new User(obj);
      return user.update(updatedUser);
    })
    .catch(self.processNetworkError);
};

/**
 *
 * @returns {Promise}
 */
ServerConnector.resetUserTos = function () {
  var self = this;
  return self.getUsers(true).then(function (users) {
    var promises = [];
    for (var i = 0; i < users.length; i++) {
      var user = users[i];
      user.setTermsOfUseConsent(false);
      promises.push(self.sendPatchRequest(self.getUserUrl({login: user.getLogin()}), {user: {termsOfUseConsent: false}}));
    }
    return Promise.all(promises);
  });
};

/**
 *
 * @param {User} user
 * @returns {Promise}
 */
ServerConnector.addUser = function (user) {
  var self = this;
  var queryParams = {
    login: user.getLogin()
  };
  var filterParams = {
    login: user.getLogin(),
    name: user.getName(),
    surname: user.getSurname(),
    password: user.getPassword(),
    email: user.getEmail()
  };
  return self.sendPostRequest(self.getUserUrl(queryParams), filterParams)
    .catch(function (error) {
      return self.processNetworkError(error);
    }).then(function () {
      return self.getConfiguration();
    }).then(function (configuration) {
      return self.grantUserPrivileges({user: user, privileges: user.getPrivileges()});
    });
};

/**
 *
 * @param {string} login
 * @returns {Promise}
 */
ServerConnector.removeUser = function (login) {
  var self = this;
  var queryParams = {
    login: login
  };
  return self.sendDeleteRequest(self.getUserUrl(queryParams));
};

/**
 *
 * @param {Object} params
 * @param {User} params.user
 * @param {Authority[]} params.privileges
 *
 * @returns {Promise}
 */
ServerConnector.grantUserPrivileges = function (params) {
  var self = this;
  var queryParams = {
    login: params.user.getLogin()
  };
  var privileges = {};
  for (var i = 0; i < params.privileges.length; i++) {
    var privilege = params.privileges[i];
    if (privilege.objectId !== undefined && privilege.objectId !== null) {
      privileges[privilege.privilegeType + ":" + privilege.objectId] = true;
    } else {
      privileges[privilege.privilegeType] = true;
    }
  }

  return self.sendPatchRequest(self.getUpdateUserPrivilegesUrl(queryParams), {
    privileges: privileges
  }).then(function (content) {
    var obj = JSON.parse(content);
    var user = new User(obj);
    var promise = Promise.resolve();
    if (self._usersByLogin[user.getLogin()] !== undefined) {
      promise = self._usersByLogin[user.getLogin()].update(user);
    } else {
      self._usersByLogin[user.getLogin()] = user;
    }
    return promise;
  }).then(function () {
    return self._usersByLogin[params.user.getLogin()];
  }).catch(function (error) {
    return self.processNetworkError(error);
  });
};

/**
 *
 * @param {Object} params
 * @param {User} params.user
 * @param {string} params.projectId
 * @param {string} params.privilegeType
 *
 * @returns {Promise}
 */
ServerConnector.grantProjectPrivilege = function (params) {
  var self = this;
  var queryParams = {
    projectId: params.projectId
  };
  var privileges = [{login: params.user.getLogin(), privilegeType: params.privilegeType}];

  return self.sendPatchRequest(self.getGrantProjectPrivilegesUrl(queryParams), privileges).then(function (content) {
    if (self._usersByLogin[params.user.getLogin()] !== undefined) {
      self._usersByLogin[params.user.getLogin()].setPrivilege({
        privilegeType: params.privilegeType,
        objectId: params.projectId
      });
    }
  }).catch(function (error) {
    return self.processNetworkError(error, true);
  });
};

/**
 *
 * @param {Object} params
 * @param {User} params.user
 * @param {string} params.projectId
 * @param {string} params.privilegeType
 *
 * @returns {Promise}
 */
ServerConnector.revokeProjectPrivilege = function (params) {
  var self = this;
  var queryParams = {
    projectId: params.projectId
  };
  var privileges = [{login: params.user.getLogin(), privilegeType: params.privilegeType}];

  return self.sendPatchRequest(self.getRevokeProjectPrivilegesUrl(queryParams), privileges).then(function (content) {
    if (self._usersByLogin[params.user.getLogin()] !== undefined) {
      self._usersByLogin[params.user.getLogin()].revokePrivilege({
        privilegeType: params.privilegeType,
        objectId: params.projectId
      });
    }
  }).catch(function (error) {
    return self.processNetworkError(error, true);
  });
};

/**
 *
 * @param {Object} params
 * @param {User} params.user
 * @param {Authority[]} params.privileges
 *
 * @returns {Promise}
 */
ServerConnector.revokeUserPrivileges = function (params) {
  var self = this;
  var queryParams = {
    login: params.user.getLogin()
  };
  var privileges = {};
  for (var i = 0; i < params.privileges.length; i++) {
    var privilege = params.privileges[i];
    if (privilege.objectId !== undefined && privilege.objectId !== null) {
      privileges[privilege.privilegeType + ":" + privilege.objectId] = false;
    } else {
      privileges[privilege.privilegeType] = false;
    }
  }

  return self.sendPatchRequest(self.getUpdateUserPrivilegesUrl(queryParams), {
    privileges: privileges
  }).then(function (content) {
    var obj = JSON.parse(content);
    var user = new User(obj);
    if (self._usersByLogin[user.getLogin()] !== undefined) {
      self._usersByLogin[user.getLogin()].update(user);
    } else {
      self._usersByLogin[user.getLogin()] = user;
    }
    return self._usersByLogin[user.getLogin()];
  }).then(null, function (error) {
    return self.processNetworkError(error);
  });
};

/**
 *
 * @param {Error} error
 * @param {boolean} [forbidNotFound=false]
 * @returns {Promise}
 */
ServerConnector.processNetworkError = function (error, forbidNotFound) {
  if ((error instanceof NetworkError)) {
    switch (error.statusCode) {
      case HttpStatus.NOT_FOUND:
        if (forbidNotFound) {
          return Promise.reject(new ObjectNotFoundError("Object not found."));
        }
        return null;
      case HttpStatus.CONFLICT:
        return Promise.reject(new ObjectExistsError("Object already exists."));
      case HttpStatus.FORBIDDEN:
        return Promise.reject(new SecurityError("Access denied."));
      default:
        return Promise.reject(error);
    }
  } else {
    return Promise.reject(error);
  }
};

/**
 *
 * @param {Object} params
 * @param {User} params.user
 * @param {UserPreferences} params.preferences
 *
 * @returns {Promise}
 */
ServerConnector.updateUserPreferences = function (params) {
  var self = this;
  var queryParams = {
    login: params.user.getLogin()
  };

  return self.sendPatchRequest(self.getUpdateUserPreferencesUrl(queryParams), {
    preferences: params.preferences.toExport()
  }).then(function (content) {
    var obj = JSON.parse(content);
    var user = new User(obj);
    params.user.getPreferences().update(user.getPreferences());
    return params.user;
  });
};

/**
 *
 * @param {boolean} [forceRefresh=false]
 *
 * @returns {Promise}
 */
ServerConnector.getUsers = function (forceRefresh) {
  var self = this;

  if (self._users.length > 0 && !forceRefresh) {
    return Promise.resolve(self._users);
  } else {
    return self.sendGetRequest(self.getUsersUrl()).then(function (content) {
      var parsedData = JSON.parse(content);
      self._users.length = 0;
      for (var i = 0; i < parsedData.length; i++) {
        var user = new User(parsedData[i]);
        if (self._usersByLogin[user.getLogin()] !== undefined) {
          self._usersByLogin[user.getLogin()].update(user);
        } else {
          self._usersByLogin[user.getLogin()] = user;
        }
        self._users.push(self._usersByLogin[user.getLogin()]);
      }
      return self._users;
    }).then(null, function (error) {
      return self.processNetworkError(error);
    });
  }
};

/**
 *
 * @returns {Promise<Array>}
 */
ServerConnector.getReferenceGenomes = function () {
  var self = this;

  return self.sendGetRequest(self.getReferenceGenomesUrl()).then(function (content) {
    var result = [];
    var parsedData = JSON.parse(content);
    for (var i = 0; i < parsedData.length; i++) {
      var genome = new ReferenceGenome(parsedData[i]);
      result.push(genome);
    }
    return result;
  });
};

/**
 *
 * @param {Object} [params]
 * @param {string} [params.creator] user login
 * @param {boolean} [params.publicOverlay]
 * @param {string} [params.projectId]
 *
 * @returns {Promise<Array>}
 */
ServerConnector.getOverlays = function (params) {
  var self = this;
  if (params === undefined) {
    params = {};
  }
  var queryParams = {};
  var filterParams = {
    creator: params.creator,
    publicOverlay: params.publicOverlay
  };
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendGetRequest(self.getOverlaysUrl(queryParams, filterParams));
  }).then(function (content) {
    var arr = JSON.parse(content);
    var result = [];
    for (var i = 0; i < arr.length; i++) {
      var overlay = new DataOverlay(arr[i]);
      result.push(overlay);
    }
    return result;
  });
};

/**
 *
 * @param {Object} [params]
 * @param {string} [params.projectId]
 *
 * @returns {Promise<Array>}
 */
ServerConnector.getBackgrounds = function (params) {
  var self = this;
  if (params === undefined) {
    params = {};
  }
  var queryParams = {};
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendGetRequest(self.getBackgroundsUrl(queryParams));
  }).then(function (content) {
    var arr = JSON.parse(content);
    var result = [];
    for (var i = 0; i < arr.length; i++) {
      var background = new Background(arr[i]);
      result.push(background);
    }
    return result;
  });
};

/**
 *
 * @param {number} overlayId
 * @param {string} [projectId]
 *
 * @returns {Promise}
 */
ServerConnector.getOverlayElements = function (overlayId, projectId) {
  var self = this;
  if (overlayId === undefined) {
    throw new Error("Overlay id must be defined");
  }
  var filterParams = {};
  var modelIds = ["*"];
  return self.getProjectId(projectId).then(function (result) {
    var queryParams = {
      overlayId: overlayId,
      projectId: result,
      modelId: "*"
    };
    if (self.getSessionData().getProject() !== null) {
      modelIds = [];
      var project = self.getSessionData().getProject();
      for (var i = 0; i < project.getModels().length; i++) {
        modelIds.push(project.getModels()[i].getId());
      }
    }
    var promises = [];
    for (var j = 0; j < modelIds.length; j++) {
      promises.push(self.sendGetRequest(self.getOverlayElementsUrl({
        overlayId: overlayId,
        projectId: result,
        modelId: modelIds[j]
      }, filterParams)));
    }
    return Promise.all(promises);
  }).then(function (contentList) {
    var result = [];
    for (var j = 0; j < contentList.length; j++) {
      var content = contentList[j];
      var arr = JSON.parse(content);
      for (var i = 0; i < arr.length; i++) {
        var element = arr[i];
        if (element.type === "REACTION") {
          result.push(new LayoutReaction(element.overlayContent));
        } else if (element.type === "ALIAS") {
          result.push(new LayoutAlias(element.overlayContent));
        } else {
          throw new Error("Unknown element type: " + element.type);
        }
      }
    }
    return result;
  });
};

/**
 *
 * @param params
 * @return {Promise<[LayoutReaction|LayoutAlias]>}
 */
ServerConnector.getFullOverlayElement = function (params) {
  var self = this;

  var queryParams = {
    overlayId: params.overlay.getId(),
    modelId: params.element.getModelId(),
    id: params.element.getId()
  };
  var filterParams = {};

  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendGetRequest(self.getFullOverlayElementUrl(queryParams, filterParams));
  }).then(function (content) {
    var element = JSON.parse(content);
    var result = [];
    for (var i = 0; i < element.length; i++) {
      var entry = element[i];
      if (entry.type === "REACTION") {
        result.push(new LayoutReaction(entry.overlayContent));
      } else if (entry.type === "ALIAS") {
        result.push(new LayoutAlias(entry.overlayContent));
      } else {
        throw new Error("Unknown element type: " + entry.type);
      }
    }
    return result;
  });
};

/**
 *
 * @param {string|null} [projectId]
 * @returns {Promise<string>| PromiseLike<string>}
 */
ServerConnector.getProjectId = function (projectId) {
  var self = this;
  if (projectId !== undefined && projectId !== null && projectId !== "") {
    return Promise.resolve(projectId);
  } else if (GuiConnector.getParams['id'] !== undefined) {
    return Promise.resolve(GuiConnector.getParams['id']);
  } else {
    return self.getConfigurationParam(ConfigurationType.DEFAULT_MAP);
  }
};

ServerConnector.getMaxSearchDistance = function () {
  return this.getConfigurationParam(ConfigurationType.SEARCH_DISTANCE);
};

/**
 *
 * @param {number} overlayId
 * @param {string} projectId
 * @returns {PromiseLike<DataOverlay>|Promise<DataOverlay>}
 */
ServerConnector.getOverlayById = function (overlayId, projectId) {
  var self = this;
  var queryParams = {
    overlayId: overlayId
  };
  var filterParams = {};
  return self.getProjectId(projectId).then(function (data) {
    queryParams.projectId = data;
    return self.sendGetRequest(self.getOverlayByIdUrl(queryParams, filterParams));
  }).then(function (content) {
    return new DataOverlay(JSON.parse(content));
  }).catch(self.processNetworkError);
};

/**
 *
 * @param {number} [params.modelId]
 * @param {number[]} [params.ids]
 * @param {number[]} [params.participantId]
 * @param {string[]} [params.columns]
 * @param {string} [params.projectId]
 * @returns {*}
 */
ServerConnector.getReactions = function (params) {
  var self = this;
  var queryParams = {
    modelId: params.modelId
  };
  if (params.ids === undefined) {
    params.ids = [];
  }
  if (params.participantId === undefined) {
    params.participantId = [];
  }
  var filterParams = {
    id: params.ids,
    columns: params.columns,
    participantId: params.participantId
  };
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    if (filterParams.id.length > self.MAX_NUMBER_OF_IDS_IN_GET_QUERY || filterParams.participantId.length > self.MAX_NUMBER_OF_IDS_IN_GET_QUERY) {
      return self.sendPostRequest(self.getReactionsUrl(queryParams), filterParams);
    } else {
      return self.sendGetRequest(self.getReactionsUrl(queryParams, filterParams));
    }

  }).then(function (content) {
    var array = JSON.parse(content);
    var result = [];
    for (var i = 0; i < array.length; i++) {
      result.push(new Reaction(array[i]));
    }
    return result;
  });
};

/**
 *
 * @param {number} [params.modelId]
 * @param {number[]} [params.ids]
 * @param {number[]} [params.includedCompartmentIds]
 * @param {number[]} [params.excludedCompartmentIds]
 * @param {string[]} [params.columns]
 * @param {string} [params.projectId]
 * @param {string} [params.type]
 * @returns {*}
 */
ServerConnector.getAliases = function (params) {
  var self = this;
  var queryParams = {
    modelId: params.modelId
  };
  if (params.ids === undefined) {
    params.ids = [];
  }
  if (params.includedCompartmentIds === undefined) {
    params.includedCompartmentIds = [];
  }
  if (params.excludedCompartmentIds === undefined) {
    params.excludedCompartmentIds = [];
  }
  var filterParams = {
    id: params.ids,
    columns: params.columns,
    type: params.type,
    excludedCompartmentIds: params.excludedCompartmentIds,
    includedCompartmentIds: params.includedCompartmentIds

  };
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    if (filterParams.id.length > self.MAX_NUMBER_OF_IDS_IN_GET_QUERY
      || filterParams.excludedCompartmentIds.length > self.MAX_NUMBER_OF_IDS_IN_GET_QUERY
      || filterParams.includedCompartmentIds.length > self.MAX_NUMBER_OF_IDS_IN_GET_QUERY
    ) {
      return self.sendPostRequest(self.getAliasesUrl(queryParams), filterParams);
    } else {
      return self.sendGetRequest(self.getAliasesUrl(queryParams, filterParams));
    }
  }).then(function (content) {
    var array = JSON.parse(content);
    var result = [];
    for (var i = 0; i < array.length; i++) {
      result.push(new Alias(array[i]));
    }
    return result;
  });
};

ServerConnector.getLightComments = function (params) {
  params.columns = ["id", "elementId", "modelId", "type", "icon", "removed", "pinned"];
  return this.getComments(params);
};

ServerConnector.getComments = function (params) {
  var self = this;
  var queryParams = {
    elementId: params.elementId,
    elementType: params.elementType,
    coordinates: params.coordinates
  };
  var filterParams = {
    columns: params.columns
  };
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendGetRequest(self.getCommentsUrl(queryParams, filterParams));
  }).then(function (content) {
    var array = JSON.parse(content);
    var result = [];
    for (var i = 0; i < array.length; i++) {
      result.push(new Comment(array[i]));
    }
    return result;
  });
};

/**
 *
 * @param {Project} [project]
 * @returns {SessionData}
 */
ServerConnector.getSessionData = function (project) {
  if (this._sessionData === undefined) {
    this._sessionData = new SessionData(null);
  }
  if (project !== undefined && this._sessionData.getProject() === null) {
    this._sessionData.setProject(project);
  }

  return this._sessionData;
};

/**
 *
 * @param {number} params.modelId
 * @param {number} [params.projectId]
 * @param {Point} params.coordinates
 * @param {number} [params.count]
 * @param {string[]} [params.type]
 *
 * @returns {PromiseLike<IdentifiedElement[]>|Promise<IdentifiedElement[]>}
 */
ServerConnector.getClosestElementsByCoordinates = function (params) {
  var self = this;
  var queryParams = {
    modelId: params.modelId
  };
  var filterParams = {
    coordinates: params.coordinates,
    count: params.count,
    type: params.type
  };
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendGetRequest(self.getSearchUrl(queryParams, filterParams));
  }).then(function (content) {
    var array = JSON.parse(content);
    var result = [];
    for (var i = 0; i < array.length; i++) {
      result.push(new IdentifiedElement(array[i]));
    }
    return result;
  });
};

ServerConnector.createSession = function () {
  var self = this;
  return self.isSessionValid().then(function (isValid) {
    if (!isValid) {
      return self.login();
    }
  });
};
ServerConnector.login = function (login, password) {
  var self = this;
  var params = {};
  if (login !== undefined && login !== "" && login !== null) {
    params.login = login;
    params.password = password;
  } else {
    params.login = "anonymous";
    self._currentTabLogin = params.login;
    self.getSessionData().setLogin(params.login);
    return Promise.resolve(self.getSessionData().getToken());
  }
  return self.sendPostRequest(self.loginUrl(), params).then(function (content) {
    var data = JSON.parse(content);
    if (data["login"] !== undefined) {
      params.login = data["login"];
    }
    self._currentTabLogin = params.login;
    self.getSessionData().setLogin(params.login);
    return Promise.resolve(self.getSessionData().getToken());
  }).catch(function (error) {
    if (error instanceof NetworkError && (error.statusCode === HttpStatus.FORBIDDEN || error.statusCode === HttpStatus.UNAUTHORIZED)) {
      throw new InvalidCredentialsError("Invalid credentials");
    } else {
      throw error;
    }
  });
};

ServerConnector.confirmEmail = function (login, token) {
  var self = this;
  return self.sendPostRequest(self.getConfirmEmailUrl({
    login: login,
    token: token
  }), {token: token}).then(function (content) {
    return JSON.parse(content).message;
  });
};

ServerConnector.logout = function () {
  var self = this;
  return self.sendGetRequest(self.logoutUrl()).then(function () {
    self.getSessionData().clear();
    window.location.reload(false);
  });
};

/**
 *
 * @param {string} params.query
 * @param {string} [params.projectId]
 * @param {number} [params.count]
 * @param {boolean} params.perfectMatch
 * @return {PromiseLike<*[]>}
 */
ServerConnector.getElementsByQuery = function (params) {
  var self = this;
  var queryParams = {
    modelId: params.modelId
  };
  var filterParams = {
    query: encodeURIComponent(params.query),
    perfectMatch: params.perfectMatch
  };

  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.getConfigurationParam(ConfigurationType.SEARCH_RESULT_NUMBER);
  }).then(function (defaultCount) {
    if (params.count !== undefined) {
      filterParams.count = params.count;
    } else {
      filterParams.count = defaultCount;
    }
    return self.sendGetRequest(self.getSearchUrl(queryParams, filterParams));
  }).then(function (content) {
    var array = JSON.parse(content);
    var result = [];
    for (var i = 0; i < array.length; i++) {
      result.push(new IdentifiedElement(array[i]));
    }
    return result;
  });
};

ServerConnector.getDrugsByQuery = function (params) {
  var self = this;
  var queryParams = {};
  var filterParams = {
    query: encodeURIComponent(params.query)
  };
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendGetRequest(self.getSearchDrugsUrl(queryParams, filterParams));
  }).then(function (content) {
    var array = JSON.parse(content);
    var result = [];
    for (var i = 0; i < array.length; i++) {
      result.push(new Drug(array[i]));
    }
    return result;
  });
};

ServerConnector.getChemicalsByQuery = function (params) {
  var self = this;
  var queryParams = {};
  var filterParams = {
    query: encodeURIComponent(params.query)
  };
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendGetRequest(self.getSearchChemicalsUrl(queryParams, filterParams));
  }).then(function (content) {
    var array = JSON.parse(content);
    var result = [];
    for (var i = 0; i < array.length; i++) {
      result.push(new Chemical(array[i]));
    }
    return result;
  });
};

ServerConnector.getOverlaySourceDownloadUrl = function (params) {
  var self = this;
  var queryParams = {
    overlayId: params.overlayId
  };
  var filterParams = {};
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.getOverlaySourceUrl(queryParams, filterParams);
  });
};

/**
 *
 * @param {string} [params.projectId]
 * @param {number} params.modelId
 * @param {string} [params.token]
 * @param {string} [params.polygonString]
 * @param {string} params.handlerClass
 * @param {number} params.backgroundOverlayId
 * @param {number} [params.zoomLevel]
 * @param {Array<number>>} [params.overlayIds]
 * @return {PromiseLike<*>}
 */
ServerConnector.getImageDownloadUrl = function (params) {
  var self = this;
  var queryParams = {
    projectId: params.projectId,
    modelId: params.modelId
  };
  var filterParams = {
    token: params.token,
    polygonString: params.polygonString,
    handlerClass: params.handlerClass,
    backgroundOverlayId: params.backgroundOverlayId,
    zoomLevel: params.zoomLevel,
    overlayIds: this.idsToString(params.overlayIds)
  };

  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.getImageUrl(queryParams, filterParams);
  });
};

/**
 *
 * @param {Object} params
 * @param {string} [params.projectId]
 * @param {number} params.modelId
 * @param {string} [params.polygonString]
 * @param {string} params.handlerClass
 * @param {number} [params.backgroundOverlayId]
 * @param {number} [params.zoomLevel]
 * @param {number[]} [params.overlayIds]
 * @param {number[]} [params.elementIds]
 * @param {number[]} [params.reactionIds]
 * @returns {Promise<String>}
 */
ServerConnector.getModelDownloadUrl = function (params) {
  var self = this;
  var queryParams = {
    projectId: params.projectId,
    modelId: params.modelId
  };
  var filterParams = {
    polygonString: params.polygonString,
    handlerClass: params.handlerClass,
    backgroundOverlayId: params.backgroundOverlayId,
    zoomLevel: params.zoomLevel,
    overlayIds: this.idsToString(params.overlayIds),
    elementIds: this.idsToString(params.elementIds),
    reactionIds: this.idsToString(params.reactionIds)
  };
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.getModelPartUrl(queryParams, filterParams);
  });
};

/**
 *
 * @param {Object} params
 * @param {string} [params.projectId]
 * @param {number} params.modelId
 * @param {string} [params.polygonString]
 * @param {string} params.handlerClass
 * @param {number} [params.backgroundOverlayId]
 * @param {number} [params.zoomLevel]
 * @param {number[]} [params.overlayIds]
 * @param {number[]} [params.elementIds]
 * @param {number[]} [params.reactionIds]
 * @returns {Promise<Array>}
 */
ServerConnector.getModelDownloadWarnings = function (params) {
  var self = this;
  var queryParams = {
    projectId: params.projectId,
    modelId: params.modelId
  };
  var filterParams = {
    polygonString: params.polygonString,
    handlerClass: params.handlerClass,
    backgroundOverlayId: params.backgroundOverlayId,
    zoomLevel: params.zoomLevel,
    overlayIds: this.idsToString(params.overlayIds),
    elementIds: this.idsToString(params.elementIds),
    reactionIds: this.idsToString(params.reactionIds)
  };
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendGetRequest(self.getModelPartWarningsUrl(queryParams, filterParams));
  }).then(function (content) {
    return JSON.parse(content).data;
  });
};

/**
 *
 * @param {Object} params
 * @param {string} [params.projectId]
 * @param {number} params.modelId
 * @param {string} [params.polygonString]
 * @param {string} params.handlerClass
 * @param {number} [params.backgroundOverlayId]
 * @param {number} [params.zoomLevel]
 * @param {number[]} [params.overlayIds]
 * @param {number[]} [params.elementIds]
 * @param {number[]} [params.reactionIds]
 * @returns {Promise}
 */
ServerConnector.getPartOfModelInExportFormat = function (params) {
  var self = this;
  var queryParams = {
    projectId: params.projectId,
    modelId: params.modelId
  };
  var filterParams = {
    polygonString: params.polygonString,
    handlerClass: params.handlerClass,
    backgroundOverlayId: params.backgroundOverlayId,
    zoomLevel: params.zoomLevel,
    overlayIds: this.idsToString(params.overlayIds),
    elementIds: this.idsToString(params.elementIds),
    reactionIds: this.idsToString(params.reactionIds)
  };
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendPostRequest(self.getModelPartUrl(queryParams), filterParams);
  });
};

ServerConnector.getImageConverters = function () {
  var self = this;
  return self.getConfiguration().then(function (configuration) {
    return configuration.getImageConverters();
  });
};
ServerConnector.getModelConverters = function () {
  var self = this;
  return self.getConfiguration().then(function (configuration) {
    return configuration.getModelConverters();
  });
};

ServerConnector.getProjectSourceDownloadUrl = function (params) {
  if (params === undefined) {
    params = {};
  }
  var queryParams = {};
  var filterParams = {};
  var self = this;
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.getProjectSourceUrl(queryParams, filterParams);
  });
};

ServerConnector.getDrugNamesByTarget = function (params) {
  var self = this;
  var queryParams = {};
  var filterParams = {
    columns: ["name"],
    target: params.target.getType() + ":" + params.target.getId()
  };
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendGetRequest(self.getSearchDrugsUrl(queryParams, filterParams));
  }).then(function (content) {
    var result = [];
    var object = JSON.parse(content);
    for (var i = 0; i < object.length; i++) {
      result.push(object[i].name);
    }
    return result;
  });
};

ServerConnector.getChemicalNamesByTarget = function (params) {
  var self = this;
  var queryParams = {};
  var filterParams = {
    columns: ["name"],
    target: params.target.getType() + ":" + params.target.getId()
  };
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendGetRequest(self.getSearchChemicalsUrl(queryParams, filterParams));
  }).then(function (content) {
    var result = [];
    var object = JSON.parse(content);
    for (var i = 0; i < object.length; i++) {
      result.push(object[i].name);
    }
    return result;
  });
};

/**
 *
 * @param {Object} params
 * @param {number} params.modelId
 * @param {string} params.name
 * @param {string} params.email
 * @param {string} params.content
 * @param {boolean} params.pinned
 * @param {string} params.coordinates
 * @param {number} [params.elementId]
 * @param {number} [params.elementType]
 * @returns {PromiseLike<Comment>}
 */
ServerConnector.addComment = function (params) {
  var self = this;
  var queryParams = {
    elementId: params.elementId,
    elementType: params.elementType,
    coordinates: self.pointToString(params.coordinates),
    modelId: params.modelId
  };
  var filterParams = params;
  delete filterParams.elementId;
  delete filterParams.elementType;
  if (queryParams.elementType === "POINT") {
    delete filterParams.coordinates;
  } else {
    filterParams.coordinates = self.pointToString(params.coordinates);
  }
  delete filterParams.modelId;
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendPostRequest(self.addCommentUrl(queryParams), filterParams);
  }).then(function (content) {
    var response = JSON.parse(content);
    return new Comment(response);
  });
};

/**
 *
 * @param {DataOverlay} params.overlay
 * @param {string|number} [params.fileId]
 * @param {string} [params.projectId]
 * @returns {Promise<DataOverlay>}
 */
ServerConnector.addOverlay = function (params) {
  var overlay = params.overlay;
  if (!(overlay instanceof DataOverlay)) {
    throw new Error("Invalid overlay: " + overlay);
  }
  var self = this;
  var queryParams = {};
  var data = {
    name: overlay.getName(),
    description: overlay.getDescription(),
    content: overlay.getContent(),
    filename: overlay.getFilename(),
    type: overlay.getType(),
    fileId: params.fileId
  };

  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendPostRequest(self.addOverlayUrl(queryParams), data);
  }).then(function (content) {
    overlay = new DataOverlay(JSON.parse(content));
    return self.callListeners("onAddDataOverlay", overlay);
  }).then(function () {
    return overlay;
  }).catch(self.processNetworkError);
};

/**
 *
 * @param {string} name
 * @param {string} content
 * @returns {Promise}
 */
ServerConnector.addOverlayFromString = function (name, content) {
  var fileName = name + ".txt";
  var overlay = new DataOverlay({
    name: name,
    description: "",
    filename: fileName
  });
  var fileContent = new TextEncoder("UTF8").encode(content);
  var self = this;
  return self.getProjectId().then(function (projectid) {

    return ServerConnector.uploadFile({
      filename: fileName,
      content: fileContent
    }).then(function (file) {
      return ServerConnector.addOverlay({
        fileId: file.id,
        overlay: overlay,
        projectId: self.getProjectId()
      });
    }).then(function (result) {
      return result;
    });
  });
};

/**
 *
 * @param {DataOverlay} overlay
 * @param {Project} [project]
 * @return {Promise<DataOverlay>}
 */
ServerConnector.updateOverlay = function (overlay, project) {
  var self = this;
  var queryParams = {
    overlayId: overlay.getId()
  };
  if (project !== undefined) {
    queryParams.projectId = project.getProjectId();
  }
  var filterParams = {
    overlay: {
      name: overlay.getName(),
      order: overlay.getOrder(),
      description: overlay.getDescription(),
      creator: overlay.getCreator(),
      publicOverlay: overlay.getPublicOverlay(),
    }
  };
  return self.getProjectId(queryParams.projectId).then(function (projectId) {
    queryParams.projectId = projectId;
    return self.sendPatchRequest(self.updateOverlayUrl(queryParams), filterParams);
  }).catch(function (e) {
    if (e instanceof NetworkError && e.statusCode === HttpStatus.BAD_REQUEST) {
      throw new ValidationError(e.content.reason);
    } else {
      return self.processNetworkError(e);
    }
  });

};

/**
 *
 * @param {Background} background
 * @param {Project} [project]
 * @return {Promise}
 */
ServerConnector.updateBackground = function (background, project) {
  var self = this;
  var queryParams = {
    backgroundId: background.getId()
  };
  if (project !== undefined) {
    queryParams.projectId = project.getProjectId();
  }
  var filterParams = {
    name: background.getName(),
    description: background.getDescription(),
    defaultOverlay: background.isDefaultOverlay()
  };
  return self.getProjectId(queryParams.projectId).then(function (projectId) {
    queryParams.projectId = projectId;
    return self.sendPatchRequest(self.updateBackgroundUrl(queryParams), filterParams);
  }).catch(function (e) {
    if (e instanceof NetworkError && e.statusCode === HttpStatus.BAD_REQUEST) {
      throw new ValidationError(e.content.reason);
    } else {
      return self.processNetworkError(e);
    }
  });
};

/**
 *
 * @param {Object} params
 * @param {string} params.projectId
 * @param {MapModel} params.model
 * @return {Promise}
 */
ServerConnector.updateModel = function (params) {
  var self = this;
  var model = params.model;
  var queryParams = {
    projectId: params.projectId,
    modelId: model.getId()
  };
  var filterParams = {
    model: {
      id: model.getId(),
      defaultCenterX: model.getDefaultCenterX(),
      defaultCenterY: model.getDefaultCenterY(),
      defaultZoomLevel: model.getDefaultZoomLevel()
    }
  };
  return self.sendPatchRequest(self.updateModelUrl(queryParams), filterParams);
};

/**
 *
 * @param {Object} params
 * @param {number} params.overlayId
 * @param {string} params.projectId
 * @return {PromiseLike<T> | Promise<T>}
 */
ServerConnector.removeOverlay = function (params) {
  var self = this;
  var queryParams = {
    overlayId: params.overlayId
  };
  var filterParams = {};
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendDeleteRequest(self.deleteOverlayUrl(queryParams), filterParams);
  }).then(function () {
    return self.callListeners("onRemoveDataOverlay", params.overlayId);
  });
};

/**
 *
 * @param {Object} params
 * @param {number} params.backgroundId
 * @param {string} params.projectId
 * @return {PromiseLike<T> | Promise<T>}
 */
ServerConnector.removeBackground = function (params) {
  var self = this;
  var queryParams = {
    backgroundId: params.backgroundId
  };
  var filterParams = {};
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendDeleteRequest(self.deleteBackgroundUrl(queryParams), filterParams);
  }).then(function () {
    return self.callListeners("onRemoveBackground", params.backgroundId);
  });
};

/**
 *
 * @param {Object} params
 * @param {number} params.commentId
 * @param {string} [params.reason]
 * @param {string} [params.projectId]
 *
 * @returns {Promise}
 */
ServerConnector.removeComment = function (params) {
  var self = this;
  var queryParams = {
    commentId: params.commentId
  };
  var filterParams = {
    reason: params.reason
  };
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendDeleteRequest(self.deleteCommentUrl(queryParams), filterParams);
  });
};

ServerConnector.getSuggestedQueryList = function (projectId) {
  var self = this;
  return self.getProjectId(projectId).then(function (result) {
    projectId = result;
    return self.sendGetRequest(self.getSuggestedQueryListUrl({
      projectId: projectId
    }));
  }).then(function (content) {
    return JSON.parse(content);
  });
};

ServerConnector.getChemicalSuggestedQueryList = function (projectId) {
  var self = this;
  return self.getProjectId(projectId).then(function (result) {
    projectId = result;
    return self.sendGetRequest(self.getChemicalSuggestedQueryListUrl({
      projectId: projectId
    }));
  }).then(function (content) {
    return JSON.parse(content);
  });
};
ServerConnector.getDrugSuggestedQueryList = function (projectId) {
  var self = this;
  return self.getProjectId(projectId).then(function (result) {
    projectId = result;
    return self.sendGetRequest(self.getDrugSuggestedQueryListUrl({
      projectId: projectId
    }));
  }).then(function (content) {
    return JSON.parse(content);
  });
};

ServerConnector.getOverlayTypes = function () {
  var self = this;
  return self.getConfiguration().then(function (configuration) {
    return configuration.getOverlayTypes();
  });
};

/**
 *
 * @param params
 * @returns {Promise}
 */
ServerConnector.getPublications = function (params) {
  var self = this;
  if (params === undefined) {
    params = {};
  }

  var queryParams = {
    modelId: params.modelId
  };
  var filterParams = {
    page: params.page,
    start: params.start,
    length: params.length,
    sortColumn: params.sortColumn,
    sortOrder: params.sortOrder,
    search: params.search
  };
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendGetRequest(self.getPublicationsUrl(queryParams, filterParams));
  }).then(function (content) {
    return JSON.parse(content);
  });
};

/**
 *
 * @param {Object} params
 * @param {string} [params.version]
 * @param {string} [params.organism]
 * @param {string} [params.type]
 * @param {string|number} [params.genomeId]
 *
 * @returns {Promise}
 */
ServerConnector.getReferenceGenome = function (params) {
  var self = this;
  var filterParams = {};
  if (params.genomeId !== undefined) {
    return self.getReferenceGenomes().then(function (referenceGenomes) {
      for (var i = 0; i < referenceGenomes.length; i++) {
        var genome = referenceGenomes[i];
        if (genome.getId() === params.genomeId) {
          return genome;
        }
      }
      return null;
    });
  } else {
    return self.sendGetRequest(self.getReferenceGenomeUrl(params, filterParams)).then(function (content) {
      return new ReferenceGenome(JSON.parse(content));
    });
  }
};


/**
 *
 * @param {Object} params
 * @param {string|number} params.genomeId
 * @param {string} params.mappingName
 * @param {string} params.mappingUrl
 *
 * @returns {Promise}
 */
ServerConnector.addGeneMapping = function (params) {
  var self = this;
  var data = {
    name: params.mappingName,
    url: params.mappingUrl
  };
  return self.sendPostRequest(self.getReferenceGenomeGeneMappingsUrl(params), data).catch(function (e) {
    if (e instanceof NetworkError && e.statusCode === HttpStatus.BAD_REQUEST) {
      var content = JSON.parse(e.content);
      throw new ValidationError(content.reason);
    } else {
      return self.processNetworkError(e);
    }
  });
};
/**
 *
 * @param {Object} params
 * @param {string} params.version
 * @param {Annotation} params.organism
 * @param {string} params.type
 *
 * @returns {Promise<string[]>}
 */
ServerConnector.getAvailableGenomeUrls = function (params) {
  var self = this;
  var filterParams = {};
  return self.sendGetRequest(self.getAvailableGenomeUrlsUrl(params, filterParams)).then(function (content) {
    var result = [];
    var raw = JSON.parse(content);
    for (var i = 0; i < raw.length; i++) {
      result.push(raw[i].url);
    }
    return result;
  });
};

/**
 *
 * @param {ReferenceGenome} genome
 * @returns {Promise<ReferenceGenome>}
 */
ServerConnector.addReferenceGenome = function (genome) {
  var self = this;
  var data = {
    organismId: genome.getOrganism().getResource(),
    type: genome.getType(),
    version: genome.getVersion(),
    url: genome.getSourceUrl()
  };
  return self.sendPostRequest(self.getReferenceGenomesUrl(), data).catch(function (error) {
    return self.processNetworkError(error);
  });
};

/**
 *
 * @param {Object} params
 * @param {string} params.genomeId
 * @param {string} params.mappingId
 *
 * @returns {Promise}
 */
ServerConnector.removeReferenceGenomeGeneMapping = function (params) {
  var self = this;
  return self.sendDeleteRequest(self.getReferenceGenomeGeneMappingUrl(params));
};


/**
 *
 * @param {Object} params
 * @param {Annotation} params.organism
 * @returns {Promise}
 */
ServerConnector.getReferenceGenomeTypes = function (params) {
  var self = this;
  var filterParams = {};
  return self.sendGetRequest(self.getReferenceGenomeTypesUrl(params, filterParams)).then(function (content) {
    return JSON.parse(content);
  });
};

/**
 *
 * @param {Object} params
 * @param {Annotation} params.organism
 * @param {string} params.type
 * @returns {Promise}
 */
ServerConnector.getReferenceGenomeVersions = function (params) {
  var self = this;
  var filterParams = {};
  return self.sendGetRequest(self.getReferenceGenomeVersionsUrl(params, filterParams)).then(function (content) {
    return JSON.parse(content);
  });
};

/**
 *
 * @returns {Promise}
 */
ServerConnector.getReferenceGenomeOrganisms = function () {
  var self = this;
  return self.sendGetRequest(self.getReferenceGenomeOrganismsUrl()).then(function (content) {
    var array = JSON.parse(content);
    var result = [];
    for (var i = 0; i < array.length; i++) {
      result.push(new Annotation(array[i]));
    }
    return result;
  });
};

/**
 *
 * @param {Object} params
 * @param {string} params.genomeId
 * @returns {Promise}
 */
ServerConnector.removeReferenceGenome = function (params) {
  var self = this;
  var filterParams = {};
  return self.sendDeleteRequest(self.getReferenceGenomeUrl(params, filterParams));
};

ServerConnector.uploadFile = function (params) {
  var CHUNK_SIZE = 65535 * 8;
  var self = this;
  var data = new Uint8Array(params.content);
  var filterParams = {
    filename: params.filename,
    length: data.length
  };

  return self.sendPostRequest(self.getCreateFileUrl(), filterParams).then(function (response) {
    var uploadedLength = 0;
    var createPromise = function (resultFileJson) {
      var resultFile = JSON.parse(resultFileJson);
      if (uploadedLength >= data.length) {
        return Promise.resolve(resultFile);
      } else {
        var chunk = data.slice(uploadedLength, uploadedLength + CHUNK_SIZE);
        var url = self.getUploadFileUrl({id: resultFile.id});
        return self.sendRequest({method: "POST", url: url, body: chunk}).then(function (resultFileJson) {
          uploadedLength += chunk.length;
          return createPromise(resultFileJson);
        });
      }
    };
    return createPromise(response);
  });
};


/**
 *
 * @param {Object} [params]
 * @param {number} [params.start]
 * @param {number} [params.length]
 * @param {string} [params.sortColumn]
 * @param {string} [params.sortOrder]
 * @param {string} [params.search]
 * @param {string} [params.level]
 * @param {string} [params.projectId]
 * @return {Promise}
 */
ServerConnector.getProjectLogs = function (params) {
  var self = this;
  if (params === undefined) {
    params = {};
  }

  var queryParams = {};
  var filterParams = {
    start: params.start,
    length: params.length,
    sortColumn: params.sortColumn,
    sortOrder: params.sortOrder,
    search: params.search,
    level: params.level
  };
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendGetRequest(self.getProjectLogsUrl(queryParams, filterParams));
  }).then(function (content) {
    return JSON.parse(content);
  });
};
/**
 *
 * @param {Object} [params]
 * @param {number} [params.start]
 * @param {number} [params.length]
 * @param {string} [params.projectId]
 * @return {Promise}
 */
ServerConnector.getProjectJobs = function (params) {
  var self = this;
  if (params === undefined) {
    params = {};
  }

  var queryParams = {};
  var filterParams = {
    page: params.start,
    size: params.length
  };
  return self.getProjectId(params.projectId).then(function (result) {
    queryParams.projectId = result;
    return self.sendGetRequest(self.getProjectJobsUrl(queryParams, filterParams));
  }).then(function (content) {
    return JSON.parse(content);
  });
};


/**
 *
 * @param {Object} params
 * @param {string} params.id
 * @return {Promise<Mesh>}
 */
ServerConnector.getMesh = function (params) {
  var self = this;
  return self.sendGetRequest(self.getMeshUrl(params)).then(function (content) {
    return new Mesh(JSON.parse(content));
  });
};

/**
 *
 * @param {Object} params
 * @param {number} params.id
 * @return {Promise<Mesh>}
 */
ServerConnector.getTaxonomy = function (params) {
  var self = this;
  return self.sendGetRequest(self.getTaxonomyUrl(params)).then(function (content) {
    return new Taxonomy(JSON.parse(content));
  });
};

/**
 *
 * @param params
 * @return {Promise<SbmlFunction>}
 */
ServerConnector.getSbmlFunction = function (params) {
  var self = this;
  if (params === undefined) {
    params = {};
  }
  return self.getProjectId(params.projectId).then(function (result) {
    params.projectId = result;
    return self.sendGetRequest(self.getSbmlFunctionUrl(params));
  }).then(function (content) {
    return new SbmlFunction(JSON.parse(content));
  });
};

ServerConnector.getSbmlParameter = function (params) {
  var self = this;
  if (params === undefined) {
    params = {};
  }
  return self.getProjectId(params.projectId).then(function (result) {
    params.projectId = result;
    return self.sendGetRequest(self.getSbmlParameterUrl(params));
  }).then(function (content) {
    return new SbmlParameter(JSON.parse(content));
  });
};

/**
 *
 * @param {PluginData} pluginData
 * @returns {Promise}
 */
ServerConnector.registerPlugin = function (pluginData) {
  var self = this;
  var params = {
    hash: pluginData.getHash(),
    url: pluginData.getUrls()[0],
    name: pluginData.getName(),
    version: pluginData.getVersion(),
    isPublic: pluginData.isPublic()
  };
  return self.sendPostRequest(self.getPluginsUrl(), params);
};

ServerConnector.getPluginGlobalParam = function (params) {
  var self = this;
  if (params === undefined) {
    params = {};
  }
  return self.sendGetRequest(self.getPluginGlobalParamUrl(params));
};

ServerConnector.setPluginGlobalParam = function (params) {
  var self = this;
  if (params === undefined) {
    params = {};
  }
  var filterParams = {
    value: params.value
  };
  return self.sendPostRequest(self.getPluginGlobalParamUrl(params), filterParams);
};

ServerConnector.getPluginUserParam = function (params) {
  var self = this;
  if (params === undefined) {
    params = {};
  }
  return self.getLoggedUser().then(function (user) {
    params.login = user.getLogin();
    return self.sendGetRequest(self.getPluginUserParamUrl(params));
  });
};

ServerConnector.setPluginUserParam = function (params) {
  var self = this;
  if (params === undefined) {
    params = {};
  }
  var filterParams = {
    value: params.value
  };
  return self.getLoggedUser().then(function (user) {
    params.login = user.getLogin();
    return self.sendPostRequest(self.getPluginUserParamUrl(params), filterParams);
  });
};

/**
 *
 * @returns {Promise}
 */
ServerConnector.getSubmapConnections = function () {
  var self = this;
  var queryParams = {};
  return self.getProjectId().then(function (result) {
    queryParams.projectId = result;
    return self.sendGetRequest(self.getSubmapConnectionsUrl(queryParams));
  }).then(function (content) {
    var data = JSON.parse(content);
    var result = [];
    for (var i = 0; i < data.length; i++) {
      result.push({from: new IdentifiedElement(data[i].from), to: data[i].to.modelId});
    }
    return result;
  });
};

ServerConnector.submitErrorToMinervaNet = function (report, callback) {
  console.log(report);
  var self = this;
  return request(self.getSubmitErrorToMinervaNetUrl(), {
    method: 'POST',
    headers: {
      'Content-type': 'application/json'
    },
    body: JSON.stringify(report)
  }, callback);
};

/**
 *
 * @return {Promise<boolean>}
 */
ServerConnector.isDapiConnectionValid = function () {
  var self = this;
  return self.sendGetRequest(self.getConfigurationUrl() + "dapi/").then(function (content) {
    return JSON.parse(content).validConnection;
  });
};

/**
 *
 * @param {string} params.login
 * @param {string} params.password
 * @param {string} params.email
 * @return {PromiseLike|Promise}
 */
ServerConnector.registerDapiUser = function (params) {
  var self = this;
  return this.sendRequest({
    method: "POST",
    url: self.getConfigurationUrl() + "dapi:registerUser",
    json: params
  }).catch(function (error) {
    return self.processNetworkError(error);
  });
};

/**
 *
 * @return {Promise<Array>}
 */
ServerConnector.getDapiDatabases = function () {
  var self = this;
  var result;
  return self.sendGetRequest(self.getConfigurationUrl() + "dapi/database/").then(function (content) {
    var promises = [];
    result = JSON.parse(content);
    result.forEach(function (element) {
      promises.push(self.sendGetRequest(self.getConfigurationUrl() + "dapi/database/" + element.name + "/release/").then(function (content) {
        element.releases = JSON.parse(content);
      }))
    });
    return Promise.all(promises);
  }).then(function () {
    return result;
  });
};

/**
 *
 * @param {string} database
 * @return {Promise<Array>}
 */
ServerConnector.getDapiDatabaseReleases = function (database) {
  var self = this;
  return self.sendGetRequest(self.getConfigurationUrl() + "dapi/database/" + database + "/release/").then(function (content) {
    return JSON.parse(content);
  });
};

/**
 * @param {string} databaseName
 * @param {string} releaseName
 * @return {Promise<T>}
 */
ServerConnector.getDapiDatabaseRelease = function (databaseName, releaseName) {
  var self = this;
  var result = null;
  return self.sendGetRequest(self.getConfigurationUrl() + "dapi/database/" + databaseName + "/release/").then(function (content) {
    var releases = JSON.parse(content);

    releases.forEach(function (element) {
      if (element.name === releaseName) {
        result = element;
      }
    });
    return result;
  });
};

/**
 *
 * @param {string} databaseName
 * @param {string} releaseName
 * @return {Promise}
 */
ServerConnector.acceptDapiDatabaseRelease = function (databaseName, releaseName) {
  var self = this;
  var result;
  return self.sendPostRequest(self.getConfigurationUrl() + "dapi/database/" + databaseName + "/release/" + releaseName + ":acceptLicense", '');
};


/**
 *
 * @param {string} params.login
 * @return {Promise}
 */
ServerConnector.requestResetPassword = function (params) {
  return this.sendPostRequest(this.getUserUrl(params) + ":requestResetPassword", '');
};

/**
 *
 * @param {string} params.token
 * @param {string} params.password
 * @return {Promise}
 */
ServerConnector.resetPassword = function (params) {
  return this.sendPostRequest(this.getApiUrl({type: "users:resetPassword"}), {
    token: params.token,
    password: params.password
  });
};

/**
 *
 * @param {string} params.email
 * @param {string} params.password
 * @param {string} params.name
 * @param {string} params.surname
 * @return {Promise}
 */
ServerConnector.signUp = function (params) {
  return this.sendJsonPostRequest(this.getApiUrl({type: "users:registerUser"}), {
    email: params.email,
    name: params.name,
    surname: params.surname,
    password: params.password
  });
};

/**
 *
 * @returns {PromiseLike<IdentifiedElement[]>|Promise<IdentifiedElement[]>}
 */
ServerConnector.isConnectedToMinervaNet = function () {
  var self = this;
  return self.sendGetRequest(self.getIsRegisteredInMinervaNetUrl()).then(function (content) {
    var data = JSON.parse(content);
    return data.status === true;
  }).catch(self.processMinervaNetError);
};

/**
 *
 * @returns {PromiseLike<IdentifiedElement[]>|Promise<IdentifiedElement[]>}
 */
ServerConnector.registerInMinervaNet = function () {
  var self = this;
  return self.sendPostRequest(self.getRegisterMachineUrl(), {}).catch(self.processMinervaNetError);
};

/**
 *
 * @returns {PromiseLike<IdentifiedElement[]>|Promise<IdentifiedElement[]>}
 */
ServerConnector.unregisterInMinervaNet = function () {
  var self = this;
  return self.sendPostRequest(self.getUnregisterMachineUrl(), {}).catch(self.processMinervaNetError);
};

ServerConnector.registerProjectInMinervaNet = function (projectId) {
  var self = this;
  return self.sendPostRequest(self.getRegisterProjectUrl({projectId: projectId}), {}).catch(self.processMinervaNetError);
};

ServerConnector.unregisterProjectInMinervaNet = function (projectId) {
  var self = this;
  return self.sendPostRequest(self.getUnregisterProjectUrl({projectId: projectId}), {}).catch(self.processMinervaNetError);
};

ServerConnector.processMinervaNetError = function (error) {
  if (error instanceof NetworkError) {
    if (error.statusCode === HttpStatus.FAILED_DEPENDENCY) {
      return Promise.reject(new MinervaNetError("Minerva-net rejected your request. " +
        "Please try again later. If the issue persist contact with <a target='_blank' href='https://minerva.pages.uni.lu/'>minerva developers</a>."));
    } else {
      return Promise.reject(error);
    }
  } else {
    return Promise.reject(error);
  }
};

/**
 * {string} id
 * @return {(PromiseLike<Configuration> | Promise<Configuration>)}
 */
ServerConnector.getStacktraceById = function (id) {
  var self = this;
  return self.sendGetRequest(self.getStacktraceUrl() + id + "/").then(function (content) {
    return JSON.parse(content);
  });
};

ServerConnector.getLicenses = function () {
  var self = this;
  if (this._licenses === undefined) {
    return self.sendGetRequest(self.getLicensesUrl() + "?size=1000").then(function (content) {
      self._licenses = [];
      var data = JSON.parse(content);
      for (var i = 0; i < data.data.length; i++) {
        self._licenses.push(new License(data.data[i]));
      }
      return self._licenses;
    });
  } else {
    return Promise.resolve(self._licenses);
  }
};

module.exports = ServerConnector;
