"use strict";

var ObjectWithListeners = require('./ObjectWithListeners');

var logger = require('./logger');

/**
 * @typedef {Object} ConfigurationOptionParam
 * @property {string} type
 * @property {string} commonName
 * @property {string} group
 * @property {string} value
 * @property {string} valueType
 */

/**
 *
 * @param {ConfigurationOption|ConfigurationOptionParam} data
 * @constructor
 * @extends ObjectWithListeners
 */
function ConfigurationOption(data) {
  // call super constructor
  ObjectWithListeners.call(this);

  var self = this;
  if (data instanceof ConfigurationOption) {
    self.setType(data.getType());
    self.setCommonName(data.getCommonName());
    self.setValue(data.getValue());
    self.setGroup(data.getGroup());
    self.setValueType(data.getValueType());
  } else {
    self.setType(data.type);
    self.setCommonName(data.commonName);
    self.setGroup(data.group);
    self.setValue(data.value);
    self.setValueType(data.valueType);
  }
}

ConfigurationOption.prototype = Object.create(ObjectWithListeners.prototype);
ConfigurationOption.prototype.constructor = ConfigurationOption;

/**
 *
 * @param {string} type
 */
ConfigurationOption.prototype.setType = function (type) {
  this._type = type;
};

/**
 *
 * @returns {string}
 */
ConfigurationOption.prototype.getType = function () {
  return this._type;
};

/**
 *
 * @param {string} value
 */
ConfigurationOption.prototype.setValue = function (value) {
  this._value = value;
};

/**
 *
 * @returns {string|null}
 */
ConfigurationOption.prototype.getValue = function () {
  return this._value;
};

/**
 *
 * @param {string} valueType
 */
ConfigurationOption.prototype.setValueType = function (valueType) {
  this._valueType = valueType;
};

/**
 *
 * @returns {string}
 */
ConfigurationOption.prototype.getValueType = function () {
  return this._valueType;
};

/**
 *
 * @param {string} commonName
 */
ConfigurationOption.prototype.setCommonName = function (commonName) {
  this._commonName = commonName;
};

/**
 *
 * @returns {string}
 */
ConfigurationOption.prototype.getCommonName = function () {
  return this._commonName;
};

/**
 *
 * @param {string} group
 */
ConfigurationOption.prototype.setGroup = function (group) {
  this._group = group;
};

/**
 *
 * @returns {string}
 */
ConfigurationOption.prototype.getGroup = function () {
  return this._group;
};


module.exports = ConfigurationOption;
