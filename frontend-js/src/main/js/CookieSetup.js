require("vanilla-cookieconsent")
var ServerConnector = require("./ServerConnector")
var ConfigurationType = require("./ConfigurationType");
var Promise = require("bluebird");

var cc;
var cookiePolicyUrl

window.addEventListener('load', function () {
  return new Promise(function (resolve) {
    setTimeout(resolve, 5000) //wait for basic resources to load (otherwise we might load data before session/credentials are defined)
  }).then(function () {
    return ServerConnector.getConfiguration();
  }).then(function (configuration) {

    var option = configuration.getOption(ConfigurationType.COOKIE_POLICY_URL);
    if (option !== undefined && option != null) {
      cookiePolicyUrl = option.getValue();
    }
    var cookieConfig = {
      current_lang: 'en',
      autoclear_cookies: true,                   // default: false
      page_scripts: true,                        // default: false

      // mode: 'opt-in'                          // default: 'opt-in'; value: 'opt-in' or 'opt-out'
      // delay: 0,                               // default: 0
      // auto_language: '',                      // default: null; could also be 'browser' or 'document'
      autorun: false,                          // default: true
      // force_consent: true,                   // default: false
      // hide_from_bots: false,                  // default: false
      // remove_cookie_tables: false             // default: false
      // cookie_name: 'cc_cookie',               // default: 'cc_cookie'
      // cookie_expiration: 182,                 // default: 182 (days)
      // cookie_necessary_only_expiration: 182   // default: disabled
      // cookie_domain: location.hostname,       // default: current domain
      // cookie_path: '/',                       // default: root
      // cookie_same_site: 'Lax',                // default: 'Lax'
      // use_rfc_cookie: false,                  // default: false
      // revision: 0,                            // default: 0

      onFirstAction: function (user_preferences, cookie) {
        // callback triggered only once on the first accept/reject action
      },

      onAccept: function (cookie) {
        // callback triggered on the first accept/reject action, and after each page load
      },

      onChange: function (cookie, changed_categories) {
        // callback triggered when user changes preferences after consent has already been given
      },

      languages: {
        'en': {
          consent_modal: {
            title: 'We use cookies!',
            description: 'Minerva platform uses essential cookies to ensure its proper operation.',
            primary_btn: {
              text: 'OK',
              role: 'accept_all'              // 'accept_selected' or 'accept_all'
            }
          },
          settings_modal: {
            title: 'Cookie preferences',
            save_settings_btn: 'Save settings',
            accept_all_btn: 'OK',
            close_btn_label: 'Close',
            blocks: [
              {
                title: 'Cookie usage 📢',
                description: 'I use cookies to ensure the basic functionalities of the website and to enhance your online experience. For more details relative to cookies and other sensitive data, please read the full <a href="' + cookiePolicyUrl + '" class="cc-link" target="minerva_cookie">privacy policy</a>.'
              }, {
                title: 'Strictly necessary cookies',
                description: 'These cookies are essential for the proper functioning of the website. Without these cookies, the website would not work properly',
                toggle: {
                  value: 'necessary',
                  enabled: true,
                  readonly: true          // cookie categories with readonly=true are all treated as "necessary cookies"
                }
              }
            ]
          }
        }
      }
    };
    cookieConfig.languages.en.consent_modal.description = "Minerva platform uses essential cookies to ensure its " +
      "proper operation. For any queries in relation to our policy on cookies and your choices, please " +
      "<a class='cc-link' href='" + cookiePolicyUrl + "' target='minerva_cookie'>read here</a>."
    cookieConfig.languages.en.settings_modal.blocks = [{
      title: 'More information',
      description: 'For any queries in relation to our policy on cookies and your choices, please <a class="cc-link" href="' + cookiePolicyUrl + '" target="minerva_cookie">read here</a>.'
    }];
    cookieConfig.languages.en.consent_modal.primary_btn = {
      text: 'OK',
      role: 'accept_all'
    };
    cookieConfig.languages.en.consent_modal.secondary_btn = undefined;

    // obtain plugin
    cc = initCookieConsent();

    // run plugin with your configuration
    cc.run(cookieConfig);
  });
});

module.exports = {
  showCookieConsent: function () {
    if (cookiePolicyUrl !== undefined && cookiePolicyUrl !== null && cookiePolicyUrl !== "") {
      cc.show();
    } else {
      console.log("Cookie policy url is not defined");
    }
  }
};
