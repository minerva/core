"use strict";

var ContextMenu = require('./ContextMenu');
var PluginDialog = require('./PluginDialog');
var Promise = require('bluebird');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} params.customMap
 * @param {Configuration} params.configuration
 * @param {Project} [params.project]
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 *
 * @extends ContextMenu
 */
function OptionsMenu(params) {
  ContextMenu.call(this, params);
  var self = this;

  self._createMenuGui();
}

OptionsMenu.prototype = Object.create(ContextMenu.prototype);
OptionsMenu.prototype.constructor = OptionsMenu;


/**
 *
 * @private
 */
OptionsMenu.prototype._createMenuGui = function () {
  var self = this;
  self.addOption({
    name: "Plugins",
    handler: function () {
      var initPromise = Promise.resolve();
      if (self._pluginDialog === undefined) {
        self._pluginDialog = new PluginDialog({
          element: document.createElement("div"),
          customMap: self.getMap(),
          pluginManager: self.getPluginManager(),
          configuration: self.getConfiguration(),
          project: self.getProject()
        });
        initPromise = self._pluginDialog.init();
      }
      return initPromise.then(function () {
        return self._pluginDialog.open();
      })
    }
  });
  self.addOption({
    name: "API docs",
    handler: function () {
      window.open(self.getServerConnector().getServerBaseUrl() + "docs/", '_api_docs');
    }
  });
  self.addOption({
    name: "NEW interface",
    handler: function () {
      self.getServerConnector().getProjectId().then(function (projectId) {
        window.open(self.getServerConnector().getServerBaseUrl() + "?id=" + projectId, '_new_interface');
      })
    }
  });
};

/**
 *
 * @returns {Promise}
 */
OptionsMenu.prototype.init = function () {
  return Promise.resolve();
};

/**
 *
 * @returns {Promise}
 */
OptionsMenu.prototype.destroy = function () {
  var self = this;
  var promises = [];
  if (self._pluginDialog !== undefined) {
    promises.push(self._pluginDialog.destroy());
  }
  return Promise.all(promises);
};

/**
 *
 * @param {PluginManager} pluginManager
 */
OptionsMenu.prototype.setPluginManager = function (pluginManager) {
  this._pluginManager = pluginManager;
};

/**
 *
 * @returns {PluginManager}
 */
OptionsMenu.prototype.getPluginManager = function () {
  return this._pluginManager;
};

module.exports = OptionsMenu;
