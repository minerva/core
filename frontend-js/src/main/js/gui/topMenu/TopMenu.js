"use strict";

var Promise = require("bluebird");
var $ = require('jquery');

/* exported logger */

var AbstractGuiElement = require('../AbstractGuiElement');
var ConfigurationType = require('../../ConfigurationType');
var GuiConnector = require('../../GuiConnector');
var OverviewDialog = require('../OverviewDialog');
var PanelControlElementType = require('../PanelControlElementType');

var Functions = require('../../Functions');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} params.customMap
 * @param {Configuration} params.configuration
 * @param {Project} [params.project]
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 *
 * @extends AbstractGuiElement
 */
function TopMenu(params) {
  AbstractGuiElement.call(this, params);
  var self = this;

  self.registerListenerType("onShowCommentsToggle");
  self._createGui();

  self._cronFunction = function () {
    return self.checkIfSessionIsGoingToExpire();
  };

  setInterval(self._cronFunction, 1000);
}

TopMenu.prototype = Object.create(AbstractGuiElement.prototype);
TopMenu.prototype.constructor = TopMenu;

/**
 *
 * @private
 */
TopMenu.prototype._createGui = function () {
  var self = this;

  var overviewDialogDiv = Functions.createElement({type: "div"});
  self.getElement().appendChild(overviewDialogDiv);
  self.setControlElement(PanelControlElementType.OVERVIEW_DIALOG_DIV, overviewDialogDiv);

  var hideButtonDiv = Functions.createElement({
    type: "div",
    className: "minerva-header-hide-div-button"
  });
  self.getElement().appendChild(hideButtonDiv);

  var hideButton = Functions.createElement({
    type: "button",
    className: "minerva-header-hide-button"
  });
  hideButtonDiv.appendChild(hideButton);
  self.setControlElement(PanelControlElementType.MENU_HIDE_LEFT_PANEL_BUTTON, hideButton);

  var hideButtonIcon = Functions.createElement({
    type: "i",
    className: "fa fa-chevron-left"
  });
  hideButton.appendChild(hideButtonIcon);
  self.setControlElement(PanelControlElementType.MENU_HIDE_LEFT_PANEL_BUTTON_ICON, hideButtonIcon);

  var versionDiv = Functions.createElement({
    type: "div",
    className: "minerva-version-div"
  });
  self.getElement().appendChild(versionDiv);
  self.setControlElement(PanelControlElementType.MENU_VERSION_DIV, versionDiv);

  self.getElement().appendChild(Functions.createElement({
    type: "div",
    className: "minerva-session-expire",
    style: "display:none;"
  }));

  var showOverviewDiv = Functions.createElement({type: "div", className:"minerva-overview-div"});
  self.getElement().appendChild(showOverviewDiv);

  var showOverviewButton = Functions.createElement({
    type: "button",
    className: "minerva-overview-button",
    content: "<i class='fa fa-sitemap' style='font-size:18px; padding-right:10px;'></i><span >SHOW OVERVIEW</span>",
    style: "display:none",
    xss: false
  });
  showOverviewDiv.appendChild(showOverviewButton);
  self.setControlElement(PanelControlElementType.MENU_SHOW_OVERVIEW_BUTTON, showOverviewButton);

  var clearButton = Functions.createElement({
    type: "button",
    name: "clearButton",
    className: "minerva-overview-button",
    style: "float:right",
    content: "<i class='fa fa-times' style='padding-right:10px;'></i>CLEAR",
    xss: false
  });
  self.getElement().appendChild(clearButton);

  var div4checkboxes = Functions.createElement({
    type: "div",
    style: "float:right",
    className: "minerva-top-checkbox-div"
  });
  self.getElement().appendChild(div4checkboxes);

  var legendCheckbox = Functions.createElement({
    type: "input",
    inputType: "checkbox"
  });
  div4checkboxes.appendChild(legendCheckbox);
  self.setControlElement(PanelControlElementType.MENU_LEGEND_CHECKBOX, legendCheckbox);

  div4checkboxes.appendChild(Functions.createElement({
    type: "label",
    content: "LEGEND"
  }));

  var commentCheckbox = Functions.createElement({
    type: "input",
    inputType: "checkbox",
    name: "commentCheckbox"
  });
  div4checkboxes.appendChild(commentCheckbox);
  self.setControlElement(PanelControlElementType.MENU_COMMENTS_CHECKBOX, commentCheckbox);

  div4checkboxes.appendChild(Functions.createElement({
    type: "label",
    content: "COMMENTS"
  }));

  var refreshCommentButton = Functions.createElement({
    type: "button",
    className: "minerva-overview-button",
    content: "<i class='fa fa-refresh'></i>",
    style: "display:none",
    xss: false
  });
  div4checkboxes.appendChild(refreshCommentButton);
  self.setControlElement(PanelControlElementType.MENU_REFRESH_COMMENTS_BUTTON, refreshCommentButton);

  self.setControlElement(PanelControlElementType.MENU_CLEAR_BUTTON, clearButton);

};

/**
 *
 * @returns {Promise}
 */
TopMenu.prototype.init = function () {
  var self = this;
  self.getControlElement(PanelControlElementType.MENU_LEGEND_CHECKBOX).onclick = function () {
    return self.toggleLegend();
  };
  var hideButton = self.getControlElement(PanelControlElementType.MENU_HIDE_LEFT_PANEL_BUTTON);
  var icon = self.getControlElement(PanelControlElementType.MENU_HIDE_LEFT_PANEL_BUTTON_ICON);
  hideButton.onclick = function () {
    if (icon.className.indexOf("fa-chevron-left") >= 0) {
      icon.className = "fa fa-chevron-right";
      self.getLeftPanel().hide();
    } else {
      icon.className = "fa fa-chevron-left";
      self.getLeftPanel().show();
      $(".minerva-panel").each(onresize);
    }
    self.getMap().getMapCanvas().triggerListeners('resize');
  };

  var project = self.getMap().getProject();
  self.getControlElement(PanelControlElementType.MENU_VERSION_DIV).innerHTML = project.getVersion();

  var commentCheckbox = self.getControlElement(PanelControlElementType.MENU_COMMENTS_CHECKBOX);
  var refreshCommentButton = self.getControlElement(PanelControlElementType.MENU_REFRESH_COMMENTS_BUTTON);
  commentCheckbox.onclick = function () {
    self.getServerConnector().getSessionData(project).setShowComments(commentCheckbox.checked);
    if (commentCheckbox.checked) {
      $(refreshCommentButton).css("display", "inline");
    } else {
      $(refreshCommentButton).css("display", "none");
    }
    return self.getMap().refreshComments().then(function () {
      return self.callListeners("onShowCommentsToggle", commentCheckbox.checked);
    }).catch(GuiConnector.alert);
  };
  refreshCommentButton.onclick = (function () {
    return function () {
      return self.getMap().refreshComments();
    };
  })();

  var clearButton = self.getControlElement(PanelControlElementType.MENU_CLEAR_BUTTON);
  clearButton.onclick = (function () {
    return function () {
      $(commentCheckbox).prop('checked', false);
      return Promise.all([self.getMap().clearDbOverlays(), commentCheckbox.onclick()]);
    };
  })();

  if (project.getTopOverviewImage() !== undefined) {
    self._overviewDialog = new OverviewDialog({
      customMap: self.getMap(),
      configuration: self.getConfiguration(),
      project: self.getProject(),
      element: self.getControlElement(PanelControlElementType.OVERVIEW_DIALOG_DIV)
    });
    var showOverviewButton = self.getControlElement(PanelControlElementType.MENU_SHOW_OVERVIEW_BUTTON);
    showOverviewButton.onclick = function () {
      return self._overviewDialog.showOverview();
    };
    $(showOverviewButton).css("display", "inline-block");
    self.getMap().setOverviewDialog(self._overviewDialog);
  }

  if (self.getServerConnector().getSessionData().getShowComments()) {
    self.getControlElement(PanelControlElementType.MENU_COMMENTS_CHECKBOX).checked = true;
    return self.getMap().refreshComments();
  } else {
    return Promise.resolve();
  }
};

/**
 *
 * @param {Legend} legend
 */
TopMenu.prototype.setLegend = function (legend) {
  this._legend = legend;
};

/**
 *
 * @returns {Legend}
 */
TopMenu.prototype.getLegend = function () {
  return this._legend;
};

/**
 *
 * @param {LeftPanel} leftPanel
 */
TopMenu.prototype.setLeftPanel = function (leftPanel) {
  this._leftPanel = leftPanel;
};

/**
 *
 * @returns {LeftPanel}
 */
TopMenu.prototype.getLeftPanel = function () {
  return this._leftPanel;
};

/**
 *
 * @returns {Promise}
 */
TopMenu.prototype.destroy = function () {
  var self = this;
  if (self._overviewDialog !== undefined) {
    self._overviewDialog.destroy();
  }
  clearInterval(self._cronFunction);
  return Promise.resolve();
};

/**
 *
 */
TopMenu.prototype.toggleLegend = function () {
  var self = this;
  var legend = self.getLegend();
  if (self.getControlElement(PanelControlElementType.MENU_LEGEND_CHECKBOX).checked) {
    legend.show();
  } else {
    legend.hide();
  }
};

/**
 *
 * @returns {Promise}
 */
TopMenu.prototype.checkIfSessionIsGoingToExpire = function () {
  var self = this;
  var lastAccessTimestamp = self.getServerConnector().getSessionData().getLastRequestTimeStamp();
  var sessionInactivityOption = self.getConfiguration().getOption(ConfigurationType.SESSION_LENGTH);
  var sessionInactivityLength = parseInt(sessionInactivityOption.getValue());
  var sessionExpireTimestamp = lastAccessTimestamp + sessionInactivityLength;
  var currentTimestamp = Math.floor(Date.now() / 1000);

  //if session is going to expire in 10 minutes show information
  if (sessionExpireTimestamp - currentTimestamp < 10 * 60) {
    self.showSessionExpire(sessionExpireTimestamp - currentTimestamp);
  } else {
    self.hideSessionExpire();
  }

  //if session expired
  if (sessionExpireTimestamp < currentTimestamp) {
    return self.getServerConnector().logout();
  }
  return Promise.resolve();
};

/**
 *
 * @param {number} timeInSeconds
 */
TopMenu.prototype.showSessionExpire = function (timeInSeconds) {
  var self = this;
  var minutes = Math.floor(timeInSeconds / 60);
  var seconds = Math.floor(timeInSeconds - minutes * 60);
  var message = "Session expires in " + minutes + " minutes " + seconds + " seconds";
  var button = Functions.createElement({
    type: "button",
    content: "EXTEND",
    className: "minerva-extend-session-button",
    onclick: function () {
      return self.getServerConnector().extendSession();
    }
  });
  $(".minerva-session-expire", self.getElement()).empty().append("<span>" + message + "</span>").append(button).show();
};

/**
 *
 */
TopMenu.prototype.hideSessionExpire = function () {
  $(".minerva-session-expire", this.getElement()).empty().hide();
};

module.exports = TopMenu;