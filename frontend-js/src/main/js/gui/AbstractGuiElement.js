"use strict";

/* exported logger */
var $ = require('jquery');

var ObjectWithListeners = require('../ObjectWithListeners');
var PanelControlElementType = require('./PanelControlElementType');

// noinspection JSUnusedLocalSymbols
var logger = require('../logger');
var Functions = require('../Functions');

var Promise = require('bluebird');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} [params.customMap]
 * @param {Configuration} params.configuration
 * @param {Project} [params.project]
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 *
 * @extends ObjectWithListeners
 */
function AbstractGuiElement(params) {
  ObjectWithListeners.call(this, params);

  var self = this;

  self.setElement(params.element);
  self.setConfiguration(params.configuration);
  self.setProject(params.project);
  self.setMap(params.customMap);
  self.setServerConnector(params.serverConnector);

  self._controlElements = [];

}

AbstractGuiElement.prototype = Object.create(ObjectWithListeners.prototype);
AbstractGuiElement.prototype.constructor = AbstractGuiElement;

/**
 *
 * @param {CustomMap} map
 */
AbstractGuiElement.prototype.setMap = function (map) {
  this._map = map;
};

/**
 *
 * @returns {CustomMap}
 */
AbstractGuiElement.prototype.getMap = function () {
  return this._map;
};

/**
 *
 * @param {HTMLElement} element
 */
AbstractGuiElement.prototype.setElement = function (element) {
  if (element === undefined || element === null) {
    throw new Error("DOM Element must be defined");
  }
  this._element = element;
};

/**
 *
 * @returns {HTMLElement}
 */
AbstractGuiElement.prototype.getElement = function () {
  return this._element;
};

/**
 *
 * @param {Project} project
 */
AbstractGuiElement.prototype.setProject = function (project) {
  this._project = project;
};

/**
 *
 * @returns {Project}
 */
AbstractGuiElement.prototype.getProject = function () {
  if (this._project === undefined || this._project === null) {
    if (this.getMap()) {
      return this.getMap().getProject();
    } else {
      return null;
    }
  } else {
    return this._project;
  }
};


/**
 *
 * @param {string} type
 * @param {HTMLElement} element
 */
AbstractGuiElement.prototype.setControlElement = function (type, element) {
  if (type === null || type === undefined) {
    throw new Error("Unknown control element type");
  }
  if (PanelControlElementType[type] === undefined) {
    throw new Error("Unknown control element type: " + type);
  }

  this._controlElements[type] = element;
};

/**
 *
 * @param {string} type
 * @returns {HTMLElement}
 */
AbstractGuiElement.prototype.getControlElement = function (type) {
  if (type === null || type === undefined) {
    throw new Error("Unknown control element type");
  }
  if (PanelControlElementType[type] === undefined) {
    throw new Error("Unknown control element type: " + type);
  }

  return this._controlElements[type];
};

/**
 *
 * @param {string} url
 */
AbstractGuiElement.prototype.downloadFile = function (url) {
  this._downloadFile = url;
  window.open(url, '_blank');
};

/**
 *
 * @returns {string}
 */
AbstractGuiElement.prototype.getLastDownloadUrl = function () {
  return this._downloadFile;
};

/**
 *
 * @param {Configuration} configuration
 */
AbstractGuiElement.prototype.setConfiguration = function (configuration) {
  this._configuration = configuration;
};

/**
 *
 * @returns {Configuration}
 */
AbstractGuiElement.prototype.getConfiguration = function () {
  return this._configuration;
};

/**
 *
 * @param {ServerConnector} serverConnector
 */
AbstractGuiElement.prototype.setServerConnector = function (serverConnector) {
  this._serverConnector = serverConnector;
};

/**
 *
 * @returns {ServerConnector}
 */
AbstractGuiElement.prototype.getServerConnector = function () {
  if (this._serverConnector !== undefined) {
    return this._serverConnector;
  } else {
    return this.getMap().getServerConnector();
  }
};

/**
 *
 * @param {string} [params.content]
 * @param {string} params.title
 * @param {boolean} [params.input]
 * @returns {Promise}
 */
AbstractGuiElement.prototype.askConfirmRemoval = function (params) {
  return new Promise(function (resolve) {
    var html;
    var content = '';
    if (params.content) {
      content = params.content;
    }
    if (params.input) {
      html = '<form><input type="text" style="z-index:10000" name="reason-value"><br></form>';
    } else {
      html = '<form><span>' + content + '</span><input type="text" style="z-index:10000;visibility: hidden" name="reason-value"><br></form>';
    }
    $(html).dialog({
      modal: true,
      title: params.title,
      close: function () {
        $(this).dialog('destroy').remove();
        resolve({status: false});
      },
      buttons: {
        'OK': function () {
          var reason = $('input[name="reason-value"]').val();
          $(this).dialog('destroy').remove();
          resolve({reason: reason, status: true});
        },
        'Cancel': function () {
          $(this).dialog('destroy').remove();
          resolve({status: false});
        }
      }
    });
  })
};

module.exports = AbstractGuiElement;
