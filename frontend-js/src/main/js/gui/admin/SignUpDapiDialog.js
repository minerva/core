"use strict";

var Promise = require("bluebird");
var $ = require('jquery');

var AbstractGuiElement = require('../AbstractGuiElement');
var GuiConnector = require('../../GuiConnector');

var ObjectExistsError = require('../../ObjectExistsError');
var ValidationError = require('../../ValidationError');

var Functions = require('../../Functions');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
var request = require('request');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {Configuration} params.configuration
 * @param {ServerConnector} params.serverConnector

 * @constructor
 *
 * @extends AbstractGuiElement
 */
function SignUpDapiDialog(params) {
  params["customMap"] = null;
  AbstractGuiElement.call(this, params);
  var self = this;

  $(self.getElement()).css({overflow: "hidden"});

  self.createGui();
  self.registerListenerType("onSave");
}

SignUpDapiDialog.prototype = Object.create(AbstractGuiElement.prototype);
SignUpDapiDialog.prototype.constructor = SignUpDapiDialog;

/**
 *
 */
SignUpDapiDialog.prototype.createGui = function () {
  var self = this;

  var result = Functions.createElement({
    type: "div",
    className: "minerva-dapi-sign-form-content"
  });

  var table = Functions.createElement({
    type: "table",
    className: "display"
  });
  result.appendChild(table);


  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row"
  });
  result.appendChild(menuRow);

  var saveUserButton = Functions.createElement({
    type: "button",
    class: "minerva-create-dapi-account",
    content: '<span class="ui-icon ui-icon-disk"></span>&nbsp;SAVE',
    onclick: function () {
      return self.onSaveClicked().then(function () {
        return self.close();
      }).catch(GuiConnector.alert);
    },
    xss: false
  });
  var cancelButton = Functions.createElement({
    type: "button",
    content: '<span class="ui-icon ui-icon-cancel"></span>&nbsp;CANCEL',
    onclick: function () {
      return self.close();
    },
    xss: false
  });
  menuRow.appendChild(saveUserButton);
  menuRow.appendChild(cancelButton);

  return self.getElement().appendChild(result);
};

/**
 *
 * @returns {Promise}
 */
SignUpDapiDialog.prototype.onSaveClicked = function () {
  var self = this;
  var login = $("[name='dapi-login']", self.getElement()).val();
  var password = $("[name='dapi-password']", self.getElement()).val();
  var password2 = $("[name='dapi-password-2']", self.getElement()).val();

  if (password!==password2) {
    return Promise.reject(new ValidationError("Password does not match"));
  }
  return self.getServerConnector().registerDapiUser({
    login: login,
    password: password,
    email: $("[name='dapi-email']", self.getElement()).val()
  }).then(function () {
    self.close();
    return self.callListeners("onSave", {login: login, password: password});
  }).catch(function(error){
    if (error instanceof ObjectExistsError) {
      GuiConnector.alert("User login/email is already used. Please choose different account name.")
    } else {
      GuiConnector.alert(error);
    }

  });
};

/**
 *
 * @returns {Promise}
 */
SignUpDapiDialog.prototype.init = function () {
  var self = this;
  var detailsTable = $(".minerva-dapi-sign-form-content  table", self.getElement())[0];

  // noinspection JSCheckFunctionSignatures
  var dataTable = $(detailsTable).DataTable({
    columns: [{
      title: "Name"
    }, {
      title: "Value"
    }],
    paging: false,
    ordering: false,
    searching: false,
    bInfo: false
  });

  var data = [];

  data.push(['Login', "<input name='dapi-login'/>"]);
  data.push(['Password', "<input type='password' name='dapi-password' />"]);
  data.push(['Repeat password', "<input type='password' name='dapi-password-2' />"]);
  data.push(['Email address', "<input name='dapi-email' />"]);

  dataTable.clear().rows.add(data).draw();

  return Promise.resolve();
};

/**
 *
 *
 * @returns {Promise}
 */
SignUpDapiDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();

  var detailsTable = $("[name=detailsTable]", div)[0];
  if ($.fn.DataTable.isDataTable(detailsTable)) {
    $(detailsTable).DataTable().destroy();
  }
  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
  return Promise.resolve();
};

/**
 *
 */
SignUpDapiDialog.prototype.open = function () {
  var self = this;
  var div = self.getElement();
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      dialogClass: 'minerva-sign-up-dapi-dialog',
      title: "Create DAPI account",
      width: 420,
      height: 360
    });
  }
  self.clear();
  $(div).dialog("open");
};

SignUpDapiDialog.prototype.clear = function () {
  var self = this;
  $("[name='dapi-login']", self.getElement()).val("");
  $("[name='dapi-password']", self.getElement()).val("");
  $("[name='dapi-password-2']", self.getElement()).val("");
  $("[name='dapi-email']", self.getElement()).val("");
};

/**
 *
 */
SignUpDapiDialog.prototype.close = function () {
  var self = this;
  $(self.getElement()).dialog("close");
};

module.exports = SignUpDapiDialog;
