"use strict";

var Promise = require("bluebird");
var $ = require('jquery');

var AbstractGuiElement = require('../AbstractGuiElement');
var GuiConnector = require('../../GuiConnector');

var ConfigurationType = require('../../ConfigurationType');

var Functions = require('../../Functions');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {Configuration} params.configuration
 * @param {ServerConnector} params.serverConnector

 * @constructor
 *
 * @extends AbstractGuiElement
 */
function SignInDapiDialog(params) {
  params["customMap"] = null;
  AbstractGuiElement.call(this, params);
  var self = this;

  $(self.getElement()).css({overflow: "hidden"});

  self.createGui();
  self.registerListenerType("onSave");
}

SignInDapiDialog.prototype = Object.create(AbstractGuiElement.prototype);
SignInDapiDialog.prototype.constructor = SignInDapiDialog;

/**
 *
 */
SignInDapiDialog.prototype.createGui = function () {
  var self = this;

  var result = Functions.createElement({
    type: "div",
    className: "minerva-dapi-sign-form-content"
  });

  var table = Functions.createElement({
    type: "table",
    className: "display"
  });
  result.appendChild(table);


  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row"
  });
  result.appendChild(menuRow);

  var saveUserButton = Functions.createElement({
    type: "button",
    class: "minerva-create-dapi-account",
    content: '<span class="ui-icon ui-icon-disk"></span>&nbsp;LOG IN',
    onclick: function () {
      return self.onSaveClicked().then(function () {
        return self.close();
      }).catch(GuiConnector.alert);
    },
    xss: false
  });
  var cancelButton = Functions.createElement({
    type: "button",
    content: '<span class="ui-icon ui-icon-cancel"></span>&nbsp;CANCEL',
    onclick: function () {
      return self.close();
    },
    xss: false
  });
  menuRow.appendChild(saveUserButton);
  menuRow.appendChild(cancelButton);

  return self.getElement().appendChild(result);
};

/**
 *
 * @returns {Promise}
 */
SignInDapiDialog.prototype.onSaveClicked = function () {
  var self = this;
  var login = $("[name='dapi-login']", self.getElement()).val();
  var password = $("[name='dapi-password']", self.getElement()).val();

  var loginOption = self.getConfiguration().getOption(ConfigurationType.DAPI_LOGIN);
  var passwordOption = self.getConfiguration().getOption(ConfigurationType.DAPI_PASSWORD);
  loginOption.setValue(login);
  passwordOption.setValue(password);
  GuiConnector.showProcessing();
  return Promise.all([self.getServerConnector().updateConfigurationOption(loginOption),
    self.getServerConnector().updateConfigurationOption(passwordOption)]
  ).then(function () {
    return self.getServerConnector().isDapiConnectionValid();
  }).then(function (isValid) {
    if (isValid) {
      GuiConnector.info("Login successful");
    } else {
      GuiConnector.warn("Invalid credentials");
    }
    self.close();
    return self.callListeners("onSave", {login: login, password: password});
  }).finally(GuiConnector.hideProcessing);
};

/**
 *
 * @returns {Promise}
 */
SignInDapiDialog.prototype.init = function () {
  var self = this;
  var detailsTable = $(".minerva-dapi-sign-form-content  table", self.getElement())[0];

  // noinspection JSCheckFunctionSignatures
  var dataTable = $(detailsTable).DataTable({
    columns: [{
      title: "Name"
    }, {
      title: "Value"
    }],
    paging: false,
    ordering: false,
    searching: false,
    bInfo: false
  });

  var data = [];

  data.push(['Login', "<input name='dapi-login'/>"]);
  data.push(['Password', "<input type='password' name='dapi-password' />"]);

  dataTable.clear().rows.add(data).draw();

  $(self.getElement()).on('keydown', "[name='dapi-login']", function (e) {
    if (e.which === 13) {
      $("[name='dapi-password']", self.getElement()).focus();
    }
  });
  $(self.getElement()).on('keydown', "[name='dapi-password']", function (e) {
    if (e.which === 13) {
      return self.onSaveClicked().then(function () {
        return self.close();
      }).catch(GuiConnector.alert);
    }
  });


  return Promise.resolve();
};

/**
 *
 *
 * @returns {Promise}
 */
SignInDapiDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();

  var detailsTable = $("[name=detailsTable]", div)[0];
  if ($.fn.DataTable.isDataTable(detailsTable)) {
    $(detailsTable).DataTable().destroy();
  }
  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
  return Promise.resolve();
};

/**
 *
 */
SignInDapiDialog.prototype.open = function () {
  var self = this;
  var div = self.getElement();
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      dialogClass: 'minerva-sign-up-dapi-dialog',
      title: "Log in to DAPI",
      width: 420,
      height: 240
    });
  }
  self.clear();
  $(div).dialog("open");
};

SignInDapiDialog.prototype.clear = function () {
  var self = this;
  $("[name='dapi-login']", self.getElement()).val("");
  $("[name='dapi-password']", self.getElement()).val("");
};

/**
 *
 */
SignInDapiDialog.prototype.close = function () {
  var self = this;
  $(self.getElement()).dialog("close");
};

module.exports = SignInDapiDialog;
