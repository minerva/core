"use strict";

var Promise = require("bluebird");
var $ = require('jquery');

/* exported logger */

var AbstractGuiElement = require('../AbstractGuiElement');

var Functions = require('../../Functions');
var GuiConnector = require('../../GuiConnector');


/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} [params.customMap]
 * @param {Configuration} params.configuration
 * @param {Project} [params.project]
 * @param {string} params.projectId
 * @param {ServerConnector} params.serverConnector
 *
 * @constructor
 * @extends AbstractGuiElement
 */
function JobListDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self.createJobListDialogGui();

  /**
   * @type {string}
   * @private
   */
  self._projectId = params.projectId;
}

JobListDialog.prototype = Object.create(AbstractGuiElement.prototype);
JobListDialog.prototype.constructor = JobListDialog;

/**
 *
 */
JobListDialog.prototype.createJobListDialogGui = function () {
  var self = this;
  var head = Functions.createElement({
    type: "thead",
    content: "<tr>" +
      "<th>ID</th>" +
      "<th>Type</th>" +
      "<th>Status</th>" +
      "<th>Progress</th>" +
      "<th>Created at</th>" +
      "<th>Finished at</th>" +
      "<th>Priority</th>" +
      "</tr>"
  });
  var body = Functions.createElement({
    type: "tbody"
  });
  var tableElement = Functions.createElement({
    type: "table",
    className: "minerva-logs-table",
    style: "width: 100%"
  });

  tableElement.appendChild(head);
  tableElement.appendChild(body);

  self.tableElement = tableElement;
  self.getElement().appendChild(tableElement);

  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });
  self.getElement().appendChild(menuRow);

};

function removeNull(object) {
  if (object === null || object === undefined) {
    return "";
  }
  return object;
}

function entryToRow(entry) {
  var row = [];
  row[0] = removeNull(entry.id);
  row[1] = removeNull(entry.jobType);
  row[2] = removeNull(entry.jobStatus);
  row[3] = removeNull(entry.progress);
  row[4] = removeNull(entry.jobStarted);
  row[5] = removeNull(entry.jobFinished);
  row[6] = removeNull(entry.priority);
  return row;
}

/**
 *
 * @param {Object} data
 * @param {number} data.start
 * @param {number} data.length
 * @param {Object} data.search
 * @param {Object} data.draw
 * @param {Array} data.order
 * @param {function} callback
 * @returns {Promise}
 * @private
 */
JobListDialog.prototype._dataTableAjaxCall = function (data, callback) {
  var self = this;
  return self.getServerConnector().getProjectJobs({
    start: data.start / data.length,
    length: data.length,
    projectId: self._projectId
  }).then(function (jobEntries) {
    var out = [];

    for (var i = 0; i < jobEntries.content.length; i++) {
      var entry = jobEntries.content[i];

      var row = entryToRow(entry);
      out.push(row);
    }
    callback({
      draw: data.draw,
      recordsTotal: jobEntries.totalElements,
      recordsFiltered: jobEntries.totalElements,
      data: out
    });
  });
};

JobListDialog.prototype.open = function () {
  var self = this;
  if (!$(self.getElement()).hasClass("ui-dialog-content")) {
    $(self.getElement()).dialog({
      dialogClass: 'minerva-logs-dialog',
      title: "Job list",
      autoOpen: false,
      resizable: false,
      width: Math.max(window.innerWidth / 2, window.innerWidth - 100),
      height: Math.max(window.innerHeight / 2, window.innerHeight - 100)
    });
  }

  $(self.getElement()).dialog("open");

  if (!$.fn.DataTable.isDataTable(self.tableElement)) {
    return new Promise(function (resolve) {
      $(self.tableElement).dataTable({
        serverSide: true,
        ordering: false,
        searching: false,
        ajax: function (data, callback) {
          resolve(self._dataTableAjaxCall(data, callback));
        },
        columns: self.getColumnsDefinition()
      });
    });
  } else {
    $(self.tableElement).dataTable().api(true).draw();
    return Promise.resolve();
  }

};

/**
 *
 * @returns {Array}
 */
JobListDialog.prototype.getColumnsDefinition = function () {
  return [{
    name: "id"
  }, {
    name: "jobType"
  }, {
    name: "jobStatus"
  }, {
    name: "progress"
  }, {
    name: "jobStarted"
  }, {
    name: "jobFinished"
  }, {
    name: "priority"
  }];
};

/**
 *
 * @returns {Promise}
 */
JobListDialog.prototype.init = function () {
  return Promise.resolve();
};

/**
 *
 */
JobListDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();
  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
  if ($.fn.DataTable.isDataTable(self.tableElement)) {
    $(self.tableElement).DataTable().destroy();
  }
};

module.exports = JobListDialog;
