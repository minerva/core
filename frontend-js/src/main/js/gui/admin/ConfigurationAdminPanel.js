"use strict";

var $ = require('jquery');
require('spectrum-colorpicker');

var AbstractAdminPanel = require('./AbstractAdminPanel');
var PrivilegeType = require('../../map/data/PrivilegeType');
var ConfigurationType = require('../../ConfigurationType');

var Functions = require('../../Functions');
var GuiConnector = require('../../GuiConnector');
var NetworkError = require('../../NetworkError');
var ValidationError = require('../../ValidationError');
var SignUpDapiDialog = require('./SignUpDapiDialog');
var SignInDapiDialog = require('./SignInDapiDialog');

var logger = require('../../logger');
var HttpStatus = require('http-status-codes');

var Promise = require("bluebird");
var xss = require("xss");

/**
 *
 * @param {Configuration} [params.configuration]
 * @param {HTMLElement} params.element
 * @param {string} params.panelName
 * @param params.parent
 *
 * @constructor
 * @extends AbstractAdminPanel
 */
function ConfigurationAdminPanel(params) {
  AbstractAdminPanel.call(this, params);

  var self = this;
  $(self.getElement()).removeClass("pre-scrollable");
  $(self.getElement()).addClass("minerva-configuration-tab");

  self._createGui();
}

ConfigurationAdminPanel.prototype = Object.create(AbstractAdminPanel.prototype);
ConfigurationAdminPanel.prototype.constructor = ConfigurationAdminPanel;

/**
 *
 * @private
 */
ConfigurationAdminPanel.prototype._createGui = function () {
  var self = this;
  self.getGuiUtils().initTabContent(self);


  $(self.getElement()).on("input", ".minerva-color-input", function () {
    var input = this;
    var value = $(input).val();
    var type = $(input).attr("data");
    if (value.length !== 6) {
      value = "FFFFFF";
    }
    $("[name='edit-color-" + type + "']", self.getElement()).css("background-color", "#" + value);
  });

  // $("[name='configurationTable']", self.getElement()).on("change", "input", function () {
  $(self.getElement()).on("change", "input, textarea", function () {
    var input = this;
    var type = $(input).attr("data");
    var name = $(input).attr("name");
    if (name !== undefined && name.indexOf("edit") === 0) {
      return self.saveOption(type);
    }
  });

  $(self.getElement()).on("click", ".minerva-color-button", function () {
    var button = this;
    var value = $(button).css("background-color");
    var type = $(button).attr("data");
    // noinspection JSUnusedGlobalSymbols
    var colorPicker = $(button).parent().spectrum({
      color: value,
      move: function (color) {
        var value = color.toHexString().replace("#", '');
        $("[name='edit-" + type + "']", self.getElement()).val(value);
        $("[name='edit-color-" + type + "']", self.getElement()).css("background-color", "#" + value);
      },
      hide: function () {
        colorPicker.spectrum("destroy");
        return self.saveOption(type);
      }
    });
    return new Promise.delay(1).then(function () {
      colorPicker.show();
      colorPicker.spectrum("show");
    });
  });

  $(self.getElement()).on("click", ".minerva-dapi-sign-up", function () {
    var dialog = new SignUpDapiDialog({
        element: Functions.createElement({
          type: "div"
        }),
        configuration: self.getConfiguration(),
        customMap: null,
        serverConnector: self.getServerConnector()
      }
    );
    self._signUpDialog = dialog;
    dialog.addListener("onSave", function (e) {
      var data = e.arg;
      var loginOption = self.getConfiguration().getOption(ConfigurationType.DAPI_LOGIN);
      var passwordOption = self.getConfiguration().getOption(ConfigurationType.DAPI_PASSWORD);
      loginOption.setValue(data.login);
      passwordOption.setValue(data.password);
      $("[name='edit-" + ConfigurationType.DAPI_LOGIN + "']", self.getElement()).val(data.login);
      $("[name='edit-" + ConfigurationType.DAPI_PASSWORD + "']", self.getElement()).val(data.password);
      return Promise.all([self.getServerConnector().updateConfigurationOption(loginOption),
        self.getServerConnector().updateConfigurationOption(passwordOption)]
      ).then(function () {
        GuiConnector.info("Account in DAPI created successfully. You should receive confirmation email from DAPI." +
          "Please click received confirmation link and refresh minerva to continue with DAPI configuration.")
      });
    });
    return dialog.init().then(function () {
      return dialog.open();
    });
  });
  $(self.getElement()).on("click", ".minerva-dapi-logout", function () {
    var loginOption = self.getConfiguration().getOption(ConfigurationType.DAPI_LOGIN);
    var passwordOption = self.getConfiguration().getOption(ConfigurationType.DAPI_PASSWORD);
    loginOption.setValue("");
    passwordOption.setValue("");
    return Promise.all([self.getServerConnector().updateConfigurationOption(loginOption),
      self.getServerConnector().updateConfigurationOption(passwordOption)]);
  });

  $(self.getElement()).on("click", ".minerva-dapi-sign-in", function () {
    var dialog = new SignInDapiDialog({
        element: Functions.createElement({
          type: "div"
        }),
        configuration: self.getConfiguration(),
        customMap: null,
        serverConnector: self.getServerConnector()
      }
    );
    self._signInDialog = dialog;
    return dialog.init().then(function () {
      return dialog.open();
    });
  });

  $(self.getElement()).on("click", ".minerva-accept-dapi-release", function () {
    var button = this;
    var tmp = $(this).attr("data").split(";");
    var databaseName = tmp[0];
    var releaseName = tmp[1];
    return self.getServerConnector().getDapiDatabaseRelease(databaseName, releaseName).then(function (release) {
      return GuiConnector.showConfirmationDialog({
        title: databaseName + " version: " + releaseName,
        message: "Do you accept the <a href='" + release.licenseUrl + "' target='" + databaseName + "'>license</a>?"
      })
    }).then(function (response) {
      if (response) {
        return self.getServerConnector().acceptDapiDatabaseRelease(databaseName, releaseName).then(function () {
          $('<div><i class="fa fa-check-circle minerva-dapi-status-ok"></i> Accepted<div>').insertAfter($(button));
          $(button).hide();
        });
      }
    }).catch(GuiConnector.alert);
  });

}
;

/**
 *
 * @param {ConfigurationOption[]} options
 * @param {string} type
 */
ConfigurationAdminPanel.prototype.createOptionsTable = function (options, type) {
  var self = this;

  var configurationDiv = Functions.createElement({
    type: "div",
    className: "pre-scrollable"
  });
  var configurationTable = Functions.createElement({
    type: "table",
    name: "configurationTable",
    className: "display",
    style: "width:100%"
  });
  configurationDiv.appendChild(configurationTable);

  // noinspection JSUnusedGlobalSymbols
  var dataTable = $(configurationTable).DataTable({
    columns: [{
      title: 'Name'
    }, {
      title: 'Value'
    }],
    columnDefs: [
      {"orderable": false, "targets": 1}
    ],
    order: [[0, "asc"]]
  });

  var data = [];

  for (var i = 0; i < options.length; i++) {
    var option = options[i];
    if (option.getGroup() === type) {
      var rowData = self.optionToTableRow(option);
      data.push(rowData);
    }
  }

  dataTable.clear().rows.add(data).draw();

  self.getGuiUtils().addTab(self, {name: type, content: configurationDiv});

  return Promise.all([self.bindDataTablePageLengthToUserPreference({
    element: configurationTable,
    preferenceName: 'admin-configuration-datatable-length'
  }), self.bindDataTableOrderToUserPreference({
    element: configurationTable,
    preferenceName: 'admin-configuration-' + type + '-datatable-order'
  })]).then(function () {
    return configurationDiv;
  });
};

/**
 *
 * @returns {Promise}
 */
ConfigurationAdminPanel.prototype.init = function () {
  var self = this;
  return AbstractAdminPanel.prototype.init.call(this).then(function () {
    return self.getServerConnector().getLoggedUser();
  }).then(function (user) {
    var configuration = self.getConfiguration();
    var privilege = configuration.getPrivilegeType(PrivilegeType.IS_ADMIN);
    if (user.hasPrivilege(privilege)) {
      return self.setOptions(configuration.getOptions());
    } else {
      self.disablePanel("You have no privilege to manage configuration");
    }
  });
};

/**
 *
 * @param {ConfigurationOption[]} options
 *
 * @returns {Promise}
 */
ConfigurationAdminPanel.prototype.setOptions = function (options) {
  var self = this;

  var categories = {"": true};
  var promises = [];
  for (var i = 0; i < options.length; i++) {
    var option = options[i];

    var group = option.getGroup();
    if (categories[group] === undefined && group !== undefined) {
      categories[group] = true;
      var promise;
      if (group === "Data-API configuration") {
        promise = self.createDapiConfiguration(options);
      } else {
        promise = self.createOptionsTable(options, group);
      }
      promises.push(promise);
    }
  }
  return Promise.all(promises);
};

ConfigurationAdminPanel.prototype.createDapiConfiguration = function (options) {
  var type = "Data-API configuration";
  var self = this;
  var contentDiv = Functions.createElement({
    type: "div",
    className: "pre-scrollable"
  });
  var configurationTable = Functions.createElement({
    type: "table",
    className: "display minerva-dapi-configuration",
    style: "width:100%"
  });
  contentDiv.appendChild(configurationTable);

  // noinspection JSUnusedGlobalSymbols
  var dataTable = $(configurationTable).DataTable({
    columns: [{
      title: 'Name'
    }, {
      title: 'Value'
    }],
    "ordering": false,
    "paging": false,
    "searching": false
  });

  self.getGuiUtils().addTab(self, {name: type, content: contentDiv});

  return self.refreshDapiData(options, type).then(function () {
    self.getConfiguration().addListener("onOptionChanged", function (e) {
      var option = e.arg;
      if (option.getGroup() === type) {
        return self.refreshDapiData(self.getConfiguration().getOptions());
      }
    });
    return contentDiv;
  });

};

ConfigurationAdminPanel.prototype.refreshDapiData = function (options) {
  var self = this;

  var dataTable = $(".minerva-dapi-configuration", self.getElement()).DataTable();
  var data = [];

  var loginOption = self.getConfiguration().getOption(ConfigurationType.DAPI_LOGIN);
  var login = xss(loginOption.getValue());

  return self.getServerConnector().isDapiConnectionValid().then(function (isValid) {
    var row = [];
    row[0] = "DAPI connection validity";
    if (isValid) {
      row[1] = "<div class='minerva-dapi-status'>User logged in (" + login + ")<i class='fa fa-check-circle minerva-dapi-status-ok'></i></div>" +
        "<button class='minerva-dapi-logout'>Log out</button>";
    } else {
      row[1] = "<div class='minerva-dapi-status'>No user logged in <i class='fa fa-times-circle minerva-dapi-status-not-ok'></i></div>" +
        "<button class='minerva-dapi-sign-in'>Log in</button> <button class='minerva-dapi-sign-up'>Create account</button>";
    }
    data.push(row);
    if (isValid) {
      return self.getReleasesRows();
    } else {
      return []
    }
  }).then(function (rows) {
    data = data.concat(rows);
    dataTable.clear().rows.add(data).draw();
  });
};

/**
 *
 * @return {Promise<[]>}
 * @private
 */
ConfigurationAdminPanel.prototype.getReleasesRows = function () {
  var data = [];
  var self = this;
  return self.getServerConnector().getDapiDatabases().then(function (databases) {
    databases.forEach(function (database) {
      data.push(["<h4>" + database.name + " database configuration</h4>", ""]);
      database.releases.forEach(function (release) {
        var description = "Release " + release.name + " (license - <a href='" + release.licenseUrl + "'>" + release.licenseUrl + "</a>)";
        if (release.accepted) {
          data.push([description, "<i class='fa fa-check-circle minerva-dapi-status-ok'></i> Accepted"]);
        } else {
          data.push([description, "<button class='minerva-accept-dapi-release' data='" + database.name + ";" + release.name + "'>Accept license</button>"]);
        }
      })
    });
    return data;
  });
};

/**
 *
 * @param {ConfigurationOption} option
 * @returns {Array}
 */
ConfigurationAdminPanel.prototype.optionToTableRow = function (option) {
  var value = option.getValue();
  if (value === undefined) {
    value = "";
  }
  var row = [];
  var editOption;

  if (option.getValueType() === "STRING" ||
    option.getValueType() === "INTEGER" ||
    option.getValueType() === "DOUBLE" ||
    option.getValueType() === "EMAIL" ||
    option.getValueType() === "URL") {
    editOption = "<input name='edit-" + option.getType() + "' data='" + option.getType() + "' value='" + value + "'/>";
  } else if (option.getValueType() === "PASSWORD") {
    editOption = "<input type='password' name='edit-" + option.getType() + "' autocomplete='new-password' data='" + option.getType() + "' value='" + value + "'/>";
  } else if (option.getValueType() === "TEXT") {
    editOption = "<textarea name='edit-" + option.getType() + "' data='" + option.getType() + "' >" + xss(value) + "</textarea>";
  } else if (option.getValueType() === "BOOLEAN") {
    var checked = "";
    if (value.toLowerCase() === "true") {
      checked = " checked ";
    }
    editOption = "<input type='checkbox' name='edit-" + option.getType() + "' " + checked + " data='" + option.getType() + "' />";
  } else if (option.getValueType() === "COLOR") {
    editOption = "<div>" +
      "<input class='minerva-color-input' name='edit-" + option.getType() + "' data='" + option.getType() + "' value='" + value + "'/>" +
      "<button class='minerva-color-button' name='edit-color-" + option.getType() + "' data='" + option.getType() + "' style='background-color: #" + value + "'>&nbsp</button>" +
      "</div>";
  } else {
    logger.warn("Don't know how to handle: " + option.getValueType());
    editOption = "<input name='edit-" + option.getType() + "' value='" + value + "' readonly data='" + option.getType() + "' />";
  }
  row[0] = option.getCommonName();
  row[1] = editOption;
  return row;
};

/**
 *
 * @param {string} type
 * @returns {Promise}
 */
ConfigurationAdminPanel.prototype.saveOption = function (type) {
  var self = this;
  var option;

  var value;
  var element = $("[name='edit-" + type + "']", self.getElement());
  if (element.is(':checkbox')) {
    if (element.is(':checked')) {
      value = "true";
    } else {
      value = "false";
    }
  } else {
    value = element.val();
  }

  var oldVal;
  GuiConnector.showProcessing();
  return self.getServerConnector().getConfiguration().then(function (configuration) {
    option = configuration.getOption(type);
    oldVal = option.getValue();
    if (option === undefined) {
      return Promise.reject(new ValidationError("Unknown configuration type: " + type));
    }
    if (option.getValueType() === 'PASSWORD' && value === '') {
      return new Promise(function (resolve) {
        var html = '<form><span>Do you want to reset this password?</span><br></form>';
        $(html).dialog({
          modal: true,
          title: 'Reset password',
          close: function () {
            $(this).dialog('destroy').remove();
            resolve(false);
          },
          buttons: {
            'YES': function () {
              $(this).dialog('destroy').remove();
              resolve(true);
            },
            'NO': function () {
              $(this).dialog('destroy').remove();
              resolve(false);
            }
          }
        });
      });
    } else {
      return true;
    }
  }).then(function (performUpdate) {
    if (performUpdate) {
      option.setValue(value);
      return self.getServerConnector().updateConfigurationOption(option);
    }
  }).then(function () {
    if (type === ConfigurationType.TERMS_OF_USE) {
      return new Promise(function (resolve) {
        var html = '<form><span>Do you want to reset user the status of ToS acceptance for all users of the platform?</span><br></form>';
        $(html).dialog({
          modal: true,
          title: 'Status of users ToS acceptance',
          close: function () {
            $(this).dialog('destroy').remove();
            resolve(false);
          },
          buttons: {
            'YES': function () {
              $(this).dialog('destroy').remove();
              resolve(true);
            },
            'NO': function () {
              $(this).dialog('destroy').remove();
              resolve(false);
            }
          }
        });
      }).then(function (status) {
        if (status) {
          return self.getServerConnector().resetUserTos();
        }
      });
    } else if (type === ConfigurationType.DAPI_PASSWORD) {
      return self.getServerConnector().isDapiConnectionValid().then(function (isValid) {
        if (!isValid) {
          GuiConnector.warn("Invalid credentials");
        }
      });
    }
  }).catch(function (e) {
    if (e instanceof NetworkError && e.statusCode === HttpStatus.BAD_REQUEST) {
      var content = e.content;
      GuiConnector.alert(new ValidationError(content.reason));
      option.setValue(oldVal);
      element.val(oldVal);
    } else {
      GuiConnector.alert(e);
    }
  }).finally(GuiConnector.hideProcessing);
};

/**
 *
 */
ConfigurationAdminPanel.prototype.destroy = function () {
  var self = this;
  var tables = $("[name='configurationTable']", self.getElement());
  for (var i = 0; i < tables.length; i++) {
    if ($.fn.DataTable.isDataTable(tables[i])) {
      $(tables[i]).DataTable().destroy();
    }
  }

};

module.exports = ConfigurationAdminPanel;
