"use strict";

var md5 = require('md5');

var $ = require('jquery');

var mockMinervaPluginInterface = require('./mockMinervaPluginInterface');

var AbstractAdminPanel = require('./AbstractAdminPanel');
var AddPluginDialog = require('./AddPluginDialog');

var Functions = require('../../Functions');
var GuiConnector = require('../../GuiConnector');
var PrivilegeType = require('../../map/data/PrivilegeType');
var NetworkError = require('../../NetworkError');
var ValidationError = require('../../ValidationError');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var Promise = require('bluebird');
var xss = require('xss');
var HttpStatus = require('http-status-codes');

/**
 *
 * @param {Configuration} params.configuration
 * @param {HTMLElement} params.element
 *
 * @constructor
 */
function PluginAdminPanel(params) {
  params["panelName"] = "plugins";
  AbstractAdminPanel.call(this, params);

  var self = this;
  $(self.getElement()).addClass("minerva-plugin-tab");

  self._createGui();
}

PluginAdminPanel.prototype = Object.create(AbstractAdminPanel.prototype);
PluginAdminPanel.prototype.constructor = PluginAdminPanel;

/**
 *
 * @private
 */
PluginAdminPanel.prototype._createGui = function () {
  var self = this;
  var pluginDiv = Functions.createElement({
    type: "div"
  });
  self.getElement().appendChild(pluginDiv);

  pluginDiv.appendChild(self._createMenuRow());

  var pluginsTable = Functions.createElement({
    type: "table",
    name: "pluginsTable",
    className: "display",
    style: "width:100%"
  });
  pluginDiv.appendChild(pluginsTable);

  // noinspection JSUnusedGlobalSymbols
  $(pluginsTable).DataTable({
    columns: [{
      title: 'Name'
    }, {
      title: 'Version'
    }, {
      title: 'Url'
    }, {
      title: 'Default'
    }, {
      title: 'Re-Validate'
    }, {
      title: 'Remove',
      orderable: false
    }],
    order: [[1, "asc"]]
  });
  self.bindDataTablePageLengthToUserPreference({
    element: pluginsTable,
    preferenceName: 'admin-plugins-datatable-length'
  });

  $(pluginsTable).on("click", "[name='removePlugin']", function () {
    var button = this;
    return self.askConfirmRemoval({
      title: "INFO",
      content: "Do you really want to remove this plugin?",
      input: false
    }).then(function (param) {
      if (param.status) {
        $(button).prop('disabled', true);
        return self.getServerConnector().removePlugin({hash: $(button).attr("data")}).catch(function (error) {
          if (error instanceof NetworkError && error.statusCode === HttpStatus.NOT_FOUND) {
            GuiConnector.warn("Plugin does not exist anymore");
          } else {
            throw error;
          }
        }).finally(function () {
          $(button).prop('disabled', false);
          return self.onRefreshClicked();
        });
      }
    }).catch(GuiConnector.alert);
  });

  $(pluginsTable).on("click", "[name='re-validate-plugin']", function () {
    var button = this;
    var hash = $(button).attr("data");
    var error = false;
    var plugin;
    var url;

    var pluginData = {
      pluginRawNewData: {}
    }

    return self.getServerConnector().getPluginData(hash).then(function (response) {
      plugin = response;
      url = plugin.getUrls()[0]
      if (url.includes('?')) {
        url += '&_=' + new Date().getTime();
      } else {
        url += '?_=' + new Date().getTime();
      }
      return self.getServerConnector().sendRequest({
        url: url,
        description: "Loading plugin: " + url,
        method: "GET"
      }).catch(function (e) {
        error = e;
      });
    }).then(function (content) {
      var newHash = md5(content);
      var pluginRawData = undefined;
      // noinspection JSUnusedLocalSymbols
      var minervaDefine = function (pluginFunction) {
        try {
          if (typeof pluginFunction === "function") {
            pluginRawData = pluginFunction();
          } else {
            pluginRawData = pluginFunction;
          }
        } catch (e) {
          error = e;
        }
      };
      content += "//# sourceURL=" + url;


      var minerva = mockMinervaPluginInterface(pluginData, self.getConfiguration(), self.getServerConnector());

      try {
        eval(content);
      } catch (e) {
        logger.warn("Problem with loading of a plugin", e);
        error = new ValidationError("Plugin could not be loaded. Verify if the file is a valid minerva plugin.", e);
      }
      if (error) {
        return self.askConfirmRemoval({
          title: "INFO",
          content: "Plugin source file does not exist. Do you want to remove this plugin?",
          input: false
        }).then(function (param) {
          if (param.status) {
            return self.getServerConnector().removePlugin({hash: hash}).then(function () {
              return self.onRefreshClicked();
            });
          }
        });
      } else {

        var version;
        var name;
        if (pluginRawData) {
          version = pluginRawData.getVersion();
          name = pluginRawData.getName();
        } else {
          name = pluginData.pluginRawNewData.pluginName;
          version = pluginData.pluginRawNewData.pluginVersion;
        }
        return self.getServerConnector().updatePlugin({
          hash: hash,
          newHash: newHash,
          version: version,
          name: name
        }).then(function () {
          return self.onRefreshClicked();
        });
      }
    }).catch(GuiConnector.alert);
  });


  $(pluginsTable).on("click", "[name='edit-default-plugin']", function () {
    var checkbox = this;
    var hash = $(this).attr("data");
    return self.getServerConnector().updatePlugin({
      hash: hash,
      isDefault: $(checkbox).is(':checked')
    }).then(function () {
      return self.onRefreshClicked();
    }).catch(GuiConnector.alert);
  });

  pluginDiv.appendChild(self._createMenuRow());
};

/**
 *
 * @returns {HTMLElement}
 * @private
 */
PluginAdminPanel.prototype._createMenuRow = function () {
  var self = this;
  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });

  var addPluginButton = Functions.createElement({
    type: "button",
    name: "addPlugin",
    content: '<span class="ui-icon ui-icon-circle-plus"></span>&nbsp;ADD PLUGIN',
    onclick: function () {
      return self.onAddClicked().catch(GuiConnector.alert);
    },
    xss: false
  });
  var refreshButton = Functions.createElement({
    type: "button",
    name: "refreshPlugins",
    content: '<span class="ui-icon ui-icon-refresh"></span>&nbsp;REFRESH',
    onclick: function () {
      return self.onRefreshClicked().catch(GuiConnector.alert);
    },
    xss: false
  });
  menuRow.appendChild(addPluginButton);
  menuRow.appendChild(refreshButton);
  return menuRow;
};

/**
 *
 * @returns {Promise}
 */
PluginAdminPanel.prototype.init = function () {
  var self = this;
  return AbstractAdminPanel.prototype.init.call(this).then(function () {
    return self.getServerConnector().getLoggedUser();
  }).then(function (user) {
    if (user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN))) {
      return self.onRefreshClicked();
    } else {
      self.disablePanel("You have no privilege to manage plugins");
    }
  }).then(function () {
    var pluginsTable = $("[name='pluginsTable']", self.getElement())[0];

    return self.bindDataTableOrderToUserPreference({
      element: pluginsTable,
      preferenceName: 'admin-plugins-datatable-order'
    });
  });
};

/**
 *
 * @returns {Promise}
 */
PluginAdminPanel.prototype.onRefreshClicked = function () {
  var self = this;
  return self.getServerConnector().getPluginsData().then(function (plugins) {
    return self.setPlugins(plugins);
  });
};

/**
 *
 * @param {PluginData[]} plugins
 *
 */
PluginAdminPanel.prototype.setPlugins = function (plugins) {
  var self = this;

  var dataTable = $("[name='pluginsTable']", self.getElement()).DataTable();
  var data = [];
  var page = dataTable.page();

  for (var i = 0; i < plugins.length; i++) {
    var plugin = plugins[i];
    if (plugin.isPublic()) {
      data.push(self.pluginToTableRow(plugin));
    }
  }
  //it should be simplified, but I couldn't make it work
  dataTable.clear().rows.add(data).page(page).draw(false).page(page).draw(false);
};

/**
 *
 * @param {PluginData} plugin
 * @returns {Array}
 */
PluginAdminPanel.prototype.pluginToTableRow = function (plugin) {
  var row = [];
  var urls = plugin.getUrls().slice();
  for (var i = 0; i < urls.length; i++) {
    urls[i] = xss(urls[i]);
  }

  row[0] = xss(plugin.getName());
  row[1] = xss(plugin.getVersion());
  row[2] = urls;

  var checked = "";
  if (plugin.isDefault()) {
    checked = " checked ";
  }
  row[3] = "<input type='checkbox' name='edit-default-plugin' " + checked + " data='" + plugin.getHash() + "' />";
  row[4] = "<button name='re-validate-plugin' data='" + plugin.getHash() + "' ><i class='fa fa-check-circle'></button>";

  row[5] = "<button name='removePlugin' data='" + plugin.getHash() + "' ><i class='fa fa-trash-alt'></button>";
  return row;
};


/**
 *
 * @returns {Promise}
 */
PluginAdminPanel.prototype.destroy = function () {
  var self = this;
  var table = $("[name='pluginsTable']", self.getElement())[0];
  $(table).DataTable().destroy();
  var promises = [];
  if (self._addPluginDialog !== undefined) {
    promises.push(self._addPluginDialog.destroy());
  }
  promises.push(AbstractAdminPanel.prototype.destroy.call(self));

  return Promise.all(promises);
};

/**
 *
 * @returns {Promise}
 */
PluginAdminPanel.prototype.getDialog = function () {
  var self = this;
  var dialog = self._addPluginDialog;
  if (dialog === undefined) {
    dialog = new AddPluginDialog({
      element: Functions.createElement({
        type: "div"
      }),
      configuration: self.getConfiguration(),
      customMap: null,
      serverConnector: self.getServerConnector()
    });
    self._addPluginDialog = dialog;
    dialog.addListener("onSave", function () {
      return self.onRefreshClicked();
    });
    return dialog.init().then(function () {
      return dialog;
    });
  } else {
    return Promise.resolve(dialog);
  }
};

/**
 *
 * @returns {Promise}
 */
PluginAdminPanel.prototype.onAddClicked = function () {
  var self = this;
  GuiConnector.showProcessing();
  return self.getDialog().then(function (dialog) {
    dialog.open();
    GuiConnector.hideProcessing();
  }).catch(function (error) {
    GuiConnector.hideProcessing();
    return Promise.reject(error);
  });
};


module.exports = PluginAdminPanel;
