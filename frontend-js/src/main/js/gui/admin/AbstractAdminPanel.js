"use strict";

var $ = require('jquery');

/* exported logger */
/* exported Promise*/

var Panel = require('../Panel');
var UserPreferences = require('../../map/data/UserPreferences');
var GuiConnector = require('../../GuiConnector');

var logger = require('../../logger');
var Functions = require('../../Functions');
var NetworkError = require('../../NetworkError');
var Promise = require("bluebird");
var HttpStatus = require('http-status-codes');

/**
 *
 * @param {Configuration} [params.configuration]
 * @param {HTMLElement} params.element
 * @param {Project} params.project
 * @param {CustomMap} params.customMap
 * @param {string} params.panelName
 * @param {string} [params.helpTip]
 * @param params.parent
 *
 * @constructor
 * @extends Panel
 */
function AbstractAdminPanel(params) {
  params["scrollable"] = true;
  Panel.call(this, params);

  this._initPromises = [];
  this._eventBinds = [];
}

AbstractAdminPanel.prototype = Object.create(Panel.prototype);
AbstractAdminPanel.prototype.constructor = AbstractAdminPanel;

/**
 *
 * @returns {Promise}
 */
AbstractAdminPanel.prototype.init = function () {
  this._initialized = true;
  return Promise.all(this._initPromises);
};

/**
 *
 * @param {Object} params
 * @param {string} params.event
 * @param {jQuery} params.jQueryObject
 * @param {string} params.preferenceName
 * @param {Object} params.defaultValue
 * @param {function} params.getter
 * @param {function} params.setter
 */
AbstractAdminPanel.prototype.bindUserGuiPreference = function (params) {
  var self = this;
  params.jQueryObject.on(params.event, function () {
    return self.getServerConnector().getLoggedUser().then(function (user) {
      var oldValue = user.getPreferences().getGuiPreference(params.preferenceName, params.defaultValue);
      var newValue = params.getter();
      if (oldValue !== newValue) {
        user.getPreferences().setGuiPreference(params.preferenceName, newValue);

        var data = new UserPreferences();
        data.setGuiPreference(params.preferenceName, newValue);
        return self.getServerConnector().updateUserPreferences({user: user, preferences: data})
          .catch(function (error) {
            if (error instanceof NetworkError && error.statusCode === HttpStatus.CONFLICT) {
              logger.warn("Problem with updating preference");
            } else {
              throw error;
            }
          });
      }
    }).catch(GuiConnector.alert);
  });
  var promise = self.getServerConnector().getLoggedUser().then(function (user) {
    var value = user.getPreferences().getGuiPreference(params.preferenceName, params.defaultValue);
    return params.setter(value);
  });

  this._eventBinds.push(params);
  if (!this._initialized) {
    this._initPromises.push(promise);
  } else {
    return promise;
  }

};

/**
 *
 * @param {Object} param
 * @param {HTMLElement} param.element
 * @param {string} param.preferenceName
 */
AbstractAdminPanel.prototype.bindDataTablePageLengthToUserPreference = function (param) {
  var jQueryObject = $(param.element);
  return this.bindUserGuiPreference({
    jQueryObject: jQueryObject,
    event: 'length.dt',
    preferenceName: param.preferenceName,
    defaultValue: '10',
    getter: function () {
      return jQueryObject.DataTable().page.len() + '';
    },
    setter: function (value) {
      return jQueryObject.DataTable().page.len(value).draw();
    }
  })
};

/**
 *
 * @param {Object} param
 * @param {HTMLElement} param.element
 * @param {string} param.preferenceName
 */
AbstractAdminPanel.prototype.bindDataTableOrderToUserPreference = function (param) {
  var jQueryObject = $(param.element);
  return this.bindUserGuiPreference({
    jQueryObject: jQueryObject,
    event: 'order.dt',
    preferenceName: param.preferenceName,
    defaultValue: '0-asc',
    getter: function () {
      var order = jQueryObject.DataTable().order();
      return order[0][0] + "-" + order[0][1];
    },
    setter: function (value) {
      var tmp = value.split("-");
      var column = parseInt(tmp[0]);
      var order = tmp[1];
      if (Functions.isInt(column) && (order === "asc" || order === "desc")) {
        return jQueryObject.DataTable().order([column, order]).draw();
      } else {
        logger.warn("Invalid order: " + column + "; " + order);
      }
    }
  })
};


/**
 *
 * @returns {Promise}
 */
AbstractAdminPanel.prototype.destroy = function () {
  return Promise.each(this._eventBinds, function (params) {
    params.jQueryObject.off(params.event);
  });
};


module.exports = AbstractAdminPanel;
