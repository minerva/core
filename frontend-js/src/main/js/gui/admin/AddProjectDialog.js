"use strict";

var $ = require('jquery');
var Promise = require("bluebird");
var JSZip = require("jszip");
var xss = require('xss');

var AbstractGuiElement = require('../AbstractGuiElement');
var ChooseAnnotatorsDialog = require('./ChooseAnnotatorsDialog');
var GuiConnector = require('../../GuiConnector');
var OverlayParser = require('../../map/OverlayParser');
var ZipEntry = require('./ZipEntry');
var ConfigurationType = require('../../ConfigurationType');

var Functions = require('../../Functions');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var guiUtils = new (require('../leftPanel/GuiUtils'))();

var ObjectExistsError = require("../../ObjectExistsError");
var UserPreferences = require("../../map/data/UserPreferences");
var ValidationError = require("../../ValidationError");

/**
 *
 * @param params
 * @param {HTMLElement} params.element
 * @param {Configuration} params.configuration
 * @param {ServerConnector} params.serverConnector
 * @constructor
 * @extends {AbstractGuiElement}
 */
function AddProjectDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self.registerListenerType("onFileUpload");
  self.registerListenerType("onZipFileUpload");
  self.registerListenerType("onProjectAdd");
  self.setZipEntries([]);
  $(self.getElement()).addClass("minerva-edit-project-dialog");
  $(self.getElement()).css({overflow: "hidden"});

  self.createGui();
}

AddProjectDialog.prototype = Object.create(AbstractGuiElement.prototype);
AddProjectDialog.prototype.constructor = AddProjectDialog;

/**
 *
 */
AddProjectDialog.prototype.createGui = function () {
  var self = this;

  guiUtils.initTabContent(self);

  guiUtils.addTab(self, {
    name: "GENERAL",
    content: self.createGeneralTabContent()
  });
  guiUtils.addTab(self, {
    name: "OVERLAYS",
    content: self.createOverlaysTabContent(),
    disabled: true
  });
  guiUtils.addTab(self, {
    name: "GLYPHS",
    content: self.createGlyphsTabContent(),
    disabled: true
  });
  guiUtils.addTab(self, {
    name: "SUBMAPS",
    content: self.createSubmapsTabContent(),
    disabled: true
  });
  guiUtils.addTab(self, {
    name: "IMAGES",
    content: self.createOverviewImagesTabContent(),
    disabled: true
  });
};

/**
 *
 * @returns {Promise}
 */
AddProjectDialog.prototype.showAnnotatorsDialog = function () {
  var self = this;
  var promise;
  if (self._annotatorsDialog === undefined) {
    self._annotatorsDialog = new ChooseAnnotatorsDialog({
      element: Functions.createElement({type: "div"}),
      customMap: null,
      configuration: self.getConfiguration(),
      serverConnector: self.getServerConnector()
    });
    promise = self._annotatorsDialog.init();
  } else {
    promise = Promise.resolve();
  }
  return promise.then(function () {
    return self._annotatorsDialog.open();
  });
};

/**
 *
 * @returns {HTMLElement}
 */
AddProjectDialog.prototype.createGeneralTabContent = function () {
  var self = this;

  var result = Functions.createElement({
    type: "div",
    className: "minerva-project-general-tab"
  });

  var table = Functions.createElement({
    type: "div",
    style: "display:table; width:100%"
  });
  result.appendChild(table);

  var fileInput = Functions.createElement({
    type: "input",
    inputType: "file",
    name: "project-file"
  });
  fileInput.addEventListener("change", function () {
    return self.callListeners("onFileUpload", fileInput.files[0]).catch(GuiConnector.alert);
  }, false);
  self.addListener("onFileUpload", function (e) {
    var file = e.arg;
    return self.processFile(file);
  });
  self.addListener("onFileUpload", function (e) {
    var file = e.arg;
    return self.setZipFileContent(file);
  });
  self.addListener("onFileUpload", function (e) {
    var file = e.arg;
    if (file.name.lastIndexOf('.') > 0) {
      return self.setProjectId(file.name.substring(0, file.name.lastIndexOf('.')).replace(/\./g, '-'));
    } else {
      return file.name;
    }
  });
  self.addListener("onFileUpload", function (e) {
    var file = e.arg;
    return new Promise(function (resolve, reject) {
      var reader = new FileReader();
      reader.readAsText(file);

      reader.onload = function (e) {
        try {
          resolve(e.target.result);
        } catch (error) {
          reject(error);
        }
      };
      reader.onerror = function () {
        reject(new Error("Problem reading file"));
      };

    }).then(function (content) {
      return self.setFileParserForFilename(file.name, content);
    })
  });

  var manualUrl = self.getConfiguration().getOption(ConfigurationType.USER_MANUAL_FILE).getValue();
  var help = 'File with the map. For a quick start, CellDesigner files are accepted directly. Available options and configurations of the source file are discussed in <a href="' + manualUrl + '">manual</a>.';
  table.appendChild(self.createRow([guiUtils.createLabel("Upload file: "), fileInput, guiUtils.createHelpButton(help)]));

  var fileFormatSelect = Functions.createElement({
    type: "select",
    name: "project-format"
  });

  table.appendChild(self.createRow([guiUtils.createLabel("File format: "), fileFormatSelect]));

  var licenseTypeSelect = Functions.createElement({
    type: "select",
    name: "project-license",
    style: "width: 172px;",
    onchange: function () {
      var license = self.getLicense();
      if (license !== null) {
        $("div:has(> div > [name='project-custom-license-name'])", self.getElement()).hide();
        $("div:has(> div > [name='project-custom-license-url'])", self.getElement()).hide();
      } else {
        $("div:has(> div > [name='project-custom-license-name'])", self.getElement()).show();
        $("div:has(> div > [name='project-custom-license-url'])", self.getElement()).show();
      }
    }
  });

  table.appendChild(self.createRow([guiUtils.createLabel("License: "), licenseTypeSelect]));

  table.appendChild(self.createInputRow({
    labelName: "Custom license name",
    inputName: "project-custom-license-name"
  }));

  table.appendChild(self.createInputRow({
    labelName: "Custom license url",
    inputName: "project-custom-license-url"
  }));

  table.appendChild(self.createInputRow({
    labelName: "ProjectId:",
    inputName: "project-id",
    help: 'A working name of the uploaded project on the MINERVA platform. Unique in the platform.'
  }));
  table.appendChild(self.createInputRow({
    labelName: "Project name:",
    inputName: "project-name",
    help: 'The name of the uploaded project displayed in the top left corner of the Admin and User panels; your official name of the project.'
  }));
  table.appendChild(self.createInputRow({
    labelName: "Project Disease:",
    inputName: "project-disease",
    help: 'Mesh ID identifying disease connected to this map (ie. for Parkinson\'s Disease it would be D010300).'
  }));
  table.appendChild(self.createInputRow({
    labelName: "Organism:",
    inputName: "project-organism",
    help: 'Taxonomy ID identifying organism for which project is dedicated (ie. for Human map it would be 9606).'
  }));
  table.appendChild(self.createInputRow({
    labelName: "Version:",
    inputName: "project-version",
    help: 'A text field displayed next to the name of your project in the User panel.'
  }));
  table.appendChild(self.createInputRow({
    labelName: "Notify email:",
    inputName: "project-notify-email",
    help: 'E-mail address that should be used for project change notifications.'
  }));

  var showAnnotatorsButton = Functions.createElement({
    type: "button",
    name: "project-show-annotators",
    content: "Advanced",
    onclick: function () {
      return self.showAnnotatorsDialog().then(null, GuiConnector.alert);
    }
  });

  table.appendChild(self.createCheckboxRow({
    labelName: "Annotate model automatically:",
    defaultValue: false,
    inputName: "project-annotate-automatically",
    elements: [showAnnotatorsButton],
    help: 'If this checkbox is checked, elements of the uploaded map will be automatically annotated using built in ' +
      'annotators. Behavior of the annotators can be configured by clicking the Advanced button.'
  }));
  table.appendChild(self.createCheckboxRow({
    labelName: "Auto margin:",
    defaultValue: true,
    inputName: "project-auto-margin",
    help: 'If this checkbox is checked, upon generation of the graphics, empty spaces surrounding elements and ' +
      'interactions will be cropped.'
  }));
  table.appendChild(self.createCheckboxRow({
    labelName: "Display as SBGN:",
    defaultValue: false,
    inputName: "project-sbgn-visualization",
    help: 'If this checkbox is checked, the uploaded model will be displayed in SBGN format, instead of default ' +
      'CellDesigner format.'
  }));

  var saveProjectButton = Functions.createElement({
    type: "button",
    name: "saveProject",
    content: '<span class="ui-icon ui-icon-disk"></span>&nbsp;SAVE',
    onclick: function () {
      return self.onSaveClicked().then(function () {
        return self.close();
      }).catch(GuiConnector.alert);
    },
    xss: false
  });
  var cancelButton = Functions.createElement({
    type: "button",
    name: "cancelProject",
    content: '<span class="ui-icon ui-icon-cancel"></span>&nbsp;CANCEL',
    onclick: function () {
      return self.close();
    },
    xss: false
  });
  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });
  result.appendChild(menuRow);
  menuRow.appendChild(saveProjectButton);
  menuRow.appendChild(cancelButton);

  return result;
};

/**
 *
 * @param {string} [params.defaultValue=""]
 * @param {string} params.labelName
 * @param {string} params.inputName
 * @param {string} [params.help]
 *
 * @returns {HTMLElement}
 */
AddProjectDialog.prototype.createInputRow = function (params) {
  if (params.defaultValue === undefined) {
    params.defaultValue = "";
  }
  var label = Functions.createElement({
    type: "div",
    style: "display:table-cell; width:200px",
    content: xss(params.labelName)
  });
  var input = Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='" + xss(params.inputName) + "' value='" + xss(params.defaultValue) + "' style='width:100%'/>",
    xss: false
  });
  var elements = [label, input];
  if (params.help !== undefined) {
    elements.push(guiUtils.createHelpButton(params.help));
  }
  return this.createRow(elements);
};

/**
 *
 * @param {string} params.labelName
 * @param {boolean} [params.defaultValue=false]
 * @param {string} params.inputName
 * @param {HTMLElement[]} [params.elements]
 * @param {string} [params.help]
 * @returns {HTMLElement}
 */
AddProjectDialog.prototype.createCheckboxRow = function (params) {
  var labelName = params.labelName;
  var defaultValue = params.defaultValue;
  var inputName = params.inputName;
  var elements = params.elements;
  var tooltip = params.help;
  if (elements === undefined) {
    elements = [];
  }
  var label = Functions.createElement({
    type: "div",
    style: "display:table-cell;vertical-align: middle;",
    content: xss(labelName)
  });
  var checked = "";
  if (defaultValue) {
    checked = "checked";
  }
  var checkbox = Functions.createElement({
    type: "div",
    content: "<input type='checkbox' name='" + xss(inputName) + "' " + checked + "/>",
    style: "float:left;",
    xss: false
  });
  var checkBoxDiv = Functions.createElement({type: "div", style: "display:table-cell; vertical-align: middle"});
  checkBoxDiv.appendChild(checkbox);
  var rowElements = [label, checkBoxDiv];

  if (tooltip !== undefined) {
    rowElements.push(guiUtils.createHelpButton(tooltip));
  }
  for (var i = 0; i < elements.length; i++) {
    $(elements[i]).css("float", "left");
    checkBoxDiv.appendChild(elements[i]);
  }
  return this.createRow(rowElements);
};

/**
 *
 * @param {HTMLElement[]} elements
 * @returns {HTMLElement}
 */
AddProjectDialog.prototype.createRow = function (elements) {
  var result = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  for (var columnNumber = 0; columnNumber < elements.length; columnNumber++) {
    var element = elements[columnNumber];
    if (element.tagName.toLowerCase() !== 'div') {
      var style = "display:table-cell; vertical-align: middle; ";
      if (columnNumber === 0) {
        style += "width:200px;";
      }
      var labelContainer = Functions.createElement({
        type: "div",
        style: style
      });
      labelContainer.appendChild(element);
      element = labelContainer;
    }
    result.appendChild(element);
  }
  return result;
};

/**
 *
 * @returns {HTMLElement}
 */
AddProjectDialog.prototype.createOverlaysTabContent = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div",
    className: "minerva-project-overlays-tab"
  });
  result.appendChild(self._createOverlayTable());
  return result;
};

/**
 *
 * @returns {HTMLElement}
 * @private
 */
AddProjectDialog.prototype._createOverlayTable = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  var overlaysTable = Functions.createElement({
    type: "table",
    name: "overlaysTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(overlaysTable);

  $(overlaysTable).DataTable({
    columns: [{
      title: 'File name'
    }, {
      title: 'Name'
    }, {
      title: 'Description'
    }]
  });

  $(overlaysTable).on("input", "[name='overlayName']", function () {
    var input = this;
    var filename = $(input).attr("data");
    self.getEntryByFilename(filename).getData().name = $(input).val();
  });

  $(overlaysTable).on("input", "[name='overlayDescription']", function () {
    var input = this;
    var filename = $(input).attr("data");
    self.getEntryByFilename(filename).getData().description = $(input).val();
  });

  self.addListener("onZipFileUpload", function () {
    var entries = self.getZipEntries();
    var dataTable = $($("[name='overlaysTable']", self.getElement())[0]).DataTable();
    var data = [];
    for (var i = 0; i < entries.length; i++) {
      var entry = entries[i];
      if (entry.getType() === "OVERLAY") {
        var name = entry.getData().name;

        var row = [];
        row[0] = entry.getFilename();
        row[1] = "<input data='" + entry.getFilename() + "' name='overlayName' value='" + name + "'/>";
        row[2] = "<input data='" + entry.getFilename() + "' name='overlayDescription' value='" + entry.getData().description + "'/>";
        data.push(row);
      }
    }
    dataTable.clear().rows.add(data).draw();
  });

  return result;
};

/**
 *
 * @returns {HTMLElement}
 */
AddProjectDialog.prototype.createGlyphsTabContent = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div",
    className: "minerva-project-glyphs-tab"
  });
  result.appendChild(self._createGlyphsTable());
  return result;
};

/**
 *
 * @returns {HTMLElement}
 * @private
 */
AddProjectDialog.prototype._createGlyphsTable = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  var glyphsTable = Functions.createElement({
    type: "table",
    name: "glyphsTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(glyphsTable);

  $(glyphsTable).DataTable({
    columns: [{
      title: 'File name'
    }]
  });

  self.addListener("onZipFileUpload", function () {
    var entries = self.getZipEntries();
    var dataTable = $($("[name='glyphsTable']", self.getElement())[0]).DataTable();
    var data = [];
    for (var i = 0; i < entries.length; i++) {
      var entry = entries[i];
      if (entry.getType() === "GLYPH") {
        var row = [];
        row[0] = entry.getFilename();
        data.push(row);
      }
    }
    dataTable.clear().rows.add(data).draw();
  });

  return result;
};

/**
 *
 * @returns {HTMLElement}
 */
AddProjectDialog.prototype.createSubmapsTabContent = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div",
    className: "minerva-project-submaps-tab",
    style: "margin-top:10px;"
  });
  var submapsTable = Functions.createElement({
    type: "table",
    name: "submapsTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(submapsTable);

  $(submapsTable).DataTable({
    columns: [{
      title: 'File name'
    }, {
      title: 'Name'
    }, {
      title: 'Map type'
    }]
  });

  $(submapsTable).on("input", "[name='submapName']", function () {
    var input = this;
    var filename = $(input).attr("data");
    self.getEntryByFilename(filename).getData().name = $(input).val();
  });

  $(submapsTable).on("change", "[name='submapType']", function () {
    var input = this;
    var filename = $(input).attr("data");
    var configuration = self.getConfiguration();
    var mapTypes = configuration.getMapTypes();

    var selectedId = $(input).val();

    var data = self.getEntryByFilename(filename).getData();
    data.type = undefined;
    data.root = false;
    data.mapping = false;
    for (var j = 0; j < mapTypes.length; j++) {
      var mapType = mapTypes[j];
      if (mapType.id === selectedId) {
        data.type = mapType;
      }
    }
    if (selectedId === "ROOT") {
      data.root = true;
    }
    if (selectedId === "MAPPING") {
      data.mapping = true;
    }
  });

  self.addListener("onZipFileUpload", function () {
    var configuration = self.getConfiguration();
    var entries = self.getZipEntries();
    var dataTable = $($("[name='submapsTable']", self.getElement())[0]).DataTable();
    var data = [];
    for (var i = 0; i < entries.length; i++) {
      var entry = entries[i];
      if (entry.getType() === "MAP") {
        var row = [], selected;
        var typeSelect = "<select data='" + entry.getFilename() + "' name='submapType'>";

        var mapTypes = configuration.getMapTypes();
        for (var j = 0; j < mapTypes.length; j++) {
          var mapType = mapTypes[j];
          if (mapType === entry.getData().type) {
            selected = " selected";
          } else {
            selected = "";
          }
          typeSelect += "<option value='" + mapType.id + "' " + selected + ">" + mapType.name + "</option>";
        }

        if (entry.getData().root) {
          selected = " selected";
        } else {
          selected = "";
        }
        typeSelect += "<option value='ROOT' " + selected + ">ROOT</option>";

        if (entry.getData().mapping) {
          selected = " selected";
        } else {
          selected = "";
        }
        typeSelect += "<option value='MAPPING' " + selected + ">MAPPING</option>";

        typeSelect += "</select>";


        row[0] = entry.getFilename();
        row[1] = "<input data='" + entry.getFilename() + "' name='submapName' value='" + entry.getData().name + "' readonly disabled/>";
        row[2] = typeSelect;
        data.push(row);
      }
    }
    dataTable.clear().rows.add(data).draw();
  });

  return result;
};

/**
 *
 * @returns {HTMLElement}
 */
AddProjectDialog.prototype.createOverviewImagesTabContent = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;",
    className: "minerva-project-overview-images-tab"
  });

  var imagesTable = Functions.createElement({
    type: "table",
    name: "imagesTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(imagesTable);

  $(imagesTable).DataTable({
    columns: [{
      title: 'File name'
    }]
  });

  self.addListener("onZipFileUpload", function () {
    var entries = self.getZipEntries();
    var dataTable = $($("[name='imagesTable']", self.getElement())[0]).DataTable();
    var data = [];
    for (var i = 0; i < entries.length; i++) {
      var entry = entries[i];
      if (entry.getType() === "IMAGE") {
        var row = [];
        row[0] = entry.getFilename();
        data.push(row);
      }
    }
    dataTable.clear().rows.add(data).draw();
  });

  return result;
};

/**
 *
 * @returns {Promise}
 */
AddProjectDialog.prototype.init = function () {
  var self = this;
  var configuration = self.getConfiguration();
  var select = $("[name='project-format']", self.getElement())[0];
  self._fillProjectFormatSelectOptions(select, configuration);

  return self.getServerConnector().getLoggedUser().then(function (user) {
    self.bindProjectUploadPreferences(user, "annotateModel", "project-annotate-automatically");
    self.bindProjectUploadPreferences(user, "autoResize", "project-auto-margin");
    self.bindProjectUploadPreferences(user, "sbgn", "project-sbgn-visualization");
    return ServerConnector.getLicenses();
  }).then(function (licenses) {
    self._licenses = licenses;
    var select = $("[name='project-license']", self.getElement())[0];
    $(select).empty();
    select.appendChild(Functions.createElement({
      type: "option",
      content: "--OTHER--"
    }));
    for (var i = 0; i < licenses.length; i++) {
      var license = licenses[i];
      var option = Functions.createElement({
        type: "option",
        content: license.getName(),
        value: license.getId(),
        data: license.getContent()
      });
      select.appendChild(option);
    }
  });
};

/**
 *
 * @param {HTMLElement} select
 * @param {Configuration} configuration
 * @private
 */
AddProjectDialog.prototype._fillProjectFormatSelectOptions = function (select, configuration) {
  var option = Functions.createElement({
    type: "option",
    content: "---"
  });
  option.selected = true;
  select.appendChild(option);
  var converters = configuration.getModelConverters();
  converters.sort(function (a, b) {
    if (a.name < b.name) {
      return -1;
    }
    if (a.name > b.name) {
      return 1;
    }
    return 0;
  });
  for (var i = 0; i < converters.length; i++) {
    var converter = converters[i];
    option = Functions.createElement({
      type: "option",
      content: converter.name
    });
    $(option).data(converter.extensions);
    option.value = converter.handler;
    select.appendChild(option);
  }
};

/**
 *
 * @param {User} user
 * @param {string} type
 * @param {string} elementName
 */
AddProjectDialog.prototype.bindProjectUploadPreferences = function (user, type, elementName) {
  var element = $("[name='" + elementName + "']", this.getElement());

  var value = user.getPreferences().getProjectUpload()[type];
  element.prop('checked', value);

  element.change(function () {
    var data = new UserPreferences();
    data.getProjectUpload()[type] = element.is(":checked");
    GuiConnector.showProcessing();
    return ServerConnector.updateUserPreferences({
      user: user,
      preferences: data
    }).finally(GuiConnector.hideProcessing).catch(GuiConnector.alert);
  });
};

/**
 *
 * @return {Promise}
 */
AddProjectDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();
  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
  if (self._annotatorsDialog !== undefined) {
    self._annotatorsDialog.destroy();
  }
  var overlaysTable = $("[name=overlaysTable]", self.getElement())[0];
  if ($.fn.DataTable.isDataTable(overlaysTable)) {
    $(overlaysTable).DataTable().destroy();
  }
  var glyphsTable = $("[name=glyphsTable]", self.getElement())[0];
  if ($.fn.DataTable.isDataTable(glyphsTable)) {
    $(glyphsTable).DataTable().destroy();
  }
  var submapsTable = $("[name=submapsTable]", self.getElement())[0];
  if ($.fn.DataTable.isDataTable(submapsTable)) {
    $(submapsTable).DataTable().destroy();
  }

  var imagesTable = $("[name=imagesTable]", self.getElement())[0];
  if ($.fn.DataTable.isDataTable(imagesTable)) {
    $(imagesTable).DataTable().destroy();
  }
  return Promise.resolve();
};

/**
 *
 * @return {Promise<T>}
 */
AddProjectDialog.prototype.open = function () {
  var self = this;
  var div = self.getElement();
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      dialogClass: 'minerva-add-project-dialog',
      title: "ADD PROJECT",
      width: window.innerWidth / 2,
      height: window.innerHeight / 2,
      close: function () {
        $(".minerva-help-dialog > .ui-dialog-content").dialog('close');
      }
    });
  }
  $(div).dialog("open");
  return self.getServerConnector().getLoggedUser().then(function (user) {
    self.setNotifyEmail(user.getEmail());
    self.setProjectId("id");
    self.setName("NEW DISEASE MAP");
  });
};

/**
 *
 */
AddProjectDialog.prototype.close = function () {
  var self = this;
  $(self.getElement()).dialog("close");
};

/**
 *
 * @returns {Promise}
 */
AddProjectDialog.prototype.clear = function () {
  var self = this;
  $("input", self.getElement()).val("");
  self.setFileContent(undefined);
  return Promise.resolve();
};

/**
 *
 * @param file
 * @returns {Promise}
 */
AddProjectDialog.prototype.processFile = function (file) {
  var self = this;
  self.setFileContent(undefined);
  if (file) {
    return new Promise(function (resolve, reject) {
      var reader = new FileReader();
      reader.readAsArrayBuffer(file);
      reader.onload = function (evt) {
        try {
          self.setFileContent(evt.target.result);
          resolve(self.getFileContent());
        } catch (error) {
          reject(error);
        }
      };
      reader.onerror = function () {
        reject(new Error("Problem reading file"));
      };
    });
  } else {
    return Promise.resolve(null);
  }
};

/**
 *
 * @param {string|undefined} fileContent
 */
AddProjectDialog.prototype.setFileContent = function (fileContent) {
  this._fileContent = fileContent;
};

/**
 *
 * @returns {string|undefined}
 */
AddProjectDialog.prototype.getFileContent = function () {
  return this._fileContent;
};

/**
 *
 * @param {string} projectId
 */
AddProjectDialog.prototype.setProjectId = function (projectId) {
  $("[name='project-id']", this.getElement()).val(projectId);
};

/**
 *
 * @returns {?License}
 */
AddProjectDialog.prototype.getLicense = function () {
  var self = this;
  var id = parseInt($("[name='project-license']", this.getElement()).val());
  for (var i = 0; i < self._licenses.length; i++) {
    var license = self._licenses[i];
    if (license.getId() === id) {
      return license;
    }
  }
  return null;
};


/**
 *
 * @returns {string}
 */
AddProjectDialog.prototype.getProjectId = function () {
  return $("[name='project-id']", this.getElement()).val();
};

/**
 *
 * @param {string} name
 */
AddProjectDialog.prototype.setName = function (name) {
  $("[name='project-name']", this.getElement()).val(name);
};

/**
 *
 * @param {string} email
 */
AddProjectDialog.prototype.setNotifyEmail = function (email) {
  $("[name='project-notify-email']", this.getElement()).val(email);
};

/**
 *
 * @returns {string}
 */
AddProjectDialog.prototype.getName = function () {
  return $("[name='project-name']", this.getElement()).val();
};

/**
 *
 * @returns {string}
 */
AddProjectDialog.prototype.getDisease = function () {
  return $("[name='project-disease']", this.getElement()).val();
};

/**
 *
 * @returns {number}
 */
AddProjectDialog.prototype.getOrganism = function () {
  return $("[name='project-organism']", this.getElement()).val();
};

/**
 *
 * @param {string} organism
 */
AddProjectDialog.prototype.setOrganism = function (organism) {
  $("[name='project-organism']", this.getElement()).val(organism);
};

/**
 *
 * @returns {string}
 */
AddProjectDialog.prototype.getVersion = function () {
  return $("[name='project-version']", this.getElement()).val();
};

/**
 *
 * @returns {string}
 */
AddProjectDialog.prototype.getCustomLicenseName = function () {
  return $("[name='project-custom-license-name']", this.getElement()).val();
};

/**
 *
 * @returns {string}
 */
AddProjectDialog.prototype.getCustomLicenseUrl = function () {
  return $("[name='project-custom-license-url']", this.getElement()).val();
};

/**
 *
 * @returns {string}
 */
AddProjectDialog.prototype.getNotifyEmail = function () {
  return $("[name='project-notify-email']", this.getElement()).val();
};

/**
 *
 * @returns {boolean}
 */
AddProjectDialog.prototype.isAnnotateAutomatically = function () {
  return $("[name='project-annotate-automatically']", this.getElement()).is(':checked');
};

/**
 *
 * @returns {boolean}
 */
AddProjectDialog.prototype.isAutoMargin = function () {
  return $("[name='project-auto-margin']", this.getElement()).is(':checked');
};

/**
 *
 * @returns {boolean}
 */
AddProjectDialog.prototype.isSbgn = function () {
  return $("[name='project-sbgn-visualization']", this.getElement()).is(':checked');
};

/**
 *
 * @param {boolean} value
 */
AddProjectDialog.prototype.setSbgn = function (value) {
  return $("[name='project-sbgn-visualization']", this.getElement()).prop('checked', value).trigger("change");
};

/**
 *
 * @param {string} filename
 * @param {string} content
 */
AddProjectDialog.prototype.setFileParserForFilename = function (filename, content) {
  var self = this;
  self._filename = filename;
  var select = $("[name='project-format']", self.getElement())[0];
  var options = select.options;
  var optionId = 0;

  var isCellDesigner = self.isCellDesignerContent(content);

  for (var i = 0; i < options.length; i++) {
    var option = options[i];
    if (option.value !== undefined) {
      var extensions = $(option).data();
      for (var j in extensions) {
        if (extensions.hasOwnProperty(j)) {
          var extension = extensions[j];
          if (filename.endsWith(extension) && optionId <= 0) {
            if (extension === "xml") { //for xml we need to check if it's CellDesigner option
              if (isCellDesigner) {
                optionId = i;
              } else if (option.innerHTML.indexOf("CellDesigner") === -1) {
                optionId = i;
              }
            } else {
              optionId = i;
            }
          }
        }
      }
    }
  }
  options[optionId].selected = true;
};

/**
 *
 * @param {string} content
 * @return {boolean}
 */
AddProjectDialog.prototype.isCellDesignerContent = function (content) {
  return content.indexOf("xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\"") >= 0
    && content.indexOf("<celldesigner:extension>") >= 0;
};

/**
 *
 * @returns {*}
 */
AddProjectDialog.prototype.getConverter = function () {
  var self = this;
  var select = $("[name='project-format']", self.getElement())[0];
  var configuration = self.getConfiguration();
  var handler = select.options[select.selectedIndex].value;
  var converters = configuration.getModelConverters();
  for (var i = 0; i < converters.length; i++) {
    var converter = converters[i];
    if (handler === converter.handler) {
      return converter;
    }
  }
  return null;
};

/**
 *
 * @returns {Promise<Project>}
 */
AddProjectDialog.prototype.onSaveClicked = function () {
  var self = this;
  var parserClass;
  return self.checkValidity().then(function () {
    return self.getConverter();
  }).then(function (converter) {
    if (converter !== null) {
      parserClass = converter.handler;
    }
    GuiConnector.showProcessing("Uploading...");
    return self.getServerConnector().uploadFile({filename: self._filename, content: self.getFileContent()});
  }).then(function (file) {
    var license = self.getLicense();

    var licenseId = null;
    if (license != null) {
      licenseId = license.getId();
    }
    var options = {
      "projectId": self.getProjectId(),
      "name": self.getName(),
      "parser": parserClass,
      "file-id": file.id,
      "auto-resize": self.isAutoMargin(),
      "notify-email": self.getNotifyEmail(),
      "disease": self.getDisease(),
      "version": self.getVersion(),
      "organism": self.getOrganism(),
      "license-id": licenseId,
      'custom-license-name': self.getCustomLicenseName(),
      'custom-license-url': self.getCustomLicenseUrl(),
      "sbgn": self.isSbgn(),
      "annotate": self.isAnnotateAutomatically(),
      "zip-entries": self.getZipEntries()
    };
    return self.getServerConnector().addProject(options);
  }).catch(function (error) {
    if (error instanceof ObjectExistsError) {
      return Promise.reject(new ValidationError("Project with given id already exists"));
    }

    return Promise.reject(error);
  }).then(function (project) {
    return self.callListeners("onProjectAdd", project);
  }).finally(function () {
    GuiConnector.hideProcessing();
  });
};

/**
 *
 * @returns {Promise}
 */
AddProjectDialog.prototype.checkValidity = function () {
  var self = this;
  var isValid = true;
  var error = "<b>There is problem with input data.</b><ul>";

  if (self.getFileContent() === undefined) {
    error += "<li>Please select file before uploading</li>";
    isValid = false;
  }
  var converter = self.getConverter();
  if (converter === null) {
    error += "<li>Please select converter before uploading</li>";
    isValid = false;
  }
  var projectId = self.getProjectId();
  if (!(/^[a-z0-9A-Z\-_]+$/.test(projectId))) {
    error += "<li>projectId can contain only alphanumeric characters and -_</li>";
    isValid = false;
  }
  if (projectId.length > 255) {
    error += "<li>projectId must be shorter than 256 characters</li>";
    isValid = false;
  }

  var name = self.getName();
  if (name.length > 255) {
    error += "<li>name must be shorter than 256 characters</li>";
    isValid = false;
  }

  if (self.getVersion().length > 20) {
    error += "<li>version must be shorter than 20 characters</li>";
    isValid = false;
  }

  var rootExist = 0, i;
  for (i = 0; i < self.getZipEntries().length; i++) {
    if (self.getZipEntries()[i].getData().root) {
      rootExist++;
    }
  }
  if (self.getZipEntries().length > 0 && rootExist !== 1) {
    error += "<li>one root map must be selected</li>";
    isValid = false;
  }

  var mappingExist = 0;
  for (i = 0; i < self.getZipEntries().length; i++) {
    var entry = self.getZipEntries()[i];
    if (entry.getData().mapping) {
      mappingExist++;
    }
    if (entry.getType() === "OVERLAY") {
      if (entry.getData().name === "" || entry.getData().name === undefined) {
        error += "<li>overlay name cannot be empty</li>";
        isValid = false;
      }
    }
  }
  if (mappingExist > 1) {
    error += "<li>only one mapping map can be selected</li>";
    isValid = false;
  }

  GuiConnector.showProcessing();
  return Promise.resolve().then(function () {
    if (self.getDisease() !== "") {
      return self.getServerConnector().getMesh({id: self.getDisease()}).catch(function () {
        error += "<li>Invalid mesh id: " + self.getDisease() + "</li>";
        isValid = false;
      });
    }
  }).then(function () {
    if (self.getOrganism() !== "") {
      return self.getServerConnector().getTaxonomy({id: self.getOrganism()}).catch(function () {
        error += "<li>Invalid taxonomy id: " + self.getOrganism() + "</li>";
        isValid = false;
      });
    }
  }).then(function () {
    error += "</ul>";
    if (isValid) {
      return Promise.resolve(true);
    } else {
      return Promise.reject(new ValidationError(error));
    }
  }).finally(GuiConnector.hideProcessing);
}
;

/**
 *
 * @param file
 * @returns {Promise}
 */
AddProjectDialog.prototype.setZipFileContent = function (file) {
  var self = this;
  if (file.name.toLowerCase().endsWith("zip")) {
    var jsZip = new JSZip();
    return jsZip.loadAsync(file).then(function (zip) {
      var files = zip.files;
      var promises = [];
      for (var key in files) {
        if (files.hasOwnProperty(key)) {
          promises.push(self.createZipEntry(files[key], zip));
        }
      }
      return Promise.all(promises);
    }).then(function (result) {
      var entries = [];
      var overlays = 0;
      var glyphs = 0;
      var maps = 0;
      var images = 0;

      for (var i = 0; i < result.length; i++) {
        var entry = result[i];
        if (entry !== null) {
          entries.push(entry);
          if (entry.getType() === 'MAP') {
            maps++;
          } else if (entry.getType() === 'IMAGE') {
            images++;
          } else if (entry.getType() === 'OVERLAY') {
            overlays++;
          } else if (entry.getType() === 'GLYPH') {
            glyphs++;
          }
        }
      }
      if (overlays > 0) {
        guiUtils.showTab(self, $(".minerva-project-overlays-tab", self.getElement())[0]);
      } else {
        guiUtils.hideTab(self, $(".minerva-project-overlays-tab", self.getElement())[0]);
      }
      if (glyphs > 0) {
        guiUtils.showTab(self, $(".minerva-project-glyphs-tab", self.getElement())[0]);
      } else {
        guiUtils.hideTab(self, $(".minerva-project-glyphs-tab", self.getElement())[0]);
      }
      if (maps > 1) {
        guiUtils.showTab(self, $(".minerva-project-submaps-tab", self.getElement())[0]);
      } else {
        guiUtils.hideTab(self, $(".minerva-project-submaps-tab", self.getElement())[0]);
      }
      if (images > 1) {
        guiUtils.showTab(self, $(".minerva-project-overview-images-tab", self.getElement())[0]);
      } else {
        guiUtils.hideTab(self, $(".minerva-project-overview-images-tab", self.getElement())[0]);
      }
      if (maps === 0) {
        throw new ValidationError("Cannot find valid map file in the archive");
      }

      return self.setZipEntries(entries).then(function () {
        self._filename = file.name;
      });
    });
  } else {
    guiUtils.hideTab(self, $(".minerva-project-overlays-tab", self.getElement())[0]);
    guiUtils.hideTab(self, $(".minerva-project-submaps-tab", self.getElement())[0]);
    guiUtils.hideTab(self, $(".minerva-project-overview-images-tab", self.getElement())[0]);
    guiUtils.hideTab(self, $(".minerva-project-glyphs-tab", self.getElement())[0]);
    return self.setZipEntries([]);
  }
};

/**
 *
 * @param jsZipEntry
 * @param zipObject
 * @returns {Promise}
 */
AddProjectDialog.prototype.createZipEntry = function (jsZipEntry, zipObject) {
  var self = this;
  if (jsZipEntry.dir) {
    return null;
  }
  var filename = jsZipEntry.name;
  var lowercaseFilename = jsZipEntry.name.toLowerCase();
  var type;
  var data = {};
  var processingPromise = Promise.resolve();
  if (this.isIgnoredZipEntry(lowercaseFilename)) {
    type = undefined;
  } else if (lowercaseFilename.indexOf("submaps/") >= 0 || lowercaseFilename.indexOf("submaps\\") >= 0) {
    type = "MAP";
    if (lowercaseFilename.endsWith("mapping.xml")) {
      data.mapping = true;
    }
  } else if (lowercaseFilename.indexOf("images/") >= 0 || lowercaseFilename.indexOf("images\\") >= 0) {
    type = "IMAGE";
  } else if (lowercaseFilename.indexOf("glyphs/") >= 0 || lowercaseFilename.indexOf("glyphs\\") >= 0) {
    type = "GLYPH";
  } else if (lowercaseFilename.indexOf("layouts/") >= 0 || lowercaseFilename.indexOf("overlays/") >= 0
    || lowercaseFilename.indexOf("layouts\\") >= 0 || lowercaseFilename.indexOf("overlays\\") >= 0) {
    type = "OVERLAY";
    processingPromise = zipObject.file(jsZipEntry.name).async("string").then(function (content) {
      var overlayParser = new OverlayParser();
      var overlay = overlayParser.parse(content);
      if (overlay.getName()) {
        data.name = overlay.getName();
      } else {
        data.name = filename.substring(0, filename.lastIndexOf('.'));
        if (data.name.indexOf("/") >= 0) {
          data.name = data.name.substring(data.name.lastIndexOf("/") + 1);
        }
      }
      if (overlay.getDescription()) {
        data.description = overlay.getDescription();
      } else {
        data.description = "";
      }
    });
  } else if ((lowercaseFilename.match(/\/\\/g) || []).length <= 1) {
    type = "MAP";
    data.root = true;
  } else {
    var version = self.getConfiguration().getVersion();
    var minorVersion = version.split(".")[0] + "." + version.split(".")[1];

    var url = 'https://minerva.pages.uni.lu/doc/admin_manual/v' + minorVersion + '/advanced_upload/';
    throw new ValidationError("Unrecognized file: " + filename + ". Please refer to <a target='#' href='" + url +
      "'>manual</a> for information about allowed zip file content");
  }
  if (type === "MAP") {
    var name = filename;

    processingPromise = zipObject.file(jsZipEntry.name).async("string").then(function (content) {
      self.setFileParserForFilename(name, content);
      if (name.indexOf(".") > 0) {
        name = name.substr(0, name.indexOf("."));
      }
      if (name.lastIndexOf("\\") >= 0) {
        name = name.substr(name.lastIndexOf("\\") + 1);
      }
      if (name.lastIndexOf("/") >= 0) {
        name = name.substr(name.lastIndexOf("/") + 1);
      }
      data.name = name;
    }).then(function () {
      if (!data.root && !data.mapping) {
        var configuration = self.getConfiguration();
        var mapTypes = configuration.getMapTypes();
        for (var i = 0; i < mapTypes.length; i++) {
          if (mapTypes[i].id === "UNKNOWN") {
            data.type = mapTypes[i];
          }
        }
        if (data.type === undefined) {
          data.type = mapTypes[0];
        }
      }
    });
  }

  var extensions = self.getValidExtensions();

  return processingPromise.then(function () {
    var extension = filename.toLowerCase().substring(filename.lastIndexOf(".") + 1);
    if (type !== undefined && (type !== "MAP" || extensions.indexOf(extension) >= 0)) {
      return new ZipEntry({filename: filename, type: type, data: data});
    } else {
      return null;
    }
  });
};

AddProjectDialog.prototype.getValidExtensions = function () {
  var configuration = this.getConfiguration();
  var converters = configuration.getModelConverters();
  var extensions = [];
  for (var i = 0; i < converters.length; i++) {
    var converter = converters[i];
    extensions = extensions.concat(converter.extensions);
  }
  return extensions;
};

/**
 *
 * @returns {ZipEntry[]}
 */
AddProjectDialog.prototype.getZipEntries = function () {
  return this._zipEntries;
};

/**
 *
 * @param entries
 * @returns {PromiseLike|Promise}
 */
AddProjectDialog.prototype.setZipEntries = function (entries) {
  var self = this;
  self._zipEntries = entries;
  return self.callListeners("onZipFileUpload", entries);
};

/**
 *
 * @param {string} filename
 * @returns {boolean}
 */
AddProjectDialog.prototype.isIgnoredZipEntry = function (filename) {
  var lowercaseString = filename.toLowerCase();
  // noinspection SpellCheckingInspection
  if (lowercaseString.indexOf("__macosx") === 0) {
    return true;
  } else if (lowercaseString.indexOf(".ds_store") === 0 || lowercaseString.endsWith(".ds_store")) {
    return true;
  }
  return false;
};

/**
 *
 * @param filename
 * @returns {ZipEntry}
 */
AddProjectDialog.prototype.getEntryByFilename = function (filename) {
  var self = this;
  var entries = self.getZipEntries();
  for (var i = 0; i < entries.length; i++) {
    var entry = entries[i];
    if (entry.getFilename() === filename) {
      return entry;
    }
  }
  return null;
};

/**
 *
 * @returns {string}
 */
AddProjectDialog.prototype.getFilename = function () {
  return this._filename;
};

module.exports = AddProjectDialog;
