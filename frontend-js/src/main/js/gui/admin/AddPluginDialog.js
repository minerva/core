"use strict";

var Promise = require("bluebird");
var $ = require('jquery');
var md5 = require('md5');

var AbstractGuiElement = require('../AbstractGuiElement');
var GuiConnector = require('../../GuiConnector');
var NetworkError = require('../../NetworkError');
var ValidationError = require('../../ValidationError');

var Functions = require('../../Functions');
var PluginData = require('../../map/data/PluginData');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
var ConfigurationType = require("../../ConfigurationType");

var mockMinervaPluginInterface = require('./mockMinervaPluginInterface');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {Configuration} params.configuration
 * @param {ServerConnector} params.serverConnector

 * @constructor
 *
 * @extends AbstractGuiElement
 */
function AddPluginDialog(params) {
  params["customMap"] = null;
  AbstractGuiElement.call(this, params);
  var self = this;

  $(self.getElement()).css({overflow: "hidden"});

  self.createGui();
  self.registerListenerType("onSave");
}

AddPluginDialog.prototype = Object.create(AbstractGuiElement.prototype);
AddPluginDialog.prototype.constructor = AddPluginDialog;

/**
 *
 */
AddPluginDialog.prototype.createGui = function () {
  var self = this;

  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  var table = Functions.createElement({
    type: "table",
    name: "detailsTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(table);


  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });
  result.appendChild(menuRow);

  var saveUserButton = Functions.createElement({
    type: "button",
    name: "savePlugin",
    content: '<span class="ui-icon ui-icon-disk"></span>&nbsp;SAVE',
    onclick: function () {
      return self.onSaveClicked().then(function () {
        return self.close();
      }).catch(GuiConnector.alert);
    },
    xss: false
  });
  $(saveUserButton).attr("disabled", true);
  var cancelButton = Functions.createElement({
    type: "button",
    name: "cancelGenome",
    content: '<span class="ui-icon ui-icon-cancel"></span>&nbsp;CANCEL',
    onclick: function () {
      return self.close();
    },
    xss: false
  });
  menuRow.appendChild(saveUserButton);
  menuRow.appendChild(cancelButton);

  $(self.getElement()).on("click", "[name='validateUrl']", function () {
    return self.onValidateClicked().catch(GuiConnector.alert);
  });
  $(self.getElement()).on("input", "[name='pluginUrl']", function () {
    $('[name="savePlugin"]', self.getElement()).attr("disabled", true);
  });

  return self.getElement().appendChild(result);
};

/**
 *
 * @returns {PluginData}
 */
AddPluginDialog.prototype.getPluginData = function () {
  return this._pluginData;
};

/**
 *
 * @param {PluginData} pluginData
 */
AddPluginDialog.prototype.setPluginData = function (pluginData) {
  this._pluginData = pluginData;
};

/**
 *
 * @returns {Promise}
 */
AddPluginDialog.prototype.onSaveClicked = function () {
  var self = this;
  var pluginData = self.getPluginData();
  if (pluginData === undefined) {
    return Promise.reject(new Error("You must validate the url first"));
  } else {
    return self.getServerConnector().registerPlugin(pluginData).then(function () {
      return self.callListeners("onSave");
    });
  }
};

/**
 *
 * @returns {Promise|PromiseLike}
 */
AddPluginDialog.prototype.onValidateClicked = function () {
  var self = this;
  var url = $("[name='pluginUrl']").val();
  var error;

  var pluginData = {
    pluginRawNewData: {}
  }

  return self.getServerConnector().sendRequest({
    url: url,
    description: "Loading plugin: " + url,
    method: "GET"
  }).then(function (content) {
    var hash = md5(content);
    var pluginRawData = undefined;
    // noinspection JSUnusedLocalSymbols
    var minervaDefine = function (pluginFunction) {
      try {
        if (typeof pluginFunction === "function") {
          pluginRawData = pluginFunction();
        } else {
          pluginRawData = pluginFunction;
        }
      } catch (e) {
        error = e;
      }
    };
    var minerva = mockMinervaPluginInterface(pluginData, self.getConfiguration(), self.getServerConnector());
    content += "//# sourceURL=" + url;
    try {
      eval(content);
    } catch (e) {
      logger.warn("Problem with loading of a plugin", e);
      error = new ValidationError("Plugin could not be loaded. Verify if the file is a valid minerva plugin.", e);
    }
    if (error) {
      return Promise.reject(error);
    }
    if (pluginRawData) {
      $("[name='pluginVersion']").val(pluginRawData.getVersion());
      $("[name='pluginName']").val(pluginRawData.getName());
      self.setPluginData(new PluginData({
        hash: hash,
        urls: [url],
        name: pluginRawData.getName(),
        version: pluginRawData.getVersion(),
        isPublic: true
      }));
    } else {
      $("[name='pluginVersion']").val(pluginData.pluginRawNewData.pluginVersion);
      $("[name='pluginName']").val(pluginData.pluginRawNewData.pluginName);
      self.setPluginData(new PluginData({
        hash: hash,
        urls: [url],
        name: pluginData.pluginRawNewData.pluginName,
        version: pluginData.pluginRawNewData.pluginVersion,
        isPublic: true
      }));
    }
    return self.getServerConnector().getPluginsData();
  }).then(function (plugins) {
    for (var i = 0; i < plugins.length; i++) {
      var pluginData = plugins[i];
      if (pluginData.getUrls().indexOf(url) >= 0 && pluginData.isPublic()) {
        throw new ValidationError("Plugin is already registered");
      }
    }
    var saveUserButton = $('[name="savePlugin"]');
    saveUserButton.attr("disabled", false);
  }).catch(function (e) {
    if (e instanceof NetworkError) {
      logger.warn(e);
      throw new ValidationError("Problem with accessing plugin file (url: '" + url + "')");
    } else {
      throw e;
    }
  });
};

/**
 *
 * @returns {Promise}
 */
AddPluginDialog.prototype.init = function () {
  var self = this;
  var detailsTable = $("[name=detailsTable]", self.getElement())[0];

  // noinspection JSCheckFunctionSignatures
  var dataTable = $(detailsTable).DataTable({
    columns: [{
      title: "Name"
    }, {
      title: "Value"
    }],
    paging: false,
    ordering: false,
    searching: false,
    bInfo: false
  });

  var data = [];

  data.push(['Url', "<div><input type='text' name='pluginUrl'/><button name='validateUrl'><span class='ui-icon ui-icon-circle-check'></span>&nbsp;VALIDATE</button></div>"]);
  data.push(['Name', "<input name='pluginName' readonly/>"]);
  data.push(['Version', "<input name='pluginVersion' readonly/>"]);

  dataTable.clear().rows.add(data).draw();

  return Promise.resolve();
};

/**
 *
 *
 * @returns {Promise}
 */
AddPluginDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();

  var detailsTable = $("[name=detailsTable]", div)[0];
  if ($.fn.DataTable.isDataTable(detailsTable)) {
    $(detailsTable).DataTable().destroy();
  }
  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
  return Promise.resolve();
};

/**
 *
 */
AddPluginDialog.prototype.open = function () {
  var self = this;
  var div = self.getElement();
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      dialogClass: 'minerva-add-plugin-dialog',
      title: "Add plugin",
      width: window.innerWidth / 2,
      height: window.innerHeight / 2
    });
  }
  self.clear();
  $(div).dialog("open");
};

AddPluginDialog.prototype.clear = function () {
  var self = this;
  $("[name='pluginUrl']", self.getElement()).val("");
  $("[name='pluginName']", self.getElement()).val("");
  $("[name='pluginVersion']", self.getElement()).val("");
  $('[name="savePlugin"]', self.getElement()).attr("disabled", true);
};

/**
 *
 */
AddPluginDialog.prototype.close = function () {
  var self = this;
  $(self.getElement()).dialog("close");
};

module.exports = AddPluginDialog;
