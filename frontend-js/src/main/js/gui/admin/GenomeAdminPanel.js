"use strict";

var $ = require('jquery');

var AbstractAdminPanel = require('./AbstractAdminPanel');
var EditGenomeDialog = require('./EditGenomeDialog');

var Functions = require('../../Functions');
var GuiConnector = require('../../GuiConnector');
var PrivilegeType = require('../../map/data/PrivilegeType');
var ReferenceGenome = require('../../map/data/ReferenceGenome');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var Promise = require('bluebird');

/**
 *
 * @param {Configuration} params.configuration
 * @param {HTMLElement} params.element
 *
 * @constructor
 */
function GenomeAdminPanel(params) {
  params["panelName"] = "genomes";
  AbstractAdminPanel.call(this, params);

  var self = this;
  $(self.getElement()).addClass("minerva-genome-tab");

  self._createGui();
}

GenomeAdminPanel.prototype = Object.create(AbstractAdminPanel.prototype);
GenomeAdminPanel.prototype.constructor = GenomeAdminPanel;

GenomeAdminPanel.AUTO_REFRESH_TIME = 5000;

/**
 *
 * @private
 */
GenomeAdminPanel.prototype._createGui = function () {
  var self = this;
  var genomeDiv = Functions.createElement({
    type: "div"
  });
  self.getElement().appendChild(genomeDiv);

  genomeDiv.appendChild(self._createMenuRow());

  var genomesTable = Functions.createElement({
    type: "table",
    name: "genomeTable",
    className: "display",
    style: "width:100%"
  });
  genomeDiv.appendChild(genomesTable);

  // noinspection JSUnusedGlobalSymbols
  $(genomesTable).DataTable({
    columns: [{
      title: 'Type'
    }, {
      title: 'Organism'
    }, {
      title: 'Version'
    }, {
      title: 'Progress'
    }, {
      title: 'Source'
    }, {
      title: 'Edit',
      orderable: false
    }, {
      title: 'Remove',
      orderable: false
    }],
    columnDefs: [{
      targets: [0],
      orderData: [0, 4]
    }, {
      targets: [1],
      orderData: [1, 4]
    }, {
      targets: [2],
      orderData: [2, 4]
    }, {
      targets: [3],
      orderData: [3, 4]
    }],
    order: [[1, "asc"], [4, "asc"]]
  });
  self.bindDataTablePageLengthToUserPreference({
    element: genomesTable,
    preferenceName: 'admin-genome-datatable-length'
  });

  $(genomesTable).on("click", "[name='removeGenome']", function () {
    var button = this;
    return self.askConfirmRemoval({
      title: "INFO",
      content: "Do you really want to remove this genome?",
      input: false
    }).then(function (param) {
      if (param.status) {
        return self.getServerConnector().removeReferenceGenome({genomeId: $(button).attr("data")});
      }
    }).then(function () {
      return self.onRefreshClicked();
    }).catch(GuiConnector.alert);
  });

  $(genomesTable).on("click", "[name='editGenome']", function () {
    var button = this;
    return self.showEditDialog(parseInt($(button).attr("data"))).catch(GuiConnector.alert);
  });

  genomeDiv.appendChild(self._createMenuRow());

};

/**
 *
 * @returns {HTMLElement}
 * @private
 */
GenomeAdminPanel.prototype._createMenuRow = function () {
  var self = this;
  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });

  var addProjectButton = Functions.createElement({
    type: "button",
    name: "addGenome",
    content: '<span class="ui-icon ui-icon-circle-plus"></span>&nbsp;ADD GENOME',
    onclick: function () {
      return self.onAddClicked().catch(GuiConnector.alert);
    },
    xss: false
  });
  var refreshButton = Functions.createElement({
    type: "button",
    name: "refreshGenomes",
    content: '<span class="ui-icon ui-icon-refresh"></span>&nbsp;REFRESH',
    onclick: function () {
      return self.onRefreshClicked().catch(GuiConnector.alert);
    },
    xss: false
  });
  menuRow.appendChild(addProjectButton);
  menuRow.appendChild(refreshButton);
  return menuRow;
};

/**
 *
 * @returns {Promise}
 */
GenomeAdminPanel.prototype.init = function () {
  var self = this;
  return AbstractAdminPanel.prototype.init.call(this).then(function () {
    return self.getServerConnector().getLoggedUser();
  }).then(function (user) {
    if (user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN))) {
      return self.onRefreshClicked();
    } else {
      self.disablePanel("You have no privilege to manage genomes");
    }

  }).then(function () {
    var genomesTable = $("[name='genomeTable']", self.getElement())[0];

    return self.bindDataTableOrderToUserPreference({
      element: genomesTable,
      preferenceName: 'admin-genome-datatable-order'
    });
  });
};

/**
 *
 * @returns {Promise}
 */
GenomeAdminPanel.prototype.onRefreshClicked = function () {
  var self = this;
  return self.getServerConnector().getReferenceGenomes().then(function (referenceGenomes) {
    return self.setReferenceGenomes(referenceGenomes);
  });
};

/**
 *
 * @param {ReferenceGenome[]} referenceGenomes
 *
 * @returns {Promise}
 *
 */
GenomeAdminPanel.prototype.setReferenceGenomes = function (referenceGenomes) {
  var self = this;

  return self.getServerConnector().getLoggedUser().then(function (user) {
    var requireUpdate = false;

    var dataTable = $("[name='genomeTable']", self.getElement()).DataTable();
    var data = [];
    var page = dataTable.page();

    for (var i = 0; i < referenceGenomes.length; i++) {
      var genome = referenceGenomes[i];
      var rowData = self.genomeToTableRow(genome, user);
      data.push(rowData);
      if (genome.getDownloadProgressStatus() !== "READY" &&
        genome.getDownloadProgressStatus() !== "ERROR" &&
        genome.getDownloadProgressStatus() !== "N/A") {
        requireUpdate = true;
      }

    }
    //it should be simplified, but I couldn't make it work
    dataTable.clear().rows.add(data).page(page).draw(false).page(page).draw(false);

    if (requireUpdate) {
      setTimeout(function () {
        logger.debug("Genomes auto refresh");
        return self.onRefreshClicked();
      }, GenomeAdminPanel.AUTO_REFRESH_TIME);
    }

  });
};

/**
 *
 * @param {ReferenceGenome} genome
 * @param {User} user
 * @returns {Array}
 */
GenomeAdminPanel.prototype.genomeToTableRow = function (genome, user) {
  var self = this;
  var row = [];

  row[0] = genome.getType();
  row[1] = self.getGuiUtils().createAnnotationLink(genome.getOrganism()).outerHTML;
  row[2] = genome.getVersion();
  row[3] = genome.getDownloadProgressStatus();
  row[4] = genome.getSourceUrl();

  var disabled = " disabled ";
  if (user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN))) {
    disabled = "";
  }
  row[5] = "<button name='editGenome' data='" + genome.getId() + "'" + disabled + "><i class='fa fa-edit'></i></button>";
  row[6] = "<button name='removeGenome' data='" + genome.getId() + "'" + disabled + "><i class='fa fa-trash-alt'></button>";
  return row;
};


/**
 *
 * @returns {Promise}
 */
GenomeAdminPanel.prototype.destroy = function () {
  var self = this;
  var table = $("[name='genomeTable']", self.getElement())[0];
  if ($.fn.DataTable.isDataTable(table)) {
    $(table).DataTable().destroy();
  }
  var promises = [];
  for (var key in self._dialogs) {
    if (self._dialogs.hasOwnProperty(key)) {
      promises.push(self._dialogs[key].destroy());
    }
  }
  promises.push(AbstractAdminPanel.prototype.destroy.call(self));

  return Promise.all(promises);
};

/**
 *
 * @param {ReferenceGenome} [genome]
 * @returns {Promise<EditGenomeDialog>}
 */
GenomeAdminPanel.prototype.getDialog = function (genome) {
  var self = this;
  if (self._dialogs === undefined) {
    self._dialogs = [];
  }
  var id = genome.getId();
  var dialog = self._dialogs[id];
  if (dialog === undefined) {
    dialog = new EditGenomeDialog({
      element: Functions.createElement({
        type: "div"
      }),
      configuration: self.getConfiguration(),
      referenceGenome: genome,
      customMap: null,
      serverConnector: self.getServerConnector()
    });
    self._dialogs[id] = dialog;
    if (id === undefined) {
      dialog.addListener("onSave", function () {
        return self.onRefreshClicked();
      });
    }
    return dialog.init().then(function () {
      return dialog;
    });
  } else {
    return Promise.resolve(dialog);
  }
};

/**
 *
 * @param {number} id
 * @returns {Promise}
 */
GenomeAdminPanel.prototype.showEditDialog = function (id) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getServerConnector().getReferenceGenome({genomeId: id}).then(function (referenceGenome) {
    if (referenceGenome === null) {
      referenceGenome = new ReferenceGenome();
    }
    return self.getDialog(referenceGenome);
  }).then(function (dialog) {
    return dialog.open();
  }).finally(function () {
    GuiConnector.hideProcessing();
  });
};

/**
 *
 * @returns {Promise}
 */
GenomeAdminPanel.prototype.onAddClicked = function () {
  var self = this;
  GuiConnector.showProcessing();
  var referenceGenome = new ReferenceGenome();
  return self.getDialog(referenceGenome).then(function (dialog) {
    return dialog.open();
  }).finally(function () {
    GuiConnector.hideProcessing();
  });
};


module.exports = GenomeAdminPanel;
