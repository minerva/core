"use strict";

/* exported logger */
var Promise = require("bluebird");
var $ = require('jquery');

var AbstractGuiElement = require('../AbstractGuiElement');
var AddOverlayDialog = require('../AddOverlayDialog');
var Annotation = require('../../map/data/Annotation');
var CommentsTab = require('./CommentsAdminPanel');
var GuiConnector = require('../../GuiConnector');
var PrivilegeType = require('../../map/data/PrivilegeType');
var ValidationError = require("../../ValidationError");
var HttpStatus = require('http-status-codes');
var NetworkError = require('../../NetworkError');

var Functions = require('../../Functions');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var guiUtils = new (require('../leftPanel/GuiUtils'))();
var xss = require('xss');
var ConfigurationType = require("../../ConfigurationType");

/**
 *
 * @param params
 * @param {HTMLElement} params.element
 * @param {Configuration} params.configuration
 * @param {ServerConnector} params.serverConnector
 *
 * @constructor
 * @extends {AbstractGuiElement}
 */
function EditProjectDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  /**
   *
   * @type {string}
   * @private
   */
  self._title = self.getProject().getProjectId();

  guiUtils.setConfiguration(params.configuration);
  self.setConfiguration(params.configuration);
  $(self.getElement()).addClass("minerva-edit-project-dialog");
  $(self.getElement()).css({overflow: "hidden"});
  self.createGui();

  /**
   @name EditProjectDialog#_overlayById
   @type DataOverlay[]
   @private
   */
  self._overlayById = [];

  /**
   @name EditProjectDialog#_backgroundById
   @type Background[]
   @private
   */
  self._backgroundById = [];

  /**
   @name EditProjectDialog#_mapsById
   @type MapModel[]
   @private
   */
  self._mapsById = [];

  /**
   @name EditProjectDialog#_userByLogin
   @type User[]
   @private
   */
  self._userByLogin = [];

}

EditProjectDialog.prototype = Object.create(AbstractGuiElement.prototype);
EditProjectDialog.prototype.constructor = EditProjectDialog;

/**
 *
 */
EditProjectDialog.prototype.createGui = function () {
  var self = this;

  guiUtils.initTabContent(self);

  var archived = false;
  if (self.getProject().getStatus().toLowerCase() === "archived") {
    archived = true;
    guiUtils.addTab(self, {
      name: 'REVIVE',
      content: self.createReviveTabContent()
    });
  }

  guiUtils.addTab(self, {
    name: "GENERAL",
    content: self.createGeneralTabContent(),
    disabled: archived
  });

  guiUtils.addTab(self, {
    name: "BACKGROUNDS",
    content: self.createBackgroundsTabContent(),
    disabled: archived
  });

  guiUtils.addTab(self, {
    name: "OVERLAYS",
    content: self.createOverlaysTabContent(),
    disabled: archived
  });
  guiUtils.addTab(self, {
    name: "MAPS",
    content: self.createMapsTabContent(),
    disabled: archived
  });
  guiUtils.addTab(self, {
    name: "USERS",
    content: self.createUsersTabContent(),
    disabled: archived
  });

  guiUtils.addTab(self, {
    name: "COMMENTS",
    panelClass: CommentsTab,
    disabled: archived
  });
  self.setCommentsTab(self._panels[0]);

  if (!archived) {
    guiUtils.addTab(self, {
      name: 'ARCHIVE',
      content: self.createReviveTabContent()
    });
  }

};

/**
 *
 * @returns {HTMLElement}
 */
EditProjectDialog.prototype.createGeneralTabContent = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div",
    className: "minerva-project-general-tab"
  });

  var table = Functions.createElement({
    type: "div",
    style: "display:table"
  });
  result.appendChild(table);

  var projectIdRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(projectIdRow);
  projectIdRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "ProjectId"
  }));
  projectIdRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='projectId'/>",
    xss: false,
    onchange: function () {
      var project = self.getProject();
      return self.moveProject(project, $("[name='projectId']", this).val());
    }
  }));

  var licenseRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(licenseRow);

  licenseRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "License: "
  }));
  licenseRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: Functions.createElement({
      type: "select",
      name: "project-license",
      style: "width: 172px;"
    }),
    onchange: function () {
      var select = this;
      var project = self.getProject();
      var license = self.getLicense();
      project.setLicense(license);
      if (license !== null) {
        $("[name='project-license-show']", self.getElement()).show();
        $("[name='custom-license-name-row']", self.getElement()).hide();
        $("[name='custom-license-url-row']", self.getElement()).hide();
      } else {
        $("[name='project-license-show']", self.getElement()).hide();
        $("[name='custom-license-name-row']", self.getElement()).show();
        $("[name='custom-license-url-row']", self.getElement()).show();
      }
      return self.updateProject(project);
    }

  }));
  licenseRow.appendChild(Functions.createElement({
    type: "a",
    name: "project-license-show",
    content: "<i class=\"fa fa-solid fa-eye\"></i>",
    xss: false,
    href: "#",
    onclick: function () {
      var license = self.getLicense();
      if (license !== null) {
        GuiConnector.info("<a href='" + license.getUrl() + "'>link</a><br/>" + license.getContent());
      }
    }
  }));

  var customLicenseNameRow = Functions.createElement({
    type: "div",
    style: "display:table-row",
    name: "custom-license-name-row"
  });
  table.appendChild(customLicenseNameRow);
  customLicenseNameRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "License name"
  }));
  customLicenseNameRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='customLicenseName'/>",
    xss: false,
    onchange: function () {
      var project = self.getProject();
      project.setCustomLicenseName($("[name='customLicenseName']", this).val());
      return self.updateProject(project);
    }
  }));

  var customLicenseUrlRow = Functions.createElement({
    type: "div",
    style: "display:table-row",
    name: "custom-license-url-row"
  });
  table.appendChild(customLicenseUrlRow);
  customLicenseUrlRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "License url"
  }));
  customLicenseUrlRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='customLicenseUrl'/>",
    xss: false,
    onchange: function () {
      var project = self.getProject();
      project.setCustomLicenseUrl($("[name='customLicenseUrl']", this).val());
      return self.updateProject(project);
    }
  }));

  var nameRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(nameRow);
  nameRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "Name"
  }));
  nameRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='projectName'/>",
    xss: false,
    onchange: function () {
      var project = self.getProject();
      project.setName($("[name='projectName']", this).val());
      return self.updateProject(project);
    }
  }));

  var versionRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(versionRow);
  versionRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "Version"
  }));
  versionRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='projectVersion'/>",
    xss: false,
    onchange: function () {
      var project = self.getProject();
      project.setVersion($("[name='projectVersion']", this).val());
      return self.updateProject(project);
    }
  }));

  var diseaseRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(diseaseRow);
  diseaseRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "Disease"
  }));
  diseaseRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='projectDisease'/>",
    xss: false,
    onchange: function () {
      var project = self.getProject();
      project.setDisease(prepareMiriamData("MESH_2012", $("[name='projectDisease']", this).val()));
      return self.updateProject(project);
    }
  }));

  var organismRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(organismRow);
  organismRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "Organism"
  }));
  organismRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='projectOrganism'/>",
    xss: false,
    onchange: function () {
      var id = $("[name='projectOrganism']", this).val();
      var project = self.getProject();
      GuiConnector.showProcessing();
      return self.getServerConnector().getTaxonomy({id: id}).catch(function () {
        if (id !== "") {
          var newId = "";
          if (project.getOrganism() !== undefined) {
            newId = xss(project.getOrganism().getResource());
          }
          $("[name='projectOrganism']", this).val(newId);
          $("[name='projectOrganism']", this).focus();
          throw new ValidationError("Invalid taxonomy id: " + id);
        }
      }).then(function () {
        project.setOrganism(prepareMiriamData("TAXONOMY", id));
        return self.updateProject(project);
      }).catch(GuiConnector.alert).finally(GuiConnector.hideProcessing);
    }
  }));

  var emailRow = Functions.createElement({
    type: "div",
    style: "display:table-row"
  });
  table.appendChild(emailRow);
  emailRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "Notify email"
  }));
  emailRow.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell",
    content: "<input name='projectNotifyEmail'/>",
    xss: false,
    onchange: function () {
      var project = self.getProject();
      project.setNotifyEmail($("[name='projectNotifyEmail']", this).val());
      return self.updateProject(project);
    }
  }));

  return result;
};

/**
 *
 * @returns {HTMLElement}
 */
EditProjectDialog.prototype.createMapsTabContent = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div",
    className: "minerva-maps-tab"
  });
  result.appendChild(self._createMapsTable());
  return result;
};

EditProjectDialog.prototype._createMapsTable = function () {
  var self = this;

  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  var mapsTable = Functions.createElement({
    type: "table",
    name: "mapsTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(mapsTable);

  $(mapsTable).DataTable({
    fnRowCallback: function (nRow, aData) {
      nRow.setAttribute('id', "map-" + aData[0]);
    },
    columns: [{
      title: 'Id'
    }, {
      title: 'Name'
    }, {
      title: 'Default center x',
      orderable: false
    }, {
      title: 'Default center y',
      orderable: false
    }, {
      title: 'Default zoom level',
      orderable: false
    }]
  });

  $(mapsTable).on("change", "[name='defaultCenterX']", function () {
    var mapId = parseInt($(this).attr("data"));
    var map = self._mapsById[mapId];
    map.setDefaultCenterX(parseInt($(this).val()));
    return self.updateMap(map);
  });

  $(mapsTable).on("change", "[name='defaultCenterY']", function () {
    var mapId = parseInt($(this).attr("data"));
    var map = self._mapsById[mapId];
    map.setDefaultCenterY(parseInt($(this).val()));
    return self.updateMap(map);
  });

  $(mapsTable).on("change", "[name='defaultZoomLevel']", function () {
    var mapId = parseInt($(this).attr("data"));
    var map = self._mapsById[mapId];
    map.setDefaultZoomLevel(parseInt($(this).val()));
    return self.updateMap(map);
  });

  return result;
};

/**
 *
 * @returns {HTMLElement}
 */
EditProjectDialog.prototype.createOverlaysTabContent = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div",
    className: "minerva-project-overlays-tab"
  });
  result.appendChild(self._createOverlayTable());
  return result;
};

/**
 *
 * @returns {HTMLElement}
 */
EditProjectDialog.prototype.createBackgroundsTabContent = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div",
    className: "minerva-project-backgrounds-tab"
  });
  result.appendChild(self._createBackgroundsTable());
  return result;
};

/**
 *
 * @returns {HTMLElement}
 * @private
 */
EditProjectDialog.prototype._createOverlayTable = function () {
  var self = this;

  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  var overlaysTable = Functions.createElement({
    type: "table",
    name: "overlaysTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(overlaysTable);

  // noinspection JSUnusedGlobalSymbols
  $(overlaysTable).DataTable({
    fnRowCallback: function (nRow, aData) {
      nRow.setAttribute('id', "overlay-" + aData[0]);
    },
    columns: [{
      title: 'Id'
    }, {
      title: 'Name'
    }, {
      title: 'Description'
    }, {
      title: 'Public',
      orderable: false
    }, {
      title: 'Owner',
      orderable: false
    }, {
      title: 'Data',
      orderable: false
    }, {
      title: 'Remove',
      orderable: false
    }],
    columnDefs: [
      {
        orderDataType: "dom-input",
        type: "string",
        targets: [1, 2]
      }
    ],
    dom: '<"minerva-datatable-toolbar">frtip',
    initComplete: function () {
      $("div.minerva-datatable-toolbar", $(result)).html('<button name="addOverlay">Add overlay</button>');
    }

  });

  $(overlaysTable).on("click", "[name='removeOverlay']", function () {
    var button = this;
    var overlayId = parseInt($(button).attr("data"));
    var overlay = self._overlayById[overlayId];
    var confirmationMessage = "Do you want to delete overlay: " + overlay.getName() + "?";
    return GuiConnector.showConfirmationDialog({
      message: confirmationMessage
    }).then(function (confirmation) {
      if (confirmation) {
        $(button).attr("disabled", true);
        return self.removeOverlay(overlayId).catch(GuiConnector.alert);
      }
    });
  });

  $(overlaysTable).on("change", "[name='overlayName']", function () {
    var overlayId = parseInt($(this).attr("data"));
    var overlay = self._overlayById[overlayId];
    overlay.setName($(this).val());
    return self.updateOverlay(overlay);
  });

  $(overlaysTable).on("change", "[name='overlayDescription']", function () {
    var overlayId = parseInt($(this).attr("data"));
    var overlay = self._overlayById[overlayId];
    overlay.setDescription($(this).val());
    return self.updateOverlay(overlay);
  });

  $(overlaysTable).on("change", "[name='publicOverlay']", function () {
    var overlayId = parseInt($(this).attr("data"));
    var overlay = self._overlayById[overlayId];
    overlay.setPublicOverlay($(this).prop('checked'));
    return self.updateOverlay(overlay);
  });

  $(overlaysTable).on("change", "[name='creator']", function () {
    var overlayId = parseInt($(this).attr("data"));
    var overlay = self._overlayById[overlayId];
    var creator = $(this).val();
    if (creator === "") {
      creator = undefined;
    }
    //put it on the bottom of ordered list of data overlays for given user
    var order = 1;
    for (var key in self._overlayById) {
      if (self._overlayById.hasOwnProperty(key)) {
        var existingOverlay = self._overlayById[key];
        if (existingOverlay.getCreator() === creator) {
          if (existingOverlay.getId() !== overlayId) {
            order = Math.max(order, self._overlayById[key].getOrder() + 1);
          } else {
            order = Math.max(order, self._overlayById[key].getOrder());
          }
        }
      }
    }
    overlay.setOrder(order);

    overlay.setCreator($(this).val());
    return self.updateOverlay(overlay);
  });

  $(overlaysTable).on("click", "[name='downloadSource']", function () {
    var button = this;
    return self.getServerConnector().getOverlaySourceDownloadUrl({
      overlayId: $(button).attr("data"),
      projectId: self.getProject().getProjectId()
    }).then(function (url) {
      return self.downloadFile(url);
    }).then(null, GuiConnector.alert);
  });

  $(result).on("click", "[name='addOverlay']", function () {
    return self.openAddOverlayDialog();
  });

  return result;
};


/**
 *
 * @returns {HTMLElement}
 * @private
 */
EditProjectDialog.prototype._createBackgroundsTable = function () {
  var self = this;

  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  var backgroundsTable = Functions.createElement({
    type: "table",
    name: "backgroundsTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(backgroundsTable);

  // noinspection JSUnusedGlobalSymbols
  $(backgroundsTable).DataTable({
    fnRowCallback: function (nRow, aData) {
      nRow.setAttribute('id', "background-" + aData[0]);
    },
    columns: [{
      title: 'Id'
    }, {
      title: 'Name'
    }, {
      title: 'Description'
    }, {
      title: 'Default',
      orderable: false
    }, {
      title: 'Remove',
      orderable: false
    }],
    columnDefs: [
      {
        orderDataType: "dom-input",
        type: "string",
        targets: [1, 2]
      }
    ]
  });

  $(backgroundsTable).on("click", "[name='removeBackground']", function () {
    var button = this;
    var backgroundId = parseInt($(button).attr("data"));
    var background = self._backgroundById[backgroundId];
    var confirmationMessage = "Do you want to delete background: " + background.getName() + "?" +
      "<br/><span style='color:red;font-weight:bold'>Warning: This contains one of the map's " +
      "diagrams and is required<br/>" +
      "for map display. Removing it cannot be undone.</span>"

    return GuiConnector.showConfirmationDialog({
      message: confirmationMessage
    }).then(function (confirmation) {
      if (confirmation) {
        $(button).attr("disabled", true);
        return self.removeBackground(backgroundId).catch(GuiConnector.alert);
      }
    });
  });

  $(backgroundsTable).on("change", "[name='backgroundName']", function () {
    var backgroundId = parseInt($(this).attr("data"));
    var background = self._backgroundById[backgroundId];
    background.setName($(this).val());
    return self.updateBackground(background);
  });

  $(backgroundsTable).on("change", "[name='backgroundDescription']", function () {
    var backgroundId = parseInt($(this).attr("data"));
    var background = self._backgroundById[backgroundId];
    background.setDescription($(this).val());
    return self.updateBackground(background);
  });

  $(backgroundsTable).on("change", "[name='defaultBackground']", function () {
    var backgroundId = parseInt($(this).attr("data"));
    var background = self._backgroundById[backgroundId];
    var checked = $(this).prop('checked');
    background.setDefaultOverlay(checked);
    return self.updateBackground(background).then(function () {
      if (checked) {
        var checkboxes = $("[name='defaultBackground']", backgroundsTable);
        for (var i = 0; i < checkboxes.length; i++) {
          var checkbox = checkboxes[i];
          var otherBackgroundId = parseInt($(checkbox).attr("data"));
          if (otherBackgroundId !== backgroundId) {
            var otherChecked = $(checkbox).prop('checked');
            if (otherChecked) {
              $(checkbox).prop('checked', false).trigger('change');
            }
          }
        }
      }
    });
  });

  return result;
};

/**
 *
 * @returns {HTMLElement}
 */
EditProjectDialog.prototype.createUsersTabContent = function () {
  var self = this;

  var result = Functions.createElement({
    type: "div",
    className: "minerva-project-users-tab",
    style: "margin-top:10px;"
  });

  var usersTable = Functions.createElement({
    type: "table",
    name: "usersTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(usersTable);

  $(usersTable).on("change", "[name='privilege']", function () {
    var type = $(this).attr("data").split(",")[0];
    var login = $(this).attr("data").split(",")[1];
    if ($(this).prop("checked")) {
      if (type === PrivilegeType.WRITE_PROJECT) {
        var readCheckbox = $("[data='" + PrivilegeType.READ_PROJECT + "," + login + "']", usersTable);
        if (!readCheckbox.is(":checked")) {
          readCheckbox.click();
        }
      }
      return self.grantPrivilege(self._userByLogin[login], type, self.getProject().getProjectId());
    } else {
      if (type === PrivilegeType.READ_PROJECT) {
        var writeCheckbox = $("[data='" + PrivilegeType.WRITE_PROJECT + "," + login + "']", usersTable);
        if (writeCheckbox.is(":checked")) {
          writeCheckbox.click();
        }
      }
      return self.revokePrivilege(self._userByLogin[login], type, self.getProject().getProjectId());
    }
  });

  return result;
};

/**
 *
 * @returns {HTMLElement}
 */
EditProjectDialog.prototype.createReviveTabContent = function () {
  var self = this;

  var result = Functions.createElement({
    type: "div",
    className: "minerva-project-archive-tab",
    style: "margin-top:10px;"
  });

  if (self.getProject().getStatus().toLowerCase() === "archived") {
    result.innerHTML = "<div>Restore this project to its previous functionality.<br/>" +
      "Restoring will take time necessary to re-create the images.</div></br>" +
      "<button title='Revive' name='reviveProject' data='" + self.getProject().getProjectId() + "'>Revive <i class='fa fa-box-open'></i></button> ";
  } else {
    result.innerHTML = "Archiving this project will make it inaccessible.<br/>" +
      "Its images will be removed.<br/>" +
      "All the remaining data will be kept.<br/>" +
      "It will be possible to restore the project to full functionality afterwards.<br/>" +
      "Restoring will take time necessary to re-create the images.<br/></br>" +
      "<button title='Archive' name='archiveProject' data='" + self.getProject().getProjectId() + "'>Archive <i class='fa fa-box'></i></button>";
  }
  $(result).on("click", "[name='archiveProject']", function () {
    var button = this;
    return GuiConnector.showConfirmationDialog({
      title: 'Archive project',
      message: "<div style='font-size:120%;font-weight: bold;line-height:150%;'>" +
        "<p>Do you want to archive project?</p>" +
        "</div>",
      defaultButton: "No",
      align: "left"
    }).then(function (response) {
      if (response) {
        return self.getServerConnector().archiveProject($(button).attr("data"))
          .then(function () {
            self.close();
          }).catch(GuiConnector.alert);
      }
    });
  });
  $(result).on("click", "[name='reviveProject']", function () {
    var button = this;
    return GuiConnector.showConfirmationDialog({
      title: 'Revive project',
      message: "<div style='font-size:120%;font-weight: bold;line-height:150%;'>" +
        "<p>Do you want to revive project?</p>" +
        "</div>",
      defaultButton: "No",
      align: "left"
    }).then(function (response) {
      if (response) {
        return self.getServerConnector().reviveProject($(button).attr("data"))
          .then(function () {
            self.close();
          }).catch(GuiConnector.alert);
      }
    });
  });

  return result;
};

/**
 *
 * @returns {{title: string, privilegeType: PrivilegeType|undefined}[]}
 */
EditProjectDialog.prototype.createUserPrivilegeColumns = function () {
  var self = this;

  if (self._userPrivilegeColumns !== undefined) {
    return self._userPrivilegeColumns;
  }

  var configuration = self.getConfiguration();
  self._userPrivilegeColumns = [{
    title: "Name",
    orderable: true
  }];
  var privilegeTypes = configuration.getPrivilegeTypes();
  for (var i = 0; i < privilegeTypes.length; i++) {
    var type = privilegeTypes[i];
    if (type.getObjectType() === "Project") {
      self._userPrivilegeColumns.push({
        "title": type.getCommonName(),
        privilegeType: type,
        orderable: false
      });
    }
  }
  return self._userPrivilegeColumns;
};

/**
 *
 * @returns {Promise}
 */
EditProjectDialog.prototype.init = function () {
  var self = this;
  var onReloadListener = function () {
    self.projectDataUpdated(self.getProject());
  };
  onReloadListener.listenerName = "EDIT_PROJECT_UPDATE";
  self.getProject().addListener("onreload", onReloadListener);

  self.getServerConnector().addListener("onAddDataOverlay", function () {
    return self.refreshOverlays();
  });

  return self.initUsersTab().then(function () {
    return self.getServerConnector().getLoggedUser();
  }).then(function (loggedUser) {
    var readOnlyAccess = loggedUser.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR)) &&
      !loggedUser.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.WRITE_PROJECT), self.getProject().getProjectId());
    if (readOnlyAccess) {
      self._title += " (View only)";
    }
    return self.refresh();
  }).then(function () {
    $(window).trigger('resize');
  });
};

/**
 *
 * @param {Project} project
 */

EditProjectDialog.prototype.projectDataUpdated = function (project) {
  var element = this.getElement();
  $("[name='projectName']", element).val(xss(project.getName()));
  var disableProjectId = this.getConfiguration().getOption(ConfigurationType.DEFAULT_MAP).getValue() === project.getProjectId();
  $("[name='projectId']", element).val(xss(project.getProjectId()));
  $("[name='projectId']", element).attr("disabled", disableProjectId);
  if (disableProjectId) {
    $("[name='projectId']", element).attr("title", "Cannot change projectId of projects shared in minervanet and default project");
  } else {
    $("[name='projectId']", element).attr("title", "");
  }
  $("[name='projectVersion']", element).val(xss(project.getVersion()));
  $("[name='customLicenseName']", element).val(xss(project.getCustomLicenseName()));
  $("[name='customLicenseUrl']", element).val(xss(project.getCustomLicenseUrl()));

  var disease = "";
  if (project.getDisease() !== undefined) {
    disease = xss(project.getDisease().getResource());
  }
  $("[name='projectDisease']", element).val(disease);
  var organism = "";
  if (project.getOrganism() !== undefined) {
    organism = xss(project.getOrganism().getResource());
  }
  $("[name='projectOrganism']", element).val(organism);

  var email = "";
  if (project.getNotifyEmail() !== undefined) {
    email = xss(project.getNotifyEmail());
  }
  $("[name='projectNotifyEmail']", element).val(email);
};

/**
 *
 * @returns {Promise}
 */
EditProjectDialog.prototype.refresh = function () {
  var self = this;
  var project = self.getProject();

  self.projectDataUpdated(project);


  return Promise.all([
    self.refreshUsers(),
    self.refreshMaps(),
    self.refreshOverlays(),
    self.refreshBackgrounds(),
    self.refreshComments()
  ]).then(function () {
    return ServerConnector.getLicenses();
  }).then(function (licenses) {
    self._licenses = licenses;
    var select = $("[name='project-license']", self.getElement())[0];
    $(select).empty();
    $("[name='project-license-show']", self.getElement()).hide();
    select.appendChild(Functions.createElement({
      type: "option",
      content: "--OTHER--"
    }));
    for (var i = 0; i < licenses.length; i++) {
      var license = licenses[i];
      var option = Functions.createElement({
        type: "option",
        content: license.getName(),
        value: license.getId(),
        data: license.getContent()
      });
      if (license.getId() === self.getProject().getLicenseId()) {
        option.selected = true;
        $("[name='project-license-show']", self.getElement()).show();
        $("[name='custom-license-name-row']", self.getElement()).hide();
        $("[name='custom-license-url-row']", self.getElement()).hide();
      }
      select.appendChild(option);
    }
  });
};

/**
 *
 * @returns {Promise}
 */
EditProjectDialog.prototype.initUsersTab = function () {
  var self = this;

  var usersTable = $("[name=usersTable]", self.getElement())[0];

  var columns = self.createUserPrivilegeColumns();
  $(usersTable).DataTable({
    columns: columns,
    ordering: true
  });
  return Promise.resolve();
};

/**
 *
 * @returns {Promise}
 */
EditProjectDialog.prototype.refreshOverlays = function () {
  var self = this;
  return self.getServerConnector().getLoggedUser().then(function (user) {
    var curatorPrivilege = self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR);
    var adminPrivilege = self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN);
    //we need to refresh users as well because of privileges
    if (user.hasPrivilege(curatorPrivilege) || user.hasPrivilege(adminPrivilege)) {
      return self.getServerConnector().getOverlays({
        projectId: self.getProject().getProjectId()
      }).then(function (overlays) {
        return self.setOverlays(overlays);
      });
    } else {
      guiUtils.disableTab($(".minerva-project-overlays-tab", self.getElement())[0], "You have no privileges to manage users data");
    }
  });
};

/**
 *
 * @returns {Promise}
 */
EditProjectDialog.prototype.refreshBackgrounds = function () {
  var self = this;
  return self.getServerConnector().getLoggedUser().then(function (user) {
    var curatorPrivilege = self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR);
    var adminPrivilege = self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN);
    //we need to refresh users as well because of privileges
    if (user.hasPrivilege(curatorPrivilege) || user.hasPrivilege(adminPrivilege)) {
      return self.getServerConnector().getBackgrounds({
        projectId: self.getProject().getProjectId()
      }).then(function (backgrounds) {
        return self.setBackgrounds(backgrounds);
      });
    } else {
      guiUtils.disableTab($(".minerva-project-backgrounds-tab", self.getElement())[0], "You have no privileges to manage users data");
    }
  });
};

/**
 *
 * @returns {Promise}
 */
EditProjectDialog.prototype.refreshMaps = function () {
  var self = this;
  return self.getServerConnector().getModels(self.getProject().getProjectId()).then(function (maps) {
    return self.setMaps(maps);
  });
};

/**
 *
 * @returns {Promise}
 */
EditProjectDialog.prototype.refreshUsers = function () {
  var self = this;
  return self.getServerConnector().getLoggedUser().then(function (loggedUser) {
    var isAdmin = loggedUser.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN));
    var isCurator = loggedUser.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR)) &&
      loggedUser.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.WRITE_PROJECT), self.getProject().getProjectId());

    //we need to refresh users as well because of privileges
    if (isAdmin || isCurator) {
      return self.getServerConnector().getUsers(true).then(function (users) {
        return self.setUsers(users);
      });
    } else {
      guiUtils.disableTab($(".minerva-project-users-tab", self.getElement())[0], "You have no privileges to manage users data");
    }
  });
};

/**
 *
 * @param {DataOverlay[]} overlays
 * @returns {Promise}
 */
EditProjectDialog.prototype.setOverlays = function (overlays) {
  var self = this;
  self._overlayById = [];
  return ServerConnector.getUsers().then(function (users) {
    var dataTable = $($("[name='overlaysTable']", self.getElement())[0]).DataTable();
    var data = [];
    for (var i = 0; i < overlays.length; i++) {
      var overlay = overlays[i];
      self._overlayById[overlay.getId()] = overlay;
      var rowData = self.overlayToTableRow(overlay, users);
      data.push(rowData);
    }
    dataTable.clear().rows.add(data).draw();
  });
};

/**
 *
 * @param {Background[]} backgrounds
 * @returns {Promise}
 */
EditProjectDialog.prototype.setBackgrounds = function (backgrounds) {
  var self = this;
  self._backgroundById = [];
  return ServerConnector.getUsers().then(function (users) {
    var dataTable = $($("[name='backgroundsTable']", self.getElement())[0]).DataTable();
    var data = [];
    for (var i = 0; i < backgrounds.length; i++) {
      var background = backgrounds[i];
      self._backgroundById[background.getId()] = background;
      var rowData = self.backgroundToTableRow(background, users);
      data.push(rowData);
    }
    dataTable.clear().rows.add(data).draw();
  });
};

/**
 *
 * @param {MapModel[]} maps
 * @return {Promise<T>}
 */
EditProjectDialog.prototype.setMaps = function (maps) {
  var self = this;
  return self.getServerConnector().getLoggedUser().then(function (user) {
    self._mapsById = [];
    var dataTable = $($("[name='mapsTable']", self.getElement())[0]).DataTable();
    var data = [];
    for (var i = 0; i < maps.length; i++) {
      var map = maps[i];
      self._mapsById[map.getId()] = map;
      var rowData = self.mapToTableRow(map, user);
      data.push(rowData);
    }
    dataTable.clear().rows.add(data).draw();
  });
};

/**
 *
 * @param {User[]} users
 */
EditProjectDialog.prototype.setUsers = function (users) {
  var self = this;
  self._userByLogin = [];
  var columns = self.createUserPrivilegeColumns();
  var dataTable = $($("[name='usersTable']", self.getElement())[0]).DataTable();
  var data = [];
  for (var i = 0; i < users.length; i++) {
    var user = users[i];
    self._userByLogin[user.getLogin()] = user;
    var rowData = self.userToTableRow(user, columns);
    data.push(rowData);
  }
  dataTable.clear().rows.add(data).draw();
};

/**
 *
 * @param {User} user
 * @param {Array} columns
 * @returns {Array}
 */
EditProjectDialog.prototype.userToTableRow = function (user, columns) {
  var self = this;
  var row = [];
  var login = user.getLogin();

  var isAdmin = user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN));
  var disabled = "";
  if (isAdmin) {
    disabled = " disabled "
  }

  row[0] = user.getName() + " " + user.getSurname() + " (" + login + ")";
  for (var i = 1; i < columns.length; i++) {
    var column = columns[i];
    if (column.privilegeType !== undefined) {
      if (column.privilegeType.getValueType() === "boolean") {
        var checked = '';
        if (user.hasPrivilege(column.privilegeType, self.getProject().getProjectId())) {
          checked = 'checked';
        }
        row[i] = "<input type='checkbox' name='privilege" + "' data='" + column.privilegeType.getName() + "," + login + "' "
          + checked + disabled + "/>";
        if (isAdmin) {
          row[i] += " ADMIN";
        }
      } else {
        throw new Error("Unsupported type: " + column.privilegeType.getValueType());
      }
    }
  }
  return row;
};

/**
 *
 * @param {DataOverlay} overlay
 * @param {User[]} users
 * @returns {Array}
 */
EditProjectDialog.prototype.overlayToTableRow = function (overlay, users) {
  var self = this;

  var loggedUser = null, i;
  for (i = 0; i < users.length; i++) {

    if (users[i].getLogin() === self.getServerConnector().getSessionData().getLogin()) {
      loggedUser = users[i];
    }
  }

  var disabled = " disabled ";
  var isAdmin = loggedUser.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN));
  var isCurator = loggedUser.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR)) &&
    loggedUser.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.WRITE_PROJECT), self.getProject().getProjectId());
  if (isAdmin || isCurator) {
    disabled = "";
  }

  var row = [];
  var id = overlay.getId();
  var creatorSelect;

  creatorSelect = "<select data='" + id + "' name='creator'" + disabled + ">";

  var selected = "";
  if (overlay.getCreator() === "") {
    selected = "selected";
  }
  for (i = 0; i < users.length; i++) {
    selected = "";
    var user = users[i];
    if (overlay.getCreator() === user.getLogin()) {
      selected = "selected";
    }

    creatorSelect += "<option value='" + user.getLogin() + "' " + selected + ">" + user.getLogin() + "("
      + user.getName() + " " + user.getSurname() + ")</option>";

    if (user.getLogin() === self.getServerConnector().getSessionData().getLogin()) {
      loggedUser = user;
    }
  }
  creatorSelect += "</select>";

  var checked = '';
  if (overlay.getPublicOverlay()) {
    checked = "checked";
  }
  var publicOverlayCheckbox = "<input type='checkbox' data='" + id + "' name='publicOverlay' " + checked + disabled + "/>";

  var downloadSourceButton = "<button name='downloadSource' data='" + id + "'" + disabled + ">"
    + "<span class='ui-icon ui-icon-arrowthickstop-1-s'></span>" + "</button>";

  row[0] = id;
  row[1] = "<input data='" + id + "' name='overlayName' value='" + overlay.getName() + "'" + disabled + "/>";
  row[2] = "<input data='" + id + "' name='overlayDescription' value='" + overlay.getDescription() + "'" + disabled + "/>";
  row[3] = publicOverlayCheckbox;
  row[4] = creatorSelect;
  row[5] = downloadSourceButton;


  row[6] = "<button name='removeOverlay' data='" + id + "'" + disabled + "><i class='fa fa-trash-alt'></button>";

  return row;
};

/**
 *
 * @param {Background} background
 * @returns {Array}
 */
EditProjectDialog.prototype.backgroundToTableRow = function (background) {
  var row = [];
  var id = background.getId();

  var checked = '';
  if (background.isDefaultOverlay()) {
    checked = "checked";
  }
  var defaultCheckbox = "<input type='checkbox' data='" + id + "' name='defaultBackground' " + checked + "/>";

  row[0] = id;
  row[1] = "<input data='" + id + "' name='backgroundName' value='" + background.getName() + "'/>";
  row[2] = "<input data='" + id + "' name='backgroundDescription' value='" + background.getDescription() + "'/>";
  row[3] = defaultCheckbox;
  row[4] = "<button name='removeBackground' data='" + id + "'><i class='fa fa-trash-alt'></button>";

  return row;
};

/**
 *
 * @param value
 * @returns {*}
 */
function getValueOrEmpty(value) {
  if (value !== undefined && value !== null) {
    return value;
  } else {
    return "";
  }
}

/**
 *
 * @param {MapModel} map
 * @param {User} user
 * @returns {Array}
 */
EditProjectDialog.prototype.mapToTableRow = function (map, user) {
  var self = this;

  var canWrite = user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN)) ||
    user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.WRITE_PROJECT), self.getProject().getProjectId());

  var disabled = " disabled ";

  if (canWrite) {
    disabled = '';
  }

  var row = [];
  var id = map.getId();
  var centerX = getValueOrEmpty(map.getDefaultCenterX());
  var centerY = getValueOrEmpty(map.getDefaultCenterY());
  var zoomLevel = getValueOrEmpty(map.getDefaultZoomLevel());
  row[0] = id;
  row[1] = map.getName();
  row[2] = "<input name='defaultCenterX' data='" + id + "' value='" + centerX + "'" + disabled + "/>";
  row[3] = "<input name='defaultCenterY' data='" + id + "' value='" + centerY + "'" + disabled + "/>";

  var selected = "";
  if ('' === zoomLevel) {
    selected = " selected ";
  }

  var zoomLevelSelect = "<select name='defaultZoomLevel' data='" + id + "' " + disabled + ">" +
    "<option value=''" + selected + ">---</option>";
  for (var i = map.getMinZoom(); i <= map.getMaxZoom(); i++) {
    selected = "";
    if (i === zoomLevel) {
      selected = " selected ";
    }
    zoomLevelSelect += "<option value='" + i + "' " + selected + ">" + (i - map.getMinZoom()) + "</option>";
  }
  zoomLevelSelect += "</select>";
  row[4] = zoomLevelSelect;

  return row;
};

/**
 *
 */
EditProjectDialog.prototype.open = function () {
  var self = this;
  var div = self.getElement();
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      dialogClass: 'minerva-edit-project-dialog',
      title: self._title,
      width: window.innerWidth * 0.75,
      height: window.innerHeight / 2
    });
    $(".ui-dialog-titlebar-close", $(div).dialog().siblings('.ui-dialog-titlebar')).on("mousedown", function () {
      //we need to close dialog on mouse down, because processing modal dialogs prevents the click to finish happening
      $(div).dialog("close");
    });
  }
  $(div).dialog("open");
};

/**
 *
 * @param {string} type
 * @param {?null|string} resource
 * @returns {Annotation|null}
 */
var prepareMiriamData = function (type, resource) {
  if (resource === "" || resource === undefined || resource === null) {
    return null;
  } else {
    return new Annotation({
      type: type,
      resource: resource
    });
  }
};

/**
 *
 * @param {Project} project
 * @returns {Promise}
 */
EditProjectDialog.prototype.updateProject = function (project) {
  var self = this;

  GuiConnector.showProcessing();
  return self.getServerConnector().updateProject(project).catch(function (error) {
    if ((error instanceof NetworkError && error.statusCode === HttpStatus.BAD_REQUEST)) {
      GuiConnector.alert(error.content.reason);
    } else {
      GuiConnector.alert(error);
    }
  }).finally(GuiConnector.hideProcessing);
};

/**
 *
 * @param {Project} project
 * @param {string} projectId
 * @returns {Promise}
 */
EditProjectDialog.prototype.moveProject = function (project, projectId) {
  var self = this;

  GuiConnector.showProcessing();
  return self.getServerConnector().moveProject({
    oldProjectId: project.getProjectId(),
    newProjectId: projectId
  }).then(function () {
    project.setProjectId(projectId);
    return project.callListeners("onreload");
  }).catch(function (error) {
    if ((error instanceof NetworkError && error.statusCode === HttpStatus.BAD_REQUEST)) {
      GuiConnector.alert(error.content.reason);
    } else {
      GuiConnector.alert(error);
    }
  }).finally(GuiConnector.hideProcessing);
};

/**
 *
 * @returns {Promise}
 */
EditProjectDialog.prototype.checkValidity = function () {
  var self = this;
  var isValid = true;
  var error = "<b>Some data is missing.</b><ul>";

  if (isValid) {
    return Promise.resolve(true);
  } else {
    return Promise.reject(new ValidationError(error));
  }
};

/**
 *
 */
EditProjectDialog.prototype.close = function () {
  var self = this;
  $(self.getElement()).dialog("close");
};

/**
 *
 * @param {DataOverlay} overlay
 * @returns {Promise}
 */
EditProjectDialog.prototype.updateOverlay = function (overlay) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getServerConnector().updateOverlay(overlay, self.getProject()).catch(GuiConnector.alert).finally(GuiConnector.hideProcessing);
};

/**
 *
 * @param {Background} background
 * @returns {Promise}
 */
EditProjectDialog.prototype.updateBackground = function (background) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getServerConnector().updateBackground(background, self.getProject()).catch(GuiConnector.alert).finally(GuiConnector.hideProcessing);
};

/**
 *
 * @param {MapModel} map
 * @returns {Promise}
 */
EditProjectDialog.prototype.updateMap = function (map) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getServerConnector().updateModel({
    projectId: self.getProject().getProjectId(),
    model: map
  }).catch(GuiConnector.alert).finally(GuiConnector.hideProcessing);
};

/**
 *
 * @param {User} user
 * @param {string} privilegeType
 * @param {string} projectId
 * @returns {Promise}
 */
EditProjectDialog.prototype.grantPrivilege = function (user, privilegeType, projectId) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getServerConnector().grantProjectPrivilege({
    projectId: projectId,
    user: user,
    privilegeType: privilegeType
  }).catch(GuiConnector.alert).finally(GuiConnector.hideProcessing);
};

/**
 *
 * @param {User} user
 * @param {string} privilegeType
 * @param {string} projectId
 * @returns {Promise}
 */
EditProjectDialog.prototype.revokePrivilege = function (user, privilegeType, projectId) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getServerConnector().revokeProjectPrivilege({
    projectId: projectId,
    user: user,
    privilegeType: privilegeType
  }).catch(GuiConnector.alert).finally(GuiConnector.hideProcessing);
};


/**
 *
 * @param {number} overlayId
 * @returns {Promise}
 */
EditProjectDialog.prototype.removeOverlay = function (overlayId) {
  var self = this;
  return self.getServerConnector().removeOverlay({
    overlayId: overlayId,
    projectId: self.getProject().getProjectId()
  }).then(function () {
    return self.refreshOverlays();
  });
};

/**
 *
 * @param {number} backgroundId
 * @returns {Promise}
 */
EditProjectDialog.prototype.removeBackground = function (backgroundId) {
  var self = this;
  return self.getServerConnector().removeBackground({
    backgroundId: backgroundId,
    projectId: self.getProject().getProjectId()
  }).then(function () {
    return self.refreshBackgrounds();
  });
};

/**
 *
 * @returns {Promise<AddOverlayDialog>}
 */
EditProjectDialog.prototype.openAddOverlayDialog = function () {
  var self = this;
  if (self._addOverlayDialog !== undefined) {
    self._addOverlayDialog.destroy();
  }
  self._addOverlayDialog = new AddOverlayDialog({
    project: self.getProject(),
    customMap: null,
    element: document.createElement("div"),
    configuration: self.getConfiguration(),
    serverConnector: self.getServerConnector()
  });
  return self._addOverlayDialog.init().then(function () {
    return self._addOverlayDialog.open();
  }).then(function () {
    return self._addOverlayDialog;
  });
};

/**
 *
 * @param {CommentsTab} commentsTab
 */
EditProjectDialog.prototype.setCommentsTab = function (commentsTab) {
  this._commentsTab = commentsTab;
};

/**
 *
 * @returns {CommentsTab}
 */
EditProjectDialog.prototype.getCommentsTab = function () {
  return this._commentsTab;
};

/**
 *
 * @returns {Promise}
 */
EditProjectDialog.prototype.refreshComments = function () {
  return this.getCommentsTab().refreshComments();
};


/**
 *
 */
EditProjectDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();
  var usersTable = $("[name=usersTable]", self.getElement())[0];
  var overlaysTable = $("[name=overlaysTable]", self.getElement())[0];
  var backgroundsTable = $("[name=backgroundsTable]", self.getElement())[0];
  var mapsTable = $("[name=mapsTable]", self.getElement())[0];
  if ($.fn.DataTable.isDataTable(usersTable)) {
    $(usersTable).DataTable().destroy();
  }
  if ($.fn.DataTable.isDataTable(overlaysTable)) {
    $(overlaysTable).DataTable().destroy();
  }
  if ($.fn.DataTable.isDataTable(backgroundsTable)) {
    $(backgroundsTable).DataTable().destroy();
  }
  if ($.fn.DataTable.isDataTable(mapsTable)) {
    $(mapsTable).DataTable().destroy();
  }

  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
  if (self._addOverlayDialog !== undefined) {
    self._addOverlayDialog.destroy();
    delete self._addOverlayDialog;
  }
  if (this.getCommentsTab() !== undefined) {
    this.getCommentsTab().destroy();
  }

  var listenerName = "EDIT_PROJECT_UPDATE";
  var listeners = self.getProject().getListeners("onreload");
  for (var i = 0; i < listeners.length; i++) {
    if (listeners[i].listenerName === listenerName) {
      self.getProject().removeListener("onreload", listeners[i]);
    }
  }
};

/**
 * @returns {?License}
 */
EditProjectDialog.prototype.getLicense = function () {
  var self = this;
  var id = parseInt($("[name='project-license']", this.getElement()).val());
  for (var i = 0; i < self._licenses.length; i++) {
    var license = self._licenses[i];
    if (license.getId() === id) {
      return license;
    }
  }
  return null;
};

module.exports = EditProjectDialog;
