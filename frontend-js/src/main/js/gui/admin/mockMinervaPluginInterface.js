var DEFAULT_MAP = require("../../ConfigurationType").DEFAULT_MAP;

function mockInterface(pluginData, configuration, serverConnector) {
  var minerva = {
    plugins: {
      registerPlugin: function (newData) {
        pluginData.pluginRawNewData = newData;

        //mock connection to the new interface
        return {
          element: $("<div/>")[0],
          events: {
            addListener: function () {
            },
            removeListener: function () {
            },
            removeAllListeners: function () {
            }
          },
          legend: {
            setLegend: function () {
            },
            removeLegend: function () {
            }
          }
        };
      }
    },
    data: {
      bioEntities: {
        getAllBioEntities: function () {
        },
        getAllChemicals: function () {
        },
        getAllDrugs: function () {
        },
        getAllMarkers: function () {
        },
        getShownElements: function () {
        },
        addSingleMarker: function () {
        },
        removeSingleMarker: function () {
        },
        removeAllMarkers: function () {
        },
        clearAllElements: function () {
        }
      }
    },
    map: {
      data: {
        getBounds: function () {
        },
        getOpenMapId: function () {
        },
        getModels: function () {
          return [];
        }
      },
      fitBounds: function () {
      },
      openMap: function () {
      },
      triggerSearch: function () {
      },
      getZoom: function () {
      },
      setZoom: function () {
      },
      getCenter: function () {
      },
      setCenter: function () {
      }
    },
    overviewImage: {
      getCurrentOverviewImage: function () {
      },
      getOverviewImages: function () {
      },
      hideOverviewImageModal: function () {
      },
      selectOverviewImage: function () {
      },
      showOverviewImageModal: function () {
      }
    },
    overlays: {
      data: {
        getDataOverlays: function () {
        },
        getVisibleDataOverlays: function () {
          return [];
        }
      },
      showDataOverlay: function () {
      },
      hideDataOverlay: function () {
      },
      removeDataOverlay: function () {
      },
      addDataOverlay: function () {
      }
    },
    project: {
      data: {
        getProjectId: function () {
          return configuration.getOption(DEFAULT_MAP).getValue()
        },
        getName: function () {
        },
        getVersion: function () {
        },
        getDisease: function () {
        },
        getOrganism: function () {
        },
        getApiUrls: function () {
          return {
            baseApiUrl: serverConnector.getApiBaseUrl().replace(new RegExp("\\/$"), "")
          }
        }
      }
    }
  }
  global.minerva = minerva;
  window.minerva = minerva;
  return minerva;
}

module.exports = mockInterface;
