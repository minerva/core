"use strict";

/* exported logger */
var $ = require('jquery');

var AbstractAdminPanel = require('./AbstractAdminPanel');
var AddProjectDialog = require('./AddProjectDialog');
var EditProjectDialog = require('./EditProjectDialog');
var LogListDialog = require('./LogListDialog');
var JobListDialog = require('./JobListDialog');
var PrivilegeType = require('../../map/data/PrivilegeType');
var ConfigurationType = require('../../ConfigurationType');
var NetworkError = require('../../NetworkError');
var ValidationError = require('../../ValidationError');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var Functions = require('../../Functions');
var GuiConnector = require('../../GuiConnector');
var Promise = require("bluebird");
var xss = require('xss');
var RegisterInMinervaNetDialog = require("./RegisterInMinervaNetDialog");

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} [params.customMap]
 * @param {Configuration} params.configuration
 * @param {Project} [params.project]
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 * @extends AbstractGuiElement
 */
function MapsAdminPanel(params) {
  var self = this;
  AbstractAdminPanel.call(self, params);
  self._createGui();

  $(self.getElement()).addClass("minerva-projects-tab");

}

MapsAdminPanel.prototype = Object.create(AbstractAdminPanel.prototype);
MapsAdminPanel.prototype.constructor = MapsAdminPanel;

MapsAdminPanel.AUTO_REFRESH_TIME = 5000;

/**
 *
 * @private
 */
MapsAdminPanel.prototype._createGui = function () {
  var self = this;
  var projectsDiv = Functions.createElement({
    type: "div"
  });
  self.getElement().appendChild(projectsDiv);

  var dataDiv = Functions.createElement({
    type: "div",
    style: "display:table; width:100%"
  });
  projectsDiv.appendChild(dataDiv);

  dataDiv.appendChild(self._createMenuRow());
  dataDiv.appendChild(self._createProjectTableRow());
  dataDiv.appendChild(self._createMenuRow());

};

/**
 *
 * @returns {HTMLElement}
 * @private
 */
MapsAdminPanel.prototype._createMenuRow = function () {
  var self = this;
  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });

  var addProjectButton = Functions.createElement({
    type: "button",
    name: "addProject",
    content: '<span class="ui-icon ui-icon-circle-plus"></span>&nbsp;ADD PROJECT',
    onclick: function () {
      return self.onAddClicked().then(null, GuiConnector.alert);
    },
    xss: false
  });
  var refreshButton = Functions.createElement({
    type: "button",
    name: "refreshProject",
    content: '<span class="ui-icon ui-icon-refresh"></span>&nbsp;REFRESH',
    onclick: function () {
      return self.onRefreshClicked().catch(GuiConnector.alert);
    },
    xss: false
  });
  menuRow.appendChild(addProjectButton);
  menuRow.appendChild(refreshButton);
  menuRow.appendChild(Functions.createElement({
    type: "button",
    name: "registerInMinervaNet",
    style: "display: none",
    content: '<span class="fa fa-share-alt"></span>&nbsp;REGISTER IN MINERVA-NET',
    onclick: function () {
      if (self._registerInMinervaNetDialog === undefined) {
        self._registerInMinervaNetDialog = new RegisterInMinervaNetDialog({
          element: Functions.createElement({
            type: "div"
          }),
          configuration: self.getConfiguration(),
          serverConnector: self.getServerConnector(),
          customMap: null
        });
        self._registerInMinervaNetDialog.addListener("onRegister", function () {
          return self.refreshMinervaNetStatus();
        });

        return self._registerInMinervaNetDialog.init().then(function () {
          return self._registerInMinervaNetDialog.open();
        });
      } else {
        return self._registerInMinervaNetDialog.refresh().then(function () {
          return self._registerInMinervaNetDialog.open();
        });
      }
    },
    xss: false
  }));
  menuRow.appendChild(Functions.createElement({
    type: "button",
    name: "unregisterInMinervaNet",
    style: "display: none",
    content: '<span class="fa fa-share-alt" style="color:red"></span>&nbsp;UNREGISTER IN MINERVA-NET',
    onclick: function () {
      return GuiConnector.showConfirmationDialog({
        title: 'Unregister from minerva net',
        message: "Are you sure you want to unregister this machine from minerva-net? Information about all shared projects will be removed from minerva-net."
      }).then(function (response) {
        if (response) {
          GuiConnector.showProcessing();
          return self.getServerConnector().unregisterInMinervaNet().then(function () {
            return self.getServerConnector().getProjects(true)
          }).then(function () {
            return self.refreshMinervaNetStatus();
          }).finally(function () {
            GuiConnector.hideProcessing();
          });
        }
      }).catch(GuiConnector.alert);
    },
    xss: false
  }));
  return menuRow;
}
;

/**
 *
 * @returns {HTMLElement}
 * @private
 */
MapsAdminPanel.prototype._createProjectTableRow = function () {
  var self = this;
  var projectsRow = Functions.createElement({
    type: "div",
    style: "display:table-row; width:100%"
  });

  var projectsTable = Functions.createElement({
    type: "table",
    name: "projectsTable",
    className: "display",
    style: "width:100%"
  });
  projectsRow.appendChild(projectsTable);

  $(projectsTable).DataTable({
    columns: [{
      title: 'ProjectId'
    }, {
      title: 'Created'
    }, {
      title: 'Created by'
    }, {
      title: 'Name'
    }, {
      title: 'Disease'
    }, {
      title: 'Organism'
    }, {
      title: 'Shared in minerva-net'
    }, {
      title: 'Status'
    }, {
      title: 'Edit'
    }, {
      title: 'Remove'
    }],
    order: [[0, "asc"]],
    columnDefs: [
      {"orderable": false, "targets": [6, 7, 8, 9]}
    ]
  });
  self.bindDataTablePageLengthToUserPreference({
    element: projectsTable,
    preferenceName: 'admin-projects-datatable-length'
  });

  $(projectsTable).on("click", "[name='removeProject']", function () {
    var button = this;
    return self.askConfirmRemoval({
      title: "INFO",
      content: "Do you really want to remove '" + $(button).attr("data") + "' map?",
      input: false
    }).then(function (param) {
      if (param.status) {
        return self.removeProject($(button).attr("data"))
      }
    }).catch(function (e) {
      if (e instanceof NetworkError && e.statusCode === 404) {
        GuiConnector.warn("Project does not exist")
        return self.onRefreshClicked();
      } else {
        GuiConnector.alert(e);
      }
    });
  });

  $(projectsTable).on("click", "[name='showEditDialog']", function () {
    var button = this;
    return self.showEditDialog($(button).attr("data")).catch(GuiConnector.alert);
  });
  $(projectsTable).on("click", "[name='showLogEntries']", function () {
    var button = this;
    return self.showLogs($(button).attr("data")).catch(GuiConnector.alert);
  });

  $(projectsTable).on("click", "[name='showJobs']", function () {
    var button = this;
    return self.showJobs($(button).attr("data")).catch(GuiConnector.alert);
  });

  $(projectsTable).on("click", "[name='minerva-shared']", function () {
      var field = this;
      var projectId = $(field).attr('data');
      GuiConnector.showProcessing();
      var changeToShared = $(field).is(":checked");
      var promise;
      if (changeToShared) {
        promise = GuiConnector.showConfirmationDialog({
          title: 'Share a project in MINERVA-NET',
          message: "<div style='font-size:120%;font-weight: bold;line-height:150%;'>" +
            "You are about to share information about this publicly visible project via the MINERVA-NET service." +
            "<p>The following information about the project will be sent and become publicly accessible in MINERVA-NET:" +
            "<ul>" +
            "<li>project name, identifier and version\n" +
            "<li>timestamps of project creation and sharing\n" +
            "<li>disease and organism associated with the project\n" +
            "</ul>" +
            "Your email will be registered as associated with this project, but will not be shared. " +
            "It will be used only to send you warnings about MINERVA-NET connectivity issues, in case they happen." +
            "<p>By stopping project sharing, project data and your email will no longer be shared via MINERVA-NET. " +
            "They will be stored internally for performance analysis. If you want them to be removed completely, " +
            "please send your request to minerva@uni.lu</p>" +
            "</div>",
          defaultButton: "No",
          align: "left"
        }).then(function (response) {
          if (response) {
            return self.getServerConnector().getLoggedUser().then(function (user) {
              if (user.getEmail() === '' || user.getEmail() === undefined || user.getEmail() === null) {
                return Promise.reject(new ValidationError("You don't have valid email address specified in the account settings"));
              }
              return self.getServerConnector().getUser("anonymous");
            }).then(function (anonymous) {
              if (!anonymous.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.READ_PROJECT), projectId)) {
                $(field).prop('checked', false);
                return Promise.reject(new ValidationError("This project is not publicly visible. To share, provide view access to the user 'anonymous'"));
              }
            }).then(function () {
              return self.getServerConnector().registerProjectInMinervaNet(projectId);
            })
          } else {
            $(field).prop('checked', false);
          }
        });
      } else {
        promise = GuiConnector.showConfirmationDialog({
          title: 'Share a project in MINERVA-NET',
          message: "<div style='font-size:120%;font-weight: bold;line-height:150%;'>" +
            "<p>By stopping project sharing, project data and your email will no longer be shared via MINERVA-NET. " +
            "They will be stored internally for performance analysis. If you want them to be removed completely, " +
            "please send your request to minerva@uni.lu</p>" +
            "</div>",
          defaultButton: "No",
          align: "left"
        }).then(function (response) {
          if (response) {
            return self.getServerConnector().unregisterProjectInMinervaNet(projectId);
          } else {
            $(field).prop('checked', true);
          }
        });
      }
      return Promise.resolve(promise).catch(GuiConnector.alert).finally(function () {
        GuiConnector.hideProcessing();
        return self.getServerConnector().getProject(projectId);
      })
    }
  )
  ;


  return projectsRow;
};

/**
 *
 * @returns {Promise}
 */
MapsAdminPanel.prototype.init = function () {
  var self = this;
  var user
  self.getConfiguration().addListener("onOptionChanged", function (option) {
    if (option.arg.getType() === ConfigurationType.MINERVANET_AUTH_TOKEN
      || option.arg.getType() === ConfigurationType.MINERVANET_URL
      || option.arg.getType() === ConfigurationType.MINERVA_ROOT) {
      return self.refreshMinervaNetStatus();
    }
  });
  return AbstractAdminPanel.prototype.init.call(self).then(function () {
    return self.getServerConnector().getProjects();
  }).then(function (projects) {
    return self.setProjects(projects);
  }).then(function () {
    return self.refreshMinervaNetStatus();
  }).then(function () {
    return self.getServerConnector().getLoggedUser();
  }).then(function (loggedUser) {
    user = loggedUser;
    var configuration = self.getConfiguration();
    var canAddProject = user.hasPrivilege(configuration.getPrivilegeType(PrivilegeType.IS_CURATOR)) ||
      user.hasPrivilege(configuration.getPrivilegeType(PrivilegeType.IS_ADMIN));
    $("[name='addProject']", self.getElement()).attr("disabled", !canAddProject);

    var projectsTable = $("[name='projectsTable']", self.getElement())[0];

    self.bindDataTableOrderToUserPreference({
      element: projectsTable,
      preferenceName: 'admin-projects-datatable-order'
    });

    var isAdmin = user.hasPrivilege(configuration.getPrivilegeType(PrivilegeType.IS_ADMIN));
    if (!isAdmin) {
      $("[name='registerInMinervaNet']", self.getElement()).prop('disabled', true);
      $("[name='unregisterInMinervaNet']", self.getElement()).prop('disabled', true);
    }
  });
};

/**
 *
 * @returns {Promise}
 */
MapsAdminPanel.prototype.refreshMinervaNetStatus = function () {
  var self = this;
  return self.getServerConnector().isConnectedToMinervaNet().then(function (status) {
    if (status) {
      $("[name='registerInMinervaNet']", self.getElement()).hide();
      $("[name='unregisterInMinervaNet']", self.getElement()).show();
    } else {
      $("[name='registerInMinervaNet']", self.getElement()).show();
      $("[name='unregisterInMinervaNet']", self.getElement()).hide();
    }
    self._minervaNetStatus = status;
    //refresh datatable
    return self.getServerConnector().getProjects(false);
  }).then(function (projects) {
    return self.setProjects(projects);
  });
};

/**
 *
 * @param {Project} project
 * @param {Array} [row]
 * @param {User} user
 * @returns {Array}
 */
MapsAdminPanel.prototype.projectToTableRow = function (project, row, user) {
  var self = this;
  var disease = self.getHtmlStringLink(project.getDisease());
  var organism = self.getHtmlStringLink(project.getOrganism());

  if (row === undefined) {
    row = [];
  }
  var projectId = project.getProjectId();
  var formattedProjectId;
  if (project.getStatus().toLowerCase() === "ok") {
    formattedProjectId = "<a href='" + "index.xhtml?id=" + projectId + "' target='" + project.getId() + "'>" + projectId + "</a>";
  } else {
    formattedProjectId = projectId;
  }
  var status = project.getStatus();
  if (project.getStatus().toLowerCase() !== "ok" && project.getStatus().toLowerCase() !== "failure" && project.getStatus().toLowerCase() !== "archived") {
    var step;
    if (status === "Parsing data") {
      step = 1;
    } else if (status === "Uploading to db") {
      step = 2;
    } else if (status === "Annotating") {
      step = 3;
    } else if (status === "Generating images") {
      step = 4;
    } else if (status === "Validating miriam data") {
      step = 5;
    } else if (status === "Caching pubmed data") {
      step = 6;
    } else if (status === "Caching miriam data") {
      step = 7;
    } else if (status === "Caching chemical data") {
      step = 8;
    } else if (status === "Caching drug data") {
      step = 9;
    } else if (status === "Project removing") {
      status = "Removing project, please wait";
    } else {
      status += ' (' + project.getProgress().toFixed(2) + ' %)';
    }
    if (step !== undefined) {
      status += " step " + step + " ( of 10)";
    }
  }

  var isAdmin = user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN)) ||
    user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR));

  var icon;
  if (project.hasLogEntries()) {
    icon = "<i class='fa fa-exclamation-triangle' style='font-size:18px; padding-right:10px;color:black'></i>";
    if (isAdmin) {
      status += "<a name='showLogEntries' href='#' data='" + project.getProjectId() + "'>" + icon + "</a>";
    } else {
      status += icon;
    }
  }

  if (isAdmin) {
    icon = "<i class='fa fa-info' style='font-size:18px; padding-right:10px;color:#337ab7'></i>";
    status += " <a name='showJobs' href='#' data='" + project.getProjectId() + "'>" + icon + "</a>";
  } else {
    status += icon;
  }


  row[0] = formattedProjectId;
  var date = project.getCreationDate();
  if (date === undefined) {
    date = "N/A";
  } else {
    date = date.split(" ")[0];
  }
  row[1] = date;
  row[2] = project.getOwner();
  if (row[2] === undefined) {
    row[2] = 'N/A';
  }
  row[3] = xss(project.getName());
  row[4] = disease;
  row[5] = organism;


  var minervaNetSharedId = 'minerva-shared-id-' + project.getProjectId();
  var readonly = ''
  var hasFullControlOnProject = (user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN)) ||
    (user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR)) &&
      user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.WRITE_PROJECT), project.getProjectId())));
  if (!self._minervaNetStatus || !hasFullControlOnProject) {
    readonly = ' disabled ';
  }
  if (project.getSharedInMinervaNet()) {
    row[6] = "<input type='checkbox' name='minerva-shared' " + readonly + " data='" + projectId + "' checked/> SHARED ";
  } else {
    row[6] = "<input type='checkbox' name='minerva-shared' " + readonly + " data='" + projectId + "'/> SHARED ";
  }
  row[7] = status;

  var disabledEdit = " disabled ";
  var disabledRemove = " disabled ";
  if ((user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN)) ||
      user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR)) ||
      user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.WRITE_PROJECT), project.getProjectId()))
    && (status.indexOf("Ok") === 0 || status.indexOf("Failure") === 0 || status.indexOf("Archived") === 0)) {
    disabledEdit = "";
  }

  if (hasFullControlOnProject && (status.indexOf("Ok") === 0 || status.indexOf("Failure") === 0 || status.indexOf("Archived") === 0)) {
    disabledRemove = "";
  }

  if (self.getConfiguration().getOption(ConfigurationType.DEFAULT_MAP).getValue() === projectId) {
    disabledRemove = " disabled ";
  }

  row[8] = "<button title='Edit' name='showEditDialog' data='" + project.getProjectId() + "'" + disabledEdit + "><i class='fa fa-edit'></i></button> ";

  row[9] = "<button name='removeProject' data='" + project.getProjectId() + "'" + disabledRemove + "><i class='fa fa-trash-alt'></button>";

  return row;
};

/**
 *
 * @param {Annotation} annotation
 * @returns {string}
 */
MapsAdminPanel.prototype.getHtmlStringLink = function (annotation) {
  var self = this;
  if (annotation !== undefined && annotation !== null) {
    var link = self.getGuiUtils().createAnnotationLink(annotation, true);
    var tmp = document.createElement("div");
    tmp.appendChild(link);
    return tmp.innerHTML;
  } else {
    return "N/A";
  }

};

/**
 *
 * @param {Project[]} projects
 * @returns {Promise}
 */
MapsAdminPanel.prototype.setProjects = function (projects) {
  var self = this;

  return self.getServerConnector().getLoggedUser().then(function (user) {
    var dataTable = $("[name='projectsTable']", self.getElement()).DataTable();
    var data = [];
    var page = dataTable.page();

    for (var i = 0; i < projects.length; i++) {
      var project = projects[i];
      var rowData = self.projectToTableRow(project, undefined, user);
      self.addUpdateListener(project);
      data.push(rowData);
    }
    //it should be simplified, but I couldn't make it work
    dataTable.clear().rows.add(data).page(page).draw(false).page(page).draw(false);
    if (page >= dataTable.page.info().pages) {
      page = dataTable.page.info().pages - 1;
      dataTable.page(page).draw(false)
    }
  });
};

/**
 *
 * @param {Project} project
 */
MapsAdminPanel.prototype.addUpdateListener = function (project) {
  var self = this;

  var listenerName = "PROJECT_LIST_LISTENER";
  var listeners = project.getListeners("onreload");
  for (var i = 0; i < listeners.length; i++) {
    if (listeners[i].listenerName === listenerName) {
      project.removeListener("onreload", listeners[i]);
    }
  }
  var createRefreshEventFunction = function () {
    var projectId = project.getProjectId();
    setTimeout(function () {
      console.log("Refreshing project: " + projectId);
      return self.getServerConnector().getProject(projectId).then(function (project) {
        if (project === null) {
          return self.onRefreshClicked();
        }
      });
    }, MapsAdminPanel.AUTO_REFRESH_TIME);
  };
  var listener = function () {
    return self.getServerConnector().getLoggedUser().then(function (user) {
      var table = $("[name='projectsTable']", self.getElement())[0];
      if (!$.fn.DataTable.isDataTable(table)) {
        return;
      }
      var dataTable = $(table).DataTable();

      var length = dataTable.data().length;
      for (var i = 0; i < length; i++) {
        var row = dataTable.row(i);
        var data = row.data();
        if (data[0].indexOf("target='" + project.getId() + "'") >= 0 || data[0].indexOf(project.getProjectId()) === 0) {
          self.projectToTableRow(project, data, user);
          var page = dataTable.page();
          row.data(data).draw();
          dataTable.page(page).draw(false);
        }
      }

      if (project.getStatus().toLowerCase() !== "ok" && project.getStatus().toLowerCase() !== "archived" && project.getStatus().toLowerCase() !== "failure") {
        createRefreshEventFunction();
      }
    });
  };
  listener.listenerName = listenerName;
  project.addListener("onreload", listener);

  if (project.getStatus().toLowerCase() !== "ok" && project.getStatus().toLowerCase() !== "archived" && project.getStatus().toLowerCase() !== "failure") {
    createRefreshEventFunction();
  }
};

/**
 *
 * @returns {Promise}
 */
MapsAdminPanel.prototype.onAddClicked = function () {
  var self = this;
  var dialog = self._addDialog;
  if (dialog === undefined) {
    dialog = new AddProjectDialog({
      element: Functions.createElement({
        type: "div"
      }),
      customMap: null,
      configuration: self.getConfiguration(),
      serverConnector: self.getServerConnector()
    });
    self._addDialog = dialog;
    dialog.addListener("onProjectAdd", function () {
      return self.onRefreshClicked()
    });
    return dialog.init().then(function () {
      return dialog.open();
    });
  } else {
    return dialog.clear().then(function () {
      return dialog.open();
    })
  }
};

/**
 *
 * @returns {Promise}
 */
MapsAdminPanel.prototype.destroy = function () {
  var promises = [];
  var self = this;
  var dialog = self._addDialog;
  if (dialog !== undefined) {
    promises.push(dialog.destroy());
  }
  var table = $("[name='projectsTable']", self.getElement())[0];
  if ($.fn.DataTable.isDataTable(table)) {
    promises.push($(table).DataTable().destroy());
  }

  var key;
  for (key in self._logDialogs) {
    if (self._logDialogs.hasOwnProperty(key)) {
      promises.push(self._logDialogs[key].destroy());
    }
  }
  for (key in self._dialogs) {
    if (self._dialogs.hasOwnProperty(key)) {
      promises.push(self._dialogs[key].destroy());
    }
  }

  dialog = self._registerInMinervaNetDialog;
  if (dialog !== undefined) {
    promises.push(dialog.destroy());
  }

  promises.push(self.getServerConnector().getProjects().then(function (projects) {
    var listenerName = "PROJECT_LIST_LISTENER";
    for (var j = 0; j < projects.length; j++) {
      var project = projects[j];
      var listeners = project.getListeners("onreload");
      for (var i = 0; i < listeners.length; i++) {
        if (listeners[i].listenerName === listenerName) {
          project.removeListener("onreload", listeners[i]);
        }
      }
    }
  }));

  return Promise.all(promises);
};

/**
 *
 * @returns {Promise}
 */
MapsAdminPanel.prototype.onRefreshClicked = function () {
  var self = this;
  return self.getServerConnector().getProjects(true).then(function (projects) {
    return self.setProjects(projects);
  }).then(function () {
    return self.getServerConnector().getLoggedUser();
  }).then(function (user) {
    var curatorPrivilege = self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR);
    var adminPrivilege = self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN);
    //we need to refresh users as well because of privileges
    if (user.hasPrivilege(curatorPrivilege) || user.hasPrivilege(adminPrivilege)) {
      return self.getServerConnector().getUsers(true);
    } else {
      return Promise.resolve();
    }
  });
};

/**
 *
 * @param {Project} project
 * @returns {*}
 */
MapsAdminPanel.prototype.getDialog = function (project) {
  var self = this;
  if (self._dialogs === undefined) {
    self._dialogs = [];
  }
  var dialog = self._dialogs[project.getProjectId()];
  if (dialog !== undefined) {
    dialog.destroy()
  }
  dialog = new EditProjectDialog({
    element: Functions.createElement({
      type: "div"
    }),
    project: project,
    configuration: self.getConfiguration(),
    customMap: null,
    serverConnector: self.getServerConnector()
  });
  self._dialogs[project.getProjectId()] = dialog;
  return dialog.init().then(function () {
    return dialog;
  });
};

/**
 *
 * @param {string} projectId
 * @returns {Promise<LogListDialog>}
 */
MapsAdminPanel.prototype.getLogDialog = function (projectId) {
  var self = this;
  if (self._logDialogs === undefined) {
    self._logDialogs = [];
  }
  var dialog = self._logDialogs[projectId];
  if (dialog === undefined) {
    dialog = new LogListDialog({
      element: Functions.createElement({
        type: "div"
      }),
      configuration: self.getConfiguration(),
      projectId: projectId,
      serverConnector: self.getServerConnector(),
      customMap: null
    });
    self._logDialogs[projectId] = dialog;
    return dialog.init().then(function () {
      return dialog;
    });
  } else {
    return Promise.resolve(dialog);
  }
};

/**
 *
 * @param {string} projectId
 * @returns {Promise<JobListDialog>}
 */
MapsAdminPanel.prototype.getJobDialog = function (projectId) {
  var self = this;
  if (self._jobDialogs === undefined) {
    self._jobDialogs = [];
  }
  var dialog = self._jobDialogs[projectId];
  if (dialog === undefined) {
    dialog = new JobListDialog({
      element: Functions.createElement({
        type: "div"
      }),
      configuration: self.getConfiguration(),
      projectId: projectId,
      serverConnector: self.getServerConnector(),
      customMap: null
    });
    self._jobDialogs[projectId] = dialog;
    return dialog.init().then(function () {
      return dialog;
    });
  } else {
    return Promise.resolve(dialog);
  }
};

/**
 *
 * @param {string} id projectId
 * @returns {Promise}
 */
MapsAdminPanel.prototype.showEditDialog = function (id) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getServerConnector().getProject(id).then(function (project) {
    if (project === null) {
      GuiConnector.warn("Project \"" + id + "\" does not exist");
    } else {
      return self.getDialog(project).then(function (dialog) {
        dialog.open();
      });
    }
  }).finally(function () {
    GuiConnector.hideProcessing();
  });
};

/**
 *
 * @param {string} id projectId
 * @returns {Promise<LogListDialog>}
 */
MapsAdminPanel.prototype.showLogs = function (id) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getLogDialog(id).then(function (dialog) {
    return dialog.open();
  }).finally(function () {
    GuiConnector.hideProcessing();
  });
};

/**
 *
 * @param {string} id projectId
 * @returns {Promise<JobListDialog>}
 */
MapsAdminPanel.prototype.showJobs = function (id) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getJobDialog(id).then(function (dialog) {
    return dialog.open();
  }).finally(function () {
    GuiConnector.hideProcessing();
  });
};

/**
 *
 * @param {string} id projectId
 * @returns {Promise}
 */
MapsAdminPanel.prototype.removeProject = function (id) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getServerConnector().removeProject(id).then(function () {
    return self.onRefreshClicked();
  }).finally(function () {
    GuiConnector.hideProcessing();
  });
};

module.exports = MapsAdminPanel;
