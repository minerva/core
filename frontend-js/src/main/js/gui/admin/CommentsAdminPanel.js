"use strict";

var $ = require('jquery');
var AbstractGuiElement = require('../AbstractGuiElement');

var Functions = require('../../Functions');
var GuiConnector = require('../../GuiConnector');
var PrivilegeType = require('../../map/data/PrivilegeType');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

// noinspection JSUnusedLocalSymbols
var Promise = require("bluebird");
var xss = require('xss');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {Configuration} params.configuration
 * @param {Project} params.project
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 * @extends AbstractGuiElement
 */
function CommentsAdminPanel(params) {
  params.customMap = null;
  AbstractGuiElement.call(this, params);

  this._createGui();
}

CommentsAdminPanel.prototype = Object.create(AbstractGuiElement.prototype);
CommentsAdminPanel.prototype.constructor = CommentsAdminPanel;

/**
 *
 * @private
 */
CommentsAdminPanel.prototype._createGui = function () {
  var self = this;

  var commentsTable = Functions.createElement({
    type: "table",
    name: "commentsTable",
    className: "display",
    style: "width:100%"
  });

  self.getElement().appendChild(commentsTable);

  $(commentsTable).DataTable({
    columns: [{
      title: 'Id'
    }, {
      title: 'Title'
    }, {
      title: 'Author'
    }, {
      title: 'Email'
    }, {
      title: 'Content'
    }, {
      title: 'Removed',
      orderable: false
    }, {
      title: 'Pinned'
    }]
  });

  $("[name='commentsTable']", self.getElement()).on("click", "[name='removeComment']", function () {
    var button = this;
    return self.askConfirmRemoval({
      title: "Why do you want to remove this comment?",
      input: true
    }).then(function (param) {
      if (param.status) {
        return ServerConnector.removeComment({
          commentId: $(button).attr("data"),
          reason: param.reason,
          projectId: self.getProject().getProjectId()
        }).then(function () {
          $(button).after("<span>YES (" + param.reason + ")</span>");
          button.style.display = "none";
        });
      }
    }).catch(GuiConnector.alert)
  });

};

/**
 *
 * @returns {Promise}
 */
CommentsAdminPanel.prototype.init = function () {
  var self = this;
  return self.refreshComments();
};

/**
 *
 * @returns {Promise}
 */
CommentsAdminPanel.prototype.refreshComments = function () {
  var self = this;
  var comments;

  return self.getServerConnector().getComments({
    projectId: self.getProject().getProjectId()
  }).then(function (result) {
    comments = result;
    return self.getServerConnector().getLoggedUser();
  }).then(function (user) {
    var curatorAccess = self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR);
    var writeAccess = self.getConfiguration().getPrivilegeType(PrivilegeType.WRITE_PROJECT);
    var adminAccess = self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN);
    var disable = true;
    if ((user.hasPrivilege(writeAccess, self.getProject().getProjectId()) && user.hasPrivilege(curatorAccess)) || user.hasPrivilege(adminAccess)) {
      disable = false;
    }

    var dataTable = $($("[name='commentsTable']", self.getElement())[0]).DataTable();
    var data = [];
    for (var i = 0; i < comments.length; i++) {
      data.push(self.commentToTableRow(comments[i], disable && comments[i].getAuthor() !== user.getLogin()));
    }
    dataTable.clear().rows.add(data).draw();
  });
};

/**
 *
 * @param {boolean} disable
 * @param {Comment} comment
 * @returns {string[]}
 */
CommentsAdminPanel.prototype.commentToTableRow = function (comment, disable) {
  var self = this;
  var projectId = self.getProject().getProjectId();
  var toYesNo = function (val) {
    if (val) {
      return "YES";
    } else {
      return "NO";
    }
  };
  var title = null;
  if (!comment.isRemoved()) {
    var commentLink = "index.xhtml?id=" + projectId +
      "&x=" + comment.getCoordinates().x +
      "&y=" + comment.getCoordinates().y +
      "&zoom=12" +
      "&comments=on";
    title = "<a href='" + commentLink + "' target='" + projectId + "'>" + xss(comment.getTitle()) + "</a>";
  } else {
    title = xss(comment.getTitle());
  }

  var disabled = "";
  if (disable) {
    disabled = " disabled ";
  }
  var remove = null;
  if (comment.isRemoved()) {
    remove = "YES (" + comment.getRemoveReason() + ")";
  } else {
    remove = "<button name='removeComment' data='" + comment.getId() + "'" + disabled + "><i class='fa fa-trash-alt'></button>";
  }

  var author = comment.getAuthor();
  if (author === undefined || author === null) {
    author = "N/A";
  }

  var email = comment.getEmail();
  if (email === undefined || email === null) {
    email = "N/A";
  }

  return [comment.getId(),
    title,
    xss(author),
    xss(email),
    xss(comment.getContent()),
    remove,
    toYesNo(comment.isPinned())];
};

/**
 *
 */
CommentsAdminPanel.prototype.destroy = function () {
  var self = this;
  var table = $("[name='commentsTable']", self.getElement())[0];
  if ($.fn.DataTable.isDataTable(table)) {
    $(table).DataTable().destroy();
  }
};


module.exports = CommentsAdminPanel;
