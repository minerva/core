"use strict";

var Promise = require("bluebird");
var $ = require('jquery');
var xss = require('xss');

/* exported logger */

var AbstractGuiElement = require('../AbstractGuiElement');

var Functions = require('../../Functions');
var GuiConnector = require('../../GuiConnector');
var ConfigurationType = require("../../ConfigurationType");


/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} [params.customMap]
 * @param {Configuration} params.configuration
 * @param {Project} [params.project]
 * @param {string} params.projectId
 * @param {ServerConnector} params.serverConnector
 *
 * @constructor
 * @extends AbstractGuiElement
 */
function RegisterInMinervaNetDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self.createDialogGui();
  self.registerListenerType("onRegister");
}

RegisterInMinervaNetDialog.prototype = Object.create(AbstractGuiElement.prototype);
RegisterInMinervaNetDialog.prototype.constructor = RegisterInMinervaNetDialog;

/**
 *
 */
RegisterInMinervaNetDialog.prototype.createDialogGui = function () {
  var self = this;

  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  var tableElement = Functions.createElement({
    type: "table",
    name: "detailsTable",
    className: "display",
    style: "width:100%"
  });

  var message = Functions.createElement({
    type: "div",
    style: "font-size:120%;",
    content: "<p><b><p>You are about to register this MINERVA instance at the MINERVA-NET server, located in the " +
      "infrastructure of the University of Luxembourg. This will store the URL of this machine at the server and " +
      "make it publicly visible. No information about projects is shared by this action. You will have to share " +
      "projects individually." +
      "<p>Your email will be registered at the MINERVA-NET server, but will not be shared. " +
      "It will be used only to send you warnings about MINERVA-NET connectivity issues, in case they happen." +
      "<p>By un-registering, these data will no longer be shared via MINERVA-NET. They will be stored internally " +
      "for performance analysis. If you want them to be removed completely, please send your request to minerva@uni.lu." +
      "</b>"
  });

  result.appendChild(tableElement);
  result.appendChild(message)


  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });
  result.appendChild(menuRow);

  var registerButton = Functions.createElement({
    type: "button",
    name: "register",
    content: '<span class="ui-icon ui-icon-disk"></span>&nbsp;REGISTER',
    onclick: function () {
      GuiConnector.showProcessing();
      var option = self.getConfiguration().getOption(ConfigurationType.MINERVA_ROOT);
      option.setValue($('[name="minervaRootUrl"]', self.getElement()).text());
      return self.getServerConnector().updateConfigurationOption(option).then(function () {
        return self.getServerConnector().getLoggedUser();
      }).then(function (user) {
        user.setEmail($('[name="userEmail"]', self.getElement()).val());
        return self.getServerConnector().updateUser(user);
      }).then(function () {
        return self.getServerConnector().registerInMinervaNet();
      }).then(function () {
        return self.callListeners("onRegister");
      }).then(function () {
        return self.close();
      }).catch(GuiConnector.alert).finally(function () {
        GuiConnector.hideProcessing();
      });
    },
    xss: false
  });
  menuRow.appendChild(registerButton);

  self.getElement().appendChild(result);

};


RegisterInMinervaNetDialog.prototype.open = function () {
  var self = this;
  var url = self.getConfiguration().getOption(ConfigurationType.MINERVA_ROOT).getValue();
  if (url === undefined || url === null || url === "") {
    GuiConnector.alert("Cannot join MINERVA-NET, please provide 'MINERVA Root URL' in 'Configuration > Server");
    return Promise.resolve();
  }
  if (!$(self.getElement()).hasClass("ui-dialog-content")) {
    $(self.getElement()).dialog({
      dialogClass: 'minerva-logs-dialog',
      title: "Log list",
      autoOpen: false,
      resizable: false,
      width: Math.max(window.innerWidth / 2, window.innerWidth - 100),
      height: Math.max(window.innerHeight / 2, window.innerHeight - 100)
    });
  }

  $(self.getElement()).dialog("open");

  return Promise.resolve();

};

/**
 *
 * @returns {Promise}
 */
RegisterInMinervaNetDialog.prototype.init = function () {
  var self = this;
  $("[name=detailsTable]", self.getElement()).DataTable({
    columns: [{
      title: "Name"
    }, {
      title: "Value"
    }],
    paging: false,
    ordering: false,
    searching: false,
    bInfo: false
  });

  return this.refresh();
};

RegisterInMinervaNetDialog.prototype.refresh = function () {
  var self = this;

  var dataTable = $("[name=detailsTable]", self.getElement()).DataTable();

  var data = [];

  return self.getServerConnector().getLoggedUser().then(function (user) {
    var email = getStringIfDefined(user.getEmail());
    var emailReadOnly = '';
    if (email !== "") {
      emailReadOnly = 'readonly';
    }
    var url = getStringIfDefined(self.getConfiguration().getOption(ConfigurationType.MINERVA_ROOT).getValue());
    var urlReadOnly = '';
    if (url === "") {
      url = self.getServerConnector().getServerBaseUrl();
    } else {
      urlReadOnly = 'readonly';
    }

    data.push(['Root URL', '<label name="minervaRootUrl" style="width:100%">' + url + '</label>']);
    data.push(['Email', '<input name="userEmail" ' + emailReadOnly + ' autocomplete="off"  value="' + email + '" style="width:100%"/>']);
    dataTable.clear().rows.add(data).draw();
  });
};


/**
 *
 * @return {Promise}
 */
RegisterInMinervaNetDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();
  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
  if ($.fn.DataTable.isDataTable($("[name=detailsTable]", self.getElement()))) {
    $("[name=detailsTable]", self.getElement()).DataTable().destroy();
  }
  return Promise.resolve();
};

RegisterInMinervaNetDialog.prototype.close = function () {
  var self = this;
  $(self.getElement()).dialog("close");
};

/**
 *
 * @param value
 * @returns {string}
 */
function getStringIfDefined(value) {
  if (value === undefined) {
    return "";
  }
  return xss(value);
}

module.exports = RegisterInMinervaNetDialog;
