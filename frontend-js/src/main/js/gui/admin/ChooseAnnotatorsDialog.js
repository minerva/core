"use strict";

/* exported logger */
var $ = require('jquery');

var AbstractAnnotatorsDialog = require('./AbstractAnnotatorsDialog');
var Annotator = require("../../map/data/Annotator");
var AnnotatorParameter = require("../../map/data/AnnotatorParameter");
var GuiConnector = require("../../GuiConnector");
var MultiCheckboxList = require("multi-checkbox-list");

var Functions = require('../../Functions');
var Promise = require('bluebird');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} [params.customMap]
 * @param {Configuration} params.configuration
 * @param {Project} [params.project]
 * @param {ServerConnector} params.serverConnector
 *
 * @constructor
 * @extends AbstractAnnotatorsDialog
 */
function ChooseAnnotatorsDialog(params) {
  AbstractAnnotatorsDialog.call(this, params);
  var self = this;
  self.createGui();
  self._selectedElementType = undefined;
}

ChooseAnnotatorsDialog.prototype = Object.create(AbstractAnnotatorsDialog.prototype);
ChooseAnnotatorsDialog.prototype.constructor = ChooseAnnotatorsDialog;

/**
 *
 */
ChooseAnnotatorsDialog.prototype.createGui = function () {
  var self = this;
  var content = Functions.createElement({
    type: "div",
    style: "display:table;height:100%;width:100%"
  });
  content.appendChild(Functions.createElement({
    type: "div",
    style: "display:table-cell;height:100%",
    content: "<div name='elementList' style='height:100%;padding:10px'/>",
    xss: false
  }));

  var annotatorsDiv = Functions.createElement({
    type: "div",
    style: "display:table-cell;width:100%;height:100%;position:relative",
    content: "<div style='height:100%;width:100%;overflow-y:auto;position:absolute;left:0;top:0'>" +
      "<div name='annotatorListBox'></div>" +
      "<div class='minerva-annotators-params'></div>" +
      "</div>",
    xss: false
  });
  content.appendChild(annotatorsDiv);

  self.getElement().appendChild(content);
};

/**
 *
 * @param {HTMLInputElement} element
 * @param {AnnotatorParameter} annotatorParameter
 * @param {Annotator} annotator
 * @returns {Promise}
 */
ChooseAnnotatorsDialog.prototype.onChangeParameterValue = function (element, annotatorParameter, annotator) {
  var self = this;
  GuiConnector.showProcessing();
  var elementType = undefined;
  var user;
  return self.getServerConnector().getLoggedUser().then(function (data) {
    user = data;
    if (annotatorParameter.getType() === "CONFIG") {
      if (element.type === 'checkbox') {
        annotator.setParameterValue(annotatorParameter, element.checked.toString());
      } else {
        annotator.setParameterValue(annotatorParameter, element.value);
      }
    } else {
      if (element.checked) {
        annotator.addParameter(new AnnotatorParameter(annotatorParameter));
      } else {
        annotator.removeParameter(annotatorParameter);
      }
    }
    for (var key in user.getPreferences()._elementAnnotators) {
      if (user.getPreferences()._elementAnnotators.hasOwnProperty(key)) {
        var annotators = user.getPreferences()._elementAnnotators[key];
        for (var i = 0; i < annotators.length; i++) {
          if (annotators[i] === annotator) {
            elementType = key;
          }
        }
      }
    }
    return self.getServerConnector().updateUserPreferences({
      user: user,
      preferences: user.getPreferences()
    });
  }).then(function () {
    //this is very hacky - it's due to bug in updating data from server when updatePreferences is finished
    //if we don't do it later changes will not be applied (https://git-r3lab.uni.lu/minerva/core/-/issues/1446)
    var annotators = user.getPreferences()._elementAnnotators[elementType];
    if (annotators !== undefined) {
      for (var i = 0; i < annotators.length; i++) {
        if (annotators[i]._className === annotator._className) {
          annotators[i] = annotator;
        }
      }
    }
  }).catch(GuiConnector.alert).finally(function () {

    GuiConnector.hideProcessing();
  });
};

/**
 *
 * @param {BioEntityType[]} elementTypes
 * @param {Annotator[]} selectedAnnotators
 * @returns {Promise}
 */
ChooseAnnotatorsDialog.prototype.saveAnnotatorsInfo = function (elementTypes, selectedAnnotators) {
  var self = this;
  selectedAnnotators = selectedAnnotators.slice();
  GuiConnector.showProcessing();
  return self.getServerConnector().getLoggedUser().then(function (user) {
    var elementAnnotators = user.getPreferences()._elementAnnotators;

    for (var i = 0; i < elementTypes.length; i++) {
      elementAnnotators[elementTypes[i].className] = selectedAnnotators;
    }
    return self.getServerConnector().updateUserPreferences({
      user: user,
      preferences: user.getPreferences()
    });
  }).finally(GuiConnector.hideProcessing).catch(GuiConnector.alert);
};

/**
 *
 * @param {BioEntityType} elementType
 * @returns {Promise}
 */
ChooseAnnotatorsDialog.prototype.setElementType = function (elementType) {
  var self = this;
  self._selectedElementType = elementType;

  var configuration = self.getConfiguration();

  return self.getServerConnector().getLoggedUser().then(function (user) {
    var element = $("[name='annotatorListBox']", self.getElement())[0];
    Functions.removeChildren(element);

    var selectElement = Functions.createElement({
      type: "div",
      style: "width:50%; height:200px;float:left; OVERFLOW-Y:scroll"
    });

    element.appendChild(selectElement);
    var copyFromButton = Functions.createElement({
      type: "button", content: "Copy from", onclick: function () {
        var typeClassName = copyFromSelect.value;
        var annotators;
        for (var i = 0; i < configuration.getElementTypes().length; i++) {
          var type = configuration.getElementTypes()[i];
          if (typeClassName === type.className) {
            annotators = user.getPreferences().getElementAnnotators(typeClassName);
          }
        }
        if (annotators === undefined) {
          return GuiConnector.alert("Invalid element type: " + copyFromSelect.value);
        } else {
          return self.saveAnnotatorsInfo(self.getAllChildrenTypesIfNeeded(elementType, false), annotators).then(function () {
            return self.setElementType(elementType);
          });
        }
      }
    });
    element.appendChild(Functions.createElement({
      type: "div",
      style: "position:absolute; right:20px; top:20px; width: 180px; border: 1px solid #efefef",
      content: "Please click on selected annotator to display details at the bottom"
    }));
    element.appendChild(copyFromButton);
    var copyFromSelect = Functions.createElement({type: "select", style: "margin:5px"});
    element.appendChild(copyFromSelect);
    var options = [], i;
    var nodeList = self.getTypeNodeList();
    for (i = 0; i < nodeList.length; i++) {
      var type = nodeList[i];
      options.push(Functions.createElement({
        type: "option",
        value: type.data.className,
        content: type.text
      }));
    }
    options.sort(function (a, b) {
      return a.text === b.text ? 0 : a.text < b.text ? -1 : 1
    });
    for (i = 0; i < options.length; i++) {
      copyFromSelect.appendChild(options[i]);
    }

    var annotators = configuration.getElementAnnotators(elementType);

    var selectedAnnotators = user.getPreferences().getElementAnnotators(elementType.className);

    var entries = [];

    for (i = 0; i < annotators.length; i++) {
      var annotator = annotators[i];
      var entry = {name: annotator.getName(), value: annotator.getClassName(), selected: false};
      for (var j = 0; j < selectedAnnotators.length; j++) {
        if (annotator.getClassName() === selectedAnnotators[j].getClassName()) {
          entry.selected = true;
        }
      }
      entries.push(entry);
    }
    entries.sort(function (entryA, entryB) {
      if (entryA.name < entryB.name) {
        return -1;
      }
      if (entryA.name > entryB.name) {
        return 1;
      }
      return 0;
    });
    var checkboxList = new MultiCheckboxList(selectElement, {
      entries: entries,
      listTitle: "Available annotators",
      selectedTitle: "Selected annotators",
      selectedList: true
    });
    var changeSelection = function (elementId, selected) {
      GuiConnector.showProcessing();
      return Promise.resolve().then(function () {

        var annotators = configuration.getElementAnnotators();
        var annotator;
        for (var i = 0; i < annotators.length; i++) {
          if (elementId === annotators[i].getClassName()) {
            annotator = new Annotator(annotators[i]);
          }
        }
        if (selected) {
          selectedAnnotators.push(annotator);
          self.createAnnotatorsParams(annotator);
        } else {
          var index = -1;
          for (var j = 0; j < selectedAnnotators.length; j++) {
            if (selectedAnnotators[j].getClassName() === annotator.getClassName()) {
              index = j;
            }
          }
          if (index > -1) {
            selectedAnnotators.splice(index, 1);
          }
          self.createAnnotatorsParams(undefined);
        }
        return self.saveAnnotatorsInfo(self.getAllChildrenTypesIfNeeded(elementType, false), selectedAnnotators);
      }).catch(GuiConnector.alert).finally(function () {
        GuiConnector.hideProcessing();
      });
    };

    checkboxList.addListener("select", function (element) {
      return changeSelection(element.value, true);
    });
    checkboxList.addListener("deselect", function (element) {
      return changeSelection(element.value, false);
    });


    self.createAnnotatorsParams(undefined);
  });

};

/**
 *
 * @param {AnnotatorParameter} param
 * @param {Annotator} annotator
 * @return {HTMLElement}
 */
ChooseAnnotatorsDialog.prototype.createParameterInput = function (param, annotator) {
  var self = this;
  if (param.getType() === "INPUT" || param.getType() === "OUTPUT" || param.getInputType().indexOf("Boolean") >= 0) {
    var result = Functions.createElement({
      type: "input",
      inputType: "checkbox",
      onchange: function () {
        return self.onChangeParameterValue(this, param, annotator);
      }
    });
    var checked = false;
    if (annotator.getParameterValue(param)) {
      checked = true;
    }
    result.checked = checked;
    return result;
  } else if (param.getInputType().indexOf("String") >= 0) {
    return Functions.createElement({
      type: "textarea",
      value: annotator.getParameterValue(param),
      onchange: function () {
        return self.onChangeParameterValue(this, param, annotator);
      }
    });
  } else if (param.getInputType().indexOf("Integer") >= 0) {
    return Functions.createElement({
      type: "input",
      value: annotator.getParameterValue(param),
      inputType: "number",
      onchange: function () {
        return self.onChangeParameterValue(this, param, annotator);
      }
    });
  } else {
    throw new Error("Unknown annotator parameter type: " + param.getInputType());
  }
};
/**
 *
 * @param {Annotator} selectedAnnotator
 */
ChooseAnnotatorsDialog.prototype.createAnnotatorsParams = function (selectedAnnotator) {
  var self = this;
  var annotatorsParams = $(".minerva-annotators-params", self.getElement())[0];
  Functions.removeChildren(annotatorsParams);

  if (selectedAnnotator !== undefined) {
    var annotator;
    for (var i = 0; i < self.getConfiguration().getAnnotators().length; i++) {
      if (self.getConfiguration().getAnnotators()[i].getClassName() === selectedAnnotator.getClassName()) {
        annotator = self.getConfiguration().getAnnotators()[i];
      }
    }

    var paramsDefs = annotator.getParametersDefinitions();
    annotatorsParams.appendChild(Functions.createElement({
      type: "div",
      className: "minerva-annotators-params-header",
      content: '<div>' + annotator.getName() + ' - Available parameters</div>'
    }));

    var inputParams = Functions.createElement({
      type: "div",
      className: "minerva-annotator-params"
    });

    inputParams.appendChild(Functions.createElement({
      type: "div",
      className: "minerva-annotator-params-header",
      content: '<div>Identify element by:</div>'
    }));

    var outputParams = Functions.createElement({
      type: "div",
      className: "minerva-annotator-params"
    });
    outputParams.appendChild(Functions.createElement({
      type: "div",
      className: "minerva-annotator-params-header",
      content: '<div>Provide information:</div>'
    }));


    var configParams = Functions.createElement({
      type: "div",
      className: "minerva-annotator-params"
    });
    configParams.appendChild(Functions.createElement({
      type: "div",
      className: "minerva-annotator-params-header",
      content: '<div>Config options:</div>'
    }));


    for (var k = 0; k < paramsDefs.length; k++) {
      var param = paramsDefs[k];
      var paramElement = Functions.createElement({
        type: "div",
        className: "minerva-annotator-param"
      });

      var paramName = Functions.createElement({
        type: "div",
        className: "minerva-annotator-param-name",
        content: self.getParameterName(param)
      });

      if (param.getDescription() !== undefined) {
        var tooltipContainer = Functions.createElement({
          type: "span"
        });
        tooltipContainer.appendChild(Functions.createElement({
          type: "span",
          className: "glyphicon glyphicon-question-sign tooltip-icon"
        }));
        tooltipContainer.appendChild(Functions.createElement({
          type: "span",
          className: "annotator-tooltip",
          content: param.getDescription()
        }));

        paramName.appendChild(tooltipContainer);
      }
      paramElement.appendChild(paramName);

      var paramValue;

      paramValue = self.createParameterInput(param, selectedAnnotator);
      if (param.getType() === "CONFIG") {
        configParams.appendChild(paramElement);
      } else if (param.getType() === "INPUT") {
        inputParams.appendChild(paramElement);
      } else if (param.getType() === "OUTPUT") {
        outputParams.appendChild(paramElement);
      } else {
        throw new Error("Unknown annotator parameter type");
      }

      paramElement.appendChild(paramValue);
    }
    annotatorsParams.appendChild(inputParams);
    annotatorsParams.appendChild(outputParams);
    annotatorsParams.appendChild(configParams);
  }
};


/**
 *
 * @returns {Promise}
 */
ChooseAnnotatorsDialog.prototype.init = function () {
  var self = this;
  return self.getServerConnector().getLoggedUser().then(function (user) {

    var configuration = self.getConfiguration();

    var types = self.getTypeNodeList();

    var element = $('[name="elementList"]', self.getElement())[0];
    var select = Functions.createElement({
      type: "select",
      style: "height:100%",
      onchange: function () {
        var className = $(this).find(":selected").attr("data");
        var type;
        for (var i = 0; i < types.length; i++) {
          if (types[i].data !== undefined && types[i].data.className === className) {
            type = types[i];
          }
        }
        return self.setElementType(type.data);
      }
    });
    select.size = 15;
    for (var i = 0; i < types.length; i++) {
      var type = types[i];
      var option = Functions.createElement({
        type: "option",
        content: type.text
      });
      $(option).attr("data", type.data.className);
      select.appendChild(option);
      for (var j = 0; j < configuration.getBioEntityTypes().length; j++) {
        var bioEntityType = configuration.getBioEntityTypes()[j];
        if (bioEntityType.parentClass === type.data.className) {
          self.copyAnnotatorsToChildren(type.data, bioEntityType, user);
        }
      }
    }
    element.appendChild(select);

    $(self.getElement()).on("click", ".multi-checkbox-list-selected-entry", function () {
      var div = this;
      return self.getServerConnector().getLoggedUser().then(function (user) {
        var annotatorClassName, i;
        for (i = 0; i < self.getConfiguration().getAnnotators().length; i++) {
          if (self.getConfiguration().getAnnotators()[i].getName() === div.innerHTML) {
            annotatorClassName = self.getConfiguration().getAnnotators()[i].getClassName();
          }
        }
        if (annotatorClassName === undefined) {
          throw new Error("Annotator cannot be found: " + div.innerHTML);
        }

        var annotators = user.getPreferences().getElementAnnotators(self._selectedElementType.className);
        var annotator;
        for (i = 0; i < annotators.length; i++) {
          if (annotatorClassName === annotators[i].getClassName()) {
            annotator = annotators[i];
          }
        }
        return self.createAnnotatorsParams(annotator);
      }).catch(GuiConnector.alert);
    });
  })
};

/**
 *
 */
ChooseAnnotatorsDialog.prototype.destroy = function () {
  $(this.getElement()).dialog("destroy");
};

/**
 *
 */
ChooseAnnotatorsDialog.prototype.open = function () {
  var self = this;
  var div = self.getElement();
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      dialogClass: 'minerva-select-annotator-dialog',
      title: "Select annotators",
      modal: true,
      width: Math.min(Math.max(840, window.innerWidth / 2), window.innerWidth),
      height: window.innerHeight / 2

    });
  }
  $(div).dialog("open");
  $(div).css("overflow", "hidden");
};

/**
 *
 * @param {AnnotatorParameter} parameter
 */
ChooseAnnotatorsDialog.prototype.getParameterName = function (parameter) {
  var result;
  var field = this.getConfiguration().getBioEntityFieldByName(parameter.getField());
  if (parameter.getType() === "CONFIG") {
    result = parameter.getCommonName();
  } else if (parameter.getField() !== undefined && parameter.getField() !== '' &&
    parameter.getAnnotationType() !== undefined && parameter.getAnnotationType() !== '') {
    result = "Element '" + field.getCommonName() + "' as " + this.getConfiguration().getMiriamTypeByName(parameter.getAnnotationType()).getCommonName();
  } else if (parameter.getField() !== undefined && parameter.getField() !== '') {
    result = "Element '" + field.getCommonName() + "'";
  } else if (parameter.getAnnotationType() !== undefined && parameter.getAnnotationType() !== '') {
    result = this.getConfiguration().getMiriamTypeByName(parameter.getAnnotationType()).getCommonName();
  } else {
    throw new Error("Undefined name for parameter");
  }
  return result;
};

/**
 *
 * @param {BioEntityType} from
 * @param {BioEntityType} to
 * @param {User} user
 */
ChooseAnnotatorsDialog.prototype.copyAnnotatorsToChildren = function (from, to, user) {
  if (user.getPreferences()._elementAnnotators[from.className] === undefined) {
    logger.warn("Cannot copy annotator from " + from.className + " to " + to.className + ". Annotators don't exist");
    user.getPreferences()._elementAnnotators[from.className] = user.getPreferences().getElementAnnotators(to.className);
  }
  user.getPreferences()._elementAnnotators[to.className] = user.getPreferences().getElementAnnotators(from.className);
};
module.exports = ChooseAnnotatorsDialog;
