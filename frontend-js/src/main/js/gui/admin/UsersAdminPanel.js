"use strict";

var $ = require('jquery');

var Promise = require('bluebird');

var PrivilegeType = require('../../map/data/PrivilegeType');

var AbstractAdminPanel = require('./AbstractAdminPanel');
var EditUserDialog = require('./EditUserDialog');

var User = require("../../map/data/User");

var Functions = require('../../Functions');
var GuiConnector = require('../../GuiConnector');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
var ConfigurationType = require("../../ConfigurationType");

/**
 *
 * @param {Object} params
 *
 * @constructor
 * @extends AbstractAdminPanel
 */
function UsersAdminPanel(params) {
  var self = this;
  AbstractAdminPanel.call(self, params);
  self._createGui();
  $(self.getElement()).addClass("minerva-users-tab");
}

UsersAdminPanel.prototype = Object.create(AbstractAdminPanel.prototype);
UsersAdminPanel.prototype.constructor = UsersAdminPanel;

/**
 *
 * @private
 */
UsersAdminPanel.prototype._createGui = function () {
  var self = this;
  var usersDiv = Functions.createElement({
    type: "div"
  });
  self.getElement().appendChild(usersDiv);

  var dataDiv = Functions.createElement({
    type: "div",
    style: "display:table; width:100%"
  });
  usersDiv.appendChild(dataDiv);

  dataDiv.appendChild(self._createMenuRow());
  dataDiv.appendChild(self._createUsersTableRow());
  dataDiv.appendChild(self._createMenuRow());


};

/**
 *
 * @returns {HTMLElement}
 * @private
 */
UsersAdminPanel.prototype._createMenuRow = function () {
  var self = this;
  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });

  var addUserButton = Functions.createElement({
    type: "button",
    name: "addUser",
    content: '<span class="ui-icon ui-icon-circle-plus"></span>&nbsp;ADD USER',
    onclick: function () {
      return self.onAddClicked().then(null, GuiConnector.alert);
    },
    xss: false
  });
  var refreshButton = Functions.createElement({
    type: "button",
    name: "refreshUser",
    content: '<span class="ui-icon ui-icon-refresh"></span>&nbsp;REFRESH',
    onclick: function () {
      return self.onRefreshClicked().then(null, GuiConnector.alert);
    },
    xss: false
  });
  menuRow.appendChild(addUserButton);
  menuRow.appendChild(refreshButton);
  return menuRow;
};

/**
 *
 * @returns {HTMLElement}
 * @private
 */
UsersAdminPanel.prototype._createUsersTableRow = function () {
  var self = this;
  var projectsRow = Functions.createElement({
    type: "div",
    style: "display:table-row; width:100%"
  });

  var usersTable = Functions.createElement({
    type: "table",
    name: "usersTable",
    className: "display",
    style: "width:100%"
  });
  projectsRow.appendChild(usersTable);

  // noinspection JSCheckFunctionSignatures
  $(usersTable).DataTable({
    fnRowCallback: function (nRow, aData) {
      var firstColumn = aData[0];
      var login = firstColumn;
      if (typeof firstColumn === 'string' || firstColumn instanceof String) {
        login = firstColumn.split(' ')[0];
      }
      nRow.setAttribute('id', login);
    },
    columns: [{
      title: 'Login'
    }, {
      title: 'Name'
    }, {
      title: 'Surname'
    }, {
      title: 'Email'
    }, {
      title: 'Email confirmed'
    }, {
      title: 'Authentication'
    }, {
      title: 'Account active'
    }, {
      title: 'Edit'
    }, {
      title: 'Remove'
    }],
    columnDefs: [
      {"orderable": false, "targets": [7, 8]}
    ]
  });

  self.bindDataTablePageLengthToUserPreference({
    element: usersTable,
    preferenceName: 'admin-users-datatable-length'
  });

  $(usersTable).on("click", "[name='removeUser']", function () {
    var button = this;
    var login = $(button).attr("data");
    var loggedUser;
    return self.getServerConnector().getLoggedUser().then(function (user) {
      loggedUser = user;

      return self.askConfirmRemoval({
        title: "INFO",
        content: "Do you really want to remove '" + login + "'user?",
        input: false
      });
    }).then(function (param) {
      if (param.status) {
        var projects;
        return self.getUserProjects(login).then(function (result) {
          projects = result;
          return self.askProjectRemovalBehaviour(projects)
        }).then(function (result) {
          var promises = [];
          if (result.status === 'cancel') {
            //continue
          } else if (result.status === 'remove') {
            for (var j = 0; j < projects.length; j++) {
              promises.push(ServerConnector.removeProject(projects[j].getProjectId()));
            }
            return Promise.all(promises).then(function () {
              return self.removeUser(login);
            });
          } else if (result.status === 'reassign') {
            for (var i = 0; i < projects.length; i++) {
              projects[i].setOwner(loggedUser.getLogin());
              promises.push(ServerConnector.updateProject(projects[i]));
            }
            return Promise.all(promises).then(function () {
              return self.removeUser(login);
            });
          } else if (result.status === 'orphan') {
            return self.removeUser(login);
          } else {
            throw new Error('Unknown status: ' + result.status);
          }
        });
      }
    }).catch(GuiConnector.alert);
  });

  $(usersTable).on("click", "[name='ldap-auth']", function () {
    var field = this;
    var login = $(this).attr('data');
    GuiConnector.showProcessing();
    return self.getServerConnector().getUser(login).then(function (user) {
      var newIsConnected = $('input:checked', field).val() === "LDAP";
      var isConnected = user.isConnectedToLdap();
      if (isConnected !== newIsConnected) {
        user.setConnectedToLdap(newIsConnected);
        return Promise.all([user.callListeners("onreload"), self.getServerConnector().updateUser(user)]);
      }
    }).catch(function (error) {
      GuiConnector.alert(error);
    }).finally(function () {
      GuiConnector.hideProcessing();
    });
  });

  $(usersTable).on("click", "[name='showEditDialog']", function () {
    var button = this;
    return self.showEditDialog($(button).attr("data")).then(null, GuiConnector.alert);
  });

  return projectsRow;
};

/**
 *
 * @param {array<Project>} projects
 * @returns {Promise}
 */
UsersAdminPanel.prototype.askProjectRemovalBehaviour = function (projects) {
  return new Promise(function (resolve) {
    var content = 'What should happen with the following projects owned by the user:<ul>';
    for (var i = 0; i < projects.length; i++) {
      content += "<li>" + projects[i].getProjectId() + "</li>";
    }
    content += "</ul>";
    var html = Functions.createElement({type: 'div', content: content});
    $(html).dialog({
      modal: true,
      title: 'There are projects owned by the user.',
      close: function () {
        $(this).dialog('destroy').remove();
        resolve({status: "cancel"});
      },
      buttons: {
        'Remove': function () {
          var reason = $('input[name="reason-value"]').val();
          $(this).dialog('destroy').remove();
          resolve({status: "remove"});
        },
        'Reassign': function () {
          $(this).dialog('destroy').remove();
          resolve({status: "reassign"});
        },
        'Orphan': function () {
          $(this).dialog('destroy').remove();
          resolve({status: "orphan"});
        }
      }
    });
  })
};

/**
 *
 * @param {string} login
 * @return Promise<Project>
 */
UsersAdminPanel.prototype.getUserProjects = function (login) {
  return ServerConnector.getProjects().then(function (projects) {
    var result = [];
    for (var i = 0; i < projects.length; i++) {
      if (projects[i].getOwner() === login) {
        result.push(projects[i]);
      }
    }
    return result;
  });
}

/**
 *
 * @param {String} login
 * @returns {Promise}
 */
UsersAdminPanel.prototype.showEditDialog = function (login) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getServerConnector().getUser(login).then(function (user) {
    return self.getDialog(user);
  }).then(function (dialog) {
    dialog.open();
    GuiConnector.hideProcessing();
  }).then(null, function (error) {
    GuiConnector.hideProcessing();
    return Promise.reject(error);
  });
};

/**
 *
 * @param {User} user
 * @returns {Promise}
 */
UsersAdminPanel.prototype.getDialog = function (user) {
  var self = this;
  if (self._dialogs === undefined) {
    self._dialogs = [];
  }
  var dialog = self._dialogs[user.getLogin()];
  if (dialog === undefined) {
    dialog = new EditUserDialog({
      element: Functions.createElement({
        type: "div"
      }),
      user: user,
      configuration: self.getConfiguration(),
      serverConnector: self.getServerConnector()
    });
    if (user.getLogin() === undefined) {
      dialog.addListener("onSave", function () {
        return self.onRefreshClicked().then(function () {
          return dialog.destroy();
        });
      });
      self._dialogs["new user"] = dialog;
    } else {
      self._dialogs[user.getLogin()] = dialog;
    }
    return dialog.init().then(function () {
      return dialog;
    });
  } else {
    return dialog.refresh().then(function () {
      return dialog;
    });
  }
};

/**
 *
 * @returns {Promise}
 */
UsersAdminPanel.prototype.init = function () {
  var self = this;
  return AbstractAdminPanel.prototype.init.call(this).then(function () {
    return self.getServerConnector().getLoggedUser();
  }).then(function (user) {
    var privilege = self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN);
    if (user.hasPrivilege(privilege)) {
      return self.getServerConnector().getUsers().then(function (users) {
        return self.setUsers(users);
      });
    } else {
      self.disablePanel("You have no privilege to manage users");
    }

  }).then(function () {
    var usersTable = $("[name='usersTable']", self.getElement())[0];

    return self.bindDataTableOrderToUserPreference({
      element: usersTable,
      preferenceName: 'admin-users-datatable-order'
    });
  });
};

/**
 *
 * @param {User[]} users
 */
UsersAdminPanel.prototype.setUsers = function (users) {
  var self = this;
  var dataTable = $($("[name='usersTable']", self.getElement())[0]).DataTable();
  var page = dataTable.page();
  var data = [];
  for (var i = 0; i < users.length; i++) {
    var user = users[i];
    var rowData = self.userToTableRow(user);
    data.push(rowData);
    self.addUpdateListener(user);
  }
  //it should be simplified, but I couldn't make it work
  dataTable.clear().rows.add(data).page(page).draw(false).page(page).draw(false);
};


/**
 *
 * @param {User} user
 */
UsersAdminPanel.prototype.addUpdateListener = function (user) {
  var self = this;

  var listenerName = "USER_LIST_LISTENER";
  var listeners = user.getListeners("onreload");
  for (var i = 0; i < listeners.length; i++) {
    if (listeners[i].listenerName === listenerName) {
      user.removeListener("onreload", listeners[i]);
    }
  }
  var listener = function () {
    var table = $("[name='usersTable']", self.getElement())[0];
    if (!$.fn.DataTable.isDataTable(table)) {
      return;
    }

    var login = user.getLogin();

    var dataTable = $("[name='usersTable']", self.getElement()).DataTable();
    var length = dataTable.data().length;
    for (var i = 0; i < length; i++) {
      var row = dataTable.row(i);
      var data = row.data();
      var rowLogin = data[0];
      if (typeof rowLogin === 'string' || rowLogin instanceof String) {
        rowLogin = rowLogin.split(' ')[0];
      }
      if (rowLogin === login) {
        self.userToTableRow(user, data);
        var page = dataTable.page();
        row.data(data).draw();
        dataTable.page(page).draw(false);
      }
    }
  };
  listener.listenerName = listenerName;
  user.addListener("onreload", listener);
};


/**
 *
 * @param {Date} before
 * @param {Date} after
 * @returns {int}
 */
function getTimeDifferenceInMinutes(before, after) {
  return Math.floor((after - before) / 1000) / 60;
}

/**
 *
 * @param {User} user
 * @param {Array} [row]
 * @returns {Array}
 */
UsersAdminPanel.prototype.userToTableRow = function (user, row) {
  if (row === undefined) {
    row = [];
  }

  row[0] = user.getLogin();
  var color = "gray";
  var title = "User is not logged in"
  if (user.getLastActive() !== null && user.getLastActive() !== undefined) {
    var diff = getTimeDifferenceInMinutes(user.getLastActive(), new Date());
    var maxSeconds = this.getConfiguration().getOption(ConfigurationType.SESSION_LENGTH).getValue();
    if (diff * 60 > maxSeconds) {
      color = "gray";
      title = "User is not logged in"
    } else {
      color = "red";
      title = "Logged user is inactive"
      if (diff < 15) {
        title = "Logged user is active";
        color = "green";
      } else if (diff < 30) {
        color = "orange";
        title = "Logged user is becoming inactive";
      }
    }
  }
  var activityTag = '<span style="height: 10px;width: 10px;background-color: ' + color + ';border-radius: 50%;display: inline-block;" title="' + title + '"></span>';
  if (user.getLogin() !== "anonymous") {
    row[0] += " " + activityTag;
  }
  row[1] = user.getName();
  row[2] = user.getSurname();
  row[3] = user.getEmail();
  row[4] = user.isConfirmed() ? "YES" : "NO";
  var ldapFieldId = 'ldap-auth-' + user.getLogin();
  if (user.isConnectedToLdap()) {
    row[5] = "<fieldset name='ldap-auth' id='" + ldapFieldId + "' data='" + user.getLogin() + "'> <input type='radio' name='" + ldapFieldId + "' value='LOCAL'> LOCAL <input type='radio' name='" + ldapFieldId + "' checked value='LDAP'> LDAP </fieldset>";
  } else {
    if (user.isLdapAccountAvailable()) {
      row[5] = "<fieldset name='ldap-auth' id='" + ldapFieldId + "' data='" + user.getLogin() + "'> <input type='radio' name='" + ldapFieldId + "' value='LOCAL' checked> LOCAL <input type='radio' name='" + ldapFieldId + "' value='LDAP'> LDAP </fieldset>";
      // row[4] = "LOCAL <button name='connectLdap' data='" + user.getLogin() + "'>CONNECT LDAP</button>"
    } else {
      row[5] = "<fieldset name='ldap-auth' id='" + ldapFieldId + "' data='" + user.getLogin() + "'> <input type='radio' name='" + ldapFieldId + "' value='LOCAL' checked> LOCAL</fieldset>";
    }
  }

  row[6] = user.isActive() ? "YES" : "NO";
  row[7] = "<button name='showEditDialog' data='" + user.getLogin() + "'><i class='fa fa-edit'></i></button>";
  var disabled = "";
  if (user.getLogin() === "anonymous") {
    disabled = " disabled ";
  }
  row[8] = "<button name='removeUser' " + disabled + " data='" + user.getLogin() + "'><i class='fa fa-trash-alt'></i></button>";

  return row;
};

/**
 *
 * @returns {Promise}
 */
UsersAdminPanel.prototype.onRefreshClicked = function () {
  var self = this;
  return self.getServerConnector().getUsers(true).then(function (users) {
    return self.setUsers(users);
  });
};


/**
 *
 */
UsersAdminPanel.prototype.destroy = function () {
  var self = this;
  var table = $("[name='usersTable']", self.getElement())[0];
  if ($.fn.DataTable.isDataTable(table)) {
    $(table).DataTable().destroy();
  }

  for (var key in self._dialogs) {
    if (self._dialogs.hasOwnProperty(key)) {
      self._dialogs[key].destroy();
    }
  }
  return self.getServerConnector().getUsers().then(function (users) {
    var listenerName = "USER_LIST_LISTENER";
    for (var j = 0; j < users.length; j++) {
      var user = users[j];
      var listeners = user.getListeners("onreload");
      for (var i = 0; i < listeners.length; i++) {
        if (listeners[i].listenerName === listenerName) {
          user.removeListener("onreload", listeners[i]);
        }
      }
    }
  });
};

/**
 *
 * @returns {Promise}
 */
UsersAdminPanel.prototype.onAddClicked = function () {
  var self = this;
  var user = new User({});
  GuiConnector.showProcessing();
  return self.getDialog(user).then(function (dialog) {
    dialog.open();
    GuiConnector.hideProcessing();
  }).then(null, function (error) {
    GuiConnector.hideProcessing();
    return Promise.reject(error);
  });
};

/**
 *
 * @param {string} login
 * @returns {Promise}
 */
UsersAdminPanel.prototype.removeUser = function (login) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getServerConnector().removeUser(login).then(function () {
    return self.onRefreshClicked();
  }).then(function () {
    GuiConnector.hideProcessing();
  }).then(null, function (error) {
    GuiConnector.hideProcessing();
    return Promise.reject(error);
  });
};

module.exports = UsersAdminPanel;
