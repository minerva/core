"use strict";

var $ = require('jquery');
var Promise = require("bluebird");
var xss = require('xss');

var AbstractGuiElement = require('../AbstractGuiElement');
var GuiConnector = require('../../GuiConnector');
var ValidationError = require('../../ValidationError');
var NetworkError = require('../../NetworkError');
var ObjectExistsError = require('../../ObjectExistsError');

var Functions = require('../../Functions');
var PrivilegeType = require('../../map/data/PrivilegeType');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var guiUtils = new (require('../leftPanel/GuiUtils'))();

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {User} params.user
 * @param {Configuration} params.configuration
 * @param {ServerConnector} params.serverConnector
 *
 * @constructor
 * @extends {AbstractGuiElement}
 */
function EditUserDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self.setUser(params.user);

  $(self.getElement()).css({overflow: "hidden"});

  self.createGui();
  self.registerListenerType("onSave");
}

EditUserDialog.prototype = Object.create(AbstractGuiElement.prototype);
EditUserDialog.prototype.constructor = EditUserDialog;

/**
 *
 * @param {User} user
 */
EditUserDialog.prototype.setUser = function (user) {
  this._user = user;
  this.setIsNewUser(user.getLogin() === undefined);
};

/**
 *
 * @returns {User}
 */
EditUserDialog.prototype.getUser = function () {
  return this._user;
};

/**
 *
 * @param {boolean} isNewUser
 */
EditUserDialog.prototype.setIsNewUser = function (isNewUser) {
  this._isNewUser = isNewUser;
};

/**
 *
 * @returns {boolean}
 */
EditUserDialog.prototype.getIsNewUser = function () {
  return this._isNewUser;
};

/**
 *
 */
EditUserDialog.prototype.createGui = function () {
  var self = this;

  guiUtils.initTabContent(self);

  guiUtils.addTab(self, {
    name: "DETAILS",
    content: self.createGeneralTabContent()
  });
  guiUtils.addTab(self, {
    name: "PROJECT PRIVILEGES",
    content: self.createProjectsTabContent()
  });

};

/**
 *
 * @param value
 * @returns {string}
 */
function getStringIfDefined(value) {
  if (value === undefined) {
    return "";
  }
  return xss(value);
}

/**
 *
 * @returns {HTMLElement}
 */
EditUserDialog.prototype.createGeneralTabContent = function () {

  var self = this;

  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  var table = Functions.createElement({
    type: "table",
    name: "detailsTable",
    className: "display",
    style: "width:100%"
  });
  result.appendChild(table);

  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });
  result.appendChild(menuRow);

  var saveUserButton = Functions.createElement({
    type: "button",
    name: "saveUser",
    content: '<span class="ui-icon ui-icon-disk"></span>&nbsp;SAVE',
    onclick: function () {
      GuiConnector.showProcessing();
      return self.onSaveClicked().then(function () {
        return self.close();
      }).catch(GuiConnector.alert).finally(function () {
        GuiConnector.hideProcessing();
      });
    },
    xss: false
  });
  var cancelButton = Functions.createElement({
    type: "button",
    name: "cancelProject",
    content: '<span class="ui-icon ui-icon-cancel"></span>&nbsp;CANCEL',
    onclick: function () {
      return self.close();
    },
    xss: false
  });
  menuRow.appendChild(saveUserButton);
  menuRow.appendChild(cancelButton);

  if (!self.getIsNewUser()) {
    $(".minerva-menu-row", result).hide();
  }

  $(result).on('change', '[name="userName"]', function () {
    if (!self.getIsNewUser()) {
      self.getUser().setName($(this).val());
      return self.updateUser();
    }
  });

  $(result).on('change', '[name="userSurname"]', function () {
    if (!self.getIsNewUser()) {
      self.getUser().setSurname($(this).val());
      return self.updateUser();
    }
  });

  $(result).on('change', '[name="userEmail"]', function () {
    if (!self.getIsNewUser()) {
      self.getUser().setEmail($(this).val());
      return self.updateUser();
    }
  });

  $(result).on('change', '[name="userActive"]', function () {
    if (!self.getIsNewUser()) {
      self.getUser().setActive($(this).is(':checked'));
      return self.updateUser();
    }
  });

  $(result).on('change', '[name^="userOrcidId"]', function () {
    if (!self.getIsNewUser()) {
      self.getUser().setOrcidId($(this).val());
      return self.updateUser("Orcid ID");
    }
  });

  $(result).on('change', '[name^="userPassword"]', function () {
    if (!self.getIsNewUser()) {
      if (self.getPassword() !== self.getPassword2()) {
        $('[name^="userPassword"]', self.getElement()).css("background-color", "red");
      } else {
        $('[name^="userPassword"]', self.getElement()).css("background-color", "");
        self.getUser().setPassword($(this).val());
        return self.updateUser();
      }
    }
  });

  $(result).on("click", "[name='user-role-" + self.getUser().getLogin() + "']", function () {
      var role = $('[name="user-role-' + self.getUser().getLogin() + '"]:checked', result).val();
      var promises = [];
      if (role === "ADMIN") {
        promises.push(self.grantPrivilege({privilegeType: PrivilegeType.IS_ADMIN}));
        promises.push(self.revokePrivilege({privilegeType: PrivilegeType.IS_CURATOR}));
        guiUtils.disableTab($(".minerva-project-privileges-tab", self.getElement())[0], "Admin can do everything")
      } else if (role === "CURATOR") {
        promises.push(self.revokePrivilege({privilegeType: PrivilegeType.IS_ADMIN}));
        promises.push(self.grantPrivilege({privilegeType: PrivilegeType.IS_CURATOR}));
        guiUtils.enableTab($(".minerva-project-privileges-tab", self.getElement())[0]);
      } else if (role === "USER") {
        promises.push(self.revokePrivilege({privilegeType: PrivilegeType.IS_ADMIN}));
        promises.push(self.revokePrivilege({privilegeType: PrivilegeType.IS_CURATOR}));
        guiUtils.enableTab($(".minerva-project-privileges-tab", self.getElement())[0]);
      } else {
        GuiConnector.alert("Invalid role: " + role);
      }
      return Promise.all(promises).then(function () {
        return self.getServerConnector().getLoggedUser();
      }).then(function (loggedUser) {
        if (loggedUser.getLogin() === self.getUser().getLogin()) {
          window.location.reload(false);
        }
      });
    }
  );
  return result;
};

/**
 *
 * @returns {HTMLElement}
 */
EditUserDialog.prototype.createProjectsTabContent = function () {
  var self = this;
  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;",
    className: "minerva-project-privileges-tab"
  });
  result.appendChild(self._createProjectsTable());
  return result;
};

/**
 *
 * @returns {HTMLElement}
 * @private
 */
EditUserDialog.prototype._createProjectsTable = function () {
  var result = Functions.createElement({
    type: "div",
    style: "margin-top:10px;"
  });

  result.appendChild(Functions.createElement({
    type: "div",
    name: "defaultProjectsRow",
    style: "width:100%"
  }));

  result.appendChild(Functions.createElement({
    type: "table",
    name: "projectsTable",
    className: "display",
    style: "width:100%"
  }));

  return result;
};

/**
 *
 * @returns {Promise}
 */
EditUserDialog.prototype.init = function () {
  var self = this;

  var detailsTable = $("[name=detailsTable]", self.getElement())[0];

  $(detailsTable).DataTable({
    columns: [{
      title: "Name"
    }, {
      title: "Value"
    }],
    paging: false,
    ordering: false,
    searching: false,
    bInfo: false
  });

  self.initProjectsTab();

  return self.refresh().then(function () {
    $(window).trigger('resize');
  });
};

EditUserDialog.prototype.refresh = function () {
  var self = this;

  var dataTable = $("[name=detailsTable]", self.getElement()).DataTable();

  var data = [];

  var user = self.getUser();

  var readonly = '';
  if (user.getLogin() !== undefined) {
    readonly = ' readonly ';
  }

  data.push(['Login', '<input name="userLogin" value="' + getStringIfDefined(user.getLogin()) + '" ' + readonly + '/>']);
  data.push(['Active', '<input name="userActive" type="checkbox"  ' + (user.isActive() ? "checked" : "") + '/>']);
  data.push(['Password', '<input type="password" name="userPassword" value=""/>']);
  data.push(['Confirm password', '<input type="password" name="userPassword2" value=""/>']);
  data.push(['Name', '<input name="userName" value="' + getStringIfDefined(user.getName()) + '"/>']);
  data.push(['Surname', '<input name="userSurname" value="' + getStringIfDefined(user.getSurname()) + '"/>']);
  data.push(['Email', '<input name="userEmail" value="' + getStringIfDefined(user.getEmail()) + '"/>']);
  data.push(['Orcid ID', '<input name="userOrcidId" value="' + getStringIfDefined(user.getOrcidId()) + '"/>']);

  var userRoleId = 'user-role-' + user.getLogin();

  var adminChecked = '';
  var curatorChecked = '';
  var userChecked = '';

  if (user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN))) {
    adminChecked = " checked ";
    guiUtils.disableTab($(".minerva-project-privileges-tab", self.getElement())[0], "Admin can do everything");
  } else if (user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR))) {
    curatorChecked = " checked ";
  } else {
    userChecked = " checked ";
  }
  data.push(["Role",
    "<fieldset name='userRole' id='" + userRoleId + "'> " +
    "<input type='radio' name='" + userRoleId + "' value='ADMIN' " + adminChecked + "> ADMIN<br/>" +
    "<input type='radio' name='" + userRoleId + "' value='CURATOR' " + curatorChecked + "> CURATOR<br/>" +
    "<input type='radio' name='" + userRoleId + "' value='USER' " + userChecked + "> USER <br/>" +
    "</fieldset>"]);

  dataTable.clear().rows.add(data).draw();

  return self.refreshProjects();
};

/**
 *
 */
EditUserDialog.prototype.initProjectsTab = function () {
  var self = this;

  var projectsTable = $("[name=projectsTable]", self.getElement())[0];

  var columns = self.createUserPrivilegeColumns();
  $(projectsTable).DataTable({
    columns: columns
  });
  $(self.getElement()).on("click", "[name='project-privilege-checkbox']", function () {
    var data = $(this).attr("data").split(":");
    var type = data[0];
    var projectId = data[1];
    if ($(this).is(":checked")) {
      if (type === PrivilegeType.WRITE_PROJECT) {
        var readCheckbox = $("[data='" + PrivilegeType.READ_PROJECT + ":" + data[1] + "']", self.getElement());
        if (!readCheckbox.is(":checked")) {
          readCheckbox.click();
        }
      }
      return self.grantPrivilege({privilegeType: type, objectId: projectId});
    } else {
      if (type === PrivilegeType.READ_PROJECT) {
        var writeCheckbox = $("[data='" + PrivilegeType.WRITE_PROJECT + ":" + data[1] + "']", self.getElement());
        if (writeCheckbox.is(":checked")) {
          writeCheckbox.click();
        }
      }
      return self.revokePrivilege({privilegeType: type, objectId: projectId});
    }
  });
};

/**
 *
 * @param {Authority} privilege
 * @returns {Promise}
 */
EditUserDialog.prototype.grantPrivilege = function (privilege) {
  var self = this;
  if (self._isNewUser) {
    self.getUser().setPrivilege(privilege);
    return Promise.resolve();
  } else {
    GuiConnector.showProcessing();
    return self.getServerConnector().grantUserPrivileges({
      user: self.getUser(),
      privileges: [privilege]
    }).catch(GuiConnector.alert).finally(GuiConnector.hideProcessing);
  }
};

/**
 *
 * @param {Authority} privilege
 * @returns {Promise}
 */
EditUserDialog.prototype.revokePrivilege = function (privilege) {
  var self = this;
  if (self._isNewUser) {
    self.getUser().revokePrivilege(privilege);
    return Promise.resolve();
  } else {
    GuiConnector.showProcessing();
    return self.getServerConnector().revokeUserPrivileges({
      user: self.getUser(),
      privileges: [privilege]
    }).catch(GuiConnector.alert).finally(GuiConnector.hideProcessing);
  }
};

/**
 *
 * @returns {Promise}
 */
EditUserDialog.prototype.refreshProjects = function () {
  var self = this;
  return self.getServerConnector().getProjects().then(function (projects) {
    return self.setProjects(projects);
  });
};

/**
 *
 * @param projects
 */
EditUserDialog.prototype.setProjects = function (projects) {
  var self = this;
  self._userByLogin = [];
  var columns = self.createUserPrivilegeColumns();
  var dataTable = $("[name='projectsTable']", self.getElement()).DataTable();
  var data = [], i;

  var rowData = self.projectToTableRow(null, columns);
  var defaultRow = $("[name='defaultProjectsRow']", self.getElement())[0];
  defaultRow.innerHTML = "";
  defaultRow.appendChild(Functions.createElement({
    type: "span",
    content: "<b>DEFAULT PRIVILEGE FOR NEW PROJECT</b>",
    xss: false
  }));
  defaultRow.appendChild(Functions.createElement({type: "br"}));
  for (i = 1; i < columns.length; i++) {
    defaultRow.appendChild(Functions.createElement({
      type: "div",
      content: rowData[i] + columns[i].title,
      style: "float:left;padding:5px;",
      xss: false
    }));
  }
  defaultRow.appendChild(Functions.createElement({type: "br"}));
  defaultRow.appendChild(Functions.createElement({type: "hr"}));
  // data.push(rowData);
  for (i = 0; i < projects.length; i++) {
    var project = projects[i];
    rowData = self.projectToTableRow(project, columns);
    data.push(rowData);
  }
  dataTable.clear().rows.add(data).draw();
};

/**
 *
 * @param {Project} project
 * @param {Array} columns
 * @returns {Array}
 */
EditUserDialog.prototype.projectToTableRow = function (project, columns) {
  var user = this.getUser();
  var row = [];
  var id = null;
  var projectId = "*";
  if (project !== null) {
    id = project.getId();
    projectId = project.getProjectId();
  }

  row[0] = "<span data='" + id + "'>" + projectId + "</span>";

  for (var i = 1; i < columns.length; i++) {
    var privilege = columns[i].privilegeType;
    var checked = "";
    if (user.hasPrivilege(privilege, projectId)) {
      checked = "checked";
    } else if (this.getIsNewUser()) {
      var option = this.getConfiguration().getOption('DEFAULT_' + privilege.getName());
      if (option !== null && option !== undefined) {
        if (option.getValue().toLowerCase() === "true") {

          user.setPrivilege({privilegeType: privilege.getName(), objectId: projectId});
          checked = "checked";
        }
      }
    }
    row.push("<input type='checkbox' name='project-privilege-checkbox' data='" + privilege.getName() + ":" + projectId + "' " + checked + " />");
  }

  return row;
};


/**
 *
 */
EditUserDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();
  var usersTable = $("[name=projectsTable]", div)[0];
  if ($.fn.DataTable.isDataTable(usersTable)) {
    $(usersTable).DataTable().destroy();
  }

  var detailsTable = $("[name=detailsTable]", div)[0];
  if ($.fn.DataTable.isDataTable(detailsTable)) {
    $(detailsTable).DataTable().destroy();
  }


  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
};

/**
 *
 */
EditUserDialog.prototype.open = function () {
  var self = this;
  var div = self.getElement();
  var title = self.getUser().getLogin();
  if (title === undefined) {
    title = "NEW USER";
  }
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      dialogClass: 'minerva-edit-user-dialog',
      title: title,
      width: window.innerWidth / 2,
      height: window.innerHeight / 2
    });
    $(".ui-dialog-titlebar-close", $(div).dialog().siblings('.ui-dialog-titlebar')).on("mousedown", function () {
      //we need to close dialog on mouse down, because processing modal dialogs prevents the click to finish happening
      $(div).dialog("close");
    });
  }
  $(div).dialog("open");
};

/**
 *
 * @returns {Promise}
 */
EditUserDialog.prototype.onSaveClicked = function () {
  var self = this;
  var user = self.getUser();
  return self.checkValidity().then(function () {
    user.setLogin(self.getLogin());
    user.setPassword(self.getPassword());
    user.setEmail(self.getEmail());
    user.setOrcidId(self.getOrcidId());
    user.setName(self.getName());
    user.setSurname(self.getSurname());
    return self.getServerConnector().addUser(user).catch(function (error) {
      user.setLogin(undefined);
      if (error instanceof ObjectExistsError) {
        return Promise.reject(new ValidationError("User with given login already exists"));
      }
      return Promise.reject(error);
    });
  }).then(function () {
    return self.callListeners("onSave", user);
  });
};


/**
 *
 * @param {string} [fieldName]
 * @returns {Promise}
 */
EditUserDialog.prototype.updateUser = function (fieldName) {
  var self = this;
  GuiConnector.showProcessing();
  return self.getServerConnector().updateUser(self.getUser()).then(function (user) {
    return self.callListeners("onSave", user);
  }).catch(function (error) {
    if (error instanceof ObjectExistsError) {
      var message = "Data already used by another user"
      if (fieldName !== undefined) {
        message = fieldName + " is already used by another user";
      }
      GuiConnector.alert(message);
    } else if (error instanceof NetworkError && error.statusCode === 400) {
      GuiConnector.alert("Invalid data");
    } else {
      GuiConnector.alert(error);
    }
  }).finally(GuiConnector.hideProcessing);
};

/**
 *
 * @returns {Promise}
 */
EditUserDialog.prototype.checkValidity = function () {
  var self = this;
  var isValid = true;
  var error = "<b>Some data is missing.</b><ul>";
  if (self.getPassword() !== self.getPassword2()) {
    error += "<li>Password doesn't match</li>";
    isValid = false;
  }
  if (self.getIsNewUser() && self.getPassword() === "") {
    error += "<li>Password for the user is not defined</li>";
    isValid = false;
  }

  if (self.getLogin() === "" || self.getLogin() === undefined) {
    error += "<li>Login must not be empty</li>";
    isValid = false;
  }
  if (self.getLogin().length > 255) {
    error += "<li>Login must be shorter than 256 characters</li>";
    isValid = false;
  }
  if (isValid) {
    return Promise.resolve(true);
  } else {
    return Promise.reject(new ValidationError(error));
  }
};

/**
 *
 * @returns {string}
 */
EditUserDialog.prototype.getPassword = function () {
  var self = this;
  return $("[name='userPassword']", self.getElement()).val();
};

/**
 *
 * @returns {string}
 */
EditUserDialog.prototype.getPassword2 = function () {
  var self = this;
  return $("[name='userPassword2']", self.getElement()).val();
};

/**
 *
 * @returns {string}
 */
EditUserDialog.prototype.getLogin = function () {
  var self = this;
  return $("[name='userLogin']", self.getElement()).val();
};

EditUserDialog.prototype.setLogin = function (login) {
  $("[name='userLogin']", this.getElement()).val(login);
};

/**
 *
 * @returns {string}
 */
EditUserDialog.prototype.getEmail = function () {
  var self = this;
  return $("[name='userEmail']", self.getElement()).val();
};

/**
 *
 * @returns {string}
 */
EditUserDialog.prototype.getOrcidId = function () {
  var self = this;
  return $("[name='userOrcidId']", self.getElement()).val();
};

/**
 *
 * @returns {string}
 */
EditUserDialog.prototype.getName = function () {
  var self = this;
  return $("[name='userName']", self.getElement()).val();
};

/**
 *
 * @returns {string}
 */
EditUserDialog.prototype.getSurname = function () {
  var self = this;
  return $("[name='userSurname']", self.getElement()).val();
};

/**
 *
 */
EditUserDialog.prototype.close = function () {
  var self = this;
  if ($(self.getElement()).hasClass("ui-dialog-content")) {
    //close it only if it wasn't destroyed
    $(self.getElement()).dialog("close");
  }
};

/**
 *
 * @returns {{title: string, privilegeType: PrivilegeType|undefined}[]}
 */
EditUserDialog.prototype.createUserPrivilegeColumns = function () {
  var self = this;

  if (self._userPrivilegeColumns !== undefined) {
    return self._userPrivilegeColumns;
  }

  var configuration = self.getConfiguration();
  self._userPrivilegeColumns = [{
    title: "ProjectId"
  }];
  var privilegeTypes = configuration.getPrivilegeTypes();
  for (var i = 0; i < privilegeTypes.length; i++) {
    var type = privilegeTypes[i];
    if (type.getObjectType() === "Project") {
      self._userPrivilegeColumns.push({
        "title": type.getCommonName(),
        privilegeType: type
      });
    }
  }
  return self._userPrivilegeColumns;
};


module.exports = EditUserDialog;
