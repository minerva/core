"use strict";

var $ = require('jquery');

/* exported logger */

var AbstractGuiElement = require('./AbstractGuiElement');
var Functions = require('../Functions');

// noinspection JSUnusedLocalSymbols
var logger = require('../logger');

function SubMenu(params) {
  AbstractGuiElement.call(this, params);
  var self = this;

  self._createGui(params);

}

SubMenu.prototype = Object.create(AbstractGuiElement.prototype);
SubMenu.prototype.constructor = SubMenu;

SubMenu.prototype._createGui = function (params) {
  var self = this;

  var element = self.getElement();
  element.className = "dropdown-submenu";

  var link = Functions.createElement({
    type: "a",
    href: "#",
    className: "dropdown-item",
    content: params.name
  });
  link.tabIndex = -1;
  self.getElement().appendChild(link);

  var ul = Functions.createElement({
    type: "ul",
    className: "dropdown-menu"
  });
  self.getElement().appendChild(ul);
  self.setUl(ul);

};
/**
 *
 * @param {Object} params
 * @param {string} [params.name]
 * @param {SubMenu} [params.submenu]
 * @param {function} [params.handler]
 */
SubMenu.prototype.addOption = function (params) {
  var self = this;
  if (params.submenu instanceof SubMenu) {
    self.getElement().appendChild(params.submenu.getElement());
  } else {
    var option = Functions.createElement({
      type: "li"
    });
    var link = Functions.createElement({
      type: "a",
      href: "#",
      className: "dropdown-item",
      content: params.name
    });
    $(link).data("handler", params.handler);
    option.appendChild(link);
    self.getUl().appendChild(option);
  }
};

SubMenu.prototype.setUl = function (ul) {
  this._ul = ul;
};

SubMenu.prototype.getUl = function () {
  return this._ul;
};

module.exports = SubMenu;
