"use strict";

/* exported logger */
var $ = require('jquery');

var AbstractGuiElement = require('./AbstractGuiElement');
var PanelControlElementType = require('./PanelControlElementType');
var Functions = require('../Functions');
var OptionsMenu = require('./OptionsMenu');

var Promise = require("bluebird");

var logger = require('../logger');
var xss = require('xss');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} params.customMap
 * @param {boolean} [params.optionsMenu=false]
 * @param {Configuration} params.configuration
 * @param {Project} [params.project]
 * @param {LeftPanel} params.parent
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 *
 * @extends AbstractGuiElement
 */
function Header(params) {
  AbstractGuiElement.call(this, params);
  var self = this;

  var guiParams = {
    optionsMenu: params.optionsMenu
  };

  self._createHeaderGui(guiParams, params.parent);
}

Header.prototype = Object.create(AbstractGuiElement.prototype);
Header.prototype.constructor = Header;

/**
 *
 * @param {Object} guiParams
 * @param {boolean} guiParams.optionsMenu
 * @param {LeftPanel} parent
 * @private
 */
Header.prototype._createHeaderGui = function (guiParams, parent) {
  var self = this;
  self.getElement().className = "minerva-header";

  var projectId = self.getProject().getProjectId();
  var projectName = xss(self.getProject().getName());

  var loadingDiv = Functions.createElement({
    type: "div",
    className: "minerva-loading-div"
  });

  var loadingImg = Functions.createElement({
    type: "img",
    src: 'resources/images/icons/ajax-loader.gif'
  });
  loadingDiv.appendChild(loadingImg);
  this.setControlElement(PanelControlElementType.FOOTER_LOADING_DIV, loadingDiv);

  self.getElement().appendChild(Functions.createElement({
    type: "a",
    className: "minerva-menu-link",
    content: '<i class="fa fa-user-cog" style="font-size:17px"></i>&nbsp;',
    href: self.getServerConnector().getServerBaseUrl() + "admin.xhtml?id=" + projectId,
    title: "Admin panel",
    xss: false
  }));

  if (self.getServerConnector().getSessionData().getLogin() === "anonymous") {
    self.getElement().appendChild(Functions.createElement({
      type: "a",
      className: "minerva-menu-link",
      content: '<i class="fa fa-lock" style="font-size:17px"></i>&nbsp;',
      href: "#",
      onclick: function() {
        return parent.getLoginDialog().open();
      },
      title: "Login",
      xss: false
    }));
  } else {
    self.getElement().appendChild(Functions.createElement({
      type: "a",
      className: "minerva-menu-link",
      content: '<i class="fa fa-lock-open" style="font-size:17px"></i>&nbsp;',
      href: "#",
      onclick: function() {
        return self.getServerConnector().logout().catch(GuiConnector.alert);
      },
      title: "Logout",
      xss: false
    }));
  }

  if (guiParams.optionsMenu) {
    var optionsElement = Functions.createElement({type: "ul", className: "minerva-options-menu"});
    document.body.appendChild(optionsElement);
    self._optionsMenu = new OptionsMenu({
      customMap: self.getMap(),
      element: optionsElement,
      configuration: self.getConfiguration()
    });

    var menuLink = Functions.createElement({
      type: "a",
      className: "minerva-menu-link",
      content: '<i class="fa fa-bars" style="font-size:17px"></i>&nbsp;',
      href: "#",
      onclick: function () {
        var link = $(menuLink);
        var offset = link.offset();

        var top = offset.top;
        var left = offset.left;

        var bottom = top + link.outerHeight();

        return self._optionsMenu.open(left, bottom, new Date().getTime());
      },
      xss: false
    });
    self.getElement().appendChild(menuLink);
  }

  self.getElement().appendChild(loadingDiv);

  var homeLink = Functions.createElement({
    type: "a",
    content: '<i class="fa fa-home" style="font-size:17px"></i> ' + projectName,
    xss: false
  });
  homeLink.href = self.getServerConnector().getServerBaseUrl() + "index.xhtml?id=" + projectId;
  self.getElement().appendChild(homeLink);
};

/**
 *
 * @param {string} message
 */
Header.prototype.addLoadMessage = function (message) {
  var self = this;
  self._loadMessages.push(message);
};

/**
 *
 * @param {string} message
 */
Header.prototype.removeLoadMessage = function (message) {
  var self = this;
  var index = self._loadMessages.indexOf(message);
  if (index > -1) {
    self._loadMessages.splice(index, 1);
  } else {
    logger.debug("Removing message that is not there: " + message);
  }
};

/**
 * return {Promise}
 */
Header.prototype.init = function () {
  var self = this;
  return new Promise(function (resolve) {
    var div = self.getControlElement(PanelControlElementType.FOOTER_LOADING_DIV);

    self._loadMessages = [];
    self._onDataLoadStart = function (e) {
      self.addLoadMessage(e.arg);
      $(div).show();
      div.title = e.arg;
    };

    self._onDataLoadStop = function (e) {
      self.removeLoadMessage(e.arg);
      if (self._loadMessages.length > 0) {
        div.title = self._loadMessages[0];
      } else {
        $(div).hide();
      }
    };
    self.getServerConnector().addListener("onDataLoadStart", self._onDataLoadStart);
    self.getServerConnector().addListener("onDataLoadStop", self._onDataLoadStop);
    resolve();
  });
};

/**
 *
 */
Header.prototype.destroy = function () {
  var self = this;
  if (self._onDataLoadStart) {
    self.getServerConnector().removeListener("onDataLoadStart", self._onDataLoadStart);
    self.getServerConnector().removeListener("onDataLoadStop", self._onDataLoadStop);
  }
  if (self._optionsMenu !== undefined) {
    document.body.removeChild(self._optionsMenu.getElement());
  }
};

/**
 *
 * @param {PluginManager} pluginManager
 */
Header.prototype.setPluginManager = function (pluginManager) {
  var self = this;
  self._pluginManager = pluginManager;
  if (self._optionsMenu !== undefined) {
    self._optionsMenu.setPluginManager(pluginManager);
  }
};

/**
 *
 * @returns {PluginManager}
 */
Header.prototype.getPluginManager = function () {
  return this._pluginManager;
};

module.exports = Header;
