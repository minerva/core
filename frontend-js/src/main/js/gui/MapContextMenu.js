"use strict";

/* exported logger */

var ContextMenu = require('./ContextMenu');

var $ = require('jquery');
var logger = require('../logger');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} params.customMap
 * @param {Configuration} params.configuration
 * @param {Project} params.project
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 *
 * @extends ContextMenu
 */
function MapContextMenu(params) {
  ContextMenu.call(this, params);
  var self = this;

  self._createMapContextMenuGui();
  self.setMolArt(params.molArt);
}

MapContextMenu.prototype = Object.create(ContextMenu.prototype);
MapContextMenu.prototype.constructor = MapContextMenu;

/**
 *
 * @private
 */
MapContextMenu.prototype._createMapContextMenuGui = function () {
  var self = this;
  self.addOption({
    name: "Add comment",
    handler: function () {
      return self.getMap().openCommentDialog();
    }
  });
};

/**
 *
 * @returns {Promise}
 */
MapContextMenu.prototype.init = function () {
  var self = this;
  return self.createExportAsImageSubmenu().then(function (submenu) {
    var link = self.addOption({name: "Export current view:"})
    $(link).css('cursor', 'default');
    self.addOption({submenu: submenu});
    return self.createExportAsModelSubmenu();
  }).then(function (submenu) {
    self.addOption({submenu: submenu});
  });
};

/**
 *
 * @param {MolArt} molArt
 */
MapContextMenu.prototype.setMolArt = function (molArt) {
  this._molArt = molArt;
};

/**
 *
 * @returns {MolArt}
 */
MapContextMenu.prototype.getMolArt = function () {
  return this._molArt;
};

module.exports = MapContextMenu;
