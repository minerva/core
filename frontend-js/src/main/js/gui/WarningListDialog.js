"use strict";

var Promise = require("bluebird");
var $ = require('jquery');

/* exported logger */

var AbstractGuiElement = require('./AbstractGuiElement');

var Functions = require('../Functions');
var GuiConnector = require('../GuiConnector');


/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} [params.customMap]
 * @param {Configuration} params.configuration
 * @param {Project} [params.project]
 * @param {Array} params.warnings
 * @param {ServerConnector} params.serverConnector
 *
 * @constructor
 * @extends AbstractGuiElement
 */
function LogListDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self.createLogListDialogGui();

  self._warnings = params.warnings;
}

LogListDialog.prototype = Object.create(AbstractGuiElement.prototype);
LogListDialog.prototype.constructor = LogListDialog;

/**
 *
 */
LogListDialog.prototype.createLogListDialogGui = function () {
  var self = this;
  var head = Functions.createElement({
    type: "thead",
    content: "<tr>" +
      "<th>Type</th>" +
      "<th>Object ID</th>" +
      "<th>Object Class</th>" +
      "<th>Content</th>" +
      "</tr>"
  });
  var body = Functions.createElement({
    type: "tbody"
  });
  var tableElement = Functions.createElement({
    type: "table",
    className: "minerva-logs-table",
    style: "width: 100%"
  });

  tableElement.appendChild(head);
  tableElement.appendChild(body);

  self.tableElement = tableElement;
  self.getElement().appendChild(tableElement);

  var menuRow = Functions.createElement({
    type: "div",
    className: "minerva-menu-row",
    style: "display:table-row; margin:10px"
  });
  self.getElement().appendChild(menuRow);

  var downloadButton = Functions.createElement({
    type: "button",
    name: "saveUser",
    content: '<span class="ui-icon ui-icon-disk"></span>&nbsp;DOWNLOAD',
    onclick: function () {
      var result = self.getAllInTabFormat();
      var blob = new Blob([result], {
        type: "text/plain;charset=utf-8"
      });
      var FileSaver = require("file-saver");
      return FileSaver.saveAs(blob, self.getProject().getProjectId() + "-logs.txt");
    },
    xss: false
  });
  menuRow.appendChild(downloadButton);

};

function removeNull(object) {
  if (object === null || object === undefined) {
    return "";
  }
  return object;
}

function extractTypeName(type) {
  if (type === undefined || type === null) {
    return "";
  }
  var result = type.toLowerCase().replace("_", " ");
  return result[0].toUpperCase() + result.slice(1);
}

function entryToRow(entry) {
  var row = [];
  row[0] = removeNull(extractTypeName(entry.type));
  row[1] = removeNull(entry.objectIdentifier);
  row[2] = removeNull(entry.objectClass);
  row[3] = removeNull(entry.content);
  return row;
}

/**
 *
 * @returns {string}
 */
LogListDialog.prototype.getAllInTabFormat = function () {
  var result = [];
  for (var i = 0; i < this._warnings.length; i++) {
    result.push(entryToRow(this._warnings[i]).join("\t"));
  }
  return result.join("\n");
};

LogListDialog.prototype.open = function () {
  var self = this;
  if (!$(self.getElement()).hasClass("ui-dialog-content")) {
    $(self.getElement()).dialog({
      dialogClass: 'minerva-logs-dialog',
      title: "Conversion warnings",
      autoOpen: false,
      resizable: false,
      width: Math.max(window.innerWidth / 2, window.innerWidth - 100),
      height: Math.max(window.innerHeight / 2, window.innerHeight - 100)
    });
  }

  $(self.getElement()).dialog("open");

  var dataTable = $(".minerva-logs-table", self.getElement()).DataTable({

  });

  var data = [];
  for (var i = 0; i < self._warnings.length; i++) {
    var entry = self._warnings[i];
    var row = entryToRow(entry);
    data.push(row);
  }
  dataTable.clear().rows.add(data).draw();
  dataTable.columns.adjust().draw();
};

/**
 *
 * @returns {Promise}
 */
LogListDialog.prototype.init = function () {
  return Promise.resolve();
};

/**
 *
 */
LogListDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();
  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
  if ($.fn.DataTable.isDataTable(self.tableElement)) {
    $(self.tableElement).DataTable().destroy();
  }
};

module.exports = LogListDialog;
