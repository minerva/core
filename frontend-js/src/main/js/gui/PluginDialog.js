"use strict";

var $ = require('jquery');
var AbstractGuiElement = require('./AbstractGuiElement');
var GuiConnector = require('../GuiConnector');
var GuiUtils = require('./leftPanel/GuiUtils');

var Functions = require('../Functions');
var Promise = require('bluebird');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} params.customMap
 * @param {Configuration} params.configuration
 * @param {Project} params.project
 * @param {PluginManager} params.pluginManager
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 *
 * @extends AbstractGuiElement
 */
function PluginDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self.setPluginManager(params.pluginManager);
  // @type {PluginData[]}
  self._knownPlugins = [];
}

PluginDialog.prototype = Object.create(AbstractGuiElement.prototype);
PluginDialog.prototype.constructor = PluginDialog;

/**
 *
 * @private
 */
PluginDialog.prototype._createPluginGui = function () {
  var self = this;
  var guiUtils = new GuiUtils(self.getConfiguration());

  self.getElement().innerHTML = "";

  var pluginFormTab = Functions.createElement({
    type: "div",
    name: "minerva-plugin-list-tab",
    style: "width:100%;display: table;border-spacing: 10px;"
  });
  self.getElement().appendChild(pluginFormTab);

  var pluginUrlLabel = Functions.createElement({
    type: "span",
    content: "URL:",
    style: "color:#999999"
  });
  var pluginUrlInput = Functions.createElement({
    type: "input",
    name: "pluginUrlValue"
  });

  var loadPluginButton = Functions.createElement({
    type: "button",
    name: "loadPluginButton",
    content: "LOAD",
    onclick: function () {
      $(loadPluginButton).prop('disabled', true);
      return self.getPluginManager().addPlugin({url: pluginUrlInput.value}).then(function () {
        self.close();
      }).catch(GuiConnector.alert).finally(function () {
        $(loadPluginButton).prop('disabled', false);
      });
    }
  });
  pluginFormTab.appendChild(guiUtils.createTableRow([pluginUrlLabel, pluginUrlInput, loadPluginButton]));
  var helpButton = guiUtils.createHelpButton(
    "Plugins allow creating client-side custom visualizations, independent from the core functionalities of MINERVA. " +
    "In the URL field one needs to supply a URL to the plugin, written in JavaScript. To learn how to write your own " +
    "plugins, please, visit the <a href='https://git-r3lab.uni.lu/minerva/plugins/starter-kit' target='_starter_kit'>“Plugin starter kit” repository</a>. " +
    "For example, you can use a link for an example plugin: <a href='https://minerva-dev.lcsb.uni.lu/plugins/starter-kit/plugin.js' " +
    "target='_starter_kit_source'>https://minerva-dev.lcsb.uni.lu/plugins/starter-kit/plugin.js</a>");
  self.getElement().appendChild(helpButton);

  var pluginManager = self.getPluginManager();

  var loadedPlugins = [];
  var plugins = pluginManager.getPlugins();
  var i;
  for (i = 0; i < plugins.length; i++) {
    loadedPlugins[plugins[i].getOptions().url] = true;
  }

  for (i = 0; i < self._knownPlugins.length; i++) {
    var pluginData = self._knownPlugins[i];

    if (!loadedPlugins[pluginData.getUrls()[0]]) {
      var pluginUrl = Functions.createElement({
        type: "input",
        value: pluginData.getUrls()[0],
        style: "color:#999999"
      });
      var pluginName = Functions.createElement({
        type: "span",
        content: pluginData.getName() + " (" + pluginData.getVersion() + ")"
      });
      pluginUrl.readOnly = true;
      var button;
      button = Functions.createElement({
        type: "button",
        content: "LOAD",
        onclick: function () {
          var thisButton = this;
          $(thisButton).prop('disabled', true);
          return pluginManager.addPlugin({url: $(this).data("url")}).then(function () {
            self.close();
          }).catch(GuiConnector.alert).finally(function () {
            $(thisButton).prop('disabled', false);
          });
        }
      });
      $(button).data("url", pluginData.getUrls()[0]);

      pluginFormTab.appendChild(guiUtils.createTableRow([pluginName, pluginUrl, button]));
    }
  }
  for (i = 0; i < plugins.length; i++) {
    var plugin = plugins[i];
    var removePlugin = (function () {
      var pluginToRemove = plugin;
      return function () {
        $(this).attr("disabled", true);
        return pluginManager.removePlugin(pluginToRemove).then(function () {
          self.close();
        }).then(null, GuiConnector.alert);
      };
    })();
    pluginUrl = Functions.createElement({
      type: "input",
      value: plugin.getOptions().url,
      style: "color:#999999"
    });
    pluginName = Functions.createElement({
      type: "span",
      content: plugin.getName() + " (" + plugin.getVersion() + ")"
    });

    pluginUrl.readOnly = true;
    button = Functions.createElement({
      type: "button",
      content: "UNLOAD",
      onclick: removePlugin
    });

    pluginFormTab.appendChild(guiUtils.createTableRow([pluginName, pluginUrl, button]));

  }
};

/**
 *
 * @returns {Promise}
 */
PluginDialog.prototype.init = function () {
  var self = this;
  return self.getServerConnector().getPluginsData().then(function (pluginsData) {
    for (var i = 0; i < pluginsData.length; i++) {
      var pluginData = pluginsData[i];
      if (pluginData.isPublic()) {
        self._knownPlugins.push(pluginData);
      }
    }
  });
};

/**
 *
 */
PluginDialog.prototype.open = function () {
  var self = this;
  self._createPluginGui();
  var div = self.getElement();
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      autoOpen: false,
      resizable: false,
      width: 400,
      classes: {"ui-dialog": "minerva-plugin-dialog"}
    });
  }

  $(div).dialog('option', 'title', 'PLUGINS [BETA]');
  $(div).dialog("open");
};

/**
 *
 */
PluginDialog.prototype.close = function () {
  var self = this;
  $(self.getElement()).dialog("close");
};

/**
 *
 */
PluginDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();
  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
};

/**
 *
 * @param {PluginManager} pluginManager
 */
PluginDialog.prototype.setPluginManager = function (pluginManager) {
  this._pluginManager = pluginManager;
};

/**
 *
 * @returns {PluginManager}
 */
PluginDialog.prototype.getPluginManager = function () {
  return this._pluginManager;
};

module.exports = PluginDialog;
