"use strict";

/* exported logger */
var $ = require('jquery');

var AbstractExportPanel = require('./AbstractExportPanel');
var ValidationError = require('../../ValidationError');
var IdentifiedElement = require('../../map/data/IdentifiedElement');

var Functions = require('../../Functions');
var GuiConnector = require('../../GuiConnector');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var Promise = require("bluebird");

/**
 *
 * @param {Configuration} [params.configuration]
 * @param {HTMLElement} params.element
 * @param {Project} params.project
 * @param {string} [params.helpTip]
 * @param params.parent
 *
 * @constructor
 * @extends AbstractExportPanel
 */
function ElementExportPanel(params) {
  params["panelName"] = "elementExport";
  AbstractExportPanel.call(this, params);
}

ElementExportPanel.prototype = Object.create(AbstractExportPanel.prototype);
ElementExportPanel.prototype.constructor = ElementExportPanel;

/**
 *
 * @returns {Promise}
 */
ElementExportPanel.prototype.init = function () {
  var self = this;
  var element = self.getElement();
  var configuration = self.getConfiguration();
  var typeDiv = self._createSelectTypeDiv(configuration.getElementTypes());
  element.appendChild(typeDiv);
  var submapDiv = Functions.createElement({type: "div", name: "submapTypes"});
  submapDiv.appendChild(self._createSelectSubmapDiv(self.getProject().getModels()));
  element.appendChild(submapDiv);
  element.appendChild(self._createSelectColumnDiv(self.getAllColumns()));
  GuiConnector.showProcessing("LOADING...");
  return self.getServerConnector().getProjectStatistics(self.getProject().getProjectId()).then(function (statistics) {
    return self._createMiriamTypeDiv(statistics.getElementAnnotations());
  }).then(function (div) {
    element.appendChild(div);
    return self._createSelectIncludedCompartmentPathwayDiv();
  }).then(function (div) {
    element.appendChild(div);
    return self._createSelectExcludedCompartmentPathwayDiv();
  }).then(function (div) {
    element.appendChild(div);
    element.appendChild(self._createDownloadButton());
  }).then(function () {
    $(window).trigger('resize');
  }).finally(GuiConnector.hideProcessing);
};

/**
 *
 * @returns {Promise<string>}
 */
ElementExportPanel.prototype.createResponseString = function () {
  var self = this;
  var types, miriamTypes;
  var includedCompartmentIds = [];
  var excludedCompartmentIds = [];
  var submapIds = self.getSelectedSubmapIds();
  var models = self.getProject().getModels();

  var elements = [];
  return self.getSelectedTypes().then(function (result) {
    if (result.length === 0) {
      return Promise.reject(new ValidationError("You must select at least one type"));
    }
    types = result;
    return self.getSelectedIncludedCompartments();
  }).then(function (result) {
    result.forEach(function (compartment) {
      includedCompartmentIds.push(compartment.getId());
    });
    return self.getSelectedExcludedCompartments();
  }).then(function (result) {
    result.forEach(function (compartment) {
      excludedCompartmentIds.push(compartment.getId());
    });

    var promises = [];
    for (var i = 0; i < models.length; i++) {
      var model = models[i];
      if ($.inArray(model.getId(), submapIds) >= 0) {
        promises.push(model.getAliases({
          type: types,
          complete: true,
          includedCompartmentIds: includedCompartmentIds,
          excludedCompartmentIds: excludedCompartmentIds
        }));
      }
    }
    return Promise.all(promises);
  }).then(function (aliasesByModel) {
    for (var i = 0; i < aliasesByModel.length; i++) {
      for (var j = 0; j < aliasesByModel[i].length; j++) {
        elements.push(aliasesByModel[i][j]);
      }
    }

    return self.getSelectedMiriamTypes();
  }).then(function (result) {
    miriamTypes = result;
    return self.getSelectedColumns();
  }).then(function (selectedColumns) {
    if (selectedColumns.length === 0) {
      return Promise.reject(new ValidationError("You must select at least one column"));
    }

    var rowPromises = [];
    rowPromises.push(self.createResponseHeader(selectedColumns, miriamTypes));
    for (var i = 0; i < elements.length; i++) {
      rowPromises.push(self.createResponseRow(elements[i], selectedColumns, miriamTypes));
    }
    return Promise.all(rowPromises).then(function (rows) {
      return rows.join("\n");
    });
  });
};

/**
 *
 * @param {Alias} alias
 * @param {Array} columns
 * @param {MiriamType[]} miriamTypes
 * @returns {Promise<string>}
 */
ElementExportPanel.prototype.createResponseRow = function (alias, columns, miriamTypes) {
  var valuePromises = [];
  var i;
  for (i = 0; i < columns.length; i++) {
    valuePromises.push(this._createResponseCell(alias, columns[i]));
  }
  for (i = 0; i < miriamTypes.length; i++) {
    var value = "";
    var miriamType = miriamTypes[i];
    var references = alias.getReferences();
    for (var j = 0; j < references.length; j++) {
      var reference = references[j];
      if (reference.getType() === miriamType.getName()) {
        value += reference.getResource() + ",";
      }
    }
    valuePromises.push(value);
  }
  return Promise.all(valuePromises).then(function (values) {
    return values.join("\t");
  });
};

/**
 *
 * @returns {ExportColumn[]}
 */
ElementExportPanel.prototype.getAllColumns = function () {
  return this.getBioEntityAllColumns().concat([{
    "columnName": "name",
    "method": "getName",
    "name": "Name"
  }, {
    "columnName": "type",
    "method": "getType",
    "name": "Type"
  }, {
    "columnName": "complexId",
    "method": "getComplexId",
    "name": "Complex id"
  }, {
    "columnName": "complexName",
    /**
     *
     * @param {Alias} bioEntity
     * @param {Project} project
     * @returns {Promise<string>}
     */
    "method": function (bioEntity, project) {
      var modelId = bioEntity.getModelId();
      var complexId = bioEntity.getComplexId();
      if (complexId !== undefined) {
        return project.getModelById(modelId).getByIdentifiedElement(new IdentifiedElement({
          id: complexId,
          modelId: modelId,
          type: "ALIAS"
        }), true).then(function (complex) {
          return complex.getName();
        });
      } else {
        return Promise.resolve("");
      }
    },
    "name": "Complex name"
  }, {
    "columnName": "compartmentId",
    "method": "getCompartmentId",
    "name": "Compartment/Pathway id"
  }, {
    "columnName": "compartmentName",
    /**
     *
     * @param {Alias} bioEntity
     * @param {Project} project
     * @returns {Promise<string>}
     */
    "method": function (bioEntity, project) {
      var modelId = bioEntity.getModelId();
      var compartmentId = bioEntity.getCompartmentId();
      if (compartmentId !== undefined) {
        return project.getModelById(modelId).getByIdentifiedElement(new IdentifiedElement({
          id: compartmentId,
          modelId: modelId,
          type: "ALIAS"
        }), true).then(function (compartment) {
          return compartment.getName();
        });
      } else {
        return Promise.resolve("");
      }
    },
    "name": "Compartment/Pathway name"
  }, {
    "columnName": "charge",
    "method": "getCharge",
    "name": "Charge"
  }, {
    "columnName": "fullName",
    "method": "getFullName",
    "name": "Full name"
  }, {
    "columnName": "formula",
    "method": "getFormula",
    "name": "Formula"
  }, {
    "columnName": "formerSymbols",
    "method": "getFormerSymbols",
    "name": "Former symbols"
  }, {
    "columnName": "linkedSubmodelId",
    "method": "getLinkedSubmodelId",
    "name": "Linked submap id"
  }, {
    "columnName": "elementId",
    "method": "getElementId",
    "name": "Element external id"
  }]);
};


module.exports = ElementExportPanel;
