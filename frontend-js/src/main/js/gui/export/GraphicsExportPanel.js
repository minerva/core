"use strict";

/* exported logger */
var $ = require('jquery');

var AbstractExportPanel = require('./AbstractExportPanel');
var Functions = require('../../Functions');
var GuiConnector = require('../../GuiConnector');

var logger = require('../../logger');
var xss = require('xss');

/**
 *
 * @param {Configuration} [params.configuration]
 * @param {HTMLElement} params.element
 * @param {Project} params.project
 * @param {string} [params.helpTip]
 * @param params.parent
 *
 * @constructor
 * @extends AbstractExportPanel
 */
function GraphicsExportPanel(params) {
  params.panelName = "graphicsExport";
  AbstractExportPanel.call(this, params);
}

GraphicsExportPanel.prototype = Object.create(AbstractExportPanel.prototype);
GraphicsExportPanel.prototype.constructor = GraphicsExportPanel;

/**
 *
 * @returns {Promise}
 */
GraphicsExportPanel.prototype.init = function () {
  var self = this;
  var element = self.getElement();
  var configuration;
  element.appendChild(self._createSelectProjectDiv());
  element.appendChild(self._createSelectResolutionDiv());
  return ServerConnector.getConfiguration().then(function (result) {
    configuration = result;
    element.appendChild(self._createSelectGraphicsFormatDiv(configuration.getImageConverters()));
    element.appendChild(self._createDownloadButton());
  }).then(function () {
    $(window).trigger('resize');

    var model = self.getModels()[0];
    var width = model.getWidth();
    var height = model.getHeight();

    if (width > height) {
      self.recalculateRatio({width: 256});
    } else {
      self.recalculateRatio({height: 256});
    }


    $('.minerva-image-width', self.getElement()).keyup(function () {
      if (/\D/g.test(this.value)) {
        this.value = this.value.replace(/\D/g, '');
      }
      if (this.value === '') {
        this.value = '1';
      }
      self.recalculateRatio({width: this.value});
    });

    $('.minerva-image-height', self.getElement()).keyup(function () {
      if (/\D/g.test(this.value)) {
        this.value = this.value.replace(/\D/g, '');
      }
      if (this.value === '') {
        this.value = '1';
      }
      self.recalculateRatio({height: this.value});
    });


  });
};

/**
 * @param {string|number} [param.width]
 * @param {string|number} [param.height]
 * @param param
 */
GraphicsExportPanel.prototype.recalculateRatio = function (param) {
  var self = this;

  var model = self.getSelectedSubmap();
  if (model === null) {
    logger.warn("Cannot find model with id: " + id);
    return;
  }

  var ratio = model.getWidth() / model.getHeight();

  var width = param.width;
  var height = param.height;

  if (width !== undefined) {
    width = parseInt(width);
    height = Math.floor(width / ratio);
  } else {
    height = parseInt(height);
    width = Math.floor(height * ratio);
  }

  $('.minerva-image-height', self.getElement()).val(height);
  $('.minerva-image-width', self.getElement()).val(width);
};

/**
 *
 * @returns {HTMLElement}
 * @private
 */
GraphicsExportPanel.prototype._createSelectProjectDiv = function () {
  var self = this;
  var typeDiv = Functions.createElement({
    type: "div",
    name: "modelSelectDiv"
  });
  typeDiv.appendChild(Functions.createElement({
    type: "h4",
    content: "(Sub)map:"
  }));

  var choicesContainer = Functions.createElement({
    type: "ul"
  });
  typeDiv.appendChild(choicesContainer);

  var models = self.getModels();

  for (var i = 0; i < models.length; i++) {
    var model = models[i];
    var checkedString = "";
    if (i === 0) {
      checkedString = ' checked="checked" ';
    }
    var modelName = xss(model.getName());
    var row = Functions.createElement({
      type: "li",
      content: '<div><label> <input type="radio" name="model" value="' + model.getId() + '"' + checkedString + '/>'
        + modelName + '</label></div>',
      xss: false
    });
    choicesContainer.appendChild(row);
  }

  return typeDiv;
};

/**
 *
 * @returns {HTMLElement}
 * @private
 */
GraphicsExportPanel.prototype._createSelectResolutionDiv = function () {
  var self = this;
  var typeDiv = Functions.createElement({
    type: "div",
    name: "ratioSelectDiv"
  });
  typeDiv.appendChild(Functions.createElement({
    type: "h4",
    content: "Image size:"
  }));

  var widthContainer = Functions.createElement({type: "div"});
  widthContainer.appendChild(Functions.createElement({type: "label", content: "Width: "}));
  widthContainer.appendChild(Functions.createElement({
    type: "input",
    className: "minerva-image-width",
    value: ""
  }));

  typeDiv.appendChild(widthContainer);

  var heightContainer = Functions.createElement({type: "div"});
  heightContainer.appendChild(Functions.createElement({type: "label", content: "Height: "}));
  heightContainer.appendChild(Functions.createElement({
    type: "input",
    className: "minerva-image-height",
    value: ""
  }));

  typeDiv.appendChild(heightContainer);

  return typeDiv;
};

/**
 *
 * @param {ImageConverter[]} formats
 * @returns {HTMLElement}
 * @private
 */
GraphicsExportPanel.prototype._createSelectGraphicsFormatDiv = function (formats) {
  var typeDiv = Functions.createElement({
    type: "div",
    name: "formatSelectDiv"
  });
  typeDiv.appendChild(Functions.createElement({
    type: "h4",
    content: "Format:"
  }));

  var choicesContainer = Functions.createElement({
    type: "ul"
  });
  typeDiv.appendChild(choicesContainer);

  for (var i = 0; i < formats.length; i++) {
    var format = formats[i];
    var checkedString = "";
    if (i === 0) {
      checkedString = ' checked="checked" ';
    }
    var row = Functions.createElement({
      type: "li",
      content: '<div><label> <input type="radio" name="format" value="' + format.handler + '"' + checkedString + '/>'
        + format.name + '</label></div>',
      xss: false

    });
    choicesContainer.appendChild(row);
  }

  return typeDiv;
};

/**
 *
 * @returns {string}
 */
GraphicsExportPanel.prototype.getSubmapId = function () {
  var self = this;
  var div = $("div[name='modelSelectDiv']", $(self.getElement()))[0];
  var id = null;
  $(":checked", $(div)).each(function (index, element) {
    id = element.value;
  });
  return id;
};

/**
 *
 * @return {null|MapModel}
 */
GraphicsExportPanel.prototype.getSelectedSubmap = function () {
  var self = this;
  var id = self.getSubmapId();
  var model = null;
  for (var i = 0; i < self.getModels().length; i++) {
    if (self.getModels()[i].getId() === parseInt(id)) {
      model = self.getModels()[i];
    }
  }
  return model;
};

/**
 *
 * @returns {string}
 */
GraphicsExportPanel.prototype.getFormatHandler = function () {
  var self = this;
  var div = $("div[name='formatSelectDiv']", $(self.getElement()))[0];
  var format = null;
  $(":checked", $(div)).each(function (index, element) {
    format = element.value;
  });
  return format;
};

/**
 *
 * @returns {HTMLElement}
 * @private
 */
GraphicsExportPanel.prototype._createDownloadButton = function () {
  var self = this;
  var downloadDiv = Functions.createElement({
    type: "div",
    name: "downloadDiv",
    style: "clear:both; padding: 10px;"
  });
  var button = Functions.createElement({
    type: "button",
    name: "downloadButton",
    content: " Download",
    onclick: function () {
      var identifier = null;
      var defaultOverlayName = "Network";
      for (var i = 0; i < self.getProject().getBackgrounds().length; i++) {
        var background = self.getProject().getBackgrounds()[i];
        if (identifier === null || background.getName() === defaultOverlayName) {
          identifier = background.getId();
        }
      }

      var ratio = Math.log2(parseInt($('.minerva-image-width', self.getElement()).val()) / 256) + 2;

      return ServerConnector.getImageDownloadUrl({
        modelId: self.getSubmapId(),
        backgroundOverlayId: identifier,
        handlerClass: self.getFormatHandler(),
        zoomLevel: ratio
      }).then(function (url) {
        return self.downloadFile(url);
      }).then(null, GuiConnector.alert);
    }
  });
  downloadDiv.appendChild(button);

  return downloadDiv;
};

if (!Math.log2) Math.log2 = function(x) {
  return Math.log(x) * Math.LOG2E;
};

module.exports = GraphicsExportPanel;
