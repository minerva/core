"use strict";

/* exported logger */

var $ = require('jquery');
var Panel = require('../Panel');

var GuiConnector = require('../../GuiConnector');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
var Functions = require('../../Functions');
var DualListbox = require('dual-listbox').DualListbox;
var Promise = require("bluebird");

/**
 * @typedef {Object} ExportColumn
 * @property {string} columnName
 * @property {string|function(BioEntity):Promise<string>} method
 * @property {string} name
 */

/**
 *
 * @param {Configuration} [params.configuration]
 * @param {HTMLElement} params.element
 * @param {Project} params.project
 * @param {CustomMap} params.customMap
 * @param {string} params.panelName
 * @param {string} [params.helpTip]
 * @param params.parent
 *
 * @constructor
 * @extends Panel
 */
function AbstractExportPanel(params) {
  params["scrollable"] = true;
  Panel.call(this, params);

}

AbstractExportPanel.prototype = Object.create(Panel.prototype);
AbstractExportPanel.prototype.constructor = AbstractExportPanel;

/**
 *
 */
AbstractExportPanel.prototype.init = function () {

};

/**
 *
 * @param {Object} val1
 * @param {Object} val2
 * @returns {number}
 */
function compareSimple(val1, val2) {
  if (val1 < val2) {
    return -1;
  } else if (val1 > val2) {
    return 1;
  } else {
    return 0;
  }
}

/**
 *
 * @param annotations
 * @returns {HTMLElement}
 * @protected
 */
AbstractExportPanel.prototype._createMiriamTypeDiv = function (annotations) {
  var self = this;
  var typeDiv = Functions.createElement({
    type: "div",
    name: "miriamSelectDiv",
    className: "minerva-export-dual-listbox-container"
  });
  typeDiv.appendChild(Functions.createElement({
    type: "h4",
    content: " Annotations:"
  }));

  var selectElement = Functions.createElement({
    type: "select",
    className: "minerva-multi-select"
  });
  typeDiv.appendChild(selectElement);

  function compare(entry1, entry2) {
    if ((entry1.count === 0 && entry2.count === 0) || (entry1.count !== 0 && entry2.count !== 0)) {
      return compareSimple(entry1.miriamType.getCommonName(), entry2.miriamType.getCommonName());
    } else {
      return compareSimple(-entry1.count, -entry2.count);
    }
  }

  annotations.sort(compare);

  for (var i = 0; i < annotations.length; i++) {
    var miriamType = annotations[i].miriamType;
    var count = annotations[i].count;

    var option = new Option();
    option.value = miriamType.getName();
    if (count > 0) {
      option.innerHTML = "<div class='minerva-multi-select-special'>" + miriamType.getCommonName() + " (" + count
        + ")</div>";
    } else {
      option.innerHTML = "<div >" + miriamType.getCommonName() + "</div>";
    }
    selectElement.appendChild(option);
  }

  self.setMiriamTypesDualListbox(self.createDualListbox(selectElement));
  return typeDiv;
};

/**
 *
 * @param {HTMLElement} selectElement
 * @returns {DualListbox}
 */
AbstractExportPanel.prototype.createDualListbox = function (selectElement) {
  return new DualListbox(selectElement, {
    availableTitle: 'Available',
    selectedTitle: 'Used',
    addButtonText: '>',
    removeButtonText: '<',
    addAllButtonText: '>>',
    removeAllButtonText: '<<'
  });
};

/**
 *
 * @param {BioEntityType[]} elementTypes
 * @param {string} [panelName]
 * @returns {HTMLElement}
 * @protected
 */
AbstractExportPanel.prototype._createSelectTypeDiv = function (elementTypes, panelName) {
  if (panelName === undefined) {
    panelName = " TYPE:";
  }
  var typeDiv = Functions.createElement({
    type: "div",
    name: "typeSelectDiv"
  });
  typeDiv.appendChild(Functions.createElement({
    type: "h4",
    content: panelName
  }));
  var choicesContainer = Functions.createElement({
    type: "ul",
    className: "minerva-checkbox-grid"
  });
  typeDiv.appendChild(choicesContainer);
  var i;
  var toBeExcluded = [];
  for (i = 0; i < elementTypes.length; i++) {
    toBeExcluded[elementTypes[i].parentClass] = true;
  }
  var processedNames = [];
  for (i = 0; i < elementTypes.length; i++) {
    var elementType = elementTypes[i];
    var name = elementType.name;
    if (processedNames[name] === undefined && toBeExcluded[elementType.className] === undefined) {
      processedNames[name] = true;
      var row = Functions.createElement({
        type: "li",
        content: "<div class=\"checkbox\"><label> <input type=\"checkbox\" name=\"" + name + "\" value=\"" + name + "\" />" + name + "</label></div>",
        xss: false
      });
      choicesContainer.appendChild(row);
    }
  }
  choicesContainer.appendChild(Functions.createElement({
    type: "li",
    content: "<div class=\"checkbox\"><label> <input type=\"checkbox\" name=\"ALL\" value=\"ALL\" />ALL</label></div>",
    xss: false
  }));
  return typeDiv;
};

/**
 *
 * @param {MapModel[]} models
 * @returns {HTMLElement}
 * @protected
 */
AbstractExportPanel.prototype._createSelectSubmapDiv = function (models) {
  var panelName = " (sub)map:";
  var mapDiv = Functions.createElement({
    type: "div",
    name: "mapSelectDiv"
  });
  mapDiv.appendChild(Functions.createElement({
    type: "h4",
    content: panelName
  }));
  var choicesContainer = Functions.createElement({
    type: "ul",
    className: "minerva-checkbox-grid"
  });
  mapDiv.appendChild(choicesContainer);

  var sortedModels = models.slice();
  sortedModels.sort(function(a, b) {
    if (a.getName() < b.getName()) {
      return -1;
    }
    if (a.getName() > b.getName()) {
      return 1;
    }
    return 0;
  });

  for (var i = 0; i < sortedModels.length; i++) {
    var model = sortedModels[i];
    var name = model.getName();
    var id = model.getId();
    var row = Functions.createElement({
      type: "li",
      content: "<div class=\"checkbox\"><label> <input type=\"checkbox\" name=\"" + name + "\" value=\"" + id + "\" checked/>" + name + "</label></div>",
      xss: false
    });
    choicesContainer.appendChild(row);
  }
  return mapDiv;
};

/**
 *
 * @param {HTMLElement} [htmlElement]
 * @returns {number[]}
 */
AbstractExportPanel.prototype.getSelectedSubmapIds = function (htmlElement) {
  var self = this;
  if (htmlElement === undefined) {
    htmlElement = self.getElement();
  }
  var div = $("div[name='mapSelectDiv']", $(htmlElement))[0];
  var result = [];
  $(":checked", $(div)).each(function (index, element) {
    result.push(parseInt($(element).val()));
  });
  return result;
};

/**
 *
 * @param {HTMLElement} [htmlElement]
 * @returns {Promise<BioEntityType[]>}
 */
AbstractExportPanel.prototype.getSelectedTypes = function (htmlElement) {
  var self = this;
  if (htmlElement === undefined) {
    htmlElement = self.getElement();
  }

  var div = $("div[name='typeSelectDiv']", $(htmlElement))[0];
  var result = [];
  var selectedAll = false;
  $(":checked", $(div)).each(function (index, element) {
    if (element.value === "ALL") {
      selectedAll = true;
    }
    result.push(element.value);
  });
  if (selectedAll) {
    result = [];
    $("input[type='checkbox']", $(div)).each(function (index, element) {
      if (element.value !== "ALL") {
        result.push(element.value);
      }
    });
  }

  return Promise.resolve(result);
};

/**
 *
 * @param {DualListbox} dualListbox
 */
AbstractExportPanel.prototype.setMiriamTypesDualListbox = function (dualListbox) {
  this._miriamTypesDualListbox = dualListbox;
};

/**
 *
 * @returns {DualListbox}
 */
AbstractExportPanel.prototype.getMiriamTypesDualListbox = function () {
  return this._miriamTypesDualListbox;
};

/**
 *
 * @param {{columnName:string, name:string}[]} columnTypes
 * @returns {HTMLElement}
 * @protected
 */
AbstractExportPanel.prototype._createSelectColumnDiv = function (columnTypes) {
  var columnDiv = Functions.createElement({
    type: "div",
    name: "columnSelectDiv"
  });
  columnDiv.appendChild(Functions.createElement({
    type: "h4",
    content: " COLUMN:"
  }));
  var choicesContainer = Functions.createElement({
    type: "ul",
    className: "minerva-checkbox-grid"
  });

  columnDiv.appendChild(choicesContainer);
  for (var i = 0; i < columnTypes.length; i++) {
    var columnType = columnTypes[i];
    var row = Functions.createElement({
      type: "li",
      content: "<div class=\"checkbox\"><label> <input type=\"checkbox\" name=\"column_" + columnType.columnName
        + "\" value=\"" + columnType.columnName + "\" />" + columnType.name + "</label></div>",
      xss: false
    });
    choicesContainer.appendChild(row);
  }
  choicesContainer.appendChild(Functions.createElement({
    type: "li",
    content: "<div class=\"checkbox\"><label> <input type=\"checkbox\" name=\"ALL\" value=\"ALL\" />ALL</label></div>",
    xss: false
  }));
  return columnDiv;
};

/**
 *
 * @returns {Promise<MiriamType[]>}
 */
AbstractExportPanel.prototype.getSelectedMiriamTypes = function () {
  var self = this;
  return ServerConnector.getConfiguration().then(function (configuration) {
    var selected = self.getMiriamTypesDualListbox().selected;
    var result = [];
    for (var i = 0; i < selected.length; i++) {
      var miriamType = configuration.getMiriamTypeByName(selected[i].dataset.id);
      result.push(miriamType);
    }
    return result;
  });
};

/**
 *
 * @returns {Promise<ExportColumn[]>}
 */
AbstractExportPanel.prototype.getSelectedColumns = function () {
  var self = this;

  var div = $("div[name='columnSelectDiv']", $(self.getElement()))[0];
  var selectedColumnMap = [];
  var selectedAll = false;
  $(":checked", $(div)).each(function (index, element) {
    if (element.value === "ALL") {
      selectedAll = true;
    }
    selectedColumnMap[element.value] = true;
  });
  var columnTypes = self.getAllColumns();

  if (selectedAll) {
    return Promise.resolve(columnTypes);
  }

  var result = [];

  for (var i = 0; i < columnTypes.length; i++) {
    var columnType = columnTypes[i];
    if (selectedColumnMap[columnType.columnName] === true) {
      result.push(columnType);
    }
  }
  return Promise.resolve(result);
};

/**
 *
 * @returns {Promise<string[]>}
 * @private
 */
AbstractExportPanel.prototype._getCompartmentAndPathwayNames = function () {
  var self = this;
  var compartments = [];
  return self._getAllCompartmentsAndPathways().then(function (result) {

    var addedNames = [];
    for (var i = 0; i < result.length; i++) {
      if (addedNames[result[i].getName()] === undefined) {
        compartments.push(result[i].getName());
        addedNames[result[i].getName()] = true;
      }
    }
    compartments.sort(compareSimple);
    return compartments;
  });
};

/**
 *
 * @returns {MapModel[]}
 */
AbstractExportPanel.prototype.getModels = function () {
  return this.getProject().getModels();
};

/**
 *
 * @returns {Promise}
 * @private
 */
AbstractExportPanel.prototype._getAllCompartmentsAndPathways = function () {
  var self = this;
  var compartments = [];

  var models = self.getModels();

  var promises = [];
  for (var i = 0; i < models.length; i++) {
    promises.push(models[i].getCompartmentsAndPathways());
  }

  return Promise.all(promises).then(function (result) {
    for (var i = 0; i < result.length; i++) {
      var modelCompartments = result[i];
      for (var j = 0; j < modelCompartments.length; j++) {
        compartments.push(modelCompartments[j]);
      }
    }
    return compartments;
  });
};

/**
 *
 * @returns {HTMLElement}
 * @protected
 */
AbstractExportPanel.prototype._createDownloadButton = function () {
  var self = this;
  var downloadDiv = Functions.createElement({
    type: "div",
    name: "downloadDiv",
    style: "clear:both; padding: 10px;"
  });
  var button = Functions.createElement({
    type: "button",
    name: "downloadButton",
    content: " Download",
    onclick: function () {
      GuiConnector.showProcessing();
      return self.createResponseString().then(function (result) {
        var blob = new Blob([result], {
          type: "text/plain;charset=utf-8"
        });
        var FileSaver = require("file-saver");
        return FileSaver.saveAs(blob, self.getSaveFilename());
      }).then(function () {
        GuiConnector.hideProcessing();
      }, function (error) {
        GuiConnector.hideProcessing();
        GuiConnector.alert(error);
      });
    }
  });
  downloadDiv.appendChild(button);

  return downloadDiv;
};

/**
 *
 * @returns {string}
 */
AbstractExportPanel.prototype.getSaveFilename = function () {
  var self = this;
  return self.getProject().getProjectId() + "-" + self.getPanelName() + ".txt";
};

/**
 *
 * @returns {Promise<HTMLElement>}
 * @protected
 */
AbstractExportPanel.prototype._createSelectIncludedCompartmentPathwayDiv = function () {
  var self = this;
  var typeDiv = Functions.createElement({
    type: "div",
    name: "includedCompartmentSelectDiv",
    className: "minerva-export-dual-listbox-container"
  });
  typeDiv.appendChild(Functions.createElement({
    type: "h4",
    content: " Included compartment/pathways:"
  }));

  return self._getCompartmentAndPathwayNames().then(function (compartmentNames) {

    var selectElement = Functions.createElement({
      type: "select",
      className: "minerva-multi-select"
    });
    typeDiv.appendChild(selectElement);

    for (var i = 0; i < compartmentNames.length; i++) {
      var name = compartmentNames[i];

      var option = new Option();
      option.value = name;
      option.innerHTML = "<div >" + name + "</div>";
      selectElement.appendChild(option);
    }
    self.setIncludedCompartmentsDualListbox(self.createDualListbox(selectElement));
    return typeDiv;
  });
};

/**
 *
 * @returns {Promise<HTMLElement>}
 * @protected
 */
AbstractExportPanel.prototype._createSelectExcludedCompartmentPathwayDiv = function () {
  var self = this;
  var typeDiv = Functions.createElement({
    type: "div",
    name: "excludedCompartmentSelectDiv",
    className: "minerva-export-dual-listbox-container"
  });
  typeDiv.appendChild(Functions.createElement({
    type: "h4",
    content: " Excluded compartment/pathway:"
  }));

  return self._getCompartmentAndPathwayNames().then(function (compartmentNames) {

    var selectElement = Functions.createElement({
      type: "select",
      className: "minerva-multi-select"
    });
    typeDiv.appendChild(selectElement);

    for (var i = 0; i < compartmentNames.length; i++) {
      var name = compartmentNames[i];

      var option = new Option();
      option.value = name;
      option.innerHTML = "<div >" + name + "</div>";
      selectElement.appendChild(option);
    }
    self.setExcludedCompartmentsDualListbox(self.createDualListbox(selectElement));
    return typeDiv;
  });
};

/**
 *
 * @param {DualListbox} dualListbox
 */
AbstractExportPanel.prototype.setIncludedCompartmentsDualListbox = function (dualListbox) {
  this._includedCompartmentsDualListbox = dualListbox;
};

/**
 *
 * @returns {DualListbox}
 */
AbstractExportPanel.prototype.getIncludedCompartmentsDualListbox = function () {
  return this._includedCompartmentsDualListbox;
};

/**
 *
 * @returns {Promise<Array>}
 */
AbstractExportPanel.prototype.getSelectedIncludedCompartments = function () {
  var self = this;
  var list = self.getIncludedCompartmentsDualListbox().selected;
  var names = [];
  var result = [];
  for (var i = 0; i < list.length; i++) {
    var element = list[i];
    names[element.dataset.id] = true;
  }
  return self._getAllCompartmentsAndPathways().then(function (compartments) {
    for (var i = 0; i < compartments.length; i++) {
      var compartment = compartments[i];
      if (names[compartment.getName()]) {
        result.push(compartment);
      }
    }
    return result;
  });
};

/**
 *
 * @param {DualListbox} dualListbox
 */
AbstractExportPanel.prototype.setExcludedCompartmentsDualListbox = function (dualListbox) {
  this._excludedCompartmentsDualListbox = dualListbox;
};

/**
 *
 * @returns {DualListbox}
 */
AbstractExportPanel.prototype.getExcludedCompartmentsDualListbox = function () {
  return this._excludedCompartmentsDualListbox;
};

/**
 *
 * @returns {Promise<Array>}
 */
AbstractExportPanel.prototype.getSelectedExcludedCompartments = function () {
  var self = this;
  var list = self.getExcludedCompartmentsDualListbox().selected;
  var names = [];
  var result = [];
  for (var i = 0; i < list.length; i++) {
    var element = list[i];
    names[element.dataset.id] = true;
  }
  return self._getAllCompartmentsAndPathways().then(function (compartments) {
    for (var i = 0; i < compartments.length; i++) {
      var compartment = compartments[i];
      if (names[compartment.getName()]) {
        result.push(compartment);
      }
    }
    return result;
  });
};

/**
 *
 * @param {ExportColumn[]} columns
 * @param {MiriamType[]} miriamTypes
 * @returns {Promise<string>}
 */
AbstractExportPanel.prototype.createResponseHeader = function (columns, miriamTypes) {
  var stringBuilder = [];
  var i;
  for (i = 0; i < columns.length; i++) {
    var column = columns[i];
    stringBuilder.push(column.name);
  }
  for (i = 0; i < miriamTypes.length; i++) {
    var miriamType = miriamTypes[i];
    stringBuilder.push(miriamType.getCommonName());
  }
  return Promise.resolve(stringBuilder.join("\t"));

};

/**
 *
 * @returns {ExportColumn[]}
 */
AbstractExportPanel.prototype.getBioEntityAllColumns = function () {
  return [{
    "columnName": "id",
    "method": "getId",
    "name": "Id"
  }, {
    "columnName": "description",
    "method": "getDescription",
    "name": "Description"
  }, {
    "columnName": "modelId",
    "method": "getModelId",
    "name": "Map id"
  }, {
    "columnName": "mapName",
    /**
     *
     * @param {BioEntity} bioEntity
     * @param {Project} project
     * @returns {string}
     */
    "method": function (bioEntity, project) {
      return project.getModelById(bioEntity.getModelId()).getName();
    },
    "name": "Map name"
  }, {
    "columnName": "symbol",
    "method": "getSymbol",
    "name": "Symbol"
  }, {
    "columnName": "abbreviation",
    "method": "getAbbreviation",
    "name": "Abbreviation"
  }, {
    "columnName": "synonyms",
    "method": "getSynonyms",
    "name": "Synonyms"
  }, {
    "columnName": "references",
    "name": "References",
    "method": function (bioEntity) {
      var references = bioEntity.getReferences();
      var stringBuilder = [];
      for (var i = 0; i < references.length; i++) {
        var reference = references[i];
        stringBuilder.push(reference.getType() + ":" + reference.getResource());
      }
      return stringBuilder.join(",");
    }
  }];
};


/**
 *
 * @param {BioEntity} bioEntity
 * @param {ExportColumn} column
 * @returns {Promise<string>}
 * @protected
 */
AbstractExportPanel.prototype._createResponseCell = function (bioEntity, column) {
  var valuePromise;
  if (Functions.isString(column.method)) {
    valuePromise = Promise.resolve(bioEntity[column.method]());
  } else {
    valuePromise = Promise.resolve(column.method(bioEntity, this.getProject()));
  }
  return valuePromise.then(function (value) {
    if (Functions.isString(value)) {
      value = value.replace(/[\n\r]/g, ' ');
    }
    return value;
  });
};

module.exports = AbstractExportPanel;
