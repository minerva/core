"use strict";

/* exported logger */
var $ = require('jquery');

var AbstractExportPanel = require('./AbstractExportPanel');

var Promise = require("bluebird");
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
var Functions = require('../../Functions');
var ValidationError = require('../../ValidationError');
var Alias = require('../../map/data/Alias');

var ACTIVITY_FLOW_TYPES = [
  "Reduced modulation",
  "Reduced physical stimulation",
  "Reduced trigger",
  "Unknown reduced modulation",
  "Unknown reduced physical stimulation",
  "Unknown reduced trigger",
  "Negative influence",
  "Positive influence",
  "Unknown negative influence",
  "Unknown positive influence"
];
var PROCESS_DESCRIPTION_TYPES = [
  "Dissociation",
  "Heterodimer association",
  "Inhibition",
  "Known transition omitted",
  "Modulation",
  "Physical stimulation",
  "State transition",
  "Transcription",
  "Translation",
  "Transport",
  "Trigger",
  "Truncation",
  "Unknown catalysis",
  "Unknown inhibition",
  "Unknown transition"
];

/**
 *
 * @param {Configuration} [params.configuration]
 * @param {ServerConnector} params.serverConnector
 * @param {HTMLElement} params.element
 * @param {Project} params.project
 * @param {string} [params.helpTip]
 * @param params.parent
 *
 * @constructor
 * @extends AbstractExportPanel
 */
function NetworkExportPanel(params) {
  params["panelName"] = "networkExport";
  AbstractExportPanel.call(this, params);
}

NetworkExportPanel.prototype = Object.create(AbstractExportPanel.prototype);
NetworkExportPanel.prototype.constructor = NetworkExportPanel;

/**
 *
 * @returns {Promise}
 */
NetworkExportPanel.prototype.init = function () {

  var self = this;
  var element = self.getElement();
  var configuration = self.getConfiguration();
  var elementTypeDiv = Functions.createElement({type: "div", name: "elementTypes"});
  elementTypeDiv.appendChild(self._createSelectTypeDiv(configuration.getElementTypes(), "Element type"));
  element.appendChild(elementTypeDiv);
  var reactionTypeDiv = Functions.createElement({type: "div", name: "reactionTypes"});
  reactionTypeDiv.appendChild(self._createSelectTypeDiv(
    [{
      name: "Activity flow",
      parentClass: "Reaction",
      className: "ActivityFlow"
    }, {
      name: "Process description",
      parentClass: "Reaction",
      className: "ProcessDescription"
    }], "Reaction type"));
  element.appendChild(reactionTypeDiv);
  var submapDiv = Functions.createElement({type: "div", name: "submapTypes"});
  submapDiv.appendChild(self._createSelectSubmapDiv(self.getProject().getModels()));
  element.appendChild(submapDiv);

  element.appendChild(self._createSelectColumnDiv(self.getAllColumns()));
  return self.getServerConnector().getProjectStatistics(self.getProject().getProjectId()).then(function (statistics) {
    return self._createMiriamTypeDiv(statistics.getReactionAnnotations());
  }).then(function (div) {
    element.appendChild(div);
    return self._createSelectIncludedCompartmentPathwayDiv();
  }).then(function (div) {
    element.appendChild(div);
    return self._createSelectExcludedCompartmentPathwayDiv();
  }).then(function (div) {
    element.appendChild(div);
    element.appendChild(self._createDownloadButton());
  }).then(function () {
    $(window).trigger('resize');
  });
};

/**
 *
 * @returns {*[]}
 */
NetworkExportPanel.prototype.getAllColumns = function () {
  return this.getBioEntityAllColumns().concat([{
    "columnName": "elements",
    "name": "Elements",
    "method": function (reaction) {
      var promises = [];
      /**
       *
       * @param {number|Alias} aliasId
       * @param {string} participantType
       */
      var elementFormat = function (aliasId, participantType) {
        if (aliasId instanceof Alias) {
          return participantType + ":" + aliasId.getId();
        } else {
          return participantType + ":" + aliasId;
        }
      };
      var i;
      for (i = 0; i < reaction.getReactants().length; i++) {
        promises.push(elementFormat(reaction.getReactants()[i].getAlias(), "REACTANT"));
      }
      for (i = 0; i < reaction.getProducts().length; i++) {
        promises.push(elementFormat(reaction.getProducts()[i].getAlias(), "PRODUCT"));
      }
      for (i = 0; i < reaction.getModifiers().length; i++) {
        promises.push(elementFormat(reaction.getModifiers()[i].getAlias(), "MODIFIER"));
      }
      return Promise.all(promises).then(function (values) {
        return values.join(",");
      });
    }
  }, {
    "columnName": "reactionId",
    "method": "getReactionId",
    "name": "Reaction external id"
  }, {
    "columnName": "mechanicalConfidenceScore",
    "method": "getMechanicalConfidenceScore",
    "name": "Mechanical Confidence Score"
  }, {
    "columnName": "lowerBound",
    "method": "getLowerBound",
    "name": "Lower Bound"
  }, {
    "columnName": "upperBound",
    "method": "getUpperBound",
    "name": "Upper Bound"
  }, {
    "columnName": "geneProteinReaction",
    "method": "getGeneProteinReaction",
    "name": "Gene Protein Reaction"
  }, {
    "columnName": "subsystem",
    "method": "getSubsystem",
    "name": "Subsystem"
  }, {
    "columnName": "type",
    "method": "getType",
    "name": "Reaction type"
  }]);
};

/**
 *
 * @param {Reaction} reaction
 * @param {Object<string,boolean>} elementIds
 * @param {string[]} reactionTypes
 * @param {number[]} submapIds
 * @returns {boolean}
 */
function matchReaction(reaction, elementIds, reactionTypes, submapIds) {
  var types = [];

  if ($.inArray("Activity flow", reactionTypes) >= 0) {
    types = types.concat(ACTIVITY_FLOW_TYPES);
  }
  if ($.inArray("Process description", reactionTypes) >= 0) {
    types = types.concat(PROCESS_DESCRIPTION_TYPES);
  }
  if ($.inArray(reaction.getType(), types) === -1) {
    return false;
  }
  if ($.inArray(reaction.getModelId(), submapIds) === -1) {
    return false;
  }
  var count = 0;
  reaction.getElements().forEach(function (element) {
    if (elementIds[element.getId()]) {
      count++;
    }
  });
  return count > 1;
}

/**
 *
 * @returns {Promise<string>}
 */
NetworkExportPanel.prototype.createResponseString = function () {
  var self = this;
  var elementTypes, reactionTypes, miriamTypes;
  var submapIds = self.getSelectedSubmapIds();
  var includedCompartmentIds = [];
  var excludedCompartmentIds = [];
  var models = self.getProject().getModels();

  var elementIds = [];
  var reactions = [];
  var elementTypesDiv = $("[name='elementTypes']", self.getElement())[0];
  var reactionTypesDiv = $("[name='reactionTypes']", self.getElement())[0];
  return self.getSelectedTypes(elementTypesDiv).then(function (result) {
    if (result.length === 0) {
      return Promise.reject(new ValidationError("You must select at least one element type"));
    }
    elementTypes = result;
    return self.getSelectedTypes(reactionTypesDiv);
  }).then(function (result) {
    if (result.length === 0) {
      return Promise.reject(new ValidationError("You must select at least one reaction type"));
    }
    reactionTypes = result;
    return self.getSelectedIncludedCompartments();
  }).then(function (result) {
    result.forEach(function (compartment) {
      includedCompartmentIds.push(compartment.getId());
    });
    return self.getSelectedExcludedCompartments();
  }).then(function (result) {
    result.forEach(function (compartment) {
      excludedCompartmentIds.push(compartment.getId());
    });

    var promises = [];
    for (var i = 0; i < models.length; i++) {
      promises.push(models[i].getAliases({
        type: elementTypes,
        complete: true,
        includedCompartmentIds: includedCompartmentIds,
        excludedCompartmentIds: excludedCompartmentIds
      }));
    }
    return Promise.all(promises);
  }).then(function (aliasesByModel) {
    var promises = [];
    for (var i = 0; i < models.length; i++) {
      promises.push(models[i].getReactionsForElements(aliasesByModel[i], true));
      for (var j = 0; j < aliasesByModel[i].length; j++) {
        elementIds[aliasesByModel[i][j].getId()] = true;
      }
    }
    return Promise.all(promises);
  }).then(function (reactionsByModel) {
    for (var i = 0; i < models.length; i++) {
      for (var j = 0; j < reactionsByModel[i].length; j++) {
        var reaction = reactionsByModel[i][j];
        if (matchReaction(reaction, elementIds, reactionTypes, submapIds)) {
          reactions.push(reaction);
        }
      }
    }
    return self.getSelectedMiriamTypes();
  }).then(function (result) {
    miriamTypes = result;
    return self.getSelectedColumns();
  }).then(function (selectedColumns) {
    if (selectedColumns.length === 0) {
      return Promise.reject(new ValidationError("You must select at least one column"));
    }

    var rowPromises = [];
    rowPromises.push(self.createResponseHeader(selectedColumns, miriamTypes));
    for (var i = 0; i < reactions.length; i++) {
      rowPromises.push(self.createResponseRow(reactions[i], selectedColumns, miriamTypes));
    }
    return Promise.all(rowPromises);
  }).then(function (rows) {
    return rows.join("\n");
  });
};

/**
 *
 * @param {Reaction} reaction
 * @param {Object} columns
 * @param {MiriamType[]} miriamTypes
 * @returns {Promise<string>}
 */
NetworkExportPanel.prototype.createResponseRow = function (reaction, columns, miriamTypes) {
  var valuePromises = [];
  var i, value;
  for (i = 0; i < columns.length; i++) {
    valuePromises.push(this._createResponseCell(reaction, columns[i]));
  }
  for (i = 0; i < miriamTypes.length; i++) {
    value = "";
    var miriamType = miriamTypes[i];
    var references = reaction.getReferences();
    for (var j = 0; j < references.length; j++) {
      var reference = references[j];
      if (reference.getType() === miriamType.getName()) {
        value += reference.getResource() + ",";
      }
    }
    valuePromises.push(value);
  }
  return Promise.all(valuePromises).then(function (values) {
    return values.join("\t");
  });
};

module.exports = NetworkExportPanel;
