"use strict";

/* exported logger */

var $ = require('jquery');
var autocomplete = require('autocomplete.js');

var Promise = require("bluebird");

var AbstractDbOverlay = require('../../map/overlay/AbstractDbOverlay');
var GuiConnector = require('../../GuiConnector');
var IdentifiedElement = require('../../map/data/IdentifiedElement');
var Panel = require('../Panel');
var PanelControlElementType = require('../PanelControlElementType');
var SearchBioEntityGroup = require('../../map/data/SearchBioEntityGroup');

var logger = require('../../logger');
var Functions = require('../../Functions');
var SearchMapBioEntityGroup = require("../../map/data/SearchMapBioEntityGroup");

/**
 *
 * @param {string} [params.placeholder]
 * @param {string} params.panelName
 * @constructor
 * @extends Panel
 */
function AbstractDbPanel(params) {
  Panel.call(this, params);
  var self = this;

  this._initializeGui(params.placeholder);
  this.setOverlayDb(self.getMap().getOverlayByName(params.panelName));
  this._createEventHandlers();

  this._tabIdCount = 0;
}

AbstractDbPanel.prototype = Object.create(Panel.prototype);
AbstractDbPanel.prototype.constructor = AbstractDbPanel;

/**
 *
 * @private
 */
AbstractDbPanel.prototype._createEventHandlers = function () {
  var self = this;
  var searchButton = self.getControlElement(PanelControlElementType.SEARCH_BUTTON);
  var searchInput = self.getControlElement(PanelControlElementType.SEARCH_INPUT);

  var searchByQuery = function () {
    var busyImage = $("[name=busyImage]", self.getElement());
    busyImage.show();
    return self.searchByQuery().finally(function () {
      busyImage.hide();
    }).catch(GuiConnector.alert);
  };

  searchButton.onclick = searchByQuery;
  searchInput.onkeypress = function (event) {
    if (event.keyCode === 13) {
      return searchByQuery();
    }
  };

  autocomplete(searchInput, {
    minLength: 1,
    hint: true
  }, {
    displayKey: function (suggestion) {
      return suggestion;
    },
    source: function (query, callback) {
      callback(self.getAutocomplete(query.toLowerCase()));
    }
  }).on('autocomplete:selected', function () {
    searchByQuery();
  });

  self.getOverlayDb().addListener("onSearch", function () {
    return self.refreshSearchResults();
  });

  self.getOverlayDb().addListener("onClear", function () {
    searchInput.value = "";
  });

  self.getOverlayDb().addListener("onSearch", function (data) {
    if (data.arg.type === AbstractDbOverlay.QueryType.SEARCH_BY_COORDINATES) {
      $(searchInput).val("");
    } else if (data.arg.type === AbstractDbOverlay.QueryType.SEARCH_BY_QUERY) {
      $(searchInput).val(data.arg.query);
      if (data.arg.query !== '') {
        self._showSearchTab();
      }
    } else if (data.arg.type === AbstractDbOverlay.QueryType.SEARCH_BY_TARGET_MAP) {
      $(searchInput).val("");
      if (data.arg.query !== '') {
        self._showSearchTab();
      }
    }
  });
};

/**
 *
 * @private
 */
AbstractDbPanel.prototype._showSearchTab = function () {
  var self = this;
  var link = $("a[href='#" + self.getElement().id + "']", self.getElement().parentNode.parentNode)[0];
  if (link !== undefined) {
    link.click();
  }
  if (self.getParent() !== undefined) {
    link = $("a[href='#" + self.getParent().getElement().id + "']", self.getParent().getElement().parentNode.parentNode)[0];
    if (link !== undefined) {
      link.click();
    }
  }
};

/**
 *
 * @param {string} [placeholder]
 * @private
 */
AbstractDbPanel.prototype._initializeGui = function (placeholder) {
  var searchQueryDiv = Functions.createElement({
    type: "div",
    name: "searchQuery",
    className: "searchPanel"
  });
  this.getElement().appendChild(searchQueryDiv);
  this.setControlElement(PanelControlElementType.SEARCH_DIV, searchQueryDiv);

  var searchLabel = Functions.createElement({
    type: "div",
    name: "searchLabel",
    content: "SEARCH:"
  });
  searchQueryDiv.appendChild(searchLabel);
  this.setControlElement(PanelControlElementType.SEARCH_LABEL, searchLabel);

  var searchInputDiv = Functions.createElement({
    type: "table"
  });
  searchQueryDiv.appendChild(searchInputDiv);

  var searchInputRow = Functions.createElement({
    type: "tr"
  });
  searchInputDiv.appendChild(searchInputRow);

  var searchInputCell = Functions.createElement({
    type: "td"
  });
  searchInputRow.appendChild(searchInputCell);

  var searchInput = Functions.createElement({
    type: "input",
    name: "searchInput",
    className: "input-field typeahead"
  });
  if (placeholder !== undefined) {
    searchInput.placeholder = placeholder;
  }
  searchInputCell.appendChild(searchInput);
  this.setControlElement(PanelControlElementType.SEARCH_INPUT, searchInput);

  var searchButtonCell = Functions.createElement({
    type: "td"
  });
  searchInputRow.appendChild(searchButtonCell);

  var searchButton = Functions.createElement({
    type: "a",
    href: "#",
    className: "minerva-search-link",
    content: "<i class='minerva-search-button'></i>",
    xss: false
  });

  searchButtonCell.appendChild(searchButton);
  this.setControlElement(PanelControlElementType.SEARCH_BUTTON, searchButton);

  var busyImage = Functions.createElement({
    type: "img",
    name: "busyImage",
    style: "display:block;margin-left: 40%;z-index: 10;display:none",
    src: "resources/images/icons/ajax-loader.gif"
  });
  this.getElement().appendChild(busyImage);

  var searchResultsDiv = Functions.createElement({
    type: "div",
    name: "searchResults",
    className: "tabbable boxed parentTabs"
  });
  this.getElement().appendChild(searchResultsDiv);

  var searchResultsNavTabDiv = Functions.createElement({
    type: "ul",
    className: "nav nav-tabs",
    content: '<li class="active"><a href="#set1"/></li>',
    xss: false
  });
  searchResultsDiv.appendChild(searchResultsNavTabDiv);
  this.setControlElement(PanelControlElementType.SEARCH_RESULTS_NAV_TAB, searchResultsNavTabDiv);

  var searchResultsContentTabDiv = Functions.createElement({
    type: "div",
    className: "tab-content",
    content: '<div class="tab-pane fade active in" name="set1" id="set1"/>',
    xss: false
  });
  searchResultsDiv.appendChild(searchResultsContentTabDiv);
  this.setControlElement(PanelControlElementType.SEARCH_RESULTS_CONTENT_TAB, searchResultsContentTabDiv);
};

/**
 *
 * @param {AbstractDbOverlay} overlayDb
 */
AbstractDbPanel.prototype.setOverlayDb = function (overlayDb) {
  if (overlayDb === undefined) {
    throw new Error("Undefined overlayDb for panel: " + this.getPanelName());
  }
  this._overlayDb = overlayDb;
};

/**
 *
 * @returns {AbstractDbOverlay}
 */
AbstractDbPanel.prototype.getOverlayDb = function () {
  return this._overlayDb;
};

/**
 *
 */
AbstractDbPanel.prototype.clearResults = function () {
  var navElement = this.getControlElement(PanelControlElementType.SEARCH_RESULTS_NAV_TAB);
  while (navElement.firstChild) {
    navElement.removeChild(navElement.firstChild);
  }

  var contentElement = this.getControlElement(PanelControlElementType.SEARCH_RESULTS_CONTENT_TAB);
  while (contentElement.firstChild) {
    contentElement.removeChild(contentElement.firstChild);
  }
};

/**
 *
 * @returns {Promise}
 */
AbstractDbPanel.prototype.refreshSearchResults = function () {
  var self = this;
  self.clearResults();
  var searchDb = self.getOverlayDb();
  var queries = searchDb.getQueries();

  var promises = [];
  for (var i = 0; i < queries.length; i++) {
    promises.push(searchDb.getElementsByQuery(queries[i]));
  }
  return Promise.all(promises).then(function (results) {
    return Promise.each(queries, function (query, index) {
      return self.addResultTab(query, results[index]);
    });
  }).then(function () {
    self.onresize();
  });
};

/**
 *
 * @return {Promise}
 */
AbstractDbPanel.prototype.init = function () {
  var self = this;
  self.clearResults();
  return self.addResultTab("{}", []).then(function () {
    self.onresize();
  });
};

/**
 *
 */
AbstractDbPanel.prototype.getAutocomplete = function () {
  logger.warn("Get autocomplete not implemented");
};

/**
 *
 */
AbstractDbPanel.prototype.searchByQuery = function () {
  throw new Error("searchByQuery is not implemented");
};

/**
 *
 * @param {string} query
 * @param {BioEntity[]} elements
 * @returns {Promise}
 */
AbstractDbPanel.prototype.addResultTab = function (query, elements) {
  var self = this;
  var name = JSON.parse(query).query;

  var tabId = this.getPanelName() + "Tab_" + this._tabIdCount;
  this._tabIdCount++;

  var navElement = this.getControlElement(PanelControlElementType.SEARCH_RESULTS_NAV_TAB);
  var contentElement = this.getControlElement(PanelControlElementType.SEARCH_RESULTS_CONTENT_TAB);
  var navClass = '';
  var contentClass = 'tab-pane';
  if (navElement.children.length === 0) {
    navClass = "active";
    contentClass = "tab-pane active";
  }

  if (name !== undefined) {
    if (name.length > 12) {
      name = name.substring(0, 10) + "...";
    }
  }
  if (query.name !== undefined) {
    name = query.name;
  }

  var navLi = self.getGuiUtils().createTabMenuObject({
    name: name,
    id: tabId,
    navigationBar: navElement,
    onclick: function () {
      var identifiedElements = [];
      for (var i = 0; i < elements.length; i++) {
        var element = elements[i].element;
        if (element instanceof SearchBioEntityGroup) {
          for (var j = 0; j < element.getBioEntities().length; j++) {
            identifiedElements.push(new IdentifiedElement(element.getBioEntities()[j]));
          }
        } else if (element instanceof SearchMapBioEntityGroup) {
          identifiedElements.push.apply(identifiedElements, element.getIdentifiedElements());
        } else {
          identifiedElements.push(new IdentifiedElement(element));
        }
      }
      return self.getOverlayDb().callListeners("onFocus", {query: query, identifiedElements: identifiedElements});
    }
  });
  navElement.appendChild(navLi);

  var contentDiv = document.createElement("div");
  contentDiv.className = "pre-scrollable " + contentClass;
  contentDiv.style.maxHeight = "10px";
  contentDiv.style.position = "relative";

  contentDiv.id = tabId;

  contentElement.appendChild(contentDiv);

  contentDiv.appendChild(this.createPreamble(elements.element));

  var tableDiv = document.createElement("table");
  tableDiv.className = "table table-bordered";
  contentDiv.appendChild(tableDiv);
  var tableBody = document.createElement("tbody");
  tableDiv.appendChild(tableBody);

  return Promise.each(elements, function (entry) {
    var element = entry.element;
    var icon = entry.icon;
    return self.createTableElement(element, icon).then(function (div) {
      return tableBody.appendChild(div);
    });
  });
};

/**
 * @param element
 * @param {string} icon
 *
 * @returns {Promise<HTMLTableRowElement>}
 */
AbstractDbPanel.prototype.createTableElement = function (element, icon) {
  return Promise.reject(new Error("Not implemented"));
};

/**
 *
 * @param {Target} target
 * @param {string} icon
 * @returns {Promise<HTMLTableRowElement>}
 */
AbstractDbPanel.prototype.createTargetRow = function (target, icon) {
  var self = this;
  var guiUtils = self.getGuiUtils();
  var result = document.createElement("tr");
  var iconColumn = document.createElement("td");
  iconColumn.style.width = "20px";
  iconColumn.style.verticalAlign = "middle";
  iconColumn.style.textAlign = "center";
  result.appendChild(iconColumn);
  var submaps = [];
  var i;
  if (target.getTargetElements().length > 0) {
    var submapAddedIds = [];
    var elements = target.getTargetElements();
    for (i = 0; i < elements.length; i++) {
      var elementId = elements[i].getModelId();
      if (elementId !== self.getMap().getId() && !submapAddedIds[elementId]) {
        submaps.push(elementId);
        submapAddedIds[elementId] = true;
      }
    }

    iconColumn.appendChild(guiUtils.createIcon({
      icon: icon, onclickFunction: function () {
        var maps = submaps.concat([self.getMap().getId()]);
        var promises = [];
        for (var j = 0; j < maps.length; j++) {
          var submap = self.getMap().getSubmapById(maps[j]);
          var submapElements = [];
          for (var k = 0; k < elements.length; k++) {
            if (elements[k].getModelId() === submap.getId()) {
              submapElements.push(elements[k]);
            }
          }
          if (submap.isInitialized()) {
            promises.push(submap.getModel().getByIdentifiedElements(submapElements, false).then(function (fetchedElements) {
              return self.getMap().getSubmapById(fetchedElements[0].getModelId()).fitBounds(fetchedElements);
            }));
          }
        }
        return Promise.all(promises);
      }
    }));
    var checkbox = document.createElement('input');
    checkbox.type = "checkbox";
    checkbox.checked = target.isVisible();
    checkbox.onclick = function () {
      target.setIsVisible(!target.isVisible());
      self.getOverlayDb().callListeners("onTargetVisibilityChange");
    };

    iconColumn.appendChild(checkbox);
  }

  var descColumn = document.createElement("td");
  result.appendChild(descColumn);

  descColumn.appendChild(guiUtils.createParamLine({label: "Name: ", value: target.getName()}));
  descColumn.appendChild(guiUtils.createAnnotations({
    label: "Elements: ",
    annotations: target.getTargetParticipants(),
    showType: true,
    inline: true,
    groupAnnotations: false
  }));
  descColumn.appendChild(guiUtils.createAnnotations({
    label: "References: ", annotations: target.getReferences(),
    showType: true,
    groupAnnotations: false
  }));
  if (submaps.length > 0) {
    descColumn.appendChild(guiUtils.createLabel("Available in submaps: "));
    for (i = 0; i < submaps.length; i++) {
      var model = self.getMap().getSubmapById(submaps[i]).getModel();
      var onclick = (function () {
        var id = model.getId();
        return function () {
          self.getMap().openSubmap(id);
        };
      }());
      var button = Functions.createElement({
        type: "button",
        className: "minerva-open-submap-button",
        content: model.getName(),
        onclick: onclick
      });
      descColumn.appendChild(button);
    }
  }
  return Promise.resolve(result);
};

/**
 *
 * @param {string[]} queries
 */
AbstractDbPanel.prototype.computeAutocompleteDictionary = function (queries) {
  var result = {};

  var i, j, k, mainString, substring, list;
  //compute dict for prefixes
  for (i = 0; i < queries.length; i++) {
    mainString = queries[i].toLowerCase();
    for (j = 0; j < mainString.length; j++) {
      substring = mainString.substring(0, j + 1);
      if (result[substring] !== undefined) {
        continue;
      }

      list = [];
      for (k = 0; k < 5; k++) {
        if (k + i >= queries.length) {
          break;
        } else if (queries[k + i].toLowerCase().indexOf(substring.toLowerCase()) === 0) {
          list.push(queries[k + i]);
        }
      }

      result[substring] = list;
    }
  }

  //and now we add substring from inside
  for (i = 0; i < queries.length; i++) {
    mainString = queries[i].toLowerCase();
    for (var startSubstring = 1; startSubstring < mainString.length; startSubstring++) {
      for (var endSubstring = startSubstring + 1; endSubstring <= mainString.length; endSubstring++) {
        substring = mainString.substring(startSubstring, endSubstring);
        if (mainString.indexOf(substring) !== 0) {
          if (result[substring] === undefined) {
            result[substring] = [];
          }
          if (result[substring].length < 5) {
            var found = false;
            for (k = 0; k < result[substring].length; k++) {
              found |= result[substring][k] === mainString;
            }
            if (!found) {
              result[substring].push(mainString);
            }
          }
        }
      }
    }
  }

  return result;
};


module.exports = AbstractDbPanel;
