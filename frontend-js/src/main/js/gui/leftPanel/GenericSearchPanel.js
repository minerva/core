"use strict";

var Promise = require("bluebird");
var $ = require('jquery');

/* exported logger */

var AbstractDbPanel = require('./AbstractDbPanel');
var Alias = require('../../map/data/Alias');
var ConfigurationType = require('../../ConfigurationType');
var GuiConnector = require('../../GuiConnector');
var IdentifiedElement = require('../../map/data/IdentifiedElement');
var InvalidArgumentError = require('../../InvalidArgumentError');
var PanelControlElementType = require('../PanelControlElementType');
var Reaction = require('../../map/data/Reaction');
var SearchBioEntityGroup = require('../../map/data/SearchBioEntityGroup');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
var Functions = require('../../Functions');
var SearchMapBioEntityGroup = require("../../map/data/SearchMapBioEntityGroup");
var AbstractDbOverlay = require("../../map/overlay/AbstractDbOverlay");
var QueryType = require("../../map/overlay/AbstractDbOverlay").QueryType;

/**
 *
 * @param params
 * @param {Configuration} params.configuration
 * @constructor
 * @extends AbstractDbPanel
 */
function GenericSearchPanel(params) {
  var self = this;
  params["panelName"] = "search";
  params["helpTip"] = "<p>search tab allows to search for particular elements or interactions in the map</p>"
    + "<p>perfect match tick box active: only terms with an exact match to the query will be returned</p>"
    + "<p>separate multiple search by semicolon</p>";
  params["placeholder"] = "keyword";

  AbstractDbPanel.call(self, params);
  self.getControlElement(PanelControlElementType.SEARCH_LABEL).innerHTML = "SEARCH IN CONTENT:";

  $(params.element).addClass("minerva-generic-search-panel");
  $(".minerva-generic-search-tab-link").css("border-top-color", "white");
  self.createSearchGui();

  self.getMap().addListener("onBioEntityClick", function (e) {
    var identifiedElement = e.arg;
    var queries = self.getOverlayDb().getQueries();
    var promises = [];
    for (var i = 0; i < queries.length; i++) {
      promises.push(self.getOverlayDb().getElementsByQuery(queries[i]));
    }
    return Promise.all(promises).then(function (elements) {
      var tab = -1;
      for (var i = 0; i < elements.length; i++) {
        for (var j = 0; j < elements[i].length; j++) {
          var element = elements[i][j].element;
          if (element instanceof SearchBioEntityGroup) {
            for (var k = 0; k < element.getBioEntities().length; k++) {
              if (new IdentifiedElement(element.getBioEntities()[k]).equals(identifiedElement)) {
                tab = i;
              }
            }
          } else if (new IdentifiedElement(element).equals(identifiedElement)) {
            tab = i;
          }
        }
      }
      var link = $(".parentTabs > .nav-tabs > li > a", self.getElement())[tab];
      if (link !== undefined) {
        return link.click();
      }
    });
  });

  self.getOverlayDb().addListener("onSearch", function (data) {
    if (data.arg.type === AbstractDbOverlay.QueryType.SEARCH_BY_COORDINATES) {
      if (data.arg.identifiedElements.length > 0 && data.arg.identifiedElements[0].length > 0) {
        var identifiedElement = data.arg.identifiedElements[0][0];
        var model = self.getMap().getSubmapById(identifiedElement.getModelId()).getModel();
        return model.getByIdentifiedElement(identifiedElement, true).then(function (element) {
          if (element instanceof Alias) {
            var link = element.getImmediateLink();
            if (link != null) {
              var tab = window.open(link, '_blank');
              if (tab) {
                tab.focus();
              } else {
                GuiConnector.warn('Browser prevented minerva from opening link: <a href="' + link + '" target="_blank">' + link + '</a>');
              }
            }
          }
        });
      }
    }
  });
}

GenericSearchPanel.prototype = Object.create(AbstractDbPanel.prototype);
GenericSearchPanel.prototype.constructor = GenericSearchPanel;

/**
 *
 */
GenericSearchPanel.prototype.createSearchGui = function () {
  var searchDiv = this.getControlElement(PanelControlElementType.SEARCH_DIV);

  var perfectMatchCheckbox = Functions.createElement({
    type: "input",
    name: "searchPerfectMatch",
    inputType: "checkbox"
  });
  searchDiv.appendChild(perfectMatchCheckbox);
  this.setControlElement(PanelControlElementType.SEARCH_PERFECT_MATCH_CHECKBOX, perfectMatchCheckbox);

  var perfectMatchLabel = Functions.createElement({
    type: "span",
    content: "PERFECT MATCH"
  });
  searchDiv.appendChild(perfectMatchLabel);
};

/**
 *
 * @param {SearchBioEntityGroup|BioEntity} element
 * @param {string} icon
 * @returns {Promise<HTMLTableRowElement>}
 * @override
 */
GenericSearchPanel.prototype.createTableElement = function (element, icon) {
  var self = this;
  if (element instanceof Alias) {
    return this.createAliasElement(element, icon);
  } else if (element instanceof Reaction) {
    return this.createReactionElement(element);
  } else if (element instanceof SearchBioEntityGroup) {
    return this.createSearchBioEntityGroupElement(element);
  } else if (element instanceof SearchMapBioEntityGroup) {
    var promises = [];
    element.getBioEntities().forEach(function (bioEntity) {
      promises.push(self.createTableElement(bioEntity, icon));
    });
    return Promise.all(promises).then(function (divs) {
      var guiUtils = self.getGuiUtils();

      var result = document.createElement("div");
      var table = document.createElement("table");
      var expandStyle = "fa-eye";
      var label = "";
      if (element.getModelId() !== self.getMap().getId()) {
        label = "Submap: ";
      } else {
        label = "Top map: ";
      }
      var headerDiv = guiUtils.createSubMapLink({
        label: label + "(" + element.getBioEntities().length + " hits): ",
        mapId: element.getModelId()
      });
      $(headerDiv).css("padding", "5px");
      $(table).hide();
      result.appendChild(headerDiv);

      headerDiv.appendChild(Functions.createElement({
        type: "a",
        className: "minerva-toggle-hide-show",
        content: '<i class="fa ' + expandStyle + '" style="font-size:17px"></i>&nbsp;',
        href: "#",
        onclick: function () {
          if ($(table).is(":visible")) {
            $(table).hide();
            $("a.minerva-toggle-hide-show>i", headerDiv).removeClass("fa-eye-slash");
            $("a.minerva-toggle-hide-show>i", headerDiv).addClass("fa-eye");
          } else {
            $(table).show();
            $("a.minerva-toggle-hide-show>i", headerDiv).removeClass("fa-eye");
            $("a.minerva-toggle-hide-show>i", headerDiv).addClass("fa-eye-slash");
          }
        },
        title: "Hide/Show",
        style: "float:right",
        xss: false
      }));

      $(table).css("width", "100%");
      result.appendChild(table);
      divs.forEach(function (htmlTag) {
        table.appendChild(htmlTag);
      });
      return result;
    });
  } else {
    throw new Error("Unknown element type: " + element.constructor.name);
  }
};

GenericSearchPanel.prototype.addResultTab = function (query, elements) {
  var queryObj = JSON.parse(query);
  if (queryObj.type === QueryType.SEARCH_BY_COORDINATES) {
    return AbstractDbPanel.prototype.addResultTab.call(this, query, elements);
  }
  var self = this;
  var groupsByMap = {};
  var groups = [];
  for (var i = 0; i < elements.length; i++) {
    var element = elements[i].element;
    var icon = elements[i].icon;
    if (element instanceof Alias || element instanceof Reaction) {
      element = new SearchBioEntityGroup(element);
      element.setIcon(icon);
    }
    if (groupsByMap[element.getModelId()] === undefined) {
      groupsByMap[element.getModelId()] = new SearchMapBioEntityGroup(element);
      groups.push({element: groupsByMap[element.getModelId()], icon: null});
    } else {
      groupsByMap[element.getModelId()].addBioEntity(element);
    }
  }
  return AbstractDbPanel.prototype.addResultTab.call(this, query, groups);
}

/**
 *
 * @returns {HTMLDivElement}
 */
GenericSearchPanel.prototype.createPreamble = function () {
  return document.createElement("div");
};

/**
 *
 * @param {Reaction} reaction
 * @returns {Promise<HTMLTableRowElement>}
 */
GenericSearchPanel.prototype.createReactionElement = function (reaction) {
  var self = this;
  var guiUtils = self.getGuiUtils();
  var result = document.createElement("tr");
  var td = document.createElement("td");
  result.appendChild(td);

  return guiUtils.createReactionElement({reaction: reaction}).then((function (div) {
    div.appendChild(guiUtils.createSeparator());
    td.appendChild(div);
    return result;
  }));
};

/**
 *
 * @param {Alias} alias
 * @param {string} [icon]
 * @returns {Promise<HTMLTableRowElement>}
 */
GenericSearchPanel.prototype.createAliasElement = function (alias, icon) {
  var self = this;
  var guiUtils = self.getGuiUtils();

  var result = document.createElement("tr");
  var td = document.createElement("td");
  result.appendChild(td);
  return guiUtils.createAliasElement({
    alias: alias,
    icon: icon
  }).then(function (div) {
    div.appendChild(guiUtils.createSeparator());
    td.appendChild(div);

    return result;
  });

};

/**
 *
 * @param {SearchBioEntityGroup} group
 * @returns {Promise<HTMLTableRowElement>}
 */
GenericSearchPanel.prototype.createSearchBioEntityGroupElement = function (group) {
  var self = this;
  var guiUtils = self.getGuiUtils();

  var result = document.createElement("tr");
  var td = document.createElement("td");
  result.appendChild(td);
  return guiUtils.createSearchBioEntityGroupElement(group).then(function (div) {
    div.appendChild(guiUtils.createSeparator());
    td.appendChild(div);

    return result;
  });

};

/**
 *
 * @returns {Promise}
 */
GenericSearchPanel.prototype.searchByQuery = function () {
  var self = this;
  var query = this.getControlElement(PanelControlElementType.SEARCH_INPUT).value;
  var perfect = this.getControlElement(PanelControlElementType.SEARCH_PERFECT_MATCH_CHECKBOX).checked;
  return self.getOverlayDb().searchByQuery(query, perfect, true).then(function (result) {
    var max = 0;
    for (var i = 0; i < result.length; i++) {
      max = Math.max(max, result[i].length);
    }
    if (max >= parseInt(self.getConfiguration().getOption(ConfigurationType.SEARCH_RESULT_NUMBER).getValue())) {
      GuiConnector.warn("Max number of results is exceeded - some results are omitted; " +
        "please narrow you criteria to see all results  or use 'perfect match'");
    }
  });
};

/**
 *
 * @param {string} query
 * @returns {string[]}
 */
GenericSearchPanel.prototype.getAutocomplete = function (query) {
  if (this._searchAutocomplete === undefined) {
    this.refreshSearchAutocomplete();
    return [];
  }

  return this._searchAutocomplete[query];
};

/**
 *
 * @returns {Promise}
 */
GenericSearchPanel.prototype.refreshSearchAutocomplete = function () {
  var self = this;
  self._searchAutocomplete = [];
  return ServerConnector.getSuggestedQueryList().then(function (queries) {
    self._searchAutocomplete = self.computeAutocompleteDictionary(queries);
    return self._searchAutocomplete;
  });
};

/**
 *
 * @returns {Promise}
 */
GenericSearchPanel.prototype.init = function () {
  var self = this;
  return AbstractDbPanel.prototype.init.call(this).then(function () {
    var query = ServerConnector.getSessionData().getSearchQuery();
    if (query !== undefined) {
      return self.getOverlayDb().searchByEncodedQuery(query, false).catch(function (error) {
        if (error instanceof InvalidArgumentError) {
          logger.warn(error.message);
        } else {
          throw error;
        }
      });
    }
  });
};


module.exports = GenericSearchPanel;
