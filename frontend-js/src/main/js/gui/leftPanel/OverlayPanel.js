"use strict";

/* exported logger */
var $ = require('jquery');
require('datatables.net')();
require('datatables.net-rowreorder')();

var AddOverlayDialog = require('../AddOverlayDialog');
var Panel = require('../Panel');
var PanelControlElementType = require('../PanelControlElementType');
var ValidationError = require('../../ValidationError');
var NetworkError = require('../../NetworkError');

var GuiConnector = require('../../GuiConnector');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
var Functions = require('../../Functions');

var Promise = require('bluebird');
var PrivilegeType = require("../../map/data/PrivilegeType");

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} params.customMap
 * @param {Configuration} [params.configuration]
 * @param {Project} [params.project]
 * @param params.parent
 *
 * @constructor
 * @extends Panel
 */
function OverlayPanel(params) {
  params["panelName"] = "overlays";
  params["scrollable"] = true;
  params["helpTip"] = "<p>Overlays tab allows to display or generate custom coloring of elements and interactions in the map.</p>"
    + "<p>General overlays are overlays accessible for every user viewing the content.</p>"
    + "<p>Custom overlays are user-provided overlays, this menu becomes available upon login (see below).</p>";
  if (params.project === undefined) {
    params.project = params.customMap.getProject();
  }
  Panel.call(this, params);

  //overflow is defined in minerva-overlay-panel, so remove the one that is already there
  $(params.element).css("overflow", "");
  $(params.element).addClass("minerva-overlay-panel");

  var self = this;

  self._createOverlayPanelGui();

  var addButton = $("[name='addOverlay']", self.getElement());
  var refreshButton = $("[name='refreshOverlays']", self.getElement());

  addButton.on("click", function () {
    return self.openAddOverlayDialog();
  });

  refreshButton.on("click", function () {
    return self.refresh();
  });

  var titleElement = this.getControlElement(PanelControlElementType.OVERLAY_CUSTOM_OVERLAY_TITLE);
  self.setCustomOverlaysMessage(titleElement.innerHTML);

  $(self.getElement()).on("click", "[name='overlayToggle']", function () {
    var thisCheckbox = this;
    var overlayId = $(thisCheckbox).attr("data");
    var toggleOverlayPromise;
    if (thisCheckbox.checked) {
      toggleOverlayPromise = self.getMap().openDataOverlay(overlayId);
    } else {
      toggleOverlayPromise = self.getMap().hideDataOverlay(overlayId);
    }
    $(thisCheckbox).prop("disabled", true);
    return toggleOverlayPromise.catch(GuiConnector.alert).finally(function () {
      $(thisCheckbox).prop("disabled", false);
      if (thisCheckbox.checked) {
        self.showLegend(overlayId);
      } else {
        self.hideLegend(overlayId);
      }
    });
  });
  $(self.getElement()).on("click", "[name='overlayLink']", function () {
    var overlayId = parseInt($(this).attr("data"));
    return self.getMap().openBackground(overlayId);
  });
  self.getMap().addListener("onBackgroundOverlayChange", function (e) {
    var overlayId = e.arg.toString();
    var buttons = $("[name='overlayLink']", self.getElement());
    for (var i = 0; i < buttons.length; i++) {
      var button = buttons[i];
      if ($(button).attr("data").toString() === overlayId) {
        $(button.parentNode.parentNode).addClass('active').siblings().removeClass('active');
      }
    }
  });
  $(self.getElement()).on("click", "[name='download-overlay']", function () {
    var overlayId = $(this).attr("data");
    return self.getServerConnector().getOverlaySourceDownloadUrl({
      overlayId: overlayId
    }).then(function (url) {
      return self.downloadFile(url);
    }).then(null, GuiConnector.alert);
  });
  $(self.getElement()).on("click", "[name='editButton']", function () {
    var overlayId = $(this).attr("data");
    return self.getProject().getDataOverlayById(overlayId).then(function (overlay) {
      return self.openEditOverlayDialog(overlay);
    });
  });
}

OverlayPanel.prototype = Object.create(Panel.prototype);
OverlayPanel.prototype.constructor = OverlayPanel;

OverlayPanel.prototype.showLegend = function (overlayId) {
  var name = 'overlay-legend-' + overlayId;
  var div;
  if ($('[name="' + name + '"').length === 0) {
    div = $("<div>", {name: name});
    var projectId = this.getProject().getProjectId()
    div.html('<img src="' + ServerConnector.getApiBaseUrl() + 'projects/' + projectId + '/overlays/' + overlayId + ':downloadLegend" alt="' + overlayId + ' legend" width="100%"/>')
    $(div).dialog({
      dialogClass: 'minerva-overlay-legend-dialog',
      autoOpen: false,
      resizable: false,
      position: {my: "left bottom", at: "left bottom"},
      width: "350px"
    });
    $(div).dialog('option', 'title', 'Overlay legend');
    this.getProject().getDataOverlayById(overlayId).then(function (overlay) {
      if (overlay !== null) {
        $(div).dialog('option', 'title', overlay.getName() + " overlay legend");
      }
    });
  } else {
    div = $('[name="' + name + '"');
  }
  $(div).dialog("open");
}

OverlayPanel.prototype.hideLegend = function (overlayId) {
  var name = 'overlay-legend-' + overlayId;
  var div = $('[name="' + name + '"');
  $(div).dialog("close");
}

/**
 *
 * @private
 */
OverlayPanel.prototype._createOverlayPanelGui = function () {
  var backgroundsDiv = Functions.createElement({
    type: "div",
    name: "backgroundsOverlays",
    className: "searchPanel"
  });
  this.getElement().appendChild(backgroundsDiv);

  var backgroundsTitle = Functions.createElement({
    type: "h5",
    content: "BACKGROUNDS:"
  });
  backgroundsDiv.appendChild(backgroundsTitle);

  var backgroundsTableDiv = Functions.createElement({
    type: "table",
    name: "backgroundsTab",
    className: "table table-bordered",
    style: "width:100%"
  });
  backgroundsDiv.appendChild(backgroundsTableDiv);

  var generalOverlaysDiv = Functions.createElement({
    type: "div",
    name: "generalOverlays",
    className: "searchPanel"
  });
  this.getElement().appendChild(generalOverlaysDiv);
  this.setControlElement(PanelControlElementType.OVERLAY_GENERAL_OVERLAY_DIV, generalOverlaysDiv);

  var generalOverlaysTitle = Functions.createElement({
    type: "h5",
    content: "GENERAL OVERLAYS:"
  });
  generalOverlaysDiv.appendChild(generalOverlaysTitle);

  var generalOverlaysTableDiv = Functions.createElement({
    type: "table",
    name: "generalOverlaysTab",
    className: "table table-bordered",
    style: "width:100%"
  });
  generalOverlaysDiv.appendChild(generalOverlaysTableDiv);
  this.setControlElement(PanelControlElementType.OVERLAY_GENERAL_OVERLAY_TABLE, generalOverlaysTableDiv);

  var customOverlaysDiv = Functions.createElement({
    type: "div",
    name: "customOverlays",
    className: "searchPanel"
  });
  this.getElement().appendChild(customOverlaysDiv);
  this.setControlElement(PanelControlElementType.OVERLAY_CUSTOM_OVERLAY_DIV, customOverlaysDiv);

  var customOverlaysTitle = Functions.createElement({
    type: "h5",
    name: "customOverlaysTitle",
    content: "USER-PROVIDED OVERLAYS:"
  });
  customOverlaysDiv.appendChild(customOverlaysTitle);
  this.setControlElement(PanelControlElementType.OVERLAY_CUSTOM_OVERLAY_TITLE, customOverlaysTitle);

  var customOverlaysTableDiv = Functions.createElement({
    type: "table",
    name: "customOverlaysTab",
    className: "table table-bordered",
    style: "width:100%"
  });
  customOverlaysDiv.appendChild(customOverlaysTableDiv);

  var centerTag = Functions.createElement({
    type: "div",
    style: "text-align: center;"
  });
  customOverlaysDiv.appendChild(centerTag);

  var addOverlayButton = Functions.createElement({
    type: "button",
    name: "addOverlay",
    content: "Add overlay",
    style: "margin: 4px;"
  });
  centerTag.appendChild(addOverlayButton);

  var refreshButton = Functions.createElement({
    type: "button",
    name: "refreshOverlays",
    content: "<span class=\"ui-icon ui-icon-refresh\"></span>&nbsp;Refresh",
    xss: false
  });
  centerTag.appendChild(refreshButton);
};

/**
 *
 */
OverlayPanel.prototype.clear = function () {
  var table = $("[name='generalOverlaysTab']", this.getElement()).DataTable();
  table.clear().draw();

  table = $("[name='customOverlaysTab']", this.getElement()).DataTable();
  table.clear().draw();
};

/**
 *
 * @param {boolean} [edit=false]
 * @returns {HTMLElement}
 */
OverlayPanel.prototype.createTableHeader = function (edit) {
  if (edit === undefined) {
    edit = false;
  }
  var result = document.createElement("thead");

  var row = document.createElement("tr");

  var nameTd = document.createElement("th");
  nameTd.innerHTML = "Name";
  row.appendChild(nameTd);

  var viewTd = document.createElement("th");
  viewTd.innerHTML = "View";
  row.appendChild(viewTd);

  var dataTd = document.createElement("th");
  dataTd.innerHTML = "Data";
  row.appendChild(dataTd);

  if (edit) {
    var editTd = document.createElement("th");
    editTd.innerHTML = "Edit";
    row.appendChild(editTd);
  }

  result.appendChild(row);
  return result;
};

/**
 *
 * @param {Background} background
 * @param {boolean} [checked=false]
 * @returns {HTMLElement}
 */
OverlayPanel.prototype.createBackgroundRow = function (background, checked) {
  var result = document.createElement("tr");

  if (checked) {
    result.className = "active";
  }

  var nameTd = Functions.createElement({type: "td", content: background.getName(), className: "word_wrap", xss: true});
  result.appendChild(nameTd);

  var viewTd = document.createElement("td");

  var link = Functions.createElement({
    type: "a",
    href: "#",
    className: "minerva-search-link",
    name: "overlayLink",
    data: background.getId(),
    content: "<i class='minerva-search-button'></i>",
    xss: false
  });
  viewTd.appendChild(link);

  result.appendChild(viewTd);

  result.title = background.getDescription();
  return result;
};

/**
 *
 * @param {DataOverlay} overlay
 * @param {boolean} checked
 * @returns {Array}
 */
OverlayPanel.prototype.overlayToDataRow = function (overlay, checked) {
  var result = [];
  result[0] = overlay.getOrder();
  result[1] = overlay.getName();

  var checkedString = "";
  if (checked) {
    checkedString = " checked ";
  }
  result[2] = "<input type='checkbox' " + checkedString + " data='" + overlay.getId() + "' name='overlayToggle'/>";

  result[3] = "<button data='" + overlay.getId() + "' name='download-overlay'><span class='ui-icon ui-icon-arrowthickstop-1-s'></span></button>";

  if (overlay.getCreator() !== "" && overlay.getCreator() !== undefined) {
    result[4] = "<button data='" + overlay.getId() + "' name='editButton'><span class='ui-icon ui-icon-document'></span></button>";
  } else {
    result[4] = "";
  }
  if (overlay.getDescription() !== "") {
    for (var i = 1; i < result.length; i++) {

      result[i] = "<div class=\"minerva-tooltip\">" + result[i] +
        "<span class=\"minerva-tooltip-text\">" + overlay.getDescription() + "</span>" +
        "</div>";
    }
  }
  return result;
};

/**
 *
 * @param {Background} background
 * @param {boolean} checked
 * @returns {Array}
 */
OverlayPanel.prototype.backgroundToDataRow = function (background, checked) {
  var result = [];
  result[0] = background.getOrder();
  result[1] = background.getName();

  result[2] = "<a href='#' class='minerva-search-link' data='" + background.getId() + "' name='overlayLink'><i class='minerva-search-button'></i></a>";
  result[3] = "";

  if (background.getCreator() !== "" && background.getCreator() !== undefined) {
    result[4] = "<button data='" + background.getId() + "' name='editButton'><span class='ui-icon ui-icon-document'></span></button>";
  } else {
    result[4] = "";
  }
  if (background.getDescription() !== "") {
    for (var i = 1; i < result.length; i++) {

      result[i] = "<div class=\"minerva-tooltip\">" + result[i] +
        "<span class=\"minerva-tooltip-text\">" + background.getDescription() + "</span>" +
        "</div>";
    }
  }
  return result;
};

/**
 *
 * @param {DataOverlay} overlay
 */
OverlayPanel.prototype.openEditOverlayDialog = function (overlay) {
  var self = this;
  var guiUtils = self.getGuiUtils();
  var content = document.createElement("fieldset");
  var nameInput = guiUtils.createInputText(overlay.getName());
  var row = guiUtils.createTableRow([guiUtils.createLabel("Name: "), nameInput]);
  content.appendChild(row);

  var descriptionInput = guiUtils.createTextArea(overlay.getDescription());
  row = guiUtils.createTableRow([guiUtils.createLabel("Description: "), descriptionInput]);
  content.appendChild(row);

  var buttons = [{
    text: "SAVE",
    click: function () {
      var windowSelf = this;
      overlay.setName(nameInput.value);
      overlay.setDescription(descriptionInput.value);

      return self.getServerConnector().updateOverlay(overlay).then(function () {
        return self.refresh();
      }).then(function () {
        $(windowSelf).dialog("close");
      }).catch(GuiConnector.alert);
    }
  }, {
    text: "REMOVE",
    click: function () {
      var windowSelf = this;
      return GuiConnector.showConfirmationDialog({
        message: "Do you want to delete overlay: " + overlay.getName() + "?"
      }).then(function (confirmation) {
        if (confirmation) {
          GuiConnector.showProcessing("Removing");
          return self.removeOverlay(overlay).then(function () {
            $(windowSelf).dialog("close");
          }).catch(function (error) {
            if (error instanceof NetworkError && error.statusCode === 404) {
              GuiConnector.warn("Overlay has been already removed.");
              $(windowSelf).dialog("close");
              return self.refresh();
            } else {
              GuiConnector.alert(error);
            }
          }).finally(function () {
            GuiConnector.hideProcessing();
          });
        }
      });
    }
  }, {
    text: "CANCEL",
    click: function () {
      $(this).dialog("close");
    }
  }];
  self.openDialog(content, {
    width: "600px",
    id: overlay.getId(),
    buttons: buttons,
    title: "Data overlay: " + overlay.getName(),
    className: "minerva-overlay-dialog"
  });
};

/**
 *
 * @param {boolean} [showDefault=false]
 * @returns {PromiseLike}
 */
OverlayPanel.prototype.refresh = function (showDefault) {
  if (showDefault === undefined) {
    showDefault = false;
  }
  var self = this;
  var user = null;

  var selectedOverlay = [];
  var selectedBackground = self.getMap().getBackgroundDataOverlay();
  var overlaysFromServer = [];
  var backgrounds = [];

  return self.getServerConnector().getLoggedUser().then(function (loggedUser) {
    user = loggedUser;
    return self.getMap().getVisibleDataOverlays();
  }).then(function (visibleDataOverlays) {
    for (var j = 0; j < visibleDataOverlays.length; j++) {
      selectedOverlay[visibleDataOverlays[j].getId()] = true;
    }

    return self.getServerConnector().getBackgrounds();
  }).then(function (result) {
    backgrounds = result;
    self.getProject().addOrUpdateBackgrounds(backgrounds);
    return self.getServerConnector().getOverlays({creator: user.getLogin()});
  }).then(function (userOverlays) {
    overlaysFromServer = userOverlays;
    return self.getServerConnector().getOverlays({publicOverlay: true});
  }).then(function (publicOverlays) {
    overlaysFromServer = overlaysFromServer.concat((publicOverlays));

    self.getProject().addOrUpdateDataOverlays(overlaysFromServer);

    if (user.getLogin() === "anonymous") {
      self.hideUserOverlaySection();
    }

    var overlays = self.getProject().getDataOverlays();
    for (var index = 0; index < overlays.length; index++) {
      var remove = true;
      for (var index2 = 0; index2 < overlaysFromServer.length; index2++) {
        if (overlaysFromServer[index2].getId() === overlays[index].getId()) {
          remove = false;
        }
      }
      if (remove) {
        self.getProject().removeDataOverlay(overlays[index]);
      }
    }

    if (backgrounds.length === 0) {
      if (self.getProject().getStatus() === "Archived") {
        return Promise.reject(new ValidationError("Project is archived."));
      } else {
        return Promise.reject(new ValidationError("Project doesn't have a background defined. Please re-upload map in admin panel."));
      }
    }
    if (!showDefault) {
      if (self.getMap().getBackgroundDataOverlay() == null) {
        return Promise.reject(new ValidationError("Project doesn't have a background defined. Please re-upload map in admin panel."));
      }
    } else {
      selectedBackground = backgrounds[0];
      backgrounds.forEach(function (background) {
        if (background.isDefaultOverlay()) {
          selectedBackground = background;
        }
      });
    }

    self.clear();

    var generalOverlays = [];
    var overlay;

    overlays = self.getProject().getDataOverlays();
    var customOverlays = [];
    for (var i = 0; i < overlays.length; i++) {
      overlay = overlays[i];
      if (overlay.getPublicOverlay()) {
        generalOverlays.push(overlay);
      } else {
        customOverlays.push(overlay);
      }

    }

    var backgroundsTable = $("[name='backgroundsTab']", self.getElement())[0];
    backgroundsTable.innerHTML = "<thead><tr><th>Name</th><th>View</th></tr></thead>";
    var body = document.createElement("tbody");
    backgroundsTable.appendChild(body);

    for (i = 0; i < backgrounds.length; i++) {
      var background = backgrounds[i];
      body.appendChild(self.createBackgroundRow(background, background === selectedBackground));
    }

    var generalOverlayTableElement = $("[name='generalOverlaysTab']", self.getElement());
    var generalOverlayTable = generalOverlayTableElement.on('order.dt', function () {
      var isAdmin = user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN));
      var isCurator = user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.IS_CURATOR)) &&
        user.hasPrivilege(self.getConfiguration().getPrivilegeType(PrivilegeType.WRITE_PROJECT), self.getProject().getProjectId());

      if (generalOverlayTableElement.dataTable().fnSettings().aaSorting[0][0] === 0
        && (isCurator || isAdmin)) {
        generalOverlayTable.rowReorder.enable();
      } else {
        generalOverlayTable.rowReorder.disable();
      }
    }).DataTable();

    generalOverlays.sort(function (o1, o2) {
      var val1 = o1.getId();
      var val2 = o2.getId();
      if (o1.getOrder() !== o2.getOrder()) {
        val1 = o1.getOrder();
        val2 = o2.getOrder();
      }
      if (val1 < val2) {
        return -1;
      }
      if (val1 > val2) {
        return 1;
      }
      return 0;
    });

    var generalOverlayData = [];
    for (i = 0; i < generalOverlays.length; i++) {
      overlay = generalOverlays[i];
      generalOverlayData.push(self.overlayToDataRow(overlay, selectedOverlay[overlay.getId()]));
    }
    generalOverlayTable.clear().rows.add(generalOverlayData).draw();

    if (generalOverlays.length > 0) {
      $(generalOverlayTable.parentNode).show();
    } else {
      $(generalOverlayTable.parentNode).hide();
    }

    var title = self.getControlElement(PanelControlElementType.OVERLAY_CUSTOM_OVERLAY_TITLE);
    var addButton = $("[name='addOverlay']", self.getElement());
    var refreshButton = $("[name='refreshOverlays']", self.getElement());
    var tableElement = $("[name='customOverlaysTab']", self.getElement());
    if (user.getLogin() === "anonymous") {
      self.hideUserOverlaySection();
    } else {
      tableElement.show();
      title.innerHTML = self.getCustomOverlaysMessage();
      addButton.show();
      refreshButton.show();

      var table = tableElement.on('order.dt', function () {
        if (tableElement.dataTable().fnSettings().aaSorting[0][0] === 0) {
          table.rowReorder.enable();
        } else {
          table.rowReorder.disable();
        }
      }).DataTable();
      var data = [];
      for (i = 0; i < customOverlays.length; i++) {
        overlay = customOverlays[i];
        data.push(self.overlayToDataRow(overlay, selectedOverlay[overlay.getId()]));
      }
      table.clear().rows.add(data).draw();

    }

    self.onresize();

    var promises = [];
    if (showDefault) {
      for (var key in selectedOverlay) {
        if (selectedOverlay.hasOwnProperty(key) && selectedOverlay[key]) {
          promises.push(self.getMap().openDataOverlay(key));
        }
      }
      self.getMap().openBackground(selectedBackground.getId());
    }
    return Promise.all(promises);
  }).then(function () {
    return self.getMap().getVisibleDataOverlays();
  }).then(function (visibleDataOverlays) {
    var promises = [];
    for (var i = 0; i < visibleDataOverlays.length; i++) {
      var overlay = visibleDataOverlays[i];
      if (self.getProject().getDataOverlayById(overlay.getId()) === undefined) {
        promises.push(self.getMap().hideDataOverlay(overlay.getId()));
      }
    }
    return Promise.all(promises);
  }).then(function () {
    return self.getMap().redrawSelectedDataOverlays();
  });
};

OverlayPanel.prototype.hideUserOverlaySection = function () {
  var self = this;

  var title = self.getControlElement(PanelControlElementType.OVERLAY_CUSTOM_OVERLAY_TITLE);
  var addButton = $("[name='addOverlay']", self.getElement());
  var refreshButton = $("[name='refreshOverlays']", self.getElement());
  var tableElement = $("[name='customOverlaysTab']", self.getElement());
  title.innerHTML = 'YOU ARE NOT LOGGED IN. PLEASE, <a href="#">LOG IN</a> '
    + 'TO UPLOAD AND VIEW CUSTOM OVERLAYS<br/><center><button>LOGIN</button></center>';
  var openLoginDialog = function () {
    return self.getParent().getLoginDialog().open();
  };
  $(title).find("a")[0].onclick = openLoginDialog;
  $(title).find("button")[0].onclick = openLoginDialog;
  addButton.hide();
  refreshButton.hide();
  tableElement.hide();
}

/**
 *
 * @param {string} customOverlaysMessage
 */
OverlayPanel.prototype.setCustomOverlaysMessage = function (customOverlaysMessage) {
  this._customOverlaysMessage = customOverlaysMessage;
};

/**
 *
 * @returns {string}
 */
OverlayPanel.prototype.getCustomOverlaysMessage = function () {
  return this._customOverlaysMessage;
};


/**
 *
 * @returns {Promise}
 */
OverlayPanel.prototype.openAddOverlayDialog = function () {
  var self = this;
  if (self._addOverlayDialog !== undefined) {
    self._addOverlayDialog.destroy();
  }
  self._addOverlayDialog = new AddOverlayDialog({
    project: self.getProject(),
    customMap: self.getMap(),
    element: document.createElement("div"),
    configuration: self.getConfiguration()
  });
  return self._addOverlayDialog.init().then(function () {
    return self._addOverlayDialog.open();
  });
};

/**
 *
 * @returns {PromiseLike}
 */
OverlayPanel.prototype.init = function () {
  var self = this;
  var backgroundOverlay = self.getServerConnector().getSessionData().getSelectedBackgroundOverlay();
  var showDefault = (backgroundOverlay === undefined || backgroundOverlay === "undefined");
  var generalTable = $("[name='generalOverlaysTab']", self.getElement()).DataTable({
    columns: [{
      title: 'No',
      type: 'num',
      className: "no_padding"
    }, {
      title: 'Name',
      className: "word_wrap"
    }, {
      title: 'View',
      orderable: false,
      className: "no_padding"
    }, {
      title: 'Data',
      orderable: false,
      className: "no_padding"
    }],
    paging: false,
    searching: false,
    info: false,
    rowReorder: true
  });

  generalTable.on('row-reorder', function (e, diff) {
    var promises = [];

    for (var i = 0, ien = diff.length; i < ien; i++) {
      var rowData = generalTable.row(diff[i].node).data();
      var checkbox = $("[type='checkbox']", rowData[2])[0];
      if (checkbox === undefined) {
        checkbox = rowData[2];
      }
      var overlayId = parseInt($(checkbox).attr("data"));
      var order;
      if (Functions.isInt(diff[i].newData)) {
        order = diff[i].newData;
      } else {
        order = parseInt($.parseHTML(diff[i].newData)[0].innerHTML);
      }
      promises.push(self.updateOverlayOrder(overlayId, order));
    }
    promises.push(self.getMap().redrawSelectedDataOverlays());
    return Promise.all(promises).catch(GuiConnector.alert);
  });

  var customTable = $("[name='customOverlaysTab']", self.getElement()).DataTable({
    columns: [{
      title: 'No',
      type: 'num',
      className: "no_padding"
    }, {
      title: 'Name',
      className: "word_wrap"
    }, {
      title: 'View',
      orderable: false,
      className: "no_padding"
    }, {
      title: 'Data',
      orderable: false,
      className: "no_padding"
    }, {
      title: 'Edit',
      orderable: false,
      className: "no_padding"
    }],
    paging: false,
    searching: false,
    info: false,
    rowReorder: true
  });
  customTable.on('row-reorder', function (e, diff) {
    var promises = [];

    for (var i = 0, ien = diff.length; i < ien; i++) {
      var rowData = customTable.row(diff[i].node).data();
      var checkbox = $("[type='checkbox']", rowData[2])[0];
      if (checkbox === undefined) {
        checkbox = rowData[2];
      }
      var overlayId = parseInt($(checkbox).attr("data"));
      var order;
      if (Functions.isInt(diff[i].newData)) {
        order = diff[i].newData;
      } else {
        order = parseInt($.parseHTML(diff[i].newData)[0].innerHTML);
      }
      promises.push(self.updateOverlayOrder(overlayId, order));
    }
    promises.push(self.getMap().redrawSelectedDataOverlays());
    return Promise.all(promises);
  });

  self.getServerConnector().addListener("onAddDataOverlay", function (e) {
    self.getProject().addDataOverlay(e.arg);
    return self.refresh();
  });
  self.getServerConnector().addListener("onRemoveDataOverlay", function (e) {
    self.getProject().removeDataOverlay(e.arg);
    return self.refresh();
  });


  return this.refresh(showDefault);
};

/**
 *
 * @param {number} overlayId
 * @param {number} order
 * @returns {Promise}
 */
OverlayPanel.prototype.updateOverlayOrder = function (overlayId, order) {
  var self = this;
  return self.getProject().getDataOverlayById(overlayId).then(function (overlay) {
    overlay.setOrder(order);
    return self.getServerConnector().updateOverlay(overlay);
  });
};

/**
 *
 * @param {DataOverlay} overlay
 * @returns {PromiseLike<any>}
 */
OverlayPanel.prototype.removeOverlay = function (overlay) {
  var self = this;
  return self.getMap().hideDataOverlay(overlay.getId()).then(function () {
    return self.getServerConnector().removeOverlay({overlayId: overlay.getId()});
  })
};

/**
 *
 * @returns {PromiseLike}
 */
OverlayPanel.prototype.destroy = function () {
  var self = this;
  Panel.prototype.destroy.call(this);
  var generalOverlayDataTable = $("[name='generalOverlaysTab']", self.getElement());
  if ($.fn.DataTable.isDataTable(generalOverlayDataTable)) {
    $(generalOverlayDataTable).DataTable().destroy();
  }

  var customOverlayDataTable = $("[name='customOverlaysTab']", self.getElement());
  if ($.fn.DataTable.isDataTable(customOverlayDataTable)) {
    $(customOverlayDataTable).DataTable().destroy();
  }

  if (self._addOverlayDialog !== undefined) {
    return self._addOverlayDialog.destroy();
  } else {
    return Promise.resolve();
  }
};

module.exports = OverlayPanel;
