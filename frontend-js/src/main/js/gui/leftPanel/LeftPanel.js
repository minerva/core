"use strict";

var Promise = require("bluebird");
var $ = require('jquery');

/* exported logger */

var AbstractGuiElement = require('../AbstractGuiElement');
var AbstractDbOverlay = require('../../map/overlay/AbstractDbOverlay');
var Alias = require('../../map/data/Alias');
var GuiUtils = require('./GuiUtils');
var Header = require('../Header');
var LoginDialog = require('../LoginDialog');
var OverlayPanel = require('./OverlayPanel');
var PointData = require('../../map/data/PointData');
var ProjectInfoPanel = require('./ProjectInfoPanel');
var Reaction = require('../../map/data/Reaction');
var SearchPanel = require('./SearchPanel');
var SubmapPanel = require('./SubmapPanel');

var Functions = require('../../Functions');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} params.customMap
 * @param {Configuration} params.configuration
 * @param {Project} [params.project]
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 *
 * @extends AbstractGuiElement
 */
function LeftPanel(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self._createPanelGui();
}

LeftPanel.prototype = Object.create(AbstractGuiElement.prototype);
LeftPanel.prototype.constructor = LeftPanel;

/**
 *
 * @private
 */
LeftPanel.prototype._createPanelGui = function () {
  var self = this;
  var panels = self.getPanelsDefinition();

  var headerDiv = Functions.createElement({
    type: "div"
  });
  var header = new Header({
    element: headerDiv,
    customMap: self.getMap(),
    optionsMenu: true,
    configuration: self.getConfiguration(),
    parent: this
  });
  self.getElement().appendChild(headerDiv);

  var loginDialogDiv = Functions.createElement({
    type: "div",
    style: "display:none"
  });
  self.getElement().appendChild(loginDialogDiv);
  this.setLoginDialog(new LoginDialog({
    element: loginDialogDiv,
    customMap: self.getMap()
  }));

  self.getGuiUtils().initTabContent(self);

  self.setHeader(header);

  self.elementInfoDiv = Functions.createElement({
    type: "div",
    style: "background-color:#f3f3f3",
    className: "minerva-element-info-div"
  });

  for (var i = 0; i < panels.length; i++) {
    self.getGuiUtils().addTab(self, panels[i]);
  }

  self.getElement().appendChild(Functions.createElement({
    type: "div",
    className: "minerva-logo-footer",
    content: "<a href='https://minerva.pages.uni.lu/doc/' target='_blank'>" +
      "<div class='minerva-pages-logo'></div><span>Powered by MINERVA Platform (v" + self.getConfiguration().getVersion() + ")</span></a>",
    xss: false
  }));
};

/**
 *
 * @returns {*[]}
 */
LeftPanel.prototype.getPanelsDefinition = function () {
  return [{
    name: "<div class='minerva-tab-link-icon'><i class='fa fa-search'></i><br>SEARCH</div>",
    panelClass: SearchPanel,
    linkClassName: "minerva-search-tab-link",
    options: {parent: this}
  }, {
    name: "<div class='minerva-tab-link-icon'><i class='fa fa-th-list'></i><br>OVERLAYS</div>",
    panelClass: OverlayPanel,
    linkClassName: "minerva-overlay-tab-link",
    options: {parent: this}
  }, {
    name: "<div class='minerva-tab-link-icon'><i class='fa fa-sitemap'></i><br>SUBMAPS</div>",
    panelClass: SubmapPanel,
    linkClassName: "minerva-submap-tab-link",
    options: {parent: this}
  }, {
    name: "<div class='minerva-tab-link-icon'><i class='fa fa-info'></i><br>INFO</div>",
    panelClass: ProjectInfoPanel,
    linkClassName: "minerva-info-tab-link",
    options: {parent: this}
  }];
};

/**
 *
 * @returns {Promise}
 */
LeftPanel.prototype.init = function () {
  var self = this;

  var promises = [];
  for (var i = 0; i < self._panels.length; i++) {
    promises.push(self._panels[i].init());
  }
  promises.push(self.getHeader().init());

  var initEvents = new Promise(function (resolve) {
    self.getMap().addListener("onBioEntityClick", function (e) {
      return self.showElementDetails(e.arg);
    });
    self.getMap().getOverlayByName("search").addListener("onSearch", function (e) {
      if (e.arg.type === AbstractDbOverlay.QueryType.SEARCH_BY_COORDINATES ||
        e.arg.type === AbstractDbOverlay.QueryType.SEARCH_BY_TARGET) {
        return self.showElementDetails(e.arg.identifiedElements[0][0]);
      } else {
        return self.showElementDetails(undefined);
      }
    });
    resolve();
  });
  promises.push(initEvents);
  promises.push(self.getLoginDialog().init());
  return Promise.all(promises);
};

/**
 *
 * @param {IdentifiedElement} element
 * @returns {Promise}
 */
LeftPanel.prototype.showElementDetails = function (element) {
  var self = this;
  var div = self.elementInfoDiv;
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      dialogClass: 'minerva-element-info-dialog',
      resizable: false,
      width: $(self.getElement()).width(),
      height: 200,
      beforeclose: function () {
        $(this).dialog('option', 'position', [$(this).offset().left, $(this).offset().top]);
        $(this).dialog('option', 'width', $(this).width());
        $(this).dialog('option', 'height', $(this).height());
      },
      position: {
        my: "left bottom",
        at: "left bottom",
        of: $(self.getElement())
      }
    }).siblings('.ui-dialog-titlebar').css("background", "gray");
  }

  var openedTabs = $("[name='tabView'] > ul > li > a.active");
  var openTabName = openedTabs[0].innerHTML;
  var searchTabName = openedTabs[1].innerHTML;
  var isPanelHidden = (self.getElement().style.display === "none");
  if (isPanelHidden) {
    openTabName = undefined;
  }

  if (element !== undefined && openTabName !== undefined && (openTabName.indexOf("SEARCH") === -1 || searchTabName !== "CONTENT")) {
    var model = self.getMap().getSubmapById(element.getModelId()).getModel();
    var bioEntity;
    return model.getByIdentifiedElement(element, true).then(function (result) {
      bioEntity = result;
      return self.prepareElementDetailsContent(bioEntity);
    }).then(function (elementDetailsDiv) {
      div.innerHTML = "";
      div.appendChild(elementDetailsDiv);
      $(div).dialog("open");
      $(div).dialog("option", "title", self.getElementTitle(bioEntity));
      $(div).scrollTop(0);
    });
  } else {
    $(div).dialog("close");
    return Promise.resolve();
  }
};

/**
 *
 * @param {BioEntity} bioEntity
 *
 * @returns {Promise<HTMLDivElement>}
 */
LeftPanel.prototype.prepareElementDetailsContent = function (bioEntity) {
  var guiUtils = this.getGuiUtils();
  if (bioEntity instanceof Reaction) {
    return guiUtils.createReactionElement({
      forceExpand: true,
      reaction: bioEntity,
      showTitle: false
    });
  } else if (bioEntity instanceof Alias) {
    return guiUtils.createAliasElement({
      forceExpand: true,
      alias: bioEntity,
      showTitle: false
    });
  } else if (bioEntity instanceof PointData) {
    return Promise.resolve(Functions.createElement({
      type: "div"
    }));
  } else {
    throw new Error("Unknown element type:" + bioEntity);
  }
};

/**
 *
 * @returns {GuiUtils}
 */
LeftPanel.prototype.getGuiUtils = function () {
  var self = this;
  if (self._guiUtils === undefined) {
    self._guiUtils = new GuiUtils(self.getMap().getConfiguration());
    self._guiUtils.setMap(self.getMap());
  }
  return self._guiUtils;
};

/**
 *
 * @param {BioEntity} bioEntity
 * @returns {string}
 */
LeftPanel.prototype.getElementTitle = function (bioEntity) {
  if (bioEntity instanceof Reaction) {
    return bioEntity.getType() + ": " + bioEntity.getReactionId();
  } else if (bioEntity instanceof Alias) {
    return bioEntity.getType() + ": " + bioEntity.getName();
  } else if (bioEntity instanceof PointData) {
    return "POINT: " + bioEntity.getId();
  } else {
    throw new Error("Unknown element type:" + bioEntity);
  }
};

/**
 *
 */
LeftPanel.prototype.hide = function () {
  this.getElement().style.display = "none";
};

/**
 *
 */
LeftPanel.prototype.show = function () {
  this.getElement().style.display = "block";
};

/**
 *
 * @param {Header} header
 */
LeftPanel.prototype.setHeader = function (header) {
  this._header = header;
};

/**
 *
 * @returns {Header}
 */
LeftPanel.prototype.getHeader = function () {
  return this._header;
};

/**
 *
 * @param {LoginDialog} loginDialog
 */
LeftPanel.prototype.setLoginDialog = function (loginDialog) {
  this._loginDialog = loginDialog;
};

/**
 *
 * @returns {LoginDialog}
 */
LeftPanel.prototype.getLoginDialog = function () {
  return this._loginDialog;
};

/**
 *
 * @param {PluginManager} pluginManager
 */
LeftPanel.prototype.setPluginManager = function (pluginManager) {
  this._pluginManager = pluginManager;
  this.getHeader().setPluginManager(pluginManager);
};

/**
 *
 * @returns {PluginManager}
 */
LeftPanel.prototype.getPluginManager = function () {
  return this._pluginManager;
};

/**
 *
 * @returns {Promise}
 */
LeftPanel.prototype.destroy = function () {
  var self = this;
  var promises = [];
  promises.push(self.getHeader().destroy());
  var div = self.elementInfoDiv;

  var destroyPanel = new Promise(function (resolve) {
    if ($(div).hasClass("ui-dialog-content")) {
      $(div).dialog("destroy");
    }
    resolve();
  });
  promises.push(destroyPanel);
  promises.push(self.getLoginDialog().destroy());
  for (var i = 0; i < self._panels.length; i++) {
    promises.push(self._panels[i].destroy());
  }

  if (self._pluginManager !== undefined) {
    promises.push(self._pluginManager.destroy());
  }

  return Promise.all(promises);
};

module.exports = LeftPanel;
