"use strict";

/* exported logger */
var $ = require('jquery');

var Alias = require('../../map/data/Alias');
var ConfigurationType = require('../../ConfigurationType');
var IdentifiedElement = require('../../map/data/IdentifiedElement');
var Reaction = require('../../map/data/Reaction');
var SearchBioEntityGroup = require('../../map/data/SearchBioEntityGroup');


var GuiConnector = require('../../GuiConnector');
var AbstractGuiElement = require('../AbstractGuiElement');
var Functions = require('../../Functions');

var logger = require('../../logger');
var xss = require('xss');

var Promise = require('bluebird');

var tabIdCounter = 0;

/**
 *
 * @param {Configuration} configuration
 * @constructor
 */
function GuiUtils(configuration) {
  var self = this;
  self.setConfiguration(configuration);
}

GuiUtils.prototype = Object.create(AbstractGuiElement.prototype);
GuiUtils.prototype.constructor = GuiUtils;

/**
 *
 * @param {Configuration} configuration
 */
GuiUtils.prototype.setConfiguration = function (configuration) {
  this._configuration = configuration;
};

/**
 *
 * @returns {Configuration}
 */
GuiUtils.prototype.getConfiguration = function () {
  return this._configuration;
};

/**
 *
 * @param {string} value
 * @returns {HTMLSpanElement}
 */
GuiUtils.prototype.createLabel = function (value) {
  var result = document.createElement("span");
  result.innerHTML = value;
  result.className = "minerva-label";
  return result;
};

/**
 *
 * @param {Object} modification
 * @param {string} [modification.name]
 * @param {string} modification.type
 * @param {string} [modification.state]
 *
 * @returns {HTMLElement| null}
 */
GuiUtils.prototype.createModificationRow = function (modification) {
  var self = this;
  var row = null;
  var name = modification.name;
  var desc = undefined;
  if (modification.state !== undefined && modification.state !== null) {
    var modificationStateType = self.getConfiguration().getModificationStateTypeByName(modification.state);
    var state = modificationStateType.getCommonName();

    if (name !== null && name !== undefined && name !== "") {
      desc = state + " at position " + name + ", ";
    } else {
      desc = state + ",";
    }
  } else if (name !== null && name !== undefined && name !== "" &&
    (modification.type === "BINDING_REGION" ||
      modification.type === "CODING_REGION" ||
      modification.type === "PROTEIN_BINDING_DOMAIN" ||
      modification.type === "TRANSCRIPTION_SITE_LEFT" ||
      modification.type === "TRANSCRIPTION_SITE_RIGHT" ||
      modification.type === "REGULATORY_REGION"
    )) {
    desc = name + ",";
  }

  if (desc !== undefined) {
    row = Functions.createElement({
      type: "li"
    });
    row.appendChild(self.createLabelText(desc));
  }
  return row;
};

/**
 *
 * @param {string} label
 * @param {Object[]} value
 * @returns {HTMLDivElement}
 */
GuiUtils.prototype.createPostTranslationalModifications = function (label, value) {
  var result = document.createElement("div");
  var count = 0;
  if (value !== undefined && value.length > 0) {
    var self = this;
    var userFriendlyLabel = label + ": ";
    if (label === "RESIDUE") {
      userFriendlyLabel = "Posttranslational modifications: "
    } else if (label === "BINDING_REGION") {
      userFriendlyLabel = "Binding regions: "
    } else if (label === "CODING_REGION") {
      userFriendlyLabel = "Coding regions: "
    } else if (label === "PROTEIN_BINDING_DOMAIN") {
      userFriendlyLabel = "Protein binding domains: "
    } else if (label === "TRANSCRIPTION_SITE_LEFT") {
      userFriendlyLabel = "Transcription sites (left): "
    } else if (label === "TRANSCRIPTION_SITE_RIGHT") {
      userFriendlyLabel = "Transcription sites (right): "
    } else if (label === "TRANSCRIPTION_SITE") {
      userFriendlyLabel = "Transcription sites: "
    } else if (label === "MODIFICATION_SITE") {
      userFriendlyLabel = "Modification sites: "
    }
    result.appendChild(self.createLabel(userFriendlyLabel));
    result.appendChild(self.createNewLine());
    var list = Functions.createElement({
      type: "ul"
    });
    for (var i = 0; i < value.length; i++) {
      var modification = value[i];
      var row = self.createModificationRow(modification);
      if (row !== null) {
        list.appendChild(row);
        count++;
      }
    }
    result.appendChild(list);
  }
  if (count > 0) {
    return result;
  } else {
    return document.createElement("div");
  }
};

/**
 *
 * @returns {HTMLHRElement}
 */
GuiUtils.prototype.createSeparator = function () {
  return document.createElement("hr");
};

/**
 *
 * @param {number} [count]
 * @returns {HTMLParagraphElement}
 */
GuiUtils.prototype.createNewLine = function (count) {
  if (count === undefined) {
    count = 0;
  }
  var result = document.createElement("p");
  if (count > 0) {
    result.style.height = ((count - 1) * 10) + "px";
  }
  return result;
};

/**
 *
 * @param {string} [url]
 * @param {string} name
 * @returns {HTMLElement}
 */
GuiUtils.prototype.createLink = function (url, name) {
  if (url === null || url === undefined) {
    logger.warn("URL not defined for: \"" + name + "\" link");
    return Functions.createElement({type: "span", content: name});
  }
  var link = document.createElement("a");
  link.href = url;
  link.innerHTML = name;
  link.target = "_blank";
  link.style.textDecoration = "underline";
  return link;
};

/**
 *
 * @param {Annotation} annotation
 * @param {boolean} [showType=false]
 * @returns {HTMLElement}
 */
GuiUtils.prototype.createAnnotationLink = function (annotation, showType) {
  var self = this;
  var name, type, hint;
  //replace encoded parts of uri (like %2F - > "/")
  name = decodeURIComponent(annotation.getResource());
  var article = annotation.getArticle();
  if (article !== undefined) {

    hint = article.getTitle() + " " + article.getAuthors().join(", ") + ", " + article.getYear() + ", "
      + article.getJournal();
    type = "PUBMED";
  } else {
    var miriamType = self.getConfiguration().getMiriamTypeByName(annotation.getType());
    if (miriamType === null) {
      logger.warn("Unknown miriam type: " + annotation.getType());
      type = annotation.getType();
    } else {
      type = miriamType.getCommonName();
    }
  }
  var link;
  if (showType) {
    link = self.createLink(annotation.getLink(), type + " (" + name + ")");
  } else {
    link = self.createLink(annotation.getLink(), name);
  }
  if (hint !== undefined) {
    var div = document.createElement("div");
    div.title = hint;
    div.appendChild(link);
    return div;
  } else {
    return link;
  }
};

/**
 *
 * @param {Object} [params]
 * @param {string} params.label
 * @param {string} [params.className]
 * @param {Annotation[]} [params.annotations]
 * @param {boolean} [params.inline = false]
 * @param {boolean} [params.showType = true]
 * @param {boolean} [params.groupAnnotations = true]
 *
 * @returns {HTMLElement}
 */
GuiUtils.prototype.createAnnotations = function (params) {
  var self = this;

  var result = Functions.createElement({type: "div", className: params.className});
  result.appendChild(self.createLabel(params.label));
  if (params.annotations !== undefined && params.annotations.length > 0) {
    result.appendChild(self.createAnnotationList(params.annotations, params));
  } else {
    result.appendChild(self.createLabelText("No annotations"));
  }
  return result;
};

/**
 *
 * @param {boolean} inline
 * @param {string} annotatorClass
 * @param {Object.<string, Annotator>} annotatorsClassMapping
 * @param {boolean} groupAnnotations
 * @returns {HTMLElement}
 */
function createGroupContainer(inline, annotatorClass, annotatorsClassMapping, groupAnnotations) {
  var automaticallyAnnotated = !(annotatorClass === undefined || annotatorClass === "undefined" || annotatorClass === null || annotatorClass === "");
  var groupContainer = (inline ? document.createElement("span") : document.createElement("div"));
  var descContainer = (inline ? document.createElement("span") : document.createElement("div"));

  if (groupAnnotations) {
    var annotatorName = automaticallyAnnotated ? annotatorsClassMapping[annotatorClass].getName() : "Annotated by curator";
    if (inline) {
      descContainer.innerHTML = 'Source: ' + annotatorName + ': ';
    } else {
      descContainer.innerHTML = 'Source: ' + annotatorName;

      if (automaticallyAnnotated) {
        var annotatorDescription = annotatorsClassMapping[annotatorClass].getDescription();
        if (annotatorDescription) {
          var tooltipContainer = Functions.createElement({
            type: "span"
          });
          tooltipContainer.appendChild(Functions.createElement({
            type: "span",
            className: "glyphicon glyphicon-question-sign tooltip-icon"
          }));
          tooltipContainer.appendChild(Functions.createElement({
            type: "span",
            className: "annotator-tooltip",
            content: annotatorsClassMapping[annotatorClass].getDescription()
          }));

          descContainer.appendChild(tooltipContainer);
        }
      }
    }
    descContainer.className = "minerva-annotation-group-header";
    if (!inline) groupContainer.className = "minerva-annotation-group";
    groupContainer.appendChild(descContainer);
  }
  return groupContainer;
}

/**
 *
 * @param {Annotation[]} annotations
 * @param {Object} [options]
 * @param {boolean} [options.showType = true]
 * @param {boolean} [options.inline = false]
 * @param {boolean} [options.groupAnnotations = true]
 * @returns {HTMLDivElement}
 */
GuiUtils.prototype.createAnnotationList = function (annotations, options) {
  var showType = true;
  var inline = false;
  var groupAnnotations = true;
  if (options !== undefined) {
    if (options.showType !== undefined) {
      showType = options.showType;
    }
    if (options.inline !== undefined) {
      inline = options.inline;
    }
    if (options.groupAnnotations !== undefined) {
      groupAnnotations = options.groupAnnotations;
    }
  }

  var self = this;
  var result = document.createElement("div");

  var annotators = this.getConfiguration().getAnnotators();
  var annotatorsClassMapping = {};
  var grouppedAnnotations = {};
  var i;
  if (groupAnnotations) {
    for (i = 0; i < annotators.length; i++) {
      annotatorsClassMapping[annotators[i].getClassName()] = annotators[i];
    }

    for (i = 0; i < annotations.length; i++) {
      var clazz = annotations[i].getAnnotatorClassName();
      if (!(clazz in grouppedAnnotations)) grouppedAnnotations[clazz] = [];
      grouppedAnnotations[clazz].push(annotations[i])
    }
  } else {
    annotatorsClassMapping["undefined"] = {
      getName: function () {
        return ""
      }
    };
    grouppedAnnotations["undefined"] = annotations;
  }

  // = annotatorClasName ? annotatorsClassMapping[annotatorClasName].getName() : "Annotated by curator";

  var cntAnnotations = 0;

  Object.keys(grouppedAnnotations).sort().forEach(function (annotatorClass) {
    var groupContainer = createGroupContainer(inline, annotatorClass, annotatorsClassMapping, groupAnnotations);

    grouppedAnnotations[annotatorClass] = grouppedAnnotations[annotatorClass].sort(function (a, b) {
      var aType = a.getType().toUpperCase();
      var bType = b.getType().toUpperCase();
      if (aType < bType) return -1;
      else if (aType > bType) return 1;
      else return 0;
    });

    for (var j = 0; j < grouppedAnnotations[annotatorClass].length; j++) {

      cntAnnotations += 1;

      var element = grouppedAnnotations[annotatorClass][j];
      var link = self.createAnnotationLink(element, showType);
      if (inline) {
        if (j > 0) {
          var coma = document.createElement("span");
          coma.innerHTML = ", ";
          groupContainer.appendChild(coma);
        }
        groupContainer.appendChild(link);
      } else {

        var row = document.createElement("div");
        if (j % 2 === 0) {
          row.className = "minerva-annotation-row-odd";
        } else {
          row.className = "minerva-annotation-row-even";
        }

        row.appendChild(Functions.createElement({
          type: "div",
          className: "minerva-annotation-counter",
          content: "[" + cntAnnotations + "]"
        }));

        var body = Functions.createElement({
          type: "div",
          className: "minerva-annotation-body"
        });
        body.appendChild(link);
        row.appendChild(body);
        groupContainer.appendChild(row);
      }
    }

    result.appendChild(groupContainer);
  });

  return result;
};

/**
 *
 * @param {Author[]} authors
 * @returns {HTMLElement}
 */
GuiUtils.prototype.createAuthorsList = function (authors) {
  var self = this;
  var result = Functions.createElement({type: "div", className: "minerva-annotation-group"});

  for (var i = 0; i < authors.length; i++) {
    var author = authors[i];
    var row = document.createElement("div");
    if (i % 2 === 0) {
      row.className = "minerva-annotation-row-odd";
    } else {
      row.className = "minerva-annotation-row-even";
    }

    row.appendChild(Functions.createElement({
      type: "div",
      className: "minerva-annotation-counter",
      content: "[" + (i + 1) + "]"
    }));

    var body = Functions.createElement({
      type: "div",
      className: "minerva-annotation-body"
    });

    var desc = "";
    if (author.firstName !== null && author.firstName !== undefined) {
      desc = author.firstName + " ";
    }
    if (author.lastName !== null && author.lastName !== undefined) {
      desc += author.lastName;
    }
    if (desc === "") {
      desc = author.email;
    }

    var link;
    if (author.email !== undefined && author.email !== null) {
      link = Functions.createElement({type: "a", href: "mailto:" + author.email, content: desc});
    } else {
      link = Functions.createElement({type: "span", content: desc});
    }
    body.appendChild(link);

    if (author.organisation !== undefined && author.organisation !== null) {
      body.appendChild(Functions.createElement({type: "span", content: ", " + author.organisation}));
    }

    row.appendChild(body);
    result.appendChild(row);
  }

  return result;
};

/**
 *
 * @param {string} [value]
 * @param {string} [className]
 * @returns {HTMLElement}
 */
GuiUtils.prototype.createLabelText = function (value, className) {
  var result = Functions.createElement({type: "span", className: className});
  if (value !== undefined) {
    result.innerHTML = value;
  }
  return result;
};

/**
 *
 * @param {string} [value]
 * @returns {HTMLInputElement}
 */
GuiUtils.prototype.createInputText = function (value) {
  var result = document.createElement("input");
  result.setAttribute('type', 'text');

  if (value !== undefined) {
    result.setAttribute('value', value);
  }
  return result;
};

/**
 *
 * @param {string} [value]
 * @returns {HTMLTextAreaElement}
 */
GuiUtils.prototype.createTextArea = function (value) {
  var result = document.createElement("textarea");

  if (value !== undefined) {
    result.setAttribute('value', value);
    result.innerHTML = value;
  }
  return result;
};

/**
 *
 * @param {Object} params
 * @param {string} params.label
 * @param {string|number} [params.value]
 * @param {string} [params.className]
 * @returns {HTMLElement}
 */
GuiUtils.prototype.createParamLine = function (params) {
  var result = Functions.createElement({type: "div", className: params.className});
  if (params.value !== undefined && params.value !== null && params.value !== "") {
    var self = this;
    result.appendChild(self.createLabel(params.label));
    result.appendChild(self.createLabelText(params.value));
  }
  return result;
};

/**
 *
 * @param {Object} [params]
 * @param {string} [params.icon]
 * @param {string} [params.className]
 * @param {function} [params.onclickFunction]
 * @returns {HTMLElement}
 */
GuiUtils.prototype.createIcon = function (params) {
  var result = Functions.createElement({type: "div", className: params.className});

  if (params.icon !== undefined && params.icon !== null) {
    var img = document.createElement("img");
    img.src = GuiConnector.getImgPrefix() + params.icon;
    if (params.onclickFunction !== undefined) {
      img.onclick = params.onclickFunction;
      img.className = "minerva-search-result-icon minerva-clickable";
    } else {
      img.className = "minerva-search-result-icon";
    }
    result.appendChild(img);
  }
  return result;
};

/**
 *
 * @param {Object} params
 * @param {string} params.label
 * @param {string[]} [params.value]
 * @param {string} [params.className]
 *
 * @returns {HTMLElement}
 */
GuiUtils.prototype.createArrayParamLine = function (params) {
  if (params.label === undefined)
    throw new Error();
  if (params.value !== undefined && params.value.length > 0) {
    return this.createParamLine({label: params.label, value: params.value.join(", "), className: params.className})
  } else {
    return Functions.createElement({type: "div", className: params.className})
  }
};

/**
 *
 * @param {Object} params
 * @param {string} params.label
 * @param {number} [params.mapId]
 * @param {string} [params.className]
 * @returns {HTMLElement}
 */
GuiUtils.prototype.createSubMapLink = function (params) {
  var self = this;
  var result = Functions.createElement({type: "div", className: params.className});
  if (params.mapId !== undefined) {
    var button = document.createElement("button");
    var submap = self.getMap().getSubmapById(params.mapId);
    if (submap !== null) {
      var model = submap.getModel();

      button.innerHTML = model.getName();
      button.onclick = function () {
        return self.getMap().openSubmap(params.mapId);
      };
      result.appendChild(this.createLabel(params.label));
      result.appendChild(button);
    }
  }
  return result;
};

/**
 *
 * @param {Array<string|HTMLElement>} elements
 * @returns {HTMLElement}
 */
GuiUtils.prototype.createTableRow = function (elements) {
  var row = Functions.createElement({
    type: "div",
    style: "display: table-row;"
  });

  for (var i = 0; i < elements.length; i++) {
    var cell = Functions.createElement({
      type: "div",
      style: "display: table-cell;"
    });
    if (Functions.isDomElement(elements[i])) {
      cell.appendChild(elements[i]);
    } else {
      cell.innerHTML = elements[i];
    }
    row.appendChild(cell);
  }
  return row;
};

/**
 *
 * @param {Reaction|SearchBioEntityGroup} params.reaction
 * @param {string} [params.icon]
 * @param {boolean} [params.forceExpand]
 * @param {boolean} [params.showTitle=true]
 *
 * @returns {Promise<HTMLDivElement>}
 */
GuiUtils.prototype.createReactionElement = function (params) {
  var self = this;

  var reaction = params.reaction;
  var showTitle = ((params.showTitle === undefined) || params.showTitle);
  var div = document.createElement("div");

  var expandButton = self._createExpandButton(div);
  div.appendChild(expandButton);
  if (showTitle) {
    var titleDiv = Functions.createElement({
      type: "div",
      className: self._configurationOptionToClassName(ConfigurationType.SHOW_REACTION_TITLE)
    });
    titleDiv.appendChild(self.createLabel("Reaction: " + reaction.getReactionId()));
    div.appendChild(titleDiv);
  }

  div.appendChild(self.createSubMapLink({
    label: "Associated submap: ",
    mapId: reaction.getLinkedSubmodelId(),
    className: self._configurationOptionToClassName(ConfigurationType.SHOW_REACTION_LINKED_SUBMAP)
  }));
  if (showTitle) {
    div.appendChild(self.createNewLine());
  }

  div.appendChild(self.createParamLine({
    label: "Name: ",
    value: reaction.getName(), className: self._configurationOptionToClassName(ConfigurationType.SHOW_REACTION_NAME)
  }));
  div.appendChild(self.createParamLine({
    label: "Type: ",
    value: reaction.getType(), className: self._configurationOptionToClassName(ConfigurationType.SHOW_REACTION_TYPE)
  }));
  div.appendChild(self.createParamLine({
    label: "Symbol: ",
    value: reaction.getSymbol(), className: self._configurationOptionToClassName(ConfigurationType.SHOW_REACTION_SYMBOL)
  }));
  div.appendChild(self.createParamLine({
    label: "Abbreviation: ",
    value: reaction.getAbbreviation(),
    className: self._configurationOptionToClassName(ConfigurationType.SHOW_REACTION_ABBREVIATION)
  }));
  div.appendChild(self.createParamLine({
    label: "Formula: ",
    value: reaction.getFormula(),
    className: self._configurationOptionToClassName(ConfigurationType.SHOW_REACTION_FORMULA)
  }));
  div.appendChild(self.createParamLine({
    label: "Mechanical Confidence Score: ",
    value: reaction.getMechanicalConfidenceScore(),
    className: self._configurationOptionToClassName(ConfigurationType.SHOW_REACTION_MECHANICAL_CONFIDENCE_SCORE)
  }));
  div.appendChild(self.createParamLine({
    label: "Lower Bound: ",
    value: reaction.getLowerBound(),
    className: self._configurationOptionToClassName(ConfigurationType.SHOW_REACTION_LOWER_BOUND)
  }));
  div.appendChild(self.createParamLine({
    label: "Upper Bound: ",
    value: reaction.getUpperBound(),
    className: self._configurationOptionToClassName(ConfigurationType.SHOW_REACTION_UPPER_BOUND)
  }));
  div.appendChild(self.createParamLine({
    label: "Gene Protein Reaction: ",
    value: reaction.getGeneProteinReaction(),
    className: self._configurationOptionToClassName(ConfigurationType.SHOW_REACTION_GENE_PROTEIN_REACTION)
  }));
  div.appendChild(self.createParamLine({
    label: "Subsystem: ",
    value: reaction.getSubsystem(),
    className: self._configurationOptionToClassName(ConfigurationType.SHOW_REACTION_SUBSYSTEM)
  }));
  div.appendChild(self.createArrayParamLine({
    label: "Synonyms: ",
    value: reaction.getSynonyms(),
    className: self._configurationOptionToClassName(ConfigurationType.SHOW_REACTION_SYNONYMS)
  }));
  div.appendChild(self.createParamLine({
    label: "Description: ",
    value: reaction.getDescription(),
    className: self._configurationOptionToClassName(ConfigurationType.SHOW_REACTION_DESCRIPTION)
  }));
  div.appendChild(self.createAnnotations({
    label: "Annotations: ",
    annotations: reaction.getReferences(),
    className: self._configurationOptionToClassName(ConfigurationType.SHOW_REACTION_ANNOTATIONS)
  }));
  if (params.forceExpand) {
    expandButton.onclick();
    $(expandButton).hide();
  }
  return Promise.resolve(div);
};

/**
 *
 * @param {Object[]|Object} modifications
 * @param {string} [className]
 *
 * @returns {HTMLElement}
 */
GuiUtils.prototype.createModifications = function (modifications, className) {
  var self = this;
  var result = Functions.createElement({type: "div", className: className});
  var modificationsByType = [];
  if (modifications !== undefined) {
    if (modifications.length === undefined) {
      modifications = [modifications];
    }

    for (var i = 0; i < modifications.length; i++) {
      var modification = modifications[i];
      if (modificationsByType[modification.type] === undefined) {
        modificationsByType[modification.type] = [];
      }
      modificationsByType[modification.type].push(modification);
    }
    for (var key in modificationsByType) {
      if (modificationsByType.hasOwnProperty(key)) {
        result.appendChild(self.createPostTranslationalModifications(key, modificationsByType[key]));
      }
    }
  }
  return result;
};

/**
 *
 * @param {HTMLElement} parentDiv
 * @returns {HTMLElement}
 * @private
 */
GuiUtils.prototype._createExpandButton = function (parentDiv) {
  var expandButton = Functions.createElement({
    type: "button",
    style: "position:absolute;right:8px;width:18px",
    content: "<span class='ui-icon ui-icon-plusthick' style='margin-left:-0.5em'></span>",
    xss: false,
    onclick: function () {
      $(expandButton).children().toggleClass("ui-icon-plusthick ui-icon-minusthick");
      $(".minerva-search-data-hidden", parentDiv).toggle();
    }
  });
  return expandButton
};

/**
 *
 * @param {Alias|SearchBioEntityGroup} params.alias
 * @param {string} [params.icon]
 * @param {boolean} [params.forceExpand]
 * @param {boolean} [params.showTitle]
 *
 * @returns {Promise<HTMLDivElement>}
 */
GuiUtils.prototype.createAliasElement = function (params) {
  var alias = params.alias;
  var icon = params.icon;
  var self = this;
  var showTitle = ((params.showTitle === undefined) || params.showTitle);
  var div = document.createElement("div");

  var expandButton = self._createExpandButton(div);
  div.appendChild(expandButton);

  if (showTitle) {
    if (icon !== undefined) {
      div.appendChild(this.createIcon({
        icon: icon,
        className: self._configurationOptionToClassName(ConfigurationType.SHOW_ELEMENT_TITLE),
        onclickFunction: function () {
          return self.getMap().openSubmap(alias.getModelId()).then(function () {
            if (alias instanceof Alias) {
              return self.getMap().getSubmapById(alias.getModelId()).fitBounds([alias]);
            } else {
              return self.getMap().getSubmapById(alias.getModelId()).fitBounds(alias.getBioEntities());
            }
          });
        }
      }));
    }

    div.appendChild(this.createParamLine({
      label: alias.getType() + ": ",
      className: self._configurationOptionToClassName(ConfigurationType.SHOW_ELEMENT_TITLE),
      value: alias.getName()
    }));
  }
  div.appendChild(self.createSubMapLink({
    label: "Associated submap: ",
    mapId: alias.getLinkedSubmodelId(),
    className: self._configurationOptionToClassName(ConfigurationType.SHOW_ELEMENT_LINKED_SUBMAP)
  }));
  var groupSizeDiv = Functions.createElement({
    type: "div",
    className: self._configurationOptionToClassName(ConfigurationType.SHOW_ELEMENT_GROUP_SIZE)
  });
  if (alias instanceof SearchBioEntityGroup && alias.getBioEntities().length > 1) {
    groupSizeDiv.appendChild(self.createLabelText("Group of " + alias.getBioEntities().length + " elements."));
  }
  div.appendChild(groupSizeDiv);
  if (showTitle) {
    div.appendChild(self.createNewLine(3));
  }

  var promise = Promise.resolve();
  if (alias.getCompartmentId() !== undefined) {
    promise = self.getMap().getModel().getByIdentifiedElement(new IdentifiedElement({
      type: "ALIAS",
      id: alias.getCompartmentId(),
      modelId: alias.getModelId()
    }), true).then(function (compartment) {
      div.appendChild(self.createParamLine({
        label: compartment.getType() + ": ", value: compartment.getName(),
        className: self._configurationOptionToClassName(ConfigurationType.SHOW_ELEMENT_COMPARTMENT)
      }));
    })
  }
  return promise.then(function () {

    div.appendChild(self.createParamLine({
      label: "Full name: ", value: alias.getFullName(),
      className: self._configurationOptionToClassName(ConfigurationType.SHOW_ELEMENT_FULL_NAME)
    }));
    div.appendChild(self.createParamLine({
      label: "Element id(s): ", value: alias.getElementId(),
      className: self._configurationOptionToClassName(ConfigurationType.SHOW_ELEMENT_ID)
    }));
    div.appendChild(self.createParamLine({
      label: "Symbol: ", value: alias.getSymbol(),
      className: self._configurationOptionToClassName(ConfigurationType.SHOW_ELEMENT_SYMBOL)
    }));
    div.appendChild(self.createParamLine({
      label: "Abbreviation: ", value: alias.getAbbreviation(),
      className: self._configurationOptionToClassName(ConfigurationType.SHOW_ELEMENT_ABBREVIATION)
    }));
    div.appendChild(self.createParamLine({
      label: "Formula: ", value: alias.getFormula(),
      className: self._configurationOptionToClassName(ConfigurationType.SHOW_ELEMENT_FORMULA)
    }));
    div.appendChild(self.createArrayParamLine({
      label: "Former symbols: ", value: alias.getFormerSymbols(),
      className: self._configurationOptionToClassName(ConfigurationType.SHOW_ELEMENT_FORMER_SYMBOLS)
    }));
    if ((self.getConfiguration().getBooleanValue(ConfigurationType.SHOW_ELEMENT_MODIFICATIONS))) {
      div.appendChild(self.createModifications(alias.getOther('modifications')));
    }
    div.appendChild(self.createParamLine({
      label: "Charge: ", value: alias.getCharge(),
      className: self._configurationOptionToClassName(ConfigurationType.SHOW_ELEMENT_CHARGE)
    }));
    div.appendChild(self.createArrayParamLine({
      label: "Synonyms: ", value: alias.getSynonyms(),
      className: self._configurationOptionToClassName(ConfigurationType.SHOW_ELEMENT_SYNONYMS)
    }));
    div.appendChild(self.createLabelText(alias.getDescription(), self._configurationOptionToClassName(ConfigurationType.SHOW_ELEMENT_DESCRIPTION)));
    div.appendChild(self.createAnnotations({
      label: "Annotations: ", annotations: alias.getReferences(),
      className: self._configurationOptionToClassName(ConfigurationType.SHOW_ELEMENT_ANNOTATIONS)
    }));
    if (params.forceExpand) {
      $(expandButton).hide();
      return expandButton.onclick();
    }
  }).then(function () {
    return div;
  })
};

/**
 *
 * Returns "minerva-search-data-visible" for {ConfigurationType} options that are set to true. In other cases returns
 * "minerva-search-data-hidden".
 *
 * @param {string} string
 * @returns {string}
 * @private
 */
GuiUtils.prototype._configurationOptionToClassName = function (string) {
  if (this.getConfiguration().getBooleanValue(string)) {
    return "minerva-search-data-visible";
  } else {
    return "minerva-search-data-hidden";
  }
};
/**
 *
 * @param {SearchBioEntityGroup} group
 * @returns {Promise<HTMLDivElement>}
 */
GuiUtils.prototype.createSearchBioEntityGroupElement = function (group) {
  if (group.getBioEntities()[0] instanceof Alias) {
    return this.createAliasElement({alias: group, icon: group.getIcon()});
  } else if (group.getBioEntities()[0] instanceof Reaction) {
    return this.createReactionElement({reaction: group, icon: group.getIcon()});
  } else {
    throw Promise.reject("Unknown element type in the group: " + group.getBioEntities()[0]);
  }
};

/**
 *
 * @returns {HTMLAnchorElement}
 */
GuiUtils.prototype.createLogoutLink = function () {
  var logoutLink = document.createElement("a");
  logoutLink.href = "#";
  logoutLink.innerHTML = "(" + ServerConnector.getSessionData().getLogin() + ") LOGOUT";
  logoutLink.id = "logoutLink";
  logoutLink.onclick = function () {
    return ServerConnector.logout();
  };
  return logoutLink;
};

/**
 *
 * @param {string} params.name
 * @param {string} params.id
 * @param {string} [params.className]
 * @param {function} [params.onclick]
 * @param {HTMLElement} params.navigationBar
 *
 * @returns {HTMLLIElement}
 */
GuiUtils.prototype.createTabMenuObject = function (params) {
  var name = params.name;
  var id = params.id;
  var navigationBar = params.navigationBar;

  var linkClass = params.className + ' nav-link';
  var liClass = 'nav-item';
  if (navigationBar.children.length === 0) {
    linkClass += " active";
    liClass += " active";
  }
  var navLi = document.createElement("li");
  navLi.className = liClass;

  var navLink = Functions.createElement({
    type: "a",
    className: linkClass,
    href: "#" + id,
    content: name,
    onclick: function () {
      // $(navLink).tab('show');
      if (params.onclick) {
        return params.onclick();
      }
    },
    xss: false
  });
  // $(navLink).attr("role", "tab");
  $(navLink).attr("data-toggle", "tab");
  navLi.appendChild(navLink);
  return navLi;
};

/**
 *
 * @param {string} params.id
 * @param {HTMLElement} params.navigationObject
 *
 * @returns {HTMLDivElement}
 */
GuiUtils.prototype.createTabContentObject = function (params) {
  var navigationObject = params.navigationObject;
  var tabId = params.id;
  var result = document.createElement("div");
  result.style.height = "100%";

  var contentClass = 'tab-pane';
  if ($("a.active", navigationObject).length > 0) {
    contentClass = "tab-pane active";
  }

  result.className = contentClass;
  result.id = tabId;
  return result;
};

/**
 *
 * @param {string} toolTip
 * @param {boolean} [useXss=false]
 * @returns {HTMLElement}
 */
GuiUtils.prototype.createHelpButton = function (toolTip, useXss) {
  var helpContent;
  if (useXss) {
    helpContent = xss(toolTip);
  } else {
    helpContent = toolTip;
  }

  var helpTipButton = Functions.createElement({
    type: "button",
    className: "minerva-help-button",
    content: '<span class="ui-icon ui-icon-help" style="margin-left: -0.5em;"/>',
    xss: false
  });
  var helpDialogDiv = undefined;
  helpTipButton.onclick = function () {
    if (helpDialogDiv === undefined) {
      helpDialogDiv = Functions.createElement({
        type: "div",
        content: helpContent,
        xss: false
      });
      $(helpDialogDiv).dialog({
        dialogClass: 'minerva-help-dialog',
        close: function () {
          $(this).dialog('destroy').remove();
          helpDialogDiv = undefined;
        },
        position: {
          my: "left top",
          at: "left bottom",
          of: helpTipButton
        }
      });
      $('.ui-dialog').find("a").blur();
    } else {
      $(helpDialogDiv).dialog('close');
    }
  };
  return helpTipButton;
};

/**
 * @param {AbstractGuiElement} abstractGuiElement
 */
GuiUtils.prototype.initTabContent = function (abstractGuiElement) {
  if (abstractGuiElement._panels !== undefined) {
    throw new Error("Tabs were already initialized");
  }

  abstractGuiElement._panels = [];

  var tabDiv = Functions.createElement({
    type: "div",
    name: "tabView",
    className: "tabbable boxed parentTabs"
  });
  abstractGuiElement.getElement().appendChild(tabDiv);

  tabDiv.appendChild(Functions.createElement({
    type: "ul",
    className: "nav nav-tabs"
  }));

  tabDiv.appendChild(Functions.createElement({
    type: "div",
    className: "tab-content"
  }));
};

/**
 *
 * @param {AbstractGuiElement} abstractGuiElement
 * @param {Object} params
 * @param {string} params.name
 * @param {string} [params.linkClassName]
 * @param {HTMLElement} [params.content]
 * @param {boolean} [params.disabled = false]
 * @param {function} [params.panelClass]
 * @param {Object} [params.options] - optional parameters passed to the panel constructor
 */
GuiUtils.prototype.addTab = function (abstractGuiElement, params) {
  var tabId = "panel_tab_" + tabIdCounter;
  tabIdCounter++;

  var navElement = $(abstractGuiElement.getElement()).find("> .parentTabs > .nav-tabs")[0];
  var contentElement = $(abstractGuiElement.getElement()).find("> .parentTabs > .tab-content")[0];

  var navLi = this.createTabMenuObject({
    id: tabId,
    name: params.name,
    className: params.linkClassName,
    navigationBar: navElement
  });
  navElement.appendChild(navLi);

  var contentDiv = this.createTabContentObject({
    id: tabId,
    navigationObject: navLi
  });

  contentElement.appendChild(contentDiv);

  if (params.content !== undefined) {
    contentDiv.appendChild(params.content);
    if (params.disabled) {
      this.hideTab(abstractGuiElement, params.content);
    }
  } else {

    var options = {
      element: contentDiv,
      name: params.name,
      configuration: this.getConfiguration(),
      serverConnector: abstractGuiElement.getServerConnector(),
      customMap: abstractGuiElement.getMap(),
      project: abstractGuiElement.getProject()
    };

    if (params.options !== undefined) {
      options = $.extend(options, params.options);
    }

    var panel = new params.panelClass(options);
    abstractGuiElement._panels.push(panel);

    if (params.disabled) {
      this.hideTab(abstractGuiElement, panel);
    }
  }
};

/**
 *
 * @param {Panel|HTMLElement} panel
 * @returns {string}
 */
function getPanelId(panel) {
  var panelId;
  if (Functions.isDomElement(panel)) {
    if (panel.className.indexOf("tab-pane") >= 0) {
      panelId = panel.id;
    } else {
      panelId = panel.parentNode.id;
    }
  } else {
    panelId = panel.getElement().id;
  }
  return panelId;
}

/**
 *
 * @param {AbstractGuiElement} abstractGuiElement
 * @param {Panel|HTMLElement} panel
 */
GuiUtils.prototype.hideTab = function (abstractGuiElement, panel) {
  var panelId = getPanelId(panel);
  var liElement = $("li:has(a[href='#" + panelId + "'])", $(abstractGuiElement.getElement()))[0];
  if (liElement !== undefined) {
    $(liElement).hide();
  } else {
    logger.warn("Cannot find tab link for panel: " + panel);
  }
};

/**
 *
 * @param {HTMLElement} element
 * @param {string} message
 */
GuiUtils.prototype.disableTab = function (element, message) {
  var hideReason = $(".minerva-hide-reason", element);
  if (hideReason.length === 0) {
    $(element).children().css("visibility", "hidden");
    $("[class='minerva-help-button']", element).children().css("visibility", "visible");
    var hideReasonDiv = document.createElement("div");
    hideReasonDiv.className = "searchPanel minerva-hide-reason";

    var center = document.createElement("center");
    var messageDiv = document.createElement("h4");
    messageDiv.innerHTML = message;
    center.appendChild(messageDiv);
    hideReasonDiv.appendChild(center);

    $(element).prepend(hideReasonDiv);
  }
};

/**
 *
 * @param {HTMLElement} element
 */
GuiUtils.prototype.enableTab = function (element) {
  var hideReason = $(".minerva-hide-reason", element);
  if (hideReason.length > 0) {
    hideReason.remove();
    $(element).children().css("visibility", "visible");
    $("[class='minerva-help-button']", element).children().css("visibility", "visible");
  }
};

/**
 *
 * @param {AbstractGuiElement} abstractGuiElement
 * @param {Panel|HTMLElement} panel
 */
GuiUtils.prototype.removeTab = function (abstractGuiElement, panel) {
  var panelId = getPanelId(panel);
  var liElement = $("li:has(a[href='#" + panelId + "'])", $(abstractGuiElement.getElement()))[0];
  var contentElement = $("#" + panelId, $(abstractGuiElement.getElement()))[0];
  if (liElement !== undefined && contentElement !== undefined) {
    liElement.parentElement.removeChild(liElement);
    contentElement.parentElement.removeChild(contentElement);
  } else {
    logger.warn("Cannot find tab for panel: " + panel);
  }
  var isActive = $(".nav-tabs > li.active", $(abstractGuiElement.getElement())).length > 0;
  if (!isActive) {
    var links = $(".nav-tabs > li", $(abstractGuiElement.getElement()));
    if (links.length > 0) {
      $("a", $(links[0])).click();
    }
  }
};

/**
 *
 * @param {AbstractGuiElement} abstractGuiElement
 * @param {Panel} panel
 */
GuiUtils.prototype.showTab = function (abstractGuiElement, panel) {
  var panelId = getPanelId(panel);
  var liElement = $("li:has(a[href='#" + panelId + "'])", $(abstractGuiElement.getElement()))[0];
  if (liElement !== undefined) {
    $(liElement).show();
  } else {
    logger.warn("Cannot find tab link for panel: " + panel);
  }
};

module.exports = GuiUtils;