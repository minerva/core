"use strict";

var Promise = require("bluebird");
var $ = require('jquery');

var GuiConnector = require('../../GuiConnector');
var Panel = require('../Panel');
var PanelControlElementType = require('../PanelControlElementType');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
var Functions = require('../../Functions');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} params.customMap
 * @param {Configuration} [params.configuration]
 * @param {Project} [params.project]
 * @param params.parent
 *
 * @constructor
 * @extends Panel
 */
function SubmapPanel(params) {
  params["panelName"] = "submap";
  params["scrollable"] = true;
  params["helpTip"] = "The Submaps tab summarizes all the submap networks uploaded together and linked to the main network of <b>'"
    + params.parent.getMap().getProject().getName() + "'</b> project.";
  Panel.call(this, params);
  $(params.element).addClass("minerva-submap-panel");

  var self = this;
  self._createSubmapGui();
}

SubmapPanel.prototype = Object.create(Panel.prototype);
SubmapPanel.prototype.constructor = SubmapPanel;

/**
 *
 * @private
 */
SubmapPanel.prototype._createSubmapGui = function () {
  var submapDiv = Functions.createElement({
    type: "div",
    name: "submapDiv",
    className: "searchPanel"
  });
  this.getElement().appendChild(submapDiv);
  this.setControlElement(PanelControlElementType.SUBMAP_DIV, submapDiv);

};

/**
 *
 * @param {MapModel} model
 * @returns {HTMLTableRowElement}
 */
SubmapPanel.prototype.createRow = function (model) {
  var self = this;
  var guiUtils = self.getGuiUtils();
  var result = document.createElement("tr");

  var nameTd = Functions.createElement({type: "td", content: model.getName(), style: "position:relative"});

  if (model.getReferences().length > 0 || model.getAuthors().length > 0 ||
    (model.getDescription() !== null && model.getDescription() !== "")) {
    var referencesTab = Functions.createElement({
      type: "div",
      className: "minerva-search-data-hidden"
    });
    nameTd.appendChild(referencesTab);

    if (model.getReferences().length > 0) {
      var references = Functions.createElement({
        type: "div",
        content: 'References:'
      });
      referencesTab.appendChild(references);
      referencesTab.appendChild(guiUtils.createAnnotationList(model.getReferences(), {groupAnnotations: false}));
    }

    if (model.getAuthors().length > 0) {
      var authors = Functions.createElement({
        type: "div",
        content: 'Authors:'
      });
      referencesTab.appendChild(authors);
      referencesTab.appendChild(guiUtils.createAuthorsList(model.getAuthors()));
    }
    if (model.getDescription() !== null && model.getDescription() !== "") {
      referencesTab.appendChild(Functions.createElement({
        type: "div",
        content: 'Description:'
      }));
      referencesTab.appendChild(Functions.createElement({
        type: "p",
        content: model.getDescription()
      }));
    }

    var expandButton = Functions.createElement({
      type: "button",
      style: "position:absolute;right:8px;top:8px;width:18px",
      content: "<span class='ui-icon ui-icon-plusthick' style='margin-left:-0.5em'></span>",
      xss: false,
      onclick: function () {
        $(expandButton).children().toggleClass("ui-icon-plusthick ui-icon-minusthick");
        $(referencesTab).toggle();
      }
    });
    nameTd.appendChild(expandButton);
  }

  result.appendChild(nameTd);

  var searchTargetTd = document.createElement("td");

  if (model.getId() !== self.getMap().getId()) {
    searchTargetTd.appendChild(Functions.createElement({
      type: "a",
      href: "#",
      className: "minerva-search-link",
      content: "<i class='minerva-search-target-button'></i>",
      onclick: function () {
        var customMap = self.getMap();
        var searchDb = customMap.getOverlayByName('search');
        if (searchDb !== undefined) {
          return searchDb.searchByTargetMapId(model.getId()).catch(GuiConnector.alert);
        } else {
          logger.warn("Search is impossible because search db is not present");
        }
      },
      xss: false
    }));
  }

  result.appendChild(searchTargetTd);

  var openTd = document.createElement("td");

  if (model.getId() !== self.getMap().getId()) {
    var link = Functions.createElement({
      type: "a",
      href: "#",
      className: "minerva-search-link",
      content: "<i class='minerva-search-button'></i>",
      onclick: function () {
        return self.getMap().openSubmap(model.getId()).catch(GuiConnector.alert);
      },
      xss: false
    });
    openTd.appendChild(link);
  }

  result.appendChild(openTd);

  return result;
}
;

/**
 *
 * @returns {HTMLTableSectionElement}
 */
SubmapPanel.prototype.createTableHeader = function () {
  var result = document.createElement("thead");

  var row = document.createElement("tr");

  var nameTd = document.createElement("th");
  nameTd.innerHTML = "Name";
  row.appendChild(nameTd);

  row.appendChild(Functions.createElement({
    type: "th",
    className: "minerva-submap-panel-search-column",
    content: "Find anchor"
  }));

  row.appendChild(Functions.createElement({
    type: "th",
    className: "minerva-submap-panel-search-column",
    content: "View"
  }));

  result.appendChild(row);
  return result;
};

/**
 * @returns {Promise}
 */
SubmapPanel.prototype.init = function () {
  var self = this;
  return new Promise(function (resolve) {
    var div = self.getControlElement(PanelControlElementType.SUBMAP_DIV);
    div.innerHTML = "";
    var models = self.getMap().getProject().getModels();
    var modelsByType = [];
    var types = [];
    var i;
    for (i = 1; i < models.length; i++) {
      var model = models[i];
      if (modelsByType[model.getSubmodelType()] === undefined) {
        modelsByType[model.getSubmodelType()] = [];
        types.push(model.getSubmodelType());
      }
      modelsByType[model.getSubmodelType()].push(model);
    }
    for (i = 0; i < types.length; i++) {
      var type = types[i];
      var tableName = type + " submaps";
      if (type === "UNKNOWN" || type === undefined || type === null) {
        tableName = ""
      }
      modelsByType[type].sort(function (modelA, modelB) {
        if (modelA.getName() > modelB.getName()) {
          return 1;
        }
        if (modelA.getName() < modelB.getName()) {
          return -1;
        }
        return 0;
      });
      div.appendChild(self.createTable(modelsByType[type], tableName));
    }
    if (models.length <= 1) {
      self.getGuiUtils().hideTab(self.getParent(), self);
    }
    return resolve();
  });
};

/**
 *
 * @param {MapModel[]} models
 * @param {string} type
 * @returns {HTMLElement}
 */
SubmapPanel.prototype.createTable = function (models, type) {
  var self = this;
  var result = Functions.createElement({
    type: "div"
  });

  var title = Functions.createElement({
    type: "h5",
    content: type
  });
  result.appendChild(title);

  var table = Functions.createElement({
    type: "table",
    className: "table table-bordered",
    style: "width:100%"
  });
  result.appendChild(table);

  table.appendChild(self.createTableHeader());
  var tableBody = Functions.createElement({
    type: "tbody"
  });
  table.appendChild(tableBody);

  for (var i = 0; i < models.length; i++) {
    tableBody.appendChild(self.createRow(models[i]));
  }
  return result;
};

/**
 *
 * @returns {Promise}
 */
SubmapPanel.prototype.destroy = function () {
  return Promise.resolve();
};

module.exports = SubmapPanel;
