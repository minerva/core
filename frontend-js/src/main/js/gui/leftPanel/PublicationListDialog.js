"use strict";

var Promise = require("bluebird");
var $ = require('jquery');
require('datatables.net')();
require('datatables.net-rowreorder')();

/* exported logger */

var AbstractGuiElement = require('../AbstractGuiElement');
var Alias = require('../../map/data/Alias');
var GuiConnector = require('../../GuiConnector');
var IdentifiedElement = require('../../map/data/IdentifiedElement');
var Reaction = require('../../map/data/Reaction');

var Functions = require('../../Functions');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var stringify = require('csv-stringify');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} [params.customMap]
 * @param {Configuration} params.configuration
 * @param {Project} [params.project]
 * @param {ServerConnector} [params.serverConnector]
 * @constructor
 * @extends AbstractGuiElement
 */
function PublicationListDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self.createPublicationListDialogGui();

  $(self.getElement()).on("change", ".minerva-submap-filter", function () {
    $(self.tableElement).DataTable().ajax.reload();
  });

  self.getElement().insertBefore(Functions.createElement({
    type: "button",
    content: 'CSV <span class="ui-icon ui-icon-arrowthickstop-1-s"></span>',
    xss: false,
    onclick: function () {
      var data = $(self.tableElement).DataTable().data();
      GuiConnector.showProcessing();
      return self.getServerConnector().getPublications({
        start: 0,
        length: data.page.info().recordsDisplay,
        sortColumn: self.getColumnsDefinition()[data.order()[0][0]].name,
        sortOrder: data.order()[0][1],
        search: data.search(),
        modelId: self.getSubmapFilterValue()
      }).then(function (publicationList) {
        return self.publicationListAsCsvString(publicationList);
      }).then(function (result) {
        var blob = new Blob([result], {
          type: "text/plain;charset=utf-8"
        });
        var FileSaver = require("file-saver");
        return FileSaver.saveAs(blob, "publications.csv");
      }).catch(GuiConnector.alert).finally(function () {
        GuiConnector.hideProcessing();
      });
    }

  }), self.tableElement);
}

PublicationListDialog.prototype = Object.create(AbstractGuiElement.prototype);
PublicationListDialog.prototype.constructor = PublicationListDialog;

/**
 *
 */
PublicationListDialog.prototype.createPublicationListDialogGui = function () {
  var self = this;
  var submapFilterString = self.createSubmapFilter();
  var head = Functions.createElement({
    type: "thead",
    content: "<tr>" + "<th>Pubmed ID</th>" +
      "<th>Title</th>" +
      "<th>Authors</th>" +
      "<th>Journal</th>" +
      "<th>Year</th>" +
      "<th>Elements on map</th>" +
      "<th>Submaps" + submapFilterString + "</th>" +
      "</tr>",
    xss: false
  });

  var body = Functions.createElement({
    type: "tbody"
  });
  var tableElement = Functions.createElement({
    type: "table",
    className: "minerva-publication-table",
    style: "width: 100%"
  });

  tableElement.appendChild(head);
  tableElement.appendChild(body);

  self.tableElement = tableElement;
  self.getElement().appendChild(tableElement);
};

/**
 *
 * @param data
 * @param {function} callback
 * @returns {Promise}
 * @private
 */
PublicationListDialog.prototype._dataTableAjaxCall = function (data, callback) {
  var self = this;
  var publicationList;
  return self.getServerConnector().getPublications({
    page: data.start / data.length,
    start: data.start,
    length: data.length,
    sortColumn: self.getColumnsDefinition()[data.order[0].column].name,
    sortOrder: data.order[0].dir,
    search: data.search.value,
    modelId: self.getSubmapFilterValue()
  }).then(function (result) {
    publicationList = result;
    var out = [];
    var allElements = [];
    for (var i = 0; i < publicationList.data.length; i++) {
      var article = publicationList.data[i].publication.article;
      var elements = publicationList.data[i].elements;

      var row = [];
      var submaps = {};
      if (article !== null && article !== undefined) {
        row[0] = "<a href='" + article.link + "' target='#pubmed_" + article.pubmedId + "'>" + article.pubmedId + "</a>";
        row[1] = article.title;
        row[2] = article.authors.join();
        row[3] = article.journal;
        row[4] = article.year;
      } else {
        row[0] = publicationList.data[i].publication.resource;
        row[1] = "N/A";
        row[2] = "N/A";
        row[3] = "N/A";
        row[4] = "N/A";
      }
      row[5] = "<div>";
      row[6] = "<div>";
      for (var j = 0; j < elements.length; j++) {
        row[5] += "<a name='" + elements[j].id + "' href='#'>" + elements[j].type + ":" + elements[j].id + "</a>, ";
        allElements.push(new IdentifiedElement(elements[j]));
        var modelId = elements[j].modelId;
        if (submaps[modelId] === undefined) {
          row[6] += self.getMap().getSubmapById(modelId).getModel().getName() + ", ";
          submaps[elements[j].modelId] = true;
        }
      }
      row[5] += "</div>";
      row[6] += "</div>";
      out.push(row);
    }
    callback({
      draw: data.draw,
      recordsTotal: publicationList.totalSize,
      recordsFiltered: publicationList.filteredSize,
      data: out
    });
    var promises = [];
    allElements.forEach(function (element) {
      var model = self.getMap().getSubmapById(element.getModelId()).getModel();
      promises.push(model.getByIdentifiedElement(element, true).then(function (elementData) {
        var name = null;
        if (elementData instanceof Alias) {
          name = elementData.getName();
        } else if (elementData instanceof Reaction) {
          name = elementData.getReactionId();
        }
        $("a[name=" + elementData.getId() + "]", $(self.getElement())).html(name);
        var onclick = function () {
          var searchOverlay = self.getMap().getOverlayByName("search");
          var query;
          if (element.getType() === "ALIAS") {
            query = "element:" + element.getId();
          } else {
            query = "reaction:" + element.getId();
          }
          self.getMap().openSubmap(elementData.getModelId());
          return searchOverlay.searchByQuery(query, true, true).then(function () {
            $(self.getElement()).dialog("close");
          }).then(null, GuiConnector.alert);
        };
        $("a[name=" + elementData.getId() + "]", $(self.getElement())).click(onclick);
      }));
    });
    return Promise.all(promises);
  }).then(function () {
    return ServerConnector.getProjectStatistics().then(function (statistics) {
      var missing = statistics.getPublicationCount() - publicationList.totalSize;
      if (missing > 0) {
        $(".minerva-publication-count-supplementary", self.getElement()).html("&nbsp;&nbsp;&nbsp; (" + missing + " publications were not fetched from PUBMED)");
      }
    });
  });
};

/**
 *
 * @returns {Promise}
 */
PublicationListDialog.prototype.show = function () {
  var self = this;
  if (!$(self.getElement()).hasClass("ui-dialog-content")) {
    $(self.getElement()).dialog({
      dialogClass: 'minerva-publication-list-dialog',
      title: "Publication list",
      autoOpen: false,
      resizable: true,
      width: Math.max(window.innerWidth / 2, window.innerWidth - 100),
      height: Math.max(window.innerHeight / 2, window.innerHeight - 100)
    });
  }

  $(self.getElement()).dialog("open");

  if (!$.fn.DataTable.isDataTable(self.tableElement)) {
    return new Promise(function (resolve) {
      $(self.tableElement).dataTable({
        serverSide: true,
        ordering: true,
        searching: true,
        dom: 'lfrt<"info"i<"minerva-publication-count-supplementary">>p',
        ajax: function (data, callback, settings) {
          resolve(self._dataTableAjaxCall(data, callback, settings));
        },
        columns: self.getColumnsDefinition()
      });
    });
  } else {
    return Promise.resolve();
  }

};

/**
 *
 * @returns {*[]}
 */
PublicationListDialog.prototype.getColumnsDefinition = function () {
  return [{
    name: "pubmedId"
  }, {
    name: "title"
  }, {
    name: "authors"
  }, {
    name: "journal"
  }, {
    name: "year"
  }, {
    orderable: false,
    searchable: false,
    name: "elements"
  }, {
    orderable: false,
    searchable: false,
    name: "submaps"
  }];
};

/**
 *
 */
PublicationListDialog.prototype.destroy = function () {
  var self = this;
  var div = self.getElement();
  if ($(div).hasClass("ui-dialog-content")) {
    $(div).dialog("destroy");
  }
  if ($.fn.DataTable.isDataTable(self.tableElement)) {
    $(self.tableElement).DataTable().destroy();
  }
};

/**
 *
 * @returns {Promise}
 */
PublicationListDialog.prototype.publicationListToArray = function (publicationList) {
  var self = this;
  var result = [];

  var elementsToFetch = [];

  publicationList.data.map(function (entry) {
    var elements = entry.elements;
    for (var j = 0; j < elements.length; j++) {
      elementsToFetch.push(new IdentifiedElement(elements[j]));
    }
  });
  return self.getProject().getBioEntitiesByIdentifiedElements(elementsToFetch, false).then(function () {
    return Promise.all(publicationList.data.map(function (entry) {
      var article = entry.publication.article;
      var elements = entry.elements;
      var row = [];
      if (article !== null && article !== undefined) {
        row[0] = article.pubmedId;
        row[1] = article.title;
        row[2] = article.authors.join();
        row[3] = article.journal;
        row[4] = article.year;
      } else {
        row[0] = entry.publication.resource;
        row[1] = "N/A";
        row[2] = "N/A";
        row[3] = "N/A";
        row[4] = "N/A";
      }
      row[5] = "";
      row[6] = "";

      var submaps = [];
      for (var j = 0; j < elements.length; j++) {
        var modelId = elements[j].modelId;
        if (submaps[modelId] === undefined) {
          row[6] += self.getMap().getSubmapById(modelId).getModel().getName() + ", ";
          submaps[elements[j].modelId] = true;
        }
      }
      return Promise.all(elements.map(function (element) {
        var model = self.getMap().getSubmapById(element.modelId).getModel();
        return model.getByIdentifiedElement(new IdentifiedElement(element)).then(function (reaction) {
          row[5] += reaction.getElementId() + ",";
        });
      })).then(function () {
        result.push(row);
      });
    }));
  }).then(function () {
    return result;
  });

};

PublicationListDialog.prototype.publicationListAsCsvString = function (publicationList) {
  var self = this;

  return self.publicationListToArray(publicationList).then(function (data) {
    return new Promise(function (resolve) {
      stringify(data, function (err, output) {
        resolve(output);
      });
    });
  });
};

/**
 *
 * @return {string}
 */
PublicationListDialog.prototype.createSubmapFilter = function () {
  var result = "<select class='minerva-submap-filter'>";
  result += "<option value>---</option>";

  var maps = this.getProject().getModels();
  for (var i = 0; i < maps.length; i++) {
    var map = maps[i];
    result += "<option value='" + map.getId() + "'>" + map.getName() + "</option>";
  }
  result += "</select>";
  return result;
};

PublicationListDialog.prototype.getSubmapFilterValue = function () {
  var select = $(".minerva-submap-filter", this.getElement());
  return select.val();
};

module.exports = PublicationListDialog;
