"use strict";

var Promise = require("bluebird");
var $ = require('jquery');

/* exported logger */
var logger = require('../../logger');
var Functions = require('../../Functions');

var AbstractDbPanel = require('./AbstractDbPanel');
var PanelControlElementType = require('../PanelControlElementType');

/**
 *
 * @param params
 * @constructor
 * @extends AbstractDbPanel
 */
function DrugPanel(params) {
  params.panelName = "drug";
  params.helpTip = '<p>source: <a target="_drugbank" href="http://www.drugbank.ca/">DrugBank</a> and '
    + '<a target="_drugbank" href="https://www.ebi.ac.uk/chembl/">ChEMBL</a></p>'
    + "<p>use of drug names, synonyms and brand names is supported<p>separate multiple search by semicolon</p>";
  params.placeholder = "drug, synonym, brand name";

  AbstractDbPanel.call(this, params);
  $(params.element).addClass("minerva-drug-panel");
  var self = this;
  self.getControlElement(PanelControlElementType.SEARCH_LABEL).innerHTML = "SEARCH FOR TARGETS OF:";
}

DrugPanel.prototype = Object.create(AbstractDbPanel.prototype);
DrugPanel.prototype.constructor = DrugPanel;

/**
 *
 * @param {Drug} [drug]
 * @returns {HTMLDivElement}
 */
DrugPanel.prototype.createPreamble = function (drug) {
  var self = this;
  var guiUtils = self.getGuiUtils();
  var result = Functions.createElement({type: "div", style: "padding:8px"});
  if (drug === undefined || drug.getName() === undefined) {
    result.appendChild(guiUtils.createLabel("NOT FOUND"));
  } else {
    result.appendChild(guiUtils.createParamLine({label: "Drug: ", value: drug.getName()}));
    result.appendChild(guiUtils.createParamLine({label: "Description: ", value: drug.getDescription()}));
    result.appendChild(guiUtils.createArrayParamLine({label: "Synonyms: ", value: drug.getSynonyms()}));
    result.appendChild(guiUtils.createArrayParamLine({label: "Brand names: ", value: drug.getBrandNames()}));
    result.appendChild(guiUtils.createParamLine({label: "Blood brain barrier: ", value: drug.getBloodBrainBarrier()}));
    result.appendChild(guiUtils.createAnnotations({
      label: "Sources: ",
      annotations: drug.getReferences(),
      groupAnnotations: false
    }));
    result.appendChild(guiUtils.createNewLine());
  }

  return result;
};

/**
 *
 * @param {Target} target
 * @param {string} icon
 * @returns {Promise<HTMLTableRowElement>}
 */
DrugPanel.prototype.createTableElement = function (target, icon) {
  return this.createTargetRow(target, icon);
};

/**
 *
 * @returns {Promise}
 */
DrugPanel.prototype.searchByQuery = function () {
  var self = this;
  var query = self.getControlElement(PanelControlElementType.SEARCH_INPUT).value;
  return self.getOverlayDb().searchByQuery(query);
};

/**
 *
 * @returns {Promise}
 */
DrugPanel.prototype.init = function () {
  var self = this;
  return AbstractDbPanel.prototype.init.call(this).then(function () {
    return self.getServerConnector().isDapiConnectionValid();
  }).then(function (status) {
    if (!status) {
      var searchButton = $(".minerva-search-link", self.getElement());
      var warning = $("<div class='minerva-search-link-warning' " +
        "title='DAPI connection is not available. Searching in not all databases is available. Ask administrator to enable Data-API interface.'>" +
        "<i class='fa fa-exclamation-triangle'></i></div>");
      warning.insertAfter(searchButton);
      warning.tooltip({show: {delay: 0.1}});
    } else {
      return self.getServerConnector().getDapiDatabaseReleases("DrugBank").then(function (releases) {
        var accepted = 0;
        for (var i = 0; i < releases.length; i++) {
          if (releases[i].accepted) {
            accepted++;
          }
        }
        if (accepted === 0) {
          var searchButton = $(".minerva-search-link", self.getElement());
          var warning = $("<div class='minerva-search-link-warning' " +
            "title='Searching in not all databases is available. Ask administrator to enable Data-API drug database.'>" +
            "<i class='fa fa-exclamation-triangle'></i></div>");
          warning.insertAfter(searchButton);
          warning.tooltip({show: {delay: 0.1}});
        }
      });
    }
  }).then(function () {
    var query = self.getServerConnector().getSessionData().getDrugQuery();
    if (query !== undefined) {
      return self.getOverlayDb().searchByEncodedQuery(query);
    }
  });
};

/**
 *
 * @returns {Promise}
 */
DrugPanel.prototype.destroy = function () {
  return Promise.resolve();
};

/**
 *
 * @param {string} query
 * @returns {string[]}
 */
DrugPanel.prototype.getAutocomplete = function (query) {
  if (this._searchAutocomplete === undefined) {
    this.refreshSearchAutocomplete();
    return [];
  }

  return this._searchAutocomplete[query];
};

/**
 *
 * @returns {Promise}
 */
DrugPanel.prototype.refreshSearchAutocomplete = function () {
  var self = this;
  self._searchAutocomplete = [];
  return ServerConnector.getDrugSuggestedQueryList().then(function (queries) {
    self._searchAutocomplete = self.computeAutocompleteDictionary(queries);
    return self._searchAutocomplete;
  });
};

module.exports = DrugPanel;
