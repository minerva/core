"use strict";

var Promise = require("bluebird");
var $ = require('jquery');

/* exported logger */
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
var Functions = require('../../Functions');

var AbstractDbPanel = require('./AbstractDbPanel');
var PanelControlElementType = require('../PanelControlElementType');

/**
 *
 * @param params
 * @constructor
 * @extends AbstractDbPanel
 */
function ChemicalPanel(params) {
  var self = this;
  params.panelName = "chemical";
  params.placeholder = "full chemical name (CTD)";


  AbstractDbPanel.call(self, params);

  $(params.element).addClass("minerva-chemical-panel");

  if (self.getMap().getProject().getDisease() === undefined) {
    self.disablePanel("The Comparative Toxicogenomics Database (CTD) requires a disease context. " +
      "Choose an appropriate MeSH code and provide it in the via the Admin section of this MINERVA platform. " +
      "See User Manual for more details ");
  } else {
    self.getControlElement(PanelControlElementType.SEARCH_LABEL).innerHTML = "SEARCH FOR TARGETS OF:";
  }
}

ChemicalPanel.prototype = Object.create(AbstractDbPanel.prototype);
ChemicalPanel.prototype.constructor = ChemicalPanel;

/**
 *
 * @param {Chemical} [chemical]
 * @returns {HTMLDivElement}
 */
ChemicalPanel.prototype.createPreamble = function (chemical) {
  var self = this;
  var guiUtils = self.getGuiUtils();
  var result = Functions.createElement({type: "div", style: "padding:8px"});
  if (chemical === undefined || chemical.getName() === undefined) {
    result.appendChild(guiUtils.createLabel("NOT FOUND"));
  } else {
    result.appendChild(guiUtils.createParamLine({label: "Chemical: ", value: chemical.getName()}));
    result.appendChild(guiUtils.createParamLine({label: "Description: ", value: chemical.getDescription()}));
    result.appendChild(guiUtils.createArrayParamLine({label: "Synonyms: ", value: chemical.getSynonyms()}));
    result.appendChild(guiUtils.createParamLine({label: "Direct Evidence: ", value: chemical.getDirectEvidence()}));
    result.appendChild(guiUtils.createAnnotations({
      label: "Direct Evidence Publications: ",
      annotations: chemical.getDirectEvidenceReferences(),
      groupAnnotations: false
    }));
    result.appendChild(guiUtils.createAnnotations({label: "Sources: ", annotations: chemical.getReferences()}));
    result.appendChild(guiUtils.createNewLine());
  }

  return result;
};

/**
 *
 * @param {Target} target
 * @param {string} icon
 * @returns {Promise<HTMLTableRowElement>}
 */
ChemicalPanel.prototype.createTableElement = function (target, icon) {
  return this.createTargetRow(target, icon);
};

/**
 *
 * @returns {Promise}
 */
ChemicalPanel.prototype.searchByQuery = function () {
  var self = this;
  var query = self.getControlElement(PanelControlElementType.SEARCH_INPUT).value;

  return self.getOverlayDb().searchByQuery(query);
};

/**
 *
 * @returns {Promise}
 */
ChemicalPanel.prototype.init = function () {
  var self = this;
  return AbstractDbPanel.prototype.init.call(this).then(function () {
    return self.getToolTipForAnnotation(self.getProject().getDisease()).then(function (toolTip) {
      self.setHelpTip(toolTip);
      var query = ServerConnector.getSessionData().getChemicalQuery();
      if (query !== undefined) {
        return self.getOverlayDb().searchByEncodedQuery(query);
      }
    });
  }).then(function () {
    return self.getServerConnector().isDapiConnectionValid();
  }).then(function (status) {
    if (!status) {
      var searchButton = $(".minerva-search-link", self.getElement());
      var warning = $("<div class='minerva-search-link-warning' " +
        "title='DAPI connection is not available. Searching in not all databases is available. Ask administrator to enable Data-API interface.'>" +
        "<i class='fa fa-exclamation-triangle'></i></div>");
      warning.insertAfter(searchButton);
      warning.tooltip({show: {delay: 0.1}});
    } else {
      return self.getServerConnector().getDapiDatabaseReleases("CTD").then(function (releases) {
        var accepted = 0;
        for (var i = 0; i < releases.length; i++) {
          if (releases[i].accepted) {
            accepted++;
          }
        }
        if (accepted === 0) {
          var searchButton = $(".minerva-search-link", self.getElement());
          var warning = $("<div class='minerva-search-link-warning' " +
            "title='Searching in not all databases is available. Ask administrator to enable Data-API chemical database.'>" +
            "<i class='fa fa-exclamation-triangle'></i></div>");
          warning.insertAfter(searchButton);
          warning.tooltip({show: {delay: 0.1}});
        }
      });
    }
  }).then(function () {
    var query = self.getServerConnector().getSessionData().getChemicalQuery();
    if (query !== undefined) {
      return self.getOverlayDb().searchByEncodedQuery(query);
    }
  });
};

/**
 *
 * @returns {Promise}
 */
ChemicalPanel.prototype.destroy = function () {
  return Promise.resolve();
};

/**
 *
 * @param {string} query
 * @returns {string[]}
 */
ChemicalPanel.prototype.getAutocomplete = function (query) {
  if (this._searchAutocomplete === undefined) {
    this.refreshSearchAutocomplete();
    return [];
  }

  return this._searchAutocomplete[query];
};

/**
 *
 * @returns {Promise}
 */
ChemicalPanel.prototype.refreshSearchAutocomplete = function () {
  var self = this;
  self._searchAutocomplete = [];
  return ServerConnector.getChemicalSuggestedQueryList().then(function (queries) {
    self._searchAutocomplete = self.computeAutocompleteDictionary(queries);
    return self._searchAutocomplete;
  });
};

/**
 *
 * @param {Annotation} annotation
 * @returns {string}
 */
ChemicalPanel.prototype.getToolTipForAnnotation = function (annotation) {
  var self = this;
  var promise = Promise.resolve('disease');
  if (annotation !== null && annotation !== undefined) {
    promise = ServerConnector.getMesh({id: annotation.getResource()}).then(function (mesh) {
      return mesh.getName() + " (" + self.getGuiUtils().createAnnotationLink(annotation).outerHTML + ")";
    });
  }
  return promise.then(function (diseaseString) {
    var result = '<p>source: Comparative Toxicogenomics Database <a target="_ctd" href="http://ctdbase.org/">ctdbase.org</a></p>'
      + '<p>only associations between genes and chemicals with direct evidence to '
      + diseaseString + ' are displayed</p>'
      + '<p>use only the full name of chemicals according to <a target="_ctd_chemicals" href="http://ctdbase.org/voc.go?type=chem"> ctdbase/chem</a> for search</p>'
      + 'if the chemical name includes comma(s), place a semicolon behind the name to avoid a segmentation of the name</p>'
      + '<p>separate multiple search by semicolon';
    return Promise.resolve(result);
  });
};

module.exports = ChemicalPanel;
