"use strict";

var Promise = require("bluebird");
var $ = require('jquery');

/* exported logger */

var Panel = require('../Panel');
var ChemicalPanel = require('./ChemicalPanel');
var DrugPanel = require('./DrugPanel');
var GenericSearchPanel = require('./GenericSearchPanel');

var logger = require('../../logger');

/**
 *
 * @param {Configuration} params.configuration
 * @param {HTMLElement} params.element
 * @param {Project} params.project
 * @param {CustomMap} params.customMap
 * @param params.parent
 *
 * @constructor
 * @extends AbstractGuiElement
 */
function SearchPanel(params) {
  params["panelName"] = "global-search";
  Panel.call(this, params);
  var self = this;
  $(params.element).addClass("minerva-search-panel");

  self._createPanelGui();
}

SearchPanel.prototype = Object.create(Panel.prototype);
SearchPanel.prototype.constructor = SearchPanel;

/**
 *
 * @private
 */
SearchPanel.prototype._createPanelGui = function () {
  var self = this;

  self.getGuiUtils().initTabContent(self);

  var panels = self.getPanelsDefinition();
  for (var i = 0; i < panels.length; i++) {
    self.getGuiUtils().addTab(self, panels[i]);
  }
};

/**
 *
 * @returns {*[]}
 */
SearchPanel.prototype.getPanelsDefinition = function () {
  return [{
    name: "CONTENT",
    panelClass: GenericSearchPanel,
    linkClassName: "minerva-generic-search-tab-link",
    options: {parent: this}
  }, {
    name: "DRUG",
    panelClass: DrugPanel,
    linkClassName: "minerva-drug-search-tab-link",
    options: {parent: this}
  }, {
    name: "CHEMICAL",
    panelClass: ChemicalPanel,
    linkClassName: "minerva-chemical-search-tab-link",
    options: {parent: this}
  }];
};

/**
 *
 * @returns {Promise}
 */
SearchPanel.prototype.init = function () {
  var self = this;

  var promises = [];
  for (var i = 0; i < self._panels.length; i++) {
    promises.push(self._panels[i].init());
  }
  return Promise.all(promises);
};

/**
 *
 * @returns {Promise}
 */
SearchPanel.prototype.destroy = function () {
  return Promise.resolve();
};


module.exports = SearchPanel;
