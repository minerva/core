"use strict";

/* exported logger */
var $ = require('jquery');

var ConfigurationType = require('../../ConfigurationType');
var GuiConnector = require('../../GuiConnector');
var Panel = require('../Panel');
var PanelControlElementType = require('../PanelControlElementType');
var PublicationListDialog = require('./PublicationListDialog');

var logger = require('../../logger');
var Functions = require('../../Functions');
var xss = require('xss');

var Promise = require('bluebird');

var HttpStatus = require('http-status-codes');
var NetworkError = require('../../NetworkError');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} params.customMap
 * @param {Configuration} [params.configuration]
 * @param {Project} [params.project]
 * @param params.parent
 *
 * @constructor
 * @extends Panel
 */
function ProjectInfoPanel(params) {
  params.panelName = "info";
  params.scrollable = true;
  Panel.call(this, params);
  var self = this;
  $(params.element).addClass("minerva-info-panel");
  self._createInfoPanelGui();

  self._createUserDataTab();
  var logoutButton = self.getControlElement(PanelControlElementType.USER_TAB_LOGOUT_BUTTON);

  logoutButton.onclick = function () {
    return ServerConnector.logout().catch(GuiConnector.alert);
  };
}

ProjectInfoPanel.prototype = Object.create(Panel.prototype);
ProjectInfoPanel.prototype.constructor = ProjectInfoPanel;

/**
 *
 * @private
 */
ProjectInfoPanel.prototype._createInfoPanelGui = function () {
  var self = this;
  var guiUtils = self.getGuiUtils();
  var projectId = self.getMap().getProject().getProjectId();

  var infoDiv = Functions.createElement({
    type: "div",
    name: "infoDiv",
    className: "searchPanel"
  });
  this.getElement().appendChild(infoDiv);

  var infoTitle = Functions.createElement({
    type: "h4",
    content: "PROJECT INFO:"
  });
  infoDiv.appendChild(infoTitle);

  var dataTab = Functions.createElement({
    type: "div",
    style: "display: table;border-spacing: 5px;"
  });
  infoDiv.appendChild(dataTab);

  dataTab.appendChild(guiUtils.createTableRow(["Name:", Functions.createElement({
    type: "div",
    style: "width:100%",
    name: "projectNameText"
  })]));

  dataTab.appendChild(guiUtils.createTableRow(["Version:", Functions.createElement({
    type: "div",
    style: "width:100%",
    name: "projectVersionText"
  })]));

  infoTitle = Functions.createElement({
    type: "h4",
    content: "Data:"
  });
  infoDiv.appendChild(infoTitle);

  var dialogDiv = Functions.createElement({
    type: "div"
  });

  this.publicationListDialog = new PublicationListDialog({
    customMap: self.getMap(),
    element: dialogDiv
  });

  dataTab = Functions.createElement({
    type: "ul",
    className: "minerva-project-info-data-list"
  });
  infoDiv.appendChild(dataTab);

  var projectPublicationsText = Functions.createElement({
    type: "span",
    name: "projectPublicationsText",
    style: "font-weight: normal;font-style: normal;cursor: pointer"
  });

  var projectPublicationsButton = Functions.createElement({
    type: "a",
    name: "projectPublicationsButton",
    onclick: function () {
      return self.showPublicationListDialog();
    }
  });
  projectPublicationsButton.appendChild(Functions.createElement({
    type: "span",
    content: "<i class='fa fa-list'></i>&nbsp;",
    xss: false
  }));

  projectPublicationsButton.appendChild(projectPublicationsText);
  projectPublicationsButton.appendChild(Functions.createElement({
    type: "span",
    style: "font-weight: normal;font-style: normal;cursor: pointer",
    content: "&nbsp;publication(s)"
  }));

  var liElement = Functions.createElement({
    type: "li"
  });
  liElement.appendChild(projectPublicationsButton);
  dataTab.appendChild(liElement);

  var projectOriginalFileButton = Functions.createElement({
    type: "a",
    name: "projectOriginalFileButton",
    href: "#",
    content: "<i class='fa fa-files-o'></i>&nbsp;source file",
    xss: false,
    onclick: function () {
      return self.downloadSourceFile();
    }
  });
  liElement = Functions.createElement({
    type: "li"
  });
  liElement.appendChild(projectOriginalFileButton);
  dataTab.appendChild(liElement);

  var projectLicenseButton = Functions.createElement({
    type: "a",
    name: "projectLicenseButton",
    href: "#",
    xss: false,
    content: "License: <span name='projectLicenseText'>---</span>",
    onclick: function () {
    }
  });
  liElement = Functions.createElement({
    type: "li"
  });
  liElement.appendChild(projectLicenseButton);
  dataTab.appendChild(liElement);

  var exportButton = Functions.createElement({
    type: "a",
    href: ServerConnector.getServerBaseUrl() + "export.xhtml?id=" + projectId,
    content: '<i class="fa fa-mail-forward"></i>&nbsp;EXPORT',
    xss: false
  });
  liElement = Functions.createElement({
    type: "li"
  });
  liElement.appendChild(exportButton);
  dataTab.appendChild(liElement);

  var manualButton = Functions.createElement({
    type: "a",
    name: "manualLink",
    content: '<i class="fa fa-file"></i>&nbsp;MANUAL',
    xss: false
  });
  manualButton.href = "#";
  manualButton.onclick = function () {
    return ServerConnector.getConfigurationParam(ConfigurationType.USER_MANUAL_FILE).then(function (manualFileUrl) {
      var win = window.open(manualFileUrl, '_user_manual_');
      win.focus();
    });
  };
  liElement = Functions.createElement({
    type: "li"
  });
  liElement.appendChild(manualButton);
  dataTab.appendChild(liElement);

  if (self.getProject().getDisease() !== undefined) {
    var diseaseDiv = Functions.createElement({type: "div", content: "Disease: "});
    var diseaseLink = guiUtils.createAnnotationLink(self.getProject().getDisease());
    diseaseLink.name = "projectDisease";
    liElement = Functions.createElement({
      type: "li"
    });
    diseaseDiv.appendChild(diseaseLink);
    liElement.appendChild(diseaseDiv);
    dataTab.appendChild(liElement);
  }

  if (self.getProject().getOrganism() !== undefined) {
    var organismDiv = Functions.createElement({type: "div", content: "Organism: "});
    var organismLink = guiUtils.createAnnotationLink(self.getProject().getOrganism());
    organismLink.name = "projectOrganism";
    liElement = Functions.createElement({
      type: "li"
    });
    organismDiv.appendChild(organismLink);
    liElement.appendChild(organismDiv);
    dataTab.appendChild(liElement);
  }

  if (self.getProject().getModels()[0].getReferences().length > 0) {
    var referencesTab = Functions.createElement({
      type: "div"
    });
    infoDiv.appendChild(referencesTab);

    var references = Functions.createElement({
      type: "h4",
      content: 'References:'
    });
    referencesTab.appendChild(references);
    referencesTab.appendChild(guiUtils.createAnnotationList(self.getProject().getModels()[0].getReferences(), {groupAnnotations: false}));
  }

  if (self.getProject().getModels()[0].getAuthors().length > 0) {
    var authorsTab = Functions.createElement({
      type: "div"
    });
    infoDiv.appendChild(authorsTab);

    var authors = Functions.createElement({
      type: "h4",
      content: 'Authors:'
    });
    authorsTab.appendChild(authors);
    authorsTab.appendChild(guiUtils.createAuthorsList(self.getProject().getModels()[0].getAuthors()));
  }

  if (self.getProject().getModels()[0].getDescription() !== null && self.getProject().getModels()[0].getDescription() !== "") {
    var descriptionTab = Functions.createElement({
      type: "div"
    });
    infoDiv.appendChild(descriptionTab);

    descriptionTab.appendChild(Functions.createElement({
      type: "h4",
      content: 'Description:'
    }));
    descriptionTab.appendChild(Functions.createElement({
      type: "p",
      content: self.getProject().getModels()[0].getDescription()
    }));
  }


};

/**
 *
 * @returns {Promise}
 */
ProjectInfoPanel.prototype.downloadSourceFile = function () {
  var self = this;
  return ServerConnector.getProjectSourceDownloadUrl().then(function (url) {
    return self.downloadFile(url);
  }).then(null, GuiConnector.alert);
};

/**
 *
 * @returns {Promise}
 */
ProjectInfoPanel.prototype.showPublicationListDialog = function () {
  return this.publicationListDialog.show();
};

/**
 *
 * @returns {Promise}
 */
ProjectInfoPanel.prototype.refresh = function () {
  var self = this;

  var projectNameText = $("[name='projectNameText']", self.getElement())[0];
  var projectVersionText = $("[name='projectVersionText']", self.getElement())[0];
  var projectPublicationsText = $("[name='projectPublicationsText']", self.getElement())[0];
  var projectOrganismText = $("[name='projectOrganism']", self.getElement())[0];
  var projectDiseaseText = $("[name='projectDisease']", self.getElement())[0];

  var projectLicenseText = $("[name='projectLicenseText']", self.getElement())[0];

  var project = self.getProject();
  projectNameText.innerHTML = xss(project.getName());
  projectVersionText.innerHTML = xss(project.getVersion());

  var promises = [
    ServerConnector.getProjectStatistics().then(function (statistics) {
      projectPublicationsText.innerHTML = statistics.getPublicationCount()
    }),
    ServerConnector.getLoggedUser().then(function (user) {
      self.showUserProfilePage(user);
    })];
  if (projectOrganismText !== undefined) {
    promises.push(ServerConnector.getTaxonomy({id: parseInt(project.getOrganism().getResource())}).then(function (taxonomy) {
      projectOrganismText.innerHTML = taxonomy.getName();
    }));
  }
  if (projectDiseaseText !== undefined) {
    promises.push(ServerConnector.getMesh({id: project.getDisease().getResource()}).then(function (mesh) {
      projectDiseaseText.innerHTML = mesh.getName();
    }));
  }

  $("[name='projectLicenseButton']", self.getElement()).off('click');
  if (project.getLicense() !== null) {
    projectLicenseText.innerHTML = project.getLicense().getName();
    $("[name='projectLicenseButton']", self.getElement()).on('click', function(){
      GuiConnector.info(project.getLicense().getContent());
    });
  } else {
    projectLicenseText.innerHTML = "---";
  }

  return Promise.all(promises);
};

/**
 *
 * @private
 */
ProjectInfoPanel.prototype._createUserDataTab = function () {
  var self = this;
  var guiUtils = self.getGuiUtils();
  var userDataDiv = Functions.createElement({
    type: "div",
    name: "userDataTab",
    className: "searchPanel",
    style: "display:none"
  });
  this.getElement().appendChild(userDataDiv);
  this.setControlElement(PanelControlElementType.USER_TAB_USER_DIV, userDataDiv);

  var userDataTitle = Functions.createElement({
    type: "h3",
    content: "<div class='minerva-profile-img' ></div><br/>User data<br/>",
    xss: false
  });
  userDataDiv.appendChild(userDataTitle);

  var userDataFormTab = Functions.createElement({
    type: "div",
    style: "width:100%;display: table;border-spacing: 10px;"
  });
  userDataDiv.appendChild(userDataFormTab);

  var loginLabel = Functions.createElement({
    type: "span",
    content: "LOGIN:",
    style: "color:#999999"
  });
  var loginText = Functions.createElement({
    type: "span",
    name: "loginValue",
    style: "padding-left:5px"
  });
  this.setControlElement(PanelControlElementType.USER_TAB_LOGIN_TEXT, loginText);
  userDataDiv.appendChild(guiUtils.createTableRow([loginLabel, loginText]));

  var nameLabel = Functions.createElement({
    type: "span",
    content: "NAME:",
    style: "color:#999999"
  });
  var nameText = Functions.createElement({
    type: "span",
    name: "nameValue",
    style: "padding-left:5px"
  });
  this.setControlElement(PanelControlElementType.USER_TAB_NAME_TEXT, nameText);
  userDataDiv.appendChild(guiUtils.createTableRow([nameLabel, nameText]));

  var surnameLabel = Functions.createElement({
    type: "span",
    content: "SURNAME:",
    style: "color:#999999"
  });
  var surnameText = Functions.createElement({
    type: "span",
    name: "surnameValue",
    style: "padding-left:5px"
  });
  this.setControlElement(PanelControlElementType.USER_TAB_SURNAME_TEXT, surnameText);
  userDataDiv.appendChild(guiUtils.createTableRow([surnameLabel, surnameText]));

  var emailLabel = Functions.createElement({
    type: "span",
    content: "EMAIL:",
    style: "color:#999999"
  });
  var emailText = Functions.createElement({
    type: "span",
    name: "emailValue",
    style: "padding-left:5px"
  });
  self.setControlElement(PanelControlElementType.USER_TAB_EMAIL_TEXT, emailText);
  userDataDiv.appendChild(guiUtils.createTableRow([emailLabel, emailText]));

  var centerTag = Functions.createElement({
    type: "center",
    style: "padding-top: 5px;"
  });
  userDataDiv.appendChild(centerTag);

  var logoutButton = Functions.createElement({
    type: "button",
    name: "logoutButton",
    content: "LOGOUT"
  });
  centerTag.appendChild(logoutButton);
  self.setControlElement(PanelControlElementType.USER_TAB_LOGOUT_BUTTON, logoutButton);
  self.setControlElement(PanelControlElementType.USER_TAB_USER_DIV, userDataDiv);

  var loginTabDiv = Functions.createElement({
    type: "div",
    name: "userLoginTab",
    className: "searchPanel",
    style: "display:none",
    content: "<h3><div class='minerva-profile-img'></div>"
      + '<br/>User data</h3><br/>YOU ARE NOT LOGGED IN.<br/>' + '<center><button>LOGIN</button></center>' +
      '<br/><a href="#" name="requestAccount" >Request an account</a>' +
      '<a href="#" class="signup_button adminLink"><i class="fa fa-chevron-right"></i> SIGN UP</a>',
    xss: false

  });
  $(loginTabDiv).find("button")[0].onclick = function () {
    return self.getParent().getLoginDialog().open();
  };

  $(".signup_button", loginTabDiv).on('click', function () {
    var content = $('<div/>').html("<div>" +
      "<div class='minerva-menu-row'>" +
      "<label style='margin-right: 20px; width: 120px'>Email:</label>" +
      "<input class='minerva-email'/>" +
      "</div>" +
      "<div class='minerva-menu-row'>" +
      "<label style='margin-right: 20px; width: 120px'>Password:</label>" +
      "<input class='minerva-password' type='password'/>" +
      "</div>" +
      "<div class='minerva-menu-row'>" +
      "<label style='margin-right: 20px; width: 120px'>Name:</label>" +
      "<input class='minerva-name'/>" +
      "</div>" +
      "<div class='minerva-menu-row'>" +
      "<label style='margin-right: 20px; width: 120px'>Surname:</label>" +
      "<input class='minerva-surname'/>" +
      "</div>" +
      "<div class='minerva-menu-row'>" +
      "<button class='minerva-send-button'><span class='ui-icon ui-icon-mail-closed'></span>&nbsp;SIGNUP</button>" +
      "<button class='minerva-cancel-button'><span class='ui-icon ui-icon-cancel'></span>&nbsp;CANCEL</button>" +
      "</div>" +
      "</div>");
    $(content).dialog({
      dialogClass: 'minerva-sign-up-dialog',
      title: "Sign up",
      width: 400,
      height: 200,
      modal: true,
      close: function () {
        $(content).dialog("destroy");
      }
    });


    $(".minerva-send-button", content).on("click", function () {
      GuiConnector.showProcessing();
      ServerConnector.signUp({
        "email": $('.minerva-email', content).val(),
        "name": $('.minerva-name', content).val(),
        "surname": $('.minerva-surname', content).val(),
        "password": $('.minerva-password', content).val()
      }).then(function () {
        GuiConnector.info("We have sent a confirmation email to the provided address.");
        $(content).dialog("destroy");
      }).catch(function (error) {
        if (error instanceof NetworkError && error.statusCode === HttpStatus.BAD_REQUEST) {
          var message = error.content;
          if (error.content !== undefined) {
            message = error.content.reason;
          }
          GuiConnector.showErrorDialog("Error", message);
        } else {
          GuiConnector.alert(error);
        }
      }).finally(function () {
        GuiConnector.hideProcessing();
      })
    });
    $(".minerva-cancel-button", content).on("click", function () {
      $(content).dialog("destroy");
    });
  });

  self.getElement().appendChild(loginTabDiv);
  self.setControlElement(PanelControlElementType.USER_TAB_LOGIN_DIV, loginTabDiv);

};

/**
 *
 * @param {User} user
 */
ProjectInfoPanel.prototype.showUserProfilePage = function (user) {

  var self = this;
  var userDataTabDiv = self.getControlElement(PanelControlElementType.USER_TAB_USER_DIV);
  var userLoginTabDiv = self.getControlElement(PanelControlElementType.USER_TAB_LOGIN_DIV);
  if (user.getLogin() !== "anonymous") {
    var loginText = self.getControlElement(PanelControlElementType.USER_TAB_LOGIN_TEXT);
    var nameText = self.getControlElement(PanelControlElementType.USER_TAB_NAME_TEXT);
    var surnameText = self.getControlElement(PanelControlElementType.USER_TAB_SURNAME_TEXT);
    var emailText = self.getControlElement(PanelControlElementType.USER_TAB_EMAIL_TEXT);

    userLoginTabDiv.style.display = "none";
    userDataTabDiv.style.display = "block";

    loginText.innerHTML = user.getLogin();
    nameText.innerHTML = user.getName();
    surnameText.innerHTML = user.getSurname();
    emailText.innerHTML = user.getEmail();
  } else {

    userLoginTabDiv.style.display = "block";
    userDataTabDiv.style.display = "none";
  }
};

/**
 *
 * @returns {Promise}
 */
ProjectInfoPanel.prototype.init = function () {
  var self = this;
  var email = self.getConfiguration().getOption(ConfigurationType.REQUEST_ACCOUNT_EMAIL).getValue();
  var content = encodeURIComponent(self.getConfiguration().getOption(ConfigurationType.REQUEST_ACCOUNT_DEFAULT_CONTENT).getValue());
  var title = self.getConfiguration().getOption(ConfigurationType.REQUEST_ACCOUNT_DEFAULT_TITLE).getValue();
  var url = 'mailto:' + email + '?subject=' + title + '&body=' + content;
  var link = $("[name=requestAccount]", self.getElement());
  link.attr("href", url);
  return self.getServerConnector().getLoggedUser().then(function (user) {
    if (!user.isConnectedToLdap()) {
      var logoutButton = self.getControlElement(PanelControlElementType.USER_TAB_LOGOUT_BUTTON);
      $(logoutButton).before("<button class='minerva-change-password-button'>Change Password</button>");
      $("button.minerva-change-password-button", self.getElement()).on('click', function () {
        return self.getServerConnector().getLoggedUser().then(function (user) {
          var content = $('<div/>').html("<div><table class='display'></table>" +
            "<div class='minerva-menu-row'>" +
            "<button class='minerva-save-button' disabled='disabled'><span class='ui-icon ui-icon-disk'></span>&nbsp;SAVE</button>" +
            "<button class='minerva-cancel-button'><span class='ui-icon ui-icon-cancel'></span>&nbsp;CANCEL</button>" +
            "</div>" +
            "</div>");
          $(content).dialog({
            dialogClass: 'minerva-change-password-dialog',
            title: "Change password",
            width: 420,
            height: 240,
            close: function () {
              $(content).dialog("destroy");
              dataTable.destroy();
            }
          });

          // noinspection JSCheckFunctionSignatures
          var dataTable = $("table", content).DataTable({
            columns: [{
              title: "Name"
            }, {
              title: "Value"
            }],
            paging: false,
            ordering: false,
            searching: false,
            bInfo: false
          });

          var data = [];

          data.push(['Password', "<input type='password' class='minerva-password' autocomplete='new-password'>"]);
          data.push(['Repeat password', "<input type='password' class='minerva-password-2' autocomplete='new-password'/>"]);

          dataTable.clear().rows.add(data).draw();

          $('.minerva-password, .minerva-password-2').on('keyup', function () {
            if ($('.minerva-password').val() === $('.minerva-password-2').val()) {
              if ($('.minerva-password').val() !== "") {
                $(".minerva-save-button").prop('disabled', false);
              } else {
                $(".minerva-save-button").prop('disabled', true);
              }
            } else {
              $(".minerva-save-button").prop('disabled', true);
            }
          });


          $(".minerva-save-button", content).on("click", function () {
            GuiConnector.showProcessing();
            return self.getServerConnector().getLoggedUser().then(function (user) {
              user.setPassword($('.minerva-password').val());
              return self.getServerConnector().updateUser(user);
            }).catch(GuiConnector.alert).finally(function () {
              $(content).dialog("destroy");
              dataTable.destroy();
              GuiConnector.hideProcessing();
            });
          });
          $(".minerva-cancel-button", content).on("click", function () {
            $(content).dialog("destroy");
            dataTable.destroy();
          });
        });
      });
    }
    return ServerConnector.getConfiguration();
  }).then(function (configuration) {
    if (configuration.getOption(ConfigurationType.REQUEST_ACCOUNT_EMAIL).getValue() === "" ||
      configuration.getOption(ConfigurationType.REQUEST_ACCOUNT_EMAIL).getValue() === undefined ||
      configuration.getOption(ConfigurationType.ALLOW_AUTO_REGISTER).getValue().toLowerCase() === "true"
    ) {
      $("[name=requestAccount]", self.getElement()).hide();
    }
    if (configuration.getOption(ConfigurationType.ALLOW_AUTO_REGISTER).getValue().toLowerCase() !== "true") {
      $(".signup_button", self.getElement()).hide();
    }
    return self.refresh();
  });
};

/**
 *
 */
ProjectInfoPanel.prototype.destroy = function () {
  return this.publicationListDialog.destroy();
};
module.exports = ProjectInfoPanel;
