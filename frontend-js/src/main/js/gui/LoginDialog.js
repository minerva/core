"use strict";

/* exported logger */
var $ = require('jquery');
var GuiUtils = require('./leftPanel/GuiUtils');

var AbstractGuiElement = require('./AbstractGuiElement');
var GuiConnector = require('../GuiConnector');
var InvalidCredentialsError = require('../InvalidCredentialsError');
var PanelControlElementType = require('./PanelControlElementType');
var ConfigurationType = require("../ConfigurationType");

var Functions = require('../Functions');
var logger = require('../logger');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} params.customMap
 * @param {Configuration} params.configuration
 * @param {Project} params.project
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 * @extends AbstractGuiElement
 */
function LoginDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self._createLoginTab();

  var loginButton = self.getControlElement(PanelControlElementType.USER_TAB_LOGIN_BUTTON);
  loginButton.onclick = function () {
    var login = self.getControlElement(PanelControlElementType.USER_TAB_LOGIN_INPUT_TEXT).value;
    var password = self.getControlElement(PanelControlElementType.USER_TAB_PASSWORD_INPUT_TEXT).value;

    return ServerConnector.login(login, password).then(function () {
      window.location.reload(false);
    }).then(null, function (error) {
      if (error instanceof InvalidCredentialsError) {
        GuiConnector.alert("invalid credentials");
      } else {
        GuiConnector.alert(error);
      }
    });
  };

}

LoginDialog.prototype = Object.create(AbstractGuiElement.prototype);
LoginDialog.prototype.constructor = LoginDialog;

/**
 *
 * @param {HTMLElement[]} elements
 * @returns {HTMLElement}
 */
LoginDialog.prototype.createTableRow = function (elements) {
  var row = Functions.createElement({
    type: "div",
    style: "display: table-row;"
  });

  for (var i = 0; i < elements.length; i++) {
    var cell = Functions.createElement({
      type: "div",
      style: "display: table-cell;"
    });
    cell.appendChild(elements[i]);
    row.appendChild(cell);
  }
  return row;
};

/**
 *
 * @private
 */
LoginDialog.prototype._createLoginTab = function () {
  var self = this;

  var authorizationFormTab = Functions.createElement({
    type: "div",
    style: "width:100%;display: table;border-spacing: 10px;"
  });
  this.getElement().appendChild(authorizationFormTab);

  var loginLabel = Functions.createElement({
    type: "div",
    content: "LOGIN:"
  });
  var loginInput = Functions.createElement({
    type: "input",
    inputType: "text",
    style: "width:100%",
    name: "loginText"
  });
  this.setControlElement(PanelControlElementType.USER_TAB_LOGIN_INPUT_TEXT, loginInput);

  authorizationFormTab.appendChild(self.createTableRow([loginLabel, loginInput]));

  var passwordLabel = Functions.createElement({
    type: "div",
    content: "PASSWORD:"
  });
  var passwordInput = Functions.createElement({
    type: "input",
    inputType: "password",
    style: "width:100%",
    name: "passwordText"
  });
  this.setControlElement(PanelControlElementType.USER_TAB_PASSWORD_INPUT_TEXT, passwordInput);

  authorizationFormTab.appendChild(self.createTableRow([passwordLabel, passwordInput]));

  var centerTag = Functions.createElement({
    type: "center"
  });
  this.getElement().appendChild(centerTag);

  var loginButton = Functions.createElement({
    type: "button",
    name: "loginButton",
    content: "LOGIN"
  });
  centerTag.appendChild(loginButton);
  this.setControlElement(PanelControlElementType.USER_TAB_LOGIN_BUTTON, loginButton);
  this.getElement().appendChild(Functions.createElement({type: "br"}));
  this.getElement().appendChild(Functions.createElement({type: "div", className: "oauth-login"}));

  this.getElement().appendChild(Functions.createElement({
    type: "a",
    name: "requestAccount",
    content: "Request an account",
    href: "#"
  }));

  $(loginInput).on('keypress', function (e) {
    if (e.which === 13) {
      $(passwordInput).focus();
    }
  });
  $(passwordInput).on('keypress', function (e) {
    if (e.which === 13) {
      $(loginButton).click();
    }
  });

  this.getElement().appendChild(Functions.createElement({
    type: "a",
    href: "javascript:;",
    className: "signup_button adminLink",
    content: '<i class="fa fa-chevron-right"></i> SIGN UP</a>',
    xss: false
  }));

  $(".signup_button", this.getElement()).on('click', function () {
    var content = $('<div/>').html("<div>" +
      "<div class='minerva-menu-row'>" +
      "<label style='margin-right: 20px; width: 120px'>Email:</label>" +
      "<input class='minerva-email'/>" +
      "</div>" +
      "<div class='minerva-menu-row'>" +
      "<label style='margin-right: 20px; width: 120px'>Password:</label>" +
      "<input class='minerva-password' type='password'/>" +
      "</div>" +
      "<div class='minerva-menu-row'>" +
      "<label style='margin-right: 20px; width: 120px'>Name:</label>" +
      "<input class='minerva-name'/>" +
      "</div>" +
      "<div class='minerva-menu-row'>" +
      "<label style='margin-right: 20px; width: 120px'>Surname:</label>" +
      "<input class='minerva-surname'/>" +
      "</div>" +
      "<div class='minerva-menu-row'>" +
      "<button class='minerva-send-button'><span class='ui-icon ui-icon-mail-closed'></span>&nbsp;SIGNUP</button>" +
      "<button class='minerva-cancel-button'><span class='ui-icon ui-icon-cancel'></span>&nbsp;CANCEL</button>" +
      "</div>" +
      "</div>");
    $(content).dialog({
      dialogClass: 'minerva-sign-up-dialog',
      title: "Sign up",
      width: 400,
      height: 200,
      modal: true,
      close: function () {
        $(content).dialog("destroy");
      }
    });


    $(".minerva-send-button", content).on("click", function () {
      GuiConnector.showProcessing();
      ServerConnector.signUp({
        "email": $('.minerva-email', content).val(),
        "name": $('.minerva-name', content).val(),
        "surname": $('.minerva-surname', content).val(),
        "password": $('.minerva-password', content).val()
      }).then(function () {
        GuiConnector.info("We have sent a confirmation email to the provided address.");
        $(content).dialog("destroy");
      }).catch(function (error) {
        if (error instanceof NetworkError && error.statusCode === HttpStatus.BAD_REQUEST) {
          var message = error.content;
          if (error.content !== undefined) {
            message = error.content.reason;
          }
          GuiConnector.showErrorDialog("Error", message);
        } else {
          GuiConnector.alert(error);
        }
      }).finally(function () {
        GuiConnector.hideProcessing();
      })
    });
    $(".minerva-cancel-button", content).on("click", function () {
      $(content).dialog("destroy");
    });
  });
};

/**
 *
 * @returns {Promise}
 */
LoginDialog.prototype.init = function () {

  var self = this;
  var configuration;
  return ServerConnector.getConfiguration().then(function (result) {
    configuration = result;
    var email = configuration.getOption(ConfigurationType.REQUEST_ACCOUNT_EMAIL).getValue();
    var content = encodeURIComponent(configuration.getOption(ConfigurationType.REQUEST_ACCOUNT_DEFAULT_CONTENT).getValue());
    var title = configuration.getOption(ConfigurationType.REQUEST_ACCOUNT_DEFAULT_TITLE).getValue();
    var url = 'mailto:' + email + '?subject=' + title + '&body=' + content;
    var link = $("[name=requestAccount]", self.getElement());
    link.attr("href", url);

    if (configuration.getOption(ConfigurationType.REQUEST_ACCOUNT_EMAIL).getValue() === "" ||
      configuration.getOption(ConfigurationType.REQUEST_ACCOUNT_EMAIL).getValue() === undefined ||
      configuration.getOption(ConfigurationType.ALLOW_AUTO_REGISTER).getValue().toLowerCase() === "true"
    ) {
      $("[name=requestAccount]", self.getElement()).hide();
    }
    if (configuration.getOption(ConfigurationType.ALLOW_AUTO_REGISTER).getValue().toLowerCase() !== "true") {
      $(".signup_button", self.getElement()).hide();
    }

    return ServerConnector.getOauthProviders();
  }).then(function (oauthProviders) {
    var guiUtils = new GuiUtils(configuration);
    for (var key in oauthProviders) {
      if (oauthProviders.hasOwnProperty(key)) {
        var p = key.toLowerCase();
        /**
         * @var {string} r
         */
        var r = oauthProviders[key];

        (function (provider, redirect) {
          var div = $("<div style='align-items: center;justify-content: center;display: flex;'>" +
            "<button class='minerva-oauth-button oauth-" + provider + "' id='oauth-" + provider + "'>" +
            "<i class='minerva-oauth-button oauth-" + provider + "'/>" +
            " Sign in with " + key + "</button>" +
            "</div>");
          $(".minerva-oauth-button", div).on("click", function () {
            if (redirect.startsWith("/")) {
              redirect = redirect.substring(1);
            }
            window.location.href = ServerConnector.getServerBaseUrl() + redirect;
          });
          $(".oauth-login", self.getElement())[0].appendChild(div[0]);

        })(p, r);
      }
    }
  });

};

/**
 *
 */
LoginDialog.prototype.open = function () {
  var self = this;
  var div = self.getElement();
  if (!$(div).hasClass("ui-dialog-content")) {
    $(div).dialog({
      dialogClass: 'minerva-login-dialog',
      autoOpen: false,
      resizable: false
    });
  }

  $(div).dialog('option', 'title', 'AUTHORIZATION FORM');
  $(div).dialog("open");
};

/**
 *
 */
LoginDialog.prototype.destroy = function () {
  var self = this;
  if ($(self.getElement()).hasClass("ui-dialog-content")) {
    $(self.getElement()).dialog("destroy");
  }
};

module.exports = LoginDialog;
