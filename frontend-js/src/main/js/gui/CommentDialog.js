"use strict";

var $ = require('jquery');

/* exported logger */

var Promise = require("bluebird");

var AbstractGuiElement = require('./AbstractGuiElement');
var Alias = require('../map/data/Alias');
var Reaction = require('../map/data/Reaction');
// noinspection JSUnusedLocalSymbols
var logger = require('../logger');
var Functions = require('../Functions');
var GuiConnector = require('../GuiConnector');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} params.customMap
 * @param {Configuration} params.configuration
 * @param {Project} params.project
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 *
 * @extends AbstractGuiElement
 */
function CommentDialog(params) {
  AbstractGuiElement.call(this, params);
  var self = this;
  self._createGui();
  $(self.getElement()).dialog({
    dialogClass: 'minerva-comment-dialog',
    title: "Add comment",
    autoOpen: false,
    resizable: false,
    width: window.innerWidth / 2,
    height: window.innerHeight / 2
  });
}

CommentDialog.prototype = Object.create(AbstractGuiElement.prototype);
CommentDialog.prototype.constructor = CommentDialog;

CommentDialog.GENERAL = "<General>";

/**
 *
 * @param {HTMLElement} elements
 * @returns {HTMLTableRowElement}
 */
function createRow(elements) {
  var row = document.createElement('tr');
  for (var i = 0; i < elements.length; i++) {
    var container = document.createElement('td');
    container.appendChild(elements[i]);
    row.appendChild(container);
  }
  return row;
}

/**
 *
 * @param {IdentifiedElement[]} identifiedElements
 * @returns {Promise}
 */
CommentDialog.prototype.open = function (identifiedElements) {
  var self = this;
  self.setTypes([CommentDialog.GENERAL]);

  var promises = [CommentDialog.GENERAL];
  for (var i = 0; i < identifiedElements.length; i++) {
    var ie = identifiedElements[i];
    if (ie.getType() === "ALIAS") {
      promises.push(self.getMap().getSubmapById(ie.getModelId()).getModel().getAliasById(ie.getId(), true));
    } else if (ie.getType() === "REACTION") {
      promises.push(self.getMap().getSubmapById(ie.getModelId()).getModel().getReactionById(ie.getId(), true));
    } else {
      throw new Error("Unknown element type: " + ie.getType());
    }
  }
  return Promise.all(promises).then(function (elements) {
    self.setTypes(elements);
    return self.getMap().getServerConnector().getLoggedUser();
  }).then(function (user) {
    if (user.getLogin() !== "anonymous") {
      self.setEmail(user.getEmail());
    }
    var dialog = $(self.getElement());
    dialog.find('textarea').val('');
    dialog.dialog("open");
  });
};

/**
 *
 * @private
 */
CommentDialog.prototype._createGui = function () {
  var self = this;

  var table = document.createElement('table');

  var typeLabel = document.createElement('label');
  typeLabel.innerHTML = "Type";
  var typeOptions = document.createElement("select");
  this.setTypeOptions(typeOptions);

  table.appendChild(createRow([typeLabel, typeOptions]));

  var detailDiv = document.createElement('div');

  table.appendChild(createRow([document.createElement('div'), detailDiv]));

  var pinnedLabel = document.createElement('label');
  pinnedLabel.innerHTML = "Pinned";
  var pinnedCheckbox = document.createElement('input');
  pinnedCheckbox.type = "checkbox";
  $(pinnedCheckbox).prop("checked", true);


  table.appendChild(createRow([pinnedLabel, pinnedCheckbox]));
  this.setPinnedCheckbox(pinnedCheckbox);

  var emailLabel = document.createElement('label');
  emailLabel.innerHTML = "Email:<br/>(Visible to moderators only)";
  var emailInput = document.createElement('input');
  emailInput.type = "text";
  emailInput.maxLength = "255";

  table.appendChild(createRow([emailLabel, emailInput]));
  this.setEmailInput(emailInput);

  var contentLabel = document.createElement('label');
  contentLabel.innerHTML = "Content:";
  var contentInput = document.createElement('textarea');
  contentInput.cols = 80;
  contentInput.rows = 3;

  table.appendChild(createRow([contentLabel, contentInput]));
  this.setContentInput(contentInput);

  var sendButton = document.createElement('button');
  sendButton.innerHTML = "Send";
  sendButton.onclick = function () {
    return self.addComment().then(function () {
      $(self.getElement()).dialog("close");
    });
  };

  table.appendChild(createRow([sendButton]));

  self.getElement().appendChild(table);

  typeOptions.onchange = function () {
    var option = self.getSelectedType();
    var text = "";
    if (option instanceof Alias) {
      if (option.getFullName() !== undefined) {
        text = option.getFullName();
      }
    } else if (option instanceof Reaction) {
      text = "Reactants: ";
      var reactants = option.getReactants();
      var i;
      for (i = 0; i < reactants.length; i++) {
        text += reactants[i].getAlias().getName() + ",";
      }
      text += "<br/>";
      text += "Modifiers: ";
      var modifiers = option.getModifiers();
      for (i = 0; i < modifiers.length; i++) {
        text += modifiers[i].getAlias().getName() + ",";
      }
      text += "<br/>";
      text += "Products: ";
      var products = option.getProducts();
      for (i = 0; i < products.length; i++) {
        text += products[i].getAlias().getName() + ",";
      }
      text += "<br/>";
    }
    detailDiv.innerHTML = text;
  };
};

/**
 *
 * @param {Array<BioEntity|string>} types
 */
CommentDialog.prototype.setTypes = function (types) {
  var typeOptions = this.getTypeOptions();
  while (typeOptions.firstChild) {
    typeOptions.removeChild(typeOptions.firstChild);
  }

  for (var i = 0; i < types.length; i++) {
    var option = document.createElement("option");
    option.value = i.toString();
    var element = types[i];
    var text = element;
    if (element instanceof Alias) {
      text = element.getType() + ": " + element.getName();
    } else if (element instanceof Reaction) {
      text = "Reaction: " + element.getReactionId();
    }
    option.text = text;
    typeOptions.appendChild(option);
  }
  typeOptions.value = 0;

  this._types = types;
  typeOptions.onchange();
};

/**
 *
 * @returns {Array<BioEntity|string>}
 */
CommentDialog.prototype.getTypes = function () {
  return this._types;
};

/**
 *
 * @returns {BioEntity|string}
 */
CommentDialog.prototype.getSelectedType = function () {
  return this._types[this.getTypeOptions().value];
};

/**
 *
 * @param {number} value
 */
CommentDialog.prototype.setSelectedType = function (value) {
  if (Functions.isInt(value)) {
    this.getTypeOptions().value = value;
    this.getTypeOptions().onchange();
  } else {
    throw new Error("Unknown value type: " + value);
  }
};

/**
 *
 * @returns {HTMLElement}
 */
CommentDialog.prototype.getTypeOptions = function () {
  return this._typeOptions;
};

/**
 *
 * @param {HTMLElement} typeOptions
 */
CommentDialog.prototype.setTypeOptions = function (typeOptions) {
  this._typeOptions = typeOptions;
};

/**
 *
 * @param {HTMLElement} contentInput
 */
CommentDialog.prototype.setContentInput = function (contentInput) {
  this._contentInput = contentInput;
};

/**
 *
 * @returns {HTMLElement}
 */
CommentDialog.prototype.getContentInput = function () {
  return this._contentInput;
};

/**
 *
 * @param {HTMLElement} emailInput
 */
CommentDialog.prototype.setEmailInput = function (emailInput) {
  this._emailInput = emailInput;
};

/**
 *
 * @returns {HTMLElement}
 */
CommentDialog.prototype.getEmailInput = function () {
  return this._emailInput;
};

/**
 *
 * @param {HTMLElement} pinnedCheckbox
 */
CommentDialog.prototype.setPinnedCheckbox = function (pinnedCheckbox) {
  this._pinnedCheckbox = pinnedCheckbox;
};

/**
 *
 * @returns {HTMLElement}
 */
CommentDialog.prototype.getPinnedCheckbox = function () {
  return this._pinnedCheckbox;
};

/**
 *
 * @returns {string}
 */
CommentDialog.prototype.getEmail = function () {
  return this.getEmailInput().value;
};

/**
 *
 * @param {string} email
 */
CommentDialog.prototype.setEmail = function (email) {
  this.getEmailInput().value = email;
};

/**
 *
 * @returns {string}
 */
CommentDialog.prototype.getContent = function () {
  return this.getContentInput().value;
};

/**
 *
 * @returns {boolean}
 */
CommentDialog.prototype.isPinned = function () {
  return this.getPinnedCheckbox().checked;
};

/**
 *
 * @returns {number | string}
 */
CommentDialog.prototype.getSelectedTypeId = function () {
  var selected = this.getSelectedType();
  if (selected instanceof Alias) {
    return selected.getId();
  } else if (selected instanceof Reaction) {
    return selected.getId();
  } else {
    return "";
  }
};

/**
 *
 * @returns {string}
 */
CommentDialog.prototype.getSelectedTypeClass = function () {
  var selected = this.getSelectedType();
  if (selected instanceof Alias) {
    return "ALIAS";
  } else if (selected instanceof Reaction) {
    return "REACTION";
  } else {
    return "POINT";
  }
};

/**
 *
 * @returns {Promise}
 */
CommentDialog.prototype.addComment = function () {
  var self = this;
  return self.getMap().getServerConnector().addComment({
    modelId: self.getMap().getActiveSubmapId(),
    coordinates: self.getMap().getActiveSubmapClickCoordinates(),
    email: self.getEmail(),
    content: self.getContent(),
    pinned: self.isPinned(),
    elementId: self.getSelectedTypeId(),
    elementType: self.getSelectedTypeClass()

  }).then(function (comment) {
    //if we visualize comments add it to data set
    if (self.getMap().getServerConnector().getSessionData(self.getProject()).getShowComments() && comment.isPinned()) {
      return self.getMap().getOverlayByName("comment").addComment(comment);
    } else {
      return [];
    }
  }).catch(GuiConnector.alert);
};

/**
 *
 */
CommentDialog.prototype.destroy = function () {
  $(this.getElement()).dialog("destroy");
};

module.exports = CommentDialog;
