"use strict";

/* exported logger */
var $ = require('jquery');

var GuiConnector = require('../GuiConnector');
var AbstractGuiElement = require('./AbstractGuiElement');
var GuiUtils = require('./leftPanel/GuiUtils');
var PanelControlElementType = require('./PanelControlElementType');
var Functions = require('../Functions');

var logger = require('../logger');
var xss = require('xss');

/**
 *
 * @param {Configuration} [params.configuration]
 * @param {HTMLElement} params.element
 * @param {Project} params.project
 * @param {CustomMap} [params.customMap]
 * @param {string} params.panelName
 * @param {boolean} params.scrollable
 * @param {string} [params.helpTip]
 * @param params.parent
 *
 * @constructor
 * @extends AbstractGuiElement
 */

function Panel(params) {
  AbstractGuiElement.call(this, params);

  var self = this;

  self.setParent(params.parent);
  var configuration = params.configuration;
  if (params.configuration === undefined) {
    configuration = self.getMap().getConfiguration();
  }
  var guiUtils = new GuiUtils(configuration);
  self.setGuiUtils(guiUtils);
  if (self.getMap() !== undefined) {
    this.getGuiUtils().setMap(self.getMap());
  }
  self.setPanelName(params.panelName);

  var element = $(self.getElement());
  element.addClass("minerva-panel");
  if (params.scrollable) {
    element.addClass("pre-scrollable");
  } else {
    element.css("overflow-y", "auto");
  }

  element.css("position", "relative");
  if (params.helpTip !== undefined) {
    self.setHelpTip(params.helpTip);
  }

  GuiConnector.addWindowResizeEvent(function () {
    self.onresize();
  });

  $("a[href='#" + self.getElement().id + "']").on('shown.bs.tab', function () {
    self.onresize();
  });

}

Panel.prototype = Object.create(AbstractGuiElement.prototype);
Panel.prototype.constructor = Panel;

/**
 *
 */
Panel.prototype.createHelpButton = function () {
  var self = this;
  var helpTipButton = self.getGuiUtils().createHelpButton(self.getHelpTip());
  self.getElement().appendChild(helpTipButton);
};

/**
 *
 * @param {string} message
 */
Panel.prototype.disablePanel = function (message) {
  var self = this;

  $(self.getElement()).children().css("visibility", "hidden");
  $("[class='minerva-help-button']", self.getElement()).children().css("visibility", "visible");
  var hideReasonDiv = document.createElement("div");
  hideReasonDiv.className = "searchPanel";

  var center = document.createElement("center");
  var messageDiv = document.createElement("h4");
  messageDiv.innerHTML = message;
  center.appendChild(messageDiv);
  hideReasonDiv.appendChild(center);

  $(self.getElement()).prepend(hideReasonDiv);
};

/**
 *
 * @returns {boolean}
 */
Panel.prototype.isDisabled = function () {
  var self = this;
  var searchQueryElement = self.getControlElement(PanelControlElementType.SEARCH_DIV);
  return searchQueryElement.style.visibility === "hidden";
};

/**
 *
 * @param {CustomMap} map
 */
Panel.prototype.setMap = function (map) {
  this._map = map;
};

/**
 *
 * @param {string} panelName
 */
Panel.prototype.setPanelName = function (panelName) {
  this._panelName = panelName;
};

/**
 *
 * @returns {string}
 */
Panel.prototype.getPanelName = function () {
  return this._panelName;
};

/**
 *
 * @param {HTMLElement} element
 */
Panel.prototype.setElement = function (element) {
  if (element === undefined || element === null) {
    throw new Error("DOM Element must be defined");
  }
  this._element = element;
};

/**
 *
 * @returns {HTMLElement}
 */
Panel.prototype.getElement = function () {
  return this._element;
};

/**
 *
 * @param {HTMLElement} element
 * @param {string} name
 * @returns {HTMLElement}
 */
Panel.prototype.getElementByName = function (element, name) {
  if (element !== undefined) {
    if (element.getAttribute("name") === name) {
      return element;
    }
    var children = element.children;
    for (var i = 0; i < children.length; i++) {
      var child = children[i];
      var res = this.getElementByName(child, name);
      if (res !== undefined) {
        return res;
      }
    }
  }
  return undefined;
};

/**
 *
 * @param {string} id
 * @returns {HTMLElement}
 */
Panel.prototype.getDialogDiv = function (id) {
  var dialogs = this.getElementByName(this.getElement(), "dialogs");
  if (dialogs === undefined) {
    dialogs = document.createElement("div");
    dialogs.setAttribute("name", "dialogs");
    this.getElement().appendChild(dialogs);

    this._dialogs = [];
  }

  var dialogDiv = this._dialogs[id];

  if (dialogDiv === undefined) {
    dialogDiv = document.createElement("div");
    dialogDiv.className = "ui-widget";
    dialogDiv.setAttribute("name", "dialog-" + id);

    var contentDiv = document.createElement("div");
    contentDiv.setAttribute("name", "content");
    dialogDiv.appendChild(contentDiv);

    dialogs.appendChild(dialogDiv);

    this._dialogs[id] = dialogDiv;
  }
  return dialogDiv;
};

/**
 *
 * @param {HTMLElement} div
 * @param params
 */
Panel.prototype.assignDialogOptions = function (div, params) {
  var dialog = $(div);
  for (var key in params) {
    if (params.hasOwnProperty(key)) {
      if (key === "id") {
        div.setAttribute("name", "dialog-" + params[key]);
      } else if (key === "modal") {
        dialog.dialog('option', 'modal', params[key]);
      } else if (key === "buttons") {
        dialog.dialog('option', 'buttons', params[key]);
      } else if (key === "className") {
        dialog.dialog('option', 'dialogClass', params[key]);
      } else if (key === "title") {
        dialog.dialog('option', 'title', params[key]);
      } else if (key === "width") {
        dialog.dialog('option', 'width', params[key]);
      } else {
        throw new Error("Unknown dialog param: " + key + " - " + params[key]);
      }
    }
  }
};

/**
 *
 * @param {HTMLElement} content
 * @param options
 */
Panel.prototype.openDialog = function (content, options) {
  if (options === undefined) {
    options = {};
  }

  if (options.id === undefined) {
    logger.warn("Id of dialog is not defined");
  }

  var div = this.getDialogDiv(options.id);

  var contentDiv = this.getElementByName(div, "content");
  while (contentDiv.hasChildNodes()) {
    contentDiv.removeChild(contentDiv.lastChild);
  }
  contentDiv.appendChild(content);
  contentDiv.style.display = "block";

  $(div).dialog({
    close: function () {
      contentDiv.style.display = "none";
      $(this).dialog('destroy');
    },
    dialogClass: options.className
  });

  this.assignDialogOptions(div, options);

  $(div).dialog("open");
};

/**
 *
 */
Panel.prototype.init = function () {
  throw new Error(this.getPanelName() + " Not implemented");
};

/**
 *
 * @param {AbstractGuiElement} parent
 */
Panel.prototype.setParent = function (parent) {
  this._parent = parent;
};

/**
 *
 * @returns {AbstractGuiElement}
 */
Panel.prototype.getParent = function () {
  return this._parent;
};

/**
 *
 * @param {GuiUtils} guiUtils
 */
Panel.prototype.setGuiUtils = function (guiUtils) {
  this._guiUtils = guiUtils;
};

/**
 *
 * @returns {GuiUtils}
 */
Panel.prototype.getGuiUtils = function () {
  return this._guiUtils;
};

/**
 *
 * @param {string} helpTip
 */
Panel.prototype.setHelpTip = function (helpTip) {
  if (this._helpTip === undefined && helpTip !== undefined) {
    this._helpTip = helpTip;
    this.createHelpButton();
  }
};

/**
 *
 * @returns {string}
 */
Panel.prototype.getHelpTip = function () {
  return this._helpTip;
};

/**
 *
 */
Panel.prototype.onresize = function () {
  var self = this;
  var footerPosition = window.innerHeight;

  var footer =$(".minerva-logo-footer, .minerva-footer-table")
  if (footer.length > 0) {
    footerPosition -= Math.max(0, footer.outerHeight());
  }

  // compute the width (we can only compute it for visible elements)
  var size = 100000;

  if ($(self.getElement()).is(":visible")) {

    $(".pre-scrollable", self.getElement()).each(function (index, element) {
      if ($(element).is(":visible")) {
        size = Math.min(size, footerPosition - $(element).offset().top);
      }
    });
    if ($(self.getElement()).hasClass("pre-scrollable") && $(self.getElement()).is(":visible")) {
      size = Math.min(size, footerPosition - $(self.getElement()).offset().top);
    }
    if (size !== 100000) {
      $(".pre-scrollable", self.getElement()).each(function (index, element) {
        $(element).css('max-height', size);
        $(element).css('height', size);
      });
    }
    if ($(self.getElement()).hasClass("pre-scrollable") && $(self.getElement()).is(":visible")) {
      $(self.getElement()).css('max-height', size);
      $(self.getElement()).css('height', size);
    }
  }

};

/**
 *
 */
Panel.prototype.destroy = function () {
  for (var id in this._dialogs) {
    if (this._dialogs.hasOwnProperty(id)) {
      var div = this._dialogs[id];
      if ($(div).hasClass("ui-dialog-content")) {
        $(div).dialog("destroy");
      }
    }
  }
};

module.exports = Panel;
