"use strict";

var $ = require('jquery');

/* exported logger */

var AbstractGuiElement = require('./AbstractGuiElement');
var GuiConnector = require('../GuiConnector');
var SubMenu = require('./SubMenu');
var WarningListDialog = require('./WarningListDialog');

var Functions = require('../Functions');

var logger = require('../logger');

var MARGIN = 5;

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} params.customMap
 * @param {Configuration} params.configuration
 * @param {Project} params.project
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 *
 * @extends AbstractGuiElement
 */
function ContextMenu(params) {
  AbstractGuiElement.call(this, params);
  var self = this;

  var element = self.getElement();
  element.className += " dropdown-menu";
  element.setAttribute("role", "menu");
  $(element).hide();

  self._handledTimeStamp = undefined;

  self.MIN_SHOW_TIME = 250;

  self._documentClickListener = function (e) {
    var className = e.target.className;
    if (typeof className === 'string' || className instanceof String) {
      if (className.indexOf("dropdown-item") === -1) {
        self.hide(new Date().getTime() - self.MIN_SHOW_TIME);
      }
    } else {
      self.hide(new Date().getTime() - self.MIN_SHOW_TIME);
    }
  };
  $(document).on('click', self._documentClickListener);
  $(document).on('touchstart', self._documentClickListener);
}

ContextMenu.prototype = Object.create(AbstractGuiElement.prototype);
ContextMenu.prototype.constructor = ContextMenu;

/**
 *
 */
ContextMenu.prototype.destroy = function () {
  var self = this;
  $(document).off('click', self._documentClickListener);
  $(document).off('touchstart', self._documentClickListener);
};

/**
 *
 * @param {Object} params
 * @param {string} [params.name]
 * @param {SubMenu} [params.submenu]
 * @param {function} [params.handler]
 * @param {boolean} [params.disabled]
 * @param {number} [params.position]
 */
ContextMenu.prototype.addOption = function (params) {
  if (!params.disabled) {
    params.disabled = false;
  }
  var self = this, option;
  if (params.submenu instanceof SubMenu) {
    option = params.submenu.getElement();
  } else {
    option = Functions.createElement({
      type: "li"
    });
    var link = Functions.createElement({
      type: "a",
      className: "dropdown-item",
      href: "#",
      content: params.name
    });
    if (params.disabled) {
      link.className = 'dropdown-item disabled';
    }
    $(link).data("handler", params.handler);
    option.appendChild(link);
  }
  if (Functions.isInt(params.position)) {
    var child = self.getElement().childNodes[params.position];
    if (child !== null && child !== undefined) {
      self.getElement().insertBefore(option, child);
    } else {
      self.getElement().appendChild(option);
    }
  } else {
    self.getElement().appendChild(option);
  }
  return link;
};

/**
 *
 * @param {number} x
 * @param {number} y
 * @param {number} timestamp
 */
ContextMenu.prototype.open = function (x, y, timestamp) {
  var self = this;
  self._handledTimeStamp = timestamp;

  var left = x;
  var right = "";

  if ($(self.getElement()).width() + x + MARGIN > $(document).width()) {
    left = Math.max(0, $(document).width() - $(self.getElement()).width() - MARGIN);
    right = 0;
  }

  $(self.getElement()).show().css({
    position: "absolute",
    left: left,
    right: right,
    top: y
  }).off('click').on('click', 'a', function () {
    //close if this is not a submenu
    if (this.parentNode === undefined || this.parentNode.className.indexOf("dropdown-submenu") === -1) {
      self.hide(new Date().getTime());
      if ($(this).data("handler") !== undefined) {
        return $(this).data("handler")();
      } else {
        logger.debug("Nothing to do");
      }
    }
  });
};

/**
 *
 * @param {number} timestamp
 */
ContextMenu.prototype.hide = function (timestamp) {
  var self = this;
  if (self._handledTimeStamp < timestamp) {
    self._handledTimeStamp = timestamp;
    $(this.getElement()).hide();
  }
};

/**
 *
 * @param {DataOverlay[]} dataOverlays
 * @returns {number[]}
 */
function extractDataOverlayIds(dataOverlays) {
  var ids = [];
  for (var i = 0; i < dataOverlays.length; i++) {
    ids.push(dataOverlays[i].getId());
  }
  return ids;
}

/**
 *
 * @returns {Promise}
 */
ContextMenu.prototype.createExportAsImageSubmenu = function () {
  var self = this;
  return self.getMap().getServerConnector().getImageConverters().then(function (converters) {
    var li = Functions.createElement({
      type: "li"
    });
    var submenu = new SubMenu({
      element: li,
      name: "&nbsp;&nbsp;&nbsp;&nbsp;as image",
      customMap: self.getMap()
    });

    var map = self.getMap();
    converters.forEach(function (converter) {
      submenu.addOption({
        name: converter.name,
        handler: function () {
          return map.getVisibleDataOverlays().then(function (visibleDataOverlays) {
            var submapId = map.getActiveSubmapId();

            return map.getServerConnector().getImageDownloadUrl({
              polygonString: map.getSelectedPolygon(),
              modelId: submapId,
              handlerClass: converter.handler,
              backgroundOverlayId: map.getBackgroundDataOverlay().getId(),
              zoomLevel: map.getSubmapById(submapId).getZoom(),
              overlayIds: extractDataOverlayIds(visibleDataOverlays)
            });
          }).then(function (url) {
            return self.downloadFile(url);
          }).catch(GuiConnector.alert);
        }
      });
    });
    return submenu;
  });
};

/**
 *
 * @returns {Promise}
 */
ContextMenu.prototype.createExportAsModelSubmenu = function () {
  var self = this;
  return self.getMap().getServerConnector().getModelConverters().then(function (converters) {
    var li = Functions.createElement({
      type: "li"
    });
    var submenu = new SubMenu({
      element: li,
      name: "&nbsp;&nbsp;&nbsp;&nbsp;as map",
      customMap: self.getMap()
    });

    var map = self.getMap();
    converters.forEach(function (converter) {
      submenu.addOption({
        name: converter.name,
        handler: function () {
          var params;
          return map.getVisibleDataOverlays().then(function (visibleDataOverlays) {
            params = {
              polygonString: map.getSelectedPolygon(),
              modelId: map.getActiveSubmapId(),
              handlerClass: converter.handler,
              backgroundOverlayId: map.getBackgroundDataOverlay().getId(),
              zoomLevel: map.getZoom(),
              overlayIds: extractDataOverlayIds(visibleDataOverlays)
            };
            return map.getServerConnector().getModelDownloadUrl(params);
          }).then(function (url) {
            return self.downloadFile(url);
          }).then(function () {
            return self.getServerConnector().getModelDownloadWarnings(params);
          }).then(function (warnings) {
            if (warnings.length > 0) {
              var dialog = new WarningListDialog({
                element: Functions.createElement({type: "div"}),
                warnings: warnings,
                configuration: self.getConfiguration(),
                project: self.getProject(),
                serverConnector: self.getServerConnector()
              });
              dialog.open();
            }
          }).catch(GuiConnector.alert);
        }
      });
    });
    return submenu;
  });
};

module.exports = ContextMenu;
