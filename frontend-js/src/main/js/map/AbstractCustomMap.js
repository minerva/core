"use strict";

var $ = require('jquery');
var Promise = require("bluebird");

var logger = require('../logger');

var Alias = require('./data/Alias');
var AliasInfoWindow = require('./window/AliasInfoWindow');
var AliasSurface = require('./surface/AliasSurface');
var GuiConnector = require('../GuiConnector');
var IdentifiedElement = require('./data/IdentifiedElement');
var LayoutAlias = require('./data/LayoutAlias');
var ObjectWithListeners = require('../ObjectWithListeners');
var MapModel = require('./data/MapModel');
var Point = require('./canvas/Point');
var PointData = require('./data/PointData');
var PointInfoWindow = require('./window/PointInfoWindow');
var Reaction = require('./data/Reaction');
var ReactionInfoWindow = require('./window/ReactionInfoWindow');
var ReactionSurface = require('./surface/ReactionSurface');

var MarkerSurfaceCollection = require('./marker/MarkerSurfaceCollection');

var MapCanvas = require('./canvas/MapCanvas');
var OpenLayerCanvas = require('./canvas/OpenLayers/OpenLayerCanvas');

/**
 * Default constructor.
 *
 * @param {MapModel} model
 * @param {CustomMapOptions} options
 * @constructor
 * @extends ObjectWithListeners
 */
function AbstractCustomMap(model, options) {
  // call super constructor
  ObjectWithListeners.call(this);

  if (model === undefined) {
    throw Error("Model must be defined");
  }
  this.registerListenerType("onZoomChanged");
  this.registerListenerType("onCenterChanged");

  this.setElement(options.getElement());
  this.setConfiguration(options.getConfiguration());
  this.setProject(options.getProject());
  this.setServerConnector(options.getServerConnector());

  this.setModel(model);

  /**
   * this array contains elements that are presented on a specific overlay (set
   * of map object representing lines/areas that are associated with
   * overlay)
   * @type {Array<AbstractSurfaceElement[]>}
   */
  this.selectedLayoutOverlays = [];

  /**
   *
   * @type {AliasInfoWindow[]}
   * @private
   */
  this._aliasInfoWindow = [];

  /**
   *
   * @type {PointInfoWindow[]}
   * @private
   */
  this._pointInfoWindow = [];

  /**
   *
   * @type {ReactionInfoWindow[]}
   * @private
   */
  this._reactionInfoWindow = [];

  /**
   *
   * @type {MarkerSurfaceCollection}
   * @private
   */
  this._markerSurfaceCollection = new MarkerSurfaceCollection({map: this});

  /**
   *
   * @type {boolean}
   * @private
   */
  this._bigLogo = options.isBigLogo();

  this.setDebug(options.isDebug());
}

// define super constructor
AbstractCustomMap.prototype = Object.create(ObjectWithListeners.prototype);
AbstractCustomMap.prototype.constructor = AbstractCustomMap;


AbstractCustomMap.prototype.getMarkerSurfaceCollection = function () {
  return this._markerSurfaceCollection;
};

/**
 * Creates general map options used in this map.
 *
 * @returns {{center: Point, zoom: number, minZoom: number, maxZoom: number, tileSize: number, width: number, height: number, backgroundOverlays: Array}}
 */
AbstractCustomMap.prototype.createMapOptions = function () {
  var self = this;
  var model = self.getModel();
  var zoom = self.getServerConnector().getSessionData(self.getProject()).getZoomLevel(model);
  if (zoom === undefined) {
    zoom = model.getDefaultZoomLevel();
  }
  if (zoom === undefined || zoom === null) {
    zoom = self.getMinZoom();
  }

  var point = self.getServerConnector().getSessionData(self.getProject()).getCenter(model);
  if (point === undefined) {
    var x = model.getDefaultCenterX();
    var y = model.getDefaultCenterY();
    if (x !== undefined && y !== undefined && x !== null && y !== null) {
      point = new Point(x, y);
    } else {
      point = new Point(model.getWidth() / 2, model.getHeight() / 2);
    }
  }

  var backgrounds = this.getProject().getBackgrounds();
  var backgroundOverlays = [];
  for (var i = 0; i < backgrounds.length; i++) {
    var background = backgrounds[i];
    backgroundOverlays.push({
      directory: "../map_images/" + self.getProject().getDirectory() + "/" + background.getImagesDirectory(self.getId()),
      id: background.getId(),
      name: background.getName()
    });
  }

  return {
    center: point,
    zoom: zoom,
    minZoom: self.getMinZoom(),
    maxZoom: self.getMaxZoom(),
    tileSize: self.getTileSize(),
    width: self.getModel().getWidth(),
    height: self.getModel().getHeight(),
    backgroundOverlays: backgroundOverlays
  };
};

/**
 * Register events responsible for click events
 */
AbstractCustomMap.prototype.registerMapClickEvents = function () {

  // find top map (CustomMap)

  var customMap = this.getTopMap();

  var self = this;

  // search event
  this.getMapCanvas().addListener('click', function (event) {
    var searchDb = customMap.getOverlayByName('search');
    if (searchDb !== undefined) {
      return searchDb.searchByCoordinates({
        modelId: self.getModel().getId(),
        coordinates: event.arg.point,
        zoom: self.getZoom()
      }).catch(GuiConnector.alert);
    } else {
      logger.warn("Search is impossible because search db is not present");
    }
  });

  // select last clicked map
  this.getMapCanvas().addListener('click', function (event) {
    customMap.setActiveSubmapId(self.getId());
    customMap.setActiveSubmapClickCoordinates(event.arg.point);
  });

  // select last clicked map
  this.getMapCanvas().addListener('rightclick', function (event) {
    customMap.setActiveSubmapId(self.getId());
    customMap.setActiveSubmapClickCoordinates(event.arg.point);
    return activateMolArtLink(event.arg.point, self);
  });

  // prepare for image export
  this.getMapCanvas().addListener('rightclick', function () {
    var bounds = self.getBounds();

    var topLeft = bounds.getTopLeft();
    var rightBottom = bounds.getRightBottom();

    var x1 = Math.max(0, topLeft.x);
    var y1 = Math.max(0, topLeft.y);

    var x2 = Math.min(self.getModel().getWidth(), rightBottom.x);
    var y2 = Math.min(self.getModel().getHeight(), rightBottom.y);

    var polygon = "";
    polygon += x1 + "," + y1 + ";";
    polygon += x2 + "," + y1 + ";";
    polygon += x2 + "," + y2 + ";";
    polygon += x1 + "," + y2 + ";";
    self.getTopMap().setSelectedPolygon(polygon);
  });

  // context menu event
  this.getMapCanvas().addListener('rightclick', function (e) {
    return self.getTopMap().getContextMenu().open(e.arg.client.x, e.arg.client.y, new Date().getTime());
  });


  //long click should open context menu https://stackoverflow.com/a/38457006/1127920
  var mousedUp = false;
  $(self.getElement()).on("touchstart", function (e) {
    mousedUp = false;
    setTimeout(function () {
      if (mousedUp === false) {
        self.getTopMap().getContextMenu().open(e.touches[0].clientX, e.touches[0].clientY, new Date().getTime() + 250);
      }
    }, 500);
  });
  // noinspection SpellCheckingInspection
  this.getMapCanvas().addListener('mouseup', function () {
    mousedUp = true;
  });
  $(self.getElement()).on("touchstart", function () {
    mousedUp = true;
  });
  // noinspection SpellCheckingInspection
  this.getMapCanvas().addListener('dragstart', function () {
    mousedUp = true;
  });

};

/**
 *
 * @param {Point} coordinates
 * @param {AbstractCustomMap} map
 * @returns {Promise}
 */
function activateMolArtLink(coordinates, map) {

  return map.getServerConnector().getClosestElementsByCoordinates({
    modelId: map.getId(),
    coordinates: coordinates,
    count: 1
  }).then(function (identifiedElements) {
    return map.getTopMap().getDistance({
      modelId: map.getId(),
      coordinates: coordinates,
      element: identifiedElements[0]
    }).then(function (distance) {
      if (distance === 0) return identifiedElements;
      else return Promise.reject(new Error());
    })

  }).then(function (identifiedElements) {
    if (identifiedElements[0].type !== 'ALIAS') return Promise.reject(new Error());
    return map.getModel().getAliasById(identifiedElements[0].id, true)
  }).then(function (element) {
    var uniprotIds = [];
    for (var i = 0; i < element.references.length; i++) {
      var ref = element.references[i];
      if (ref.constructor.name === 'Annotation' && ref.getType() === 'UNIPROT') {
        var uniprotId = ref.getResource();
        if (uniprotIds.indexOf(uniprotId) < 0) {
          uniprotIds.push(ref.getResource());
        }
      }
    }
    if (uniprotIds.length > 0) map.getTopMap().getContextMenu().getMolArt().activateInContextMenu(uniprotIds, element);
    else map.getTopMap().getContextMenu().getMolArt().deactivateInContextMenu();
  }).catch(function () {
    return map.getTopMap().getContextMenu().getMolArt().deactivateInContextMenu();
  });
}

/**
 * Returns top map.
 *
 * @returns {CustomMap}
 */
AbstractCustomMap.prototype.getTopMap = function () {
  throw new Error("Not implemented");
};

/**
 * Method that should be called when number of overlays to visualize changed to
 * modify boundaries of the elements to visualize. When few overlays are
 * visualized at the same time then index contains information where this new
 * overlay is placed in the list (starting from 0) and length contains
 * information how many overlays we visualize in total.
 *
 * @param {number} overlayId
 *          identifier of a overlay
 * @param {number} index
 *          when visualizing more than one overlay at the same time index
 *          contains information at which position in the list this overlay is
 *          placed
 * @param {number} length
 *          number of overlays that are currently visualized
 *
 * @returns {PromiseLike}
 */
AbstractCustomMap.prototype._resizeSelectedDataOverlay = function (overlayId, index, length) {
  var self = this;
  // if map is not initialized then don't perform this operation
  if (!self.isInitialized()) {
    logger.debug("Model " + self.getId() + " not initialized");
    return Promise.resolve();
  }
  logger.debug("Resize overlay: " + overlayId);
  // start ratio
  var startX = index * (1.0 / length);
  // end ratio
  var endX = (index + 1) * (1.0 / length);

  //this is a hacky way of finding information that we have few ReactionOverlay pointing to reaction
  var bioEntityCounter = [];
  var surface, bioEntityKey;

  for (var key in self.selectedLayoutOverlays) {
    if (self.selectedLayoutOverlays.hasOwnProperty(key)) {
      for (var i = 0; i < self.selectedLayoutOverlays[key].length; i++) {
        surface = self.selectedLayoutOverlays[key][i];
        bioEntityKey = surface.getBioEntity().getType() + " - " + surface.getBioEntity().getId();
        if (bioEntityCounter[bioEntityKey] === undefined) {
          bioEntityCounter[bioEntityKey] = 0;
        }
        bioEntityCounter[bioEntityKey]++;
      }
    }
  }

  for (i = 0; i < self.selectedLayoutOverlays[overlayId].length; i++) {
    surface = self.selectedLayoutOverlays[overlayId][i];
    bioEntityKey = surface.getBioEntity().getType() + " - " + surface.getBioEntity().getId();
    if (bioEntityCounter[bioEntityKey] > 1 || surface.getIdentifiedElement().getType() === "ALIAS") {
      self.selectedLayoutOverlays[overlayId][i].setBoundsForAlias(startX, endX);
    } else {
      self.selectedLayoutOverlays[overlayId][i].setBoundsForAlias(0, 1);
    }
  }
  return Promise.resolve();
};

/**
 * Shows all elements from a given overlay. When few overlays are visualized at
 * the same time then index contains information where this new overlay is placed
 * in the list (starting from 0) and length contains information how many
 * overlays we visualize in total.
 *
 * @param {number} overlayId
 *          identifier of a overlay
 * @param {number} index
 *          when visualizing more than one overlay at the same time index
 *          contains information at which position in the list this overlay is
 *          placed
 * @param {number} length
 *          number of overlays that are currently visualized
 * @returns {PromiseLike}
 */
AbstractCustomMap.prototype._showSelectedDataOverlay = function (overlayId, index, length) {
  var self = this;
  // if map is not initialized then don't perform this operation
  if (!self.isInitialized()) {
    logger.debug("Model " + self.getId() + " not initialized");
    return Promise.resolve();
  } else {
    logger.debug("Showing model " + self.getId());
  }

  self.selectedLayoutOverlays[overlayId] = [];

  // start ratio
  var startX = index * (1.0 / length);
  // end ratio
  var endX = (index + 1) * (1.0 / length);

  return self.getProject().getDataOverlayById(overlayId).then(function (overlay) {
    return Promise.all([self._showDataOverlayAliases(overlay, startX, endX),
      self._showDataOverlayReactions(overlay, length === 1)
    ]);
  });
};

/**
 *
 * @param {DataOverlay} overlay
 * @param {number} startX
 * @param {number} endX
 *
 * @returns {Promise|PromiseLike}
 * @private
 */
AbstractCustomMap.prototype._showDataOverlayAliases = function (overlay, startX, endX) {
  var self = this;
  var overlayAliases = overlay.getAliases();
  var overlayAliasesOnMap = [];
  var overlaysForAliasId = [];

  var i, identifiedElements = [];
  var usedAliasIds = [];
  var overlayMapIds = [];
  for (i = 0; i < overlayAliases.length; i++) {
    if (overlayMapIds[overlayAliases[i].getModelId()] === undefined) {
      overlayMapIds[overlayAliases[i].getModelId()] = [];
    }
    overlayMapIds[overlayAliases[i].getModelId()].push(overlayAliases[i]);

    if (overlayAliases[i].getModelId() === self.getId()) {
      identifiedElements.push(new IdentifiedElement(overlayAliases[i]));
      overlayAliasesOnMap.push(overlayAliases[i]);
      usedAliasIds[overlayAliases[i].getId()] = true;
      if (overlaysForAliasId[overlayAliases[i].getId()] === undefined) {
        overlaysForAliasId[overlayAliases[i].getId()] = [];
      }
      overlaysForAliasId[overlayAliases[i].getId()].push(overlayAliases[i]);
    }
  }
  var elementsPointingToSubmapPromises = [];
  for (var mapId in overlayMapIds) {
    elementsPointingToSubmapPromises.push(self.getProject().getElementsPointingToSubmap(parseInt(mapId)));
  }
  return Promise.all(elementsPointingToSubmapPromises).then(function (elementsPointingToSubmap) {
    for (var i = 0; i < elementsPointingToSubmap.length; i++) {
      var row = elementsPointingToSubmap[i];
      for (var j = 0; j < row.length; j++) {
        var identifiedElement = row[j];
        if (identifiedElement.getModelId() === self.getId() && !usedAliasIds[identifiedElement.getId()]) {
          usedAliasIds[identifiedElement.getId()] = true;
          overlayAliasesOnMap.push(new LayoutAlias({
            idObject: identifiedElement.getId(),
            modelId: identifiedElement.getModelId(),
            value: 0
          }));
        }
      }
    }
    return self.getModel().getByIdentifiedElements(identifiedElements, false)
  }).then(function () {
    return Promise.each(overlayAliasesOnMap, function (overlayAlias) {
      return self.getModel().getAliasById(overlayAlias.getId()).then(function (aliasData) {
        var overlayData;
        if (aliasData.getLinkedSubmodelId() !== undefined && overlayMapIds[aliasData.getLinkedSubmodelId()] !== undefined) {
          overlayData = overlayMapIds[aliasData.getLinkedSubmodelId()];
        } else {
          overlayData = overlaysForAliasId[overlayAlias.getId()];
        }
        var surface = new AliasSurface({
          overlayData: overlayData,
          alias: aliasData,
          map: self,
          startX: startX,
          endX: endX,
          onClick: [function () {
            return self.getTopMap().getOverlayByName("search").searchByTarget(new IdentifiedElement(aliasData));
          }, function () {
            return self.getTopMap().callListeners("onBioEntityClick", new IdentifiedElement(aliasData));
          }]
        });
        self.selectedLayoutOverlays[overlay.getId()].push(surface);
        return surface.show();
      });
    });
  });
};

/**
 *
 * @param {DataOverlay} overlay
 * @param {boolean} customized
 *
 * @returns {Promise}
 * @private
 */
AbstractCustomMap.prototype._showDataOverlayReactions = function (overlay, customized) {
  var self = this;
  var overlayReactions = overlay.getReactions();
  var overlayReactionsOnMap = [];

  var i, identifiedElements = [];
  for (i = 0; i < overlayReactions.length; i++) {
    if (overlayReactions[i].getModelId() === self.getId()) {
      identifiedElements.push(new IdentifiedElement(overlayReactions[i]));
      overlayReactionsOnMap.push(overlayReactions[i]);
    }
  }

  var surface, bioEntityKey;
  var bioEntityCounter = [];
  //this is a hacky way of finding information that we have few ReactionOverlay pointing to reaction
  for (var key in self.selectedLayoutOverlays) {
    if (self.selectedLayoutOverlays.hasOwnProperty(key)) {
      for (i = 0; i < self.selectedLayoutOverlays[key].length; i++) {
        surface = self.selectedLayoutOverlays[key][i];
        bioEntityKey = surface.getBioEntity().getType() + " - " + surface.getBioEntity().getId();
        if (bioEntityCounter[bioEntityKey] === undefined) {
          bioEntityCounter[bioEntityKey] = 0;
        }
        bioEntityCounter[bioEntityKey]++;
      }
    }
  }

  return self.getModel().getByIdentifiedElements(identifiedElements, false).then(function () {
    return Promise.each(overlayReactionsOnMap, function (overlayReaction) {
      /**
       * @var ReactionSurface
       */
      var surface;
      var bioEntityKey;
      return self.getModel().getReactionById(overlayReaction.getId()).then(function (reactionData) {
        bioEntityKey = reactionData.getType() + " - " + reactionData.getId();

        surface = new ReactionSurface({
          layoutReaction: overlayReaction,
          reaction: reactionData,
          map: self,
          onClick: [function () {
            return self.getTopMap().getOverlayByName("search").searchByTarget(new IdentifiedElement(reactionData));
          }, function () {
            return self.getTopMap().callListeners("onBioEntityClick", new IdentifiedElement(reactionData));
          }],
          customized: bioEntityCounter[bioEntityKey] <= 0
        });
        self.selectedLayoutOverlays[overlay.getId()].push(surface);
        return surface.show();
      }).then(function () {
        if (bioEntityCounter[bioEntityKey] > 0) {
          surface.changedToDefault();
        }
      });
    });
  });
};

/**
 * Hides all elements from overlay.
 *
 * @param {number} overlayId
 *          identifier of a overlay
 *
 * @returns {Promise}
 */
AbstractCustomMap.prototype._hideSelectedLayout = function (overlayId) {
  // if map is not initialized then don't perform this operation
  if (!this.isInitialized()) {
    logger.debug("Model " + this.getId() + " not initialized");
    return Promise.resolve();
  }

  if (this.selectedLayoutOverlays[overlayId] === undefined) {
    logger.warn("Specified overlay is not available");
    return Promise.resolve();
  }

  var promises = [];
  for (var i = 0; i < this.selectedLayoutOverlays[overlayId].length; i++) {
    promises.push(this.selectedLayoutOverlays[overlayId][i].hide());
  }
  this.selectedLayoutOverlays[overlayId] = [];
  return Promise.all(promises);
};

/**
 * Opens {@link AbstractInfoWindow} for a marker.
 *
 * @param {Marker} [marker]
 *          marker for which we are opening window
 * @param {IdentifiedElement} element
 *          marker for which we are opening window
 *
 * @returns {PromiseLike}
 */
AbstractCustomMap.prototype._openInfoWindowForIdentifiedElement = function (element, marker) {
  var self = this;
  if (element.getType() === "ALIAS") {
    return self.getModel().getByIdentifiedElement(element).then(function (alias) {
      return self._openInfoWindowForAlias(alias, marker);
    });
  } else if (element.getType() === "POINT") {
    return self._openInfoWindowForPoint(new PointData(element), marker);
  } else if (element.getType() === "REACTION") {
    return self.getModel().getByIdentifiedElement(element, true).then(function (reaction) {
      return self._openInfoWindowForReaction(reaction, marker);
    });
  } else {
    throw new Error("Unknown element type: " + element.getType());
  }
};


/**
 * Opens {@link AliasInfoWindow} for given alias.
 *
 * @param {Alias} alias
 * @param {Marker} marker
 *
 * @returns {PromiseLike}
 */
AbstractCustomMap.prototype._openInfoWindowForAlias = function (alias, marker) {
  var self = this;

  var infoWindow = this.getAliasInfoWindowById(alias.getId());
  if (infoWindow !== null && infoWindow !== undefined) {
    if (!infoWindow.isOpened()) {
      infoWindow.open(marker);
    } else {
      logger.warn("Info window for alias: " + alias.getId() + " is already opened");
    }
    return Promise.resolve(infoWindow);
  } else {
    infoWindow = new AliasInfoWindow({
      alias: alias,
      map: self,
      marker: marker
    });
    self._aliasInfoWindow[alias.getId()] = infoWindow;
    return infoWindow.init().then(function () {
      return infoWindow.open();
    });
  }
};

/**
 * Opens {@link ReactionInfoWindow} for given reaction identifier.
 *
 * @param {Reaction} reaction
 * @param {Marker} [marker]
 *
 * @returns {Promise<any>|PromiseLike<any>}
 * @protected
 */
AbstractCustomMap.prototype._openInfoWindowForReaction = function (reaction, marker) {
  var infoWindow = this.getReactionInfoWindowById(reaction.getId());
  var self = this;
  if (infoWindow !== null && infoWindow !== undefined) {
    if (!infoWindow.isOpened()) {
      infoWindow.open(marker);
    } else {
      logger.warn("Info window for reaction: " + reaction.getId() + " is already opened");
    }
    return Promise.resolve(infoWindow);
  } else {
    return self.getModel().getReactionById(reaction.getId()).then(function (reaction) {
      infoWindow = new ReactionInfoWindow({
        reaction: reaction,
        map: self,
        marker: marker
      });
      self._reactionInfoWindow[reaction.getId()] = infoWindow;
      return infoWindow.init();
    }).then(function () {
      return infoWindow.open();
    }).then(function () {
      return Promise.resolve(infoWindow);
    });
  }
};

/**
 *
 * @param {PointData} pointData
 * @param {Marker} marker
 * @returns {PromiseLike}
 * @private
 */
AbstractCustomMap.prototype._openInfoWindowForPoint = function (pointData, marker) {
  var self = this;

  var infoWindow = this.getPointInfoWindowById(pointData.getId());
  if (infoWindow !== null && infoWindow !== undefined) {
    if (!infoWindow.isOpened()) {
      infoWindow.open(marker);
    } else {
      logger.warn("Info window for point: " + pointData.getId() + " is already opened");
    }
  } else {
    infoWindow = new PointInfoWindow({
      point: pointData,
      map: self,
      marker: marker
    });
    this._pointInfoWindow[pointData.getId()] = infoWindow;
    return infoWindow.init().then(function () {
      return infoWindow.open();
    }).then(function () {
      return infoWindow;
    })
  }
  return Promise.resolve(infoWindow);
};

/**
 * Opens {@link AbstractInfoWindow} for element.
 *
 * @param {IdentifiedElement} element
 *
 * @returns {AbstractInfoWindow}
 */
AbstractCustomMap.prototype.returnInfoWindowForIdentifiedElement = function (element) {
  var markerId = element.getId();
  if (element.getType() === "ALIAS") {
    return this.getAliasInfoWindowById(markerId);
  } else if (element.getType() === "POINT") {
    return this.getPointInfoWindowById(markerId);
  } else if (element.getType() === "REACTION") {
    return this.getReactionInfoWindowById(markerId);
  } else {
    throw new Error("Unknown marker type: " + element.getType());
  }
};

/**
 * Returns identifier.
 *
 * @returns {number} identifier
 */
AbstractCustomMap.prototype.getId = function () {
  return this.getModel().getId();
};

/**
 *
 * @returns {Configuration}
 */
AbstractCustomMap.prototype.getConfiguration = function () {
  return this._configuration;
};

/**
 *
 * @param {Configuration} configuration
 */
AbstractCustomMap.prototype.setConfiguration = function (configuration) {
  this._configuration = configuration;
};

AbstractCustomMap.prototype._createMapChangedCallbacks = function () {
  var self = this;
  var sessionData = self.getServerConnector().getSessionData(self.getTopMap().getProject());
  // listener for changing zoom level
  this.getMapCanvas().addListener('zoom_changed', function () {
    sessionData.setZoomLevel(self.getModel(), self.getZoom());
  });

  this.getMapCanvas().addListener('zoom_changed', function () {
    return self.callListeners("onZoomChanged", self.getZoom());
  });

  // listener for changing location of the map (moving left/right/top/bottom
  this.getMapCanvas().addListener('center_changed', function () {
    var point = self.getMapCanvas().getCenter();
    sessionData.setCenter(self.getModel(), point);
  });
  this.getMapCanvas().addListener('center_changed', function () {
    return self.callListeners("onCenterChanged", self.getCenter());
  });
};


/**
 * Returns {@link ReactionInfoWindow} for given reaction identifier
 *
 * @param {number} reactionId
 *          reaction identifier
 * @returns {ReactionInfoWindow} for given reaction identifier
 */
AbstractCustomMap.prototype.getReactionInfoWindowById = function (reactionId) {
  return this._reactionInfoWindow[reactionId];
};

/**
 * Returns {@link AliasInfoWindow} for given alias identifier
 *
 * @param {number} aliasId
 *          alias identifier
 * @returns {AliasInfoWindow} for given alias identifier
 */
AbstractCustomMap.prototype.getAliasInfoWindowById = function (aliasId) {
  return this._aliasInfoWindow[aliasId];
};

/**
 * Returns {@link PointInfoWindow} for given point identifier
 *
 * @param {string} pointId
 *          point identifier
 * @returns {PointInfoWindow} for given point identifier
 */
AbstractCustomMap.prototype.getPointInfoWindowById = function (pointId) {
  return this._pointInfoWindow[pointId];
};

/**
 *
 * @returns {MapModel}
 */
AbstractCustomMap.prototype.getModel = function () {
  return this._model;
};

/**
 * MapModel}
 * @param model
 */
AbstractCustomMap.prototype.setModel = function (model) {
  this._model = model;
};

/**
 *
 * @returns {number}
 */
AbstractCustomMap.prototype.getTileSize = function () {
  return this.getModel().getTileSize();
};

/**
 *
 * @returns {number}
 */
AbstractCustomMap.prototype.getMinZoom = function () {
  return this.getModel().getMinZoom();
};

/**
 *
 * @returns {number}
 */
AbstractCustomMap.prototype.getMaxZoom = function () {
  return this.getModel().getMaxZoom();
};

/**
 * Returns array containing elements that are presented on a specific overlay
 * (set of map objects representing lines/areas that are associated with
 * overlay).
 *
 * @returns {Array} containing elements that are presented on a specific
 *          overlay (set of map objects representing lines/areas that are
 *          associated with overlay).
 */
AbstractCustomMap.prototype.getSelectedLayoutOverlays = function () {
  return this.selectedLayoutOverlays;
};

/**
 *
 * @returns {OpenLayerCanvas}
 */
AbstractCustomMap.prototype.getMapCanvas = function () {
  return this._canvas;
};

/**
 *
 * @param {OpenLayerCanvas} canvas
 */
AbstractCustomMap.prototype.setMapCanvas = function (canvas) {
  if (!(canvas instanceof MapCanvas)) {
    throw new Error("canvas must implement MapCanvas class");
  }
  this._canvas = canvas;
};

/**
 *
 * @returns {boolean}
 */
AbstractCustomMap.prototype.isBigLogo = function () {
  return this._bigLogo;
};

/**
 *
 * @param {boolean} debug
 */
AbstractCustomMap.prototype.setDebug = function (debug) {
  if (debug !== undefined) {
    if (typeof debug !== "boolean") {
      logger.warn("param must be boolean");
    }
    this._debug = debug;
  }
};

/**
 *
 * @returns {boolean}
 */
AbstractCustomMap.prototype.isDebug = function () {
  return this._debug === true;
};

/**
 *
 * @returns {Point}
 */
AbstractCustomMap.prototype.getTopLeft = function () {
  return new Point(0, 0)
};

/**
 *
 * @returns {Point}
 */
AbstractCustomMap.prototype.getBottomRight = function () {
  return new Point(this.getModel().getWidth(), this.getModel().getHeight());
};

/**
 *
 * @returns {HTMLElement}
 */
AbstractCustomMap.prototype.getElement = function () {
  return this._element;
};

/**
 *
 * @param {HTMLElement} element
 */
AbstractCustomMap.prototype.setElement = function (element) {
  this._element = element;
};

/**
 * Sets center point for map.
 *
 * @param {Point} coordinates
 *          new center point on map
 * @returns {Promise|PromiseLike}
 */
AbstractCustomMap.prototype.setCenter = function (coordinates) {
  if (!(coordinates instanceof Point)) {
    throw new Error("expected Point structure");
  }
  if (this.isInitialized()) {
    return Promise.resolve(this.getMapCanvas().setCenter(coordinates));
  } else {
    logger.warn("cannot center map that is not opened yet");
    return Promise.resolve();
  }
};

/**
 * @returns {Point}
 */
AbstractCustomMap.prototype.getCenter = function () {
  return this.getMapCanvas().getCenter();
};

/**
 * @returns {Bounds}
 */
AbstractCustomMap.prototype.getBounds = function () {
  return this.getMapCanvas().getBounds();
};


/**
 * Sets zoom level for the map.
 *
 * @param {number} zoom
 *          new zoom level on map
 * @return {Promise|PromiseLike}
 */
AbstractCustomMap.prototype.setZoom = function (zoom) {
  var self = this;
  if (self.isInitialized()) {
    return Promise.resolve(self.getMapCanvas().setZoom(zoom));
  } else {
    logger.warn("cannot change zoom for map that is not opened yet");
    return Promise.resolve();
  }
};

/**
 *
 * @returns {number}
 */
AbstractCustomMap.prototype.getZoom = function () {
  return this.getMapCanvas().getZoom()
};

/**
 *
 * @param {BioEntity[]} bioEntities
 *
 * @returns {PromiseLike}
 */
AbstractCustomMap.prototype.fitBounds = function (bioEntities) {
  var self = this;
  if (self.isInitialized()) {
    var canvas = self.getMapCanvas();
    if (canvas !== undefined && bioEntities.length > 0) {
      var minX = self.getModel().getWidth();
      var minY = self.getModel().getHeight();
      var maxX = 0;
      var maxY = 0;
      for (var i = 0; i < bioEntities.length; i++) {
        var bioEntity = bioEntities[i];
        if (bioEntity.getModelId() === self.getId()) {
          if (bioEntity instanceof Alias) {
            minX = Math.min(minX, bioEntity.getX());
            minY = Math.min(minY, bioEntity.getY());
            maxX = Math.max(maxX, bioEntity.getX() + bioEntity.getWidth());
            maxY = Math.max(maxY, bioEntity.getY() + bioEntity.getHeight());
          } else if (bioEntity instanceof Reaction) {
            minX = Math.min(minX, bioEntity.getCenter().x);
            minY = Math.min(minY, bioEntity.getCenter().y);
            maxX = Math.max(maxX, bioEntity.getCenter().x);
            maxY = Math.max(maxY, bioEntity.getCenter().y);
          } else {
            throw new Error("Unknown BioEntity class: " + bioEntity);
          }
        }
      }
      var currentBounds = self.getBounds();

      var xScale = (maxX - minX) / (currentBounds.getRightBottom().x - currentBounds.getTopLeft().x);
      var yScale = (maxY - minY) / (currentBounds.getRightBottom().y - currentBounds.getTopLeft().y);

      var scale = Math.max(xScale, yScale);

      var zoom = self.getZoom();

      while (scale > 1) {
        zoom--;
        scale /= 2;
      }
      if (scale <= 1e-6) {
        zoom = self.getModel().getMaxZoom();
      } else {
        while (scale < 0.5) {
          zoom++;
          scale *= 2;
        }
      }

      if (zoom > self.getModel().getMaxZoom()) {
        zoom = self.getModel().getMaxZoom();
      }
      var center = new Point((minX + maxX) / 2, (minY + maxY) / 2);
      return Promise.all([canvas.setCenter(center), canvas.setZoom(zoom)]);
    }
  }
  return Promise.resolve();
};


/**
 *
 * @param {Project} project
 */
AbstractCustomMap.prototype.setProject = function (project) {
  this._project = project;
};

/**
 *
 * @returns {Project}
 */
AbstractCustomMap.prototype.getProject = function () {
  return this._project;
};

/**
 *
 * @param {ServerConnector} serverConnector
 */
AbstractCustomMap.prototype.setServerConnector = function (serverConnector) {
  this._serverConnector = serverConnector;
};

/**
 *
 * @returns {ServerConnector}
 */
AbstractCustomMap.prototype.getServerConnector = function () {
  return this._serverConnector;
};

AbstractCustomMap.prototype.destroy = function () {
  var self = this;
  $(this.getElement()).off("touchstart");

  var key;
  for (key in self._aliasInfoWindow) {
    if (self._aliasInfoWindow.hasOwnProperty(key)) {
      self._aliasInfoWindow[key].destroy();
    }
  }
  for (key in self._pointInfoWindow) {
    if (self._pointInfoWindow.hasOwnProperty(key)) {
      self._pointInfoWindow[key].destroy();
    }
  }
  for (key in self._reactionInfoWindow) {
    if (self._reactionInfoWindow.hasOwnProperty(key)) {
      self._reactionInfoWindow[key].destroy();
    }
  }
};

/**
 *
 * @returns {boolean}
 */
AbstractCustomMap.prototype.isInitialized = function () {
  return this.initialized;
};

/**
 *
 * @param {string} type
 * @param {Object} data
 * @returns {PromiseLike}
 */
AbstractCustomMap.prototype.triggerEvent = function (type, data) {
  if (type === "map-rightclick") {
    return this.getMapCanvas().triggerEvent("rightclick", data);
  } else if (type === "map-click") {
    return this.getMapCanvas().triggerEvent("click", data);
  } else if (type === "map-center_changed") {
    return this.getMapCanvas().triggerEvent("center_changed", data);
  } else {
    throw new Error("Don't know how to handle: " + type);
  }
};

/**
 *
 * @param {HTMLElement} div
 */
AbstractCustomMap.prototype.createMapCanvas = function (div) {
  var self = this;

  self.setMapCanvas(new OpenLayerCanvas(div, self.createMapOptions()));
  self.registerMapClickEvents();
};


module.exports = AbstractCustomMap;
