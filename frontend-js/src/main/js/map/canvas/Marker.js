"use strict";

var MapCanvas = require('./MapCanvas');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var ObjectWithListeners = require('../../ObjectWithListeners');

/**
 *
 * @param {MapCanvas} options.map
 *
 * @constructor
 * @augments {ObjectWithListeners}
 */
function Marker(options) {
  ObjectWithListeners.call(this);
  this.registerListenerType("click");
  this.setMap(options.map);
}

Marker.prototype = Object.create(ObjectWithListeners.prototype);
Marker.prototype.constructor = ObjectWithListeners;

/**
 * @returns {Promise}
 */
Marker.prototype.show = function () {
  throw new Error("Not implemented");
};

Marker.prototype.hide = function () {
  throw new Error("Not implemented");
};

/**
 * @returns {Bounds}
 */
Marker.prototype.getBounds = function () {
  throw new Error("Not implemented");
};

// noinspection JSUnusedLocalSymbols
/**
 *
 * @param {string} icon
 */
Marker.prototype.setIcon = function (icon) {
  throw new Error("Not implemented");
};

/**
 *
 * @param {MapCanvas} map
 */
Marker.prototype.setMap = function (map) {
  if (!(map instanceof MapCanvas)) {
    throw new Error("Map must be of MapCanvas class");
  }
  this._map = map;
};

/**
 *
 * @returns {MapCanvas}
 */
Marker.prototype.getMap = function () {
  return this._map;
};



module.exports = Marker;
