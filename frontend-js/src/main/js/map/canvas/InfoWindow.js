"use strict";

var MapCanvas = require('./MapCanvas');
var Marker = require('./Marker');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

/**
 *
 * @param {MapCanvas} options.map
 * @constructor
 */
function InfoWindow(options) {
  this.setMap(options.map);
}

InfoWindow.prototype.open = function () {
  throw new Error("Not implemented");
};

InfoWindow.prototype.hide = function () {
  throw new Error("Not implemented");
};

/**
 *
 * @param {MapCanvas} map
 */
InfoWindow.prototype.setMap = function (map) {
  if (!(map instanceof MapCanvas)) {
    throw new Error("Map must be of MapCanvas class");
  }
  this._map = map;
};

/**
 *
 * @returns {MapCanvas}
 */
InfoWindow.prototype.getMap = function () {
  return this._map;
};

// noinspection JSUnusedLocalSymbols
/**
 *
 * @param {Marker} marker
 */
InfoWindow.prototype.setMarker = function (marker) {
  throw new Error("Not implemented");
};

// noinspection JSUnusedLocalSymbols
/**
 *
 * @param {HTMLElement|string} content
 */
InfoWindow.prototype.setContent = function (content) {
  throw new Error("Not implemented");
};

/**
 *
 */
InfoWindow.prototype.destroy = function () {
  throw new Error("Not implemented");
};


module.exports = InfoWindow;
