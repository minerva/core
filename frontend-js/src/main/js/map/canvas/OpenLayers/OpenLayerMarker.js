"use strict";

var Bounds = require('../Bounds');
var Marker = require('../Marker');

// noinspection JSUnusedLocalSymbols
var logger = require('../../../logger');

/**
 *
 * @constructor
 * @augments {Marker}
 */
function OpenLayerMarker(options) {
  Marker.call(this, options);
  var self = this;
  self.registerListenerType("onHide");

  var iconFeature = new ol.Feature({
    geometry: new ol.geom.Point(options.map.fromPointToProjection(options.position))
  });

  self.setOpenLayerMarker(iconFeature);
  self.setIcon(options.icon);

  options.source.addFeature(iconFeature);

  self._icon = options.icon;

  iconFeature.__openLayerMarker = self;
}

OpenLayerMarker.prototype = Object.create(Marker.prototype);
OpenLayerMarker.prototype.constructor = OpenLayerMarker;

OpenLayerMarker.prototype.setOpenLayerMarker = function (marker) {
  this._marker = marker;
};

OpenLayerMarker.prototype.getOpenLayerMarker = function () {
  return this._marker;
};

OpenLayerMarker.prototype.show = function () {
  return this.setIcon(this._icon);
};

/**
 *
 * @returns {Promise|PromiseLike} that resolves after all listeners are resolved
 */
OpenLayerMarker.prototype.hide = function () {
  this.getOpenLayerMarker().setStyle(new ol.style.Style({}));
  return this.callListeners("onHide");
};

/**
 *
 * @returns {boolean}
 */
OpenLayerMarker.prototype.isShown = function () {
  var feature = this.getOpenLayerMarker();
  return feature.getStyle().getImage() !== null;
};

/**
 *
 * @returns {Bounds}
 */
OpenLayerMarker.prototype.getBounds = function () {
  return new Bounds(this.getMap().fromProjectionToPoint(this.getOpenLayerMarker().getGeometry().getCoordinates()));
};

/**
 *
 * @param {string} icon
 */
OpenLayerMarker.prototype.setIcon = function (icon) {
  this._icon = icon;
  this.getOpenLayerMarker().setStyle(new ol.style.Style({
      image: new ol.style.Icon({
        src: icon,
        anchor: [0.5, 1],
        anchorXUnits: 'fraction',
        anchorYUnits: 'fraction'
      })
    }
  ));
};

module.exports = OpenLayerMarker;
