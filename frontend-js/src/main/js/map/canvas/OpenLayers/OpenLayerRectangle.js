"use strict";

var Rectangle = require('../Rectangle');
var Bounds = require('../Bounds');
var Point = require('../Point');

// noinspection JSUnusedLocalSymbols
var logger = require('../../../logger');

var Functions = require('../../../Functions');

/**
 *
 * @param options
 * @constructor
 * @extends Rectangle
 */
function OpenLayersRectangle(options) {
  Rectangle.call(this, options);

  var self = this;

  if (options.strokeColor === undefined) {
    options.strokeColor = "#000000";
  }
  if (options.strokeWeight === undefined) {
    options.strokeWeight = 1;
  }
  this._options = options;
  this._rectangles = [];

  if (options.fillGradient !== undefined) {
    var totalAmount = 0;
    for (var i = 0; i < options.fillGradient.length; i++) {
      totalAmount += options.fillGradient[i].amount;
    }
    var y = options.bounds.getTopLeft().y;
    for (i = 0; i < options.fillGradient.length; i++) {
      var x1 = options.bounds.getTopLeft().x;
      var x2 = options.bounds.getRightBottom().x;
      var y1 = y;
      var ratio = options.fillGradient[i].amount / totalAmount;

      var y2 = y1 + ratio * (options.bounds.getRightBottom().y - options.bounds.getTopLeft().y);
      y = y2;

      var bounds = new Bounds(new Point(x1, y1), new Point(x2, y2));

      self.addOpenLayersRectangle(self._createFeature({
        strokeWeight: 0.5,
        strokeColor: options.fillGradient[i].color,
        strokeOpacity: options.fillOpacity,
        bounds: bounds,
        fillOpacity: options.fillOpacity,
        fillColor: options.fillGradient[i].color,
        source: options.source
      }));
    }
    self.addOpenLayersRectangle(self._createFeature({
      strokeWeight: options.strokeWeight,
      strokeColor: options.strokeColor,
      strokeOpacity: options.strokeOpacity,
      bounds: options.bounds,
      fillOpacity: 0.0,
      id: options.id,
      fillColor: "#000000",
      source: options.source
    }));
  } else {
    self.addOpenLayersRectangle(self._createFeature(options));
  }

}

OpenLayersRectangle.prototype = Object.create(Rectangle.prototype);
OpenLayersRectangle.prototype.constructor = OpenLayersRectangle;


OpenLayersRectangle.prototype._createFeature = function (options) {
  var self = this;
  var style = self.createStyle(options);
  var polygon;
  polygon = self.createPolygon(options.bounds);
  var feature = new ol.Feature({
    geometry: polygon,
    id: options.id
  });

  feature.setStyle(new ol.style.Style({}));
  options.source.addFeature(feature);

  feature.__openLayerRectangle = this;
  /**
   *
   * @type {style.Style}
   * @private
   */
  feature.__style = style;
  return feature;
};


/**
 *
 * @param {Bounds} bounds
 * @returns {geom.Polygon}
 */
OpenLayersRectangle.prototype.createPolygon = function (bounds) {
  var self = this;

  var p1 = bounds.getTopLeft();
  var p3 = bounds.getRightBottom();
  var p2 = new Point(p3.x, p1.y);
  var p4 = new Point(p1.x, p3.y);

  var points = [
    self.getMap().fromPointToProjection(p1),
    self.getMap().fromPointToProjection(p2),
    self.getMap().fromPointToProjection(p3),
    self.getMap().fromPointToProjection(p4)
  ];

  return new ol.geom.Polygon([points]);
};

/**
 *
 * @param {string} options.strokeColor
 * @param {number} options.strokeOpacity
 * @param {number} options.strokeWeight
 * @param {string} options.fillColor
 * @param {number} options.fillOpacity
 * @param {Bounds} options.bounds
 *
 * @returns {style.Style}
 */
OpenLayersRectangle.prototype.createStyle = function (options) {
  var p1 = options.bounds.getTopLeft();
  var p3 = options.bounds.getRightBottom();

  var z = 100000 / Math.abs((p1.x - p3.x) * (p1.y - p3.y));

  return new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: Functions.colorToRgbaString(options.strokeColor, options.strokeOpacity),
      width: options.strokeWeight
    }),
    fill: new ol.style.Fill({
      color: Functions.colorToRgbaString(options.fillColor, options.fillOpacity)
    }),
    zIndex: z
  });
};

OpenLayersRectangle.prototype.addOpenLayersRectangle = function (rectangle) {
  this._rectangles.push(rectangle);
};

/**
 *
 * @returns {Array}
 */
OpenLayersRectangle.prototype.getOpenLayersRectangles = function () {
  return this._rectangles;
};

/**
 *
 */
OpenLayersRectangle.prototype.show = function () {
  var rectangles = this.getOpenLayersRectangles();

  for (var i = 0; i < rectangles.length; i++) {
    var rectangle = rectangles[i];
    rectangle.setStyle(rectangle.__style);
  }
};

OpenLayersRectangle.prototype.hide = function () {
  var rectangles = this.getOpenLayersRectangles();
  var hiddenStyle = new ol.style.Style({});
  for (var i = 0; i < rectangles.length; i++) {
    var rectangle = rectangles[i];
    rectangle.setStyle(hiddenStyle);
  }
};
OpenLayersRectangle.prototype.isShown = function () {
  var rectangles = this.getOpenLayersRectangles();
  for (var i = 0; i < rectangles.length; i++) {
    var rectangle = rectangles[i];
    if (rectangle.getStyle().getFill() !== null) {
      return true;
    }
  }
};

/**
 *
 * @param newBounds {Bounds}
 */
OpenLayersRectangle.prototype.setBounds = function (newBounds) {
  var self = this;

  self._options.bounds = newBounds;
  var oldBounds = self.getBounds();

  var rectangles = self.getOpenLayersRectangles();

  for (var i = 0; i < rectangles.length; i++) {
    var rectangle = rectangles[i];
    var currentBounds = new Bounds();
    var extent = rectangle.getGeometry().getExtent();

    var projection1 = [extent[0], extent[1]];
    currentBounds.extend(self.getMap().fromProjectionToPoint(projection1));
    var projection2 = [extent[2], extent[3]];
    currentBounds.extend(self.getMap().fromProjectionToPoint(projection2));

    var topLeft = self._transformCoordinates(currentBounds.getTopLeft(), oldBounds, newBounds);
    var rightBottom = self._transformCoordinates(currentBounds.getRightBottom(), oldBounds, newBounds);

    rectangle.setGeometry(self.createPolygon(new Bounds(topLeft, rightBottom)));
  }

};

OpenLayersRectangle.prototype.getBounds = function () {
  var self = this;
  var bounds = new Bounds();
  var rectangles = this.getOpenLayersRectangles();
  for (var i = 0; i < rectangles.length; i++) {
    var rectangle = rectangles[i];
    var extent = rectangle.getGeometry().getExtent();

    var projection1 = [extent[0], extent[1]];
    bounds.extend(self.getMap().fromProjectionToPoint(projection1));
    var projection2 = [extent[2], extent[3]];
    bounds.extend(self.getMap().fromProjectionToPoint(projection2));
  }
  return bounds;
};

OpenLayersRectangle.prototype.setOptions = function (options) {
  var self = this;
  self._options = Object.assign(self._options, options);
  var rectangles = this.getOpenLayersRectangles();
  for (var i = 0; i < rectangles.length; i++) {
    var rectangle = rectangles[i];
    var style = rectangle.__style;
    if (self._options.fillColor !== undefined) {
      style.getFill().setColor(Functions.colorToRgbaString(self._options.fillColor, self._options.fillOpacity));
    }

    if (i === rectangles.length - 1) {
      style.getStroke().setWidth(self._options.strokeWeight);
      style.getStroke().setColor(Functions.colorToRgbaString(self._options.strokeColor, self._options.strokeOpacity));
    }
  }
};

module.exports = OpenLayersRectangle;
