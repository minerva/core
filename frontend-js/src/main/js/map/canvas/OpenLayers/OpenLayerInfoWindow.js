"use strict";

var $ = require('jquery');
require('bootstrap');

var InfoWindow = require('../InfoWindow');
var OpenLayerMarker = require('./OpenLayerMarker');

var logger = require('../../../logger');
// noinspection JSUnusedLocalSymbols
var Functions = require('../../../Functions');

var Promise = require('bluebird');

function OpenLayerInfoWindow(options) {
  InfoWindow.call(this, options);

  var self = this;

  if (options.marker !== undefined) {
    if (options.marker instanceof OpenLayerMarker) {
      self.setMarker(options.marker);
    } else {
      throw new Error("Invalid argument: " + options.marker);
    }
  }
  var element = Functions.createElement({type: "div", name: "popup-window"});

  options.map.getOpenLayersMap().getTargetElement().appendChild(element);

  var popup = new ol.Overlay({
    element: element,
    positioning: 'top-center'
  });
  options.map.getOpenLayersMap().addOverlay(popup);

  self._position = options.position;
  self._element = element;

  self.setOpenLayerInfoWindow(popup);
  self._visible = false;
  self.setContent(options.content);

  //the only way that I found how to keep track if the window is opened or not
  $(element).on('hidden.bs.popover', function () {
    self._visible = false;
  });
  $(element).on('shown.bs.popover', function () {
    self._visible = true;
  });

  options.map.getOpenLayersMap().on('postrender', function () {
    if (self.isOpened()) {
      var popover = $(element).data('bs.popover');
      if (!popover) return;
      var popper = popover._popper;
      if (!popper) return;
      popper.scheduleUpdate();
    }
  });
}

OpenLayerInfoWindow.prototype = Object.create(InfoWindow.prototype);
OpenLayerInfoWindow.prototype.constructor = OpenLayerInfoWindow;

/**
 *
 * @return {Promise}
 */
OpenLayerInfoWindow.prototype.updatePosition = function () {
  var self = this;
  var popup = self.getOpenLayerInfoWindow();
  var feature = self.getMarker();
  var position;
  if (feature !== undefined && feature !== null) {
    position = feature.getGeometry().getCoordinates();
    popup.setOffset([0, -36]);
  } else {
    position = self.getMap().fromPointToProjection(self._position);
    popup.setOffset([0, 0]);
  }
  popup.setPosition(position);
  return Promise.delay(100).then(function () {
    if (self.isOpened()) {
      var popover = $(self._element).data('bs.popover');
      if (!popover) return;
      var popper = popover._popper;
      if (!popper) return;
      popper.scheduleUpdate();
    }
  });
};


/**
 *
 * @return {Promise}
 */
OpenLayerInfoWindow.prototype.open = function () {
  var self = this;

  var title = Functions.createElement({type: "div", content: "&nbsp;"});
  title.appendChild(Functions.createElement({
    type: "button", className: "close", onclick: function () {
      return self.hide();
    },
    content: "&times;"
  }));

  $(self._element).popover({
    'placement': 'top',
    'title': title,
    'content': this._content,
    'html': true
  });

  var promise = self.updatePosition();
  $(self._element).popover('show');
  self._visible = true;
  return promise;
};

OpenLayerInfoWindow.prototype.hide = function () {
  var self = this;
  $(self._element).popover('hide');
  self._visible = false;
};

OpenLayerInfoWindow.prototype.setOpenLayerInfoWindow = function (infoWindow) {
  this._infoWindow = infoWindow;
};

OpenLayerInfoWindow.prototype.getOpenLayerInfoWindow = function () {
  return this._infoWindow;
};

OpenLayerInfoWindow.prototype.setContent = function (content) {
  var self = this;
  if (self._content === undefined) {
    self._content = Functions.createElement({type: "div"});
  }
  if (Functions.isDomElement(content)) {
    Functions.removeChildren(self._content);
    self._content.appendChild(content);
  } else {
    self._content.innerHTML = content;
  }
  self.updatePosition();

};

/**
 *
 * @param {OpenLayerMarker} marker
 */
OpenLayerInfoWindow.prototype.setMarker = function (marker) {
  var self = this;
  marker.addListener("onHide", function () {
    return self.hide();
  });
  self._marker = marker.getOpenLayerMarker();
};

OpenLayerInfoWindow.prototype.getMarker = function () {
  return this._marker;
};

OpenLayerInfoWindow.prototype.isOpened = function () {
  return this._visible;
};

OpenLayerInfoWindow.prototype.destroy = function () {
  $(this._element).popover('dispose');
};

module.exports = OpenLayerInfoWindow;
