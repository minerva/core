"use strict";

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

/**
 *
 * @param {number|string||Point} x
 * @param {number|string} [y]
 * @constructor
 */
function Point(x, y) {
  if (x instanceof Point) {
    this.x = x.x;
    this.y = x.y;
  } else if (typeof x === 'object') {
    this.x = parseFloat(x.x);
    this.y = parseFloat(x.y);
  } else {
    this.x = parseFloat(x);
    this.y = parseFloat(y);
  }
}

/**
 *
 * @returns {string}
 */
Point.prototype.toString = function () {
  return "[" + this.x + ", " + this.y + "]";
};

/**
 * @param {Point} otherPoint
 * @returns {number}
 */
Point.prototype.distanceTo = function (otherPoint) {
  return Math.sqrt((otherPoint.x - this.x) * (otherPoint.x - this.x) + (otherPoint.y - this.y) * (otherPoint.y - this.y));
};

module.exports = Point;
