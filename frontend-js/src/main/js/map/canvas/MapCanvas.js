"use strict";

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var Functions = require('../../Functions');
var ObjectWithListeners = require('../../ObjectWithListeners');
var Point = require('./Point');

/**
 *
 * @param {HTMLElement} element
 * @param {Object} options
 * @param {number} options.tileSize
 * @param {number} options.minZoom
 * @param {number} options.height
 * @param {number} options.width
 * @constructor
 * @extends ObjectWithListeners
 */
function MapCanvas(element, options) {
  ObjectWithListeners.call(this);
  //map canvas listeners
  this.registerListenerType("click");
  this.registerListenerType("rightclick");
  this.registerListenerType("mouseup");
  this.registerListenerType("dragstart");
  this.registerListenerType("zoom_changed");
  this.registerListenerType("center_changed");
  this.registerListenerType("maptypeid_changed");

  this.setOptions(options);
  this.setElement(element);

  // following fields are used in conversion between x,y coordinates and lat,lng
  // coordinates
  this.pixelOrigin_ = new Point(options.tileSize / 2, options.tileSize / 2);
  this.pixelsPerLonDegree_ = options.tileSize / 360;
  this.pixelsPerLonRadian_ = options.tileSize / (2 * Math.PI);
  /* jshint bitwise: false */
  this.zoomFactor = Math.max(options.width, options.height) / (options.tileSize / (1 << options.minZoom));

}

MapCanvas.prototype = Object.create(ObjectWithListeners.prototype);
MapCanvas.prototype.constructor = ObjectWithListeners;

/**
 *
 * @param {Object} options
 * @param {number} options.tileSize
 * @param {number} options.minZoom
 * @param {number} options.height
 * @param {number} options.width
 */
MapCanvas.prototype.setOptions = function (options) {
  this._options = options;
};

/**
 *
 * @returns {{tileSize: number, minZoom: number, height: number, width: number}}
 */
MapCanvas.prototype.getOptions = function () {
  return this._options;
};

MapCanvas.prototype.setElement = function (element) {
  this._element = element;
};

MapCanvas.prototype.getElement = function () {
  return this._element;
};

/**
 *
 * @returns {number}
 */
MapCanvas.prototype.getTileSize = function () {
  return this.getOptions().tileSize;
};

/**
 *
 * @returns {number}
 */
MapCanvas.prototype.getMaxZoom = function () {
  return this.getOptions().maxZoom;
};

/**
 *
 * @returns {number}
 */
MapCanvas.prototype.getMinZoom = function () {
  return this.getOptions().minZoom;
};

/**
 *
 * @returns {number}
 */
MapCanvas.prototype.getWidth = function () {
  return this.getOptions().width;
};

/**
 *
 * @returns {number}
 */
MapCanvas.prototype.getHeight = function () {
  return this.getOptions().height;
};

// noinspection JSUnusedLocalSymbols
/**
 *
 * @param {Object} options
 * @returns {Marker}
 */
MapCanvas.prototype.createMarker = function (options) {
  throw new Error("Not implemented");
};

// noinspection JSUnusedLocalSymbols
/**
 *
 * @param {Object} options
 * @returns {InfoWindow}
 */
MapCanvas.prototype.createInfoWindow = function (options) {
  throw new Error("Not implemented");
};

// noinspection JSUnusedLocalSymbols
/**
 *
 * @param {Object} options
 * @returns {Rectangle}
 */
MapCanvas.prototype.createRectangle = function (options) {
  throw new Error("Not implemented");
};

// noinspection JSUnusedLocalSymbols
/**
 *
 * @param {Object} options
 * @returns {Polyline}
 */
MapCanvas.prototype.createPolyline = function (options) {
  throw new Error("Not implemented");
};

// noinspection JSUnusedLocalSymbols
/**
 *
 * @param {HTMLElement} element
 */
MapCanvas.prototype.addLeftBottomControl = function (element) {
  throw new Error("Not implemented");
};

// noinspection JSUnusedLocalSymbols
/**
 *
 * @param {HTMLElement} element
 */
MapCanvas.prototype.addRightBottomControl = function (element) {
  throw new Error("Not implemented");
};

// noinspection JSUnusedLocalSymbols
/**
 *
 * @param {Bounds} bounds
 */
MapCanvas.prototype.fitBounds = function (bounds) {
  throw new Error("Not implemented");
};

// noinspection JSUnusedLocalSymbols
/**
 *
 * @param {Point} center
 */
MapCanvas.prototype.setCenter = function (center) {
  throw new Error("Not implemented");
};

/**
 * @returns {Point}
 */
MapCanvas.prototype.getCenter = function () {
  throw new Error("Not implemented");
};

/**
 * @returns {number}
 */
MapCanvas.prototype.getZoom = function () {
  throw new Error("Not implemented");
};


// noinspection JSUnusedLocalSymbols
/**
 *
 * @param {number} zoom
 */
MapCanvas.prototype.setZoom = function (zoom) {
  throw new Error("Not implemented");
};

/**
 * @returns {string}
 */
MapCanvas.prototype.getBackgroundId = function () {
  throw new Error("Not implemented");
};

// noinspection JSUnusedLocalSymbols
/**
 *
 * @param {number|string} backgroundId
 */
MapCanvas.prototype.getBackgroundId = function (backgroundId) {
  throw new Error("Not implemented");
};

/**
 * @returns {Bounds}
 */
MapCanvas.prototype.getBounds = function () {
  throw new Error("Not implemented");
};

// noinspection JSUnusedLocalSymbols
/**
 *
 * @param {string} type
 * @param [data]
 */
MapCanvas.prototype.triggerListeners = function (type, data) {
  throw new Error("Not implemented");
};

/**
 * Transforms coordinates on the map from {Point} to
 * LatLng
 *
 * @param {Point} point
 *          coordinates in standard x,y format
 * @return {number[]} coordinates in lat,lng format
 */
MapCanvas.prototype.pointToLatLng = function (point) {
  var me = this;

  // rescale the point (all computations are done assuming that we work on
  // TILE_SIZE square)
  var p = new Point(point.x / me.zoomFactor, point.y / me.zoomFactor);
  var origin = me.pixelOrigin_;
  var lng = (p.x - origin.x) / me.pixelsPerLonDegree_;
  var latRadians = (p.y - origin.y) / -me.pixelsPerLonRadian_;
  var lat = Functions.radiansToDegrees(2 * Math.atan(Math.exp(latRadians)) - Math.PI / 2);
  return [lat, lng];
};

/**
 * Transforms coordinates on the map from LatLng to
 * {Point}
 *
 * @param {number[]} latLng
 *          in lat,lng format
 * @returns {Point} coordinates in x,y format
 *
 */
MapCanvas.prototype.latLngToPoint = function (latLng) {
  var lat = latLng[0];
  var lng = latLng[1];
  var me = this;
  var point = new Point(0, 0);
  var origin = me.pixelOrigin_;

  point.x = origin.x + lng * me.pixelsPerLonDegree_;

  // Truncating to 0.9999 effectively limits latitude to 89.189. This is
  // about a third of a tile past the edge of the world tile.
  var siny = Functions.bound(Math.sin(Functions.degreesToRadians(lat)), -0.9999, 0.9999);
  point.y = origin.y + 0.5 * Math.log((1 + siny) / (1 - siny)) * -me.pixelsPerLonRadian_;

  // rescale the point (all computations are done assuming that we work on
  // TILE_SIZE square)
  point.x *= me.zoomFactor;
  point.y *= me.zoomFactor;
  return point;
};

/**
 *
 * @param {Point[]} points
 * @returns {string}
 */
MapCanvas.prototype.pointsToString = function (points) {
  var self = this;
  var result = "";
  for (var i = 0; i < points.length; i++) {
    var point = points[i];
    var x = point.x;
    if (x >= self.getOptions().width * 2) {
      x -= 360 * self.pixelsPerLonDegree_ * self.zoomFactor;
    }
    result += x.toFixed(2) + "," + point.y.toFixed(2) + ";";
  }
  return result;
};


module.exports = MapCanvas;
