"use strict";

/* exported logger */

var logger = require('../../logger');

var AbstractDbOverlay = require('./AbstractDbOverlay');

var IdentifiedElement = require('../data/IdentifiedElement');
var InvalidArgumentError = require('../../InvalidArgumentError');

var Promise = require("bluebird");

/**
 *
 * @param params
 * @constructor
 * @extends AbstractDbOverlay
 */
function UserDbOverlay(params) {
  params.iconType = "marker";
  params.iconColorStart = 1;
  // call super constructor
  AbstractDbOverlay.call(this, params);

  this._markerIdentifiedElements = {
    ALIAS: [],
    REACTION: [],
    POINT: []
  };
  this._surfaceIdentifiedElements = {
    ALIAS: [],
    REACTION: [],
    POINT: []
  };
}

UserDbOverlay.prototype = Object.create(AbstractDbOverlay.prototype);
UserDbOverlay.prototype.constructor = UserDbOverlay;

/**
 *
 * @param {IdentifiedElement} element
 * @returns {Promise}
 */
UserDbOverlay.prototype.getDetailDataByIdentifiedElement = function (element) {
  if (element.getType() === "POINT") {
    return Promise.resolve(null);
  }
  var model = this.getMap().getSubmapById(element.getModelId()).getModel();
  return model.getByIdentifiedElement(element, true);
};

/**
 *
 * @param {PluginElementParam[]} params
 * @param {{ALIAS:Array, REACTION:Array, POINT:Array}} sourceArray
 * @param {boolean} mustBeUndefined
 * @returns {Promise}
 * @private
 */
UserDbOverlay.prototype._createIdentifiedElements = function (params, sourceArray, mustBeUndefined) {
  var self = this;
  var result = [];
  var promises = [];
  var mustBeDefined = !mustBeUndefined;
  for (var i = 0; i < params.length; i++) {
    var singleElementParam = params[i];
    var element = new IdentifiedElement(singleElementParam.element);
    if (singleElementParam.icon !== undefined) {
      element.setIcon(singleElementParam.icon);
    }
    if (singleElementParam.options !== undefined) {
      element.setColor(singleElementParam.options.color);
      element.setOpacity(singleElementParam.options.opacity);
      element.setLineColor(singleElementParam.options.lineColor);
      element.setLineWeight(singleElementParam.options.lineWeight);
      element.setLineOpacity(singleElementParam.options.lineOpacity);
    }
    if (sourceArray[element.getType()][element.getId()] !== undefined && mustBeUndefined) {
      return Promise.reject(new Error("Element already highlighted: " + element.getId()));
    }
    if (sourceArray[element.getType()][element.getId()] === undefined && mustBeDefined) {
      return Promise.reject(new Error("Element is not highlighted: " + element.getId()));
    }
    result.push(element);
    var submap = self.getMap().getSubmapById(element.getModelId());
    if (submap === null) {
      return Promise.reject(new InvalidArgumentError("Submap doesn't exist: " + result[i].getModelId()));
    }
    promises.push(submap.getModel().getByIdentifiedElement(element));
  }
  return Promise.all(promises).then(function (elementsFromModel) {
    for (var i = 0; i < elementsFromModel.length; i++) {
      if (elementsFromModel[i] === undefined || elementsFromModel[i] === null) {
        return Promise.reject(new InvalidArgumentError("Element doesn't exist: " + result[i].getId()));
      }
    }
    return result;
  });
};

/**
 *
 * @param {PluginElementParam[]|PluginElementParam} params
 * @returns {Promise<any>}
 */
UserDbOverlay.prototype.addMarker = function (params) {
  var self = this;
  self.disableAddRemoveElements();

  if (params.length === undefined) {
    params = [params];
  }
  return self._createIdentifiedElements(params, self._markerIdentifiedElements, true).then(function (elements) {
    for (var i = 0; i < elements.length; i++) {
      var element = elements[i];
      self._markerIdentifiedElements[element.getType()][element.getId()] = element;
    }
    return self.getIdentifiedElements();
  }).then(function (elements) {
    self.enableAddRemoveElements();
    return self.callListeners("onSearch", {
      fitBounds: false,
      identifiedElements: elements
    });
  }).then(null, function (error) {
    self.enableAddRemoveElements();
    return Promise.reject(error);
  });
};

/**
 *
 * @param {PluginElementParam[]|PluginElementParam} params
 * @returns {Promise<any>}
 */
UserDbOverlay.prototype.addSurface = function (params) {
  var self = this;
  self.disableAddRemoveElements();

  if (params.length === undefined) {
    params = [params];
  }

  return self._createIdentifiedElements(params, self._surfaceIdentifiedElements, true).then(function (elements) {
    for (var i = 0; i < elements.length; i++) {
      var element = elements[i];
      self._surfaceIdentifiedElements[element.getType()][element.getId()] = element;
    }
    return self.getIdentifiedElements();
  }).then(function (elements) {
    self.enableAddRemoveElements();
    return self.callListeners("onSearch", {
      fitBounds: false,
      identifiedElements: elements
    });
  }).then(null, function (error) {
    self.enableAddRemoveElements();
    return Promise.reject(error);
  });
};

/**
 *
 * @param {PluginElementParam[]|PluginElementParam} params
 * @returns {Promise<any>}
 */
UserDbOverlay.prototype.removeMarker = function (params) {
  var self = this;
  self.disableAddRemoveElements();

  if (params.length === undefined) {
    params = [params];
  }

  return self._createIdentifiedElements(params, self._markerIdentifiedElements, false).then(function (elements) {
    for (var i = 0; i < elements.length; i++) {
      var element = elements[i];
      self._markerIdentifiedElements[element.getType()][element.getId()] = undefined;
      delete self._markerIdentifiedElements[element.getType()][element.getId()];
    }
    return self.getIdentifiedElements();
  }).then(function (elements) {
    self.enableAddRemoveElements();
    return self.callListeners("onSearch", {
      fitBounds: false,
      identifiedElements: elements
    });
  }).then(null, function (error) {
    self.enableAddRemoveElements();
    return Promise.reject(error);
  });
};

/**
 *
 * @param {PluginElementParam[]|PluginElementParam} params
 * @returns {Promise<any>}
 */
UserDbOverlay.prototype.removeSurface = function (params) {
  var self = this;
  self.disableAddRemoveElements();

  if (params.length === undefined) {
    params = [params];
  }
  return self._createIdentifiedElements(params, self._surfaceIdentifiedElements, false).then(function (elements) {
    for (var i = 0; i < elements.length; i++) {
      var element = elements[i];
      self._surfaceIdentifiedElements[element.getType()][element.getId()] = undefined;
      delete self._surfaceIdentifiedElements[element.getType()][element.getId()];
    }
    return self.getIdentifiedElements();
  }).then(function (elements) {
    self.enableAddRemoveElements();
    return self.callListeners("onSearch", {
      fitBounds: false,
      identifiedElements: elements
    });
  }).then(null, function (error) {
    self.enableAddRemoveElements();
    return Promise.reject(error);
  });
};

/**
 *
 */
UserDbOverlay.prototype.disableAddRemoveElements = function () {
  if (this._addRemoveElementsDisabled) {
    throw new Error("wait until previous Promise for showBioEntity/hideBioEntity is resolved");
  } else {
    this._addRemoveElementsDisabled = true;
  }
};

/**
 *
 */
UserDbOverlay.prototype.enableAddRemoveElements = function () {
  if (this._addRemoveElementsDisabled) {
    this._addRemoveElementsDisabled = false;
  } else {
    logger.warn("showBioEntity/hideBioEntity is not disabled");
  }
};

/**
 *
 * @returns {Promise}
 */
UserDbOverlay.prototype.getIdentifiedElements = function () {
  var result = [];
  var markerType, key, markers;
  for (markerType in this._markerIdentifiedElements) {
    if (this._markerIdentifiedElements.hasOwnProperty(markerType)) {
      markers = this._markerIdentifiedElements[markerType];
      for (key in markers) {
        if (markers.hasOwnProperty(key)) {
          var identifiedElement = markers[key];
          if (identifiedElement.getIcon() === undefined) {
            identifiedElement.setIcon("marker/generic/search_red.png");
          }
          result.push(identifiedElement);
        }
      }
    }
  }

  for (markerType in this._surfaceIdentifiedElements) {
    if (this._surfaceIdentifiedElements.hasOwnProperty(markerType)) {
      markers = this._surfaceIdentifiedElements[markerType];
      for (key in markers) {
        if (markers.hasOwnProperty(key)) {
          result.push(markers[key]);
        }
      }
    }
  }

  return Promise.resolve(result);
};

/**
 *
 * @returns {PromiseLike}
 */
UserDbOverlay.prototype.clear = function () {
  var self = this;
  self._markerIdentifiedElements = {
    ALIAS: [],
    REACTION: [],
    POINT: []
  };
  return self.callListeners("onSearch", {
    fitBounds: false,
    identifiedElements: []
  });
};

module.exports = UserDbOverlay;
