"use strict";

/* exported logger */

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var Promise = require("bluebird");

var IdentifiedElement = require('../data/IdentifiedElement');
var ObjectWithListeners = require('../../ObjectWithListeners');
var Point = require('../canvas/Point');

var ServerConnector = require('../../ServerConnector');

/**
 *
 * @param {Object} params
 * @param {CustomMap} params.map
 * @param {string} params.name
 * @param {boolean} [params.allowGeneralSearch=false]
 * @param {boolean} [params.allowSearchById=false]
 * @param {string} params.iconType
 * @param {number} params.iconColorStart
 * @constructor
 * @extends ObjectWithListeners
 */
function AbstractDbOverlay(params) {
  // call super constructor
  ObjectWithListeners.call(this);

  if (params.map === undefined) {
    throw new Error("map param must be defined");
  }

  this.setName(params.name);
  this.setMap(params.map);

  this.setAllowSearchById(params.allowSearchById);
  this.setAllowGeneralSearch(params.allowGeneralSearch);

  this.setIconType(params.iconType);
  this.setIconStart(params.iconColorStart);

  this._elementsByQuery = [];
  this._elementsByQuery[""] = [];
  this.registerListenerType('onSearch');
  this.registerListenerType('onFocus');
  this.registerListenerType('onClear');
  this.registerListenerType('onTargetVisibilityChange');

  this.setQueries([]);

  this.mapOverlays = {
    ALIAS: [],
    REACTION: [],
    POINT: []
  };

}

AbstractDbOverlay.prototype = Object.create(ObjectWithListeners.prototype);
AbstractDbOverlay.prototype.constructor = AbstractDbOverlay;

AbstractDbOverlay.QueryType = {
  SEARCH_BY_COORDINATES: "SEARCH_BY_COORDINATES",
  SEARCH_BY_TARGET: "SEARCH_BY_TARGET",
  SEARCH_BY_TARGET_MAP: "SEARCH_BY_TARGET_MAP",
  SEARCH_BY_QUERY: "SEARCH_BY_QUERY"
};

/**
 * TODO parameters to this function should be changed into a dict
 * @param {string} type
 * @param arg0
 * @param [arg1]
 * @param [arg2]
 * @returns {string}
 */
AbstractDbOverlay.prototype.encodeQuery = function (type, arg0, arg1, arg2) {
  if (type === AbstractDbOverlay.QueryType.SEARCH_BY_COORDINATES) {
    var modelId = arg0;
    var coordinates = arg1;
    // noinspection UnnecessaryLocalVariableJS
    var zoom = arg2;
    return JSON.stringify({
      type: type,
      modelId: modelId,
      coordinates: coordinates,
      zoom: zoom
    });
  } else if (type === AbstractDbOverlay.QueryType.SEARCH_BY_TARGET) {
    // noinspection UnnecessaryLocalVariableJS
    var target = arg0;
    return JSON.stringify({
      type: type,
      target: target
    });
  } else if (type === AbstractDbOverlay.QueryType.SEARCH_BY_TARGET_MAP) {
    // noinspection UnnecessaryLocalVariableJS
    var targetMap = arg0;
    return JSON.stringify({
      type: type,
      targetMap: targetMap
    });
  } else if (type === AbstractDbOverlay.QueryType.SEARCH_BY_QUERY) {
    var query = arg0;
    // noinspection UnnecessaryLocalVariableJS
    var perfect = arg1;
    return JSON.stringify({
      type: type,
      query: query,
      perfect: perfect
    });
  } else {
    throw new Error("Unknown query type: " + type);
  }
};

/**
 *
 * @param {string} query
 * @returns {Object}
 */
AbstractDbOverlay.prototype.decodeQuery = function (query) {
  return JSON.parse(query);
};

/**
 *
 * @param {string} originalQuery
 * @param {boolean} [perfect]
 * @param {boolean} [fitBounds]
 * @returns {Promise}
 */
AbstractDbOverlay.prototype.searchByQuery = function (originalQuery, perfect, fitBounds) {
  var self = this;
  var query = self.encodeQuery(AbstractDbOverlay.QueryType.SEARCH_BY_QUERY, originalQuery, perfect);
  ServerConnector.getSessionData().setQuery({
    type: self.getName(),
    query: query
  });

  var res;
  var encodedQueries = [];

  var resultPromise;
  if (originalQuery.indexOf(";") >= 0) {
    var queries = self.splitQuery(originalQuery);

    var promises = [];
    for (var i = 0; i < queries.length; i++) {
      encodedQueries.push(self.encodeQuery(AbstractDbOverlay.QueryType.SEARCH_BY_QUERY, queries[i], perfect));
      promises.push(self.searchBySingleQuery(queries[i], perfect));
    }
    resultPromise = Promise.all(promises);
  } else {
    resultPromise = self.searchBySingleQuery(originalQuery, perfect).then(function (results) {
      if (results.length > 0) {
        encodedQueries.push(query);
        return [results];
      } else {
        var queries = self.splitQuery(originalQuery);

        var promises = [];
        for (var i = 0; i < queries.length; i++) {
          encodedQueries.push(self.encodeQuery(AbstractDbOverlay.QueryType.SEARCH_BY_QUERY, queries[i], perfect));
          promises.push(self.searchBySingleQuery(queries[i], perfect));
        }

        return Promise.all(promises);
      }
    });
  }
  return resultPromise.then(function (results) {
    self.setQueries(encodedQueries);
    res = results;
    return self.callListeners('onSearch', {
      fitBounds: fitBounds,
      identifiedElements: res,
      type: AbstractDbOverlay.QueryType.SEARCH_BY_QUERY,
      query: originalQuery,
      perfect: perfect
    });
  }).then(function () {
    return res;
  });
}
;

/**
 *
 * @param {number} mapId
 * @returns {Promise}
 */
AbstractDbOverlay.prototype.searchByTargetMapId = function (mapId) {
  return Promise.reject(new Error("Not implemented"));
};

/**
 *
 * @param {string[]} queries
 */
AbstractDbOverlay.prototype.setQueries = function (queries) {
  this._queries = queries;
};

/**
 *
 * @returns {string[]}
 */
AbstractDbOverlay.prototype.getQueries = function () {
  return this._queries;
};

/**
 *
 * @param {TargettingStructure} targetingElement
 * @param {number} colourId
 * @returns {IdentifiedElement[]}
 */
AbstractDbOverlay.prototype.createIdentifiedElementsForTargetingClass = function (targetingElement, colourId) {
  var self = this;
  var result = [];
  var iconCounter = 1;
  var targets = targetingElement.getTargets();
  for (var k = 0; k < targets.length; k++) {
    var target = targets[k];
    if (target.isVisible()) {
      var elements = target.getTargetElements();
      for (var l = 0; l < elements.length; l++) {
        var element = elements[l];
        var ie = new IdentifiedElement(element);
        if (element.getType() === "ALIAS") {
          ie.setIcon(self.getIcon(colourId, iconCounter));
        } else if (element.getType() !== "REACTION") {
          throw new Error("Unknown element type: " + element.getType());
        }
        result.push(ie);
      }
    }
    iconCounter++;
  }
  return result;
};

/**
 * @returns {Promise}
 */
AbstractDbOverlay.prototype.refresh = function () {
  throw new Error("Refreshing shouldn't be called");
};

// noinspection JSUnusedLocalSymbols
/**
 *
 * @param {IdentifiedElement} element
 * @returns {Promise}
 */
AbstractDbOverlay.prototype.searchByTarget = function (element) {
  return Promise.reject(new Error("Not implemented"));
};

/**
 *
 * @param {string} originalQuery
 * @param {boolean} [fitBounds]
 * @returns {*}
 */
AbstractDbOverlay.prototype.searchByEncodedQuery = function (originalQuery, fitBounds) {
  var query = this.decodeQuery(originalQuery);
  query.fitBounds = fitBounds;
  if (query.type === AbstractDbOverlay.QueryType.SEARCH_BY_QUERY) {
    return this.searchByQuery(query.query, query.perfect);
  } else if (query.type === AbstractDbOverlay.QueryType.SEARCH_BY_TARGET) {
    return this.searchByTarget(new IdentifiedElement(query.target));
  } else if (query.type === AbstractDbOverlay.QueryType.SEARCH_BY_TARGET_MAP) {
    return this.searchByTargetMapId(parseInt(originalQuery));
  } else if (query.type === AbstractDbOverlay.QueryType.SEARCH_BY_COORDINATES) {
    query.coordinates = new Point(query.coordinates.x, query.coordinates.y);
    if (this.getMap().getSubmapById(query.modelId) === null) {
      //this can happen when cached data comes from project that was removed and something else
      //was uploaded with the same name
      logger.warn("Invalid search query. Model doesn't exist: " + query.modelId);
      return Promise.resolve();
    }
    return this.searchByCoordinates(query);
  } else {
    throw new Error("Unknown type of query: " + query.type);
  }
};

/**
 *
 * @returns {Promise<any>}
 */
AbstractDbOverlay.prototype.clear = function () {
  var self = this;
  return self.searchByQuery("").then(function () {
    return self.callListeners('onClear');
  });
};

/**
 * Returns true if overlay allows to get general data for element.
 *
 * @returns {boolean}
 */
AbstractDbOverlay.prototype.allowGeneralSearch = function () {
  return this._allowGeneralSearch;
};

/**
 * Returns true if overlay allows to get data for element by search id.
 * @returns {boolean}
 */
AbstractDbOverlay.prototype.allowSearchById = function () {
  return this._allowSearchById;
};

/**
 *
 * @param {CustomMap} map
 */
AbstractDbOverlay.prototype.setMap = function (map) {
  this._map = map;
};

/**
 *
 * @returns {CustomMap}
 */
AbstractDbOverlay.prototype.getMap = function () {
  return this._map;
};

/**
 *
 * @returns {Configuration}
 */
AbstractDbOverlay.prototype.getConfiguration = function () {
  return this.getMap().getConfiguration();
};

/**
 *
 * @param {string} name
 */
AbstractDbOverlay.prototype.setName = function (name) {
  this.name = name;
};

/**
 *
 * @returns {string}
 */
AbstractDbOverlay.prototype.getName = function () {
  return this.name;
};

/**
 *
 * @param {boolean} allowSearchById
 */
AbstractDbOverlay.prototype.setAllowSearchById = function (allowSearchById) {
  // configure if the overlay can contain detailed data about elements that
  // should be visualized in detailed mode of the Info Window
  if (typeof allowSearchById === "boolean") {
    this._allowSearchById = allowSearchById;
  } else if (allowSearchById === undefined) {
    this._allowSearchById = false;
  } else {
    throw new Error("Unknown type of allowSearchById: " + allowSearchById);
  }
};

/**
 *
 * @param {boolean} allowGeneralSearch
 */
AbstractDbOverlay.prototype.setAllowGeneralSearch = function (allowGeneralSearch) {
  if (typeof allowGeneralSearch === "boolean") {
    this._allowGeneralSearch = allowGeneralSearch;
  } else if (allowGeneralSearch === undefined) {
    this._allowGeneralSearch = false;
  } else {
    throw new Error("Unknown type of allowSearchById: " + allowGeneralSearch);
  }
};

/**
 *
 * @param {string} iconType
 */
AbstractDbOverlay.prototype.setIconType = function (iconType) {
  this._iconType = iconType;
};

/**
 *
 * @param {number} iconStart
 */
AbstractDbOverlay.prototype.setIconStart = function (iconStart) {
  this._iconStart = iconStart;
};

AbstractDbOverlay.IconColors = ["red", "blue", "green", "purple", "yellow", "pink", "paleblue", "brown", "orange"];

/**
 *
 * @param {number} colorId
 * @returns {string}
 */
AbstractDbOverlay.prototype.getColor = function (colorId) {
  var id = colorId + this._iconStart;
  id %= AbstractDbOverlay.IconColors.length;
  return AbstractDbOverlay.IconColors[id];
};

/**
 *
 * @param {number} colorId
 * @param {number} [id]
 * @returns {string}
 */
AbstractDbOverlay.prototype.getIcon = function (colorId, id) {
  var suffix = "_" + id;
  if (id >= 100 || id === undefined) {
    suffix = "";
  }
  var color = this.getColor(colorId);
  return "marker/" + this._iconType + "/" + this._iconType + "_" + color + suffix + ".png";
};

/**
 *
 * @param {string} query
 * @param {boolean} [useFullName]
 * @returns {string[]}
 */
AbstractDbOverlay.prototype.splitQuery = function (query, useFullName) {
  var result = [];
  if (query.indexOf(";") >= 0) {
    result = query.split(";");
  } else {
    result = query.split(",");
  }

  for (var i = 0; i < result.length; i++) {
    result[i] = result[i].trim();
  }
  if (result.length > 1 && useFullName) {
    result.push(query);
  }
  return result;
};

module.exports = AbstractDbOverlay;
