"use strict";

/* exported logger */

var logger = require('../../logger');

var ObjectWithListeners = require('../../ObjectWithListeners');
var ChemicalDbOverlay = require('./ChemicalDbOverlay');
var CommentDbOverlay = require('./CommentDbOverlay');
var DrugDbOverlay = require('./DrugDbOverlay');
var SearchDbOverlay = require('./SearchDbOverlay');

/**
 *
 * @param {CustomMap} params.map
 * @constructor
 * @extends ObjectWithListeners
 */
function DbOverlayCollection(params) {
  // call super constructor
  ObjectWithListeners.call(this);

  var hasDisease = params.map.getProject().getDisease() !== undefined;

  var dataCollections = [{
    name: "search"
  }, {
    name: "drug",
    allowSearchById: true,
    allowGeneralSearch: true
  }, {
    name: "chemical",
    allowSearchById: hasDisease,
    allowGeneralSearch: hasDisease
  }, {
    name: "comment",
    allowSearchById: false,
    allowGeneralSearch: true
  }];

  var map = params.map;

  for (var i = 0; i < dataCollections.length; i++) {
    var collectionParams = dataCollections[i];
    collectionParams.map = map;
    var collection = null;
    if (collectionParams.name === "comment") {
      collection = new CommentDbOverlay(collectionParams);
    } else if (collectionParams.name === "search") {
      collection = new SearchDbOverlay(collectionParams);
    } else if (collectionParams.name === "drug") {
      collection = new DrugDbOverlay(collectionParams);
    } else if (collectionParams.name === "chemical") {
      collection = new ChemicalDbOverlay(collectionParams);
    } else {
      throw new Error("Unknown overlay db: " + collectionParams.name);
    }
    map.registerDbOverlay(collection);
  }

}

DbOverlayCollection.prototype = Object.create(ObjectWithListeners.prototype);
DbOverlayCollection.prototype.constructor = DbOverlayCollection;

module.exports = DbOverlayCollection;
