"use strict";

/* exported logger */

var logger = require('../../logger');
var Functions = require('../../Functions');

var Promise = require("bluebird");

var AbstractDbOverlay = require('./AbstractDbOverlay');
var Alias = require('../data/Alias');
var ConfigurationType = require('../../ConfigurationType');
var IdentifiedElement = require('../data/IdentifiedElement');
var InvalidArgumentException = require('../../InvalidArgumentError');
var Point = require('../canvas/Point');
var Reaction = require('../data/Reaction');
var SearchBioEntityGroup = require('../data/SearchBioEntityGroup');


var ServerConnector = require('../../ServerConnector');

/**
 *
 * @param params
 * @constructor
 * @extends AbstractDbOverlay
 */
function SearchDbOverlay(params) {
  params.iconType = "marker";
  params.iconColorStart = 0;
  // call super constructor
  AbstractDbOverlay.call(this, params);

}

SearchDbOverlay.prototype = Object.create(AbstractDbOverlay.prototype);
SearchDbOverlay.prototype.constructor = SearchDbOverlay;

/**
 *
 * @param {string} query
 * @returns {Promise}
 */
SearchDbOverlay.prototype.getElementsByQuery = function (query) {
  var self = this;
  var i;
  var queryId = null;
  var queries = self.getQueries();
  for (i = 0; i < queries.length; i++) {
    if (queries[i] === query) {
      queryId = i;
    }
  }
  if (queryId === null) {
    throw new Error("Invalid query: " + query);
  }
  var elements = self._elementsByQuery[query];
  var promises = [];
  for (i = 0; i < elements.length; i++) {
    var model = self.getMap().getSubmapById(elements[0].getModelId()).getModel();
    promises.push(model.getByIdentifiedElement(elements[i], true));
  }
  return Promise.all(promises).then(function (elements) {
    var result = [];
    var iconCounter = 1;
    var groups = [];
    for (var i = 0; i < elements.length; i++) {
      var element = elements[i];
      var alreadyExists = false;
      var group;
      for (var j = 0; j < groups.length; j++) {
        group = groups[j];
        if (group.bioEntityMatch(element)) {
          alreadyExists = true;
          group.addBioEntity(element);
        }
      }

      if (!alreadyExists) {
        var icon;
        if (element instanceof Alias) {
          icon = self.getIcon(queryId, iconCounter++)
        }
        group = new SearchBioEntityGroup(element);
        group.setIcon(icon);
        groups.push(group);
        result.push({
          element: group,
          icon: icon
        });
      }
    }
    return result;
  });
};


/**
 *
 * @param {Point} coordinates
 * @param {IdentifiedElement[]} compartments
 * @param {number} maxDistance
 * @returns {Promise}
 * @private
 */
SearchDbOverlay.prototype._returnCompartmentIfClickOnBorder = function (coordinates, compartments, maxDistance) {
  var promises = [];
  for (var i = 0; i < compartments.length; i++) {
    var compartment = compartments[i];
    var model = this.getMap().getSubmapById(compartment.getModelId()).getModel();
    promises.push(model.getByIdentifiedElement(compartment).then(function (result) {
        var p1 = new Point(result.getX(), result.getY());
        var p2 = new Point(result.getX() + result.getWidth(), result.getY());
        var p3 = new Point(result.getX() + result.getWidth(), result.getY() + result.getHeight());
        var p4 = new Point(result.getX(), result.getY() + result.getHeight());
        var distance = Functions.distance(coordinates, {start: p1, end: p2});
        distance = Math.min(distance, Functions.distance(coordinates, {start: p2, end: p3}));
        distance = Math.min(distance, Functions.distance(coordinates, {start: p3, end: p4}));
        distance = Math.min(distance, Functions.distance(coordinates, {start: p4, end: p1}));

        if (maxDistance > distance) {
          return new IdentifiedElement(result);
        } else {
          return undefined;
        }
      })
    );
  }
  return Promise.all(promises).then(function (result) {
    for (var i = 0; i < result.length; i++) {
      if (result[i] !== undefined) {
        return result[i];
      }
    }
  });
};

/**
 *
 * @param {IdentifiedElement} identifiedElement
 * @param {number} zoomLevel
 * @param {Point} coordinates
 * @returns {Promise}
 * @private
 */
SearchDbOverlay.prototype._getFirstVisibleParentOrObject = function (identifiedElement, zoomLevel, coordinates) {
  if (identifiedElement === undefined) {
    return Promise.resolve();
  }
  var self = this;
  var model = self.getMap().getSubmapById(identifiedElement.getModelId()).getModel();
  return model.getByIdentifiedElement(identifiedElement, true).then(function (fullElement) {
    if (fullElement.getHierarchyVisibilityLevel() <= zoomLevel) {
      if (fullElement instanceof Alias) {
        //transparent compartments shouldn't be clickable
        if (fullElement.getTransparencyLevel() <= zoomLevel && fullElement.getType() === "Compartment") {
          var maxDistance = self.computeMaxDistance(model, zoomLevel);
          return self._returnCompartmentIfClickOnBorder(coordinates, [identifiedElement], maxDistance);
        }
      }
      return identifiedElement;
    } else {
      var parentId;
      if (fullElement instanceof Alias) {
        if (fullElement.getComplexId() !== undefined) {
          parentId = fullElement.getComplexId();
        } else if (fullElement.getCompartmentId() !== undefined) {
          parentId = fullElement.getCompartmentId();
        }
      }
      if (parentId !== undefined) {
        var parent = new IdentifiedElement({
          id: parentId,
          type: "ALIAS",
          modelId: identifiedElement.getModelId()
        });
        return self._getFirstVisibleParentOrObject(parent, zoomLevel, coordinates);
      } else {
        logger.warn("Cannot find visible parent for object. (zoomLevel=" + zoomLevel + ")");
        logger.warn(fullElement);
        return identifiedElement;
      }
    }
  });
};

/**
 *
 * @param {MapModel} model
 * @param {number} zoom
 * @returns {number}
 */
SearchDbOverlay.prototype.computeMaxDistance = function (model, zoom) {
  var distance = parseFloat(this.getConfiguration().getOption(ConfigurationType.SEARCH_DISTANCE).getValue());
  var maxZoom = model.getMaxZoom();
  var zoomDiff = maxZoom - zoom;
  for (var i = 0; i < zoomDiff; i++) {
    distance = distance * 1.5;
  }
  return distance;
};

/**
 *
 * @param {Point} coordinates
 * @param {number} zoom
 * @param {MapModel} model
 * @param {number} maxDistance
 * @returns {PromiseLike<T | never> | Promise<T | never>}
 */
SearchDbOverlay.prototype.findCompartmentByCoordinates = function (coordinates, zoom, model, maxDistance) {
  var self = this;
  var result = undefined;
  return ServerConnector.getClosestElementsByCoordinates({
    modelId: model.getId(),
    coordinates: coordinates,
    count: 10,
    type: ["Compartment", "Pathway"]
  }).then(function (elements) {
    if (elements.length === 0) {
      return undefined;
    } else {
      return self._returnCompartmentIfClickOnBorder(coordinates, elements, maxDistance).then(function (result) {
        var nestedOverlay = "Pathways and compartments";
        if (result === undefined && self.getMap().getBackgroundDataOverlay().getName() === nestedOverlay) {
          return self._getFirstVisibleParentOrObject(elements[0], zoom - model.getMinZoom(), coordinates);
        } else {
          return result;
        }
      });
    }
  }).then(function (compartment) {
    result = compartment;
    if (result !== undefined) {
      return model.getAliasById(result.getId(), true).then(function (alias) {
        //pathways with glyph and z index below 0 should not be clickable
        if (alias.getType() === "Pathway" && alias.getZ() < 0 && alias.getGlyph() !== null) {
          return undefined;
        } else {
          return result;
        }
      });
    }
  }).then(function () {
    if (result !== undefined) {
      return self.getMap().getDistance({
        modelId: result.getModelId(),
        coordinates: coordinates,
        element: result
      }).then(function (distance) {
        if (distance <= maxDistance) {
          return result;
        }
      });
    }
    return result;
  });
};

/**
 *
 * @param {Object} params
 * @param {number} params.modelId
 * @param {Point} params.coordinates
 * @param {number} params.zoom
 * @param {boolean} [params.fitBounds]
 * @returns {Promise| PromiseLike}
 */
SearchDbOverlay.prototype.searchByCoordinates = function (params) {
  var modelId = params.modelId;
  var coordinates = params.coordinates;
  var zoom = params.zoom;

  var self = this;
  var query = self.encodeQuery(AbstractDbOverlay.QueryType.SEARCH_BY_COORDINATES, modelId, coordinates, zoom);

  ServerConnector.getSessionData().setSearchQuery(query);

  if (self._elementsByQuery[query] !== undefined) {
    self.setQueries([query]);
    return self.callListeners('onSearch', {
      fitBounds: false,
      identifiedElements: [self._elementsByQuery[query]],
      type: AbstractDbOverlay.QueryType.SEARCH_BY_COORDINATES,
      coordinates: coordinates,
      modelId: modelId,
      zoom: zoom
    }).then(function () {
      return Promise.resolve(self._elementsByQuery[query]);
    });
  } else {
    var searchResult = null;
    var map = self.getMap().getSubmapById(modelId);
    if (map === null) {
      return Promise.reject(new InvalidArgumentException("model doesn't exist for modelId: " + modelId));
    }
    var model = map.getModel();
    var maxDistance = self.computeMaxDistance(model, zoom);
    return ServerConnector.getClosestElementsByCoordinates({
      modelId: modelId,
      coordinates: coordinates,
      count: 1
    }).then(function (elements) {
      var nestedOverlay = "Pathways and compartments";
      if (elements.length === 0) {
        return undefined;
      } else {
        if (self.getMap().getBackgroundDataOverlay() !== null && self.getMap().getBackgroundDataOverlay().getName() === nestedOverlay) {
          return self._getFirstVisibleParentOrObject(elements[0], zoom - model.getMinZoom(), coordinates);
        } else {
          return elements[0];
        }
      }
    }).then(function (visibleObject) {
      if (visibleObject !== undefined) {
        searchResult = [visibleObject];
        if (searchResult[0].getType() === "REACTION") {
          return model.getReactionById(searchResult[0].getId(), true).then(function (reaction) {
            var reactionElements = reaction.getElements();
            for (var i = 0; i < reactionElements.length; i++) {
              searchResult.push(new IdentifiedElement(reactionElements[i]));
            }
          });
        } else {
          return model.getAliasById(searchResult[0].getId(), true).then(function (alias) {
            //pathways with glyph and z index below 0 should not be clickable
            if (alias.getType() === "Pathway" && alias.getZ() < 0 && alias.getGlyph() !== null) {
              searchResult = [];
            }
          });
        }
      } else {
        searchResult = [];
      }
    }).then(function () {
      if (searchResult.length > 0) {
        return self.getMap().getDistance({
          modelId: modelId,
          coordinates: coordinates,
          element: searchResult[0]
        });
      } else {
        return Number.POSITIVE_INFINITY;
      }
    }).then(function (distance) {
      if (distance <= maxDistance) {
        self._elementsByQuery[query] = searchResult;
      } else {
        return self.findCompartmentByCoordinates(coordinates, zoom, model, maxDistance).then(function (visibleObject) {
          if (visibleObject !== undefined) {
            return model.getAliasById(visibleObject.getId(), true).then(function (alias) {
              //pathways with glyph and z index below 0 should not be clickable
              if (alias.getType() === "Pathway" && alias.getZ() < 0 && alias.getGlyph() !== null) {
                self._elementsByQuery[query] = [];
              } else {
                self._elementsByQuery[query] = [visibleObject];
              }
            });
          } else {
            self._elementsByQuery[query] = [];
          }
        });
      }
    }).then(function () {
      self.setQueries([query]);

      return self.callListeners('onSearch', {
        fitBounds: params.fitBounds,
        identifiedElements: [self._elementsByQuery[query]],
        type: AbstractDbOverlay.QueryType.SEARCH_BY_COORDINATES,
        coordinates: params.coordinates,
        modelId: modelId,
        zoom: zoom
      });
    }).then(function () {
      return self._elementsByQuery[query];
    });
  }
};

/**
 *
 * @param {string} originalQuery
 * @param {boolean} [perfect]
 * @returns {Promise}
 */
SearchDbOverlay.prototype.searchBySingleQuery = function (originalQuery, perfect) {
  var self = this;
  var query = self.encodeQuery(AbstractDbOverlay.QueryType.SEARCH_BY_QUERY, originalQuery, perfect);
  if (self._elementsByQuery[query] !== undefined) {
    return Promise.resolve(self._elementsByQuery[query]);
  } else {
    return ServerConnector.getElementsByQuery({
      query: originalQuery,
      perfectMatch: perfect
    }).then(function (elements) {
      var result = [];
      for (var i = 0; i < elements.length; i++) {
        result.push(new IdentifiedElement(elements[i]));
      }
      self._elementsByQuery[query] = result;
      return self.getMap().fetchIdentifiedElements(result, true);
    }).then(function () {
      return self._elementsByQuery[query];
    });
  }
};

/**
 *
 * @param {number} mapId
 * @returns {Promise}
 */
SearchDbOverlay.prototype.searchByTargetMapId = function(mapId) {
  var self = this;
  var query = self.encodeQuery(AbstractDbOverlay.QueryType.SEARCH_BY_TARGET_MAP, mapId);

  ServerConnector.getSessionData().setSearchQuery(query);

  return Promise.resolve().then(function () {
    if (self._elementsByQuery[query] === undefined) {
      return self.getMap().getProject().getElementsPointingToSubmap(mapId, true).then(function(elements){
        self._elementsByQuery[query] = elements;
      });
    }
  }).then(function () {
    return self.getMap().fetchIdentifiedElements(self._elementsByQuery[query], true);
  }).then(function () {
    self.setQueries([query]);
    return self.callListeners('onSearch', {
      fitBounds: false,
      identifiedElements: [self._elementsByQuery[query]],
      type: AbstractDbOverlay.QueryType.SEARCH_BY_TARGET_MAP
    });
  }).then(function () {
    return Promise.resolve(self._elementsByQuery[query]);
  });
};

/**
 *
 * @param {IdentifiedElement} element
 * @returns {Promise|PromiseLike}
 */
SearchDbOverlay.prototype.searchByTarget = function (element) {
  var self = this;
  var query = self.encodeQuery(AbstractDbOverlay.QueryType.SEARCH_BY_TARGET, element);

  ServerConnector.getSessionData().setSearchQuery(query);

  return Promise.resolve().then(function () {
    if (self._elementsByQuery[query] === undefined) {
      if (element.getType() === "REACTION") {
        return self.getMap().getSubmapById(element.getModelId()).getModel().getReactionById(element.getId(), true).then(function (reaction) {
          var result = [element];
          var reactionElements = reaction.getElements();
          for (var i = 0; i < reactionElements.length; i++) {
            result.push(new IdentifiedElement(reactionElements[i]));
          }
          self._elementsByQuery[query] = result;
        });
      } else {
        self._elementsByQuery[query] = [element];
      }
    }
  }).then(function () {
    return self.getMap().fetchIdentifiedElements(self._elementsByQuery[query], true);
  }).then(function () {
    self.setQueries([query]);
    return self.callListeners('onSearch', {
      fitBounds: false,
      identifiedElements: [self._elementsByQuery[query]],
      type: AbstractDbOverlay.QueryType.SEARCH_BY_TARGET
    });
  }).then(function () {
    return Promise.resolve(self._elementsByQuery[query]);
  });
};

/**
 *
 * @returns {Promise}
 */
SearchDbOverlay.prototype.getIdentifiedElements = function () {
  var self = this;
  var queries = self.getQueries();
  var result = [];

  return Promise.each(queries, function (query, index) {
    var identifiedElements = self._elementsByQuery[query];
    return self.getMap().fetchIdentifiedElements(identifiedElements, true).then(function (elements) {
      var iconCounter = 1;
      var groups = [];
      for (var i = 0; i < elements.length; i++) {
        var element = elements[i];
        var alreadyExists = false;
        var icon = null;
        var group;
        for (var j = 0; j < groups.length; j++) {
          group = groups[j];
          if (group.bioEntityMatch(element)) {
            alreadyExists = true;
            group.addBioEntity(element);
            icon = group.getIcon();
          }
        }

        if (!alreadyExists) {
          if (element instanceof Alias) {
            icon = self.getIcon(index, iconCounter++)
          }
          group = new SearchBioEntityGroup(element);
          group.setIcon(icon);
          groups.push(group);

        }
        var ie = new IdentifiedElement(element);
        if (element instanceof Alias) {
          ie.setIcon(icon);
        } else if (!(element instanceof Reaction)) {
          throw new Error("Unknown element type: " + ie.getType());
        }
        result.push(ie);

      }
    });

  }).then(function () {
    return result;
  });
};

/**
 *
 * @param element
 * @returns {Promise<BioEntity>}
 */
SearchDbOverlay.prototype.getDetailDataByIdentifiedElement = function (element) {
  if (element.getType() === "POINT") {
    return Promise.resolve(null);
  }
  var model = this.getMap().getSubmapById(element.getModelId()).getModel();
  return model.getByIdentifiedElement(element, true);
};

module.exports = SearchDbOverlay;
