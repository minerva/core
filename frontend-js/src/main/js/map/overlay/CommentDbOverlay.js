"use strict";

/* exported logger */

var Promise = require("bluebird");

var AbstractDbOverlay = require('./AbstractDbOverlay');
var ServerConnector = require('../../ServerConnector');

var logger = require('../../logger');

/**
 *
 * @param params
 * @constructor
 * @extends AbstractDbOverlay
 */
function CommentDbOverlay(params) {
  // call super constructor
  AbstractDbOverlay.call(this, params);

  this._elements = [];
  this._detailDataByIdentifiedElement = [];
  this.registerListenerType('onRefresh');
}

CommentDbOverlay.prototype = Object.create(AbstractDbOverlay.prototype);
CommentDbOverlay.prototype.constructor = CommentDbOverlay;

/**
 *
 * @returns {Promise}
 */
CommentDbOverlay.prototype.refresh = function () {
  var self = this;
  return ServerConnector.getLightComments({}).then(function (comments) {
    self._elements = comments;
    return self.callListeners('onRefresh');
  }).then(function () {
    return self._elements;
  });
};

/**
 *
 * @param {Comment} newComment
 * @returns {PromiseLike}
 */
CommentDbOverlay.prototype.addComment = function (newComment) {
  var self = this;
  self._elements.push(newComment);
  return self.callListeners('onRefresh').then(function () {
    return self._elements;
  });
};

/**
 *
 * @returns {PromiseLike}
 */
CommentDbOverlay.prototype.clear = function () {
  var self = this;
  self._elements = [];
  self._detailDataByIdentifiedElement = [];
  return self.callListeners('onClear');
};

/**
 *
 * @param {IdentifiedElement} element
 * @returns {Promise}
 */
CommentDbOverlay.prototype.getDetailDataByIdentifiedElement = function (element) {
  var self = this;
  var elementKey = element.getId() + "," + element.getType() + "," + element.getModelId();
  if (this._detailDataByIdentifiedElement[elementKey] !== undefined) {
    return Promise.resolve(this._detailDataByIdentifiedElement[elementKey]);
  } else {
    var coordinates;
    if (element.getType() === "POINT") {
      coordinates = element.getPoint().x.toFixed(2) + "," + element.getPoint().y.toFixed(2);
    }
    return ServerConnector.getComments({
      elementId: element.getId(),
      elementType: element.getType(),
      coordinates: coordinates
    }).then(function (comments) {
      self._detailDataByIdentifiedElement[elementKey] = comments;
      return comments;
    });
  }
};

/**
 * TODO this method can be simplified (instead of new Promise, Promise.resolve should be used)
 * @returns {Promise}
 */
CommentDbOverlay.prototype.getIdentifiedElements = function () {
  var self = this;
  return new Promise(function (resolve) {
    var result = [];
    for (var i = 0; i < self._elements.length; i++) {
      // we return only elements that are pinned to the map and weren't removed
      var comment = self._elements[i];
      if (!comment.isRemoved() && comment.isPinned()) {
        result.push(comment.getIdentifiedElement());
      }
    }
    resolve(result);
  });
};

module.exports = CommentDbOverlay;
