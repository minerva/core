"use strict";

/* exported logger */

var logger = require('../../logger');

var AbstractTargettingDbOverlay = require('./AbstractTargettingDbOverlay');

var ServerConnector = require('../../ServerConnector');
var GuiConnector = require('../../GuiConnector');

var Promise = require('bluebird');

/**
 *
 * @param params
 * @constructor
 * @extends AbstractTargettingDbOverlay
 */
function ChemicalDbOverlay(params) {
  params.iconType = "ball";
  params.iconColorStart = 1;
  // call super constructor
  AbstractTargettingDbOverlay.call(this, params);

}

ChemicalDbOverlay.prototype = Object.create(AbstractTargettingDbOverlay.prototype);
ChemicalDbOverlay.prototype.constructor = ChemicalDbOverlay;

/**
 *
 * @param {IdentifiedElement} param
 * @returns {Promise<string[]>}
 */
ChemicalDbOverlay.prototype.getNamesByTargetFromServer = function (param) {
  return ServerConnector.getChemicalNamesByTarget(param);
};

/**
 *
 * @param {string} param.query
 * @returns {Promise<Chemical[]>}
 */
ChemicalDbOverlay.prototype.getElementsByQueryFromServer = function (param) {
  var self = this;
  if (self.getMap().getProject().getDisease() !== undefined && self.getMap().getProject().getDisease() !== null) {
    return ServerConnector.getChemicalsByQuery(param);
  } else {
    if (param.query !== "") {
      GuiConnector.warn("Cannot search for chemicals in a project without defined disease.");
    }
    return Promise.resolve([]);
  }
};

module.exports = ChemicalDbOverlay;
