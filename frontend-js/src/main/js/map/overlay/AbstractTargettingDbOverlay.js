"use strict";

/* exported logger */

var logger = require('../../logger');

var Promise = require("bluebird");

var IdentifiedElement = require('../data/IdentifiedElement');
var AbstractDbOverlay = require('./AbstractDbOverlay');

/**
 *
 * @param {Object} params
 * @param {CustomMap} params.map
 * @param {string} params.name
 * @param {boolean} params.allowGeneralSearch
 * @param {boolean} params.allowSearchById
 * @param {string} params.iconType
 * @param {number} params.iconColorStart
 * @constructor
 * @extends AbstractDbOverlay
 */
function AbstractTargettingDbOverlay(params) {
  // call super constructor
  AbstractDbOverlay.call(this, params);
}

AbstractTargettingDbOverlay.prototype = Object.create(AbstractDbOverlay.prototype);
AbstractTargettingDbOverlay.prototype.constructor = AbstractTargettingDbOverlay;

/**
 *
 * @param {string} query
 * @returns {Promise}
 */
AbstractTargettingDbOverlay.prototype.getElementsByQuery = function (query) {
  var self = this;
  return new Promise(function (resolve) {
    var i;
    var queryId = null;
    var queries = self.getQueries();
    for (i = 0; i < queries.length; i++) {
      if (queries[i] === query) {
        queryId = i;
      }
    }
    if (queryId === null) {
      throw new Error("Invalid query: " + query);
    }
    var colourId = queryId;
    var elements = self._elementsByQuery[query];

    var result = [];
    if (elements.length > 0) {
      if (elements.length > 1) {
        logger.warn("More than one element per query not implemented");
      }
      var element = elements[0];
      result.element = element;
      var iconCounter = 1;
      for (i = 0; i < element.getTargets().length; i++) {
        var target = element.getTargets()[i];
        var icon = self.getIcon(colourId, iconCounter++);
        if (target.getTargetElements().length === 0) {
          icon = null;
        }
        result.push({
          element: target,
          icon: icon
        });
      }
    }
    resolve(result);
  });
};

/**
 *
 * @param {string} originalQuery
 * @returns {Promise}
 */
AbstractTargettingDbOverlay.prototype.searchBySingleQuery = function (originalQuery) {
  var self = this;
  var query = self.encodeQuery(AbstractDbOverlay.QueryType.SEARCH_BY_QUERY, originalQuery);
  if (self._elementsByQuery[query] !== undefined) {
    return Promise.resolve(self._elementsByQuery[query]);
  } else {
    return self.getElementsByQueryFromServer({
      query: originalQuery
    }).then(function (elements) {
      self._elementsByQuery[query] = elements;

      var identifiedElements = [];
      for (var i = 0; i < elements.length; i++) {
        var targets = elements[i].getTargets();
        for (var j = 0; j < targets.length; j++) {
          var target = targets[j];
          for (var k = 0; k < target.targetElements; k++) {
            identifiedElements.push(new IdentifiedElement(target.targetElements[i]));
          }
        }
      }
      return self.getMap().fetchIdentifiedElements(identifiedElements, true);
    }).then(function () {
      return self._elementsByQuery[query];
    });
  }
};

/**
 *
 * @param {number} mapId
 * @returns {Promise}
 */
AbstractTargettingDbOverlay.prototype.searchByTargetMapId = function (mapId) {
  return Promise.reject(new Error("Not implemented"));
};

/**
 * @returns {Promise}
 */
AbstractTargettingDbOverlay.prototype.getIdentifiedElements = function () {
  var self = this;

  return new Promise(function (resolve) {
    var queries = self.getQueries();
    var result = [];
    var colourId = 0;
    for (var i = 0; i < queries.length; i++) {
      var query = queries[i];
      var elements = self._elementsByQuery[query];

      for (var j = 0; j < elements.length; j++) {
        var element = elements[j];

        var targetElements = self.createIdentifiedElementsForTargetingClass(element, colourId);

        result.push.apply(result, targetElements);

        colourId++;
      }
    }
    resolve(result);
  });
};

/**
 *
 * @param {IdentifiedElement} element
 * @returns {Promise}
 */
AbstractTargettingDbOverlay.prototype.searchByTarget = function (element) {
  return this.searchNamesByTarget(element);
};

/**
 *
 * @param {IdentifiedElement} element
 * @returns {string[]}
 */
AbstractTargettingDbOverlay.prototype.searchNamesByTarget = function (element) {
  var self = this;
  var query = self.encodeQuery(AbstractDbOverlay.QueryType.SEARCH_BY_TARGET, element);

  if (self._elementsByQuery[query] !== undefined) {
    return Promise.resolve(self._elementsByQuery[query]);
  } else {
    return self.getNamesByTargetFromServer({
      target: element
    }).then(function (drugNames) {
      self._elementsByQuery[query] = drugNames;
      return self._elementsByQuery[query];
    });
  }
};

/**
 *
 * @param {IdentifiedElement} element
 * @param {boolean} general
 * @returns {Promise}
 */
AbstractTargettingDbOverlay.prototype.getDetailDataByIdentifiedElement = function (element, general) {
  var self = this;
  if (general) {
    return self.searchNamesByTarget(element).then(function (names) {
      var promises = [];
      names.forEach(function (name) {
        promises.push(self.searchBySingleQuery(name));
      });
      return Promise.all(promises);
    }).then(function (drugs) {
      var result = [];
      drugs.forEach(function (drugList) {
        drugList.forEach(function (drug) {
          result.push(drug);
        });
      });
      return result;
    });
  } else {
    return new Promise(function (resolve) {
      var drugNames = [];
      var result = [];
      var queries = self.getQueries();
      for (var i = 0; i < queries.length; i++) {
        var drugs = self._elementsByQuery[queries[i]];
        if (drugs === undefined) {
          drugs = [];
        }
        for (var j = 0; j < drugs.length; j++) {
          var drug = drugs[j];
          var targets = drug.getTargets();
          for (var k = 0; k < targets.length; k++) {
            var elements = targets[k].getTargetElements();
            for (var l = 0; l < elements.length; l++) {
              if (element.equals(elements[l]) && drugNames[drug.getName()] === undefined) {
                result.push(drug);
                drugNames[drug.getName()] = true;
              }
            }
          }
        }
      }
      resolve(result);
    });
  }
};

module.exports = AbstractTargettingDbOverlay;
