"use strict";

var $ = require('jquery');

var Promise = require("bluebird");

var logger = require('../logger');
var Functions = require('../Functions');

var AbstractCustomMap = require('./AbstractCustomMap');
var Alias = require('./data/Alias');
var CommentDialog = require('../gui/CommentDialog');
var ConfigurationType = require('../ConfigurationType');
var CustomMapOptions = require('./CustomMapOptions');
var DataOverlay = require('./data/DataOverlay');
var IdentifiedElement = require('./data/IdentifiedElement');
var OverviewDialog = require('../gui/OverviewDialog');
var Reaction = require('./data/Reaction');
var ReferenceGenome = require('./data/ReferenceGenome');
var SecurityError = require('../SecurityError');
var Submap = require('./Submap');
var ValidationError = require('../ValidationError');

var Bounds = require('./canvas/Bounds');
var Point = require('./canvas/Point');

/**
 * Default constructor.
 *
 * @param {CustomMapOptions} options
 *          CustomMapOptions object representing all parameters needed for map
 *          creation
 *
 * @constructor
 * @extends AbstractCustomMap
 */
function CustomMap(options) {
  if (!(options instanceof CustomMapOptions)) {
    options = new CustomMapOptions(options);
  }
  AbstractCustomMap.call(this, options.getProject().getModels()[0], options);

  this.registerListenerType("onBioEntityClick");
  this.registerListenerType("onShowOverlay");
  this.registerListenerType("onHideOverlay");
  this.registerListenerType("onRedrawSelectedOverlays");
  this.registerListenerType("onBackgroundOverlayChange");
  this.registerListenerType("onSubmapOpen");
  this.registerListenerType("onSubmapClose");

  // @type {boolean[]}
  this._selectedOverlays = [];

  this.customizeMapView(options.getElement());

  this.createMapChangedCallbacks();

  /**
   *
   * @type {Object.<string, AbstractDbOverlay>}
   */
  this.overlayCollections = {};

  // which submap is active (where user made interaction for the last time)
  // @type {number}
  this._activeSubmapId = null;

  // @type {boolean}
  this.initialized = true;

  // list of reference genomes
  this._referenceGenome = [];

  this.createSubmaps();

  //@type {HTMLElement[]}
  this._dialogs = [];
}

CustomMap.prototype = Object.create(AbstractCustomMap.prototype);
CustomMap.prototype.constructor = CustomMap;

/**
 * This code must be run after the object is created. It requires to download
 * some data via promises.
 *
 * @return {PromiseLike} with empty result
 */
CustomMap.prototype.init = function () {
  var self = this;
  var sessionData = self.getServerConnector().getSessionData(self.getProject());

  // if we have background overlay stored in the session then restore it
  var mapType = sessionData.getSelectedBackgroundOverlay();
  // if we have user data overlays stored in the session then restore it
  var ids = sessionData.getVisibleOverlays();
  if (mapType === undefined || isNaN(mapType)) {
    self.getProject().getBackgrounds().forEach(function (background) {
      if (background.isDefaultOverlay()) {
        mapType = background.getId();
      }
    });
  }
  if (mapType !== undefined && !isNaN(mapType)) {
    self.openBackground(mapType);
  }

  // center map and zoom in to fit into browser window if there is no
  // information about coordinates in the session
  var x = self.getModel().getDefaultCenterX();
  var y = self.getModel().getDefaultCenterY();
  var zoom = self.getModel().getDefaultZoomLevel();
  var autoFit = false;
  if (self.getServerConnector().getSessionData(self.getProject()).getCenter(self.getModel()) === undefined &&
    (x === undefined || y === undefined || x === null || y === null) &&
    (zoom === undefined || zoom === null)) {
    autoFit = true;
  }

  return Promise.all([self.getMapCanvas().triggerListeners('resize'),
    // noinspection SpellCheckingInspection
    self.getMapCanvas().triggerListeners('maptypeid_changed'),
    self.getMapCanvas().triggerListeners('projection_changed')
  ]).then(function () {

    if (autoFit) {
      var bounds = new Bounds();
      bounds.extend(self.getTopLeft());
      bounds.extend(self.getBottomRight());

      self.getMapCanvas().fitBounds(bounds);
    }

    return Promise.each(ids, function (overlayId) {
      return self.openDataOverlay(overlayId).catch(function (e) {
        if (e instanceof SecurityError) {
          logger.debug(e.message);
          if (self.getProject().getBackgrounds().length === 0) {
            if (self.getProject().getStatus() === "Archived") {
              return Promise.reject(new ValidationError("Project is archived."));
            } else {
              return Promise.reject(new ValidationError("Project doesn't have a background defined. Please re-upload map in admin panel."));
            }
          }
          sessionData.setSelectedBackgroundOverlay(self.getProject().getBackgrounds()[0].getId());
        } else {
          return Promise.reject(e);
        }
      });
    });
  });
};

/**
 * Create submaps.
 */
CustomMap.prototype.createSubmaps = function () {
  var self = this;
  self.submaps = [];
  for (var i = 1; i < self.getProject().getModels().length; i++) {
    var submap = new Submap(self, self.getProject().getModels()[i]);
    submap.addListener("onClose", function (event) {
      return self.callListeners("onSubmapClose", {mapId: event.object.getId()});
    });
    self.submaps.push(submap);
  }
};

/**
 * Creates logo and put it on the map.
 */
CustomMap.prototype.createLogo = function () {
  var self = this;
  var logoControlDiv2 = Functions.createElement({type: "div", index: 0});
  var logo2 = Functions.createElement({
    type: 'IMG',
    className: "minerva-logo",
    title: self.getConfiguration().getOption(ConfigurationType.LEFT_LOGO_TEXT).getValue(),
    src: self.getConfiguration().getOption(ConfigurationType.LEFT_LOGO_IMG).getValue(),
    onclick: function () {
      var win = window.open(self.getConfiguration().getOption(ConfigurationType.LEFT_LOGO_LINK).getValue(), '_blank');
      win.focus();
    }
  });
  logoControlDiv2.appendChild(logo2);
  this.getMapCanvas().addLeftBottomControl(logoControlDiv2);

  var logoControlDiv = Functions.createElement({type: "div", index: 1});
  var logo = Functions.createElement({
    type: "IMG",
    title: self.getConfiguration().getOption(ConfigurationType.RIGHT_LOGO_TEXT).getValue(),
    src: self.getConfiguration().getOption(ConfigurationType.RIGHT_LOGO_IMG).getValue(),
    className: "minerva-logo",
    onclick: function () {
      var win = window.open(self.getConfiguration().getOption(ConfigurationType.RIGHT_LOGO_LINK).getValue(), '_blank');
      win.focus();
    }
  });
  logoControlDiv.appendChild(logo);
  this.getMapCanvas().addRightBottomControl(logoControlDiv);

  this.getMapCanvas().addRightTopControl(Functions.createElement({
    type: "a",
    content: "<i class='fa fa-crosshairs'></i>&nbsp;",
    title: "center map",
    className: "minerva-center-map-button",
    href: "#",
    onclick: function () {
      var submaps = self.getSubmaps();
      var bounds;
      for (var i = 0; i < submaps.length; i++) {
        var submap = submaps[i];
        if (submap.isInitialized()) {
          bounds = new Bounds(new Point(0, 0), new Point(submap.getModel().getWidth(), self.getModel().getHeight()));
          submap.getMapCanvas().fitBounds(bounds);
        }
      }
      bounds = new Bounds(new Point(0, 0), new Point(self.getModel().getWidth(), self.getModel().getHeight()));
      return self.getMapCanvas().fitBounds(bounds);
    },
    xss: false
  }));
};

/**
 * Clear all AbstractDbOverlay.
 *
 * @returns {PromiseLike}
 */
CustomMap.prototype.clearDbOverlays = function () {
  var promises = [];
  var overlays = this.getDbOverlays();
  for (var i = 0; i < overlays.length; i++) {
    promises.push(overlays[i].clear());
  }
  return Promise.all(promises);
};

/**
 * Open data overlay given in the parameter.
 *
 * @param {DataOverlay|number|string} param
 *          identifier or DataOverlay identifying data overlay to open
 *
 * @returns {PromiseLike|Promise}
 */
CustomMap.prototype.openDataOverlay = function (param) {
  logger.debug("Opening data overlay: " + param);

  var self = this;
  var identifier = param;
  if (param instanceof DataOverlay) {
    identifier = param.getId();
  }
  identifier = parseInt(identifier);

  if (isNaN(identifier)) {
    return Promise.reject(new Error("invalid id: " + param));
  }

  var overlayToOpen = null;
  var overlays = self.getProject().getDataOverlays();
  for (var j = 0; j < overlays.length; j++) {
    var overlay = overlays[j];
    if (overlay.getId() === identifier) {
      overlayToOpen = overlay;
    }
  }
  if (overlayToOpen === null) {
    return Promise.reject(new SecurityError("You have no privileges for selected overlay"));
  } else {
    return overlayToOpen.init().then(function () {
      if (self._selectedOverlays[identifier] === true) {
        logger.warn("Overlay " + identifier + " already selected");
        return Promise.resolve();
      } else {
        self._selectedOverlays[identifier] = true;
        return self.getVisibleDataOverlays().then(function (visibleDataOverlays) {
          var ids = [];
          for (var i = 0; i < visibleDataOverlays.length; i++) {
            ids.push(visibleDataOverlays[i].getId());
          }
          self.getServerConnector().getSessionData(self.getProject()).setVisibleOverlays(ids);
          return self.redrawSelectedDataOverlays();
        }).then(function () {
          var backgroundToOpen;
          var backgrounds = self.getProject().getBackgrounds();
          for (var i = 0; i < backgrounds.length; i++) {
            var background = backgrounds[i];
            if (background.getName().toLowerCase() === "empty") {
              backgroundToOpen = background;
            }
          }
          if (backgroundToOpen === undefined) {
            logger.warn("Cannot find empty background overlay");
          } else {
            return self.openBackground(backgroundToOpen.getId());
          }
        });
      }
    }).then(function () {
      return self.callListeners("onShowOverlay", overlayToOpen);
    });
  }
};

/**
 * @param {number} identifier
 */
CustomMap.prototype.openBackground = function (identifier) {
  if (identifier.toString() !== this.getMapCanvas().getBackgroundId()) {
    var backgroundExists = false;
    this.getProject().getBackgrounds().forEach(function (background) {
      if (background.getId() === identifier) {
        backgroundExists = true;
      }
    });
    if (backgroundExists) {
      this.getMapCanvas().setBackgroundId(identifier.toString());

      var submaps = this.getSubmaps();
      for (var i = 0; i < submaps.length; i++) {
        var submap = submaps[i];
        submap.openDataOverlay(identifier);
      }
      return this.callListeners("onBackgroundOverlayChange", identifier);
    } else {
      logger.warn("Background " + identifier + " does not exist");
    }
  }
  return Promise.resolve();
}
/**
 * @returns {Background}
 */
CustomMap.prototype.getBackgroundDataOverlay = function () {
  var identifier = parseInt(this.getMapCanvas().getBackgroundId());
  var backgrounds = this.getProject().getBackgrounds();
  for (var i = 0; i < backgrounds.length; i++) {
    var background = backgrounds[i];
    if (background.getId() === identifier) {
      return background;
    }
  }
  return null;
};

/**
 * Register AbstractDbOverlay in the map.
 *
 * @param {AbstractDbOverlay} dbOverlay
 *          database overlay to be connected to the map
 */
CustomMap.prototype.registerDbOverlay = function (dbOverlay) {
  var self = this;

  this.overlayCollections[dbOverlay.getName()] = dbOverlay;


  if (dbOverlay.getName() !== undefined) {
    if (dbOverlay.getName() === "search" || dbOverlay.getName().indexOf("plugin") === 0) {
      dbOverlay.addListener("onSearch", function (e) {
        return self.renderOverlayCollection({
          overlayCollection: dbOverlay,
          fitBounds: e.arg.fitBounds
        });
      });
    }
    if (dbOverlay.getName() === "drug" || dbOverlay.getName() === "chemical") {
      dbOverlay.addListener("onSearch", function (e) {
        return self.renderOverlayCollection({
          overlayCollection: dbOverlay,
          fitBounds: e.arg.fitBounds
        });
      });

      dbOverlay.addListener("onTargetVisibilityChange", function () {
        return self.renderOverlayCollection({
          overlayCollection: dbOverlay,
          fitBounds: false
        });
      });
    }
    if (dbOverlay.getName() === "comment") {
      var listener = function () {
        return self.renderOverlayCollection({
          overlayCollection: dbOverlay,
          fitBounds: false
        });
      };
      dbOverlay.addListener("onRefresh", listener);
      dbOverlay.addListener("onClear", listener);
    }
  }

};

/**
 * Refresh comment list.
 *
 * @return {Promise} promise that is resolved when comment list is refreshed
 */
CustomMap.prototype.refreshComments = function () {
  var self = this;

  var commentDbOverlay = self.getOverlayByName("comment");
  if (commentDbOverlay !== undefined) {
    if (self.getServerConnector().getSessionData(self.getProject()).getShowComments()) {
      return commentDbOverlay.refresh();
    } else {
      return commentDbOverlay.clear();
    }
  }
  return Promise.reject(new Error("comment DbOverlay not found"));
};

/**
 *
 * @returns {PromiseLike}
 */
CustomMap.prototype.refreshMarkers = function () {
  var promises = [];
  logger.debug("Refresh Markers: ");

  var overlays = this.getDbOverlays();
  for (var i = 0; i < overlays.length; i++) {
    promises.push(this.refreshOverlayMarkers(overlays[i]));
  }

  return Promise.all(promises);
};

/**
 *
 * @param {AbstractDbOverlay} overlay
 * @returns {PromiseLike}
 */
CustomMap.prototype.refreshOverlayMarkers = function (overlay) {
  var promises = [];
  var self = this;
  logger.debug("Refresh overlay: " + overlay.getName());

  promises.push(self.getMarkerSurfaceCollection().refreshOverlayMarkers(overlay));
  var submaps = self.getSubmaps();
  for (var i = 0; i < submaps.length; i++) {
    if (submaps[i].isInitialized()) {
      promises.push(submaps[i].getMarkerSurfaceCollection().refreshOverlayMarkers(overlay));
    }
  }
  return Promise.all(promises);
};

/**
 * Returns HTML div that is attached to jQuery dialog that should be used for
 * opening submap.
 *
 * @param {number} id
 *          identifier of the submap
 *
 * @returns {HTMLElement}
 */
CustomMap.prototype.getSubmapDialogDiv = function (id) {
  var dialogDiv = this._dialogs[id];

  if (dialogDiv === undefined) {
    dialogDiv = document.createElement("div");
    dialogDiv.setAttribute("name", "dialog-" + id);

    this._dialogs[id] = dialogDiv;

    $(dialogDiv).dialog({
      dialogClass: 'minerva-submap',
      autoOpen: false
    });
  }
  return dialogDiv;
};

/**
 * Opens a dialog with a submodel.
 *
 * @param {number} id
 * @returns {PromiseLike}
 */
CustomMap.prototype.openSubmap = function (id) {
  var self = this;
  var submap = self.getSubmapById(id);
  if (submap === null) {
    throw new Error("Unknown submap for id: " + id);
  } else if (submap !== this) {
    var wasInitialized = submap.isInitialized();

    var dialogDiv = self.getSubmapDialogDiv(id);

    submap.open(dialogDiv);

    var promise = Promise.resolve();
    if (!wasInitialized) {
      // now we have to visualize overlays
      promise = self.getVisibleDataOverlays().then(function (overlays) {
        var promises = [];
        // show overlays that should be visualized (resize or show them)
        for (var i = 0; i < overlays.length; i++) {
          promises.push(submap._showSelectedDataOverlay(overlays[i].getId(), i, overlays.length));
        }
        return Promise.all(promises);
      }).then(function () {
        return self.refreshMarkers();
      });
    }
    return promise.then(function () {
      return self.callListeners("onSubmapOpen", {mapId: id})
    });
  } else {
    return Promise.resolve();
  }
};

/**
 *
 * @param {HTMLElement} div
 */
CustomMap.prototype.customizeMapView = function (div) {
  var self = this;

  self.createMapCanvas(div);

  self.createLogo();
};

/**
 * Creates listeners for map object that will actualize the data in
 * user session.
 */
CustomMap.prototype.createMapChangedCallbacks = function () {
  this._createMapChangedCallbacks();
  var self = this;
  var sessionData = self.getServerConnector().getSessionData(self.getProject());

  // listener for changing type of background overlay
  // noinspection SpellCheckingInspection
  this.getMapCanvas().addListener('maptypeid_changed', function () {
    sessionData.setSelectedBackgroundOverlay(self.getMapCanvas().getBackgroundId());
  });

};

/**
 * Returns submap (or this map) by id.
 *
 * @param {number|string} identifier
 *          identifier of the submap
 * @returns {AbstractCustomMap} (submap or this map) with given identifier of the model
 */
CustomMap.prototype.getSubmapById = function (identifier) {
  identifier = parseInt(identifier);
  if (this.getId() === identifier) {
    return this;
  }
  for (var i = 0; i < this.submaps.length; i++) {
    if (this.submaps[i].getId() === identifier) {
      return this.submaps[i];
    }
  }
  logger.warn("Cannot find submodel with id: " + identifier);
  return null;
};

/**
 * Removes overlay from visualization.
 *
 * @param {number} identifier
 *          identifier of overlay to remove
 *
 * @returns {PromiseLike}
 */
CustomMap.prototype.hideDataOverlay = function (identifier) {
  var self = this;
  logger.debug("Hiding overlay: " + identifier);

  if (this._selectedOverlays[identifier] !== true) {
    logger.warn("Overlay " + identifier + " is not selected");
    return Promise.resolve();
  } else {
    self._selectedOverlays[identifier] = false;
    return self.redrawSelectedDataOverlays().then(function () {
      return self.getVisibleDataOverlays();
    }).then(function (visibleDataOverlays) {
      var ids = [];
      for (var i = 0; i < visibleDataOverlays.length; i++) {
        ids.push(visibleDataOverlays[i].getId());
      }
      self.getServerConnector().getSessionData(self.getProject()).setVisibleOverlays(ids);
      return self.redrawSelectedDataOverlays();
    }).then(function () {
      return self.getProject().getDataOverlayById(identifier);
    }).then(function (overlay) {
      return self.callListeners("onHideOverlay", overlay);
    });
  }
};

/**
 * Redrawing selected overlays.
 *
 * @returns {PromiseLike|Promise}
 */
CustomMap.prototype.redrawSelectedDataOverlays = function () {
  logger.debug("Redrawing overlays");
  var self = this;
  return self.getVisibleDataOverlays().then(function (visibleDataOverlays) {
    // show overlays that should be visualized (resize or show them)
    var promises = [];
    for (var i = 0; i < visibleDataOverlays.length; i++) {
      var overlayId = visibleDataOverlays[i].getId();
      if (self.layoutContainsOverlays(overlayId)) {
        // resize element on the map
        promises.push(self.resizeSelectedDataOverlay(overlayId, i, visibleDataOverlays.length));
      } else {
        promises.push(self.showSelectedDataOverlay(overlayId, i, visibleDataOverlays.length));
      }
    }
    return Promise.all(promises);
  }).then(function () {
    var promises = [];
    // hide overlays that were visible
    var overlays = self.getProject().getDataOverlays();
    for (var i = 0; i < overlays.length; i++) {
      var id = overlays[i].getId();
      if (!self._selectedOverlays[id] && self.layoutContainsOverlays(id)) {
        promises.push(self.hideSelectedLayout(id));
      }
    }
    return Promise.all(promises);
  }).then(function () {
    return self.callListeners("onRedrawSelectedOverlays");
  });
};

/**
 * Hides overlay on the map and all submaps
 *
 * @param {number} overlayId
 *          identifier of a overlay to hide
 *
 * @returns {Promise}
 */
CustomMap.prototype.hideSelectedLayout = function (overlayId) {
  var promises = [];
  promises.push(this._hideSelectedLayout(overlayId));
  for (var i = 0; i < this.submaps.length; i++) {
    promises.push(this.submaps[i]._hideSelectedLayout(overlayId));
  }
  return Promise.all(promises);
};

/**
 * Resize(refresh) overlay on the map and all submaps. Resizing should be called
 * when number of overlays to visualize change.
 *
 * @param {number} overlayId
 *          identifier of overlay to refresh
 * @param {number} index
 *          position of the overlay in list of overlays that we visualize
 * @param {number} length
 *          number of overlays that we currently visualize
 *
 * @returns {PromiseLike}
 */
CustomMap.prototype.resizeSelectedDataOverlay = function (overlayId, index, length) {
  logger.debug("Resize overlay: " + overlayId);
  var promises = [];
  promises.push(this._resizeSelectedDataOverlay(overlayId, index, length));
  for (var i = 0; i < this.submaps.length; i++) {
    promises.push(this.submaps[i]._resizeSelectedDataOverlay(overlayId, index, length));
  }
  return Promise.all(promises);
};

/**
 * Show overlay on the map and all submaps.
 *
 * @param {number} overlayId
 *          identifier of overlay to show
 * @param {number} index
 *          position of the overlay in list of overlays that we visualize
 * @param {number} length
 *          number of overlays that we currently visualize
 *
 * @returns {PromiseLike}
 */
CustomMap.prototype.showSelectedDataOverlay = function (overlayId, index, length) {
  logger.debug("Show overlay: " + overlayId);
  var promises = [];
  promises.push(this._showSelectedDataOverlay(overlayId, index, length));

  for (var i = 0; i < this.submaps.length; i++) {
    promises.push(this.submaps[i]._showSelectedDataOverlay(overlayId, index, length));
  }
  return Promise.all(promises);
};

/**
 * This method checks if the overlay contains any overlays (like AliasSurface or
 * ReactionSurface) that is currently visible on the map.
 *
 * @param {number} overlayId
 *          identifier of the overlay
 * @returns {boolean} <code>true</code> if the overlay contains overlays to
 *          visualize, <code>false</code> otherwise
 */
CustomMap.prototype.layoutContainsOverlays = function (overlayId) {

  // first, check top map
  if (this.selectedLayoutOverlays[overlayId] !== undefined && this.selectedLayoutOverlays[overlayId].length > 0) {
    return true;
  }

  // now check all submaps
  for (var i = 0; i < this.submaps.length; i++) {
    if (this.submaps[i].isInitialized()) {
      if (this.submaps[i].selectedLayoutOverlays[overlayId] !== undefined
        && this.submaps[i].selectedLayoutOverlays[overlayId].length > 0) {
        return true;
      }
    }
  }
  return false;
};

/**
 * Renders markers, lines, etc. for elements highlighted in OverlayCollection.
 *
 * @param {boolean} [params.fitBounds=false] <code>true</code> if the borders should fit bounds after creating
 *          all elements
 * @param {AbstractDbOverlay} params.overlayCollection to be processed
 *
 * @param params
 * @returns {Promise}
 */
CustomMap.prototype.renderOverlayCollection = function (params) {
  var self = this;
  var fitBounds = params.fitBounds;
  var overlayCollection = params.overlayCollection;

  var elements;
  var submaps = self.getSubmaps().concat([self]);

  return overlayCollection.getIdentifiedElements().then(function (identifiedElements) {
    return self.appendElementsPointingToSubmap(identifiedElements, overlayCollection);
  }).then(function (identifiedElements) {
    elements = identifiedElements;
    var promises = [];
    for (var i = 0; i < submaps.length; i++) {
      promises.push(submaps[i].getMarkerSurfaceCollection().renderOverlay(identifiedElements, overlayCollection));
    }
    return Promise.all(promises);
  }).then(function () {
    return self.fetchIdentifiedElements(elements, true);
  }).then(function (fullElements) {
    var promises = [];
    if (elements.length > 0 && fitBounds) {
      for (var j = 0; j < submaps.length; j++) {
        promises.push(submaps[j].fitBounds(fullElements));
      }
    }
    return Promise.all(promises);
  });
};

/**
 *
 * @param {IdentifiedElement[]} identifiedElements
 * @param {AbstractDbOverlay} overlayDb
 * @returns {PromiseLike}
 */
CustomMap.prototype.appendElementsPointingToSubmap = function (identifiedElements, overlayDb) {
  var self = this;
  var existingElements = {};
  var i;
  for (i = 0; i < identifiedElements.length; i++) {
    var element = identifiedElements[i];
    existingElements[element.getId() + "_" + element.getIcon() + "_" + element.getType()] = true;
  }
  return Promise.each(identifiedElements, function (ie) {
    return self.getProject().getElementsPointingToSubmap(ie.getModelId()).then(function (elementsPointingToSubmap) {
      for (var j = 0; j < elementsPointingToSubmap.length; j++) {
        var element = elementsPointingToSubmap[j];
        if (!existingElements[element.getId() + "_" + ie.getIcon() + "_" + element.getType()]) {
          existingElements[element.getId() + "_" + ie.getIcon() + "_" + element.getType()] = true;
          var newElement = new IdentifiedElement(elementsPointingToSubmap[j]);
          if (ie.getIcon() === undefined) {
            newElement.setIcon(overlayDb.getIcon(0));
          } else {
            newElement.setIcon(ie.getIcon());
          }

          identifiedElements.push(newElement);
        }
      }
    })
  });
};


/**
 * Opens {@link AbstractInfoWindow} for a marker.
 *
 * @param {Marker} [marker]
 *           for which info window should be opened
 * @param {IdentifiedElement} element
 *           for which info window should be opened
 *
 * @returns {PromiseLike<AbstractInfoWindow>|Promise<AbstractInfoWindow>}
 */
CustomMap.prototype.openInfoWindowForIdentifiedElement = function (element, marker) {
  var self = this;
  var submap = self.getSubmapById(element.getModelId());
  logger.debug(element + ": Opening info window");

  var infoWindow;
  return submap._openInfoWindowForIdentifiedElement(element, marker).then(function () {
    infoWindow = submap.returnInfoWindowForIdentifiedElement(element);
    return self.retrieveOverlayDetailDataForElement(element, infoWindow.getOverlayFullViewArray());
  }).then(function () {
    return infoWindow;
  });
};

/**
 * Sends requests to download detailed data about element in all
 * {OverlayCollection}.
 *
 * @param {IdentifiedElement} element
 *          element for which we want to have detailed information
 * @param {Object.<string,boolean>} general
 */
CustomMap.prototype.retrieveOverlayDetailDataForElement = function (element, general) {
  var promises = [];
  if (general === undefined) {
    logger.warn("general param is undefined!");
    general = {};
  }
  var overlays = this.getDbOverlays();
  for (var i = 0; i < overlays.length; i++) {
    var overlay = overlays[i];

    var generalRequest = general[overlay.getName()];
    if (generalRequest === undefined) {
      logger.warn("No information about general overlay request for overlay: " + overlay.getName());
      generalRequest = false;
    }
    generalRequest = generalRequest || (!overlay.allowSearchById() && overlay.allowGeneralSearch());

    promises.push(overlay.getDetailDataByIdentifiedElement(element, generalRequest));
  }
  return Promise.all(promises);
};

/**
 * Returns data from all {OverlayCollection} for a given alias.
 *
 * @param {Alias} alias
 *           for which overlay data will be returned
 * @param {Object.<string,boolean>} general
 *          if true then all elements will be returned, if false then only ones
 *          available right now in the overlay
 * @returns {Promise} of the data from all {OverlayCollection} for a given alias
 */
CustomMap.prototype.getOverlayDataForAlias = function (alias, general) {
  var identifiedElement = new IdentifiedElement(alias);
  return this.getOverlayDataForIdentifiedElement(identifiedElement, general);
};

/**
 * Returns data from all {OverlayCollection} for a given reaction.
 *
 * @param {Reaction} reaction
 *           for which overlay data will be returned
 * @param {Object.<string,boolean>} general
 * @returns {Promise} of data from all {OverlayCollection} for a given alias
 */
CustomMap.prototype.getOverlayDataForReaction = function (reaction, general) {
  var identifiedElement = new IdentifiedElement(reaction);
  return this.getOverlayDataForIdentifiedElement(identifiedElement, general);
};

/**
 * Returns data from all {OverlayCollection} for a given {PointData}
 *
 * @param {Object.<string,boolean>} general
 * @param {PointData} point
 *           for which overlay data will be returned
 * @returns {Promise} of data from all {OverlayCollection} for a given
 *          {PointData}
 */
CustomMap.prototype.getOverlayDataForPoint = function (point, general) {
  var identifiedElement = new IdentifiedElement(point);
  return this.getOverlayDataForIdentifiedElement(identifiedElement, general);
};

/**
 * Returns data from all {OverlayCollection} for element identified by the
 * parameter
 *
 * @param {Object.<string,boolean>} general
 * @param {IdentifiedElement} identifiedElement
 *           for which overlay data will be returned
 * @returns {Promise} of data from all {OverlayCollection} for a given
 *          {IdentifiedElement}
 */
CustomMap.prototype.getOverlayDataForIdentifiedElement = function (identifiedElement, general) {
  if (general === undefined) {
    logger.warn("general parameter must be defined");
    general = [];
  }
  var promises = [];
  var overlays = [];
  var allOverlays = this.getDbOverlays();
  for (var i = 0; i < allOverlays.length; i++) {
    var overlay = allOverlays[i];
    if (overlay.allowGeneralSearch() || overlay.allowSearchById()) {
      var generalFlag = general[overlay.getName()];
      if (generalFlag === undefined) {
        logger.warn("General flag for overlay: " + overlay.getName() + " is not defined, assuming false");
        generalFlag = false;
      }
      overlays.push(overlay);
      promises.push(overlay.getDetailDataByIdentifiedElement(identifiedElement, !overlay.allowSearchById()
        || generalFlag));

    }
  }
  return Promise.all(promises).then(function (values) {
    var result = [];
    for (var i = 0; i < values.length; i++) {
      result.push({
        overlay: overlays[i],
        data: values[i]
      });
    }
    return result;
  });
};

/**
 *
 * @param {string} name
 * @returns {AbstractDbOverlay}
 */
CustomMap.prototype.getOverlayByName = function (name) {
  return this.overlayCollections[name];
};

/**
 *
 * @returns {AbstractDbOverlay[]}
 */
CustomMap.prototype.getDbOverlays = function () {
  var result = [];
  for (var overlayName in this.overlayCollections) {
    if (this.overlayCollections.hasOwnProperty(overlayName)) {
      result.push(this.overlayCollections[overlayName]);
    }
  }
  return result;
};

/**
 * Returns {@link AbstractInfoWindow} for element identified by the parameter.
 *
 * @param {IdentifiedElement} identifiedElement
 *           that determines for which element we want
 *          {AbstractInfoWindow}
 * @returns {AbstractInfoWindow} for element identified by the parameter
 */
CustomMap.prototype.getInfoWindowForIdentifiedElement = function (identifiedElement) {
  var model = this.getSubmapById(identifiedElement.modelId);
  var infoWindow = null;
  if (identifiedElement.type === "ALIAS") {
    infoWindow = model.getAliasInfoWindowById(identifiedElement.getId());
  } else if (identifiedElement.type === "POINT") {
    infoWindow = model.getPointInfoWindowById(identifiedElement.getId());
  } else if (identifiedElement.type === "REACTION") {
    infoWindow = model.getReactionInfoWindowById(identifiedElement.getId());
  } else {
    throw new Error("Unknown type of IdentifiedElement: " + identifiedElement.type);
  }
  return infoWindow;
};

/**
 *
 * @returns {number}
 */
CustomMap.prototype.getActiveSubmapId = function () {
  return this._activeSubmapId;
};

/**
 *
 * @param {number} submapId
 */
CustomMap.prototype.setActiveSubmapId = function (submapId) {
  this._activeSubmapId = submapId;
};

/**
 *
 * @param {Point} coordinates
 */
CustomMap.prototype.setActiveSubmapClickCoordinates = function (coordinates) {
  if (!(coordinates instanceof Point)) {
    throw new Error("Coordinates must be provided as Point object, but found: " + coordinates);
  }
  this._activeSubmapCoordinates = coordinates;
};

/**
 *
 * @returns {Point}
 */
CustomMap.prototype.getActiveSubmapClickCoordinates = function () {
  return this._activeSubmapCoordinates;
};

/**
 *
 * @param {string} type
 * @param {string} version
 * @returns {PromiseLike}
 */
CustomMap.prototype.getReferenceGenome = function (type, version) {
  var self = this;
  if (self.getProject().getOrganism() === undefined) {
    return Promise.resolve(new ReferenceGenome(null));
  }

  if (self._referenceGenome[type] === undefined) {
    self._referenceGenome[type] = [];
  }
  if (self._referenceGenome[type][version] === undefined) {
    return self.getServerConnector().getReferenceGenome({
      type: type,
      version: version,
      organism: self.getProject().getOrganism().getResource()
    }).then(function (genome) {
      self._referenceGenome[type][version] = genome;
      return genome;
    });
  } else {
    return Promise.resolve(self._referenceGenome[type][version]);
  }
};

/**
 *
 * @returns {CustomMap}
 */
CustomMap.prototype.getTopMap = function () {
  return this;
};

/**
 *
 * @param {CommentDialog} commentDialog
 */
CustomMap.prototype.setCommentDialog = function (commentDialog) {
  this._commentDialog = commentDialog;
};

/**
 *
 * @returns {CommentDialog}
 */
CustomMap.prototype.getCommentDialog = function () {
  return this._commentDialog;
};

/**
 *
 * @param {ContextMenu} contextMenu
 */
CustomMap.prototype.setContextMenu = function (contextMenu) {
  this._contextMenu = contextMenu;
};

/**
 *
 * @returns {ContextMenu}
 */
CustomMap.prototype.getContextMenu = function () {
  return this._contextMenu;
};

/**
 *
 * @returns {PromiseLike|Promise}
 */
CustomMap.prototype.openCommentDialog = function () {
  var self = this;
  return self.getServerConnector().getClosestElementsByCoordinates({
    modelId: this.getActiveSubmapId(),
    coordinates: this.getActiveSubmapClickCoordinates()
  }).then(function (elements) {
    var commentDialog = self.getCommentDialog();
    if (commentDialog === undefined) {
      var div = Functions.createElement({
        type: "div"
      });
      self.getElement().appendChild(div);
      commentDialog = new CommentDialog({
        element: div,
        customMap: self
      });
      self.setCommentDialog(commentDialog);
    }
    self.setCommentDialog(commentDialog);

    return commentDialog.open(elements);
  });
};

/**
 *
 * @param {IdentifiedElement[]} elements
 * @param {boolean} complete
 * @returns {PromiseLike<BioEntity[]> | Promise<BioEntity[]>}
 */
CustomMap.prototype.fetchIdentifiedElements = function (elements, complete) {
  var modelIds = [];
  var modelElements = [];
  var i;
  var elementPosition = [];
  for (i = 0; i < elements.length; i++) {
    var element = elements[i];
    if (elementPosition[element.getId()] === undefined) {
      elementPosition[element.getId()] = [];
    }
    elementPosition[element.getId()].push(i);
    if (modelElements[element.getModelId()] === undefined) {
      modelIds.push(element.getModelId());
      modelElements[element.getModelId()] = [];
    }
    modelElements[element.getModelId()].push(element);
  }

  var promises = [];
  for (i = 0; i < modelIds.length; i++) {
    var modelId = modelIds[i];
    promises.push(this.getSubmapById(modelId).getModel().getByIdentifiedElements(modelElements[modelId], complete));
  }
  var result = [];
  return Promise.all(promises).then(function (data) {
    for (var i = 0; i < data.length; i++) {
      data[i].forEach(function (element) {
        //preserve the order
        elementPosition[element.getId()].forEach(function (position) {
          result[position] = element;
        })
      });
    }
  }).then(function () {
    return result;
  });

};


/**
 *
 * @param {string} polygonData
 */
CustomMap.prototype.setSelectedPolygon = function (polygonData) {
  this._selectedPolygon = polygonData;
};

/**
 *
 * @returns {string}
 */
CustomMap.prototype.getSelectedPolygon = function () {
  return this._selectedPolygon;
};

/**
 *
 * @param {OverviewDialog} overviewDialog
 */
CustomMap.prototype.setOverviewDialog = function (overviewDialog) {
  this._overviewDialog = overviewDialog;
};

/**
 *
 * @returns {OverviewDialog}
 */
CustomMap.prototype.getOverviewDialog = function () {
  var self = this;
  if (this._overviewDialog === undefined) {
    logger.warn("OverviewDialog is not defined");
    var overviewDialogDiv = Functions.createElement({type: "div"});
    self.getElement().appendChild(overviewDialogDiv);
    this._overviewDialog = new OverviewDialog({
      customMap: self,
      configuration: self.getConfiguration(),
      project: self.getProject(),
      element: overviewDialogDiv
    });
  }
  return this._overviewDialog;
};

/**
 *
 * @param {number} params.modelId
 * @param {IdentifiedElement} params.element
 * @param {Point} params.coordinates
 *
 * @returns {PromiseLike|Promise}
 */
CustomMap.prototype.getDistance = function (params) {
  var self = this;
  var ie = params.element;
  var model = self.getSubmapById(ie.getModelId()).getModel();
  if (ie.getModelId() !== params.modelId) {
    throw new Error("Element and coordinates are on different maps: " + ie.getModelId() + ", " + params.modelId);
  }
  var x = params.coordinates.x;
  var y = params.coordinates.y;
  var p1 = new Point(x, y);
  return model.getByIdentifiedElement(ie).then(function (element) {
    if (element instanceof Alias) {
      if (element.getX() <= x && element.getX() + element.getWidth() >= x) {
        if (element.getY() <= y && element.getY() + element.getHeight() >= y) {
          return 0;
        } else {
          return Math.min(
            Math.abs(element.getY() - y),
            Math.abs(element.getY() + element.getHeight() - y)
          );
        }
      } else if (element.getY() <= y && element.getY() + element.getHeight() >= y) {
        return Math.min(
          Math.abs(element.getX() - x),
          Math.abs(element.getX() + element.getWidth() - x)
        );
      } else {
        var elementX = element.getX();
        var elementY = element.getY();
        var elementWidth = element.getWidth();
        var elementHeight = element.getHeight();
        return Math.min(
          Functions.distance(p1, new Point(elementX, elementY)),
          Functions.distance(p1, new Point(elementX + elementWidth, elementY)),
          Functions.distance(p1, new Point(elementX, elementY + elementHeight)),
          Functions.distance(p1, new Point(elementX + elementWidth, elementY + elementHeight))
        );
      }
    } else if (element instanceof Reaction) {
      var distance = Number.POSITIVE_INFINITY;
      var lines = element.getLines();
      for (var i = 0; i < lines.length; i++) {
        distance = Math.min(distance, Functions.distance(p1, lines[i]));
      }
      return distance;
    } else {
      throw new Error("Unknown element type: " + (typeof element));
    }
  });
};

/**
 *
 * @returns {Submap[]}
 */
CustomMap.prototype.getSubmaps = function () {
  var submaps = this.submaps;
  if (submaps === undefined) {
    submaps = [];
  }
  return submaps;
};

/**
 *
 */
CustomMap.prototype.destroy = function () {
  var self = this;
  AbstractCustomMap.prototype.destroy.call(self);
  var commentDialog = self.getCommentDialog();
  if (commentDialog !== undefined) {
    commentDialog.destroy();
  }
  var submaps = self.getSubmaps();
  for (var i = 0; i < submaps.length; i++) {
    submaps[i].destroy();
  }
  if (self.getContextMenu() !== undefined) {
    self.getContextMenu().destroy();
  }

  if (self._overviewDialog !== undefined) {
    var div = $(self._overviewDialog.getElement());
    if (div.hasClass("ui-dialog-content")) {
      div.dialog("destroy");
    }
  }
};

/**
 *
 * @returns {Promise<DataOverlay[]>}
 */
CustomMap.prototype.getVisibleDataOverlays = function () {
  var self = this;

  var dataOverlayPromises = [];

  for (var key in self._selectedOverlays) {
    if (self._selectedOverlays.hasOwnProperty(key) && self._selectedOverlays[key] === true) {
      dataOverlayPromises.push(function () {
        var id = parseInt(key);
        return self.getProject().getDataOverlayById(id).then(function (overlay) {
          if (overlay === null) {
            self._selectedOverlays[id] = false;
            return self.hideSelectedLayout(id).then(function () {
              return overlay;
            })
          } else {
            return overlay;
          }
        });
      }());
    }
  }

  return Promise.all(dataOverlayPromises).then(
    /**
     *
     * @param  {DataOverlay[]} overlays
     * */
    function (overlays) {

      overlays = overlays.filter(function (overlay) {
        return overlay != null;
      });


      overlays.sort(function (dataOverlay1, dataOverlay2) {
        if (dataOverlay1.getPublicOverlay() === dataOverlay2.getPublicOverlay()) {
          if (dataOverlay1.getOrder() < dataOverlay2.getOrder())
            return -1;
          if (dataOverlay1.getOrder() > dataOverlay2.getOrder())
            return 1;
          return 0;
        } else {
          if (dataOverlay1.getPublicOverlay() && !dataOverlay2.getPublicOverlay()) {
            return -1;
          } else {
            return 1;
          }
        }
      });

      return overlays;
    });
};


module.exports = CustomMap;
