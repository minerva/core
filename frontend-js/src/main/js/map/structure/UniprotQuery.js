var $ = require('jquery');

function ajaxQuery(url, type) {
  if (type === undefined) type = "GET";

  return $.ajax({
    type: type,
    url: url
  });
}

/**
 *
 * @param accession
 * @return {Promise}
 */
var getUniprotSequence = function (accession) {
  return ajaxQuery('https://rest.uniprot.org/uniprotkb/search?query=accession%3A' + accession + '&fields=sequence&format=tsv').then(function (data) {

    return data.split(/\r?\n/)[1];
  })
};

module.exports = getUniprotSequence;