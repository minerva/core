var Functions = require('../../Functions');
var SubMenu = require('../../gui/SubMenu');
var Variant = require('./MolArtVariant');
var getUniprotSequence = require('./UniprotQuery');
var $ = require('jquery');
var Promise = require('bluebird');

/**
 *
 * @param {HTMLElement} containerParentElement
 * @param {CustomMap} customMap
 * @constructor
 */
function MolArt(containerParentElement, customMap) {

  var molartDiv = Functions.createElement({
    type: "div",
    id: "minervaMolArtContainer",
    className: "minerva-molart-container"
  });
  containerParentElement.appendChild(molartDiv);

  this.setContainerElement(molartDiv);
  this._customMap = customMap;
}

/**
 *
 * @param {HTMLElement} containerElement
 */
MolArt.prototype.setContainerElement = function (containerElement) {
  this._containerElement = containerElement;
};

/**
 *
 * @returns {HTMLElement}
 */
MolArt.prototype.getContainerElement = function () {
  return this._containerElement;
};

/**
 *
 * @param {ContextMenu} menu
 */
function removeFromContextMenu(menu) {
  $(menu.getElement()).find('li:contains("MolArt")').remove();
}

/**
 *
 * @returns {Alias} MINERVA element to which the instance of molart is bound
 */
MolArt.prototype.getAlias = function () {
  return this._alias;
};

/**
 *
 * @param {Alias} alias MINERVA element to which the instance of molart is bound
 */
MolArt.prototype.setAlias = function (alias) {
  this._alias = alias;
};

/**
 *
 * @param {string[]} uniprotIds
 * @param {Alias} alias
 */
MolArt.prototype.activateInContextMenu = function (uniprotIds, alias) {

  var self = this;
  var menu = this._customMap.getContextMenu();
  removeFromContextMenu(menu);

  self.setAlias(alias);

  var submenu = new SubMenu({
    element: Functions.createElement({type: "li"}),
    name: "Open MolArt",
    customMap: self._customMap
  });
  uniprotIds.forEach(function (uniprotId) {
    submenu.addOption({
      name: uniprotId,
      handler: function () {
        self._activate(uniprotId);
      }
    });
  });
  menu.addOption({submenu: submenu, position: 0});

};

/**
 *
 */
MolArt.prototype.deactivateInContextMenu = function () {
  var menu = this._customMap.getContextMenu();
  removeFromContextMenu(menu);
  menu.addOption({
    name: "Open MolArt (no UniProt ID available)",
    handler: function () {
    },
    disabled: true,
    position: 0
  });
};

/**
 *
 * @private
 */
MolArt.prototype._deactivate = function () {
  var container = this.getContainerElement();
  container.style.display = 'none';
  $(container).empty();
  this.molArt.destroy();
  this.molArt = undefined;
};

/**
 *
 * @param {string} uniprotId
 * @private
 */
MolArt.prototype._activate = function (uniprotId) {

  var self = this;

  var container = this.getContainerElement();
  if (!container) return;

  var maxZIndex = Math.max.apply(null,
    $.map($('body *'), function (e) {
      if ($(e).css('position') !== 'static') {
        return parseInt($(e).css('z-index')) || 1;
      }
    }));

  $(container).css('z-index', maxZIndex + 1);

  var molArtCloseButton = Functions.createElement({
    type: "div",
    className: "minerva-molart-close-button",
    content: 'x'
  });
  molArtCloseButton.addEventListener('click', function () {
    self._deactivate();
    return false;
  });
  container.appendChild(molArtCloseButton);

  var MolArtPlugin = require('molart');

  var sequence;

  return getUniprotSequence(uniprotId).then(function (_sequence) {
    sequence = _sequence;
    return Promise.resolve();
  }).then(function () {
    retrieveVariants(self._customMap, self.getAlias().getId()).then(function (variants) {

      if (variants) {
        var customDataSources = [];
        for (var i = 0; i < variants.length; i++) {
          customDataSources.push({
            source: 'MINERVA: ' + variants[i].overlayName,
            useExtension: false,
            data: constructVariantsData(sequence, variants[i].variants)
          });
        }

        self.molArt = new MolArtPlugin({
          uniprotId: uniprotId,
          containerId: container.id,
          customDataSources: customDataSources
        });
      } else {
        self.molArt = new MolArtPlugin({
          uniprotId: uniprotId,
          containerId: container.id
        });
      }

      container.style.display = 'block';
    });
  });
};

var constructVariantsData = function (sequence, variants) {
  return {
    sequence: sequence,
    features: variants.map(v => {
        return {
          type: "VARIANT",
          category: "VARIATION",
          description: v.description,
          begin: v.posFrom,
          end: v.posTo ? v.posTo : v.posFrom,
          wildType: v.aaFrom,
          alternativeSequence: v.aaTo ? v.aaTo : 'd',
          consequence: v.varType,
        }
      }
    )
  }
};

var retrieveVariants = function (customMap, elementId) {

  var overlays;

  return customMap.getTopMap().getVisibleDataOverlays().then(function (dataOverlays) {

    overlays = dataOverlays;
    var promises = [];
    for (var i = 0; i < dataOverlays.length; i++) {
      promises.push(dataOverlays[i].getFullAliasesById(elementId));
    }

    return Promise.all(promises);
  }).then(function (aliasOverlayDataArray) {

    //The resolved promises should be in the same order in which the corresponding promises were stored
    var variants = [];
    aliasOverlayDataArray.forEach(function (aliasOverlayData) {
      for (var i = 0; i < aliasOverlayData.length; i++) {
        if (!aliasOverlayData[i]) continue;
        var overlayVariants = [];
        var geneVariants = aliasOverlayData[i].getGeneVariants();
        for (var j = 0; j < geneVariants.length; j++) {
          var variant = geneVariants[j];
          if (variant.getAminoAcidChange() !== null) {
            // not all variants need to have an aminoacid change associated
            overlayVariants = overlayVariants.concat(processGeneVariant(variant));
          }

        }

        if (overlayVariants.length > 0) {
          variants.push({
            overlayName: overlays[i].getName(),
            variants: overlayVariants
          });
        }
      }
    });

    return variants;
  });
}

var processGeneVariant = function (minervaVariant) {

  // synonymous_SNV LRRK2:NM_198578:exon1:c.T45C:p.T15T
  // synonymous_SNV C20orf96:NM_080571:exon10:c.G1020A:p.R340R,C20orf96:NM_153269:exon10:c.G1023A:p.R341R

  // nonsynonymous_SNV	LRRK2:NM_198578:exon4:c.T356C:p.L119P
  // nonsynonymous_SNV	TRIB3:NM_001301188:exon2:c.G82A:p.E28K,TRIB3:NM_001301190:exon2:c.G82A:p.E28K,TRIB3:NM_001301193:exon2:c.G82A:p.E28K,TRIB3:NM_001301196:exon2:c.G82A:p.E28K,TRIB3:NM_021158:exon2:c.G82A:p.E28K,TRIB3:NM_001301201:exon3:c.G163A:p.E55K

  // frameshift insertion: LRRK2:NM_198578:exon1:c.151_152insCCTCCAAGTTATTTCAAGGCAAAAATATCCATGTGCCTCT:p.A51fs
  // frameshift insertion: p.A51fsins
  // frameshift insertion: NCOA3:NM_001174087:exon22:c.4171_4172insG:p.M1391fs,NCOA3:NM_001174088:exon22:c.4147_4148insG:p.M1383fs,NCOA3:NM_006534:exon22:c.4162_4163insG:p.M1388fs,NCOA3:NM_181659:exon22:c.4174_4175insG:p.M1392fs

  // nonframeshift_insertion	LRRK2:NM_198578:exon27:c.3777_3778insATTCCTCCT:p.E1259delinsEIPP
  // nonframeshift_insertion	p.E1259delinsEIPP
  // nonframeshift_insertion SYNJ1:NM_001160306:exon28:c.3957_3958insAATACT:p.L1320delinsNTL,SYNJ1:NM_003895:exon32:c.4215_4216insAATACT:p.L1406delinsNTL

  // frameshift_deletion	LRRK2:NM_198578:exon17:c.2069_2070del:p.Q690fs
  // frameshift_deletion	p.Q690fsdel
  // frameshift_deletion ESF1:NM_001276380:exon14:c.2519_2520del:p.E840fs,ESF1:NM_016649:exon14:c.2519_2520del:p.E840fsins

  // nonframeshift_deletion	LRRK2:NM_198578:exon9:c.1099_1101del:p.367_367del
  // nonframeshift_deletion	p.367_367del
  // nonframeshift_deletion	RRBP1:NM_004587:exon15:c.2022_2024del:p.674_675del,RRBP1:NM_001042576:exon16:c.2022_2024del:p.674_675del


  var vars = [];

  var change = minervaVariant.getAminoAcidChange();
  var sChange = change.split(',');

  for (var i = 0; i < sChange.length; i++) {

    try {

      var aaChange = sChange[i].split('.').slice(-1)[0]; // LRRK2:NM_198578:exon1:c.T45C:p.T15T
      var variant = new Variant();

      if (aaChange.endsWith('del') > 0) {

        var m = aaChange.match(/(\d+)_(\d+)/);
        [, variant.posFrom, variant.posTo] = m;
        variant.varType = 'Nonframeshift deletion'

      } else if (aaChange.indexOf('delins') >= 0) {
        var m = aaChange.match(/([A-Z])(\d+)delins([A-Z]+)/);
        [, variant.aaFrom, variant.posFrom, variant.aaTo] = m;
        variant.aaTo = 'delins: ' + variant.aaTo; //This is because ProtVista can't handle insertions so we need to prefix it by 'd' which will place the variant into the 'd' line
        variant.varType = 'Nonframeshift insertion';
      } else if (aaChange.endsWith('fs')) {

        var m = aaChange.match(/([A-Z])(\d+)/);
        [, variant.aaFrom, variant.posFrom] = m;
        if (change.indexOf('del') >= 0) variant.varType = 'Frameshift deletion';
        else variant.varType = 'Frameshift insertion';

      } else {

        var m = aaChange.match(/([a-zA-Z]+)(\d+)([a-zA-Z])+/);

        if (!m || m.length != 4) continue;

        [, variant.aaFrom, variant.posFrom, variant.aaTo] = m;
        variant.varType = variant.aaFrom == variant.aaTo ? 'Synonymous SNV' : 'Nonsynonymous SNV';

      }

      vars.push(variant);
    } catch (err) {
      console.error("Problem with parsing variant " + minervaVariant.getAminoAcidChange(), err);
    }
  }

  return vars;
};


module.exports = MolArt;
