"use strict";

var logger = require('../../logger');

var Bounds = require('../canvas/Bounds');
var ObjectWithListeners = require('../../ObjectWithListeners');
var Promise = require('bluebird');

/**
 * Class representing abstract overlay element on the map relevant for a
 * specific overlay.
 *
 * @param {IdentifiedElement} params.element
 * @param {AbstractCustomMap} params.map
 * @param {function|function[]} params.onClick
 * @constructor
 */
function AbstractSurfaceElement(params) {
  var self = this;
  // call super constructor
  ObjectWithListeners.call(this);

  self.registerListenerType("onClick");
  self.setCustomMap(params.map);
  if (params.element !== undefined && params.element !== null) {
    self.setIdentifiedElement(params.element);
  }
  self._mapCanvasObjects = [];

  if (params.onClick !== undefined) {
    if (typeof params.onClick !== "function") {
      for (var i = 0; i < params.onClick.length; i++) {
        self.addListener("onClick", params.onClick[i]);
      }
    } else {
      self.addListener("onClick", params.onClick);
    }
  }
}

AbstractSurfaceElement.prototype = Object.create(ObjectWithListeners.prototype);
AbstractSurfaceElement.prototype.constructor = AbstractSurfaceElement;

/**
 *
 * @returns {boolean}
 */
AbstractSurfaceElement.prototype.isShown = function () {
  var result = false;
  for (var i = 0; i < this.getMapCanvasObjects().length; i++) {
    if (this.getMapCanvasObjects()[i].isShown()) {
      result = true;
    }
  }
  return result;
};

/**
 *
 * @returns {Promise}
 */
AbstractSurfaceElement.prototype.show = function () {
  var self = this;
  if (!this.isInitialized()) {
    return this.init().then(function () {
      return self.show();
    });
  }
  if (this.isShown()) {
    logger.warn("Surface already shown");
    return Promise.resolve();
  }
  var promises = [];
  for (var i = 0; i < this.getMapCanvasObjects().length; i++) {
    promises.push(this.getMapCanvasObjects()[i].show());
  }
  return Promise.all(promises);
};

/**
 *
 * @returns {boolean}
 */
AbstractSurfaceElement.prototype.isInitialized = function () {
  return this.getMapCanvasObjects().length > 0;
};

/**
 *
 * @returns {Promise}
 */
AbstractSurfaceElement.prototype.hide = function () {
  var promises = [];
  for (var i = 0; i < this.getMapCanvasObjects().length; i++) {
    promises.push(this.getMapCanvasObjects()[i].hide());
  }
  return Promise.all(promises);
};

/**
 *
 * @returns {Promise|PromiseLike}
 */
AbstractSurfaceElement.prototype.onClickHandler = function () {
  return this.callListeners("onClick");
};

/**
 *
 * @returns {Array}
 */
AbstractSurfaceElement.prototype.getMapCanvasObjects = function () {
  return this._mapCanvasObjects;
};

/**
 *
 * @param {Rectangle|Polyline} object
 */
AbstractSurfaceElement.prototype.addMapCanvasObject = function (object) {
  this._mapCanvasObjects.push(object);

  var self = this;
  var onclick = function () {
    return self.onClickHandler();
  };
  object.addListener('click', onclick);
};

/**
 *
 * @returns {IdentifiedElement}
 */
AbstractSurfaceElement.prototype.getIdentifiedElement = function () {
  return this._identifiedElement;
};

/**
 *
 * @param {IdentifiedElement} identifiedElement
 */
AbstractSurfaceElement.prototype.setIdentifiedElement = function (identifiedElement) {
  this._identifiedElement = identifiedElement;
};

/**
 *
 * @returns {BioEntity}
 */
AbstractSurfaceElement.prototype.getBioEntity = function () {
  return this._bioEntity;
};

/**
 *
 * @param {BioEntity} bioEntity
 */
AbstractSurfaceElement.prototype.setBioEntity = function (bioEntity) {
  this._bioEntity = bioEntity;
};


/**
 *
 * @returns {number}
 */
AbstractSurfaceElement.prototype.getModelId = function () {
  return this.getIdentifiedElement().getModelId();
};

/**
 *
 * @param {IdentifiedElement} identifiedElement
 */
AbstractSurfaceElement.prototype.updateIdentifiedElement = function (identifiedElement) {
  if (identifiedElement.getColor() !== undefined) {
    this.setColor(identifiedElement.getColor());
  }
};

/**
 *
 * @returns {Bounds}
 */
AbstractSurfaceElement.prototype.getBounds = function () {
  var self = this;
  var bounds = new Bounds();
  for (var i = 0; i < self.getMapCanvasObjects().length; i++) {
    var elementBounds = self.getMapCanvasObjects()[i].getBounds();
    bounds.extend(elementBounds.getTopLeft());
    bounds.extend(elementBounds.getRightBottom());
  }
  return bounds;
};

/**
 * Returns {@link AbstractCustomMap} where surface is located.
 *
 * @returns {AbstractCustomMap} where surface is located
 */
AbstractSurfaceElement.prototype.getCustomMap = function () {
  return this._customMap;
};

/**
 *
 * @param {AbstractCustomMap} customMap
 */
AbstractSurfaceElement.prototype.setCustomMap = function (customMap) {
  this._customMap = customMap;
};

/**
 *
 * @returns {Point}
 */
AbstractSurfaceElement.prototype.getCenter = function () {
  return this.getBioEntity().getCenter();
};

AbstractSurfaceElement.prototype.setBoundsForAlias = function (startX, endX) {
  throw new Error("Abstract method. Not implemented");
};

module.exports = AbstractSurfaceElement;
