"use strict";

var Bounds = require('../canvas/Bounds');
var Point = require('../canvas/Point');
var Promise = require("bluebird");

/* exported logger */

var functions = require('../../Functions');
// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var AbstractSurfaceElement = require('./AbstractSurfaceElement');
var IdentifiedElement = require('../data/IdentifiedElement');

/**
 *
 * @param {IdentifiedElement} [params.element]
 * @param {AbstractCustomMap} params.map
 * @param {function|function[]} [params.onClick]
 * @param {Reaction} params.reaction
 * @param {LayoutReaction} [params.layoutReaction]
 * @param {boolean} params.customized
 * @param {string} [params.color]
 * @constructor
 * @extends AbstractSurfaceElement
 */
function ReactionSurface(params) {
  // call super constructor
  AbstractSurfaceElement.call(this, params);

  this.setWidth(5.0);
  this.setOverlayData(params.layoutReaction);
  this.setBioEntity(params.reaction);

  var color = params.color;

  if (color === undefined) {
    color = "#0000FF";
  }

  this.setColor(color);
  this.setCustomized(params.customized);
  this.setIdentifiedElement(new IdentifiedElement(this.getBioEntity()));
}

ReactionSurface.prototype = Object.create(AbstractSurfaceElement.prototype);
ReactionSurface.prototype.constructor = ReactionSurface;

/**
 * Returns {@link Bounds} of all map elements
 * included in the object.
 *
 * @returns {Bounds} of all map elements
 *          included in the object
 */
ReactionSurface.prototype.getBounds = function () {
  var bounds = new Bounds();

  for (var i = 0; i < this.getMapCanvasObjects().length; i++) {
    var line = this.getMapCanvasObjects()[i];
    bounds.extend(line.getBounds());
  }
  return bounds;
};

/**
 *
 * @param {boolean} customized
 */
ReactionSurface.prototype.setCustomized = function (customized) {
  this.customized = customized;
};

/**
 * Sets color to be used by the reaction overlay visualization when visualized
 * in customized mode.
 *
 * @param {string} color
 *          new color value
 */
ReactionSurface.prototype.setColor = function (color) {
  this._color = color;
  var mapCanvasObjects = this.getMapCanvasObjects();
  for (var i = 0; i < mapCanvasObjects.length; i++) {
    mapCanvasObjects[i].setOptions({
      strokeColor: color
    });
  }
  this.setCustomized(true);
};

/**
 * Sets width of line to be used by the reaction overlay visualization when
 * visualized in customized mode.
 *
 * @param {number} width
 *          new width value
 */
ReactionSurface.prototype.setWidth = function (width) {
  if (width !== undefined && width !== null) {
    this.width = width;
  } else {
    this.width = 1.0;
  }

};

/**
 * Returns color that should be used when visualized in customized mode.
 *
 * @returns {string} color that should be used when visualized in customized mode.
 */
ReactionSurface.prototype.getColor = function () {
  return this._color;
};

/**
 * Returns width that should be used when visualized in customized mode.
 *
 * @returns {number} width that should be used when visualized in customized mode.
 */
ReactionSurface.prototype.getWidth = function () {
  return this.width;
};

/**
 * This function is used when some overlays are added/removed from visualization.
 * The syntax is compatible with AliasSurface class so they can be handled in
 * the same way. When visualizing more than one overlay startX or endX will be
 * set to a value between 0..1 (not inclusive). In such case the visualization
 * of reaction should turn to some default mode where we know that reaction is
 * highlighted, but we have no more information. In this way user will know that
 * he should investigate the reaction manually.
 *
 * @param {number} startX
 *          see {AliasSurface} class for details, in this implementation the
 *          only important information is that when there are more than one
 *          overlay visualized at least one of the two parameters (startX,endX)
 *          will differ from {0.0, 1.0} values
 * @param {number} endX
 *          see {AliasSurface} class for details, in this implementation the
 *          only important information is that when there are more than one
 *          overlay visualized at least one of the two parameters (startX,endX)
 *          will differ from {0.0, 1.0} values
 */
ReactionSurface.prototype.setBoundsForAlias = function (startX, endX) {
  if (this.isCustomized() && (startX > 0 || endX < 1)) {
    this.changedToDefault();
  } else if (!this.isCustomized() && (startX <= 0 && endX >= 1)) {
    this.changedToCustomized();
  }
};

/**
 * Changes visualization of the ReactionSurface to default mode where we mark
 * reaction as highlighted, but we skip customized reaction overlay data.
 */
ReactionSurface.prototype.changedToDefault = function () {
  for (var i = 0; i < this.getMapCanvasObjects().length; i++) {
    this.getMapCanvasObjects()[i].setOptions({
      strokeColor: "#0000FF",
      strokeWeight: 5
    });
  }
  this.setCustomized(false);
};

/**
 * Changes visualization of the ReactionSurface to customized mode where we mark
 * reaction as highlighted with customized reaction overlay data.
 */
ReactionSurface.prototype.changedToCustomized = function () {
  for (var i = 0; i < this.getMapCanvasObjects().length; i++) {
    this.getMapCanvasObjects()[i].setOptions({
      strokeColor: this.getColor(),
      strokeWeight: this.getWidth()
    });
  }
  this.setCustomized(true);
};

/**
 * Creates {Polyline} from input data.
 *
 * @param {Object} line
 *          raw data about line taken from server
 * @param {number} line.start.x
 * @param {number} line.start.y
 * @param {number} line.end.x
 * @param {number} line.end.y
 *
 * @param {string} color
 *          color that should be used for visualization of the line
 * @param {number} width
 *          width that should be used for visualization of the line
 * @param {AbstractCustomMap} map
 *          where line should be visualized
 * @returns {Polyline} from input data
 */
ReactionSurface.createLine = function (line, color, width, map) {
  var pointA = new Point(line.start.x, line.start.y);
  var pointB = new Point(line.end.x, line.end.y);
  var path = [pointA, pointB];

  return map.getMapCanvas().createPolyline({
    strokeColor: color,
    strokeOpacity: 1.0,
    strokeWeight: width,
    path: path
  });
};

/**
 *
 * @returns {number}
 */
ReactionSurface.prototype.getId = function () {
  return this._id;
};

/**
 *
 * @param {number} id
 */
ReactionSurface.prototype.setId = function (id) {
  this._id = id;
};

/**
 *
 * @returns {LayoutReaction}
 */
ReactionSurface.prototype.getOverlayData = function () {
  return this._overlayData;
};

/**
 *
 * @param {LayoutReaction} overlayData
 */
ReactionSurface.prototype.setOverlayData = function (overlayData) {
  this._overlayData = overlayData;
  if (overlayData !== undefined) {
    if (overlayData.width) {
      this.setWidth(overlayData.width);
    }
  }
};

/**
 *
 * @param {BioEntity} bioEntity
 */
ReactionSurface.prototype.setBioEntity = function (bioEntity) {
  if (bioEntity === undefined || bioEntity === null) {
    throw new Error("Reaction must be defined");
  }
  AbstractSurfaceElement.prototype.setBioEntity.call(this, bioEntity);
  this.setId(bioEntity.getId());
};

/**
 *
 * @returns {boolean}
 */
ReactionSurface.prototype.isCustomized = function () {
  return this.customized;
};

/**
 *
 * @returns {Promise}
 */
ReactionSurface.prototype.init = function () {
  var self = this;
  var reaction = this.getBioEntity();
  var overlayData = this.getOverlayData();

  var promise = Promise.resolve(self.getColor());
  if (overlayData !== undefined && overlayData !== null) {
    promise = functions.overlayToColor(overlayData);
  }
  return promise.then(function (color) {
    if (color === undefined) {
      color = "#0000FF";
    }
    self.setColor(color);
    var i;
    var line;
    var polyline;
    for (i = 0; i < reaction.startLines.length; i++) {
      line = reaction.startLines[i];
      polyline = ReactionSurface.createLine(line, self.getColor(), self.getWidth(), self.getCustomMap());
      self.addMapCanvasObject(polyline);
    }
    for (i = 0; i < reaction.endLines.length; i++) {
      line = reaction.endLines[i];
      polyline = ReactionSurface.createLine(line, self.getColor(), self.getWidth(), self.getCustomMap());
      self.addMapCanvasObject(polyline);
    }
    for (i = 0; i < reaction.midLines.length; i++) {
      line = reaction.midLines[i];
      polyline = ReactionSurface.createLine(line, self.getColor(), self.getWidth(), self.getCustomMap());
      self.addMapCanvasObject(polyline);
    }
    if (!self.isCustomized()) {
      self.changedToDefault();
    }
  });
};

module.exports = ReactionSurface;
