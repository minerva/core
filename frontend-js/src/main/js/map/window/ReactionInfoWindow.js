"use strict";

var Promise = require("bluebird");

var AbstractInfoWindow = require('./AbstractInfoWindow');
var GuiUtils = require('../../gui/leftPanel/GuiUtils');
var IdentifiedElement = require('../data/IdentifiedElement');
var Reaction = require('../data/Reaction');
var Functions = require('../../Functions');
var logger = require('../../logger');

/**
 * Class representing info window that should be opened when clicking on
 * reaction.
 */
/**
 *
 * @param {IdentifiedElement} [params.identifiedElement]
 * @param {AbstractCustomMap} params.map
 * @param {Marker} params.marker
 * @param {Reaction} params.reaction
 *
 * @constructor
 * @extends AbstractInfoWindow
 */
function ReactionInfoWindow(params) {
  if (params.identifiedElement === undefined) {
    params.identifiedElement = new IdentifiedElement(params.reaction);
  }
  // call super constructor
  AbstractInfoWindow.call(this, params);

  var self = this;

  self.setReactionData(params.reaction);
  if (params.reaction.getKineticLaw() !== undefined) {
    self.addListener("onShow", function () {
      return MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
    });
  }
}

ReactionInfoWindow.prototype = Object.create(AbstractInfoWindow.prototype);
ReactionInfoWindow.prototype.constructor = ReactionInfoWindow;

/**
 * Methods that creates and return html code with the content of the window.
 *
 * @returns {Promise} representing html code for content of the info window
 */
ReactionInfoWindow.prototype.createContentDiv = function () {
  var self = this;
  var reaction = self.getReactionData();
  var result = document.createElement("div");
  var title = document.createElement("h3");
  title.innerHTML = "Reaction: " + reaction.getReactionId();
  result.appendChild(title);

  return self.createKineticsDiv(reaction, result).then(function () {
    result.appendChild(self.createElementsDiv(reaction));
    return result;
  });
};

/**
 *
 * @param {Reaction} reaction
 * @param {HTMLElement} result
 * @returns {Promise}
 */
ReactionInfoWindow.prototype.createKineticsDiv = function (reaction, result) {
  var self = this;
  if (reaction.getKineticLaw() === undefined) {
    return Promise.resolve();
  } else {
    var kineticLaw = reaction.getKineticLaw();
    return Functions.loadScript('https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_CHTML').then(function () {
      return MathJax.Hub.Config({
        tex2jax: {
          skipTags: ["script", "noscript", "style", "textarea", "pre", "code"]
        }
      });
    }).then(function () {

      result.appendChild(Functions.createElement({type: "h4", content: "Kinetic law"}));

      if (kineticLaw.getMathMlPresentation() !== undefined) {
        result.appendChild(Functions.createElement({
          type: "div",
          content: kineticLaw.getMathMlPresentation(),
          xss: false
        }));
      } else {

        result.appendChild(Functions.createElement({
          type: "div",
          content: "<p>Problematic MathML</p>"
        }));
        logger.warn("Problematic MathML: " + kineticLaw.getDefinition());
      }
      var promises = [];
      for (var i = 0; i < kineticLaw.getFunctionIds().length; i++) {
        promises.push(self.getCustomMap().getModel().getSbmlFunctionById(kineticLaw.getFunctionIds()[i]));
      }
      return Promise.all(promises);
    }).then(function (functions) {
      result.appendChild(self.createSbmlFunctionDiv(functions));

      var promises = [];
      for (var i = 0; i < kineticLaw.getParameterIds().length; i++) {
        promises.push(self.getCustomMap().getModel().getSbmlParameterById(kineticLaw.getParameterIds()[i]));
      }
      return Promise.all(promises);
    }).then(function (parameters) {
      result.appendChild(self.createSbmlParameterDiv(parameters));
    });
  }
};

/**
 *
 * @param {SbmlFunction[]} functions
 * @returns {HTMLElement}
 */
ReactionInfoWindow.prototype.createSbmlFunctionDiv = function (functions) {
  var result = Functions.createElement({type: "div"});
  if (functions.length > 0) {
    result.appendChild(Functions.createElement({type: "h5", content: "Functions: "}));
    var guiUtils = new GuiUtils();
    var table = Functions.createElement({type: "div", style: "display: table;", className: "borderTable"});
    table.appendChild(guiUtils.createTableRow(["Name", "Definition", "Arguments"]));
    for (var i = 0; i < functions.length; i++) {
      var sbmlFunction = functions[i];
      var mathML;
      if (sbmlFunction.getMathMlPresentation() !== undefined) {
        mathML = sbmlFunction.getMathMlPresentation();
      } else {
        mathML = "<p>Problematic MathML</p>";
        logger.warn("Problematic MathML: " + sbmlFunction.getDefinition());
      }
      var functionArguments = sbmlFunction.getArguments().join(", ");
      table.appendChild(guiUtils.createTableRow([sbmlFunction.getName(), mathML, functionArguments]));
    }
    result.appendChild(table);
  }
  return result;
};

/**
 *
 * @param {SbmlParameter[]} parameters
 * @returns {HTMLElement}
 */
ReactionInfoWindow.prototype.createSbmlParameterDiv = function (parameters) {
  var result = Functions.createElement({type: "div"});
  if (parameters.length > 0) {
    result.appendChild(Functions.createElement({type: "h5", content: "Parameters: "}));
    var guiUtils = new GuiUtils();
    var table = Functions.createElement({type: "div", style: "display: table;", className: "borderTable"});
    table.appendChild(guiUtils.createTableRow(["Name", "parameterId", "Value", "Global"]));
    for (var i = 0; i < parameters.length; i++) {
      var sbmlFunction = parameters[i];
      table.appendChild(guiUtils.createTableRow([sbmlFunction.getName(), sbmlFunction.getParameterId(), sbmlFunction.getValue(), sbmlFunction.getGlobal()]));
    }
    result.appendChild(table);
  }
  return result;
};

/**
 *
 * @param {Reaction} reaction
 * @returns {HTMLElement}
 */
ReactionInfoWindow.prototype.createElementsDiv = function (reaction) {
  var result = Functions.createElement({type: "div"});
  result.appendChild(Functions.createElement({type: "h5", content: "Elements: "}));
  var guiUtils = new GuiUtils();
  var table = Functions.createElement({type: "div", style: "display: table;", className: "borderTable"});
  table.appendChild(guiUtils.createTableRow(["Name", "Role", "elementId", "Constant", "Boundary condition", "Initial concentration", "Initial amount", "Stoichiometry"]));
  var elements = reaction.getReactants();
  var i, element;

  function createRow(node, title) {
    var element = node.getAlias();
    var stoichiometry = node.getStoichiometry();
    if (stoichiometry === undefined) {
      stoichiometry = "";
    }
    return guiUtils.createTableRow([element.getName(), title, element.getElementId(), element.getConstant(), element.getBoundaryCondition(), element.getInitialConcentration(), element.getInitialAmount(), stoichiometry])
  }

  for (i = 0; i < elements.length; i++) {
    element = elements[i].getAlias();
    table.appendChild(createRow(elements[i], "Reactant"));
  }
  elements = reaction.getProducts();
  for (i = 0; i < elements.length; i++) {
    element = elements[i].getAlias();
    table.appendChild(createRow(elements[i], "Product"));
  }
  elements = reaction.getModifiers();
  for (i = 0; i < elements.length; i++) {
    element = elements[i].getAlias();
    table.appendChild(createRow(elements[i], "Modifier"));
  }
  result.appendChild(table);
  return result;
};

/**
 *
 * @returns {PromiseLike<any> | Promise<any>}
 */
ReactionInfoWindow.prototype.init = function () {
  var self = this;
  return Promise.resolve().then(function () {
    return AbstractInfoWindow.prototype.init.call(self);
  }).then(function () {
    return self.update();
  });
};

/**
 * Returns array with data taken from all known {@link AbstractDbOverlay}.
 *
 * @param {Object.<string,boolean>} general
 * @returns {Promise} of an array with data from {@link AbstractDbOverlay}
 */
ReactionInfoWindow.prototype.getDbOverlaysData = function (general) {
  return this.getCustomMap().getTopMap().getOverlayDataForReaction(this.getReactionData(), general);
};

/**
 *
 * @returns {Reaction}
 */
ReactionInfoWindow.prototype.getReactionData = function () {
  return this._reactionData;
};

/**
 *
 * @param {Reaction} reactionData
 */
ReactionInfoWindow.prototype.setReactionData = function (reactionData) {
  if (reactionData === undefined || reactionData === null) {
    throw new Error("Reaction must be specified");
  } else if (reactionData instanceof Reaction) {
    this._reactionData = reactionData;
  } else {
    throw new Error("Parameter must be of Reaction type, but found" + reactionData);
  }
};

/**
 *
 * @returns {Point}
 */
ReactionInfoWindow.prototype.getPosition = function () {
  return this.getReactionData().getCenter();
};

module.exports = ReactionInfoWindow;
