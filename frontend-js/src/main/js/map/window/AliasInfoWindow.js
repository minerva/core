"use strict";

var $ = require('jquery');
var logger = require('../../logger');
var Functions = require('../../Functions');

var AbstractInfoWindow = require('./AbstractInfoWindow');
var Alias = require('../data/Alias');
var IdentifiedElement = require('../data/IdentifiedElement');
var LayoutAlias = require('../data/LayoutAlias');
var Point = require('../canvas/Point');

var pileup = require('pileup');

var Promise = require("bluebird");
var ChemicalDbOverlay = require("../overlay/ChemicalDbOverlay");
var DrugDbOverlay = require("../overlay/DrugDbOverlay");

/**
 * @typedef {Object} PileupRange
 * @property {number} start
 * @property {number} stop
 * @property {string} contig
 */

/**
 * Class representing info window that should be opened when clicking on alias.
 *
 * @param {IdentifiedElement} [params.identifiedElement]
 * @param {AbstractCustomMap} params.map
 * @param {Marker} params.marker
 * @param {Alias} params.alias
 *
 * @constructor
 * @extends AbstractInfoWindow
 */
function AliasInfoWindow(params) {
  var self = this;

  if (params.identifiedElement === undefined) {
    params.identifiedElement = new IdentifiedElement(params.alias);
  }
  // call super constructor
  AbstractInfoWindow.call(this, params);

  this.setAlias(params.alias);

  var overlayListChanged = function () {
    return self.update();
  };

  var dbOverlaySearchChanged = function (arg) {
    return self.update().then(function () {
      if (arg.object instanceof ChemicalDbOverlay) {
        $("a:contains(chemical)", self.getContent()).click();
      } else if (arg.object instanceof DrugDbOverlay) {
        $("a:contains(drug)", self.getContent()).click();
      }
    });
  };

  params.map.getTopMap().addListener("onShowOverlay", overlayListChanged);
  params.map.getTopMap().addListener("onHideOverlay", overlayListChanged);
  params.map.getTopMap().addListener("onRedrawSelectedOverlays", overlayListChanged);

  var drugDbOverlay = params.map.getTopMap().getOverlayByName("drug");
  if (drugDbOverlay !== undefined) {
    drugDbOverlay.addListener("onSearch", dbOverlaySearchChanged);
  }
  var chemicalDbOverlay = params.map.getTopMap().getOverlayByName("chemical");
  if (chemicalDbOverlay !== undefined) {
    chemicalDbOverlay.addListener("onSearch", dbOverlaySearchChanged);
  }
}

AliasInfoWindow.prototype = Object.create(AbstractInfoWindow.prototype);
AliasInfoWindow.prototype.constructor = AliasInfoWindow;


/**
 * Updates content of info window. The content will be automatically obtained
 * from {@link CustomMap} instance. The only optional parameter is {@link Alias}
 * data.
 *
 * @returns {Promise}
 */
AliasInfoWindow.prototype.update = function () {
  return AbstractInfoWindow.prototype.update.call(this);
};

/**
 *
 * @returns {PromiseLike<any>}
 */
AliasInfoWindow.prototype.init = function () {
  var self = this;
  var alias = self.getAlias();
  return AbstractInfoWindow.prototype.init.call(self).then(function () {
    return self.getCustomMap().getModel().getAliasById(alias.getId(), true);
  }).then(function (alias) {
    self.setAlias(alias);
    return self.update();
  });
};

/**
 *
 * @param {DataOverlay} overlay
 * @return {string}
 * @package
 */
AliasInfoWindow.prototype._getPrintableOverlayName = function (overlay) {
  var name = overlay.getName();
  if (name.length > 20) {
    name = name.substr(0, 20) + "...";
  }
  if (!overlay.getPublicOverlay()) {
    name = "[" + overlay.getOrder() + "] " + name;
  }
  return name;
};

/**
 * Creates and returns chart representing data related to alias on different
 * overlays.
 *
 * @param {DataOverlay[]} params.overlays
 *
 * @returns {PromiseLike<HTMLElement>} html element representing chart with data related to alias
 *          on different overlays
 */
AliasInfoWindow.prototype.createChartDiv = function (params) {
  var overlays = params.overlays;

  var result = document.createElement("div");
  var promises = [];
  var self = this;

  overlays.sort(function (overlayA, overlayB) {
    if (overlayA.getPublicOverlay() !== overlayB.getPublicOverlay()) {
      if (overlayA.getPublicOverlay()) {
        return -1;
      }
      return 1;
    }
    return overlayA.getOrder() - overlayB.getOrder();
  });

  overlays.forEach(function (overlay, i) {
    promises.push(overlay.getFullAliasesById(self.getAlias().getId()).then(function (aliases) {
      var copy = aliases.slice();
      var rowPromises = [];
      if (copy.length === 0) {
        copy.push(null);
      }
      copy.forEach(function (data) {
        var rowDiv = document.createElement("div");
        if (i % 2 === 0 || self.getAlias().getLinkedSubmodelId() !== undefined) {
          rowDiv.className = "minerva-chart-row-even";
        } else {
          rowDiv.className = "minerva-chart-row-odd";
        }
        var name = self._getPrintableOverlayName(overlays[i]);
        var nameDiv = document.createElement("div");
        nameDiv.className = "minerva-chart-name";
        nameDiv.innerHTML = name + "&nbsp;";
        rowDiv.appendChild(nameDiv);

        if (data !== undefined && data !== null) {
          rowPromises.push(Functions.overlayToColor(data).then(function (color) {
            var value = parseFloat(data.value);
            var description = data.description;
            if (description === null || description === undefined || description === "") {
              description = "";
              if (!isNaN(value)) {
                description = value.toFixed(2);
              }
            }
            var leftMarginDiv = document.createElement("div");
            leftMarginDiv.innerHTML = "&nbsp;";
            leftMarginDiv.style.float = "left";
            var centerBarDiv = document.createElement("div");
            centerBarDiv.style.width = "1px";
            centerBarDiv.style.float = "left";
            centerBarDiv.style.background = "#000000";
            centerBarDiv.innerHTML = "&nbsp;";

            var rightBarDiv = document.createElement("div");
            rightBarDiv.innerHTML = "&nbsp;";
            rightBarDiv.style.float = "left";
            rightBarDiv.style.background = color;
            rightBarDiv.style.width = Math.abs(value * 100) + "px";
            var offset = 100;
            var descDiv = document.createElement("div");
            descDiv.style.float = "right";
            descDiv.style.textAlign = "right";
            descDiv.style.position = "absolute";
            descDiv.style.right = "0";
            descDiv.innerHTML = "<span>" + description + "</span>";
            if (!isNaN(value)) {
              if (value > 0) {
                offset = 100;
                leftMarginDiv.style.width = offset + "px";

                rightBarDiv.style.textAlign = "right";

                rowDiv.appendChild(leftMarginDiv);
                rowDiv.appendChild(centerBarDiv);
                rowDiv.appendChild(rightBarDiv);
              } else {
                offset = 100 + (value * 100);
                leftMarginDiv.style.width = offset + "px";

                rowDiv.appendChild(leftMarginDiv);
                rowDiv.appendChild(rightBarDiv);
                rowDiv.appendChild(centerBarDiv);
              }

            } else {
              offset = 100;
              leftMarginDiv.style.width = offset + "px";
              leftMarginDiv.style.background = color;
              rightBarDiv.style.width = offset + "px";
              rightBarDiv.style.background = color;
              rightBarDiv.style.textAlign = "right";
              rowDiv.appendChild(leftMarginDiv);
              rowDiv.appendChild(centerBarDiv);
              rowDiv.appendChild(rightBarDiv);
            }
            rowDiv.appendChild(descDiv);
            return rowDiv;
          }));
        } else {
          var emptyDiv = document.createElement("div");
          emptyDiv.innerHTML = "&nbsp;";
          emptyDiv.style.float = "left";
          emptyDiv.style.width = "201px";
          rowDiv.appendChild(emptyDiv);
          rowPromises.push(rowDiv);
        }
      });
      return Promise.all(rowPromises);
    }));
    if (self.getAlias().getLinkedSubmodelId() !== undefined) {
      promises.push(new Promise.resolve(overlay).then(function (overlay) {
        var overlayData = overlay.getAliases();
        var overlayDataOnSubmap = [];
        for (var j = 0; j < overlayData.length; j++) {
          var data = overlayData[j];
          if (data.getModelId() === self.getAlias().getLinkedSubmodelId()) {
            overlayDataOnSubmap.push(data);
          }
        }
        return Functions.overlaysToColorDataStructure(overlayDataOnSubmap).then(function (colors) {
          var rowDiv = document.createElement("div");
          rowDiv.className = "minerva-chart-row-odd";
          var nameDiv = document.createElement("div");
          nameDiv.className = "minerva-chart-name";
          var submapName = self.getCustomMap().getTopMap().getSubmapById(self.getAlias().getLinkedSubmodelId()).getModel().getName();
          nameDiv.innerHTML = "Submap " + submapName;
          rowDiv.appendChild(nameDiv);

          var emptyDiv = document.createElement("div");
          emptyDiv.style.float = "left";
          emptyDiv.style.width = "201px";
          rowDiv.appendChild(emptyDiv);

          var totalAmount = 0;
          for (var j = 0; j < colors.length; j++) {
            totalAmount += colors[j].amount;
          }

          for (j = 0; j < colors.length; j++) {
            var overlayDataDiv = document.createElement("div");
            overlayDataDiv.innerHTML = "&nbsp;";
            overlayDataDiv.style.float = "left";
            overlayDataDiv.style.width = Math.floor(100 * colors[j].amount / totalAmount) + "%";
            overlayDataDiv.style.background = colors[j].color;
            emptyDiv.appendChild(overlayDataDiv);
          }
          return rowDiv;
        });
      }));
    }
  });
  return Promise.all(promises).then(function (rows) {
    for (var i = 0; i < rows.length; i++) {
      for (var j = 0; j < rows[i].length; j++) {
        result.appendChild(rows[i][j]);
      }
    }
    result.appendChild(document.createElement("br"));
    return result;
  });
};

/**
 * Methods that creates and return DOM object with the content of the window.
 *
 * @returns {Promise<HTMLElement>| PromiseLike<HTMLElement> } DOM object representing html code for content of the info window
 */
AliasInfoWindow.prototype.createContentDiv = function () {
  var self = this;
  var alias = self.getAlias();
  if (alias.isComplete()) {
    var result = document.createElement("div");
    var title = document.createElement("h3");
    title.innerHTML = alias.getType() + ": " + alias.getName();
    result.appendChild(title);

    var overlayDiv = document.createElement("div");

    result.appendChild(overlayDiv);

    var overlays;

    return self.getCustomMap().getTopMap().getVisibleDataOverlays().then(function (dataOverlays) {
      overlays = dataOverlays;
      return self.createChartDiv({overlays: overlays});
    }).then(function (chartDiv) {
      overlayDiv.appendChild(chartDiv);
      return self.createGenomicDiv({overlays: overlays});
    }).then(function (genomicDiv) {
      overlayDiv.appendChild(genomicDiv);
      return result;
    });
  } else {
    return Promise.resolve(self.createWaitingContentDiv());
  }
};

/**
 * Returns array with data taken from all known {@link AbstractDbOverlay}.
 *
 * @param {Object.<string,boolean>} general
 *          if true then all elements will be returned, if false then only ones
 *          available right now in the overlay
 *
 * @returns {Promise} array with data from {@link AbstractDbOverlay}
 */
AliasInfoWindow.prototype.getDbOverlaysData = function (general) {
  return this.getCustomMap().getTopMap().getOverlayDataForAlias(this.getAlias(), general);
};

/**
 *
 * @param {PileupRange} pileupRange
 */
AliasInfoWindow.prototype.adjustPileupRangeVisibility = function (pileupRange) {
  pileupRange.start--;
  pileupRange.stop++;

  var dist = pileupRange.stop - pileupRange.start;
  pileupRange.stop += Math.ceil(dist * 0.05);
  pileupRange.start -= Math.ceil(dist * 0.05);
  if (dist < 50) {
    pileupRange.start -= Math.ceil((50 - dist) / 2);
    pileupRange.stop += Math.ceil((50 - dist) / 2);
  }

  if (pileupRange.stop < pileupRange.start) {
    var tmp = pileupRange.stop;
    pileupRange.stop = pileupRange.start;
    pileupRange.start = tmp;
  }

  if (pileupRange.start < 0) {
    pileupRange.stop -= pileupRange.start;
    pileupRange.start = 0;
  }

};
/**
 *
 * @param {DataOverlay[]} params.overlays
 *
 * @returns {Promise|PromiseLike}
 */
AliasInfoWindow.prototype.createGenomicDiv = function (params) {
  var overlays = params.overlays;

  var self = this;

  var result = Functions.createElement({type: "div", className: "minerva-genome-viewer-container"});

  var titleElement = document.createElement("h3");
  titleElement.innerHTML = "Gene variants";
  result.appendChild(titleElement);

  var contentElement = document.createElement("div");
  result.appendChild(contentElement);

  var geneticInformation = false;
  var genomes = [];
  var genomeUrls = [];

  var pileupSource = [{
    viz: pileup.viz.scale(),
    name: 'Scale'
  }, {
    viz: pileup.viz.location(),
    name: 'Location'
  }];
  var pileupRange = {
    contig: 'chr1',
    start: 3000000000,
    stop: 0
  };

  var globalGeneVariants = [];

  var promises = [];
  var overlaysData = [];

  var overlayGeneticInformation = [];

  overlays.forEach(function (overlay, index) {
    promises.push(overlay.getFullAliasesById(self.getAlias().getId()));
    globalGeneVariants[index] = [];
    overlayGeneticInformation[index] = (overlay.getType() === LayoutAlias.GENETIC_VARIANT);
    if (overlay.getType() === LayoutAlias.GENETIC_VARIANT) {
      geneticInformation = true;
      promises.push(self.getCustomMap().getTopMap().getReferenceGenome(overlay.getGenomeType(),
        overlay.getGenomeVersion()).then(function (genome) {
        if (genome.getUrl() !== null && genome.getUrl() !== undefined) {
          if (genomes[genome.getUrl()] === undefined) {
            genomes[genome.getUrl()] = genome;
            genomeUrls.push(genome.getUrl());
          }
        } else {
          logger.warn("Genome for " + overlay.getGenomeType() + ","
            + overlay.getGenomeVersion() + " not loaded");
        }
      }).catch(function () {
        logger.warn("Genome for " + overlay.getGenomeType() + ","
          + overlay.getGenomeVersion() + " not loaded");

      }));
    }
  });
  return Promise.all(promises).then(function (result) {
    promises = [];
    result.forEach(function (overlayEntries) {
      if (overlayEntries !== undefined) {
        //check if we have gene variants split into multiple groups
        if (overlayEntries.length > 1 && overlayEntries[0].getType() === LayoutAlias.GENETIC_VARIANT) {
          var tmpAlias = new LayoutAlias(overlayEntries[0]);
          for (var i = 1; i < overlayEntries.length; i++) {
            tmpAlias.setGeneVariants(tmpAlias.getGeneVariants().concat(overlayEntries[i].getGeneVariants()));
          }
          overlaysData.push(tmpAlias);
        } else {
          overlaysData = overlaysData.concat(overlayEntries);
        }
      }
    });
    return Promise.all(promises);
  }).then(function () {
    for (var i = 0; i < genomeUrls.length; i++) {
      var genome = genomes[genomeUrls[i]];
      pileupSource.splice(0, 0, {
        viz: pileup.viz.genome(),
        isReference: pileupSource.length === 2,
        data: pileup.formats.twoBit({
          url: genome.getUrl()
        }),
        name: 'Reference ' + genome.getVersion()
      });
      for (var k = 0; k < genome.getGeneMappings().length; k++) {
        var mapping = genome.getGeneMappings()[k];
        if (mapping.getUrl() === null || mapping.getUrl() === undefined) {
          logger.warn("No url for gene mapping " + mapping.getName());
        } else {
          pileupSource.push({
            viz: pileup.viz.genes(),
            data: pileup.formats.bigBed({
              url: mapping.getUrl()
            }),
            name: 'Genes ' + mapping.getName()
          });
        }
      }
    }
    for (i = 0; i < overlaysData.length; i++) {
      var data = overlaysData[i];
      if (data !== null && data !== undefined && data.getType() === LayoutAlias.GENETIC_VARIANT) {
        var geneVariants = data.getGeneVariants();
        for (var j = 0; j < geneVariants.length; j++) {
          var variant = geneVariants[j];
          globalGeneVariants[i].push(variant);
          if (variant.getContig() !== undefined) {
            pileupRange.contig = variant.getContig();
          }
          pileupRange.start = Math.min(pileupRange.start, variant.getPosition());
          var length = Math.max(variant.getModifiedDna().length, variant.getOriginalDna().length);
          pileupRange.stop = Math.max(pileupRange.stop, variant.getPosition() + length);
        }
      }
    }
    if (geneticInformation) {
      if (genomeUrls.length === 0) {
        if (self.getCustomMap().getProject().getOrganism() === undefined) {
          contentElement.innerHTML = "No organism defined for this project, cannot display variant data.";
        } else {
          contentElement.innerHTML = "No reference genome defined for this MINERVA instance, cannot display variant data.";
        }
      } else {
        for (i = 0; i < overlays.length; i++) {
          var vcfContent = undefined;
          if (overlayGeneticInformation[i]) {
            vcfContent = self.createVcfString(globalGeneVariants[i]);
          }
          if (vcfContent !== undefined) {
            pileupSource.push({
              viz: pileup.viz.variants(),
              data: pileup.formats.vcf({
                content: vcfContent
              }),
              name: overlays[i].getName(),
              options: {
                variantHeightByFrequency: true
              }
            });
          }
        }

        self.adjustPileupRangeVisibility(pileupRange);

        if (self.pileup !== undefined) {
          logger.debug("Destroy pileup");
          self.pileup.destroy();
          logger.debug("Pileup destroyed");
        }
        self.pileup = pileup.create(contentElement, {
          range: pileupRange,
          tracks: pileupSource
        });


        //Code below is a workaround - for some reason on openlayers events bound by pileup are not working properly...
        var zoomControls = $(".zoom-controls", contentElement);

        zoomControls.children().hide();

        var zoomIn = $('<input type="button" value="+" class="btn-zoom-in"/>');
        zoomIn.on("click", function () {
          var range = self.pileup.getRange();
          var distDiff = Math.floor((range.stop - range.start + 1) / 4);
          range.start += distDiff;
          range.stop -= distDiff;

          $(".zoom-slider", contentElement).val(parseInt($(".zoom-slider", zoomControls).val()) + 1);
          self.pileup.setRange(range);
        });
        zoomIn.appendTo(zoomControls);
        var zoomOut = $('<input type="button" value="-" class="btn-zoom-out"/>');
        zoomOut.on("click", function () {
          var range = self.pileup.getRange();
          var distDiff = Math.floor((range.stop - range.start) / 2);
          if (distDiff === 0) {
            distDiff = 1;
          }
          range.start -= distDiff;
          range.stop += distDiff;
          if (range.start < 0) {
            range.stop -= range.start;
            range.start = 0;
          }

          $(".zoom-slider", contentElement).val(parseInt($(".zoom-slider", zoomControls).val()) - 1);
          self.pileup.setRange(range);
        });
        zoomOut.appendTo(zoomControls);
      }

      result.appendChild(document.createElement("br"));
      return result;
    } else {
      return document.createElement("div");
    }
  });
};

/**
 *
 * @param {GeneVariant[]} geneVariants
 * @returns {string}
 */
AliasInfoWindow.prototype.createVcfString = function (geneVariants) {
  var result = "";
  result += "##fileformat=VCFv4.0\n";
  result += "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\n";
  for (var i = 0; i < geneVariants.length; i++) {
    var variant = geneVariants[i];
    var additionalInfo = "";
    if (variant.getAllelFrequency() !== undefined) {
      additionalInfo = "AF=" + variant.getAllelFrequency();
    }
    var variantId = ".";
    if (variant.getVariantIdentifier() !== undefined) {
      variantId = variant.getVariantIdentifier();
    }
    result += variant.getContig() + "\t" +
      variant.getPosition() + "\t" +
      variantId + "\t" +
      variant.getOriginalDna() + "\t" +
      variant.getModifiedDna() + "\t" +
      "100.0\t" +
      "PASS\t" +
      additionalInfo + "\n";
  }
  return result;
};

/**
 *
 * @param {Alias} alias
 */
AliasInfoWindow.prototype.setAlias = function (alias) {
  if (alias === undefined || alias === null || (!(alias instanceof Alias))) {
    throw new Error("invalid alias");
  }
  this.alias = alias;
};

/**
 *
 * @returns {Alias}
 */
AliasInfoWindow.prototype.getAlias = function () {
  return this.alias;
};

/**
 *
 */
AliasInfoWindow.prototype.destroy = function () {
  AbstractInfoWindow.prototype.destroy.call(this);
  if (this.pileup !== undefined) {
    this.pileup.destroy();
  }
};

/**
 *
 * @returns {Point}
 */
AliasInfoWindow.prototype.getPosition = function () {
  var alias = this.getAlias();
  return new Point(alias.x + alias.width / 2, alias.y + alias.height / 2);
};

module.exports = AliasInfoWindow;
