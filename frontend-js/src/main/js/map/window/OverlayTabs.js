"use strict";

var AbstractGuiElement = require('../../gui/AbstractGuiElement');

var xss = require('xss');

/**
 *
 * @param {Object} params
 * @param {HTMLElement} params.element
 * @param {CustomMap} [params.customMap]
 * @param {Project} [params.project]
 * @param {ServerConnector} [params.serverConnector]
 *
 * @constructor
 * @extends AbstractGuiElement
 */

function OverlayTabs(params) {
  AbstractGuiElement.call(this, params);
}

OverlayTabs.prototype = Object.create(AbstractGuiElement.prototype);
OverlayTabs.prototype.constructor = OverlayTabs;
module.exports = OverlayTabs;
