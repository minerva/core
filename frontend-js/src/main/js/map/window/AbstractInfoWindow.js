"use strict";

var $ = require('jquery');
var Promise = require("bluebird");

var logger = require('../../logger');
var Functions = require('../../Functions');

var Comment = require('../data/Comment');
var GuiConnector = require('../../GuiConnector');
var GuiUtils = require('../../gui/leftPanel/GuiUtils');
var IdentifiedElement = require('../data/IdentifiedElement');
var ObjectWithListeners = require('../../ObjectWithListeners');
var TargettingStructure = require('../data/TargettingStructure');
var OverlayTabs = require("./OverlayTabs");

/**
 * Class representing any info window in our map.
 *
 * @param {IdentifiedElement} params.identifiedElement
 * @param {AbstractCustomMap} params.map
 * @param {Marker} params.marker
 *
 * @constructor
 */
function AbstractInfoWindow(params) {
  // call super constructor
  ObjectWithListeners.call(this);

  var self = this;

  self.setIdentifiedElement(params.identifiedElement);

  self.setCustomMap(params.map);
  self.setMarker(params.marker);

  self.setContent(this.createWaitingContentDiv());

  self._overlayFullView = [];

  self.registerPropertyType("overlayFullView");

  self.registerListenerType("onShow");
  self.registerListenerType("onUpdate");

  self.setGuiUtils(new GuiUtils());

  var dbOverlaySearchChanged = function () {
    return self.update();
  };
  var searchDbOverlay = params.map.getTopMap().getOverlayByName("search");
  if (searchDbOverlay !== undefined) {
    searchDbOverlay.addListener("onSearch", dbOverlaySearchChanged);
  }
  var commentDbOverlay = params.map.getTopMap().getOverlayByName("comment");
  if (commentDbOverlay !== undefined) {
    commentDbOverlay.addListener("onSearch", dbOverlaySearchChanged);
  }
}

AbstractInfoWindow.prototype = Object.create(ObjectWithListeners.prototype);
AbstractInfoWindow.prototype.constructor = AbstractInfoWindow;

/**
 * Returns <code>true</code> if overlay should visualize all possible values.
 *
 * @param {string} overlayName
 *          name of the overlay
 * @returns {boolean}, <code>true</code> if overlay should visualize all possible values
 */
AbstractInfoWindow.prototype.isOverlayFullView = function (overlayName) {
  if (this._overlayFullView[overlayName] === undefined) {
    this._overlayFullView[overlayName] = false;
  }
  return this._overlayFullView[overlayName];
};

/**
 * Returns associative array with information if specific overlay should present
 * all possible results or only specified by the data searched by user.
 *
 * @returns {Object.<string,boolean>} with information if specific overlay should present all
 *          possible results or only specified by the data searched by user
 */
AbstractInfoWindow.prototype.getOverlayFullViewArray = function () {
  return this._overlayFullView;
};

/**
 *
 * @param {string} overlayName
 * @param {boolean} value
 * @returns {Promise}
 */
AbstractInfoWindow.prototype.setOverlayFullView = function (overlayName, value) {
  var oldVal = this._overlayFullView[overlayName];
  this._overlayFullView[overlayName] = value;
  if (oldVal !== value) {
    return this.firePropertyChangeListener("overlayFullView", overlayName + "," + oldVal, value);
  } else {
    return Promise.resolve();
  }
};

/**
 * This method checks if {@link AbstractInfoWindow} is opened.
 *
 * @returns {Boolean} <code>true</code> if window is opened,
 *          <code>false</code> otherwise
 */
AbstractInfoWindow.prototype.isOpened = function () {
  if (this._infoWindow === undefined) {
    return false;
  }
  return this._infoWindow.isOpened();
};

/**
 * Opens Info Window.
 *
 * @param {Marker} [newMarker]
 *
 * @returns {PromiseLike<any> | Promise<any>}
 */
AbstractInfoWindow.prototype.open = function (newMarker) {
  var self = this;
  var infoWindow = self._infoWindow;
  if (infoWindow === null || infoWindow === undefined) {
    logger.warn("Cannot open window.");
    return Promise.resolve();
  }
  if (newMarker !== undefined) {
    infoWindow.setMarker(newMarker);
  }
  infoWindow.open();

  return self.update().then(function () {
    return self.callListeners("onShow");
  });
};

/**
 * Sets new content of the info window.
 *
 * @param {HTMLElement|string} content
 *          new content of the window
 */
AbstractInfoWindow.prototype.setContent = function (content) {
  var self = this;
  self._content = content;
  if (self._infoWindow !== undefined) {
    self._infoWindow.setContent(content);
  }
};

/**
 * Returns content visualized in the info window.
 *
 * @returns {string|HTMLElement} content visualized in the info window
 */
AbstractInfoWindow.prototype.getContent = function () {
  return this._content;
};

/**
 * Creates div for an overlay data.
 *
 * @param {AbstractDbOverlay} overlay
 *          corresponding overlay
 * @param {BioEntity[]|Comment[]|Drug[]|Chemical[]} data
 *          data taken from overlay
 * @returns {HTMLElement} div for given overlay data
 */
AbstractInfoWindow.prototype.createOverlayInfoDiv = function (overlay, data) {
  var alias = this.alias;
  if (alias !== undefined) {
    if (alias.getType() !== undefined) {
      if (overlay.name === "drug") {
        if (alias.getType().toUpperCase() === "RNA" ||
          alias.getType().toUpperCase() === "PROTEIN" ||
          alias.getType().toUpperCase() === "GENE") {
          return this._createTargetInfoDiv({overlay: overlay, data: data, name: ""});
        } else {
          return null;
        }
      } else if (overlay.name === "chemical") {
        if (alias.getType().toUpperCase() === "RNA" ||
          alias.getType().toUpperCase() === "PROTEIN" ||
          alias.getType().toUpperCase() === "GENE") {
          return this._createTargetInfoDiv({overlay: overlay, data: data, name: ""});
        } else {
          return null;
        }
      } else if (overlay.name === "comment") {
        return this._createCommentInfoDiv(overlay, data);
      } else {
        logger.warn("Unknown overlay data for AliasInfoWindow: " + overlay.name);
        return this._createDefaultInfoDiv(overlay, data);
      }
    } else {
      logger.debug(alias);
      throw new Error("Cannot customize info window. Alias type is unknown ");
    }
  } else {
    if (overlay.getName() === "comment") {
      return this._createCommentInfoDiv(overlay, data);
    } else {
      logger.debug("Cannot customize info window. Alias not defined. Overlay: " + overlay.getName());
      return null;
    }
  }
};

/**
 * Creates and returns div for comment overlay information.
 *
 * @param {AbstractDbOverlay} overlay
 * @param {Comment[]} data
 *          data taken from comment overlay
 * @returns {HTMLElement} div for comment overlay information
 */
AbstractInfoWindow.prototype._createCommentInfoDiv = function (overlay, data) {
  if (data.length === 0 || data[0] === undefined) {
    return null;
  }
  var result = document.createElement("div");

  var titleElement = document.createElement("h3");
  titleElement.innerHTML = "Comments";
  result.appendChild(titleElement);
  for (var i = 0; i < data.length; i++) {
    var comment = data[i];
    if (comment instanceof Comment) {
      if (!comment.isRemoved()) {
        result.appendChild(document.createElement("hr"));
        var commentId = document.createElement("div");
        commentId.innerHTML = '#' + comment.getId();
        result.appendChild(commentId);
        result.appendChild(document.createElement("br"));
        var commentContent = Functions.createElement({type: "div", content: comment.getContent(), xss: true});

        result.appendChild(commentContent);
      }
    } else {
      throw new Error("Invalid comment data: " + comment);
    }
  }

  return result;
};

/**
 * Creates and returns div for unknown overlay.
 *
 * @param {AbstractDbOverlay} overlay
 * @param {Array} data
 *          data taken from overlay
 * @returns {HTMLElement} div for overlay information
 */

AbstractInfoWindow.prototype._createDefaultInfoDiv = function (overlay, data) {
  var divElement = document.createElement("div");
  var count = 0;

  var titleElement = document.createElement("h3");
  var title = document.createTextNode(overlay.getName());
  titleElement.appendChild(title);
  divElement.appendChild(titleElement);
  for (var searchId in data) {
    if (data.hasOwnProperty(searchId) && data[searchId] !== undefined && data[searchId] !== null) {
      count++;
      var resultTitleElement = document.createElement("h4");
      var resultTitle = document.createTextNode(searchId);
      resultTitleElement.appendChild(resultTitle);
      divElement.appendChild(resultTitleElement);

      var keys = Object.keys(data[searchId]);
      for (var i = 0; i < keys.length; i++) {
        var resultValElement = document.createElement("p");
        var resultVal = document.createTextNode(keys[i] + ": " + data[searchId][keys[i]]);
        resultValElement.appendChild(resultVal);
        divElement.appendChild(resultValElement);
      }
    }
  }

  if (count === 0) {
    divElement = null;
  }
  return divElement;
};

/**
 * Returns Marker object where this info window is attached.
 *
 * @returns {Marker} object where this info window is attached
 */
AbstractInfoWindow.prototype.getMarker = function () {
  return this._marker;
};

/**
 *
 * @param {Marker} marker
 */
AbstractInfoWindow.prototype.setMarker = function (marker) {
  this._marker = marker;
  if (this._infoWindow !== undefined) {
    this._infoWindow.setMarker(marker);
  }
};

/**
 * Returns {@link AbstractCustomMap} where this window is presented.
 *
 * @returns {AbstractCustomMap} where this window is presented
 */
AbstractInfoWindow.prototype.getCustomMap = function () {
  return this.customMap;
};

/**
 *
 * @param {AbstractCustomMap} map
 */
AbstractInfoWindow.prototype.setCustomMap = function (map) {
  if (map === undefined) {
    throw new Error("Map must be defined");
  }
  this.customMap = map;
};

/**
 * Returns html DOM object with content that should presented when waiting for
 * some data from server.
 *
 * @returns {HTMLElement} html with content that should presented when waiting for
 *          some data from server
 */
AbstractInfoWindow.prototype.createWaitingContentDiv = function () {
  var result = document.createElement("div");
  var img = document.createElement("img");
  img.src = GuiConnector.getImgPrefix() + GuiConnector.getLoadingImg();
  var message = document.createElement("h4");
  message.innerHTML = "loading...";
  result.appendChild(img);
  result.appendChild(message);
  return result;
};

/**
 * This is a generic method that updates content of the window.
 *
 * @returns {Promise|PromiseLike}
 * @private
 */
AbstractInfoWindow.prototype._updateContent = function () {
  var contentDiv = null;
  var self = this;

  if (!self.isOpened()) {
    return Promise.resolve();
  } else {
    self.setContent(self.createWaitingContentDiv());

    return self.createContentDiv().then(function (content) {
      contentDiv = content;
      return self.createDbOverlaysDiv();
    }).then(function (overlaysDiv) {
      if (overlaysDiv !== undefined && overlaysDiv !== null) {
        contentDiv.appendChild(overlaysDiv);
      }
      self.setContent(contentDiv);
      return self.callListeners("onUpdate");
    }).then(function () {
      return contentDiv;
    });
  }
};

/**
 * Creates and returns div with overlays content.
 *
 * @returns {Promise<HTMLElement>} with html representing data taken from
 *          {@link AbstractDbOverlay} for this window
 */
AbstractInfoWindow.prototype.createDbOverlaysDiv = function () {
  var self = this;
  var result = document.createElement("div");
  var overlayTabs = null;
  return this.getDbOverlaysData(self.getOverlayFullViewArray()).then(function (overlayData) {
    for (var i = 0; i < overlayData.length; i++) {
      var overlay = overlayData[i].overlay;
      var data = overlayData[i].data;
      var overlayInfo = self.createOverlayInfoDiv(overlay, data);
      if (overlayInfo !== null) {
        if (overlay.name === "drug" || overlay.name === "chemical") {
          if (overlayTabs == null) {
            var div = document.createElement("div")
            result.appendChild(div);
            overlayTabs = new OverlayTabs({element: div, project: self.getCustomMap().getProject()});
            self.getGuiUtils().initTabContent(overlayTabs);
          }
          self.getGuiUtils().addTab(overlayTabs, {name: overlay.name, content: overlayInfo});
        } else {
          result.appendChild(overlayInfo);
        }
      }
    }
    return result;
  });
};

// noinspection JSUnusedLocalSymbols
/**
 * Returns array with data taken from all known {@link AbstractDbOverlay}.
 *
 * @param {Object.<string,boolean>} general
 *          if true then all elements will be returned, if false then only ones
 *          available right now in the overlay
 *
 * @returns {Promise} array with data from {@link AbstractDbOverlay}
 */
AbstractInfoWindow.prototype.getDbOverlaysData = function (general) {
  throw new Error("Not implemented");
};

/**
 * Abstract method (to be implemented by subclasses) for updating content.
 *
 * @returns {Promise}
 */
AbstractInfoWindow.prototype.update = function () {
  return this._updateContent();
};

/**
 *
 * @param {string} params.name
 * @param {AbstractDbOverlay} params.overlay
 * @param {Array} params.data
 * @returns {HTMLElement}
 * @private
 */
AbstractInfoWindow.prototype._createTargetInfoDiv = function (params) {
  var overlay = params.overlay;
  var data = params.data;
  var name = params.name;

  var self = this;
  var result = document.createElement("div");

  var titleElement = document.createElement("h3");
  titleElement.innerHTML = name;
  result.appendChild(titleElement);
  if (overlay.allowGeneralSearch()) {
    var checkboxDiv = document.createElement("div");
    checkboxDiv.style.textAlign = "center";
    var showAllButton = document.createElement("button");
    var hideAllButton = document.createElement("button");

    var toggleAll = function (show) {
      var ie = new IdentifiedElement(self.alias);
      result.removeChild(table);
      var messageImg = Functions.createElement({
        type: "img",
        src: 'resources/images/icons/ajax-loader.gif'
      });
      result.appendChild(messageImg);
      return overlay.getDetailDataByIdentifiedElement(ie, show).then(function (data) {
        table = self._createTableForTargetDiv(data, overlay);
        result.appendChild(table);
      }).finally(function () {
        result.removeChild(messageImg);
        if (show) {
          $(showAllButton).hide();
          $(hideAllButton).show();
        } else {
          $(hideAllButton).hide();
          $(showAllButton).show();
        }
      })
    };

    showAllButton.style.display = "inline-block";
    showAllButton.innerHTML = "Click to show all " + overlay.getName() + "s for this element";
    showAllButton.onclick = function () {
      toggleAll(true);
    }
    checkboxDiv.appendChild(showAllButton);

    hideAllButton.style.display = "inline-block";
    hideAllButton.innerHTML = "Click to hide all " + overlay.getName() + "s for this element";
    hideAllButton.onclick = function () {
      toggleAll(false);
    }
    checkboxDiv.appendChild(hideAllButton);

    if (self.isOverlayFullView(overlay.getName())) {
      $(showAllButton).hide();
    } else {
      $(hideAllButton).hide();
    }


    result.appendChild(checkboxDiv);
  }
  var count = 0;
  for (var dataId in data) {
    if (data.hasOwnProperty(dataId)) {
      count++;
    }
  }

  var table = self._createTableForTargetDiv(data, overlay);

  if (count === 0 && !overlay.allowGeneralSearch() && !this.isOverlayFullView(overlay.getName())) {
    result = null;
  }
  if (result !== null) {
    result.appendChild(table);
  }
  return result;
};

/**
 *
 * @param {Array} data
 * @param {AbstractDbOverlay} overlay
 * @returns {HTMLElement}
 * @private
 */
AbstractInfoWindow.prototype._createTableForTargetDiv = function (data, overlay) {
  var result = document.createElement("div")
  var table = document.createElement("table");
  table.className = "minerva-window-drug-table";
  result.appendChild(table);

  var dataTable = $(table).DataTable({
    fnRowCallback: function (nRow, aData) {
      nRow.setAttribute('id', aData[0]);
    },
    columns: [{
      title: 'Name'
    }],
    language: {
      emptyTable: "Currently not showing any " + overlay.getName() + " entries"
    }
  });

  $(table).on("click", "a", function () {
    return overlay.searchByQuery(this.innerHTML + ";");
  });
  var page = dataTable.page();

  var rows = [];
  for (var searchId in data) {
    if (data.hasOwnProperty(searchId)) {
      var nameContent = searchId;
      if (typeof data[searchId] === "string") {
        nameContent = data[searchId];
      } else if (data[searchId] instanceof TargettingStructure) {
        nameContent = data[searchId].getName();
      }
      rows.push(["<a href='#'>" + nameContent + "</a>"]);
    }
  }

  //it should be simplified, but I couldn't make it work
  dataTable.clear().rows.add(rows).page(page).draw(false).page(page).draw(false);

  return result;
};

/**
 *
 * @returns {PromiseLike<any> | Promise<any>}
 */
AbstractInfoWindow.prototype.init = function () {
  var self = this;
  var promises = [
    // default settings of visualizing full information about elements
    this.setOverlayFullView("drug", false),
    this.setOverlayFullView("chemical", false),
    this.setOverlayFullView("search", false),
    // only all comments should be visible from the beginning
    this.setOverlayFullView("comment", true)
  ];

  return Promise.all(promises).then(function () {
    // listener called when user want to see all data about specific data overlay
    var onOverlayFullViewChanged = function (e) {
      var self = e.object;
      // first change the content of the element
      return self.update().then(function () {
        if (e.newVal) {
          var element = new IdentifiedElement({
            objectId: self.getElementId(),
            modelId: self.getCustomMap().getId(),
            type: self.getElementType()
          });
          var topMap = self.getCustomMap().getTopMap();
          return topMap.retrieveOverlayDetailDataForElement(element, self.getOverlayFullViewArray());
        }
      });

    };

    self.addPropertyChangeListener("overlayFullView", onOverlayFullViewChanged);

    self._infoWindow = self.getCustomMap().getMapCanvas().createInfoWindow({
      content: self.content,
      position: self.getPosition(),
      marker: self._marker
    });

    return ServerConnector.getConfiguration();
  }).then(function (configuration) {
    self.getGuiUtils().setConfiguration(configuration);
    self.getGuiUtils().setMap(self.getCustomMap());
  });
};

/**
 *
 * @param {GuiUtils} guiUtils
 */
AbstractInfoWindow.prototype.setGuiUtils = function (guiUtils) {
  this._guiUtils = guiUtils;
};

/**
 *
 * @returns {GuiUtils}
 */
AbstractInfoWindow.prototype.getGuiUtils = function () {
  return this._guiUtils;
};

/**
 *
 * @param {IdentifiedElement} identifiedElement
 */
AbstractInfoWindow.prototype.setIdentifiedElement = function (identifiedElement) {
  if (identifiedElement === undefined) {
    throw new Error("identifiedElement cannot be undefined");
  }
  this._identifiedElement = identifiedElement;
};

/**
 *
 * @returns {IdentifiedElement}
 */
AbstractInfoWindow.prototype.getIdentifiedElement = function () {
  return this._identifiedElement;
};


/**
 * Method returning identifier of the object for which this window was created.
 *
 * @returns {string|number}
 */
AbstractInfoWindow.prototype.getElementId = function () {
  return this.getIdentifiedElement().getId();
};

/**
 * Method returning type of the object for which this window was created.
 *
 * @returns {string}
 */
AbstractInfoWindow.prototype.getElementType = function () {
  return this.getIdentifiedElement().getType();
};

/**
 * @returns {Point}
 */
AbstractInfoWindow.prototype.getPosition = function () {
  throw new Error("Not Implemented");
};

AbstractInfoWindow.prototype.destroy = function () {
  var infoWindow = this._infoWindow;
  if (infoWindow !== null && infoWindow !== undefined) {
    return infoWindow.destroy();
  }
};

module.exports = AbstractInfoWindow;
