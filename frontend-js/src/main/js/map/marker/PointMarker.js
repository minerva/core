"use strict";

var AbstractMarker = require('./AbstractMarker');
var PointData = require('../data/PointData');

/**
 * This class describes a marker (connected to some specific point,
 * but not to any element) that is visualized on the map.
 *
 * @param {IdentifiedElement} params.element
 * @param {AbstractCustomMap} params.map
 * @param {function|function[]} params.onclick
 * @constructor
 * @extends AbstractMarker
 */
function PointMarker(params) {
  var self = this;
  AbstractMarker.call(self, params);
  self.setPointData(new PointData(self.getIdentifiedElement()));
}

PointMarker.prototype = Object.create(AbstractMarker.prototype);
PointMarker.prototype.constructor = PointMarker;

/**
 *
 * @returns {Promise<any>}
 */
PointMarker.prototype.init = function () {
  var self = this;
  return self._init().then(function () {
    return self.show();
  });
};

/**
 * Returns identifier of the object.
 *
 * @returns {string} identifier of the object
 */
PointMarker.prototype.getId = function () {
  return this.getPointData().getId();
};

/**
 * Returns a {@link PointData} that is connected to this marker.
 *
 * @returns {PointData} that is connected to this marker
 */
PointMarker.prototype.getPointData = function () {
  return this._pointData;
};

/**
 *
 * @param {PointData} pointData
 */
PointMarker.prototype.setPointData = function (pointData) {
  this._pointData = pointData;
};

/**
 *
 * @returns {Point}
 */
PointMarker.prototype.getCoordinates = function () {
  return this._pointData.getPoint();
};

module.exports = PointMarker;
