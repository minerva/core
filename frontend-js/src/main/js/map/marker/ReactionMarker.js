"use strict";

var AbstractMarker = require('./AbstractMarker');
var Point = require('../canvas/Point');

/**
 * This class describes a marker (connected to {@link Reaction}) that
 * is visualized on the map.
 *
 * @param {IdentifiedElement} params.element
 * @param {AbstractCustomMap} params.map
 * @param {function|function[]} params.onclick
 * @constructor
 * @extends AbstractMarker
 */
function ReactionMarker(params) {
  AbstractMarker.call(this, params);
}

ReactionMarker.prototype = Object.create(AbstractMarker.prototype);
ReactionMarker.prototype.constructor = ReactionMarker;

/**
 *
 * @returns {PromiseLike<any>}
 */
ReactionMarker.prototype.init = function () {
  var self = this;
  return self.getCustomMap().getModel().getReactionById(self.getId()).then(function (reactionData) {
    self.setReactionData(reactionData);
    return self._init();
  }).then(function () {
    return self.show();
  });
};

/**
 * Returns {@link Reaction} data for this marker.
 *
 * @returns {Reaction} data for this marker
 */
ReactionMarker.prototype.getReactionData = function () {
  return this._reactionData;
};

/**
 * Sets {@link Reaction} data for this marker.
 *
 * @param {Reaction} data
 *           data for this marker
 */
ReactionMarker.prototype.setReactionData = function (data) {
  this._reactionData = data;
};

/**
 * Returns coordinates where marker is pointing.
 *
 * @returns {Point} - coordinates where marker is pointing
 */
ReactionMarker.prototype.getCoordinates = function () {
  return new Point(this._reactionData.getCenter().x, this._reactionData.getCenter().y);
};

module.exports = ReactionMarker;
