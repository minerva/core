"use strict";

var AbstractMarker = require('./AbstractMarker');
var Point = require('../canvas/Point');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

/**
 * This class describes a marker (connected to {@link Alias}) that is
 * visualized on the map.
 *
 * @param {IdentifiedElement} params.element
 * @param {AbstractCustomMap} params.map
 * @param {function|function[]} [params.onClick]
 * @constructor
 * @extends AbstractMarker
 */
function AliasMarker(params) {
  AbstractMarker.call(this, params);
}

AliasMarker.prototype = Object.create(AbstractMarker.prototype);
AliasMarker.prototype.constructor = AliasMarker;

/**
 * Returns {@link Alias} data for this marker.
 *
 * @returns {Alias} data for this marker
 */
AliasMarker.prototype.getAliasData = function () {
  return this._aliasData;
};

/**
 * Sets {@link Alias} data for this marker.
 *
 * @param {Alias} data
 *           data for this marker
 */
AliasMarker.prototype.setAliasData = function (data) {
  this._aliasData = data;
};

/**
 * Returns coordinates where marker is pointing.
 *
 * @returns {Point} - coordinates where marker is pointing
 */
AliasMarker.prototype.getCoordinates = function () {
  var alias = this.getAliasData();
  return new Point(alias.getX() + alias.getWidth() / 2, alias.getY() + alias.getHeight() / 2);
};

/**
 * TODO sel._init should be called as overriden method (withouth '_')
 * @returns {Promise<any>}
 */
AliasMarker.prototype.init = function () {
  var self = this;
  var model = self.getCustomMap().getModel();
  return model.getByIdentifiedElement(self.getIdentifiedElement()).then(function (aliasData) {
    self.setAliasData(aliasData);
    return self._init();
  }).then(function () {
    return self.show();
  });
};

module.exports = AliasMarker;
