"use strict";

var AliasMarker = require('./AliasMarker');
var IdentifiedElement = require('../data/IdentifiedElement');
var PointMarker = require('./PointMarker');
var ReactionMarker = require('./ReactionMarker');

var AbstractSurfaceElement = require('../surface/AbstractSurfaceElement');
var AliasSurface = require('../surface/AliasSurface');
var ReactionSurface = require('../surface/ReactionSurface');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
var Promise = require("bluebird");

var ObjectWithListeners = require('../../ObjectWithListeners');

/**
 * @typedef {Object} IconData
 * @property {IdentifiedElement} element
 * @property {string[]} icons
 */

/**
 *
 * @param {AbstractCustomMap} params.map
 * @constructor
 */
function MarkerSurfaceCollection(params) {
  var self = this;
  ObjectWithListeners.call(this);

  /**
   * @type {Object.<string, IconData>}
   */
  self._overlayIcons = {};
  // @type {Object.<string, AbstractMarker>}
  self._markers = {};
  // @type {Object.<string, Object.<string,AbstractMarker>>}
  self._overlaySurfaces = {
    ALIAS: [],
    REACTION: [],
    POINT: []
  };

  self.setMap(params.map);
}

MarkerSurfaceCollection.prototype = Object.create(ObjectWithListeners.prototype);
MarkerSurfaceCollection.prototype.constructor = MarkerSurfaceCollection;

/**
 *
 * @param {AbstractCustomMap} map
 */
MarkerSurfaceCollection.prototype.setMap = function (map) {
  if (map === undefined) {
    throw new Error("Map must be defined");
  }
  this._map = map;
};

/**
 *
 * @returns {AbstractCustomMap}
 */
MarkerSurfaceCollection.prototype.getMap = function () {
  return this._map;
};

/**
 *
 * @param {AbstractDbOverlay} overlay
 * @returns {Promise}
 */
MarkerSurfaceCollection.prototype.refreshOverlayMarkers = function (overlay) {
  var promises = [];
  var self = this;
  var overlayData = self._overlayIcons[overlay.getName()];
  for (var elementId in overlayData) {
    if (overlayData.hasOwnProperty(elementId)) {
      var x = function () {
        var marker = self._markers[elementId];
        promises.push(marker.hide().then(function () {
          return marker.show();
        }));
      }();
    }
  }
  overlayData = self._overlaySurfaces[overlay.getName()];
  for (elementId in overlayData) {
    if (overlayData.hasOwnProperty(elementId)) {
      x = function () {
        var surface = overlayData[elementId];
        promises.push(surface.hide().then(function () {
          return surface.show();
        }));
      }();
    }
  }
  return Promise.all(promises);
};

/**
 *
 * @param {string[]} params.icons
 * @param {IdentifiedElement} params.element
 * @returns {Promise}
 */
MarkerSurfaceCollection.prototype.addMarker = function (params) {
  var element = params.element;
  var icons = params.icons;

  var self = this;

  var result = self.getMarker(element);
  if (result !== undefined) {
    return result.addIcons(icons).then(function () {
      if (self.getMap().isInitialized() && !result.isInitialized()) {
        return result.init().then(function () {
          return result;
        });
      } else {
        return Promise.resolve(result);
      }
    });
  }

  var MarkerConstructor = null;
  if (element.getType() === "ALIAS") {
    MarkerConstructor = AliasMarker;
  } else if (element.getType() === "REACTION") {
    MarkerConstructor = ReactionMarker;
  } else if (element.getType() === "POINT") {
    MarkerConstructor = PointMarker;
  } else {
    throw new Error("Unknown type of the element in overlay: " + element.type);
  }
  result = new MarkerConstructor({
    element: element,
    map: self.getMap(),
    onClick: [function () {
      return self.getMap()._openInfoWindowForIdentifiedElement(element, result.getMarker());
    }, function () {
      return self.getMap().getTopMap().callListeners("onBioEntityClick", element);
    }]
  });
  self.putMarker(element, result);
  return result.setIcons(icons).then(function () {
    if (self.getMap().isInitialized()) {
      return result.init().then(function () {
        return result;
      });
    } else {
      return Promise.resolve(result);
    }
  });
};

/**
 *
 * @param {string[]} params.icons
 * @param {IdentifiedElement} params.element
 * @returns {Promise}
 */
MarkerSurfaceCollection.prototype.removeMarker = function (params) {
  var element = params.element;
  var icons = params.icons;

  var self = this;

  var result = self.getMarker(element);
  return result.removeIcons(icons);
};

/**
 *
 * @param {IdentifiedElement} element
 * @param {AbstractDbOverlay} dbOverlay
 * @returns {PromiseLike<any>}
 */
MarkerSurfaceCollection.prototype.createSurfaceForDbOverlay = function (element, dbOverlay) {
  var self = this;

  var result = self.getSurface({element: element, overlay: dbOverlay});
  if (result !== undefined) {
    result.updateIdentifiedElement(element);
    return Promise.resolve(result);
  }

  var map = self.getMap();
  /**
   * @type {function[]}
   */
  var onclickFunctions = [function () {
    return self.getMap().getTopMap().callListeners("onBioEntityClick", element);
  }, function () {
    return self.getMap().getTopMap().getOverlayByName("search").searchByTarget(element);
  }];
  if (element.getType() === "ALIAS") {
    return map.getModel().getAliasById(element.getId()).then(function (alias) {
      result = new AliasSurface({
        alias: alias,
        map: map,
        onClick: onclickFunctions,
        color: element.getColor(),
        opacity: element.getOpacity(),
        strokeWeight: element.getLineWeight(),
        strokeColor: element.getLineColor(),
        strokeOpacity: element.getLineOpacity()
      });
      self.putSurface({element: element, overlay: dbOverlay}, result);
      if (self.getMap().isInitialized()) {
        return result.init().then(function () {
          return result.show();
        }).then(function () {
          return result;
        });
      } else {
        return result;
      }
    });
  } else if (element.getType() === "REACTION") {
    return map.getModel().getReactionById(element.getId()).then(function (reactionData) {
      result = new ReactionSurface({
        reaction: reactionData,
        map: map,
        customized: true,
        color: element.getColor(),
        onClick: onclickFunctions
      });
      result.addListener("onClick", function () {
        return map.getTopMap().openInfoWindowForIdentifiedElement(element).then(function (infoWindow) {
          result.getMapCanvasObjects()[0].addListener("onHide", function () {
            return infoWindow._infoWindow.hide();
          });
          return infoWindow;
        });
      });
      self.putSurface({element: element, overlay: dbOverlay}, result);
      if (self.getMap().isInitialized()) {
        return result.init().then(function () {
          return result.show();
        }).then(function () {
          return result;
        });
      } else {
        return result;
      }
    });
  } else if (element.getType() === "POINT") {
    throw new Error("Not implemented");
  } else {
    throw new Error("Unknown type of the element in overlay: " + element.getType());
  }
};

/**
 *
 * @returns {AbstractMarker[]}
 */
MarkerSurfaceCollection.prototype.getMarkers = function () {
  var result = [];
  var markers = this._markers;
  for (var key in markers) {
    if (markers.hasOwnProperty(key)) {
      var marker = markers[key];
      if (marker.isShown()) {
        result.push(marker);
      }
    }
  }

  return result;
};

/**
 *
 * @returns {AbstractSurfaceElement[]}
 */
MarkerSurfaceCollection.prototype.getSurfaces = function () {
  var result = [];

  var overlaySurfaces = this._overlaySurfaces;
  for (var overlayName in overlaySurfaces) {
    if (overlaySurfaces.hasOwnProperty(overlayName)) {
      var surfaces = overlaySurfaces[overlayName];
      for (var key in surfaces) {
        if (surfaces.hasOwnProperty(key)) {
          result.push(surfaces[key]);
        }
      }
    }
  }

  return result;
};

/**
 *
 * @param {IdentifiedElement} element
 * @returns {AbstractMarker|undefined}
 */
MarkerSurfaceCollection.prototype.getMarker = function (element) {
  return this._markers[element.toString()];
};

/**
 *
 * @param {IdentifiedElement} element
 * @param {AbstractMarker} marker
 */
MarkerSurfaceCollection.prototype.putMarker = function (element, marker) {
  this._markers[element.toString()] = marker;
};

/**
 *
 * @param params
 * @returns {AbstractSurfaceElement|undefined}
 */
MarkerSurfaceCollection.prototype.getSurface = function (params) {
  var element = new IdentifiedElement(params.element);
  var overlay = params.overlay;

  var overlaySurfaces = this._overlaySurfaces[overlay.getName()];
  if (overlaySurfaces === undefined) {
    this._overlaySurfaces[overlay.getName()] = [];
    overlaySurfaces = this._overlaySurfaces[overlay.getName()];
  }
  return overlaySurfaces[element.toString()];
};

/**
 *
 * @param {Object} params
 * @param {IdentifiedElement} params.element
 * @param {AbstractDbOverlay} params.overlay
 * @param {AbstractSurfaceElement} surface
 */
MarkerSurfaceCollection.prototype.putSurface = function (params, surface) {
  var element = params.element;
  var overlay = params.overlay;

  var overlaySurfaces = this._overlaySurfaces[overlay.getName()];
  if (overlaySurfaces === undefined) {
    this._overlaySurfaces[overlay.getName()] = [];
    overlaySurfaces = this._overlaySurfaces[overlay.getName()];
  }
  overlaySurfaces[element.toString()] = surface;
};

/**
 *
 * @param {AbstractSurfaceElement[]} modifiedMarkersAndSurfaces
 * @param {AbstractDbOverlay} dbOverlay
 */
MarkerSurfaceCollection.prototype.removeUnmodifiedSurfaces = function (modifiedMarkersAndSurfaces, dbOverlay) {
  var modifiedSurfaces = {
    "ALIAS": [],
    "REACTION": [],
    "POINT": []
  };

  for (var i = 0; i < modifiedMarkersAndSurfaces.length; i++) {
    var object = modifiedMarkersAndSurfaces[i];
    var identifiedElement = object.getIdentifiedElement();
    if (object instanceof AbstractSurfaceElement) {
      modifiedSurfaces[identifiedElement.toString()] = true;
    } else {
      throw new Error("Unknown class type: " + object.prototype.name);
    }
  }

  var surfaceOverlaysPerType = this._overlaySurfaces[dbOverlay.getName()];
  for (var key in surfaceOverlaysPerType) {
    if (surfaceOverlaysPerType.hasOwnProperty(key)) {
      if (!modifiedSurfaces[key]) {
        surfaceOverlaysPerType[key].hide();
        delete surfaceOverlaysPerType[key];
      }
    }
  }
};

/**
 *
 * @param {IdentifiedElement[]} allElements
 * @param {AbstractDbOverlay} overlay
 * @returns {PromiseLike<any>}
 */
MarkerSurfaceCollection.prototype.renderOverlay = function (allElements, overlay) {
  var self = this;
  var markerElements = [];
  var surfaceElements = [];
  for (var i = 0; i < allElements.length; i++) {
    var element = allElements[i];
    if (element.getModelId() === self.getMap().getId()) {
      if (element.getIcon() !== null && element.getIcon() !== undefined) {
        markerElements.push(element);
      } else {
        surfaceElements.push(element);
      }
    }
  }

  var markers;
  return self.renderOverlayMarkers(markerElements, overlay).then(function (result) {
    markers = result;
    return self.renderOverlaySurfaces(surfaceElements, overlay);
  }).then(function (surfaces) {
    return markers.concat(surfaces);
  });
};

/**
 *
 * @param {IdentifiedElement} element
 * @constructor
 */
function IconDifference(element) {
  this.element = element;
  this.iconsToAdd = [];
  this.iconsToRemove = [];
}

/**
 *
 * @param {string} icon
 */
IconDifference.prototype.addIcon = function (icon) {
  var index = this.iconsToRemove.indexOf(icon);
  if (index > -1) {
    this.iconsToRemove.splice(index, 1);
  } else {
    this.iconsToAdd.push(icon);
  }
};

/**
 *
 * @param {string} icon
 */
IconDifference.prototype.removeIcon = function (icon) {
  var index = this.iconsToAdd.indexOf(icon);
  if (index > -1) {
    this.iconsToAdd.splice(index, 1);
  } else {
    this.iconsToRemove.push(icon);
  }
};

/**
 *
 * @param {string[]} icons
 */
IconDifference.prototype.removeIcons = function (icons) {
  for (var i = 0; i < icons.length; i++) {
    this.removeIcon(icons[i]);
  }
};

/**
 *
 * @param {IdentifiedElement[]} elements
 * @param {AbstractDbOverlay} overlay
 * @returns {PromiseLike<any>}
 */
MarkerSurfaceCollection.prototype.renderOverlayMarkers = function (elements, overlay) {
  var self = this;

  var markers = [];
  return self.getMap().getModel().getByIdentifiedElements(elements).then(function () {
    var elementsToProcess = [];
    var i, element, icon, data, entry, key;
    for (i = 0; i < elements.length; i++) {
      element = elements[i];
      icon = element.getIcon();
      data = elementsToProcess[element.toString()];
      if (data === undefined) {
        elementsToProcess[element.toString()] = new IconDifference(element);
        data = elementsToProcess[element.toString()]
      }
      data.addIcon(icon);
    }
    var iconData = self.getOverlayIconData(overlay);
    for (key in iconData) {
      if (iconData.hasOwnProperty(key)) {
        entry = iconData[key];
        element = entry.element;
        data = elementsToProcess[element.toString()];
        if (data === undefined) {
          elementsToProcess[element.toString()] = new IconDifference(element);
          data = elementsToProcess[element.toString()]
        }
        data.removeIcons(entry.icons);
      }
    }

    var promises = [];

    for (key in elementsToProcess) {
      if (elementsToProcess.hasOwnProperty(key)) {
        data = elementsToProcess[key];
        if (data.iconsToAdd.length > 0) {
          promises.push(self.addMarker({element: data.element, icons: data.iconsToAdd}).then(function (marker) {
            markers.push(marker);
          }));
        }
        if (data.iconsToRemove.length > 0) {
          promises.push(self.removeMarker({element: data.element, icons: data.iconsToRemove}));
        }
      }
    }
    return Promise.all(promises);
  }).then(function () {
    self.createOverlayIconData(elements, overlay);
    return markers;
  });
};

/**
 *
 * @param {AbstractDbOverlay} overlay
 * @returns {IconData}
 */
MarkerSurfaceCollection.prototype.getOverlayIconData = function (overlay) {
  var iconData = this._overlayIcons[overlay.getName()];
  if (iconData === undefined) {
    this._overlayIcons[overlay.getName()] = {};
    iconData = this._overlayIcons[overlay.getName()]
  }
  return iconData;
};

/**
 *
 * @param {IdentifiedElement[]} elements
 * @param {AbstractDbOverlay} overlay
 */
MarkerSurfaceCollection.prototype.createOverlayIconData = function (elements, overlay) {
  var iconData = {};
  for (var i = 0; i < elements.length; i++) {
    var element = elements[i];
    if (iconData[element.toString()] === undefined) {
      iconData[element.toString()] = {icons: [], element: element};
    }
    iconData[element.toString()].icons.push(element.getIcon());
  }
  this._overlayIcons[overlay.getName()] = iconData;
};

/**
 *
 * @param {IdentifiedElement[]} elements
 * @param {AbstractDbOverlay} overlay
 * @returns {PromiseLike<AbstractSurfaceElement>}
 */
MarkerSurfaceCollection.prototype.renderOverlaySurfaces = function (elements, overlay) {
  var self = this;
  var surfaces = [];
  return self.getMap().getModel().getByIdentifiedElements(elements).then(function () {
    return Promise.each(elements, function (element) {
      return self.createSurfaceForDbOverlay(element, overlay).then(function (mapOverlay) {
        surfaces.push(mapOverlay);
      });
    });
  }).then(function () {
    self.removeUnmodifiedSurfaces(surfaces, overlay);
    return surfaces;
  });

};

module.exports = MarkerSurfaceCollection;
