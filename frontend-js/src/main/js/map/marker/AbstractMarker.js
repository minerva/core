"use strict";

var GuiConnector = require('../../GuiConnector');

var logger = require('../../logger');
var Promise = require("bluebird");

var ObjectWithListeners = require('../../ObjectWithListeners');

/**
 * Class representing Marker (called sometimes bubble) that we visualize on the
 * map.
 *
 * @param {IdentifiedElement} params.element
 * @param {AbstractCustomMap} params.map
 * @param {function|function[]} [params.onClick]
 * @constructor
 */
function AbstractMarker(params) {
  var self = this;
  // call super constructor
  ObjectWithListeners.call(this);

  self._icons = [];
  if (params.element.getIcon()) {
    self._icons.push(params.element.getIcon());
  }
  self.setIdentifiedElement(params.element);
  self.setCustomMap(params.map);

  self.registerListenerType("onClick");
  if (params.onClick !== undefined) {
    if (typeof params.onClick !== "function") {
      for (var i = 0; i < params.onClick.length; i++) {
        self.addListener("onClick", params.onClick[i]);
      }
    } else {
      self.addListener("onClick", params.onClick);
    }
  }

}

AbstractMarker.prototype = Object.create(ObjectWithListeners.prototype);
AbstractMarker.prototype.constructor = AbstractMarker;

/**
 * Returns identifier of this object.
 *
 * @returns {number|string} identifier of this object
 */
AbstractMarker.prototype.getId = function () {
  return this.getIdentifiedElement().getId();
};

/**
 * Returns icon of the marker.
 *
 * @returns {string|undefined} identifying icon of the marker
 */
AbstractMarker.prototype.getIcon = function () {
  var icons = this._icons;
  if (icons.length === 0) {
    return undefined;
  } else if (icons.length === 1) {
    return icons[0];
  } else {
    var result = icons[0];
    //if we have many identical icons then return this specific icon
    for (var i = 1; i < icons.length; i++) {
      if (icons[i] !== result) {
        return "marker/multi.png";

      }
    }
    return result;
  }
};

/**
 *
 * @returns {Promise}
 * @private
 */
AbstractMarker.prototype._updateIcon = function () {
  var self = this;
  var marker = self.getMarker();
  if (marker !== undefined) {
    if (self.getIcon() === undefined) {
      return self.hide();
    } else {
      marker.setIcon(GuiConnector.getImgPrefix() + self.getIcon());
      if (!marker.isShown()) {
        return self.show();
      }
    }
  }
  return Promise.resolve();
};

/**
 *
 * @param {string|null|undefined} icon
 * @returns {Promise}
 */
AbstractMarker.prototype.setIcon = function (icon) {
  var self = this;
  if (icon === null || icon === undefined) {
    self._icons = [];
  } else {
    self._icons = [icon];
  }
  return self._updateIcon();
};

/**
 *
 * @param {string[]} icons
 * @returns {Promise}
 */
AbstractMarker.prototype.setIcons = function (icons) {
  var self = this;
  self._icons = icons;
  return self._updateIcon();
};

/**
 *
 * @param {string} icon
 * @returns {Promise}
 */
AbstractMarker.prototype.addIcon = function (icon) {
  var self = this;
  self._icons.push(icon);
  return self._updateIcon();
};

/**
 *
 * @param {string[]} icons
 * @returns {Promise}
 */
AbstractMarker.prototype.addIcons = function (icons) {
  var self = this;
  for (var i = 0; i < icons.length; i++) {
    self._icons.push(icons[i]);
  }
  return self._updateIcon();
};

/**
 *
 * @param {string[]} icons
 * @returns {Promise}
 */
AbstractMarker.prototype.removeIcons = function (icons) {
  var self = this;
  var promises = [];
  for (var i = 0; i < icons.length; i++) {
    promises.push(self.removeIcon(icons[i], true));
  }
  return Promise.all(promises).then(function () {
    return self._updateIcon();
  });
};

/**
 *
 * @param {string} icon
 * @param {boolean} [preventMarkerUpdate]
 * @returns {Promise}
 */
AbstractMarker.prototype.removeIcon = function (icon, preventMarkerUpdate) {
  var self = this;
  var index = self._icons.indexOf(icon);

  if (index > -1) {
    self._icons.splice(index, 1);
    if (!preventMarkerUpdate) {
      return self._updateIcon();
    }
  } else {
    logger.warn("Unknown icon for marker: " + icon);
  }
  return Promise.resolve();
};

/**
 * Shows marker on the map.
 *
 * @returns {Promise}
 */
AbstractMarker.prototype.show = function () {
  var self = this;
  if (this.getMarker() === undefined) {
    return this.init().then(function () {
      return self.getMarker().show();
    });
  } else {
    return this.getMarker().show();
  }
};

/**
 *
 * @returns {Promise}
 */
AbstractMarker.prototype.hide = function () {
  var self = this;
  if (this.getMarker() === undefined) {
    return this.init().then(function () {
      return self.getMarker().hide();
    });
  } else {
    return Promise.resolve(this.getMarker().hide());
  }
};

/**
 *
 * @returns {boolean}
 */
AbstractMarker.prototype.isShown = function () {
  if (this.getMarker() === undefined) {
    return false;
  }
  return this.getMarker().isShown();
};

/**
 * Returns {@link AbstractCustomMap} where marker is located.
 *
 * @returns {AbstractCustomMap} where marker is located
 */
AbstractMarker.prototype.getCustomMap = function () {
  return this._map;
};

/**
 *
 * @param {AbstractCustomMap} map
 */
AbstractMarker.prototype.setCustomMap = function (map) {
  this._map = map;
};

/**
 * Returns {Bounds} of the marker (it's a single point).
 *
 * @returns {Bounds|null} bounds of the marker (it's a single point)
 */
AbstractMarker.prototype.getBounds = function () {
  var marker = this.getMarker();
  if (marker === undefined || marker === null) {
    logger.warn("Marker not initialized");
    return null;
  }

  return marker.getBounds();
};

/**
 * Initializes {@link Marker} object connected to this object.
 *
 * @returns {Promise}
 * @protected
 */
AbstractMarker.prototype._init = function () {
  var self = this;
  var point = this.getCoordinates();

  this._marker = this.getCustomMap().getMapCanvas().createMarker({
    position: point,
    icon: GuiConnector.getImgPrefix() + self.getIcon(),
    id: self.getId()
  });

  this._marker.addListener('click', function () {
    return self.onClickHandler().then(null, GuiConnector.alert);
  });
  return Promise.resolve();
};

/**
 *
 * @returns {boolean}
 */
AbstractMarker.prototype.isInitialized = function () {
  return this._marker !== undefined;
};

/**
 *
 * @returns {Promise|PromiseLike}
 */
AbstractMarker.prototype.onClickHandler = function () {
  return this.callListeners("onClick");
};

/**
 * Returns {Marker} connected to this object.
 *
 * @returns {Marker} connected to this object
 */
AbstractMarker.prototype.getMarker = function () {
  return this._marker;
};

/**
 * Abstract function returning string marker type.
 *
 * @returns {string} marker type
 */
AbstractMarker.prototype.getType = function () {
  return this.getIdentifiedElement().getType();
};

/**
 *
 * @returns {number}
 */
AbstractMarker.prototype.getModelId = function () {
  return this.getIdentifiedElement().getModelId();
};

/**
 *
 * @returns {IdentifiedElement}
 */
AbstractMarker.prototype.getIdentifiedElement = function () {
  return this._identifiedElement;
};

/**
 *
 * @param {IdentifiedElement} identifiedElement
 */
AbstractMarker.prototype.setIdentifiedElement = function (identifiedElement) {
  this._identifiedElement = identifiedElement;
};

module.exports = AbstractMarker;
