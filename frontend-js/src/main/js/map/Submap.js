"use strict";

var $ = require('jquery');

/* exported logger */

// noinspection JSUnusedLocalSymbols
var logger = require('../logger');
var AbstractCustomMap = require('./AbstractCustomMap');
var CustomMapOptions = require('./CustomMapOptions');

/**
 * Constructor of a submap. Submaps are created on application start. But dialog
 * (popup window) is initialized on demand using init function.
 *
 * @param {CustomMap} customMap
 *          parent CustomMap
 * @param {MapModel} model
 *
 * @constructor
 * @extends AbstractCustomMap
 */
function Submap(customMap, model) {
  var self = this;
  this.setCustomMap(customMap);

  AbstractCustomMap.call(self, model, new CustomMapOptions({
    element: customMap.getElement(),
    bigLogo: customMap.isBigLogo(),
    project: customMap.getProject(),
    configuration: customMap.getConfiguration(),
    projectId: customMap.getProject().getProjectId(),
    debug: customMap.isDebug(),
    serverConnector: customMap.getServerConnector()
  }));

  self.registerListenerType("onOpen");
  self.registerListenerType("onClose");
  self.registerListenerType("onDrag");
  self.registerListenerType("onDragStart");
  self.registerListenerType("onDragStop");
  self.registerListenerType("onResize");
  self.registerListenerType("onResizeStart");
  self.registerListenerType("onResizeStop");
  self.registerListenerType("onFocus");

  self.initialized = false;
}

// implementation of object inheritance
Submap.prototype = Object.create(AbstractCustomMap.prototype);
Submap.prototype.constructor = Submap;

/**
 * This method initializes submap with gui component. Before this point submap
 * is created and contains data, but cannot be visualized in the browser.
 *
 * @param {HTMLElement} htmlTag
 *          html div tag where map should be placed
 */
Submap.prototype.open = function (htmlTag) {
  var self = this;

  if (!this.isInitialized()) {
    self.setElement(htmlTag);

    var contentDiv = document.createElement("div");
    contentDiv.setAttribute("name", "submap-div-" + self.getId());
    contentDiv.style.width = "100%";
    contentDiv.style.height = "100%";
    htmlTag.appendChild(contentDiv);

    var mapDiv = document.createElement("div");
    mapDiv.style.width = "100%";
    mapDiv.style.height = "100%";
    contentDiv.appendChild(mapDiv);

    // noinspection JSUnusedGlobalSymbols
    $(self.getElement()).dialog({
      title: self.getModel().getName(),
      width: Math.floor(window.innerWidth * 2 / 3),
      height: Math.floor(window.innerHeight * 2 / 3),
      position: {
        my: "center",
        at: "center",
        of: $(self.getTopMap().getElement())
      },
      resize: function () {
        return self.getMapCanvas().triggerListeners('resize').then(function () {
          return self.callListeners('onResize');
        });
      },
      resizeStart: function () {
        return self.callListeners('onResizeStart');
      },
      resizeStop: function () {
        return self.callListeners('onResizeStop');
      },
      open: function () {
        return self.callListeners('onOpen');
      },
      close: function () {
        return self.callListeners('onClose');
      },
      focus: function () {
        return self.callListeners('onFocus');
      },
      dragStop: function () {
        return self.callListeners('onDragStop');
      },
      dragStart: function () {
        return self.callListeners('onDragStart');
      },
      drag: function () {
        return self.callListeners('onDrag');
      }
    });

    $(self.getElement()).dialog("open");

    self.createMapCanvas(mapDiv);

    self._createMapChangedCallbacks();

    self.getMapCanvas().triggerListeners('resize');
    self.getMapCanvas().setBackgroundId(self.getTopMap().getBackgroundDataOverlay().getId());

    self.initialized = true;
  } else {
    $(self.getElement()).dialog("open");
  }
};

Submap.prototype.close = function () {
  var self = this;
  if (self.isInitialized()) {
    $(self.getElement()).dialog("close");
  }
};


/**
 *
 * @param {number} identifier
 */
Submap.prototype.openDataOverlay = function (identifier) {
  if (this.isInitialized()) {
    this.getMapCanvas().setBackgroundId(identifier.toString());
  }
};

/**
 *
 * @returns {CustomMap}
 */
Submap.prototype.getTopMap = function () {
  return this.getCustomMap();
};

/**
 *
 * @returns {CustomMap}
 */
Submap.prototype.getCustomMap = function () {
  return this._customMap;
};

/**
 *
 * @param {CustomMap} customMap
 */
Submap.prototype.setCustomMap = function (customMap) {
  this._customMap = customMap;
};

/**
 *
 * @returns {Project}
 */
Submap.prototype.getProject = function () {
  return this.getCustomMap().getProject();
};

/**
 *
 */
Submap.prototype.destroy = function () {
  var self = this;
  AbstractCustomMap.prototype.destroy.call(self);
  if (self.isInitialized()) {
    $(self.getElement()).dialog("destroy");
  }
};

module.exports = Submap;
