"use strict";

/**
 *
 * @param javaObject
 * @constructor
 */
function BioEntityField(javaObject) {
  this.setName(javaObject.name);
  this.setCommonName(javaObject.commonName);
}

BioEntityField.prototype = Object.create(Object.prototype);
BioEntityField.prototype.constructor = BioEntityField;

/**
 *
 * @param {string} name
 */
BioEntityField.prototype.setName = function (name) {
  this._name = name;
};

/**
 *
 * @returns {string}
 */
BioEntityField.prototype.getName = function () {
  return this._name;
};

/**
 *
 * @param {string} commonName
 */
BioEntityField.prototype.setCommonName = function (commonName) {
  this._commonName = commonName;
};

/**
 *
 * @returns {string}
 */
BioEntityField.prototype.getCommonName = function () {
  return this._commonName;
};

module.exports = BioEntityField;
