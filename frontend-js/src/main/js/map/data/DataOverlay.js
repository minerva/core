"use strict";

var Promise = require("bluebird");

var IdentifiedElement = require('./IdentifiedElement');
var LayoutAlias = require('./LayoutAlias');
var LayoutReaction = require('./LayoutReaction');
var logger = require('../../logger');

/**
 * Class representing data in a specific overlay.
 *
 * @param {number|Object} overlayId
 * @param {string} [name]
 * @constructor
 */
function DataOverlay(overlayId, name) {
  this.setInitialized(false);

  if (name === undefined) {
    // from json structure
    var object = overlayId;
    this.setId(object.idObject);
    this.setOrder(object.order);
    this.setName(object.name);
    this.setDescription(object.description);
    this.setCreator(object.creator);
    this.setContent(object.content);
    this.setFilename(object.filename);
    this.setPublicOverlay(object.publicOverlay);
    this.setType(object.type);
    if (this.getType() === LayoutAlias.GENETIC_VARIANT) {
      this.setGenomeVersion(object.genomeVersion);
      this.setGenomeType(object.genomeType);
    }
  } else {
    // default two param call
    this.setId(overlayId);
    this.setName(name);
  }
  this.aliases = [];

  this.aliasesById = [];
  this.reactions = [];
}

/**
 * Adds alias to the {@link DataOverlay}
 *
 * @param {LayoutAlias} overlayAlias
 *          information about alias in the overlay
 */
DataOverlay.prototype.addAlias = function (overlayAlias) {
  this.aliases.push(overlayAlias);
  if (this.aliasesById[overlayAlias.getId()] === undefined) {
    this.aliasesById[overlayAlias.getId()] = [];
  }
  this.aliasesById[overlayAlias.getId()].push(overlayAlias);
};

/**
 * Adds reaction to the {@link DataOverlay}
 *
 * @param {LayoutReaction} overlayReaction
 *          information about reaction in the overlay
 */
DataOverlay.prototype.addReaction = function (overlayReaction) {
  this.reactions.push(overlayReaction);
};

/**
 *
 * @returns {number}
 */
DataOverlay.prototype.getId = function () {
  return this.id;
};

/**
 *
 * @param {number|string} id
 */
DataOverlay.prototype.setId = function (id) {
  this.id = parseInt(id);
};

/**
 *
 * @returns {string}
 */
DataOverlay.prototype.getDescription = function () {
  return this._description;
};

/**
 *
 * @param {string} description
 */
DataOverlay.prototype.setDescription = function (description) {
  this._description = description;
};

/**
 *
 * @returns {string}
 */
DataOverlay.prototype.getCreator = function () {
  return this._creator;
};

/**
 *
 * @param {string} creator
 */
DataOverlay.prototype.setCreator = function (creator) {
  this._creator = creator;
};

/**
 *
 * @returns {string}
 */
DataOverlay.prototype.getName = function () {
  return this.name;
};

/**
 *
 * @param {string} name
 */
DataOverlay.prototype.setName = function (name) {
  this.name = name;
};

/**
 *
 * @param {LayoutAlias} overlayAlias
 */
DataOverlay.prototype.updateAlias = function (overlayAlias) {
  if (this.aliasesById[overlayAlias.getId()] === undefined) {
    logger.warn("Cannot update alias, it doesn't exist. Alias: " + overlayAlias.getId());
  } else {
    for (var i = 0; i < this.aliasesById[overlayAlias.getId()].length; i++) {
      var oldAlias = this.aliasesById[overlayAlias.getId()][i];
      if (oldAlias.getDataOverlayEntryId() === overlayAlias.getDataOverlayEntryId()) {
        oldAlias.update(overlayAlias);
      }
    }
  }
};

/**
 *
 * @param {number} id
 * @returns {[LayoutAlias]}
 */
DataOverlay.prototype.getAliasesById = function (id) {
  if (this.aliasesById[id] === undefined) {
    this.aliasesById[id] = [];
  }
  return this.aliasesById[id];
};

/**
 *
 * @param {number} id
 * @returns {Promise<Array<LayoutAlias>>}
 */
DataOverlay.prototype.getFullAliasesById = function (id) {
  var self = this;
  var aliases = self.getAliasesById(id);
  if (aliases !== undefined && aliases.length > 0) {
    if (aliases[0].getType() === LayoutAlias.LIGHT) {
      return ServerConnector.getFullOverlayElement({
        element: new IdentifiedElement(aliases[0]),
        overlay: self
      }).then(function (data) {
        var geneVariants = [];
        for (var j = 0; j < data.length; j++) {
          if (data[j] instanceof LayoutAlias && data[j].getGeneVariants() !== undefined) {
            geneVariants = geneVariants.concat(data[j].getGeneVariants());
          }
        }
        for (var i = 0; i < data.length; i++) {
          self.updateAlias(data[i]);
        }
        //this is a hacky solution - we cannot distinguish many LayoutAliases per single alias (no unique id) therefore
        // we store the data only in the first
        if (aliases[0] instanceof LayoutAlias) {
          aliases[0].setGeneVariants(geneVariants);
        }
        return aliases;
      });
    }
  }
  return Promise.resolve(aliases);
};

/**
 *
 * @param {boolean} value
 */
DataOverlay.prototype.setInitialized = function (value) {
  this._initialized = value;
};

/**
 *
 * @returns {boolean}
 */
DataOverlay.prototype.isInitialized = function () {
  return this._initialized;
};

/**
 *
 * @returns {LayoutAlias[]}
 */
DataOverlay.prototype.getAliases = function () {
  return this.aliases;
};

/**
 *
 * @returns {LayoutReaction[]}
 */
DataOverlay.prototype.getReactions = function () {
  return this.reactions;
};

/**
 *
 * @returns {Promise}
 */
DataOverlay.prototype.init = function () {
  var self = this;
  if (this.isInitialized()) {
    return Promise.resolve();
  }
  return ServerConnector.getOverlayElements(self.getId()).then(function (data) {
    for (var i = 0; i < data.length; i++) {
      if (data[i] instanceof LayoutAlias) {
        self.addAlias(data[i]);
      } else if (data[i] instanceof LayoutReaction) {
        self.addReaction(data[i]);
      } else {
        return Promise.reject("Unknown element type: " + typeof (data[i]));
      }

    }
    self.setInitialized(true);
  });

};

/**
 *
 * @returns {boolean}
 */
DataOverlay.prototype.getPublicOverlay = function () {
  return this._publicOverlay;
};

/**
 *
 * @param {boolean} publicOverlay
 */
DataOverlay.prototype.setPublicOverlay = function (publicOverlay) {
  this._publicOverlay = publicOverlay;
};

/**
 *
 * @returns {string}
 */
DataOverlay.prototype.getContent = function () {
  return this._content;
};

/**
 *
 * @param {string} content
 */
DataOverlay.prototype.setContent = function (content) {
  this._content = content;
};

/**
 *
 * @returns {number}
 */
DataOverlay.prototype.getOrder = function () {
  return this._order;
};

/**
 *
 * @param {number} order
 */
DataOverlay.prototype.setOrder = function (order) {
  this._order = order;
};

/**
 *
 * @returns {string}
 */
DataOverlay.prototype.getFilename = function () {
  return this._filename;
};

/**
 *
 * @param {string} filename
 */
DataOverlay.prototype.setFilename = function (filename) {
  this._filename = filename;
};

/**
 *
 * @returns {string}
 */
DataOverlay.prototype.getType = function () {
  return this._type;
};

/**
 *
 * @param {string} type
 */
DataOverlay.prototype.setType = function (type) {
  this._type = type;
};

/**
 *
 * @returns {string}
 */
DataOverlay.prototype.getGenomeType = function () {
  return this._genomeType;
};

/**
 *
 * @param {string} type
 */
DataOverlay.prototype.setGenomeType = function (type) {
  this._genomeType = type;
};

/**
 *
 * @returns {string}
 */
DataOverlay.prototype.getGenomeVersion = function () {
  return this._genomeVersion;
};

/**
 *
 * @param {string} version
 */
DataOverlay.prototype.setGenomeVersion = function (version) {
  this._genomeVersion = version;
};

/**
 *
 * @param {DataOverlay} overlay
 */
DataOverlay.prototype.update = function (overlay) {
  this.setPublicOverlay(overlay.getPublicOverlay());
  this.setOrder(overlay.getOrder());
  this.setCreator(overlay.getCreator());
  this.setName(overlay.getName());
  this.setDescription(overlay.getDescription());
};


module.exports = DataOverlay;
