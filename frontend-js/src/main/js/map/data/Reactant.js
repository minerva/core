"use strict";

var Alias = require("./Alias");


/**
 *
 * @param {ModifierOptions} javaObject
 * @constructor
 */
function Reactant(javaObject) {
  this.setAlias(javaObject.aliasId);
  this.setStoichiometry(javaObject.stoichiometry);
}

/**
 *
 * @returns {number}
 */
Reactant.prototype.getAlias = function () {
  return this._alias;
};

/**
 *
 * @param {number} alias
 */
Reactant.prototype.setAlias = function (alias) {
  this._alias = alias;
};

/**
 * TODO check if this method is used
 * @returns {boolean}
 */
Reactant.prototype.isComplete = function () {
  return this.getAlias() instanceof Alias;
};

/**
 *
 * @returns {string}
 */
Reactant.prototype.getStoichiometry = function () {
  return this._stoichiometry;
};

/**
 *
 * @param {string} stoichiometry
 */
Reactant.prototype.setStoichiometry = function (stoichiometry) {
  this._stoichiometry = stoichiometry;
};

module.exports = Reactant;
