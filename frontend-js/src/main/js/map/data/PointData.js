"use strict";

var Point = require('../canvas/Point');

/**
 * Object representing information for specific point on the map.
 */
function PointData(javaObject, modelId) {
  if (javaObject instanceof Point) {
    this._point = javaObject;
    this._modelId = modelId;
  } else if (javaObject.constructor.name === "IdentifiedElement") {
    this._point = javaObject.getPoint();
    this._modelId = javaObject.getModelId();
  } else {
    var tmp = javaObject.idObject.replace("Point2D.Double", "");
    tmp = JSON.parse(tmp);
    var x = parseFloat(tmp[0]).toFixed(2);
    var y = parseFloat(tmp[1]).toFixed(2);
    this._point = new Point(x, y);
    this._modelId = modelId;
  }
  this._id = "(" + parseFloat(this._point.x).toFixed(2) + "," + parseFloat(this._point.y).toFixed(2) + ")";
}

/**
 * Returns identifier of the object.
 * 
 * @returns {string} that is identifier of the object
 */
PointData.prototype.getId = function() {
  return this._id;
};

/**
 * Returns point to which this object corresponds.
 * 
 * @returns {Point} to which this object corresponds
 */
PointData.prototype.getPoint = function() {
  return this._point;
};

/**
 *
 * @returns {number}
 */
PointData.prototype.getModelId = function() {
  return this._modelId;
};

/**
 *
 * @returns {boolean}
 */
PointData.prototype.isComplete = function() {
  return true;
};

module.exports = PointData;
