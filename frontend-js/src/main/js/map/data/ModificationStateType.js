"use strict";

var ObjectWithListeners = require('../../ObjectWithListeners');

/**
 * @typedef {Object} ModificationStateTypeOptions
 * @property {string} abbreviation
 * @property {string} commonName
 */

/**
 *
 * @param {ModificationStateTypeOptions} data
 * @param {string} name
 * @constructor
 * @extends ObjectWithListeners
 */
function ModificationStateType(data, name) {
  // call super constructor
  ObjectWithListeners.call(this);

  var self = this;
  self.setAbbreviation(data.abbreviation);
  self.setName(name);
  self.setCommonName(data.commonName);
}

ModificationStateType.prototype = Object.create(ObjectWithListeners.prototype);
ModificationStateType.prototype.constructor = ModificationStateType;

/**
 *
 * @param {string} name
 */
ModificationStateType.prototype.setName = function (name) {
  this._name = name;
};

/**
 *
 * @returns {string}
 */
ModificationStateType.prototype.getName = function () {
  return this._name;
};

/**
 *
 * @param {string} abbreviation
 */
ModificationStateType.prototype.setAbbreviation = function (abbreviation) {
  this._abbreviation = abbreviation;
};

/**
 *
 * @returns {string}
 */
ModificationStateType.prototype.getAbbreviation = function () {
  return this._abbreviation;
};

/**
 *
 * @param {string} commonName
 */
ModificationStateType.prototype.setCommonName = function (commonName) {
  this._commonName = commonName;
};

/**
 *
 * @returns {string}
 */
ModificationStateType.prototype.getCommonName = function () {
  return this._commonName;
};

module.exports = ModificationStateType;
