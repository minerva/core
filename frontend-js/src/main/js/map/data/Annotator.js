"use strict";

/* exported logger */

var AnnotatorParameter = require("./AnnotatorParameter");

var logger = require('../../logger');

/**
 * @typedef {Object} AnnotatorOptions
 * @property {string} className
 * @property {string[]} elementClassNames
 * @property {string} name
 * @property {string} description
 * @property {AnnotatorParameterDefinition[]} parameters
 * @property {string} url
 */

/**
 * @typedef {Object} AnnotatorDataOptions
 * @property {string} annotatorClass
 * @property {number} order
 * @property {AnnotatorParameterDefinition[]} parameters
 */

/**
 *
 * @param {Annotator|AnnotatorOptions|AnnotatorDataOptions} javaObject
 * @param {Configuration} [configuration]
 * @constructor
 */
function Annotator(javaObject, configuration) {
  var self = this;
  if (javaObject instanceof Annotator) {
    self.setClassName(javaObject.getClassName());
    this._elementTypes = javaObject.getElementTypes();
    self.setName(javaObject.getName());
    self.setDescription(javaObject.getDescription());
    self.setParametersDefinitions(javaObject.getParametersDefinitions());
    self.setUrl(javaObject.getUrl());
  } else {
    if (javaObject.className === undefined) {
      self.setClassName(javaObject.annotatorClass);
    } else {
      self.setClassName(javaObject.className);
    }
    self.setElementTypes(javaObject.elementClassNames, configuration);
    self.setName(javaObject.name);
    self.setDescription(javaObject.description);
    self.setParametersDefinitions(javaObject.parameters);
    self.setUrl(javaObject.url);
  }
}

/**
 *
 * @param {string} className
 */
Annotator.prototype.setClassName = function (className) {
  this._className = className;
};

/**
 *
 * @returns {string}
 */
Annotator.prototype.getClassName = function () {
  return this._className;
};

/**
 *
 * @param {string} name
 */
Annotator.prototype.setName = function (name) {
  this._name = name;
};

/**
 *
 * @returns {string}
 */
Annotator.prototype.getName = function () {
  return this._name;
};

/**
 *
 * @param {string} description
 */
Annotator.prototype.setDescription = function (description) {
  this._description = description;
};

/**
 *
 * @returns {string}
 */
Annotator.prototype.getDescription = function () {
  return this._description;
};

/**
 *
 * @param {string} url
 */
Annotator.prototype.setUrl = function (url) {
  this._url = url;
};

/**
 *
 * @returns {string}
 */
Annotator.prototype.getUrl = function () {
  return this._url;
};

/**
 *
 * @returns {AnnotatorParameter[]}
 */
Annotator.prototype.getParametersDefinitions = function () {
  return this._parametersDefinitions;
};

/**
 *
 * @param {AnnotatorParameter} annotatorParameter
 */
Annotator.prototype.removeParameter = function (annotatorParameter) {
  var index = -1;
  for (var i = 0; i < this._parametersDefinitions.length; i++) {
    var existing = this._parametersDefinitions[i];
    if (existing.getName() === annotatorParameter.getName() &&
      existing.getType() === annotatorParameter.getType() &&
      existing.getInputType() === annotatorParameter.getInputType() &&
      existing.getAnnotationType() === annotatorParameter.getAnnotationType() &&
      existing.getField() === annotatorParameter.getField()) {
      index = i;
    }
  }
  if (index < 0) {
    logger.warn("Parameter doesn't exist in the annotator");
  } else {
    this._parametersDefinitions.splice(index, 1);
  }
};

/**
 *
 * @param {AnnotatorParameter} annotatorParameter
 * @returns {string|number|boolean|null}
 */
Annotator.prototype.getParameterValue = function (annotatorParameter) {
  for (var i = 0; i < this._parametersDefinitions.length; i++) {
    var existing = this._parametersDefinitions[i];
    if (existing.getName() === annotatorParameter.getName() &&
      existing.getType() === annotatorParameter.getType() &&
      existing.getInputType() === annotatorParameter.getInputType() &&
      existing.getAnnotationType() === annotatorParameter.getAnnotationType() &&
      existing.getField() === annotatorParameter.getField()) {
      if (existing.getType() === "CONFIG") {
        return existing.getValue();
      } else {
        return true;
      }
    }
  }
  return null;
};

/**
 *
 * @param {AnnotatorParameter} annotatorParameter
 * @param {string|boolean|number} value
 */
Annotator.prototype.setParameterValue = function (annotatorParameter, value) {
  for (var i = 0; i < this._parametersDefinitions.length; i++) {
    var existing = this._parametersDefinitions[i];
    if (existing.getName() === annotatorParameter.getName() &&
      existing.getType() === annotatorParameter.getType() &&
      existing.getInputType() === annotatorParameter.getInputType() &&
      existing.getAnnotationType() === annotatorParameter.getAnnotationType() &&
      existing.getField() === annotatorParameter.getField()) {
      if (existing.getType() === "CONFIG") {
        existing.setValue(value);
        return;
      }
    }
  }
  var param = new AnnotatorParameter(annotatorParameter);
  param.setValue(value);
  this.addParameter(param);
};

/**
 *
 * @param {AnnotatorParameter} annotatorParameter
 */
Annotator.prototype.addParameter = function (annotatorParameter) {
  var index = -1;
  for (var i = 0; i < this._parametersDefinitions.length; i++) {
    var existing = this._parametersDefinitions[i];
    if (existing.getName() === annotatorParameter.getName() &&
      existing.getType() === annotatorParameter.getType() &&
      existing.getInputType() === annotatorParameter.getInputType() &&
      existing.getAnnotationType() === annotatorParameter.getAnnotationType() &&
      existing.getField() === annotatorParameter.getField()) {
      index = i;
    }
  }
  if (index < 0) {
    this._parametersDefinitions.push(annotatorParameter);
  } else {
    logger.warn("Parameter already exists in the annotator");
  }
};

/**
 *
 * @param {AnnotatorParameterDefinition[]|AnnotatorParameter[]} parametersDefinitions
 */
Annotator.prototype.setParametersDefinitions = function (parametersDefinitions) {
  this._parametersDefinitions = [];
  for (var i = 0; i < parametersDefinitions.length; i++) {
    this._parametersDefinitions.push(new AnnotatorParameter(parametersDefinitions[i]));
  }
};

/**
 *
 * @param {string[]} elementTypesClassNames
 * @param {Configuration} configuration
 */
Annotator.prototype.setElementTypes = function (elementTypesClassNames, configuration) {
  this._elementTypes = [];
  if (elementTypesClassNames === undefined || elementTypesClassNames.length === 0) {
    return;
  }
  var typeByClassName = {};
  var types = configuration.getElementTypes();
  var i, type;
  for (i = 0; i < types.length; i++) {
    type = types[i];
    typeByClassName[type.className] = type;
  }
  types = configuration.getReactionTypes();
  for (i = 0; i < types.length; i++) {
    type = types[i];
    typeByClassName[type.className] = type;
  }

  for (i = 0; i < elementTypesClassNames.length; i++) {
    type = typeByClassName[elementTypesClassNames[i]];
    if (type !== undefined) {
      this._elementTypes.push(type)
    } else {
      throw new Error("Unknown elementType: " + elementTypesClassNames[i]);
    }
  }
};

/**
 *
 * @returns {BioEntityType[]}
 */
Annotator.prototype.getElementTypes = function () {
  return this._elementTypes;
};

/**
 *
 * @return {Object}
 */
Annotator.prototype.toExport = function () {
  var self = this;
  var parameters = [];
  for (var i = 0; i < this._parametersDefinitions.length; i++) {
    parameters.push(this._parametersDefinitions[i].toExport());
  }
  return {
    "annotatorClass": self.getClassName(),
    "parameters": parameters
  };
};


module.exports = Annotator;
