"use strict";

/**
 * @typedef {Object} MeshOptions
 * @property {string} name
 * @property {string[]} synonyms
 * @property {string} id
 * @property {string} description
 */


/**
 *
 * @param {MeshOptions} jsonObject
 * @constructor
 */
function Mesh(jsonObject) {
  var self = this;
  self.setSynonyms(jsonObject.synonyms);
  self.setName(jsonObject.name);
  self.setId(jsonObject.id);
  self.setDescription(jsonObject.description);
}

/**
 *
 * @param {string[]} synonyms
 */
Mesh.prototype.setSynonyms = function (synonyms) {
  this._synonyms = synonyms;
};

/**
 *
 * @returns {string[]}
 */
Mesh.prototype.getSynonyms = function () {
  return this._synonyms;
};

/**
 *
 * @param {string} name
 */
Mesh.prototype.setName = function (name) {
  this._name = name;
};

/**
 *
 * @returns {string}
 */
Mesh.prototype.getName = function () {
  return this._name;
};

/**
 *
 * @param {string} id
 */
Mesh.prototype.setId = function (id) {
  this._id = id;
};

/**
 *
 * @returns {string}
 */
Mesh.prototype.getId = function () {
  return this._id;
};

/**
 *
 * @param {string} description
 */
Mesh.prototype.setDescription = function (description) {
  this._description = description;
};

/**
 *
 * @returns {string}
 */
Mesh.prototype.getDescription = function () {
  return this._description;
};

module.exports = Mesh;
