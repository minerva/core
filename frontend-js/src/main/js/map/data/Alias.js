"use strict";

var BioEntity = require("./BioEntity");
var Point = require("../canvas/Point");

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

/**
 * Class representing alias data.
 *
 * @param {Alias|Object} javaObject
 *          object de-serialized ajax query to the server side
 */
function Alias(javaObject) {
  BioEntity.call(this, javaObject);
  if (javaObject.idObject !== undefined) {
    this.setId(javaObject.idObject);
  } else {
    this.setId(javaObject.id);
  }
  this.setModelId(javaObject.modelId);
  if (javaObject.bounds !== undefined) {
    this.setX(javaObject.bounds.x);
    this.setY(javaObject.bounds.y);
    this.setZ(javaObject.bounds.z);
    this.setWidth(javaObject.bounds.width);
    this.setHeight(javaObject.bounds.height);
  }
  this.setLinkedSubmodelId(javaObject.linkedSubmodel);

  if (this._modelId === undefined) {
    throw new Error("ModelId is not defined for alias" + javaObject);
  }

  if (javaObject.name === undefined) {
    this.setIsComplete(false);
  } else {
    this.update(javaObject);
  }
}

Alias.prototype = Object.create(BioEntity.prototype);
Alias.prototype.constructor = Alias;

/**
 * Updates alias with full data information. This function should be called when
 * full information about alias is retrieved from server.
 *
 * @param {Alias|Object} javaObject
 *          object representing data from server side
 */
Alias.prototype.update = function (javaObject) {
  if (javaObject instanceof Alias) {
    if (javaObject.getName() === undefined) {
      return;
    }
    this.setDescription(javaObject.getDescription());
    this.setImmediateLink(javaObject.getImmediateLink());
    this.setElementId(javaObject.getElementId());
    this.setType(javaObject.getType());
    this.setCharge(javaObject.getCharge());
    this.setSymbol(javaObject.getSymbol());
    this.setFullName(javaObject.getFullName());
    this.setAbbreviation(javaObject.getAbbreviation());
    this.setFormula(javaObject.getFormula());
    this.setName(javaObject.getName());
    this.setSynonyms(javaObject.getSynonyms());
    this.setFormerSymbols(javaObject.getFormerSymbols());
    this.setReferences(javaObject.getReferences());
    this.setOther(javaObject.getOther());
    this.setHierarchyVisibilityLevel(javaObject.getHierarchyVisibilityLevel());
    this.setComplexId(javaObject.getComplexId());
    this.setTransparencyLevel(javaObject.getTransparencyLevel());
    this.setLinkedSubmodelId(javaObject.getLinkedSubmodelId());
    this.setCompartmentId(javaObject.getCompartmentId());
    this.setDescription(javaObject.getDescription());

    this.setConstant(javaObject.getConstant());
    this.setBoundaryCondition(javaObject.getBoundaryCondition());
    this.setInitialAmount(javaObject.getInitialAmount());
    this.setInitialConcentration(javaObject.getInitialConcentration());

    this.setGlyph(javaObject.getGlyph());

    this.setX(javaObject.getX());
    this.setY(javaObject.getY());
    this.setZ(javaObject.getZ());
    this.setWidth(javaObject.getWidth());
    this.setHeight(javaObject.getHeight());

    this.setIsComplete(true);
  } else {
    if (javaObject.name === undefined) {
      return;
    }
    this.setDescription(javaObject.notes);
    this.setImmediateLink(javaObject.immediateLink);
    this.setElementId(javaObject.elementId);
    this.setType(javaObject.type);
    this.setCharge(javaObject.charge);
    this.setSymbol(javaObject.symbol);
    this.setFullName(javaObject.fullName);
    this.setAbbreviation(javaObject.abbreviation);
    this.setFormula(javaObject.formula);
    this.setName(javaObject.name);
    this.setSynonyms(javaObject.synonyms);
    this.setFormerSymbols(javaObject.formerSymbols);
    this.setReferences(javaObject.references);
    this.setOther(javaObject.other);
    this.setHierarchyVisibilityLevel(javaObject.hierarchyVisibilityLevel);
    this.setComplexId(javaObject.complexId);
    this.setTransparencyLevel(javaObject.transparencyLevel);
    this.setLinkedSubmodelId(javaObject.linkedSubmodel);
    this.setCompartmentId(javaObject.compartmentId);

    this.setConstant(javaObject.constant);
    this.setBoundaryCondition(javaObject.boundaryCondition);
    this.setInitialAmount(javaObject.initialAmount);
    this.setInitialConcentration(javaObject.initialConcentration);

    this.setGlyph(javaObject.glyph);

    if (javaObject.bounds !== undefined) {
      this.setX(javaObject.bounds.x);
      this.setY(javaObject.bounds.y);
      this.setZ(javaObject.bounds.z);
      this.setWidth(javaObject.bounds.width);
      this.setHeight(javaObject.bounds.height);
    }
    this.setIsComplete(true);
  }
};

/**
 *
 * @returns {number}
 */
Alias.prototype.getCharge = function () {
  return this.charge;
};

/**
 *
 * @param {number} charge
 */
Alias.prototype.setCharge = function (charge) {
  this.charge = charge;
};

/**
 *
 * @returns {boolean}
 */
Alias.prototype.getConstant = function () {
  return this._constant;
};

/**
 *
 * @param {boolean} constant
 */
Alias.prototype.setConstant = function (constant) {
  this._constant = constant;
};

/**
 *
 * @returns {boolean}
 */
Alias.prototype.getBoundaryCondition = function () {
  return this._boundaryCondition;
};

/**
 *
 * @param {boolean} boundaryCondition
 */
Alias.prototype.setBoundaryCondition = function (boundaryCondition) {
  this._boundaryCondition = boundaryCondition;
};

/**
 *
 * @returns {number}
 */
Alias.prototype.getInitialAmount = function () {
  return this._initialAmount;
};

/**
 *
 * @param {number} initialAmount
 */
Alias.prototype.setInitialAmount = function (initialAmount) {
  this._initialAmount = initialAmount;
};

/**
 *
 * @returns {number}
 */
Alias.prototype.getInitialConcentration = function () {
  return this._initialConcentration;
};

/**
 *
 * @param {number} initialConcentration
 */
Alias.prototype.setInitialConcentration = function (initialConcentration) {
  this._initialConcentration = initialConcentration;
};

/**
 *
 * @returns {string[]}
 */
Alias.prototype.getFormerSymbols = function () {
  return this.formerSymbols;
};

/**
 *
 * @param {string[]} formerSymbols
 */
Alias.prototype.setFormerSymbols = function (formerSymbols) {
  this.formerSymbols = formerSymbols;
};

/**
 *
 * @returns {number}
 */
Alias.prototype.getX = function () {
  return this.x;
};

/**
 *
 * @param {number} x
 */
Alias.prototype.setX = function (x) {
  if (x !== undefined) {
    this.x = x;
  }
};

/**
 *
 * @param {number}  y
 */
Alias.prototype.setY = function (y) {
  if (y !== undefined) {
    this.y = y;
  }
};

/**
 *
 * @returns {number}
 */
Alias.prototype.getY = function () {
  return this.y;
};

/**
 *
 * @param {number}  z
 */
Alias.prototype.setZ = function (z) {
  if (z !== undefined) {
    this.z = z;
  }
};

/**
 *
 * @returns {number}
 */
Alias.prototype.getZ = function () {
  return this.z;
};

/**
 *
 * @param {number} elementId
 */
Alias.prototype.setElementId = function (elementId) {
  this._elementId = elementId;
};

/**
 *
 * @returns {number}
 */
Alias.prototype.getElementId = function () {
  return this._elementId;
};

/**
 *
 * @param {number} width
 */
Alias.prototype.setWidth = function (width) {
  if (width !== undefined) {
    this.width = width;
  }
};

/**
 *
 * @param {number} height
 */
Alias.prototype.setHeight = function (height) {
  if (height !== undefined) {
    this.height = height;
  }
};

/**
 *
 * @returns {number}
 */
Alias.prototype.getWidth = function () {
  return this.width;
};

/**
 *
 * @returns {number}
 */
Alias.prototype.getHeight = function () {
  return this.height;
};

/**
 *
 * @returns {string}
 */
Alias.prototype.getName = function () {
  return this.name;
};

/**
 *
 * @param {string} name
 */
Alias.prototype.setName = function (name) {
  this.name = name;
};

/**
 *
 * @returns {string}
 */
Alias.prototype.getFullName = function () {
  return this.fullName;
};

/**
 *
 * @param {string} fullName
 */
Alias.prototype.setFullName = function (fullName) {
  this.fullName = fullName;
};

/**
 *
 * @returns {number|undefined}
 */
Alias.prototype.getCompartmentId = function () {
  return this._compartmentId;
};

/**
 *
 * @param {number} compartmentId
 */
Alias.prototype.setCompartmentId = function (compartmentId) {
  if (compartmentId !== null) {
    this._compartmentId = compartmentId;
  }
};

/**
 *
 * @returns {number}
 */
Alias.prototype.getComplexId = function () {
  return this._complexId;
};

/**
 *
 * @param {number} complexId
 */
Alias.prototype.setComplexId = function (complexId) {
  if (complexId !== null) {
    this._complexId = complexId;
  }
};

/**
 *
 * @returns {number}
 */
Alias.prototype.getTransparencyLevel = function () {
  return this._transparencyLevel;
};

/**
 *
 * @param {number} transparencyLevel
 */
Alias.prototype.setTransparencyLevel = function (transparencyLevel) {
  this._transparencyLevel = transparencyLevel;
};

/**
 *
 * @returns {Point}
 */
Alias.prototype.getCenter = function () {
  return new Point(this.getX() + this.getWidth() / 2, this.getY() + this.getHeight() / 2);
};

/**
 *
 * @returns {Object}
 */
Alias.prototype.getGlyph = function () {
  return this._glyph;
};

/**
 *
 * @param {Object} glyph
 */
Alias.prototype.setGlyph = function (glyph) {
  this._glyph = glyph;
};

/**
 *
 * @param {string|null} immediateLink
 */
Alias.prototype.setImmediateLink = function (immediateLink) {
  this._immediateLink = immediateLink;
};

/**
 *
 * @returns {string|null}
 */
Alias.prototype.getImmediateLink = function () {
  return this._immediateLink;
};


module.exports = Alias;
