"use strict";

var Alias = require("./Alias");
var BioEntity = require("./BioEntity");
var Reaction = require("./Reaction");

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

/**
 * Class representing merged search bioEntities.
 *
 * @param {BioEntity} bioEntity
 *          initial bioEntity from which group is created
 * @constructor
 */
function SearchBioEntityGroup(bioEntity) {
  if (!(bioEntity instanceof BioEntity)) {
    throw new Error("Invalid argument");
  }
  this._bioEntites = [bioEntity];
}

/**
 *
 * @param {BioEntity} newBioEntity
 * @returns {boolean}
 */
SearchBioEntityGroup.prototype.bioEntityMatch = function (newBioEntity) {
  var result = true;
  var self = this;

  for (var i = 0; i < self._bioEntites.length; i++) {
    var bioEntity = self._bioEntites[i];
    if (self.bioEntityComparator(bioEntity, newBioEntity) !== 0) {
      result = false;
    }
  }
  return result;
};

/**
 *
 * @param {BioEntity} newBioEntity
 */
SearchBioEntityGroup.prototype.addBioEntity = function (newBioEntity) {
  this._bioEntites.push(newBioEntity);
};

/**
 *
 * @returns {BioEntity[]}
 */
SearchBioEntityGroup.prototype.getBioEntities = function () {
  return this._bioEntites;
};

/**
 *
 * @param {BioEntity} bioEntity1
 * @param {BioEntity} bioEntity2
 * @returns {number}
 */
SearchBioEntityGroup.prototype.bioEntityComparator = function (bioEntity1, bioEntity2) {
  if (bioEntity1 instanceof Alias && bioEntity2 instanceof Alias) {
    if (bioEntity1.getName() !== bioEntity2.getName()) {
      return -1;
    }
    if (bioEntity1.getModelId() !== bioEntity2.getModelId()) {
      return -2;
    }
    if (bioEntity1.getCompartmentId() !== bioEntity2.getCompartmentId()) {
      return -3;
    }
    if (bioEntity1.getType() !== bioEntity2.getType()) {
      return -4;
    }
    if (bioEntity1.getOther("structuralState") !== bioEntity2.getOther("structuralState")) {
      return -5;
    }

    var computeSerializedModifications = function (modifications) {
      if (modifications === undefined) {
        return [];
      }
      var result = [];
      for (var i = 0; i < modifications.length; i++) {
        var modification = modifications[i];
        result.push(modification.name + "_" + modification.state);
      }
      result.sort();
      return result;
    };
    var serializedModifications1 = computeSerializedModifications(bioEntity1.getOther("modifications"));
    var serializedModifications2 = computeSerializedModifications(bioEntity2.getOther("modifications"));
    if (serializedModifications1.length !== serializedModifications2.length) {
      return -6;
    }
    for (var i = 0; i < serializedModifications1.length; i++) {
      if (serializedModifications1[i] !== serializedModifications2[i]) {
        return -7;
      }
    }
    return 0;
  }
  if (bioEntity1 instanceof Reaction && bioEntity2 instanceof Reaction) {
    if (bioEntity1.getId() !== bioEntity2.getId()) {
      return -8;
    }
    return 0;
  }
  return -9;
};

/**
 *
 * @param {string} icon
 */
SearchBioEntityGroup.prototype.setIcon = function (icon) {
  this._icon = icon;
};

/**
 *
 * @returns {string}
 */
SearchBioEntityGroup.prototype.getIcon = function () {
  return this._icon;
};

//aggregated data
/**
 *
 * @returns {string}
 */
SearchBioEntityGroup.prototype.getType = function () {
  return this._bioEntites[0].getType();
};

/**
 *
 * @returns {string}
 */
SearchBioEntityGroup.prototype.getName = function () {
  return this._bioEntites[0].getName();
};

SearchBioEntityGroup.prototype.getElementId = function () {
  var result = [];
  for (var i = 0; i < this._bioEntites.length; i++) {
    result.push(this._bioEntites[i].getElementId());
  }
  return result.join(", ");
};

/**
 *
 * @returns {number}
 */
SearchBioEntityGroup.prototype.getModelId = function () {
  return this._bioEntites[0].getModelId();
};

/**
 *
 * @returns {number}
 */
SearchBioEntityGroup.prototype.getCompartmentId = function () {
  return this._bioEntites[0].getCompartmentId();
};

/**
 *
 * @returns {Reactant[]}
 */
SearchBioEntityGroup.prototype.getReactants = function () {
  return this._bioEntites[0].getReactants();
};

/**
 *
 * @returns {Product[]}
 */
SearchBioEntityGroup.prototype.getProducts = function () {
  return this._bioEntites[0].getProducts();
};

/**
 *
 * @returns {Modifier[]}
 */
SearchBioEntityGroup.prototype.getModifiers = function () {
  return this._bioEntites[0].getModifiers();
};

/**
 * @returns {string}
 */
SearchBioEntityGroup.prototype.getFullName = function () {
  return this.getMergedParameterByFunction("getFullName");
};

/**
 * @returns {string}
 */
SearchBioEntityGroup.prototype.getReactionId = function () {
  return this.getMergedParameterByFunction("getReactionId");
};

/**
 * @returns {number}
 */
SearchBioEntityGroup.prototype.getLinkedSubmodelId = function () {
  return this.getMergedParameterByFunction("getLinkedSubmodelId");
};

/**
 * @returns {string}
 */
SearchBioEntityGroup.prototype.getSymbol = function () {
  return this.getMergedParameterByFunction("getSymbol");
};

/**
 * @returns {string}
 */
SearchBioEntityGroup.prototype.getAbbreviation = function () {
  return this.getMergedParameterByFunction("getAbbreviation");
};

/**
 * @returns {string}
 */
SearchBioEntityGroup.prototype.getFormula = function () {
  return this.getMergedParameterByFunction("getFormula");
};

/**
 * @returns {string}
 */
SearchBioEntityGroup.prototype.getMechanicalConfidenceScore = function () {
  return this.getMergedParameterByFunction("getMechanicalConfidenceScore");
};

/**
 * @returns {string}
 */
SearchBioEntityGroup.prototype.getLowerBound = function () {
  return this.getMergedParameterByFunction("getLowerBound");
};

/**
 * @returns {string}
 */
SearchBioEntityGroup.prototype.getUpperBound = function () {
  return this.getMergedParameterByFunction("getUpperBound");
};

/**
 * @returns {string}
 */
SearchBioEntityGroup.prototype.getGeneProteinReaction = function () {
  return this.getMergedParameterByFunction("getGeneProteinReaction");
};

/**
 * @returns {string}
 */
SearchBioEntityGroup.prototype.getSubsystem = function () {
  return this.getMergedParameterByFunction("getSubsystem");
};

/**
 * @returns {string}
 */
SearchBioEntityGroup.prototype.getDescription = function () {
  return this.getMergedParameterByFunction("getDescription");
};

/**
 * @returns {number}
 */
SearchBioEntityGroup.prototype.getCharge = function () {
  return this.getMergedParameterByFunction("getCharge");
};

/**
 * @returns {string[]}
 */
SearchBioEntityGroup.prototype.getSynonyms = function () {
  return this.getIntersectionListByFunction("getSynonyms");
};

/**
 * @returns {string[]}
 */
SearchBioEntityGroup.prototype.getFormerSymbols = function () {
  return this.getIntersectionListByFunction("getFormerSymbols");
};

/**
 * @returns {Annotation[]}
 */
SearchBioEntityGroup.prototype.getReferences = function () {
  return this.getIntersectionListByFunction("getReferences");
};

/**
 *
 * @param {string} param
 * @returns {Object}
 */
SearchBioEntityGroup.prototype.getOther = function (param) {
  if (param === "modifications") {
    return this.getIntersectionListByFunction(function (alias) {
      return alias.getOther(param)
    });
  } else {
    throw new Error("Don't now how to handle: " + param);
  }
};

/**
 *
 * @param {string} functionName
 * @returns {*}
 */
SearchBioEntityGroup.prototype.getMergedParameterByFunction = function (functionName) {
  var bioEntities = this.getBioEntities();
  var result = bioEntities[0][functionName]();
  for (var i = 1; i < bioEntities.length; i++) {
    var newEntry = bioEntities[i][functionName]();
    if (newEntry !== result) {
      result = "Value different among merged elements";
    }
  }
  return result;
};

/**
 *
 * @param {string|function} functionName
 * @returns {Array}
 */
SearchBioEntityGroup.prototype.getIntersectionListByFunction = function (functionName) {
  var bioEntities = this.getBioEntities();
  var result;
  if (typeof functionName === "function") {
    result = functionName(bioEntities[0]);
  } else {
    result = bioEntities[0][functionName]();
  }
  for (var i = 1; i < bioEntities.length; i++) {
    var newList;
    if (typeof functionName === "function") {
      newList = functionName(bioEntities[i]);
    } else {
      newList = bioEntities[0][functionName]();
    }
    //intersection of two arrays
    result = result.filter(function (n) {
      return newList.indexOf(n) !== -1;
    });
  }
  return result;
};


module.exports = SearchBioEntityGroup;
