"use strict";

/**
 *
 * @param {Object} jsonObject
 * @param {string} jsonObject.parameterId
 * @param {number} jsonObject.value
 * @param {boolean} jsonObject.global
 * @param {string} jsonObject.name
 * @param {number} jsonObject.id
 * @param {number} jsonObject.unitsId
 * @constructor
 */
function SbmlParameter(jsonObject) {
  var self = this;
  self.setParameterId(jsonObject.parameterId);
  self.setValue(jsonObject.value);
  self.setGlobal(jsonObject.global);
  self.setName(jsonObject.name);
  self.setId(jsonObject.id);
  self.setUnitsId(jsonObject.unitsId);
}

/**
 *
 * @param {string} parameterId
 */
SbmlParameter.prototype.setParameterId = function (parameterId) {
  this._parameterId = parameterId;
};

/**
 *
 * @returns {string}
 */
SbmlParameter.prototype.getParameterId = function () {
  return this._parameterId;
};

/**
 *
 * @param {number} unitsId
 */
SbmlParameter.prototype.setUnitsId = function (unitsId) {
  this._unitsId = unitsId;
};

/**
 *
 * @returns {number}
 */
SbmlParameter.prototype.getUnitsId = function () {
  return this._unitsId;
};

/**
 *
 * @param {string} value
 */
SbmlParameter.prototype.setValue = function (value) {
  this._value = value;
};

/**
 *
 * @returns {string}
 */
SbmlParameter.prototype.getValue = function () {
  return this._value;
};

/**
 *
 * @param {boolean} global
 */
SbmlParameter.prototype.setGlobal = function (global) {
  this._global = global;
};

/**
 *
 * @returns {boolean}
 */
SbmlParameter.prototype.getGlobal = function () {
  return this._global;
};

/**
 *
 * @param {string} name
 */
SbmlParameter.prototype.setName = function (name) {
  this._name = name;
};

/**
 *
 * @returns {string}
 */
SbmlParameter.prototype.getName = function () {
  return this._name;
};

/**
 *
 * @param {number} id
 */
SbmlParameter.prototype.setId = function (id) {
  this._id = id;
};

/**
 *
 * @returns {number}
 */
SbmlParameter.prototype.getId = function () {
  return this._id;
};


module.exports = SbmlParameter;
