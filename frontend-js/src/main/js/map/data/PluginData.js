"use strict";

var Functions = require('../../Functions');

/**
 *
 * @param {Object} params
 * @param {string} params.hash
 * @param {string[]} params.urls
 * @param {string} params.name
 * @param {string} params.version
 * @param {boolean} [params.isPublic=false]
 * @param {boolean} [params.isDefault=false]
 * @constructor
 */
function PluginData(params) {
  this.setHash(params.hash);
  this.setUrls(params.urls);
  this.setName(params.name);
  this.setPublic(params.isPublic);
  this.setDefault(params.isDefault);
  this.setVersion(params.version);
}

/**
 *
 * @param {string} hash
 */
PluginData.prototype.setHash = function (hash) {
  this._hash = hash;
};

/**
 *
 * @returns {string}
 */
PluginData.prototype.getHash = function () {
  return this._hash;
};

/**
 *
 * @param {?null|string|boolean} value
 * @return {boolean}
 */
function getBooleanValue(value) {
  if (value === null || value === undefined) {
    return false;
  } else if (Functions.isString(value)) {
    return (value === "true");
  } else {
    return value;
  }
}

/**
 *
 * @param {?null|string|boolean} isPublic
 */
PluginData.prototype.setPublic = function (isPublic) {
  this._public = getBooleanValue(isPublic);
};

/**
 *
 * @returns {boolean}
 */
PluginData.prototype.isPublic = function () {
  return this._public;
};

/**
 *
 * @param {?null|string|boolean} isDefault
 */
PluginData.prototype.setDefault = function (isDefault) {
  this._default = getBooleanValue(isDefault);
};

/**
 *
 * @returns {boolean}
 */
PluginData.prototype.isDefault = function () {
  return this._default;
};

/**
 *
 * @param {string[]} urls
 */
PluginData.prototype.setUrls = function (urls) {
  this._urls = urls;
};

/**
 *
 * @returns {string[]}
 */
PluginData.prototype.getUrls = function () {
  return this._urls;
};

/**
 *
 * @param {string} name
 */
PluginData.prototype.setName = function (name) {
  this._name = name;
};

/**
 *
 * @returns {string}
 */
PluginData.prototype.getName = function () {
  return this._name;
};

/**
 *
 * @param {string} version
 */
PluginData.prototype.setVersion = function (version) {
  this._version = version;
};

/**
 *
 * @returns {string}
 */
PluginData.prototype.getVersion = function () {
  return this._version;
};

module.exports = PluginData;
