"use strict";

/* exported logger */

var Annotation = require("./Annotation");
var IdentifiedElement = require('./IdentifiedElement');

var logger = require('../../logger');

/**
 * @typedef {Object} TargetOptions
 * @property {string} name
 * @property {IdentifiedElementInput[]} targetElements
 * @property {AnnotationOptions[]} targetParticipants
 * @property {AnnotationOptions[]} references
 */

/**
 *
 * @param {TargetOptions|Target} javaObject
 * @constructor
 */
function Target(javaObject) {
  if (javaObject instanceof Target) {
    this.setName(javaObject.getName());
    this.setTargetElements(javaObject.getTargetElements());
    this.setTargetParticipants(javaObject.getTargetParticipants());
    this.setReferences(javaObject.getReferences());
  } else {
    this.setName(javaObject.name);
    this.setTargetElements(javaObject.targetElements);
    this.setTargetParticipants(javaObject.targetParticipants);
    this.setReferences(javaObject.references);
  }
}

/**
 *
 * @param {IdentifiedElementInput[]|IdentifiedElement[]} targetElements
 */
Target.prototype.setTargetElements = function (targetElements) {
  this._targetElements = [];
  for (var i = 0; i < targetElements.length; i++) {
    this._targetElements.push(new IdentifiedElement(targetElements[i]));
  }
  this.setIsVisible(this._targetElements.length > 0);
};

/**
 *
 * @returns {IdentifiedElement[]}
 */
Target.prototype.getTargetElements = function () {
  return this._targetElements;
};

/**
 *
 * @param {AnnotationOptions[]|Annotation[]} targetParticipants
 */
Target.prototype.setTargetParticipants = function (targetParticipants) {
  this._targetParticipants = [];
  for (var i = 0; i < targetParticipants.length; i++) {
    this._targetParticipants.push(new Annotation(targetParticipants[i]));
  }
};

/**
 *
 * @returns {Annotation[]}
 */
Target.prototype.getTargetParticipants = function () {
  return this._targetParticipants;
};

/**
 *
 * @param {string} name
 */
Target.prototype.setName = function (name) {
  this._name = name;
};

/**
 *
 * @returns {string}
 */
Target.prototype.getName = function () {
  return this._name;
};

/**
 *
 * @param {boolean} visible
 */
Target.prototype.setIsVisible = function (visible) {
  this._isVisible = visible;
};

/**
 *
 * @returns {boolean}
 */
Target.prototype.isVisible = function () {
  return this._isVisible;
};

/**
 *
 * @param {AnnotationOptions[]|Annotation[]} references
 */
Target.prototype.setReferences = function (references) {
  this._references = [];
  for (var i = 0; i < references.length; i++) {
    this._references.push(new Annotation(references[i]));
  }
};

/**
 *
 * @returns {Annotation[]}
 */
Target.prototype.getReferences = function () {
  return this._references;
};

module.exports = Target;
