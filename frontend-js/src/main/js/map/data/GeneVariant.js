"use strict";

/**
 * @typedef GeneVariantOptions
 * @property {number} position
 * @property {string} originalDna
 * @property {string} modifiedDna
 * @property {string} referenceGenomeType
 * @property {string} referenceGenomeVersion
 * @property {string} contig
 * @property {string} [aminoAcidChange]
 * @property {null|string} [allelFrequency]
 * @property {null|string} [variantIdentifier]
 */

/**
 *
 * @param {GeneVariantOptions|GeneVariant} javaObject
 * @constructor
 * */
function GeneVariant(javaObject) {
  if (javaObject instanceof GeneVariant) {
    this.setPosition(javaObject.getPosition());
    this.setOriginalDna(javaObject.getOriginalDna());
    this.setModifiedDna(javaObject.getModifiedDna());
    this.setReferenceGenomeType(javaObject.getReferenceGenomeType());
    this.setReferenceGenomeVersion(javaObject.getReferenceGenomeVersion());
    this.setContig(javaObject.getContig());
    this.setAllelFrequency(javaObject.getAllelFrequency());
    this.setVariantIdentifier(javaObject.getVariantIdentifier());
    this.setAminoAcidChange(javaObject.getAminoAcidChange());
  } else {
    this.setPosition(javaObject.position);
    this.setOriginalDna(javaObject.originalDna);
    this.setModifiedDna(javaObject.modifiedDna);
    this.setReferenceGenomeType(javaObject.referenceGenomeType);
    this.setReferenceGenomeVersion(javaObject.referenceGenomeVersion);
    this.setContig(javaObject.contig);
    this.setAllelFrequency(javaObject.allelFrequency);
    this.setVariantIdentifier(javaObject.variantIdentifier);
    this.setAminoAcidChange(javaObject.aminoAcidChange);
  }
}

/**
 *
 * @param {string} aminoAcidChange
 */
GeneVariant.prototype.setAminoAcidChange = function (aminoAcidChange) {
  this._aminoAcidChange = aminoAcidChange;
};

/**
 *
 * @returns {string}
 */
GeneVariant.prototype.getAminoAcidChange = function () {
  return this._aminoAcidChange;
};

/**
 *
 * @param {number} position
 */
GeneVariant.prototype.setPosition = function (position) {
  this._position = position;
};

/**
 *
 * @returns {number}
 */
GeneVariant.prototype.getPosition = function () {
  return this._position;
};

/**
 *
 * @param {string} originalDna
 */
GeneVariant.prototype.setOriginalDna = function (originalDna) {
  this._original = originalDna;
};

/**
 *
 * @returns {string}
 */
GeneVariant.prototype.getOriginalDna = function () {
  return this._original;
};

/**
 *
 * @param {string} modifiedDna
 */
GeneVariant.prototype.setModifiedDna = function (modifiedDna) {
  this._modifiedDna = modifiedDna;
};

/**
 *
 * @returns {string}
 */
GeneVariant.prototype.getModifiedDna = function () {
  return this._modifiedDna;
};

/**
 *
 * @param {string} contig
 */
GeneVariant.prototype.setContig = function (contig) {
  this._contig = contig;
};

/**
 *
 * @returns {string}
 */
GeneVariant.prototype.getContig = function () {
  return this._contig;
};

/**
 *
 * @param {string|number} allelFrequency
 */
GeneVariant.prototype.setAllelFrequency = function (allelFrequency) {
  if (allelFrequency === null) {
    this._allelFrequency = undefined;
  } else {
    this._allelFrequency = parseFloat(allelFrequency);
  }
};

/**
 *
 * @returns {number}
 */
GeneVariant.prototype.getAllelFrequency = function () {
  return this._allelFrequency;
};

/**
 *
 * @param {string} variantIdentifier
 */
GeneVariant.prototype.setVariantIdentifier = function (variantIdentifier) {
  if (variantIdentifier === null) {
    this._variantIdentifier = undefined;
  } else {
    this._variantIdentifier = variantIdentifier;
  }
};

/**
 *
 * @returns {string}
 */
GeneVariant.prototype.getVariantIdentifier = function () {
  return this._variantIdentifier;
};

/**
 *
 * @param {string} referenceGenomeType
 */
GeneVariant.prototype.setReferenceGenomeType = function (referenceGenomeType) {
  this._referenceGenomeType = referenceGenomeType;
};

/**
 *
 * @returns {string}
 */
GeneVariant.prototype.getReferenceGenomeType = function () {
  return this._referenceGenomeType;
};

/**
 *
 * @param {string} referenceGenomeVersion
 */
GeneVariant.prototype.setReferenceGenomeVersion = function (referenceGenomeVersion) {
  this._referenceGenomeVersion = referenceGenomeVersion;
};

/**
 *
 * @returns {string}
 */
GeneVariant.prototype.getReferenceGenomeVersion = function () {
  return this._referenceGenomeVersion;
};

module.exports = GeneVariant;
