"use strict";

var Annotation = require("./Annotation");
var Functions = require("../../Functions");

/**
 * Class representing BioEntity.
 *
 * @constructor
 */
function BioEntity() {
}

/**
 *
 * @returns {number|undefined}
 */
BioEntity.prototype.getLinkedSubmodelId = function () {
  return this._linkedSubmodelId;
};

/**
 *
 * @param {number} linkedSubmodelId
 */
BioEntity.prototype.setLinkedSubmodelId = function (linkedSubmodelId) {
  this._linkedSubmodelId = Functions.getIntOrUndefined(linkedSubmodelId);
};

/**
 * Returns identifier of the BioEntity.
 *
 * @returns {number} identifier of the BioEntity
 */
BioEntity.prototype.getId = function () {
  return this.id;
};

/**
 *
 * @param {number} id
 */
BioEntity.prototype.setId = function (id) {
  this.id = id;
};

/**
 * Returns model identifier where {@link BioEntity} is located.
 *
 * @returns {number} model identifier where {@link BioEntity} is located
 */
BioEntity.prototype.getModelId = function () {
  return this._modelId;
};

/**
 *
 * @param {number} modelId
 */
BioEntity.prototype.setModelId = function (modelId) {
  this._modelId = modelId;
};

/**
 *
 * @returns {boolean}
 */
BioEntity.prototype.isComplete = function () {
  return this._complete;
};

/**
 *
 * @param {boolean} complete
 */
BioEntity.prototype.setIsComplete = function (complete) {
  this._complete = complete;
};

/**
 *
 * @returns {string}
 */
BioEntity.prototype.getSymbol = function () {
  return this.symbol;
};

/**
 *
 * @param {string} symbol
 */
BioEntity.prototype.setSymbol = function (symbol) {
  this.symbol = symbol;
};

/**
 *
 * @returns {string}
 */
BioEntity.prototype.getAbbreviation = function () {
  return this._abbreviation;
};

/**
 *
 * @param {string} abbreviation
 */
BioEntity.prototype.setAbbreviation = function (abbreviation) {
  this._abbreviation = abbreviation;
};

/**
 *
 * @returns {string}
 */
BioEntity.prototype.getFormula = function () {
  return this._formula;
};

/**
 *
 * @param {string} formula
 */
BioEntity.prototype.setFormula = function (formula) {
  this._formula = formula;
};

/**
 *
 * @param {string[]} synonyms
 */
BioEntity.prototype.setSynonyms = function (synonyms) {
  this._synonyms = synonyms;
};

/**
 *
 * @returns {string[]}
 */
BioEntity.prototype.getSynonyms = function () {
  return this._synonyms;
};

/**
 *
 * @param {string} description
 */
BioEntity.prototype.setDescription = function (description) {
  this._description = description;
};

/**
 *
 * @returns {string}
 */
BioEntity.prototype.getDescription = function () {
  return this._description;
};

/**
 *
 * @param {string} type
 */
BioEntity.prototype.setType = function (type) {
  if (type === undefined) {
    throw new Error("type cannot be undefined");
  }
  this._type = type;
};

/**
 *
 * @returns {string}
 */
BioEntity.prototype.getType = function () {
  return this._type;
};

/**
 *
 * @param {string} type
 * @returns {Object}
 */
BioEntity.prototype.getOther = function (type) {
  if (this._other !== undefined) {
    return (type === undefined) ? this._other : this._other[type];
  }
};

/**
 *
 * @param {Object} other
 */
BioEntity.prototype.setOther = function (other) {
  this._other = other;
};

/**
 *
 * @returns {number}
 */
BioEntity.prototype.getHierarchyVisibilityLevel = function () {
  return this._hierarchyVisibilityLevel;
};

/**
 *
 * @param {number} hierarchyVisibilityLevel
 */
BioEntity.prototype.setHierarchyVisibilityLevel = function (hierarchyVisibilityLevel) {
  this._hierarchyVisibilityLevel = hierarchyVisibilityLevel;
};

/**
 *
 * @returns {Annotation[]}
 */
BioEntity.prototype.getReferences = function () {
  return this.references;
};

/**
 *
 * @param {Annotation[]} references
 */
BioEntity.prototype.setReferences = function (references) {
  if (references === undefined) {
    throw new Error("references must be defined");
  }
  this.references = [];
  for (var i = 0; i < references.length; i++) {
    this.references.push(new Annotation(references[i]));
  }
};

/**
 * @returns {Point}
 */
BioEntity.prototype.getCenter = function () {
  throw new Error("Not implemented");
};

module.exports = BioEntity;
