"use strict";

var ObjectWithListeners = require('../../ObjectWithListeners');

/**
 * @typedef {Object} PrivilegeTypeOptions
 * @property {string} commonName
 * @property {string} objectType
 * @property {string} valueType
 */

/**
 *
 * @param {PrivilegeTypeOptions} data
 * @param {string} name
 * @constructor
 * @extends ObjectWithListeners
 */
function PrivilegeType(data, name) {
  // call super constructor
  ObjectWithListeners.call(this);

  var self = this;
  self.setName(name);
  self.setCommonName(data.commonName);
  self.setObjectType(data.objectType);
  self.setValueType(data.valueType);
}

PrivilegeType.prototype = Object.create(ObjectWithListeners.prototype);
PrivilegeType.prototype.constructor = PrivilegeType;

/**
 *
 * @type {string}
 */
PrivilegeType.READ_PROJECT = 'READ_PROJECT';
/**
 *
 * @type {string}
 */
PrivilegeType.WRITE_PROJECT = 'WRITE_PROJECT';
/**
 *
 * @type {string}
 */
PrivilegeType.IS_ADMIN = 'IS_ADMIN';
/**
 *
 * @type {string}
 */
PrivilegeType.IS_CURATOR = 'IS_CURATOR';


/**
 *
 * @param {string} objectType
 */
PrivilegeType.prototype.setObjectType = function (objectType) {
  this._objectType = objectType;
};

/**
 *
 * @returns {?null|string}
 */
PrivilegeType.prototype.getObjectType = function () {
  return this._objectType;
};

/**
 *
 * @param {string} valueType
 */
PrivilegeType.prototype.setValueType = function (valueType) {
  this._valueType = valueType;
};

/**
 *
 * @returns {string}
 */
PrivilegeType.prototype.getValueType = function () {
  return this._valueType;
};

/**
 *
 * @param {string} commonName
 */
PrivilegeType.prototype.setCommonName = function (commonName) {
  this._commonName = commonName;
};

/**
 *
 * @returns {string}
 */
PrivilegeType.prototype.getCommonName = function () {
  return this._commonName;
};

/**
 *
 * @param {string} name
 */
PrivilegeType.prototype.setName = function (name) {
  this._name = name;
};

/**
 *
 * @returns {string}
 */
PrivilegeType.prototype.getName = function () {
  return this._name;
};

module.exports = PrivilegeType;
