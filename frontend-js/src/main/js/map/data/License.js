"use strict";

/**
 * @typedef {Object} LicenseOptions
 * @property {number} id
 * @property {string} name
 * @property {string} content
 */


/**
 *
 * @param {LicenseOptions} jsonObject
 * @constructor
 */
function License(jsonObject) {
  var self = this;
  self.setName(jsonObject.name);
  self.setId(jsonObject.id);
  self.setContent(jsonObject.content);
  self.setUrl(jsonObject.url);
}

/**
 *
 * @param {string} content
 */
License.prototype.setContent = function (content) {
  this._content = content;
};

/**
 *
 * @returns {string}
 */
License.prototype.getContent = function () {
  return this._content;
};

/**
 *
 * @param {string} url
 */
License.prototype.setUrl = function (url) {
  this._url = url;
};

/**
 *
 * @returns {string}
 */
License.prototype.getUrl = function () {
  return this._url;
};

/**
 *
 * @param {number} id
 */
License.prototype.setId = function (id) {
  this._id = id;
};

/**
 *
 * @returns {number}
 */
License.prototype.getId = function () {
  return this._id;
};

/**
 *
 * @param {string} name
 */
License.prototype.setName = function (name) {
  this._name = name;
};

/**
 *
 * @returns {string}
 */
License.prototype.getName = function () {
  return this._name;
};

module.exports = License;
