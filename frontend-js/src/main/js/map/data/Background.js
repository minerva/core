"use strict";

/**
 * Class representing background.
 *
 * @param {Object} data
 * @constructor
 */
function Background(data) {
  this.setId(data.id);
  this.setOrder(data.order);
  this.setName(data.name);
  this.setImagesDirectory(data.images);
  this.setDescription(data.description);
  this.setCreator(data.creator.login);
  this.setDefaultOverlay(data.defaultOverlay);

}

/**
 *
 * @returns {number}
 */
Background.prototype.getId = function () {
  return this.id;
};

/**
 *
 * @param {number|string} id
 */
Background.prototype.setId = function (id) {
  this.id = parseInt(id);
};

/**
 *
 * @returns {string}
 */
Background.prototype.getDescription = function () {
  return this._description;
};

/**
 *
 * @param {string} description
 */
Background.prototype.setDescription = function (description) {
  this._description = description;
};

/**
 *
 * @returns {string}
 */
Background.prototype.getCreator = function () {
  return this._creator;
};

/**
 *
 * @param {string} creator
 */
Background.prototype.setCreator = function (creator) {
  this._creator = creator;
};

/**
 *
 * @returns {string}
 */
Background.prototype.getName = function () {
  return this.name;
};

/**
 *
 * @param {string} name
 */
Background.prototype.setName = function (name) {
  this.name = name;
};

/**
 *
 * @param {number} modelId
 * @returns {?string|null}
 */
Background.prototype.getImagesDirectory = function (modelId) {
  for (var i = 0; i < this._imagesDirectory.length; i++) {
    if (parseInt(this._imagesDirectory[i].model.id) === modelId) {
      return this._imagesDirectory[i].path;
    }
  }
  return null;
};

/**
 *
 * @param {Object.<number,string>} imagesDirectory
 */
Background.prototype.setImagesDirectory = function (imagesDirectory) {
  this._imagesDirectory = imagesDirectory;
};

/**
 *
 * @returns {boolean}
 */
Background.prototype.isDefaultOverlay = function () {
  return this._defaultOverlay;
};

/**
 *
 * @param {boolean} defaultOverlay
 */
Background.prototype.setDefaultOverlay = function (defaultOverlay) {
  this._defaultOverlay = defaultOverlay;
};

/**
 *
 * @returns {number}
 */
Background.prototype.getOrder = function () {
  return this._order;
};

/**
 *
 * @param {number} order
 */
Background.prototype.setOrder = function (order) {
  this._order = order;
};

/**
 *
 * @param {Background} overlay
 */
Background.prototype.update = function (overlay) {
  this.setOrder(overlay.getOrder());
  this.setCreator(overlay.getCreator());
  this.setName(overlay.getName());
  this.setDescription(overlay.getDescription());
};


module.exports = Background;
