"use strict";

/**
 * @typedef {Object} Authority
 * @property {string} privilegeType
 * @property {string|?null} [objectId]
 */

/* exported logger */

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var UserPreferences = require('./UserPreferences');
var ObjectWithListeners = require('../../ObjectWithListeners');
var InvalidArgumentError = require('../../InvalidArgumentError');

/**
 *
 * @param javaObject
 *
 * @constructor
 */
function User(javaObject) {
  // call super constructor
  ObjectWithListeners.call(this);

  this.registerListenerType("onreload");

  this.setLogin(javaObject.login);
  this.setName(javaObject.name);
  this.setSurname(javaObject.surname);
  this.setEmail(javaObject.email);
  this.setConfirmed(javaObject.confirmed);
  this.setActive(javaObject.active);
  this.setRemoved(javaObject.removed);
  this.setPrivileges(javaObject.privileges);
  this.setPreferences(javaObject.preferences);
  this.setMinColor(javaObject.minColor);
  this.setMaxColor(javaObject.maxColor);
  this.setNeutralColor(javaObject.neutralColor);
  this.setSimpleColor(javaObject.simpleColor);
  this.setTermsOfUseConsent(javaObject.termsOfUseConsent);
  this.setLdapAccountAvailable(javaObject.ldapAccountAvailable);
  this.setConnectedToLdap(javaObject.connectedToLdap);
  this.setLastActive(javaObject.lastActive);
  this.setOrcidId(javaObject.orcidId);
}

// this class inherits from ObjectWithListeners class where generic methods for
// listeners are set
User.prototype = Object.create(ObjectWithListeners.prototype);
User.prototype.constructor = User;

/**
 *
 * @param {string} login
 */
User.prototype.setLogin = function (login) {
  this._login = login;
};

/**
 *
 * @returns {string}
 */
User.prototype.getLogin = function () {
  return this._login;
};

/**
 *
 * @param {string} name
 */
User.prototype.setName = function (name) {
  this._name = name;
};

/**
 *
 * @returns {string}
 */
User.prototype.getName = function () {
  return this._name;
};

/**
 *
 * @param {string} orcidId
 */
User.prototype.setOrcidId = function (orcidId) {
  this._orcidId = orcidId;
};

/**
 *
 * @returns {string}
 */
User.prototype.getOrcidId = function () {
  return this._orcidId;
};

/**
 *
 * @param {string} surname
 */
User.prototype.setSurname = function (surname) {
  this._surname = surname;
};

/**
 *
 * @returns {string}
 */
User.prototype.getSurname = function () {
  return this._surname;
};

/**
 *
 * @param {string} email
 */
User.prototype.setEmail = function (email) {
  this._email = email;
};

/**
 *
 * @returns {string}
 */
User.prototype.getEmail = function () {
  return this._email;
};

/**
 *
 * @param {string|boolean} confirmed
 */
User.prototype.setConfirmed = function (confirmed) {
  if (typeof confirmed == "boolean") {
    this._confirmed = confirmed;
  } else {
    this._confirmed = ((confirmed + "").toLowerCase() === "true");
  }
};

/**
 *
 * @returns {boolean}
 */
User.prototype.isConfirmed = function () {
  return this._confirmed;
};

/**
 *
 * @param {string|boolean} active
 */
User.prototype.setActive = function (active) {
  if (typeof active == "boolean") {
    this._active = active;
  } else {
    this._active = ((active + "").toLowerCase() === "true");
  }
};

/**
 *
 * @returns {boolean}
 */
User.prototype.isActive = function () {
  return this._active;
};

/**
 *
 * @param {boolean} removed
 */
User.prototype.setRemoved = function (removed) {
  this._removed = removed;
};

/**
 *
 * @returns {boolean}
 */
User.prototype.getRemoved = function () {
  return this._removed;
};

/**
 *
 * @param {string} minColor
 */
User.prototype.setMinColor = function (minColor) {
  this._minColor = minColor;
};

/**
 *
 * @returns {string}
 */
User.prototype.getMinColor = function () {
  return this._minColor;
};

/**
 *
 * @param {string} simpleColor
 */
User.prototype.setSimpleColor = function (simpleColor) {
  this._simpleColor = simpleColor;
};

/**
 *
 * @returns {string}
 */
User.prototype.getSimpleColor = function () {
  return this._simpleColor;
};

/**
 *
 * @param {string} neutralColor
 */
User.prototype.setNeutralColor = function (neutralColor) {
  this._neutralColor = neutralColor;
};

/**
 *
 * @returns {string}
 */
User.prototype.getNeutralColor = function () {
  return this._neutralColor;
};

/**
 *
 * @param {string} maxColor
 */
User.prototype.setMaxColor = function (maxColor) {
  this._maxColor = maxColor;
};

/**
 *
 * @returns {string}
 */
User.prototype.getMaxColor = function () {
  return this._maxColor;
};

/**
 *
 * @param {string} password
 */
User.prototype.setPassword = function (password) {
  this._password = password;
};

/**
 *
 * @returns {string}
 */
User.prototype.getPassword = function () {
  return this._password;
};

/**
 *
 * @param {Authority[]} privileges
 */
User.prototype.setPrivileges = function (privileges) {
  this._privileges = privileges;
  if (this._privileges === undefined) {
    this._privileges = [];
  } else if (Object.prototype.toString.call(privileges) !== '[object Array]') {
    throw new InvalidArgumentError("privileges property must be an array");
  }
};

/**
 *
 * @returns {Authority[]}
 */
User.prototype.getPrivileges = function () {
  return this._privileges;
};

/**
 *
 * @param {UserPreferences|Object} preferences
 */
User.prototype.setPreferences = function (preferences) {
  if (!(preferences instanceof UserPreferences)) {
    preferences = new UserPreferences(preferences);
  }
  this._preferences = preferences;
};

/**
 *
 * @returns {UserPreferences}
 */
User.prototype.getPreferences = function () {
  return this._preferences;
};

/**
 *
 * @param {PrivilegeType} type
 * @param {number|string} [objectId]
 * @returns {boolean}
 */
User.prototype.hasPrivilege = function (type, objectId) {
  if (objectId === undefined) {
    objectId = null;
  }
  for (var i = 0; i < this._privileges.length; i++) {
    var privilege = this._privileges[i];
    if (privilege.privilegeType === type.getName()) {
      if (objectId === privilege.objectId) {
        return true;
      }
    }
  }
  return false;
};

/**
 *
 * @param {Authority} authority
 */
User.prototype.setPrivilege = function (authority) {
  if (authority.objectId === undefined) {
    authority.objectId = null;
  }
  for (var i = 0; i < this._privileges.length; i++) {
    var privilege = this._privileges[i];
    if (privilege.privilegeType === authority.privilegeType &&
      privilege.objectId === authority.objectId) {
      return;
    }
  }
  this._privileges.push(authority);
};

/**
 *
 * @param {Authority} authority
 */
User.prototype.revokePrivilege = function (authority) {
  if (authority.objectId === undefined) {
    authority.objectId = null;
  }
  for (var i = 0; i < this._privileges.length; i++) {
    var privilege = this._privileges[i];
    if (privilege.privilegeType === authority.privilegeType &&
      privilege.objectId === authority.objectId) {
      this._privileges.splice(i, 1);
      return;
    }
  }
};

/**
 *
 * @param {User} user
 * @returns {PromiseLike}
 */
User.prototype.update = function (user) {
  var self = this;
  self.setLogin(user.getLogin());
  self.setName(user.getName());
  self.setSurname(user.getSurname());
  self.setEmail(user.getEmail());
  self.setConfirmed(user.isConfirmed());
  self.setActive(user.isActive());
  self.setRemoved(user.getRemoved());
  self.setPrivileges(user.getPrivileges());
  self.getPreferences().update(user.getPreferences());
  self.setMinColor(user.getMinColor());
  self.setMaxColor(user.getMaxColor());
  self.setSimpleColor(user.getSimpleColor());
  self.setNeutralColor(user.getNeutralColor());
  self.setTermsOfUseConsent(user.isTermsOfUseConsent());
  self.setLdapAccountAvailable(user.isLdapAccountAvailable());
  self.setConnectedToLdap(user.isConnectedToLdap());
  self.setLastActive(user.getLastActive());
  self.setOrcidId(user.getOrcidId());

  return self.callListeners("onreload");
};

/**
 *
 * @param {boolean} termsOfUseConsent
 */
User.prototype.setTermsOfUseConsent = function (termsOfUseConsent) {
  this._termsOfUseConsent = termsOfUseConsent;
};

/**
 *
 * @returns {boolean}
 */
User.prototype.isTermsOfUseConsent = function () {
  return this._termsOfUseConsent;
};

/**
 *
 * @param {boolean} ldapAccountAvailable
 */
User.prototype.setLdapAccountAvailable = function (ldapAccountAvailable) {
  if (ldapAccountAvailable !== undefined) {
    this._ldapAccountAvailable = ldapAccountAvailable;
  }
};

/**
 *
 * @returns {boolean}
 */
User.prototype.isLdapAccountAvailable = function () {
  return this._ldapAccountAvailable;
};

/**
 *
 * @param {boolean} connectedToLdap
 */
User.prototype.setConnectedToLdap = function (connectedToLdap) {
  if (connectedToLdap !== undefined) {
    this._connectedToLdap = connectedToLdap;
  }
};

/**
 *
 * @returns {boolean}
 */
User.prototype.isConnectedToLdap = function () {
  return this._connectedToLdap;
};

/**
 *
 * @param {?string|Date|null} lastActive
 */
User.prototype.setLastActive = function (lastActive) {
  if (lastActive !== undefined && lastActive !== null) {
    if (lastActive instanceof Date) {
      this._lastActive = lastActive;
    } else {
      lastActive = lastActive.replace(/ /g, "T");
      this._lastActive = new Date(lastActive);
    }
  } else {
    this._lastActive = null;
  }
};

/**
 *
 * @returns {?Date|null}
 */
User.prototype.getLastActive = function () {
  return this._lastActive;
};

module.exports = User;
