"use strict";

var IdentifiedElement = require('./IdentifiedElement');
var Point = require('../canvas/Point');

/**
 *
 * @param javaObject
 * @constructor
 */
function Comment(javaObject) {
  this.setIdentifiedElement(new IdentifiedElement({
    id: javaObject.elementId,
    type: javaObject.type,
    modelId: javaObject.modelId,
    icon: javaObject.icon
  }));
  this.setId(javaObject.id);
  this.setRemoved(javaObject.removed);
  this.setRemoveReason(javaObject.removeReason);
  this.setPinned(javaObject.pinned);

  if (javaObject.title !== undefined) {
    this.setCoordinates(javaObject.coord);
    this.setTitle(javaObject.title);
    this.setContent(javaObject.content);
  }
  this.setAuthor(javaObject.owner);
  this.setEmail(javaObject.email);
}

/**
 *
 * @param {IdentifiedElement} ie
 */
Comment.prototype.setIdentifiedElement = function (ie) {
  this._ie = ie;
};

/**
 *
 * @returns {IdentifiedElement}
 */
Comment.prototype.getIdentifiedElement = function () {
  return this._ie;
};

/**
 *
 * @param {boolean} pinned
 */
Comment.prototype.setPinned = function (pinned) {
  this._pinned = (pinned === true);
};

/**
 *
 * @param {{x:number, y:number}} coordinates
 */
Comment.prototype.setCoordinates = function (coordinates) {
  this._coordinates = new Point(coordinates.x, coordinates.y);
};

/**
 *
 * @param {boolean} removed
 */
Comment.prototype.setRemoved = function (removed) {
  this._removed = (removed === true);
};

/**
 *
 * @param {number} id
 */
Comment.prototype.setId = function (id) {
  this._id = id;
};

/**
 *
 * @param {string} content
 */
Comment.prototype.setContent = function (content) {
  this._content = content;
};

/**
 *
 * @returns {boolean}
 */
Comment.prototype.isPinned = function () {
  return this._pinned;
};

/**
 *
 * @returns {Point}
 */
Comment.prototype.getCoordinates = function () {
  return this._coordinates;
};

/**
 *
 * @returns {boolean}
 */
Comment.prototype.isRemoved = function () {
  return this._removed;
};

/**
 *
 * @returns {number}
 */
Comment.prototype.getId = function () {
  return this._id;
};

/**
 *
 * @param {string} title
 */
Comment.prototype.setTitle = function (title) {
  this._title = title;
};

/**
 *
 * @returns {string}
 */
Comment.prototype.getTitle = function () {
  return this._title;
};

/**
 *
 * @returns {string}
 */
Comment.prototype.getContent = function () {
  return this._content;
};

/**
 *
 * @param {string} author
 */
Comment.prototype.setAuthor = function (author) {
  this._author = author;
};

/**
 *
 * @returns {string}
 */
Comment.prototype.getAuthor = function () {
  return this._author;
};

/**
 *
 * @param {string} email
 */
Comment.prototype.setEmail = function (email) {
  this._email = email;
};

/**
 *
 * @returns {string}
 */
Comment.prototype.getEmail = function () {
  return this._email;
};

/**
 *
 * @param {string} removeReason
 */
Comment.prototype.setRemoveReason = function (removeReason) {
  this._removeReason = removeReason;
};

/**
 *
 * @returns {string}
 */
Comment.prototype.getRemoveReason = function () {
  return this._removeReason;
};

module.exports = Comment;
