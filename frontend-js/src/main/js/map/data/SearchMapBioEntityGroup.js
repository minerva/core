"use strict";

var BioEntity = require("./BioEntity");
var SearchBioEntityGroup = require("./SearchBioEntityGroup");

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
var IdentifiedElement = require("./IdentifiedElement");

/**
 * Class representing merged search bioEntities based on map.
 *
 * @param {BioEntity|SearchBioEntityGroup} bioEntity
 *          initial bioEntity from which group is created
 * @constructor
 */
function SearchMapBioEntityGroup(bioEntity) {
  if (!(bioEntity instanceof BioEntity) && !(bioEntity instanceof SearchBioEntityGroup)) {
    throw new Error("Invalid argument");
  }
  this._bioEntites = [bioEntity];
}

/**
 *
 * @param {BioEntity} newBioEntity
 */
SearchMapBioEntityGroup.prototype.addBioEntity = function (newBioEntity) {
  this._bioEntites.push(newBioEntity);
};

/**
 *
 * @returns {BioEntity[]}
 */
SearchMapBioEntityGroup.prototype.getBioEntities = function () {
  return this._bioEntites;
};

/**
 *
 * @returns {number}
 */
SearchMapBioEntityGroup.prototype.getModelId = function () {
  return this._bioEntites[0].getModelId();
};

/**
 *
 * @returns {IdentifiedElement[]}
 */
SearchMapBioEntityGroup.prototype.getIdentifiedElements = function () {
  var result = [];
  for (var i = 0; i < this.getBioEntities().length; i++) {
    var element = this.getBioEntities()[i];
    if (element instanceof SearchBioEntityGroup) {
      for (var j = 0; j < element.getBioEntities().length; j++) {
        result.push(new IdentifiedElement(element.getBioEntities()[j]));
      }
    } else {
      result.push(new IdentifiedElement(element));
    }
  }
  return result;
}

module.exports = SearchMapBioEntityGroup;
