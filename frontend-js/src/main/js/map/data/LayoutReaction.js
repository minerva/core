"use strict";

/**
 * Class representing reaction visualized in a overlay.
 *
 * @param javaObject
 *          object de-serialized from ajax query to the server side
 */
function LayoutReaction(javaObject) {
  this.setId(javaObject.idObject);
  this.setModelId(javaObject.modelId);
  this.setWidth(javaObject.width);
  this.setColor(javaObject.color);
  this.setReverse(javaObject.reverse);
  this.setValue(javaObject.value);
}

/**
 *
 * @returns {number }
 */
LayoutReaction.prototype.getId = function () {
  return this.id;
};

/**
 *
 * @param id
 */
LayoutReaction.prototype.setId = function (id) {
  this.id = parseInt(id);
};

/**
 *
 * @returns {number}
 */
LayoutReaction.prototype.getModelId = function () {
  return this._modelId;
};

/**
 *
 * @param modelId
 */
LayoutReaction.prototype.setModelId = function (modelId) {
  this._modelId = parseInt(modelId);
};

/**
 *
 * @returns {number}
 */
LayoutReaction.prototype.getValue = function () {
  return this.value;
};

/**
 *
 * @param {number} value
 */
LayoutReaction.prototype.setValue = function (value) {
  this.value = value;
};

/**
 *
 * @param {number} width
 */
LayoutReaction.prototype.setWidth = function (width) {
  this.width = width;
};

/**
 *
 * @param {string} color
 */
LayoutReaction.prototype.setColor = function (color) {
  this.color = color;
};

/**
 *
 * @param {boolean} reverse
 */
LayoutReaction.prototype.setReverse = function (reverse) {
  this.reverse = reverse;
};

/**
 *
 * @returns {number}
 */
LayoutReaction.prototype.getWidth = function () {
  return this.width;
};

/**
 *
 * @returns {string}
 */
LayoutReaction.prototype.getColor = function () {
  return this.color;
};

/**
 *
 * @returns {boolean}
 */
LayoutReaction.prototype.getReverse = function () {
  return this.reverse;
};

module.exports = LayoutReaction;
