"use strict";

/* exported logger */

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var Annotator = require('./Annotator');

/**
 *
 * @param [javaObject]
 * @constructor
 */
function UserPreferences(javaObject) {
  if (javaObject !== undefined) {
    this.setProjectUpload(javaObject["project-upload"]);
    this.setElementAnnotators(javaObject["element-annotators"]);
    this.setGuiPreferences(javaObject["gui-preferences"]);
  } else {
    this._projectUpload = {};
    /**
     * @private
     */
    this._elementAnnotators = {};
    this._guiPreferences = {};
  }
}

/**
 *
 * @param {UserPreferences} userPreferences
 */
UserPreferences.prototype.update = function (userPreferences) {
  var updateDict = function (originalDict, newDict) {
    for (var key in newDict) {
      if (newDict.hasOwnProperty(key)) {
        originalDict[key] = newDict[key];
      }
    }
  };

  updateDict(this._projectUpload, userPreferences.getProjectUpload());
  updateDict(this._elementAnnotators, userPreferences._elementAnnotators);
  updateDict(this._guiPreferences, userPreferences.getGuiPreferences());
};

/**
 *
 * @param {Object} projectUpload
 * @param {boolean} projectUpload.auto-resize
 * @param {boolean} projectUpload.sbgn
 */
UserPreferences.prototype.setProjectUpload = function (projectUpload) {
  this._projectUpload = {
    autoResize: projectUpload["auto-resize"],
    annotateModel: projectUpload["annotate-model"],
    sbgn: projectUpload["sbgn"]
  };
};

UserPreferences.prototype.getGuiPreferences = function () {
  return this._guiPreferences;
};
UserPreferences.prototype.setGuiPreference = function (key, value) {
  this._guiPreferences[key] = value;
};
UserPreferences.prototype.getGuiPreference = function (key, defaultValue) {
  var result = this._guiPreferences[key];
  if (result === undefined) {
    result = defaultValue;
  }
  return result;
};
UserPreferences.prototype.setGuiPreferences = function (guiPreferences) {
  this._guiPreferences = guiPreferences;
};
UserPreferences.prototype.getProjectUpload = function () {
  return this._projectUpload;
};

/**
 *
 * @param {Object<string, Annotator>} elementAnnotators
 */
UserPreferences.prototype.setElementAnnotators = function (elementAnnotators) {
  this._elementAnnotators = {};
  for (var key in elementAnnotators) {
    if (elementAnnotators.hasOwnProperty(key)) {
      var annotators = elementAnnotators[key];
      this._elementAnnotators[key] = [];
      for (var i = 0; i < annotators.length; i++) {
        this._elementAnnotators[key].push(new Annotator(annotators[i]));
      }
    }
  }
};

/**
 *
 * @param {string} className
 * @returns {Annotator[]}
 */
UserPreferences.prototype.getElementAnnotators = function (className) {
  var result = this._elementAnnotators[className];
  if (result === undefined) {
    this._elementAnnotators[className] = [];
    result = this._elementAnnotators[className];
  }
  return result;
};


UserPreferences.prototype.toExport = function () {
  var elementAnnotators = {};

  for (var key in this._elementAnnotators) {
    if (this._elementAnnotators.hasOwnProperty(key)) {
      elementAnnotators[key] = [];
      for (var i = 0; i < this._elementAnnotators[key].length; i++) {
        elementAnnotators[key].push(this._elementAnnotators[key][i].toExport());
      }
    }
  }

  return {
    "project-upload": {
      "auto-resize": this._projectUpload.autoResize,
      "annotate-model": this._projectUpload.annotateModel,
      "sbgn": this._projectUpload.sbgn,
    },
    "element-annotators": elementAnnotators,
    "gui-preferences": this.getGuiPreferences()
  };
};

module.exports = UserPreferences;
