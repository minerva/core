"use strict";

/**
 *
 * @param {ModifierOptions} javaObject
 * @constructor
 */
function Product(javaObject) {
  this.setAlias(javaObject.aliasId);
  this.setStoichiometry(javaObject.stoichiometry);
}

/**
 *
 * @returns {number}
 */
Product.prototype.getAlias = function () {
  return this._alias;
};

/**
 *
 * @param {number} alias
 */
Product.prototype.setAlias = function (alias) {
  this._alias = alias;
};

/**
 *
 * @returns {string}
 */
Product.prototype.getStoichiometry = function () {
  return this._stoichiometry;
};

/**
 *
 * @param {string} stoichiometry
 */
Product.prototype.setStoichiometry = function (stoichiometry) {
  this._stoichiometry = stoichiometry;
};

module.exports = Product;
