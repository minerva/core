"use strict";

/**
 * @typedef {Object} TaxonomyOptions
 * @property {string} name
 * @property {string} id
 */


/**
 *
 * @param {TaxonomyOptions} jsonObject
 * @constructor
 */
function Taxonomy(jsonObject) {
  var self = this;
  self.setName(jsonObject.name);
  self.setId(jsonObject.id);
}

/**
 *
 * @param {string} name
 */
Taxonomy.prototype.setName = function (name) {
  this._name = name;
};

/**
 *
 * @returns {string}
 */
Taxonomy.prototype.getName = function () {
  return this._name;
};

/**
 *
 * @param {string} id
 */
Taxonomy.prototype.setId = function (id) {
  this._id = id;
};

/**
 *
 * @returns {string}
 */
Taxonomy.prototype.getId = function () {
  return this._id;
};

module.exports = Taxonomy;
