"use strict";

/* exported logger */

var BioEntity = require("./BioEntity");
var KineticLaw = require("./KineticLaw");
var Point = require("../canvas/Point");

var Modifier = require('./Modifier');
var Product = require('./Product');
var Reactant = require('./Reactant');

var logger = require('../../logger');

/**
 * Class representing reaction data.
 *
 * @param javaObject
 *          object de-serialized from ajax query to the server side
 * @constructor
 * @extends BioEntity
 */
function Reaction(javaObject) {
  BioEntity.call(this, javaObject);
  this.startLines = [];
  this.endLines = [];
  this.midLines = [];
  if (javaObject instanceof Reaction) {
    this.setId(javaObject.getId());
    this.setCenter(javaObject.getCenter());
    this.setModelId(javaObject.getModelId());

    this.startLines.push.apply(this.startLines, javaObject.getStartLines());
    this.endLines.push.apply(this.endLines, javaObject.getEndLines());
    this.midLines.push.apply(this.midLines, javaObject.getMidLines());

  } else {
    if (javaObject.idObject !== undefined) {
      this.setId(javaObject.idObject);
    } else {
      this.setId(javaObject.id);
    }
    for (var i = 0; i < javaObject.lines.length; i++) {
      var line = javaObject.lines[i];
      if (line.type === "START") {
        this.startLines.push(line);
      } else if (line.type === "END") {
        this.endLines.push(line);
      } else if (line.type === "MIDDLE") {
        this.midLines.push(line);
      } else {
        throw new Error("Unknown line type: " + line.type);
      }
    }
    this.setCenter(javaObject.centerPoint);
    this.setModelId(javaObject.modelId);
    this.setIsComplete(false);
    this.update(javaObject);
  }
}

Reaction.prototype = Object.create(BioEntity.prototype);
Reaction.prototype.constructor = Reaction;

/**
 *
 * @returns {Point}
 */
Reaction.prototype.getCenter = function () {
  return this._center;
};

/**
 *
 * @returns {Object[]}
 */
Reaction.prototype.getLines = function () {
  var result = [];
  result = result.concat(this.startLines);
  result = result.concat(this.endLines);
  result = result.concat(this.midLines);
  return result;
};

/**
 *
 * @returns {Object[]}
 */
Reaction.prototype.getMidLines = function () {
  return this.midLines;
};

/**
 *
 * @returns {Object[]}
 */
Reaction.prototype.getStartLines = function () {
  return this.startLines;
};

/**
 *
 * @returns {Object[]}
 */
Reaction.prototype.getEndLines = function () {
  return this.endLines;
};

/**
 *
 * @param {Point| {x:number, y:number}} center
 */
Reaction.prototype.setCenter = function (center) {
  if (center === null || center === undefined) {
    throw new Error("Setting undefined center: " + center);
  }
  this._center = new Point(center);
};

/**
 *
 * @param javaObject
 */
Reaction.prototype.update = function (javaObject) {
  if (javaObject.reactionId === undefined) {
    return;
  }
  this.setReactionId(javaObject.reactionId);
  this.setName(javaObject.name);
  this.setSymbol(javaObject.symbol);
  this.setAbbreviation(javaObject.abbreviation);
  this.setFormula(javaObject.formula);
  this.setMechanicalConfidenceScore(javaObject.mechanicalConfidenceScore);
  this.setLowerBound(javaObject.lowerBound);
  this.setUpperBound(javaObject.upperBound);
  this.setGeneProteinReaction(javaObject.geneProteinReaction);
  this.setSubsystem(javaObject.subsystem);
  this.setSynonyms(javaObject.synonyms);
  this.setDescription(javaObject.notes);
  this.setOther(javaObject.other);
  this.setReferences(javaObject.references);
  this.setType(javaObject.type);

  var reactants = [];
  var products = [];
  var modifiers = [];
  var i;
  if (javaObject.reactants !== undefined) {
    for (i = 0; i < javaObject.reactants.length; i++) {
      reactants.push(new Reactant(javaObject.reactants[i]));
    }
  }
  if (javaObject.products !== undefined) {
    for (i = 0; i < javaObject.products.length; i++) {
      products.push(new Product(javaObject.products[i]));
    }
  }
  if (javaObject.modifiers !== "" && javaObject.modifiers !== undefined) {
    for (i = 0; i < javaObject.modifiers.length; i++) {
      modifiers.push(new Modifier(javaObject.modifiers[i]));
    }
  }
  this.setReactants(reactants);
  this.setProducts(products);
  this.setModifiers(modifiers);

  if (javaObject.kineticLaw !== undefined && javaObject.kineticLaw !== null) {
    this.setKineticLaw(new KineticLaw(javaObject.kineticLaw));
  }

  this.setHierarchyVisibilityLevel(javaObject.hierarchyVisibilityLevel);
  this.setIsComplete(true);
};

/**
 *
 * @returns {boolean}
 */
Reaction.prototype.isComplete = function () {
  var self = this;
  var result = self._complete;
  if (result) {
    var reactants = self.getReactants();
    if (reactants.length === 0) {
      result = false;
    } else {
      result = reactants[0].isComplete();
    }
  }
  return result;
};

/**
 *
 * @returns {string}
 */
Reaction.prototype.getReactionId = function () {
  return this._reactionId;
};

/**
 *
 * @returns {string}
 */
Reaction.prototype.getElementId = function () {
  return this.getReactionId();
};

/**
 *
 * @param {string} reactionId
 */
Reaction.prototype.setReactionId = function (reactionId) {
  this._reactionId = reactionId;
};

/**
 *
 * @param {string} name
 */
Reaction.prototype.setName = function (name) {
  this._name = name;
};

/**
 *
 * @return {string}
 */
Reaction.prototype.getName = function () {
  return this._name;
};

/**
 *
 * @returns {string}
 */
Reaction.prototype.getMechanicalConfidenceScore = function () {
  return this._mechanicalConfidenceScore;
};

/**
 *
 * @param {string} mechanicalConfidenceScore
 */
Reaction.prototype.setMechanicalConfidenceScore = function (mechanicalConfidenceScore) {
  this._mechanicalConfidenceScore = mechanicalConfidenceScore;
};

/**
 *
 * @returns {string}
 */
Reaction.prototype.getLowerBound = function () {
  return this._lowerBound;
};

/**
 *
 * @param {string} lowerBound
 */
Reaction.prototype.setLowerBound = function (lowerBound) {
  this._lowerBound = lowerBound;
};

/**
 *
 * @returns {string}
 */
Reaction.prototype.getUpperBound = function () {
  return this._upperBound;
};

/**
 *
 * @param {string} upperBound
 */
Reaction.prototype.setUpperBound = function (upperBound) {
  this._upperBound = upperBound;
};

/**
 *
 * @param {string} geneProteinReaction
 */
Reaction.prototype.setGeneProteinReaction = function (geneProteinReaction) {
  this._geneProteinReaction = geneProteinReaction;
};

/**
 *
 * @returns {string}
 */
Reaction.prototype.getGeneProteinReaction = function () {
  return this._geneProteinReaction;
};

/**
 *
 * @param {string} subsystem
 */
Reaction.prototype.setSubsystem = function (subsystem) {
  this._subsystem = subsystem;
};

/**
 *
 * @returns {string}
 */
Reaction.prototype.getSubsystem = function () {
  return this._subsystem;
};

/**
 *
 * @returns {Reactant[]}
 */
Reaction.prototype.getReactants = function () {
  return this._reactants;
};

/**
 *
 * @param {Reactant[]} reactants
 */
Reaction.prototype.setReactants = function (reactants) {
  this._reactants = reactants;
};

/**
 *
 * @param {Product[]} products
 */
Reaction.prototype.setProducts = function (products) {
  this._products = products;
};

/**
 *
 * @returns {Product[]}
 */
Reaction.prototype.getProducts = function () {
  return this._products;
};

/**
 *
 * @returns {Alias[]}
 */
Reaction.prototype.getElements = function () {
  var result = [], i;
  for (i = 0; i < this.getReactants().length; i++) {
    result.push(this.getReactants()[i].getAlias());
  }
  for (i = 0; i < this.getProducts().length; i++) {
    result.push(this.getProducts()[i].getAlias());
  }
  for (i = 0; i < this.getModifiers().length; i++) {
    result.push(this.getModifiers()[i].getAlias());
  }
  return result;
};

/**
 *
 * @param {Modifier[]} modifiers
 */
Reaction.prototype.setModifiers = function (modifiers) {
  this._modifiers = modifiers;
};

/**
 *
 * @returns {Modifier[]}
 */
Reaction.prototype.getModifiers = function () {
  return this._modifiers;
};

/**
 *
 * @param {KineticLaw} kineticLaw
 */
Reaction.prototype.setKineticLaw = function (kineticLaw) {
  this._kineticLaw = kineticLaw;
};

/**
 *
 * @returns {KineticLaw}
 */
Reaction.prototype.getKineticLaw = function () {
  return this._kineticLaw;
};

module.exports = Reaction;
