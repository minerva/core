"use strict";

/* exported logger */

var ObjectWithListeners = require('../../ObjectWithListeners');

var Annotation = require("./Annotation");
var DataOverlay = require("./DataOverlay");
var License = require("./License");
var Model = require('./MapModel');

var Functions = require('../../Functions');

var Promise = require('bluebird');

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');
var Background = require("./Background");

/**
 * @typedef {Object} OverviewImage
 * @property {number} idObject
 * @property {number} width
 * @property {number} height
 * @property {string} filename
 */


/**
 *
 * @param {Project|string|Object} [data]
 * @constructor
 */
function Project(data) {
  // call super constructor
  ObjectWithListeners.call(this);
  this.registerListenerType("onreload");

  /**
   * @type {Array<DataOverlay>}
   * @private
   */
  this._dataOverlays = [];

  /**
   * @type {Array<Background>}
   * @private
   */
  this._backgrounds = [];
  this._models = [];
  this._elementsPointingToSubmap = [];

  if (data !== undefined) {
    this.loadFromData(data);
  }
}

// this class inherits from ObjectWithListeners class where generic methods for
// listeners are set
Project.prototype = Object.create(ObjectWithListeners.prototype);
Project.prototype.constructor = Project;

/**
 *
 * @param {Project|string} data
 * @private
 */
Project.prototype.loadFromData = function (data) {
  var self = this;
  if (typeof data === "string") {
    // replace is due to some strange problem with serialization
    data = JSON.parse(data.replace(/\n/g, " "));
  }
  if (data instanceof Project) {
    self._update(data);
  } else {
    self.setId(parseInt(data.idObject));
    self.setProjectId(data.projectId);
    self.setCreationDate(data.creationDate);
    self.setDirectory(data.directory);
    self.setVersion(data.version);
    self.setName(data.name);
    self.setOverviewImages(data.overviewImageViews);
    self.setTopOverviewImage(data.topOverviewImage);
    self.setDisease(data.disease);
    self.setOrganism(data.organism);
    self.setOwner(data.owner);
    self.setStatus(data.status);
    self.setNotifyEmail(data.notifyEmail);
    self.setProgress(data.progress);
    self.setHasLogEntries(data.logEntries);
    if (data.license !== null && data.license !== undefined) {
      self.setLicense(new License(data.license));
    }
    self.setSharedInMinervaNet(data.sharedInMinervaNet);
    if (data.license !== null && data.license !== undefined) {
      self.setLicense(new License(data.license));
    } else {
      self.setLicense(null);
    }
    self.setCustomLicenseName(data.customLicenseName);
    self.setCustomLicenseUrl(data.customLicenseUrl);
  }
};

/**
 *
 * @param {Project} data
 * @returns {PromiseLike}
 */
Project.prototype.update = function (data) {
  this._update(data);
  return this.callListeners("onreload");
};

/**
 *
 * @param {Project} data
 * @private
 */
Project.prototype._update = function (data) {
  var self = this;
  self.setId(data.getId());
  self.setProjectId(data.getProjectId());
  self.setDirectory(data.getDirectory());
  self.setVersion(data.getVersion());
  self.setName(data.getName());
  self.setOverviewImages(data.getOverviewImages());
  self.setTopOverviewImage(data.getTopOverviewImage());
  self.setDisease(data.getDisease());
  self.setOrganism(data.getOrganism());
  self.setOwner(data.getOwner());
  self.setStatus(data.getStatus());
  self.setProgress(data.getProgress());
  self.setNotifyEmail(data.getNotifyEmail());
  self.setHasLogEntries(data.hasLogEntries());
  self.setSharedInMinervaNet(data.getSharedInMinervaNet());
  self.setLicense(data.getLicense());

  if (data.getModels() !== undefined) {
    var models = data.getModels();
    for (var i = 0; i < models.length; i++) {
      self.addModel(new Model(models[i]));
    }
  }
};

/**
 *
 * @returns {number}
 */
Project.prototype.getId = function () {
  return this._id;
};

/**
 *
 * @param {number|string} id
 */
Project.prototype.setId = function (id) {
  this._id = parseInt(id);
};

/**
 *
 * @returns {boolean}
 */
Project.prototype.hasLogEntries = function () {
  return this._hasLogEntries === true;
};

/**
 *
 * @param {boolean} hasLogEntries
 */
Project.prototype.setHasLogEntries = function (hasLogEntries) {
  this._hasLogEntries = hasLogEntries;
};


/**
 *
 * @returns {string}
 */
Project.prototype.getProjectId = function () {
  return this._projectId;
};

/**
 *
 * @param {string} projectId
 */
Project.prototype.setProjectId = function (projectId) {
  this._projectId = projectId;
};

/**
 *
 * @returns {string}
 */
Project.prototype.getDirectory = function () {
  return this._directory;
};

/**
 *
 * @param {string} directory
 */
Project.prototype.setDirectory = function (directory) {
  this._directory = directory;
};

/**
 *
 * @returns {string}
 */
Project.prototype.getVersion = function () {
  return this._version;
};

/**
 *
 * @param {string} version
 */
Project.prototype.setVersion = function (version) {
  this._version = version;
};

/**
 *
 * @returns {string}
 */
Project.prototype.getName = function () {
  return this._name;
};

/**
 *
 * @param {string} name
 */
Project.prototype.setName = function (name) {
  this._name = name;
};

/**
 *
 * @returns {string}
 */
Project.prototype.getCustomLicenseName = function () {
  return this._customLicenseName;
};

/**
 *
 * @param {string} customLicenseName
 */
Project.prototype.setCustomLicenseName = function (customLicenseName) {
  this._customLicenseName = customLicenseName;
};

/**
 *
 * @returns {string}
 */
Project.prototype.getCustomLicenseUrl = function () {
  return this._customLicenseUrl;
};

/**
 *
 * @param {string} customLicenseUrl
 */
Project.prototype.setCustomLicenseUrl = function (customLicenseUrl) {
  this._customLicenseUrl = customLicenseUrl;
};

/**
 *
 * @returns {MapModel[]}
 */
Project.prototype.getModels = function () {
  return this._models;
};

/**
 *
 * @param modelId
 * @returns {MapModel|null}
 */
Project.prototype.getModelById = function (modelId) {
  for (var i = 0; i < this._models.length; i++) {
    if (this._models[i].getId() === modelId) {
      return this._models[i];
    }
  }
  return null;
};

/**
 *
 * @param {MapModel[]} models
 */
Project.prototype.setModels = function (models) {
  this._models = models;
};

/**
 *
 * @param {MapModel} model
 */
Project.prototype.addModel = function (model) {
  this._models.push(model);
};

/**
 *
 * @returns {OverviewImage[]}
 */
Project.prototype.getOverviewImages = function () {
  return this._overviewImages;
};

/**
 *
 * @param {OverviewImage[]} overviewImages
 */
Project.prototype.setOverviewImages = function (overviewImages) {
  this._overviewImages = overviewImages;
};

/**
 *
 * @returns {OverviewImage}
 */
Project.prototype.getTopOverviewImage = function () {
  return this._topOverviewImage;
};

/**
 *
 * @param {OverviewImage} topOverviewImage
 */
Project.prototype.setTopOverviewImage = function (topOverviewImage) {
  if (topOverviewImage === null) {
    this._topOverviewImage = undefined;
  } else {
    this._topOverviewImage = topOverviewImage;
  }
};

/**
 *
 * @returns {Annotation}
 */
Project.prototype.getDisease = function () {
  return this._disease;
};

/**
 *
 * @param {AnnotationOptions|Annotation|null} disease
 */
Project.prototype.setDisease = function (disease) {
  if (disease !== undefined && disease !== null) {
    this._disease = new Annotation(disease);
  } else {
    this._disease = undefined;
  }
};

/**
 *
 * @returns {Annotation}
 */
Project.prototype.getOrganism = function () {
  return this._organism;
};

/**
 *
 * @param {AnnotationOptions|Annotation|null} organism
 */
Project.prototype.setOrganism = function (organism) {
  if (organism !== undefined && organism !== null) {
    this._organism = new Annotation(organism);
  } else {
    this._organism = undefined;
  }
};

/**
 *
 * @returns {string}
 */
Project.prototype.getStatus = function () {
  return this._status;
};

/**
 *
 * @param {string} status
 */
Project.prototype.setStatus = function (status) {
  this._status = status;
};

/**
 *
 * @returns {string}
 */
Project.prototype.getOwner = function () {
  return this._owner;
};

/**
 *
 * @param {string} owner
 */
Project.prototype.setOwner = function (owner) {
  this._owner = owner;
};

/**
 *
 * @returns {number}
 */
Project.prototype.getProgress = function () {
  return this._progress;
};

/**
 *
 * @param {number} progress
 */
Project.prototype.setProgress = function (progress) {
  this._progress = progress;
};

/**
 *
 * @returns {string}
 */
Project.prototype.getNotifyEmail = function () {
  return this._notifyEmail;
};

/**
 *
 * @param {string} notifyEmail
 */
Project.prototype.setNotifyEmail = function (notifyEmail) {
  this._notifyEmail = notifyEmail;
};

/**
 *
 * @returns {License}
 */
Project.prototype.getLicense = function () {
  return this._license;
};

/**
 *
 * @returns {?number}
 */
Project.prototype.getLicenseId = function () {
  if (this._license !== null && this._license !== undefined) {
    return this._license.getId();
  } else {
    return null;
  }
};

/**
 *
 * @param {License} license
 */
Project.prototype.setLicense = function (license) {
  this._license = license;
};

/**
 *
 * @param {DataOverlay} overlay
 * @param {boolean} [updateWhenExists=false]
 */
Project.prototype.addDataOverlay = function (overlay, updateWhenExists) {
  var overlayToAdd = null;
  if (overlay instanceof DataOverlay) {
    overlayToAdd = overlay;
  } else {
    overlayToAdd = new DataOverlay(overlay);
  }
  var object = this._dataOverlays[overlayToAdd.getId()];
  if (object === undefined) {
    this._dataOverlays[overlayToAdd.getId()] = overlayToAdd;
  } else {
    if (updateWhenExists) {
      object.update(overlay);
    } else {
      throw new Error("Overlay " + overlayToAdd.getId() + " already exists in a project " + this.getProjectId());
    }
  }
};

/**
 *
 * @param {DataOverlay|number} overlay
 */
Project.prototype.removeDataOverlay = function (overlay) {
  var id;
  if (Functions.isInt(overlay)) {
    id = overlay;
  } else {
    id = overlay.getId();
  }
  var object = this._dataOverlays[id];
  if (object === undefined) {
    throw new Error("Overlay " + id + " doesn't exist in a project " + this.getProjectId());
  } else {
    delete this._dataOverlays[id];
  }
};

/**
 *
 * @param {DataOverlay} overlay
 */
Project.prototype.addOrUpdateDataOverlay = function (overlay) {
  this.addDataOverlay(overlay, true);
};


/**
 *
 * @param {DataOverlay[]} overlays
 */
Project.prototype.addOrUpdateDataOverlays = function (overlays) {
  this.addDataOverlays(overlays, true);
};

/**
 *
 * @param {Background[]} backgrounds
 */
Project.prototype.addOrUpdateBackgrounds = function (backgrounds) {
  for (var i = 0; i < backgrounds.length; i++) {
    var background = backgrounds[i];
    var backgroundToAdd = null;
    if (background instanceof Background) {
      backgroundToAdd = background;
    } else {
      backgroundToAdd = new Background(background);
    }
    var object = this._backgrounds[backgroundToAdd.getId()];
    if (object === undefined) {
      this._backgrounds[backgroundToAdd.getId()] = backgroundToAdd;
    } else {
      object.update(backgroundToAdd);
    }
  }
};

/**
 *
 * @param {DataOverlay[]} overlays
 * @param {boolean} updateWhenExists
 */
Project.prototype.addDataOverlays = function (overlays, updateWhenExists) {
  if (overlays === undefined) {
    logger.warn("Overlays are undefined...");
  } else {
    for (var i = 0; i < overlays.length; i++) {
      this.addDataOverlay(overlays[i], updateWhenExists);
    }
  }
};

/**
 *
 * @returns {DataOverlay[]}
 */
Project.prototype.getDataOverlays = function () {
  var result = [];
  for (var id in this._dataOverlays) {
    if (this._dataOverlays.hasOwnProperty(id)) {
      result.push(this._dataOverlays[id]);
    }
  }
  result.sort(function (dataOverlay1, dataOverlay2) {
    if (dataOverlay1.getOrder() < dataOverlay2.getOrder())
      return -1;
    if (dataOverlay1.getOrder() > dataOverlay2.getOrder())
      return 1;
    return 0;
  });
  return result;
};

/**
 *
 * @returns {Background[]}
 */
Project.prototype.getBackgrounds = function () {
  var result = [];
  for (var id in this._backgrounds) {
    if (this._backgrounds.hasOwnProperty(id)) {
      result.push(this._backgrounds[id]);
    }
  }
  result.sort(function (background1, background2) {
    if (background1.getOrder() < background2.getOrder())
      return -1;
    if (background1.getOrder() > background2.getOrder())
      return 1;
    return 0;
  });
  return result;
};


/**
 * Returns data overlay for a given overlay identifier.
 *
 * @param {number} overlayId
 *          overlay identifier
 * @returns {Promise<DataOverlay>} for a given overlay identifier
 */
Project.prototype.getDataOverlayById = function (overlayId) {
  var self = this;
  if (self._dataOverlays[overlayId] !== undefined) {
    return Promise.resolve(self._dataOverlays[overlayId]);
  } else {
    return ServerConnector.getOverlayById(overlayId).then(function (overlay) {
      if (overlay !== null) {
        self.addDataOverlay(overlay, false);
        return self._dataOverlays[overlayId];
      } else {
        return null;
      }
    });
  }
};

/**
 * Returns promise with list of elements pointing to the submap.
 *
 * @param {number} modelId id of the submap
 * @param {boolean} [direct=false]
 * @returns {Promise} for a given overlay identifier
 */
Project.prototype.getElementsPointingToSubmap = function (modelId, direct) {
  var self = this;
  if (direct === undefined) {
    direct = false;
  }
  if (self._elementsPointingToSubmap[modelId] !== undefined && self._elementsPointingToSubmap[modelId][direct] !== undefined) {
    return Promise.resolve(self._elementsPointingToSubmap[modelId][direct]);
  }
  if (self._submapConnections === undefined) {
    self._submapConnections = ServerConnector.getSubmapConnections().then(function (data) {
      self._submapConnections = data;
      return data;
    });
  }
  return Promise.resolve(self._submapConnections).then(function (data) {
    self._elementsPointingToSubmap[modelId] = [];
    self._elementsPointingToSubmap[modelId][true] = [];
    self._elementsPointingToSubmap[modelId][false] = [];

    var queue = [modelId];

    while (queue.length > 0) {
      var id = queue.shift();
      for (var i = 0; i < data.length; i++) {
        var connection = data[i];
        if (connection.to === id) {
          var elementToAdd = connection.from;
          var alreadyAdded = false;
          for (var j = 0; j < self._elementsPointingToSubmap[modelId][false].length; j++) {
            var elementAlreadyAdded = self._elementsPointingToSubmap[modelId][false][j];
            if (elementToAdd.getId() === elementAlreadyAdded.getId()) {
              alreadyAdded = true;
            }
          }
          if (!alreadyAdded) {
            self._elementsPointingToSubmap[modelId][false].push(elementToAdd);
            if (id === modelId) {
              self._elementsPointingToSubmap[modelId][true].push(elementToAdd);
            }
            queue.push(elementToAdd.getModelId());
          }
        }
      }
    }

    return self._elementsPointingToSubmap[modelId][direct];
  });
};

/**
 *
 * @param {IdentifiedElement[]} elements
 * @param {boolean} complete
 * @returns {Promise}
 */
Project.prototype.getBioEntitiesByIdentifiedElements = function (elements, complete) {
  var self = this;
  var elementsByModelId = [];
  var i;
  for (i = 0; i < elements.length; i++) {
    var element = elements[i];
    var modelId = element.getModelId();
    if (elementsByModelId[modelId] === undefined) {
      elementsByModelId[modelId] = [];
    }
    elementsByModelId[modelId].push(element);
  }

  var models = self.getModels();
  var promises = [];
  for (i = 0; i < models.length; i++) {
    if (elementsByModelId[models[i].getId()] !== undefined) {
      promises.push(models[i].getByIdentifiedElements(elementsByModelId[models[i].getId()], complete));
    }
  }
  return Promise.all(promises);
};

/**
 *
 * @returns {string}
 */
Project.prototype.getCreationDate = function () {
  return this._creationDate;
};

/**
 *
 * @param {string|null} creationDate
 */
Project.prototype.setCreationDate = function (creationDate) {
  if (creationDate === null) {
    creationDate = undefined;
  }
  this._creationDate = creationDate;
};

/**
 *
 * @returns {boolean}
 */
Project.prototype.getSharedInMinervaNet = function () {
  return this._sharedInMinervaNet;
};

/**
 *
 * @param {boolean|null} sharedInMinervaNet
 */
Project.prototype.setSharedInMinervaNet = function (sharedInMinervaNet) {
  if (sharedInMinervaNet === null || sharedInMinervaNet === undefined) {
    sharedInMinervaNet = false;
  }
  this._sharedInMinervaNet = sharedInMinervaNet;
};


module.exports = Project;
