"use strict";

var GeneVariant = require('./GeneVariant');

/**
 * @typedef LayoutAliasOptions
 * @property {number} idObject
 * @property {number} [value]
 * @property {{rgb:number}} [color]
 * @property {number} modelId
 * @property {string} [description]
 * @property {number} [uniqueId]
 * @property {string} [type=LayoutAlias.LIGHT]
 * @property {Array} [geneVariations] */

/**
 * Class representing alias visualized in a overlay.
 *
 * @param {LayoutAliasOptions|LayoutAlias} javaObject
 * @constructor
 * */
function LayoutAlias(javaObject) {
  if (javaObject instanceof LayoutAlias) {
    this.setId(javaObject.getId());
    this.setValue(javaObject.getValue());
    this.setColor(javaObject.getColor());
    this.setModelId(javaObject.getModelId());
    this.setDescription(javaObject.getDescription());
    this.setDataOverlayEntryId(javaObject.getDataOverlayEntryId());
    this.setType(javaObject.getType());

    this.setGeneVariants([]);
    for (var i = 0; i < javaObject.getGeneVariants().length; i++) {
      this.addGeneVariant(new GeneVariant(javaObject.getGeneVariants()[i]));
    }
  } else {
    this.setId(javaObject.idObject);
    this.setValue(javaObject.value);
    this.setColor(javaObject.color);
    this.setModelId(javaObject.modelId);
    this.setDescription(javaObject.description);
    this.setDataOverlayEntryId(javaObject.uniqueId);
    if (javaObject.type === undefined) {
      this.setType(LayoutAlias.LIGHT);
    } else if (javaObject.type === LayoutAlias.GENETIC_VARIANT) {
      if (javaObject.geneVariations !== undefined) {
        this.setType(LayoutAlias.GENETIC_VARIANT);
      } else {
        this.setType(LayoutAlias.LIGHT);
      }
    } else if (javaObject.type === LayoutAlias.GENERIC) {
      this.setType(LayoutAlias.GENERIC);
    } else {
      throw new Error("Unknown type: " + javaObject.type);
    }

    this.setGeneVariants([]);
    if (javaObject.geneVariations !== undefined) {
      for (var j = 0; j < javaObject.geneVariations.length; j++) {
        this.addGeneVariant(new GeneVariant(javaObject.geneVariations[j]));
      }
    }
  }
}

LayoutAlias.LIGHT = "LIGHT";
LayoutAlias.GENETIC_VARIANT = "GENETIC_VARIANT";
LayoutAlias.GENERIC = "GENERIC";

/**
 *
 * @returns {number}
 */
LayoutAlias.prototype.getId = function () {
  return this.id;
};

/**
 *
 * @param id
 */
LayoutAlias.prototype.setId = function (id) {
  this.id = parseInt(id);
};

/**
 *
 * @returns {number}
 */
LayoutAlias.prototype.getModelId = function () {
  return this._modelId;
};

/**
 *
 * @param modelId
 */
LayoutAlias.prototype.setModelId = function (modelId) {
  this._modelId = parseInt(modelId);
};

/**
 *
 * @returns {number}
 */
LayoutAlias.prototype.getValue = function () {
  return this.value;
};

/**
 *
 * @returns {{rgb:number}}
 */
LayoutAlias.prototype.getColor = function () {
  return this.color;
};

/**
 *
 * @returns {string}
 */
LayoutAlias.prototype.getType = function () {
  return this._type;
};

/**
 *
 * @returns {GeneVariant[]}
 */
LayoutAlias.prototype.getGeneVariants = function () {
  return this._geneVariants;
};

/**
 *
 * @param {number} newValue
 */
LayoutAlias.prototype.setValue = function (newValue) {
  this.value = newValue;
};

/**
 *
 * @param {{rgb:number}} newColor
 */
LayoutAlias.prototype.setColor = function (newColor) {
  this.color = newColor;
};

/**
 *
 * @param {string} newType
 */
LayoutAlias.prototype.setType = function (newType) {
  this._type = newType;
};

/**
 *
 * @param {GeneVariant[]} newGeneVariants
 */
LayoutAlias.prototype.setGeneVariants = function (newGeneVariants) {
  this._geneVariants = newGeneVariants;
};

/**
 *
 * @param {LayoutAlias} alias
 */
LayoutAlias.prototype.update = function (alias) {
  if (!(alias instanceof LayoutAlias)) {
    throw new Error("Unknown parameter type: " + alias);
  }

  this.setValue(alias.getValue());
  this.setColor(alias.getColor());
  this.setGeneVariants(alias.getGeneVariants());
  this.setType(alias.getType());
  this.setDescription(alias.getDescription());
};

/**
 *
 * @param {GeneVariant} geneVariant
 */
LayoutAlias.prototype.addGeneVariant = function (geneVariant) {
  this._geneVariants.push(geneVariant);
};

/**
 *
 * @returns {string}
 */
LayoutAlias.prototype.getDescription = function () {
  return this._description;
};

/**
 *
 * @param {string} description
 */
LayoutAlias.prototype.setDescription = function (description) {
  this._description = description;
};

/**
 *
 * @returns {number}
 */
LayoutAlias.prototype.getDataOverlayEntryId = function () {
  return this._dataOverlayEntryId;
};

/**
 *
 * @param {number} dataOverlayEntryId
 */
LayoutAlias.prototype.setDataOverlayEntryId = function (dataOverlayEntryId) {
  this._dataOverlayEntryId = dataOverlayEntryId;
};

module.exports = LayoutAlias;
