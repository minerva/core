"use strict";

/**
 * @typedef {Object} KineticLawOptions
 * @property {number[]} parameterIds
 * @property {number[]} functionIds
 * @property {string} definition
 * @property {string} mathMlPresentation
 */

/**
 *
 * @param {KineticLawOptions} jsonObject
 * @constructor
 */
function KineticLaw(jsonObject) {
  var self = this;
  self.setParameterIds(jsonObject.parameterIds);
  self.setFunctionIds(jsonObject.functionIds);
  self.setDefinition(jsonObject.definition);
  self.setMathMlPresentation(jsonObject.mathMlPresentation);
}

/**
 *
 * @param {number[]} parameterIds
 */
KineticLaw.prototype.setParameterIds = function (parameterIds) {
  this._parameterIds = parameterIds;
};

/**
 *
 * @returns {number[]}
 */
KineticLaw.prototype.getParameterIds = function () {
  return this._parameterIds;
};

/**
 *
 * @param {number[]} functionIds
 */
KineticLaw.prototype.setFunctionIds = function (functionIds) {
  this._functionIds = functionIds;
};

/**
 *
 * @returns {number[]}
 */
KineticLaw.prototype.getFunctionIds = function () {
  return this._functionIds;
};

/**
 *
 * @param {string} definition
 */
KineticLaw.prototype.setDefinition = function (definition) {
  this._definition = definition;
};

/**
 *
 * @returns {string}
 */
KineticLaw.prototype.getDefinition = function () {
  return this._definition;
};

/**
 *
 * @param {string} mathMlPresentation
 */
KineticLaw.prototype.setMathMlPresentation = function (mathMlPresentation) {
  this._mathMlPresentation = mathMlPresentation;
};

/**
 *
 * @returns {string}
 */
KineticLaw.prototype.getMathMlPresentation = function () {
  return this._mathMlPresentation;
};

module.exports = KineticLaw;
