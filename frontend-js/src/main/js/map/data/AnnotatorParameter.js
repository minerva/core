"use strict";

/**
 * @typedef {Object} AnnotatorParameterDefinition
 * @property {string} type
 * @property {number} order
 * @property {string} [annotation_type]
 * @property {string} [field]
 * @property {string} [description]
 * @property {string} [name]
 * @property {string} [commonName]
 * @property {string} [inputType]
 * @property {string} [value]
 */


/**
 *
 * @param {AnnotatorParameter|AnnotatorParameterDefinition} javaObject
 * @constructor
 */
function AnnotatorParameter(javaObject) {
  var self = this;
  if (javaObject instanceof AnnotatorParameter) {
    self.setType(javaObject.getType());
    self.setInputType(javaObject.getInputType());
    self.setOrder(javaObject.getOrder());
    self.setAnnotationType(javaObject.getAnnotationType());
    self.setField(javaObject.getField());
    self.setDescription(javaObject.getDescription());
    self.setName(javaObject.getName());
    self.setCommonName(javaObject.getCommonName());
    self.setValue(javaObject.getValue());
  } else {
    self.setType(javaObject.type);
    self.setInputType(javaObject.inputType);
    self.setOrder(javaObject.order);
    self.setAnnotationType(javaObject.annotation_type);
    self.setField(javaObject.field);
    self.setDescription(javaObject.description);
    self.setName(javaObject.name);
    self.setCommonName(javaObject.commonName);
    self.setValue(javaObject.value);
  }
}

/**
 *
 * @returns {string}
 */
AnnotatorParameter.prototype.getType = function () {
  return this._type;
};

/**
 *
 * @param {string} type
 */
AnnotatorParameter.prototype.setType = function (type) {
  this._type = type;
};

/**
 *
 * @returns {string}
 */
AnnotatorParameter.prototype.getInputType = function () {
  return this._inputType;
};

/**
 *
 * @param {string} inputType
 */
AnnotatorParameter.prototype.setInputType = function (inputType) {
  this._inputType = inputType;
};

/**
 *
 * @returns {number}
 */
AnnotatorParameter.prototype.getOrder = function () {
  return this._order;
};

/**
 *
 * @param {number} order
 */
AnnotatorParameter.prototype.setOrder = function (order) {
  this._order = order;
};

/**
 *
 * @returns {string}
 */
AnnotatorParameter.prototype.getName = function () {
  return this._name;
};

/**
 *
 * @param {string} name
 */
AnnotatorParameter.prototype.setName = function (name) {
  this._name = name;
};

/**
 *
 * @returns {string}
 */
AnnotatorParameter.prototype.getCommonName = function () {
  return this._commonName;
};

/**
 *
 * @param {string} commonName
 */
AnnotatorParameter.prototype.setCommonName = function (commonName) {
  this._commonName = commonName;
};

/**
 *
 * @returns {string}
 */
AnnotatorParameter.prototype.getValue = function () {
  return this._value;
};

/**
 *
 * @param {string} value
 */
AnnotatorParameter.prototype.setValue = function (value) {
  this._value = value;
};

/**
 *
 * @returns {string}
 */
AnnotatorParameter.prototype.getField = function () {
  return this._field;
};

/**
 *
 * @param {string} field
 */
AnnotatorParameter.prototype.setField = function (field) {
  if (field === null) {
    field = undefined;
  }
  this._field = field;
};

/**
 *
 * @returns {string}
 */
AnnotatorParameter.prototype.getDescription = function () {
  return this._description;
};

/**
 *
 * @param {string} description
 */
AnnotatorParameter.prototype.setDescription = function (description) {
  this._description = description;
};

/**
 *
 * @returns {string}
 */
AnnotatorParameter.prototype.getAnnotationType = function () {
  return this._annotationType;
};

/**
 *
 * @param {string} annotationType
 */
AnnotatorParameter.prototype.setAnnotationType = function (annotationType) {
  if (annotationType === null) {
    annotationType = undefined;
  }
  this._annotationType = annotationType;
};

/**
 *
 * @return {Object}
 */
AnnotatorParameter.prototype.toExport = function () {
  var self = this;
  return {
    "type": self.getType(),
    "order": self.getOrder(),
    "annotation_type": self.getAnnotationType(),
    "field": self.getField(),
    "name": self.getName(),
    "value": self.getValue()
  };
};

module.exports = AnnotatorParameter;
