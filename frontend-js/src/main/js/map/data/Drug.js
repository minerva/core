"use strict";

var Annotation = require("./Annotation");
var TargettingStructure = require("./TargettingStructure");

/**
 * @typedef {TargettingStructureOptions} DrugOptions
 * @property {string[]} brandNames
 * @property {Annotation[]} references
 * @property {string[]} synonyms
 * @property {string} description
 * @property {string} bloodBrainBarrier
 */

/**
 *
 * @param {DrugOptions|Drug} javaObject
 * @constructor
 */
function Drug(javaObject) {
  TargettingStructure.call(this, javaObject);
  if (javaObject !== undefined) {
    if (javaObject instanceof Drug) {
      this.setBrandNames(javaObject.getBrandNames());
      this.setReferences(javaObject.getReferences());
      this.setSynonyms(javaObject.getSynonyms());
      this.setDescription(javaObject.getDescription());
      this.setBloodBrainBarrier(javaObject.getBloodBrainBarrier());
    } else {
      this.setBrandNames(javaObject.brandNames);
      this.setReferences(javaObject.references);
      this.setSynonyms(javaObject.synonyms);
      this.setDescription(javaObject.description);
      this.setBloodBrainBarrier(javaObject.bloodBrainBarrier);
    }
  }
}

Drug.prototype = Object.create(TargettingStructure.prototype);
Drug.prototype.constructor = Drug;

/**
 *
 * @param {string[]} brandNames
 */
Drug.prototype.setBrandNames = function (brandNames) {
  this._brandNames = brandNames;
};

/**
 *
 * @returns {string[]}
 */
Drug.prototype.getBrandNames = function () {
  return this._brandNames;
};

/**
 *
 * @param {Annotation[]}references
 */
Drug.prototype.setReferences = function (references) {
  this._references = [];
  for (var i = 0; i < references.length; i++) {
    this._references.push(new Annotation(references[i]));
  }
};

/**
 *
 * @returns {Annotation[]}
 */
Drug.prototype.getReferences = function () {
  return this._references;
};

/**
 *
 * @param {string[]} synonyms
 */
Drug.prototype.setSynonyms = function (synonyms) {
  this._synonyms = synonyms;
};

/**
 *
 * @returns {string[]}
 */
Drug.prototype.getSynonyms = function () {
  return this._synonyms;
};

/**
 *
 * @param {string} description
 */
Drug.prototype.setDescription = function (description) {
  this._description = description;
};

/**
 *
 * @returns {string}
 */
Drug.prototype.getDescription = function () {
  return this._description;
};

/**
 *
 * @param {string} bloodBrainBarrier
 */
Drug.prototype.setBloodBrainBarrier = function (bloodBrainBarrier) {
  this._bloodBrainBarrier = bloodBrainBarrier;
};

/**
 *
 * @returns {string}
 */
Drug.prototype.getBloodBrainBarrier = function () {
  return this._bloodBrainBarrier;
};

module.exports = Drug;
