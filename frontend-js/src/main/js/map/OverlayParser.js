"use strict";

var logger = require('./../logger');
var DataOverlay = require('./data/DataOverlay');
var TextDecoder = require('text-encoding').TextDecoder;

/**
 *
 * @constructor
 */
function OverlayParser() {
}

/**
 *
 * @param {string| Uint8Array | ArrayBuffer} content
 * @returns {string}
 * @private
 */
OverlayParser.prototype._extractContent = function (content) {
  if (content instanceof Uint8Array || content instanceof ArrayBuffer) {
    content = new TextDecoder("UTF8").decode(content);
  }
  return content;
};

/**
 *
 * @param {string| Uint8Array|ArrayBuffer} content
 * @returns {DataOverlay}
 */
OverlayParser.prototype.parse = function (content) {
  content = this._extractContent(content);
  var data = {content: content};
  var lines = content.split("\n");
  for (var i = 0; i < lines.length; i++) {
    var line = lines[i];
    if (line.indexOf("#") === 0) {
      if (line.indexOf("=") > 0) {
        var name = line.substring(1, line.indexOf("=")).trim();
        var value = line.substring(line.indexOf("=") + 1).trim();
        if (name === "NAME") {
          data.name = value;
        } else if (name === "DESCRIPTION") {
          data.description = value;
        } else if (name === "TYPE") {
          data.type = value;
        }
      } else {
        logger.warn("Invalid overlay header line: " + line);
      }
    } else {
      break;
    }
  }

  return new DataOverlay(data);
};

/**
 *
 * @param {string| Uint8Array | ArrayBuffer} content
 * @returns {boolean}
 */
OverlayParser.prototype.containsMixedNewLineCharacters = function (content) {
  content = this._extractContent(content);
  var newLineRegEx = /[\r\n]+/g;
  var match = newLineRegEx.exec(content);
  var newLineFormats = {};
  var counter = 0;
  while (match !== null && match !== undefined) {
    var foundMultiplication = false;
    var key = '';
    //this is just a heuristic - let's assume there are at most 10 empty lines in a file
    for (var i = 0; i < 10; i++) {
      key += match[0];
      if (newLineFormats[key]) {
        foundMultiplication = true;
      } else {
        newLineFormats[key] = true;
      }
    }
    if (!foundMultiplication) {
      counter++;
    }
    match = newLineRegEx.exec(content);
  }
  return counter > 1;
};


module.exports = OverlayParser;
