"use strict";

/* exported logger */

var Point = require('./map/canvas/Point');
var SessionObjectType = require('./SessionObjectType');

// noinspection JSUnusedLocalSymbols
var logger = require('./logger');

/**
 *
 * @param {Project} project
 * @constructor
 */
function SessionData(project) {
  var self = this;
  if (project === undefined) {
    throw new Error("Project must be passed as an argument");
  }
  self.setProject(project);

  ServerConnector.addListener("onDataLoadStart", function () {
    self.setLastRequestTimeStamp(Math.floor(Date.now() / 1000));
  });
}

/**
 *
 * @param {number} timestamp time stamp in seconds
 */
SessionData.prototype.setLastRequestTimeStamp = function (timestamp) {
  this._lastRequestTimeStamp = timestamp;
};

/**
 *
 * @returns {number} time stamp in seconds
 */
SessionData.prototype.getLastRequestTimeStamp = function () {
  return this._lastRequestTimeStamp;
};

/**
 *
 * @param {Project} project
 */
SessionData.prototype.setProject = function (project) {
  this._project = project;
};

/**
 *
 * @returns {Project}
 */
SessionData.prototype.getProject = function () {
  return this._project;
};

/**
 *
 * @returns {string}
 */
SessionData.prototype.getProjectId = function () {
  return this._project.getProjectId();
};

/**
 *
 * @returns {boolean}
 */
SessionData.prototype.getShowComments = function () {
  var key = this.getKey(SessionObjectType.SHOW_COMMENT);
  return window.localStorage.getItem(key) === "true";
};

/**
 *
 * @param {Object} value
 */
SessionData.prototype.setSearchQuery = function (value) {
  var key = this.getKey(SessionObjectType.SEARCH_QUERY);
  this._setStorageItem(key, JSON.stringify(value));
};


/**
 *
 * @param {string} key
 * @param {Object} value
 * @private
 */
SessionData.prototype._setStorageItem = function (key, value) {
  try {
    window.localStorage.setItem(key, value);
  } catch (e) {
    window.localStorage.clear();
    logger.warn("Problem with setting value in local storage: " + key);
  }
};

/**
 *
 * @param {Object} param
 * @param {string} param.type
 * @param {string} param.query
 */
SessionData.prototype.setQuery = function (param) {
  if (param.type === "drug") {
    this.setDrugQuery(param.query);
  } else if (param.type === "search") {
    this.setSearchQuery(param.query);
  } else if (param.type === "chemical") {
    this.setChemicalQuery(param.query);
  } else {
    throw new Error("Invalid query type: " + param.type);
  }
};

/**
 *
 * @returns {string}
 */
SessionData.prototype.getSearchQuery = function () {
  var key = this.getKey(SessionObjectType.SEARCH_QUERY);
  var result = window.localStorage.getItem(key);
  if (result !== undefined && result !== null) {
    return JSON.parse(result);
  } else {
    return undefined;
  }
};

/**
 *
 * @param {string} value
 */
SessionData.prototype.setDrugQuery = function (value) {
  var key = this.getKey(SessionObjectType.DRUG_QUERY);
  this._setStorageItem(key, JSON.stringify(value));
};

/**
 *
 * @returns {string}
 */
SessionData.prototype.getDrugQuery = function () {
  var key = this.getKey(SessionObjectType.DRUG_QUERY);
  var result = window.localStorage.getItem(key);
  if (result !== undefined && result !== null) {
    return JSON.parse(result);
  } else {
    return undefined;
  }
};

/**
 *
 * @param {Object} value
 */
SessionData.prototype.setChemicalQuery = function (value) {
  var key = this.getKey(SessionObjectType.CHEMICAL_QUERY);
  this._setStorageItem(key, JSON.stringify(value));
};

/**
 *
 * @returns {any}
 */
SessionData.prototype.getChemicalQuery = function () {
  var key = this.getKey(SessionObjectType.CHEMICAL_QUERY);
  var result = window.localStorage.getItem(key);
  if (result !== undefined && result !== null) {
    return JSON.parse(result);
  } else {
    return undefined;
  }
};

/**
 *
 * @param {boolean} value
 */
SessionData.prototype.setShowComments = function (value) {
  var key = this.getKey(SessionObjectType.SHOW_COMMENT);
  this._setStorageItem(key, value + "");
};

/**
 *
 * @returns {number}
 */
SessionData.prototype.getSelectedBackgroundOverlay = function () {
  var key = this.getKey(SessionObjectType.SELECTED_BACKGROUND_OVERLAY);
  var result = window.localStorage.getItem(key);
  if (result !== undefined && result !== null) {
    return parseInt(result);
  } else {
    return undefined;
  }
};

/**
 *
 * @param {number} value
 */
SessionData.prototype.setSelectedBackgroundOverlay = function (value) {
  var key = this.getKey(SessionObjectType.SELECTED_BACKGROUND_OVERLAY);
  this._setStorageItem(key, value + "");
};

/**
 *
 * @returns {number[]}
 */
SessionData.prototype.getVisibleOverlays = function () {
  var key = this.getKey(SessionObjectType.VISIBLE_OVERLAYS);
  var value = window.localStorage.getItem(key);
  if (value === undefined || value === null || value === "") {
    return [];
  } else {
    var result = [];
    value.split(",").forEach(function (element) {
      result.push(parseInt(element));
    });
    return result;
  }
};

/**
 *
 * @param {number[]} value
 */
SessionData.prototype.setVisibleOverlays = function (value) {
  var key = this.getKey(SessionObjectType.VISIBLE_OVERLAYS);
  this._setStorageItem(key, value + "");
};

/**
 *
 * @param {MapModel} model
 * @param {number} value
 */
SessionData.prototype.setZoomLevel = function (model, value) {
  var key = this.getKey(SessionObjectType.ZOOM_LEVEL, [model.getId()]);
  this._setStorageItem(key, value + "");
};

/**
 *
 * @param {MapModel} model
 * @returns {number}
 */
SessionData.prototype.getZoomLevel = function (model) {
  var key = this.getKey(SessionObjectType.ZOOM_LEVEL, [model.getId()]);
  var value = window.localStorage.getItem(key);
  if (value !== undefined && value !== null) {
    return parseInt(value);
  } else {
    return undefined;
  }
};

/**
 * TODO remove this method
 * @param {?null|string} token
 */
SessionData.prototype.setToken = function (token) {
  var key = SessionObjectType.TOKEN;
  if (token === undefined || token === null) {
    window.localStorage.removeItem(key);
  } else {
    this._setStorageItem(key, token);
  }
};

/**
 * TODO remove this method
 * @returns {string}
 */
SessionData.prototype.getToken = function () {
  var key = SessionObjectType.TOKEN;
  return window.localStorage.getItem(key);
};

/**
 *
 * @param {?null|string} login
 */
SessionData.prototype.setLogin = function (login) {
  var key = SessionObjectType.LOGIN;
  if (login === undefined || login === null) {
    window.localStorage.removeItem(key);
  } else {
    this._setStorageItem(key, login);
  }
};

/**
 *
 * @returns {?null|string}
 */
SessionData.prototype.getLogin = function () {
  var key = SessionObjectType.LOGIN;
  return window.localStorage.getItem(key);
};

/**
 *
 * @param {MapModel} model
 * @param {Point} value
 */
SessionData.prototype.setCenter = function (model, value) {
  var key = this.getKey(SessionObjectType.CENTER, [model.getId()]);
  this._setStorageItem(key, value.x + "," + value.y);
};

/**
 *
 * @param {MapModel} model
 * @returns {Point}
 */
SessionData.prototype.getCenter = function (model) {
  var key = this.getKey(SessionObjectType.CENTER, [model.getId()]);
  var value = window.localStorage.getItem(key);
  if (value !== undefined && value !== null) {
    var tmp = value.split(",");
    return new Point(tmp[0], tmp[1]);
  } else {
    return undefined;
  }
};

/**
 *
 * @param {string} type
 * @param {Array} [args]
 * @returns {string}
 */
SessionData.prototype.getKey = function (type, args) {
  if (type === undefined) {
    throw new Error("Undefined type");
  }
  if (args === undefined) {
    args = [];
  }
  return type + "_" + this.getProjectId() + "_" + args.join("_");
};

SessionData.prototype.clear = function () {
  window.localStorage.clear();
};

module.exports = SessionData;
