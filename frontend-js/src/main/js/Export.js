"use strict";

/* exported logger */

var Promise = require("bluebird");


var AbstractGuiElement = require('./gui/AbstractGuiElement');
var CustomMapOptions = require('./map/CustomMapOptions');
var ElementExportPanel = require('./gui/export/ElementExportPanel');
var GraphicsExportPanel = require('./gui/export/GraphicsExportPanel');
var NetworkExportPanel = require('./gui/export/NetworkExportPanel');
var Header = require('./gui/Header');

// noinspection JSUnusedLocalSymbols
var logger = require('./logger');
var Functions = require('./Functions');
var GuiUtils = require('./gui/leftPanel/GuiUtils');

/**
 * Default constructor.
 *
 * @param {CustomMapOptions} options
 *          CustomMapOptions object representing all parameters needed for map
 *          creation
 * @constructor
 */
function Export(options) {
  var self = this;
  if (!(options instanceof CustomMapOptions)) {
    options = new CustomMapOptions(options);
  }
  self.setProject(options.getProject());
  self.setElement(options.getElement());

  self.setConfiguration(options.getConfiguration());
  self.setServerConnector(options.getServerConnector());
  self._createGui();
}

Export.prototype = Object.create(AbstractGuiElement.prototype);
Export.prototype.constructor = AbstractGuiElement;

/**
 *
 * @private
 */
Export.prototype._createGui = function () {
  var self = this;
  self.getElement().innerHTML = "";
  var headerDiv = Functions.createElement({
    type: "div"
  });
  new Header({
    element: headerDiv,
    customMap: null,
    project: self.getProject(),
    configuration: self.getConfiguration(),
    serverConnector: self.getServerConnector()
  });
  self.getElement().appendChild(headerDiv);

  var panels = [{
    name: "ELEMENTS",
    panelClass: ElementExportPanel
  }, {
    name: "NETWORK",
    panelClass: NetworkExportPanel
  }, {
    name: "GRAPHICS",
    panelClass: GraphicsExportPanel
  }];

  self.getGuiUtils().initTabContent(self);

  for (var i = 0; i < panels.length; i++) {
    self.getGuiUtils().addTab(self, panels[i]);
  }
};

/**
 *
 * @returns {Promise}
 */
Export.prototype.init = function () {
  var promises = [];
  for (var i = 0; i < this._panels.length; i++) {
    promises.push(this._panels[i].init());
  }
  return Promise.all(promises);
};

/**
 * @returns {GuiUtils}
 */
Export.prototype.getGuiUtils = function () {
  var self = this;
  if (self._guiUtils === undefined) {
    self._guiUtils = new GuiUtils(self.getConfiguration());
  }
  return self._guiUtils;
};

module.exports = Export;
