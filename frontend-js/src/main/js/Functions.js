"use strict";

var Promise = require("bluebird");
var Point = require('./map/canvas/Point');

var logger = require('./logger');

var xss = require('xss');

function Functions() {
}

/**
 * Bounds value between opt_min and opt_max (result will be not smaller than
 * opt_min and not bigger than opt_max).
 */
Functions.prototype.bound = function (value, minVal, maxVal) {
  if (minVal !== null && minVal !== undefined) {
    value = Math.max(value, minVal);
  }
  if (maxVal !== null && maxVal !== undefined) {
    value = Math.min(value, maxVal);
  }
  return value;
};

/**
 *
 * @param {number} deg
 * @returns {number}
 */
Functions.prototype.degreesToRadians = function (deg) {
  return deg * (Math.PI / 180);
};

/**
 *
 * @param {number} rad
 * @returns {number}
 */
Functions.prototype.radiansToDegrees = function (rad) {
  return rad / (Math.PI / 180);
};

/**
 *
 * @param {number} value
 * @returns {string}
 */
Functions.prototype.intToColorString = function (value) {
  /* jslint bitwise: true */
  var trimmedValue = (value & 0xFFFFFF);
  var colorStr = trimmedValue.toString(16);
  while (colorStr.length < 6) {
    colorStr = "0" + colorStr;
  }
  return '#' + colorStr;
};

/**
 *
 * @param value
 * @returns {number|undefined}
 */
Functions.prototype.getIntOrUndefined = function (value) {
  if (this.isInt(value)) {
    return value;
  } else if (value === undefined || value === null) {
    return undefined;
  } else {
    logger.warn("Invalid argument type: " + value);
    return undefined;
  }
};


/**
 *
 * @param {string} color
 * @param {number} opacity
 * @returns {string}
 */
Functions.prototype.colorToRgbaString = function (color, opacity) {
  if (color[0] !== '#' || color.length !== 7) {
    throw new Error("Expected color in #RRGGBB format, but found: " + color);
  }
  var red = parseInt(color.substr(1, 2), 16);
  var green = parseInt(color.substr(3, 2), 16);
  var blue = parseInt(color.substr(5, 2), 16);
  var alpha = opacity !== undefined ? opacity : 1.0;
  return "rgba(" + red + "," + green + "," + blue + "," + alpha + ")";
};


/**
 *
 * @param {LayoutAlias[]|LayoutReaction[]} overlayData
 * @returns {Promise<Array>}
 */
Functions.prototype.overlaysToColorDataStructure = function (overlayData) {
  var promises = [];
  for (var i = 0; i < overlayData.length; i++) {
    promises.push(this.overlayToColor(overlayData[i]));
  }
  return Promise.all(promises).then(function (colors) {
    var countByColor = [], color;
    for (var i = 0; i < colors.length; i++) {
      color = colors[i];
      if (countByColor[color] !== undefined) {
        countByColor[color]++;
      } else {
        countByColor[color] = 1;
      }
    }
    var result = [];
    for (color in countByColor) {
      if (countByColor.hasOwnProperty(color)) {
        result.push({color: color, amount: countByColor[color]});
      }
    }
    var compare = function (a, b) {
      return a.color.localeCompare(b.color);
    };
    result.sort(compare);
    return result;
  });

};

/**
 * Returns stack trace.
 *
 * @returns {string} stack trace
 */
Functions.prototype.stackTrace = function () {
  var err = new Error();
  return err.stack;
};

/**
 * Returns the position of the element on html page.
 *
 * @param element
 *          element for which we want to get the position (top left corner)
 *
 * @return {Point} coordinates of the element
 *
 */
Functions.prototype.getPosition = function (element) {
  var xPosition = 0;
  var yPosition = 0;

  while (element) {
    xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
    yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
    element = element.offsetParent;
  }
  return new Point(xPosition, yPosition);
};

/**
 * Checks if the point given as a first argument belongs to a polygon defined as
 * a second parameter.
 *
 * @param {Point} point
 *          point which we want to check
 *
 * @param {Point[]} polygon
 *          polygon where we check the point
 */

Functions.prototype.pointInsidePolygon = function (point, polygon) {
  var x = point.x;
  var y = point.y;

  var inside = false;
  for (var i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
    var xi = polygon[i].x, yi = polygon[i].y;
    var xj = polygon[j].x, yj = polygon[j].y;
    var intersect = ((yi > y) !== (yj > y)) && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
    if (intersect) {
      inside = !inside;
    }
  }
  return inside;
};

/**
 * Returns true if parameter is integer, false otherwise.
 *
 * @param n
 *          object to check
 */
Functions.prototype.isInt = function (n) {
  return Number(n) === n && n % 1 === 0;
};

/**
 * Returns true if parameter is a DOM element, false otherwise.
 *
 * @param o
 *          object to check
 */
Functions.prototype.isDomElement = function (o) {
  if (!o) {
    return false;
  }
  return (typeof HTMLElement === "object" ? o instanceof HTMLElement : // DOM2
    o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName === "string");
};

/**
 *
 * @param {LayoutAlias|LayoutReaction} elementOverlay
 *
 * @returns {Promise<string>}
 */

Functions.prototype.overlayToColor = function (elementOverlay) {
  var self = this;
  if (elementOverlay === null || elementOverlay === undefined) {
    return Promise.reject("elementOverlay cannot be null!");
  } else if (elementOverlay.color !== undefined && elementOverlay.color !== null) {
    return Promise.resolve(self.intToColorString(elementOverlay.color.rgb));
  } else {
    var ratio = 0;
    var promiseColors;
    if (elementOverlay.value !== undefined && elementOverlay.value !== null) {
      if (elementOverlay.value < 0) {
        ratio = -elementOverlay.value;
        promiseColors = [ServerConnector.getMinOverlayColorInt(), ServerConnector.getNeutralOverlayColorInt()];
      } else {
        ratio = elementOverlay.value;
        promiseColors = [ServerConnector.getMaxOverlayColorInt(), ServerConnector.getNeutralOverlayColorInt()];
      }
    } else {
      ratio = 1;
      promiseColors = [ServerConnector.getSimpleOverlayColorInt(), ServerConnector.getNeutralOverlayColorInt()];
    }

    return Promise.all(promiseColors).then(function (colors) {
      var maxColor = colors[0];
      var neutralColor = colors[1];

      ratio = 1 - ratio;
      var MAX_RED = 0xFF0000;
      var MAX_GREEN = 0x00FF00;
      var MAX_BLUE = 0x0000FF;

      var red = maxColor & MAX_RED;
      var neutralRed = neutralColor & MAX_RED;

      red = red + (neutralRed - red) * ratio;
      red = parseInt(red);
      red = red & MAX_RED;

      var green = maxColor & MAX_GREEN;
      var neutralGreen = neutralColor & MAX_GREEN;
      green = green + (neutralGreen - green) * ratio;
      green = parseInt(green);
      green = green & MAX_GREEN;

      var blue = maxColor & MAX_BLUE;
      var neutralBlue = neutralColor & MAX_BLUE;
      blue = blue + (neutralBlue - blue) * ratio;
      blue = parseInt(blue);
      blue = blue & MAX_BLUE;

      var color = red | green | blue;
      return self.intToColorString(color);
    });
  }
};

/**
 *
 * @param {HTMLElement} element
 * @param {string} name
 * @returns {HTMLElement}
 */
Functions.prototype.getElementByName = function (element, name) {
  if (element !== undefined) {
    if (element.getAttribute("name") === name) {
      return element;
    }
    var children = element.children;
    for (var i = 0; i < children.length; i++) {
      var child = children[i];
      var res = this.getElementByName(child, name);
      if (res !== undefined) {
        return res;
      }
    }
  }
  return undefined;
};


/**
 *
 * @param {HTMLElement}element
 * @param {string} name class name
 * @returns {HTMLElement|undefined}
 */
Functions.prototype.getElementByClassName = function (element, name) {
  var $ = require('jquery');
  if (name.indexOf(".") !== 0) {
    name = "." + name;
  }
  return $(name, element)[0];
};

/**
 *
 * @param {Object} params
 * @param {string} params.type type of the {HTMLElement} to be created
 * @param {string} [params.inputType] type of the input to be created
 * @param {string} [params.className] css class of the element
 * @param {string} [params.style] css styling
 * @param {string} [params.value]
 * @param {string} [params.name]
 * @param {string} [params.id]
 * @param {string} [params.href]
 * @param {string} [params.src]
 * @param {string} [params.title]
 * @param {number} [params.index]
 * @param {boolean} [params.xss]
 * @param {string|number|boolean} [params.data]
 * @param {function} [params.onclick]
 * @param {function} [params.onchange]
 * @param {string|HTMLElement} [params.content]
 *
 * @returns {HTMLElement}
 */
Functions.prototype.createElement = function (params) {
  var result = document.createElement(params.type);
  if (params.id !== null && params.id !== undefined) {
    result.id = params.id;
  }
  if (params.name !== null && params.name !== undefined) {
    result.setAttribute("name", params.name);
  }
  if (params.className !== null && params.className !== undefined) {
    result.className = params.className;
  }
  if (params.inputType !== null && params.inputType !== undefined) {
    result.type = params.inputType;
  }
  if (params.content !== null && params.content !== undefined) {
    if (this.isDomElement(params.content)) {
      result.appendChild(params.content);
    } else if (params.xss !== false) {
      var content = xss(params.content);
      if (content !== params.content) {
        logger.warn("XSS changed content: " + params.content);
      }
      result.innerHTML = content;
    } else {
      result.innerHTML = params.content;
    }
  }
  if (params.style !== null && params.style !== undefined) {
    result.style.cssText = params.style;
  }
  if (params.onclick !== null && params.onclick !== undefined) {
    result.onclick = params.onclick;
  }
  if (params.onchange !== null && params.onchange !== undefined) {
    result.onchange = params.onchange;
  }
  if (params.href !== null && params.href !== undefined) {
    result.href = params.href;
  }
  if (params.src !== null && params.src !== undefined) {
    result.src = params.src;
  }
  if (params.value !== null && params.value !== undefined) {
    result.value = params.value;
  }
  if (params.title !== null && params.title !== undefined) {
    result.title = params.title;
  }
  if (params.index !== null && params.index !== undefined) {
    result.index = params.index;
  }
  if (params.data !== null && params.data !== undefined) {
    var $ = require('jquery');
    $(result).attr("data", params.data);
  }
  return result;
};

/**
 *
 * @param {number} x
 * @returns {number}
 */
function sqr(x) {
  return x * x;
}

/**
 *
 * @param {Point} v
 * @param {Point} w
 * @returns {number}
 */
function dist2(v, w) {
  return sqr(v.x - w.x) + sqr(v.y - w.y);
}

/**
 *
 * @param {Point} p
 * @param {Point} v
 * @param {Point} w
 * @returns {number}
 */
function distToSegmentSquared(p, v, w) {
  var l2 = dist2(v, w);

  if (l2 === 0) {
    return dist2(p, v);
  }

  var t = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / l2;

  if (t < 0) {
    return dist2(p, v);
  }
  if (t > 1) {
    return dist2(p, w);
  }

  return dist2(p, new Point(v.x + t * (w.x - v.x), v.y + t * (w.y - v.y)));
}

/**
 *
 * @param {Point} p1
 * @param {Point| {start:Point, end:Point}} el2
 * @returns {number}
 */
Functions.prototype.distance = function (p1, el2) {
  if (el2 instanceof Point) {
    var p2 = el2;
    return Math.sqrt((Math.pow(p1.x - p2.x, 2)) + (Math.pow(p1.y - p2.y, 2)));
  } else {
    return Math.sqrt(distToSegmentSquared(p1, el2.start, el2.end));
  }
};

/**
 *
 * @param {HTMLElement} element
 */
Functions.prototype.removeChildren = function (element) {
  while (element.firstChild) {
    element.removeChild(element.firstChild);
  }
};

/**
 * @param {string} url
 *
 * @returns {Promise} resolved after javascript is loaded
 */
Functions.prototype.loadScript = function (url) {
  return new Promise(function (resolve) {
    var scriptExists = false;
    var scripts = document.getElementsByTagName('script');
    for (var i = scripts.length; i--;) {
      if (scripts[i].src === url)
        scriptExists = true;
    }
    if (!scriptExists) {
      var head = document.getElementsByTagName('head')[0];
      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = url;
      script.onload = function () {
        resolve();
      };
      head.appendChild(script);
    } else {
      resolve();
    }
  });
};

Functions.prototype.isString = function (value) {
  return (value instanceof String || typeof value === "string");
};

/**
 *
 * @param {String} selector
 * @return {String}
 */
Functions.prototype.escapeSelector = function (selector) {
  // return $.escapeSelector(selector);
  return "".replace.call(selector,
    /(^[^_a-zA-Z\u00a0-\uffff]|[^-_a-zA-Z0-9\u00a0-\uffff])/g,
    "\\$1");
};

var singleton = new Functions();

module.exports = singleton;
