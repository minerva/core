"use strict";

var log4js = require('log4js');
var inMemoryAppender = require('log4js-in-memory-appender');

var $depth = 16;

log4js.configure({
  appenders: {
    consoleAppender: {
      type: "console",
      layout: {
        type: "pattern",
        pattern: "%[%p {%x{ln}} -%]\t%m",
        tokens: {
          ln: function () {
            // The caller:
            var filePath = (new Error()).stack.split("\n")[$depth].split("\\");
            filePath = filePath[filePath.length - 1].split("/");
            return filePath[filePath.length - 1];
          }
        }
      }
    },
    memoryAppender: {
      type: 'log4js-in-memory-appender',
      maxBufferSize: 100 // Optional default 100
    }
  },
  categories: {
    default: {appenders: ['memoryAppender', 'consoleAppender'], level: 'debug'}
  }
});

var logger = log4js.getLogger();
logger.getEvents = function () {
  return inMemoryAppender.buffer.default;
};
logger.getErrors = function () {
  var result = [];
  var buffer = inMemoryAppender.buffer.default;
  for (var i = 0; i < buffer.length; i++) {
    var message = buffer[i];
    if (message.indexOf("ERROR") !== -1) {
      result.push(message);
    }
  }
  return result;
};
logger.getWarnings = function () {
  var result = [];
  var buffer = inMemoryAppender.buffer.default;
  for (var i = 0; i < buffer.length; i++) {
    var message = buffer[i];
    if (message.indexOf("WARN") !== -1) {
      result.push(message);
    }
  }
  return result;
};
logger.flushBuffer = function () {
  return inMemoryAppender.flush('default');
};

module.exports = logger;
