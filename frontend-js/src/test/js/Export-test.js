"use strict";
require("./mocha-config");

var Export = require('../../main/js/Export');
var ServerConnector = require('./ServerConnector-mock');
var logger = require('./logger');

var chai = require('chai');
var assert = chai.assert;

describe('Export', function () {

  describe('init', function () {
    it('default behaviour', function () {
      var exportObject;
      var project;
      var configuration;

      return ServerConnector.getConfiguration().then(function (result) {
        configuration = result;
        return ServerConnector.getProject();
      }).then(function (result) {
        project = result;
        exportObject = new Export({
          element: testDiv,
          project: project,
          configuration: configuration,
          serverConnector: ServerConnector
        });
        return exportObject.init();
      }).then(function () {
        assert.equal(0, logger.getWarnings().length);
      });
    });
  });

});
