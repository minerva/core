"use strict";

require("../../mocha-config");

var Point = require('../../../../main/js/map/canvas/Point');
var PointData = require('../../../../main/js/map/data/PointData');
var IdentifiedElement = require('../../../../main/js/map/data/IdentifiedElement');
var chai = require('chai');
var assert = chai.assert;

describe('PointData', function() {
  describe("constructor", function() {
    it("from coordinates", function() {
      var point = new Point(2, 3.45);
      var pointData = new PointData(point);
      assert.ok(pointData);
      assert.ok(pointData.getId());
      assert.ok(pointData.getPoint());
      assert.ok(pointData.getPoint() instanceof Point);
    });
    it("from javaObj", function() {

      var pointData = new PointData({
        idObject : "Point2D.Double[2.0, 3.0]"
      });
      assert.equal(pointData.getPoint().x, 2.0);
      assert.equal(pointData.getPoint().y, 3.0);
    });

    it("from IdentifiedObject", function() {

      var pointData = new PointData({
        idObject : "Point2D.Double[2.0, 3.0]"
      }, 102);
      var ie = new IdentifiedElement(pointData);
      var pointData2 = new PointData(ie);
      assert.equal(pointData.getModelId(), pointData2.getModelId());
    });
  });
});
