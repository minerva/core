"use strict";

require("../../mocha-config");

var Comment = require('../../../../main/js/map/data/Comment');
var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('Comment', function () {
  beforeEach(function () {
    logger.flushBuffer();
  });

  it("constructor", function () {
    var comment = new Comment({
      elementId: 1,
      type: "ALIAS",
      modelId: 3,
      icon: "icons/comment.png",
      id: 4,
      pinned: true,
      coord: {
        x: 321.5,
        y: 289.0
      },
      removed: false,
      title: "title fo comment: ",
      content: "content of the comment # "
    });
    assert.ok(comment.isPinned());
    assert.notOk(comment.isRemoved());
  });
});
