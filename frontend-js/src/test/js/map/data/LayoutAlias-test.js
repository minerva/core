"use strict";

require("../../mocha-config.js");

var LayoutAlias = require('../../../../main/js/map/data/LayoutAlias');
var chai = require('chai');
var assert = chai.assert;

var logger = require('../../logger');

describe('LayoutAlias', function () {

  it("constructor", function () {
    var aliasId = 908;
    var val = 0.2;
    var colorVal = {
      a: 23
    };
    var javaObj = {
      idObject: aliasId,
      value: val,
      color: colorVal,
      geneVariations: [{}]
    };
    var alias = new LayoutAlias(javaObj);
    assert.ok(alias);
    assert.equal(aliasId, alias.getId());
    assert.equal(val, alias.getValue());
    assert.equal(colorVal, alias.getColor());
    assert.equal(alias.getGeneVariants().length, 1);
  });

  it("update", function () {
    var aliasId = 908;
    var val = 0.2;
    var colorVal = {
      a: 23
    };
    var javaObj = {
      idObject: aliasId,
      value: val,
      color: colorVal,
      geneVariations: [{}]
    };
    var alias = new LayoutAlias(javaObj);

    var aliasId2 = 908;
    var val2 = 0.2;
    var colorVal2 = {
      a: 24
    };
    var javaObj2 = {
      idObject: aliasId2,
      value: val2,
      color: colorVal2,
      type: LayoutAlias.GENETIC_VARIANT
    };

    var alias2 = new LayoutAlias(javaObj2);
    alias.update(alias2);

    assert.equal(aliasId2, alias2.getId());
    assert.equal(val2, alias2.getValue());
    assert.equal(colorVal2, alias2.getColor());
    assert.equal(alias2.getGeneVariants().length, 0);
  });

  it("invalid update", function () {
    var alias = helper.createLayoutAlias();
    try {
      alias.update("invalid data");
      assert.ok(false);
    } catch (exception) {
      assert.ok(exception.message.indexOf("Unknown parameter type") >= 0);
    }

  });

  it("genetic variant constructor", function () {
    var aliasId = 908;
    var val = 0.2;
    var colorVal = {
      a: 23
    };
    var javaObj = {
      idObject: aliasId,
      value: val,
      color: colorVal,
      type: LayoutAlias.GENETIC_VARIANT,
      geneVariations: {}

    };
    var alias = new LayoutAlias(javaObj);
    assert.equal(alias.getType(), LayoutAlias.GENETIC_VARIANT);
  });

  it("GENERIC constructor", function () {
    var aliasId = 908;
    var val = 0.2;
    var colorVal = {
      a: 23
    };
    var javaObj = {
      idObject: aliasId,
      value: val,
      color: colorVal,
      type: LayoutAlias.GENERIC
    };
    var alias = new LayoutAlias(javaObj);
    assert.equal(alias.getType(), LayoutAlias.GENERIC);
    assert.equal(logger.getWarnings().length, 0);
  });
  it("unknown type constructor", function () {
    var aliasId = 908;
    var val = 0.2;
    var colorVal = {
      a: 23
    };
    var javaObj = {
      idObject: aliasId,
      value: val,
      color: colorVal,
      type: "some strange val"
    };

    try {
      new LayoutAlias(javaObj);
      assert.ok(false);
    } catch (exception) {
      assert.ok(exception.message.indexOf("Unknown type") >= 0);
    }
  });
});
