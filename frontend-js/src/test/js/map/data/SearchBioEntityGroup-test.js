"use strict";

require("../../mocha-config");

var Alias = require('../../../../main/js/map/data/Alias');
var SearchBioEntityGroup = require('../../../../main/js/map/data/SearchBioEntityGroup');


var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('SearchBioEntityGroup', function () {
  beforeEach(function () {
    logger.flushBuffer();
  });

  describe("constructor", function () {
    it("with alias", function () {
      var alias = helper.createAlias();
      var group = new SearchBioEntityGroup(alias);
      assert.ok(group);
      assert.equal(logger.getWarnings().length, 0);
      assert.ok(group.bioEntityMatch(alias));
    });
    it("with reaction", function () {
      var reaction = helper.createReaction();
      var group = new SearchBioEntityGroup(reaction);
      assert.ok(group);
      assert.equal(logger.getWarnings().length, 0);
      assert.ok(group.bioEntityMatch(reaction));
    });
    it("with invalid", function () {
      try {
        new SearchBioEntityGroup({});
        assert.ok(false, "Error expected");
      } catch (e) {
        assert.equal(e.message.indexOf("Error expected"), -1)
      }
    });

  });
  describe("bioEntityComparator", function () {
    it("equal", function () {
      var map = helper.createCustomMap();
      var alias1 = helper.createAlias(map);
      var alias2 = helper.createAlias(map);
      var group = new SearchBioEntityGroup(alias1);
      assert.ok(group.bioEntityComparator(alias1, alias2) === 0);
    });
    it("different name", function () {
      var map = helper.createCustomMap();
      var alias1 = helper.createAlias(map);
      var alias2 = helper.createAlias(map);
      alias2.setName("different name");
      var group = new SearchBioEntityGroup(alias1);
      assert.ok(group.bioEntityComparator(alias1, alias2) !== 0);
    });
    it("different type", function () {
      var map = helper.createCustomMap();
      var alias1 = helper.createAlias(map);
      var alias2 = helper.createAlias(map);
      alias2.setType("different type");
      var group = new SearchBioEntityGroup(alias1);
      assert.ok(group.bioEntityComparator(alias1, alias2) !== 0);
    });
    it("different state", function () {
      var map = helper.createCustomMap();
      var alias1 = helper.createAlias(map);
      var alias2 = helper.createAlias(map);
      var otherData = {structuralState: "different state"};
      alias2.setOther(otherData);
      var group = new SearchBioEntityGroup(alias1);
      assert.ok(group.bioEntityComparator(alias1, alias2) !== 0);
    });
    it("different modifications", function () {
      var map = helper.createCustomMap();
      var alias1 = helper.createAlias(map);
      var alias2 = helper.createAlias(map);
      var otherData = {modifications: [{"name": "S250", "state": "PHOSPHORYLATED"}]};
      alias2.setOther(otherData);
      var group = new SearchBioEntityGroup(alias1);
      assert.ok(group.bioEntityComparator(alias1, alias2) !== 0);
    });
    it("different class", function () {
      var map = helper.createCustomMap();
      var alias = helper.createAlias(map);
      var reaction = helper.createReaction(map);
      var group = new SearchBioEntityGroup(alias);
      assert.ok(group.bioEntityComparator(alias, reaction) !== 0);
    });

    it("different map", function () {
      var map = helper.createCustomMap();
      var alias1 = helper.createAlias(map);
      var alias2 = helper.createAlias(map);
      alias2.setModelId(-1);
      var group = new SearchBioEntityGroup(alias1);
      assert.ok(group.bioEntityComparator(alias1, alias2) !== 0);
    });

    it("different compartment", function () {
      var map = helper.createCustomMap();
      var alias1 = helper.createAlias(map);
      var alias2 = helper.createAlias(map);
      alias2.setCompartmentId(-3);
      var group = new SearchBioEntityGroup(alias1);
      assert.ok(group.bioEntityComparator(alias1, alias2) !== 0);
    });


    it("different reactions", function () {
      var map = helper.createCustomMap();
      var reaction1 = helper.createReaction(map);
      var reaction2 = helper.createReaction(map);
      var group = new SearchBioEntityGroup(reaction1);
      assert.ok(group.bioEntityComparator(reaction1, reaction2) !== 0);
    });
  });

  it("addBioEntity", function () {
    var map = helper.createCustomMap();
    var alias1 = helper.createAlias(map);
    var alias2 = helper.createAlias(map);
    var group = new SearchBioEntityGroup(alias1);
    group.addBioEntity(alias2);
    assert.equal(2, group.getBioEntities().length);
  });

  it("setIcon", function () {
    var map = helper.createCustomMap();
    var alias1 = helper.createAlias(map);
    var group = new SearchBioEntityGroup(alias1);
    group.setIcon("icon.png");
    assert.equal("icon.png", group.getIcon());
  });

});
