"use strict";

var ReferenceGenomeGeneMapping = require('../../../../main/js/map/data/ReferenceGenomeGeneMapping');
var chai = require('chai');
var assert = chai.assert;

describe('ReferenceGenomeGeneMapping', function () {
  it("constructor", function () {
    var data = {
      name: "TYPe",
      localUrl: "http://google.pl/"
    };
    var mapping = new ReferenceGenomeGeneMapping(data);
    assert.equal(mapping.getName(), data.name);
    assert.equal(mapping.getUrl(), data.localUrl);
  });
});
