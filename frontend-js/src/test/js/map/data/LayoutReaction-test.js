"use strict";

var LayoutReaction = require('../../../../main/js/map/data/LayoutReaction');

var chai = require('chai');
var assert = chai.assert;

var logger = require('../../logger');

describe('LayoutReaction', function () {

  it("constructor", function () {
    var reactionId = 908;
    var width = 3.5;
    var reverse = false;
    var colorVal = {
      a: 23
    };
    var javaObj = {
      idObject: reactionId,
      width: width,
      color: colorVal,
      reverse: reverse
    };
    var reaction = new LayoutReaction(javaObj);
    assert.ok(reaction);
    assert.equal(reactionId, reaction.getId());
    assert.equal(width, reaction.getWidth());
    assert.equal(colorVal, reaction.getColor());
    assert.equal(reverse, reaction.getReverse());

    assert.equal(logger.getWarnings().length, 0);
  });
});