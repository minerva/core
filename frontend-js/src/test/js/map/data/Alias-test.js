"use strict";

var Alias = require('../../../../main/js/map/data/Alias');
var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('Alias', function () {
  beforeEach(function () {
    logger.flushBuffer();
  });

  describe("constructor", function () {
    it("with invalid arg", function () {
      try {
        new Alias({});
        assert.ok(null);
      } catch (exception) {
        assert.ok(exception.message.indexOf("ModelId is not defined") >= 0);
      }
    });
    it("simple", function () {
      var javaObject = {
        bounds: {
          x: 190,
          y: 44,
          width: 80,
          height: 40
        },
        modelId: 57,
        idObject: 18554
      };
      var alias = new Alias(javaObject);
      assert.equal(alias.isComplete(), false);
      assert.equal(null, alias.name);
      assert.equal(alias.getX(), 190);
    });
    it("complex", function () {
      var javaObject = {
        notes: "",
        type: "Protein",
        name: "s1",
        synonyms: [],
        formerSymbols: [],
        references: [],
        other: [],
        bounds: {
          x: 59,
          y: 73,
          width: 80,
          height: 40
        },
        modelId: 54,
        idObject: 18552
      };
      var alias = new Alias(javaObject);
      assert.ok(alias.isComplete());
      assert.equal('s1', alias.name);
    });

  });


  describe("update", function () {
    describe("from json", function () {
      it("full update", function () {
        var javaObject = {
          bounds: {
            x: 190,
            y: 44,
            width: 80,
            height: 40
          },
          modelId: 57,
          idObject: 18554
        };
        var alias = new Alias(javaObject);
        var javaObject2 = {
          notes: "",
          type: "Protein",
          name: "s1",
          synonyms: [],
          formerSymbols: [],
          references: [],
          other: [],
          bounds: {
            x: 59,
            y: 73,
            width: 80,
            height: 40
          },
          modelId: 54,
          idObject: 18552
        };
        alias.update(javaObject2);
        assert.ok(alias.isComplete());
        assert.equal('s1', alias.getName());
      });

      it("partial update", function () {
        var javaObject = {
          bounds: {
            x: 190,
            y: 44,
            width: 80,
            height: 40
          },
          modelId: 57,
          idObject: 18554
        };
        var alias = new Alias(javaObject);
        var javaObject2 = {
          notes: "",
          type: "Protein",
          synonyms: [],
          formerSymbols: [],
          references: [],
          other: [],
          bounds: {
            x: 59,
            y: 73,
            width: 80,
            height: 40
          },
          modelId: 54,
          idObject: 18552
        };
        alias.update(javaObject2);
        assert.equal(alias.isComplete(), false);
      });
    });

    it("from Alias", function () {
      var javaObject = {
        bounds: {
          x: 190,
          y: 44,
          width: 80,
          height: 40
        },
        modelId: 57,
        idObject: 18554
      };
      var alias = new Alias(javaObject);
      var alias2 = new Alias(javaObject);
      alias.update(alias2);
      assert.equal(alias.isComplete(), false);
    });
  });

});
