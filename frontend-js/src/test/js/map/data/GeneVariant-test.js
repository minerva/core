"use strict";

var GeneVariant = require('../../../../main/js/map/data/GeneVariant');
var chai = require('chai');
var assert = chai.assert;

describe('GeneVariant', function () {
  it("constructor", function () {
    // noinspection SpellCheckingInspection
    var data = {
      position: 12,
      originalDna: "A",
      modifiedDna: "C",
      referenceGenomeType: "unk",
      referenceGenomeVersion: "v1",
      contig: "1",
      allelFrequency: "0.2",
      variantIdentifier: "id",
      aminoAcidChange: "LRRK2:NM_198578:exon1:c.T45C:p.T15T"
    };
    var variant = new GeneVariant(data);
    assert.equal(variant.getPosition(), data.position);
    assert.equal(variant.getOriginalDna(), data.originalDna);
    assert.equal(variant.getModifiedDna(), data.modifiedDna);
    assert.equal(variant.getReferenceGenomeType(), data.referenceGenomeType);
    assert.equal(variant.getContig(), data.contig);
    assert.equal(variant.getAllelFrequency(), data.allelFrequency);
    assert.equal(variant.getVariantIdentifier(), data.variantIdentifier);
    assert.equal(variant.getReferenceGenomeVersion(), data.referenceGenomeVersion);
    assert.equal(variant.getAminoAcidChange(), data.aminoAcidChange);
  });
});
