"use strict";

require("../../mocha-config");

var IdentifiedElement = require('../../../../main/js/map/data/IdentifiedElement');
var TargettingStructure = require('../../../../main/js/map/data/TargettingStructure');
var ServerConnector = require('../../ServerConnector-mock');

var logger = require('../../logger');

var chai = require('chai');
var assert = chai.assert;

describe('TargettingStructure', function () {
  it("getTargetsForIdentifiedElement", function () {
    helper.setUrl("http://test-page/?id=drug_target_sample");

    var map, searchDb;
    return ServerConnector.getProject("drug_target_sample").then(function (project) {
      map = helper.createCustomMap(project);
      searchDb = helper.createDrugDbOverlay(map);
      return searchDb.searchByQuery("NADH");
    }).then(function (drugs) {
      var drug = drugs[0][0];
      var ie = new IdentifiedElement({
        id: 436152,
        modelId: 20637,
        type: 'ALIAS'
      });
      var targets = drug.getTargetsForIdentifiedElement(ie);
      assert.equal(targets.length, 1);

      ie = new IdentifiedElement({
        id: 111111111111,
        modelId: 20637,
        type: 'ALIAS'
      });
      targets = drug.getTargetsForIdentifiedElement(ie);
      assert.equal(targets.length, 0);
      assert.equal(0, logger.getWarnings().length);
    });
  });
  describe("constructor", function () {
    it("from existing instance", function () {
      helper.setUrl("http://test-page/?id=drug_target_sample");

      var map, searchDb;
      return ServerConnector.getProject("drug_target_sample").then(function (project) {
        map = helper.createCustomMap(project);
        searchDb = helper.createDrugDbOverlay(map);
        return searchDb.searchByQuery("NADH");
      }).then(function (drugs) {
        var drug = drugs[0][0];
        var newDrug = new TargettingStructure(drug);
        assert.equal(newDrug.getId(), drug.getId());
        assert.equal(newDrug.getName(), drug.getName());
        assert.equal(newDrug.getTargets().length, drug.getTargets().length);
      });
    });
  });
});
