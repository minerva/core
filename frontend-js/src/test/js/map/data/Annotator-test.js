"use strict";

var Annotator = require('../../../../main/js/map/data/Annotator');
var AnnotatorParameter = require('../../../../main/js/map/data/AnnotatorParameter');
var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('Annotator', function () {
  beforeEach(function () {
    logger.flushBuffer();
  });

  describe("addParameter", function () {
    it("INPUT field", function () {
      var annotator = new Annotator({
        annotatorClass: "lcsb.mapviewer.annotation.services.annotators.HgncAnnotator",
        order: 0,
        parameters: []
      });

      var annotatorParameter = new AnnotatorParameter({
        type: "INPUT",
        order: 0,
        field: "NAME"
      });
      annotator.addParameter(annotatorParameter);
      assert.equal(1, annotator.getParametersDefinitions().length);
      assert.equal(0, logger.getWarnings().length);
      annotator.addParameter(annotatorParameter);
      assert.equal(1, annotator.getParametersDefinitions().length);
      assert.equal(1, logger.getWarnings().length);
    });
  });

  describe("removeParameter", function () {
    it("INPUT field", function () {
      var annotator = new Annotator({
        annotatorClass: "lcsb.mapviewer.annotation.services.annotators.HgncAnnotator",
        order: 0,
        parameters: [{
          type: "INPUT",
          order: 0,
          field: "NAME"
        }]
      });

      var annotatorParameter = new AnnotatorParameter({
        type: "INPUT",
        order: 0,
        field: "NAME"
      });
      annotator.removeParameter(annotatorParameter);
      assert.equal(0, annotator.getParametersDefinitions().length);
      assert.equal(0, logger.getWarnings().length);
      annotator.removeParameter(annotatorParameter);
      assert.equal(0, annotator.getParametersDefinitions().length);
      assert.equal(1, logger.getWarnings().length);
    });
    it("2 fields", function () {
      var annotator = new Annotator({
        annotatorClass: "lcsb.mapviewer.annotation.services.annotators.HgncAnnotator",
        order: 0,
        parameters: [{
          type: "INPUT",
          order: 0,
          field: "NAME"
        },{
          type: "INPUT",
          order: 0,
          field: "NAME2"
        }]
      });

      var annotatorParameter = new AnnotatorParameter({
        type: "INPUT",
        order: 0,
        field: "NAME"
      });
      annotator.removeParameter(annotatorParameter);
      assert.equal(1, annotator.getParametersDefinitions().length);
      assert.equal(0, logger.getWarnings().length);
    });
  });
});
