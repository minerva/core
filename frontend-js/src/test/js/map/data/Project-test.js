"use strict";
require("../../mocha-config");

var DataOverlay = require('../../../../main/js/map/data/DataOverlay');
var Project = require('../../../../main/js/map/data/Project');
var ServerConnector = require('../../ServerConnector-mock');

var logger = require('../../logger');

var chai = require('chai');
var assert = chai.assert;

describe('Project', function () {
  describe("constructor", function () {
    it("default", function () {
      return ServerConnector.sendGetRequest("testFiles/apiCalls/projects/sample/token=MOCK_TOKEN_ID&").then(function (res) {
        var project = new Project(res);
        assert.ok(project);

        assert.equal(project.getVersion(), "0");
        assert.equal(project.getId(), 14898);
        assert.equal(project.getName(), "UNKNOWN DISEASE MAP");
        assert.equal(project.getProjectId(), "sample");
        assert.deepEqual(project.getOverviewImages(), []);

        assert.equal(logger.getWarnings().length, 0);
      });
    });
    it("from Project obj", function () {
      return ServerConnector.sendGetRequest("testFiles/apiCalls/projects/sample/token=MOCK_TOKEN_ID&").then(function (res) {
        var tmpProject = new Project(res);

        var project = new Project(tmpProject);
        assert.ok(project);

        assert.equal(project.getVersion(), "0");
        assert.equal(project.getId(), 14898);
        assert.equal(project.getName(), "UNKNOWN DISEASE MAP");
        assert.equal(project.getProjectId(), "sample");
        assert.deepEqual(project.getOverviewImages(), []);

        assert.equal(logger.getWarnings().length, 0);
      });
    });
  });

  it("getDataOverlayById", function () {
    var project = helper.createProject();
    var overlay = helper.createOverlay();

    return project.getDataOverlayById(overlay.getId()).then(function (result) {
      assert.equal(null, result);
      project.addDataOverlay(overlay);

      return project.getDataOverlayById(overlay.getId()).then(function (overlayResult) {
        assert.ok(overlayResult);
        assert.ok(overlayResult instanceof DataOverlay);
      });
    });
  });

  describe("getElementsPointingToSubmap", function () {
    it("with no elements pointing", function () {
      helper.setUrl("http://localhost/?id=complex_model_with_submaps");
      return ServerConnector.getProject().then(function (project) {
        return project.getElementsPointingToSubmap(16728).then(function (elements) {
          assert.equal(0, elements.length);
        });
      });
    });
    it("with one element pointing", function () {
      helper.setUrl("http://localhost/?id=complex_model_with_submaps");
      return ServerConnector.getProject().then(function (project) {
        return project.getElementsPointingToSubmap(16731).then(function (elements) {
          assert.equal(1, elements.length);
        });
      });
    });
    it("with recursive elements pointing", function () {
      helper.setUrl("http://localhost/?id=complex_model_with_submaps");
      return ServerConnector.getProject().then(function (project) {
        return project.getElementsPointingToSubmap(16730).then(function (elements) {
          assert.equal(2, elements.length);
        });
      });
    });
  });

  it("getElementsPointingToSubmap", function () {
    var project = new Project("{}");
    var dataOverlay = helper.createOverlay();
    var dataOverlay2 = helper.createOverlay();
    dataOverlay2.setId(dataOverlay.getId());
    project.addOrUpdateDataOverlay(dataOverlay);

    var count = project.getDataOverlays().length;
    project.addOrUpdateDataOverlay(dataOverlay2);
    var count2 = project.getDataOverlays().length;

    assert.equal(count, count2);
  });

});
