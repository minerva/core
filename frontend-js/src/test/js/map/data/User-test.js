"use strict";

require("../../mocha-config");

var User = require('../../../../main/js/map/data/User');
var ServerConnector = require('../../ServerConnector-mock');

var logger = require('../../logger');

var chai = require('chai');
var assert = chai.assert;

describe('User', function () {
  describe("constructor", function () {
    it("empty data", function () {
      var user = new User({});
      assert.ok(user);

      assert.equal(logger.getWarnings().length, 0);
    });
  });

  describe("update", function () {
    it("ldap account available", function () {
      var user = new User({});
      assert.ok(user);
      var user2 = new User({ldapAccountAvailable:true});
      user.update(user2);
      assert.ok(user.isLdapAccountAvailable());
    });
  });


  describe("setPrivilege", function () {
    it("non existing data", function () {
      var user = new User({});
      assert.ok(user);

      return ServerConnector.getConfiguration().then(function (configuration) {
        var privilege = configuration.getPrivilegeTypes()[0];
        user.setPrivilege({privilegeType: privilege.getName()});
        assert.ok(user.hasPrivilege(privilege));
      });
    });

    it("existing data", function () {
      var user = new User({});
      assert.ok(user);

      return ServerConnector.getConfiguration().then(function (configuration) {
        var privilege = configuration.getPrivilegeTypes()[0];
        user.setPrivilege({privilegeType: privilege.getName()});
        user.setPrivilege({privilegeType: privilege.getName()});
        assert.ok(user.hasPrivilege(privilege));
      });
    });

    it("revoked privilege", function () {
      var user = new User({});
      assert.ok(user);

      return ServerConnector.getConfiguration().then(function (configuration) {
        var privilege = configuration.getPrivilegeTypes()[0];
        user.revokePrivilege({privilegeType: privilege.getName()});
        assert.notOk(user.hasPrivilege(privilege));
        user.setPrivilege({privilegeType: privilege.getName()});
        assert.ok(user.hasPrivilege(privilege));
      });
    });
  });
});
