"use strict";

var Mesh = require('../../../../main/js/map/data/Mesh');
var chai = require('chai');
var assert = chai.assert;

describe('Mesh', function () {
  describe("constructor", function () {
    it("from json", function () {
      var mesh = new Mesh({
        "synonyms": ["Parkinson's Disease"],
        "name": "Parkinson Disease",
        "description": "A progressive, degenerative ...",
        "id": "D010300"
      });
      assert.ok(mesh);
      assert.equal("Parkinson's Disease",mesh.getSynonyms()[0]);
      assert.equal("Parkinson Disease",mesh.getName());
      assert.equal("A progressive, degenerative ...",mesh.getDescription());
      assert.equal("D010300",mesh.getId());
    });
  });
});
