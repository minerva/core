"use strict";

require("../../mocha-config");

var Alias = require('../../../../main/js/map/data/Alias');
var Annotation = require('../../../../main/js/map/data/Annotation');
var IdentifiedElement = require('../../../../main/js/map/data/IdentifiedElement');
var MapModel = require('../../../../main/js/map/data/MapModel');
var NetworkError = require('../../../../main/js/NetworkError');
var Point = require('../../../../main/js/map/canvas/Point');
var PointData = require('../../../../main/js/map/data/PointData');
var ServerConnector = require('../../ServerConnector-mock');

var assert = require('chai').assert;

var logger = require('../../logger');

describe('MapModel', function () {
  var emptyData = {
    idObject: 123,
    references: [],
    authors: [],
    modificationDates: []
  };

  it("constructor", function () {
    var model = new MapModel(emptyData);

    assert.equal(123, model.getId());

  });

  it("setReferences", function () {
    var model = new MapModel(emptyData);

    model.setReferences([new Annotation({type: "", resource: ""})]);
    assert.equal(1, model.getReferences().length);

  });

  it("update alias", function () {
    var model = helper.createModel();

    var aliasObj = {
      idObject: 32,
      modelId: model.id,
      references: []
    };

    model.addAlias(aliasObj);

    return model.getAliasById(32).then(function (alias) {

      assert.equal(alias.getName(), undefined);

      var aliasObj2 = {
        idObject: 32,
        modelId: model.id,
        name: "testName",
        type: "Protein",
        references: []
      };

      model.addAlias(aliasObj2);
      return model.getAliasById(32);
    }).then(function (alias) {
      assert.equal(alias.getName(), "testName");
    });
  });

  it("getPointDataByPoint", function () {
    var model = helper.createModel();

    var point = new Point(2, 3.45);
    var point2 = new Point(2, 3.45);

    var data = model.getPointDataByPoint(point);

    var data2 = model.getPointDataByPoint(point2);

    assert.ok(data);
    assert.ok(data2);
    assert.equal(data, data2);
  });

  it("getPointDataBy invalid point", function () {
    var model = helper.createModel();

    var data = model.getPointDataByPoint({});

    assert.equal(data, null);
    assert.equal(logger.getWarnings().length, 1);
  });

  it("getAliasById", function () {
    var model = helper.createModel();

    var alias = helper.createAlias();

    return model.getAliasById(alias.getId()).then(function (result) {
      assert.equal(null, result);
    }, function (exception) {
      assert.ok(exception.message.indexOf("no such file"));
      // check if this is exception about not finding file
      model.addAlias(alias);
      return model.getAliasById(alias.getId());
    }).then(function (result) {
      assert.deepEqual(alias, result);
    });
  });

  describe("getReactionById", function () {
    it("not existing", function () {
      var model = helper.createModel();

      var reaction = helper.createReaction();

      return model.getReactionById(reaction.getId()).then(function (result) {
        assert.equal(null, result);
      }, function (exception) {
        assert.ok(exception instanceof NetworkError);
        // check if this is exception about not finding file
        model.addReaction(reaction);
        return model.getReactionById(reaction.getId());
      }).then(function (result) {
        assert.equal(reaction, result);
      });
    });

    it("check reactants", function () {
      var model;
      return ServerConnector.getProject().then(function (project) {
        model = project.getModels()[0];

        return model.getReactionById(153510, true);
      }).then(function (result) {
        assert.ok(result.getReactants()[0].getAlias() instanceof Alias);
        assert.ok(result.getProducts()[0].getAlias() instanceof Alias);
        assert.ok(result.getModifiers()[0].getAlias() instanceof Alias);

        //let assume we downloaded it partially
        result.setIsComplete(false);
        return model.getReactionById(153510, true);
      }).then(function (result) {
        assert.ok(result.getReactants()[0].getAlias() instanceof Alias);
        assert.ok(result.getProducts()[0].getAlias() instanceof Alias);
        assert.ok(result.getModifiers()[0].getAlias() instanceof Alias);
      });
    });

  });

  it("addReaction 2", function () {
    var model = helper.createModel();

    var reaction = helper.createReaction();
    model.addReaction(reaction);
    assert.equal(logger.getWarnings().length, 0);
    model.addReaction(reaction);
    assert.equal(logger.getWarnings().length, 1);
  });

  it("getMissingElements when everything is up to date", function () {
    var model = helper.createModel();

    return model.getMissingElements({}).then(function (result) {
      assert.equal(result.length, 0);
    });
  });

  it("getReactionByParticipantId", function () {
    var model;
    return ServerConnector.getProject().then(function (project) {

      model = project.getModels()[0];
      var ie = new IdentifiedElement({
        modelId: 15781,
        type: "ALIAS",
        id: 329167
      });

      return model.getByIdentifiedElement(ie);
    }).then(function (element) {

      return model.getReactionsForElement(element);
    }).then(function (reactions) {
      assert.equal(reactions.length, 5);
    });
  });

  describe("getByIdentifiedElement", function () {
    it("by POINT", function () {
      var model = new MapModel();
      var ie = new IdentifiedElement({
        modelId: 15781,
        type: "POINT",
        id: "(1.00,2.00)"
      });
      return model.getByIdentifiedElement(ie).then(function (element) {
        assert.ok(element instanceof PointData);
      });
    });
  });

});
