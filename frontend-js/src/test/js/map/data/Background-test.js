"use strict";

require("../../mocha-config");

var chai = require('chai');
var assert = chai.assert;

var logger = require('../../logger');
var Background = require("../../../../main/js/map/data/Background");

describe('Background', function () {
  describe("constructor", function () {
    it("from json obj", function () {
      var obj = {
        id: 15781,
        name: "test",
        description: "test",
        status: "OK",
        progress: "0.00",
        images: [{model: {id: "15781"}, path: "subDir"}],
        creator: {login: "admin"},
        idObject: 14852
      };
      var data = new Background(obj);
      assert.ok(data);
      assert.equal(data.getImagesDirectory(15781), "subDir");
      assert.equal(data.getImagesDirectory(15782), null);
    });
  });
});
