"use strict";

require("../../mocha-config");

var LayoutAlias = require('../../../../main/js/map/data/LayoutAlias');
var DataOverlay = require('../../../../main/js/map/data/DataOverlay');

var chai = require('chai');
var assert = chai.assert;

var logger = require('../../logger');

describe('DataOverlay', function () {
  describe("constructor", function () {
    it("default", function () {
      var layoutId = 3;
      var name = "nm";
      var overlay = new DataOverlay(layoutId, name);
      assert.equal(overlay.getId(), layoutId);
      assert.equal(overlay.getName(), name);
    });

    it("with content", function () {
      var overlay = new DataOverlay({
        content: "test"
      });
      assert.ok(overlay.getContent());
    });

    it("from json obj", function () {
      var obj = {
        modelId: 15781,
        name: "test",
        description: "test",
        status: "OK",
        progress: "0.00",
        creator: "admin ",
        idObject: 14852
      };
      var data = new DataOverlay(obj);
      assert.ok(data);
      assert.equal(data.getCreator(), "admin ");
    });
  });

  it("updateAlias", function () {
    var layoutId = 3;
    var name = "nm";
    var overlay = new DataOverlay(layoutId, name);

    var aliasId = 4;
    var alias = new LayoutAlias({
      idObject: aliasId
    });

    overlay.addAlias(alias);

    assert.equal(overlay.getAliasesById(aliasId)[0].getValue(), null);

    var val = 5;
    var alias2 = new LayoutAlias({
      idObject: aliasId,
      value: val
    });
    overlay.updateAlias(alias2);

    assert.equal(overlay.getAliasesById(aliasId)[0].getValue(), val);

    assert.equal(logger.getWarnings().length, 0);
  });

  it("getFullAliasesById", function () {
    var layoutId = 18076;
    var name = "nm";
    var overlay = new DataOverlay(layoutId, name);

    var aliasId = 329163;
    var alias = new LayoutAlias({
      idObject: aliasId,
      modelId: 15781,
      uniqueId: aliasId
    });

    overlay.addAlias(alias);

    return overlay.getFullAliasesById(aliasId).then(function (data) {
      assert.equal(data[0].getType(), LayoutAlias.GENERIC);
    });
  });

  it("update invalid alias", function () {
    var layoutId = 3;
    var name = "nm";
    var overlay = new DataOverlay(layoutId, name);

    var aliasId = 4;
    var alias = new LayoutAlias({
      idObject: aliasId
    });

    overlay.updateAlias(alias);

    assert.equal(logger.getWarnings().length, 1);
  });
});
