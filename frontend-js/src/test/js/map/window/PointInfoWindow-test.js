"use strict";
require("../../mocha-config");
var ServerConnector = require('../../ServerConnector-mock');

var Promise = require("bluebird");

var Point = require('../../../../main/js/map/canvas/Point');
var PointData = require('../../../../main/js/map/data/PointData');
var PointInfoWindow = require('../../../../main/js/map/window/PointInfoWindow');

var chai = require('chai');
var assert = chai.assert;

var logger = require('../../logger');

describe('PointInfoWindow', function () {
  it('getPosition', function () {
    var map = helper.createCustomMap();

    var infoWindow = new PointInfoWindow({
      point: new PointData(new Point(1, 2), 3),
      map: map
    });
    assert.ok(infoWindow.getPosition());
  });

});
