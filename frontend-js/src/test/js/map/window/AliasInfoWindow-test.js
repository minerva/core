"use strict";
require("../../mocha-config");
var ServerConnector = require('../../ServerConnector-mock');

var Promise = require("bluebird");

var functions = require('../../../../main/js/Functions');

var Alias = require('../../../../main/js/map/data/Alias');
var ReferenceGenome = require('../../../../main/js/map/data/ReferenceGenome');
var LayoutAlias = require('../../../../main/js/map/data/LayoutAlias');
var Drug = require('../../../../main/js/map/data/Drug');
var GeneVariant = require('../../../../main/js/map/data/GeneVariant');
var AliasInfoWindow = require('../../../../main/js/map/window/AliasInfoWindow');
var IdentifiedElement = require('../../../../main/js/map/data/IdentifiedElement');
var DataOverlay = require('../../../../main/js/map/data/DataOverlay');

var chai = require('chai');
var assert = chai.assert;

var logger = require('../../logger');

describe('AliasInfoWindow', function () {
  describe('constructor', function () {
    it("default", function () {
      var map = helper.createCustomMap();

      var alias = helper.createAlias(map);
      alias.setIsComplete(false);

      var aliasWindow = new AliasInfoWindow({
        alias: alias,
        map: map,
        marker: helper.createMarker({element: alias, map: map})
      });

      assert.equal(alias, aliasWindow.getAlias());
      assert.equal(map, aliasWindow.getCustomMap());
      assert.ok(aliasWindow.getContent().innerHTML.indexOf("loading") >= 0);
      assert.equal(logger.getWarnings().length, 0);
    });
    it("loading data", function () {
      var map = helper.createCustomMap();
      map.getOverlayDataForAlias = function () {
        return Promise.resolve([]);
      };

      var javaObject = {
        bounds: {
          x: 190,
          y: 44,
          width: 80,
          height: 40
        },
        modelId: map.getId(),
        idObject: 30001
      };
      var alias = new Alias(javaObject);
      map.getModel().addAlias(alias);

      var aliasWindow = new AliasInfoWindow({
        alias: alias,
        map: map,
        marker: helper.createMarker({element: alias, map: map})
      });

      assert.equal(alias, aliasWindow.alias);
      assert.equal(map, aliasWindow.customMap);
      assert.ok(aliasWindow.getContent().innerHTML.indexOf("loading") > -1);
    });
  });

  it("createOverlayInfoDiv", function () {
    var map = helper.createCustomMap();

    var oc = helper.createDrugDbOverlay(map);

    var alias = helper.createAlias(map);
    var aliasWindow = new AliasInfoWindow({
      alias: alias,
      map: map,
      marker: helper.createMarker({element: alias, map: map})
    });

    oc.searchNamesByTarget = function () {
      return Promise.resolve(["xField"]);
    };
    oc.getElementsByQueryFromServer = function () {
      return Promise.resolve([new Drug({
        name: "xField",
        references: [],
        targets: []
      })]);
    };
    return aliasWindow.init().then(function () {
      return oc.getDetailDataByIdentifiedElement(new IdentifiedElement(alias), true);
    }).then(function (data) {
      var overlayDiv = aliasWindow.createOverlayInfoDiv(oc, data);
      assert.ok(functions.isDomElement(overlayDiv));
      assert.ok(overlayDiv.innerHTML.indexOf('xField') >= 0);
    });

  });

  it("createDrugOverlayInfoDiv", function () {
    helper.setUrl("http://test-page/?id=drug_target_sample");

    var map, ie, aliasWindow, oc;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);

      oc = helper.createDrugDbOverlay(map);

      ie = new IdentifiedElement({
        id: 436152,
        modelId: map.getId(),
        type: "ALIAS"
      });
      return map.getModel().getByIdentifiedElement(ie, true);
    }).then(function (alias) {
      aliasWindow = new AliasInfoWindow({
        alias: alias,
        map: map,
        marker: helper.createMarker({element: alias, map: map})
      });
      return aliasWindow.init()
    }).then(function () {
      return oc.getDetailDataByIdentifiedElement(ie, true);
    }).then(function (data) {
      var overlayDiv = aliasWindow.createOverlayInfoDiv(oc, data);
      assert.ok(functions.isDomElement(overlayDiv));
      assert.ok(overlayDiv.innerHTML.indexOf('NADH') >= 0);
    });
  });

  it("createChemicalOverlayInfoDiv", function () {
    var map, ie, aliasWindow, oc;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);

      oc = helper.createChemicalDbOverlay(map);

      ie = new IdentifiedElement({
        id: 329170,
        modelId: map.getId(),
        type: "ALIAS"
      });

      return map.getModel().getByIdentifiedElement(ie, true);
    }).then(function (alias) {
      aliasWindow = new AliasInfoWindow({
        alias: alias,
        map: map,
        marker: helper.createMarker({element: alias, map: map})
      });
      return oc.getDetailDataByIdentifiedElement(ie, true);
    }).then(function (data) {
      var overlayDiv = aliasWindow.createOverlayInfoDiv(oc, data);
      assert.ok(functions.isDomElement(overlayDiv));
    });
  });

  describe("createOverlayInfoDiv", function () {
    it("for comment", function () {
      var map = helper.createCustomMap();

      var oc = helper.createCommentDbOverlay(map);

      var alias = helper.createAlias();
      alias.setId(329157);
      alias.setIsComplete(true);
      alias.setModelId(map.getId());
      map.getModel().addAlias(alias);

      var aliasWindow = new AliasInfoWindow({
        alias: alias,
        map: map,
        marker: helper.createMarker({element: alias, map: map})
      });

      return oc.getDetailDataByIdentifiedElement(new IdentifiedElement(alias), true).then(function (data) {
        var comment = helper.createComment(alias);
        comment.setContent("test comment Content");

        data[0] = comment;
        data['__FULL__'] = null;

        var overlayDiv = aliasWindow.createOverlayInfoDiv(oc, data);

        assert.ok(functions.isDomElement(overlayDiv));
        assert.ok(overlayDiv.innerHTML.indexOf(comment.getContent()) >= 0);
        assert.ok(overlayDiv.innerHTML.indexOf(comment.getId()) >= 0);
      });

    });
    it("xss", function () {
      var map = helper.createCustomMap();

      var oc = helper.createCommentDbOverlay(map);

      var alias = helper.createAlias(map);
      alias.setId(329157);
      alias.setIsComplete(true);

      var aliasWindow = new AliasInfoWindow({
        alias: alias,
        map: map,
        marker: helper.createMarker({element: alias, map: map})
      });

      return oc.getDetailDataByIdentifiedElement(new IdentifiedElement(alias), true).then(function (data) {
        var comment = helper.createComment(alias);
        // noinspection HtmlUnknownTarget
        comment.setContent("<img id=\"xss-id\" src=\"invalid/path\" onerror='alert(\"XSS test\")' />");

        data[0] = comment;
        data['__FULL__'] = null;

        var overlayDiv = aliasWindow.createOverlayInfoDiv(oc, data);
        assert.ok(overlayDiv.innerHTML.indexOf("alert") === -1);
      });

    });
  });

  describe("createGeneticsDiv", function () {
    it("on top map", function () {
      var map;
      var overlay;

      var layoutAliases;
      var win;

      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        overlay = new DataOverlay(18077, "xxx");
        return overlay.init();
      }).then(function () {
        return overlay.getFullAliasesById(overlay.getAliases()[0].getId());
      }).then(function (data) {
        layoutAliases = data;
        return map.getModel().getAliasById(layoutAliases[0].getId());
      }).then(function (alias) {
        win = new AliasInfoWindow({
          alias: alias,
          map: map,
          marker: helper.createMarker({element: alias, map: map})
        });
        return win.init();
      }).then(function () {
        // var overlay = helper.createOverlay();
        overlay.addAlias(layoutAliases[0]);
        map._referenceGenome = {
          'UCSC': {
            'eboVir3': new ReferenceGenome({
              type: "TYP",
              version: "v3",
              localUrl: "http://google.pl/",
              sourceUrl: "http://google.pl/",
              organism: {
                "annotatorClassName": "",
                "descriptionByType": "",
                "descriptionByTypeRelation": "",
                "id": 517528,
                "link": "http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=8822",
                "resource": "8822",
                "type": "TAXONOMY"
              }
            })
          }
        };
        return win.createGenomicDiv({overlays: [overlay]});
      }).then(function (div) {
        assert.ok(div);
        assert.ok(div.innerHTML.indexOf("No organism defined for this project, cannot display variant data") === -1);
        win.destroy();
      });

    });

    it("with no genetic data", function () {
      var map;

      var win;
      var aliasId = 329173;

      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        var overlay = new DataOverlay(18077, "xxx");
        return overlay.init();
      }).then(function () {
        return map.getModel().getAliasById(aliasId);
      }).then(function (alias) {
        win = new AliasInfoWindow({
          alias: alias,
          map: map,
          marker: helper.createMarker({element: alias, map: map})
        });
        return win.init();
      }).then(function () {
        var overlays = [helper.createOverlay()];
        return win.createGenomicDiv({overlays: overlays});
      }).then(function (div) {
        assert.ok(div);
        assert.ok(div.innerHTML.indexOf("No organism defined for this project, cannot display variant data") === -1);
        win.destroy();
      });

    });

    it("for unknown organism", function () {
      var map;
      var overlay;

      var layoutAliases;
      var win;

      return ServerConnector.getProject().then(function (project) {
        project.setOrganism({
          type: "TAXONOMY",
          resource: "123456"
        });
        map = helper.createCustomMap(project);
        overlay = new DataOverlay(18077, "xxx");
        return overlay.init();
      }).then(function () {
        return overlay.getFullAliasesById(overlay.getAliases()[0].getId());
      }).then(function (data) {
        layoutAliases = data;
        return map.getModel().getAliasById(layoutAliases[0].getId());
      }).then(function (alias) {
        win = new AliasInfoWindow({
          alias: alias,
          map: map,
          marker: helper.createMarker({element: alias, map: map})
        });
        return win.init();
      }).then(function () {
        overlay.addAlias(layoutAliases[0]);
        return win.createGenomicDiv({overlays: [overlay]});
      }).then(function (div) {
        assert.ok(div);
        assert.ok(div.innerHTML.indexOf("No organism defined for this project, cannot display variant data") >= -1);
        win.destroy();
      });

    });
  });

  it("createWaitingContentDiv", function () {
    var map = helper.createCustomMap();
    var alias = helper.createAlias(map);
    alias.setIsComplete(true);

    var aliasWindow = new AliasInfoWindow({
      alias: alias,
      map: map,
      marker: helper.createMarker({element: alias, map: map})
    });

    assert.ok(functions.isDomElement(aliasWindow.createWaitingContentDiv()));
  });

  it("getPrintableOverlayName", function () {
    var map = helper.createCustomMap();
    var alias = helper.createAlias(map);
    alias.setIsComplete(true);

    var aliasWindow = new AliasInfoWindow({
      alias: alias,
      map: map,
      marker: helper.createMarker({element: alias, map: map})
    });

    var overlay = helper.createOverlay();
    overlay.setPublicOverlay(true);
    overlay.setOrder(137778);
    assert.equal(-1, aliasWindow._getPrintableOverlayName(overlay).indexOf(overlay.getOrder() + ""));
  });

  describe("createChartDiv ", function () {
    it("on submap map", function () {
      helper.setUrl("http://test-page/?id=complex_model_with_submaps");

      var map, submap, overlay, win, aliasOnSubmap, alias;

      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        submap = map.getSubmapById(16731);
        overlay = new DataOverlay(18077, "xxx");
        overlay.setInitialized(true);
        return map.openSubmap(submap.getId());
      }).then(function () {
        return map.getModel().getAliasById(345339);
      }).then(function (data) {
        alias = data;
        return submap.getModel().getAliasById(345337);
      }).then(function (data) {
        aliasOnSubmap = data;
        win = new AliasInfoWindow({
          alias: alias,
          map: map,
          marker: helper.createMarker({element: alias, map: map})
        });
        return win.init();
      }).then(function () {
        var layoutAlias = helper.createLayoutAlias(aliasOnSubmap);
        layoutAlias.setType(LayoutAlias.GENERIC);
        var overlay = helper.createOverlay();
        overlay.addAlias(layoutAlias);
        return win.createChartDiv({overlays: [overlay]});
      }).then(function (div) {
        assert.ok(div);
        win.destroy();
        return map.destroy();
      });

    });

    it("default ", function () {
      var map, ie, aliasWindow;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        ie = new IdentifiedElement({
          id: 329170,
          modelId: map.getId(),
          type: "ALIAS"
        });

        return map.getModel().getByIdentifiedElement(ie, true);
      }).then(function (alias) {
        aliasWindow = new AliasInfoWindow({
          alias: alias,
          map: map,
          marker: helper.createMarker({element: alias, map: map})
        });
        var overlay = helper.createOverlay();
        overlay.addAlias(helper.createLayoutAlias(alias, LayoutAlias.GENERIC));
        var overlays = [overlay, helper.createOverlay()];

        return aliasWindow.createChartDiv({overlays: overlays});
      }).then(function (div) {
        assert.ok(div);
      });
    });

    it("overlay order", function () {
      var map = helper.createCustomMap();
      var alias = helper.createAlias(map);
      var aliasWindow = new AliasInfoWindow({
        alias: alias,
        map: map,
        marker: helper.createMarker({element: alias, map: map})
      });
      var overlay1 = helper.createOverlay();
      overlay1.setOrder(2);
      var overlay2 = helper.createOverlay();
      overlay2.setPublicOverlay(true);
      overlay2.setOrder(10);
      var overlay3 = helper.createOverlay();
      overlay3.setPublicOverlay(true);
      overlay3.setOrder(9);
      var overlay4 = helper.createOverlay();
      overlay4.setOrder(1);
      var overlays = [overlay1, overlay2, overlay3, overlay4];

      return aliasWindow.createChartDiv({overlays: overlays}).then(function (htmlDiv) {
        var overlay1Position = htmlDiv.innerHTML.indexOf(overlay1.getName());
        var overlay2Position = htmlDiv.innerHTML.indexOf(overlay2.getName());
        var overlay3Position = htmlDiv.innerHTML.indexOf(overlay3.getName());
        var overlay4Position = htmlDiv.innerHTML.indexOf(overlay4.getName());
        assert.ok(overlay3Position < overlay2Position);
        assert.ok(overlay2Position < overlay4Position);
        assert.ok(overlay4Position < overlay1Position);
      });
    });
  });

  it("createVcfString ", function () {
    var map = helper.createCustomMap();
    var alias = helper.createAlias(map);
    var aliasWindow = new AliasInfoWindow({
      alias: alias,
      map: map,
      marker: helper.createMarker({element: alias, map: map})
    });

    var nullVal = null;

    var variant = new GeneVariant({
      contig: "11",
      modifiedDna: "T",
      originalDna: "C",
      position: 233067,
      referenceGenomeType: "UCSC",
      referenceGenomeVersion: "hg19",
      allelFrequency: nullVal,
      variantIdentifier: nullVal
    });
    var variant2 = new GeneVariant({
      contig: "11",
      modifiedDna: "T",
      originalDna: "C",
      position: 233068,
      referenceGenomeType: "UCSC",
      referenceGenomeVersion: "hg19"
    });
    var str = aliasWindow.createVcfString([variant, variant2]);
    assert.ok(str.indexOf("null") === -1, "null shouldn't appear in vcf format");
  });
  describe("adjustPileupRangeVisibility ", function () {
    it("single variant ", function () {
      var map = helper.createCustomMap();
      var alias = helper.createAlias(map);
      var aliasWindow = new AliasInfoWindow({
        alias: alias,
        map: map,
        marker: helper.createMarker({element: alias, map: map})
      });

      var position = 100;

      var range = {
        start: position,
        stop: position,
        contig: "xx"
      };

      aliasWindow.adjustPileupRangeVisibility(range);
      assert.ok(range.stop - range.start >= 50);
      assert.ok(position > range.start);
      assert.ok(position < range.stop);
    });

    it("single variant at the beginning", function () {
      var map = helper.createCustomMap();
      var alias = helper.createAlias(map);
      var aliasWindow = new AliasInfoWindow({
        alias: alias,
        map: map,
        marker: helper.createMarker({element: alias, map: map})
      });

      var position = 10;

      var range = {
        start: position,
        stop: position,
        contig: "xx"
      };

      aliasWindow.adjustPileupRangeVisibility(range);
      assert.ok(range.stop - range.start >= 50);
      assert.ok(position > range.start);
      assert.ok(position < range.stop);
      assert.ok(range.start >= 0);
    });

    it("two variants", function () {
      var map = helper.createCustomMap();
      var alias = helper.createAlias(map);
      var aliasWindow = new AliasInfoWindow({
        alias: alias,
        map: map,
        marker: helper.createMarker({element: alias, map: map})
      });

      var startPosition = 400;
      var stopPosition = 500;

      var range = {
        start: startPosition,
        stop: stopPosition,
        contig: "xx"
      };

      aliasWindow.adjustPileupRangeVisibility(range);
      assert.ok(range.stop - range.start >= 50);
      assert.ok(startPosition > range.start);
      assert.ok(stopPosition < range.stop);
      assert.ok((range.stop - range.start) / (stopPosition - startPosition) >= 1.05)
    });
  });

});
