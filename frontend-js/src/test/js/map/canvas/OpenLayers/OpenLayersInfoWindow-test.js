"use strict";
require("../../../mocha-config");

// noinspection JSUnusedLocalSymbols
var logger = require('../../../logger');

var OpenLayerCanvas = require('../../../../../main/js/map/canvas/OpenLayers/OpenLayerCanvas');
var Point = require('../../../../../main/js/map/canvas/Point');

var chai = require('chai');
var assert = chai.assert;

describe('OpenLayerInfoWindow', function () {
  var testOptions = {
    center: new Point(0, 0),
    tileSize: 256,
    width: 300,
    height: 600,
    zoom: 3,
    minZoom: 2,
    maxZoom: 10,
    backgroundOverlays: [{
      id: 1,
      name: "overlay",
      directory: "overlay_dir"
    }]
  };
  it("hide when marker is hidden", function () {
    var canvas = new OpenLayerCanvas(testDiv, testOptions);
    var marker = canvas.createMarker({icon: "test.png", position: new Point(0, 0)});
    var infoWindow = canvas.createInfoWindow({marker: marker, id: "1", position: new Point(0, 0)});
    infoWindow.open();
    assert.ok(infoWindow.isOpened());
    return marker.hide().then(function () {
      assert.notOk(infoWindow.isOpened());
    });
  });
});
