"use strict";
require("../../../mocha-config");

// noinspection JSUnusedLocalSymbols
var logger = require('../../../logger');

var OpenLayerCanvas = require('../../../../../main/js/map/canvas/OpenLayers/OpenLayerCanvas');
var OpenLayerPolyline = require('../../../../../main/js/map/canvas/OpenLayers/OpenLayerPolyline');
var Bounds = require('../../../../../main/js/map/canvas/Bounds');
var Point = require('../../../../../main/js/map/canvas/Point');

var chai = require('chai');
var assert = chai.assert;

describe('OpenLayerPolyline', function () {
  var testOptions = {
    center: new Point(0, 0),
    tileSize: 256,
    width: 300,
    height: 600,
    zoom: 3,
    minZoom: 2,
    maxZoom: 10,
    backgroundOverlays: [{
      id: 1,
      name: "overlay",
      directory: "overlay_dir"
    }]
  };
  it("getBounds", function () {
    var canvas = new OpenLayerCanvas(testDiv, testOptions);
    var path = [new Point(0, 0), new Point(10, 5)];

    var polyline = canvas.createPolyline({
      strokeWeight: 1,
      strokeColor: "#00ff00",
      path: path
    });
    polyline.show();
    assert.ok(polyline.getBounds());
  });
});
