"use strict";
require("../../../mocha-config");

// noinspection JSUnusedLocalSymbols
var logger = require('../../../logger');

var OpenLayerCanvas = require('../../../../../main/js/map/canvas/OpenLayers/OpenLayerCanvas');
var Bounds = require('../../../../../main/js/map/canvas/Bounds');
var Point = require('../../../../../main/js/map/canvas/Point');

var chai = require('chai');
var assert = chai.assert;

describe('OpenLayerCanvas', function () {
  var testOptions = {
    center: new Point(0, 0),
    tileSize: 256,
    width: 300,
    height: 600,
    zoom: 3,
    minZoom: 2,
    maxZoom: 10,
    backgroundOverlays: [{
      id: 1,
      name: "overlay",
      directory: "overlay_dir"
    }]
  };
  it("constructor", function () {
    var canvas = new OpenLayerCanvas(testDiv, testOptions);
    assert.ok(canvas.pixelOrigin_);
    assert.ok(canvas.pixelsPerLonDegree_);
    assert.ok(canvas.pixelsPerLonRadian_);
    assert.ok(canvas.zoomFactor);

  });

  it("Point - Projection conversion", function () {
    var canvas = new OpenLayerCanvas(testDiv, testOptions);

    var x = 199;
    var y = 110;
    var point = new Point(x, y);

    var coordinates = canvas.fromPointToProjection(point);

    var point2 = canvas.fromProjectionToPoint(coordinates);

    assert.closeTo(point2.x, point.x, 0.5, "X coordinate is invalid after transformation");
    assert.closeTo(point2.y, point.y, 0.5, "Y coordinate is invalid after transformation");

  });

  it("setCenter", function () {
    var canvas = new OpenLayerCanvas(testDiv, testOptions);

    var point = new Point(199, 110);
    canvas.setCenter(point);
    var point2 = canvas.getCenter();

    assert.closeTo(point2.x, point.x, 0.5, "X coordinate is not correct");
    assert.closeTo(point2.y, point.y, 0.5, "Y coordinate is not correct");

  });

  it("setZoom", function () {
    var canvas = new OpenLayerCanvas(testDiv, testOptions);

    canvas.setZoom(5);
    var zoom = canvas.getZoom();

    assert.equal(zoom, 5, "zoom is incorrect");

  });

  it("getBounds", function () {
    var canvas = new OpenLayerCanvas(testDiv, testOptions);
    //set size of the map in DOM
    canvas.getOpenLayersMap().setSize([800, 600]);

    var bounds = canvas.getBounds(5);
    assert.ok(bounds instanceof Bounds);

  });

  describe("fitBounds", function () {
    it("part of map", function () {
      var canvas = new OpenLayerCanvas(testDiv, testOptions);
      //set size of the map in DOM
      canvas.getOpenLayersMap().setSize([800, 600]);

      var p1 = new Point(10, 10);
      var p2 = new Point(20, 10);
      var bounds = new Bounds(p1, p2);

      canvas.fitBounds(bounds);

      var mapBounds = canvas.getBounds();

      assert.ok(mapBounds.contains(p1));
      assert.ok(mapBounds.contains(p2));
      assert.notOk(mapBounds.contains(new Point(0, 0)) && mapBounds.contains(new Point(canvas.getWidth(), canvas.getHeight())));

    });
    it("full map", function () {
      var canvas = new OpenLayerCanvas(testDiv, testOptions);
      //set size of the map in DOM
      canvas.getOpenLayersMap().setSize([800, 600]);

      var p1 = new Point(0, 0);
      var p2 = new Point(canvas.getWidth(), canvas.getHeight());
      var bounds = new Bounds(p1, p2);

      canvas.fitBounds(bounds);

      var mapBounds = canvas.getBounds();

      assert.ok(mapBounds.contains(p1));
      assert.ok(mapBounds.contains(p2));

    });
  });

  it("featureToString", function () {
    var canvas = new OpenLayerCanvas(testDiv, testOptions);
    //set size of the map in DOM
    canvas.getOpenLayersMap().setSize([800, 600]);

    var feature = new ol.Feature({
      geometry: new ol.geom.Polygon([[
        canvas.fromPointToProjection(new Point(0, 10)),
        canvas.fromPointToProjection(new Point(10, 10)),
        canvas.fromPointToProjection(new Point(10, 0))
      ]])
    });

    var polygon = canvas.featureToString(feature);
    assert.ok(polygon);
  });

});
