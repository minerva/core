"use strict";
require("../../../mocha-config");

// noinspection JSUnusedLocalSymbols
var logger = require('../../../logger');

var OpenLayerCanvas = require('../../../../../main/js/map/canvas/OpenLayers/OpenLayerCanvas');
var OpenLayerRectangle = require('../../../../../main/js/map/canvas/OpenLayers/OpenLayerRectangle');
var Bounds = require('../../../../../main/js/map/canvas/Bounds');
var Point = require('../../../../../main/js/map/canvas/Point');

var chai = require('chai');
var assert = chai.assert;

describe('OpenLayerRectangle', function () {
  var testOptions = {
    center: new Point(0, 0),
    tileSize: 256,
    width: 300,
    height: 600,
    zoom: 3,
    minZoom: 2,
    maxZoom: 10,
    backgroundOverlays: [{
      id: 1,
      name: "overlay",
      directory: "overlay_dir"
    }]
  };
  it("isShown", function () {
    var canvas = new OpenLayerCanvas(testDiv, testOptions);
    var bounds = new Bounds(new Point(0,0), new Point(10,10));
    var rectangle = canvas.createRectangle({
      fillOpacity: 1,
      strokeWeight: 1,
      fillColor: "#00ff00",
      bounds: bounds
    });
    rectangle.show();
    assert.ok(rectangle.isShown());
    rectangle.hide();
    assert.notOk(rectangle.isShown());
  });
});
