"use strict";
require("../../../mocha-config");

// noinspection JSUnusedLocalSymbols
var logger = require('../../../logger');

var OpenLayerCanvas = require('../../../../../main/js/map/canvas/OpenLayers/OpenLayerCanvas');
var OpenLayerMarker = require('../../../../../main/js/map/canvas/OpenLayers/OpenLayerMarker');
var Bounds = require('../../../../../main/js/map/canvas/Bounds');
var Point = require('../../../../../main/js/map/canvas/Point');

var chai = require('chai');
var assert = chai.assert;

describe('OpenLayerMarker', function () {
  var testOptions = {
    center: new Point(0, 0),
    tileSize: 256,
    width: 300,
    height: 600,
    zoom: 3,
    minZoom: 2,
    maxZoom: 10,
    backgroundOverlays: [{
      id: 1,
      name: "overlay",
      directory: "overlay_dir"
    }]
  };
  it("isShown", function () {
    var canvas = new OpenLayerCanvas(testDiv, testOptions);
    var marker = canvas.createMarker({icon:"test.png", position:new Point(0,0)});
    assert.ok(marker.isShown());
    marker.hide();
    assert.notOk(marker.isShown());
  });
});
