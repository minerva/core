"use strict";
require("../../mocha-config");

var MapCanvas = require('../../../../main/js/map/canvas/MapCanvas');
var Point = require('../../../../main/js/map/canvas/Point');

var chai = require('chai');
var assert = chai.assert;

describe('MapCanvas', function () {
  it("test existence of abstract methods", function () {

    var canvas = new MapCanvas(testDiv, {
      tileSize: 256,
      minZoom: 2,
      height: 600,
      width: 800
    });
    assert.ok(canvas.createMarker);
    assert.ok(canvas.createInfoWindow);
    assert.ok(canvas.createRectangle);
    assert.ok(canvas.createPolyline);
    assert.ok(canvas.addLeftBottomControl);
    assert.ok(canvas.addRightBottomControl);
    assert.ok(canvas.fitBounds);
    assert.ok(canvas.setCenter);
    assert.ok(canvas.getCenter);
    assert.ok(canvas.getZoom);
    assert.ok(canvas.getBackgroundId);
    assert.ok(canvas.getBounds);
    assert.ok(canvas.triggerListeners);
  });

  describe("pointsToString", function () {
    it("proper values", function () {

      var canvas = new MapCanvas(testDiv, {
        tileSize: 256,
        minZoom: 2,
        height: 600,
        width: 800
      });
      var result = canvas.pointsToString([new Point(17, 10), new Point(12, 15)]);
      assert.ok(result.indexOf("15") >= 0);
      assert.ok(result.indexOf("17") >= 0);
    });

    it("exceeded values", function () {

      var canvas = new MapCanvas(testDiv, {
        tileSize: 256,
        minZoom: 2,
        height: 600,
        width: 800
      });
      var result = canvas.pointsToString([new Point(2000, 10), new Point(12, 15)]);
      assert.ok(result.indexOf("2000") === -1);
    });
  });

});
