"use strict";
require("../mocha-config");
var $ = require('jquery');

var logger = require('../logger');

var AliasMarker = require('../../../main/js/map/marker/AliasMarker');
var IdentifiedElement = require('../../../main/js/map/data/IdentifiedElement');
var ServerConnector = require('../ServerConnector-mock');
var Submap = require('../../../main/js/map/Submap');

var chai = require('chai');
var assert = chai.assert;

describe('Submap', function () {
  it("simple constructor", function () {
    var map = helper.createCustomMap();

    var model = helper.createModel();

    var submap = new Submap(map, model);
    assert.ok(submap);
    assert.equal(logger.getWarnings().length, 0);
    assert.equal(logger.getErrors().length, 0);
  });

  describe("open", function () {
    it("default", function () {
      var map = helper.createCustomMap();

      var model = helper.createModel();

      var submap = new Submap(map, model);

      $(testDiv).dialog({
        autoOpen: false
      });

      submap.open(testDiv);

      $(testDiv).dialog("destroy");
      assert.ok(submap);
      assert.equal(logger.getWarnings().length, 0);
      assert.equal(logger.getErrors().length, 0);
    });

    it("with different background", function () {
      var map, submap;
      var projectId = "complex_model_with_submaps";
      helper.setUrl("http://test-page/?id=" + projectId);
      return ServerConnector.getProject(projectId).then(function (project) {
        map = helper.createCustomMap(project);
        submap = map.getSubmapById(16729);
        return map.openBackground(14960);
      }).then(function () {
        return submap.open(testDiv);
      }).then(function () {
        assert.equal(map.getMapCanvas().getBackgroundId(), submap.getMapCanvas().getBackgroundId(), "Background on top map different from submap");
      }).finally(function () {
        return submap.destroy();
      });
    });
  });

  it("getTopMap", function () {
    var map = helper.createCustomMap();

    var model = helper.createModel();

    var submap = new Submap(map, model);

    assert.ok(submap.getTopMap());
  });

  it("create marker for submap", function () {
    var map;
    var submap;
    var marker;
    var projectId = "complex_model_with_submaps";
    helper.setUrl("http://test-page/?id=" + projectId);
    return ServerConnector.getProject(projectId).then(function (project) {
      map = helper.createCustomMap(project);
      submap = map.getSubmapById(16729);
      return submap.open(testDiv);
    }).then(function () {
      return submap.getModel().getAliasById(345330);
    }).then(function (alias) {
      marker = new AliasMarker({
        element: new IdentifiedElement(alias),
        map: submap
      });
      return marker.init();
    }).then(function () {
      assert.ok(marker.getAliasData());
    }).finally(function () {
      return submap.destroy();
    });
  });
});
