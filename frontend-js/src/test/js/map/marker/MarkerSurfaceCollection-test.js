"use strict";

require("../../mocha-config.js");

var ServerConnector = require('../../ServerConnector-mock');

/* exported logger */

// noinspection JSUnusedLocalSymbols
var logger = require('../../logger');

var MarkerSurfaceCollection = require('../../../../main/js/map/marker/MarkerSurfaceCollection');
var IdentifiedElement = require('../../../../main/js/map/data/IdentifiedElement');
var chai = require('chai');
var assert = chai.assert;

describe('MarkerSurfaceCollection', function () {

  describe("addMarker", function () {
    it("update marker", function () {
      var map, marker1, marker2, collection;
      var element = new IdentifiedElement({
        id: 329159,
        modelId: 15781,
        type: "ALIAS"
      });
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        collection = map.getMarkerSurfaceCollection();
        return collection.addMarker({element: element, icons: ["empty.png"]});
      }).then(function (result) {
        marker1 = result;
        element.setIcon("another.png");
        return collection.addMarker({element: element, icons: ["empty2.png"]});
      }).then(function (result) {
        marker2 = result;
        assert.equal(marker1, marker2);
      });
    });

    it("reaction marker", function () {
      var map, overlay, collection;
      var element = new IdentifiedElement({
        id: 153508,
        modelId: 15781,
        type: "REACTION"
      });
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        overlay = helper.createDbOverlay(map);
        collection = map.getMarkerSurfaceCollection();
        return collection.addMarker({element: element, icons: ["empty.png"]});
      }).then(function (result) {
        assert.ok(result.getReactionData());
      });
    });

    it("click on marker", function () {
      var map, marker, collection;
      var element = new IdentifiedElement({
        id: 329159,
        modelId: 15781,
        type: "ALIAS"
      });
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        collection = map.getMarkerSurfaceCollection();
        return collection.addMarker({element: element, icons: ["empty.png"]});
      }).then(function (result) {
        marker = result;
        return marker.onClickHandler();
      }).finally(function () {
        return map.destroy();
      });
    });

    it("click on marker on submap", function () {
      helper.setUrl("http://test-page/?id=complex_model_with_submaps");
      var submap, marker, collection;
      var element = new IdentifiedElement({
        id: 345325,
        modelId: 16729,
        type: "ALIAS"
      });
      return ServerConnector.getProject().then(function (project) {
        submap = helper.createCustomMap(project).getSubmapById(16729);
        return submap.open(testDiv);
      }).then(function () {
        collection = submap.getMarkerSurfaceCollection();
        return collection.addMarker({element: element, icons: ["empty.png"]});
      }).then(function (result) {
        marker = result;
        return marker.onClickHandler();
      }).finally(function () {
        return submap.destroy();
      });
    });

    it("init after submap is opened", function () {
      helper.setUrl("http://test-page/?id=complex_model_with_submaps");
      var map, marker, collection;
      var element = new IdentifiedElement({
        id: 345325,
        modelId: 16729,
        type: "ALIAS"
      });
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        collection = map.getSubmapById(16729).getMarkerSurfaceCollection();
        return collection.addMarker({element: element, icons: ["empty.png"]});
      }).then(function (result) {
        marker = result;
        assert.notOk(marker.isInitialized());
        return map.openSubmap(16729);
      }).then(function () {
        return collection.addMarker({element: element, icons: ["empty.png"]});
      }).then(function () {
        assert.ok(marker.isInitialized());
      }).finally(function () {
        return map.destroy();
      });
    });

  });

  describe("createSurfaceForDbOverlay", function () {
    it("click on element surface", function () {
      var map, collection, overlay;
      var element = new IdentifiedElement({
        id: 329159,
        modelId: 15781,
        type: "ALIAS"
      });
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        overlay = helper.createSearchDbOverlay(map);
        collection = map.getMarkerSurfaceCollection();
        return collection.createSurfaceForDbOverlay(element, overlay);
      }).then(function (surface) {
        return surface.onClickHandler();
      });
    });

    it("click on reaction surface", function () {
      var map, collection, overlay;
      var element = new IdentifiedElement({
        id: 153508,
        modelId: 15781,
        type: "REACTION"
      });
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        overlay = helper.createSearchDbOverlay(map);
        collection = map.getMarkerSurfaceCollection();
        return collection.createSurfaceForDbOverlay(element, overlay);
      }).then(function (surface) {
        return surface.onClickHandler();
      }).finally(function () {
        return map.destroy();
      });
    });

    it("click on alias surface on submap", function () {
      var map;
      var submap;
      var collection, overlay;
      var projectId = "complex_model_with_submaps";
      helper.setUrl("http://test-page/?id=" + projectId);
      return ServerConnector.getProject(projectId).then(function (project) {
        map = helper.createCustomMap(project);
        submap = map.getSubmapById(16729);
        return submap.open(testDiv);
      }).then(function () {

        var element = new IdentifiedElement({
          modelId: submap.getId(),
          id: 345325,
          type: "ALIAS"
        });

        overlay = helper.createSearchDbOverlay(map);
        collection = submap.getMarkerSurfaceCollection();
        return collection.createSurfaceForDbOverlay(element, overlay);

      }).then(function (surface) {
        return surface.onClickHandler();
      }).finally(function () {
        return submap.destroy();
      });
    });


    it("update surface", function () {
      var map, collection, marker1, marker2, overlay;
      var element = new IdentifiedElement({
        id: 329159,
        modelId: 15781,
        type: "ALIAS"
      });
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        overlay = helper.createDbOverlay(map);
        collection = map.getMarkerSurfaceCollection();
        return collection.createSurfaceForDbOverlay(element, overlay);
      }).then(function (result) {
        marker1 = result;
        element.setColor("#990099");
        return collection.createSurfaceForDbOverlay(element, overlay);
      }).then(function (result) {
        marker2 = result;
        assert.equal(marker1, marker2);
      });
    });

  });

});
