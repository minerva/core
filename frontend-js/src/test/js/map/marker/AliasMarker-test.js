"use strict";

require("../../mocha-config.js");

var ServerConnector = require('../../ServerConnector-mock');

var logger = require('../../logger');

var AliasMarker = require('../../../../main/js/map/marker/AliasMarker');
var IdentifiedElement = require('../../../../main/js/map/data/IdentifiedElement');
var chai = require('chai');
var assert = chai.assert;

describe('AliasMarker', function () {

  it("Constructor", function () {
    var map, alias, marker;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);
      return map.getModel().getAliasById(329171);
    }).then(function (result) {
      alias = result;

      var identifiedElement = new IdentifiedElement(alias);
      identifiedElement.setIcon("empty.png");

      marker = new AliasMarker({
        element: identifiedElement,
        map: map
      });

      assert.equal(alias.getId(), marker.getId());
      assert.equal(map, marker.getCustomMap());
      assert.equal("empty.png", marker.getIcon());
      return marker.init();
    }).then(function () {
      assert.equal(alias, marker.getAliasData());
      assert.ok(marker._marker);
      assert.ok(marker.getBounds());
      assert.equal(0, logger.getWarnings().length);
    });
  });

  it("getIcon", function () {
    var map = helper.createCustomMap();
    var element = helper.createAlias(map);
    var marker = new AliasMarker({
      element: new IdentifiedElement(element),
      map: map
    });
    var icon = "test.png";
    return marker.init().then(function () {
      return marker.addIcon("test.png");
    }).then(function () {
      assert.equal(icon, marker.getIcon());
      return marker.addIcon("test.png");
    }).then(function () {
      assert.equal(icon, marker.getIcon());
      return marker.removeIcon("test.png");
    }).then(function () {
      assert.equal(icon, marker.getIcon());
      return marker.removeIcon("test.png");
    }).then(function () {
      assert.equal(undefined, marker.getIcon());
    });
  });

});
