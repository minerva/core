"use strict";

var Promise = require("bluebird");
require("../mocha-config");

var AliasMarker = require('../../../main/js/map/marker/AliasMarker');
var AbstractCustomMap = require('../../../main/js/map/AbstractCustomMap');
var IdentifiedElement = require('../../../main/js/map/data/IdentifiedElement');
var Point = require('../../../main/js/map/canvas/Point');

var ServerConnector = require('../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;

var logger = require("../logger");

describe('AbstractCustomMap', function () {
  it("Constructor", function () {
    var model = helper.createModel();
    var options = helper.createCustomMapOptions();

    var mockObject = new AbstractCustomMap(model, options);

    assert.ok(mockObject.getId());
    assert.ok(mockObject.getModel());
    assert.ok(mockObject.getSelectedLayoutOverlays());

    assert.equal(mockObject.getAliasInfoWindowById(-1), null);
    assert.equal(mockObject.getPointInfoWindowById("-1"), null);
    assert.equal(mockObject.getReactionInfoWindowById(-1), null);

    assert.equal(logger.getWarnings().length, 0);
  });

  describe("createMapOptions", function () {
    it("default", function () {
      var model = helper.createModel();
      var options = helper.createCustomMapOptions();

      var mockObject = new AbstractCustomMap(model, options);

      assert.ok(mockObject.createMapOptions());
    });
    it("check params", function () {
      var model = helper.createModel();
      var options = helper.createCustomMapOptions();

      var mockObject = new AbstractCustomMap(model, options);

      var mapOptions = mockObject.createMapOptions();
      assert.ok(mapOptions.center instanceof Point);
      assert.ok(mapOptions.zoom);
    });

    it("backgroundOverlays", function () {
      return ServerConnector.getProject().then(function (project) {
        var model = project.getModels()[0];

        var options = helper.createCustomMapOptions(project);

        var mockObject = new AbstractCustomMap(model, options);

        var mapOptions = mockObject.createMapOptions();
        assert.ok(project.getBackgrounds().length === mapOptions.backgroundOverlays.length);
      });
    });
  });

  it("registerMapClickEvents", function () {
    var mockObject = helper.createAbstractCustomMap();

    // mock function to not put error log on console
    mockObject.getTopMap = function () {
      return null;
    };

    mockObject.registerMapClickEvents();
  });

  it("_openInfoWindowForReaction", function () {
    var mockObject = helper.createAbstractCustomMap();

    var reaction = helper.createReaction(mockObject, true);

    // mock the behaviour of the map
    mockObject.getTopMap = function () {
      return helper.createCustomMap();
    };
    mockObject.getOverlayDataForReaction = function () {
      return Promise.resolve([]);
    };

    mockObject.getModel().addReaction(reaction);

    assert.equal(null, mockObject.getReactionInfoWindowById(reaction.getId()));

    return mockObject._openInfoWindowForReaction(reaction).then(function () {
      assert.ok(mockObject.getReactionInfoWindowById(reaction.getId()));
      return mockObject.getReactionInfoWindowById(reaction.getId()).destroy();
    });

  });

  describe("_openInfoWindowForAlias", function () {
    it("default", function () {
      var map = helper.createCustomMap();
      var layout = helper.createOverlay();
      var alias = helper.createAlias();
      alias.setModelId(map.getId());
      var layoutAlias = helper.createLayoutAlias();
      layoutAlias.setId(alias.getId());

      layout.addAlias(layoutAlias);

      // create layout
      map.getProject().addDataOverlay(layout, false);
      map.getModel().addAlias(alias);
      return map.init().then(function () {
        var marker = map.getMapCanvas().createMarker({position: new Point(0, 0), icon: "empty.png"});

        return map._openInfoWindowForAlias(alias, marker);
      }).then(function () {
        assert.ok(map.getAliasInfoWindowById(alias.getId()));
        assert.ok(map.getAliasInfoWindowById(alias.getId()).isOpened());
      }).finally(function () {
        return map.destroy();
      });
    });

    it("for incomplete alias", function () {
      var map, alias;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);

        return map.getModel().getAliasById(329171, false);
      }).then(function (result) {
        var marker = map.getMapCanvas().createMarker({position: new Point(0, 0), icon: "empty.png"});
        alias = result;
        assert.notOk(alias.isComplete());
        return map._openInfoWindowForAlias(alias, marker);
      }).then(function () {
        assert.ok(map.getAliasInfoWindowById(alias.getId()));
        assert.ok(map.getAliasInfoWindowById(alias.getId()).isOpened());
      }).finally(function () {
        return map.destroy();
      });
    });
  });


  it("getDebug", function () {
    var map = helper.createAbstractCustomMap();
    map.setDebug(true);
    assert.ok(map.isDebug());
  });

  describe("fitBounds", function () {
    it("alias", function () {
      var map = helper.createCustomMap();
      var alias = helper.createAlias(map);
      var center = map.getCenter();
      return map.fitBounds([alias]).then(function () {
        var center2 = map.getCenter();
        assert.ok(center.x !== center2.x || center.y !== center2.y);
      });
    });
    it("reaction", function () {
      var map = helper.createCustomMap();
      var reaction = helper.createReaction(map);
      var center = map.getCenter();
      return map.fitBounds([reaction]).then(function () {
        var center2 = map.getCenter();
        assert.ok(center.x !== center2.x || center.y !== center2.y);
      })
    });
  });

});
