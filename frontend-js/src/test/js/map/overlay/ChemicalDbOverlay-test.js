"use strict";

require("../../mocha-config");

var logger = require('../../logger');

var AbstractDbOverlay = require('../../../../main/js/map/overlay/AbstractDbOverlay');
var IdentifiedElement = require('../../../../main/js/map/data/IdentifiedElement');
var ChemicalDbOverlay = require('../../../../main/js/map/overlay/ChemicalDbOverlay');
var ServerConnector = require('../../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;

describe('ChemicalDbOverlay', function () {

  it("constructor 1", function () {
    var map = helper.createCustomMap();
    var oc = new ChemicalDbOverlay({
      map: map,
      name: 'chemical'
    });
    assert.ok(oc);
    assert.equal(oc.getName(), 'chemical');

    assert.equal(logger.getWarnings.length, 0);
  });

  it("searchByQuery", function () {
    var map = helper.createCustomMap();
    map.getProject().setDisease({type: "xt", resource: "mock-data"});
    map.getModel().setId(15781);
    var searchDb = helper.createChemicalDbOverlay(map);
    return searchDb.searchByQuery("rotenone").then(function (chemicals) {
      assert.equal(chemicals.length, 1);
      assert.equal(searchDb.getQueries().length, 1);
      return searchDb.getElementsByQuery(searchDb.getQueries()[0]);
    }).then(function (elements) {
      //these are targets
      assert.ok(elements.length > 0);
      var chemical = elements.element;
      assert.equal(chemical.getName(), "Rotenone");
      return searchDb.getIdentifiedElements();
    }).then(function (elements) {
      assert.equal(elements.length, 0);
    });
  });

  it("searchNamesByTarget", function () {
    var map = helper.createCustomMap();
    map.getModel().setId(15781);
    var searchDb = helper.createChemicalDbOverlay(map);

    var target = new IdentifiedElement({
      type: "ALIAS",
      id: 329170,
      modelId: 15781
    });

    return searchDb.searchNamesByTarget(target).then(function (chemicalNames) {
      assert.equal(chemicalNames.length, 0);
    });
  });

  it("searchByEncodedQuery", function () {
    return ServerConnector.getProject().then(function (project) {
      var map = helper.createCustomMap(project);
      var searchDb = helper.createChemicalDbOverlay(map);
      var queryType = AbstractDbOverlay.QueryType.SEARCH_BY_TARGET;

      var target = new IdentifiedElement({
        type: "ALIAS",
        id: 329170,
        modelId: 15781
      });
      var query = searchDb.encodeQuery(queryType, target);
      return searchDb.searchByEncodedQuery(query);
    }).then(function (result) {
      assert.equal(result.length, 0);
    });
  });


});
