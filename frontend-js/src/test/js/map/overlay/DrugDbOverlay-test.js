"use strict";

var logger = require('../../logger');
require("../../mocha-config");

var IdentifiedElement = require('../../../../main/js/map/data/IdentifiedElement');
var DrugDbOverlay = require('../../../../main/js/map/overlay/DrugDbOverlay');
var Point = require('../../../../main/js/map/canvas/Point');
var ServerConnector = require('../../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;

describe('DrugDbOverlay', function () {
  it("constructor 1", function () {
    var map = helper.createCustomMap();
    var oc = new DrugDbOverlay({
      map: map,
      name: 'drug'
    });
    assert.ok(oc);
    assert.equal(oc.getName(), 'drug');

    assert.equal(logger.getWarnings.length, 0);
  });

  describe("searchByQuery", function () {
    it("simple", function () {
      helper.setUrl("http://test-page/?id=drug_target_sample");

      var map, searchDb;
      return ServerConnector.getProject("drug_target_sample").then(function (project) {
        map = helper.createCustomMap(project);
        searchDb = helper.createDrugDbOverlay(map);
        return searchDb.searchByQuery("NADH");
      }).then(function (drugs) {
        assert.equal(drugs.length, 1);
        assert.equal(searchDb.getQueries().length, 1);
        return searchDb.getElementsByQuery(searchDb.getQueries()[0]);
      }).then(function (elements) {
        assert.ok(elements.length > 1);
        return searchDb.getIdentifiedElements();
      }).then(function (elements) {
        assert.equal(elements.length, 1);
      });
    });
  });

  it("searchNamesByTarget", function () {
    return ServerConnector.getProject().then(function (project) {
      var map = helper.createCustomMap(project);
      var searchDb = helper.createDrugDbOverlay(map);

      var target = new IdentifiedElement({
        type: "ALIAS",
        id: 329170,
        modelId: 15781
      });
      return searchDb.searchNamesByTarget(target);
    }).then(function (drugNames) {
      assert.equal(drugNames.length, 1);
      assert.equal(drugNames[0], "NADH");
    });
  });

});
