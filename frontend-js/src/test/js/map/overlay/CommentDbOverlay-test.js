"use strict";

var logger = require('../../logger');
require("../../mocha-config");

var CommentDbOverlay = require('../../../../main/js/map/overlay/CommentDbOverlay');
var IdentifiedElement = require('../../../../main/js/map/data/IdentifiedElement');

var chai = require('chai');
var assert = chai.assert;

describe('CommentDbOverlay', function () {

  it("constructor", function () {
    var map = helper.createCustomMap();
    var oc = new CommentDbOverlay({
      map: map,
      name: 'test name'
    });
    assert.ok(oc);
    assert.equal(oc.getName(), 'test name');

    assert.equal(logger.getWarnings.length, 0);
  });

  describe("getDetailDataByIdentifiedElement ", function () {
    it("get by point", function () {
      var map = helper.createCustomMap();
      var oc = new CommentDbOverlay({
        map: map,
        name: 'test name'
      });

      return oc.getDetailDataByIdentifiedElement(new IdentifiedElement({
        id: "(1.00,200.00)",
        type: "POINT",
        modelId: 15781
      })).then(function (data) {
        assert.ok(data.length > 0);
      });
    });
  });

});
