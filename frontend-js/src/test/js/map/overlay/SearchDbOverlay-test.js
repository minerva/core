"use strict";

var logger = require('../../logger');
require("../../mocha-config");

var IdentifiedElement = require('../../../../main/js/map/data/IdentifiedElement');
var SearchDbOverlay = require('../../../../main/js/map/overlay/SearchDbOverlay');
var AbstractDbOverlay = require('../../../../main/js/map/overlay/AbstractDbOverlay');
var Point = require('../../../../main/js/map/canvas/Point');
var ServerConnector = require('../../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;
var Promise = require('bluebird');

describe('SearchDbOverlay', function () {
  it("constructor", function () {
    var map = helper.createCustomMap();
    var oc = new SearchDbOverlay({
      map: map,
      name: 'search'
    });
    assert.ok(oc);
    assert.equal(oc.getName(), 'search');

    assert.equal(logger.getWarnings.length, 0);
  });

  it("searchByCoordinates with too far alias as result", function () {
    return ServerConnector.getProject().then(function (project) {
      var map = helper.createCustomMap(project);
      var searchDb = helper.createSearchDbOverlay(map);

      var searchParams = {
        modelId: map.getModel().getId(),
        coordinates: new Point(207.73, 479.18),
        zoom: 4
      };
      return searchDb.searchByCoordinates(searchParams);
    }).then(function (result) {
      assert.equal(result.length, 0);
    });
  });

  describe("searchByQuery", function () {
    it("with perfectMatch", function () {
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);
        map.getModel().setId(15781);
        var searchDb = helper.createSearchDbOverlay(map);

        return searchDb.searchByQuery("s1", true);
      }).then(function (result) {
        assert.equal(result.length, 1);
        assert.equal(result[0].length, 1);
      });
    });
    it("check state when network problems", function () {
      var problematicQuery = "sxx1";
      var searchDb;
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);
        searchDb = helper.createSearchDbOverlay(map);

        return searchDb.searchByQuery(problematicQuery);
      }).then(null, function () {
        var queries = searchDb.getQueries();
        var promises = [];
        for (var i = 0; i < queries.length; i++) {
          promises.push(searchDb.getElementsByQuery(queries[i]));
        }
        return Promise.all(promises);
      });
    });
    it("on submap", function () {
      helper.setUrl("http://localhost/?id=complex_model_with_submaps");
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);
        var searchDb = helper.createSearchDbOverlay(map);
        return searchDb.searchByQuery("s1", true);
      }).then(function (result) {
        assert.equal(result.length, 1);
        assert.equal(result[0].length, 3);
      });
    });

  });

  describe("searchByEncodedQuery", function () {
    it("SEARCH_BY_COORDINATES", function () {
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);
        var searchDb = helper.createSearchDbOverlay(map);
        var point = new Point(316.05, 253.61);
        var queryType = AbstractDbOverlay.QueryType.SEARCH_BY_COORDINATES;
        var query = searchDb.encodeQuery(queryType, map.getModel().getId(), point, 2);
        return searchDb.searchByEncodedQuery(query);
      }).then(function (result) {
        assert.ok(result.length > 0);
      });
    });

    it("SEARCH_BY_TARGET", function () {
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);
        var searchDb = helper.createSearchDbOverlay(map);
        var queryType = AbstractDbOverlay.QueryType.SEARCH_BY_TARGET;

        var target = new IdentifiedElement({
          type: "ALIAS",
          id: 329170,
          modelId: 15781
        });
        var query = searchDb.encodeQuery(queryType, target);
        return searchDb.searchByEncodedQuery(query);
      }).then(function (result) {
        assert.equal(result.length, 1);
      });
    });
  });

  describe("searchByCoordinates", function () {
    it("with too far reaction as result", function () {
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);
        var searchDb = helper.createSearchDbOverlay(map);

        var searchParams = {
          modelId: map.getModel().getId(),
          coordinates: new Point(553.10, 479.18),
          zoom: 4
        };
        return searchDb.searchByCoordinates(searchParams);
      }).then(function (result) {
        assert.equal(result.length, 0);
      });

    });

    it("on empty map", function () {
      helper.setUrl("http://test-page/?id=empty");
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);
        var searchDb = helper.createSearchDbOverlay(map);

        var searchParams = {
          modelId: map.getModel().getId(),
          coordinates: new Point(553.10, 479.18),
          zoom: 4
        };
        return searchDb.searchByCoordinates(searchParams);
      }).then(function (result) {
        assert.equal(result.length, 0);
      });

    });

    it("on compartment border", function () {
      helper.setUrl("http://test-page/?id=complex_model_with_submaps");
      var map;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        return map.openBackground(14961);
      }).then(function () {

        var searchDb = helper.createSearchDbOverlay(map);

        var searchParams = {
          modelId: map.getModel().getId(),
          coordinates: new Point(104.36, 182.81),
          zoom: 4
        };
        return searchDb.searchByCoordinates(searchParams);
      }).then(function (result) {
        assert.equal(result.length, 1);
      });

    });


    it("on hidden nested object", function () {
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);
        var searchDb = helper.createSearchDbOverlay(map);

        var searchParams = {
          modelId: map.getModel().getId(),
          coordinates: new Point(316.05, 253.61),
          zoom: 2
        };
        return searchDb.searchByCoordinates(searchParams);
      }).then(function (result) {
        // id of the parent
        assert.equal(result[0].getId(), 329158);
      });

    });

    it("on nested object", function () {
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);
        var searchDb = helper.createSearchDbOverlay(map);

        var searchParams = {
          modelId: map.getModel().getId(),
          coordinates: new Point(316.05, 253.61),
          zoom: 10
        };
        return searchDb.searchByCoordinates(searchParams);
      }).then(function (result) {
        assert.equal(result[0].getId(), 329159);
      });
    });
  });

  describe("getDetailDataByIdentifiedElement", function () {
    it("get by alias", function () {
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);
        var searchDb = helper.createSearchDbOverlay(map);

        var searchParams = new IdentifiedElement({
          type: "ALIAS",
          id: 329177,
          modelId: project.getModels()[0].getId()
        });
        return searchDb.getDetailDataByIdentifiedElement(searchParams);
      }).then(function (result) {
        assert.equal(result.getId(), 329177);
      });

    });
    it("get by point", function () {
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);
        var searchDb = helper.createSearchDbOverlay(map);

        var searchParams = new IdentifiedElement({
          type: "POINT",
          id: "2,3",
          modelId: project.getModels()[0].getId()
        });
        return searchDb.getDetailDataByIdentifiedElement(searchParams);
      }).then(function (result) {
        assert.equal(result, null);
      });
    });
  });

  it("searchByTarget", function () {
    var target = new IdentifiedElement({
      type: "ALIAS",
      id: 329170,
      modelId: 15781
    });

    var map;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);
      var searchDb = helper.createSearchDbOverlay(map);

      return searchDb.searchByTarget(target);
    }).then(function (result) {
      assert.ok(map.getModel().isAvailable(target));
    });
  });


})
;
