"use strict";

require("../../mocha-config");

var AbstractDbOverlay = require('../../../../main/js/map/overlay/AbstractDbOverlay');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('AbstractDbOverlay', function () {
  var mapMock = {
    registerDbOverlay: function () {
    }
  };
  describe("constructor", function () {
    it("default", function () {
      var oc = new AbstractDbOverlay({
        map: mapMock,
        name: 'test name'
      });
      assert.ok(oc);
      assert.equal(oc.getName(), 'test name');
      assert.equal(logger.getWarnings().length, 0);
    });
    it("allowSearchById", function () {
      var oc = new AbstractDbOverlay({
        map: mapMock,
        name: 'test name',
        allowSearchById: true
      });
      assert.ok(oc);
      assert.ok(oc.allowSearchById());
    });
    it("allowGeneralSearch", function () {
      var oc = new AbstractDbOverlay({
        map: mapMock,
        name: 'test name',
        allowSearchById: false,
        allowGeneralSearch: true
      });
      assert.ok(oc);
      assert.ok(oc.allowGeneralSearch());
    });
  });


  describe("splitQuery", function () {
    it("nothing to split", function () {
      var map = helper.createCustomMap();

      var oc = helper.createSearchDbOverlay(map);

      var result = oc.splitQuery("test q");
      assert.equal(result[0], "test q");
    });
    it("split by comma", function () {
      var map = helper.createCustomMap();

      var oc = helper.createSearchDbOverlay(map);

      var result = oc.splitQuery("test,q");
      assert.equal(result[0], "test");
      assert.equal(result[1], "q");
    });

    it("split by semicolon", function () {
      var map = helper.createCustomMap();

      var oc = helper.createSearchDbOverlay(map);

      var result = oc.splitQuery("test;q");
      assert.equal(result[0], "test");
      assert.equal(result[1], "q");
    });
    it("split by semicolon and comma", function () {
      var map = helper.createCustomMap();

      var oc = helper.createSearchDbOverlay(map);

      var result = oc.splitQuery("test;q,bla");
      assert.equal(result[0], "test");
      assert.equal(result[1], "q,bla");
      assert.equal(result.length, 2);
    });
    it("split by semicolon, comma and add full text search", function () {
      var map = helper.createCustomMap();

      var oc = helper.createSearchDbOverlay(map);

      var result = oc.splitQuery("test;q,bla", true);
      assert.equal(result[0], "test");
      assert.equal(result[1], "q,bla");
      assert.equal(result[2], "test;q,bla");
    });
  });


});
