"use strict";

var Promise = require("bluebird");

require("../mocha-config.js");
var $ = require('jquery');

var AliasMarker = require('../../../main/js/map/marker/AliasMarker');
var AliasSurface = require('../../../main/js/map/surface/AliasSurface');
var Comment = require('../../../main/js/map/data/Comment');
var CustomMap = require('../../../main/js/map/CustomMap');
var IdentifiedElement = require('../../../main/js/map/data/IdentifiedElement');
var MapContextMenu = require('../../../main/js/gui/MapContextMenu');
var MolArt = require('../../../main/js/map/structure/MolArt');
var Point = require('../../../main/js/map/canvas/Point');
var PointData = require('../../../main/js/map/data/PointData');
var PointMarker = require('../../../main/js/map/marker/PointMarker');
var ReactionMarker = require('../../../main/js/map/marker/ReactionMarker');
var ReactionSurface = require('../../../main/js/map/surface/ReactionSurface');

var ServerConnector = require('./../ServerConnector-mock');
var ValidationError = require('../../../main/js/ValidationError');

var logger = require('./../logger');

var chai = require('chai');
var assert = chai.assert;

describe('CustomMap', function () {
  describe("constructor", function () {
    it("default", function () {
      var options = helper.createCustomMapOptions();
      var map = new CustomMap(options);
      assert.ok(map);
    });

    it("with submaps", function () {
      var options = helper.createCustomMapOptions();

      options.getProject().addModel(helper.createModel());

      var map = new CustomMap(options);
      assert.ok(map);
    });

    it("with session data pointing to not existing overlay", function () {
      var options = helper.createCustomMapOptions();

      ServerConnector.getSessionData(options.getProject()).setSelectedBackgroundOverlay("-1");

      var map = new CustomMap(options);
      assert.ok(map);
    });
  });

  it("getSubmapById", function () {
    var map = helper.createCustomMap();
    assert.ok(map.getSubmapById(map.getId()));
  });

  it("getSubmapById (invalid)", function () {
    var map = helper.createCustomMap();
    assert.equal(map.getSubmapById(-1), null);
  });

  it("getSubmapById (string id)", function () {
    var map = helper.createCustomMap();
    assert.ok(map.getSubmapById(map.getId() + ""));
  });

  describe("openBackground", function () {
    it("existing", function () {
      var map = helper.createCustomMap();
      var background = helper.createBackground(map.getModel());
      map.getProject().addOrUpdateBackgrounds([background]);

      return map.openBackground(background.getId()).then(function () {
        var overlays = ServerConnector.getSessionData(map.getProject()).getVisibleOverlays();
        assert.equal(0, overlays.length);
      });
    });

  });

  describe("openDataOverlay", function () {
    it("for not existing id", function () {
      var map = helper.createCustomMap();
      return map.openDataOverlay(-1).then(function () {
        assert.ok(false);
      }).catch(function (exception) {
        assert.ok(exception.message.indexOf("You have no privileges") >= 0);
      });
    });

    it("for int id", function () {
      var options = helper.createCustomMapOptions();
      var layout = options.getProject().getDataOverlays()[0];
      var map = new CustomMap(options);
      map.openDataOverlay(layout.getId());
      assert.equal(logger.getErrors().length, 0);
    });

    it("for overlay object", function () {
      var options = helper.createCustomMapOptions();
      var layout = options.getProject().getDataOverlays()[0];
      var map = new CustomMap(options);
      map.openDataOverlay(layout);
      assert.equal(logger.getErrors().length, 0);
    });

    it("for string id", function () {
      var options = helper.createCustomMapOptions();
      var layout = options.getProject().getDataOverlays()[0];
      var map = new CustomMap(options);
      map.openDataOverlay(layout.getId() + "");
      assert.equal(logger.getErrors().length, 0);
    });

    it("with non background overlay", function () {
      var project = helper.createProject();
      var overlay = helper.createOverlay();
      overlay.setInitialized(true);
      project.addDataOverlay(overlay);

      var emptyBackground = helper.createBackground();
      emptyBackground.setName("Empty");
      project.addOrUpdateBackgrounds([emptyBackground]);

      var map = helper.createCustomMap(project);


      return map.openDataOverlay(overlay.getId()).then(function () {
        var backgroundId = ServerConnector.getSessionData(project).getSelectedBackgroundOverlay();
        assert.equal(backgroundId, emptyBackground.getId());
      });
    });

    it("check backgroundChangeListener", function () {
      var project = helper.createProject();
      var overlay = helper.createOverlay();
      overlay.setInitialized(true);
      project.addDataOverlay(overlay);

      var overlay2 = helper.createOverlay();
      overlay2.setInitialized(true);
      project.addDataOverlay(overlay2);

      var emptyBackground = helper.createBackground();
      emptyBackground.setName("Empty");
      project.addOrUpdateBackgrounds([emptyBackground]);

      var map = helper.createCustomMap(project);

      var counter = 0;
      map.addListener("onBackgroundOverlayChange", function () {
        counter++;
      });

      return map.openDataOverlay(overlay.getId()).then(function () {
        assert.equal(1, counter, "listener wasn't fired");
        return map.openDataOverlay(overlay2.getId());
      }).then(function () {
        assert.equal(1, counter, "listener shouldn't be fired again because nothing changed");
      });
    });

    it("simple", function () {
      var map = helper.createCustomMap();
      var alias = helper.createAlias(map);
      map.getModel().addAlias(alias);

      var reaction = helper.createReaction(map);
      map.getModel().addReaction(reaction);

      var layout = helper.createOverlay();
      layout.setInitialized(true);
      var layoutAlias = helper.createLayoutAlias(alias);
      layout.addAlias(layoutAlias);

      var layoutReaction = helper.createLayoutReaction(reaction);
      layout.addReaction(layoutReaction);

      map.getProject().addDataOverlay(layout);

      return map.openDataOverlay(layout.getId()).then(function () {
        return map._showSelectedDataOverlay(layout.getId(), 0, 1);
      }).then(function () {
        var vLayouts = ServerConnector.getSessionData(map.getProject()).getVisibleOverlays();
        assert.equal(1, vLayouts.length);
        assert.equal(layout.getId(), vLayouts[0]);

        assert.equal(2, map.selectedLayoutOverlays[layout.getId()].length);
        var surface1 = map.selectedLayoutOverlays[layout.getId()][0];
        var surface2 = map.selectedLayoutOverlays[layout.getId()][1];
        assert.ok(surface1 instanceof AliasSurface || surface2 instanceof AliasSurface);
        assert.ok(surface1 instanceof ReactionSurface || surface2 instanceof ReactionSurface);

        // now hide the layout
        return map._hideSelectedLayout(layout.getId());
      }).then(function () {
        assert.ok(map.selectedLayoutOverlays[layout.getId()]);
        assert.equal(0, map.selectedLayoutOverlays[layout.getId()].length);
      });

    });

    it("with submaps", function () {
      var projectId = "complex_model_with_submaps";
      helper.setUrl("http://test-page/?id=" + projectId);
      var customMap;
      var emptySubmodelId = 16730;
      var filledSubmodelId = 16731;
      var overlayId = 18083;

      return ServerConnector.getProject(projectId).then(function (project) {
        var options = helper.createCustomMapOptions(project);
        customMap = new CustomMap(options);

        return customMap.openSubmap(emptySubmodelId);
      }).then(function () {
        return customMap.openSubmap(filledSubmodelId);
      }).then(function () {

        var emptySubmap = customMap.getSubmapById(emptySubmodelId);
        var filledSubmap = customMap.getSubmapById(filledSubmodelId);

        return customMap.openDataOverlay(overlayId).then(function () {

          assert.equal(2, customMap.selectedLayoutOverlays[overlayId].length);
          assert.equal(1, filledSubmap.selectedLayoutOverlays[overlayId].length);
          assert.equal(0, emptySubmap.selectedLayoutOverlays[overlayId].length);

          // now hide the layout

          return customMap.hideSelectedLayout(overlayId);
        }).then(function () {
          assert.equal(0, customMap.selectedLayoutOverlays[overlayId].length);
          assert.equal(0, filledSubmap.selectedLayoutOverlays[overlayId].length);
          assert.equal(0, emptySubmap.selectedLayoutOverlays[overlayId].length);
        });

      }).then(function () {
        return customMap.destroy();
      });
    });

    it("with submap double opened", function () {
      var projectId = "complex_model_with_submaps";
      helper.setUrl("http://test-page/?id=" + projectId);
      var customMap, filledSubmap;
      var filledSubmodelId = 16731;
      var overlayId = 18083;

      return ServerConnector.getProject(projectId).then(function (project) {
        customMap = helper.createCustomMap(project);
        return customMap.openSubmap(filledSubmodelId);
      }).then(function () {
        filledSubmap = customMap.getSubmapById(filledSubmodelId);
        return customMap.openDataOverlay(overlayId);
      }).then(function () {
        assert.equal(1, filledSubmap.selectedLayoutOverlays[overlayId].length);
        return customMap.openSubmap(filledSubmodelId);
      }).then(function () {
        //after opening submap nothing should change
        assert.equal(1, filledSubmap.selectedLayoutOverlays[overlayId].length);
      }).finally(function () {
        return customMap.destroy();
      });
    });

    it("check if info window content changed", function () {
      var project = helper.createProject();
      var layout = helper.createOverlay();
      layout.setInitialized(true);
      project.addDataOverlay(layout);

      var map = helper.createCustomMap(project);

      var alias = helper.createAlias(map);
      var ie = helper.createIdentifiedElement(alias);
      var marker = map.getMapCanvas().createMarker({icon: "empty.png", position: new Point(0, 0)});
      var infoWindow;
      return map.openInfoWindowForIdentifiedElement(ie, marker).then(function (result) {
        infoWindow = result;
        assert.notOk(infoWindow.getContent().innerHTML.indexOf(layout.getName()) >= 0);
        return map.openDataOverlay(layout.getId());
      }).then(function () {
        assert.ok(infoWindow.getContent().innerHTML.indexOf(layout.getName()) >= 0);
        assert.equal(logger.getErrors().length, 0);
      }).finally(function () {
        return map.destroy();
      });
    });
  });

  describe("hideDataOverlay", function () {
    it("default", function () {
      var map = helper.createCustomMap();
      var layout = helper.createOverlay();
      layout.setInitialized(true);
      map.getProject().addDataOverlay(layout);

      return map.openDataOverlay(layout.getId()).then(function () {
        return map.hideDataOverlay(layout.getId());
      }).then(function () {
        var vLayouts = ServerConnector.getSessionData(map.getProject()).getVisibleOverlays();
        assert.equal(0, vLayouts.length);
      });
    });
    it("check if info window content changed", function () {
      var project = helper.createProject();
      var layout = helper.createOverlay();
      layout.setInitialized(true);
      project.addDataOverlay(layout);

      var map = helper.createCustomMap(project);

      var alias = helper.createAlias(map);
      var ie = helper.createIdentifiedElement(alias);
      var marker = map.getMapCanvas().createMarker({icon: "empty.png", position: new Point(0, 0)});
      var infoWindow;
      return map.openInfoWindowForIdentifiedElement(ie, marker).then(function (result) {
        infoWindow = result;
        return map.openDataOverlay(layout.getId());
      }).then(function () {
        assert.ok(infoWindow.getContent().innerHTML.indexOf(layout.getName()) >= 0);
        return map.hideDataOverlay(layout.getId());
      }).then(function () {
        assert.notOk(infoWindow.getContent().innerHTML.indexOf(layout.getName()) >= 0);
        assert.equal(logger.getErrors().length, 0);
      }).finally(function () {
        return map.destroy();
      });
    });
  });


  describe("renderOverlayCollection", function () {
    it("for alias", function () {
      var map = helper.createCustomMap();
      var alias = helper.createAlias(map);

      var oc = helper.createDbOverlay(map);

      oc.getIdentifiedElements = function () {
        return Promise.resolve([new IdentifiedElement({
          objectId: alias.getId(),
          icon: "empty.png",
          modelId: map.getId(),
          type: "Alias"
        })]);
      };
      return map.renderOverlayCollection({
        overlayCollection: oc
      }).then(function () {
        var markers = map.getMarkerSurfaceCollection().getMarkers();
        assert.equal(1, markers.length);
      });
    });

    it("alias re-rendering with different icon", function () {
      var map = helper.createCustomMap();
      var reaction = helper.createReaction(map, true);
      var alias = helper.createAlias(map);
      map.getModel().addAlias(alias);
      map.getModel().addReaction(reaction);

      var identifiedElement = new IdentifiedElement(alias);
      identifiedElement.setIcon("empty.png");
      var identifiedElement2 = new IdentifiedElement(alias);
      identifiedElement2.setIcon("new_icon.png");

      var marker;
      var oc = helper.createDbOverlay(map);

      oc.getIdentifiedElements = function () {
        return Promise.resolve([identifiedElement, new IdentifiedElement(reaction)]);
      };
      return map.renderOverlayCollection({
        overlayCollection: oc
      }).then(function () {
        marker = map.getMarkerSurfaceCollection().getMarker(identifiedElement);
        oc.getIdentifiedElements = function () {
          return Promise.resolve([identifiedElement2]);
        };
        assert.ok(map.getMarkerSurfaceCollection().getSurface({element: reaction, overlay: oc}));

        return map.renderOverlayCollection({
          overlayCollection: oc
        });
      }).then(function () {
        assert.equal(marker.getIcon(), "new_icon.png");

        assert.notOk(oc.mapOverlays["REACTION"][reaction.getId()]);
      });
    });

    it("for set of aliases", function () {
      var map = helper.createCustomMap();

      var oc = helper.createDbOverlay(map);

      var identifiedElements = [];
      for (var i = 0; i < 3; i++) {
        var alias = helper.createAlias(map);
        map.getModel().addAlias(alias);
        var ie = helper.createIdentifiedElement(alias);
        ie.setIcon("");
        identifiedElements.push(ie);
      }

      oc.getIdentifiedElements = function () {
        return Promise.resolve(identifiedElements);
      };

      return map.renderOverlayCollection({
        overlayCollection: oc
      }).then(function () {
        var markers = map.getMarkerSurfaceCollection().getMarkers();
        assert.equal(3, markers.length);
      });
    });

    it("for point", function () {
      var map = helper.createCustomMap();

      var oc = helper.createDbOverlay(map);

      var javaObj = {
        objectId: "Point2D.Double[117.685546875, 204.6923828125001]",
        modelId: map.getId(),
        type: "POINT",
        icon: "marker/empty.png"
      };

      oc.getIdentifiedElements = function () {
        return Promise.resolve([new IdentifiedElement(javaObj)]);
      };

      return map.renderOverlayCollection({
        overlayCollection: oc
      }).then(function () {
        var markers = map.getMarkerSurfaceCollection().getMarkers();
        assert.equal(1, markers.length);
      });
    });

    it("for reaction", function () {
      var map = helper.createCustomMap();
      var reaction = helper.createReaction(map, true);
      map.getModel().addReaction(reaction);

      var oc = helper.createDbOverlay(map);

      oc.getIdentifiedElements = function () {
        return Promise.resolve([new IdentifiedElement(reaction)]);
      };

      return map.renderOverlayCollection({
        overlayCollection: oc
      }).then(function () {
        var surfaces = map.getMarkerSurfaceCollection().getSurfaces();
        assert.equal(1, surfaces.length);
      });
    });
  });

  it("refreshMarkers", function () {
    var map = helper.createCustomMap();
    var alias = helper.createAlias(map);
    map.getModel().addAlias(alias);

    var oc = helper.createDbOverlay(map);

    oc.getIdentifiedElements = function () {
      var element = new IdentifiedElement(alias);
      element.setIcon("icon");
      return Promise.resolve([element]);
    };
    return map.renderOverlayCollection({
      overlayCollection: oc
    }).then(function () {
      return map.refreshMarkers(true);
    }).then(function () {
      var markers = map.getMarkerSurfaceCollection().getMarkers();
      assert.equal(1, markers.length);
    });
  });

  it("clearDbOverlays", function () {
    var map = helper.createCustomMap();

    var oc = helper.createDbOverlay(map);

    var javaObj = {
      objectId: "Point2D.Double[117.685546875, 204.6923828125001]",
      modelId: map.getId(),
      type: "POINT",
      icon: "marker/empty.png"
    };
    var searchResults = [new IdentifiedElement(javaObj)];

    oc.getIdentifiedElements = function () {
      return Promise.resolve(searchResults);
    };
    oc.clear = function () {
      searchResults = [];
      return this.callListeners("onSearch", searchResults);
    };

    return map.renderOverlayCollection({
      overlayCollection: oc
    }).then(function () {

      return map.clearDbOverlays();
    }).then(function () {
      var markerCount = 0;
      for (var id in oc.pointMarkers) {
        if (oc.pointMarkers.hasOwnProperty(id)) {
          markerCount++;
        }
      }

      assert.equal(0, markerCount);
    });

  });

  it("getInfoWindowForIdentifiedElement ( reaction)", function () {
    var map = helper.createCustomMap();
    var reaction = helper.createReaction(map, true);
    map.getModel().addReaction(reaction);

    var ie = helper.createIdentifiedElement(reaction);
    var marker = map.getMapCanvas().createMarker({icon: "empty.png", position: new Point(0, 0)});

    var infoWindow = map.getInfoWindowForIdentifiedElement(ie);

    assert.equal(null, infoWindow);
    return map.openInfoWindowForIdentifiedElement(ie, marker).then(function () {
      infoWindow = map.getInfoWindowForIdentifiedElement(ie);
      assert.ok(infoWindow);
    }).finally(function () {
      return map.destroy();
    });

  });

  it("right click on map", function () {
    var map = helper.createCustomMap();
    map.setContextMenu(new MapContextMenu({
      customMap: map,
      element: testDiv,
      molArt: new MolArt(map.getElement(), map)
    }));

    return map.getContextMenu().init().then(function () {
      var data = {
        stop: null,
        point: new Point(10.0, 20.0)
      };

      assert.notOk(map.getActiveSubmapId());
      return map.triggerEvent("map-rightclick", data);
    }).then(function () {
      //we need to wait because some implementations don't use promises...
      return Promise.delay(100);
    }).then(function () {
      assert.equal(map.getId(), map.getActiveSubmapId());
      return map.destroy();
    });

  });

  it("left click on map", function () {
    var map;
    var searchOverlay;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);
      searchOverlay = helper.createSearchDbOverlay(map);

      var data = {
        stop: null,
        point: new Point(184.79, 365.76)
      };
      map.setZoom(4);

      assert.notOk(map.getActiveSubmapId());
      return map.triggerEvent("map-click", data);
    }).then(function () {
      //we need to wait because some implementations don't use promises...
      return Promise.delay(150);
    }).then(function () {
      assert.equal(map.getId(), map.getActiveSubmapId());
      var element = new IdentifiedElement({id: 329171, type: "ALIAS", modelId: map.getId()});
      assert.ok(map.getMarkerSurfaceCollection().getMarker(element));
    });

  });

  it("left click on reaction", function () {
    var map;
    var searchOverlay;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);
      searchOverlay = helper.createSearchDbOverlay(map);

      var data = {
        stop: null,
        point: new Point(457.51, 356.84)
      };
      map.setZoom(4);

      assert.notOk(map.getActiveSubmapId());
      return map.triggerEvent("map-click", data);
    }).then(function () {
      //we need to wait because some implementations don't use promises...
      return Promise.delay(100);
    }).then(function () {
      assert.equal(map.getId(), map.getActiveSubmapId());

      var reaction = new IdentifiedElement({id: 153521, type: "REACTION", modelId: map.getId()});
      var surface = map.getMarkerSurfaceCollection().getSurface({element: reaction, overlay: searchOverlay});

      assert.ok(surface);
      assert.ok(surface.isShown());

      var element = new IdentifiedElement({id: 329165, type: "ALIAS", modelId: map.getId()});
      var marker = map.getMarkerSurfaceCollection().getMarker(element);
      assert.ok(marker);
    });
  });

  it("changed coordinates in map", function () {
    var map = helper.createCustomMap();
    var oldCenter = map.getCenter();
    var newCenter = new Point(3, 87);
    map.setCenter(newCenter);
    map.triggerEvent("map-center_changed");

    var center = ServerConnector.getSessionData(map.getProject()).getCenter(map.getModel());
    assert.ok(center !== oldCenter);
    assert.ok(center instanceof Point);
  });

  it("refreshComments", function () {
    var map = helper.createCustomMap();
    map.getModel().setId(15781);
    helper.createCommentDbOverlay(map);

    ServerConnector.getSessionData().setShowComments(true);
    return map.refreshComments().then(function () {
      assert.notOk(map.getMarkerSurfaceCollection().getMarker(new IdentifiedElement({
        id: '(241.01,372.35)',
        modelId: map.getId(),
        type: "POINT"
      })));
      assert.ok(map.getMarkerSurfaceCollection().getMarker(new IdentifiedElement({
        id: '(643.96,144.09)',
        modelId: map.getId(),
        type: "POINT"
      })));
      assert.notOk(map.getMarkerSurfaceCollection().getMarker(new IdentifiedElement({
        id: '(216.65,370.00)',
        modelId: map.getId(),
        type: "POINT"
      })));
      assert.notOk(map.getMarkerSurfaceCollection().getMarker(new IdentifiedElement({
        id: 'unkId',
        modelId: map.getId(),
        type: "POINT"
      })));
    });
  });

  it("hide comments", function () {
    var map = helper.createCustomMap();
    map.getModel().setId(15781);
    helper.createCommentDbOverlay(map);

    ServerConnector.getSessionData().setShowComments(true);
    return map.refreshComments().then(function () {
      ServerConnector.getSessionData().setShowComments(false);
      return map.refreshComments();
    }).then(function () {
      assert.notOk(map.getMarkerSurfaceCollection().getMarker(new IdentifiedElement({
        id: '(241.01, 372.35)',
        modelId: map.getId(),
        type: "POINT"
      })));
    });
  });

  it("openCommentDialog", function () {
    var map = helper.createCustomMap();
    map.getModel().setId(15781);
    map.setActiveSubmapId(15781);
    map.setActiveSubmapClickCoordinates(new Point(2, 12));
    return map.openCommentDialog().then(function () {
      var types = map.getCommentDialog().getTypes();
      assert.equal(types.length, 6);
      var selected = map.getCommentDialog().getSelectedType();
      assert.ok(selected === "<General>");

      map.getCommentDialog().setSelectedType(1);
      selected = map.getCommentDialog().getSelectedType();
      assert.notOk(selected === "<General>");

      map.getCommentDialog().setSelectedType(2);
      selected = map.getCommentDialog().getSelectedType();
      assert.notOk(selected === "<General>");
    }).then(function () {
      map.getCommentDialog().destroy();
    });
  });

  describe("addComment", function () {
    it("default", function () {
      var map;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        helper.createCommentDbOverlay(map);
        map.setActiveSubmapId(map.getId());
        map.setActiveSubmapClickCoordinates(new Point(2, 12));
        return map.openCommentDialog();
      }).then(function () {
        return map.getCommentDialog().addComment();
      }).then(function (comments) {
        assert.equal(0, comments.length);
        map.getCommentDialog().destroy();
      });
    });

    it("when comments are visible", function () {
      var map;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        helper.createCommentDbOverlay(map);
        map.setActiveSubmapId(map.getId());
        map.setActiveSubmapClickCoordinates(new Point(2, 12));
        return map.openCommentDialog();
      }).then(function () {
        ServerConnector.getSessionData().setShowComments(true);
        return map.getCommentDialog().addComment();
      }).then(function (comments) {
        assert.ok(comments.length > 0);
        map.getCommentDialog().destroy();
      });
    });

    it("add for protein", function () {
      var map;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        helper.createCommentDbOverlay(map);
        map.setActiveSubmapId(map.getId());
        map.setActiveSubmapClickCoordinates(new Point(2, 12));
        return map.openCommentDialog();
      }).then(function () {
        var dialog = map.getCommentDialog();
        dialog.setSelectedType(1);
        return dialog.addComment();
      }).then(function () {
        map.getCommentDialog().destroy();
      });
    });
  });

  it("retrieveOverlayDetailDataForElement for comment", function () {
    var map = helper.createCustomMap();
    helper.createCommentDbOverlay(map);

    var alias = helper.createAlias(map);
    alias.setId(329157);
    map.getModel().addAlias(alias);

    var ie = new IdentifiedElement(alias);

    return map.retrieveOverlayDetailDataForElement(ie, {
      comment: true
    }).then(function (details) {
      assert.ok(details);
      assert.equal(details.length, 1);
      assert.equal(details[0].length, 1);
      assert.ok(details[0][0] instanceof Comment);
    });
  });

  it("getOverlayDataForIdentifiedElement", function () {
    var map = helper.createCustomMap();
    var commentOverlay = helper.createCommentDbOverlay(map);

    var alias = helper.createAlias(map);
    alias.setId(329157);
    map.getModel().addAlias(alias);

    var ie = new IdentifiedElement(alias);

    return map.getOverlayDataForIdentifiedElement(ie, {
      comment: true
    }).then(function (details) {
      assert.equal(details.length, 1);

      var overlayData = details[0];
      assert.equal(overlayData.overlay, commentOverlay);
      assert.ok(overlayData.data);
      assert.equal(overlayData.data.length, 1);
      assert.ok(overlayData.data[0] instanceof Comment);
    });
  });

  it("openSubmap", function () {
    var options = helper.createCustomMapOptions();

    var submodel = helper.createModel();
    options.getProject().addModel(submodel);

    var map = new CustomMap(options);

    map.openSubmap(submodel.getId());
    map.destroy();
  });

  describe("setCenter", function () {
    it("default", function () {
      var options = helper.createCustomMapOptions();

      var map = new CustomMap(options);

      map.setCenter(new Point(10, 20));
      assert.ok(ServerConnector.getSessionData().getCenter(map.getModel()));
    });

    it("on submap", function () {
      var options = helper.createCustomMapOptions();

      var submodel = helper.createModel();
      options.getProject().addModel(submodel);

      var map = new CustomMap(options);
      map.openSubmap(submodel.getId());
      var submap = map.getSubmapById(submodel.getId());
      submap.setCenter(new Point(10, 20));
      assert.ok(ServerConnector.getSessionData().getCenter(submodel));
      map.destroy();
    });
  });

  it("setZoom", function () {
    var options = helper.createCustomMapOptions();

    var map = new CustomMap(options);

    map.setZoom(3);
    assert.equal(3, ServerConnector.getSessionData().getZoomLevel(map.getModel()));
  });

  describe("_openInfoWindowForIdentifiedElement", function () {
    it("for AliasMarker", function () {
      var map;
      var alias, marker;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        return map.getModel().getAliasById(329171);
      }).then(function (result) {
        alias = result;

        var identifiedElement = new IdentifiedElement(alias);
        identifiedElement.setIcon("empty.png");

        marker = new AliasMarker({
          element: identifiedElement,
          map: map
        });

        return marker.init();
      }).then(function () {
        assert.equal(null, map.getAliasInfoWindowById(alias.getId()));
        return map._openInfoWindowForIdentifiedElement(marker).then(function () {
          assert.ok(map.getAliasInfoWindowById(alias.getId()));
        });
      }).finally(function () {
        return map.destroy();
      });

    });
    it("for ReactionMarker", function () {
      var map;
      var reaction, marker;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        return map.getModel().getReactionById(153510);
      }).then(function (result) {
        reaction = result;

        marker = new ReactionMarker({
          element: new IdentifiedElement(reaction),
          map: map
        });
        return marker.init();
      }).then(function () {
        assert.equal(null, map.getReactionInfoWindowById(reaction.getId()));
        return map._openInfoWindowForIdentifiedElement(marker);
      }).then(function () {
        assert.ok(map.getReactionInfoWindowById(reaction.getId()));
      }).finally(function () {
        return map.destroy();
      });

    });
    it("for PointMarker", function () {

      var mockObject = helper.createCustomMap();

      mockObject.getOverlayDataForPoint = function () {
        return Promise.resolve([]);
      };

      var point = new Point(2, 3.45);
      var pointData = new PointData(point, mockObject.getId());

      var pointMarker = new PointMarker({
        element: new IdentifiedElement(pointData),
        map: mockObject
      });

      return pointMarker.init().then(function () {
        assert.equal(null, mockObject.getPointInfoWindowById(pointData.getId()));
        return mockObject._openInfoWindowForIdentifiedElement(pointMarker.getIdentifiedElement());
      }).then(function () {
        assert.ok(mockObject.getPointInfoWindowById(pointData.getId()));
      }).finally(function () {
        return mockObject.destroy();
      });
    });
  });

  describe("init", function () {
    it("non-existing background overlay", function () {
      var options = helper.createCustomMapOptions();
      ServerConnector.getSessionData(options.getProject()).setSelectedBackgroundOverlay(-1);
      var map = new CustomMap(options);
      return map.init().then(function () {
        assert.ok(ServerConnector.getSessionData(options.getProject()).getSelectedBackgroundOverlay() > 0);
        return map.destroy();
      })
    });

    it("project without overlay", function () {
      var options = helper.createCustomMapOptions();
      options.getProject()._backgrounds = [];
      ServerConnector.getSessionData(options.getProject()).setVisibleOverlays([-1]);
      var map = new CustomMap(options);
      return map.init().then(function () {
        assert.fail();
      }).catch(function (e) {
        assert.ok(e instanceof ValidationError);
      });
    });

    it("with session data overlays", function () {
      var project = helper.createProject();
      project._backgrounds = [];
      var overlay1 = helper.createOverlay();
      overlay1.setInitialized(true);
      project.addDataOverlay(overlay1);

      var options = helper.createCustomMapOptions(project);
      ServerConnector.getSessionData(options.getProject()).setVisibleOverlays([overlay1.getId()]);
      var map = new CustomMap(options);
      return map.init().then(function () {
        return map.getVisibleDataOverlays();
      }).then(function (overlays) {
        assert.equal(1, overlays.length);
        map.destroy();
      })
    });

  });

  it("get visible data overlays with removed overlay", function () {
    var project = helper.createProject();
    var overlay1 = helper.createOverlay();
    overlay1.setInitialized(true);
    project.addDataOverlay(overlay1);

    var options = helper.createCustomMapOptions(project);
    ServerConnector.getSessionData(options.getProject()).setVisibleOverlays([overlay1.getId()]);
    var map = new CustomMap(options);
    return map.init().then(function () {
      map._selectedOverlays[-1] = true;
      map.selectedLayoutOverlays[-1] = [];
      return map.getVisibleDataOverlays();
    }).then(function (overlays) {
      assert.equal(1, overlays.length);
      assert.notOk(map._selectedOverlays[-1]);
      map.destroy();
    })
  });

  it("center button", function () {
    var options = helper.createCustomMapOptions();
    var map = new CustomMap(options);
    return map.init().then(function () {
      ServerConnector.getSessionData().setCenter(map.getModel(), new Point(0, 0));
      return $(".minerva-center-map-button", map.getElement())[0].onclick();
    }).then(function () {
      var center = ServerConnector.getSessionData().getCenter(map.getModel());
      assert.ok(center.distanceTo(new Point(0, 0)) > helper.EPSILON);
      return map.destroy();
    })
  });


  describe("appendElementsPointingToSubmap", function () {
    it("point to reaction", function () {
      helper.setUrl("http://test-page/?id=complex_model_with_submaps");
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);
        var oc = helper.createSearchDbOverlay(map);

        var reaction = new IdentifiedElement({id: 161955, modelId: 16729, type: "REACTION"});

        return map.appendElementsPointingToSubmap([reaction], oc).then(function (elements) {
          for (var i = 0; i < elements.length; i++) {
            var element = elements[i];
            //alias pointing to reaction should have an icon
            if (element.type === "ALIAS") {
              assert.ok(element.getIcon() !== undefined);
            }
          }
        });
      });
    });
  });
  describe("logo link", function () {
    it("left logo click", function () {
      var map = helper.createCustomMap();
      return $(".minerva-logo", map.getElement())[0].onclick()
    });
    it("right logo click", function () {
      var map = helper.createCustomMap();
      return $(".minerva-logo", map.getElement())[1].onclick()
    });
  });
  describe("getDistance", function () {
    it("inside element", function () {
      var map = helper.createCustomMap();
      var alias = helper.createAlias(map);
      alias.setX(50);
      alias.setY(60);
      alias.setWidth(100);
      alias.setHeight(200);
      var ie = new IdentifiedElement(alias);
      return map.getDistance({
        modelId: map.getId(),
        coordinates: new Point(100, 200),
        element: ie
      }).then(function (distance) {
        assert.equal(0, distance);
        return map.destroy();
      });
    });
    it("to the left", function () {
      var map = helper.createCustomMap();
      var alias = helper.createAlias(map);
      alias.setX(50);
      alias.setY(60);
      alias.setWidth(100);
      alias.setHeight(200);
      var ie = new IdentifiedElement(alias);
      return map.getDistance({
        modelId: map.getId(),
        coordinates: new Point(1, 200),
        element: ie
      }).then(function (distance) {
        assert.equal(49, distance);
        return map.destroy();
      });
    });
    it("to the right", function () {
      var map = helper.createCustomMap();
      var alias = helper.createAlias(map);
      alias.setX(50);
      alias.setY(60);
      alias.setWidth(100);
      alias.setHeight(200);
      var ie = new IdentifiedElement(alias);
      return map.getDistance({
        modelId: map.getId(),
        coordinates: new Point(161, 200),
        element: ie
      }).then(function (distance) {
        assert.equal(11, distance);
        return map.destroy();
      });
    });
    it("to the top", function () {
      var map = helper.createCustomMap();
      var alias = helper.createAlias(map);
      alias.setX(50);
      alias.setY(60);
      alias.setWidth(100);
      alias.setHeight(200);
      var ie = new IdentifiedElement(alias);
      return map.getDistance({
        modelId: map.getId(),
        coordinates: new Point(100, 11),
        element: ie
      }).then(function (distance) {
        assert.equal(49, distance);
        return map.destroy();
      });
    });
    it("to the bottom", function () {
      var map = helper.createCustomMap();
      var alias = helper.createAlias(map);
      alias.setX(50);
      alias.setY(60);
      alias.setWidth(100);
      alias.setHeight(200);
      var ie = new IdentifiedElement(alias);
      return map.getDistance({
        modelId: map.getId(),
        coordinates: new Point(100, 300),
        element: ie
      }).then(function (distance) {
        assert.equal(40, distance);
        return map.destroy();
      });
    });
    it("corner", function () {
      var map = helper.createCustomMap();
      var alias = helper.createAlias(map);
      alias.setX(50);
      alias.setY(60);
      alias.setWidth(100);
      alias.setHeight(200);
      var ie = new IdentifiedElement(alias);
      return map.getDistance({
        modelId: map.getId(),
        coordinates: new Point(46, 57),
        element: ie
      }).then(function (distance) {
        assert.equal(5, distance);
        return map.destroy();
      });
    });
  });
});
