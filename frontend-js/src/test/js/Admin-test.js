"use strict";

require("./mocha-config");
var $ = require('jquery');

var Admin = require('../../main/js/Admin');
var ServerConnector = require('./ServerConnector-mock');
var logger = require('./logger');
var semver = require('semver');

var chai = require('chai');
var assert = chai.assert;

describe('Admin', function () {
  describe('constructor', function () {
    it('default', function () {
      var admin = new Admin(helper.createCustomMapOptions(null));
      assert.ok(admin);
      return admin.init().then(function () {
        return admin.destroy();
      });
    });
  });

  describe('logout', function () {
    it('default', function () {
      helper.loginAsAdmin();
      var admin = new Admin(helper.createCustomMapOptions(null));
      var token = ServerConnector.getSessionData().getToken();
      var warningCount;
      return admin.init().then(function () {
        warningCount = logger.getWarnings().length;
        assert.ok(token === ServerConnector.getSessionData().getToken());
        var link = $("#logoutLink", testDiv)[0];
        return link.onclick();
      }).then(function () {
        assert.ok(token !== ServerConnector.getSessionData().getToken());
        assert.equal(warningCount, logger.getWarnings().length, "Didn't expect any new warnings");
        return admin.destroy();
      })
    });
  });
  describe('getLatestPublishedVersion', function () {
    it('default', function () {
      helper.loginAsAdmin();
      helper.mockWebdav();
      var admin = new Admin(helper.createCustomMapOptions(null));
      return admin.getLatestPublishedVersion().then(function (version) {
        var oldVersion = "1.0.0";
        assert.ok(semver.gt(version, oldVersion));
        admin.destroy();
      })
    });
  });

});
