"use strict";

require("../../mocha-config");
var $ = require('jquery');

var AddPluginDialog = require('../../../../main/js/gui/admin/AddPluginDialog');
var ServerConnector = require('../../ServerConnector-mock');
var ValidationError = require('../../../../main/js/ValidationError');
var PluginData = require('../../../../main/js/map/data/PluginData');


var logger = require('../../logger');

var fs = require("fs");
var chai = require('chai');
var assert = chai.assert;

describe('AddPluginDialog', function () {
  /**
   *
   * @returns {AddPluginDialog}
   */
  var createDialog = function () {
    return new AddPluginDialog({
      element: testDiv,
      customMap: null,
      configuration: helper.getConfiguration(),
      serverConnector: ServerConnector
    });
  };
  var getPluginsData = ServerConnector.getPluginsData;

  afterEach(function () {
    ServerConnector.getPluginsData = getPluginsData;
  });

  it('init', function () {
    helper.loginAsAdmin();
    var dialog = createDialog();
    return dialog.init().then(function () {
      assert.equal(0, logger.getWarnings().length);
      return dialog.destroy();
    });
  });
  describe('onSaveClicked', function () {
    it('without validation', function () {
      helper.loginAsAdmin();
      var dialog = createDialog();
      return dialog.init().then(function () {
        return dialog.onSaveClicked();
      }).then(function () {
        assert.notOk("error expected");
      }).catch(function () {
      }).finally(function () {
        return dialog.destroy();
      });
    });
    it('with validation', function () {
      helper.loginAsAdmin();
      var dialog = createDialog();
      return dialog.init().then(function () {
        $("[name='pluginUrl']").val("./testFiles/plugin/empty.js");
        return dialog.onValidateClicked();
      }).then(function () {
        return dialog.onSaveClicked();
      }).finally(function () {
        return dialog.destroy();
      });
    });
  });

  describe('onValidateClicked', function () {
    it('invalid file', function () {
      helper.loginAsAdmin();
      var dialog = createDialog();
      return dialog.init().then(function () {
        return dialog.onValidateClicked();
      }).then(function () {
        assert.notOk("error expected");
      }).catch(function (e) {
        assert.ok(e instanceof ValidationError);
      }).finally(function () {
        return dialog.destroy();
      });
    });
    it('valid file', function () {
      helper.loginAsAdmin();
      var dialog = createDialog();
      return dialog.init().then(function () {
        $("[name='pluginUrl']").val("./testFiles/plugin/empty.js");
        return dialog.onValidateClicked();
      }).finally(function () {
        return dialog.destroy();
      });
    });
    it('valid file exists', function () {
      helper.loginAsAdmin();
      var dialog = createDialog();
      ServerConnector.getPluginsData = function () {
        return Promise.resolve([new PluginData({urls: "./testFiles/plugin/empty.js", isPublic: "true"})]);
      };
      return dialog.init().then(function () {
        $("[name='pluginUrl']").val("./testFiles/plugin/empty.js");
        return dialog.onValidateClicked();
      }).then(function () {
        assert.notOk("error expected");
      }).catch(function (e) {
        assert.ok(e instanceof ValidationError);
      }).finally(function () {
        return dialog.destroy();
      });
    });
    it('valid file exists as a private plugin', function () {
      helper.loginAsAdmin();
      var dialog = createDialog();
      ServerConnector.getPluginsData = function () {
        return Promise.resolve([new PluginData({urls: "./testFiles/plugin/empty.js", isPublic: "false"})]);
      };
      return dialog.init().then(function () {
        $("[name='pluginUrl']").val("./testFiles/plugin/empty.js");
        return dialog.onValidateClicked();
      }).finally(function () {
        return dialog.destroy();
      });
    });
  });
});
