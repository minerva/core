"use strict";

require("../../mocha-config");

var Promise = require("bluebird");

var UsersAdminPanel = require('../../../../main/js/gui/admin/UsersAdminPanel');
var SecurityError = require('../../../../main/js/SecurityError');
var ServerConnector = require('../../ServerConnector-mock');
var logger = require('../../logger');

var chai = require('chai');
var assert = chai.assert;

/**
 *
 * @returns {UsersAdminPanel}
 */
function createUserAdminPanel() {
  return new UsersAdminPanel({
    element: testDiv,
    configuration: helper.getConfiguration(),
    serverConnector: ServerConnector
  });
}

describe('UsersAdminPanel', function () {
  describe('init', function () {
    it('default', function () {
      helper.loginAsAdmin();
      var usersTab = createUserAdminPanel();
      return usersTab.init().then(function () {
        assert.equal(0, logger.getWarnings().length);
        return usersTab.destroy();
      });
    });
    it('user without access', function () {
      var oldFun = ServerConnector.getUsers;
      ServerConnector.getUsers = function () {
        return Promise.reject(new SecurityError("Access denied."));
      };
      var usersTab = createUserAdminPanel();
      return usersTab.init().then(function () {
        assert.equal(0, logger.getWarnings().length);
        assert.ok(usersTab.getElement().innerHTML.indexOf("no privilege") >= 0);
        ServerConnector.getUsers = oldFun;
        return usersTab.destroy();
      }).finally(function () {
        ServerConnector.getUsers = oldFun;
      });
    });
  });

  it('refresh', function () {
    helper.loginAsAdmin();
    var usersTab = createUserAdminPanel();
    return usersTab.init().then(function () {
      return usersTab.onRefreshClicked();
    }).then(function () {
      return usersTab.destroy();
    });
  });

  describe('showEditDialog', function () {
    it('default', function () {
      helper.loginAsAdmin();
      var usersTab = createUserAdminPanel();
      return usersTab.init().then(function () {
        return usersTab.showEditDialog("anonymous");
      }).then(function () {
        return usersTab.destroy();
      });
    });

    it('after user was changed locally', function () {
      helper.loginAsAdmin();
      var usersTab = createUserAdminPanel();
      var originalFunction = ServerConnector.getUser;
      return usersTab.init().then(function () {
        return usersTab.showEditDialog("anonymous");
      }).then(function () {
        return ServerConnector.getUser("anonymous");
      }).then(function (user) {
        user.setEmail("email.changed@uni.lu");
        ServerConnector.getUser = function () {
          return Promise.resolve(user);
        };
        return usersTab.showEditDialog("anonymous");
      }).then(function () {
        assert.ok(document.body.innerHTML.indexOf("email.changed@uni.lu") >= 0, "User data wasn't refreshed on second opening");
        return usersTab.destroy();
      }).finally(function () {
        ServerConnector.getUser = originalFunction;
      });
    });
  });
  it('onAddClicked', function () {
    helper.loginAsAdmin();
    var usersTab = createUserAdminPanel();
    return usersTab.init().then(function () {
      return usersTab.onAddClicked();
    }).then(function () {
      return usersTab.destroy();
    });
  });

});
