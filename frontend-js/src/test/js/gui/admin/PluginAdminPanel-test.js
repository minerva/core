"use strict";

require("../../mocha-config");
var $ = require('jquery');

var PluginData = require('../../../../main/js/map/data/PluginData');
var PrivilegeType = require('../../../../main/js/map/data/PrivilegeType');
var PluginAdminPanel = require('../../../../main/js/gui/admin/PluginAdminPanel');
var ServerConnector = require('../../ServerConnector-mock');
var logger = require('../../logger');
var Promise = require('bluebird');

var chai = require('chai');
var assert = chai.assert;

describe('PluginAdminPanel', function () {
  function createPluginAdminPanel() {
    return new PluginAdminPanel({
      element: testDiv,
      configuration: helper.getConfiguration(),
      serverConnector: ServerConnector
    });
  }

  describe('init', function () {
    it('admin', function () {
      helper.loginAsAdmin();
      var mapTab;
      return ServerConnector.getLoggedUser().then(function (user) {
        user.setPrivilege({type: helper.getConfiguration().getPrivilegeType(PrivilegeType.IS_ADMIN)});
      }).then(function () {
        mapTab = createPluginAdminPanel();
        return mapTab.init();
      }).then(function () {
        assert.equal(0, logger.getWarnings().length);
        assert.equal(testDiv.innerHTML.indexOf("You have no privilege"), -1);
        return mapTab.destroy();
      });
    });
    it('user', function () {
      var mapTab = createPluginAdminPanel();
      return mapTab.init().then(function () {
        assert.equal(0, logger.getWarnings().length);
        assert.ok(testDiv.innerHTML.indexOf("You have no privilege") >= 0);
        return mapTab.destroy();
      });
    });
  });

  it('open add plugin dialog', function () {
    helper.loginAsAdmin();
    var mapTab = createPluginAdminPanel();
    return mapTab.init().then(function () {
      assert.equal(0, logger.getWarnings().length);
      var element = $("[name='addPlugin']")[0];
      return element.onclick();
    }).then(function () {
      return mapTab.destroy();
    });
  });

  it('remove plugin', function () {
    helper.loginAsAdmin();
    var mapTab = createPluginAdminPanel();
    return mapTab.init().then(function () {
      mapTab.askConfirmRemoval = function () {
        return Promise.resolve({status: true});
      };
      mapTab.setPlugins([new PluginData({
        urls: [],
        name: "x",
        hash: "8f2112859d40de86dacc1994a224ea3d",
        version: "0.0.1",
        isPublic:"true"
      })]);
      assert.equal(0, logger.getWarnings().length);
      var element = $("[name='removePlugin']")[0];
      return helper.triggerJqueryEvent(element, "click");
    }).then(function () {
      return mapTab.destroy();
    });
  });
});
