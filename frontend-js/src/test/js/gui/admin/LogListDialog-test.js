"use strict";

require('../../mocha-config.js');

var LogListDialog = require('../../../../main/js/gui/admin/LogListDialog');
var ServerConnector = require('../../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('LogListDialog', function () {

  it('constructor', function () {
    var div = testDiv;

    var map = helper.createCustomMap();

    var dialog = new LogListDialog({
      element: div,
      customMap: map
    });
    assert.equal(logger.getWarnings().length, 0);

    dialog.destroy();
  });

  it('_dataTableAjaxCall', function () {
    helper.loginAsAdmin();
    var dialog;
    var callbackCalled = false;
    return ServerConnector.getProject().then(function (project) {
      dialog = new LogListDialog({
        element: testDiv,
        customMap: helper.createCustomMap(project),
        level: 'error'
      });
      return dialog._dataTableAjaxCall({
        start: 0,
        length: 10,
        order: [{column: 0, dir: 'asc'}],
        search: {value: '', regex: false}
      }, function () {
        callbackCalled = true;
      });
    }).then(function () {
      assert.ok(callbackCalled);
    }).finally(function () {
      return dialog.destroy();
    });
  });

  it('downloadAll', function () {
    var div = testDiv;

    var map = helper.createCustomMap();

    var dialog = new LogListDialog({
      element: div,
      customMap: map
    });

    helper.loginAsAdmin();
    return dialog.downloadAll().then(function (content) {
      assert.ok(content);
      return dialog.destroy();
    });

  });

});
