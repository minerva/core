"use strict";

require("../../mocha-config");
var $ = require('jquery');

var EditUserDialog = require('../../../../main/js/gui/admin/EditUserDialog');
var User = require('../../../../main/js/map/data/User');
var ValidationError = require('../../../../main/js/ValidationError');
var ServerConnector = require('../../ServerConnector-mock');

var logger = require('../../logger');

var chai = require('chai');
var assert = chai.assert;
var expect = chai.expect;

/**
 *
 * @param {User} user
 * @return {EditUserDialog}
 */
function createEditUserDialog(user) {
  return new EditUserDialog({
    element: testDiv,
    user: user,
    configuration: helper.getConfiguration(),
    serverConnector: ServerConnector
  });
}

describe('EditUserDialog', function () {

  describe('init', function () {
    it('empty user', function () {
      var user = new User({});
      var dialog = createEditUserDialog(user);

      return dialog.init().then(function () {
        assert.equal(0, logger.getWarnings().length);
        assert.ok(testDiv.innerHTML.indexOf("DEFAULT PRIVILEGE FOR NEW PROJECT") >= 0);
        assert.equal(testDiv.innerHTML.indexOf("DEFAULT PRIVILEGE FOR NEW PROJECT"), testDiv.innerHTML.lastIndexOf("DEFAULT PRIVILEGE FOR NEW PROJECT"));
        dialog.destroy();
      });
    });
  });

  describe('checkValidity', function () {
    it('empty user', function () {
      var user = new User({});
      var dialog = createEditUserDialog(user);

      return dialog.init().then(function () {
        return dialog.checkValidity().then(function () {
          assert.ok(null);
        }, function (error) {
          assert.ok(error instanceof ValidationError);
        });
      }).then(function () {
        dialog.destroy();
      });
    });

    it('new user without password', function () {
      var user = new User({});
      var dialog = createEditUserDialog(user);

      return dialog.init().then(function () {
        dialog.setLogin("x");
        return dialog.checkValidity().then(function () {
          assert.ok(null);
        }, function (error) {
          assert.ok(error instanceof ValidationError);
        });
      }).then(function () {
        dialog.destroy();
      });
    });

    it('too long user login', function () {
      var dialog;
      var user = new User({});
      dialog = createEditUserDialog(user);
      return dialog.init().then(function () {
        dialog.setLogin("x".repeat(256));
        return dialog.checkValidity().then(function () {
          assert.ok(null);
        }).catch(function (error) {
          assert.ok(error instanceof ValidationError);
        });
      }).then(function () {
        dialog.destroy();
      });
    });

    it('existing user', function () {
      var dialog;
      var user;
      return ServerConnector.getUser("anonymous").then(function (result) {
        user = result;
        dialog = createEditUserDialog(user);
        return dialog.init();
      }).then(function () {
        return dialog.checkValidity();
      }).then(function (result) {
        assert.ok(result);
      }).then(function () {
        dialog.destroy();
      });
    });
  });

  describe('onSaveClicked', function () {
    it('existing user', function () {
      helper.loginAsAdmin();
      var dialog;
      var user;
      return ServerConnector.getUser("admin").then(function (result) {
        user = result;
        dialog = createEditUserDialog(user);
        return dialog.init();
      }).then(function () {
        return dialog.onSaveClicked();
      }).then(function () {
        dialog.destroy();
      });
    });

  });
});
