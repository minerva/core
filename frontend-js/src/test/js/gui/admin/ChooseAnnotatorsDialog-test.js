"use strict";

require("../../mocha-config");
var $ = require('jquery');

var ChooseAnnotatorsDialog = require('../../../../main/js/gui/admin/ChooseAnnotatorsDialog');
var Annotator = require('../../../../main/js/map/data/Annotator');
var AnnotatorParameter = require('../../../../main/js/map/data/AnnotatorParameter');
var UserPreferences = require('../../../../main/js/map/data/UserPreferences');
var ServerConnector = require('../../ServerConnector-mock');
var logger = require('../../logger');
var Functions = require('../../../../main/js/Functions');

var chai = require('chai');
var assert = chai.assert;

describe('ChooseAnnotatorsDialog', function () {
  function createDialog() {
    return new ChooseAnnotatorsDialog({
      element: testDiv,
      customMap: null,
      configuration: helper.getConfiguration(),
      serverConnector: ServerConnector
    });
  }

  it('init', function () {
    var dialog = createDialog();
    assert.equal(0, logger.getWarnings().length);
    return dialog.init();
  });


  it('getTypeNodeList sorted', function () {
    var dialog = createDialog();
    var types = dialog.getTypeNodeList();
    for (var i = 1; i < types.length; i++) {
      assert.ok(types[i - 1].text.localeCompare(types[i].text) <= 0, types[i - 1].text + " and " + types[i].text + " are not sorted alphabetically");
    }
  });

  it('setElementType', function () {
    var dialog = createDialog();
    var configuration = helper.getConfiguration();
    return dialog.setElementType(configuration.getReactionTypes()[0]);
  });

  it('saveAnnotatorsInfo', function () {
    var dialog = createDialog();
    var user;
    return ServerConnector.getLoggedUser().then(function (result) {
      user = result;
      user.setPreferences(new UserPreferences());
      var configuration = helper.getConfiguration();
      var elementTypes = configuration.getElementTypes();
      return dialog.saveAnnotatorsInfo(elementTypes, []);
    });
  });

  it('getAllChildrenTypesIfNeeded', function () {
    var dialog = createDialog();
    var configuration = helper.getConfiguration();
    var elementTypes = configuration.getElementTypes();
    var elementType;
    for (var i = 0; i < elementTypes.length; i++) {
      if (elementTypes[i].className === "lcsb.mapviewer.model.map.species.Protein") {
        elementType = elementTypes[i];
      }
    }
    assert.equal(1, dialog.getAllChildrenTypesIfNeeded(elementType, false).length);
    assert.ok(dialog.getAllChildrenTypesIfNeeded(elementType, true).length >= 5);
  });

  it('onchange triggered', function () {
    var dialog = createDialog();
    var user;
    return ServerConnector.getLoggedUser().then(function (result) {
      user = result;
      user.setPreferences(new UserPreferences());
      var configuration = helper.getConfiguration();
      return dialog.setElementType(configuration.getReactionTypes()[0]);
    }).then(function () {
      // return $("[type='checkbox']",testDiv).click();
      $("[type='checkbox']", testDiv)[0].checked = true;
      return $("[type='checkbox']", testDiv)[0].onchange();
    });
  });

  it('click on selected annotator', function () {
    var dialog = createDialog();
    var configuration = helper.getConfiguration();
    return dialog.init().then(function () {
      return ServerConnector.getLoggedUser();
    }).then(function (result) {
      var elementType;
      for (var i = 0; i < configuration.getElementTypes().length; i++) {
        if (configuration.getElementTypes()[i].className === "lcsb.mapviewer.model.map.species.Ion") {
          elementType = configuration.getElementTypes()[i];
        }
      }
      return dialog.setElementType(elementType);
    }).then(function () {
      assert.notOk("" !== $(".minerva-annotators-params")[0].innerHTML);
      return helper.triggerJqueryEvent($(".multi-checkbox-list-selected-entry")[0], "click");
    }).then(function () {
      assert.ok("" !== $(".minerva-annotators-params")[0].innerHTML);
    });
  });

  describe('onChangeParameterValue', function () {
    it('checkbox checked', function () {
      var annotator = new Annotator({
          annotatorClass: "lcsb.mapviewer.annotation.services.annotators.HgncAnnotator",
          order: 0,
          parameters: []
        }
      );
      return ServerConnector.getLoggedUser().then(function (user) {
        user.setPreferences(new UserPreferences());
        var checkbox = Functions.createElement({type: "input", inputType: "checkbox"});
        checkbox.checked = true;
        var dialog = createDialog();

        var parameter = new AnnotatorParameter({
          type: "INPUT",
          order: 0,
          field: "NAME"
        });

        return dialog.onChangeParameterValue(checkbox, parameter, annotator);
      }).then(function () {
        assert.equal(1, annotator.getParametersDefinitions().length);
      });
    });
    describe('config param', function () {
      it('checkbox checked', function () {
        var annotator = new Annotator({
            annotatorClass: "lcsb.mapviewer.annotation.services.annotators.HgncAnnotator",
            order: 0,
            parameters: []
          }
        );
        return ServerConnector.getLoggedUser().then(function (user) {
          user.setPreferences(new UserPreferences());
          var checkbox = Functions.createElement({type: "input", inputType: "checkbox"});
          checkbox.checked = true;
          var dialog = createDialog();

          var parameter = new AnnotatorParameter({
            type: "CONFIG",
            order: 0,
            field: "NAME"
          });

          return dialog.onChangeParameterValue(checkbox, parameter, annotator);
        }).then(function () {
          assert.equal(1, annotator.getParametersDefinitions().length);
        });
      });
      it('checkbox unchecked', function () {
        var annotator = new Annotator({
            annotatorClass: "lcsb.mapviewer.annotation.services.annotators.HgncAnnotator",
            order: 0,
            parameters: []
          }
        );
        return ServerConnector.getLoggedUser().then(function (user) {
          user.setPreferences(new UserPreferences());
          var checkbox = Functions.createElement({type: "input", inputType: "checkbox"});
          checkbox.checked = false;
          var dialog = createDialog();

          var parameter = new AnnotatorParameter({
            type: "CONFIG",
            order: 0,
            field: "NAME"
          });

          return dialog.onChangeParameterValue(checkbox, parameter, annotator);
        }).then(function () {
          assert.equal(1, annotator.getParametersDefinitions().length);
        });
      });

      it('input', function () {
        var annotator = new Annotator({
            annotatorClass: "lcsb.mapviewer.annotation.services.annotators.HgncAnnotator",
            order: 0,
            parameters: []
          }
        );
        return ServerConnector.getLoggedUser().then(function (user) {
          user.setPreferences(new UserPreferences());
          var checkbox = Functions.createElement({type: "input"});
          var dialog = createDialog();

          var parameter = new AnnotatorParameter({
            type: "CONFIG",
            order: 0,
            field: "NAME"
          });

          return dialog.onChangeParameterValue(checkbox, parameter, annotator);
        }).then(function () {
          assert.equal(1, annotator.getParametersDefinitions().length);
        });
      });

    });
  });

  describe('createParameterInput', function () {
    it('by field', function () {
      return ServerConnector.getLoggedUser().then(function (user) {
        user.setPreferences(new UserPreferences());
        var dialog = createDialog();
        var annotator = new Annotator({
            annotatorClass: "lcsb.mapviewer.annotation.services.annotators.HgncAnnotator",
            order: 0,
            parameters: []
          }
        );
        var parameter = new AnnotatorParameter({
          type: "INPUT",
          order: 0,
          field: "NAME"
        });
        var element = dialog.createParameterInput(parameter, annotator);
        assert.notOk(element.checked);
        return element.onchange();
      });
    });
    it('checked', function () {
      return ServerConnector.getLoggedUser().then(function (user) {
        user.setPreferences(new UserPreferences());
        var dialog = createDialog();
        var annotator = new Annotator({
            annotatorClass: "lcsb.mapviewer.annotation.services.annotators.HgncAnnotator",
            order: 0,
            parameters: []
          }
        );
        var parameter = new AnnotatorParameter({
          type: "INPUT",
          order: 0,
          field: "NAME"
        });
        annotator.addParameter(parameter);
        var element = dialog.createParameterInput(parameter, annotator);
        assert.ok(element.checked);
      });
    });
  });

});
