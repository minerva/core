"use strict";

require("../../mocha-config");
var $ = require('jquery');

var MapsAdminPanel = require('../../../../main/js/gui/admin/MapsAdminPanel');
var ServerConnector = require('../../ServerConnector-mock');
var logger = require('../../logger');

var chai = require('chai');
var assert = chai.assert;
var Promise = require('bluebird');

function createMapsAdminPanel() {
  return new MapsAdminPanel({
    element: testDiv,
    configuration: helper.getConfiguration(),
    serverConnector: ServerConnector
  });
}

describe('MapsAdminPanel', function () {

  describe('refresh', function () {
    it('default', function () {
      var mapTab = createMapsAdminPanel();
      return mapTab.init().then(function () {
        return mapTab.onRefreshClicked();
      }).then(function () {
        assert.equal(0, logger.getWarnings().length);
        return mapTab.destroy();
      });
    });
    it('with auto refresh', function () {
      var mapTab = createMapsAdminPanel();
      var originalFunction = ServerConnector.getProjects;
      var project = helper.createProject();
      project.setStatus("xyz");

      var calls = 0;
      MapsAdminPanel.AUTO_REFRESH_TIME=50;
      ServerConnector.getProjects = function(){
        calls++;
        return project.callListeners("onreload").then(function(){
          return Promise.resolve([project]);
        })
      };
      return mapTab.init().then(function () {
        project.setStatus("Ok");
        return Promise.delay(100);
      }).then(function () {
        assert.ok(calls>2);
      }).finally(function(){
        MapsAdminPanel.AUTO_REFRESH_TIME=5000;
        return mapTab.destroy().finally(function(){
          ServerConnector.getProjects = originalFunction;
        });
      });
    });
  });
  it('showLogs', function () {
    helper.loginAsAdmin();
    var mapTab = createMapsAdminPanel();
    return mapTab.init().then(function () {
      return mapTab.showLogs("sample", 'error');
    }).then(function () {
      assert.equal(0, logger.getWarnings().length);
      return mapTab.destroy();
    });
  });

  it('getDialog', function () {
    helper.loginAsAdmin();
    var mapTab = createMapsAdminPanel();
    var project;
    return mapTab.init().then(function () {
      return ServerConnector.getProject();
    }).then(function (result) {
      project = result;
      return mapTab.getDialog(project);
    }).then(function (dialog) {
      assert.ok(project.getListeners("onreload").length > 0);
      return mapTab.destroy();
    });
  });


  describe('onAddClicked', function () {
    it('default', function () {
      var mapTab = createMapsAdminPanel();
      return mapTab.init().then(function () {
        return mapTab.onAddClicked();
      }).then(function () {
        return mapTab.destroy();
      });
    });
    it('close and reopen', function () {
      var mapTab = createMapsAdminPanel();
      return mapTab.init().then(function () {
        return mapTab.onAddClicked();
      }).then(function () {
        mapTab._addDialog.close();
        return mapTab.onAddClicked();
      }).then(function () {
        return mapTab.destroy();
      });
    });
  });

  describe('showEditDialog', function () {
    it('default', function () {
      var mapTab = createMapsAdminPanel();
      return mapTab.init().then(function () {
        return mapTab.showEditDialog("sample");
      }).then(function () {
        return mapTab.destroy();
      });
    });
    it('reopen after data changed', function () {
      var mapTab = createMapsAdminPanel();
      var originalFunction = ServerConnector.getProject;
      return mapTab.init().then(function () {
        return mapTab.showEditDialog("sample");
      }).then(function () {
        return ServerConnector.getProject("sample");
      }).then(function (project) {
        project.setName("new name");
        ServerConnector.getProject = function () {
          return Promise.resolve(project);
        };
        return mapTab.showEditDialog("sample");
      }).then(function () {
        var inputs = $('input').filter(function () {
          return this.value === 'new name'
        });
        assert.ok(inputs.length > 0, "Data wasn't updated after reopening popup");
        return mapTab.destroy();
      }).finally(function () {
        ServerConnector.getProject = originalFunction;
      });
    });
  });

});
