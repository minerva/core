"use strict";

require('../../mocha-config.js');

var RegisterInMinervaNetDialog = require('../../../../main/js/gui/admin/RegisterInMinervaNetDialog');
var ServerConnector = require('../../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('RegisterInMinervaNetDialog', function () {

  it('constructor', function () {
    var div = testDiv;

    var map = helper.createCustomMap();

    var dialog = new RegisterInMinervaNetDialog({
      element: div,
      customMap: map,
      configuration: helper.getConfiguration()
    });
    assert.equal(logger.getWarnings().length, 0);

    return dialog.init().then(function() {
      return dialog.open();
    }).then(function(){
      return dialog.destroy();
    })

  });
});
