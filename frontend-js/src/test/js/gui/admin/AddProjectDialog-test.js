"use strict";

require("../../mocha-config");
var $ = require('jquery');

var AddProjectDialog = require('../../../../main/js/gui/admin/AddProjectDialog');
var ServerConnector = require('../../ServerConnector-mock');
var ValidationError = require('../../../../main/js/ValidationError');
var ZipEntry = require('../../../../main/js/gui/admin/ZipEntry');

var logger = require('../../logger');

var fs = require("fs");
var chai = require('chai');
var assert = chai.assert;
var toBlob = require('stream-to-blob');


describe('AddProjectDialog', function () {
  /**
   *
   * @returns {AddProjectDialog}
   */
  var createDialog = function () {
    return new AddProjectDialog({
      element: testDiv,
      customMap: null,
      configuration: helper.getConfiguration(),
      serverConnector: ServerConnector
    });
  };
  it('open', function () {
    helper.loginAsAdmin();
    var dialog = createDialog();
    return dialog.init().then(function () {
      return dialog.open();
    }).then(function () {
      assert.ok(dialog.getNotifyEmail() !== "");
      assert.equal(0, logger.getWarnings().length);
      return dialog.destroy();
    });
  });

  it('check help buttons', function () {
    helper.loginAsAdmin();
    var dialog = createDialog();
    return dialog.init().then(function () {
      assert.ok(testDiv.innerHTML.indexOf("minerva-help-button") >= 0);
      return dialog.destroy();
    });
  });

  it('onUserPreferencesChange', function () {
    helper.loginAsAdmin();
    var dialog = createDialog();
    return dialog.init().then(function () {
      dialog.setSbgn(false);
      assert.notOk(dialog.isSbgn());
      dialog.setSbgn(true);
      assert.ok(dialog.isSbgn());
      return dialog.destroy();
    });
  });

  describe('onFileUpload', function () {
    it('default', function () {
      var dialog = createDialog();

      var file = new Blob(["<node></node>"]);
      file.name = "test.xml";
      return dialog.init().then(function () {
        return dialog.callListeners("onFileUpload", file);
      }).then(function () {
        assert.ok(dialog.getFileContent());
        assert.equal("test", dialog.getProjectId());
        return dialog.getConverter();
      }).then(function (converter) {
        assert.equal("xml", converter.extension);
        return dialog.destroy();
      });
    });

    it('sbml file', function () {
      var dialog = createDialog();

      return dialog.init().then(function () {
        return helper.fileToBlob('testFiles/map/sbml.xml');
      }).then(function (file) {
        return dialog.callListeners("onFileUpload", file);
      }).then(function () {
        var converter = dialog.getConverter();
        assert.equal("sbml", converter.name.toLowerCase());
        return dialog.destroy();
      });
    });
    it('invalid zip', function () {
      var dialog = createDialog();

      var buf = fs.readFileSync("testFiles/map/invalid-project.zip");
      buf.name = "invalid-project.zip";
      return dialog.init().then(function () {
        return dialog.setZipFileContent(buf);
      }).catch(function (e) {
        assert.ok(e instanceof ValidationError);
      }).finally(function () {
        return dialog.destroy();
      });
    });
    it('zip with no map', function () {
      var dialog = createDialog();

      var buf = fs.readFileSync("testFiles/map/no-map.zip");
      buf.name = "invalid-project.zip";
      return dialog.init().then(function () {
        return dialog.setZipFileContent(buf);
      }).then(function () {
        assert.ok(false);
      }).catch(function (e) {
        console.log(e);
        assert.ok(e instanceof ValidationError);
      }).finally(function () {
        return dialog.destroy();
      });
    });
    it('CellDesigner file', function () {
      var dialog = createDialog();

      return dialog.init().then(function () {
        return helper.fileToBlob('testFiles/map/cell_designer.xml');
      }).then(function (file) {
        return dialog.callListeners("onFileUpload", file);
      }).then(function () {
        var converter = dialog.getConverter();
        assert.notEqual("sbml", converter.name.toLowerCase());
        return dialog.destroy();
      });
    });
  });

  describe('setZipFileContent', function () {
    it('submaps', function () {
      var dialog = createDialog();
      var buf = fs.readFileSync("testFiles/map/complex_model_with_submaps.zip");
      buf.name = "complex_model_with_submaps.zip";
      return dialog.init().then(function () {
        return dialog.setZipFileContent(buf);
      }).then(function () {
        assert.equal(5, dialog.getZipEntries().length);
        return dialog.destroy();
      });
    });
    it('filename should be the same', function () {
      var filename = "complex_model_with_submaps.zip";
      var dialog = createDialog();
      var buf = fs.readFileSync("testFiles/map/" + filename);
      buf.name = filename;
      return dialog.init().then(function () {
        return dialog.setZipFileContent(buf);
      }).then(function () {
        assert.equal(filename, dialog.getFilename());
        return dialog.destroy();
      });
    });
    it('overlays', function () {
      var dialog = createDialog();
      var buf = fs.readFileSync("testFiles/map/complex_model_with_overlays.zip");
      buf.name = "complex_model_with_overlays.zip";
      return dialog.init().then(function () {
        var dataTable = $($("[name='overlaysTable']", testDiv)[0]).DataTable();
        assert.equal(0, dataTable.data().count());
        return dialog.setZipFileContent(buf);
      }).then(function () {
        var dataTable = $($("[name='overlaysTable']", testDiv)[0]).DataTable();
        assert.ok(dataTable.data().count() > 0);
        return dialog.destroy();
      });
    });
    it('glyphs', function () {
      var dialog = createDialog();
      var buf = fs.readFileSync("testFiles/map/complex_model_with_glyphs.zip");
      buf.name = "complex_model_with_glyphs.zip";
      return dialog.init().then(function () {
        var dataTable = $($("[name='glyphsTable']", testDiv)[0]).DataTable();
        assert.equal(0, dataTable.data().count());
        return dialog.setZipFileContent(buf);
      }).then(function () {
        var dataTable = $($("[name='glyphsTable']", testDiv)[0]).DataTable();
        assert.ok(dataTable.data().count() > 0);
        return dialog.destroy();
      });
    });
  });

  describe('showAnnotatorsDialog', function () {
    it('default', function () {
      var dialog = createDialog();

      return dialog.showAnnotatorsDialog().then(function () {
        return dialog.destroy();
      });
    });
  });


  describe('onSaveClicked', function () {
    var originalAddProject;
    beforeEach(function () {
      originalAddProject = ServerConnector.addProject;
    });
    afterEach(function () {
      ServerConnector.addProject = originalAddProject;
    });
    it('default', function () {
      var options;
      ServerConnector.addProject = function (params) {
        options = params;
      };
      var dialog = createDialog();
      var projectAdded = false;
      dialog.addListener("onProjectAdd", function () {
        projectAdded = true;
      });

      var file = new Blob(["<node></node>"]);
      file.name = "test.xml";
      return dialog.init().then(function () {
        return dialog.callListeners("onFileUpload", file);
      }).then(function () {
        return dialog.onSaveClicked();
      }).then(function () {
        assert.ok(options["name"] !== undefined);
        assert.ok(options["projectId"] !== undefined);
        assert.ok(options["file-id"] !== undefined);
        assert.ok(options["parser"] !== undefined);
        assert.ok(options["auto-resize"] !== undefined);
        assert.ok(options["notify-email"] !== undefined);
        assert.ok(options["disease"] !== undefined);
        assert.ok(options["organism"] !== undefined);
        assert.ok(options["sbgn"] !== undefined);
        assert.ok(options["version"] !== undefined);

        assert.ok(projectAdded);
      }).finally(function () {
        return dialog.destroy();
      });
    });
  });

  it('getOrganism', function () {
    var dialog = createDialog();
    dialog.setOrganism("9606");
    assert.equal("9606", dialog.getOrganism());
    return dialog.destroy();
  });

  describe('checkValidity', function () {
    it('invalid project name', function () {
      var dialog = createDialog();

      var file = new Blob(["<node></node>"]);
      file.name = "test.xml";
      return dialog.init().then(function () {
        return dialog.callListeners("onFileUpload", file);
      }).then(function () {
        dialog.setProjectId("(invalid id)");
        return dialog.checkValidity().then(function () {
          assert.notOk("Expected validity to reject");
        }).catch(function (error) {
          assert.ok(error instanceof ValidationError);
        });
      }).then().finally(function () {
        return dialog.destroy();
      });
    });
    it('valid project name', function () {
      var dialog = createDialog();

      var file = new Blob(["<node></node>"]);
      file.name = "test.xml";
      return dialog.init().then(function () {
        return dialog.callListeners("onFileUpload", file);
      }).then(function () {
        dialog.setProjectId("-valid_id");
        return dialog.checkValidity();
      }).finally(function () {
        return dialog.destroy();
      });
    });
    it('valid taxonomy id', function () {
      var dialog = createDialog();

      var file = new Blob(["<node></node>"]);
      file.name = "test.xml";
      return dialog.init().then(function () {
        return dialog.callListeners("onFileUpload", file);
      }).then(function () {
        dialog.setProjectId("-valid_id");
        dialog.setOrganism("9606");
        return dialog.checkValidity();
      }).finally(function () {
        return dialog.destroy();
      });
    });
    it('invalid taxonomy id', function () {
      var dialog = createDialog();

      var file = new Blob(["<node></node>"]);
      file.name = "test.xml";
      return dialog.init().then(function () {
        return dialog.callListeners("onFileUpload", file);
      }).then(function () {
        dialog.setProjectId("-valid_id");
        dialog.setOrganism("-9606");
        return dialog.checkValidity().then(function () {
          assert.notOk("Expected validity to reject");
        }).catch(function (error) {
          assert.ok(error instanceof ValidationError);
        });
      }).finally(function () {
        return dialog.destroy();
      });
    });
    it('for zip file', function () {
      var dialog = createDialog();

      var file = fs.readFileSync("testFiles/map/complex_model_with_submaps.zip");
      file.name = "complex_model_with_submaps.zip";
      return dialog.init().then(function () {
        dialog.setFileContent("");
        return dialog.setZipFileContent(file);
      }).then(function () {
        dialog.setProjectId("invalid_id");
        return dialog.checkValidity();
      }).then().finally(function () {
        return dialog.destroy();
      });
    });

    it('validate overlay data', function () {
      var dialog = createDialog();

      var file = fs.readFileSync("testFiles/map/complex_model_with_overlays.zip");
      file.name = "complex_model_with_submaps.zip";
      return dialog.init().then(function () {
        dialog.setFileContent("");
        return dialog.setZipFileContent(file);
      }).then(function () {
        dialog.setProjectId("invalid_id");
        var entry = new ZipEntry({type: "OVERLAY", filename: "file.txt", data: {name: ""}});
        dialog.getZipEntries().push(entry);
        return dialog.checkValidity();
      }).then(function (isValid) {
        assert.notOk("exception expected");
      }).catch(function (error) {
        assert.ok(error instanceof ValidationError);
      }).finally(function () {
        return dialog.destroy();
      });
    });

  });

  describe('isIgnoredZipEntry', function () {
    it('valid entry', function () {
      var dialog = createDialog();

      assert.notOk(dialog.isIgnoredZipEntry("images/a.png"));
      assert.notOk(dialog.isIgnoredZipEntry("main.xml"));
      return dialog.destroy();
    });
    it('invalid MAC OS entry', function () {
      var dialog = createDialog();

      // noinspection SpellCheckingInspection
      assert.ok(dialog.isIgnoredZipEntry("__MACOSX/.desc"));
      assert.ok(dialog.isIgnoredZipEntry("__macosx/.desc"));
      return dialog.destroy();
    });
    it('invalid old MAC OS entry', function () {
      var dialog = createDialog();

      assert.ok(dialog.isIgnoredZipEntry(".DS_Store/.desc"));
      assert.ok(dialog.isIgnoredZipEntry(".ds_store/.desc"));
      assert.ok(dialog.isIgnoredZipEntry("images/.ds_store"));
      return dialog.destroy();
    });
  });

  describe('createInputRow', function () {
    it('with help button', function () {
      var dialog = createDialog();
      var row = dialog.createInputRow({help: "help me"});
      assert.ok(row.innerHTML.indexOf("minerva-help-button") >= 0);
      return dialog.destroy();
    });
  });

});
