"use strict";

require("../../mocha-config");
var $ = require('jquery');

var EditProjectDialog = require('../../../../main/js/gui/admin/EditProjectDialog');
var ServerConnector = require('../../ServerConnector-mock');
var logger = require('../../logger');
var PrivilegeType = require('../../../../main/js/map/data/PrivilegeType');

var chai = require('chai');
var assert = chai.assert;
var Promise = require('bluebird');

describe('EditProjectDialog', function () {

  /**
   *
   * @param [project]
   * @returns {Promise<EditProjectDialog>}
   */
  function createDialog(project) {
    if (project !== undefined) {
      return Promise.resolve(new EditProjectDialog({
        element: testDiv,
        project: project,
        customMap: null,
        configuration: helper.getConfiguration(),
        serverConnector: ServerConnector
      }));
    } else {
      return ServerConnector.getProject().then(function (project) {
        return new EditProjectDialog({
          element: testDiv,
          project: project,
          customMap: null,
          configuration: helper.getConfiguration(),
          serverConnector: ServerConnector
        });
      });
    }
  }

  it('open', function () {
    var dialog;
    return createDialog().then(function (result) {
      dialog = result;
      return dialog.open();
    }).then(function () {
      assert.equal(0, logger.getWarnings().length);
      dialog.destroy();
    });
  });

  it('init', function () {
    helper.loginAsAdmin();
    var dialog;
    return createDialog().then(function (result) {
      dialog = result;
      return dialog.init();
    }).then(function () {
      dialog.destroy();
    });
  });

  it('update project rejected', function () {
    helper.loginAsAdmin();
    var dialog, input;
    return createDialog().then(function (result) {
      dialog = result;
      return dialog.init();
    }).then(function () {
      input = $("[name='projectDisease']");
      input.val("XYZ");
      return input[0].parentNode.onchange();
    }).then(function () {
      assert.notOk("Error expected");
    }).catch(function (e) {
      assert.notEqual(input.val(), "XYZ", "After reject the data should be restored");
    }).then(function () {
      return dialog.destroy();
    });
  });

  it('openAddOverlayDialog', function () {
    var dialog;
    return createDialog().then(function (result) {
      dialog = result;
      return dialog.openAddOverlayDialog();
    }).then(function (overlayDialog) {
      overlayDialog.setFileContent("Xyz");
      return $("button:contains('UPLOAD')", dialog.getElement()).click();
    }).then(function () {
      return dialog.destroy();
    });
  });

  describe('overlayToTableRow', function () {
    it('as not admin', function () {
      var dialog;
      helper.loginAsAdmin();
      return createDialog().then(function (result) {
        dialog = result;
        return ServerConnector.getUsers();
      }).then(function (users) {
        helper.loginWithoutAccess();
        var overlay = helper.createOverlay();
        var data = dialog.overlayToTableRow(overlay, users);
        assert.ok(data[6].indexOf("disabled") >= 0)
      }).then(function () {
        return dialog.destroy();
      });
    });
    it('as admin', function () {
      var dialog;
      helper.loginAsAdmin();
      return createDialog().then(function (result) {
        dialog = result;
        return ServerConnector.getUsers();
      }).then(function (users) {
        var overlay = helper.createOverlay();
        var data = dialog.overlayToTableRow(overlay, users);
        assert.equal(data[6].indexOf("disabled"), -1)
      }).then(function () {
        return dialog.destroy();
      });
    });
  });

  it('grantPrivilege', function () {
    var dialog;
    var user;
    helper.loginAsAdmin();
    return createDialog().then(function (result) {
      dialog = result;
      return ServerConnector.getUser("anonymous");
    }).then(function (result) {
      user = result;
      return dialog.grantPrivilege(user, PrivilegeType.WRITE_PROJECT, "sample");
    }).then(function () {
      assert.ok(user.hasPrivilege(helper.getConfiguration().getPrivilegeType(PrivilegeType.WRITE_PROJECT), "sample"));
      return dialog.destroy();
    });
  });

  it('revokePrivilege', function () {
    var dialog;
    var user;
    helper.loginAsAdmin();
    return createDialog().then(function (result) {
      dialog = result;
      return ServerConnector.getUser("anonymous");
    }).then(function (result) {
      user = result;
      return dialog.grantPrivilege(user, PrivilegeType.WRITE_PROJECT, "sample");
    }).then(function () {
      return dialog.revokePrivilege(user, PrivilegeType.WRITE_PROJECT, "sample");
    }).then(function () {
      assert.notOk(user.hasPrivilege(helper.getConfiguration().getPrivilegeType(PrivilegeType.WRITE_PROJECT), "sample"));
      return dialog.destroy();
    });
  });


});
