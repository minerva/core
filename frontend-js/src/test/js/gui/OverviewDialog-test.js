"use strict";

require("../mocha-config.js");
var ServerConnector = require('../ServerConnector-mock');

var CustomMap = require('../../../main/js/map/CustomMap');
var OverviewDialog = require('../../../main/js/gui/OverviewDialog');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../logger');

describe('OverviewDialog', function () {

  describe('showOverview', function () {
    it('valid image', function () {
      helper.setUrl("http://test-page/?id=complex_model_with_images");
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);

        var dialog = new OverviewDialog({
          element: testDiv,
          customMap: map
        });

        dialog.showOverview(project.getOverviewImages()[1].idObject);
        dialog.destroy();
      });
    });

    it('invalid image', function () {
      helper.setUrl("http://test-page/?id=complex_model_with_images");
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);

        var dialog = new OverviewDialog({
          element: testDiv,
          customMap: map
        });

        dialog.showOverview(-1123);
        dialog.destroy();
      });
    });
  });

  describe('openLink', function () {
    it('link to map', function () {
      helper.setUrl("http://test-page/?id=complex_model_with_images");
      var dialog;
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);

        dialog = new OverviewDialog({
          element: testDiv,
          customMap: map
        });
        var link = project.getOverviewImages()[0].links[0];
        assert.equal(link.type, "OverviewModelLink");

        dialog.openLink(link);
        assert.equal(logger.getWarnings().length, 0);
        assert.ok(link.modelPoint.x - ServerConnector.getSessionData().getCenter(map.getModel()).x < helper.EPSILON);
        assert.ok(link.modelPoint.y - ServerConnector.getSessionData().getCenter(map.getModel()).y < helper.EPSILON);
      }).finally(function () {
        dialog.destroy();
      });
    });
    it('link to search', function () {
      helper.setUrl("http://test-page/?id=complex_model_with_images");
      var dialog, searchOverlay;
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);
        searchOverlay = helper.createSearchDbOverlay(map);

        dialog = new OverviewDialog({
          element: testDiv,
          customMap: map
        });

        return dialog.openLink({
          type: "OverviewSearchLink",
          query: "some_query"
        });
      }).then(function () {
        assert.equal(logger.getWarnings().length, 0);
        assert.equal(1, searchOverlay.getQueries().length);
        return searchOverlay.getIdentifiedElements();
      }).then(function (elements) {
        assert.equal(0, elements.length);
      }).finally(function () {
        dialog.destroy();
      });
    });
  });

});
