"use strict";

require("../mocha-config.js");
var $ = require('jquery');

var Legend = require('../../../main/js/gui/Legend');
var ConfigurationType = require('../../../main/js/ConfigurationType');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../logger');
var nock = require('nock');

describe('Legend', function () {

  it('constructor', function () {
    var map = helper.createCustomMap();

    new Legend({
      element: testDiv,
      customMap: map
    });

    assert.equal(logger.getWarnings().length, 0);

  });

  describe('init', function () {
    it('default', function () {
      var map = helper.createCustomMap();

      var legend = new Legend({
        element: testDiv,
        customMap: map,
        configuration: helper.getConfiguration()
      });

      return legend.init().then(function () {
        //there are no elements visible (all of them have urls that cannot be resolved)
        assert.equal(0, $(".item", testDiv).length);
      });
    });

    it('existing file', function () {
      nock('https://www.google.pl/')
        .get('/')
        .reply(200, '<html lang=""></html>');

      var map = helper.createCustomMap();

      var legend = new Legend({
        element: testDiv,
        customMap: map
      });

      var originalUrl;
      var files;
      return ServerConnector.getConfigurationParam(ConfigurationType.LEGEND_FILES).then(function (legendFiles) {
        files = legendFiles;
        originalUrl = legendFiles[0].getValue();
        legendFiles[0].setValue("https://www.google.pl/");
        return legend.init();
      }).then(function () {
        assert.equal(1, $(".carousel-item", testDiv).length);
      }).finally(function () {
        files[0].setValue(originalUrl);
      });
    });
  });

});
