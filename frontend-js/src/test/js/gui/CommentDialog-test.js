"use strict";

require('../mocha-config');
var $ = require('jquery');

var CommentDialog = require('../../../main/js/gui/CommentDialog');
var ServerConnector = require('../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../logger');

describe('CommentDialog', function () {

  it('constructor', function () {
    var map = helper.createCustomMap();

    var dialog = new CommentDialog({
      element: testDiv,
      customMap: map
    });
    assert.ok(testDiv.innerHTML);
    assert.equal(logger.getWarnings().length, 0);
    dialog.destroy();
  });

  it('getName', function () {
    var map = helper.createCustomMap();

    var dialog = new CommentDialog({
      element: testDiv,
      customMap: map
    });
    assert.ok(dialog.getEmail() !== undefined);
    dialog.destroy();
  });

  describe('open', function () {
    it('anonymous user', function () {
      var map = helper.createCustomMap();
      var dialog = new CommentDialog({
        element: testDiv,
        customMap: map
      });
      return dialog.open([]).then(function () {
        assert.ok($(testDiv).dialog('isOpen'));
        dialog.destroy();
      });
    });
    it('logged user', function () {
      helper.loginAsAdmin();
      var map = helper.createCustomMap();
      var dialog = new CommentDialog({
        element: testDiv,
        customMap: map
      });
      return dialog.open([]).then(function () {
        assert.ok($(testDiv).dialog('isOpen'));
        dialog.destroy();
      });
    });
  });

});
