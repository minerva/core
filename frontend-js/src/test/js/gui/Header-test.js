"use strict";

require("../mocha-config.js");
var $ = require('jquery');

var Header = require('../../../main/js/gui/Header');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../logger');

describe('Header', function () {

  describe('constructor', function () {
    it('default', function () {
      var map = helper.createCustomMap();

      new Header({
        element: testDiv,
        configuration: helper.getConfiguration(),
        customMap: map
      });
      assert.equal(logger.getWarnings().length, 0);
      assert.equal(1, $(".fa-lock", $(testDiv)).length);
    });
    it('with options-link', function () {
      var map = helper.createCustomMap();

      var header = new Header({
        element: testDiv,
        configuration: helper.getConfiguration(),
        customMap: map,
        optionsMenu: true
      });
      assert.equal(logger.getWarnings().length, 0);
      assert.equal(1, $(".fa-bars", $(testDiv)).length);
      return header.destroy();
    });
  });

  it('init', function () {
    var map = helper.createCustomMap();

    var header = new Header({
      element: testDiv,
      configuration: helper.getConfiguration(),
      customMap: map
    });

    return header.init().then(function () {
      return header.destroy();
    });
  });

  it('open menu', function () {
    var map = helper.createCustomMap();

    var header = new Header({
      element: testDiv,
      configuration: helper.getConfiguration(),
      customMap: map,
      optionsMenu: true
    });

    return header.init().then(function () {
      assert.ok($(".dropdown-menu").css('display') === 'none');
      var fun = null;
      $(".minerva-menu-link", testDiv).each(function (k, v) {
        if (v.innerHTML.indexOf("fa-bars") >= 0) {
          fun = v.onclick;
        }
      });
      return fun();
    }).then(function () {
      assert.notOk($(".dropdown-menu").css('display') === 'none');
      return header.destroy();
    });
  });


});
