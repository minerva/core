"use strict";

require("../mocha-config.js");
var $ = require('jquery');

var MapContextMenu = require('../../../main/js/gui/MapContextMenu');
var Point = require('../../../main/js/map/canvas/Point');
var ServerConnector = require('../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../logger');

describe('MapContextMenu', function () {

  it('constructor', function () {
    var map = helper.createCustomMap();

    new MapContextMenu({
      element: testDiv,
      customMap: map
    });

    assert.equal(logger.getWarnings().length, 0);
  });

  it('init', function () {
    var map = helper.createCustomMap();

    var menu = new MapContextMenu({
      element: testDiv,
      customMap: map
    });
    return menu.init();
  });

  it('open comment dialog', function () {
    var map;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);

      new MapContextMenu({
        element: testDiv,
        customMap: map
      });
      map.setActiveSubmapId(map.getProject().getModels()[0].getId());
      map.setActiveSubmapClickCoordinates(new Point(2, 12));

      var handler = $($("a:contains('comment')", $(testDiv))[0]).data("handler");
      return handler();
    }).then(function () {
      map.destroy();
    });
  });

});
