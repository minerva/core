"use strict";

require('../mocha-config.js');

var LoginDialog = require('../../../main/js/gui/LoginDialog');
var PanelControlElementType = require('../../../main/js/gui/PanelControlElementType');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../logger');

describe('LoginDialog', function () {

  it('constructor', function () {
    var map = helper.createCustomMap();

    var dialog = new LoginDialog({
      element: testDiv,
      customMap: map
    });
    assert.equal(logger.getWarnings().length, 0);
    dialog.destroy();
  });

  it('open', function () {
    var map = helper.createCustomMap();

    var dialog = new LoginDialog({
      element: testDiv,
      customMap: map
    });

    dialog.open();
    dialog.destroy();
  });

  it('login', function () {
    var map = helper.createCustomMap();

    var dialog = new LoginDialog({
      element: testDiv,
      customMap: map
    });

    return dialog.getControlElement(PanelControlElementType.USER_TAB_LOGIN_BUTTON).onclick().then(function () {
      dialog.destroy();
    });
  });
});
