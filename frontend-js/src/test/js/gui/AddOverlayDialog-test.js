"use strict";

require("../mocha-config");
var $ = require('jquery');

var AddOverlayDialog = require('../../../main/js/gui/AddOverlayDialog');
var ServerConnector = require('../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../logger');


describe('AddOverlayDialog', function () {

  it('addOverlay', function () {
    var dialog;
    return ServerConnector.getProject().then(function (project) {
      dialog = createDialog(project);

      dialog.setFileContent("s1\n");
      assert.equal(0, logger.getWarnings().length);
      return dialog.addOverlay();
    });
  });

  /**
   *
   * @param {Project} project
   * @returns {AddOverlayDialog}
   */
  function createDialog(project) {
    return new AddOverlayDialog({
      element: testDiv,
      project: project,
      customMap: null,
      configuration: helper.getConfiguration(),
      serverConnector: ServerConnector
    });
  }

  describe('processFile', function () {
    it('set default type', function () {
      var dialog;
      return ServerConnector.getProject().then(function (project) {
        dialog = createDialog(project);
        return dialog.init();
      }).then(function () {

        dialog.setType("GENETIC_VARIANT");
        assert.equal("GENETIC_VARIANT", dialog.getType());

        return helper.stringToBlob("s1\n");
      }).then(function (file) {
        return dialog.processFile(file);
      }).then(function () {
        assert.equal("GENERIC", dialog.getType());
      });
    });
    it('set type from header', function () {
      var dialog;
      return ServerConnector.getProject().then(function (project) {
        dialog = createDialog(project);
        return dialog.init();
      }).then(function () {
        return helper.stringToBlob("#TYPE=GENETIC_VARIANT\ns1\n");
      }).then(function (file) {
        return dialog.processFile(file);
      }).then(function () {
        assert.equal("GENETIC_VARIANT", dialog.getType());
      });
    });
  });


});
