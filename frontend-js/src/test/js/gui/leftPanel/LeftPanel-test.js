"use strict";

require('../../mocha-config.js');
var $ = require('jquery');

var LeftPanel = require('../../../../main/js/gui/leftPanel/LeftPanel');
var IdentifiedElement = require('../../../../main/js/map/data/IdentifiedElement');
var ServerConnector = require('../../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('LeftPanel', function () {

  function createLeftPanel(map) {
    return new LeftPanel({
      element: testDiv,
      customMap: map,
      configuration: helper.getConfiguration()
    });
  }

  it('constructor', function () {
    var map = helper.createCustomMap();
    helper.createSearchDbOverlay(map);
    helper.createDrugDbOverlay(map);
    helper.createChemicalDbOverlay(map);

    var panel = createLeftPanel(map);

    assert.equal(logger.getWarnings().length, 0);

    return panel.destroy();
  });

  describe('prepareElementDetailsContent', function () {
    it('for element', function () {
      var map;
      var panel;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);

        helper.createSearchDbOverlay(map);
        helper.createDrugDbOverlay(map);
        helper.createChemicalDbOverlay(map);

        panel = createLeftPanel(map);

        var element = new IdentifiedElement({
          id: 329163,
          type: "ALIAS",
          modelId: map.getId()
        });
        return map.getModel().getByIdentifiedElement(element, true);
      }).then(function (alias) {
        var div = panel.prepareElementDetailsContent(alias);
        assert.ok(div);
        return panel.destroy();
      });
    });
    it('for point', function () {
      var map;
      var panel;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);

        helper.createSearchDbOverlay(map);
        helper.createDrugDbOverlay(map);
        helper.createChemicalDbOverlay(map);

        panel = createLeftPanel(map);

        var element = new IdentifiedElement({
          id: "(1.00,2.00)",
          type: "POINT",
          modelId: map.getId()
        });
        return map.getModel().getByIdentifiedElement(element, true);
      }).then(function (point) {
        var div = panel.prepareElementDetailsContent(point);
        assert.ok(div);
        return panel.destroy();
      });
    });
  });

  describe('showElementDetails', function () {
    it('default', function () {
      var map;
      var panel;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);

        helper.createSearchDbOverlay(map);
        helper.createDrugDbOverlay(map);
        helper.createChemicalDbOverlay(map);

        panel = createLeftPanel(map);

        return panel.init();
      }).then(function () {

        var element = new IdentifiedElement({
          id: 329163,
          type: "ALIAS",
          modelId: map.getId()
        });
        return map.getModel().getByIdentifiedElement(element, true);
      }).then(function (alias) {
        return panel.showElementDetails(alias);
      }).then(function () {
        assert.notOk($(panel.elementInfoDiv).dialog('isOpen'));
        return panel.destroy();
      });
    });

    it('when panel is hidden', function () {
      var map;
      var panel;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);

        helper.createSearchDbOverlay(map);
        helper.createDrugDbOverlay(map);
        helper.createChemicalDbOverlay(map);

        panel = createLeftPanel(map);
        return panel.init();
      }).then(function () {
        panel.hide();

        var element = new IdentifiedElement({
          id: 329163,
          type: "ALIAS",
          modelId: map.getId()
        });
        return map.getModel().getByIdentifiedElement(element, true);
      }).then(function (alias) {
        return panel.showElementDetails(alias);
      }).then(function () {
        assert.notOk($(panel.elementInfoDiv).dialog('isOpen'));
        return panel.destroy();
      });
    });
    it('when different tab is active', function () {
      var map;
      var panel;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);

        helper.createSearchDbOverlay(map);
        helper.createDrugDbOverlay(map);
        helper.createChemicalDbOverlay(map);

        panel = createLeftPanel(map);
        return panel.init();
      }).then(function () {
        return $('a:contains("DRUG")').click();
      }).then(function () {

        var element = new IdentifiedElement({
          id: 329163,
          type: "ALIAS",
          modelId: map.getId()
        });
        return panel.showElementDetails(element);
      }).then(function () {
        assert.ok($(panel.elementInfoDiv).dialog('isOpen'));
        return panel.destroy();
      });
    });
  });

});
