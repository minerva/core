"use strict";

require('../../mocha-config.js');
var $ = require('jquery');

var AbstractDbOverlay = require('../../../../main/js/map/overlay/AbstractDbOverlay');
var Alias = require('../../../../main/js/map/data/Alias');
var ConfigurationType = require('../../../../main/js/ConfigurationType');
var GenericSearchPanel = require('../../../../main/js/gui/leftPanel/GenericSearchPanel');
var PanelControlElementType = require('../../../../main/js/gui/PanelControlElementType');
var Point = require('../../../../main/js/map/canvas/Point');
var ServerConnector = require('../../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('GenericSearchPanel', function () {
  /**
   *
   * @param {CustomMap} [map]
   * @returns {GenericSearchPanel}
   */
  var createPanel = function (map) {
    if (map===undefined) {
      map = helper.createCustomMap();
      map.getModel().setId(15781);
    }
    helper.createSearchDbOverlay(map);

    return new GenericSearchPanel({
      element: testDiv,
      customMap: map,
      configuration: helper.getConfiguration()
    });
  };

  it('constructor', function () {
    var map = helper.createCustomMap();
    helper.createSearchDbOverlay(map);

    new GenericSearchPanel({
      element: testDiv,
      customMap: map,
      configuration: helper.getConfiguration()
    });
    assert.equal(logger.getWarnings().length, 0);
  });

  it('on searchResults changed', function () {
    var originalGetBooleanFun = helper.getConfiguration().getBooleanValue;
    helper.getConfiguration().getBooleanValue = function () {
      return true;
    };

    var map = helper.createCustomMap();
    map.getModel().setId(15781);

    createPanel(map);
    var searchDbOverlay = map.getOverlayByName("search");

    var searchParams = {
      modelId: map.getModel().getId(),
      coordinates: new Point(457.51, 356.84),
      zoom: 4
    };

    return searchDbOverlay.searchByCoordinates(searchParams).then(function () {
      assert.equal(logger.getWarnings().length, 0);
      assert.ok(testDiv.innerHTML.indexOf("Reaction") >= 0);
    }).finally(function () {
      helper.getConfiguration().getBooleanValue = originalGetBooleanFun;
    });
  });

  it('on searchResults changed 2', function () {
    var map = helper.createCustomMap();
    map.getModel().setId(15781);
    createPanel(map);
    var searchDbOverlay = map.getOverlayByName("search");

    return searchDbOverlay.searchByQuery("s1", false).then(function () {
      assert.equal(logger.getWarnings().length, 0);
      assert.ok(testDiv.innerHTML.indexOf("s1") >= 0);
    });
  });

  it('search by query with no results', function () {
    var map = helper.createCustomMap();
    map.getModel().setId(15781);
    var panel = createPanel(map);

    panel.getControlElement(PanelControlElementType.SEARCH_INPUT).value = "s1";

    return panel.searchByQuery().then(function () {
      assert.equal(logger.getWarnings().length, 0);
      assert.ok(testDiv.innerHTML.indexOf("s1") >= 0);
    });
  });


  describe('createReactionElement', function () {
    describe('type in desc', function () {
      it('visible', function () {
        helper.getConfiguration().getOption(ConfigurationType.SHOW_REACTION_TYPE).setValue("true");
        var map = helper.createCustomMap();
        var panel = createPanel(map);
        var reaction = helper.createReaction(map);
        var reactionType = "XX TYPE";
        reaction.setType(reactionType);

        return panel.createReactionElement(reaction).then(function (div) {
          assert.ok(div.innerHTML.indexOf(reactionType) >= 0);
        });
      });

      it('invisible', function () {
        helper.getConfiguration().getOption(ConfigurationType.SHOW_REACTION_TYPE).setValue("true");
        var map = helper.createCustomMap();
        var panel = createPanel(map);
        var reaction = helper.createReaction(map);
        var reactionType = "XX TYPE";
        reaction.setType(reactionType);

        helper.getConfiguration().getOption(ConfigurationType.SHOW_REACTION_TYPE).setValue("false");
        return panel.createReactionElement(reaction).then(function (div) {
          assert.ok($("div.minerva-search-data-hidden:contains('" + reactionType + "')", div).length > 0);
        });
      });


    });
  });

  describe('createAliasElement', function () {
    it('default', function () {
      var map = helper.createCustomMap();

      var aliasObj = {
        symbol: "S1_SYMBOL",
        formerSymbols: [],
        modelId: map.getId(),
        synonyms: ["syn44"],
        description: "DESCRIPTION",
        type: "Simple molecule",
        abbreviation: "ABBREVIATION",
        name: "s1",
        bounds: {
          x: 170.0,
          y: 171.5,
          width: 70.0,
          height: 25.0
        },
        formula: "FORMULA",
        id: 380217,
        references: []
      };
      var alias = new Alias(aliasObj);


      var panel = createPanel(map);

      return panel.createAliasElement(alias).then(function (aliasDiv) {
        assert.ok(aliasDiv.innerHTML);
      });
    });


    describe('full name in desc', function () {
      it('visible', function () {
        var originalGetBooleanFun = helper.getConfiguration().getBooleanValue;
        helper.getConfiguration().getBooleanValue = function () {
          return true;
        };

        var map = helper.createCustomMap();
        var panel = createPanel(map);
        var alias = helper.createAlias(map);

        alias.setFullName("xxx");
        return panel.createAliasElement(alias).then(function (div) {

          assert.ok(div.innerHTML.indexOf("Full name") >= 0);
        }).finally(function () {
          helper.getConfiguration().getBooleanValue = originalGetBooleanFun;
        });
      });

      it('invisible', function () {
        var map = helper.createCustomMap();
        var panel = createPanel(map);
        var alias = helper.createAlias(map);

        alias.setFullName("");
        return panel.createAliasElement(alias).then(function (div) {
          assert.ok(div.innerHTML.indexOf("Full name") === -1);
        });

      });
    });
  });

  describe('clear', function () {
    it('input text', function () {
      var map = helper.createCustomMap();
      var panel = createPanel(map);

      $(".typeahead", testDiv)[0].value = "some input";
      return map.clearDbOverlays().then(function () {
        assert.equal("", $(":input", testDiv)[0].value);
      });
    });
  });

  it('clear after search by coordinates', function () {
    var map = helper.createCustomMap();
    map.getModel().setId(15781);
    var panel = createPanel(map);

    $("[name='searchInput']", panel.getElement()).val("some input");
    var searchParams = {
      modelId: map.getModel().getId(),
      coordinates: new Point(553.10, 479.18),
      zoom: 4
    };

    return panel.getOverlayDb().searchByCoordinates(searchParams).then(function () {
      assert.equal("", $("[name='searchInput']", panel.getElement()).val());
    });
  });

  describe("getAutocomplete", function () {
    it("without initialization", function () {
      var panel = createPanel();

      var t = panel.getAutocomplete("s");
      assert.ok(t);
      assert.equal(t.length, 0);
    });

    it("after initialization", function () {
      var panel = createPanel();

      return panel.refreshSearchAutocomplete().then(function () {
        var t = panel.getAutocomplete("s");
        assert.ok(t);
        assert.ok(t.length > 0);
      });
    });
  });

  describe("computeAutocompleteDictionary", function () {
    it("using prefixes", function () {
      var queries = ["text 1", "text 2", "text 3", "alphabet"];
      var panel = createPanel();

      var dictionary = panel.computeAutocompleteDictionary(queries);
      assert.ok(dictionary);
      assert.equal(dictionary["te"].length, 3);
    });

    it("using prefixes and inside", function () {
      var queries = ["text 1", "text 2", "text 3", "alphabet", "unknown"];
      var panel = createPanel();

      var dictionary = panel.computeAutocompleteDictionary(queries);
      assert.ok(dictionary);
      assert.equal(dictionary["t"].length, 4);
      assert.equal(dictionary["t"][0], "text 1");
      assert.equal(dictionary["t"][3], "alphabet");
    });

    it("function names as query list", function () {
      var queries = ["-pop", "some", "map"];
      var panel = createPanel();

      var dictionary = panel.computeAutocompleteDictionary(queries);
      assert.ok(dictionary);
      assert.equal(dictionary["p"].length, 2);
    });

  });

  describe('init', function () {
    it('default', function () {
      var panel = createPanel();
      return panel.init().then(function () {
        panel.destroy();
      });
    });
    it('map invalid session search query', function () {
      var panel = new createPanel();
      var unknownModelId = -1;
      var coordinates = new Point(0, 0);
      var zoom = 2;
      var encodedQuery = panel.getOverlayDb().encodeQuery(AbstractDbOverlay.QueryType.SEARCH_BY_COORDINATES, unknownModelId, coordinates, zoom);
      ServerConnector.getSessionData().setSearchQuery(encodedQuery);
      return panel.init().then(function () {
        assert.ok(logger.getWarnings().length > 0);
        panel.destroy();
      });
    });
  });

});
