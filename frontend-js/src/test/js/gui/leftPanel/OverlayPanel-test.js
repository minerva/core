"use strict";

require('../../mocha-config.js');
var $ = require('jquery');

var OverlayPanel = require('../../../../main/js/gui/leftPanel/OverlayPanel');
var ServerConnector = require('../../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

/**
 *
 * @param {CustomMap} map
 * @returns {OverlayPanel}
 */
function createOverlayPanel(map) {
  return new OverlayPanel({
    element: testDiv,
    customMap: map,
    configuration: helper.getConfiguration()
  });
}

describe('OverlayPanel', function () {

  it('constructor', function () {
    var map = helper.createCustomMap();

    var overlay = new OverlayPanel({
      element: testDiv,
      customMap: map
    });
    assert.equal(logger.getWarnings().length, 0);
    return overlay.destroy();
  });

  describe('refresh', function () {
    it('anonymous', function () {
      var panel;
      return ServerConnector.getProject("sample").then(function (project) {
        project.addOrUpdateBackgrounds([helper.createBackground()]);
        var map = helper.createCustomMap(project);

        panel = new OverlayPanel({
          element: testDiv,
          customMap: map,
          configuration: helper.getConfiguration()

        });
        return panel.init();
      }).then(function () {
        return panel.refresh();
      }).then(function () {
        assert.ok(panel.getElement().innerHTML.indexOf("testLayout") < 0);
        assert.ok(panel.getElement().innerHTML.indexOf("YOU ARE NOT LOGGED") >= 0);
        return panel.destroy();
      });
    });
    it('admin', function () {
      helper.loginAsAdmin();
      var panel;
      return ServerConnector.getProject("sample").then(function (project) {
        project.addOrUpdateBackgrounds([helper.createBackground()]);
        var map = helper.createCustomMap(project);

        panel = new OverlayPanel({
          element: testDiv,
          customMap: map,
          configuration: helper.getConfiguration()

        });

        return panel.init();
      }).then(function () {

        return panel.refresh();
      }).then(function () {
        assert.ok(panel.getElement().innerHTML.indexOf("testLayout") < 0);
        assert.ok(panel.getElement().innerHTML.indexOf("YOU ARE NOT LOGGED") < 0);
        return panel.destroy();
      });
    });
  });

  it('select background', function () {
    var panel;
    return ServerConnector.getProject().then(function (project) {
      panel = new OverlayPanel({
        element: testDiv,
        customMap: helper.createCustomMap(project),
        configuration: helper.getConfiguration()
      });
      return panel.init();
    }).then(function () {
      var backgroundLink = $("[name='overlayLink']", panel.getElement())[0];
      assert.ok($(backgroundLink).attr("data"));
      return helper.triggerJqueryEvent(backgroundLink, "click");
    }).then(function () {
      return panel.destroy();
    });
  });
  it('download', function () {
    var panel;
    return ServerConnector.getProject("sample").then(function (project) {
      var map = helper.createCustomMap(project);

      panel = new OverlayPanel({
        element: testDiv,
        customMap: map,
        configuration: helper.getConfiguration()

      });

      return panel.init();
    }).then(function () {
      return panel.refresh();
    }).then(function () {
      var buttons = panel.getElement().getElementsByTagName("button");
      var downloadButton;
      for (var i = 0; i < buttons.length; i++) {
        var name = buttons[i].getAttribute("name");
        if (name !== undefined && name !== null && name.indexOf("download-overlay") >= 0) {
          downloadButton = buttons[i];
        }
      }
      assert.ok(downloadButton);
      assert.notOk(panel.getLastDownloadUrl());
      return helper.triggerJqueryEvent(downloadButton, "click");
    }).then(function () {
      assert.ok(panel.getLastDownloadUrl());
      return panel.destroy();
    });
  });

  describe('openAddOverlayDialog', function () {
    it('open', function () {
      it('open', function () {
        var map = helper.createCustomMap();

        var panel = createOverlayPanel(map);

        return panel.openAddOverlayDialog().then(function () {
          return panel.destroy();
        });
      });

    });
  });

  describe('openEditOverlayDialog', function () {
    it('open for map', function () {
      helper.loginAsAdmin();
      var panel;
      return ServerConnector.getProject().then(function (project) {
        panel = new OverlayPanel({
          element: testDiv,
          customMap: helper.createCustomMap(project),
          configuration: helper.getConfiguration()
        });
        return panel.init();
      }).then(function () {
        var backgroundLink = $("[name='editButton']", panel.getElement())[0];
        assert.ok($(backgroundLink).attr("data"));
        return helper.triggerJqueryEvent(backgroundLink, "click");
      }).then(function () {
        return panel.destroy();
      });
    });

  });
});
