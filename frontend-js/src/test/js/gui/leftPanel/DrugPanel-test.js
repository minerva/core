"use strict";

require('../../mocha-config.js');

var AbstractDbOverlay = require('../../../../main/js/map/overlay/AbstractDbOverlay');
var Drug = require('../../../../main/js/map/data/Drug');
var DrugPanel = require('../../../../main/js/gui/leftPanel/DrugPanel');
var PanelControlElementType = require('../../../../main/js/gui/PanelControlElementType');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('DrugPanel', function () {

  function createPanel() {
    var map = helper.createCustomMap();
    map.getModel().setId(15781);
    helper.createDrugDbOverlay(map);

    return new DrugPanel({
      element: testDiv,
      customMap: map
    });

  }

  it('constructor', function () {
    var map = helper.createCustomMap();
    helper.createDrugDbOverlay(map);

    new DrugPanel({
      element: testDiv,
      customMap: map
    });
    assert.equal(logger.getWarnings().length, 0);
  });

  describe('init', function () {
    it('with drug query in session', function () {
      var panel;
      return ServerConnector.getProject().then(function (project) {
        var map = helper.createCustomMap(project);
        helper.createDrugDbOverlay(map);

        panel = new DrugPanel({
          element: testDiv,
          customMap: map
        });
        var query = panel.getOverlayDb().encodeQuery(AbstractDbOverlay.QueryType.SEARCH_BY_QUERY, "NADH");
        ServerConnector.getSessionData(project).setQuery({
          type: "drug",
          query: query
        });
        return panel.init();
      }).then(function () {
        panel.destroy();
      });
    });
  });


  it('create DrugPanel', function () {
    var map = helper.createCustomMap();
    helper.createDrugDbOverlay(map);

    var panel = new DrugPanel({
      element: testDiv,
      customMap: map
    });

    assert.ok(panel.createPreamble().innerHTML.indexOf("NOT FOUND") > 0);
  });

  it('create DrugPanel for empty drug', function () {
    var map = helper.createCustomMap();
    helper.createDrugDbOverlay(map);

    var panel = new DrugPanel({
      element: testDiv,
      customMap: map
    });

    assert.ok(panel.createPreamble(new Drug()).innerHTML.indexOf("NOT FOUND") > 0);
  });

  it('on searchResults changed', function () {
    return ServerConnector.getProject().then(function (project) {
      var map = helper.createCustomMap(project);
      var drugDbOverlay = helper.createDrugDbOverlay(map);

      new DrugPanel({
        element: testDiv,
        customMap: map
      });

      return drugDbOverlay.searchByQuery("aspirin");
    }).then(function () {
      assert.equal(logger.getWarnings().length, 0);
      assert.ok(testDiv.innerHTML.indexOf("Acetylsalicylic acid") >= 0);
    });
  });

  it('searchByQuery', function () {
    var panel = createPanel();
    panel.getControlElement(PanelControlElementType.SEARCH_INPUT).value = "aspirin";

    return panel.searchByQuery().then(function () {
      assert.equal(logger.getWarnings().length, 0);
      assert.ok(testDiv.innerHTML.indexOf("Acetylsalicylic acid") >= 0);
    });
  });

  it("refreshSearchAutocomplete", function () {
    var panel = createPanel();

    return panel.refreshSearchAutocomplete().then(function (data) {
      assert.ok(data);
    });
  });

});
