"use strict";

require('../../mocha-config.js');
var $ = require('jquery');

var AbstractDbPanel = require('../../../../main/js/gui/leftPanel/AbstractDbPanel');
var PanelControlElementType = require('../../../../main/js/gui/PanelControlElementType');
var ServerConnector = require('../../ServerConnector-mock');
var Target = require('../../../../main/js/map/data/Target');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('AbstractDbPanel', function () {

  it('constructor', function () {
    var div = document.createElement("div");

    var map = helper.createCustomMap();
    map.registerDbOverlay(helper.createSearchDbOverlay(map));

    var panel = new AbstractDbPanel({
      element: div,
      customMap: map,
      panelName: "search"
    });

    assert.ok(panel.getControlElement(PanelControlElementType.SEARCH_LABEL).innerHTML !== "");
    assert.equal(0, logger.getWarnings().length);
  });
  describe('createTargetRow', function () {
    it('target with elements', function () {
      var div = document.createElement("div");
      helper.setUrl("http://test-page/?id=complex_model_with_submaps");
      var map;

      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        map.registerDbOverlay(helper.createSearchDbOverlay(map));

        var panel = new AbstractDbPanel({
          element: div,
          customMap: map,
          panelName: "search"
        });

        var target = new Target({
          targetElements: [{
            id: 345337,
            modelId: 16731,
            type: "ALIAS"
          }, {
            id: 345338,
            modelId: 16731,
            type: "ALIAS"
          }],
          references: [],
          targetParticipants: [],
          name: "target name"
        });
        return panel.createTargetRow(target, "empty.png").then(function (htmlTag) {
          assert.ok(htmlTag);
          assert.equal(1, $(".minerva-open-submap-button", $(htmlTag)).length);
        });
      });
    });
  });
});
