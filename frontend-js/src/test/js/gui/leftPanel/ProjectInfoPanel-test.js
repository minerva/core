"use strict";

require('../../mocha-config.js');
var $ = require('jquery');

var ProjectInfoPanel = require('../../../../main/js/gui/leftPanel/ProjectInfoPanel');
var ServerConnector = require('../../ServerConnector-mock');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('ProjectInfoPanel', function () {

  it('constructor', function () {
    var div = testDiv;

    var map = helper.createCustomMap();

    var panel = new ProjectInfoPanel({
      element: div,
      customMap: map
    });
    assert.equal(logger.getWarnings().length, 0);
    panel.destroy();
  });

  it('refresh', function () {
    var div = testDiv;
    var panel;
    return ServerConnector.getProject().then(function (project) {
      panel = new ProjectInfoPanel({
        element: div,
        customMap: helper.createCustomMap(project)
      });
      return panel.refresh();
    }).then(function () {
      assert.ok(div.innerHTML.indexOf("UNKNOWN DISEASE MAP") >= 0);
      return panel.destroy();
    });
  });

  it('download source', function () {
    var div = testDiv;
    var panel = new ProjectInfoPanel({
      element: div,
      customMap: helper.createCustomMap()
    });
    return panel.downloadSourceFile().then(function () {
      return panel.destroy();
    });
  });

  it('open manual', function () {
    var div = testDiv;
    var panel = new ProjectInfoPanel({
      element: div,
      customMap: helper.createCustomMap()
    });
    return $("[name='manualLink']", $(div))[0].onclick().then(function () {
      return panel.destroy();
    });
  });

  it('show publication list', function () {
    var panel;
    return ServerConnector.getProject().then(function (project) {
      panel = new ProjectInfoPanel({
        element: testDiv,
        customMap: helper.createCustomMap(project)
      });
      return panel.showPublicationListDialog();
    }).then(function () {
      return panel.destroy();
    });
  });

  it('show profile', function () {
    var map = helper.createCustomMap();

    var panel = new ProjectInfoPanel({
      element: testDiv,
      customMap: map
    });

    var user = helper.createUser();
    panel.showUserProfilePage(user);
    panel.destroy();
  });

  it('list authors', function () {
    var map = helper.createCustomMap();
    map.getModel().getAuthors().push({firstName: "Piotr", lastName: "G", organisation: "LCSB", email: "a@a.lu"});
    map.getModel().getAuthors().push({firstName: "Joe", lastName: "G", organisation: "LCSB", email: "a@a.lu"});

    var panel = new ProjectInfoPanel({
      element: testDiv,
      customMap: map
    });

    assert.ok(testDiv.innerHTML.indexOf("Piotr") >= 0);
    assert.ok(testDiv.innerHTML.indexOf("Joe") >= 0);

    panel.destroy();
  });

});
