"use strict";

require('../../mocha-config.js');
var $ = require('jquery');

var Alias = require('../../../../main/js/map/data/Alias');
var Annotation = require('../../../../main/js/map/data/Annotation');
var GuiUtils = require('../../../../main/js/gui/leftPanel/GuiUtils');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../../logger');

describe('GuiUtils', function () {
  function createGuiUtils(map) {
    var result = new GuiUtils(helper.getConfiguration());
    if (map !== undefined) {
      result.setMap(map);
    }
    return result;
  }

  var originalGetBooleanFun;
  beforeEach(function () {
    originalGetBooleanFun = helper.getConfiguration().getBooleanValue;
    helper.getConfiguration().getBooleanValue = function () {
      return true;
    }
  });
  afterEach(function () {
    helper.getConfiguration().getBooleanValue = originalGetBooleanFun;
  });

  it('constructor', function () {
    var map = helper.createCustomMap();
    helper.createSearchDbOverlay(map);

    new GuiUtils(helper.getConfiguration());
    assert.equal(logger.getWarnings().length, 0);
  });

  it('default', function () {
    var aliasObj = {
      symbol: "S1_SYMBOL",
      formerSymbols: [],
      modelId: 15781,
      synonyms: ["syn44"],
      description: "DESCRIPTION",
      type: "Simple molecule",
      abbreviation: "ABBREVIATION",
      name: "s1",
      bounds: {
        x: 170.0,
        y: 171.5,
        width: 70.0,
        height: 25.0
      },
      formula: "FORMULA",
      id: 380217,
      references: []
    };
    var alias = new Alias(aliasObj);

    var map = helper.createCustomMap();
    map.getModel().setId(15781);
    helper.createSearchDbOverlay(map);

    var guiUtils = createGuiUtils(map);

    return guiUtils.createAliasElement({alias: alias}).then(function (aliasDiv) {
      assert.ok(aliasDiv.innerHTML);
    });
  });

  it('createLabelText for undefined', function () {
    var map = helper.createCustomMap();

    var guiUtils = createGuiUtils(map);

    var res = guiUtils.createLabelText();
    assert.notOk(res.innerHTML);
  });

  it('createSubMapLink', function () {
    var map = helper.createCustomMap();

    var guiUtils = createGuiUtils(map);

    var res = guiUtils.createSubMapLink({label: "TEST", mapId: map.getId()});
    assert.ok(res.innerHTML);
  });

  it('createPostTranslationalModifications', function () {
    var modifications = [{
      "name": "S63",
      "state": "PHOSPHORYLATED"
    }, {
      "name": "S73",
      "state": "PHOSPHORYLATED"
    }, {
      "name": "",
      "state": "PHOSPHORYLATED"
    }, {
      "name": null,
      "state": "PHOSPHORYLATED"
    }];
    var utils = new GuiUtils(helper.getConfiguration());

    var res = utils.createPostTranslationalModifications("label", modifications);
    assert.ok(res.innerHTML);
    assert.ok(res.innerHTML.indexOf("phosphorylated") >= 0);
  });

  describe('createReactionElement', function () {
    it('type in desc', function () {
      var map = helper.createCustomMap();
      helper.createSearchDbOverlay(map);

      var guiUtils = createGuiUtils(map);

      var reaction = helper.createReaction(map);
      var reactionId = "XX ID";
      reaction.setReactionId(reactionId);

      return guiUtils.createReactionElement({reaction: reaction}).then(function (div) {
        assert.ok(div.innerHTML.indexOf(reactionId) >= 0);
      })
    });
    it('in submap', function () {
      var project = helper.createProject();
      project.addModel(helper.createModel());
      var map = helper.createCustomMap(project);
      helper.createSearchDbOverlay(map);

      var guiUtils = createGuiUtils(map);

      var reaction = helper.createReaction(map);
      var reactionId = "XX ID";

      reaction.setReactionId(reactionId);
      reaction.setModelId(project.getModels()[1].getId());

      return guiUtils.createReactionElement({reaction: reaction}).then(function (div) {
        assert.ok(div.innerHTML.indexOf(reactionId) >= 0);
      })
    });
  });

  describe('createAliasElement', function () {
    it('full name in desc visible', function () {
      var map = helper.createCustomMap();
      helper.createSearchDbOverlay(map);

      var guiUtils = createGuiUtils(map);
      var alias = helper.createAlias(map);

      alias.setFullName("xxx");
      return guiUtils.createAliasElement({alias: alias}).then(function (div) {
        assert.ok(div.innerHTML.indexOf("Full name") >= 0);
      });
    });

    it('compartment visible', function () {
      var map = helper.createCustomMap();
      helper.createSearchDbOverlay(map);

      var guiUtils = createGuiUtils(map);
      var alias = helper.createAlias(map);
      var compartment = helper.createAlias(map);
      compartment.setName("compartment_name");
      compartment.setType("XYZ");

      alias.setCompartmentId(compartment.getId());

      return guiUtils.createAliasElement({alias: alias}).then(function (div) {
        assert.ok(div.innerHTML.indexOf(compartment.getName()) >= 0);
        assert.ok(div.innerHTML.indexOf(compartment.getType()) >= 0);
      });
    });


    it('full name in desc invisible', function () {
      var map = helper.createCustomMap();
      helper.createSearchDbOverlay(map);

      var guiUtils = createGuiUtils(map);
      var alias = helper.createAlias(map);

      alias.setFullName("");
      return guiUtils.createAliasElement({alias: alias}).then(function (div) {
        assert.ok(div.innerHTML.indexOf("Full name") === -1);
      });
    });

    it('onclick function', function () {
      var map = helper.createCustomMap();
      helper.createSearchDbOverlay(map);

      var guiUtils = createGuiUtils(map);
      var alias = helper.createAlias(map);

      alias.setFullName("xxx");
      return guiUtils.createAliasElement({
        alias: alias,
        icon: "empty.png"
      }).then(function (htmlElement) {
        var img = $("img", htmlElement)[0];

        return img.onclick();
      });
    });
  });

  describe('createLink', function () {
    it('normal', function () {
      var guiUtils = createGuiUtils();
      var link = guiUtils.createLink("http://www.minerva.uni.lu", "PD map");
      assert.ok(link);
      assert.equal(0, logger.getWarnings().length);
    });

    it('with null link', function () {
      var guiUtils = createGuiUtils();
      // noinspection JSCheckFunctionSignatures
      var link = guiUtils.createLink(null, "PD map");
      assert.ok(link);
      assert.ok(logger.getWarnings().length > 0);
    });
  });
  describe('createAnnotationLink', function () {
    it('with encoded part in resource', function () {
      // noinspection SpellCheckingInspection
      var annotation = new Annotation({
        resource: "10.1038%2Fnpjsba.2016.20",
        link: "http://doi.org/10.1038%2Fnpjsba.2016.20",
        id: 1116748,
        type: "DOI"
      });
      var guiUtils = new GuiUtils(helper.getConfiguration());
      var link = guiUtils.createAnnotationLink(annotation);
      assert.ok(link);
      assert.equal(0, logger.getWarnings().length);
      assert.ok(link.innerHTML.indexOf("%2F") < 0, "url contains encoded %2F part of id");
    });

  });
  describe('createHelpButton', function () {
    it('default', function () {
      var guiUtils = new GuiUtils(helper.getConfiguration());
      var button = guiUtils.createHelpButton("test tooltip");
      button.onclick(new MouseEvent("click"));
      $(".ui-dialog-titlebar-close").click();
    });
    it('click twice', function () {
      var guiUtils = new GuiUtils(helper.getConfiguration());
      var button = guiUtils.createHelpButton("test tooltip");
      button.onclick(new MouseEvent("click"));
      // noinspection JSJQueryEfficiency
      var counts = $("div").length;
      button.onclick(new MouseEvent("click"));
      // noinspection JSJQueryEfficiency
      var counts2 = $("div").length;
      assert.ok(counts >= counts2);
      $(".ui-dialog-titlebar-close").click();
    });
  });

  describe('createAnnotationList', function () {
    it('default', function () {
      var guiUtils = new GuiUtils(helper.getConfiguration());
      var annotation = helper.createAnnotation();
      var element = guiUtils.createAnnotationList([annotation]);
      assert.ok(element.innerHTML.indexOf("Annotated by curator") >= 0);
    });
    it('without annotator type separation', function () {
      var guiUtils = new GuiUtils(helper.getConfiguration());
      var annotation = helper.createAnnotation();
      var element = guiUtils.createAnnotationList([annotation], {groupAnnotations: false});
      assert.equal(element.innerHTML.indexOf("Annotated by curator"), -1, "annotator type description shouldn't be visible");
    });
  });

  describe('createModificationRow', function () {
    describe('PTM', function () {
      it('default', function () {
        var guiUtils = new GuiUtils(helper.getConfiguration());
        var li = guiUtils.createModificationRow({name: "n", type: "RESIDUE", state: "PHOSPHORYLATED"});
        assert.ok(li);
      });
      it('empty PTM', function () {
        var guiUtils = new GuiUtils(helper.getConfiguration());
        var li = guiUtils.createModificationRow({name: "n", type: "RESIDUE"});
        assert.notOk(li);
      });
    });
    describe('Modification Site', function () {
      it('default', function () {
        var guiUtils = new GuiUtils(helper.getConfiguration());
        var li = guiUtils.createModificationRow({name: "n", type: "MODIFICATION_SITE", state: "PHOSPHORYLATED"});
        assert.ok(li);
      });
      it('empty', function () {
        var guiUtils = new GuiUtils(helper.getConfiguration());
        var li = guiUtils.createModificationRow({name: "n", type: "MODIFICATION_SITE"});
        assert.notOk(li);
      });
    });
    describe('Binding Region', function () {
      it('default', function () {
        var guiUtils = new GuiUtils(helper.getConfiguration());
        var li = guiUtils.createModificationRow({name: "n", type: "BINDING_REGION"});
        assert.ok(li);
      });
      it('empty', function () {
        var guiUtils = new GuiUtils(helper.getConfiguration());
        var li = guiUtils.createModificationRow({type: "BINDING_REGION"});
        assert.notOk(li);
      });
    });
    describe('Coding Region', function () {
      it('default', function () {
        var guiUtils = new GuiUtils(helper.getConfiguration());
        var li = guiUtils.createModificationRow({name: "n", type: "CODING_REGION"});
        assert.ok(li);
      });
      it('empty', function () {
        var guiUtils = new GuiUtils(helper.getConfiguration());
        var li = guiUtils.createModificationRow({type: "CODING_REGION"});
        assert.notOk(li);
      });
    });
    describe('Protein binding region', function () {
      it('default', function () {
        var guiUtils = new GuiUtils(helper.getConfiguration());
        var li = guiUtils.createModificationRow({name: "n", type: "PROTEIN_BINDING_DOMAIN"});
        assert.ok(li);
      });
      it('empty', function () {
        var guiUtils = new GuiUtils(helper.getConfiguration());
        var li = guiUtils.createModificationRow({type: "PROTEIN_BINDING_DOMAIN"});
        assert.notOk(li);
      });
    });
    describe('Regulatory region', function () {
      it('default', function () {
        var guiUtils = new GuiUtils(helper.getConfiguration());
        var li = guiUtils.createModificationRow({name: "n", type: "REGULATORY_REGION"});
        assert.ok(li);
      });
      it('empty', function () {
        var guiUtils = new GuiUtils(helper.getConfiguration());
        var li = guiUtils.createModificationRow({type: "REGULATORY_REGION"});
        assert.notOk(li);
      });
    });
    describe('Transcription sites', function () {
      it('default', function () {
        var guiUtils = new GuiUtils(helper.getConfiguration());
        var li = guiUtils.createModificationRow({name: "n", type: "TRANSCRIPTION_SITE_LEFT"});
        assert.ok(li);
      });
      it('empty', function () {
        var guiUtils = new GuiUtils(helper.getConfiguration());
        var li = guiUtils.createModificationRow({type: "TRANSCRIPTION_SITE_LEFT"});
        assert.notOk(li);
      });
    });
  });


});
