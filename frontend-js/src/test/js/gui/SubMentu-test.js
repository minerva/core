"use strict";

require("../mocha-config.js");

var SubMenu = require('../../../main/js/gui/SubMenu');

var chai = require('chai');
var assert = chai.assert;
var logger = require('../logger');

describe('SubMenu', function () {

  it('constructor', function () {
    var map = helper.createCustomMap();

    var menu = new SubMenu({
      element: testDiv,
      customMap: map
    });

    menu.addOption({name: "xxx"});

    assert.equal(logger.getWarnings().length, 0);
  });
});
