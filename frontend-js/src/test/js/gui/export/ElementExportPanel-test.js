"use strict";

require("../../mocha-config.js");
var $ = require('jquery');

var ElementExportPanel = require('../../../../main/js/gui/export/ElementExportPanel');
var ValidationError = require('../../../../main/js/ValidationError');
var MiriamType = require('../../../../main/js/map/data/MiriamType');
var ServerConnector = require('../../ServerConnector-mock');
var logger = require('../../logger');

var chai = require('chai');
var assert = chai.assert;

describe('ElementExportPanel', function () {

  function createExportPanel(project) {
    return new ElementExportPanel({
      element: testDiv,
      project: project,
      serverConnector: ServerConnector,
      configuration: helper.getConfiguration()
    });
  }

  describe('getSelectedTypes', function () {
    it('select single element', function () {
      var exportObject;
      var project;
      return ServerConnector.getProject().then(function (result) {
        project = result;
        exportObject = createExportPanel(project);
        return exportObject.init();
      }).then(function () {
        $("input[name='Degraded']", $(testDiv)).each(function (index, element) {
          $(element).prop("checked", true);
        });
        return exportObject.getSelectedTypes();
      }).then(function (selectedTypes) {
        assert.equal(0, logger.getWarnings().length);
        assert.deepEqual(["Degraded"], selectedTypes);
      });
    });
    it('select all', function () {
      var exportObject;
      var project;

      function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
      }

      var elementTypes = helper.getConfiguration().getSimpleElementTypeNames().filter(onlyUnique);

      return ServerConnector.getProject().then(function (result) {
        project = result;
        exportObject = createExportPanel(project);
        return exportObject.init();
      }).then(function () {
        $("input[name='Degraded']", $(testDiv)).each(function (index, element) {
          $(element).prop("checked", true);
        });
        $("input[name='ALL']", $(testDiv)).each(function (index, element) {
          $(element).prop("checked", true);
        });
        return exportObject.getSelectedTypes();
      }).then(function (selectedTypes) {
        assert.deepEqual(elementTypes, selectedTypes);
      });
    });
  });

  describe('getSelectedColumns', function () {
    it('select single element', function () {
      var exportObject;
      var project;
      return ServerConnector.getProject().then(function (result) {
        project = result;
        exportObject = createExportPanel(project);
        return exportObject.init();
      }).then(function () {
        $("input[name='column_name']", $(testDiv)).each(function (index, element) {
          $(element).prop("checked", true);
        });
        return exportObject.getSelectedColumns();
      }).then(function (selectedTypes) {
        assert.equal(["name"], selectedTypes[0].columnName);
      });
    });
    it('select all', function () {
      var exportObject;
      var project;
      return ServerConnector.getProject().then(function (result) {
        project = result;
        exportObject = createExportPanel(project);
        return exportObject.init();
      }).then(function () {
        $("input[name='column_name']", $(testDiv)).each(function (index, element) {
          $(element).prop("checked", true);
        });
        $("input[name='ALL']", $(testDiv)).each(function (index, element) {
          $(element).prop("checked", true);
        });
        return exportObject.getSelectedColumns();
      }).then(function (selectedTypes) {
        assert.equal(exportObject.getAllColumns().length, selectedTypes.length);
      });
    });
  });

  describe('getSelectedMiriamTypes', function () {
    it('select single element', function () {
      var exportObject;
      var project;
      return ServerConnector.getProject().then(function (result) {
        project = result;
        exportObject = createExportPanel(project);
        return exportObject.init();
      }).then(function () {
        var dlb = exportObject.getMiriamTypesDualListbox();
        var listItem = dlb.available[0];
        dlb.addSelected(listItem);
        return exportObject.getSelectedMiriamTypes();
      }).then(function (result) {
        assert.equal(result.length, 1);
        assert.ok(result[0] instanceof MiriamType);
      });
    });
  });

  describe('getSelectedIncludedCompartments', function () {
    it('empty selection', function () {
      helper.setUrl("http://test-page/?id=complex_model_with_submaps");

      var exportObject;
      var project;
      return ServerConnector.getProject().then(function (result) {
        project = result;
        exportObject = createExportPanel(project);
        return exportObject.init();
      }).then(function () {
        assert.ok(exportObject.getIncludedCompartmentsDualListbox().available.length > 0);
        return exportObject.getSelectedIncludedCompartments();
      }).then(function (result) {
        assert.equal(result.length, 0);
      });
    });
  });

  describe('getSelectedExcludedCompartments', function () {
    it('empty selection', function () {
      helper.setUrl("http://test-page/?id=complex_model_with_submaps");

      var exportObject;
      var project;
      return ServerConnector.getProject().then(function (result) {
        project = result;
        exportObject = createExportPanel(project);
        return exportObject.init();
      }).then(function () {
        var dlb = exportObject.getExcludedCompartmentsDualListbox();
        var listItem = dlb.available[0];
        dlb.addSelected(listItem);
        return exportObject.getSelectedExcludedCompartments();
      }).then(function (result) {
        assert.equal(result.length, 1);
      });
    });
  });

  describe('createResponseString', function () {
    it('get all', function () {
      var exportObject;
      var project;
      return ServerConnector.getProject().then(function (result) {
        project = result;
        exportObject = createExportPanel(project);
        return exportObject.init();
      }).then(function () {
        $("input[name='ALL']", $(testDiv)).each(function (index, element) {
          $(element).prop("checked", true);
        });
        var dlb = exportObject.getMiriamTypesDualListbox();
        var listItem = dlb.available[0];
        dlb.addSelected(listItem);
        return exportObject.createResponseString();
      }).then(function (result) {
        // protein id
        assert.equal(result.indexOf("[object Object]"), -1);
        assert.ok(result.indexOf("329156") >= 0);
      });
    });
    it('get invalid', function () {
      var exportObject;
      var project;
      return ServerConnector.getProject().then(function (result) {
        project = result;
        exportObject = createExportPanel(project);
        return exportObject.init();
      }).then(function () {
        return exportObject.createResponseString();
      }).then(null, function (error) {
        assert.ok(error instanceof ValidationError);
      });
    });
  });

  describe('createResponseRow', function () {
    it('description with new line', function () {
      var exportObject = createExportPanel(helper.createProject());
      var alias = helper.createAlias(exportObject.getProject().getModels()[0]);
      var desc = "test\ntest2\n";
      alias.setDescription(desc);
      return exportObject.createResponseRow(alias, exportObject.getAllColumns(), []).then(function (rowString) {
        assert.ok(rowString.indexOf(desc) === -1);
      });
    });
    it('complex name', function () {
      var complexName = "Complex name2";
      var exportObject = createExportPanel(helper.createProject());
      var alias = helper.createAlias(exportObject.getProject().getModels()[0]);
      var complex = helper.createAlias(exportObject.getProject().getModels()[0]);
      complex.setName(complexName);
      alias.setComplexId(complex.getId());
      return exportObject.createResponseRow(alias, exportObject.getAllColumns(), []).then(function (rowString) {
        assert.ok(rowString.indexOf(complexName) >= 0);
      });
    });
    it('compartment name', function () {
      var compartmentName = "Compartment name2";
      var exportObject = createExportPanel(helper.createProject());
      var alias = helper.createAlias(exportObject.getProject().getModels()[0]);
      var compartment = helper.createAlias(exportObject.getProject().getModels()[0]);
      compartment.setName(compartmentName);
      alias.setCompartmentId(compartment.getId());
      return exportObject.createResponseRow(alias, exportObject.getAllColumns(), []).then(function (rowString) {
        assert.ok(rowString.indexOf(compartmentName) >= 0);
      });
    });
    it('column with function manipulating data', function () {
      var exportObject = createExportPanel(helper.createProject());
      var alias = helper.createAlias();
      return exportObject.createResponseRow(alias, [{
        "columnName": "id",
        "method": function (alias) {
          return "Alias id: " + alias.getId();
        },
        "name": "Id"
      }], []).then(function (rowString) {
        assert.ok(rowString.indexOf("Alias id") >= 0);
        assert.ok(rowString.indexOf(alias.getId() + "") >= 0);
      });
    });
  });

});
