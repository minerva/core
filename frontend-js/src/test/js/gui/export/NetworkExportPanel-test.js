"use strict";

require("../../mocha-config");
var $ = require('jquery');

var NetworkExportPanel = require('../../../../main/js/gui/export/NetworkExportPanel');
var ServerConnector = require('../../ServerConnector-mock');

var logger = require('../../logger');
var chai = require('chai');
var assert = chai.assert;

describe('NetworkExportPanel', function () {

  function createExportPanel(project) {
    return new NetworkExportPanel({
      element: testDiv,
      project: project,
      serverConnector: ServerConnector,
      configuration: helper.getConfiguration()
    });
  }

  it('createResponseString', function () {
    var exportObject;
    var project;
    helper.setUrl("http://test-page/?id=complex_model_with_submaps");
    return ServerConnector.getProject().then(function (result) {
      project = result;
      exportObject = createExportPanel(project);
      return exportObject.init();
    }).then(function () {
      $("input[name='ALL']", $(testDiv)).each(function (index, element) {
        $(element).prop("checked", true);
      });
      assert.equal(0, logger.getWarnings().length);
      return exportObject.createResponseString();
    });
  });

  describe('createResponseRow', function () {
    it('map name', function () {
      var mapName = "map name2";
      var project = helper.createProject();
      var exportObject = createExportPanel(project);
      var alias = helper.createAlias(exportObject.getProject().getModels()[0]);
      project.getModels()[0].setName(mapName);
      var reaction = helper.createReaction(project.getModels()[0], true);
      return exportObject.createResponseRow(reaction, exportObject.getAllColumns(), [], []).then(function (rowString) {
        assert.ok(rowString.indexOf(mapName) >= 0);
      });
    });
  });

});
