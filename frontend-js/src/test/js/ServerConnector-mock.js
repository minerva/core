'use strict';

var Promise = require("bluebird");

// noinspection JSUnusedLocalSymbols
var logger = require('./logger');

var OriginalServerConnector = require('../../main/js/ServerConnector');
var NetworkError = require('../../main/js/NetworkError');

var fs = require('fs');
var request = require('request');
var chai = require('chai');
var assert = chai.assert;

var ServerConnectorMock = OriginalServerConnector;

function replaceAsterisk(str) {
  return str.replace(/\*/g, "all").replace(/:/g, ".");
}

function urlToFileName(url) {
  var result = url;
  assert.equal(-1, url.indexOf("//"), "Invalid url: " + url);
  var token = OriginalServerConnector.getSessionData().getToken();
  if (token !== undefined && token !== "" && url.indexOf("./testFiles/apiCalls") === 0) {
    if (!result.endsWith("&") && !result.endsWith("_")) {
      result += "/";
    }
    result += "token=" + token + "&";
  }
  return replaceAsterisk(result);
}

function encodeParams(params) {
  var result = {};
  for (var key in params) {
    if (params.hasOwnProperty(key) && params[key] !== undefined) {
      result[key] = encodeURIComponent(params[key]);
    }
  }
  return result;
}

ServerConnectorMock.checkIfUserLogoutFromDifferentTab = function () {
  logger.debug("checkIfUserLogoutFromDifferentTab is ignored");
};
ServerConnectorMock.isSessionValid = function () {
  return Promise.resolve(true);
};

ServerConnectorMock._sendRequest = function (params) {
  var self = this;
  var url = params.url;
  var prefix = params.method;
  if (prefix === "GET") {
    prefix = "";
  } else {
    prefix = prefix + "_";
    if (!url.endsWith("/")) {
      prefix = "/" + prefix;
    }
  }
  var suffix = "";
  if (params.form !== undefined) {
    suffix += self.createGetParams(encodeParams(params.form));
  }
  if (params.json !== undefined) {
    suffix += self.createGetParams(params.json);
  }
  return new Promise(function (resolve, reject) {
    if (url.indexOf("http") === 0) {
      request.get(url, function (error, response, body) {
        if (error) {
          reject(error);
        } else if (response.statusCode !== 200) {
          reject(response);
        } else {
          resolve(body);
        }
      });
    } else {
      var fileName = urlToFileName(url + prefix + suffix);
      fs.readFile(fileName, 'utf8', function (err, content) {
        if (err) {
          logger.warn("File doesn't exists: " + fileName);
          reject(new NetworkError(err.message, {
            content: content,
            url: fileName,
            statusCode: 404
          }));
        } else {
          resolve(content);
        }
      });
    }
  });
};

ServerConnectorMock.Original = {
  getApiBaseUrl: ServerConnectorMock.getApiBaseUrl,
  getNewApiBaseUrl: ServerConnectorMock.getNewApiBaseUrl,
  getApiUrl: ServerConnectorMock.getApiUrl
}

ServerConnectorMock.Mock = {
  getApiBaseUrl: function () {
    return "./testFiles/apiCalls/";
  },
  getNewApiBaseUrl: function () {
    return "./testFiles/apiCallsNew/";
  },
  getApiUrl: function (paramObj) {
    // replace '?' (or '/?') with '/'
    // the call is done on ServerConnectorObject (so 'this' is set properly)
    return ServerConnectorMock.Original.getApiUrl.call(this, paramObj).replace(/\/?\?/g, '/');
  }
}

ServerConnectorMock.useMock = function () {
  ServerConnectorMock.getApiBaseUrl = ServerConnectorMock.Mock.getApiBaseUrl;
  ServerConnectorMock.getApiUrl = ServerConnectorMock.Mock.getApiUrl;
  ServerConnectorMock.getNewApiBaseUrl = ServerConnectorMock.Mock.getNewApiBaseUrl;

}

ServerConnectorMock.useOriginal = function () {
  ServerConnectorMock.getApiBaseUrl = ServerConnectorMock.Original.getApiBaseUrl;
  ServerConnectorMock.getApiUrl = ServerConnectorMock.Original.getApiUrl;
  ServerConnectorMock.getNewApiBaseUrl = ServerConnectorMock.Original.getNewApiBaseUrl;
}

ServerConnectorMock.useMock();

module.exports = ServerConnectorMock;
