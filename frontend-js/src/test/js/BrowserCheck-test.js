"use strict";

require('./mocha-config');

// noinspection JSUnusedLocalSymbols
var Promise = require("bluebird");
var BrowserCheck = require("../../main/js/BrowserCheck");

var chai = require('chai');
var assert = chai.assert;

var originalNavigator = null;
describe('check browser', function () {

  beforeEach(function () {
    originalNavigator = global.navigator;
  });

  afterEach(function () {
    global.navigator = originalNavigator;
    global.alert = undefined;
  });

  it('browser detection simulation with unsupported detection browser', function () {
    global.alert = function () {
      assert.notOk("browser info should be fine");
    };
    BrowserCheck();
  });

  it('browser detection simulation with IE', function () {
    global.navigator = {
      appName: 'Microsoft Internet Explorer',
      userAgent: "MSIE 7.0"
    };
    var alertCalled = false;
    global.alert = function () {
      alertCalled = true;
    };
    BrowserCheck();
    assert.ok(alertCalled, "expected alert with info about invalid browser");
  });

  it('browser detection simulation with other', function () {
    global.navigator = {
      appName: 'Netscape',
      userAgent: "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0"
    };

    global.alert = function () {
      assert.notOk("browser info should be fine");
    };
    BrowserCheck();
  });

});
