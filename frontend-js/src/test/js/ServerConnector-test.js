"use strict";

require("./mocha-config.js");

var Promise = require("bluebird");

var HttpStatus = require('http-status-codes');

var Alias = require('../../main/js/map/data/Alias');
var Configuration = require('../../main/js/Configuration');
var DataOverlay = require('../../main/js/map/data/DataOverlay');
var LayoutAlias = require('../../main/js/map/data/LayoutAlias');
var MapModel = require('../../main/js/map/data/MapModel');
var NetworkError = require('../../main/js/NetworkError');
var Project = require('../../main/js/map/data/Project');
var Point = require('../../main/js/map/canvas/Point');
var Reaction = require('../../main/js/map/data/Reaction');
var ServerConnector = require('../../main/js/ServerConnector');
var SecurityError = require('../../main/js/SecurityError');

var logger = require('./logger');
var chai = require('chai');
var assert = chai.assert;

describe('ServerConnector', function () {
  describe('getProject', function () {
    it('default', function () {
      return ServerConnector.getProject().then(function (result) {
        assert.ok(result instanceof Project);
        assert.equal(result.getProjectId(), "sample");
        assert.equal(logger.getWarnings().length, 0);
      });
    });
    it('invalid project id', function () {
      return ServerConnector.getProject("invalid_project_id").then(function (result) {
        assert.equal(result, null);
      });
    });
    it('caching', function () {
      var project;
      return ServerConnector.getProject().then(function (result) {
        project = result;
        return ServerConnector.getProject();

      }).then(function (result) {
        assert.ok(result === project);
      });
    });
  });

  describe('updateProject', function () {
    it('default', function () {
      var project;
      var newVersion = "2.01";
      return ServerConnector.getProject().then(function (result) {
        project = result;
        project.setVersion(newVersion);
        return ServerConnector.updateProject(project);
      }).then(function (result) {
        assert.ok(project === result);
        assert.equal(newVersion, result.getVersion());
      });
    });

    it('empty data', function () {
      var project = new Project({projectId: "empty"});

      return ServerConnector.updateProject(project);
    });
  });
  describe('removeProject', function () {
    it('default', function () {
      var project;
      return ServerConnector.getProject().then(function (result) {
        project = result;
        return ServerConnector.removeProject(project.getProjectId());
      }).then(function (result) {
        assert.ok(project === result);
      });
    });
  });

  it('getModels', function () {
    return ServerConnector.getModels("sample").then(function (models) {
      assert.equal(1, models.length);
      assert.ok(models[0] instanceof MapModel);
    });
  });

  it('getPublications', function () {
    return ServerConnector.getPublications().then(function (result) {
      assert.equal(result.totalSize, 12);
    });
  });

  it('getProjectId from GET params', function () {
    helper.setUrl("http://test-page/?id=test");
    return ServerConnector.getProjectId().then(function (result) {
      assert.equal(result, "test");
    });
  });

  describe('getReactions', function () {
    it('with empty list of ids', function () {
      return ServerConnector.getReactions({ids: []}).then(function (result) {
        assert.equal(result.length, 28);
        var reaction = result[0];
        assert.ok(reaction instanceof Reaction);
        assert.equal(reaction.getId(), 153524);
        assert.equal(reaction.getModelId(), 15781);
      });
    });
    it('without ids', function () {
      return ServerConnector.getReactions({}).then(function (result) {
        assert.equal(result.length, 28);
        var reaction = result[0];
        assert.ok(reaction instanceof Reaction);
        assert.equal(reaction.getId(), 153524);
        assert.equal(reaction.getModelId(), 15781);
      });
    });

    it('with ids', function () {
      return ServerConnector.getReactions({
        modelId: 15781,
        participantId: [329167]
      }).then(function (result) {
        assert.equal(result.length, 5);
        var reaction = result[0];
        assert.ok(reaction instanceof Reaction);
        assert.equal(reaction.getId(), 153518);
        assert.equal(reaction.getModelId(), 15781);
      });
    });
    it('with ids when asking for too many elements for get query', function () {
      ServerConnector.MAX_NUMBER_OF_IDS_IN_GET_QUERY = 0;
      return ServerConnector.getReactions({
        modelId: 15781,
        participantId: [329167]
      }).then(function (result) {
        assert.equal(result.length, 5);
        var reaction = result[0];
        assert.ok(reaction instanceof Reaction);
        assert.equal(reaction.getId(), 153518);
        assert.equal(reaction.getModelId(), 15781);
      });
    });
  });


  it('getElements with empty list of ids', function () {
    return ServerConnector.getAliases({}).then(function (result) {
      assert.equal(result.length, 30);
      var alias = result[0];
      assert.ok(alias instanceof Alias);
      assert.equal(alias.getModelId(), 15781);
    });
  });

  it('getOverlayElements', function () {
    return ServerConnector.getOverlayElements(18077).then(function (result) {
      assert.equal(result.length, 1);
      var layoutAlias = result[0];
      assert.ok(layoutAlias instanceof LayoutAlias);
      assert.equal(-65536, layoutAlias.getColor().rgb);
      assert.equal(15781, layoutAlias.getModelId());
      assert.equal(329163, layoutAlias.getId());
    });
  });

  it('idsToString', function () {
    var ids = [3, 2, 9, 1, 6, 8, 3, 2, 9, 1, 7, 3];
    var str = ServerConnector.idsToString(ids);
    assert.equal(str, "1,2,3,6,7,8,9");
  });

  it('getOverlaySourceDownloadUrl', function () {
    var id = 18083;
    return ServerConnector.getOverlaySourceDownloadUrl({
      overlayId: id
    }).then(function (url) {
      assert.ok(url);
      assert.ok(url.indexOf(id) >= 0);
      return ServerConnector.sendGetRequest(url);
    });
  });

  it('getImageDownloadUrl', function () {
    var modelId = 15781;
    return ServerConnector.getImageDownloadUrl({
      modelId: modelId,
      handlerClass: "lcsb.mapviewer.converter.graphics.PngImageGenerator"
    }).then(function (url) {
      assert.ok(url);
      assert.ok(url.indexOf(modelId) >= 0);
      return ServerConnector.sendGetRequest(url);
    });
  });

  it('getModelDownloadUrl', function () {
    var modelId = 15781;
    return ServerConnector.getModelDownloadUrl({
      modelId: modelId,
      handlerClass: "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"
    }).then(function (url) {
      assert.ok(url);
      assert.ok(url.indexOf(modelId) >= 0);
      return ServerConnector.sendGetRequest(url);
    });
  });

  it('getProjectSourceDownloadUrl', function () {
    return ServerConnector.getProjectSourceDownloadUrl().then(function (url) {
      assert.ok(url);
      return ServerConnector.sendGetRequest(url);
    });
  });

  it('addOverlay', function () {
    return ServerConnector.addOverlay({
      overlay: new DataOverlay({
        name: "test nam",
        description: "test desc",
        content: "name color\nCAPN1 #00FF00\nPARK7 #AC0000",
        filename: "test.txt"
      })
    }).then(function (overlay) {
      assert.ok(overlay);
    });
  });

  it('removeOverlay', function () {
    return ServerConnector.removeOverlay({
      overlayId: 17296
    });
  });

  it('removeComment', function () {
    return ServerConnector.removeComment({
      commentId: 4290
    });
  });

  it('updateOverlay', function () {
    var overlay = new DataOverlay({});
    overlay.setId(17296);
    overlay.setName("test nam2");
    overlay.setDescription("test desc2");

    return ServerConnector.updateOverlay(overlay);
  });

  it('logout', function () {
    return ServerConnector.logout().then(function () {
      assert.equal(ServerConnector.getSessionData().getToken(), undefined);
    });
  });

  it('getModelDownloadUrl', function () {
    return ServerConnector.getModelDownloadUrl({
      backgroundOverlayId: 14081,
      modelId: 1,
      handlerClass: "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"
    }).then(function (url) {
      assert.ok(url);
    });
  });

  it('getOverlayById', function () {
    return ServerConnector.getOverlayById(18083, "complex_model_with_submaps").then(function (overlay) {
      assert.ok(overlay);
    });
  });

  it('getConfiguration', function () {
    return ServerConnector.getConfiguration().then(function (configuration) {
      assert.ok(configuration instanceof Configuration);
      assert.ok(configuration.getElementTypes().length > 0);
      assert.ok(configuration.getReactionTypes().length > 0);
    });
  });

  describe('getProjects', function () {
    it('test caching', function () {
      var projects;
      return ServerConnector.getProjects().then(function (result) {
        projects = result;
        return ServerConnector.getProjects();
      }).then(function (result) {
        assert.ok(result === projects);
      });
    });

    it('test force reload', function () {
      var projects;
      var originalName;
      return ServerConnector.getProjects().then(function (result) {
        projects = result;
        originalName = projects[0].getName();
        projects[0].setName("test name");
        return ServerConnector.getProjects(true);
      }).then(function (result) {
        assert.ok(result === projects);
        assert.equal(originalName, projects[0].getName());
      });
    });
  });

  describe('login', function () {
    it('try invalid credentials', function () {
      var method = ServerConnector.sendPostRequest;
      ServerConnector.sendPostRequest = function () {
        return Promise.reject(new NetworkError("xxx", {
          statusCode: HttpStatus.FORBIDDEN
        }));
      };
      return ServerConnector.login("unknown", "unknown password").then(function () {
        ServerConnector.sendPostRequest = method;
        assert.ok(false);
      }, function (error) {
        ServerConnector.sendPostRequest = method;
        assert.ok(error.message.indexOf("credentials") >= 0);
      });
    });
  });

  describe('getServerBaseUrl', function () {
    it('url with GET arg that looks similar to original url', function () {
      helper.setUrl("http://localhost:8080/minerva/login.xhtml?from=http://localhost:8080/minerva/?id=sample");
      var url = ServerConnector.getServerBaseUrl();
      assert.ok(url.indexOf("?") === -1);
    });
  });


  describe('returnUserOrSystemColor ', function () {
    it('user has empty color', function () {
      var systemColor = "0000FF";
      return ServerConnector.returnUserOrSystemColor("", Promise.resolve(systemColor)).then(function (result) {
        assert.ok(255, result);
      });
    });
    it('user has defined color', function () {
      var userColor = "000001";
      var systemColor = "000010";
      return ServerConnector.returnUserOrSystemColor(userColor, Promise.resolve(systemColor)).then(function (result) {
        assert.ok(1, result);
      });
    });
  });

  describe('getUsers', function () {
    it('default', function () {
      helper.loginAsAdmin();
      return ServerConnector.getUsers().then(function (users) {
        assert.ok(users.length > 0);
      });
    });
    it('refresh', function () {
      helper.loginAsAdmin();
      var user, users;
      var modifiedName = "xxx name";
      return ServerConnector.getUsers().then(function (result) {
        users = result;
        user = users[0];
        user.setName(modifiedName);
        return ServerConnector.getUsers(true);
      }).then(function (result) {
        assert.ok(users === result);
        assert.ok(user.getName() !== modifiedName);
      });
    });
    it('access denied', function () {
      var originalFun = ServerConnector._sendRequest;
      ServerConnector._sendRequest = function () {
        return Promise.reject(new NetworkError("", {
          statusCode: HttpStatus.FORBIDDEN
        }))
      };
      return ServerConnector.getUsers().then(function () {
          assert.notOk("Security error expected");
        }, function (error) {
          assert.ok(error instanceof SecurityError);
        }
      ).finally(function () {
        ServerConnector._sendRequest = originalFun;
      });
    });
  });

  describe('uploadFile', function () {
    it('small file', function () {
      return ServerConnector.uploadFile({
        filename: "test.txt",
        content: new Uint8Array([1, 65, 90, 4, 8])
      }).then(function (file) {
        logger.debug(file);
        assert.ok(file.id);
      });
    });
  });

  it('getNeutralOverlayColorInt', function () {
    return ServerConnector.getNeutralOverlayColorInt().then(function (color) {
      assert.ok(color);
    });
  });

  it('getMesh', function () {
    return ServerConnector.getMesh({id: "D010300"}).then(function (mesh) {
      assert.ok(mesh.getSynonyms().length > 0);
      assert.equal("Parkinson Disease", mesh.getName());
      assert.ok(mesh.getDescription().indexOf("A progressive, degenerative ..." >= 0));
      assert.equal("D010300", mesh.getId());
    });
  });

  it('getSbmlFunction', function () {
    helper.setUrl("http://localhost:8080/?id=kinteics_test");
    return ServerConnector.getSbmlFunction({functionId: 7}).then(function (sbmlFunction) {
      assert.equal("fun", sbmlFunction.getName());
      assert.equal(7, sbmlFunction.getId());
    });
  });

  it('getSbmlParameter', function () {
    helper.setUrl("http://localhost:8080/?id=kinteics_test");
    return ServerConnector.getSbmlParameter({parameterId: 7}).then(function (sbmlParameter) {
      assert.equal("local param", sbmlParameter.getName());
      assert.equal(7, sbmlParameter.getId());
    });
  });

  it('getLoggedUser called twice', function () {
    var called = 0;
    var orginalFunction = ServerConnector.getUser;
    ServerConnector.getUser = function (login) {
      called++;
      return Promise.delay(20, orginalFunction.call(ServerConnector, login));
    };

    var promises = [ServerConnector.getLoggedUser(), ServerConnector.getLoggedUser()];
    return Promise.all(promises).then(function (result) {
      assert.ok(result[0] === result[1]);
      assert.equal(1, called);
    }).finally(function () {
      ServerConnector.getUser = orginalFunction;
    });
  });


  describe('objectToRequestString', function () {
    it('string', function () {
      assert.equal("test_string", ServerConnector.objectToRequestString("test_string"));
    });
    it('number', function () {
      assert.equal("12", ServerConnector.objectToRequestString(12));
    });
    it('point', function () {
      assert.equal("0.00,10.00", ServerConnector.objectToRequestString(new Point(0, 10)));
    });
    it('array of ids', function () {
      assert.equal("1,4", ServerConnector.objectToRequestString([1, 4]));
    });
    it('not sorted array of ids', function () {
      assert.equal("1,4,5", ServerConnector.objectToRequestString([5, 1, 4]));
    });
  });

  it('getProjectJobsUrl', function () {
    try {
      ServerConnector.useOriginal();
      var url = ServerConnector.getProjectJobsUrl("test", {});
      assert.isTrue(url.includes('new_api'), url);
    } finally {
      ServerConnector.useMock();
    }
  });

});
