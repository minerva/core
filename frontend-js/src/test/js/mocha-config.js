"use strict";

var Promise = require("bluebird");

mockEnvironment();

var $ = require('jquery');

var Helper = require('./helper');

var GuiConnector = require('./GuiConnector-mock');

var path = require('path');

// -----------------------------

var logger = require('./logger');

function mockEnvironment() {
  require('../../main/js/Functions').loadScript = function () {
    global.MathJax = {
      Hub: {
        Config: function () {
        },
        Queue: function () {

        }
      }
    };
    return Promise.resolve();
  };
  // requirejs.config({
  //   baseUrl: path.dirname(require.main.filename) + "/../../../"
  // });

// GLOBAL configuration
  global.navigator = {
    userAgent: 'node.js',
    appName: 'MinervaUnitTest',
    appVersion: '0.0.1'

  };

  var jsdom = require('jsdom');
  global.dom = new jsdom.JSDOM();
  global.window = global.dom.window;
  global.document = window.document;
  require('mock-local-storage');
  global.document.elementFromPoint = function () {
  };

  // https://github.com/FezVrasta/popper.js/issues/478#issuecomment-407422016
  global.document.createRange = function () {
    return {
      setStart: function () {
      },
      setEnd: function () {
      },
      commonAncestorContainer: {
        nodeName: 'BODY',
        ownerDocument: document
      }
    }
  };

// additions to jsdom implementations:
  global.Option = window.Option;
  global.Blob = window.Blob;
  global.MouseEvent = window.MouseEvent;
  global.FileReader = window.FileReader;

  global.getComputedStyle = window.getComputedStyle;
  global.Image = window.Image;

  global.Node = {DOCUMENT_POSITION_FOLLOWING: 4};
  window.open = function () {
    var result = {};
    result.focus = function () {

    };
    return result;
  };
  window.URL.createObjectURL = function () {

  };

  //CANVAS mocking
  window.HTMLCanvasElement.prototype.getContext = function () {
    return {
      fillRect: function () {
      },
      clearRect: function () {
      },
      getImageData: function (x, y, w, h) {
        return {
          data: new Array(w * h * 4)
        };
      },
      putImageData: function () {
      },
      createImageData: function () {
        return []
      },
      setTransform: function () {
      },
      drawImage: function () {
      },
      save: function () {
      },
      fillText: function () {
      },
      restore: function () {
      },
      beginPath: function () {
      },
      moveTo: function () {
      },
      lineTo: function () {
      },
      closePath: function () {
      },
      stroke: function () {
      },
      translate: function () {
      },
      scale: function () {
      },
      rotate: function () {
      },
      arc: function () {
      },
      fill: function () {
      },
      measureText: function () {
        return {width: 0};
      },
      transform: function () {
      },
      rect: function () {
      },
      clip: function () {
      },
      canvas: document.createElement("div")
    };
  };

  window.HTMLCanvasElement.prototype.toDataURL = function () {
    return "";
  };

  //open layers uses this function
  global.requestAnimationFrame = function () {
  };
  global.cancelAnimationFrame = function () {
  };
  global.CanvasPattern = function () {

  };
  global.CanvasGradient = function () {

  };

// pileup is using heavily some browser defined javascript
  var pileup = require('pileup');
  pileup.create = function () {
    return {
      destroy: function () {
      }
    };
  };
  pileup.formats.twoBit = function () {
    return {};
  };
  pileup.formats.bigBed = function () {
    return {};
  };
// ---
  // MolArt is inproperly packed
  global.MolArt = function () {

  };
// ---

  global.initCookieConsent = function () {
    console.log("Mock initCookieConsent");
    return {
      "run": function () {
        console.log("Mock run cookie")
      }
    };
  }

  global.ServerConnector = require('./ServerConnector-mock');

  Promise.longStackTraces();

  global.fetch = require("node-fetch");

}


beforeEach(function () {
  Helper.prototype.setUrl.call(this, "http://test-page/");

  window.onresize = undefined;

  logger.flushBuffer();

  ServerConnector.init();

  ServerConnector.getSessionData(null).clear();
  ServerConnector.getSessionData(null).setToken("MOCK_TOKEN_ID");
  ServerConnector.getSessionData(null).setLogin("anonymous");


  // @type HTMLElement
  global.testDiv = document.createElement("div");
  global.testDiv.id = "test";
  document.body.appendChild(testDiv);

  return ServerConnector.getConfiguration().then(function (configuration) {
    global.helper = new Helper(configuration);
    configuration.setVersion("1111.1111.1111");
  }).catch(function (error) {
    logger.error(error);
    throw error;
  });
});

afterEach(function () {
  GuiConnector.destroy();
  document.body.removeChild(global.testDiv);
  delete global.testDiv;
  if (this.currentTest.state !== 'failed') {
    if (document.body.hasChildNodes()) {
      var content = document.body.innerHTML;
      document.body.innerHTML = "";
      this.test.error(new Error("Test didn't left clean document. Found: " + content));
    } else if ($._data(window, "events") && $._data(window, "events").resize) {
      var events = $._data(window, "events").resize;
      logger.warn(events);
      $(window).off("resize");
      this.test.error(new Error("Test didn't left clean resize events handlers."));
    }
  } else {
    //when failed don't forget to clean events
    if ($._data(window, "events") && $._data(window, "events").resize) {
      logger.warn("Clearing events: " + $._data(window, "events").resize);
      $(window).off("resize");
    }
    if (document.body.hasChildNodes()) {
      logger.warn("Clearing html");
      document.body.innerHTML = "";
    }
  }
});
