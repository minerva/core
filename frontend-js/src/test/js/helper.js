"use strict";

var Readable = require('stream').Readable;
var toBlob = require('stream-to-blob');

require("./mocha-config");
var $ = require('jquery');

var logger = require('./logger');

var Alias = require("../../main/js/map/data/Alias");
var AbstractCustomMap = require("../../main/js/map/AbstractCustomMap");
var Annotation = require("../../main/js/map/data/Annotation");
var ChemicalDbOverlay = require("../../main/js/map/overlay/ChemicalDbOverlay");
var Comment = require("../../main/js/map/data/Comment");
var CommentDbOverlay = require("../../main/js/map/overlay/CommentDbOverlay");
var CustomMap = require("../../main/js/map/CustomMap");
var CustomMapOptions = require("../../main/js/map/CustomMapOptions");
var DrugDbOverlay = require("../../main/js/map/overlay/DrugDbOverlay");
var IdentifiedElement = require("../../main/js/map/data/IdentifiedElement");
var KineticLaw = require("../../main/js/map/data/KineticLaw");
var LayoutAlias = require("../../main/js/map/data/LayoutAlias");
var DataOverlay = require("../../main/js/map/data/DataOverlay");
var LayoutReaction = require("../../main/js/map/data/LayoutReaction");
var Model = require("../../main/js/map/data/MapModel");
var AbstractDbOverlay = require("../../main/js/map/overlay/AbstractDbOverlay");
var Point = require('../../main/js/map/canvas/Point');
var Product = require("../../main/js/map/data/Product");
var Project = require("../../main/js/map/data/Project");
var Reactant = require("../../main/js/map/data/Reactant");
var Reaction = require("../../main/js/map/data/Reaction");
var SbmlFunction = require("../../main/js/map/data/SbmlFunction");
var SbmlParameter = require("../../main/js/map/data/SbmlParameter");
var SearchDbOverlay = require("../../main/js/map/overlay/SearchDbOverlay");
var ServerConnector = require("../../main/js/ServerConnector");
var User = require("../../main/js/map/data/User");

var fs = require('fs');
var Promise = require('bluebird');

var GuiConnector = require('../../main/js/GuiConnector');
var Background = require("../../main/js/map/data/Background");

var OpenLayerCanvas = require('../../main/js/map/canvas/OpenLayers/OpenLayerCanvas');

/**
 *
 * @param {Configuration} configuration
 * @constructor
 */
function Helper(configuration) {
  if (configuration === undefined) {
    throw new Error("Configuration must be defined");
  }
  this.setConfiguration(configuration);
  this.idCounter = 1000000;
  this.EPSILON = Helper.EPSILON;
}

/**
 *
 * @param {Configuration} configuration
 */
Helper.prototype.setConfiguration = function (configuration) {
  this._configuration = configuration;
};

/**
 *
 * @returns {Configuration}
 */
Helper.prototype.getConfiguration = function () {
  return this._configuration;
};

/**
 *
 * @param {CustomMap} map
 * @returns {CommentDbOverlay}
 */
Helper.prototype.createCommentDbOverlay = function (map) {
  var result = new CommentDbOverlay({
    map: map,
    name: "comment",
    allowGeneralSearch: true
  });
  map.registerDbOverlay(result);
  return result;
};

/**
 *
 * @param {CustomMap} map
 * @returns {SearchDbOverlay}
 */
Helper.prototype.createSearchDbOverlay = function (map) {
  var result = new SearchDbOverlay({
    map: map,
    name: "search"
  });
  map.registerDbOverlay(result);
  return result;
};

/**
 *
 * @param {CustomMap} map
 * @returns {AbstractDbOverlay}
 */
Helper.prototype.createDbOverlay = function (map) {
  var result = new AbstractDbOverlay({
    iconType: "target",
    iconColorStart: 0,
    map: map,
    name: "search"
  });
  result.clear = function () {
    logger.debug("Clear mock");
  };
  map.registerDbOverlay(result);
  return result;
};

/**
 *
 * @param {CustomMap} map
 * @returns {DrugDbOverlay}
 */
Helper.prototype.createDrugDbOverlay = function (map) {
  var result = new DrugDbOverlay({
    map: map,
    name: "drug",
    allowGeneralSearch: true,
    allowSearchById: true
  });
  map.registerDbOverlay(result);
  return result;
};

/**
 *
 * @param {CustomMap} map
 * @returns {ChemicalDbOverlay}
 */
Helper.prototype.createChemicalDbOverlay = function (map) {
  var result = new ChemicalDbOverlay({
    map: map,
    name: "chemical",
    allowGeneralSearch: true,
    allowSearchById: true
  });
  map.registerDbOverlay(result);
  return result;
};

/**
 *
 * @param {Alias} [element]
 * @returns {Comment}
 */
Helper.prototype.createComment = function (element) {
  var elementType = "ALIAS";
  var elementId = this.idCounter++;
  var id = this.idCounter++;
  var modelId = this.idCounter++;

  if (element !== undefined) {
    if (element instanceof Alias) {
      elementType = "ALIAS";
      elementId = element.getId();
      modelId = element.getModelId();
    } else {
      throw new Error("Unknown element type: " + element);
    }
  }

  return new Comment({
    elementId: elementId,
    type: elementType,
    modelId: modelId,
    icon: "icons/comment.png",
    id: id,
    pinned: true,
    coord: {
      x: 321.5,
      y: 289.0
    },
    removed: false,
    title: "title fo comment: " + id,
    content: "content of the comment # " + id
  });
};

/**
 *
 * @param {Object} [params]
 * @returns {Project}
 */
Helper.prototype.createProject = function (params) {
  if (params === undefined) {
    params = {};
  }
  var result = new Project();
  result.setProjectId("testId");
  result.setName("");
  result.setProgress(0);
  result.setId(this.idCounter++);
  var map = this.createModel();
  result.addModel(map);
  var overlay = this.createOverlay(map);
  result.addDataOverlay(overlay, false);
  result.addOrUpdateBackgrounds([this.createBackground(map)])
  return result;
};

/**
 *
 * @returns {User}
 */
Helper.prototype.createUser = function () {
  return new User({
    login: "test_login",
    id: this.idCounter++,
    name: "some name",
    surname: "surname"
  });
};

/**
 *
 * @param {AbstractCustomMap| MapModel} [map]
 * @returns {Alias}
 */
Helper.prototype.createAlias = function (map) {
  var mapId;
  if (map instanceof AbstractCustomMap) {
    mapId = map.getId();
  } else if (map instanceof Model) {
    mapId = map.getId();
  } else {
    mapId = this.idCounter++;
  }
  var result = new Alias({
    idObject: this.idCounter++,
    name: "Test element",
    type: "RNA",
    modelId: mapId,
    bounds: {
      x: 10.0,
      y: 20.0,
      width: 30.0,
      height: 40.0
    },
    references: []
  });
  if (map instanceof AbstractCustomMap) {
    map.getModel().addAlias(result);
  } else if (map instanceof Model) {
    map.addAlias(result);
  }
  return result;
};

/**
 *
 * @param {Alias} [alias]
 * @param {string} [type]
 *
 * @returns {LayoutAlias}
 */
Helper.prototype.createLayoutAlias = function (alias, type) {
  var id;
  var modelId;
  if (alias instanceof Alias) {
    id = alias.getId();
    modelId = alias.getModelId();
  } else {
    id = this.idCounter++;
    modelId = this.idCounter++;
  }
  return new LayoutAlias({
    idObject: id,
    value: 0.2,
    color: {
      rgb: 23
    },
    modelId: modelId,
    geneVariations: [],
    type: type
  });
};

/**
 *
 * @param {Reaction} reaction
 * @returns {LayoutReaction}
 */
Helper.prototype.createLayoutReaction = function (reaction) {
  var reactionId;
  var modelId;
  if (reaction instanceof Reaction) {
    reactionId = reaction.getId();
    modelId = reaction.getModelId();
  } else {
    reactionId = this.idCounter++;
    modelId = this.idCounter++;
  }

  return new LayoutReaction({
    idObject: reactionId,
    modelId: modelId,
    width: 3.4,
    color: {
      a: 24
    },
    reverse: true
  });
};

/**
 *
 * @param {BioEntity} [element]
 * @returns {IdentifiedElement}
 */
Helper.prototype.createIdentifiedElement = function (element) {
  if (element === undefined) {
    return new IdentifiedElement({
      type: "ALIAS",
      id: this.idCounter++,
      modelId: this.idCounter++
    });
  }
  return new IdentifiedElement(element);
};

/**
 *
 * @param {AbstractCustomMap|MapModel} map
 * @param {boolean} [complete]
 * @returns {Reaction}
 */
Helper.prototype.createReaction = function (map, complete) {
  var mapId = null;
  if (map !== undefined) {
    mapId = map.getId();
  } else {
    mapId = this.idCounter++;
  }
  var result = new Reaction({
    idObject: this.idCounter++,
    lines: [{
      start: {
        x: 434.8904109589041,
        y: 85.0
      },
      end: {
        x: 410.8341500923087,
        y: 104.95576185524392
      },
      type: "START"
    }, {
      start: {
        x: 404.6769250286157,
        y: 110.06345991944379
      },
      end: {
        x: 380.62066416202026,
        y: 130.0192217746877
      },
      type: "END"
    }],
    centerPoint: new Point(0, 0),
    modelId: mapId,
    references: []
  });
  if (complete) {
    result.setIsComplete(true);
    result.setReactants([new Reactant({aliasId: this.createAlias(map)})]);
    result.setProducts([new Product({aliasId: this.createAlias(map)})]);
    result.setModifiers([]);
    result.setReferences([]);
  }
  return result;
};

/**
 *
 * @returns {MapModel}
 */
Helper.prototype.createModel = function () {
  var result = new Model();
  result.setId(this.idCounter++);
  result.setTileSize(256);
  result.setWidth(26547.3333333333);
  result.setHeight(800);
  result.setMaxZoom(8);
  result.setMinZoom(2);
  return result;
};

/**
 *
 * @param {MapModel} [model]
 * @returns {DataOverlay}
 */
Helper.prototype.createOverlay = function (model) {
  var id = this.idCounter++;
  return new DataOverlay({
    idObject: id,
    name: "testLayout" + id
  });
};

/**
 *
 * @param {MapModel} [model]
 * @returns {Background}
 */
Helper.prototype.createBackground = function (model) {
  var images = [];
  if (model !== undefined) {
    images.push({model: {id: model.getId()}, path: "xxx"});
  }
  var id = this.idCounter++;
  return new Background({
    id: id,
    name: "empty",
    images: images,
    creator: {login: "admin"}
  });
};

/**
 *
 * @param {Project} [project]
 * @returns {CustomMapOptions}
 */
Helper.prototype.createCustomMapOptions = function (project) {
  if (project === undefined) {
    project = this.createProject();
  }

  return new CustomMapOptions({
    project: project,
    element: testDiv,
    configuration: this.getConfiguration(),
    serverConnector: ServerConnector
  });

};

/**
 *
 * @returns {AbstractCustomMap}
 */
Helper.prototype.createAbstractCustomMap = function () {
  var options = this.createCustomMapOptions();
  var result = new AbstractCustomMap(options.getProject().getModels()[0], options);
  result.setMapCanvas(new OpenLayerCanvas(testDiv, result.createMapOptions()));
  return result;

};

/**
 *
 * @param {Project} [project]
 * @returns {CustomMap}
 */
Helper.prototype.createCustomMap = function (project) {
  if (project === null) {
    throw new Error("Project cannot be null");
  }
  var options = this.createCustomMapOptions(project);
  return new CustomMap(options);
};

/**
 *
 * @param {BioEntity} params.element
 * @param {AbstractCustomMap} params.map
 */
Helper.prototype.createMarker = function (params) {
  var position = params.element.getCenter();
  return params.map.getMapCanvas().createMarker({position: position, id: params.element.getId(), icon: "empty.png"});
};

/**
 * Changes url but saves the cookies.
 */
Helper.prototype.setUrl = function (url) {
  global.dom.reconfigure({
    url: url
  });
  // for (var cookie in cookies) {
  //   if (cookies.hasOwnProperty(cookie)) {
  //     Cookies.set(cookie, cookies[cookie]);
  //   }
  // }
  GuiConnector.init();
};

/**
 *
 * @param {String} filename
 * @returns {Promise}
 */
Helper.prototype.readFile = function (filename) {
  return new Promise(function (resolve, reject) {
    fs.readFile(filename, 'utf8', function (err, content) {
      if (err) {
        reject(err);
      } else {
        resolve(content);
      }
    });
  });
};

/**
 *
 * @returns {SbmlFunction}
 */
Helper.prototype.createSbmlFunction = function () {
  return new SbmlFunction({
    "functionId": "fun",
    "name": "fun name",
    "definition": "<lambda>" +
      "<bvar><ci> x </ci></bvar>" +
      "<bvar><ci> y </ci></bvar>" +
      "<apply><plus/><ci> x </ci><ci> y </ci><cn type=\"integer\"> 2 </cn></apply>" +
      "</lambda>\n\n",
    "arguments": ["x", "y"],
    "id": this.idCounter++
  });
};

/**
 *
 * @returns {SbmlParameter}
 */
Helper.prototype.createSbmlParameter = function () {
  return new SbmlParameter({
    "parameterId": "local_param",
    "unitsId": 24,
    "name": "local param",
    "global": false,
    "id": this.idCounter++,
    "value": 5.0
  });
};

/**
 *
 * @returns {KineticLaw}
 */
Helper.prototype.createKineticLaw = function () {
  return new KineticLaw({
    "parameterIds": [],
    "definition": '<math xmlns="http://www.w3.org/1998/Math/MathML">' +
      '<apply xmlns="http://www.w3.org/1998/Math/MathML">' +
      '<divide/><apply><times/><ci>ca1</ci><apply><plus/><ci>sa1</ci><ci>sa2</ci></apply></apply><cn type=\"integer\"> 2 </cn>' +
      '</apply></math>',
    "functionIds": []
  });
};

/**
 *
 */
Helper.prototype.loginAsAdmin = function () {
  ServerConnector.getSessionData().setLogin("admin");
  ServerConnector.getSessionData().setToken("ADMIN_TOKEN_ID");
};

Helper.prototype.mockWebdav = function () {
  var nock = require('nock');

  var content = fs.readFileSync('./testFiles/minervaNet/webdav-index.html', 'utf8');

  nock('https://minerva-net.lcsb.uni.lu/')
    .get('/proxy/?url=https://webdav-r3lab.uni.lu/public/minerva/')
    .reply(200, content);
};

/**
 *
 */
Helper.prototype.loginWithoutAccess = function () {
  // noinspection SpellCheckingInspection
  ServerConnector.getSessionData().setLogin("noaccessuser");
  ServerConnector.getSessionData().setToken("NO_ACCESS_USER_TOKEN_ID");
};


/**
 *
 * @param {HTMLElement} element
 * @param {string} eventType
 * @returns {Promise}
 */
Helper.prototype.triggerJqueryEvent = function (element, eventType) {
  var domElements = $("*");
  var promises = [];
  for (var i = 0; i < domElements.length; i++) {
    var domElement = domElements[i];
    $.each($._data(domElement, "events"), function (type, events) {
      if (type === eventType) {
        for (var j = 0; j < events.length; j++) {
          var event = events[j];
          var eventTargets = $(event.selector, domElement);
          for (var k = 0; k < eventTargets.length; k++) {
            if (element === eventTargets[k]) {
              promises.push(event.handler.call(element))
            }
          }
        }
      }
    });
  }
  return Promise.all(promises);
};

/**
 *
 * @returns {Annotation}
 */
Helper.prototype.createAnnotation = function () {
  // noinspection SpellCheckingInspection
  return new Annotation({
    resource: "10.1038%2Fnpjsba.2016.20",
    link: "http://doi.org/10.1038%2Fnpjsba.2016.20",
    id: this.idCounter++,
    type: "DOI"
  });
};

/**
 *
 * @param {string} content
 * @return {Promise<Blob>}
 */
Helper.prototype.stringToBlob = function (content) {
  var stream = new Readable;
  stream.push(content);
  stream.push(null);

  return new Promise(function (resolve, reject) {
    toBlob(stream, function (err, blob) {
      if (err) return reject(err.message);
      blob.name = "test.txt";
      resolve(blob);
    })
  });
};

/**
 *
 * @param {string} path
 * @return {Promise<Blob>}
 */
Helper.prototype.fileToBlob = function (path) {
  var self = this;
  return self.readFile(path).then(function (content) {
    return self.stringToBlob(content);
  }).then(function (blob) {
    blob.name = require('path').basename(path);
    return blob;
  });
};
/**
 *
 * @type {number}
 */
Helper.EPSILON = 1e-6;

module.exports = Helper;
