"use strict";

require("./mocha-config");
var $ = require('jquery');

var Promise = require("bluebird");

var minerva = require('../../main/js/minerva');
var ConfigurationType = require('../../main/js/ConfigurationType');
var GuiConnector = require('../../main/js/GuiConnector');
var SecurityError = require('../../main/js/SecurityError');
var ServerConnectorMock = require('./ServerConnector-mock');
var Point = require('../../main/js/map/canvas/Point');
var ProxyAccessPlugin = require('./plugin/ProxyAccessPlugin');

var chai = require('chai');
var assert = chai.assert;
var logger = require('./logger');

describe('minerva global', function () {
  beforeEach(function () {
    global.ServerConnector = undefined;
    helper.mockWebdav();
  });

  afterEach(function () {
    global.ServerConnector = ServerConnectorMock;
  });

  describe('create', function () {
    it('default', function () {
      var options = null;
      return ServerConnectorMock.getProject().then(function (project) {
        options = helper.createCustomMapOptions(project);

        return minerva.create(options);
      }).then(function (result) {
        assert.ok(result);
        assert.equal(logger.getWarnings().length, 0);
        return result.destroy();
      });
    });
    it('invalid projectId', function () {
      var options = {
        projectId: "unknownId",
        element: testDiv
      };
      return minerva.create(options).then(function () {
        assert.ok(false);
      }, function (error) {
        assert.ok(error.message.indexOf("Project with given id doesn't exist") >= 0);
      });
    });
    it('with overview', function () {
      helper.setUrl("http://test-page/?id=complex_model_with_images&zoom=5");
      var customMap;
      return ServerConnectorMock.getProject().then(function (project) {
        var options = helper.createCustomMapOptions(project);

        return minerva.create(options);
      }).then(function (result) {
        customMap = result;
        assert.notEqual(5, ServerConnectorMock.getSessionData().getZoomLevel(customMap.getProject().getModels()[0]), "Zoom should be changed because we don't have full info about position");
        assert.ok(result);
        assert.equal(logger.getWarnings().length, 0);
        var showOverviewButton = $(".minerva-overview-button")[0];
        return showOverviewButton.onclick();
      }).then(function () {
        return customMap.destroy();
      });
    });
  });


  it("showComments", function () {
    var options = null;
    var map;
    return ServerConnectorMock.getProject().then(function (project) {
      options = helper.createCustomMapOptions(project);
      return minerva.create(options);
    }).then(function (result) {
      map = result;
      var commentCheckbox = document.getElementsByName("commentCheckbox")[0];
      commentCheckbox.checked = true;
      return commentCheckbox.onclick();
    }).then(function () {
      assert.ok(ServerConnectorMock.getSessionData(options.getProject()).getShowComments());
      return map.destroy();
    });
  });

  describe("constructor", function () {

    it("with GET zoom param", function () {
      helper.setUrl("http://test-page/?zoom=5");
      var options;
      return ServerConnectorMock.getProject().then(function (project) {
        options = helper.createCustomMapOptions(project);
        return minerva.create(options);
      }).then(function (result) {
        var sessionData = ServerConnectorMock.getSessionData(options.getProject());
        assert.equal(sessionData.getZoomLevel(options.getProject().getModels()[0]), 5);
        return result.destroy();
      });
    });

    it("with GET coordinates param", function () {
      helper.setUrl("http://test-page/?x=5&y=6");
      var options;
      return ServerConnectorMock.getProject().then(function (project) {
        options = helper.createCustomMapOptions(project);
        return minerva.create(options);
      }).then(function (result) {
        var center = ServerConnectorMock.getSessionData(options.getProject()).getCenter(options.getProject().getModels()[0]);
        assert.ok(center instanceof Point);
        assert.closeTo(center.x, 5, helper.EPSILON);
        assert.closeTo(center.y, 6, helper.EPSILON);
        return result.destroy();
      });
    });

    it("simple", function () {
      var options = {
        projectId: "sample",
        element: testDiv
      };
      return minerva.create(options).then(function (result) {
        assert.ok(result);
        result.destroy();
      });
    });
  });

  describe('create with layout', function () {
    it('background as layout param in URL', function () {
      var background, project, plugin, map;
      return ServerConnectorMock.getProject().then(function (result) {
        project = result;
        var options = helper.createCustomMapOptions(project);

        plugin = new ProxyAccessPlugin({});
        options.getPlugins().push(plugin);

        background = project.getBackgrounds()[1];

        helper.setUrl("http://test-page/?background=" + background.getId());

        return minerva.create(options);
      }).then(function (result) {
        map = result;
        assert.ok(result);
        // input file is not available so it's the background
        return plugin.getMinervaPluginProxy().project.map.getVisibleDataOverlays();
      }).then(function (visibleDataOverlays) {
        // input file is available so it's not the background file but overlay
        assert.equal(visibleDataOverlays.length, 0);
        assert.equal(ServerConnectorMock.getSessionData(project).getSelectedBackgroundOverlay(), background.getId());
        return map.destroy();
      });
    });
    it('overlay from session data', function () {
      var overlay;
      return ServerConnectorMock.getProject().then(function (project) {
        var options = helper.createCustomMapOptions(project);

        overlay = project.getBackgrounds()[1];

        ServerConnectorMock.getSessionData(project).setSelectedBackgroundOverlay(overlay.getId());

        return minerva.create(options);
      }).then(function (result) {
        assert.equal(ServerConnectorMock.getSessionData().getSelectedBackgroundOverlay(), overlay.getId());
        return result.destroy();
      });
    });
  });

  describe('create with overlays', function () {
    it('invalid overlay as param in URL', function () {
      helper.setUrl("http://test-page/?overlays=xxx");
      return ServerConnectorMock.getProject().then(function (project) {
        var options = helper.createCustomMapOptions(project);
        return minerva.create(options);
      }).then(function (result) {
        assert.equal(1, logger.getWarnings().length);
        return result.destroy();
      });
    });
    it('valid overlay as param in URL', function () {
      helper.setUrl("http://test-page/?overlays=1");
      return ServerConnectorMock.getProject().then(function (project) {
        var options = helper.createCustomMapOptions(project);
        return minerva.create(options);
      }).then(function (result) {
        assert.equal(0, logger.getWarnings().length);
        return result.destroy();
      });
    });
  });

  it('create restricted map', function () {
    var originalFunction = ServerConnectorMock.getProject;
    ServerConnectorMock.getProject = function () {
      return Promise.reject(new SecurityError("Access denied."));
    };
    helper.setUrl("http://test-page/?id=restricted_sample");
    var options = {
      element: testDiv,
      mapDiv: testDiv,
      configuration: helper.getConfiguration()
    };

    return minerva.create(options).then(function (map) {
      assert.equal(null, map);
    }).catch(function (e) {
      assert.ok(e.message.indexOf("Access denied") >= 0);
    }).finally(function () {
      ServerConnectorMock.getProject = originalFunction;
    });
  });


  it('create with search overlay and GET search param', function () {
    helper.setUrl("http://test-page/?search=s1");

    var globalObject, plugin;
    return ServerConnectorMock.getProject().then(function (project) {
      var options = helper.createCustomMapOptions(project);
      plugin = new ProxyAccessPlugin();
      options.getPlugins().push(plugin);
      return minerva.create(options);
    }).then(function (result) {
      globalObject = result;
      return plugin.getMinervaPluginProxy().project.map.getHighlightedBioEntities("search");
    }).then(function (elements) {
      assert.ok(elements.length > 0);
      return globalObject.destroy();
    });
  });

  it('create with show submodel GET param', function () {
    helper.setUrl("http://test-page/?submap=15781");

    return ServerConnectorMock.getProject().then(function (project) {
      var options = helper.createCustomMapOptions(project);

      return minerva.create(options);
    }).then(function (result) {
      return result.destroy();
    });
  });

  it('create Export', function () {
    var options = null;
    return ServerConnectorMock.getProject().then(function (project) {
      options = helper.createCustomMapOptions(project);

      return minerva.createExport(options);
    }).then(function (result) {
      assert.ok(result);
      assert.equal(logger.getWarnings().length, 0);
    });
  });

  describe('createAdmin', function () {
    this.timeout(5000);
    it('as admin', function () {
      helper.loginAsAdmin();

      var options = helper.createCustomMapOptions(null);
      var url = window.location.href;

      return minerva.createAdmin(options).then(function (result) {
        assert.ok(result);
        assert.equal(logger.getWarnings().length, 0);
        assert.equal(url, window.location.href);
        result.destroy();
      });
    });
    it('as anonymous', function () {
      var options = null;
      return ServerConnectorMock.getProject().then(function (project) {
        options = helper.createCustomMapOptions(project);

        return minerva.createAdmin(options);
      }).then(function (result) {
        assert.notOk(result);
      });
    });
  });
  describe('createLogin', function () {
    it('no request account email configured', function () {
      helper.getConfiguration().getOption(ConfigurationType.REQUEST_ACCOUNT_EMAIL).setValue("");
      return minerva.createLogin({element: testDiv}).then(function () {
        assert.ok($("#register_button").css("display") === "none");
      });
    });
    it('default', function () {
      return minerva.createLogin({element: testDiv}).then(function () {
        assert.ok($("#register_button").css("display") !== "none");
        assert.ok(testDiv.innerHTML.indexOf("MiNERVA") >= 0);
      });
    });
    it('anonymous without access to default map', function () {
      var originalFunction = ServerConnectorMock.getProject;
      ServerConnectorMock.getProject = function () {
        return Promise.reject(new SecurityError("Access denied."));
      };
      return minerva.createLogin({element: testDiv}).then(function () {
        assert.ok($("#go_to_map_button").css('display') === 'none');
      }).finally(function () {
        ServerConnectorMock.getProject = originalFunction;
      });
    });
  });
  it('getProject', function () {
    return ServerConnectorMock.getProject().then(function (project) {
      var options = helper.createCustomMapOptions(project);
      return minerva.create(options);
    }).then(function (result) {
      assert.equal(result.getProject().getProjectId(), "sample");
      return result.destroy();
    });
  });

  it('plugin onFocus functionality', function () {
    helper.setUrl("http://test-page/?search=s1");

    var globalObject, plugin, callbackCalled = false;
    return ServerConnectorMock.getProject().then(function (project) {
      var options = helper.createCustomMapOptions(project);
      plugin = new ProxyAccessPlugin();
      options.getPlugins().push(plugin);
      return minerva.create(options);
    }).then(function (result) {
      globalObject = result;
      plugin.getMinervaPluginProxy().project.map.addListener({
        dbOverlayName: "search", type: "onFocus", callback: function () {
          callbackCalled = true;
        }
      });
      return $(".minerva-generic-search-panel a[href*='search']")[0].onclick();
    }).then(function () {
      assert.ok(callbackCalled);
      return globalObject.destroy();
    });
  });

  describe("close submap", function () {
    it("check if url parameters changed", function () {
      var data;
      var map;
      var submapId = 16729;
      var submapParameter;
      var projectId = "complex_model_with_submaps";
      helper.setUrl("http://test-page/?id=complex_model_with_submaps");
      return ServerConnectorMock.getProject(projectId).then(function (project) {
        var options = helper.createCustomMapOptions(project);
        options.setDebug(true);
        return minerva.create(options);
      }).then(function (result) {
        data = result;
        map = result.customMap;
        return map.openSubmap(submapId);
      }).then(function () {
        submapParameter = GuiConnector.getParams["submap"];
        return map.getSubmapById(submapId).close();
      }).then(function () {
        assert.notEqual(GuiConnector.getParams["submap"], submapParameter, "Submap parameter should change after submap was closed");
        return data.destroy();
      })
    });
  });

});
