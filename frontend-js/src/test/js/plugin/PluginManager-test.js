"use strict";

require("../mocha-config");
var $ = require('jquery');

var Plugin = require('../../../main/js/plugin/Plugin');
var PluginManager = require('../../../main/js/plugin/PluginManager');
var ProxyAccessPlugin = require('./ProxyAccessPlugin');

var GuiConnector = require('../../../main/js/GuiConnector');

var logger = require('../logger');
var chai = require('chai');
var assert = chai.assert;

describe('PluginManager', function () {
  var createParams = function () {
    var map = helper.createCustomMap();
    return {
      customMap: map,
      configuration: helper.getConfiguration(),
      element: testDiv,
      project: map.getProject()
    };
  };
  var createPluginManager = function () {
    var result = new PluginManager(createParams());
    result.isValidUrl = function () {
      return true;
    };
    return result;
  };
  it('constructor', function () {
    var manager = new PluginManager(createParams());
    assert.ok(manager);
    assert.equal(0, logger.getWarnings().length);
  });

  it('getPlugins', function () {
    var manager = createPluginManager();
    assert.equal(0, manager.getPlugins().length);
  });

  describe('addPlugin', function () {
    it('default', function () {
      var manager = createPluginManager();
      return manager.addPlugin({url: "./testFiles/plugin/empty.js"}).then(function () {
        assert.equal(1, manager.getPlugins().length);
      });
    });
    it('check url', function () {
      var manager = createPluginManager();
      return manager.addPlugin({url: "./testFiles/plugin/empty.js"}).then(function () {
        assert.ok(GuiConnector.getParams['plugins']);
      });
    });
    it('with min width', function () {
      var manager = createPluginManager();
      return manager.addPlugin({url: "./testFiles/plugin/min-width.js"}).then(function () {
        assert.equal("200px", $(manager.getElement()).css("min-width"));
      });
    });
    it('with onResize listener', function () {
      var manager = createPluginManager();
      var plugin = new ProxyAccessPlugin({});
      var listenerCalled = false;
      return manager.addPlugin(plugin).then(function () {
        plugin.getMinervaPluginProxy().project.map.addListener({
          object: "plugin", type: "onResize", callback: function () {
            listenerCalled = true;
          }
        });
        return manager.addPlugin({url: "./testFiles/plugin/min-width.js"});
      }).then(function () {
        assert.ok(listenerCalled);
      });
    });
    it('after removal', function () {
      var manager = createPluginManager();
      return manager.addPlugin({url: "./testFiles/plugin/empty.js"}).then(function (plugin) {
        return manager.removePlugin(plugin);
      }).then(function () {
        return manager.addPlugin({url: "./testFiles/plugin/empty.js"});
      }).then(function () {
        assert.equal(1, manager.getPlugins().length);
      });
    });

    it('with error', function () {
      var manager = createPluginManager();
      return manager.addPlugin({url: "./invalid.js"}).then(function () {
        assert.notOk("Expected error");
      }).catch(function (e) {
        assert.ok(e.message.indexOf("Problem with loading plugin") >= 0);
        assert.equal(0, manager.getPlugins().length);
      });
    });
    it('with invalid url', function () {
      var manager = createPluginManager();
      manager.isValidUrl = function () {
        return false;
      };
      return manager.addPlugin({url: "xx"}).then(function () {
        assert.notOk("Expected error");
      }).catch(function (e) {
        assert.ok(e.message.indexOf("Problem with loading plugin") >= 0)
      });
    });


  });

  describe('isValidUrl', function () {
    it('invalid url 1', function () {
      var manager = new PluginManager(createParams());
      assert.notOk(manager.isValidUrl(""));
    });
    it('invalid undefined', function () {
      var manager = new PluginManager(createParams());
      assert.notOk(manager.isValidUrl(undefined));
    });
    it('valid', function () {
      var manager = new PluginManager(createParams());
      assert.ok(manager.isValidUrl("http://onet.pl/"));
    });
  });

  describe('removePlugin', function () {
    it('default', function () {
      var manager = createPluginManager();
      return manager.addPlugin({url: "testFiles/plugin/empty.js"}).then(function (plugin) {
        return manager.removePlugin(plugin);
      }).then(function () {
        assert.equal(0, manager.getPlugins().length);
        assert.equal(-1, testDiv.innerHTML.indexOf("tab-pane"));
      });
    });

    it('check url', function () {
      var manager = createPluginManager();
      return manager.addPlugin({url: "./testFiles/plugin/empty.js"}).then(function (plugin) {
        return manager.removePlugin(plugin);
      }).then(function () {
        assert.notOk(GuiConnector.getParams['plugins']);
      });
    });

    it('removing non existing plugin', function () {
      var manager = createPluginManager();
      var plugin = new Plugin({
        url: "testFiles/plugin/empty.js",
        map: manager.getMap(),
        configuration: manager.getConfiguration(),
        serverConnector: manager.getServerConnector(),
        element: testDiv
      });

      return manager.removePlugin(plugin).then(function () {
        assert.notOk("Error expected");
      }, function (error) {
        assert.ok(error.message.indexOf("Plugin not registered") >= 0);
      });
    });
  });

});
