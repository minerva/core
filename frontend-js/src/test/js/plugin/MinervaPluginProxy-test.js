"use strict";

require("../mocha-config");

// noinspection JSUnusedLocalSymbols
var Promise = require("bluebird");

var Alias = require('../../../main/js/map/data/Alias');
var DataOverlay = require('../../../main/js/map/data/DataOverlay');
var MinervaPluginProxy = require('../../../main/js/plugin/MinervaPluginProxy');
var Plugin = require('../../../main/js/plugin/Plugin');
var ServerConnector = require('../ServerConnector-mock');
var Point = require('../../../main/js/map/canvas/Point');

var logger = require('../logger');

var chai = require('chai');
var assert = chai.assert;

/**
 *
 * @param {CustomMap} map
 * @returns {MinervaPluginProxy}
 */
function createProxy(map) {
  // noinspection SpellCheckingInspection
  return new MinervaPluginProxy({
    hash: "8f2112859d40de86dacc1994a224ea3d",
    map: map,
    element: testDiv,
    plugin: new Plugin({
      url: "./testFiles/plugin/empty.js",
      map: map,
      element: testDiv,
      configuration: helper.getConfiguration(),
      serverConnector: ServerConnector
    }),
    pluginId: "xx",
    configuration: helper.getConfiguration(),
    serverConnector: ServerConnector
  });
}

describe('MinervaPluginProxy', function () {
  it('constructor', function () {
    var map = helper.createCustomMap();
    var proxy = createProxy(map);
    assert.ok(proxy);
    assert.ok(proxy.pluginId);
    assert.ok(proxy.element);
    assert.ok(proxy.project);
    assert.ok(proxy.configuration);
    assert.equal(0, logger.getWarnings().length);
  });

  it('add search listener', function () {
    var callbackOk = false;
    var map;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);
      helper.createSearchDbOverlay(map);
      var proxy = createProxy(map);

      proxy.project.map.addListener({
        dbOverlayName: "search",
        type: "onSearch",
        callback: function (elements) {
          assert.ok(elements.length > 0);
          assert.ok(elements[0].length !== undefined, "Array of arrays expected as onSearch result");
          callbackOk = true;
        }
      });
      return map.getOverlayByName("search").searchByQuery("s1");
    }).then(function () {
      assert.ok(callbackOk);
      callbackOk = false;
      return map.getOverlayByName("search").searchByQuery("s1");
    }).then(function () {
      assert.ok(callbackOk);
      callbackOk = false;
      var params = {
        coordinates: new Point(184.79, 365.76),
        zoom: 2,
        modelId: map.getProject().getModels()[0].getId()
      };
      return map.getOverlayByName("search").searchByCoordinates(params);
    }).then(function () {
      assert.ok(callbackOk);
      return map.destroy();
    });
  });

  it('add data overlay', function () {
    var callbackOk = false;
    var map;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);
      helper.createSearchDbOverlay(map);
      var proxy = createProxy(map);

      return proxy.project.data.addDataOverlay({
        name: "test-name",
        content: "tmp",
        description: "desc",
        filename: "unknown.txt",
      });
    }).then(function (overlay) {
      return map.destroy();
    });
  });

  it('remove data overlay', function () {
    var map, proxy, overlay;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);
      helper.createSearchDbOverlay(map);
      proxy = createProxy(map);
      overlay = getDataOverlayFromProject(project);

      return proxy.project.data.removeDataOverlay(overlay.getId());
    }).then(function () {
      return map.destroy();
    });
  });

  describe('removeListener', function () {
    it('valid listener', function () {
      var callbackOk = false;
      var map, options;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        helper.createSearchDbOverlay(map);
        var proxy = createProxy(map);

        options = {
          dbOverlayName: "search",
          type: "onSearch",
          callback: function (elements) {
            assert.ok(elements.length > 0);
            assert.ok(elements[0].length !== undefined, "Array of arrays expected as onSearch result");
            callbackOk = true;
          }
        };
        proxy.project.map.addListener(options);
        proxy.project.map.removeListener(options);
        assert.equal(0, logger.getWarnings().length);
        return map.destroy();
      });
    });
    it('unknown listener', function () {
      var callbackOk = false;
      var map, options;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        helper.createSearchDbOverlay(map);
        var proxy = createProxy(map);

        options = {
          dbOverlayName: "search",
          type: "onSearch",
          callback: function (elements) {
            assert.ok(elements.length > 0);
            assert.ok(elements[0].length !== undefined, "Array of arrays expected as onSearch result");
            callbackOk = true;
          }
        };
        proxy.project.map.addListener(options);
        proxy.project.map.removeListener({});
      }).then(function () {
        assert.notOk("Exception expected");
      }, function (error) {
        assert.ok(error.message.indexOf("Listener doesn't exist") >= 0);

        return map.destroy();
      });
    });
  });
  describe('removeAllListeners', function () {
    it('valid listener', function () {
      var callbackOk = false;
      var map, options;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        helper.createSearchDbOverlay(map);
        var proxy = createProxy(map);

        options = {
          dbOverlayName: "search",
          type: "onSearch",
          callback: function (elements) {
            assert.ok(elements.length > 0);
            assert.ok(elements[0].length !== undefined, "Array of arrays expected as onSearch result");
            callbackOk = true;
          }
        };
        proxy.project.map.addListener(options);
        var removedListeners = proxy.project.map.removeAllListeners();
        assert.equal(1, removedListeners.length);
        return map.destroy();
      });
    });
    it('call twice', function () {
      var map = helper.createCustomMap();
      var proxy = createProxy(map);

      var options = {
        object: "map",
        type: "onZoomChanged",
        callback: function () {
        }
      };
      proxy.project.map.addListener(options);
      var removedListeners = proxy.project.map.removeAllListeners();
      assert.equal(1, removedListeners.length);
      removedListeners = proxy.project.map.removeAllListeners();
      assert.equal(0, removedListeners.length);
      return map.destroy();
    });
    it('no listeners', function () {
      var map;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        helper.createSearchDbOverlay(map);
        var proxy = createProxy(map);
        var removedListeners = proxy.project.map.removeAllListeners();
        assert.equal(0, removedListeners.length);
        return map.destroy();
      });
    });
  });

  describe("getBioEntityById", function () {
    it("for alias", function () {
      var map;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        var proxy = createProxy(map);
        return proxy.project.data.getBioEntityById({
          id: 329177,
          modelId: 15781,
          type: "ALIAS"
        });
      }).then(function (result) {
        assert.ok(result);
      }).then(function () {
        map.destroy();
      });
    });

    it("for reaction", function () {
      var map;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        var proxy = createProxy(map);
        return proxy.project.data.getBioEntityById({
          id: 153508,
          modelId: 15781,
          type: "REACTION"
        });
      }).then(function (result) {
        assert.ok(result);
        assert.ok(result.getReactants()[0].getAlias() instanceof Alias);
      }).then(function () {
        map.destroy();
      });
    });
  });

  describe("exportToMapFormat", function () {
    it("for alias", function () {
      var map;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        var proxy = createProxy(map);
        return proxy.project.data.exportToMapFormat({
          modelId: 15781,
          bioEntities: [{id: 329177, modelId: 15781, type: "ALIAS"}],
          handlerClass: "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"
        });
      }).then(function (result) {
        assert.ok(result);
      }).then(function () {
        map.destroy();
      });
    });

  });

  describe("showElement", function () {
    it("alias", function () {
      var elementToShow = {
        element: {
          id: 329171,
          modelId: 15781,
          type: "ALIAS"
        },
        type: "ICON"
      };

      var elementToShow2 = {
        element: {
          id: 329171,
          modelId: 15781,
          type: "ALIAS"
        },
        type: "SURFACE",
        options: {
          color: "#FF0000"
        }
      };
      var map;
      var proxy;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        proxy = createProxy(map);
        return proxy.project.map.showBioEntity(elementToShow);
      }).then(function () {
        return proxy.project.map.getHighlightedBioEntities();
      }).then(function (elements) {
        assert.equal(elements.length, 1);
        return proxy.project.map.showBioEntity(elementToShow2);
      }).then(function () {
        return proxy.project.map.getHighlightedBioEntities();
      }).then(function (elements) {
        assert.equal(elements.length, 2);
        return proxy.project.map.hideBioEntity(elementToShow);
      }).then(function () {
        return proxy.project.map.getHighlightedBioEntities();
      }).then(function (elements) {
        assert.equal(elements.length, 1);
        return proxy.project.map.hideBioEntity(elementToShow2);
      }).then(function () {
        return proxy.project.map.getHighlightedBioEntities();
      }).then(function (elements) {
        assert.equal(elements.length, 0);
        map.destroy();
      });
    });

    it("show twice", function () {
      var elementToShow = {
        element: {
          id: 329171,
          modelId: 15781,
          type: "ALIAS"
        },
        type: "ICON"
      };
      var elementToShow2 = {
        element: {
          id: 329171,
          modelId: 15781,
          type: "ALIAS"
        },
        type: "SURFACE",
        options: {
          color: "#FF0000"
        }
      };

      var map, proxy;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        proxy = createProxy(map);
        proxy.project.map.showBioEntity(elementToShow);
        return proxy.project.map.showBioEntity(elementToShow2);
      }).then(function () {
        map.destroy();
        assert.ok(false, "Should be rejected due to fact that there is show in progress");
      }, function (error) {
        assert.ok(error.message.indexOf("wait until previous Promise for showBioEntity/hideBioEntity is resolved") >= 0);
        map.destroy();
      });
    });
  });

  it("setCenter", function () {
    var map, proxy;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);
      proxy = createProxy(map);
      return proxy.project.map.setCenter({
        modelId: 15781,
        x: 10,
        y: 20
      });
    }).then(function () {
      var sessionData = ServerConnector.getSessionData(map.getProject());
      var center = sessionData.getCenter(map.getProject().getModels()[0]);
      assert.ok(center instanceof Point);
      assert.closeTo(parseFloat(center.x), 10, helper.EPSILON);
      assert.closeTo(parseFloat(center.y), 20, helper.EPSILON);
    }).then(function () {
      map.destroy();
    });
  });

  it("getCenter", function () {
    var map, proxy;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);
      proxy = createProxy(map);
      return proxy.project.map.setCenter({
        modelId: 15781,
        x: 10,
        y: 20
      });
    }).then(function () {
      var center = proxy.project.map.getCenter({modelId: 15781});
      assert.ok(center instanceof Point);
      assert.closeTo(parseFloat(center.x), 10, helper.EPSILON);
      assert.closeTo(parseFloat(center.y), 20, helper.EPSILON);
    }).then(function () {
      map.destroy();
    });
  });

  it("setZoom", function () {
    var map, proxy;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);
      proxy = createProxy(map);
      return proxy.project.map.setZoom({
        modelId: 15781,
        zoom: 4
      });
    }).then(function () {
      var sessionData = ServerConnector.getSessionData(map.getProject());
      var zoom = sessionData.getZoomLevel(map.getProject().getModels()[0]);
      assert.equal(zoom, 4);
    }).then(function () {
      map.destroy();
    });
  });

  it("getZoom", function () {
    var map, proxy;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);
      proxy = createProxy(map);
      return proxy.project.map.setZoom({
        modelId: 15781,
        zoom: 4
      });
    }).then(function () {
      var zoom = proxy.project.map.getZoom({modelId: 15781});
      assert.equal(zoom, 4);
    }).then(function () {
      map.destroy();
    });
  });

  it("getReactionByParticipantId", function () {
    var ie = {
      modelId: 15781,
      type: "ALIAS",
      id: 329167
    };

    var map, proxy;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);
      proxy = createProxy(map);
      return proxy.project.data.getReactionsWithElement(ie);
    }).then(function (reactions) {
      assert.equal(reactions.length, 5);
    }).then(function () {
      map.destroy();
    });

  });

  it("getAllBioEntities", function () {
    helper.setUrl("http://test-page/?id=complex_model_with_submaps");
    var map, proxy;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);
      proxy = createProxy(map);
      return proxy.project.data.getAllBioEntities();
    }).then(function (result) {
      assert.ok(result);
      assert.ok(result.length > 0);
    }).then(function () {
      map.destroy();
    });
  });

  describe("configuration", function () {
    it("elementTypes", function () {
      var map, proxy;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        proxy = createProxy(map);
        assert.ok(proxy.configuration.elementTypes.length > 0);
        map.destroy();
      });
    });
  });

  it("fitBounds", function () {
    var map, proxy, center;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);
      proxy = createProxy(map);
      center = map.getCenter();
      return proxy.project.map.fitBounds({
        modelId: 15781,
        x1: 10,
        x2: 10,
        y1: 20,
        y2: 30
      });
    }).then(function () {
      var center2 = map.getCenter();
      assert.ok(center.x !== center2.x || center.y !== center2.y);
    }).then(function () {
      return map.destroy();
    });
  });

  it("getProjectId", function () {
    return ServerConnector.getProject().then(function (project) {
      var map = helper.createCustomMap(project);
      var proxy = createProxy(map);
      assert.equal("sample", proxy.project.data.getProjectId());
      map.destroy();
    });
  });
  it("getName", function () {
    return ServerConnector.getProject().then(function (project) {
      var map = helper.createCustomMap(project);
      var proxy = createProxy(map);
      assert.equal("UNKNOWN DISEASE MAP", proxy.project.data.getName());
      map.destroy();
    });
  });
  it("getVersion", function () {
    return ServerConnector.getProject().then(function (project) {
      var map = helper.createCustomMap(project);
      var proxy = createProxy(map);
      assert.equal("0", proxy.project.data.getVersion());
      map.destroy();
    });
  });
  it("getDisease", function () {
    return ServerConnector.getProject().then(function (project) {
      var map = helper.createCustomMap(project);
      var proxy = createProxy(map);
      assert.ok(proxy.project.data.getDisease());
      map.destroy();
    });
  });
  it("getOrganism", function () {
    return ServerConnector.getProject().then(function (project) {
      var map = helper.createCustomMap(project);
      var proxy = createProxy(map);
      assert.ok(proxy.project.data.getOrganism());
      map.destroy();
    });
  });
  it("getModels", function () {
    return ServerConnector.getProject().then(function (project) {
      var map = helper.createCustomMap(project);
      var proxy = createProxy(map);
      assert.equal(1, proxy.project.data.getModels().length);
      map.destroy();
    });
  });

  it("getOverviewImages", function () {
    helper.setUrl("http://test-page/?id=complex_model_with_images");
    return ServerConnector.getProject().then(function (project) {
      var map = helper.createCustomMap(project);
      var proxy = createProxy(map);
      assert.ok(proxy.project.data.getOverviewImages().length > 0);
      map.destroy();
    });
  });

  describe("plugin params", function () {
    it("getGlobalParam", function () {
      var map;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        var proxy = createProxy(map);
        return proxy.pluginData.getGlobalParam("test");
      }).then(function (result) {
        assert.ok(result);
        map.destroy();
      });
    });
    it("setGlobalParam", function () {
      var map;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        var proxy = createProxy(map);
        return proxy.pluginData.setGlobalParam("test", "y");
      }).then(function (result) {
        assert.ok(result);
        map.destroy();
      });
    });
    it("getUserParam", function () {
      var map;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        var proxy = createProxy(map);
        return proxy.pluginData.getUserParam("test2");
      }).then(function (result) {
        assert.ok(result);
        map.destroy();
      });
    });
    it("setUserParam", function () {
      var map;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        var proxy = createProxy(map);
        return proxy.pluginData.setUserParam("test2", "y");
      }).then(function (result) {
        assert.ok(result);
        map.destroy();
      });
    });
  });

  /**
   *
   * @param {Project} project
   * @return {DataOverlay}
   */
  function getDataOverlayFromProject(project) {
    for (var i = 0; i < project.getDataOverlays().length; i++) {
      return project.getDataOverlays()[i];
    }
    return null;
  }

  describe('showDataOverlay', function () {
    it('by id', function () {
      var map, proxy, overlay;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        helper.createSearchDbOverlay(map);
        proxy = createProxy(map);
        overlay = getDataOverlayFromProject(project);

        return proxy.project.map.showDataOverlay(overlay.getId());
      }).then(function () {
        return proxy.project.map.getVisibleDataOverlays();
      }).then(function (visibleDataOverlays) {
        assert.equal(1, visibleDataOverlays.length);
        return map.destroy();
      });
    });
    it('by invalid id', function () {
      var map;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        helper.createSearchDbOverlay(map);
        var proxy = createProxy(map);
        return proxy.project.map.showDataOverlay(-1);
      }).then(function () {
        assert.notOk("Expected error");
      }).catch(function () {
      }).finally(function () {
        return map.destroy();
      });
    });

    it('by object', function () {
      var map, proxy, overlay;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        helper.createSearchDbOverlay(map);
        proxy = createProxy(map);
        overlay = getDataOverlayFromProject(project);

        return proxy.project.map.showDataOverlay(overlay);
      }).then(function () {
        return proxy.project.map.getVisibleDataOverlays();
      }).then(function (visibleDataOverlays) {
        assert.equal(1, visibleDataOverlays.length);
        return map.destroy();
      });
    });
  });

  it('showOverviewImage', function () {
    helper.setUrl("http://test-page/?id=complex_model_with_images");
    var map, proxy;
    var html;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);
      return map.init();
    }).then(function () {
      proxy = createProxy(map);
      html = document.body.innerHTML;
      return proxy.project.map.showOverviewImage(proxy.project.data.getOverviewImages()[0]);
    }).then(function () {
      assert.ok(html !== document.body.innerHTML);
      return proxy.project.map.hideOverviewImage();
    }).then(function () {
      return map.destroy();
    });
  });

  describe('hideDataOverlay', function () {
    it('by id', function () {
      var map, proxy, overlay;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        helper.createSearchDbOverlay(map);
        proxy = createProxy(map);
        overlay = getDataOverlayFromProject(project);

        return proxy.project.map.showDataOverlay(overlay.getId());
      }).then(function () {
        return proxy.project.map.hideDataOverlay(overlay.getId());
      }).then(function () {
        return proxy.project.map.getVisibleDataOverlays();
      }).then(function (visibleDataOverlays) {
        assert.equal(0, visibleDataOverlays.length);
        return map.destroy();
      });
    });
    it('by invalid id', function () {
      var map, proxy;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        helper.createSearchDbOverlay(map);
        proxy = createProxy(map);
        return proxy.project.map.hideDataOverlay(-1);
      }).then(function () {
        assert.notOk("Expected error");
      }).catch(function () {
      }).finally(function () {
        return map.destroy();
      });
    });
    it('by object', function () {
      var map, proxy, overlay;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        helper.createSearchDbOverlay(map);
        proxy = createProxy(map);
        overlay = getDataOverlayFromProject(project);

        return proxy.project.map.showDataOverlay(overlay.getId());
      }).then(function () {
        return proxy.project.map.hideDataOverlay(overlay.getId());
      }).then(function () {
        return proxy.project.map.getVisibleDataOverlays();
      }).then(function (visibleDataOverlays) {
        assert.equal(0, visibleDataOverlays.length);
        return map.destroy();
      });
    });
  });

  describe('getDataOverlays', function () {
    it('default', function () {
      var map, proxy;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        helper.createSearchDbOverlay(map);
        proxy = createProxy(map);
        var overlays = proxy.project.data.getDataOverlays();
        assert.ok(overlays.length > 0);
        return map.destroy();
      });
    });
  });

  describe('addListener', function () {
    it('on show overlay', function () {
      var callbackOk = false;
      var map;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        helper.createSearchDbOverlay(map);
        var proxy = createProxy(map);

        proxy.project.map.addListener({
          object: "overlay",
          type: "onShow",
          callback: function (overlay) {
            assert.ok(overlay instanceof DataOverlay);
            callbackOk = true;
          }
        });
        return map.openDataOverlay(map.getProject().getDataOverlays()[0]);
      }).then(function () {
        assert.ok(callbackOk);
        return map.destroy();
      });
    });
    it('on hide overlay', function () {
      var callbackOk = false;
      var map;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        helper.createSearchDbOverlay(map);
        var proxy = createProxy(map);

        proxy.project.map.addListener({
          object: "overlay",
          type: "onHide",
          callback: function (overlay) {
            assert.ok(overlay instanceof DataOverlay);
            callbackOk = true;
          }
        });
        return map.openDataOverlay(18077);
      }).then(function () {
        assert.notOk(callbackOk);
        return map.hideDataOverlay(18077);
      }).then(function () {
        assert.ok(callbackOk);
        return map.destroy();
      });
    });
    it('on zoom changed', function () {
      var callbackOk = false;
      var map;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        helper.createSearchDbOverlay(map);
        var proxy = createProxy(map);

        proxy.project.map.addListener({
          object: "map",
          type: "onZoomChanged",
          callback: function (data) {
            assert.equal(map.getId(), data.modelId);
            assert.equal(4, data.zoom);
            callbackOk = true;
          }
        });
        return map.setZoom(4);
      }).then(function () {
        assert.ok(callbackOk);
        return map.destroy();
      });
    });
    it('on center changed', function () {
      var callbackOk = false;
      var map;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        helper.createSearchDbOverlay(map);
        var proxy = createProxy(map);

        proxy.project.map.addListener({
          object: "map",
          type: "onCenterChanged",
          callback: function (data) {
            assert.equal(map.getId(), data.modelId);
            var center = data.center;
            assert.ok(center instanceof Point);
            assert.closeTo(parseFloat(center.x), 10, helper.EPSILON);
            assert.closeTo(parseFloat(center.y), 20, helper.EPSILON);
            callbackOk = true;
          }
        });
        return proxy.project.map.setCenter({
          modelId: 15781,
          x: 10,
          y: 20
        });
      }).then(function () {
        assert.ok(callbackOk);
        return map.destroy();
      });
    });

    describe('map-dialog', function () {
      it('onOpen', function () {
        helper.setUrl("http://test-page/?id=complex_model_with_submaps");
        var callbackOk = false;
        var map, submap;
        return ServerConnector.getProject().then(function (project) {
          map = helper.createCustomMap(project);
          submap = map.getSubmaps()[0];
          var proxy = createProxy(map);

          proxy.project.map.addListener({
            object: "map-dialog",
            type: "onOpen",
            callback: function (data) {
              assert.equal(submap.getId(), data.modelId);
              assert.ok(data.position);
              assert.ok(data.size);
              callbackOk = true;
            }
          });
          return submap.open(testDiv);
        }).then(function () {
          assert.ok(callbackOk);
          return map.destroy();
        });
      });

      it('onClose', function () {
        helper.setUrl("http://test-page/?id=complex_model_with_submaps");
        var callbackOk = false;
        var map, submap;
        return ServerConnector.getProject().then(function (project) {
          map = helper.createCustomMap(project);
          submap = map.getSubmaps()[0];
          var proxy = createProxy(map);

          proxy.project.map.addListener({
            object: "map-dialog",
            type: "onClose",
            callback: function (data) {
              assert.equal(submap.getId(), data.modelId);
              callbackOk = true;
            }
          });
          return submap.open(testDiv);
        }).then(function () {
          return submap.close();
        }).then(function () {
          assert.ok(callbackOk);
          return map.destroy();
        });
      });
    });

    it('onFocus', function () {
      helper.setUrl("http://test-page/?id=complex_model_with_submaps");
      var callbackOk = false;
      var map, submap;
      return ServerConnector.getProject().then(function (project) {
        map = helper.createCustomMap(project);
        submap = map.getSubmaps()[0];
        var proxy = createProxy(map);

        proxy.project.map.addListener({
          object: "map-dialog",
          type: "onFocus",
          callback: function (data) {
            assert.equal(submap.getId(), data.modelId);
            callbackOk = true;
          }
        });
        return submap.open(testDiv);
      }).then(function () {
        assert.ok(callbackOk);
        return map.destroy();
      });
    });
  });

  describe('triggerSearch', function () {
    describe('general', function () {
      it('query search', function () {
        var callbackOk = false;
        var map;
        return ServerConnector.getProject().then(function (project) {
          map = helper.createCustomMap(project);
          helper.createSearchDbOverlay(map);
          var proxy = createProxy(map);

          proxy.project.map.addListener({
            dbOverlayName: "search",
            type: "onSearch",
            callback: function (elements) {
              assert.ok(elements.length > 0);
              assert.ok(elements[0].length > 0);
              callbackOk = true;
            }
          });
          return proxy.project.map.triggerSearch({query: "s1", dbOverlayName: "search"});
        }).then(function () {
          assert.ok(callbackOk);
          return map.destroy();
        });
      });
      it('coordinates search', function () {
        var callbackOk = false;
        var map;
        return ServerConnector.getProject().then(function (project) {
          map = helper.createCustomMap(project);
          helper.createSearchDbOverlay(map);
          var proxy = createProxy(map);

          proxy.project.map.addListener({
            dbOverlayName: "search",
            type: "onSearch",
            callback: function (elements) {
              assert.ok(elements.length > 0);
              assert.ok(elements[0].length > 0);
              callbackOk = true;
            }
          });
          return proxy.project.map.triggerSearch({
            coordinates: new Point(316.05, 253.61),
            modelId: map.getModel().getId(),
            dbOverlayName: "search",
            zoom: 2
          });
        }).then(function () {
          assert.ok(callbackOk);
          return map.destroy();
        });
      });
      it('coordinates without zoom', function () {
        var callbackOk = false;
        var map;
        return ServerConnector.getProject().then(function (project) {
          map = helper.createCustomMap(project);
          helper.createSearchDbOverlay(map);
          var proxy = createProxy(map);

          proxy.project.map.addListener({
            dbOverlayName: "search",
            type: "onSearch",
            callback: function (elements) {
              assert.ok(elements.length > 0);
              assert.ok(elements[0].length > 0);
              callbackOk = true;
            }
          });
          return proxy.project.map.triggerSearch({
            coordinates: new Point(316.05, 253.61),
            modelId: map.getModel().getId(),
            dbOverlayName: "search"
          });
        }).then(function () {
          assert.ok(callbackOk);
          assert.equal(0, logger.getWarnings().length);
          return map.destroy();
        });
      });
    });
    describe('drug', function () {
      it('query search', function () {
        var callbackOk = false;
        var map;
        return ServerConnector.getProject().then(function (project) {
          map = helper.createCustomMap(project);
          helper.createDrugDbOverlay(map);
          var proxy = createProxy(map);

          proxy.project.map.addListener({
            dbOverlayName: "drug",
            type: "onSearch",
            callback: function (elements) {
              assert.ok(elements.length > 0);
              assert.ok(elements[0].length > 0);
              callbackOk = true;
            }
          });
          return proxy.project.map.triggerSearch({query: "aspirin", dbOverlayName: "drug"});
        }).then(function () {
          assert.ok(callbackOk);
          return map.destroy();
        });
      });
    });

    describe('chemical', function () {
      it('query search', function () {
        var callbackOk = false;
        var map;
        return ServerConnector.getProject().then(function (project) {
          map = helper.createCustomMap(project);
          helper.createChemicalDbOverlay(map);
          var proxy = createProxy(map);

          proxy.project.map.addListener({
            dbOverlayName: "chemical",
            type: "onSearch",
            callback: function (elements) {
              assert.ok(elements.length > 0);
              assert.ok(elements[0].length > 0);
              callbackOk = true;
            }
          });
          return proxy.project.map.triggerSearch({query: "rotenone", dbOverlayName: "chemical"});
        }).then(function () {
          assert.ok(callbackOk);
          return map.destroy();
        });
      });
    });
  });

  it('clear', function () {
    var callbackOk = false;
    var map, proxy;
    return ServerConnector.getProject().then(function (project) {
      map = helper.createCustomMap(project);
      helper.createSearchDbOverlay(map);
      helper.createCommentDbOverlay(map);
      proxy = createProxy(map);

      return proxy.project.map.triggerSearch({query: "s1", dbOverlayName: "search"});
    }).then(function () {
      proxy.project.map.addListener({
        dbOverlayName: "search",
        type: "onSearch",
        callback: function (elements) {
          assert.equal(elements[0].length, 0);
          callbackOk = true;
        }
      });
      return proxy.project.map.clear();
    }).then(function () {
      assert.ok(callbackOk);
      return map.destroy();
    });
  });

});
