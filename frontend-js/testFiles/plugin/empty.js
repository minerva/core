minervaDefine(function () {
  return {
    register: function (object) {
      console.log("registering test plugin");
    },
    unregister: function () {
      console.log("unregistering test plugin");
    },
    getName: function () {
      return "test plugin";
    },
    getVersion: function () {
      return "0.0.1";
    }
  };
});