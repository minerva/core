minervaDefine(function () {
  return {
    register: function (minervaObject) {
      var options = {
        dbOverlayName: "search",
        type: "onSearch",
        callback: function (elements) {
          console.log("hi there");
        }
      };
      minervaObject.project.map.addListener(options);
    },
    unregister: function () {
      console.log("unregistering test plugin");
    },
    getName: function () {
      return "high test";
    },
    getVersion: function () {
      return "0.0.1";
    }
  };
});