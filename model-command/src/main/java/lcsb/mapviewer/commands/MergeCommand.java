package lcsb.mapviewer.commands;

import java.util.ArrayList;
import java.util.Collection;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class MergeCommand extends NewModelCommand {
  private static int MARGIN = 200;
  private static int MIN_SIZE = 100;

  private Collection<Model> models;

  public MergeCommand(final Collection<Model> models) {
    super(null);
    this.models = models;
  }

  @Override
  public Model execute() throws CommandExecutionException {
    int count = models.size();

    double offsetX = 0;
    double offsetY = 0;

    Model result = new ModelFullIndexed(null);

    int rowSize = (int) Math.sqrt(count);

    double mapWidth = MIN_SIZE;
    double mapHeight = MIN_SIZE;

    int counter = 0;
    double rowHeight = 0;
    for (final Model model : models) {

      new MoveCommand(model, offsetX, offsetY).execute();
      new AddElementPrefixCommand(model, "path_" + counter + "_").execute();

      result.addElements(model.getElements());
      result.addReactions(new ArrayList<>(model.getReactions()));
      result.addLayers(model.getLayers());

      mapHeight = Math.max(mapHeight, offsetY + model.getHeight() + MARGIN);
      mapWidth = Math.max(mapWidth, offsetX + model.getWidth() + MARGIN);

      counter++;
      rowHeight = Math.max(rowHeight, model.getHeight() + MARGIN);
      if (counter % rowSize == 0) {
        offsetX = 0;
        offsetY += rowHeight;
        rowHeight = 0;
      } else {
        offsetX += model.getWidth() + MARGIN;
      }
    }

    result.setWidth(mapWidth);
    result.setHeight(mapHeight);

    return result;
  }

}
