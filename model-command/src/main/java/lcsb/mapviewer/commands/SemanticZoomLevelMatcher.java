package lcsb.mapviewer.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SemanticZoomLevelMatcher {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger();

  /**
   * Checks if level belongs to the range defined in semanticZoomLevelVisibility.
   * 
   * @param level
   *          level to ve checked
   * @param semanticZoomLevelVisibility
   *          range of levels to be checked
   * @return true if level is in the range
   */
  public boolean isVisible(final int level, final String semanticZoomLevelVisibility) {
    return matchLevel(level, semanticZoomLevelVisibility);
  }

  public boolean isTransparent(final int level, final String semanticZoomLevelVisibility) {
    return matchLevel(level, semanticZoomLevelVisibility);
  }

  private boolean matchLevel(final int level, final String semanticZoomLevelVisibility) {
    if (semanticZoomLevelVisibility == null || semanticZoomLevelVisibility.isEmpty()) {
      return true;
    }
    if (semanticZoomLevelVisibility.contains("{")) {
      String strLevels = semanticZoomLevelVisibility.replace("{", "").replace("}", "");
      String[] ranges = strLevels.split(",");
      for (final String string : ranges) {
        if (Integer.valueOf(string).equals(level)) {
          return true;
        }
      }
    }
    if (Integer.valueOf(semanticZoomLevelVisibility) <= level) {
      return true;
    }
    return false;
  }

}
