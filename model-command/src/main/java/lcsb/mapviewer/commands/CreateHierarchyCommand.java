package lcsb.mapviewer.commands;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * This {@link ModelCommand command} class allows to transform model into
 * multilevel (nested) component structure. Some artificial compartments will
 * appear. All compartments have information about children compartments. All
 * objects have information about parents.
 */
public class CreateHierarchyCommand extends ModelCommand {

  public static final String TEXT_LAYER_NAME = "text";

  /**
   * Natural logarithm of four.
   */
  private static final double LOG_4 = Math.log(4);
  /**
   * Top left corner x coordinate of the text associated with compartment.
   */
  private static final double DEFAULT_TITLE_X_COORD_IN_ARTIFICIAL_COMPARTMENT = 10;
  /**
   * Top left corner y coordinate of the text associated with compartment.
   */
  private static final double DEFAULT_TITLE_Y_COORD_IN_ARTIFICIAL_COMPARTMENT = 10;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static final Logger logger = LogManager.getLogger();
  /**
   * How many levels are possible.
   */
  private final int zoomLevels;
  /**
   * What is the maximum zoom factor in the view.
   */
  private final double maxZoomFactor;

  /**
   * Default constructor that initializes data.
   *
   * @param model         model on which command will be executed
   * @param maxZoomFactor what is the maximum zoom out factor
   * @param zoomLevels    how many levels are possible
   */
  public CreateHierarchyCommand(final Model model, final int zoomLevels, final double maxZoomFactor) {
    super(model);
    this.zoomLevels = zoomLevels;
    this.maxZoomFactor = maxZoomFactor;
  }

  @Override
  protected void undoImplementation() {
    throw new NotImplementedException();
  }

  @Override
  protected void redoImplementation() {
    throw new NotImplementedException();
  }

  @Override
  protected void executeImplementation() {
    if (!ModelCommandStatus.CREATED.equals(getStatus()) && !ModelCommandStatus.UNDONE.equals(getStatus())) {
      throw new InvalidStateException(
          "To execute command, the command must be in CREATED or UNDONE state. " + getStatus() + " found.");
    }
    Model model = getModel();
    List<Species> compacts = new ArrayList<>();
    for (final Element alias : model.getElements()) {
      if (alias instanceof Complex) {
        if (((Complex) alias).getState().contains("brief")) {
          compacts.add((Species) alias);
          compacts.addAll(((Complex) alias).getElements());
        }
      }
    }
    model.getElements().removeAll(compacts);
    clean();
    createArtificials();
    assignAliases();

    List<Element> sortedAliases = model.getElementsSortedBySize();
    setParentingAndChildreningOfNonComplexChildrens(sortedAliases);
    model.getElements().addAll(compacts);
    sortedAliases = model.getElementsSortedBySize();
    settingOfTransparencyLevel(sortedAliases);

    setDefaultVisibilityLevel(sortedAliases);

    setStatus(ModelCommandStatus.EXECUTED);
  }

  private void setDefaultVisibilityLevel(final List<Element> sortedAliases) {
    for (final Element element : sortedAliases) {
      if (element.getVisibilityLevel() == null || element.getVisibilityLevel().isEmpty()) {
        element.setVisibilityLevel(0);
      }
    }
  }

  /**
   * Cleans hierarchical information from the model.
   */
  protected void clean() {
    for (final Element alias : getModel().getElements()) {
      alias.setCompartment(null);
      alias.setPathway(null);
      if (alias.getTransparencyLevel() == null || alias.getTransparencyLevel().isEmpty()) {
        alias.setTransparencyLevel("");
      }
    }
    Set<Compartment> toRemove = new HashSet<>();
    for (final Compartment alias : getModel().getCompartments()) {
      if (alias instanceof PathwayCompartment) {
        toRemove.add(alias);
      }
    }
    for (final Compartment alias : toRemove) {
      getModel().removeElement(alias);
    }
  }

  /**
   * Creates artificial compartment alias for rectangles and texts object on
   * additional graphic layers.
   */
  void createArtificials() {
    RestAnnotationParser rap = new RestAnnotationParser();
    Model model = getModel();
    int id = 0;
    for (final Layer layer : model.getLayers()) {
      if (!Objects.equals(layer.getName(), TEXT_LAYER_NAME)) {
        for (final LayerRect rect : layer.getRectangles()) {
          int nextId = getNextArtId(model, id);
          id = nextId + 1;
          PathwayCompartment compartment = new PathwayCompartment("art" + nextId);
          compartment.setX(rect.getX());
          compartment.setY(rect.getY());
          compartment.setWidth(rect.getWidth());
          compartment.setHeight(rect.getHeight());
          compartment.setFontColor(rect.getBorderColor());
          compartment.setBorderColor(rect.getBorderColor());
          if (rect.getFillColor() != null) {
            compartment.setFillColor(rect.getFillColor());
          }
          compartment.setNameX(rect.getX() + DEFAULT_TITLE_X_COORD_IN_ARTIFICIAL_COMPARTMENT);
          compartment.setNameY(rect.getY() + DEFAULT_TITLE_Y_COORD_IN_ARTIFICIAL_COMPARTMENT);
          compartment.setNameWidth(rect.getWidth() - 2 * DEFAULT_TITLE_X_COORD_IN_ARTIFICIAL_COMPARTMENT);
          compartment.setNameHeight(rect.getHeight() - 2 * DEFAULT_TITLE_Y_COORD_IN_ARTIFICIAL_COMPARTMENT);
          compartment.setNameHorizontalAlign(HorizontalAlign.LEFT);
          compartment.setNameVerticalAlign(VerticalAlign.TOP);
          compartment.setZ(rect.getZ());

          model.addElement(compartment);
        }
        for (final LayerText text : layer.getTexts()) {
          int nextId = getNextArtId(model, id);
          id = nextId + 1;
          PathwayCompartment compartment = new PathwayCompartment("art" + nextId);
          compartment.setX(text.getX());
          compartment.setY(text.getY());
          compartment.setWidth(text.getWidth());
          compartment.setHeight(text.getHeight());
          compartment.setFontColor(text.getColor());
          compartment.setFillColor(text.getBackgroundColor());
          compartment.setBorderColor(text.getBorderColor());
          compartment.setName(extractNameFromText(text.getNotes()));
          compartment.setNotes(extractNotesFromText(text.getNotes()));
          compartment.setZ(text.getZ());
          compartment.setGlyph(text.getGlyph());
          rap.processNotes(compartment);
          text.setNotes(compartment.getName() + "\n" + compartment.getNotes());

          compartment.setNameX(text.getX() + DEFAULT_TITLE_X_COORD_IN_ARTIFICIAL_COMPARTMENT);
          compartment.setNameY(text.getY() + DEFAULT_TITLE_Y_COORD_IN_ARTIFICIAL_COMPARTMENT);
          compartment.setNameWidth(text.getWidth() - 2 * DEFAULT_TITLE_X_COORD_IN_ARTIFICIAL_COMPARTMENT);
          compartment.setNameHeight(text.getHeight() - 2 * DEFAULT_TITLE_Y_COORD_IN_ARTIFICIAL_COMPARTMENT);
          compartment.setNameHorizontalAlign(HorizontalAlign.LEFT);
          compartment.setNameVerticalAlign(VerticalAlign.TOP);

          model.addElement(compartment);

        }
      }
    }
  }

  private int getNextArtId(final Model model, final int id) {
    int result = id;
    int numberOfElements = model.getElements().size();
    for (int i = 0; i < numberOfElements + 1; i++) {
      if (model.getElementByElementId("art" + result) == null) {
        return result;
      }
      result++;
    }
    return result;
  }

  private String extractNotesFromText(final String notes) {
    if (notes.contains("\n")) {
      return notes.substring(notes.indexOf("\n"));
    } else {
      return "";
    }
  }

  private String extractNameFromText(final String notes) {
    if (notes.contains("\n")) {
      return notes.substring(0, notes.indexOf("\n"));
    } else {
      return notes;
    }
  }

  /**
   * Assign aliases in hierarchical structure.
   */
  private void assignAliases() {
    assignToCompartments();
  }

  private int computeVisibility(final Element alias) {
    if (!NumberUtils.isCreatable(alias.getVisibilityLevel())) {
      double rate = computeRate(alias, Configuration.MIN_VISIBLE_OBJECT_SIZE);
      int logValue = (int) ((int) Math.ceil(Math.log(rate)) / LOG_4);
      boolean hasCompartment = alias.getCompartment() != null;
      boolean hasPathway = alias.getPathway() != null;
      boolean hasComplex = false;
      if (alias instanceof Species) {
        hasComplex = ((Species) alias).getComplex() != null;
      }
      if (!hasCompartment && !hasPathway && !hasComplex) {
        logValue = 0;
      }
      if (logValue >= zoomLevels) {
        if (hasComplex) {
          logValue = zoomLevels - 1;
        } else {
          logValue = zoomLevels;
        }
      }
      return logValue;
    } else {
      return Integer.parseInt(alias.getVisibilityLevel());
    }
  }

  /**
   * Sets transparency level in hierarchical view for compartment alias.
   *
   * @param compartment compartment alias
   */
  private void settingTransparencyLevelForCompartment(final Compartment compartment) {
    if (compartment.getTransparencyLevel() == null || compartment.getTransparencyLevel().isEmpty()) {
      int maxVisibilityLevel = Integer.MAX_VALUE;
      double rate = computeRate(compartment, Configuration.MAX_VISIBLE_OBJECT_SIZE);
      maxVisibilityLevel = (int) ((int) Math.ceil(Math.log(rate)) / LOG_4);
      for (final Element child : getCompartmentElements(compartment)) {
        maxVisibilityLevel = Math.min(maxVisibilityLevel, computeVisibility(child));
      }
      if (maxVisibilityLevel >= zoomLevels) {
        maxVisibilityLevel = zoomLevels;
      }
      if (maxVisibilityLevel <= 0) {
        maxVisibilityLevel = 1;
      }
      compartment.setTransparencyLevel(maxVisibilityLevel + "");
    }
    for (final Element child : getCompartmentElements(compartment)) {
      if (!isExcludedChildVisibility(child)) {
        child.setVisibilityLevel(compartment.getTransparencyLevel());
      }
    }
  }

  private Set<Element> getCompartmentElements(final Compartment compartment) {
    Set<Element> children = new HashSet<>();
    for (Element element : compartment.getModel().getElements()) {
      if (element.getCompartment() == compartment) {
        if (element.getPathway() == null) {
          children.add(element);
        } else if (element.getCompartment().getSize() < element.getPathway().getSize()) {
          children.add(element);
        }
      }
    }
    return children;
  }

  private boolean isExcludedChildVisibility(final Element child) {
    return child.getVisibilityLevel() != null && !child.getVisibilityLevel().isEmpty();
  }

  private void settingTransparencyLevelForPathway(final PathwayCompartment pathway) {
    Set<Element> pathwayChildren = getPathwayChildren(pathway);
    if (pathway.getTransparencyLevel() == null || pathway.getTransparencyLevel().isEmpty()) {
      double rate = computeRate(pathway, Configuration.MAX_VISIBLE_OBJECT_SIZE);
      int maxVisibilityLevel = (int) ((int) Math.ceil(Math.log(rate)) / LOG_4);
      for (final Element child : pathwayChildren) {
        maxVisibilityLevel = Math.min(maxVisibilityLevel, computeVisibility(child));
      }
      if (maxVisibilityLevel >= zoomLevels) {
        maxVisibilityLevel = zoomLevels;
      }
      if (maxVisibilityLevel <= 0) {
        maxVisibilityLevel = 1;
      }
      pathway.setTransparencyLevel(maxVisibilityLevel + "");
    }
    for (final Element child : pathwayChildren) {
      if (!isExcludedChildVisibility(child)) {
        child.setVisibilityLevel(pathway.getTransparencyLevel());
      }
    }
  }

  private Set<Element> getPathwayChildren(final PathwayCompartment pathway) {
    Set<Element> children = new HashSet<>();
    for (Element element : pathway.getModel().getElements()) {
      if (element.getPathway() == pathway) {
        if (element.getCompartment() == null) {
          children.add(element);
        } else if (element.getPathway().getSize() < element.getCompartment().getSize()) {
          children.add(element);
        }
      }
    }
    return children;
  }

  /**
   * Sets transparency level in hierarchical view for complex alias.
   *
   * @param complex complex alias
   */
  private void settingTransparencyLevelForComplex(final Complex complex) {
    if (complex.getTransparencyLevel() == null || complex.getTransparencyLevel().isEmpty()) {
      int maxVisibilityLevel = Integer.MAX_VALUE;
      double rate = computeRate(complex, Configuration.MAX_VISIBLE_OBJECT_SIZE);
      maxVisibilityLevel = (int) ((int) Math.ceil(Math.log(rate)) / LOG_4);
      for (final Element child : complex.getElements()) {
        maxVisibilityLevel = Math.min(maxVisibilityLevel, computeVisibility(child));
      }
      if (maxVisibilityLevel >= zoomLevels) {
        maxVisibilityLevel = zoomLevels;
      }
      if (maxVisibilityLevel <= 0) {
        maxVisibilityLevel = 1;
      }
      complex.setTransparencyLevel(maxVisibilityLevel + "");
    }
    for (final Element child : complex.getElements()) {
      if (!isExcludedChildVisibility(child)) {
        child.setVisibilityLevel(complex.getTransparencyLevel());
      }
    }
  }

  private final Set<Element> excluded = new HashSet<>();

  /**
   * Sets transparency level in hierarchical view for all elements.
   *
   * @param sortedAliases list of aliases
   */
  private void settingOfTransparencyLevel(final List<Element> sortedAliases) {
    excluded.clear();

    for (final Element alias : sortedAliases) {
      if (alias.getVisibilityLevel() != null && !alias.getVisibilityLevel().isEmpty()) {
        excluded.add(alias);
      }
    }

    for (final Element alias : sortedAliases) {
      if (alias instanceof PathwayCompartment) {
        settingTransparencyLevelForPathway((PathwayCompartment) alias);
      } else if (alias instanceof Compartment) {
        settingTransparencyLevelForCompartment((Compartment) alias);
      } else if (alias instanceof Complex) {
        settingTransparencyLevelForComplex((Complex) alias);
      } else {
        if (alias.getTransparencyLevel() == null || alias.getTransparencyLevel().isEmpty()) {
          alias.setTransparencyLevel("0");
        }
      }
    }
  }

  /**
   * Removes invalid compartment children.
   *
   * @param sortedAliases list of aliases
   */
  private void setChildrening(final List<Element> sortedAliases) {
    for (final Element element : sortedAliases) {
      if (element instanceof Compartment) {
        Compartment compartment = (Compartment) element;
        Set<Element> removable = new HashSet<>();
        for (final Element alias : compartment.getElements()) {
          if (alias.getCompartment() != compartment) {
            removable.add(alias);
          }
        }
        compartment.getElements().removeAll(removable);
      }
    }
  }

  /**
   * Sets parent for elements.
   *
   * @param sortedAliases list of aliases
   */
  private void setParentingOfNonComplexChildrens(final List<Element> sortedAliases) {
    for (final Element element : getModel().getElements()) {
      if (element.getCompartment() == null) {
        for (final Element compartment : sortedAliases) {
          if (compartment instanceof PathwayCompartment) {
            if (compartment.contains(element)) {
              element.setPathway((PathwayCompartment) compartment);
            }
          } else if (compartment instanceof Compartment) {
            if (((Compartment) compartment).getElements().contains(element)) {
              element.setCompartment((Compartment) compartment);
            }
          }
        }

      }
      if (element instanceof Species) {
        Species species = (Species) element;
        if (species.getComplex() == null) {
          for (final Element complex : sortedAliases) {
            if (complex instanceof Complex) {
              if (((Complex) complex).getElements().contains(element)) {
                species.setComplex((Complex) complex);
              }
            }
          }
        }
      }
    }
  }

  /**
   * Set parents for the elements in hierarchical view.
   *
   * @param sortedAliases list of aliases
   */
  private void setParentingAndChildreningOfNonComplexChildrens(final List<Element> sortedAliases) {
    setParentingOfNonComplexChildrens(sortedAliases);
    setChildrening(sortedAliases);
  }

  /**
   * Assign aliases to compartments.
   */
  private void assignToCompartments() {
    Compartment nullCompartment = new Compartment("null");
    nullCompartment.setWidth(Double.MAX_VALUE);
    nullCompartment.setHeight(Double.MAX_VALUE);
    for (final Element element : getModel().getElements()) {
      Compartment supposedCompartment = nullCompartment;
      PathwayCompartment supposedPathway = null;
      for (final Compartment compartment : getModel().getCompartments()) {
        if (compartment instanceof PathwayCompartment) {
          if (compartment.contains(element)) {
            if (supposedPathway == null || compartment.getSize() < supposedPathway.getSize()) {
              supposedPathway = (PathwayCompartment) compartment;
            }
          }
        } else if (compartment.contains(element) && compartment.getSize() < supposedCompartment.getSize()) {
          supposedCompartment = compartment;
        }
      }
      if (supposedCompartment != nullCompartment) {
        supposedCompartment.addElement(element);
      }
      element.setPathway(supposedPathway);
    }
  }

  /**
   * Computes the ratio between the minimal object that should be visible, and
   * alias visible on the top level.
   *
   * @param alias alias for which computation is done
   * @param limit size of the minimal visible object
   * @return ratio between the minimal object that should be visible, and alias visible on the top level
   */
  private double computeRate(final Element alias, final double limit) {
    double length = alias.getWidth() / maxZoomFactor;
    double height = alias.getHeight() / maxZoomFactor;
    double size = length * height;
    return Math.ceil(limit / size);
  }

}
