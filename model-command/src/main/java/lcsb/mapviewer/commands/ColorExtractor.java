package lcsb.mapviewer.commands;

import java.awt.Color;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;

/**
 * Class that extracts color that should be used for drawing from
 * {@link DataOverlayEntry}.
 * 
 * @author Piotr Gawron
 *
 */
public class ColorExtractor {

  /**
   * Color that should be used for min values of {@link DataOverlayEntry#value}.
   */
  private Color minColor;

  /**
   * Color that should be used for max values of {@link DataOverlayEntry#value}.
   */
  private Color maxColor;

  private Color simpleColor;

  private Color neutralColor;

  /**
   * Default constructor.
   * 
   * @param minColor
   *          Color that should be used for min values of
   *          {@link DataOverlayEntry#value}
   * @param maxColor
   *          Color that should be used for max values of
   *          {@link DataOverlayEntry#value}
   */
  public ColorExtractor(final Color minColor, final Color maxColor, final Color simpleColor, final Color neutralColor) {
    if (minColor == null || maxColor == null || simpleColor == null || neutralColor == null) {
      throw new InvalidArgumentException("Parameters cannot be null");
    }
    this.minColor = minColor;
    this.maxColor = maxColor;
    this.simpleColor = simpleColor;
    this.neutralColor = neutralColor;
  }

  /**
   * Extracts color from {@link DataOverlayEntry} object.
   * 
   * @param colorSchema
   *          {@link DataOverlayEntry} from which {@link Color} should be extracted
   * 
   * @return color from {@link DataOverlayEntry} object
   */
  public Color getNormalizedColor(final DataOverlayEntry colorSchema) {
    if (colorSchema.getColor() != null) {
      return colorSchema.getColor();
    } else if (colorSchema.getValue() == null) {
      return simpleColor;
    } else {
      return getColorForValue(colorSchema.getValue());
    }
  }

  /**
   * Returns color from red - green scale for the given normalized double value
   * (from range -1,1).
   * 
   * @param value
   *          double value that should be converted into color
   * @return color for the double value
   */
  protected Color getColorForValue(final Double value) {
    int maxRed = maxColor.getRed();
    int maxGreen = maxColor.getGreen();
    int maxBlue = maxColor.getBlue();

    int minRed = minColor.getRed();
    int minGreen = minColor.getGreen();
    int minBlue = minColor.getBlue();

    int neutralRed = neutralColor.getRed();
    int neutralGreen = neutralColor.getGreen();
    int neutralBlue = neutralColor.getBlue();

    if (value > 0) {
      double ratio = value;
      int red = getColorForRatio(neutralRed, maxRed, ratio);
      int green = getColorForRatio(neutralGreen, maxGreen, ratio);
      int blue = getColorForRatio(neutralBlue, maxBlue, ratio);
      return new Color(red, green, blue);
    }
    if (value < 0) {
      double ratio = -value;
      int red = getColorForRatio(neutralRed, minRed, ratio);
      int green = getColorForRatio(neutralGreen, minGreen, ratio);
      int blue = getColorForRatio(neutralBlue, minBlue, ratio);
      return new Color(red, green, blue);
    }
    return neutralColor;
  }

  private int getColorForRatio(final int neutralRed, final int maxRed, final double ratio) {
    int red = (int) Math.round(neutralRed + (maxRed - neutralRed) * ratio);
    red = Math.max(0, red);
    red = Math.min(255, red);
    return red;
  }

  /**
   * @return the minColor
   * @see #minColor
   */
  public Color getMinColor() {
    return minColor;
  }

  /**
   * @return the maxColor
   * @see #maxColor
   */
  public Color getMaxColor() {
    return maxColor;
  }

  public Color getSimpleColor() {
    return simpleColor;
  }

  public Color getNeutralColor() {
    return neutralColor;
  }
}
