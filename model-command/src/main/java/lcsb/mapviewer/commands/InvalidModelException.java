package lcsb.mapviewer.commands;

/**
 * Exception thrown when the {@link lcsb.mapviewer.model.map.model.Model Model}
 * is inconsitent.
 * 
 * @author Piotr Gawron
 * 
 */
public class InvalidModelException extends Exception {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor with message passed in the argument.
   * 
   * @param string
   *          message of this exception
   */
  public InvalidModelException(final String string) {
    super(string);
  }

}
