package lcsb.mapviewer.commands;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Abstract class representing operation on the model. There are following steps
 * during processing command:
 * <ul>
 * <li>initialization with some data (constructor)</li>
 * <li>execution - command is performed</li>
 * <li>undo - if command was performed then this method will undo the operation
 * </li>
 * <li>redo - if command was undone then operation is redone</li>
 * </ul>
 * 
 * @author Piotr Gawron
 * 
 */
public abstract class ModelCommand {

  /**
   * Status of the command.
   */
  private ModelCommandStatus status = ModelCommandStatus.CREATED;

  /**
   * Model on which command is performed.
   */
  private Model model;

  /**
   * Region that is affected by this command. If null then command didn't affected
   * layout.
   */
  private Rectangle2D affectedRegion;

  /**
   * Default constructor.
   *
   * @param model
   *          {@link #model}
   */
  public ModelCommand(final Model model) {
    this.model = model;
  }

  /**
   * Undo the operation.
   *
   * @throws CommandExecutionException
   *           thrown when undo cannot be performed due to invalid input data
   */
  public void undo() throws CommandExecutionException {
    if (!ModelCommandStatus.EXECUTED.equals(getStatus())) {
      throw new InvalidStateException(
          "To undo command, the command must be executed first. " + getStatus() + " found.");
    } else {
      affectedRegion = null;
      undoImplementation();
    }
  }

  /**
   * Redo the operation if the command was undone.
   *
   * @throws CommandExecutionException
   *           thrown when undo cannot be executed due to problemtaic input data
   */
  public void redo() throws CommandExecutionException {
    if (!ModelCommandStatus.UNDONE.equals(getStatus())) {
      throw new InvalidStateException("To redo command, the command must be undone first. " + getStatus() + " found.");
    } else {
      affectedRegion = null;
      redoImplementation();
    }
  }

  /**
   * Executed the operation.
   *
   * @throws CommandExecutionException
   *           thrown when execution cannot be performed due to invalid input data
   */
  public void execute() throws CommandExecutionException {
    if (!ModelCommandStatus.CREATED.equals(getStatus()) && !ModelCommandStatus.UNDONE.equals(getStatus())) {
      throw new InvalidStateException(
          "To execute command, the command must be in CREATED or UNDONE state. " + getStatus() + " found.");
    } else {
      affectedRegion = null;
      executeImplementation();
      setStatus(ModelCommandStatus.EXECUTED);
    }
  }

  /**
   * Command specific implementation of {@link #undo()} operation.
   *
   * @throws CommandExecutionException
   *           thrown when undo cannot be executed due to problematic input data
   */
  protected abstract void undoImplementation() throws CommandExecutionException;

  /**
   * Command specific implementation of {@link #redo()} operation.
   *
   * @throws CommandExecutionException
   *           thrown when redo cannot be executed due to problematic input data
   */
  protected abstract void redoImplementation() throws CommandExecutionException;

  /**
   * Command specific implementation how to execute this command.
   *
   * @throws CommandExecutionException
   *           thrown when command cannot be executed due to problematic input
   *           data
   */
  protected abstract void executeImplementation() throws CommandExecutionException;

  /**
   * @return the status
   * @see #status
   */
  public ModelCommandStatus getStatus() {
    return status;
  }

  /**
   * @param status
   *          the status to set
   * @see #status
   */
  protected void setStatus(final ModelCommandStatus status) {
    this.status = status;
  }

  /**
   * @return the model
   * @see #model
   */
  protected Model getModel() {
    return model;
  }

  /**
   * @param model
   *          the model to set
   * @see #model
   */
  protected void setModel(final Model model) {
    this.model = model;
  }

  /**
   * @return the affectedRegion
   * @see #affectedRegion
   */
  public Rectangle2D getAffectedRegion() {
    return affectedRegion;
  }

  /**
   * Adds point to {@link #affectedRegion}.
   * 
   * @param point
   *          point to be included in affected region
   */
  protected void includeInAffectedRegion(final Point2D point) {
    if (affectedRegion == null) {
      affectedRegion = new Rectangle2D.Double(point.getY(), point.getY(), 0, 0);
    } else {
      affectedRegion.add(point);
    }
  }

  /**
   * Adds line to {@link #affectedRegion}.
   * 
   * @param pd
   *          line to be included in affected region
   */
  protected void includeInAffectedRegion(final PolylineData pd) {
    if (affectedRegion == null) {
      affectedRegion = new Rectangle2D.Double(pd.getStartPoint().getX(), pd.getStartPoint().getY(), 0, 0);
    }
    for (final Line2D point : pd.getLines()) {
      affectedRegion.add(point.getP1());
      affectedRegion.add(point.getP2());
    }
  }

  /**
   * Adds alias to {@link #affectedRegion}.
   * 
   * @param alias
   *          alias to be included in affected region
   */
  protected void includeInAffectedRegion(final Element alias) {
    if (affectedRegion == null) {
      affectedRegion = new Rectangle2D.Double(alias.getX(), alias.getY(), alias.getWidth(), alias.getHeight());
    } else {
      affectedRegion.add(alias.getX(), alias.getY());
      affectedRegion.add(alias.getX() + alias.getWidth(), alias.getY() + alias.getHeight());
    }
  }

  /**
   * Adds reaction to {@link #affectedRegion}.
   * 
   * @param reaction
   *          reaction to be included in affected region
   */
  protected void includeInAffectedRegion(final Reaction reaction) {
    if (affectedRegion == null) {
      Point2D point = reaction.getNodes().iterator().next().getLine().getStartPoint();
      affectedRegion = new Rectangle2D.Double(point.getX(), point.getY(), 0, 0);
    }
    for (final AbstractNode node : reaction.getNodes()) {
      includeInAffectedRegion(node.getLine());
    }

  }

}
