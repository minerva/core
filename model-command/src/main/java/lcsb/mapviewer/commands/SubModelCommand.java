package lcsb.mapviewer.commands;

import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Creates a new instance of the model with data limited to region described by
 * polygon.
 * 
 * @author Piotr Gawron
 * 
 */
public class SubModelCommand extends NewModelCommand {

  /**
   * Polygon that limits the area for the new model.
   * 
   */
  private Path2D polygon;

  private Set<Integer> elementIds;

  private Set<Integer> reactionIds;

  private boolean strictCutoff = true;

  /**
   * Default constructor.
   * 
   * @param model
   *          original {@link NewModelCommand#model}
   * @param polygon
   *          #polygon that limits the area for the new model
   */
  public SubModelCommand(final Model model, final Path2D polygon) {
    this(model, polygon, new HashSet<>(), new HashSet<>(), true);
  }

  public SubModelCommand(final Model model, final Path2D polygon, final Set<Integer> elementIds,
      final Set<Integer> reactionIds) {
    this(model, polygon, elementIds, reactionIds, true);
  }

  /**
   * Default constructor.
   * 
   * @param model
   *          original {@link NewModelCommand#model}
   * @param polygon
   *          #polygon that limits the area for the new model
   */
  public SubModelCommand(final Model model, final Path2D polygon, final Set<Integer> elementIds,
      final Set<Integer> reactionIds, final boolean strictCutoff) {
    super(model);
    this.polygon = polygon;
    this.elementIds = elementIds;
    this.reactionIds = reactionIds;
    this.strictCutoff = strictCutoff;
  }

  @Override
  public Model execute() {
    CopyCommand copy = new CopyCommand(getModel());
    Model result = copy.execute();

    Set<Element> inputElements = new HashSet<>();
    if (elementIds.size() == 0) {
      inputElements.addAll(result.getElements());
    } else {
      for (final Element element : getModel().getElements()) {
        if (elementIds.contains(element.getId())) {
          inputElements.add(result.getElementByElementId(element.getElementId()));
        }
      }
    }

    Set<Element> aliasNotToRemove = new HashSet<>();

    for (final Element alias : result.getElements()) {
      if (polygon.intersects(alias.getBorder())) {
        aliasNotToRemove.add(alias);
      }
    }
    if (!strictCutoff) {
      Set<Element> keepElements = new HashSet<>();
      for (Reaction reaction : result.getReactions()) {
        boolean keep = false;
        for (Line2D line : reaction.getLines()) {
          if (polygon.contains(line.getP1()) || polygon.contains(line.getP2())) {
            keep = true;
          }
        }
        if (keep) {
          for (final ReactionNode node : reaction.getReactionNodes()) {
            keepElements.add(node.getElement());
          }
        }
      }
      aliasNotToRemove.addAll(keepElements);
    }
    aliasNotToRemove.retainAll(inputElements);

    boolean added = false;
    do {
      Set<Element> iterativeAliasNotToRemove = new HashSet<>();
      for (final Element alias : aliasNotToRemove) {
        Element parent = alias.getCompartment();
        if (parent != null) {
          if (!aliasNotToRemove.contains(parent)) {
            iterativeAliasNotToRemove.add(parent);
          }
        }
        if (alias instanceof Species) {
          parent = ((Species) alias).getComplex();
          if (parent != null) {
            if (!aliasNotToRemove.contains(parent)) {
              iterativeAliasNotToRemove.add(parent);
            }
          }
        }
      }

      added = iterativeAliasNotToRemove.size() != 0;
      aliasNotToRemove.addAll(iterativeAliasNotToRemove);
    } while (added);

    List<Element> aliasToRemove = new ArrayList<>();
    for (final Element element : result.getElements()) {
      if (!(polygon.intersects(element.getBorder())) || !inputElements.contains(element)) {
        boolean remove = true;
        if (element instanceof Species) {
          remove = ((Species) element).getComplex() == null;
        }
        if (aliasNotToRemove.contains(element)) {
          remove = false;
        }
        if (remove) {
          aliasToRemove.add(element);
          if (element instanceof Complex) {
            List<Species> aliases = ((Complex) element).getAllChildren();
            aliasToRemove.addAll(aliases);
          }
        }
      }

    }
    for (final Element alias : aliasToRemove) {
      result.removeElement(alias);
    }

    Set<String> reactionStringIds = new HashSet<>();
    for (final Reaction reaction : getModel().getReactions()) {
      if (reactionIds.size() == 0 || reactionIds.contains(reaction.getId())) {
        reactionStringIds.add(reaction.getElementId());
      }
    }

    List<Reaction> reactionsToRemove = new ArrayList<>();
    for (final Reaction reaction : result.getReactions()) {
      boolean remove = false;
      if (!reactionStringIds.contains(reaction.getElementId())) {
        remove = true;
      } else {
        for (final ReactionNode node : reaction.getReactionNodes()) {
          if (!result.getElements().contains(node.getElement())) {
            remove = true;
            break;
          }
        }
      }
      if (remove) {
        reactionsToRemove.add(reaction);
      }
    }
    for (final Reaction reaction : reactionsToRemove) {
      result.removeReaction(reaction);
    }

    for (final Layer layer : result.getLayers()) {
      List<LayerText> textToRemove = new ArrayList<>();
      for (final LayerText text : layer.getTexts()) {
        if (!(polygon.intersects(text.getBorder())) || elementIds.size() > 0 || reactionIds.size() > 0) {
          textToRemove.add(text);
        }
      }
      for (final LayerText text : textToRemove) {
        layer.removeLayerText(text);
      }
    }

    return result;
  }

}
