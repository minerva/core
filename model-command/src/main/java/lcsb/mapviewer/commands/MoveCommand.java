package lcsb.mapviewer.commands;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationResidue;

import java.awt.geom.Line2D;

/**
 * Command which moves model by dx, dy coordinates.
 *
 * @author Piotr Gawron
 */
public class MoveCommand extends ModelCommand {

  /**
   * Delta x.
   */
  private final double dx;

  /**
   * Delta y.
   */
  private final double dy;

  /**
   * Default constructor.
   *
   * @param model model to move
   * @param dx    delta x
   * @param dy    delta y
   */
  public MoveCommand(final Model model, final double dx, final double dy) {
    super(model);
    this.dx = dx;
    this.dy = dy;
  }

  @Override
  protected void undoImplementation() {
    throw new NotImplementedException();
  }

  @Override
  protected void redoImplementation() {
    throw new NotImplementedException();
  }

  @Override
  protected void executeImplementation() {

    Model model = getModel();

    for (final Element element : model.getElements()) {
      element.setX(element.getX() + dx);
      element.setY(element.getY() + dy);
      element.setNameX(element.getNameX() + dx);
      element.setNameY(element.getNameY() + dy);
      if (element instanceof SpeciesWithModificationResidue) {
        for (final ModificationResidue mr : ((SpeciesWithModificationResidue) element).getModificationResidues()) {
          double x = mr.getX() + dx;
          double y = mr.getY() + dy;
          mr.setPosition(x, y);
          mr.setNameX(mr.getX());
          mr.setNameY(mr.getY());
        }
      }
    }
    for (final Reaction reaction : model.getReactions()) {
      for (final AbstractNode node : reaction.getNodes()) {
        for (int i = 0; i < node.getLine().getLines().size(); i++) {
          Line2D line = node.getLine().getLines().get(i);
          node.getLine().setLine(i, line.getX1() + dx, line.getY1() + dy, line.getX2() + dx, line.getY2() + dy);
        }
      }
      for (int i = 0; i < reaction.getLine().getLines().size(); i++) {
        Line2D line = reaction.getLine().getLines().get(i);
        reaction.getLine().setLine(i, line.getX1() + dx, line.getY1() + dy, line.getX2() + dx, line.getY2() + dy);
      }
    }

    for (Layer layer : model.getLayers()) {
      for (LayerText text : layer.getTexts()) {
        text.setX(text.getX() + dx);
        text.setY(text.getY() + dy);
      }
      for (LayerRect rect : layer.getRectangles()) {
        rect.setX(rect.getX() + dx);
        rect.setY(rect.getY() + dy);
      }
      for (LayerOval oval : layer.getOvals()) {
        oval.setX(oval.getX() + dx);
        oval.setY(oval.getY() + dy);
      }
      for (PolylineData line : layer.getLines()) {
        for (int i = 0; i < line.getLines().size(); i++) {
          Line2D l = line.getLines().get(i);
          line.setLine(i, l.getX1() + dx, l.getY1() + dy, l.getX2() + dx, l.getY2() + dy);
        }
      }
    }
  }

}
