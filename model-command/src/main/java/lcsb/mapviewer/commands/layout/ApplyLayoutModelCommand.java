package lcsb.mapviewer.commands.layout;

import java.awt.geom.Dimension2D;
import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.ModelCommand;
import lcsb.mapviewer.common.geometry.DoubleDimension;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.model.Model;

public abstract class ApplyLayoutModelCommand extends ModelCommand {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger();
  private Collection<BioEntity> bioEntities;
  private Double minX;
  private Double minY;
  private Double maxX;
  private Double maxY;
  private boolean addReactionPrefixes;

  private int idCounter = 0;

  public ApplyLayoutModelCommand(final Model model, final Collection<BioEntity> bioEntities, final Double minX, final Double minY, final Double maxX,
      final Double maxY, final boolean addReactionPrefixes) {
    super(model);
    if (bioEntities == null) {
      Set<BioEntity> allBioEntities = new HashSet<>();
      if (model != null) {
        allBioEntities.addAll(model.getBioEntities());
        for (final Model submodel : model.getSubmodels()) {
          allBioEntities.addAll(submodel.getBioEntities());
        }
      }
      this.setBioEntities(allBioEntities);
    } else {
      this.setBioEntities(bioEntities);
    }
    this.setMinX(minX);
    this.setMinY(minY);
    this.setMaxX(maxX);
    this.setMaxY(maxY);
    this.addReactionPrefixes = addReactionPrefixes;
  }

  protected Collection<BioEntity> getBioEntities() {
    return bioEntities;
  }

  protected void setBioEntities(final Collection<BioEntity> bioEntities) {
    this.bioEntities = bioEntities;
  }

  protected Double getMinX() {
    return minX;
  }

  protected void setMinX(final Double minX) {
    this.minX = minX;
  }

  protected Double getMinY() {
    return minY;
  }

  protected void setMinY(final Double minY) {
    this.minY = minY;
  }

  protected Double getMaxX() {
    return maxX;
  }

  protected void setMaxX(final Double maxX) {
    this.maxX = maxX;
  }

  protected Double getMaxY() {
    return maxY;
  }

  protected void setMaxY(final Double maxY) {
    this.maxY = maxY;
  }

  protected Point2D getStartPoint() {
    if (minX != null && minY != null) {
      return new Point2D.Double(minX, minY);
    } else {
      return null;
    }
  }

  protected Dimension2D getStartDimension() {
    if (minX != null && minY != null && maxX != null && maxY != null) {
      return new DoubleDimension(maxX - minX, maxY - minY);
    } else {
      return null;
    }
  }

  public boolean isAddReactionPrefixes() {
    return addReactionPrefixes;
  }

  protected String getNextId() {
    return (idCounter++) + "";
  }

}
