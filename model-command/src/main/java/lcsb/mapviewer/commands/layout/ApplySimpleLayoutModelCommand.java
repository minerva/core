package lcsb.mapviewer.commands.layout;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.common.geometry.DoubleDimension;
import lcsb.mapviewer.common.geometry.LineTransformation;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.converter.ZIndexPopulator;
import lcsb.mapviewer.converter.model.celldesigner.types.ModifierTypeUtils;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationResidue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.geom.AffineTransform;
import java.awt.geom.Dimension2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ApplySimpleLayoutModelCommand extends ApplyLayoutModelCommand {

  public static final double COMPARTMENT_BORDER = 10;
  private static final double BORDER_FROM_EXISTING_ELEMENTS = 10;
  private static final int SPECIES_WIDTH = 90;
  private static final int SPECIES_HEIGHT = 30;
  private static final double COMPLEX_PADDING = 5;

  private double borderOffset = COMPARTMENT_BORDER;
  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();
  private final PointTransformation pt = new PointTransformation();

  public ApplySimpleLayoutModelCommand(final Model model, final Collection<BioEntity> bioEntities, final Double minX, final Double minY,
                                       final Double maxX, final Double maxY, final boolean addReactionPrefixes) {
    super(model, bioEntities, minX, minY, maxX, maxY, addReactionPrefixes);
  }

  public ApplySimpleLayoutModelCommand(final Model model, final Collection<BioEntity> bioEntities) {
    this(model, bioEntities, null, null, null, null, false);
  }

  public ApplySimpleLayoutModelCommand(final Model model, final Collection<BioEntity> bioEntities, final boolean addReactionPrefixes) {
    this(model, bioEntities, null, null, null, null, addReactionPrefixes);
  }

  public ApplySimpleLayoutModelCommand(final Model model) {
    this(model, null);
  }

  private void createLayout(final Model model, final Collection<BioEntity> bioEntites, final Compartment compartment, final Point2D point,
                            final Dimension2D suggestedDimension) {
    Dimension2D dimension = suggestedDimension;
    if (dimension == null) {
      dimension = estimateDimension(bioEntites);
    }
    Point2D minPoint = point;
    if (minPoint == null) {
      minPoint = estimateLayoutMinPoint(model);
    }
    modifyModelSize(model, minPoint, dimension);
    final Collection<Element> elements = new HashSet<>();
    final Collection<Reaction> reactions = new HashSet<>();
    for (final BioEntity bioEntity : bioEntites) {
      if (bioEntity instanceof Element) {
        elements.add((Element) bioEntity);
      } else if (bioEntity instanceof Reaction) {
        reactions.add((Reaction) bioEntity);
      } else {
        throw new InvalidArgumentException("Unknown class type: " + bioEntity);
      }
    }

    modifyElementLocation(elements, null, minPoint, dimension);
    modifyReactionLocation(reactions);

    assignLineWidthIfNecessary(model.getBioEntities());

  }

  private void assignLineWidthIfNecessary(final List<BioEntity> bioEntities) {
    for (final BioEntity bioEntity : bioEntities) {
      if (bioEntity instanceof Species) {
        assignLineWidthIfNecessary((Species) bioEntity);
      } else if (bioEntity instanceof Compartment) {
        assignLineWidthIfNecessary((Compartment) bioEntity);
      }
    }

  }

  private void assignLineWidthIfNecessary(final Compartment compartment) {
    if (compartment.getInnerWidth() <= 1) {
      compartment.setInnerWidth(1.0);
    }
    if (compartment.getOuterWidth() < 1) {
      compartment.setOuterWidth(2.0);
    }
    if (compartment.getThickness() < 1) {
      compartment.setThickness(12.0);
    }
  }

  private void assignLineWidthIfNecessary(final Species species) {
    if (species.getLineWidth() == null) {
      if (species instanceof Complex) {
        species.setLineWidth(3.0);
      } else {
        species.setLineWidth(1.0);
      }
    }

  }

  protected void modifyElementLocation(final Collection<Element> elements, final Compartment parent, final Point2D minPoint,
                                       final Dimension2D dimension) {

    final Set<Compartment> compartments = new HashSet<>();
    final Set<Species> elementToAlign = new HashSet<>();
    final Map<Compartment, Set<Element>> elementsByStaticCompartment = new HashMap<>();
    for (final Element element : elements) {
      if (element.getWidth() == null || element.getWidth() == 0) {
        if (element instanceof Degraded) {
          element.setHeight(SPECIES_HEIGHT);
          element.setWidth(element.getHeight());
        } else {
          element.setWidth(SPECIES_WIDTH);
          element.setHeight(SPECIES_HEIGHT);
        }
      }
      if (element.getCompartment() == null || element.getCompartment() == parent) {
        if (element instanceof Compartment) {
          compartments.add((Compartment) element);
        } else if (element instanceof Species) {
          elementToAlign.add((Species) element);
        } else {
          throw new InvalidArgumentException("Unknown element type: " + element.getClass());
        }
      } else {
        Set<Element> elementsSet = elementsByStaticCompartment.get(element.getCompartment());
        if (elementsSet == null) {
          elementsSet = new HashSet<>();
          elementsByStaticCompartment.put(element.getCompartment(), elementsSet);
        }
        elementsSet.add(element);
      }
    }

    if (compartments.size() == 1 && elementToAlign.size() == 0) {
      final Compartment compartment = compartments.iterator().next();
      final Rectangle2D border = computeCompartmentBorder(compartment, elements, minPoint, dimension);
      compartment.setBorder(border);
      compartment.setNameX(border.getX() + COMPARTMENT_BORDER * 2);
      compartment.setNameY(border.getY() + COMPARTMENT_BORDER * 2);
      compartment.setNameWidth(border.getWidth() - COMPARTMENT_BORDER * 4);
      compartment.setNameHeight(border.getHeight() - COMPARTMENT_BORDER * 4);
      compartment.setNameHorizontalAlign(HorizontalAlign.LEFT);
      compartment.setNameVerticalAlign(VerticalAlign.TOP);
      final Point2D point = new Point2D.Double(border.getX(), border.getY());
      final Dimension2D recursiveDimension = new DoubleDimension(border.getWidth(), border.getHeight());
      final Set<Element> elementsToModify = new HashSet<>(compartment.getElements());
      elementsToModify.retainAll(elements);
      modifyElementLocation(elementsToModify, compartment, point, recursiveDimension);
    } else if (compartments.size() == 0) {
      modifySpeciesListLocation(elementToAlign, minPoint, dimension);
    } else {
      final Collection<Element> firstHalf = new HashSet<>();
      final Collection<Element> secondHalf = new HashSet<>();

      int firstHalfCount = 0;
      int secondHalfCount = 0;

      for (final Compartment compartment : compartments) {
        final int compSize = compartment.getAllSubElements().size() + 1;
        if (firstHalfCount < secondHalfCount) {
          firstHalf.add(compartment);
          firstHalfCount += compSize;
        } else {
          secondHalf.add(compartment);
          secondHalfCount += compSize;
        }
      }
      if (firstHalfCount < secondHalfCount) {
        firstHalf.addAll(elementToAlign);
        firstHalfCount += elementToAlign.size();
      } else {
        secondHalf.addAll(elementToAlign);
        secondHalfCount += elementToAlign.size();
      }
      if (dimension.getWidth() > dimension.getHeight()) {
        final Dimension2D recursiveDimension = new DoubleDimension(dimension.getWidth() / 2, dimension.getHeight());
        modifyElementLocation(firstHalf, parent, minPoint, recursiveDimension);
        final Point2D point = new Point2D.Double(minPoint.getX() + dimension.getWidth() / 2, minPoint.getY());
        modifyElementLocation(secondHalf, parent, point, recursiveDimension);
      } else {
        final Dimension2D recursiveDimension = new DoubleDimension(dimension.getWidth(), dimension.getHeight() / 2);
        modifyElementLocation(firstHalf, parent, minPoint, recursiveDimension);
        final Point2D point = new Point2D.Double(minPoint.getX(), minPoint.getY() + dimension.getHeight() / 2);
        modifyElementLocation(secondHalf, parent, point, recursiveDimension);
      }
    }
    final List<Compartment> compartmentWithElementsToRearrange = new ArrayList<>();
    compartmentWithElementsToRearrange.addAll(elementsByStaticCompartment.keySet());
    Collections.sort(compartmentWithElementsToRearrange, Element.SIZE_COMPARATOR);
    for (final Compartment compartment : compartmentWithElementsToRearrange) {
      final Point2D point = new Point2D.Double(compartment.getX(), compartment.getY());
      final Dimension2D compDimension = new DoubleDimension(compartment.getWidth(), compartment.getHeight());
      modifyElementLocation(elementsByStaticCompartment.get(compartment), compartment, point, compDimension);
    }

  }

  private Rectangle2D computeCompartmentBorder(final Compartment compartment, final Collection<Element> elements, final Point2D minPoint,
                                               final Dimension2D dimension) {
    final Rectangle2D border = new Rectangle2D.Double(minPoint.getX() + borderOffset,
        minPoint.getY() + borderOffset, dimension.getWidth() - borderOffset * 2,
        dimension.getHeight() - borderOffset * 2);
    for (final Element element : compartment.getElements()) {
      if (!elements.contains(element) && element.getBorder() != null && element.getX() != 0 && element.getY() != 0) {
        border.add(element.getBorder());
      }
    }
    return border;
  }

  private void modifyReactionLocation(final Collection<Reaction> reactions) {
    for (final Reaction reaction : reactions) {
      modifyReaction(reaction);
    }
  }

  private void modifyReaction(final Reaction reaction) {
    for (final AbstractNode node : reaction.getOperators()) {
      reaction.removeNode(node);
    }

    final Element productElement = reaction.getProducts().get(0).getElement();
    final Element reactantElement = reaction.getReactants().get(0).getElement();

    final Point2D centerLineStart = pt.getPointOnLine(reactantElement.getCenter(), productElement.getCenter(), 0.4);
    final Point2D centerLineEnd = pt.getPointOnLine(reactantElement.getCenter(), productElement.getCenter(), 0.6);
    final Point2D middle = pt.getPointOnLine(reactantElement.getCenter(), productElement.getCenter(), 0.5);

    final PolylineData centerLine = new PolylineData();
    // for self reactions
    if (centerLineStart.equals(centerLineEnd)) {
      centerLine.addLine(centerLineStart.getX(), centerLineStart.getY(), centerLineStart.getX(),
          centerLineStart.getY() + 50);
      centerLine.addLine(centerLineStart.getX(), centerLineStart.getY() + 50, centerLineEnd.getX(),
          centerLineEnd.getY());

      centerLineStart.setLocation(centerLineStart.getX(), centerLineStart.getY() + 50);
      centerLineEnd.setLocation(centerLineStart.getX(), centerLineEnd.getY() + 50);
    } else {
      centerLine.addLine(centerLineStart, centerLineEnd);
    }
    reaction.setLine(centerLine);

    modifyReactants(reaction, centerLineStart);
    modifyProducts(reaction, centerLineEnd);
    modifyModifiers(reaction, middle);

    if (super.isAddReactionPrefixes()) {
      if (reaction.getIdReaction().contains("__")) {
        logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, reaction), "Reaction identifier already prefixed");
      } else {
        super.getModel().removeReaction(reaction);
        reaction.setIdReaction("reaction_glyph_" + getNextId() + "__" + reaction.getIdReaction());
        super.getModel().addReaction(reaction);
      }
    }
  }

  private void modifyReactants(final Reaction reaction, final Point2D center) {
    NodeOperator operator = null;
    Point2D middle = center;
    if (reaction.getReactants().size() > 1) {
      operator = new AndOperator();
      final Point2D productPoint = reaction.getReactants().get(0).getElement().getCenter();
      final Point2D operatorPoint = pt.getPointOnLine(middle, productPoint, 0.5);
      operator.setLine(new PolylineData(operatorPoint, middle));
      reaction.addNode(operator);
      middle = operatorPoint;
    }
    for (final Reactant reactant : reaction.getReactants()) {
      final Element element = reactant.getElement();
      final PolylineData line = new PolylineData(element.getCenter(), middle);
      final Point2D startPoint = findCrossPoint(element, line);
      if (startPoint != null) {
        line.setStartPoint(startPoint);
      }
      if (reaction.isReversible()) {
        line.getBeginAtd().setArrowType(ArrowType.FULL);
      }
      reactant.setLine(line);
      if (operator != null) {
        operator.addInput(reactant);
      }
    }
  }

  private Point2D findCrossPoint(final Element element, final PolylineData line) {
    final LineTransformation lt = new LineTransformation();
    final Point2D startPoint = lt.getIntersectionWithPathIterator(
        new Line2D.Double(line.getStartPoint(), line.getEndPoint()),
        element.getBorder().getPathIterator(new AffineTransform()));
    return startPoint;
  }

  private void modifyModifiers(final Reaction reaction, final Point2D middle) {
    for (final Modifier modifier : reaction.getModifiers()) {
      final Element element = modifier.getElement();
      final PolylineData line = new PolylineData(element.getCenter(), middle);
      final Point2D startPoint = findCrossPoint(element, line);
      if (startPoint != null) {
        line.setStartPoint(startPoint);
      }
      modifier.setLine(line);
      new ModifierTypeUtils().updateLineEndPoint(modifier);
    }
  }

  private void modifyProducts(final Reaction reaction, final Point2D center) {
    Point2D middle = center;
    NodeOperator operator = null;
    if (reaction.getProducts().size() > 1) {
      operator = new AndOperator();
      final Point2D productPoint = reaction.getProducts().get(0).getElement().getCenter();
      final Point2D operatorPoint = pt.getPointOnLine(middle, productPoint, 0.5);
      operator.setLine(new PolylineData(operatorPoint, middle));
      reaction.addNode(operator);
      middle = operatorPoint;
    }
    for (final Product product : reaction.getProducts()) {
      final Element element = product.getElement();
      final PolylineData line = new PolylineData(middle, element.getCenter());
      final Point2D endPoint = findCrossPoint(element, line);
      if (endPoint != null) {
        line.setEndPoint(endPoint);
      }
      line.getEndAtd().setArrowType(ArrowType.FULL);
      product.setLine(line);
      if (operator != null) {
        operator.addOutput(product);
      }
    }

  }

  protected void modifySpeciesListLocation(final Set<Species> speciesList, final Point2D minPoint, final Dimension2D dimension) {
    final double middleX = dimension.getWidth() / 2 + minPoint.getX();
    final double middleY = dimension.getHeight() / 2 + minPoint.getY();

    double radiusX = Math.min(dimension.getWidth() / 3, dimension.getWidth() - SPECIES_WIDTH / 2);
    if (radiusX < 1) {
      radiusX = 1;
    }
    double radiusY = Math.min(dimension.getHeight() / 3, dimension.getHeight() - SPECIES_HEIGHT / 2);
    if (radiusY < 1) {
      radiusY = 1;
    }
    final double radius = Math.min(radiusX, radiusY);

    final double size = speciesList.size();
    double index = 0;

    for (final Species element : speciesList) {
      if (element.getComplex() == null) {
        final double angle = index / size * 2 * Math.PI;
        final double elementCenterX = middleX + Math.sin(angle) * radius;
        final double elementCenterY = middleY + Math.cos(angle) * radius;
        if (element instanceof Complex) {
          modifyComplexLocation((Complex) element, elementCenterX, elementCenterY);
        } else {
          if (element instanceof Degraded) {
            element.setHeight(SPECIES_HEIGHT);
            element.setWidth(element.getHeight());
          } else {
            element.setWidth(SPECIES_WIDTH);
            element.setHeight(SPECIES_HEIGHT);
          }
          element.setX(elementCenterX - SPECIES_WIDTH / 2);
          element.setY(elementCenterY - SPECIES_HEIGHT / 2);
        }
        assignNameCoordinates(element);
        index++;
      }
    }

    for (final Species element : speciesList) {
      if (element instanceof SpeciesWithModificationResidue) {
        modifyElementModificationResiduesLocation((SpeciesWithModificationResidue) element);
      }
    }

  }

  private void assignNameCoordinates(final Species element) {
    element.setNameX(element.getX());
    element.setNameY(element.getY());
    element.setNameWidth(element.getWidth());
    element.setNameHeight(element.getHeight());
    element.setNameHorizontalAlign(HorizontalAlign.CENTER);
    if (element instanceof Complex && !((Complex) element).getElements().isEmpty()) {
      element.setNameVerticalAlign(VerticalAlign.BOTTOM);
    } else {
      element.setNameVerticalAlign(VerticalAlign.MIDDLE);
    }
  }

  private void modifyElementModificationResiduesLocation(final SpeciesWithModificationResidue element) {
    for (final ModificationResidue mr : element.getModificationResidues()) {
      final List<Point2D> usedPoints = new ArrayList<>();
      for (final ModificationResidue inUseModification : element.getModificationResidues()) {
        if (inUseModification.getX() != null && inUseModification.getY() != null) {
          usedPoints.add(inUseModification.getCenter());
        }
      }
      final Point2D point = findFarthestEmptyPositionOnRectangle(((Species) element).getBorder(), usedPoints);
      mr.setPosition(point.getX() - mr.getWidth() / 2, point.getY() - mr.getHeight() / 2);
      mr.setNameX(mr.getX());
      mr.setNameY(mr.getY());
    }
  }

  Point2D findFarthestEmptyPositionOnRectangle(final Rectangle2D border, final List<Point2D> usedPoints) {
    // list of points reduced to one dimension
    // every point is taking two position - so we can easier solve wrapping
    // issues
    final List<Double> linearizedPoints = new ArrayList<>();
    // there are two "guarding" points at the beginning and the end
    linearizedPoints.add(0.0);
    linearizedPoints.add(4 * (border.getWidth() + border.getHeight()));

    for (final Point2D point : usedPoints) {
      Double linearizedPoint = null;
      if (Math.abs(point.getY() - border.getMinY()) < Configuration.EPSILON) {
        // upper edge
        linearizedPoint = point.getX() - border.getMinX();
      } else if (Math.abs(point.getX() - border.getMaxX()) < Configuration.EPSILON) {
        // right edge
        linearizedPoint = point.getY() - border.getMinY() + border.getWidth();
      } else if (Math.abs(point.getY() - border.getMaxY()) < Configuration.EPSILON) {
        // bottom edge
        linearizedPoint = border.getMaxX() - point.getX() + border.getWidth() + border.getHeight();
      } else if (Math.abs(point.getX() - border.getMinX()) < Configuration.EPSILON) {
        // left edge
        linearizedPoint = border.getMaxY() - point.getY() + border.getWidth() + border.getHeight() + border.getWidth();
      } else {
        logger.warn("Cannot align modification position on the species border");
      }
      if (linearizedPoint != null) {
        linearizedPoints.add(linearizedPoint);
        linearizedPoints.add(linearizedPoint + 2 * (border.getWidth() + border.getHeight()));
      }

    }
    Collections.sort(linearizedPoints);

    Double longest = linearizedPoints.get(1) - linearizedPoints.get(0);
    Double position = (linearizedPoints.get(1) + linearizedPoints.get(0)) / 2;
    for (int i = 1; i < linearizedPoints.size() - 1; i++) {
      if (longest < linearizedPoints.get(i + 1) - linearizedPoints.get(i)) {
        longest = linearizedPoints.get(i + 1) - linearizedPoints.get(i);
        position = (linearizedPoints.get(i + 1) + linearizedPoints.get(i)) / 2;
      }
    }

    if (position > 2 * (border.getWidth() + border.getHeight())) {
      position -= 2 * (border.getWidth() + border.getHeight());
    }

    if (position < border.getWidth()) {
      // upper edge
      return new Point2D.Double(border.getMinX() + position, border.getMinY());
    } else if (position < border.getWidth() + border.getHeight()) {
      // right edge
      return new Point2D.Double(border.getMaxX(), border.getMinY() + position - border.getWidth());
    } else if (position < border.getWidth() + border.getHeight() + border.getWidth()) {
      // bottom edge
      return new Point2D.Double(border.getMinX() + position - border.getHeight() - border.getWidth(), border.getMaxY());
    } else {
      // left edge
      return new Point2D.Double(border.getMinX(),
          border.getMinY() + position - border.getWidth() - border.getHeight() - border.getWidth());
    }
  }

  protected void modifyModelSize(final Model model, final Point2D minPoint, final Dimension2D dimension) {
    double width = model.getWidth();
    double height = model.getHeight();
    width = Math.max(width, minPoint.getX() + dimension.getWidth());
    height = Math.max(height, minPoint.getY() + dimension.getHeight());
    model.setWidth(width);
    model.setHeight(height);
    borderOffset = Math.max(borderOffset, Math.max(width, height) * 0.01);
  }

  protected Point2D estimateLayoutMinPoint(final Model model) {
    final double minX = 0.0;
    double minY = 0.0;
    for (final Element element : model.getElements()) {
      if (!getBioEntities().contains(element)) {
        if (element.getWidth() > 0 && element.getHeight() > 0) {
          minY = Math.max(minY, element.getBorder().getMaxY() + BORDER_FROM_EXISTING_ELEMENTS);
        }
      }
    }
    return new Point2D.Double(minX, minY);
  }

  protected Dimension2D estimateDimension(final Collection<BioEntity> bioEntites) {
    int elementsCount = 0;
    for (final BioEntity bioEntity : bioEntites) {
      if (bioEntity instanceof Element) {
        elementsCount++;
      }
    }
    double suggestedSize = Math.max(100 * elementsCount,
        Math.sqrt((SPECIES_WIDTH + 10) * (SPECIES_HEIGHT + 10) * elementsCount));
    if (bioEntites.size() > 0) {
      suggestedSize = Math.max(suggestedSize, 2 * (SPECIES_WIDTH + 10));
    }
    return new DoubleDimension(suggestedSize, suggestedSize);
  }

  @Override
  protected void undoImplementation() throws CommandExecutionException {
    throw new NotImplementedException();

  }

  @Override
  protected void redoImplementation() throws CommandExecutionException {
    throw new NotImplementedException();
  }

  @Override
  protected void executeImplementation() throws CommandExecutionException {
    final Set<Model> models = new HashSet<>();
    models.add(this.getModel());
    models.addAll(this.getModel().getSubmodels());
    for (final Model model : models) {
      final Collection<BioEntity> bioEntites = new HashSet<>();
      for (final BioEntity bioEntity : this.getBioEntities()) {
        if (bioEntity.getModel() == model) {
          bioEntites.add(bioEntity);
        }
      }
      if (bioEntites.size() > 0) {
        Dimension2D dimension = null;
        Point2D minPoint = null;
        if (model == this.getModel()) {
          dimension = getStartDimension();
          minPoint = getStartPoint();
        }
        createLayout(model, bioEntites, null, minPoint, dimension);
      }
      new ZIndexPopulator().populateZIndex(bioEntites);
    }
  }

  protected void modifyComplexLocation(final Complex complex, final double centerX, final double centerY) {
    computeComplexSize(complex);

    double childWidth = SPECIES_WIDTH;
    double childHeight = SPECIES_HEIGHT;
    for (final Species species : complex.getElements()) {
      childWidth = Math.max(childWidth, species.getWidth());
      childHeight = Math.max(childHeight, species.getHeight());
    }
    final int rowSize = (int) Math.ceil(Math.sqrt(complex.getElements().size()));

    complex.setX(centerX - complex.getWidth() / 2);
    complex.setY(centerY - complex.getHeight() / 2);

    int x = 0;
    int y = 0;

    for (final Species species : complex.getElements()) {
      species.setX(complex.getX() + COMPLEX_PADDING + (COMPLEX_PADDING + childWidth) * x);
      species.setY(complex.getY() + COMPLEX_PADDING + (COMPLEX_PADDING + childHeight) * y);

      x++;
      if (x == rowSize) {
        x = 0;
        y++;
      }
      if (species instanceof Complex) {
        modifyComplexLocation((Complex) species, species.getCenterX(), species.getCenterY());
      }
      assignNameCoordinates(species);
    }

  }

  private void computeComplexSize(final Complex complex) {
    double childWidth = SPECIES_WIDTH;
    double childHeight = SPECIES_HEIGHT;
    for (final Species species : complex.getElements()) {
      if (species instanceof Complex) {
        computeComplexSize((Complex) species);
      } else {
        species.setWidth(SPECIES_WIDTH);
        if (species instanceof Degraded) {
          species.setHeight(species.getWidth());
        } else {
          species.setHeight(SPECIES_HEIGHT);
        }
      }
      childWidth = Math.max(childWidth, species.getWidth());
      childHeight = Math.max(childHeight, species.getHeight());
    }
    int rowSize = (int) Math.ceil(Math.sqrt(complex.getElements().size()));
    int rowCount = 0;
    if (rowSize == 0) {
      rowSize = 1;
      rowCount = 1;
    } else {
      rowCount = (complex.getElements().size() + rowSize - 1) / rowSize;
    }

    complex.setWidth((childWidth + COMPLEX_PADDING) * rowSize + COMPLEX_PADDING);
    complex.setHeight((childHeight + COMPLEX_PADDING) * rowCount + COMPLEX_PADDING);
  }

}
