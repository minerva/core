package lcsb.mapviewer.commands;

import java.awt.Color;
import java.util.ArrayList;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * This {@link ModelCommand} clear info about colors in a model.
 * 
 * @author Piotr Gawron
 * 
 */
public class ClearColorModelCommand extends ModelCommand {

  /**
   * Coloring command that will clear colors.
   */
  private ColorModelCommand colorModelCommand;

  /**
   * Default constructor.
   * 
   * @param model
   *          original model
   */
  public ClearColorModelCommand(final Model model) {
    super(model);
    colorModelCommand = new ColorModelCommand(model, new ArrayList<>(),
        new ColorExtractor(Color.WHITE, Color.WHITE, Color.WHITE, Color.WHITE));
  }

  @Override
  protected void undoImplementation() throws CommandExecutionException {
    colorModelCommand.undo();
  }

  @Override
  protected void redoImplementation() throws CommandExecutionException {
    colorModelCommand.redo();
  }

  @Override
  protected void executeImplementation() throws CommandExecutionException {
    colorModelCommand.execute();
    for (final Element element : colorModelCommand.getModel().getElements()) {
      element.setFontColor(Color.BLACK);
    }
  }
}
