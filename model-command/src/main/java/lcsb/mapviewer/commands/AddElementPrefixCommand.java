package lcsb.mapviewer.commands;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;

public class AddElementPrefixCommand extends ModelCommand {

  private String prefix;

  public AddElementPrefixCommand(final Model model, final String prefix) {
    super(model);
    this.prefix = prefix;
  }

  @Override
  protected void undoImplementation() {
    throw new NotImplementedException();
  }

  @Override
  protected void redoImplementation() {
    throw new NotImplementedException();
  }

  @Override
  protected void executeImplementation() {

    Model model = getModel();

    for (final Element element : model.getElements()) {
      element.setElementId(prefix + element.getElementId());
      model.addElement(element);
    }
    for (final Reaction reaction : model.getReactions()) {
      reaction.setIdReaction(prefix + reaction.getElementId());
      model.addReaction(reaction);
    }
    for (Layer layer : model.getLayers()) {
      layer.setLayerId(prefix + layer.getLayerId());
    }
  }

}
