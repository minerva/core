package lcsb.mapviewer.commands;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.ArrowTypeData;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.overlay.InvalidDataOverlayException;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * This {@link ModelCommand} colors a model (nodes and reactions) according to
 * set of {@link DataOverlayEntry rules}.
 * 
 * @author Piotr Gawron
 * 
 */
public class ColorModelCommand extends ModelCommand {

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger();

  /**
   * Set of color schemas used in this command to color model.
   */
  private Collection<DataOverlayEntry> dataOverlayEntries;

  /**
   * Object that helps to convert {@link DataOverlayEntry} values into colors.
   * 
   */
  private ColorExtractor colorExtractor;

  /**
   * Util class for all {@link BioEntity} elements.
   */
  private ElementUtils eu = new ElementUtils();

  private StringComparator stringComparator = new StringComparator();

  private Map<DataOverlayEntry, Integer> matches = new HashMap<>();

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          object that helps to convert {@link DataOverlayEntry} values into
   *          colors
   * @param model
   *          original model
   * @param schemas
   *          set of color schemas used in this command to color model.
   */
  public ColorModelCommand(final Model model, final Collection<DataOverlayEntry> schemas, final ColorExtractor colorExtractor) {
    super(model);
    this.dataOverlayEntries = schemas;
    this.colorExtractor = colorExtractor;
    for (final DataOverlayEntry dataOverlayEntry : schemas) {
      matches.put(dataOverlayEntry, 0);
    }
  }

  /**
   * Applies color schema into the reaction.
   * 
   * @param reaction
   *          object to be colored
   * @param schema
   *          color schema to be used
   */
  void applyColor(final Reaction reaction, final DataOverlayEntry schema) {
    if (!reaction.getReactants().get(0).getLine().getColor().equals(Color.BLACK)
        && !isEqualColoring(schema, reaction.getReactants().get(0).getLine().getColor())) {
      logger.debug("At least two rows try to set color to reaction: " + reaction.getIdReaction());
    }

    Color color = colorExtractor.getNormalizedColor(schema);
    reaction.getLine().setColor(color);
    if (schema.getLineWidth() != null) {
      reaction.getLine().setWidth(schema.getLineWidth());
    }
    for (final AbstractNode node : reaction.getNodes()) {
      node.getLine().setColor(color);
      if (schema.getLineWidth() != null) {
        node.getLine().setWidth(schema.getLineWidth());
      }
    }
    if (schema.getReverseReaction() != null && schema.getReverseReaction()) {
      ArrowTypeData atdBegining = reaction.getReactants().get(0).getLine().getBeginAtd();
      ArrowTypeData atdEnd = reaction.getProducts().get(0).getLine().getEndAtd();
      for (final Reactant reactant : reaction.getReactants()) {
        reactant.getLine().setBeginAtd(atdEnd);
      }
      for (final Product product : reaction.getProducts()) {
        product.getLine().setEndAtd(atdBegining);
      }
    }
  }

  /**
   * Applies color schema into the {@link Element}.
   *
   * @param element
   *          object to be colored
   * @param schema
   *          color schema to be used
   * @throws InvalidDataOverlayException
   *           thrown when alias was already colored by other schema
   */
  private void applyColor(final Element element, final DataOverlayEntry schema) {
    if (element instanceof Species) {
      if (!element.getFillColor().equals(Color.WHITE) && !isEqualColoring(schema, element.getFillColor())) {
        logger.debug("At least two rows try to set color to element: " + element.getName());
      }
      element.setFillColor(colorExtractor.getNormalizedColor(schema));
      element.setBorderColor(Color.BLACK);
    }
  }

  private void applyColor(final BioEntity element, final DataOverlayEntry schema) {
    if (element instanceof Element) {
      applyColor((Element) element, schema);
    } else if (element instanceof Reaction) {
      applyColor((Reaction) element, schema);
    } else {
      throw new InvalidArgumentException("Don't know how to handle: " + element);
    }
  }

  /**
   * Checks if the coloring schema should be used for the reaction.
   * 
   * @param reaction
   *          reaction to which coloring schema is checked
   * @param schema
   *          coloring schema
   * @return <code>true</code> if coloring schema should be used for
   *         {@link Reaction}, <code>false</code> otherwise
   */
  private boolean match(final Reaction reaction, final DataOverlayEntry schema) {
    if (!schema.getName().isEmpty()) {
      if (!reaction.getName().equalsIgnoreCase(schema.getName())) {
        return false;
      }
    }
    if (!modelMatch(reaction.getModelData(), schema)) {
      return false;
    }

    for (final MiriamData md : schema.getMiriamData()) {
      if (!reaction.getMiriamData().contains(md)) {
        return false;
      }
    }

    return true;
  }

  protected boolean match(final BioEntity element, final DataOverlayEntry schema) {
    if (schema.getElementId() != null && !schema.getElementId().isEmpty()) {
      if (!element.getElementId().equalsIgnoreCase(schema.getElementId())) {
        return false;
      }
    }
    if (element instanceof Element) {
      return match((Element) element, schema);
    } else if (element instanceof Reaction) {
      return match((Reaction) element, schema);
    } else {
      throw new InvalidArgumentException("Don't know how to handle object: " + element);
    }
  }

  /**
   * Checks if the coloring schema should be used for the {@link Element}.
   * 
   * @param element
   *          {@link Element} for which coloring schema is checked
   * @param schema
   *          coloring schema
   * @return <code>true</code> if coloring schema should be used for
   *         {@link Element}, <code>false</code> otherwise
   */
  protected boolean match(final Element element, final DataOverlayEntry schema) {
    if (element instanceof Species) {
      if (!modelMatch(element.getModelData(), schema)) {
        return false;
      }
      if (!schema.getName().isEmpty()) {
        if (!element.getName().equalsIgnoreCase(schema.getName())) {
          return false;
        }
      }
      if (schema.getTypes().size() > 0) {
        boolean found = false;
        for (final Class<?> clazz : schema.getTypes()) {
          if (clazz.isAssignableFrom(element.getClass())) {
            found = true;
          }
        }

        if (!found) {
          return false;
        }
      }
      for (final MiriamData md : schema.getMiriamData()) {
        boolean found = false;
        for (final MiriamData elementMiriamData : element.getMiriamData()) {
          if (elementMiriamData.getDataType().equals(md.getDataType())
              && elementMiriamData.getResource().equals(md.getResource())) {
            found = true;
          }
        }
        if (!found) {
          return false;
        }
      }

      if (schema.getCompartments().size() > 0) {
        boolean found = false;
        for (final Compartment compartment : element.getModelData().getModel().getCompartments()) {
          for (final String compartmentName : schema.getCompartments()) {
            if (compartment.getName().equalsIgnoreCase(compartmentName)) {
              if (compartment.cross(element)) {
                found = true;
              }
            }
          }
        }
        if (!found) {
          return false;
        }
      }

      return true;
    } else {
      return false;
    }
  }

  private boolean modelMatch(final ModelData model, final DataOverlayEntry schema) {
    if (schema.getModelName() != null && !schema.getModelName().isEmpty()) {
      if (model == null) {
        logger.warn("Model of element is null...");
        return false;
      }
      if (stringComparator.compare(model.getName(), schema.getModelName()) != 0) {
        return false;
      }
    }
    return true;
  }

  private boolean isEqualColoring(final DataOverlayEntry schema, final Color color) {
    return Objects.equals(colorExtractor.getNormalizedColor(schema), color);
  }

  private boolean isEqualColoring(final DataOverlayEntry colorSchema, final DataOverlayEntry colorSchema2) {
    return isEqualColoring(colorSchema, colorExtractor.getNormalizedColor(colorSchema2));
  }

  /**
   * Checks which coloring schemas weren't used in the model coloring process.
   * 
   * @return list of schemas that weren't used during coloring
   */
  public Collection<DataOverlayEntry> getMissingSchema() {
    List<DataOverlayEntry> result = new ArrayList<DataOverlayEntry>();
    for (final DataOverlayEntry schema : dataOverlayEntries) {
      boolean found = false;

      for (final BioEntity element : getModel().getBioEntities()) {
        if (match(element, schema)) {
          found = true;
        }
      }
      if (!found) {
        result.add(schema);
      }
    }
    return result;
  }

  /**
   * Returns list of elements ({@link Reaction reactions} and {@link Element
   * elements}) that are modified by the coloring command.
   * 
   * @return {@link Map}, where key corresponds to modified {@link Reaction} or
   *         {@link Element} and value is a {@link DataOverlayEntry} that should
   *         be used for coloring
   */
  public Map<BioEntity, DataOverlayEntry> getModifiedElements() {
    Map<BioEntity, DataOverlayEntry> result = new HashMap<>();

    List<Model> models = new ArrayList<>();
    models.add(getModel());
    models.addAll(getModel().getSubmodels());

    for (final Model model : models) {
      for (final BioEntity element : model.getBioEntities()) {
        for (final DataOverlayEntry schema : dataOverlayEntries) {
          if (match(element, schema)) {
            if (result.get(element) != null && !isEqualColoring(result.get(element), schema)
                && !colorExtractor.getNormalizedColor(result.get(element)).equals(Color.WHITE)) {
              logger.debug(eu.getElementTag(element) + "BioEntity is colored by more than one rule.");
            }
            result.put(element, schema);
          }
        }
      }
    }
    return result;
  }

  @Override
  protected void undoImplementation() throws CommandExecutionException {
    throw new NotImplementedException();

  }

  @Override
  protected void redoImplementation() throws CommandExecutionException {
    throw new NotImplementedException();
  }

  @Override
  protected void executeImplementation() throws CommandExecutionException {
    colorModel(getModel(), true);
  }

  @Override
  public void execute() {
    try {
      super.execute();
    } catch (CommandExecutionException e) {
      throw new InvalidStateException(e);
    }
  }

  /**
   * Colors parameter model using coloring for this command. This method is used
   * internally to color either top {@link Model} or one of it's submodels.
   *
   * @param result
   *          model to color
   * @param top
   *          is the model a top (parent) model
   */
  private void colorModel(final Model result, final boolean top) {
    for (Drawable drawable : result.getDrawables()) {
      drawable.setBorderColor(Color.BLACK);
    }
    for (final Element element : result.getElements()) {
      element.setFillColor(Color.WHITE);
      element.setGlyph(null);
    }
    for (final Reaction reaction : result.getReactions()) {
      for (final AbstractNode node : reaction.getNodes()) {
        node.getLine().setColor(Color.BLACK);
      }
    }

    for (final DataOverlayEntry entry : dataOverlayEntries) {
      for (final BioEntity element : result.getBioEntities()) {
        if (match(element, entry)) {
          matches.put(entry, matches.get(entry) + 1);
          applyColor(element, entry);
        }
      }
    }

    if (top) {
      for (final ModelSubmodelConnection connection : result.getSubmodelConnections()) {
        colorModel(connection.getSubmodel().getModel(), false);
      }
    }

  }

  public Map<DataOverlayEntry, Integer> getMatches() {
    return matches;
  }

}
