package lcsb.mapviewer.commands;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidClassException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.kinetics.SbmlArgument;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.model.map.model.ElementSubmodelConnection;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Command that creates a new instance of the model with the same data.
 * 
 */

public class CopyCommand extends NewModelCommand {

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  /**
   * Default constructor.
   * 
   * @param model
   *          {@link #model}
   */
  public CopyCommand(final Model model) {
    super(model);
  }

  /**
   * Executed the operation.
   * 
   * @return copy of the model
   */
  public Model execute() {
    Map<ModelData, ModelData> copies = new HashMap<>();

    Model model = getModel();

    synchronized (model) {
      if (model.getProject() != null && model.getProject().getTopModel().getModelData() != model.getModelData()) {
        Model parent = model.getProject().getTopModel();
        CopyCommand copyParentCommand = new CopyCommand(parent);
        Model copy = copyParentCommand.execute();
        for (final ModelData submodel : copy.getProject().getModels()) {
          if (Objects.equals(submodel.getName(), model.getName())) {
            return submodel.getModel();
          }
        }
        throw new InvalidStateException("Problem with copying submodel of a model");
      }

      Queue<ModelData> children = new LinkedList<>();
      children.add(model.getModelData());

      while (!children.isEmpty()) {
        ModelData child = children.poll();
        if (copies.get(child) == null) {

          Model copy = createNotNestedCopy(child);
          copies.put(child, copy.getModelData());

          for (final ModelSubmodelConnection submodelConnection : child.getSubmodels()) {
            children.add(submodelConnection.getSubmodel());
          }
          for (final Element element : child.getElements()) {
            if (element.getSubmodel() != null) {
              children.add(element.getSubmodel().getSubmodel());
            }
          }
        }
      }

      Model result = copies.get(model.getModelData()).getModel();

      for (final ModelData modelCopy : copies.values()) {
        try {
          result.getProject().addModel(modelCopy);
          assignModelCopies(modelCopy, copies);
        } catch (final InvalidModelException e) {
          throw new InvalidArgumentException(e);
        }
      }
      return result;
    }
  }

  /**
   * Assign information about submodel to the copied links.
   * 
   * @param modelCopy
   *          copy of a model where substitution should be made
   * @param copies
   *          map with map copies
   * @throws InvalidModelException
   *           when there is inconsistency in data
   */
  private void assignModelCopies(final ModelData modelCopy, final Map<ModelData, ModelData> copies) throws InvalidModelException {
    for (final ModelSubmodelConnection connection : modelCopy.getSubmodels()) {
      // copy connection reference
      ModelData original = connection.getSubmodel();
      ModelData copy = copies.get(original);
      if (copy == null) {
        throw new InvalidModelException("Original model contain reference to model that wasn't copied");
      }
      connection.setSubmodel(copy);

      // copy connection parent reference
      if (connection.getParentModel() != null) {
        original = connection.getParentModel();
        copy = copies.get(original);
        if (copy == null) {
          if (!original.equals(modelCopy)) {
            throw new InvalidModelException("Original model contain reference to model that wasn't copied");
          }
        } else {
          connection.setParentModel(copy);
        }
      }
    }
    for (final Element alias : modelCopy.getElements()) {
      // if alias has connection to submodel
      if (alias.getSubmodel() != null) {
        ElementSubmodelConnection connection = alias.getSubmodel();
        // copy information about submodel
        ModelData original = connection.getSubmodel();
        ModelData copy = copies.get(original);

        if (copy == null) {
          throw new InvalidModelException("Original model contain reference to model that wasn't copied");
        }
        connection.setSubmodel(copy);

        // copy information about original element
        if (connection.getFromElement() != null) {
          connection.setFromElement(modelCopy.getModel().getElementByElementId(connection.getFromElement().getElementId()));
        }
        // copy information about reference element
        if (connection.getToElement() != null) {
          connection.setToElement(modelCopy.getModel().getElementByElementId(connection.getToElement().getElementId()));
        }
      }
    }
  }

  /**
   * Creates copy of the model without modifying information about submodels
   * (information about original submodels will be left in the copy).
   * 
   * @param model
   *          original model to copy
   * @return copy of the model
   */
  protected Model createNotNestedCopy(final ModelData model) {
    Model result = new ModelFullIndexed(null);

    for (final SbmlUnit unit : model.getUnits()) {
      result.addUnit(unit.copy());
    }

    for (final SbmlFunction function : model.getFunctions()) {
      result.addFunction(function.copy());
    }

    for (final SbmlParameter parameter : model.getParameters()) {
      result.addParameter(parameter.copy());
    }

    for (final Element alias : model.getElements()) {
      if (alias instanceof Compartment) {
        Compartment copy = ((Compartment) alias).copy();
        copy.getElements().clear();
        copy.setCompartment(null);
        result.addElement(copy);
      }

    }

    for (final Element alias : model.getElements()) {
      if (alias instanceof Species) {
        Species copy = ((Species) alias).copy();
        copy.setCompartment(null);
        result.addElement(copy);

        Compartment parentCompartment = alias.getCompartment();

        if (parentCompartment != null) {
          parentCompartment = result.getElementByElementId(parentCompartment.getElementId());
          copy.setCompartment(parentCompartment);
        }

      } else if (alias instanceof Compartment) {

        Compartment parentCompartment = alias.getCompartment();

        if (parentCompartment != null) {
          Compartment copy = result.getElementByElementId(alias.getElementId());
          parentCompartment = result.getElementByElementId(parentCompartment.getElementId());
          copy.setCompartment(parentCompartment);
        }
      } else {
        throw new InvalidClassException("Don't know what to do with: " + alias.getClass());
      }
    }

    for (final Layer layer : model.getLayers()) {
      result.addLayer(layer.copy());
    }

    for (final Reaction reaction : model.getReactions()) {
      result.addReaction(createCopy(reaction));
    }

    assignSimpleDataToCopy(result, model);
    new Project().addModel(result);

    for (final Element element : result.getElements()) {
      updateElementReferences(element, result, model);
    }
    for (final Reaction reaction : result.getReactions()) {
      updateReactionReferences(reaction, result);
    }
    for (final ModelSubmodelConnection connection : model.getSubmodels()) {
      result.addSubmodelConnection(connection.copy());
    }

    for (final Author author : model.getAuthors()) {
      result.addAuthor(author.copy());
    }

    for (final MiriamData md : model.getMiriamData()) {
      result.addMiriamData(new MiriamData(md));
    }

    if (model.getCreationDate() != null) {
      result.setCreationDate((Calendar) (model.getCreationDate().clone()));
    }

    for (final Calendar calendar : model.getModificationDates()) {
      result.addModificationDate((Calendar) calendar.clone());
    }

    return result;
  }

  /**
   * Copies simple information about model to the copy.
   * 
   * @param copy
   *          to this model data will be copied
   * @param original
   *          original model
   */
  private void assignSimpleDataToCopy(final Model copy, final ModelData original) {
    copy.setName(original.getName());

    copy.setWidth(original.getWidth());
    copy.setHeight(original.getHeight());
    copy.setNotes(original.getNotes());
    copy.setIdModel(original.getIdModel());
    copy.setZoomLevels(original.getZoomLevels());
    copy.setTileSize(original.getTileSize());
  }

  /**
   * Creates copy of the reaction that doesn't contain any references to the
   * original (so we have to substitute all references with the new ones...).
   * 
   * @param reaction
   *          original reaction
   * @return copy of the reaction
   */
  Reaction createCopy(final Reaction reaction) {
    Reaction copy = reaction.copy();
    for (final AbstractNode node : reaction.getNodes()) {
      node.setReaction(reaction);
    }
    copy.getNodes().clear();

    for (final AbstractNode node : reaction.getNodes()) {
      AbstractNode nodeCopy = node.copy();
      nodeCopy.setLine(new PolylineData(nodeCopy.getLine()));
      copy.addNode(nodeCopy);
    }

    for (int i = 0; i < copy.getOperators().size(); i++) {
      NodeOperator nodeCopy = copy.getOperators().get(i);
      NodeOperator nodeOriginal = reaction.getOperators().get(i);
      for (int j = 0; j < nodeCopy.getInputs().size(); j++) {
        AbstractNode input = nodeCopy.getInputs().get(j);
        int index1 = reaction.getOperators().indexOf(input);
        int index2 = reaction.getNodes().indexOf(input);
        if (index1 >= 0) {
          nodeCopy.getInputs().set(j, copy.getOperators().get(index1));
          copy.getOperators().get(index1).setNodeOperatorForInput(nodeCopy);

          reaction.getOperators().get(index1).setNodeOperatorForInput(nodeOriginal);
        } else if (index2 >= 0) {
          nodeCopy.getInputs().set(j, copy.getNodes().get(index2));
          copy.getNodes().get(index2).setNodeOperatorForInput(nodeCopy);

          reaction.getNodes().get(index2).setNodeOperatorForInput(nodeOriginal);
        } else {
          throw new InvalidStateException("WTF");
        }
      }

      for (int j = 0; j < nodeCopy.getOutputs().size(); j++) {
        AbstractNode output = nodeCopy.getOutputs().get(j);
        int index1 = reaction.getOperators().indexOf(output);
        int index2 = reaction.getNodes().indexOf(output);
        if (index1 >= 0) {
          nodeCopy.getOutputs().set(j, copy.getOperators().get(index1));
          copy.getOperators().get(index1).setNodeOperatorForOutput(nodeCopy);

          reaction.getOperators().get(index1).setNodeOperatorForOutput(nodeOriginal);
        } else if (index2 >= 0) {
          nodeCopy.getOutputs().set(j, copy.getNodes().get(index2));
          copy.getNodes().get(index2).setNodeOperatorForOutput(nodeCopy);

          reaction.getNodes().get(index2).setNodeOperatorForOutput(nodeOriginal);
        } else {
          throw new InvalidStateException("WTF");
        }
      }
    }

    return copy;
  }

  /**
   * Updates references to elements and aliases in reaction to the objects taken
   * from model.
   * 
   * @param reaction
   *          references in this reaction will be updated
   * @param model
   *          references from this model will be taken
   */
  private void updateReactionReferences(final Reaction reaction, final Model model) {
    reaction.setModel(model);
    for (final ReactionNode reactionNode : reaction.getReactionNodes()) {
      reactionNode.setElement(model.getElementByElementId(reactionNode.getElement().getElementId()));
    }
    SbmlKinetics kinetics = reaction.getKinetics();
    if (kinetics != null) {
      Collection<SbmlArgument> arguments = new HashSet<>();
      for (final SbmlArgument argument : kinetics.getArguments()) {
        if (argument instanceof Element) {
          arguments.add(model.getElementByElementId(argument.getElementId()));
        } else if (argument instanceof SbmlParameter) {
          if (model.getParameterById(argument.getElementId()) != null) {
            arguments.add(model.getParameterById(argument.getElementId()));
          } else {
            arguments.add(argument);
          }
        } else if (argument instanceof SbmlFunction) {
          arguments.add(model.getFunctionById(argument.getElementId()));
        } else {
          throw new InvalidStateException("Don't know what to do with class: " + argument.getClass());
        }
      }
      kinetics.removeArguments(kinetics.getArguments());
      kinetics.addArguments(arguments);
    }

  }

  /**
   * Updates information (like elements, parent aliases) in the alias with the
   * data from new model.
   * 
   * @param element
   *          object to be updated
   * @param model
   *          data from this model will be used for update
   * @param originalModel
   *          original model from which alias copy was created
   */
  private void updateElementReferences(final Element element, final Model model, final ModelData originalModel) {
    if (element instanceof Compartment) {
      Compartment compartment = (Compartment) element;
      Compartment original = originalModel.getModel().getElementByElementId(element.getElementId());
      for (final Element child : original.getElements()) {
        compartment.addElement(model.getElementByElementId(child.getElementId()));
      }
    }

    if (element instanceof Complex) {
      Complex complex = (Complex) element;
      for (int i = 0; i < complex.getElements().size(); i++) {
        Species newElement = model.getElementByElementId(complex.getElements().get(i).getElementId());
        complex.getElements().set(i, newElement);
      }
    }
    if (element instanceof Species) {
      Species species = (Species) element;

      if (species.getComplex() != null) {
        species.setComplex((Complex) model.getElementByElementId(species.getComplex().getElementId()));
      }

    }

    element.setModel(model);

    if (element.getCompartment() != null) {
      element.setCompartment(model.getElementByElementId(element.getCompartment().getElementId()));
    }
  }
}
