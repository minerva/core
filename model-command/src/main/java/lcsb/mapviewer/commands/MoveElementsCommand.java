package lcsb.mapviewer.commands;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationResidue;

/**
 * Command which moves elements in model by dx, dy coordinates.
 * 
 * @author Piotr Gawron
 * 
 */
public class MoveElementsCommand extends ModelCommand {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger();

  /**
   * Delta x.
   */
  private double dx;

  /**
   * Delta y.
   */
  private double dy;

  /**
   * List of objects to move.
   */
  private List<Object> objectsToMove = new ArrayList<>();

  /**
   * Default constructor.
   * 
   * @param model
   *          model to move
   * @param elements
   *          elements that should be moved
   * @param dx
   *          delta x
   * @param dy
   *          delta y
   */
  public MoveElementsCommand(final Model model, final Collection<? extends BioEntity> elements, final double dx, final double dy) {
    super(model);
    this.dx = dx;
    this.dy = dy;
    for (final Object object : elements) {
      if (object instanceof Element) {
        if (((Element) object).getModel() != model) {
          throw new InvalidArgumentException("Object doesnt belong to specified model: " + object);
        }
      } else if (object instanceof Reaction) {
        if (((Reaction) object).getModel() != model) {
          throw new InvalidArgumentException("Object doesnt belong to specified model: " + object);
        }
      } else {
        throw new InvalidArgumentException("Cannot move element: " + object);
      }
    }
    objectsToMove.addAll(elements);
  }

  @Override
  protected void undoImplementation() {
    dx = -dx;
    dy = -dy;
    executeImplementation();
    dx = -dx;
    dy = -dy;
    setStatus(ModelCommandStatus.UNDONE);
  }

  @Override
  protected void redoImplementation() {
    executeImplementation();
    setStatus(ModelCommandStatus.EXECUTED);
  }

  @Override
  protected void executeImplementation() {
    Set<Element> aliases = new HashSet<>();

    for (final Object object : objectsToMove) {
      if (object instanceof Element) {
        Element alias = (Element) object;

        includeInAffectedRegion(alias);

        alias.setX(alias.getX() + dx);
        alias.setY(alias.getY() + dy);
        alias.setNameX(alias.getNameX() + dx);
        alias.setNameY(alias.getNameY() + dy);

        if (alias instanceof SpeciesWithModificationResidue) {
          for (final ModificationResidue mr : ((SpeciesWithModificationResidue) alias).getModificationResidues()) {
            mr.setX(mr.getX() + dx);
            mr.setY(mr.getY() + dy);
          }
        }
        includeInAffectedRegion(alias);

        aliases.add(alias);
      } else if (object instanceof Reaction) {
        Reaction reaction = (Reaction) object;
        for (final Reactant node : reaction.getReactants()) {
          Point2D startPoint = node.getLine().getStartPoint();
          moveNode(node);
          node.getLine().setStartPoint(startPoint);
        }
        for (final Product node : reaction.getProducts()) {
          Point2D endPoint = node.getLine().getEndPoint();
          moveNode(node);
          node.getLine().setEndPoint(endPoint);
        }
        for (final Modifier node : reaction.getModifiers()) {
          Point2D startPoint = node.getLine().getStartPoint();
          moveNode(node);
          node.getLine().setStartPoint(startPoint);
        }
        for (final NodeOperator node : reaction.getOperators()) {
          moveNode(node);
        }
        includeInAffectedRegion(reaction);
      } else {
        throw new InvalidStateException("Unknown class type: " + object);
      }
    }
    if (aliases.size() > 0) {
      // TODO this must be improved, we cannot do full search on every move
      for (final Reaction reaction : getModel().getReactions()) {
        for (final ReactionNode node : reaction.getReactionNodes()) {
          if (aliases.contains(node.getElement())) {
            if (node instanceof Reactant) {
              Point2D point = node.getLine().getStartPoint();
              node.getLine().setStartPoint(point.getX() + dx, point.getY() + dy);

              // we don't have to include point that we change as it's already
              // on the border of the element
              includeInAffectedRegion(node.getLine().getLines().get(0).getP2());
            } else if (node instanceof Modifier) {
              Point2D point = node.getLine().getStartPoint();
              node.getLine().setStartPoint(point.getX() + dx, point.getY() + dy);
              // we don't have to include point that we change as it's already
              // on the border of the element
              includeInAffectedRegion(node.getLine().getLines().get(0).getP2());
            } else if (node instanceof Product) {
              Point2D point = node.getLine().getEndPoint();
              node.getLine().setEndPoint(point.getX() + dx, point.getY() + dy);
              // we don't have to include point that we change as it's already
              // on the border of the element
              includeInAffectedRegion(node.getLine().getEndPoint());
            }
          }
        }
      }
    }
  }

  private void moveNode(final AbstractNode node) {
    for (int i = 0; i < node.getLine().getLines().size(); i++) {
      Line2D line = node.getLine().getLines().get(i);
      node.getLine().setLine(i, line.getX1() + dx, line.getY1() + dy, line.getX2() + dx, line.getY2() + dy);
    }
  }
}
