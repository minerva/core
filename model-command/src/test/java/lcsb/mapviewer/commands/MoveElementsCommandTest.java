package lcsb.mapviewer.commands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;

public class MoveElementsCommandTest extends CommandTestFunctions {

  private ModelComparator modelComparator = new ModelComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testMoveAlias() throws Exception {
    Model model = getModelForFile("testFiles/spliting_test_Case.xml", false);
    Model model2 = getModelForFile("testFiles/spliting_test_Case.xml", false);
    Element alias = model.getElementByElementId("sa2");
    Element alias2 = model.getElementByElementId("sa1");
    double anotherAliasX = alias2.getX();
    double anotherAliasY = alias2.getY();

    List<BioEntity> list = new ArrayList<>();
    list.add(alias);
    double x = alias.getX();
    double y = alias.getY();

    double dx = 10;
    double dy = 2;

    // models should be equal before move
    assertEquals(0, modelComparator.compare(model, model2));

    MoveElementsCommand moveCommand = new MoveElementsCommand(model, list, dx, dy);
    moveCommand.execute();

    // after move models should be different
    assertTrue(0 != modelComparator.compare(model, model2));

    // check new coordinates
    assertEquals(x + dx, alias.getX(), Configuration.EPSILON);
    assertEquals(y + dy, alias.getY(), Configuration.EPSILON);

    // and check if another alias didn't change coordinates
    assertEquals(anotherAliasX, alias2.getX(), Configuration.EPSILON);
    assertEquals(anotherAliasY, alias2.getY(), Configuration.EPSILON);

    list = new ArrayList<>();
    list.add(model2.getElementByElementId("sa2"));
    MoveElementsCommand moveCommand2 = new MoveElementsCommand(model2, list, dx, dy);
    moveCommand2.execute();

    // if we perform the same operator on the second model then they should be
    // equal
    assertEquals(0, modelComparator.compare(model, model2));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testMoveSpecies() throws Exception {
    Model model = getModelForFile("testFiles/spliting_test_Case.xml", false);
    List<BioEntity> list = new ArrayList<>();
    list.add(Mockito.mock(BioEntity.class));

    MoveElementsCommand moveCommand = new MoveElementsCommand(model, list, 10, 10);
    moveCommand.execute();
  }

  @Test
  public void testAliasWithReaction() throws Exception {
    Model model = getModelForFile("testFiles/spliting_test_Case.xml", false);
    Model model2 = getModelForFile("testFiles/spliting_test_Case.xml", false);
    Element alias = model.getElementByElementId("sa1");
    Element alias2 = model.getElementByElementId("sa2");
    double anotherAliasX = alias2.getX();
    double anotherAliasY = alias2.getY();
    Reaction reaction = model.getReactionByReactionId("re1");

    List<BioEntity> list = new ArrayList<>();
    list.add(alias);
    double x = reaction.getReactants().get(0).getLine().getStartPoint().getX();
    double y = reaction.getReactants().get(0).getLine().getStartPoint().getY();

    double dx = 10;
    double dy = 2;

    // models should be equal before move
    assertEquals(0, modelComparator.compare(model, model2));

    MoveElementsCommand moveCommand = new MoveElementsCommand(model, list, dx, dy);
    moveCommand.execute();

    // after move models should be different
    assertTrue(0 != modelComparator.compare(model, model2));

    // check new coordinates of reaction
    assertEquals(x + dx, reaction.getReactants().get(0).getLine().getStartPoint().getX(), Configuration.EPSILON);
    assertEquals(y + dy, reaction.getReactants().get(0).getLine().getStartPoint().getY(), Configuration.EPSILON);

    // and check if another alias didn't change coordinates
    assertEquals(anotherAliasX, alias2.getX(), Configuration.EPSILON);
    assertEquals(anotherAliasY, alias2.getY(), Configuration.EPSILON);

    list = new ArrayList<>();
    list.add(model2.getElementByElementId("sa1"));
    MoveElementsCommand moveCommand2 = new MoveElementsCommand(model2, list, dx, dy);
    moveCommand2.execute();

    // if we perform the same operator on the second model then they should be
    // equal
    assertEquals(0, modelComparator.compare(model, model2));
  }

  @Test
  public void testMoveReaction() throws Exception {
    Model model = getModelForFile("testFiles/spliting_test_Case.xml", false);
    Element alias2 = model.getElementByElementId("sa1");
    double anotherAliasX = alias2.getX();
    double anotherAliasY = alias2.getY();
    Reaction reaction = model.getReactionByReactionId("re1");

    List<BioEntity> list = new ArrayList<>();
    list.add(reaction);
    double x = reaction.getReactants().get(0).getLine().getStartPoint().getX();
    double y = reaction.getReactants().get(0).getLine().getStartPoint().getY();

    double x2 = reaction.getReactants().get(0).getLine().getEndPoint().getX();
    double y2 = reaction.getReactants().get(0).getLine().getEndPoint().getY();

    double dx = 10;
    double dy = 2;

    MoveElementsCommand moveCommand = new MoveElementsCommand(model, list, dx, dy);
    moveCommand.execute();

    // check new coordinates of reaction (point attached to alias shouldn't
    // move, the one not attached should move)
    assertEquals(x, reaction.getReactants().get(0).getLine().getStartPoint().getX(), Configuration.EPSILON);
    assertEquals(y, reaction.getReactants().get(0).getLine().getStartPoint().getY(), Configuration.EPSILON);
    assertEquals(x2 + dx, reaction.getReactants().get(0).getLine().getEndPoint().getX(), Configuration.EPSILON);
    assertEquals(y2 + dy, reaction.getReactants().get(0).getLine().getEndPoint().getY(), Configuration.EPSILON);

    // and check if another alias didn't change coordinates
    assertEquals(anotherAliasX, alias2.getX(), Configuration.EPSILON);
    assertEquals(anotherAliasY, alias2.getY(), Configuration.EPSILON);
  }

  @Test
  public void testUndo() throws Exception {
    Model model = getModelForFile("testFiles/spliting_test_Case.xml", false);
    Model model2 = getModelForFile("testFiles/spliting_test_Case.xml", false);
    Element alias = model.getElementByElementId("sa2");

    List<BioEntity> list = new ArrayList<>();
    list.add(alias);

    double dx = 10;
    double dy = 2;

    // models should be equal before move
    assertEquals(0, modelComparator.compare(model, model2));

    MoveElementsCommand moveCommand = new MoveElementsCommand(model, list, dx, dy);
    moveCommand.execute();

    // after move models should be different
    assertTrue(0 != modelComparator.compare(model, model2));

    // undo command
    moveCommand.undo();

    // after undo they should be the same again
    assertEquals(0, modelComparator.compare(model, model2));

    moveCommand.redo();

    // after redo they should be different again
    assertTrue(0 != modelComparator.compare(model, model2));
  }

  @Test
  public void testGetAffectedRegion() throws Exception {
    Model model = getModelForFile("testFiles/spliting_test_Case.xml", false);
    Element alias = model.getElementByElementId("sa2");

    List<BioEntity> list = new ArrayList<>();
    list.add(alias);
    double x = alias.getX();
    double y = alias.getY();

    double dx = 10;
    double dy = 2;

    double minx = x;
    double maxx = alias.getWidth() + x + dx;

    double miny = y;
    double maxy = alias.getHeight() + y + dy;

    MoveElementsCommand moveCommand = new MoveElementsCommand(model, list, dx, dy);
    assertNull(moveCommand.getAffectedRegion());
    moveCommand.execute();
    assertNotNull(moveCommand.getAffectedRegion());
    Rectangle2D affectedRegion = moveCommand.getAffectedRegion();
    assertEquals(minx, affectedRegion.getX(), Configuration.EPSILON);
    assertEquals(miny, affectedRegion.getY(), Configuration.EPSILON);
    assertEquals(maxx, affectedRegion.getX() + affectedRegion.getWidth(), Configuration.EPSILON);
    assertEquals(maxy, affectedRegion.getY() + affectedRegion.getHeight(), Configuration.EPSILON);

    moveCommand.undo();

    affectedRegion = moveCommand.getAffectedRegion();
    assertEquals(minx, affectedRegion.getX(), Configuration.EPSILON);
    assertEquals(miny, affectedRegion.getY(), Configuration.EPSILON);
    assertEquals(maxx, affectedRegion.getX() + affectedRegion.getWidth(), Configuration.EPSILON);
    assertEquals(maxy, affectedRegion.getY() + affectedRegion.getHeight(), Configuration.EPSILON);
  }

}
