package lcsb.mapviewer.commands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.util.Arrays;
import java.util.HashSet;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;

public class SubModelCommandTest extends CommandTestFunctions {

  private ModelComparator comparator = new ModelComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetSubmodel1() throws Exception {
    Model model = getModelForFile("testFiles/spliting_test_Case.xml", true);

    Path2D polygon = new Path2D.Double();
    polygon.moveTo(0, 0);
    polygon.lineTo(0, 100);
    polygon.lineTo(100, 100);
    polygon.lineTo(100, 0);
    polygon.closePath();

    Model copy = new SubModelCommand(model, polygon).execute();

    assertEquals(2, copy.getElements().size());
    assertEquals(0, copy.getReactions().size());
  }

  @Test
  public void testGetSubmodel2() throws Exception {
    Model model = getModelForFile("testFiles/spliting_test_Case.xml", true);

    Path2D polygon = new Path2D.Double();
    polygon.moveTo(50, 50);
    polygon.lineTo(350, 50);
    polygon.lineTo(350, 200);
    polygon.lineTo(50, 200);
    polygon.closePath();

    Model copy = new SubModelCommand(model, polygon).execute();

    assertEquals(9, copy.getElements().size());
    assertEquals(1, copy.getReactions().size());
  }

  @Test
  public void testGetSubmodelWithDisableStrictCutoof() throws Exception {
    Model model = getModelForFile("testFiles/spliting_test_Case.xml", true);

    Path2D polygon = new Path2D.Double();
    polygon.moveTo(50, 50);
    polygon.lineTo(350, 50);
    polygon.lineTo(350, 200);
    polygon.lineTo(50, 200);
    polygon.closePath();

    Model copy = new SubModelCommand(model, polygon, new HashSet<>(), new HashSet<>(), false).execute();

    assertEquals(10, copy.getElements().size());
    assertEquals(3, copy.getReactions().size());
  }

  @Test
  public void testGetSubmodel3() throws Exception {
    Model model = getModelForFile("testFiles/spliting_test_Case.xml", true);

    Path2D polygon = new Path2D.Double();
    polygon.moveTo(0, 200);
    polygon.lineTo(350, 200);
    polygon.lineTo(350, 400);
    polygon.lineTo(0, 400);
    polygon.closePath();

    Model copy = new SubModelCommand(model, polygon).execute();

    double dx = 10;
    double dy = -10;

    new MoveCommand(copy, dx, dy).execute();

    assertEquals(model.getElementByElementId("sa3").getCenterX(), copy.getElementByElementId("sa3").getCenterX() - dx,
        EPSILON);
    assertEquals(model.getElementByElementId("sa3").getCenterY(), copy.getElementByElementId("sa3").getCenterY() - dy,
        EPSILON);

    assertEquals(model.getReactionByReactionId("re3").getLines().get(0).getX2(),
        copy.getReactionByReactionId("re3").getLines().get(0).getX2() - dx, EPSILON);
    assertEquals(model.getReactionByReactionId("re3").getLines().get(0).getY2(),
        copy.getReactionByReactionId("re3").getLines().get(0).getY2() - dy, EPSILON);

    Model copy2 = serializeViaCellDesigner(copy);

    // check if after conversion to xml everything works
    assertEquals(0, comparator.compare(copy, copy2));
  }

  @Test
  public void testGetSubmodelWithoutCompartments() throws Exception {
    Model model = getModelForFile("testFiles/compartments.xml", true);

    Path2D polygon = new Path2D.Double();
    polygon.moveTo(0, 10);
    polygon.lineTo(10, 10);
    polygon.lineTo(10, 0);
    polygon.lineTo(0, 0);
    polygon.closePath();

    Model copy = new SubModelCommand(model, polygon).execute();

    // we should cut off some of compartments
    assertFalse(model.getLayers().iterator().next().getTexts().size() == copy.getLayers().iterator().next().getTexts()
        .size());
  }

  @Test
  public void testGetSubmodelWithoutCompartments2() throws Exception {
    Model model = getModelForFile("testFiles/problematic/cutting_without_compartment.xml", true);

    Path2D polygon = new Path2D.Double();
    polygon.moveTo(0, 0);
    polygon.lineTo(0, 500);
    polygon.lineTo(300, 500);
    polygon.lineTo(300, 0);
    polygon.closePath();

    Model copy = new SubModelCommand(model, polygon).execute();

    Model model2 = serializeViaCellDesigner(copy);

    assertEquals(0, comparator.compare(copy, model2));
  }

  @Test
  public void testGetSubmodelWithElementIds() throws Exception {
    Model model = getModelForFile("testFiles/spliting_test_Case.xml", false);

    Path2D polygon = new Path2D.Double();
    polygon.moveTo(0, 0);
    polygon.lineTo(0, 1000);
    polygon.lineTo(1000, 1000);
    polygon.lineTo(1000, 0);
    polygon.closePath();
    model.getElementByElementId("sa2").setId(-2);

    Model copy = new SubModelCommand(model, polygon, new HashSet<>(Arrays.asList(-2)), new HashSet<>()).execute();

    assertEquals(1, copy.getElements().size());
    assertEquals(0, copy.getReactions().size());
  }

  @Test
  public void testGetSubmodelWithElementIdsAndTextArea() throws Exception {
    Model model = getModelForFile("testFiles/spliting_test_Case.xml", false);

    Path2D polygon = new Path2D.Double();
    polygon.moveTo(0, 0);
    polygon.lineTo(0, 1000);
    polygon.lineTo(1000, 1000);
    polygon.lineTo(1000, 0);
    polygon.closePath();
    model.getElementByElementId("sa2").setId(-2);

    Layer layer = new Layer();
    LayerText text = new LayerText(new Rectangle2D.Double(10, 10, 20, 20), "xy");
    layer.addLayerText(text);
    model.addLayer(layer);

    Model copy = new SubModelCommand(model, polygon, new HashSet<>(Arrays.asList(-2)), new HashSet<>()).execute();

    assertEquals(1, copy.getElements().size());
    assertEquals(0, copy.getReactions().size());
    assertEquals(1, copy.getLayers().size());
    assertEquals(0, copy.getLayers().iterator().next().getTexts().size());
  }

  @Test
  public void testGetSubmodelWithEmptyElementIds() throws Exception {
    Model model = getModelForFile("testFiles/spliting_test_Case.xml", true);

    Path2D polygon = new Path2D.Double();
    polygon.moveTo(0, 0);
    polygon.lineTo(0, 1000);
    polygon.lineTo(1000, 1000);
    polygon.lineTo(1000, 0);
    polygon.closePath();

    Model copy = new SubModelCommand(model, polygon, new HashSet<>(), new HashSet<>(), true).execute();

    assertEquals(12, copy.getElements().size());
    assertEquals(3, copy.getReactions().size());
  }

  @Test
  public void testGetSubmodelWithReactionIds() throws Exception {
    Model model = getModelForFile("testFiles/spliting_test_Case.xml", false);

    Path2D polygon = new Path2D.Double();
    polygon.moveTo(0, 0);
    polygon.lineTo(0, 1000);
    polygon.lineTo(1000, 1000);
    polygon.lineTo(1000, 0);
    polygon.closePath();
    model.getReactions().iterator().next().setId(-2);

    Model copy = new SubModelCommand(model, polygon, new HashSet<>(), new HashSet<>(Arrays.asList(-2)), true).execute();

    assertEquals(1, copy.getReactions().size());
  }

}
