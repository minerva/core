package lcsb.mapviewer.commands.layout;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.commands.CommandTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.geometry.DoubleDimension;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.Residue;
import org.junit.Test;

import java.awt.geom.Dimension2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ApplySimpleLayoutModelCommandTest extends CommandTestFunctions {

  @Test
  public void testEstimateLayoutMinPoint() {
    Model model = new ModelFullIndexed(null);
    Protein protein = new GenericProtein("id");
    protein.setX(5);
    protein.setY(10);
    protein.setWidth(50);
    protein.setHeight(100);
    model.addElement(protein);

    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(model, new ArrayList<>());
    Point2D minPoint = layoutModelCommand.estimateLayoutMinPoint(model);
    assertNotNull(minPoint);
    assertTrue(minPoint.getX() > protein.getBorder().getMaxX() || minPoint.getY() > protein.getBorder().getMaxY());
  }

  @Test
  public void testEstimateLayoutMinPointIgnoringElements() {
    Model model = new ModelFullIndexed(null);
    Protein protein = new GenericProtein("id");
    protein.setX(5);
    protein.setY(10);
    protein.setWidth(50);
    protein.setHeight(100);
    model.addElement(protein);

    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(model, model.getBioEntities());
    Point2D minPoint = layoutModelCommand.estimateLayoutMinPoint(model);
    assertNotNull(minPoint);
    assertEquals(0.0, minPoint.getX(), Configuration.EPSILON);
    assertEquals(0.0, minPoint.getY(), Configuration.EPSILON);
  }

  @Test
  public void testEstimateLayoutMinPointWithDataWithoutLayout() {
    Model model = new ModelFullIndexed(null);
    Protein protein = new GenericProtein("id");
    model.addElement(protein);

    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(model, model.getBioEntities());
    Point2D minPoint = layoutModelCommand.estimateLayoutMinPoint(model);
    assertNotNull(minPoint);
    assertEquals(0.0, minPoint.getX(), Configuration.EPSILON);
    assertEquals(0.0, minPoint.getY(), Configuration.EPSILON);
  }

  @Test
  public void testEstimateLayoutMinPointWithEmptyMap() {
    Model model = new ModelFullIndexed(null);

    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(model, model.getBioEntities());
    Point2D minPoint = layoutModelCommand.estimateLayoutMinPoint(model);
    assertNotNull(minPoint);
    assertEquals(0.0, minPoint.getX(), Configuration.EPSILON);
    assertEquals(0.0, minPoint.getY(), Configuration.EPSILON);
  }

  @Test
  public void testModifyModelSize() {
    Model model = new ModelFullIndexed(null);

    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(model);
    Point2D minPoint = new Point2D.Double(2, 3);
    Dimension2D dimension = new DoubleDimension(4, 5);
    layoutModelCommand.modifyModelSize(model, minPoint, dimension);
    assertEquals(6.0, model.getWidth(), Configuration.EPSILON);
    assertEquals(8.0, model.getHeight(), Configuration.EPSILON);
  }

  @Test
  public void testEstimateDimension() {
    Model model = new ModelFullIndexed(null);

    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(model);

    Dimension2D dimension = layoutModelCommand.estimateDimension(new ArrayList<>());

    assertEquals(0.0, dimension.getWidth(), Configuration.EPSILON);
    assertEquals(0.0, dimension.getHeight(), Configuration.EPSILON);
  }

  @Test
  public void testEstimateDimensionWithElements() {
    Model model = new ModelFullIndexed(null);
    Protein protein = new GenericProtein("id");
    List<BioEntity> bioEntities = new ArrayList<>();
    bioEntities.add(protein);

    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(model);

    Dimension2D dimension = layoutModelCommand.estimateDimension(bioEntities);

    assertTrue(dimension.getWidth() > 0.0);
    assertTrue(dimension.getHeight() > 0.0);
  }

  @Test
  public void testModifyComplexWithSimpleChildren() {
    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(null);
    Complex complex = createComplex();
    GenericProtein p1 = createProtein();
    GenericProtein p2 = createProtein();
    complex.addSpecies(p1);
    complex.addSpecies(p2);

    layoutModelCommand.modifyComplexLocation(complex, 100, 100);

    assertFalse(p1.contains(p2));
    assertTrue(complex.contains(p1));
    assertTrue(complex.contains(p2));
  }

  @Test
  public void testModifyComplexWithSubComplexes() {
    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(null);
    Complex complex = createComplex();
    Complex complex2 = createComplex();
    GenericProtein p1 = createProtein();
    GenericProtein p2 = createProtein();
    complex.addSpecies(p1);
    complex2.addSpecies(p2);
    complex.addSpecies(complex2);

    layoutModelCommand.modifyComplexLocation(complex, 100, 100);

    assertFalse(p1.contains(p2));
    assertTrue(complex.contains(p1));
    assertTrue(complex.contains(p2));
    assertTrue(complex.contains(complex2));
    assertTrue(complex2.contains(p2));
    assertFalse(complex2.contains(p1));
  }

  @Test
  public void testModifySpeciesLocation() {
    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(null);
    GenericProtein p1 = createProtein();
    GenericProtein p2 = createProtein();
    Set<Species> elements = new HashSet<>();
    elements.add(p1);
    elements.add(p2);

    layoutModelCommand.modifySpeciesListLocation(elements, new Point2D.Double(100, 100), new DoubleDimension(200, 200));

    assertTrue(p1.getX() >= 100);
    assertTrue(p1.getY() >= 100);
    assertTrue(p1.getBorder().getMaxX() <= 300);
    assertTrue(p1.getBorder().getMaxY() <= 300);
    assertTrue(p2.getX() >= 100);
    assertTrue(p2.getY() >= 100);
    assertTrue(p2.getBorder().getMaxX() <= 300);
    assertTrue(p2.getBorder().getMaxY() <= 300);
    assertNotNull(p2.getNameX());
    assertNotNull(p2.getNameY());
    assertNotNull(p2.getNameWidth());
    assertNotNull(p2.getNameHeight());
    assertNotNull(p2.getNameHorizontalAlign());
    assertNotNull(p2.getNameVerticalAlign());
  }

  @Test
  public void testModifyResiduesLocationInComplex() {
    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(null);
    GenericProtein p1 = createProtein();
    Complex c1 = createComplex();
    c1.addSpecies(p1);
    BindingRegion r = new BindingRegion();
    p1.addBindingRegion(r);
    Set<Species> elements = new HashSet<>();
    elements.add(p1);
    elements.add(c1);

    layoutModelCommand.modifySpeciesListLocation(elements, new Point2D.Double(100, 100), new DoubleDimension(200, 200));

    assertTrue(p1.getX() >= 100);
    assertTrue(p1.getY() >= 100);
    assertTrue(p1.getBorder().getMaxX() <= 300);
    assertTrue(p1.getBorder().getMaxY() <= 300);
    assertNotNull(r.getX());
    assertNotNull(r.getY());
  }

  @Test
  public void testModifyElementLocation() {
    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(null);
    GenericProtein p1 = createProtein();
    Compartment p2 = new SquareCompartment("comp-id");
    Set<Element> elements = new HashSet<>();
    elements.add(p1);
    elements.add(p2);
    p2.addElement(p1);

    layoutModelCommand.modifyElementLocation(elements, null, new Point2D.Double(100, 100),
        new DoubleDimension(200, 200));

    assertTrue(p1.getX() >= 100);
    assertTrue(p1.getY() >= 100);
    assertTrue(p1.getBorder().getMaxX() <= 300);
    assertTrue(p1.getBorder().getMaxY() <= 300);
    assertTrue(p2.getX() >= 100);
    assertTrue(p2.getY() >= 100);
    assertTrue(p2.getBorder().getMaxX() <= 300);
    assertTrue(p2.getBorder().getMaxY() <= 300);

    assertTrue(p2.contains(p1));
  }

  @Test
  public void testModifyElementLocationWithinCompartmentWithoutLayout() {
    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(null);
    GenericProtein p1 = createProtein();
    p1.setX(100);
    p1.setY(200);
    p1.setWidth(200);
    p1.setHeight(30);
    Rectangle2D border = p1.getBorder();
    Compartment p2 = new SquareCompartment("comp-id");
    Set<Element> elements = new HashSet<>();
    elements.add(p2);
    p2.addElement(p1);

    layoutModelCommand.modifyElementLocation(elements, null, new Point2D.Double(100, 100),
        new DoubleDimension(200, 200));

    assertEquals(border, p1.getBorder());
  }

  @Test
  public void testModifyElementModificationResidueLocation() {
    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(null);
    GenericProtein p1 = createProtein();
    p1.addResidue(new Residue());
    Set<Element> elements = new HashSet<>();
    elements.add(p1);

    layoutModelCommand.modifyElementLocation(elements, null, new Point2D.Double(100, 100),
        new DoubleDimension(200, 200));

    assertNotNull(p1.getModificationResidues().get(0).getX());
    assertNotNull(p1.getModificationResidues().get(0).getY());
  }

  @Test
  public void testModifyElementModificationResidueLocationWhenElementHasLayout() {
    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(null);
    GenericProtein p1 = createProtein();
    p1.setX(10);
    p1.setY(10);
    p1.setWidth(10);
    p1.setHeight(10);
    p1.addResidue(new Residue());
    Set<Element> elements = new HashSet<>();
    elements.add(p1);

    layoutModelCommand.modifyElementLocation(elements, null, new Point2D.Double(100, 100),
        new DoubleDimension(200, 200));

    assertNotNull(p1.getModificationResidues().get(0).getX());
    assertNotNull(p1.getModificationResidues().get(0).getY());
  }

  @Test
  public void testModifyElementLocationInsideCompartmentInsideStaticCompartment() {
    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(null);
    GenericProtein protein = createProtein();
    Compartment staticCompartment = new SquareCompartment("comp-id");
    staticCompartment.setX(10);
    staticCompartment.setY(20);
    staticCompartment.setWidth(200);
    staticCompartment.setHeight(200);

    Compartment compartmentToLayout = new SquareCompartment("comp-id2");
    compartmentToLayout.addElement(protein);

    staticCompartment.addElement(compartmentToLayout);

    Set<Element> elements = new HashSet<>();
    elements.add(protein);
    elements.add(compartmentToLayout);

    layoutModelCommand.modifyElementLocation(elements, null, new Point2D.Double(1000, 1000),
        new DoubleDimension(2000, 2000));

    assertFalse(protein.getX() >= 1000);
    assertFalse(protein.getY() >= 1000);

    assertTrue(staticCompartment.contains(protein));

    assertFalse(compartmentToLayout.getX() >= 1000);
    assertFalse(compartmentToLayout.getY() >= 1000);

    assertTrue(staticCompartment.contains(compartmentToLayout));
  }

  @Test
  public void testFindFarthestEmptyPositionOnRectangleWithEmptyPoints() {
    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(null);
    Rectangle2D rect = new Rectangle2D.Double(10, 20, 100, 30);

    assertNotNull(layoutModelCommand.findFarthestEmptyPositionOnRectangle(rect, new ArrayList<>()));
  }

  @Test
  public void testFindFarthestPointNotOnBorder() {
    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(null);
    Rectangle2D rect = new Rectangle2D.Double(10, 20, 100, 30);

    assertNotNull(layoutModelCommand.findFarthestEmptyPositionOnRectangle(rect, Collections.singletonList(new Point2D.Double(0, 0))));
  }

  @Test
  public void testFindFarthestEmptyPositionOnRectangleSinglePoint() {
    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(null);
    Rectangle2D rect = new Rectangle2D.Double(10, 20, 100, 30);

    List<Point2D> points = Arrays.asList(new Point2D[]{new Point2D.Double(55, rect.getMinY())});
    Point2D resultPoint = layoutModelCommand.findFarthestEmptyPositionOnRectangle(rect, points);
    assertNotNull(resultPoint);
    assertEquals("Expected point on lower edge", rect.getMaxY(), resultPoint.getY(), Configuration.EPSILON);

    points = Arrays.asList(new Point2D[]{new Point2D.Double(55, rect.getMaxY())});
    resultPoint = layoutModelCommand.findFarthestEmptyPositionOnRectangle(rect, points);
    assertNotNull(resultPoint);
    assertEquals("Expected point on upper edge", rect.getMinY(), resultPoint.getY(), Configuration.EPSILON);

    points = Arrays.asList(new Point2D[]{new Point2D.Double(rect.getMinX(), 35)});
    resultPoint = layoutModelCommand.findFarthestEmptyPositionOnRectangle(rect, points);
    assertNotNull(resultPoint);
    assertEquals(rect.getMaxX(), resultPoint.getX(), Configuration.EPSILON);

    points = Arrays.asList(new Point2D[]{new Point2D.Double(rect.getMaxX(), 35)});
    resultPoint = layoutModelCommand.findFarthestEmptyPositionOnRectangle(rect, points);
    assertNotNull(resultPoint);
    assertEquals("Expected point on left edge", rect.getMinX(), resultPoint.getX(), Configuration.EPSILON);
  }

  @Test
  public void testFindFarthestEmptyPositionOnRectangleWithTwoPoints() {
    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(null);
    Rectangle2D rect = new Rectangle2D.Double(10, 20, 100, 30);

    List<Point2D> points = Arrays
        .asList(new Point2D[]{new Point2D.Double(55, rect.getMinY()), new Point2D.Double(55, rect.getMaxY())});
    Point2D resultPoint = layoutModelCommand.findFarthestEmptyPositionOnRectangle(rect, points);
    assertNotNull(resultPoint);
    assertFalse("Expected point shouldn't be on upper edge",
        Math.abs(rect.getMinY() - resultPoint.getY()) < Configuration.EPSILON);
    assertFalse("Expected point shouldn't be on lower edge",
        Math.abs(rect.getMaxY() - resultPoint.getY()) < Configuration.EPSILON);

    points = Arrays
        .asList(new Point2D[]{new Point2D.Double(rect.getMinX(), 35), new Point2D.Double(rect.getMaxX(), 35)});
    resultPoint = layoutModelCommand.findFarthestEmptyPositionOnRectangle(rect, points);
    assertNotNull(resultPoint);
    assertFalse("Expected point shouldn't be on left edge",
        Math.abs(rect.getMinX() - resultPoint.getX()) < Configuration.EPSILON);
    assertFalse("Expected point shouldn't be on right edge",
        Math.abs(rect.getMaxX() - resultPoint.getX()) < Configuration.EPSILON);
  }

  @Test
  public void testApplyLayoutForReaction() throws CommandExecutionException {
    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    model.addElement(protein);
    Protein protein2 = createProtein();
    model.addElement(protein2);

    Reaction reaction = createReaction(protein, protein2);
    model.addReaction(reaction);

    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(model);
    layoutModelCommand.execute();
    assertNotNull("Reactant doesn't have line", reaction.getReactants().get(0).getLine());
    assertNotNull("Product doesn't have line", reaction.getProducts().get(0).getLine());
    assertNotNull("Reaction doesn't have line", reaction.getLine());
  }

  @Test
  public void testDimensionShouldBeIndependentOfReactionNumber() throws CommandExecutionException {
    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    model.addElement(protein);
    Protein protein2 = createProtein();
    model.addElement(protein2);

    for (int i = 0; i < 30; i++) {
      model.addReaction(createReaction(protein, protein2));
    }

    new ApplySimpleLayoutModelCommand(model).execute();

    Model model2 = new ModelFullIndexed(null);
    Protein protein3 = createProtein();
    model2.addElement(protein3);
    Protein protein4 = createProtein();
    model2.addElement(protein4);

    model2.addReaction(createReaction(protein3, protein4));
    model2.addReaction(createReaction(protein3, protein4));

    new ApplySimpleLayoutModelCommand(model2).execute();

    assertEquals(model.getWidth(), model2.getWidth(), Configuration.EPSILON);
    assertEquals(model.getHeight(), model2.getHeight(), Configuration.EPSILON);
  }

  @Test
  public void testZIndex() throws CommandExecutionException {
    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    model.addElement(protein);
    Protein protein2 = createProtein();
    model.addElement(protein2);

    Reaction reaction = createReaction(protein, protein2);
    model.addReaction(reaction);

    new ApplySimpleLayoutModelCommand(model).execute();
    for (final Drawable d : model.getDrawables()) {
      assertNotNull(d.getZ());
    }
  }

  @Test
  public void testReactionLayoutShouldBeOnTheBorder() throws CommandExecutionException {
    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    model.addElement(protein);
    Protein protein2 = createProtein();
    model.addElement(protein2);

    Reaction reaction = createReaction(protein, protein2);
    model.addReaction(reaction);

    new ApplySimpleLayoutModelCommand(model).execute();

    PolylineData line = reaction.getReactants().get(0).getLine();
    assertTrue(protein.getCenter().distance(line.getStartPoint()) > Configuration.EPSILON);

    line = reaction.getProducts().get(0).getLine();
    assertTrue(protein2.getCenter().distance(line.getEndPoint()) > Configuration.EPSILON);
  }

  @Test
  public void testComplexWithTwoChildren() throws CommandExecutionException {
    Model model = new ModelFullIndexed(null);

    Complex c1 = createComplex();
    Complex c2 = createComplex();
    Complex c3 = createComplex();

    c1.addSpecies(c2);
    c1.addSpecies(c3);

    model.addElement(c1);
    model.addElement(c2);
    model.addElement(c3);

    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(model);

    layoutModelCommand.execute();

    assertTrue(c1.getBorder().contains(c2.getBorder()));
    assertTrue(c1.getBorder().contains(c3.getBorder()));
  }

  @Test
  public void testComplexWithThreeChildren() throws CommandExecutionException {
    Model model = new ModelFullIndexed(null);

    Complex c1 = createComplex();
    Complex c2 = createComplex();
    Complex c3 = createComplex();
    Complex c4 = createComplex();

    c1.addSpecies(c2);
    c1.addSpecies(c3);
    c1.addSpecies(c4);

    model.addElement(c1);
    model.addElement(c2);
    model.addElement(c3);
    model.addElement(c4);

    ApplySimpleLayoutModelCommand layoutModelCommand = new ApplySimpleLayoutModelCommand(model);

    layoutModelCommand.execute();

    assertTrue(c1.getBorder().contains(c2.getBorder()));
    assertTrue(c1.getBorder().contains(c3.getBorder()));
    assertTrue(c1.getBorder().contains(c4.getBorder()));

    assertEquals(VerticalAlign.BOTTOM, c1.getNameVerticalAlign());
    assertEquals(VerticalAlign.MIDDLE, c2.getNameVerticalAlign());
  }

}
