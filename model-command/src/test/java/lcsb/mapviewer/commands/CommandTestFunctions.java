package lcsb.mapviewer.commands;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.tests.UnitTestFailedWatcher;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class CommandTestFunctions {

  private static final Map<String, Model> models = new HashMap<>();
  protected static final double EPSILON = Configuration.EPSILON;

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();
  protected static Logger logger = LogManager.getLogger();

  private int counter;

  protected Model getModelForFile(final String fileName, final boolean fromCache) throws Exception {
    if (!fromCache) {
      logger.debug("File without cache: " + fileName);
      final Model result = new CellDesignerXmlParser().createModel(new ConverterParams().filename(fileName));
      result.setName("Unknown");
      return result;
    }
    Model result = models.get(fileName);
    if (result == null) {
      logger.debug("File to cache: " + fileName);

      final CellDesignerXmlParser parser = new CellDesignerXmlParser();
      result = parser.createModel(new ConverterParams().filename(fileName).sizeAutoAdjust(false));
      result.setName("Unknown");
      models.put(fileName, result);
    }
    return result;
  }

  protected Model createSimpleModel() {
    final Model model = new ModelFullIndexed(null);

    final GenericProtein alias = new GenericProtein("alias_id");
    assignCoordinates(alias);
    alias.setZ(2);

    alias.setNotes(null);
    final List<String> list = new ArrayList<>();
    list.add("synonym");
    alias.addSynonyms(list);
    final List<String> list2 = new ArrayList<>();
    list2.add("f_symbol");
    alias.setFormerSymbols(list2);

    final Complex complexAlias = new Complex("complex_alias_id");
    assignCoordinates(complexAlias);
    complexAlias.setZ(3);
    model.addElement(complexAlias);

    complexAlias.addSpecies(alias);

    model.addElement(alias);

    return model;
  }

  protected Model serializeViaCellDesigner(final Model original)
      throws InconsistentModelException, InvalidInputDataExecption {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final String xmlString = parser.model2String(original);
    final InputStream stream = new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8));
    final Model result = parser.createModel(new ConverterParams().inputStream(stream).sizeAutoAdjust(false));

    for (final Drawable bioEntity : original.getDrawables()) {
      bioEntity.setZ(null);
    }
    for (final Drawable bioEntity : result.getDrawables()) {
      bioEntity.setZ(null);
    }
    return result;
  }

  protected Reaction createReaction(final Protein protein, final Protein protein2) {
    final Reaction reaction = new TransportReaction("" + counter++);
    reaction.addReactant(new Reactant(protein));
    reaction.addProduct(new Product(protein2));
    return reaction;
  }

  protected GenericProtein createProtein() {
    final GenericProtein result = new GenericProtein("" + counter++);
    result.setWidth((Double) null);
    result.setHeight((Double) null);
    result.setX((Double) null);
    result.setY((Double) null);
    return result;
  }

  protected Gene createGene() {
    final Gene result = new Gene("" + counter++);
    return result;
  }

  protected Complex createComplex() {
    final Complex result = new Complex("" + counter++);
    result.setWidth((Double) null);
    result.setHeight((Double) null);
    result.setX((Double) null);
    result.setY((Double) null);
    return result;
  }

  private int z = 1;

  protected void assignCoordinates(final Element element) {
    assignCoordinates(1, 1, 10, 20, element);
  }

  protected void assignCoordinates(final double x, final double y, final double width, final double height, final Element element) {
    element.setX(x);
    element.setY(y);
    element.setZ(z++);
    element.setWidth(width);
    element.setHeight(height);
    element.setNameX(x);
    element.setNameY(y);
    element.setNameWidth(width);
    element.setNameHeight(height);
    element.setNameVerticalAlign(VerticalAlign.MIDDLE);
    element.setNameHorizontalAlign(HorizontalAlign.CENTER);
  }

}
