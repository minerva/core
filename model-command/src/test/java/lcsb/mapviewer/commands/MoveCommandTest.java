package lcsb.mapviewer.commands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.StructuralState;

public class MoveCommandTest extends CommandTestFunctions {

  @Test
  public void testMoveReaction() throws CommandExecutionException {
    Model model = super.createSimpleModel();

    Reaction r = new TransportReaction("re");
    Reactant reactant = new Reactant(model.getSpeciesList().get(0));
    reactant.setLine(new PolylineData(new Point2D.Double(0, 10), new Point2D.Double(0, 20)));
    r.addReactant(reactant);
    Product product = new Product(model.getSpeciesList().get(1));
    product.setLine(new PolylineData(new Point2D.Double(0, 30), new Point2D.Double(0, 40)));
    r.addProduct(product);
    r.setLine(new PolylineData(new Point2D.Double(0, 20), new Point2D.Double(0, 30)));

    model.addReaction(r);

    new MoveCommand(model, 10, 20).execute();
    for (final Line2D line : r.getLines()) {
      assertTrue(line.getX1() > 0);
      assertTrue(line.getX2() > 0);
      assertTrue(line.getY1() > 10);
      assertTrue(line.getY2() > 10);
    }

  }

  @Test
  public void testMoveProtein() throws CommandExecutionException {
    Model model = super.createSimpleModel();

    Protein protein = createProtein();

    super.assignCoordinates(10, 20, 100, 100, protein);

    BindingRegion bindingRegion = new BindingRegion();
    bindingRegion.setPosition(new Point2D.Double(30, 20));
    protein.addBindingRegion(bindingRegion);

    StructuralState state = new StructuralState();
    state.setPosition(new Point2D.Double(40, 20));
    protein.addStructuralState(state);

    model.addElement(protein);

    new MoveCommand(model, 10, 20).execute();

    assertEquals(20, protein.getX(), Configuration.EPSILON);
    assertEquals(40, protein.getY(), Configuration.EPSILON);
    assertEquals(40, bindingRegion.getX(), Configuration.EPSILON);
    assertEquals(40, bindingRegion.getY(), Configuration.EPSILON);
    assertEquals(50, state.getX(), Configuration.EPSILON);
    assertEquals(40, state.getY(), Configuration.EPSILON);
  }

  @Test
  public void testMoveLayerText() throws CommandExecutionException {
    Model model = super.createSimpleModel();

    Layer layer = new Layer();
    LayerText text = new LayerText(new Rectangle2D.Double(10, 20, 30, 40), "xyz");
    layer.addLayerText(text);
    model.addLayer(layer);

    new MoveCommand(model, 10, 20).execute();

    assertEquals(20, text.getX(), Configuration.EPSILON);
    assertEquals(40, text.getY(), Configuration.EPSILON);
  }

  @Test
  public void testMoveLayerRect() throws CommandExecutionException {
    Model model = super.createSimpleModel();

    Layer layer = new Layer();
    LayerRect rect = new LayerRect(new Rectangle2D.Double(10, 20, 30, 40));
    layer.addLayerRect(rect);
    model.addLayer(layer);

    new MoveCommand(model, 10, 20).execute();

    assertEquals(20, rect.getX(), Configuration.EPSILON);
    assertEquals(40, rect.getY(), Configuration.EPSILON);
  }

  @Test
  public void testMoveLayerOval() throws CommandExecutionException {
    Model model = super.createSimpleModel();

    Layer layer = new Layer();
    LayerOval oval = new LayerOval();
    oval.setX(10.0);
    oval.setY(20.0);
    layer.addLayerOval(oval);
    model.addLayer(layer);

    new MoveCommand(model, 10, 20).execute();

    assertEquals(20, oval.getX(), Configuration.EPSILON);
    assertEquals(40, oval.getY(), Configuration.EPSILON);
  }

  @Test
  public void testMoveLayerLine() throws CommandExecutionException {
    Model model = super.createSimpleModel();

    Layer layer = new Layer();
    PolylineData line = new PolylineData(new Point2D.Double(10, 20), new Point2D.Double(30, 40));
    layer.addLayerLine(line);
    model.addLayer(layer);

    new MoveCommand(model, 10, 20).execute();

    assertEquals(0, new Point2D.Double(20, 40).distance(line.getStartPoint()), Configuration.EPSILON);
    assertEquals(0, new Point2D.Double(40, 60).distance(line.getEndPoint()), Configuration.EPSILON);
  }

}
