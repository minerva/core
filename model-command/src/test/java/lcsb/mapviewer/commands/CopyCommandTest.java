package lcsb.mapviewer.commands;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.model.map.model.ElementSubmodelConnection;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class CopyCommandTest extends CommandTestFunctions {

  private final ModelComparator comparator = new ModelComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  private int counter = 0;

  @Test
  public void testCopyModel() throws Exception {
    Model model = getModelForFile("testFiles/sample.xml", false);
    Model copy = new CopyCommand(model).execute();

    assertEquals(0, comparator.compare(model, copy));
  }

  @Test
  public void testCopyModelWithKinetics() throws Exception {
    Model model = getModelForFile("testFiles/kinetics_with_compartment.xml", false);
    Model copy = new CopyCommand(model).execute();

    assertEquals(0, comparator.compare(model, copy));
    for (final Reaction reaction : copy.getReactions()) {
      if (reaction.getKinetics() != null) {
        for (final Element element : reaction.getKinetics().getElements()) {
          assertTrue("Element in the copy doesn't belong to copy", copy.getElements().contains(element));
        }
        for (final SbmlFunction function : reaction.getKinetics().getFunctions()) {
          assertTrue("Function in the copy doesn't belong to copy", copy.getFunctions().contains(function));
        }
        for (final SbmlParameter parameter : reaction.getKinetics().getParameters()) {
          if (parameter.getParameterId().equals("k2")) {
            assertTrue("Global parameter in the function copy doesn't belong to copy",
                copy.getParameters().contains(parameter));
          }
        }
      }
    }
  }

  @Test
  public void testCopyCustomModel() {
    Model model = new ModelFullIndexed(null);

    GenericProtein protein = new GenericProtein("A");
    protein.setNotes(null);

    model.addElement(protein);

    Model copy = new CopyCommand(model).execute();

    assertEquals(0, comparator.compare(model, copy));
  }

  @Test
  public void testCopyCustomModel2() {
    Model model = new ModelFullIndexed(null);

    Complex complexAlias = new Complex("id2");
    GenericProtein protein = new GenericProtein("A");
    protein.setNotes(null);
    complexAlias.addSpecies(protein);
    model.addElement(protein);
    model.addElement(complexAlias);

    GenericProtein alias = new GenericProtein("B");
    alias.setNotes(null);

    complexAlias.addSpecies(alias);
    model.addElement(alias);

    Model copy = new CopyCommand(model).execute();

    assertEquals(0, comparator.compare(model, copy));
  }

  @Test
  public void testCopyModel3() throws Exception {
    Model model = getModelForFile("testFiles/complex_with_state.xml", true);

    Model copy = new CopyCommand(model).execute();

    Model copy2 = serializeViaCellDesigner(copy);

    // check if after conversion to xml everything works
    assertEquals(0, comparator.compare(copy, copy2));
  }

  @Test
  public void testCopyModel4() throws Exception {
    Model model = getModelForFile("testFiles/problematic_description.xml", true);

    Model copy = new CopyCommand(model).execute();

    Model copy2 = serializeViaCellDesigner(copy);

    // check if after conversion to xml everything works
    assertEquals(0, comparator.compare(copy, copy2));
  }

  @Test
  public void testCopyModelWithArtificialAliases() throws Exception {
    Model model = getModelForFile("testFiles/artifitial_compartments.xml", false);
    new CreateHierarchyCommand(model, 2, 2).execute();

    Model copy = new CopyCommand(model).execute();

    Model copy2 = serializeViaCellDesigner(copy);

    new CreateHierarchyCommand(copy2, 2, 2).execute();

    // check if after conversion to xml everything works
    assertEquals(0, comparator.compare(copy, copy2));
  }

  @Test
  public void testCopyModelWithSubmodels() throws Exception {
    Model model = getModel();
    Model model2 = getModel();
    model2.setNotes("ASDSA");
    model.addSubmodelConnection(new ModelSubmodelConnection(model2, SubmodelType.DOWNSTREAM_TARGETS));
    Model copy = new CopyCommand(model).execute();

    assertEquals(0, comparator.compare(model, copy));
  }

  @Test
  public void testCopySubmodel() throws Exception {
    Project project = new Project();
    Model model = getModel();
    Model model2 = getModel();
    model2.setNotes("ASDSA");
    model.addSubmodelConnection(new ModelSubmodelConnection(model2, SubmodelType.DOWNSTREAM_TARGETS, "name a"));

    Model model3 = getModel();
    model3.setNotes("ASDSA");
    model.addSubmodelConnection(new ModelSubmodelConnection(model3, SubmodelType.DOWNSTREAM_TARGETS, "name b"));
    Element alias = model2.getElementByElementId("a_id");
    alias.setSubmodel(new ElementSubmodelConnection(model3, SubmodelType.DOWNSTREAM_TARGETS, "name c"));

    project.addModel(model);
    project.addModel(model2);
    project.addModel(model3);

    Model copy = new CopyCommand(model2).execute();

    assertEquals(0, comparator.compare(model2, copy));
  }

  @Test
  public void testCopySubmodelInElement() throws Exception {
    Project project = new Project();
    Model model = getModel();
    Model model2 = getModel();
    model2.setNotes("ASDSA");
    Element alias = model.getElementByElementId("a_id");
    alias.setSubmodel(new ElementSubmodelConnection(model2, SubmodelType.DOWNSTREAM_TARGETS, "name c"));

    project.addModel(model);
    project.addModel(model2);

    Model copy = new CopyCommand(model).execute();

    assertEquals(0, comparator.compare(model, copy));
  }

  @Test
  public void testCopyModelWithSubmodels2() throws Exception {
    Model model = getModel();
    Model model2 = getModel();
    model2.setNotes("ASDSA2");

    model.addSubmodelConnection(new ModelSubmodelConnection(model2, SubmodelType.DOWNSTREAM_TARGETS));
    model.getElementByElementId("a_id")
        .setSubmodel(new ElementSubmodelConnection(model2, SubmodelType.DOWNSTREAM_TARGETS));
    Model copy = new CopyCommand(model).execute();

    assertEquals(0, comparator.compare(model, copy));
  }

  @Test
  public void testCopyModelWithName() throws Exception {
    Model model = getModel();
    model.setName("ASDSA2");

    Model copy = new CopyCommand(model).execute();

    assertEquals(0, comparator.compare(model, copy));
  }

  private Model getModel() {
    Model model = new ModelFullIndexed(null);
    model.setName("nam" + counter++);
    model.setNotes("Some description");

    GenericProtein protein = new GenericProtein("a_id");
    protein.setName("ad");
    model.addElement(protein);

    Layer layer = new Layer();
    layer.setName("layer name");
    model.addLayer(layer);

    Reaction reaction = new Reaction("re");
    Product product = new Product(protein);
    Reactant reactant = new Reactant(protein);
    product.setLine(new PolylineData());
    reactant.setLine(new PolylineData());
    reaction.addProduct(product);
    reaction.addReactant(reactant);
    model.addReaction(reaction);
    return model;
  }

  @Test
  public void testCopyModelReaction() throws Exception {
    Model model = new ModelFullIndexed(null);

    Compartment c1 = new SquareCompartment("c1");
    Compartment c2 = new SquareCompartment("c2");
    c1.setVisibilityLevel("2");
    c2.setVisibilityLevel("3");

    model.addElement(c1);
    model.addElement(c2);

    GenericProtein s1 = new GenericProtein("s1");
    s1.setCompartment(c1);
    model.addElement(s1);

    GenericProtein s2 = new GenericProtein("s2");
    s2.setCompartment(c2);
    model.addElement(s2);

    StateTransitionReaction reaction = new StateTransitionReaction("re1");
    Reactant reactant = new Reactant(s1);
    reactant.setLine(new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10)));
    reaction.addReactant(reactant);
    Product product = new Product(s2);
    product.setLine(new PolylineData(new Point2D.Double(10, 0), new Point2D.Double(120, 10)));
    reaction.addProduct(product);
    reaction.setVisibilityLevel("4");

    model.addReaction(reaction);

    assertEquals(s1, reaction.getReactants().get(0).getElement());
    assertEquals(s2, reaction.getProducts().get(0).getElement());

    Model model2 = new CopyCommand(model).execute();
    Reaction reaction2 = model2.getReactions().iterator().next();

    assertEquals(s1, reaction.getReactants().get(0).getElement());
    assertEquals(s2, reaction.getProducts().get(0).getElement());

    assertNotEquals(s1, reaction2.getReactants().get(0).getElement());
    assertNotEquals(s2, reaction2.getProducts().get(0).getElement());

    assertNotNull(reaction2.getReactants().get(0).getElement().getCompartment());
    assertNotNull(reaction2.getProducts().get(0).getElement().getCompartment());
  }

  @Test
  public void testCopyModelWithAuthor() throws Exception {
    Model model = getModel();
    model.addAuthor(new Author("Piotr", "G"));

    Model copy = new CopyCommand(model).execute();

    assertEquals(0, comparator.compare(copy, model));
  }

  @Test
  public void testCopyModelWithAnnotations() throws Exception {
    Model model = getModel();
    model.addMiriamData(new MiriamData());

    Model copy = new CopyCommand(model).execute();

    assertEquals(0, comparator.compare(copy, model));
  }

  @Test
  public void testCopyModelWithCreationDate() throws Exception {
    Model model = getModel();
    model.setCreationDate(Calendar.getInstance());

    Model copy = new CopyCommand(model).execute();

    assertEquals(0, comparator.compare(copy, model));
  }

  @Test
  public void testCopyModelWithModificationDate() throws Exception {
    Model model = getModel();
    model.addModificationDate(Calendar.getInstance());

    Model copy = new CopyCommand(model).execute();

    assertEquals(0, comparator.compare(copy, model));
  }

  @Test
  public void testMultithreadedCopyReaction() throws Exception {
    MutableBoolean exceptionHappened = new MutableBoolean(false);
    Reaction r = new TransportReaction("x");
    r.addReactant(new Reactant(createProtein()));
    r.addReactant(new Reactant(createProtein()));
    r.addReactant(new Reactant(createProtein()));
    r.addReactant(new Reactant(createProtein()));
    r.addProduct(new Product(createProtein()));
    for (final AbstractNode node : r.getNodes()) {
      node.setLine(new PolylineData(new Point2D.Double(0.0, 0.0), new Point2D.Double(110.0, 10.0)));
    }

    List<Thread> threads = new ArrayList<>();
    for (int i = 0; i < 100; i++) {
      threads.add(new Thread(new Runnable() {

        @Override
        public void run() {
          try {
            new CopyCommand(null).createCopy(r);
          } catch (final Exception e) {
            e.printStackTrace();
            exceptionHappened.setTrue();
          }
        }
      }));
    }
    for (final Thread thread : threads) {
      thread.start();
    }
    for (final Thread thread : threads) {
      thread.join();
    }

    assertFalse(exceptionHappened.booleanValue());

    for (final AbstractNode node : r.getNodes()) {
      assertEquals(r, node.getReaction());
    }

  }

  @Test
  public void testCopyModelWithProject() throws Exception {
    Model originalModel = getModelForFile("testFiles/sample.xml", false);
    Project project = new Project();
    project.addModel(originalModel);

    Model colorModel = new CopyCommand(originalModel).execute();

    assertNotEquals(originalModel.getProject(), colorModel.getProject());
  }

}
