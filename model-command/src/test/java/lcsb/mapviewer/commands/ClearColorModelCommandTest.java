package lcsb.mapviewer.commands;

import static org.junit.Assert.assertEquals;

import java.awt.Color;

import org.junit.Test;

import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.modelutils.map.ElementUtils;

public class ClearColorModelCommandTest extends CommandTestFunctions {

  private ElementUtils eu = new ElementUtils();

  @Test
  public void test() throws CommandExecutionException {
    Model model = super.createSimpleModel();
    PathwayCompartment compartment = new PathwayCompartment("id");
    compartment.setFillColor(Color.BLUE);
    compartment.setFontColor(Color.YELLOW);
    model.addElement(compartment);

    new ClearColorModelCommand(model).execute();

    for (final Element e : model.getElements()) {
      assertEquals(Color.WHITE, e.getFillColor());
      assertEquals(eu.getElementTag(e) + "Invalid font color ", Color.BLACK, e.getFontColor());
    }
  }

}
