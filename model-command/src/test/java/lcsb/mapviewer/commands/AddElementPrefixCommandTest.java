package lcsb.mapviewer.commands;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.Model;

public class AddElementPrefixCommandTest extends CommandTestFunctions {

  @Test
  public void testPrefixChange() throws CommandExecutionException {
    Model model = super.createSimpleModel();
    String id = model.getElements().iterator().next().getElementId();
    new AddElementPrefixCommand(model, "pre").execute();
    assertNotNull(model.getElementByElementId("pre" + id));
  }

  @Test
  public void testPrefixLayer() throws CommandExecutionException {
    Model model = super.createSimpleModel();
    Layer layer = new Layer();
    layer.setLayerId("x");
    model.addLayer(layer);
    new AddElementPrefixCommand(model, "pre").execute();
    assertNotEquals("x", layer.getLayerId());
  }

}
