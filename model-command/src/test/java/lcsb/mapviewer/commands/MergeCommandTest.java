package lcsb.mapviewer.commands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import lcsb.mapviewer.model.map.model.Model;

public class MergeCommandTest extends CommandTestFunctions {

  @Test
  public void testMergeTwoModels() throws CommandExecutionException {
    Model m1 = createSimpleModel();
    Model m2 = createSimpleModel();
    List<Model> models = Arrays.asList(m1, m2);

    Model result = new MergeCommand(models).execute();
    assertEquals(m1.getElements().size() + m2.getElements().size(), result.getElements().size());
    assertEquals(m1.getBioEntities().size() + m2.getBioEntities().size(), result.getBioEntities().size());
  }

  @Test
  public void testMergeSingleModels() throws CommandExecutionException {
    Model m1 = createSimpleModel();
    List<Model> models = Arrays.asList(m1);

    Model result = new MergeCommand(models).execute();
    assertEquals(m1.getElements().size(), result.getElements().size());
    assertEquals(m1.getBioEntities().size(), result.getBioEntities().size());
  }

  @Test
  public void testZeroModels() throws CommandExecutionException {
    List<Model> models = new ArrayList<>();

    Model result = new MergeCommand(models).execute();
    assertTrue(result.getWidth() > 0);
    assertTrue(result.getHeight() > 0);
  }

}
