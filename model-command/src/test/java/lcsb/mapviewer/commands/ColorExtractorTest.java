package lcsb.mapviewer.commands;

import static org.junit.Assert.assertEquals;

import java.awt.Color;

import org.junit.Test;

public class ColorExtractorTest {
  private ColorExtractor extractor = new ColorExtractor(Color.GREEN, Color.BLUE, Color.RED, Color.YELLOW);

  @Test
  public void testZeroColor() {
    assertEquals(Color.YELLOW, extractor.getColorForValue(0.0001));
    assertEquals(Color.YELLOW, extractor.getColorForValue(0.0));
  }

  @Test
  public void testMaxColor() {
    assertEquals(Color.BLUE, extractor.getColorForValue(1.0));
    assertEquals(Color.BLUE, extractor.getColorForValue(0.99999));
  }

  @Test
  public void testMinColor() {
    assertEquals(Color.GREEN, extractor.getColorForValue(-1.0));
    assertEquals(Color.GREEN, extractor.getColorForValue(-0.99999));
  }

  @Test
  public void testColorForNotNormalizedValue() {
    assertEquals(Color.GREEN, extractor.getColorForValue(-10.0));
    assertEquals(Color.BLUE, extractor.getColorForValue(10.0));
  }

}
