#!/bin/sh

file="/etc/minerva/minerva.properties"


JAVA_ARGS=''

if [ -f $file ];
then
  while IFS='=' read -r key value
  do
    key=$(echo $key | tr '.' '_')
    value=$(echo $value | tr '"' ' ')
    eval ${key}=\${value}
  done < "$file"
fi

java $JAVA_ARGS -jar /usr/share/minerva/minerva.jar

