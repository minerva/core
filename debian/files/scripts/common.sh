#file with common script functions for postrm/prerm/postinst/preinst
log(){
	echo "[" $CURRENT_VERSION "]" "$@" >>__LOG_FILE__
}
LOG_FILE="__LOG_FILE__"

#new (current) version of the package
CURRENT_VERSION="__CURRENT_VERSION__"
#if we update package then this will be the old version of the package
OLD_VERSION=$2

if [ "$OLD_VERSION" = "$CURRENT_VERSION" ]
then
	OLD_VERSION="";
fi

POSTGRES_OK=$(dpkg-query -W --showformat='${Status}\n' postgresql|grep "install ok installed")

