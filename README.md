[![build status](https://git-r3lab.uni.lu/minerva/core/badges/master/pipeline.svg)](https://git-r3lab.uni.lu/minerva/core/badges/master/pipeline.svg)
[![coverage report](https://git-r3lab.uni.lu/minerva/core/badges/master/coverage.svg)](https://git-r3lab.uni.lu/minerva/core/commits/master)

# For users
Documentation for minerva can be found here: https://minerva.pages.uni.lu/doc/

# For developers

## Installation

After cloning repo you need to install following tools:
* postgresql (>=9.1)
* tomcat (tested on tomcat8, tomcat9)
* Java8 - works on OpenJDK 8, Oracle Java 8, OpenJDK 11
* maven
* ant
* node.js + npm

You need to create user and database:
```
username: map_viewer
password: 123qweasdzxc
dbname: map_viewer
```

If you want to use different credentials or your database in not hosted locally you can configure it by creating file `/etc/minerva/db.properties`. Template of the file can be found here: https://git-r3lab.uni.lu/minerva/core/blob/master/persist/src/main/resources/db.properties

Database schema is managed automatically by [flyway](https://flywaydb.org/), so there is no need to populate the database with anything.

Now your dev environment is configured and ready to use. To test it you can run unit tests:
* back-end - go to one of the Java modules (ie 'persist') and call mvn test:
```
cd ~/workspace/minerva/commons/
mvn test
```
* front-end - go to module with front-end, install all npm dependencies and run tests:
```
cd ~/workspace/minerva/frontend-js/
npm install
npm test
```

## Build

To build `*.war` file that can be deployed on tomcat you need to call ant maven-build task:
```
cd ~/workspace/minerva/
ant maven-build
```
This will create `~/workspace/minerva/web/target/web-1.0.war` file which can be deployed on tomcat:
```
cp ~/workspace/minerva/web/target/web-1.0.war $CATALINA_HOME/webapps/minerva.war
```

After starting tomcat you can access minerva using http://localhost:8080/minerva/.
Default credentials are:
```
login: admin
password: admin
```

## Deployment and release
Before making new release update [changelog file](https://git-r3lab.uni.lu/minerva/core/blob/master/CHANGELOG).

### Debian packages
When all the changes are ready tag commit and push it to the repository. Gitlab ci will execute unit tests and prepare distribution files that can be released. For now we release minerva using debian repository. Therefore gitlab ci creates a deb package (for instance: https://git-r3lab.uni.lu/minerva/core/-/jobs/29918). This package should be uploaded to our repository:
```
echo 'PUT minerva_12.0.3_amd64.deb'| sftp -P 8022 repo-r3lab.lcsb.uni.lu
ssh repo 'sudo reprepro -b /var/www/html/debian includedeb test minerva_12.0.3_amd64.deb'
```
The password required for signing the package is available in LCSB sysdamins team.

Remember to use proper repository:
* stable - for production ready debian packages
* test - for packages that are ready to test and probably will be moved to stable soon
* experimental - for packages that might crash, can be unstable are created during development process (anything that has `alpha`, `beta` in the version number should be put here)

Moreover we store all debian packages on webdav:
```
echo 'put minerva_12.0.3_amd64.deb' | cadaver https://webdav-r3lab.uni.lu/public/minerva/
```

### Docker image
Update minerva [docker repository](https://git-r3lab.uni.lu/minerva/docker).

### VirtualBox images
We provide also VirtualBox images with minerva installed  on it. Just create a new debian virtual box image and upload it to webdav:
```
echo 'put minerva-12.0.1_release.ova' | cadaver https://webdav-r3lab.uni.lu/public/minerva/
```

Image should have root account with `123qweasdzxc` password. It should also expose port 8080 on the host.
