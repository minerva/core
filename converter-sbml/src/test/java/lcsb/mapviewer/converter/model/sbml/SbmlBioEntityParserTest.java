package lcsb.mapviewer.converter.model.sbml;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;
import org.sbml.jsbml.ext.layout.AbstractReferenceGlyph;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.SpeciesGlyph;
import org.sbml.jsbml.ext.layout.TextGlyph;

public class SbmlBioEntityParserTest extends SbmlTestFunctions {

  @Test
  public void testGetTextGlyphsByLayoutElementIdForReference() {
    AbstractReferenceGlyph glyph = new SpeciesGlyph("id");

    SbmlBioEntityParser parser = Mockito.mock(SbmlBioEntityParser.class, Mockito.CALLS_REAL_METHODS);
    Layout layout = new Layout();
    TextGlyph textGlyph = new TextGlyph();
    textGlyph.setReference(glyph.getId());
    layout.addTextGlyph(textGlyph);
    Mockito.when(parser.getLayout()).thenReturn(layout);
    
    List<TextGlyph> texts = parser.getTextGlyphsByLayoutElementId(glyph);
    assertEquals(1, texts.size());
  }

  @Test
  public void testGetTextGlyphsByLayoutElementIdForGraphicalObject() {
    AbstractReferenceGlyph glyph = new SpeciesGlyph("id");

    SbmlBioEntityParser parser = Mockito.mock(SbmlBioEntityParser.class, Mockito.CALLS_REAL_METHODS);
    Layout layout = new Layout();
    TextGlyph textGlyph = new TextGlyph();
    textGlyph.setGraphicalObject(glyph.getId());
    layout.addTextGlyph(textGlyph);
    Mockito.when(parser.getLayout()).thenReturn(layout);
    
    List<TextGlyph> texts = parser.getTextGlyphsByLayoutElementId(glyph);
    assertEquals(1, texts.size());
  }

}
