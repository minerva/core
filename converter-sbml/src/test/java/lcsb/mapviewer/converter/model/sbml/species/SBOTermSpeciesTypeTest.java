package lcsb.mapviewer.converter.model.sbml.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.mockito.Mockito;
import org.sbml.jsbml.Species;

import lcsb.mapviewer.converter.model.sbml.SbmlExporter;
import lcsb.mapviewer.converter.model.sbml.SbmlTestFunctions;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.map.sbo.SBOTermSpeciesType;
import lcsb.mapviewer.model.map.species.Element;

public class SBOTermSpeciesTypeTest extends SbmlTestFunctions {

  @Test
  public void testCreateElementForSBOTerm() {
    Species species = new Species(SbmlExporter.SUPPORTED_VERSION, SbmlExporter.SUPPORTED_LEVEL);
    species.setSBOTerm("SBO:0000000");
    Element element = SBOTermSpeciesType.createElementForSBOTerm("SBO:0000000", "1", Mockito.mock(LogMarker.class));
    assertNotNull(element);
    assertEquals(1, super.getWarnings().size());
    assertNotNull(super.getWarnings().get(0).getMarker());
  }

}
