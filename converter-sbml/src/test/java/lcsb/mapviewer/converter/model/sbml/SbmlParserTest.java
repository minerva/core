package lcsb.mapviewer.converter.model.sbml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import lcsb.mapviewer.commands.CreateHierarchyCommand;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class SbmlParserTest extends SbmlTestFunctions {
  private SbmlParser parser = new SbmlParser();

  @Test
  public void testParseCompartment() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(
        new ConverterParams().filename("testFiles/layoutExample/CompartmentGlyph_Example_level2_level3.xml"));
    assertNotNull(model);
    assertEquals(1, model.getCompartments().size());
    Compartment compartment = model.getElementByElementId("CompartmentGlyph_1");
    assertNotNull(compartment.getX());
    assertNotNull(compartment.getY());
    assertTrue(compartment.getWidth() > 0);
    assertTrue(compartment.getHeight() > 0);
    assertNotNull(model.getHeight());
    assertNotNull(model.getWidth());
    assertTrue(model.getWidth() >= compartment.getX() + compartment.getWidth());
    assertTrue(model.getHeight() >= compartment.getY() + compartment.getHeight());
    assertFalse(compartment.getClass().equals(Compartment.class));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testInvalidSpeciesId() throws FileNotFoundException, InvalidInputDataExecption {
    parser.createModel(new ConverterParams().filename("testFiles/invalid_layout_alias_without_id.xml"));
  }

  @Test
  public void testParseUnits() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(
        new ConverterParams().filename("testFiles/layoutExample/CompartmentGlyph_Example_level2_level3.xml"));
    assertNotNull(model);
    assertTrue("Units weren't parsed", model.getUnits().size() > 0);
  }

  @Test
  public void testDimensionWithoutAutoresize() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(
        new ConverterParams()
            .filename("testFiles/small/model_with_annotations.xml")
            .sizeAutoAdjust(false));
    assertTrue(model.getWidth() > 0);
    assertTrue(model.getHeight() > 0);
  }

  @Test
  public void testParseModelAnnotations() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(
        new ConverterParams().filename("testFiles/small/model_with_annotations.xml"));
    assertNotNull(model);
    assertEquals(2, model.getMiriamData().size());
  }

  @Test
  public void testParseProblematicMultiLayout() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(
        new ConverterParams().filename("testFiles/small/problematic_inflamation_sbml_layout.xml"));
    assertNotNull(model);
  }

  @Test
  public void testParseKinetics() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/layoutExample/SBML.xml"));
    for (final Reaction reaction : model.getReactions()) {
      assertNotNull("Kinetics is not parsed", reaction.getKinetics());
      assertNotNull("No math definition for kinetic law", reaction.getKinetics().getDefinition());
      assertTrue("There should be a kinetic parameter defined", reaction.getKinetics().getParameters().size() > 0);
      assertTrue("Elements used by kinetics are not available", reaction.getKinetics().getElements().size() > 0);
    }
  }

  @Test
  public void testParseGlobalParameters() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/layoutExample/kinetic_global_paramter.xml"));
    assertTrue("There should be a kinetic parameter defined", model.getParameters().size() > 0);
  }

  @Test
  public void testParseFunctions() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/layoutExample/kinetic_function.xml"));
    assertTrue("There should be a kinetic function defined", model.getFunctions().size() > 0);
  }

  @Test
  public void testParseSpecies() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/layoutExample/SpeciesGlyph_Example_level2_level3.xml"));
    assertNotNull(model);
    assertEquals(1, model.getElements().size());
    Species glucoseSpecies = model.getElementByElementId("SpeciesGlyph_Glucose");
    assertNotNull(glucoseSpecies.getX());
    assertNotNull(glucoseSpecies.getY());
    assertTrue(glucoseSpecies.getWidth() > 0);
    assertTrue(glucoseSpecies.getHeight() > 0);
    assertNotNull(model.getHeight());
    assertNotNull(model.getWidth());
    assertTrue(model.getWidth() >= glucoseSpecies.getX() + glucoseSpecies.getWidth());
    assertTrue(model.getHeight() >= glucoseSpecies.getY() + glucoseSpecies.getHeight());
    assertFalse(glucoseSpecies.getClass().equals(Species.class));
  }

  @Test
  public void testParseSpeciesInCompartments() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/layoutExample/SpeciesGlyph_Example.xml"));
    assertNotNull(model);
    assertEquals(2, model.getElements().size());
    Compartment compartment = model.getElementByElementId("Yeast");
    assertNotNull(compartment.getX());
    assertNotNull(compartment.getY());
    assertTrue(compartment.getWidth() > 0);
    assertTrue(compartment.getHeight() > 0);
    assertNotNull(model.getHeight());
    assertNotNull(model.getWidth());
    assertTrue(model.getWidth() >= compartment.getX() + compartment.getWidth());
    assertTrue(model.getHeight() >= compartment.getY() + compartment.getHeight());
    assertFalse(compartment.getClass().equals(Compartment.class));
    assertTrue(compartment.getElements().size() > 0);
  }

  @Test
  public void testParseReaction() throws Exception {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/layoutExample/Complete_Example.xml"));
    assertNotNull(model);
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    for (final ReactionNode node : reaction.getReactionNodes()) {
      assertNotNull(node.getLine());
      assertTrue(node.getLine().length() > 0);
    }
    assertEquals(2, reaction.getOperators().size());

  }

  @Test
  public void testReactionWithoutLayout() throws Exception {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/layoutExample/Complete_Example_level2.xml"));
    assertNotNull(model);
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    for (final ReactionNode node : reaction.getReactionNodes()) {
      assertNotNull(node.getLine());
      assertTrue(node.getLine().length() > 0);
    }
    assertEquals(2, reaction.getOperators().size());

  }

  @Test
  public void testAnnotationsInComp() throws Exception {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/compartment_with_annotation.xml"));
    assertNotNull(model);
    assertEquals(1, model.getCompartments().size());
    assertEquals(1, model.getCompartments().iterator().next().getMiriamData().size());
  }

  @Test
  public void testEmpyModel() throws Exception {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/small/empty.xml"));
    assertTrue(model.getWidth() > 0);
    assertTrue(model.getHeight() > 0);
  }

  @Test
  public void testParseTypeFromMulti() throws Exception {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/multi_features/species_type.xml"));
    assertTrue(model.getElementByElementId("species_0") instanceof Gene);
  }

  @Test
  public void testParseConflictingTypesFromMulti() throws Exception {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/multi_features/conflicting_species_type.xml"));
    assertTrue(model.getElementByElementId("species_0") instanceof GenericProtein);
    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testCyclicComplexes() throws Exception {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/cyclic_complex.xml"));

    new CreateHierarchyCommand(model, 8, 80).execute();

    for (Species species : model.getSpeciesList()) {
      Set<Element> parents = new HashSet<>();
      while (species.getComplex() != null) {
        assertFalse("Cyclic nesting", parents.contains(species.getComplex()));
        species = species.getComplex();
        parents.add(species);
      }

    }
  }

  @Test
  public void testCyclicComplsexes() throws Exception {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/reaction_with_species_used_twice.xml"));
    assertEquals(1, model.getReactions().size());
  }

  @Test
  public void testParseCreator() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser.createModel(
        new ConverterParams().filename("testFiles/small/model_with_creator.xml"));
    assertNotNull(model);
    assertEquals("Authors weren't parsed properly", 2, model.getAuthors().size());
    assertEquals("Modification date is not defined", 1, model.getModificationDates().size());
    assertNotNull("Creation date is not defined", model.getCreationDate());
  }

  @Test
  public void testParseProblematicLayout() throws Exception {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/small/problematic_layout.xml"));
    Reaction reaction = model.getReactions().iterator().next();
    Reactant reactant = reaction.getReactants().get(0);
    assertEquals("Reactant line should start at position 120, 40", 0,
        reactant.getLine().getStartPoint().distance(120, 40), Configuration.EPSILON);
  }

  @Test
  public void testImportExportOfProblematicLayout()
      throws FileNotFoundException, InvalidInputDataExecption, ConverterException {
    Model m = parser.createModel(new ConverterParams().filename("testFiles/problematic_layout.xml"));
    String xml = parser.model2String(m);
    assertNotNull(xml);
  }

  @Test
  public void testProblemWithCellDesignerExport() throws Exception {
    Model m = parser.createModel(new ConverterParams().filename("testFiles/problem_with_export_to_cd.xml"));
    String xml = new CellDesignerXmlParser().model2String(m);
    assertNotNull(xml);
  }

  @Test
  public void testHtmlDescription() throws Exception {
    Model m = parser.createModel(new ConverterParams().filename("testFiles/small/description_with_html.xml"));
    assertTrue(m.getNotes().contains("This model is from the article"));
    assertTrue(m.getNotes().contains("http://www.ncbi.nlm.nih.gov/pubmed/15955817"));
  }

  @Test
  public void testKineticsWithIdsFromReference() throws Exception {
    Model m = parser.createModel(new ConverterParams().filename("testFiles/small/kinetics_with_reference.xml"));
    assertNotNull(m.getReactions().iterator().next().getKinetics());
  }

  @Test
  public void testLayoutContainDuplicatesOfReactionNode() throws Exception {
    Model m = parser
        .createModel(new ConverterParams().filename("testFiles/layoutExample/reaction_with_duplicated_product.xml"));
    assertNotNull(m.getReactions().iterator().next().getKinetics());
  }

  @Test
  public void testLayoutContainDifferentNameThanSpecies() throws Exception {
    Model m = parser.createModel(new ConverterParams().filename("testFiles/small/different_name_fullname.xml"));
    Species species = m.getElementByElementId("sa1");
    assertEquals("SNCA", species.getName());
    assertEquals("Synuclein Alpha", species.getFullName());
  }

  @Test
  public void testExtractProcessCoordinates() throws Exception {
    Model m = parser.createModel(new ConverterParams().filename("testFiles/small/process_coordinates.xml"));
    Reaction reaction = m.getReactions().iterator().next();
    assertNotNull(reaction.getProcessCoordinates());
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testParseProblematicFile() throws FileNotFoundException, InvalidInputDataExecption {
    parser.createModel(new ConverterParams().filename("testFiles/problem_in_sbml.xml"));
  }

  @Test
  public void testMultiComplexes() throws Exception {
    Model model = parser.createModel(new ConverterParams().filename("testFiles/multi/complex.xml"));
    Complex complex = model.getElementByElementId("species_SMAD1_5_8");
    Species child1 = model.getElementByElementId("species_SMAD1");
    Species child2 = model.getElementByElementId("species_SMAD5");
    Species child3 = model.getElementByElementId("species_SMAD8");

    assertEquals(3, complex.getElements().size());
    assertEquals(complex, child1.getComplex());
    assertEquals(complex, child2.getComplex());
    assertEquals(complex, child3.getComplex());

    exporter.setProvideDefaults(false);

    Model model2 = super.getModelAfterSerializing(model);

    assertEquals(0, new ModelComparator().compare(model, model2));
  }

}
