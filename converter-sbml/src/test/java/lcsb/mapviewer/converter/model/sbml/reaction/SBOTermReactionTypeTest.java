package lcsb.mapviewer.converter.model.sbml.reaction;

import lcsb.mapviewer.converter.model.sbml.SbmlExporter;
import lcsb.mapviewer.converter.model.sbml.SbmlModelUtils;
import lcsb.mapviewer.converter.model.sbml.SbmlTestFunctions;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;
import org.junit.Test;
import org.sbml.jsbml.Reaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SBOTermReactionTypeTest extends SbmlTestFunctions {

  @Test
  public void testTypeForUnknownSBOTerm() {
    Reaction reaction = new Reaction(SbmlExporter.SUPPORTED_VERSION, SbmlExporter.SUPPORTED_LEVEL);
    reaction.setSBOTerm("SBO:0000000");
    Class<? extends lcsb.mapviewer.model.map.reaction.Reaction> clazz = SBOTermReactionType.getTypeSBOTerm(reaction.getSBOTermID(),
        SbmlModelUtils.createMarker(ProjectLogEntryType.PARSING_ISSUE, reaction));
    assertNotNull(clazz);
    assertEquals(1, super.getWarnings().size());
    assertNotNull(super.getWarnings().get(0).getMarker());
  }

}
