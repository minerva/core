package lcsb.mapviewer.converter.model.sbml.reaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.Curve;
import org.sbml.jsbml.ext.layout.LineSegment;
import org.sbml.jsbml.ext.layout.Point;
import org.sbml.jsbml.ext.layout.ReactionGlyph;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.model.sbml.SbmlExporter;
import lcsb.mapviewer.converter.model.sbml.SbmlTestFunctions;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;

@RunWith(Parameterized.class)
public class SbmlReactionParserExtractCurveTest extends SbmlTestFunctions {

  private ReactionGlyph glyph;
  private int length;

  private SbmlReactionParser sbmlReactionParser;
  private Curve curve;

  public SbmlReactionParserExtractCurveTest(final String name, final ReactionGlyph reactionGlyph, final int length) {
    this.glyph = reactionGlyph;
    this.length = length;

    sbmlReactionParser = new SbmlReactionParser(new Model(SbmlExporter.SUPPORTED_VERSION, SbmlExporter.SUPPORTED_LEVEL), null, null, null);
  }

  @Parameters(name = "{0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<>();
    for (int segmentCount = 0; segmentCount < 7; segmentCount++) {
      ReactionGlyph reactionGlyph = new ReactionGlyph(SbmlExporter.SUPPORTED_VERSION, SbmlExporter.SUPPORTED_LEVEL);
      Curve curve = new Curve(SbmlExporter.SUPPORTED_VERSION, SbmlExporter.SUPPORTED_LEVEL);
      reactionGlyph.setCurve(curve);
      for (int i = 0; i < segmentCount; i++) {
        LineSegment curveSegment = new LineSegment(SbmlExporter.SUPPORTED_VERSION, SbmlExporter.SUPPORTED_LEVEL);
        int startX = ((i + 1) / 2) * 100;
        int startY = (i / 2) * 100;
        int endX = ((i + 2) / 2) * 100;
        int endY = ((i + 1) / 2) * 100;
        curveSegment.setStart(createPoint(startX, startY));
        curveSegment.setEnd(createPoint(endX, endY));
        curve.addCurveSegment(curveSegment);
      }
      data.add(new Object[] { "curve with " + segmentCount + " segments", reactionGlyph, segmentCount * 100 });
    }

    return data;
  }

  private static Point createPoint(final int startX, final int startY) {
    Point result = new Point(SbmlExporter.SUPPORTED_VERSION, SbmlExporter.SUPPORTED_LEVEL);
    result.setX(startX);
    result.setY(startY);
    return result;
  }

  @Before
  public final void setUp() throws Exception {
    curve = new Curve(glyph.getCurve());
  }

  @After
  public final void tearDown() throws Exception {
    glyph.setCurve(curve);
  }

  @Test
  public void testCenterLine() throws Exception {
    Reaction reaction = createReaction(createProtein(), createProtein());

    PolylineData line = sbmlReactionParser.extractCurve(glyph, reaction, null);
    assertNotNull("Extracted line cannot be null", line);
    assertEquals(length, line.length(), Configuration.EPSILON);
  }

  @Test
  public void testCenterLineWithInputOperator() throws Exception {
    Reaction reaction = createReaction(createProtein(), createProtein());
    reaction.addReactant(new Reactant(createProtein()));

    PolylineData centerLine = sbmlReactionParser.extractCurve(glyph, reaction, null);
    assertNotNull("Extracted center line cannot be null", centerLine);
    if (length > 0) {
      assertTrue(centerLine.length() > 0);
    }

    PolylineData inputLine = sbmlReactionParser.extractCurve(glyph, reaction, true);
    assertNotNull("Extracted input line cannot be null", inputLine);
    if (length > 0) {
      assertTrue(inputLine.length() > 0);
    }

    assertEquals(length, inputLine.length() + centerLine.length(), Configuration.EPSILON);
  }

  @Test
  public void testCenterLineWithOutputOperator() throws Exception {
    Reaction reaction = createReaction(createProtein(), createProtein());
    reaction.addProduct(new Product(createProtein()));

    PolylineData centerLine = sbmlReactionParser.extractCurve(glyph, reaction, null);
    assertNotNull("Extracted center line cannot be null", centerLine);
    if (length > 0) {
      assertTrue(centerLine.length() > 0);
    }

    PolylineData outputLine = sbmlReactionParser.extractCurve(glyph, reaction, false);
    assertNotNull("Extracted output line cannot be null", outputLine);
    if (length > 0) {
      assertTrue(outputLine.length() > 0);
    }

    assertEquals(length, outputLine.length() + centerLine.length(), Configuration.EPSILON);
  }

  @Test
  public void testCenterLineWithInputAndOutputOperator() throws Exception {
    Reaction reaction = createReaction(createProtein(), createProtein());
    reaction.addProduct(new Product(createProtein()));
    reaction.addReactant(new Reactant(createProtein()));

    PolylineData centerLine = sbmlReactionParser.extractCurve(glyph, reaction, null);
    assertNotNull("Extracted center line cannot be null", centerLine);
    if (length > 0) {
      assertTrue(centerLine.length() > 0);
    }

    PolylineData inputLine = sbmlReactionParser.extractCurve(glyph, reaction, true);
    assertNotNull("Extracted input line cannot be null", inputLine);
    if (length > 0) {
      assertTrue(inputLine.length() > 0);
    }

    PolylineData outputLine = sbmlReactionParser.extractCurve(glyph, reaction, false);
    assertNotNull("Extracted output line cannot be null", outputLine);
    if (length > 0) {
      assertTrue(outputLine.length() > 0);
    }

    assertEquals(length, outputLine.length() + inputLine.length() + centerLine.length(), Configuration.EPSILON);
  }

}
