package lcsb.mapviewer.converter.model.sbml;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Point2D;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.species.Ion;

@Ignore("Not supported by multi extension")
@RunWith(Parameterized.class)
public class ReactionPropertiesExportToMultiTest extends SbmlTestFunctions {

  private static String reactionId = "reactionId";
  private Model model;

  public ReactionPropertiesExportToMultiTest(final String propertyName, final Model model) {
    this.model = model;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<>();

    data.add(createTestEntry("Empty", createReaction()));

    Reaction reaction = createReaction();
    reaction.setAbbreviation("xyz");
    data.add(createTestEntry("Abbreviation", reaction));

    reaction = createReaction();
    reaction.setMechanicalConfidenceScore(1);
    data.add(createTestEntry("Mechanical Confidence Score", reaction));

    reaction = createReaction();
    reaction.setSubsystem("sub system");
    data.add(createTestEntry("Subsystem", reaction));

    reaction = createReaction();
    reaction.setFormula("C2H5OH");
    data.add(createTestEntry("Formula", reaction));

    reaction = createReaction();
    reaction.setGeneProteinReaction("some magic string");
    data.add(createTestEntry("Gene Protein Reaction", reaction));

    reaction = createReaction();
    reaction.setLowerBound(12.0);
    data.add(createTestEntry("Lower bound", reaction));

    reaction = createReaction();
    reaction.setUpperBound(13.0);
    data.add(createTestEntry("Upper bound", reaction));

    reaction = createReaction();
    reaction.setSymbol("H2O");
    data.add(createTestEntry("Symbol", reaction));

    reaction = createReaction();
    reaction.setSynonyms(Arrays.asList(new String[] { "syn 1", "s2" }));
    data.add(createTestEntry("Synonyms", reaction));

    return data;
  }

  private static Reaction createReaction() {
    Reaction reactionm = new StateTransitionReaction("re");

    Ion ion = createIon(1);
    Ion ion2 = createIon(2);

    reactionm.setIdReaction(reactionId);
    reactionm.addReactant(createReactant(ion));
    reactionm.addProduct(createProduct(ion2));

    return reactionm;
  }

  private static Reactant createReactant(final Ion ion) {
    Reactant result = new Reactant(ion);
    Point2D point = ion.getCenter();
    point.setLocation(point.getX() + 300, point.getY());
    result.setLine(new PolylineData(ion.getCenter(), point));
    return result;
  }

  private static Product createProduct(final Ion ion) {
    Product result = new Product(ion);
    Point2D point = ion.getCenter();
    point.setLocation(point.getX() + 300, point.getY());
    result.setLine(new PolylineData(ion.getCenter(), point));
    return result;
  }

  private static Ion createIon(final int id) {
    Ion ion = new Ion("x" + id);
    ion.setName("ion " + id);
    ion.setWidth(100);
    ion.setHeight(100);
    ion.setX(200 * (id % 2 + 1));
    ion.setY(50 * (id / 2 + 1));
    ion.setOnlySubstanceUnits(true);
    ion.setConstant(true);
    ion.setInitialAmount(2.0);
    ion.setBoundaryCondition(true);
    return ion;
  }

  private static Object[] createTestEntry(final String string, final Reaction element) {
    Model result = new ModelFullIndexed(null);
    result.setIdModel("X");
    result.setWidth(200);
    result.setHeight(200);
    for (final ReactionNode node : ((Reaction) element).getReactionNodes()) {
      result.addElement(node.getElement());
    }
    result.addReaction((Reaction) element);
    return new Object[] { string, result };
  }

  @Test
  public void createModelTest() throws Exception {
    SbmlExporter sbmlExporter = new SbmlExporter();
    SbmlParser sbmlParser = new SbmlParser();
    String xmlString = sbmlExporter.toXml(model);

    InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

    Model model2 = sbmlParser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));
    model2.setName(model.getName());
    model2.getReactions().iterator().next().setIdReaction(reactionId);

    ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(model, model2));
  }

}
