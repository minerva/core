package lcsb.mapviewer.converter.model.sbml.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.sbml.jsbml.ext.render.LocalStyle;

import lcsb.mapviewer.converter.model.sbml.SbmlTestFunctions;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.modelutils.map.ElementUtils;

@RunWith(Parameterized.class)
public class SbmlSpeciesExporterRenderShapeTest extends SbmlTestFunctions {


  private Species species;

  public SbmlSpeciesExporterRenderShapeTest(final String name, final Species species) {
    this.species = species;
  }

  private SbmlSpeciesExporterTestHelper helper;

  @Before
  public void setUp()  {
    helper = new SbmlSpeciesExporterTestHelper();
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws Exception {
    Collection<Object[]> data = new ArrayList<Object[]>();
    for (final Class<?> clazz : new ElementUtils().getAvailableElementSubclasses()) {
      if (Species.class.isAssignableFrom(clazz)) {
        Species species = (Species) (clazz.getConstructor(String.class).newInstance("s1"));
        species.setX(10);
        species.setY(20);
        species.setWidth(50);
        species.setHeight(50);
        data.add(new Object[] { species.getClass().getName(), species });
      }
    }
    return data;
  }

  @Test
  public void createModelTest() throws Exception {
    Model model = new ModelFullIndexed(null);
    model.addElement(species);
    
    SbmlSpeciesExporter exporter = helper.createExporter(model);
    
    LocalStyle style = exporter.createStyle(species);
    assertNotNull(style);
    assertEquals("Style was not generated without issues", 0, super.getWarnings().size());
  }

  
}
