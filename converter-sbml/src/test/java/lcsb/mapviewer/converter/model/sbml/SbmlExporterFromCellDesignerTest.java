package lcsb.mapviewer.converter.model.sbml;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;

import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.reaction.Reaction;

public class SbmlExporterFromCellDesignerTest extends SbmlTestFunctions {

  private CellDesignerXmlParser cellDesignerXmlParser = new CellDesignerXmlParser();

  private ModelComparator comparator = new ModelComparator();

  @Test
  public void testExportHeterodimerAssociation() throws Exception {
    Model originalModel = cellDesignerXmlParser
        .createModel(new ConverterParams().filename("testFiles/cell_designer_problems/heterodimer_association.xml"));
    Model model = getModelAfterSerializing(originalModel);
    Reaction reaction1 = originalModel.getReactions().iterator().next();
    Reaction reaction2 = model.getReactions().iterator().next();
    // change reaction id - due to some issues it cannot be persisted properly in
    // sbml format
    reaction2.setIdReaction(reaction1.getIdReaction());

    model.setName(originalModel.getName());

    String cellDesignerXml = cellDesignerXmlParser.model2String(model);
    ByteArrayInputStream stream = new ByteArrayInputStream(cellDesignerXml.getBytes("UTF-8"));

    assertEquals(0, comparator.compare(model, originalModel));
    Model cdModel = cellDesignerXmlParser.createModel(new ConverterParams().inputStream(stream));

    cdModel.setName(originalModel.getName());

    assertEquals(0, comparator.compare(model, cdModel));
  }

  @Test
  public void testExportBooleanGateModifiers() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();

    Model model = parser.createModel(
        new ConverterParams().filename("testFiles/cell_designer_problems/boolean.xml").sizeAutoAdjust(false));

    Model model2 = getModelAfterSerializing(model);

    Reaction originalReaction = model.getReactions().iterator().next();

    double lineLength = originalReaction.getModifiers().get(0).getLine().length()
        + originalReaction.getOperators().get(0).getLine().length();

    Reaction serializedReaction = model2.getReactions().iterator().next();

    assertEquals(0, serializedReaction.getOperators().size());
    assertEquals(lineLength, serializedReaction.getModifiers().get(0).getLine().length(), Configuration.EPSILON);
  }

}
