package lcsb.mapviewer.converter.model.sbml.reaction;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Point2D;
import java.io.ByteArrayInputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.converter.model.sbml.SbmlTestFunctions;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionComparator;
import lcsb.mapviewer.model.map.reaction.type.SimpleReactionInterface;
import lcsb.mapviewer.model.map.reaction.type.TwoProductReactionInterface;
import lcsb.mapviewer.model.map.reaction.type.TwoReactantReactionInterface;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.modelutils.map.ElementUtils;

@RunWith(Parameterized.class)
public class SbmlReactionExportArrowTypeTest extends SbmlTestFunctions {

  private static PointTransformation pt = new PointTransformation();

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  private Model modelToBeTested;

  public SbmlReactionExportArrowTypeTest(final String name, final Model model) {
    modelToBeTested = model;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws Exception {
    Collection<Object[]> data = new ArrayList<>();
    ElementUtils eu = new ElementUtils();
    for (final Class<?> clazz : eu.getAvailableReactionSubclasses()) {
      Model modelAfterSerialization = createModelForReactionType(clazz);

      data.add(new Object[] { clazz.getSimpleName(), modelAfterSerialization });
    }
    return data;
  }

  private static Model createModelForReactionType(final Class<?> clazz) throws InstantiationException, IllegalAccessException,
      InvocationTargetException, NoSuchMethodException, InconsistentModelException, InvalidInputDataExecption,
      Exception {
    Model model = new ModelFullIndexed(null);
    model.setWidth(1000);
    model.setHeight(1000);
    Ion ion = createIon(1);
    model.addElement(ion);

    Ion ion2 = createIon(2);
    model.addElement(ion2);

    Ion ion3 = createIon(3);
    model.addElement(ion3);

    Reaction reaction = (Reaction) clazz.getConstructor(String.class).newInstance(new Object[] { "reactionId" });
    Reactant reactant = createReactant(ion);
    reaction.addReactant(reactant);
    Product product = createProduct(ion2);
    reaction.addProduct(product);
    if (TwoReactantReactionInterface.class.isAssignableFrom(clazz)) {
      reaction.addReactant(createReactant(ion3));
      NodeOperator operator = new AndOperator();
      operator.addInputs(reaction.getReactants());
      operator.setLine(new PolylineData(pt.copyPoint(reactant.getLine().getEndPoint()),
          pt.copyPoint(product.getLine().getStartPoint())));
      reaction.addNode(operator);
    } else if (TwoProductReactionInterface.class.isAssignableFrom(clazz)) {
      reaction.addProduct(createProduct(ion3));
      NodeOperator operator = new AndOperator();
      operator.addOutputs(reaction.getProducts());
      operator.setLine(new PolylineData(new Point2D.Double(10, 10), new Point2D.Double(10, 20)));
      reaction.addNode(operator);
    } else if (!SimpleReactionInterface.class.isAssignableFrom(clazz)) {
      throw new NotImplementedException();
    }
    reaction.setZ(1);
    reaction.setLine(new PolylineData(new Point2D.Double(10, 40), new Point2D.Double(10, 60)));
    model.addReaction(reaction);

    CellDesignerXmlParser cellDesignerXmlParser = new CellDesignerXmlParser();
    String xmlString = cellDesignerXmlParser.model2String(model);
    Model modelAfterSerialization = cellDesignerXmlParser
        .createModel(new ConverterParams().inputStream(new ByteArrayInputStream(xmlString.getBytes())));
    return modelAfterSerialization;
  }

  private static Ion createIon(final int id) {
    Ion ion = new Ion("x" + id);
    ion.setName("ion " + id);
    assignCoordinates(200 * (id % 2 + 1), 100 * id, 100, 100, ion);
    ion.setOnlySubstanceUnits(true);
    ion.setConstant(true);
    ion.setInitialAmount(2.0);
    ion.setBoundaryCondition(true);
    return ion;
  }

  private static Reactant createReactant(final Ion ion) {
    Reactant result = new Reactant(ion);
    Point2D point = ion.getCenter();
    point.setLocation(point.getX() + 300, point.getY());
    result.setLine(new PolylineData(ion.getCenter(), point));
    return result;
  }

  private static Product createProduct(final Ion ion) {
    Product result = new Product(ion);
    Point2D point = ion.getCenter();
    point.setLocation(point.getX() + 300, point.getY());
    result.setLine(new PolylineData(point, ion.getCenter()));
    return result;
  }

  @Test
  public void test() throws Exception {
    Model modelAfterSerialization = getModelAfterSerializing(modelToBeTested);

    ReactionComparator reactionComparator = new ReactionComparator();

    Reaction originalReaction = modelToBeTested.getReactions().iterator().next();
    Reaction afterSerializationReaction = modelAfterSerialization.getReactions().iterator().next();

    afterSerializationReaction.setIdReaction(originalReaction.getIdReaction());
    assertEquals(0, reactionComparator.compare(originalReaction, afterSerializationReaction));
  }
}
