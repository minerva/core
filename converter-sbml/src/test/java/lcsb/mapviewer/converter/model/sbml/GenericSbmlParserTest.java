package lcsb.mapviewer.converter.model.sbml;

import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator;
import lcsb.mapviewer.converter.graphics.NormalImageGenerator;
import lcsb.mapviewer.converter.graphics.PngImageGenerator;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.reaction.ReactionComparator;
import lcsb.mapviewer.model.map.species.Element;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(Parameterized.class)
public class GenericSbmlParserTest extends SbmlTestFunctions {


  private final Path filePath;

  public GenericSbmlParserTest(final Path filePath) {
    this.filePath = filePath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    final Collection<Object[]> data = new ArrayList<Object[]>();
    Files.walk(Paths.get("testFiles/layoutExample")).forEach(filePath -> {
      if (Files.isRegularFile(filePath) && filePath.toString().endsWith(".xml")) {
        data.add(new Object[]{filePath});
      }
    });
    Files.walk(Paths.get("testFiles/invalidButParseable")).forEach(filePath -> {
      if (Files.isRegularFile(filePath) && filePath.toString().endsWith(".xml")) {
        data.add(new Object[]{filePath});
      }
    });
    return data;
  }

  @Test
  public void createModelTest() throws Exception {
    final String dir = Files.createTempDirectory("sbml-temp-images-dir").toFile().getAbsolutePath();

    final Converter converter = new SbmlParser();

    final Model model = converter.createModel(new ConverterParams().filename(filePath.toString()));
    model.setName("Unknown");

    // Create and display image of parsed map
    final AbstractImageGenerator.Params params = new AbstractImageGenerator.Params().height(model.getHeight())
        .width(model.getWidth()).nested(true).scale(1).level(20).x(0).y(0).model(model);
    final NormalImageGenerator nig = new PngImageGenerator(params);
    final String pathWithouExtension = dir + "/"
        + filePath.getFileName().toString().substring(0, filePath.getFileName().toString().indexOf(".xml"));
    final String pngFilePath = pathWithouExtension.concat(".png");
    nig.saveToFile(pngFilePath);

    final CellDesignerXmlParser cellDesignerXmlParser = new CellDesignerXmlParser();
    final String xmlString = cellDesignerXmlParser.model2String(model);

    final InputStream is = new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8));

    final Model model2 = cellDesignerXmlParser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

    final AbstractImageGenerator.Params params2 = new AbstractImageGenerator.Params().height(model2.getHeight())
        .width(model2.getWidth()).nested(true).scale(1).level(20).x(0).y(0).model(model2);
    final NormalImageGenerator nig2 = new PngImageGenerator(params2);
    final String pngFilePath2 = pathWithouExtension.concat("_2.png");
    nig2.saveToFile(pngFilePath2);

    assertNotNull(model2);
    final ModelComparator comparator = new ModelComparator(1.0);
    for (final BioEntity bioEntity : model.getBioEntities()) {
      bioEntity.setZ(null);
    }
    for (final BioEntity bioEntity : model2.getBioEntities()) {
      bioEntity.setZ(null);
    }
    for (final Element bioEntity : model.getElements()) {
      bioEntity.setNameX(0);
      bioEntity.setNameY(0);
      bioEntity.setNameWidth(0);
      bioEntity.setNameHeight(0);
      bioEntity.setNameHorizontalAlign(null);
      bioEntity.setNameVerticalAlign(null);
    }
    for (final Element bioEntity : model2.getElements()) {
      bioEntity.setNameX(0);
      bioEntity.setNameY(0);
      bioEntity.setNameWidth(0);
      bioEntity.setNameHeight(0);
      bioEntity.setNameHorizontalAlign(null);
      bioEntity.setNameVerticalAlign(null);
    }
    comparator.getReactionSetComparator().setObjectComparator(new ReactionComparator(1.0, true));
    assertEquals(0, comparator.compare(model, model2));
    FileUtils.deleteDirectory(new File(dir));
  }

}
