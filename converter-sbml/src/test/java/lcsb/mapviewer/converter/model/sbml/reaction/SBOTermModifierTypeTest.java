package lcsb.mapviewer.converter.model.sbml.reaction;

import lcsb.mapviewer.converter.model.sbml.SbmlExporter;
import lcsb.mapviewer.converter.model.sbml.SbmlModelUtils;
import lcsb.mapviewer.converter.model.sbml.SbmlTestFunctions;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.sbo.SBOTermModifierType;
import org.junit.Test;
import org.sbml.jsbml.ModifierSpeciesReference;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SBOTermModifierTypeTest extends SbmlTestFunctions {

  @Test
  public void testUnknownId() {
    ModifierSpeciesReference reference = new ModifierSpeciesReference(SbmlExporter.SUPPORTED_VERSION, SbmlExporter.SUPPORTED_LEVEL);
    reference.setSBOTerm("SBO:0000000");
    Class<? extends Modifier> clazz = SBOTermModifierType.getTypeSBOTerm("SBO:0000000",
        SbmlModelUtils.createMarker(ProjectLogEntryType.PARSING_ISSUE, reference));
    assertNotNull(clazz);
    assertEquals(1, super.getWarnings().size());
    assertNotNull(super.getWarnings().get(0).getMarker());
  }

}
