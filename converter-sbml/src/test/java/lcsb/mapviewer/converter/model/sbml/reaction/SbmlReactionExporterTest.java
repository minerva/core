package lcsb.mapviewer.converter.model.sbml.reaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;

import org.junit.Test;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.ext.layout.Curve;
import org.sbml.jsbml.ext.layout.Dimensions;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.LayoutModelPlugin;
import org.sbml.jsbml.ext.layout.ReactionGlyph;

import lcsb.mapviewer.converter.model.sbml.SbmlBioEntityExporter;
import lcsb.mapviewer.converter.model.sbml.SbmlExtension;
import lcsb.mapviewer.converter.model.sbml.compartment.SbmlCompartmentExporter;
import lcsb.mapviewer.converter.model.sbml.species.SbmlSpeciesExporter;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.TriggerReaction;
import lcsb.mapviewer.model.map.species.Species;

public class SbmlReactionExporterTest {

  @Test
  public void testTriggerReactionToSbml() throws InconsistentModelException {
    ModelFullIndexed model = new ModelFullIndexed(null);

    SbmlReactionExporter exporter = createExporter(model);
    org.sbml.jsbml.Reaction result = exporter.createSbmlElement(new TriggerReaction("re"));
    assertNotNull(result);
  }

  @Test
  public void testReactionWithKineticsWithoutUnitsToSbml() throws InconsistentModelException {
    ModelFullIndexed model = new ModelFullIndexed(null);

    SbmlReactionExporter exporter = createExporter(model);
    Reaction reaction = new TriggerReaction("re");
    SbmlKinetics kinetics = new SbmlKinetics();
    reaction.setKinetics(kinetics);
    kinetics.setDefinition("<lambda>" + "<bvar><ci> x </ci></bvar>" + "<bvar><ci> y </ci></bvar>"
        + "<apply><plus/><ci> x </ci><ci> x </ci><cn type=\"integer\"> 2 </cn></apply>" + "</lambda>\n\n");
    SbmlParameter parameter = new SbmlParameter("x");
    kinetics.addParameter(parameter);
    org.sbml.jsbml.Reaction result = exporter.createSbmlElement(reaction);
    assertNotNull(result);
  }

  private SbmlReactionExporter createExporter(final ModelFullIndexed model) {
    SBMLDocument doc = new SBMLDocument(3, 1);
    Model result = doc.createModel(model.getIdModel());
    result.setName(model.getName());
    LayoutModelPlugin plugin = new LayoutModelPlugin(result);

    Layout layout = new Layout();
    Dimensions dimensions = new Dimensions();
    if (model.getHeight() > 0) {
      dimensions.setHeight(model.getHeight());
    } else {
      dimensions.setHeight(640);
    }
    if (model.getWidth() > 0) {
      dimensions.setWidth(model.getWidth());
    } else {
      dimensions.setWidth(480);
    }
    layout.setDimensions(dimensions);
    plugin.add(layout);
    result.addExtension("layout", plugin);

    SbmlCompartmentExporter compartmentExporter = new SbmlCompartmentExporter(result, model,
        Arrays.asList(SbmlExtension.values()));
    SbmlBioEntityExporter<Species, org.sbml.jsbml.Species> speciesExporter = new SbmlSpeciesExporter(result, model,
        Arrays.asList(SbmlExtension.values()),
        compartmentExporter);

    SbmlReactionExporter exporter = new SbmlReactionExporter(result, model, speciesExporter,
        Arrays.asList(SbmlExtension.values()), compartmentExporter);
    return exporter;
  }

  @Test
  public void testRemoveColinearPointsFromEmptyLine() throws InconsistentModelException {
    ModelFullIndexed model = new ModelFullIndexed(null);
    SbmlReactionExporter exporter = createExporter(model);
    ReactionGlyph reactionGlyph = new ReactionGlyph();
    reactionGlyph.setCurve(new Curve());
    exporter.removeColinearPoints(reactionGlyph);
    assertEquals(0, reactionGlyph.getCurve().getCurveSegmentCount());
  }

}
