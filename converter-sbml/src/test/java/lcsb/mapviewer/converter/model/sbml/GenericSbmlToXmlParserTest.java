package lcsb.mapviewer.converter.model.sbml;

import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(Parameterized.class)
public class GenericSbmlToXmlParserTest extends SbmlTestFunctions {


  private final Path filePath;

  public GenericSbmlToXmlParserTest(final Path filePath) {
    this.filePath = filePath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    final Collection<Object[]> data = new ArrayList<Object[]>();
    Files.walk(Paths.get("testFiles/layoutExample")).forEach(filePath -> {
      if (Files.isRegularFile(filePath) && filePath.toString().endsWith(".xml")) {
        data.add(new Object[]{filePath});
      }
    });
    Files.walk(Paths.get("testFiles/small")).forEach(filePath -> {
      if (Files.isRegularFile(filePath) && filePath.toString().endsWith(".xml")) {
        data.add(new Object[]{filePath});
      }
    });
    return data;
  }

  @Test
  public void toXmlModelTest() throws Exception {
    final SbmlParser converter = new SbmlParser();

    final Model model = converter.createModel(new ConverterParams().filename(filePath.toString()));
    testSbmlSerialization(model);
  }

  private void testSbmlSerialization(final Model model) throws InconsistentModelException,
      IOException, ConverterException, InvalidInputDataExecption {

    final SbmlParser converter = new SbmlParser();
    converter.setProvideDefaults(false);

    final String xml = converter.model2String(model);
    final InputStream stream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

    final Model model2 = converter.createModel(new ConverterParams().inputStream(stream).sizeAutoAdjust(false));

    model.setName("Unknown");
    model2.setName("Unknown");

    model.setIdModel("unk");
    model2.setIdModel("unk");

    assertNotNull(model2);
    final ModelComparator comparator = new ModelComparator(1.0);
    assertEquals(0, comparator.compare(model, model2));
  }

}
