package lcsb.mapviewer.converter.model.sbml.species;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.sbml.SbmlParser;
import lcsb.mapviewer.converter.model.sbml.SbmlTestFunctions;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.IonChannelProtein;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.ReceptorProtein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.TruncatedProtein;
import lcsb.mapviewer.model.map.species.Unknown;
import org.junit.Test;

import java.io.FileNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SbmlSpeciesParserTest extends SbmlTestFunctions {
  private final SbmlParser parser = new SbmlParser();

  @Test
  public void testParseAntisenseRna() throws FileNotFoundException, InvalidInputDataExecption {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/small/antisense_rna.xml"));
    final Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof AntisenseRna);
    assertEquals(ElementColorEnum.ANTISENSE_RNA.getColor(), element.getFillColor());
  }

  @Test
  public void testParseComplex() throws Exception {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/small/complex.xml"));
    final Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Complex);
    assertEquals(ElementColorEnum.COMPLEX.getColor(), element.getFillColor());
  }

  @Test
  public void testParseDegraded() throws Exception {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/small/degraded.xml"));
    final Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Degraded);
    assertEquals(ElementColorEnum.DEGRADED.getColor(), element.getFillColor());
  }

  @Test
  public void testParseDrug() throws Exception {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/small/drug.xml"));
    final Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Drug);
    assertEquals(ElementColorEnum.DRUG.getColor(), element.getFillColor());
  }

  @Test
  public void testParseGene() throws FileNotFoundException, InvalidInputDataExecption {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/small/gene.xml"));
    final Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Gene);
    assertEquals(ElementColorEnum.GENE.getColor(), element.getFillColor());
  }

  @Test
  public void testParseGenericProtein() throws FileNotFoundException, InvalidInputDataExecption {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/small/generic_protein.xml"));
    final Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof GenericProtein);
    assertEquals(ElementColorEnum.GENERIC_PROTEIN.getColor(), element.getFillColor());
  }

  @Test
  public void testParseTruncatedProtein() throws Exception {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/small/truncated_protein.xml"));
    final Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof TruncatedProtein);
    assertEquals(ElementColorEnum.TRUNCATED_PROTEIN.getColor(), element.getFillColor());
  }

  @Test
  public void testParseIonChannel() throws Exception {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/small/ion_channel.xml"));
    final Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof IonChannelProtein);
    assertEquals(ElementColorEnum.ION_CHANNEL.getColor(), element.getFillColor());
  }

  @Test
  public void testParseIon() throws FileNotFoundException, InvalidInputDataExecption {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/small/ion.xml"));
    final Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Ion);
    assertEquals(ElementColorEnum.ION.getColor(), element.getFillColor());
  }

  @Test
  public void testParsePhenotype() throws Exception {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/small/phenotype.xml"));
    final Element element = model.getElementByElementId("s1");
    element.setWidth(100);
    element.setHeight(100);
    assertTrue(element instanceof Phenotype);
    assertEquals(ElementColorEnum.PHENOTYPE.getColor(), element.getFillColor());
    Element element1 = super.createComplex();
    element1.setWidth(100);
    element1.setHeight(100);
    element1.setX(element.getX());
    element1.setY(element.getY());
    element1.setZ(element.getZ() - 1);
    model.addElement(element1);
    parser.model2File(model, "/tmp/phenotype.xml");
  }

  @Test
  public void testParseReceptor() throws FileNotFoundException, InvalidInputDataExecption {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/small/receptor.xml"));
    final Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof ReceptorProtein);
    assertEquals(ElementColorEnum.RECEPTOR.getColor(), element.getFillColor());
  }

  @Test
  public void testParseRna() throws FileNotFoundException, InvalidInputDataExecption {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/small/rna.xml"));
    final Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Rna);
    assertEquals(ElementColorEnum.RNA.getColor(), element.getFillColor());
  }

  @Test
  public void testParseSimpleMolecule() throws FileNotFoundException, InvalidInputDataExecption {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/small/small_molecule.xml"));
    final Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof SimpleMolecule);
    assertEquals(ElementColorEnum.SIMPLE_MOLECULE.getColor(), element.getFillColor());
  }

  @Test
  public void testParseSimpleMoleculeWithAlternativeSBOTerm() throws FileNotFoundException, InvalidInputDataExecption {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/small/small_molecule2.xml"));
    final Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof SimpleMolecule);
  }

  @Test
  public void testParseUnknown() throws FileNotFoundException, InvalidInputDataExecption {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/small/unknown_species.xml"));
    final Element element = model.getElementByElementId("s1");
    assertTrue(element instanceof Unknown);
    assertEquals(ElementColorEnum.UNKNOWN.getColor(), element.getFillColor());
  }

  @Test
  public void testParseInitialAmount() throws Exception {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/small/initial_amount.xml"));
    final Species element = model.getElementByElementId("s1");
    assertEquals((Double) 1.0, element.getInitialAmount());
  }

  @Test
  public void testParseHasOnlySubstanceUnits() throws Exception {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/small/has_only_substance_units.xml"));
    final Species element = model.getElementByElementId("s1");
    assertTrue(element.hasOnlySubstanceUnits());
  }
}
