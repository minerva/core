package lcsb.mapviewer.converter.model.sbml;

import lcsb.mapviewer.commands.CreateHierarchyCommand;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.ListComparator;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.graphics.PolylineDataComparator;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.PositionToCompartment;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.text.StringEscapeUtils;
import org.junit.Before;
import org.junit.Test;
import org.reflections.Reflections;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.ext.multi.MultiModelPlugin;
import org.sbml.jsbml.ext.multi.MultiSpeciesPlugin;
import org.sbml.jsbml.ext.multi.MultiSpeciesType;
import org.sbml.jsbml.ext.multi.PossibleSpeciesFeatureValue;
import org.sbml.jsbml.ext.multi.SpeciesFeatureType;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class SbmlExporterTest extends SbmlTestFunctions {

  private final ModelComparator comparator = new ModelComparator();

  @Before
  public void setup() {
    exporter.setProvideDefaults(false);
  }

  @Test
  public void testExportCompartment() throws Exception {
    final Model model = getModelAfterSerializing("testFiles/layoutExample/CompartmentGlyph_Example_level2_level3.xml");
    assertNotNull(model);
    assertEquals(1, model.getCompartments().size());
    final Compartment compartment = model.getElementByElementId("CompartmentGlyph_1");
    assertNotNull(compartment);
    assertNotNull(compartment.getX());
    assertNotNull(compartment.getY());
    assertTrue(compartment.getWidth() > 0);
    assertTrue(compartment.getHeight() > 0);
    assertTrue(model.getWidth() >= compartment.getX() + compartment.getWidth());
    assertTrue(model.getHeight() >= compartment.getY() + compartment.getHeight());
    assertNotEquals(Compartment.class, compartment.getClass());
  }

  private Model getModelAfterSerializing(final String filename) throws Exception {
    final Model originalModel = parser.createModel(new ConverterParams().filename(filename));
    return getModelAfterSerializing(originalModel);
  }

  @Test
  public void testExportSpecies() throws Exception {
    final Model model = getModelAfterSerializing("testFiles/layoutExample/SpeciesGlyph_Example_level2_level3.xml");
    assertNotNull(model);
    assertEquals(1, model.getElements().size());
    final Species glucoseSpecies = model.getElementByElementId("SpeciesGlyph_Glucose");
    assertNotNull(glucoseSpecies.getX());
    assertNotNull(glucoseSpecies.getY());
    assertTrue(glucoseSpecies.getWidth() > 0);
    assertTrue(glucoseSpecies.getHeight() > 0);
    assertTrue(model.getWidth() >= glucoseSpecies.getX() + glucoseSpecies.getWidth());
    assertTrue(model.getHeight() >= glucoseSpecies.getY() + glucoseSpecies.getHeight());
    assertNotEquals(Species.class, glucoseSpecies.getClass());
  }

  @Test
  public void testExportSpeciesInCompartments() throws Exception {
    final Model model = getModelAfterSerializing("testFiles/layoutExample/SpeciesGlyph_Example.xml");
    assertNotNull(model);
    assertEquals(2, model.getElements().size());
    final Compartment compartment = model.getElementByElementId("Yeast");
    assertNotNull(compartment.getX());
    assertNotNull(compartment.getY());

    assertTrue(compartment.getX() >= 0);
    assertTrue(compartment.getY() >= 0);
    assertTrue(compartment.getWidth() > 0);
    assertTrue(compartment.getHeight() > 0);
    assertTrue(model.getWidth() >= compartment.getX() + compartment.getWidth());
    assertTrue(model.getHeight() >= compartment.getY() + compartment.getHeight());
    assertNotEquals(Compartment.class, compartment.getClass());
    assertFalse(compartment.getElements().isEmpty());
  }

  @Test
  public void testExportReaction() throws Exception {
    final Model model = getModelAfterSerializing("testFiles/layoutExample/Complete_Example.xml");
    assertNotNull(model);
    assertEquals(1, model.getReactions().size());
    final Reaction reaction = model.getReactions().iterator().next();
    for (final ReactionNode node : reaction.getReactionNodes()) {
      assertNotNull(node.getLine());
      assertTrue(node.getLine().length() > 0);
    }
    assertEquals(2, reaction.getOperators().size());
  }

  @Test
  public void testExportModelId() throws Exception {
    final Model model = new ModelFullIndexed(null);
    model.setIdModel("Test123");
    final Model deserializedModel = getModelAfterSerializing(model);

    assertEquals(model.getIdModel(), deserializedModel.getIdModel());
  }

  @Test
  public void testExportModelDimension() throws Exception {
    final Model model = new ModelFullIndexed(null);
    model.setWidth(200);
    model.setHeight(300);
    final Model deserializedModel = getModelAfterSerializing(model);

    assertEquals(model.getWidth(), deserializedModel.getWidth(), Configuration.EPSILON);
    assertEquals(model.getHeight(), deserializedModel.getHeight(), Configuration.EPSILON);
  }

  @Test
  public void testExportEmptyModel() throws Exception {
    final Model model = new ModelFullIndexed(null);
    final Model deserializedModel = getModelAfterSerializing(model);

    assertNotNull(deserializedModel);
  }

  @Test
  public void testExportReactionWithLayout() throws Exception {
    final Model model = createModelWithReaction();
    final Reaction reaction = model.getReactions().iterator().next();

    final Model deserializedModel = getModelAfterSerializing(model);
    final Reaction deserializedReaction = deserializedModel.getReactions().iterator().next();

    assertEquals(reaction.getReactants().get(0).getLine().length(),
        deserializedReaction.getReactants().get(0).getLine().length(), Configuration.EPSILON);
    assertEquals(reaction.getProducts().get(0).getLine().length(),
        deserializedReaction.getProducts().get(0).getLine().length(), Configuration.EPSILON);

  }

  private Model createModelWithReaction() {
    final Model model = createEmptyModel();
    final GenericProtein p1 = createProtein();
    p1.setY(10);
    model.addElement(p1);
    final GenericProtein p2 = createProtein();
    p2.setY(50);
    model.addElement(p2);
    final Reaction reaction = createReaction(p1, p2);
    model.addReaction(reaction);
    return model;
  }

  @Test
  public void testExportReactionWithNotes() throws Exception {
    final Model model = createModelWithReaction();
    final Reaction reaction = model.getReactions().iterator().next();
    reaction.setNotes("XYZ");

    final Model deserializedModel = getModelAfterSerializing(model);
    final Reaction deserializedReaction = deserializedModel.getReactions().iterator().next();

    assertEquals(reaction.getNotes(), deserializedReaction.getNotes());

  }

  @Test
  public void testExportModelWithReaction() throws Exception {
    final String tempFilename = File.createTempFile("tmp", ".xml").getAbsolutePath();
    final SbmlParser converter = new SbmlParser();
    converter.setProvideDefaults(false);

    final Model model = converter.createModel(new ConverterParams().filename("testFiles/layoutExample/example1.xml"));
    model.setName("Unknown");

    converter.model2File(model, tempFilename);

    final Model model2 = converter.createModel(new ConverterParams().filename(tempFilename));
    model2.setName("Unknown");

    assertNotNull(model2);
    final ModelComparator comparator = new ModelComparator(1.0);
    assertEquals(0, comparator.compare(model, model2));

  }

  @Test
  public void testExportModelName() throws Exception {
    final String tempFilename = File.createTempFile("tmp", ".xml").getAbsolutePath();
    final SbmlParser converter = new SbmlParser();
    converter.setProvideDefaults(false);

    final Model model = createEmptyModel();

    converter.model2File(model, tempFilename);

    final Model model2 = converter.createModel(new ConverterParams().filename(tempFilename));

    assertNotNull(model2);
    final ModelComparator comparator = new ModelComparator(1.0);
    assertEquals(0, comparator.compare(model, model2));

  }

  @Test
  public void testExportProblematicNotes() throws Exception {
    final Model model = createModelWithReaction();
    final Reaction reaction = model.getReactions().iterator().next();
    reaction.setNotes("X=Y<Z");

    final Model deserializedModel = getModelAfterSerializing(model);
    final Reaction deserializedReaction = deserializedModel.getReactions().iterator().next();

    assertTrue(StringEscapeUtils.unescapeXml(deserializedReaction.getNotes()).contains(reaction.getNotes()));

  }

  @Test
  public void testColorParsing() throws Exception {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/layoutExample/example1.xml"));
    for (final Species element : model.getSpeciesList()) {
      element.setFillColor(Color.BLUE);
    }
    final Model model2 = getModelAfterSerializing(model);
    assertEquals(0, comparator.compare(model, model2));
  }

  @Test
  public void testModelAnnotationExport() throws Exception {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/small/model_with_annotations.xml"));
    final Model model2 = getModelAfterSerializing(model);
    assertEquals(0, comparator.compare(model, model2));
  }

  @Test
  public void testReactionColorParsing() throws Exception {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/layoutExample/example1.xml"));
    for (final Reaction element : model.getReactions()) {
      for (final AbstractNode node : element.getNodes()) {
        node.getLine().setColor(Color.BLUE);
      }
    }
    final Model model2 = getModelAfterSerializing(model);
    assertEquals(0, comparator.compare(model, model2));
  }

  @Test
  public void testReactionLineWidthParsing() throws Exception {
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/layoutExample/example1.xml"));
    for (final Reaction element : model.getReactions()) {
      for (final AbstractNode node : element.getNodes()) {
        node.getLine().setWidth(12.7);
      }
    }
    final Model model2 = getModelAfterSerializing(model);
    assertEquals(0, comparator.compare(model, model2));
  }

  @Test
  public void testExportSpeciesType() throws Exception {
    final Reflections reflections = new Reflections("lcsb.mapviewer.model.map");
    final Set<Class<? extends Species>> classes = reflections.getSubTypesOf(Species.class);
    for (final Class<? extends Element> class1 : classes) {
      if (!Modifier.isAbstract(class1.getModifiers())) {
        final Model model = createEmptyModel();
        final Element element = class1.getConstructor(String.class).newInstance("x");
        element.setName("test name");
        assignCoordinates(10, 10, 10, 10, element);
        element.setNameVerticalAlign(VerticalAlign.BOTTOM);
        model.addElement(element);
        final Model deserializedModel = getModelAfterSerializing(model);

        assertEquals("Class " + class1 + " not exported/imported properly", 0,
            comparator.compare(model, deserializedModel));
      }
    }
  }

  @Test
  public void testSetUsedExtensions() {
    final SbmlExporter exporter = new SbmlExporter();
    exporter.removeSbmlExtensions(Arrays.asList(SbmlExtension.values()));
    exporter.addSbmlExtension(SbmlExtension.LAYOUT);

    assertEquals(1, exporter.getSbmlExtensions().size());
    assertEquals(SbmlExtension.LAYOUT, exporter.getSbmlExtensions().iterator().next());

    exporter.removeSbmlExtension(SbmlExtension.LAYOUT);
    assertEquals(0, exporter.getSbmlExtensions().size());
  }

  @Test
  public void testExportWithLayoutExtension() throws Exception {
    final SbmlExporter exporter = new SbmlExporter();
    exporter.removeSbmlExtensions(Arrays.asList(SbmlExtension.values()));
    exporter.addSbmlExtension(SbmlExtension.LAYOUT);

    final Model model = createModelWithReaction();
    final String xml = exporter.toXml(model);

    assertTrue(xml.contains("layout:listOfLayouts"));
  }

  @Test
  public void testExportWithoutLayoutExtension() throws Exception {
    final SbmlExporter exporter = new SbmlExporter();
    exporter.removeSbmlExtensions(Arrays.asList(SbmlExtension.values()));

    final Model model = createModelWithReaction();
    final String xml = exporter.toXml(model);

    assertFalse(xml.contains("layout:listOfLayouts"));
  }

  @Test
  public void testExportWithRenderExtension() throws Exception {
    final SbmlExporter exporter = new SbmlExporter();
    exporter.removeSbmlExtensions(Arrays.asList(SbmlExtension.values()));
    exporter.addSbmlExtension(SbmlExtension.LAYOUT);
    exporter.addSbmlExtension(SbmlExtension.RENDER);

    final Model model = createModelWithReaction();
    final String xml = exporter.toXml(model);

    assertTrue(xml.contains("render:listOfRenderInformation"));
  }

  @Test
  public void testExportWithoutRenderExtension() throws Exception {
    final SbmlExporter exporter = new SbmlExporter();
    exporter.removeSbmlExtensions(Arrays.asList(SbmlExtension.values()));
    exporter.addSbmlExtension(SbmlExtension.LAYOUT);

    final Model model = createModelWithReaction();
    final String xml = exporter.toXml(model);

    assertFalse(xml.contains("render:listOfRenderInformation"));
  }

  @Test
  public void testExportWithoutMultiExtensionSupportSpeciesTypes() throws Exception {
    final SbmlExporter exporter = new SbmlExporter();
    exporter.removeSbmlExtensions(Arrays.asList(SbmlExtension.values()));
    exporter.addSbmlExtension(SbmlExtension.MULTI);

    final org.sbml.jsbml.Model sbmlModel = exporter.toSbmlDocument(createModelWithReaction()).getModel();
    final MultiModelPlugin multiPlugin = (MultiModelPlugin) sbmlModel.getExtension("multi");
    assertNotNull("Multi plugin is not present in sbml model", multiPlugin);

    assertFalse("Species types are not exported", multiPlugin.getListOfSpeciesTypes().isEmpty());
  }

  @Test
  public void testMultiExtensionSBOTermsForTypes() {
    final SbmlExporter exporter = new SbmlExporter();
    final SBMLDocument doc = new SBMLDocument(SbmlExporter.SUPPORTED_VERSION, SbmlExporter.SUPPORTED_LEVEL);
    final org.sbml.jsbml.Model result = doc.createModel("id");
    final MultiModelPlugin multiPlugin = exporter.createSbmlMultiPlugin(result);

    for (final MultiSpeciesType speciesType : multiPlugin.getListOfSpeciesTypes()) {
      assertNotNull("SBO term not defined for type " + speciesType.getName(), speciesType.getSBOTermID());
      assertFalse("SBO term not defined for type " + speciesType.getName(), speciesType.getSBOTermID().isEmpty());
    }

  }

  @Test
  public void testExportProteinState() throws Exception {
    final Model model = createEmptyModel();
    final GenericProtein element = createProtein();
    element.addStructuralState(createStructuralState(element));
    model.addElement(element);
    final Model deserializedModel = getModelAfterSerializing(model);

    assertEquals("Structural state not exported/imported properly", 0, comparator.compare(model, deserializedModel));
  }

  @Test
  public void testExportPositionToCompartment() throws Exception {
    final Model model = createEmptyModel();
    final GenericProtein element = createProtein();
    element.setPositionToCompartment(PositionToCompartment.INSIDE);
    model.addElement(element);
    final Model deserializedModel = getModelAfterSerializing(model);

    assertEquals("Postion to compartment not exported/imported properly", 0,
        comparator.compare(model, deserializedModel));
  }

  @Test
  public void testExportOuterSurfacePositionToCompartment() throws Exception {
    final Model model = createEmptyModel();
    final GenericProtein element = createProtein();
    element.setPositionToCompartment(PositionToCompartment.OUTER_SURFACE);
    model.addElement(element);
    final Model deserializedModel = getModelAfterSerializing(model);

    assertEquals("Position to compartment not exported/imported properly", 0,
        comparator.compare(model, deserializedModel));
  }

  @Test
  public void testMultiExtensionProteinStateInTypes() throws Exception {
    final Model model = createEmptyModel();
    final GenericProtein element = createProtein();
    final StructuralState structuralState = createStructuralState(element);
    element.addStructuralState(structuralState);
    model.addElement(element);
    final org.sbml.jsbml.Model sbmlModel = exporter.toSbmlDocument(model).getModel();
    final MultiModelPlugin multiPlugin = (MultiModelPlugin) sbmlModel.getExtension("multi");

    boolean structuralStateValueFound = false;
    for (final MultiSpeciesType speciesType : multiPlugin.getListOfSpeciesTypes()) {
      for (final SpeciesFeatureType featureType : speciesType.getListOfSpeciesFeatureTypes()) {
        for (final PossibleSpeciesFeatureValue featureValue : featureType.getListOfPossibleSpeciesFeatureValues()) {
          if (featureValue.getName().equals(structuralState.getName())) {
            structuralStateValueFound = true;
          }
        }
      }
    }
    assertTrue("Structural state not defined in the list of possible values", structuralStateValueFound);

  }

  @Test
  public void testMultiExtensionProteinStateSuplicateInTypes() throws Exception {
    final Model model = createEmptyModel();
    GenericProtein element = createProtein();
    element.addStructuralState(createStructuralState(element));
    model.addElement(element);
    element = createProtein();
    element.addStructuralState(createStructuralState(element));
    model.addElement(element);
    final org.sbml.jsbml.Model sbmlModel = exporter.toSbmlDocument(model).getModel();
    final MultiModelPlugin multiPlugin = (MultiModelPlugin) sbmlModel.getExtension("multi");

    for (final MultiSpeciesType speciesType : multiPlugin.getListOfSpeciesTypes()) {
      for (final SpeciesFeatureType featureType : speciesType.getListOfSpeciesFeatureTypes()) {
        assertTrue(featureType.getListOfPossibleSpeciesFeatureValues().size() <= 2);
      }
    }
  }

  @Test
  public void testMultiExtensionTypeDefinition() throws Exception {
    final Model model = createEmptyModel();
    final GenericProtein element = createProtein();
    model.addElement(element);
    final org.sbml.jsbml.Model sbmlModel = exporter.toSbmlDocument(model).getModel();

    final MultiModelPlugin multiPlugin = (MultiModelPlugin) sbmlModel.getExtension("multi");

    final org.sbml.jsbml.Species sbmlSpecies = sbmlModel.getSpecies(0);
    final MultiSpeciesPlugin speciesExtension = (MultiSpeciesPlugin) sbmlSpecies.getExtension("multi");
    assertNotNull("Multi extension not defined for species", speciesExtension);
    final String speciesTypeString = speciesExtension.getSpeciesType();

    MultiSpeciesType speciesType = null;
    for (final MultiSpeciesType type : multiPlugin.getListOfSpeciesTypes()) {
      if (type.getId().equals(speciesTypeString)) {
        speciesType = type;
      }
    }
    assertNotNull("Species type is not set in multi extension", speciesType);

  }

  @Test
  public void testMultiExtensionStructuralStateTypeDefinition() throws Exception {
    final Model model = createEmptyModel();
    final GenericProtein element = createProtein();
    final StructuralState structuralState = createStructuralState(element);
    element.addStructuralState(structuralState);
    model.addElement(element);
    final org.sbml.jsbml.Model sbmlModel = exporter.toSbmlDocument(model).getModel();

    final org.sbml.jsbml.Species sbmlSpecies = sbmlModel.getSpecies(0);
    final MultiSpeciesPlugin speciesExtension = (MultiSpeciesPlugin) sbmlSpecies.getExtension("multi");
    assertNotNull("Multi extension not defined for species", speciesExtension);
    assertFalse("structural state feature not defined in multi extension", speciesExtension.getListOfSpeciesFeatures().isEmpty());
  }

  @Test
  public void testExportResidue() throws Exception {
    final Model model = createEmptyModel();
    final GenericProtein element = createProtein();
    Residue mr = createResidue(element);
    mr.setName("217U");
    mr.setState(ModificationState.PHOSPHORYLATED);
    mr.setPosition(new Point2D.Double(10, 11));
    mr.setBorderColor(Color.YELLOW);
    mr.setZ(100);
    element.addResidue(mr);
    mr = createResidue(element);
    mr.setName("218");
    mr.setState(ModificationState.PHOSPHORYLATED);
    mr.setPosition(new Point2D.Double(10, 12));
    mr.setBorderColor(Color.BLUE);
    mr.setZ(102);
    element.addResidue(mr);
    mr = createResidue(element);
    mr.setName("219");
    mr.setState(ModificationState.UBIQUITINATED);
    mr.setPosition(new Point2D.Double(10, 13));
    mr.setBorderColor(Color.RED);
    mr.setZ(1000);
    element.addResidue(mr);
    model.addElement(element);
    final Model deserializedModel = getModelAfterSerializing(model);

    final GenericProtein protein = deserializedModel.getElementByElementId(element.getElementId());
    assertEquals("Residues weren't exported/imported properly", 3, protein.getModificationResidues().size());
  }

  @Test
  public void testExportNotes() throws Exception {
    final Model model = createEmptyModel();
    model.setNotes("XX");
    final Model deserializedModel = getModelAfterSerializing(model);

    assertEquals("Notes weren't exported/imported properly", model.getNotes(), deserializedModel.getNotes());

  }

  @Test
  public void testExportImportOfAdvancedInputOperator() throws Exception {
    final Model originalModel = new CellDesignerXmlParser().createModel(new ConverterParams()
        .filename("testFiles/cell_designer_problems/heterodimer_association_with_additional_reactant.xml"));
    final Model model = getModelAfterSerializing(originalModel);
    final Reaction r1 = originalModel.getReactions().iterator().next();
    final Reaction r2 = model.getReactions().iterator().next();
    final Product p1 = r1.getProducts().get(0);
    final Product p2 = r1.getProducts().get(0);
    final NodeOperator o1 = r1.getOperators().get(0);
    final NodeOperator o11 = r1.getOperators().get(1);
    final NodeOperator o2 = r2.getOperators().get(0);

    assertEquals(p1.getLine().length() + o1.getLine().length() + o11.getLine().length() + r1.getLine().length(),
        p2.getLine().length() + o2.getLine().length() + r2.getLine().length(), Configuration.EPSILON);

  }

  @Test
  public void testExportImportBooleanGate() throws Exception {
    final Model originalModel = new CellDesignerXmlParser()
        .createModel(new ConverterParams().filename("testFiles/cell_designer_problems/boolean_logic_gate.xml"));
    final Model model = getModelAfterSerializing(originalModel);

    final List<PolylineData> lines1 = new ArrayList<>();
    final List<PolylineData> lines2 = new ArrayList<>();

    for (final ReactionNode node : originalModel.getReactions().iterator().next().getReactionNodes()) {
      lines1.add(node.getLine());
    }
    for (final ReactionNode node : model.getReactions().iterator().next().getReactionNodes()) {
      lines2.add(node.getLine());
    }

    final ListComparator<PolylineData> comparator = new ListComparator<>(new PolylineDataComparator(Configuration.EPSILON));
    assertEquals(0, comparator.compare(lines1, lines2));
  }

  @Test
  public void testExportIronMetabolismReaction() throws Exception {
    final Model originalModel = new CellDesignerXmlParser()
        .createModel(new ConverterParams().filename("testFiles/cell_designer_problems/iron_metabolism_reaction.xml"));
    exporter.setProvideDefaults(false);
    final String xml = exporter.toXml(originalModel);
    assertNotNull(xml);
  }

  @Test
  public void testCreatorExport() throws Exception {
    final Model originalModel = parser.createModel(
        new ConverterParams().filename("testFiles/small/model_with_creator.xml"));
    final Model model = getModelAfterSerializing(originalModel);

    assertEquals(0, comparator.compare(model, originalModel));
  }

  @Test
  public void testInvalidSpeciesId() throws Exception {
    final Model model = createEmptyModel();

    final GenericProtein p = createProtein();
    p.setElementId("a-b");
    model.addElement(p);

    final String xml = exporter.toXml(model);
    assertNotNull(xml);

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testInvalidReactionId() throws Exception {
    final Model model = createEmptyModel();

    final Protein p1 = createProtein();
    final Protein p2 = createProtein();
    final Reaction p = createReaction(p1, p2);
    p.setIdReaction("a-b");
    model.addElement(p1);
    model.addElement(p2);
    model.addReaction(p);

    final String xml = exporter.toXml(model);
    assertNotNull(xml);
    assertEquals(1, getWarnings().size());

  }

  @Test
  public void testInvalidSpeciesIdInReaction() throws Exception {
    final Model model = createEmptyModel();

    final Protein p1 = createProtein();
    final Protein p2 = createProtein();
    final Reaction p = createReaction(p1, p2);
    p1.setElementId("a-b");
    model.addElement(p1);
    model.addElement(p2);
    model.addReaction(p);

    final String xml = exporter.toXml(model);
    assertNotNull(xml);
    assertEquals(1, getWarnings().size());

  }

  @Test
  public void testInvalidCompartmentId() throws Exception {
    final Model model = createEmptyModel();

    final Compartment p = createCompartment();
    p.setElementId("a-b");
    model.addElement(p);

    final String xml = exporter.toXml(model);
    assertNotNull(xml);

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testInvalidModelId() throws Exception {
    final Model model = createEmptyModel();
    model.setIdModel("a-b");

    final String xml = exporter.toXml(model);
    assertNotNull(xml);
    assertEquals(1, getWarnings().size());

  }

  @Test
  public void testExportReactionToCompartment() throws Exception {
    final Model model = new ModelFullIndexed(null);
    model.setWidth(200);
    model.setHeight(300);
    final Element protein = createProtein();
    final Element compartment = createCompartment();
    final Reaction reaction = createReaction(protein, compartment);

    model.addElement(protein);
    model.addElement(compartment);
    model.addReaction(reaction);

    final Model deserializedModel = getModelAfterSerializing(model);

    assertEquals(0, deserializedModel.getReactions().size());
    assertEquals(1, super.getWarnings().size());
  }

  @Test
  public void testExportMultiThreaded() throws Exception {
    final Model originalModel = getModelAfterSerializing("testFiles/small/small_molecule.xml");
    final Set<Thread> threads = new HashSet<>();
    final MutableBoolean exceptionHappened = new MutableBoolean(false);
    for (int i = 0; i < 20; i++) {
      final Thread thread = new Thread(() -> {
        try {
          new SbmlExporter().toXml(originalModel);
        } catch (final Exception e) {
          exceptionHappened.setTrue();
          logger.error(e);
        }
      });
      thread.start();
      threads.add(thread);
    }
    for (final Thread thread : threads) {
      thread.join();
    }

    assertFalse(exceptionHappened.booleanValue());
  }

  @Test
  public void testExportLayerText() throws Exception {
    final Model model = createEmptyModel();

    final Layer layer = new Layer();
    layer.setLayerId(1);
    layer.setName(CreateHierarchyCommand.TEXT_LAYER_NAME);
    final LayerText text = new LayerText(new Rectangle2D.Double(10, 20, 30, 40), "str a");
    text.setZ(0);
    layer.addLayerText(text);
    model.addLayer(layer);

    final Model deserializedModel = getModelAfterSerializing(model);

    assertEquals(0, comparator.compare(model, deserializedModel));
  }

  @Test
  public void testExportWithProblematicIds() throws Exception {
    final Model model = createEmptyModel();
    final GenericProtein p1 = createProtein();
    p1.setElementId("x-1");
    model.addElement(p1);
    final GenericProtein p2 = createProtein();
    p2.setElementId("SbmlSpeciesExporter_1");
    model.addElement(p2);

    exporter.toXml(model);

  }

  @Test
  public void testExportWithProblematicComplexIds() throws Exception {
    final Model model = createEmptyModel();
    final Complex complex = createComplex();
    complex.setElementId("x-1");
    model.addElement(complex);
    final GenericProtein protein = createProtein();
    protein.setElementId("p-1");
    complex.addSpecies(protein);
    model.addElement(protein);

    exporter.toXml(model);

  }

  @Test
  public void testExportWithCompartmentWithProblematicId() throws Exception {
    final Model model = createEmptyModel();

    final Compartment defaultCompartment = createCompartment();
    defaultCompartment.setElementId("default_compartment");
    model.addElement(defaultCompartment);

    exporter.toXml(model);

  }

  @Test
  public void testResidueSerialization() throws Exception {
    final Model originalModel = new CellDesignerXmlParser().createModel(new ConverterParams()
        .filename("testFiles/cd_for_multi/modification_residues.xml"));
    originalModel.setName("Unknown");

    final Model model2 = super.getModelAfterSerializing(originalModel);
    model2.setName("Unknown");

    assertNotNull(model2);
    final ModelComparator comparator = new ModelComparator(1.0);
    assertEquals(0, comparator.compare(originalModel, model2));
  }

  @Test
  public void testTranscriptionSiteSerialization() throws Exception {
    final Model originalModel = new CellDesignerXmlParser().createModel(new ConverterParams()
        .filename("testFiles/cd_for_multi/transcription_site.xml"));
    originalModel.setName("Unknown");

    final Model model2 = super.getModelAfterSerializing(originalModel);
    model2.setName("Unknown");

    assertNotNull(model2);
    final ModelComparator comparator = new ModelComparator(1.0);
    assertEquals(0, comparator.compare(originalModel, model2));

    final SbmlParser parser = new SbmlParser();
    parser.model2File(originalModel, "/tmp/transcription_site.xml");
  }

  @Test
  public void testModificationSiteSerialization() throws Exception {
    final Model originalModel = new CellDesignerXmlParser().createModel(new ConverterParams()
        .filename("testFiles/cd_for_multi/gene_with_modification_site.xml"));
    originalModel.setName("Unknown");

    final Model model2 = super.getModelAfterSerializing(originalModel);
    model2.setName("Unknown");

    assertNotNull(model2);
    final ModelComparator comparator = new ModelComparator(1.0);
    assertEquals(0, comparator.compare(originalModel, model2));
  }


  @Test
  public void testExportMultipleProteinState() throws Exception {
    final Model model = createEmptyModel();
    final GenericProtein element = createProtein();
    element.addStructuralState(createStructuralState(element));
    element.addStructuralState(createStructuralState(element));
    element.getModificationResidues().get(0).setX(123);
    model.addElement(element);
    final Model deserializedModel = getModelAfterSerializing(model);

    assertEquals("Structural state not exported/imported properly", 0, comparator.compare(model, deserializedModel));
  }
}
