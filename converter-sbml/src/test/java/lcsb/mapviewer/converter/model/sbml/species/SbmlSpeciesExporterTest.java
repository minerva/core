package lcsb.mapviewer.converter.model.sbml.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.converter.model.sbml.SbmlBioEntityExporter;
import lcsb.mapviewer.converter.model.sbml.SbmlExtension;
import lcsb.mapviewer.converter.model.sbml.SbmlTestFunctions;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.StructuralState;

public class SbmlSpeciesExporterTest extends SbmlTestFunctions {

  private SbmlSpeciesExporterTestHelper helper;

  @Before
  public void setUp() {
    helper = new SbmlSpeciesExporterTestHelper();
  }

  @Test
  public void testOneElementWithTwoAliases() throws InconsistentModelException {
    Element protein1 = createProtein();
    Element protein2 = createProtein();
    Model model = new ModelFullIndexed(null);
    model.addElement(protein1);
    model.addElement(protein2);

    SbmlBioEntityExporter<Species, org.sbml.jsbml.Species> exporter = helper.createExporter(model);

    exporter.exportElements();

    assertEquals(1, helper.sbmlModel.getSpeciesCount());
  }

  @Test
  public void testOneElementButInTwoCompartments() throws InconsistentModelException {
    Compartment compartment = createCompartment();
    compartment.setName("test");
    Element protein1 = createProtein();
    Element protein2 = createProtein();
    protein2.setCompartment(compartment);

    Model model = new ModelFullIndexed(null);
    model.addElement(protein1);
    model.addElement(protein2);
    model.addElement(compartment);

    SbmlBioEntityExporter<Species, org.sbml.jsbml.Species> exporter = helper.createExporter(model);

    exporter.exportElements();

    assertEquals(2, helper.sbmlModel.getSpeciesCount());
  }

  @Test
  public void testIdsOfSpeciesWithStructuralStates() throws InconsistentModelException {
    Species protein1 = createProtein();
    Protein protein2 = createProtein();
    StructuralState state = new StructuralState();
    state.setName("X");
    protein2.addStructuralState(state);
    Model model = new ModelFullIndexed(null);
    model.addElement(protein1);
    model.addElement(protein2);

    SbmlSpeciesExporter exporter = helper.createExporter(model, Arrays.asList(SbmlExtension.values()));

    assertFalse(exporter.getSbmlIdKey(protein1).equals(exporter.getSbmlIdKey(protein2)));
  }

}
