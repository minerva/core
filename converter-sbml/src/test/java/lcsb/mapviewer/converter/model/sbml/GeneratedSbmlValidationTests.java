package lcsb.mapviewer.converter.model.sbml;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.commands.layout.ApplySimpleLayoutModelCommand;
import lcsb.mapviewer.converter.ZIndexPopulator;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

@RunWith(Parameterized.class)
public class GeneratedSbmlValidationTests extends SbmlTestFunctions {

  private Model model;

  public GeneratedSbmlValidationTests(final Model model, final String name) {
    this.model = model;
  }

  @Parameters(name = "{index}: {1}")
  public static Collection<Object[]> data() throws Exception {
    List<Object[]> result = new ArrayList<>();
    result.add(createRow(createModelWithSingleSpecies()));
    result.add(createRow(createModelWithEmptySpeciesName()));
    result.add(createRow(createModelWithLayer()));

    return result;
  }

  private static Model createModelWithLayer() {
    Model model = createEmptyModel();
    model.setName("Model with layer text");
    Layer layer = new Layer();
    model.addLayer(layer);
    LayerText text = new LayerText(new Rectangle2D.Double(10, 20, 30, 40), "some funny text\nhaha");
    layer.addLayerText(text);
    return model;
  }

  private static Model createModelWithEmptySpeciesName() throws Exception {
    Model model = createModelWithSingleSpecies();
    model.setName("Species without name");
    model.getElements().iterator().next().setName("");
    return model;
  }

  private static Object[] createRow(final Model model) {
    new ZIndexPopulator().populateZIndex(model);
    return new Object[] { model, model.getName() };
  }

  private static Model createModelWithSingleSpecies() throws Exception {
    Model model = createEmptyModel();
    model.setName("Single species");
    Species species = new GenericProtein("id");
    species.setBoundaryCondition(null);
    species.setInitialConcentration(null);
    species.setInitialAmount(null);
    species.setConstant(null);
    model.addElement(species);
    new ApplySimpleLayoutModelCommand(model).execute();
    return model;
  }

  @Test
  @Ignore("sbml validatior is down")
  public void testIsValidSbml() throws Exception {
    SbmlParser parser = new SbmlParser();
    String xml = parser.model2String(model);

    validateSBML(xml, "xxx");
  }

}
