package lcsb.mapviewer.converter.model.sbml.reaction;

import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.sbo.SBOTermOperatorType;
import org.junit.Test;
import org.reflections.Reflections;

import java.util.Set;

import static org.junit.Assert.assertNotNull;

public class SBOTermOperatorTypeTest {

  @Test
  public void testSBOTermExistence() {
    Reflections reflections = new Reflections("lcsb.mapviewer.model.map.reaction");
    Set<Class<? extends NodeOperator>> classes = reflections.getSubTypesOf(NodeOperator.class);

    for (final Class<? extends NodeOperator> clazz : classes) {
      assertNotNull("No SBO term for operator: " + clazz.getName(), SBOTermOperatorType.getTermByType(clazz));
    }
  }

}
