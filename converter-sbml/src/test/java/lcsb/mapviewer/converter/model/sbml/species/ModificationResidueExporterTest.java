package lcsb.mapviewer.converter.model.sbml.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.reflections.Reflections;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.LayoutModelPlugin;
import org.sbml.jsbml.ext.render.LocalStyle;
import org.sbml.jsbml.ext.render.RenderLayoutPlugin;

import lcsb.mapviewer.converter.model.sbml.SbmlTestFunctions;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

@RunWith(Parameterized.class)
public class ModificationResidueExporterTest extends SbmlTestFunctions {

  private Model sbmlModel;

  private ModificationResidue mr;

  public ModificationResidueExporterTest(final String name, final ModificationResidue mr) {
    this.mr = mr;
  }

  @Before
  public void setUp() {
    SBMLDocument doc = new SBMLDocument(3, 1);
    sbmlModel = doc.createModel();
    LayoutModelPlugin plugin = new LayoutModelPlugin(sbmlModel);
    sbmlModel.addExtension("layout", plugin);
    Layout layout = new Layout();
    plugin.addLayout(layout);

    RenderLayoutPlugin renderPlugin = new RenderLayoutPlugin(layout);
    layout.addExtension("render", renderPlugin);
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws Exception {
    Protein protein = createProtein();

    Collection<Object[]> data = new ArrayList<Object[]>();
    Reflections reflections = new Reflections("lcsb.mapviewer.model.map");
    for (final Class<? extends ModificationResidue> class1 : reflections.getSubTypesOf(ModificationResidue.class)) {
      if (Modifier.isAbstract(class1.getModifiers())) {
        continue;
      }
      ModificationResidue mr = class1.getConstructor().newInstance();
      mr.setPosition(new Point2D.Double(10, 20));
      mr.setSpecies(protein);
      mr.setBorderColor(Color.YELLOW);
      mr.setFontSize(7);
      data.add(new Object[] { mr.getClass().getName(), mr });
    }
    return data;
  }

  @Test
  public void testCreateStyle() throws Exception {
    ModificationResidueExporter mre = new ModificationResidueExporter(sbmlModel);

    LocalStyle style = mre.createStyle(mr);
    assertNotNull(style);
    assertEquals("Problem with creating style for: " + mr.getClass(), 0, super.getWarnings().size());
    assertEquals(mr.getFontSize(), style.getGroup().getFontSize(), EPSILON);
  }

}
