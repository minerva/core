package lcsb.mapviewer.converter.model.sbml.compartment;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;

import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.sbml.SbmlParser;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;

public class SbmlCompartmentParserTest {

  private SbmlParser parser = new SbmlParser();

  @Test
  public void testCompartmentNAmePosition() throws FileNotFoundException, InvalidInputDataExecption {
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/small/compartment_with_text_position.xml"));
    Compartment element = model.getElementByElementId("ca1");
    assertEquals("Label position is too far from expected", element.getNameX(), 202.5, Configuration.EPSILON);
    assertEquals("Label position is too far from expected", element.getNameY(), 178.5, Configuration.EPSILON);
  }

}
