package lcsb.mapviewer.converter.model.sbml.units;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.sbml.jsbml.Unit.Kind;

import lcsb.mapviewer.converter.model.sbml.SbmlTestFunctions;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;

public class UnitMappingTest extends SbmlTestFunctions {

  @Test
  public void test() {
    for (Kind kind : Kind.values()) {
      if (!isAnnotatedBy(kind, Deprecated.class) && kind != Kind.INVALID) {
        SbmlUnitType type = UnitMapping.kindToUnitType(kind);
        assertNotNull(type);
      }
    }
  }

}
