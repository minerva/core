package lcsb.mapviewer.converter.model.sbml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.awt.Color;

import org.junit.Test;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.LayoutModelPlugin;
import org.sbml.jsbml.ext.render.ColorDefinition;
import org.sbml.jsbml.ext.render.GlobalRenderInformation;
import org.sbml.jsbml.ext.render.RenderLayoutPlugin;

import lcsb.mapviewer.model.graphics.ArrowType;

public class SbmlModelUtilsTest {

  @Test
  public void testEmptyGetRenderPlugin() {
    SBMLDocument doc = new SBMLDocument(3, 1);
    Model result = doc.createModel();
    LayoutModelPlugin plugin = new LayoutModelPlugin(result);
    result.addExtension("layout", plugin);
    plugin.addLayout(new Layout());

    SbmlModelUtils sbmlModelUtils = new SbmlModelUtils(result);

    assertNull(sbmlModelUtils.getRenderPlugin());
  }

  @Test
  public void testGetRenderPluginWithLayout() {
    Model result = createModelWithLayoutAndRender();

    SbmlModelUtils sbmlModelUtils = new SbmlModelUtils(result);

    assertNotNull(sbmlModelUtils.getRenderPlugin());
  }

  @Test
  public void testAddColors() {
    Model result = createModelWithLayoutAndRender();

    SbmlModelUtils sbmlModelUtils = new SbmlModelUtils(result);

    ColorDefinition c1 = sbmlModelUtils.getColorDefinition(Color.BLUE);
    ColorDefinition c2 = sbmlModelUtils.getColorDefinition(Color.BLUE);
    assertEquals(c1, c2);
  }

  @Test
  public void testCreateLineEndingStyle() {
    Model result = createModelWithLayoutAndRender();

    SbmlModelUtils sbmlModelUtils = new SbmlModelUtils(result);

    for (final ArrowType arrowType : ArrowType.values()) {
      assertNotNull("Line ending wasn't created for arrow type: " + arrowType,
          sbmlModelUtils.createLineEndingStyle(arrowType.name()));
    }
  }

  @SuppressWarnings("deprecation")
  private Model createModelWithLayoutAndRender() {
    SBMLDocument doc = new SBMLDocument(3, 1);
    Model result = doc.createModel();
    LayoutModelPlugin plugin = new LayoutModelPlugin(result);
    result.addExtension("layout", plugin);
    Layout layout = new Layout();
    plugin.addLayout(layout);

    RenderLayoutPlugin renderPlugin = new RenderLayoutPlugin(layout);
    renderPlugin.setRenderInformation(new GlobalRenderInformation());
    layout.addExtension("render", renderPlugin);
    return result;
  }

}
