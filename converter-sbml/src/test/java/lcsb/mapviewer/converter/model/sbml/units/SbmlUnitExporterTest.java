package lcsb.mapviewer.converter.model.sbml.units;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.sbml.jsbml.SBMLDocument;

import lcsb.mapviewer.converter.model.sbml.SbmlExporter;
import lcsb.mapviewer.converter.model.sbml.SbmlTestFunctions;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.model.Model;

public class SbmlUnitExporterTest extends SbmlTestFunctions {

  @Test
  public void testExportUnits() {
    Model model = super.createEmptyModel();
    model.addUnit(new SbmlUnit("su"));
    SbmlUnitExporter exporter = new SbmlUnitExporter(model);

    SBMLDocument doc = new SBMLDocument(SbmlExporter.SUPPORTED_VERSION, SbmlExporter.SUPPORTED_LEVEL);
    org.sbml.jsbml.Model sbmlModel = doc.createModel();

    exporter.exportUnits(sbmlModel);

    assertEquals(2, sbmlModel.getUnitDefinitionCount());
  }

}
