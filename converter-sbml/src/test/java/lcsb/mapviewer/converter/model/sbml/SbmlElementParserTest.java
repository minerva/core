package lcsb.mapviewer.converter.model.sbml;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.BoundingBox;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.LayoutModelPlugin;
import org.sbml.jsbml.ext.layout.SpeciesGlyph;

import lcsb.mapviewer.converter.model.sbml.species.SbmlSpeciesParser;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Protein;

public class SbmlElementParserTest extends SbmlTestFunctions {

  private SbmlElementParser<?> parser;

  @Before
  public void setUp() {
    Model sbmlModel = new Model();
    LayoutModelPlugin layoutPlugin = new LayoutModelPlugin(sbmlModel);
    Layout layout = new Layout();
    layout.setId("minerva_layout");
    layoutPlugin.add(layout);
    sbmlModel.addExtension("layout", layoutPlugin);

    parser = new SbmlSpeciesParser(sbmlModel, null);
  }

  @Test
  public void testCreateElementWithLayoutWithoutBox() throws Exception {
    Protein protein = super.createProtein();
    SpeciesGlyph glyph = new SpeciesGlyph();
    Element result = parser.createElementWithLayout(protein, glyph);
    assertNotNull(result);
  }

  @Test
  public void testCreateElementWithLayoutWithoutPosition() throws Exception {
    Protein protein = super.createProtein();
    SpeciesGlyph glyph = new SpeciesGlyph();
    glyph.setBoundingBox(new BoundingBox());
    Element result = parser.createElementWithLayout(protein, glyph);
    assertNotNull(result);
  }

}
