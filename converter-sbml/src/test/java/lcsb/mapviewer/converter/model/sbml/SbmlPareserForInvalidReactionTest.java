package lcsb.mapviewer.converter.model.sbml;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;

@RunWith(Parameterized.class)
public class SbmlPareserForInvalidReactionTest extends SbmlTestFunctions {


  private Path filePath;

  public SbmlPareserForInvalidReactionTest(final Path filePath) {
    this.filePath = filePath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<Object[]>();
    Files.walk(Paths.get("testFiles/invalidReaction")).forEach(filePath -> {
      if (Files.isRegularFile(filePath) && filePath.toString().endsWith(".xml")) {
        data.add(new Object[] { filePath });
      }
    });
    return data;
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void createModelTest() throws Exception {
    Converter converter = new SbmlParser();
    converter.createModel(new ConverterParams().filename(filePath.toString()));
  }

}
