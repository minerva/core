package lcsb.mapviewer.converter.model.sbml;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.tests.TestUtils;
import lcsb.mapviewer.common.tests.UnitTestFailedWatcher;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator;
import lcsb.mapviewer.converter.graphics.NormalImageGenerator;
import lcsb.mapviewer.converter.graphics.PngImageGenerator;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.geom.Point2D;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SbmlTestFunctions extends TestUtils {
  protected Logger logger = LogManager.getLogger();

  private static int identifierCounter = 0;

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();
  protected SbmlParser parser = new SbmlParser();
  protected SbmlExporter exporter = new SbmlExporter();

  protected static Model createEmptyModel() {
    final Model model = new ModelFullIndexed(null);
    model.setWidth(1000);
    model.setHeight(1000);
    model.setName("UNKNOWN DISEASE MAP");
    model.setIdModel("id" + (identifierCounter++));
    return model;
  }

  protected static GenericProtein createProtein() {
    final GenericProtein result = new GenericProtein("sa" + (identifierCounter++));
    result.setName("SNCA");
    assignCoordinates(10, 10, 10, 10, result);
    result.setNameHorizontalAlign(HorizontalAlign.LEFT);
    return result;
  }

  protected Complex createComplex() {
    final Complex result = new Complex("sa" + (identifierCounter++));
    result.setName("SNCA");
    assignCoordinates(10, 10, 10, 10, result);
    result.setNameHorizontalAlign(HorizontalAlign.LEFT);
    return result;
  }

  protected static Compartment createCompartment() {
    final Compartment result = new Compartment("c" + (identifierCounter++));
    result.setName("comp name");
    assignCoordinates(10, 10, 10, 10, result);
    result.setNameHorizontalAlign(HorizontalAlign.LEFT);

    return result;
  }

  private static int z = 1;

  protected static void assignCoordinates(final double x, final double y, final double width, final double height, final Element protein) {
    protein.setX(x);
    protein.setY(y);
    protein.setZ(z++);
    protein.setWidth(width);
    protein.setHeight(height);
    protein.setNameX(x);
    protein.setNameY(y);
    protein.setNameWidth(width);
    protein.setNameHeight(height);
    protein.setNameVerticalAlign(VerticalAlign.MIDDLE);
    protein.setNameHorizontalAlign(HorizontalAlign.CENTER);
  }

  protected void showImage(final Model model) throws Exception {
    final String dir = Files.createTempDirectory("sbml-temp-images-dir").toFile().getAbsolutePath();
    final AbstractImageGenerator.Params params = new AbstractImageGenerator.Params().height(model.getHeight())
        .width(model.getWidth()).nested(true).scale(1).level(20).x(0).y(0).model(model);
    final NormalImageGenerator nig = new PngImageGenerator(params);
    final String pathWithouExtension = dir + "/" + model.getName();
    final String pngFilePath = pathWithouExtension.concat(".png");
    nig.saveToFile(pngFilePath);
    Desktop.getDesktop().open(new File(pngFilePath));

  }

  protected Reaction createReaction(final Element p1, final Element p2) {
    final Reaction reaction = new StateTransitionReaction("r" + (identifierCounter++));
    final Reactant reactant = new Reactant(p1);
    final Product product = new Product(p2);

    final Point2D centerLineStart = new Point2D.Double((p1.getCenter().getX() + p2.getCenter().getX()) / 3,
        (p1.getCenter().getY() + p2.getCenter().getY()) / 3);
    final Point2D centerLineEnd = new Point2D.Double(2 * (p1.getCenter().getX() + p2.getCenter().getX()) / 3,
        2 * (p1.getCenter().getY() + p2.getCenter().getY()) / 3);

    final PolylineData reactantLine = new PolylineData(p1.getCenter(), centerLineStart);
    reactant.setLine(reactantLine);
    final PolylineData productLine = new PolylineData(centerLineEnd, p2.getCenter());
    product.setLine(productLine);
    reaction.addReactant(reactant);
    reaction.addProduct(product);
    reaction.setZ(125);

    final PolylineData centerLine = new PolylineData(centerLineStart, centerLineEnd);
    reaction.setLine(centerLine);
    return reaction;
  }

  protected Model getModelAfterSerializing(final Model originalModel) throws Exception {
    final String xml = exporter.toXml(originalModel);
    // logger.debug(xml);
    final ByteArrayInputStream stream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
    final Model result = parser.createModel(new ConverterParams().inputStream(stream));
    // showImage(originalModel);
    // showImage(result);
    return result;
  }

  protected Model getModelAfterCellDEsignerSerializing(final Model model) throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final String xmlString = parser.model2String(model);
    final InputStream is = new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8));

    final Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));
    for (final BioEntity bioEntity : model2.getBioEntities()) {
      bioEntity.setZ(0);
      if (bioEntity instanceof Element) {
        ((Element) bioEntity).setNameBorder(((Element) bioEntity).getBorder());
      }
    }
    for (final BioEntity bioEntity : model.getBioEntities()) {
      bioEntity.setZ(0);
      if (bioEntity instanceof Element) {
        ((Element) bioEntity).setNameBorder(((Element) bioEntity).getBorder());
      }
    }
    return model2;
  }

  protected void validateSBML(final String xmlContent, final String filename) throws IOException, InvalidXmlSchemaException {
    final CloseableHttpClient httpClient = HttpClients.createDefault();
    final HttpPost uploadFile = new HttpPost("http://sbml.org/validator/");
    final MultipartEntityBuilder builder = MultipartEntityBuilder.create();
    builder.addTextBody("file", xmlContent, ContentType.TEXT_PLAIN);
    builder.addBinaryBody(
        "file",
        new ByteArrayInputStream(xmlContent.getBytes(StandardCharsets.UTF_8)),
        ContentType.APPLICATION_OCTET_STREAM,
        filename);
    builder.addTextBody("output", "xml", ContentType.TEXT_PLAIN);
    builder.addTextBody("offcheck", "u,r", ContentType.TEXT_PLAIN);

    final HttpEntity multipart = builder.build();
    uploadFile.setEntity(multipart);
    final CloseableHttpResponse response = httpClient.execute(uploadFile);
    final String responseXml = EntityUtils.toString(response.getEntity());
    final Document document = XmlParser.getXmlDocumentFromString(responseXml);
    final List<Node> problems = XmlParser.getAllNotNecessirellyDirectChild("problem", document);
    if (problems.size() > 0) {
      logger.debug(responseXml);
    }
    assertEquals("SBML is invalid", 0, problems.size());
  }

  protected StructuralState createStructuralState(final Element element) {
    final StructuralState structuralState = new StructuralState("state" + (identifierCounter++));
    structuralState.setName("xxx" + (identifierCounter++));
    structuralState.setPosition(new Point2D.Double(element.getX(), element.getY() - 10));
    structuralState.setWidth(element.getWidth());
    structuralState.setHeight(20.0);
    structuralState.setFontSize(10.0);
    structuralState.setZ(element.getZ() + 2);
    structuralState.setBorderColor(Color.GREEN);
    return structuralState;
  }

  protected Residue createResidue(final Element element) {
    final Residue result = new Residue("id" + (identifierCounter++));
    result.setState(ModificationState.PHOSPHORYLATED);
    result.setPosition(new Point2D.Double(element.getX(), element.getY() - 10));
    result.setWidth(element.getWidth());
    result.setHeight(20.0);
    result.setFontSize(10.0);
    result.setZ(element.getZ() + 2);
    result.setBorderColor(Color.GREEN);
    return result;
  }

}
