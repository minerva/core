package lcsb.mapviewer.converter.model.sbml;

import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator;
import lcsb.mapviewer.converter.graphics.NormalImageGenerator;
import lcsb.mapviewer.converter.graphics.PngImageGenerator;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(Parameterized.class)
public class MultiParserTest extends SbmlTestFunctions {


  private final Path filePath;

  public MultiParserTest(final Path filePath) {
    this.filePath = filePath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    final Collection<Object[]> data = new ArrayList<Object[]>();
    Files.walk(Paths.get("testFiles/multi")).forEach(filePath -> {
      if (Files.isRegularFile(filePath) && filePath.toString().endsWith(".xml")) {
        data.add(new Object[]{filePath});
      }
    });
    return data;
  }

  @Test
  public void createModelTest() throws Exception {
    final String dir = Files.createTempDirectory("sbml-temp-images-dir").toFile().getAbsolutePath();

    final Converter converter = new SbmlParser();

    final Model model = converter.createModel(new ConverterParams().filename(filePath.toString()));
    model.setName("Unknown");

    // Create and display image of parsed map
    final AbstractImageGenerator.Params params = new AbstractImageGenerator.Params().height(model.getHeight())
        .width(model.getWidth()).nested(true).scale(1).level(20).x(0).y(0).model(model);
    final NormalImageGenerator nig = new PngImageGenerator(params);
    final String pathWithouExtension = dir + "/"
        + filePath.getFileName().toString().substring(0, filePath.getFileName().toString().indexOf(".xml"));
    final String pngFilePath = pathWithouExtension.concat(".png");
    nig.saveToFile(pngFilePath);

    final Model model2 = super.getModelAfterCellDEsignerSerializing(model);

    final AbstractImageGenerator.Params params2 = new AbstractImageGenerator.Params().height(model2.getHeight())
        .width(model2.getWidth()).nested(true).scale(1).level(20).x(0).y(0).model(model2);
    final NormalImageGenerator nig2 = new PngImageGenerator(params2);
    final String pngFilePath2 = pathWithouExtension.concat("_2.png");
    nig2.saveToFile(pngFilePath2);

    assertNotNull(model2);
    final ModelComparator comparator = new ModelComparator(1.0);
    assertEquals(0, comparator.compare(model, model2));
    FileUtils.deleteDirectory(new File(dir));
  }

}
