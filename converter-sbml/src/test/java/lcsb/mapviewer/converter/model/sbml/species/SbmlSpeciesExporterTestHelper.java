package lcsb.mapviewer.converter.model.sbml.species;

import java.util.Arrays;
import java.util.Collection;

import org.sbml.jsbml.SBMLDocument;

import lcsb.mapviewer.converter.model.sbml.SbmlExporter;
import lcsb.mapviewer.converter.model.sbml.SbmlExtension;
import lcsb.mapviewer.converter.model.sbml.compartment.SbmlCompartmentExporter;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.model.Model;

public class SbmlSpeciesExporterTestHelper {
  protected org.sbml.jsbml.Model sbmlModel;
  
  public SbmlSpeciesExporter createExporter(final Model model, final Collection<SbmlExtension> sbmlExtensions)
      throws InconsistentModelException {
    SBMLDocument doc = new SBMLDocument(3, 1);
    sbmlModel = doc.createModel(model.getIdModel());

    SbmlExporter sbmlExporter = new SbmlExporter();
    sbmlExporter.createSbmlLayout(model, sbmlModel);

    SbmlCompartmentExporter compartmentExporter = new SbmlCompartmentExporter(sbmlModel, model,
        Arrays.asList(SbmlExtension.values()));
    compartmentExporter.exportElements();

    SbmlSpeciesExporter result = new SbmlSpeciesExporter(sbmlModel, model, sbmlExtensions, compartmentExporter);
    return result;
  }

  public SbmlSpeciesExporter createExporter(final Model model) throws InconsistentModelException {
    return createExporter(model, Arrays.asList(new SbmlExtension[] { SbmlExtension.RENDER, SbmlExtension.LAYOUT }));
  }


}
