package lcsb.mapviewer.converter.model.sbml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.model.map.model.Model;

@RunWith(Parameterized.class)
public class SbmlValidationTests extends SbmlTestFunctions {

  private String filename;

  public SbmlValidationTests(final String filename) {
    this.filename = filename;
  }

  @Parameters(name = "{0}")
  public static Collection<Object[]> data() throws IOException {
    List<Object[]> result = new ArrayList<>();
    result.add(new Object[] { "testFiles/small/empty.xml" });
    result.add(new Object[] { "testFiles/small/problematic_notes.xml" });
    result.add(new Object[] { "testFiles/small/reaction/dissociation.xml" });
    result.add(new Object[] { "testFiles/cell_designer_problems/kinetics.xml" });
    return result;
  }

  @Test
  @Ignore("sbml validatior is down")
  public void testIsValidSbml() throws Exception {
    SbmlParser parser = new SbmlParser();
    Model model = parser.createModel(new ConverterParams().filename(filename));
    String xml = parser.model2String(model);
    validateSBML(xml, filename);
  }

}
