package lcsb.mapviewer.converter.model.sbml;

import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(Parameterized.class)
public class CellDesignerToMultiExportTest extends SbmlTestFunctions {

  private final Path filePath;

  public CellDesignerToMultiExportTest(final Path filePath) {
    this.filePath = filePath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    final Collection<Object[]> data = new ArrayList<Object[]>();
    Files.walk(Paths.get("testFiles/cd_for_multi")).forEach(filePath -> {
      if (Files.isRegularFile(filePath) && filePath.toString().endsWith(".xml")) {
        data.add(new Object[]{filePath});
      }
    });
    return data;
  }

  @Test
  public void createModelTest() throws Exception {
    final Converter converter = new CellDesignerXmlParser();

    final Model model = converter.createModel(new ConverterParams().filename(filePath.toString()));
    model.setName("Unknown");

    final Model model2 = super.getModelAfterSerializing(model);
    model2.setName("Unknown");

    assertNotNull(model2);
    final ModelComparator comparator = new ModelComparator(1.0);
    assertEquals(0, comparator.compare(model, model2));
  }

}
