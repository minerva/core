package lcsb.mapviewer.converter.model.sbml;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

@RunWith(Parameterized.class)
public class ElementPropertiesExport extends SbmlTestFunctions {

  private Model model;

  public ElementPropertiesExport(final String propertyName, final Model model) {
    this.model = model;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<>();

    data.add(createTestEntry("Empty", createProtein()));

    Species element = createProtein();
    element.setInitialConcentration(1.0);
    data.add(createTestEntry("Initial concentration", element));

    element = createProtein();
    element.setInitialAmount(2.0);
    data.add(createTestEntry("Initial amount", element));

    element = createProtein();
    element.setBoundaryCondition(true);
    data.add(createTestEntry("Boundary condition", element));

    element = createProtein();
    element.setConstant(true);
    data.add(createTestEntry("Constant", element));

    return data;
  }

  private static Object[] createTestEntry(final String string, final Element element) {
    Model result = new ModelFullIndexed(null);
    result.setIdModel("X");
    result.setWidth(200);
    result.setHeight(200);
    result.addElement(element);
    return new Object[] { string, result };
  }

  @Test
  public void createModelTest() throws Exception {
    SbmlExporter sbmlExporter = new SbmlExporter();
    sbmlExporter.setProvideDefaults(false);
    SbmlParser sbmlParser = new SbmlParser();
    String xmlString = sbmlExporter.toXml(model);

    InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

    Model model2 = sbmlParser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));
    model2.setName(model.getName());

    ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(model, model2));
  }

}
