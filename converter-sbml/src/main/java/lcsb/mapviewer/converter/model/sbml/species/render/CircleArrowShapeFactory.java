package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.converter.model.sbml.extension.render.CustomEllipse;
import lcsb.mapviewer.model.map.Drawable;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.RelAbsVector;

import java.util.Collections;
import java.util.List;

public class CircleArrowShapeFactory extends AShapeFactory<Drawable> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    final CustomEllipse ellipse = new CustomEllipse();
    ellipse.setCx(new RelAbsVector(0, 50));
    ellipse.setCy(new RelAbsVector(0, 50));
    ellipse.setRx(new RelAbsVector(0, 50));
    ellipse.setRy(new RelAbsVector(0, 50));
    ellipse.setFill(false);

    return Collections.singletonList(ellipse);
  }

}
