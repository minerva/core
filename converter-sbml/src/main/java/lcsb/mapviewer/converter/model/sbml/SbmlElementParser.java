package lcsb.mapviewer.converter.model.sbml;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.species.Element;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.AbstractReferenceGlyph;
import org.sbml.jsbml.ext.layout.BoundingBox;
import org.sbml.jsbml.ext.layout.Dimensions;
import org.sbml.jsbml.ext.layout.Point;
import org.sbml.jsbml.ext.layout.TextGlyph;
import org.sbml.jsbml.ext.render.LocalStyle;
import org.sbml.jsbml.ext.render.RenderGroup;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class SbmlElementParser<T extends org.sbml.jsbml.Symbol> extends SbmlBioEntityParser {

  private final Map<String, Element> elementBySbmlId = new HashMap<>();
  private final Map<String, String> sbmlIdByElementId = new HashMap<>();

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  public SbmlElementParser(final Model sbmlModel, final lcsb.mapviewer.model.map.model.Model minervaModel) {
    super(sbmlModel, minervaModel);
  }

  public List<Element> parseList() throws InvalidInputDataExecption {
    List<Element> result = new ArrayList<>();
    for (final T sbmlElement : getSbmlElementList()) {
      Element element = parse(sbmlElement);
      if (element != null) {
        result.add(element);
        elementBySbmlId.put(element.getElementId(), element);
      }
    }
    return result;
  }

  protected abstract ListOf<T> getSbmlElementList();

  public Element getAnyElementBySbmlElementId(final String id) {
    return elementBySbmlId.get(id);
  }

  public String getSbmlIdByElementId(final String compartmentId) {
    return sbmlIdByElementId.get(compartmentId);
  }

  protected List<Element> mergeLayout(final List<? extends Element> elements)
      throws InvalidInputDataExecption {
    Set<String> used = new HashSet<>();
    List<Element> result = new ArrayList<>();
    for (final Element species : elements) {
      elementBySbmlId.put(species.getElementId(), species);
    }

    for (Pair<String, AbstractReferenceGlyph> idGlyphPair : getGlyphs()) {
      String id = idGlyphPair.getLeft();
      Element source = elementBySbmlId.get(id);
      if (source == null) {
        throw new InvalidInputDataExecption("Layout contains invalid Species id: " + idGlyphPair.getLeft());
      }
      LocalStyle style = getStyleForGlyph(idGlyphPair.getRight());

      if (style != null) {
        applyStyleToElement(source, style);
      }

      used.add(source.getElementId());
      AbstractReferenceGlyph glyph = idGlyphPair.getRight();
      if (glyph.getId() == null || glyph.getId().equals("")) {
        throw new InvalidInputDataExecption("Glyph for Species " + idGlyphPair.getLeft() + " doesn't have id");
      }
      Element elementWithLayout = createElementWithLayout(source, glyph);
      getMinervaModel().addElement(elementWithLayout);
      result.add(elementWithLayout);
      elementBySbmlId.put(id, elementWithLayout);
      elementBySbmlId.put(elementWithLayout.getElementId(), elementWithLayout);
      sbmlIdByElementId.put(elementWithLayout.getElementId(), id);
    }
    for (final Element element : elements) {
      if (!used.contains(element.getElementId())) {
        logger.warn("Layout doesn't contain information about Element: " + element.getElementId());
        result.add(element);
      } else {
        getMinervaModel().removeElement(element);
      }
    }

    for (Pair<String, AbstractReferenceGlyph> idGlyphPair : getGlyphs()) {
      String id = idGlyphPair.getLeft();

      Element source = getMinervaModel().getElementByElementId(idGlyphPair.getRight().getId());
      if (source == null) {
        source = getAnyElementBySbmlElementId(id);
      }

      if (source == null) {
        throw new InvalidInputDataExecption("Layout contains invalid Species id: " + idGlyphPair.getLeft());
      }

      for (final TextGlyph textGlyph : getTextGlyphsByLayoutElementId(idGlyphPair.getRight())) {
        if (textGlyph.getBoundingBox() != null) {
          BoundingBox box = textGlyph.getBoundingBox();
          source.setNameX(box.getPosition().getX());
          source.setNameY(box.getPosition().getY());
          source.setNameWidth(box.getDimensions().getWidth());
          source.setNameHeight(box.getDimensions().getHeight());
          source.setNameHorizontalAlign(HorizontalAlign.CENTER);
          source.setNameVerticalAlign(VerticalAlign.MIDDLE);
          if (source instanceof Compartment) {
            source.setNameHorizontalAlign(HorizontalAlign.LEFT);
            source.setNameVerticalAlign(VerticalAlign.TOP);
          }
          LocalStyle style = getStyleForGlyph(textGlyph);
          if (style != null) {
            RenderGroup group = style.getGroup();

            if (group.isSetVTextAnchor()) {
              switch (group.getVTextAnchor()) {
                case BOTTOM:
                  source.setNameVerticalAlign(VerticalAlign.BOTTOM);
                  break;
                case MIDDLE:
                  source.setNameVerticalAlign(VerticalAlign.MIDDLE);
                  break;
                case TOP:
                  source.setNameVerticalAlign(VerticalAlign.TOP);
                  break;
                default:
                  logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, source),
                      group.getVTextAnchor() + " text alignment is not supported");
              }
            }
            if (group.isSetTextAnchor()) {
              switch (group.getTextAnchor()) {
                case START:
                  source.setNameHorizontalAlign(HorizontalAlign.LEFT);
                  break;
                case MIDDLE:
                  source.setNameHorizontalAlign(HorizontalAlign.CENTER);
                  break;
                case END:
                  source.setNameHorizontalAlign(HorizontalAlign.RIGHT);
                  break;
                default:
                  logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, source),
                      group.getTextAnchor() + " text alignment is not supported");
              }
            }
            if (group.isSetStroke()) {
              Color fontColor = getColorByColorDefinition(group.getStroke());
              source.setFontColor(fontColor);
            }
          }
          if (textGlyph.isSetText() && textGlyph.getText() != null && !textGlyph.getText().isEmpty()) {
            String name = textGlyph.getText();
            if (!name.equals(source.getName())) {
              if (source.getFullName() == null || source.getFullName().isEmpty()
                  || source.getFullName().equals(source.getName())) {
                source.setFullName(source.getName());
                source.setName(name);
              } else {
                logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, source),
                    "Glyph text ignored because name and full name already assigned");
              }
            }
          }
        }
      }
    }

    return result;
  }

  Element createElementWithLayout(final Element source, final AbstractReferenceGlyph glyph)
      throws InvalidInputDataExecption {
    Element elementWithLayout = source.copy();
    elementWithLayout.setElementId(glyph.getId());

    BoundingBox box = glyph.getBoundingBox();
    if (box == null) {
      logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, source), "Bounding box in glyph is undefined");
    } else {
      Point position = box.getPosition();
      if (position == null) {
        logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, source), "Position of glyph is not defined");
      } else {
        elementWithLayout.setX(position.getX());
        elementWithLayout.setY(position.getY());
        if (position.isSetZ()) {
          elementWithLayout.setZ((int) position.getZ());
        }
      }
      Dimensions dimension = box.getDimensions();
      if (dimension == null) {
        logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, source), "Dimension of glyph is not defined");
      } else {
        elementWithLayout.setWidth(dimension.getWidth());
        elementWithLayout.setHeight(dimension.getHeight());
      }
    }
    LocalStyle style = getStyleForGlyph(glyph);
    if (style != null) {
      applyStyleToElement(elementWithLayout, style);
    }
    return elementWithLayout;
  }

  protected void applyStyleToElement(final Element elementWithLayout, final LocalStyle style) {
    if (style.getGroup().isSetFill()) {
      Color backgroundColor = getColorByColorDefinition(style.getGroup().getFill());
      elementWithLayout.setFillColor(backgroundColor);
    }
    if (style.getGroup().isSetStroke()) {
      Color borderColor = getColorByColorDefinition(style.getGroup().getStroke());
      elementWithLayout.setBorderColor(borderColor);
      elementWithLayout.setFontColor(borderColor);
    }

    if (style.getGroup().isSetFontSize()) {
      elementWithLayout.setFontSize(style.getGroup().getFontSize());
    }
  }

  protected abstract List<Pair<String, AbstractReferenceGlyph>> getGlyphs();

  protected abstract Element parse(final T species) throws InvalidInputDataExecption;

}
