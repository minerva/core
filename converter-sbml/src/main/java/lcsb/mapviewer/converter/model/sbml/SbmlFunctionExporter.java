package lcsb.mapviewer.converter.model.sbml;

import org.sbml.jsbml.ASTNode;
import org.sbml.jsbml.FunctionDefinition;

import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.model.Model;

public class SbmlFunctionExporter {

  private Model minervaModel;

  public SbmlFunctionExporter(final lcsb.mapviewer.model.map.model.Model minervaModel) {
    this.minervaModel = minervaModel;
  }

  public void exportFunction(final org.sbml.jsbml.Model result) {
    for (final SbmlFunction unit : minervaModel.getFunctions()) {
      result.addFunctionDefinition(createFunction(unit));
    }
  }

  private FunctionDefinition createFunction(final SbmlFunction unit) {
    FunctionDefinition result = new FunctionDefinition();
    result.setName(unit.getName());
    String definition = unit.getDefinition();
    definition = definition.replace("lambda xmlns=\"http://www.sbml.org/sbml/level2/version4\"", "lambda");
    result.setMath(ASTNode.parseMathML(definition));
    result.setId(unit.getFunctionId());
    return result;
  }

}
