package lcsb.mapviewer.converter.model.sbml;

import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.model.map.MiriamData;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.AbstractNamedSBase;
import org.sbml.jsbml.util.StringTools;
import org.sbml.jsbml.xml.XMLNode;
import org.sbml.jsbml.xml.stax.SBMLReader;

import javax.xml.stream.XMLStreamException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * This utility class parses notes from SBML node and prepares escaped string
 * ready to use in SBML.
 *
 * @author Piotr Gawron
 */
public class NotesUtility {

  private static final Logger logger = LogManager.getLogger();

  static {
    try {
      // there is a bug in JSBML with initialization
      new SBMLReader().readNotes(StringTools.toXMLNotesString(""));
    } catch (final XMLStreamException e) {
      logger.error(e, e);
    }
  }

  /**
   * Extract notes from SBML node
   *
   * @param sbmlElement SBML node
   * @return notes
   * @throws InvalidInputDataExecption thrown when there is problem with extracting notes
   */
  public static String extractNotes(final AbstractNamedSBase sbmlElement) throws InvalidInputDataExecption {
    String notes = "";
    if (sbmlElement.getNotes() != null) {
      if (sbmlElement.getNotes().getChildCount() > 1) {
        if (sbmlElement.getNotes().getChild(1).getChildCount() > 1) {
          try {
            notes = sbmlElement.getNotes().getChild(1).getChild(1).toXMLString();
            notes = notes.trim();
            if (notes.equals("<p/>")) {
              notes = "";
            }
            if (notes.startsWith("<p>")) {
              notes = notes.substring(3);
            }
            if (notes.endsWith("</p>")) {
              notes = notes.substring(0, notes.length() - 4);
            }
          } catch (final Exception e) {
            if (sbmlElement.getNotes().getChild(1).getChild(1).getChildCount() > 0) {
              notes = sbmlElement.getNotes().getChild(1).getChild(1).getChild(0).getCharacters();
            } else {
              notes = sbmlElement.getNotes().getChild(1).getChild(1).getCharacters();
            }
          }
        }
      }
    }
    return notes;
  }

  /**
   * Prepares escaped xml string with notes.
   *
   * @param notes notes to be processed
   * @return escaped xml string with notes
   */
  public static String prepareEscapedXmlNotes(final String notes) {
    if (notes == null) {
      return "";
    }
    return StringEscapeUtils.escapeXml10(notes);
  }

  public static XMLNode getRdfNode(final Collection<MiriamData> data, final String metaId) throws XMLStreamException {
    XmlAnnotationParser parser = new XmlAnnotationParser(new ArrayList<>(), true);
    String rdf = parser.dataSetToXmlString(data, metaId);
    return getRdfNode(rdf);
  }

  public static XMLNode getRdfNode(final String rdf) throws XMLStreamException {
    return new SBMLReader().readNotes(StringTools.toXMLAnnotationString(rdf));
  }

  public static XMLNode prepareNotesNode(final String notes) throws XMLStreamException {
    String notesText = notes;
    if (notesText == null) {
      return new SBMLReader().readNotes(StringTools.toXMLNotesString(""));
    }
    try {
      notesText = "<body xmlns=\"http://www.w3.org/1999/xhtml\">\n<p>" + notesText + "</p>\n</body>";
      return new SBMLReader().readNotes(StringTools.toXMLNotesString(notesText));
    } catch (final XMLStreamException e) {
      notesText = "<body xmlns=\"http://www.w3.org/1999/xhtml\">\n<p>" + NotesUtility.prepareEscapedXmlNotes(notesText)
          + "</p>\n</body>";
      return new SBMLReader().readNotes(StringTools.toXMLNotesString(notesText));
    }
  }

}
