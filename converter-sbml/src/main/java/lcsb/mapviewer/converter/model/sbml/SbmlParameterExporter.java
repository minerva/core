package lcsb.mapviewer.converter.model.sbml;

import org.sbml.jsbml.Parameter;

import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.model.Model;

public class SbmlParameterExporter {

  private Model minervaModel;

  public SbmlParameterExporter(final lcsb.mapviewer.model.map.model.Model minervaModel) {
    this.minervaModel = minervaModel;
  }

  public void exportParameter(final org.sbml.jsbml.Model result) {
    for (final SbmlParameter parameter : minervaModel.getParameters()) {
      result.addParameter(createParameter(parameter));
    }
  }

  private Parameter createParameter(final SbmlParameter parameter) {
    Parameter result = new Parameter();
    result.setName(parameter.getName());
    result.setValue(parameter.getValue());
    result.setId(parameter.getParameterId());
    if (parameter.getUnits() != null) {
      result.setUnits(parameter.getUnits().getUnitId());
    }
    result.setConstant(true);
    return result;
  }

}
