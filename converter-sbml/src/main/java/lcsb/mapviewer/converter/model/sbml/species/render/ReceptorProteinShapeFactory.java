package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.model.map.species.ReceptorProtein;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;

import java.util.Collections;
import java.util.List;

public class ReceptorProteinShapeFactory extends AShapeFactory<ReceptorProtein> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    final Polygon polygon = new Polygon();
    createRelativePoint(polygon, 0, 0);
    createRelativePoint(polygon, 50, 20);
    createRelativePoint(polygon, 100, 0);
    createRelativePoint(polygon, 100, 80);
    createRelativePoint(polygon, 50, 100);
    createRelativePoint(polygon, 0, 80);
    return Collections.singletonList(polygon);
  }
}
