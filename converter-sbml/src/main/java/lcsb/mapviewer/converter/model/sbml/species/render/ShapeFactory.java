package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.sbml.extension.render.CustomEllipse;
import lcsb.mapviewer.converter.model.sbml.extension.render.CustomGraphicalPrimitive1D;
import lcsb.mapviewer.converter.model.sbml.extension.render.CustomRenderCubicBezier;
import lcsb.mapviewer.converter.model.sbml.extension.render.CustomRenderPoint;
import lcsb.mapviewer.converter.model.sbml.reaction.render.CatalysisReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.DissociationReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.HeterodimerAssociationReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.InhibitionReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.KnownTransitionOmittedReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.ModulationReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.NegativeInfluenceReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.PhysicalStimulationReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.PositiveInfluenceReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.ReducedModulationReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.ReducedPhysicalStimulationReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.ReducedTriggerReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.StateTransitionReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.TranscriptionReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.TranslationReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.TransportReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.TriggerReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.TruncationReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.UnknownCatalysisReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.UnknownInhibitionReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.UnknownNegativeInfluenceReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.UnknownPositiveInfluenceReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.UnknownReducedModulationReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.UnknownReducedPhysicalStimulationReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.UnknownReducedTriggerReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.reaction.render.UnknownTransitionReactionShapeFactory;
import lcsb.mapviewer.converter.model.sbml.species.render.sbgn.SbgnAntisenseRnaShapeFactory;
import lcsb.mapviewer.converter.model.sbml.species.render.sbgn.SbgnGeneShapeFactory;
import lcsb.mapviewer.converter.model.sbml.species.render.sbgn.SbgnGenericProteinShapeFactory;
import lcsb.mapviewer.converter.model.sbml.species.render.sbgn.SbgnIonChannelProteinShapeFactory;
import lcsb.mapviewer.converter.model.sbml.species.render.sbgn.SbgnReceptorProteinShapeFactory;
import lcsb.mapviewer.converter.model.sbml.species.render.sbgn.SbgnRnaShapeFactory;
import lcsb.mapviewer.converter.model.sbml.species.render.sbgn.SbgnSimpleMoleculeShapeFactory;
import lcsb.mapviewer.converter.model.sbml.species.render.sbgn.SbgnTruncatedProteinShapeFactory;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.map.reaction.type.CatalysisReaction;
import lcsb.mapviewer.model.map.reaction.type.DissociationReaction;
import lcsb.mapviewer.model.map.reaction.type.HeterodimerAssociationReaction;
import lcsb.mapviewer.model.map.reaction.type.InhibitionReaction;
import lcsb.mapviewer.model.map.reaction.type.KnownTransitionOmittedReaction;
import lcsb.mapviewer.model.map.reaction.type.ModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.NegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.PhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.PositiveInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedPhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedTriggerReaction;
import lcsb.mapviewer.model.map.reaction.type.TranscriptionReaction;
import lcsb.mapviewer.model.map.reaction.type.TranslationReaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.reaction.type.TriggerReaction;
import lcsb.mapviewer.model.map.reaction.type.TruncationReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownCatalysisReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownInhibitionReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownNegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownPositiveInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownReducedModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownReducedPhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownReducedTriggerReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownTransitionReaction;
import lcsb.mapviewer.model.map.sbo.SBOTermModificationResidueType;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;
import lcsb.mapviewer.model.map.sbo.SBOTermSpeciesType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.ext.render.Ellipse;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;
import org.sbml.jsbml.ext.render.RelAbsVector;
import org.sbml.jsbml.ext.render.RenderPoint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ShapeFactory {

  private final Logger logger = LogManager.getLogger();

  private final AntisenseRnaShapeFactory antisenseRnaShapeFactory = new AntisenseRnaShapeFactory();
  private final ComplexShapeFactory complexShapeFactory = new ComplexShapeFactory();
  private final DegradedShapeFactory degradedShapeFactory = new DegradedShapeFactory();
  private final DrugShapeFactory drugShapeFactory = new DrugShapeFactory();
  private final GeneShapeFactory geneShapeFactory = new GeneShapeFactory();
  private final GenericProteinShapeFactory genericProteinShapeFactory = new GenericProteinShapeFactory();
  private final IonChannelProteinShapeFactory ionChannelProteinShapeFactory = new IonChannelProteinShapeFactory();
  private final IonShapeFactory ionShapeFactory = new IonShapeFactory();
  private final PhenotypeShapeFactory phenotypeShapeFactory = new PhenotypeShapeFactory();
  private final ReceptorProteinShapeFactory receptorProteinShapeFactory = new ReceptorProteinShapeFactory();
  private final RnaShapeFactory rnaShapeFactory = new RnaShapeFactory();
  private final SimpleMoleculeShapeFactory simpleMoleculeShapeFactory = new SimpleMoleculeShapeFactory();
  private final TruncatedProteinShapeFactory truncatedProteinShapeFactory = new TruncatedProteinShapeFactory();
  private final UnknownShapeFactory unknownShapeFactory = new UnknownShapeFactory();

  private final SbgnAntisenseRnaShapeFactory sbgnAntisenseRnaShapeFactory = new SbgnAntisenseRnaShapeFactory();
  private final SbgnGeneShapeFactory sbgnGeneShapeFactory = new SbgnGeneShapeFactory();
  private final SbgnGenericProteinShapeFactory sbgnGenericProteinShapeFactory = new SbgnGenericProteinShapeFactory();
  private final SbgnIonChannelProteinShapeFactory sbgnIonChannelProteinShapeFactory = new SbgnIonChannelProteinShapeFactory();
  private final SbgnReceptorProteinShapeFactory sbgnReceptorProteinShapeFactory = new SbgnReceptorProteinShapeFactory();
  private final SbgnRnaShapeFactory sbgnRnaShapeFactory = new SbgnRnaShapeFactory();
  private final SbgnSimpleMoleculeShapeFactory sbgnSimpleMoleculeShapeFactory = new SbgnSimpleMoleculeShapeFactory();
  private final SbgnTruncatedProteinShapeFactory sbgnTruncatedProteinShapeFactory = new SbgnTruncatedProteinShapeFactory();

  private final ModificationSiteShapeFactory modificationSiteShapeFactory = new ModificationSiteShapeFactory();
  private final ResidueShapeFactory residueShapeFactory = new ResidueShapeFactory();
  private final StructuralStateShapeFactory structuralStateShapeFactory = new StructuralStateShapeFactory();
  private final RegulatoryRegionShapeFactory regulatoryRegionShapeFactory = new RegulatoryRegionShapeFactory();
  private final BindingRegionShapeFactory bindingRegionShapeFactory = new BindingRegionShapeFactory();
  private final CodingRegionShapeFactory codingRegionShapeFactory = new CodingRegionShapeFactory();
  private final ProteinBindingDomainShapeFactory proteinBindingDomainShapeFactory = new ProteinBindingDomainShapeFactory();
  private final TranscriptionSiteShapeFactory transcriptionSiteShapeFactory = new TranscriptionSiteShapeFactory();

  public List<GraphicalPrimitive1D> createShapeForSboTerm(final String sboTerm) {
    return this.createShapeForSboTerm(sboTerm, false);
  }

  public List<GraphicalPrimitive1D> createShapeForSboTerm(final String sboTerm, final boolean sbgn) {
    if (sbgn) {
      if (SBOTermSpeciesType.SIMPLE_MOLECULE.getSboTerms().contains(sboTerm)) {
        return sbgnSimpleMoleculeShapeFactory.getShapeDefinition();
      }
      if (SBOTermSpeciesType.RNA.getSboTerms().contains(sboTerm)) {
        return sbgnRnaShapeFactory.getShapeDefinition();
      }
      if (SBOTermSpeciesType.RECEPTOR.getSboTerms().contains(sboTerm)) {
        return sbgnReceptorProteinShapeFactory.getShapeDefinition();
      }
      if (SBOTermSpeciesType.GENE.getSboTerms().contains(sboTerm)) {
        return sbgnGeneShapeFactory.getShapeDefinition();
      }
      if (SBOTermSpeciesType.ANTISENSE_RNA.getSboTerms().contains(sboTerm)) {
        return sbgnAntisenseRnaShapeFactory.getShapeDefinition();
      }
      if (SBOTermSpeciesType.GENERIC_PROTEIN.getSboTerms().contains(sboTerm)) {
        return sbgnGenericProteinShapeFactory.getShapeDefinition();
      }
      if (SBOTermSpeciesType.TRUNCATED_PROTEIN.getSboTerms().contains(sboTerm)) {
        return sbgnTruncatedProteinShapeFactory.getShapeDefinition();
      }
      if (SBOTermSpeciesType.ION_CHANNEL.getSboTerms().contains(sboTerm)) {
        return sbgnIonChannelProteinShapeFactory.getShapeDefinition();
      }

    }
    if (SBOTermSpeciesType.ION.getSboTerms().contains(sboTerm)) {
      return ionShapeFactory.getShapeDefinition();
    }
    if (SBOTermSpeciesType.SIMPLE_MOLECULE.getSboTerms().contains(sboTerm)) {
      return simpleMoleculeShapeFactory.getShapeDefinition();
    }
    if (SBOTermSpeciesType.UNKNOWN.getSboTerms().contains(sboTerm)) {
      return unknownShapeFactory.getShapeDefinition();
    }
    if (SBOTermSpeciesType.RNA.getSboTerms().contains(sboTerm)) {
      return rnaShapeFactory.getShapeDefinition();
    }
    if (SBOTermSpeciesType.RECEPTOR.getSboTerms().contains(sboTerm)) {
      return receptorProteinShapeFactory.getShapeDefinition();
    }
    if (SBOTermSpeciesType.GENE.getSboTerms().contains(sboTerm)) {
      return geneShapeFactory.getShapeDefinition();
    }
    if (SBOTermSpeciesType.ANTISENSE_RNA.getSboTerms().contains(sboTerm)) {
      return antisenseRnaShapeFactory.getShapeDefinition();
    }
    if (SBOTermSpeciesType.GENERIC_PROTEIN.getSboTerms().contains(sboTerm)) {
      return genericProteinShapeFactory.getShapeDefinition();
    }
    if (SBOTermSpeciesType.TRUNCATED_PROTEIN.getSboTerms().contains(sboTerm)) {
      return truncatedProteinShapeFactory.getShapeDefinition();
    }
    if (SBOTermSpeciesType.ION_CHANNEL.getSboTerms().contains(sboTerm)) {
      return ionChannelProteinShapeFactory.getShapeDefinition();
    }
    if (SBOTermSpeciesType.COMPLEX.getSboTerms().contains(sboTerm) || SBOTermSpeciesType.HYPOTHETICAL_COMPLEX.getSboTerms().contains(sboTerm)) {
      return complexShapeFactory.getShapeDefinition();
    }
    if (SBOTermSpeciesType.DEGRADED.getSboTerms().contains(sboTerm)) {
      return degradedShapeFactory.getShapeDefinition();
    }
    if (SBOTermSpeciesType.PHENOTYPE.getSboTerms().contains(sboTerm)) {
      return phenotypeShapeFactory.getShapeDefinition();
    }
    if (SBOTermSpeciesType.DRUG.getSboTerms().contains(sboTerm)) {
      return drugShapeFactory.getShapeDefinition();
    }

    if (SBOTermModificationResidueType.RESIDUE.getSboTerms().contains(sboTerm)) {
      return residueShapeFactory.getShapeDefinition();
    }
    if (SBOTermModificationResidueType.MODIFICATION_SITE.getSboTerms().contains(sboTerm)) {
      return modificationSiteShapeFactory.getShapeDefinition();
    }
    if (SBOTermModificationResidueType.STRUCTURAL_STATE.getSboTerms().contains(sboTerm)) {
      return structuralStateShapeFactory.getShapeDefinition();
    }
    if (SBOTermModificationResidueType.REGULATORY_REGION.getSboTerms().contains(sboTerm)) {
      return regulatoryRegionShapeFactory.getShapeDefinition();
    }
    if (SBOTermModificationResidueType.BINDING_REGION.getSboTerms().contains(sboTerm)) {
      return bindingRegionShapeFactory.getShapeDefinition();
    }
    if (SBOTermModificationResidueType.CODING_REGION.getSboTerms().contains(sboTerm)) {
      return codingRegionShapeFactory.getShapeDefinition();
    }
    if (SBOTermModificationResidueType.PROTEIN_BINDING_DOMAIN.getSboTerms().contains(sboTerm)) {
      return proteinBindingDomainShapeFactory.getShapeDefinition();
    }
    if (SBOTermModificationResidueType.TRANSCRIPTION_SITE.getSboTerms().contains(sboTerm)) {
      return transcriptionSiteShapeFactory.getShapeDefinition();
    }

    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == CatalysisReaction.class) {
      return new CatalysisReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == DissociationReaction.class) {
      return new DissociationReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == HeterodimerAssociationReaction.class) {
      return new HeterodimerAssociationReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == InhibitionReaction.class) {
      return new InhibitionReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == KnownTransitionOmittedReaction.class) {
      return new KnownTransitionOmittedReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == ModulationReaction.class) {
      return new ModulationReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == NegativeInfluenceReaction.class) {
      return new NegativeInfluenceReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == PositiveInfluenceReaction.class) {
      return new PositiveInfluenceReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == PhysicalStimulationReaction.class) {
      return new PhysicalStimulationReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == ReducedModulationReaction.class) {
      return new ReducedModulationReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == ReducedPhysicalStimulationReaction.class) {
      return new ReducedPhysicalStimulationReactionShapeFactory().getShapeDefinition();
    }

    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == ReducedTriggerReaction.class) {
      return new ReducedTriggerReactionShapeFactory().getShapeDefinition();
    }
    if (sboTerm.equals(SBOTermReactionType.STATE_TRANSITION.getSBO())) {
      return new StateTransitionReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == TranscriptionReaction.class) {
      return new TranscriptionReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == TranslationReaction.class) {
      return new TranslationReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == TransportReaction.class) {
      return new TransportReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == TriggerReaction.class) {
      return new TriggerReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == TruncationReaction.class) {
      return new TruncationReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == UnknownCatalysisReaction.class) {
      return new UnknownCatalysisReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == UnknownInhibitionReaction.class) {
      return new UnknownInhibitionReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == UnknownNegativeInfluenceReaction.class) {
      return new UnknownNegativeInfluenceReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == UnknownPositiveInfluenceReaction.class) {
      return new UnknownPositiveInfluenceReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == UnknownReducedModulationReaction.class) {
      return new UnknownReducedModulationReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == UnknownReducedPhysicalStimulationReaction.class) {
      return new UnknownReducedPhysicalStimulationReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == UnknownReducedTriggerReaction.class) {
      return new UnknownReducedTriggerReactionShapeFactory().getShapeDefinition();
    }
    if (SBOTermReactionType.getTypeForSBOTerm(sboTerm) == UnknownTransitionReaction.class) {
      return new UnknownTransitionReactionShapeFactory().getShapeDefinition();
    }

    logger.warn("Shape for SBO term not implemented: {}. Using default", sboTerm);
    return geneShapeFactory.getShapeDefinition();
  }

  private List<GraphicalPrimitive1D> createSbmlShapeForElement(final String sboTerm, final double width, final double height) {
    final List<GraphicalPrimitive1D> shapes = new ArrayList<>(createShapeForSboTerm(sboTerm));

    for (int i = 0; i < shapes.size(); i++) {
      final GraphicalPrimitive1D shape = shapes.get(i);
      final GraphicalPrimitive1D sbmlShape = getPureSbmlShape(width, height, shape);
      shapes.set(i, sbmlShape);
    }
    return shapes;
  }

  public List<GraphicalPrimitive1D> createSbmlShapeForElement(final Element element) {
    return createSbmlShapeForElement(element.getSboTerm(), element.getWidth(), element.getHeight());
  }

  public List<GraphicalPrimitive1D> createSbmlShapeForElement(final ModificationResidue mr) {
    if (mr instanceof TranscriptionSite) { //this is a hack
      final List<GraphicalPrimitive1D> shapes = new ArrayList<>(transcriptionSiteShapeFactory.getShapeDefinition((TranscriptionSite) mr));
      shapes.replaceAll(graphicalPrimitive1D -> getPureSbmlShape(mr.getWidth(), mr.getHeight(), graphicalPrimitive1D));
      return shapes;
    }
    return createSbmlShapeForElement(mr.getSboTerm(), mr.getWidth(), mr.getHeight());
  }

  private static GraphicalPrimitive1D getPureSbmlShape(final double width, final double height, final GraphicalPrimitive1D shape) {
    GraphicalPrimitive1D result = shape;
    if (shape instanceof Polygon) {
      for (final RenderPoint point : ((Polygon) shape).getListOfElements()) {
        if (point instanceof CustomRenderPoint) {
          final CustomRenderPoint customRenderPoint = (CustomRenderPoint) point;
          if (customRenderPoint.getHeightRelative() != null) {
            final double absoluteX = Math.round(point.getX().getAbsoluteValue()
                + customRenderPoint.getHeightRelative() / 100 * height);
            final double relativeX = point.getX().getRelativeValue();
            point.setX(new RelAbsVector(absoluteX, relativeX));
          }
          if (customRenderPoint.getWidthRelative() != null) {
            final double absoluteY = Math.round(point.getY().getAbsoluteValue()
                + customRenderPoint.getWidthRelative() / 100 * width);
            final double relativeY = point.getY().getRelativeValue();
            point.setY(new RelAbsVector(absoluteY, relativeY));
          }
        } else if (point instanceof CustomRenderCubicBezier) {
          final CustomRenderCubicBezier bezierPoint = ((CustomRenderCubicBezier) point);
          if (bezierPoint.getHeightRelative() != null) {
            final double absoluteX = Math.round(bezierPoint.getX().getAbsoluteValue()
                + bezierPoint.getHeightRelative() / 100 * height);
            final double relativeX = bezierPoint.getX().getRelativeValue();
            bezierPoint.setX(new RelAbsVector(absoluteX, relativeX));
          }
          if (bezierPoint.getHeightRelative1() != null) {
            final double absoluteX = Math.round(bezierPoint.getX1().getAbsoluteValue()
                + bezierPoint.getHeightRelative1() / 100 * height);
            final double relativeX = bezierPoint.getX1().getRelativeValue();
            bezierPoint.setX1(new RelAbsVector(absoluteX, relativeX));
          }
          if (bezierPoint.getHeightRelative2() != null) {
            final double absoluteX = Math.round(bezierPoint.getX2().getAbsoluteValue()
                + bezierPoint.getHeightRelative2() / 100 * height);
            final double relativeX = bezierPoint.getX2().getRelativeValue();
            bezierPoint.setX2(new RelAbsVector(absoluteX, relativeX));
          }
          if (bezierPoint.getWidthRelative() != null) {
            throw new NotImplementedException();
          }
          if (bezierPoint.getWidthRelative1() != null) {
            throw new NotImplementedException();
          }
          if (bezierPoint.getWidthRelative2() != null) {
            throw new NotImplementedException();
          }
        } else if (point instanceof CustomGraphicalPrimitive1D) {
          throw new NotImplementedException();
        }
      }
    } else if (shape instanceof CustomEllipse) {
      final CustomEllipse customEllipse = ((CustomEllipse) shape);

      final Ellipse ellipse = new Ellipse(customEllipse);

      if (customEllipse.getCenterHeightRelative() != null) {
        final double absoluteCenterX = Math.round(customEllipse.getCx().getAbsoluteValue()
            + customEllipse.getCenterHeightRelative() / 100 * height);
        final double relativeCenterX = customEllipse.getCx().getRelativeValue();
        ellipse.setCx(new RelAbsVector(absoluteCenterX, relativeCenterX));
      }
      if (customEllipse.getCenterWidthRelative() != null) {
        final double absoluteCenterY = Math.round(customEllipse.getCy().getAbsoluteValue()
            + customEllipse.getCenterWidthRelative() / 100 * width);
        final double relativeCenterY = customEllipse.getCy().getRelativeValue();
        ellipse.setCy(new RelAbsVector(absoluteCenterY, relativeCenterY));
      }

      if (customEllipse.getRadiusHeightRelative() != null) {
        final double absoluteRadiusX = Math.round(customEllipse.getRx().getAbsoluteValue()
            + customEllipse.getRadiusHeightRelative() / 100 * height);
        final double relativeRadiusX = customEllipse.getRx().getRelativeValue();
        ellipse.setRx(new RelAbsVector(absoluteRadiusX, relativeRadiusX));
      }
      if (customEllipse.getRadiusWidthRelative() != null) {
        final double absoluteRadiusY = Math.round(customEllipse.getRy().getAbsoluteValue()
            + customEllipse.getRadiusWidthRelative() / 100 * width);
        final double relativeRadiusY = customEllipse.getRy().getRelativeValue();
        ellipse.setRy(new RelAbsVector(absoluteRadiusY, relativeRadiusY));
      }
      result = ellipse;
    } else if (shape instanceof CustomGraphicalPrimitive1D) {
      throw new NotImplementedException(shape.getClass().getSimpleName() + " is not implemented");
    }
    return result;
  }

  public Collection<? extends GraphicalPrimitive1D> createShapeForArrowType(final ArrowType arrowType) {
    switch (arrowType) {
      case FULL:
        return new FullArrowShapeFactory().getShapeDefinition();
      case CIRCLE:
        return new CircleArrowShapeFactory().getShapeDefinition();
      case NONE:
        return new NoneArrowShapeFactory().getShapeDefinition();
      case BLANK:
        return new BlankArrowShapeFactory().getShapeDefinition();
      case BLANK_CROSSBAR:
        return new BlankCrossbarArrowShapeFactory().getShapeDefinition();
      case CROSSBAR:
        return new CrossbarArrowShapeFactory().getShapeDefinition();
      case DIAMOND:
        return new DiamondArrowShapeFactory().getShapeDefinition();
      case OPEN:
        return new OpenArrowShapeFactory().getShapeDefinition();
      case FULL_CROSSBAR:
        return new FullCrossbarArrowShapeFactory().getShapeDefinition();
      default:
        throw new NotImplementedException();
    }
  }
}
