package lcsb.mapviewer.converter.model.sbml;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.species.Element;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.AbstractReferenceGlyph;
import org.sbml.jsbml.ext.layout.BoundingBox;
import org.sbml.jsbml.ext.layout.Dimensions;
import org.sbml.jsbml.ext.layout.Point;
import org.sbml.jsbml.ext.layout.TextGlyph;
import org.sbml.jsbml.ext.render.ColorDefinition;
import org.sbml.jsbml.ext.render.HTextAnchor;
import org.sbml.jsbml.ext.render.LocalStyle;
import org.sbml.jsbml.ext.render.RenderGroup;
import org.sbml.jsbml.ext.render.VTextAnchor;

import java.util.Collection;

public abstract class SbmlElementExporter<T extends Element, S extends org.sbml.jsbml.Symbol>
    extends SbmlBioEntityExporter<T, S> {

  private static final Logger logger = LogManager.getLogger();

  public SbmlElementExporter(final Model sbmlModel, final lcsb.mapviewer.model.map.model.Model minervaModel,
                             final Collection<SbmlExtension> sbmlExtensions) {
    super(sbmlModel, minervaModel, sbmlExtensions);
  }

  protected void assignLayoutToGlyph(final T element, final AbstractReferenceGlyph speciesGlyph) {
    BoundingBox boundingBox = new BoundingBox();
    boundingBox.setPosition(new Point(element.getX(), element.getY(), element.getZ()));
    Dimensions dimensions = new Dimensions();
    dimensions.setWidth(element.getWidth());
    dimensions.setHeight(element.getHeight());
    boundingBox.setDimensions(dimensions);
    speciesGlyph.setBoundingBox(boundingBox);

    if (isExtensionEnabled(SbmlExtension.RENDER)) {
      LocalStyle style = createStyle(element);

      assignStyleToGlyph(speciesGlyph, style);
    }

    String name = element.getName();
    if (name.isEmpty()) {
      logger.warn(new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, element), "Empty text label is not valid in SBML");
      name = " ";
    }

    TextGlyph textGlyph = getLayout().createTextGlyph("text_" + getElementId(element), name);
    double width = element.getNameWidth();
    double height = element.getNameHeight();
    double x = element.getNameX();
    double y = element.getNameY();
    textGlyph.setGraphicalObject(speciesGlyph.getId());

    if (isExtensionEnabled(SbmlExtension.RENDER)) {
      LocalStyle style = createStyle();
      style.getIDList().add(textGlyph.getId());
      RenderGroup group = style.getGroup();
      group.setVTextAnchor(VTextAnchor.MIDDLE);
      group.setTextAnchor(HTextAnchor.MIDDLE);
      switch (element.getNameHorizontalAlign()) {
        case CENTER:
          group.setTextAnchor(HTextAnchor.MIDDLE);
          break;
        case LEFT:
          group.setTextAnchor(HTextAnchor.START);
          break;
        case RIGHT:
          group.setTextAnchor(HTextAnchor.END);
          break;
        default:
          throw new NotImplementedException("Support of " + element.getNameHorizontalAlign() + " is not implemented");
      }
      switch (element.getNameVerticalAlign()) {
        case BOTTOM:
          group.setVTextAnchor(VTextAnchor.BOTTOM);
          break;
        case MIDDLE:
          group.setVTextAnchor(VTextAnchor.MIDDLE);
          break;
        case TOP:
          group.setVTextAnchor(VTextAnchor.TOP);
          break;
        default:
          throw new NotImplementedException("Support of " + element.getNameVerticalAlign() + " is not implemented");
      }
      group.setStroke(getColorDefinition(element.getFontColor()).getId());
    }
    textGlyph.setBoundingBox(createBoundingBox(x, y, element.getZ() + 1, width, height));

  }

  @Override
  protected LocalStyle createStyle(final T element) {
    LocalStyle result = createStyle();
    ColorDefinition color = getColorDefinition(element.getFillColor());
    result.getGroup().setFill(color.getId());
    result.getGroup().setFontSize(element.getFontSize().shortValue());

    return result;
  }

}
