package lcsb.mapviewer.converter.model.sbml.reaction.render;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.sbml.species.render.AShapeFactory;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.ReactionRect;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;
import org.sbml.jsbml.ext.render.RelAbsVector;
import org.sbml.jsbml.ext.render.RenderCubicBezier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class ReactionShapeFactory<T extends Reaction> extends AShapeFactory<Reaction> {

  protected List<GraphicalPrimitive1D> getShapeDefinition(final Class<T> clazz) {
    Reaction reaction;
    try {
      reaction = clazz.getConstructor(String.class).newInstance("temp_id");
    } catch (Exception e) {
      throw new NotImplementedException("Problem with shape for reaction type: " + clazz.getName());
    }
    if (reaction.getReactionRect() == null) {
      return getNullShapeDefinition();
    } else if (reaction.getReactionRect() == ReactionRect.RECT_BOLT) {
      return getBoltShapeDefinition();
    } else if (reaction.getReactionRect() == ReactionRect.RECT_EMPTY) {
      return getBlankShapeDefinition();
    } else if (reaction.getReactionRect() == ReactionRect.RECT_SLASH) {
      return getSlashShapeDefinition();
    } else if (reaction.getReactionRect() == ReactionRect.RECT_QUESTION) {
      return getQuestionShapeDefinition();
    } else {
      throw new NotImplementedException("Unknown reaction type: " + clazz.getName());
    }
  }

  private List<GraphicalPrimitive1D> getNullShapeDefinition() {
    final Polygon polygon = new Polygon();
    createRelativePoint(polygon, 0, 50);
    createRelativePoint(polygon, 100, 50);
    return Collections.singletonList(polygon);
  }

  private List<GraphicalPrimitive1D> getBlankShapeDefinition() {
    final Polygon polygon = new Polygon();
    createRelativePoint(polygon, 0, 0);
    createRelativePoint(polygon, 0, 100);
    createRelativePoint(polygon, 100, 100);
    createRelativePoint(polygon, 100, 0);
    return Collections.singletonList(polygon);
  }

  private List<GraphicalPrimitive1D> getSlashShapeDefinition() {
    List<GraphicalPrimitive1D> result = new ArrayList<>(getBlankShapeDefinition());

    final Polygon slash1 = new Polygon();
    createRelativePoint(slash1, 20, 80);
    createRelativePoint(slash1, 50, 20);
    result.add(slash1);

    final Polygon slash2 = new Polygon();
    createRelativePoint(slash2, 50, 80);
    createRelativePoint(slash2, 80, 20);
    result.add(slash2);

    return result;
  }

  private List<GraphicalPrimitive1D> getBoltShapeDefinition() {
    List<GraphicalPrimitive1D> result = new ArrayList<>(getBlankShapeDefinition());

    final Polygon bolt = new Polygon();

    createRelativePoint(bolt, 70, 10);
    createRelativePoint(bolt, 70, 70);
    createRelativePoint(bolt, 30, 30);
    createRelativePoint(bolt, 30, 90);
    createRelativePoint(bolt, 30, 30);
    createRelativePoint(bolt, 70, 70);
    result.add(bolt);

    return result;
  }

  private List<GraphicalPrimitive1D> getQuestionShapeDefinition() {
    List<GraphicalPrimitive1D> result = new ArrayList<>(getBlankShapeDefinition());

    // math taken from https://stackoverflow.com/a/44829356/1127920
    final double k2 = (4.0 / 3.0) * (Math.sqrt(2) - 1);

    final Polygon polygon = new Polygon();

    createAbsoluteRelativePoint(polygon,
        0, 35,
        0, 25);

    RenderCubicBezier bezierPoint = polygon.createRenderCubicBezier();
    bezierPoint.setX(new RelAbsVector(0, 50));
    bezierPoint.setY(new RelAbsVector(0, 10));

    bezierPoint.setX1(new RelAbsVector(0, 35));
    bezierPoint.setY1(new RelAbsVector(0, 10 + Math.round(15 * (1 - k2))));

    bezierPoint.setX2(new RelAbsVector(0, 35 + Math.round(15 * (1 - k2))));
    bezierPoint.setY2(new RelAbsVector(0, 10));

    createAbsoluteRelativePoint(polygon,
        0, 50,
        0, 10);

    bezierPoint = polygon.createRenderCubicBezier();
    bezierPoint.setX(new RelAbsVector(0, 65));
    bezierPoint.setY(new RelAbsVector(0, 25));

    bezierPoint.setX1(new RelAbsVector(0, 65 + Math.round(15 * (k2 - 1))));
    bezierPoint.setY1(new RelAbsVector(0, 10));

    bezierPoint.setX2(new RelAbsVector(0, 65));
    bezierPoint.setY2(new RelAbsVector(0, 10 + Math.round(15 * (1 - k2))));

    createAbsoluteRelativePoint(polygon, 0, 65,
        0, 25);

    bezierPoint = polygon.createRenderCubicBezier();
    bezierPoint.setX(new RelAbsVector(0, 50));
    bezierPoint.setY(new RelAbsVector(0, 40));

    bezierPoint.setX1(new RelAbsVector(0, 65));
    bezierPoint.setY1(new RelAbsVector(0, 40 + Math.round(15 * (k2 - 1))));

    bezierPoint.setX2(new RelAbsVector(0, 65 + Math.round(15 * (k2 - 1))));
    bezierPoint.setY2(new RelAbsVector(0, 40));

    createAbsoluteRelativePoint(polygon, 0, 50,
        0, 65);

    createAbsoluteRelativePoint(polygon, 0, 50,
        0, 40);

    bezierPoint = polygon.createRenderCubicBezier();
    bezierPoint.setX(new RelAbsVector(0, 65));
    bezierPoint.setY(new RelAbsVector(0, 25));

    bezierPoint.setX1(new RelAbsVector(0, 65 + Math.round(15 * (k2 - 1))));
    bezierPoint.setY1(new RelAbsVector(0, 40));

    bezierPoint.setX2(new RelAbsVector(0, 65));
    bezierPoint.setY2(new RelAbsVector(0, 40 + Math.round(15 * (k2 - 1))));

    bezierPoint = polygon.createRenderCubicBezier();
    bezierPoint.setX(new RelAbsVector(0, 50));
    bezierPoint.setY(new RelAbsVector(0, 10));

    bezierPoint.setX1(new RelAbsVector(0, 65));
    bezierPoint.setY1(new RelAbsVector(0, 10 + Math.round(15 * (1 - k2))));

    bezierPoint.setX2(new RelAbsVector(0, 65 + Math.round(15 * (k2 - 1))));
    bezierPoint.setY2(new RelAbsVector(0, 10));

    bezierPoint = polygon.createRenderCubicBezier();
    bezierPoint.setX(new RelAbsVector(0, 35));
    bezierPoint.setY(new RelAbsVector(0, 25));

    bezierPoint.setX1(new RelAbsVector(0, 35 + Math.round(15 * (1 - k2))));
    bezierPoint.setY1(new RelAbsVector(0, 10));

    bezierPoint.setX2(new RelAbsVector(0, 35));
    bezierPoint.setY2(new RelAbsVector(0, 10 + Math.round(15 * (1 - k2))));

    result.add(polygon);

    return result;
  }
}
