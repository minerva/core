package lcsb.mapviewer.converter.model.sbml;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.converter.model.sbml.species.ElementColorEnum;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.AbstractNamedSBase;
import org.sbml.jsbml.Annotation;
import org.sbml.jsbml.ext.SBasePlugin;
import org.sbml.jsbml.ext.layout.AbstractReferenceGlyph;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.LayoutModelPlugin;
import org.sbml.jsbml.ext.layout.TextGlyph;
import org.sbml.jsbml.ext.multi.MultiModelPlugin;
import org.sbml.jsbml.ext.render.LocalRenderInformation;
import org.sbml.jsbml.ext.render.LocalStyle;
import org.sbml.jsbml.ext.render.RenderLayoutPlugin;
import org.w3c.dom.Node;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SbmlBioEntityParser {

  /**
   * Default class logger.
   */
  private static final Logger logger = LogManager.getLogger();

  /**
   * SBML layout attached to parsed {@link #sbmlModel}.
   */
  private final Layout layout;

  /**
   * SBML model that we will parse;
   */
  private final org.sbml.jsbml.Model sbmlModel;

  /**
   * Our model to which we parse data.
   */
  private final lcsb.mapviewer.model.map.model.Model minervaModel;

  /**
   * Counter for identifiers that cannot be inferred from SBML.
   */
  private int idCounter = 0;
  private Map<String, List<TextGlyph>> textGlyphByElementId = null;

  /**
   * Default constructor.
   *
   * @param sbmlModel    SBML model that we are parsing
   * @param minervaModel Minerva model to which we are extracting data
   */
  public SbmlBioEntityParser(final org.sbml.jsbml.Model sbmlModel, final Model minervaModel) {
    super();
    this.sbmlModel = sbmlModel;
    this.minervaModel = minervaModel;
    this.layout = getSbmlLayout(sbmlModel);
  }

  protected Set<MiriamData> extractMiriamDataFromAnnotation(final Annotation annotation, final LogMarker marker) throws InvalidInputDataExecption {
    XmlAnnotationParser parser = new XmlAnnotationParser(new ArrayList<>(), true);
    Set<MiriamData> result = new HashSet<>();
    try {
      Node rdfNode = getRdfNodeFromAnnotation(annotation);
      if (rdfNode != null) {
        result.addAll(parser.parseRdfNode(rdfNode, marker));
      }
    } catch (final InvalidXmlSchemaException e) {
      throw new InvalidInputDataExecption(e);
    }
    return result;
  }

  private Node getRdfNodeFromAnnotation(final Annotation annotation) throws InvalidXmlSchemaException {
    if (!annotation.getFullAnnotationString().isEmpty()) {
      Node node = XmlParser.getXmlDocumentFromString(annotation.getFullAnnotationString());
      Node annotationNode = XmlParser.getNode("annotation", node);
      Node rdfNode = XmlParser.getNode("rdf:RDF", annotationNode);
      return rdfNode;
    }
    return null;
  }

  protected List<Author> extractAuthorsFromAnnotation(final Annotation annotation) throws InvalidInputDataExecption {
    XmlAnnotationParser parser = new XmlAnnotationParser(new ArrayList<>(), true);
    List<Author> result = new ArrayList<>();
    try {
      Node rdfNode = getRdfNodeFromAnnotation(annotation);
      if (rdfNode != null) {
        result.addAll(parser.getAuthorsFromRdf(rdfNode));
      }
    } catch (final InvalidXmlSchemaException e) {
      throw new InvalidInputDataExecption(e);
    }
    return result;
  }

  protected List<Calendar> extractModificationDatesFromAnnotation(final Annotation annotation)
      throws InvalidInputDataExecption {
    XmlAnnotationParser parser = new XmlAnnotationParser(new ArrayList<>(), true);
    List<Calendar> result = new ArrayList<>();
    try {
      Node rdfNode = getRdfNodeFromAnnotation(annotation);
      if (rdfNode != null) {
        result.addAll(parser.getModificationDatesFromRdf(rdfNode));
      }
    } catch (final InvalidXmlSchemaException e) {
      throw new InvalidInputDataExecption(e);
    }
    return result;
  }

  protected Calendar extractCreationDateFromAnnotation(final Annotation annotation) throws InvalidInputDataExecption {
    XmlAnnotationParser parser = new XmlAnnotationParser(new ArrayList<>(), true);
    try {
      Node rdfNode = getRdfNodeFromAnnotation(annotation);
      if (rdfNode != null) {
        return parser.getCreateDateFromRdf(rdfNode);
      }
    } catch (final InvalidXmlSchemaException e) {
      throw new InvalidInputDataExecption(e);
    }
    return null;
  }

  protected void assignBioEntityData(final AbstractNamedSBase sbmlElement, final BioEntity result)
      throws InvalidInputDataExecption {
    result.addMiriamData(extractMiriamDataFromAnnotation(sbmlElement.getAnnotation(), new LogMarker(ProjectLogEntryType.PARSING_ISSUE, result)));
    result.setName(sbmlElement.getName());
    String notes = NotesUtility.extractNotes(sbmlElement);
    result.setNotes(notes);

    if (result instanceof Element) {
      Element element = (Element) result;
      element.setFillColor(ElementColorEnum.getColorByClass(element.getClass()));
      if (result instanceof Compartment) {
        element.setBorderColor(ElementColorEnum.getColorByClass(element.getClass()));
      }
    }
  }

  protected String getNextId() {
    return (idCounter++) + "";
  }

  /**
   * Returns {@link LocalStyle} from the layout data identified by glyph id.
   *
   * @return {@link LocalStyle} from the layout data
   */
  protected LocalStyle getStyleForGlyph(final AbstractReferenceGlyph glyph) {
    RenderLayoutPlugin renderPlugin = (RenderLayoutPlugin) layout.getExtension("render");
    if (renderPlugin != null) {
      for (final LocalRenderInformation lri : renderPlugin.getListOfLocalRenderInformation()) {
        for (final LocalStyle style : lri.getListOfLocalStyles()) {
          if (style.getIDList().contains(glyph.getId())) {
            return style;
          }
        }
      }
    }
    return null;
  }

  protected Color getColorByColorDefinition(final String fill) {
    RenderLayoutPlugin renderPlugin = (RenderLayoutPlugin) layout.getExtension("render");
    for (final LocalRenderInformation lri : renderPlugin.getListOfLocalRenderInformation()) {
      if (lri.getColorDefinition(fill) != null) {
        return lri.getColorDefinition(fill).getValue();
      }
    }
    return null;
  }

  /**
   * Returns list of {@link TextGlyph} that describes {@link AbstractReferenceGlyph}.
   */
  protected List<TextGlyph> getTextGlyphsByLayoutElementId(final AbstractReferenceGlyph glyph) {
    Layout layout = getLayout();
    if (textGlyphByElementId == null) {
      textGlyphByElementId = new HashMap<>();
      if (layout != null) {
        for (final TextGlyph textGlyph : layout.getListOfTextGlyphs()) {
          String id = textGlyph.getGraphicalObject();
          if (id == null || id.isEmpty()) {
            id = textGlyph.getReference();
          }
          if (textGlyphByElementId.get(id) == null) {
            textGlyphByElementId.put(id, new ArrayList<>());
          }
          textGlyphByElementId.get(id).add(textGlyph);
        }
      }
    }
    List<TextGlyph> result = textGlyphByElementId.get(glyph.getId());
    if (result == null) {
      result = new ArrayList<>();
      textGlyphByElementId.put(glyph.getId(), result);
    }
    return result;
  }

  protected lcsb.mapviewer.model.map.model.Model getMinervaModel() {
    return minervaModel;
  }

  protected org.sbml.jsbml.Model getSbmlModel() {
    return sbmlModel;
  }

  protected Layout getLayout() {
    return layout;
  }

  private Layout getSbmlLayout(final org.sbml.jsbml.Model sbmlModel) {
    Layout layout = null;

    if (sbmlModel.getExtensionCount() > 0) {
      for (final SBasePlugin plugin : sbmlModel.getExtensionPackages().values()) {
        if (plugin.getClass().equals(org.sbml.jsbml.ext.layout.LayoutModelPlugin.class)) {
          LayoutModelPlugin layoutPlugin = (LayoutModelPlugin) plugin;
          if (layoutPlugin.getLayoutCount() == 0) {
            logger.warn("Layout plugin available but no layouts defined");
          } else if (layoutPlugin.getLayoutCount() > 1) {
            logger.warn(layoutPlugin.getLayoutCount() + " layouts defined. Using first one.");
            layout = layoutPlugin.getLayout(0);
          } else {
            layout = layoutPlugin.getLayout(0);
          }
        }
      }
    }
    return layout;
  }

  protected MultiModelPlugin getMultiPlugin() {
    return (MultiModelPlugin) sbmlModel.getExtension("multi");
  }

}