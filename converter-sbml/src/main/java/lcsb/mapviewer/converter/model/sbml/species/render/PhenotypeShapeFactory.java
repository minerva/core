package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.converter.model.sbml.extension.render.CustomRenderPoint;
import lcsb.mapviewer.model.map.species.Phenotype;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;
import org.sbml.jsbml.ext.render.RelAbsVector;
import org.sbml.jsbml.ext.render.RenderPoint;

import java.util.Collections;
import java.util.List;

public class PhenotypeShapeFactory extends AShapeFactory<Phenotype> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    final Polygon polygon = new Polygon();

    final CustomRenderPoint p1 = new CustomRenderPoint();
    p1.setX(new RelAbsVector(0, 0));
    p1.setY(new RelAbsVector(0, 0));
    p1.setHeightRelative(50);

    final CustomRenderPoint p2 = new CustomRenderPoint();
    p2.setX(new RelAbsVector(0, 100));
    p2.setY(new RelAbsVector(0, 0));
    p2.setHeightRelative(-50);

    final RenderPoint p3 = new RenderPoint();
    p3.setX(new RelAbsVector(0, 100));
    p3.setY(new RelAbsVector(0, 50));

    final CustomRenderPoint p4 = new CustomRenderPoint();
    p4.setX(new RelAbsVector(0, 100));
    p4.setY(new RelAbsVector(0, 100));
    p4.setHeightRelative(-50);

    final CustomRenderPoint p5 = new CustomRenderPoint();
    p5.setX(new RelAbsVector(0, 0));
    p5.setY(new RelAbsVector(0, 100));
    p5.setHeightRelative(50);

    final RenderPoint p6 = new RenderPoint();
    p6.setX(new RelAbsVector(0, 0));
    p6.setY(new RelAbsVector(0, 50));

    polygon.addElement(p1);
    polygon.addElement(p2);
    polygon.addElement(p3);
    polygon.addElement(p4);
    polygon.addElement(p5);
    polygon.addElement(p6);

    return Collections.singletonList(polygon);
  }

}
