package lcsb.mapviewer.converter.model.sbml.extension.render;

import org.sbml.jsbml.ext.render.RenderCubicBezier;

public class CustomRenderCubicBezier extends RenderCubicBezier implements CustomGraphicalPrimitive1D {

  private Double heightRelative;
  private Double widthRelative;

  private Double heightRelative1;
  private Double widthRelative1;

  private Double heightRelative2;
  private Double widthRelative2;

  public Double getHeightRelative() {
    return heightRelative;
  }

  public void setHeightRelative(final double heightRelative) {
    this.heightRelative = heightRelative;
  }

  public Double getWidthRelative() {
    return widthRelative;
  }

  public void setWidthRelative(final double widthRelative) {
    this.widthRelative = widthRelative;
  }

  public Double getHeightRelative1() {
    return heightRelative1;
  }

  public void setHeightRelative1(final double heightRelative1) {
    this.heightRelative1 = heightRelative1;
  }

  public Double getWidthRelative1() {
    return widthRelative1;
  }

  public void setWidthRelative1(final double widthRelative1) {
    this.widthRelative1 = widthRelative1;
  }

  public Double getHeightRelative2() {
    return heightRelative2;
  }

  public void setHeightRelative2(final double heightRelative2) {
    this.heightRelative2 = heightRelative2;
  }

  public Double getWidthRelative2() {
    return widthRelative2;
  }

  public void setWidthRelative2(final double widthRelative2) {
    this.widthRelative2 = widthRelative2;
  }
}
