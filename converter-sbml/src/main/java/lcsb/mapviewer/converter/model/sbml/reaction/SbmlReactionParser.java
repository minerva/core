package lcsb.mapviewer.converter.model.sbml.reaction;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.celldesigner.geometry.ReactionCellDesignerConverter;
import lcsb.mapviewer.converter.model.celldesigner.types.ModifierType;
import lcsb.mapviewer.converter.model.sbml.SbmlBioEntityParser;
import lcsb.mapviewer.converter.model.sbml.SbmlModelUtils;
import lcsb.mapviewer.converter.model.sbml.SbmlParameterParser;
import lcsb.mapviewer.converter.model.sbml.compartment.SbmlCompartmentParser;
import lcsb.mapviewer.converter.model.sbml.species.SbmlSpeciesParser;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.ArrowTypeData;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.kinetics.SbmlArgument;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.modifier.Inhibition;
import lcsb.mapviewer.model.map.modifier.Modulation;
import lcsb.mapviewer.model.map.modifier.Trigger;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.InvalidReactionParticipantNumberException;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.sbo.SBOTermModifierType;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.KineticLaw;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ModifierSpeciesReference;
import org.sbml.jsbml.SpeciesReference;
import org.sbml.jsbml.ext.layout.BoundingBox;
import org.sbml.jsbml.ext.layout.Curve;
import org.sbml.jsbml.ext.layout.CurveSegment;
import org.sbml.jsbml.ext.layout.LineSegment;
import org.sbml.jsbml.ext.layout.Point;
import org.sbml.jsbml.ext.layout.ReactionGlyph;
import org.sbml.jsbml.ext.layout.SpeciesGlyph;
import org.sbml.jsbml.ext.layout.SpeciesReferenceGlyph;
import org.sbml.jsbml.ext.render.LocalStyle;
import org.sbml.jsbml.ext.render.RenderGroup;
import org.w3c.dom.Node;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SbmlReactionParser extends SbmlBioEntityParser {
  private static Logger logger = LogManager.getLogger();

  private ElementUtils eu = new ElementUtils();

  private SbmlSpeciesParser speciesParser;
  private SbmlCompartmentParser compartmentParser;

  private PointTransformation pt = new PointTransformation();

  public SbmlReactionParser(final Model sbmlModel, final lcsb.mapviewer.model.map.model.Model minervaModel,
                            final SbmlSpeciesParser speciesParser, final SbmlCompartmentParser compartmentParser) {
    super(sbmlModel, minervaModel);
    this.speciesParser = speciesParser;
    this.compartmentParser = compartmentParser;
  }

  public List<Reaction> parseList() throws InvalidInputDataExecption {
    List<Reaction> result = new ArrayList<>();
    for (org.sbml.jsbml.Reaction sbmlElement : getSbmlElementList()) {
      result.add(parse(sbmlElement));
    }
    return result;
  }

  protected ListOf<org.sbml.jsbml.Reaction> getSbmlElementList() {
    return getSbmlModel().getListOfReactions();
  }

  public void mergeLayout(final Collection<Reaction> reactions)
      throws InvalidInputDataExecption {
    Set<Reaction> used = new HashSet<>();
    Map<String, Reaction> reactionById = new HashMap<>();
    for (final Reaction reaction : reactions) {
      if (reactionById.get(reaction.getElementId()) != null) {
        throw new InvalidInputDataExecption("Duplicated reaction id: " + reaction.getElementId());
      }
      reactionById.put(reaction.getElementId(), reaction);
    }

    for (final ReactionGlyph glyph : getLayout().getListOfReactionGlyphs()) {
      Reaction source = reactionById.get(glyph.getReaction());
      if (source == null) {
        throw new InvalidInputDataExecption("Layout contains invalid Species id: " + glyph.getReaction());
      }
      used.add(source);
      try {
        Map<ReactionNode, SpeciesReferenceGlyph> glyphByNode = new HashMap<>();
        Reaction reactionWithLayout = source.copy();
        // getId doesn't have to be unique, therefore we concatenate with
        // reaction
        reactionWithLayout.setIdReaction(glyph.getReaction() + "__" + glyph.getId());

        // z index
        if (glyph.getCurve() != null
            && glyph.getCurve().getCurveSegmentCount() > 0
            && glyph.getCurve().getCurveSegment(0).getStart() != null
            && glyph.getCurve().getCurveSegment(0).getStart().isSetZ()) {

          reactionWithLayout.setZ((int) glyph.getCurve().getCurveSegment(0).getStart().getZ());
        }
        if (reactionWithLayout.getZ() == null) {
          if (glyph.getBoundingBox() != null && glyph.getBoundingBox().getPosition() != null
              && glyph.getBoundingBox().getPosition().isSetZ()) {
            reactionWithLayout.setZ((int) glyph.getBoundingBox().getPosition().getZ());
          }
        }
        Set<Pair<Element, ReactionNode>> usedReactionNodes = new HashSet<>();
        for (final SpeciesReferenceGlyph speciesRefernceGlyph : glyph.getListOfSpeciesReferenceGlyphs()) {
          SpeciesGlyph speciesGlyph = getLayout().getSpeciesGlyph(speciesRefernceGlyph.getSpeciesGlyph());
          ReactionNode minervaNode = null;
          Class<? extends ReactionNode> nodeClass = getReactionNodeClass(speciesRefernceGlyph);

          if (reactionWithLayout.isReversible() && (nodeClass == Reactant.class || nodeClass == Product.class)) {
            nodeClass = null;
          }
          for (final ReactionNode node : reactionWithLayout.getReactionNodes()) {
            if (node.getElement().getElementId().equals(speciesGlyph.getSpecies())) {
              if (nodeClass == null) {
                minervaNode = node;
                nodeClass = node.getClass();
              } else if (node.getClass().isAssignableFrom(nodeClass)) {
                minervaNode = node;
              } else if (nodeClass.isAssignableFrom(node.getClass())) {
                minervaNode = node;
                nodeClass = node.getClass();
              }
            }
          }

          if (minervaNode == null) {
            for (Pair<Element, ReactionNode> entry : usedReactionNodes) {
              ReactionNode node = entry.getRight();
              if (entry.getLeft().getElementId().equals(speciesGlyph.getSpecies())) {
                if (nodeClass == null) {
                  minervaNode = node;
                  nodeClass = node.getClass();
                } else if (node.getClass().isAssignableFrom(nodeClass)) {
                  minervaNode = node;
                } else if (nodeClass.isAssignableFrom(node.getClass())) {
                  minervaNode = node;
                  nodeClass = node.getClass();
                }
              }
            }
            if (minervaNode != null) {
              logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, minervaNode),
                  "Reaction node exists more than once in layout");
            }
          }

          if (nodeClass == Modifier.class) {
            nodeClass = Modulation.class;
          }
          if (minervaNode == null) {
            throw new InvalidInputDataExecption(
                "Cannot find reaction node for layouted reaction: " + speciesGlyph.getSpecies() + ", " + glyph.getId());
          }
          glyphByNode.put(minervaNode, speciesRefernceGlyph);
          usedReactionNodes.add(new Pair<>(minervaNode.getElement(), minervaNode));
          Element minervaElement = getMinervaModel().getElementByElementId(speciesGlyph.getId());
          if (minervaElement == null) {
            throw new InvalidInputDataExecption("Cannot find layouted reaction node for layouted reaction: "
                + speciesGlyph.getId() + ", " + glyph.getId());
          }

          PolylineData line = getLineFromReferenceGlyph(speciesRefernceGlyph);
          if (line.length() == 0) {
            logger.warn("Line undefined for element " + minervaElement.getElementId()
                + " in reaction " + reactionWithLayout.getElementId());
          }
          if (minervaNode instanceof Reactant) {
            // in SBML line goes from reaction to reactant
            line = line.reverse();
            if (reactionWithLayout.isReversible()) {
              line.getBeginAtd().setArrowType(ArrowType.FULL);
            }
          }
          if (minervaNode instanceof Product) {
            ArrowTypeData atd = new ArrowTypeData();
            atd.setArrowType(ArrowType.FULL);
            line.setEndAtd(atd);
          } else if (minervaNode instanceof Modifier) {
            for (final ModifierType mt : ModifierType.values()) {
              if (mt.getClazz().equals(nodeClass)) {
                line.setEndAtd(mt.getAtd());
                line.setType(mt.getLineType());
              }
            }
          }
          updateKinetics(reactionWithLayout, minervaElement.getElementId(), minervaNode.getElement().getElementId());
          if (minervaElement.getCompartment() != null) {
            String newCompartmentId = minervaElement.getCompartment().getElementId();
            String oldCompartmentId = compartmentParser.getSbmlIdByElementId(newCompartmentId);
            updateKinetics(reactionWithLayout, newCompartmentId, oldCompartmentId);
          }
          minervaNode.setLine(line);
          minervaNode.setElement(minervaElement);
          if (nodeClass != minervaNode.getClass()) {
            reactionWithLayout.removeNode(minervaNode);

            try {
              ReactionNode newNode = nodeClass.getConstructor(Element.class).newInstance(minervaElement);
              newNode.setLine(line);
              reactionWithLayout.addNode(newNode);
            } catch (final InstantiationException | IllegalAccessException | IllegalArgumentException
                           | InvocationTargetException | NoSuchMethodException | SecurityException e) {
              throw new InvalidStateException(e);
            }
          }
        }

        if (reactionWithLayout.getReactants().size() > 1 && reactionWithLayout.getProducts().size() > 0) {
          NodeOperator operator = createInputOperator(reactionWithLayout, glyph);
          reactionWithLayout.addNode(operator);
        }
        if (reactionWithLayout.getReactants().size() > 0 && reactionWithLayout.getProducts().size() > 1) {
          NodeOperator operator = createOutputOperator(reactionWithLayout, glyph);
          reactionWithLayout.addNode(operator);
        }
        reactionWithLayout.setLine(extractCurve(glyph, reactionWithLayout, null));
        reactionWithLayout.setProcessCoordinates(extractProcessCoordinates(glyph));
        createCenterLineForReaction(reactionWithLayout);
        assignRenderDataToReaction(glyph, reactionWithLayout);

        for (final ReactionNode reactionNode : reactionWithLayout.getReactionNodes()) {
          SpeciesReferenceGlyph speciesGlyph = glyphByNode.get(reactionNode);
          if (speciesGlyph != null) {
            assignRenderDataToReactionNode(speciesGlyph, reactionNode, reactionNode instanceof Reactant);
          }
        }

        getMinervaModel().addReaction(reactionWithLayout);

      } catch (final Exception e) {
        throw new InvalidInputDataExecption(
            new ElementUtils().getElementTag(source) + "Problem with parsing reaction layout", e);
      }
    }
    Set<Reaction> elementsToRemove = new HashSet<>();
    for (final Reaction reaction : reactions) {
      if (!used.contains(reaction)) {
        for (final ReactionNode node : reaction.getReactionNodes()) {
          // we might have different elements here, the reason is that single
          // SBML species
          // can be split into two or more (due to layout)
          node.setElement(speciesParser.getAnyElementBySbmlElementId(node.getElement().getElementId()));
        }
      } else {
        elementsToRemove.add(reaction);
      }
    }
    for (final Reaction reaction : elementsToRemove) {
      getMinervaModel().removeReaction(reaction);
    }
  }

  private Point2D extractProcessCoordinates(final ReactionGlyph glyph) {
    if (glyph.getCurve() != null && glyph.getBoundingBox() != null) {
      BoundingBox box = glyph.getBoundingBox();
      if (box.getDimensions() != null && box.getPosition() != null) {
        return new Point2D.Double(box.getPosition().getX() + box.getDimensions().getWidth() / 2,
            box.getPosition().getY() + box.getDimensions().getHeight() / 2);
      }
    }
    return null;

  }

  /**
   * Creates center line for assembled reaction with operators.
   */
  private void createCenterLineForReaction(final Reaction reaction) {
    if (reaction.getLine().length() == 0) {
      AbstractNode startNode = reaction.getReactants().get(0);
      AbstractNode endNode = reaction.getProducts().get(0);

      for (final NodeOperator operator : reaction.getOperators()) {
        if (operator.isReactantOperator()) {
          startNode = operator;
        }
      }

      for (final NodeOperator operator : reaction.getOperators()) {
        if (operator.isProductOperator()) {
          endNode = operator;
        }
      }

      PolylineData line = new PolylineData();
      reaction.setLine(line);
      // if there is no layout don't create center line
      if (startNode.getLine() != null && startNode.getLine().length() > Configuration.EPSILON
          && endNode.getLine() != null && endNode.getLine().length() > Configuration.EPSILON) {
        startNode.getLine().trimEnd(ReactionCellDesignerConverter.RECT_SIZE);
        endNode.getLine().trimBegin(ReactionCellDesignerConverter.RECT_SIZE);

        line.addLine(pt.copyPoint(startNode.getLine().getEndPoint()), pt.copyPoint(endNode.getLine().getStartPoint()));
        line.setType(startNode.getLine().getType());
        line.setColor(startNode.getLine().getColor());
      }
    }
  }

  private void assignRenderDataToReactionNode(final SpeciesReferenceGlyph glyph, final ReactionNode modifier, final boolean reverse)
      throws InvalidInputDataExecption {
    LocalStyle style = getStyleForGlyph(glyph);
    if (style != null) {
      applyStyleToReactionNode(modifier, style, reverse);
    }
  }

  private void applyStyleToReactionNode(final ReactionNode modifier, final LocalStyle style, final boolean reverse) {
    RenderGroup group = style.getGroup();
    if (group.isSetStroke()) {
      Color color = getColorByColorDefinition(group.getStroke());
      modifier.getLine().setColor(color);
    }
    if (group.isSetStrokeWidth()) {
      modifier.getLine().setWidth(group.getStrokeWidth());
    }
    if (group.isSetStrokeDashArray()) {
      LineType type = LineType.getTypeByDashArray(group.getStrokeDashArray());
      modifier.getLine().setType(type);
    }
    if (group.isSetEndHead()) {
      try {
        ArrowType type = ArrowType.valueOf(group.getEndHead().replaceAll("line_ending_", ""));
        if (reverse) {
          modifier.getLine().getBeginAtd().setArrowType(type);
        } else {
          modifier.getLine().getEndAtd().setArrowType(type);
        }
      } catch (final Exception e) {
        logger.warn(new ElementUtils().getElementTag(modifier.getReaction()) + "Problematic arrow type: "
            + group.getStroke(), e);
      }
    }
  }

  private NodeOperator createOutputOperator(final Reaction reactionWithLayout, final ReactionGlyph reactionGlyph) {
    NodeOperator operator = new AndOperator();
    PolylineData line = extractCurve(reactionGlyph, reactionWithLayout, false);

    operator.addOutputs(reactionWithLayout.getProducts());
    operator.setLine(line.reverse());
    return operator;
  }

  private NodeOperator createInputOperator(final Reaction reactionWithLayout, final ReactionGlyph reactionGlyph) {
    NodeOperator operator = new AndOperator();
    PolylineData line = extractCurve(reactionGlyph, reactionWithLayout, true);
    operator.addInputs(reactionWithLayout.getReactants());
    operator.setLine(line);
    return operator;
  }

  /**
   * Extracts {@link Curve} associated to {@link ReactionGlyph}. If curve
   * doesn't exist then method will try to create it from {@link BoundingBox}
   * (due to SBML specification). This {@link Curve} is split into parts
   * belonging to:
   * <ul>
   * <li>input operator</li>
   * <li>center line</li>
   * <li>output operator</li>
   * </ul>
   * . {@link PolylineData} corresponding to the proper part is returned.
   */
  PolylineData extractCurve(final ReactionGlyph reactionGlyph, final Reaction reaction, final Boolean input) {
    Curve curve = getCurve(reactionGlyph);
    if (curve == null) {
      curve = new Curve(getSbmlModel().getLevel(), getSbmlModel().getVersion());
      logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, reaction), "Invalid curve line for reaction (node: "
          + reactionGlyph.getId() + ")");
    }
    if (curve.getCurveSegmentCount() == 0) {
      return new PolylineData();
    }

    int requiredLines = 1;
    if (reaction.getReactants().size() > 1) {
      requiredLines++;
    }
    if (reaction.getProducts().size() > 1) {
      requiredLines++;
    }
    int existingLines = curve.getCurveSegmentCount();

    if (requiredLines == 3 && existingLines == 1) {
      CurveSegment segment = curve.getCurveSegment(0);
      Point secondPoint = getPointOnSegment(segment, 0.4);
      Point thirdPoint = getPointOnSegment(segment, 0.6);

      LineSegment curveSegment = new LineSegment(getSbmlModel().getLevel(), getSbmlModel().getVersion());
      curveSegment.setStart(new Point(secondPoint));
      curveSegment.setEnd(new Point(thirdPoint));
      curve.addCurveSegment(curveSegment);

      curveSegment = new LineSegment(getSbmlModel().getLevel(), getSbmlModel().getVersion());
      curveSegment.setStart(new Point(thirdPoint));
      curveSegment.setEnd(new Point(segment.getEnd()));
      curve.addCurveSegment(curveSegment);

      segment.setEnd(new Point(secondPoint));
    } else if (requiredLines == 3 && existingLines == 2) {
      if (getSegmentLength(curve.getCurveSegment(0)) > getSegmentLength(curve.getCurveSegment(1))) {
        CurveSegment segment = curve.getCurveSegment(0);
        Point centerPoint = getCenter(segment);
        LineSegment curveSegment = new LineSegment(getSbmlModel().getLevel(), getSbmlModel().getVersion());
        curveSegment.setStart(new Point(centerPoint));
        curveSegment.setEnd(new Point(segment.getEnd()));

        curve.addCurveSegment(1, curveSegment);
        segment.setEnd(new Point(centerPoint));
      } else {
        CurveSegment segment = curve.getCurveSegment(1);
        Point centerPoint = getCenter(segment);
        LineSegment curveSegment = new LineSegment(getSbmlModel().getLevel(), getSbmlModel().getVersion());
        curveSegment.setStart(new Point(centerPoint));
        curveSegment.setEnd(new Point(segment.getEnd()));

        curve.addCurveSegment(curveSegment);
        segment.setEnd(new Point(centerPoint));

      }
    } else if (requiredLines == 2 && existingLines == 1) {
      CurveSegment segment = curve.getCurveSegment(0);
      Point centerPoint = getCenter(segment);
      LineSegment curveSegment = new LineSegment(getSbmlModel().getLevel(), getSbmlModel().getVersion());
      curveSegment.setStart(new Point(centerPoint));
      curveSegment.setEnd(new Point(segment.getEnd()));

      curve.addCurveSegment(curveSegment);

      segment.setEnd(new Point(centerPoint));
    }
    existingLines = curve.getCurveSegmentCount();

    Integer reactantLineEnds;
    Integer productLineStarts;
    if (requiredLines == 3) {
      reactantLineEnds = existingLines / 2;
      productLineStarts = reactantLineEnds + 1;
    } else if (reaction.getReactants().size() > 1) {
      reactantLineEnds = existingLines - 1;
      productLineStarts = reactantLineEnds + 1;
    } else if (reaction.getProducts().size() > 1) {
      reactantLineEnds = 0;
      productLineStarts = reactantLineEnds + 1;
    } else if (requiredLines == 1) {
      reactantLineEnds = 0;
      productLineStarts = existingLines;
    } else {
      throw new InvalidArgumentException(
          new ElementUtils().getElementTag(reaction) + "Reaction has no products and reactants");
    }

    PolylineData result = new PolylineData();
    if (input == null) {
      // Center line
      for (int i = reactantLineEnds; i < productLineStarts; i++) {
        CurveSegment segment = curve.getCurveSegment(i);
        result.addLine(segment.getStart().getX(), segment.getStart().getY(), segment.getEnd().getX(), segment.getEnd().getY());
      }
    } else if (!input) {
      // Product operator

      for (int i = productLineStarts; i < curve.getCurveSegmentCount(); i++) {
        CurveSegment segment = curve.getCurveSegment(i);
        result.addLine(segment.getStart().getX(), segment.getStart().getY(),
            segment.getEnd().getX(), segment.getEnd().getY());
      }
    } else {
      // Reactant operator
      for (int i = 0; i < reactantLineEnds; i++) {
        CurveSegment segment = curve.getCurveSegment(i);
        result.addLine(segment.getStart().getX(), segment.getStart().getY(), segment.getEnd().getX(), segment.getEnd().getY());
      }
    }
    return result;
  }

  private double getSegmentLength(final CurveSegment curveSegment) {
    double dx = curveSegment.getStart().getX() - curveSegment.getEnd().getX();
    double dy = curveSegment.getStart().getY() - curveSegment.getEnd().getY();
    return Math.sqrt(dx * dx + dy * dy);
  }

  private Point getCenter(final CurveSegment segment) {
    return getPointOnSegment(segment, 0.5);
  }

  Point getPointOnSegment(final CurveSegment segment, final double coef) {
    double centerX = segment.getStart().getX() + (segment.getEnd().getX() - segment.getStart().getX()) * coef;
    double centerY = segment.getStart().getY() + (segment.getEnd().getY() - segment.getStart().getY()) * coef;
    Point centerPoint = new Point(getSbmlModel().getLevel(), getSbmlModel().getVersion());
    centerPoint.setX(centerX);
    centerPoint.setY(centerY);
    return centerPoint;
  }

  Curve getCurve(final ReactionGlyph reactionGlyph) {
    Curve curve = reactionGlyph.getCurve();
    if (curve == null) {
      BoundingBox box = reactionGlyph.getBoundingBox();
      if (box != null && box.getDimensions() != null && box.getPosition() != null) {
        curve = new Curve(getSbmlModel().getLevel(), getSbmlModel().getVersion());
        if (box.getDimensions().getWidth() > 0 || box.getDimensions().getHeight() > 0) {
          LineSegment curveSegment = new LineSegment(getSbmlModel().getLevel(), getSbmlModel().getVersion());
          Point start = new Point(getSbmlModel().getLevel(), getSbmlModel().getVersion());
          start.setX(box.getPosition().getX());
          start.setY(box.getPosition().getY());
          start.setZ(box.getPosition().getZ());
          curveSegment.setStart(new Point(start));
          curveSegment.setEnd(new Point(box.getPosition().getX() + box.getDimensions().getWidth(),
              box.getPosition().getY() + box.getDimensions().getHeight()));
          curve.addCurveSegment(curveSegment);
        }
      } else {
        curve = null;
      }
    }
    if (curve != null) {
      curve = new Curve(curve);
      for (int i = curve.getChildCount() - 1; i >= 0; i--) {
        Point start = curve.getCurveSegment(i).getStart();
        Point end = curve.getCurveSegment(i).getEnd();
        if (start.equals(end)) {
          curve.removeCurveSegment(i);
        }
      }
    }
    return curve;
  }

  private Class<? extends ReactionNode> getReactionNodeClass(final SpeciesReferenceGlyph speciesRefernceGlyph) {
    Class<? extends ReactionNode> nodeClass = null;
    if (speciesRefernceGlyph.getRole() != null) {
      switch (speciesRefernceGlyph.getRole()) {
        case ACTIVATOR:
          nodeClass = Trigger.class;
          break;
        case INHIBITOR:
          nodeClass = Inhibition.class;
          break;
        case PRODUCT:
          nodeClass = Product.class;
          break;
        case SIDEPRODUCT:
          nodeClass = Product.class;
          break;
        case SIDESUBSTRATE:
          nodeClass = Reactant.class;
          break;
        case SUBSTRATE:
          nodeClass = Reactant.class;
          break;
        case MODIFIER:
          nodeClass = Modifier.class;
          break;
        case UNDEFINED:
          break;
        default:
          throw new NotImplementedException("Support for " + speciesRefernceGlyph.getRole() + " is not implemented");
      }
    }
    return nodeClass;
  }

  private void assignRenderDataToReaction(final ReactionGlyph glyph, final Reaction reactionWithLayout)
      throws InvalidInputDataExecption {
    LocalStyle style = getStyleForGlyph(glyph);
    if (style != null) {
      applyStyleToReaction(reactionWithLayout, style);
    }
  }

  public void applyStyleToReaction(final Reaction reactionWithLayout, final LocalStyle style) {
    RenderGroup group = style.getGroup();
    if (group.isSetStroke()) {
      Color color = getColorByColorDefinition(group.getStroke());
      for (final AbstractNode node : reactionWithLayout.getNodes()) {
        node.getLine().setColor(color);
      }
      reactionWithLayout.getLine().setColor(color);
    }
    if (group.isSetStrokeWidth()) {
      for (final AbstractNode node : reactionWithLayout.getNodes()) {
        node.getLine().setWidth(group.getStrokeWidth());
      }
      reactionWithLayout.getLine().setWidth(group.getStrokeWidth());
    }
    if (group.isSetStrokeDashArray()) {
      LineType type = LineType.getTypeByDashArray(group.getStrokeDashArray());
      for (final AbstractNode node : reactionWithLayout.getNodes()) {
        node.getLine().setType(type);
      }
      reactionWithLayout.getLine().setType(type);
    }
    if (group.isSetEndHead()) {
      try {
        ArrowType type = ArrowType.valueOf(group.getEndHead().replaceAll("line_ending_", ""));
        for (final Product node : reactionWithLayout.getProducts()) {
          node.getLine().getEndAtd().setArrowType(type);
        }
      } catch (final Exception e) {
        logger.warn(new ElementUtils().getElementTag(reactionWithLayout) + "Problematic arrow type: "
            + group.getStroke(), e);
      }
    }
  }

  private PolylineData getLineFromReferenceGlyph(final SpeciesReferenceGlyph speciesRefernceGlyph) {
    PolylineData line = null;
    if (speciesRefernceGlyph.getCurve() == null) {
      logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, "speciesReferenceGlyph",
          speciesRefernceGlyph.getId(), getMinervaModel().getName()), "No curve is defined");
    } else {
      for (final CurveSegment segment : speciesRefernceGlyph.getCurve().getListOfCurveSegments()) {
        Point2D start = new Point2D.Double(segment.getStart().getX(), segment.getStart().getY());
        Point2D end = new Point2D.Double(segment.getEnd().getX(), segment.getEnd().getY());
        if (!start.equals(end)) {
          if (line == null) {
            line = new PolylineData(start, end);
          } else {
            line.addLine(line.getEndPoint(), end);
          }
        }
      }
    }
    if (line == null) {
      return new PolylineData();
    }
    return line;
  }

  private void updateKinetics(final Reaction reactionWithLayout, final String newElementId, final String oldElementId) {
    SbmlKinetics kinetics = reactionWithLayout.getKinetics();
    if (kinetics != null) {
      String newDefinition = kinetics.getDefinition().replace(">" + oldElementId + "<", ">" + newElementId + "<");
      kinetics.setDefinition(newDefinition);
      Element elementToRemove = null;
      for (final Element element : kinetics.getElements()) {
        if (element.getElementId().equals(oldElementId)) {
          elementToRemove = element;
        }
      }
      if (elementToRemove != null) {
        kinetics.removeElement(elementToRemove);
        kinetics.addElement(getMinervaModel().getElementByElementId(newElementId));
      }
    }

  }

  protected Reaction parse(final org.sbml.jsbml.Reaction sbmlReaction) throws InvalidInputDataExecption {
    Reaction reaction = new Reaction(sbmlReaction.getId());
    assignBioEntityData(sbmlReaction, reaction);
    reaction.setReversible(sbmlReaction.isReversible());
    Map<String, Element> referenceMapping = new HashMap<>();
    for (final SpeciesReference reactant : sbmlReaction.getListOfReactants()) {
      Species element = getMinervaModel().getElementByElementId(reactant.getSpecies());
      if (element == null) {
        throw new InvalidInputDataExecption(
            eu.getElementTag(reaction) + "Species with id " + reactant.getSpecies() + " cannot be found");
      }
      if (reactant.getId() != null && !reactant.getId().trim().isEmpty()) {
        referenceMapping.put(reactant.getId(), element);
      }
      reaction.addReactant(new Reactant(element));
    }
    for (final SpeciesReference product : sbmlReaction.getListOfProducts()) {
      Species element = getMinervaModel().getElementByElementId(product.getSpecies());
      if (element == null) {
        throw new InvalidInputDataExecption(
            eu.getElementTag(reaction) + "Species with id " + product.getSpecies() + " cannot be found");
      }
      if (product.getId() != null && !product.getId().trim().isEmpty()) {
        referenceMapping.put(product.getId(), element);
      }
      reaction.addProduct(new Product(element));
    }

    for (final ModifierSpeciesReference modifier : sbmlReaction.getListOfModifiers()) {
      Species element = getMinervaModel().getElementByElementId(modifier.getSpecies());
      Class<? extends Modifier> nodeClass = SBOTermModifierType.getTypeSBOTerm(
          modifier.getSBOTermID(),
          SbmlModelUtils.createMarker(ProjectLogEntryType.PARSING_ISSUE, modifier));
      try {
        Modifier newNode = nodeClass.getConstructor(Element.class).newInstance(element);
        reaction.addModifier(newNode);
      } catch (final InstantiationException | IllegalAccessException | IllegalArgumentException
                     | InvocationTargetException | NoSuchMethodException | SecurityException e) {
        throw new InvalidInputDataExecption("Problem with creating modifier", e);
      }
      if (modifier.getId() != null && !modifier.getId().trim().isEmpty()) {
        referenceMapping.put(modifier.getId(), element);
      }
    }

    if (sbmlReaction.getKineticLaw() != null) {
      if (sbmlReaction.getKineticLaw().getMath() == null) {
        logger.warn(eu.getElementTag(reaction) + "Reaction contains empty kinetic law.");
      } else {
        reaction.setKinetics(createMinervaKinetics(sbmlReaction.getKineticLaw(), referenceMapping));
      }
    }

    if (reaction.getReactants().size() == 0) {
      Species element = speciesParser.getArtifitialInput();
      logger.warn(eu.getElementTag(reaction) + "Reaction doesn't have any reactant. Adding artifitial.");
      reaction.addReactant(new Reactant(element));
    }

    if (reaction.getProducts().size() == 0) {
      Species element = speciesParser.getArtifitialOutput();
      logger.warn(eu.getElementTag(reaction) + "Reaction doesn't have any products. Adding artifitial.");
      reaction.addProduct(new Product(element));
    }

    reaction = createProperReactionClassForSboTerm(reaction, sbmlReaction);

    return reaction;
  }

  private Reaction createProperReactionClassForSboTerm(final Reaction reaction, final org.sbml.jsbml.Reaction sbmlReaction)
      throws InvalidInputDataExecption {
    Reaction result;
    String sboTerm = null;
    if (sbmlReaction != null) {
      sboTerm = sbmlReaction.getSBOTermID();
    }
    Class<? extends Reaction> reactionClass = SBOTermReactionType.getTypeSBOTerm(sboTerm,
        SbmlModelUtils.createMarker(ProjectLogEntryType.PARSING_ISSUE, sbmlReaction));
    try {
      result = reactionClass.getConstructor(Reaction.class).newInstance(reaction);
    } catch (final InstantiationException | IllegalAccessException | IllegalArgumentException
                   | InvocationTargetException | NoSuchMethodException | SecurityException e) {
      if (e.getCause() instanceof InvalidReactionParticipantNumberException) {
        try {
          logger.warn(eu.getElementTag(reaction) + "SBO term indicated " + reactionClass.getSimpleName()
              + " class. However, number of reactants/products is insufficient for such class type. Using default.");
          reactionClass = SBOTermReactionType.getTypeSBOTerm(null,
              SbmlModelUtils.createMarker(ProjectLogEntryType.PARSING_ISSUE, sbmlReaction));
          result = reactionClass.getConstructor(Reaction.class).newInstance(reaction);
        } catch (final InstantiationException | IllegalAccessException | IllegalArgumentException
                       | InvocationTargetException | NoSuchMethodException | SecurityException e1) {
          throw new InvalidInputDataExecption(eu.getElementTag(reaction) + "Problem with creating reaction", e);
        }
      } else {
        throw new InvalidInputDataExecption(eu.getElementTag(reaction) + "Problem with creating reaction", e);
      }
    }
    return result;
  }

  private SbmlKinetics createMinervaKinetics(final KineticLaw kineticLaw, final Map<String, Element> referenceIds)
      throws InvalidInputDataExecption {
    SbmlKinetics result = new SbmlKinetics();
    result.setDefinition(kineticLaw.getMath().toMathML());

    SbmlParameterParser parameterParser = new SbmlParameterParser(getSbmlModel(), getMinervaModel());
    result.addParameters(parameterParser.parseList(kineticLaw.getListOfLocalParameters()));

    try {
      Node node = XmlParser.getXmlDocumentFromString(result.getDefinition());
      Set<SbmlArgument> elementsUsedInKinetics = new HashSet<>();
      for (final Node ciNode : XmlParser.getAllNotNecessirellyDirectChild("ci", node)) {
        List<String> attributesToRemove = new ArrayList<>();
        for (int y = 0; y < ciNode.getAttributes().getLength(); y++) {
          Node attr = ciNode.getAttributes().item(y);
          attributesToRemove.add(attr.getNodeName());
        }
        for (final String attributeName : attributesToRemove) {
          ciNode.getAttributes().removeNamedItem(attributeName);
          logger.warn("Kinetics attribute not supported: " + attributeName);
        }

        String id = XmlParser.getNodeValue(ciNode).trim();
        SbmlArgument element = getMinervaModel().getElementByElementId(id);
        if (element == null) {
          element = result.getParameterById(id);
        }
        if (element == null) {
          element = getMinervaModel().getParameterById(id);
        }
        if (element == null) {
          element = getMinervaModel().getFunctionById(id);
        }
        if (element == null) {
          element = referenceIds.get(id);
        }
        if (element != null) {
          ciNode.setTextContent(element.getElementId());
          elementsUsedInKinetics.add(element);
        } else if ("default".equals(id)) {
          // default compartment is skipped
        } else {
          throw new InvalidXmlSchemaException("Unknown symbol in kinetics: " + id);
        }
      }
      result.addArguments(elementsUsedInKinetics);
      result.setDefinition(XmlParser.nodeToString(node));
    } catch (final InvalidXmlSchemaException e) {
      throw new InvalidInputDataExecption(e);
    }

    return result;
  }

  public void validateReactions(final Set<Reaction> reactions) throws InvalidInputDataExecption {
    for (final Reaction reaction : reactions) {
      if (reaction.getReactants().size() == 0) {
        throw new InvalidInputDataExecption(
            eu.getElementTag(reaction) + "At least one reactant is required for reaction");
      }
      if (reaction.getProducts().size() == 0) {
        throw new InvalidInputDataExecption(
            eu.getElementTag(reaction) + "At least one product is required for reaction");
      }
    }

  }

}
