package lcsb.mapviewer.converter.model.sbml.reaction.render;

import lcsb.mapviewer.model.map.reaction.type.UnknownCatalysisReaction;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.List;

public class UnknownCatalysisReactionShapeFactory extends ReactionShapeFactory<UnknownCatalysisReaction> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return super.getShapeDefinition(UnknownCatalysisReaction.class);
  }
}
