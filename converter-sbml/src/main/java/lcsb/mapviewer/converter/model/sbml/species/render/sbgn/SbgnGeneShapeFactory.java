package lcsb.mapviewer.converter.model.sbml.species.render.sbgn;

import lcsb.mapviewer.converter.model.sbml.species.render.AShapeFactory;
import lcsb.mapviewer.model.map.species.Gene;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.Collections;
import java.util.List;

public class SbgnGeneShapeFactory extends AShapeFactory<Gene> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return Collections.singletonList(createRoundedBottomRect(AShapeFactory.RECTANGLE_CORNER_ARC_SIZE));
  }

}
