package lcsb.mapviewer.converter.model.sbml.units;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.Unit;
import org.sbml.jsbml.UnitDefinition;

import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitTypeFactor;
import lcsb.mapviewer.model.map.model.Model;

public class SbmlUnitExporter {
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  private Model minervaModel;
  private org.sbml.jsbml.Model model;

  private boolean provideDefaults = true;

  public SbmlUnitExporter(final lcsb.mapviewer.model.map.model.Model minervaModel) {
    this.minervaModel = minervaModel;
  }

  public void exportUnits(final org.sbml.jsbml.Model result) {
    this.model = result;
    UnitDefinition area = null;
    for (final SbmlUnit unit : minervaModel.getUnits()) {
      UnitDefinition unitDefinition = createUnitDefinition(unit);
      result.addUnitDefinition(unitDefinition);
      if (unit.getName() != null && unit.getName().equalsIgnoreCase("area")) {
        area = unitDefinition;
      }
    }
    // default units
    if (provideDefaults) {
      result.setExtentUnits(Unit.Kind.MOLE);
      result.setLengthUnits(Unit.Kind.METRE);
      result.setVolumeUnits(Unit.Kind.LITRE);
      result.setSubstanceUnits(Unit.Kind.MOLE);
      result.setTimeUnits(Unit.Kind.SECOND);
      if (area == null) {
        area = result.createUnitDefinition("area_unit");
        area.setName("area");
        Unit sbmlUnit = area.createUnit(Unit.Kind.METRE);
        sbmlUnit.setExponent(2.0);
        sbmlUnit.setMultiplier(1);
        sbmlUnit.setScale(0);
      }
      result.setAreaUnits(area);
    }

  }

  private UnitDefinition createUnitDefinition(final SbmlUnit unit) {
    UnitDefinition result = new UnitDefinition();
    result.setLevel(model.getLevel());
    result.setVersion(model.getVersion());
    result.setId(unit.getUnitId());
    result.setName(unit.getName());
    for (final SbmlUnitTypeFactor factor : unit.getUnitTypeFactors()) {
      Unit sbmlUnit = new Unit();
      sbmlUnit.setLevel(model.getLevel());
      sbmlUnit.setVersion(model.getVersion());
      sbmlUnit.setKind(UnitMapping.unitTypeToKind(factor.getUnitType()));
      sbmlUnit.setExponent((double) factor.getExponent());
      sbmlUnit.setMultiplier(factor.getMultiplier());
      sbmlUnit.setScale(factor.getScale());
      result.addUnit(sbmlUnit);
    }
    return result;
  }

  public boolean isProvideDefaults() {
    return provideDefaults;
  }

  public void setProvideDefaults(final boolean provideDefaults) {
    this.provideDefaults = provideDefaults;
  }

}
