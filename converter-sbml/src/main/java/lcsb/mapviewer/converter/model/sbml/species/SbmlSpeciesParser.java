package lcsb.mapviewer.converter.model.sbml.species;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.sbml.SbmlElementParser;
import lcsb.mapviewer.converter.model.sbml.SbmlLogMarker;
import lcsb.mapviewer.converter.model.sbml.SbmlModelUtils;
import lcsb.mapviewer.converter.model.sbml.extension.multi.BioEntityFeature;
import lcsb.mapviewer.converter.model.sbml.extension.multi.MultiPackageNamingUtils;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.sbo.SBOTermSpeciesType;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.PositionToCompartment;
import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;
import lcsb.mapviewer.model.map.species.field.RegulatoryRegion;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithBindingRegion;
import lcsb.mapviewer.model.map.species.field.SpeciesWithCodingRegion;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationResidue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationSite;
import lcsb.mapviewer.model.map.species.field.SpeciesWithProteinBindingDomain;
import lcsb.mapviewer.model.map.species.field.SpeciesWithRegulatoryRegion;
import lcsb.mapviewer.model.map.species.field.SpeciesWithResidue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithStructuralState;
import lcsb.mapviewer.model.map.species.field.SpeciesWithTranscriptionSite;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.AbstractReferenceGlyph;
import org.sbml.jsbml.ext.layout.CompartmentGlyph;
import org.sbml.jsbml.ext.layout.GeneralGlyph;
import org.sbml.jsbml.ext.layout.GraphicalObject;
import org.sbml.jsbml.ext.layout.ReferenceGlyph;
import org.sbml.jsbml.ext.layout.SpeciesGlyph;
import org.sbml.jsbml.ext.multi.MultiModelPlugin;
import org.sbml.jsbml.ext.multi.MultiSpeciesPlugin;
import org.sbml.jsbml.ext.multi.MultiSpeciesType;
import org.sbml.jsbml.ext.multi.OutwardBindingSite;
import org.sbml.jsbml.ext.multi.PossibleSpeciesFeatureValue;
import org.sbml.jsbml.ext.multi.SpeciesFeature;
import org.sbml.jsbml.ext.multi.SpeciesFeatureType;
import org.sbml.jsbml.ext.multi.SpeciesFeatureValue;
import org.sbml.jsbml.ext.multi.SpeciesTypeInstance;
import org.sbml.jsbml.ext.multi.SubListOfSpeciesFeature;
import org.sbml.jsbml.ext.render.LocalStyle;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class SbmlSpeciesParser extends SbmlElementParser<org.sbml.jsbml.Species> {
  private static final String ARTIFITIAL_ID = "sbml_artifitial_";
  private static final Logger logger = LogManager.getLogger();

  public SbmlSpeciesParser(final Model model, final lcsb.mapviewer.model.map.model.Model minervaModel) {
    super(model, minervaModel);
  }

  private void assignMultiData(final org.sbml.jsbml.Species sbmlSpecies, final Species minervaElement) {
    final String warnPrefix = new ElementUtils().getElementTag(minervaElement);
    final MultiModelPlugin multiPlugin = getMultiPlugin();
    final MultiSpeciesPlugin multiExtension = (MultiSpeciesPlugin) sbmlSpecies.getExtension("multi");
    if (multiExtension != null) {
      final MultiSpeciesType speciesType = multiPlugin.getListOfSpeciesTypes().get(multiExtension.getSpeciesType());
      if (speciesType == null) {
        logger.warn(warnPrefix + "Species type not defined in multi extension");
      } else {
        for (final SpeciesFeature feature : multiExtension.getListOfSpeciesFeatures()) {
          assignMultiFeatureData(minervaElement, speciesType, feature);
        }
        for (final OutwardBindingSite site : multiExtension.getListOfOutwardBindingSites()) {
          logger.warn(
              warnPrefix + "OutwardBindingSite not supported: " + site.getComponent() + "; " + site.getBindingStatus());
        }
        for (final SubListOfSpeciesFeature site : multiExtension.getListOfSubListOfSpeciesFeatures()) {
          logger.warn(
              warnPrefix + "SubListOfSpeciesFeature not supported: " + site.getComponent());
        }
      }
    }
  }

  private void assignMultiFeatureData(final Species minervaElement, final MultiSpeciesType speciesType,
                                      final SpeciesFeature feature) {
    final String warnPrefix = new ElementUtils().getElementTag(minervaElement);
    final String featureTypeString = feature.getSpeciesFeatureType();
    final List<String> featureValues = getFeatureValues(speciesType, feature);
    if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.STRUCTURAL_STATE)) {
      createModificationResidues(minervaElement, speciesType, feature);
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.POSITION_TO_COMPARTMENT)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "Position to compartment must have exactly one value");
      } else {
        minervaElement.setPositionToCompartment(PositionToCompartment.getByString(featureValues.get(0)));
      }
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.SYNONYM)) {
      minervaElement.setSynonyms(featureValues);
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.FORMER_SYMBOL)) {
      minervaElement.setFormerSymbols(featureValues);
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.DIMER)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "Dimer must have exactly one value");
      } else {
        try {
          minervaElement.setHomodimer(Integer.parseInt(featureValues.get(0)));
        } catch (final NumberFormatException e) {
          logger.warn(warnPrefix + "Dimer must have integer value, instead found: " + featureValues.get(0));
        }
      }
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.CHARGE)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "Charge must have exactly one value");
      } else {
        try {
          minervaElement.setCharge(Integer.parseInt(featureValues.get(0)));
        } catch (final NumberFormatException e) {
          logger.warn(warnPrefix + "Charge must have integer value, instead found: " + featureValues.get(0));
        }
      }
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.SYMBOL)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "Symbol must have exactly one value");
      } else {
        minervaElement.setSymbol(featureValues.get(0));
      }
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.STATE_PREFIX)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "State prefix must have exactly one value");
      } else {
        minervaElement.setStatePrefix(featureValues.get(0));
      }
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.STATE_SUFFIX)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "State suffix must have exactly one value");
      } else {
        minervaElement.setStateLabel(featureValues.get(0));
      }
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.FULL_NAME)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "Full name must have exactly one value");
      } else {
        minervaElement.setFullName(featureValues.get(0));
      }
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.FORMULA)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "Formula must have exactly one value");
      } else {
        minervaElement.setFormula(featureValues.get(0));
      }
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.ABBREVIATION)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "Abbreviation must have exactly one value");
      } else {
        minervaElement.setAbbreviation(featureValues.get(0));
      }
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.HYPOTHETICAL)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "Hypothetical must have exactly one value");
      } else {
        Boolean value = null;
        if (featureValues.get(0).equalsIgnoreCase("true")) {
          value = true;
        } else if (featureValues.get(0).equalsIgnoreCase("false")) {
          value = false;
        } else {
          logger.warn(warnPrefix + "Hypothetical must be true/false value. Found: " + featureValues.get(0));
        }
        minervaElement.setHypothetical(value);
      }
    } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.ACTIVITY)) {
      if (featureValues.size() != 1) {
        logger.warn(warnPrefix + "Activity must have exactly one value");
      } else {
        Boolean value = null;
        if (featureValues.get(0).equalsIgnoreCase("true")) {
          value = true;
        } else if (featureValues.get(0).equalsIgnoreCase("false")) {
          value = false;
        } else {
          logger.warn(warnPrefix + "Activity must be true/false value. Found: " + featureValues.get(0));
        }
        minervaElement.setActivity(value);
      }
    } else if (MultiPackageNamingUtils.isModificationFeatureId(featureTypeString)) {
      createModificationResidues(minervaElement, speciesType, feature);
    } else {
      logger.warn(warnPrefix + "Feature not supported: " + featureTypeString);
    }
  }

  private List<String> getFeatureValues(final MultiSpeciesType speciesType, final SpeciesFeature feature) {
    final SpeciesFeatureType featureType = speciesType.getListOfSpeciesFeatureTypes().get(feature.getSpeciesFeatureType());

    List<String> result = new ArrayList<>();
    if (featureType != null) {
      for (final SpeciesFeatureValue featureValue : feature.getListOfSpeciesFeatureValues()) {
        final PossibleSpeciesFeatureValue possibleSpeciesFeatureValue = featureType.getListOfPossibleSpeciesFeatureValues()
            .get(featureValue.getValue());
        if (possibleSpeciesFeatureValue.getName().equals(MultiPackageNamingUtils.NULL_REPRESENTATION)) {
          result.add(null);
        } else {
          result.add(possibleSpeciesFeatureValue.getName());
        }
      }
    } else {
      for (final SpeciesTypeInstance speciesTypeInstance : speciesType.getListOfSpeciesTypeInstances()) {
        result = getFeatureValues(getMultiPlugin().getSpeciesType(speciesTypeInstance.getSpeciesType()), feature);
        if (result.size() > 0) {
          return result;
        }
      }

    }
    return result;
  }

  private void createModificationResidues(final Species minervaElement, final MultiSpeciesType speciesType,
                                          final SpeciesFeature feature) {
    final String warnPrefix = new ElementUtils().getElementTag(minervaElement);
    ModificationResidue mr = null;

    final String featureTypeString = feature.getSpeciesFeatureType();
    final SpeciesFeatureType featureType = speciesType.getListOfSpeciesFeatureTypes().get(featureTypeString);
    for (final SpeciesFeatureValue featureValue : feature.getListOfSpeciesFeatureValues()) {
      if (MultiPackageNamingUtils.isModificationFeatureId(featureTypeString, BindingRegion.class)) {
        mr = new BindingRegion();
        if (minervaElement instanceof SpeciesWithBindingRegion) {
          ((SpeciesWithBindingRegion) minervaElement).addBindingRegion((BindingRegion) mr);
        } else {
          logger.warn(warnPrefix + "Object class doesn't support " + mr.getClass());
        }
      } else if (MultiPackageNamingUtils.isModificationFeatureId(featureTypeString, CodingRegion.class)) {
        mr = new CodingRegion();
        if (minervaElement instanceof SpeciesWithCodingRegion) {
          ((SpeciesWithCodingRegion) minervaElement).addCodingRegion((CodingRegion) mr);
        } else {
          logger.warn(warnPrefix + "Object class doesn't support " + mr.getClass());
        }
      } else if (MultiPackageNamingUtils.isModificationFeatureId(featureTypeString, ProteinBindingDomain.class)) {
        mr = new ProteinBindingDomain();
        if (minervaElement instanceof SpeciesWithProteinBindingDomain) {
          ((SpeciesWithProteinBindingDomain) minervaElement).addProteinBindingDomain((ProteinBindingDomain) mr);
        } else {
          logger.warn(warnPrefix + "Object class doesn't support " + mr.getClass());
        }
      } else if (MultiPackageNamingUtils.isModificationFeatureId(featureTypeString, RegulatoryRegion.class)) {
        mr = new RegulatoryRegion();
        if (minervaElement instanceof SpeciesWithRegulatoryRegion) {
          ((SpeciesWithRegulatoryRegion) minervaElement).addRegulatoryRegion((RegulatoryRegion) mr);
        } else {
          logger.warn(warnPrefix + "Object class doesn't support " + mr.getClass());
        }
      } else if (MultiPackageNamingUtils.isModificationFeatureId(featureTypeString, TranscriptionSite.class)) {
        mr = new TranscriptionSite();
        if (minervaElement instanceof SpeciesWithTranscriptionSite) {
          final TranscriptionSite transcriptionSite = (TranscriptionSite) mr;
          transcriptionSite.setActive(
              MultiPackageNamingUtils.getTranscriptionFactorActiveStateFromFeatureTypeName(featureTypeString));
          transcriptionSite.setDirection(
              MultiPackageNamingUtils.getTranscriptionFactorDirectionStateFromFeatureTypeName(featureTypeString));
          ((SpeciesWithTranscriptionSite) minervaElement).addTranscriptionSite(transcriptionSite);
        } else {
          logger.warn(warnPrefix + "Object class doesn't support " + mr.getClass());
        }
      } else if (MultiPackageNamingUtils.isModificationFeatureId(featureTypeString, ModificationSite.class)) {
        mr = new ModificationSite();
        ((ModificationSite) mr)
            .setState(MultiPackageNamingUtils.getModificationStateFromFeatureTypeName(featureTypeString));
        if (minervaElement instanceof SpeciesWithModificationSite) {
          ((SpeciesWithModificationSite) minervaElement).addModificationSite((ModificationSite) mr);
        } else {
          logger.warn(warnPrefix + "Object class doesn't support " + mr.getClass());
        }
      } else if (MultiPackageNamingUtils.isModificationFeatureId(featureTypeString, Residue.class)) {
        mr = new Residue();
        ((Residue) mr)
            .setState(MultiPackageNamingUtils.getModificationStateFromFeatureTypeName(featureTypeString));
        if (minervaElement instanceof SpeciesWithResidue) {
          ((SpeciesWithResidue) minervaElement).addResidue((Residue) mr);
        } else {
          logger.warn(warnPrefix + "Object class doesn't support " + mr.getClass());
        }
      } else if (MultiPackageNamingUtils.isFeatureId(featureTypeString, BioEntityFeature.STRUCTURAL_STATE)) {
        mr = new StructuralState();
        if (minervaElement instanceof SpeciesWithStructuralState) {
          ((SpeciesWithStructuralState) minervaElement).addStructuralState((StructuralState) mr);
        } else {
          logger.warn(warnPrefix + "Object class doesn't support " + mr.getClass());
        }
      } else {
        logger.warn(warnPrefix + "Unsupported modification type: " + featureTypeString);
      }
      final PossibleSpeciesFeatureValue possibleSpeciesFeatureValue = featureType.getListOfPossibleSpeciesFeatureValues()
          .get(featureValue.getValue());
      if (possibleSpeciesFeatureValue != null) {
        mr.setName(possibleSpeciesFeatureValue.getName());
        mr.setIdModificationResidue(featureValue.getId());
      } else {
        logger.warn(warnPrefix + "Invalid feature value: " + featureValue.getValue());
      }
    }

  }

  private String extractSBOTermFromSpecies(final org.sbml.jsbml.Species species) {
    String sboTerm = species.getSBOTermID();
    final MultiSpeciesPlugin multiExtension = (MultiSpeciesPlugin) species.getExtension("multi");
    final MultiSpeciesType speciesType = getMultiSpeciesType(multiExtension);

    if (speciesType != null) {
      final String sboTerm2 = speciesType.getSBOTermID();
      if (sboTerm != null && !sboTerm.isEmpty() && !sboTerm2.equals(sboTerm)) {
        logger.warn(new SbmlLogMarker(ProjectLogEntryType.PARSING_ISSUE, species, getMinervaModel()),
            "Different SBO terms defining species and speciesType: " + species.getId() + ". " + sboTerm + ";"
                + sboTerm2);
      } else {
        sboTerm = sboTerm2;
      }
      if (speciesType.getSpeciesTypeInstanceCount() > 0) {
        if (sboTerm == null || sboTerm.isEmpty()) {
          sboTerm = SBOTermSpeciesType.COMPLEX.getSBO();
        } else {
          final SBOTermSpeciesType type = SBOTermSpeciesType.getTypeSBOTerm(sboTerm2,
              SbmlModelUtils.createMarker(ProjectLogEntryType.PARSING_ISSUE, species));
          if (type != SBOTermSpeciesType.COMPLEX) {
            logger.warn(new SbmlLogMarker(ProjectLogEntryType.PARSING_ISSUE, species, getMinervaModel()),
                "SBO term defining species is not complex, but type is defined as complex.");
            sboTerm = SBOTermSpeciesType.COMPLEX.getSBO();
          }
        }

      }
    }
    return sboTerm;
  }

  private MultiSpeciesType getMultiSpeciesType(final MultiSpeciesPlugin multiExtension) {
    if (multiExtension != null) {
      final MultiModelPlugin modelPlugin = getMultiPlugin();
      if (modelPlugin != null) {
        final ListOf<MultiSpeciesType> listOfSpeciesTypes = modelPlugin.getListOfSpeciesTypes();
        if (listOfSpeciesTypes != null) {
          return listOfSpeciesTypes.get(multiExtension.getSpeciesType());
        }
      }
    }

    return null;
  }

  private void assignModificationResiduesLayout(final Element element) {
    if (element instanceof SpeciesWithModificationResidue) {
      for (final ModificationResidue mr : ((SpeciesWithModificationResidue) element).getModificationResidues()) {
        final GeneralGlyph residueGlyph = getResidueGlyphForModification(mr);
        if (residueGlyph != null) {
          if (residueGlyph.getBoundingBox() == null || residueGlyph.getBoundingBox().getDimensions() == null) {
            logger.warn(new ElementUtils().getElementTag(mr) + "Layout doesn't contain coordinates");
          } else {
            final double width = residueGlyph.getBoundingBox().getDimensions().getWidth();
            final double height = residueGlyph.getBoundingBox().getDimensions().getHeight();
            final double x = residueGlyph.getBoundingBox().getPosition().getX();
            final double y = residueGlyph.getBoundingBox().getPosition().getY();
            final double z = residueGlyph.getBoundingBox().getPosition().getZ();
            mr.setPosition(new Point2D.Double(x, y));
            mr.setZ((int) Math.round(z));

            final LocalStyle style = getStyleForGlyph(residueGlyph);
            if (style != null) {
              final Color borderColor = getColorByColorDefinition(style.getGroup().getStroke());
              mr.setBorderColor(borderColor);
              if (style.getGroup().isSetFontSize()) {
                mr.setFontSize(style.getGroup().getFontSize());
              }
              if (style.getGroup().isSetFill()) {
                final Color fillColor = getColorByColorDefinition(style.getGroup().getFill());
                mr.setFillColor(fillColor);
              }
            }

            mr.setWidth(width);
            mr.setHeight(height);
            mr.setNameX(x);
            mr.setNameY(y);
            mr.setNameWidth(width);
            mr.setNameHeight(height);
          }
        }
        if (mr.getBorderColor() == null) {
          mr.setBorderColor(Color.BLACK);
        }
      }
    }

  }

  private GeneralGlyph getResidueGlyphForModification(final ModificationResidue mr) {
    GeneralGlyph residueGlyph = null;
    for (final GraphicalObject graphicalObject : getLayout().getListOfAdditionalGraphicalObjects()) {
      if (graphicalObject instanceof GeneralGlyph) {
        if (((GeneralGlyph) graphicalObject).getReference().equals(mr.getIdModificationResidue())) {
          if (((GeneralGlyph) graphicalObject).getListOfReferenceGlyphs().size() > 0) {
            // find a reference to the alias in layout, so we know it's the
            // proper value
            final ReferenceGlyph referenceGlyph = ((GeneralGlyph) graphicalObject).getListOfReferenceGlyphs().get(0);
            if (Objects.equals(referenceGlyph.getGlyph(), mr.getSpecies().getElementId())) {
              // if
              // (referenceGlyph.getGlyph().equals(mr.getSpecies().getElementId()))
              // {
              residueGlyph = (GeneralGlyph) graphicalObject;
            }
          } else {
            residueGlyph = (GeneralGlyph) graphicalObject;
          }
        }
      }
    }
    return residueGlyph;
  }

  private void assignCompartment(final Element element, final String compartmentId) {
    Compartment compartment = getMinervaModel().getElementByElementId(compartmentId);
    if (compartment == null && getLayout() != null) {
      final List<Compartment> compartments = new ArrayList<>();
      for (final CompartmentGlyph glyph : getLayout().getListOfCompartmentGlyphs()) {
        if (glyph.getCompartment().equals(compartmentId) && !glyph.getCompartment().equals("default")) {
          compartments.add(getMinervaModel().getElementByElementId(glyph.getId()));
        }
      }
      for (final Compartment compartment2 : compartments) {
        if (compartment2.contains(element)) {
          compartment = compartment2;
        }
      }

    }
    if (compartment != null) {
      compartment.addElement(element);
    }

  }

  @Override
  protected ListOf<org.sbml.jsbml.Species> getSbmlElementList() {
    return getSbmlModel().getListOfSpecies();
  }

  @Override
  public List<Element> mergeLayout(final List<? extends Element> elements)
      throws InvalidInputDataExecption {
    final List<Element> result = super.mergeLayout(elements);

    for (final Element element : result) {
      String compartmentId = null;
      if (getLayout().getSpeciesGlyph(element.getElementId()) != null) {
        compartmentId = ((org.sbml.jsbml.Species) getLayout().getSpeciesGlyph(element.getElementId())
            .getSpeciesInstance()).getCompartment();
      } else {
        if (!element.getElementId().startsWith(ARTIFITIAL_ID)) {
          compartmentId = getSbmlModel().getSpecies(element.getElementId()).getCompartment();
        }
      }
      assignCompartment(element, compartmentId);
      assignModificationResiduesLayout(element);

      if (element.getNameX() == null) {
        element.setNameX(element.getX());
      }
      if (element.getNameY() == null) {
        element.setNameY(element.getY());
      }
      if (element.getNameWidth() == null || element.getNameHeight() == null) {
        element.setNameWidth(element.getWidth() - (element.getNameX() - element.getX()));
        element.setNameHeight(element.getHeight() - (element.getNameY() - element.getY()));
      }
      if (element.getNameHorizontalAlign() == null) {
        element.setNameHorizontalAlign(HorizontalAlign.CENTER);
      }
      if (element.getNameVerticalAlign() == null) {
        element.setNameVerticalAlign(VerticalAlign.MIDDLE);
      }
    }

    return result;
  }

  @Override
  protected void applyStyleToElement(final Element elementWithLayout, final LocalStyle style) {
    super.applyStyleToElement(elementWithLayout, style);
    final Species specisWithLayout = (Species) elementWithLayout;
    if (style.getGroup().isSetStrokeWidth()) {
      specisWithLayout.setLineWidth(style.getGroup().getStrokeWidth());
    }
  }

  @Override
  protected List<Pair<String, AbstractReferenceGlyph>> getGlyphs() {
    final List<Pair<String, AbstractReferenceGlyph>> result = new ArrayList<>();
    for (final SpeciesGlyph glyph : getLayout().getListOfSpeciesGlyphs()) {
      result.add(new Pair<String, AbstractReferenceGlyph>(glyph.getSpecies(), glyph));
    }
    return result;
  }

  @SuppressWarnings("deprecation")
  @Override
  protected Species parse(final org.sbml.jsbml.Species species) throws InvalidInputDataExecption {
    final String sboTerm = extractSBOTermFromSpecies(species);
    final Species result = SBOTermSpeciesType.createElementForSBOTerm(sboTerm, species.getId(),
        SbmlModelUtils.createMarker(ProjectLogEntryType.PARSING_ISSUE, species));
    if (!Double.isNaN(species.getInitialAmount())) {
      result.setInitialAmount(species.getInitialAmount());
    }
    if (!Double.isNaN(species.getInitialConcentration())) {
      result.setInitialConcentration(species.getInitialConcentration());
    }
    if (species.isSetHasOnlySubstanceUnits()) {
      result.setOnlySubstanceUnits(species.hasOnlySubstanceUnits());
    }
    if (species.isSetBoundaryCondition()) {
      result.setBoundaryCondition(species.getBoundaryCondition());
    }
    if (species.isSetConstant()) {
      result.setConstant(species.getConstant());
    }
    if (species.isSetCharge()) {
      result.setCharge(species.getCharge());
    }
    assignBioEntityData(species, result);
    if (result.getName() == null || result.getName().isEmpty()) {
      result.setName(result.getElementId());
    }
    if (getLayout() == null) {
      assignCompartment(result, species.getCompartment());
    }
    if (getMultiPlugin() != null) {
      assignMultiData(species, result);
    }
    return result;
  }

  public Species getArtifitialInput() {
    final Species result = new Unknown(ARTIFITIAL_ID + getNextId());
    result.setName("Artifitial source");
    getMinervaModel().addElement(result);
    return result;
  }

  public Species getArtifitialOutput() {
    final Species result = new Degraded(ARTIFITIAL_ID + getNextId());
    result.setName("Artifitial sink");
    getMinervaModel().addElement(result);
    return result;
  }

  public void addComplexRelationsBasedOnMulti() {

    final MultiModelPlugin modelPlugin = getMultiPlugin();
    if (modelPlugin != null) {
      final Map<String, Complex> childParentRelation = new HashMap<>();

      for (final org.sbml.jsbml.Species species : getSbmlModel().getListOfSpecies()) {
        final Element element = getMinervaModel().getElementByElementId(species.getId());
        if (element == null) {
          logger.warn(new SbmlLogMarker(ProjectLogEntryType.PARSING_ISSUE, species, getMinervaModel()),
              "Cannot find element in the model.");
          continue;
        }
        if (element instanceof Complex) {
          final MultiSpeciesPlugin multiExtension = (MultiSpeciesPlugin) species.getExtension("multi");
          final MultiSpeciesType speciesType = getMultiSpeciesType(multiExtension);
          if (speciesType != null) {
            for (int i = 0; i < speciesType.getSpeciesTypeInstanceCount(); i++) {
              final String childType = speciesType.getSpeciesTypeInstance(i).getSpeciesType();
              if (childParentRelation.get(childType) != null) {
                logger.warn(new SbmlLogMarker(ProjectLogEntryType.PARSING_ISSUE, species, getMinervaModel()),
                    "More than one species use the same complex type. Impossible to determine child-parent relation.");
                return;
              } else {
                childParentRelation.put(childType, (Complex) element);
              }
            }
          }
        }

      }

      for (final org.sbml.jsbml.Species species : getSbmlModel().getListOfSpecies()) {
        final Element element = getMinervaModel().getElementByElementId(species.getId());
        if (element == null) {
          logger.warn(new SbmlLogMarker(ProjectLogEntryType.PARSING_ISSUE, species, getMinervaModel()),
              "Cannot find element in the model.");
          continue;
        }

        if (element instanceof Species) {
          final MultiSpeciesPlugin multiExtension = (MultiSpeciesPlugin) species.getExtension("multi");
          final MultiSpeciesType speciesType = getMultiSpeciesType(multiExtension);
          if (speciesType != null) {
            final Complex complex = childParentRelation.get(speciesType.getId());
            if (complex != null) {
              complex.addSpecies((Species) element);
            }
          }
        }

      }

    }
  }

}
