package lcsb.mapviewer.converter.model.sbml;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.QuantityWithUnit;

import lcsb.mapviewer.model.map.kinetics.SbmlParameter;

public class SbmlParameterParser extends SbmlBioEntityParser {

  public SbmlParameterParser(final Model sbmlModel, final lcsb.mapviewer.model.map.model.Model minervaModel) {
    super(sbmlModel, minervaModel);
  }

  protected SbmlParameter parse(final org.sbml.jsbml.QuantityWithUnit unitDefinition) {
    SbmlParameter result = new SbmlParameter(unitDefinition.getId());
    result.setName(unitDefinition.getName());
    result.setValue(unitDefinition.getValue());
    result.setUnits(getMinervaModel().getUnitsByUnitId(unitDefinition.getUnits()));
    return result;
  }

  protected ListOf<org.sbml.jsbml.Parameter> getSbmlElementList(final Model sbmlModel) {
    return sbmlModel.getListOfParameters();
  }

  public Collection<SbmlParameter> parseList(final Model sbmlModel) {
    Collection<SbmlParameter> result = parseList(getSbmlElementList(sbmlModel));
    return result;
  }

  public Collection<SbmlParameter> parseList(final Collection<? extends QuantityWithUnit> parameters) {
    List<SbmlParameter> result = new ArrayList<>();
    for (org.sbml.jsbml.QuantityWithUnit unitDefinition : parameters) {
      result.add(parse(unitDefinition));
    }
    return result;
  }

}
