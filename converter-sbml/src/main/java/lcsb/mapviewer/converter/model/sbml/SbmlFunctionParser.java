package lcsb.mapviewer.converter.model.sbml;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;

public class SbmlFunctionParser extends SbmlBioEntityParser {

  public SbmlFunctionParser(final Model sbmlModel, final lcsb.mapviewer.model.map.model.Model minervaModel) {
    super(sbmlModel, minervaModel);
  }

  protected SbmlFunction parse(final org.sbml.jsbml.FunctionDefinition unitDefinition, final Model sbmlModel)
      throws InvalidInputDataExecption {
    try {
      SbmlFunction result = new SbmlFunction(unitDefinition.getId());
      result.setName(unitDefinition.getName());
      Node node = XmlParser.getXmlDocumentFromString(unitDefinition.getMath().toMathML());
      Node mathDefinition = XmlParser.getNode("math", node);
      String definition = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">"
          + XmlParser.nodeToString(mathDefinition).replace(" xmlns=\"http://www.w3.org/1998/Math/MathML\"", "")
          + "</math>";
      result.setDefinition(definition);
      return result;
    } catch (final InvalidXmlSchemaException e) {
      throw new InvalidInputDataExecption(e);
    }

  }

  protected ListOf<org.sbml.jsbml.FunctionDefinition> getSbmlElementList(final Model sbmlModel) {
    return sbmlModel.getListOfFunctionDefinitions();
  }

  public Collection<SbmlFunction> parseList(final Model sbmlModel) throws InvalidInputDataExecption {
    List<SbmlFunction> result = new ArrayList<>();
    for (org.sbml.jsbml.FunctionDefinition unitDefinition : getSbmlElementList(sbmlModel)) {
      result.add(parse(unitDefinition, sbmlModel));
    }
    return result;
  }
}
