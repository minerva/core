package lcsb.mapviewer.converter.model.sbml.reaction.render;

import lcsb.mapviewer.model.map.reaction.type.ReducedPhysicalStimulationReaction;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.List;

public class ReducedPhysicalStimulationReactionShapeFactory extends ReactionShapeFactory<ReducedPhysicalStimulationReaction> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return super.getShapeDefinition(ReducedPhysicalStimulationReaction.class);
  }
}
