package lcsb.mapviewer.converter.model.sbml.reaction.render;

import lcsb.mapviewer.model.map.reaction.type.UnknownReducedPhysicalStimulationReaction;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.List;

public class UnknownReducedPhysicalStimulationReactionShapeFactory extends ReactionShapeFactory<UnknownReducedPhysicalStimulationReaction> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return super.getShapeDefinition(UnknownReducedPhysicalStimulationReaction.class);
  }
}
