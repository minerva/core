package lcsb.mapviewer.converter.model.sbml.reaction.render;

import lcsb.mapviewer.model.map.reaction.type.KnownTransitionOmittedReaction;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.List;

public class KnownTransitionOmittedReactionShapeFactory extends ReactionShapeFactory<KnownTransitionOmittedReaction> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return super.getShapeDefinition(KnownTransitionOmittedReaction.class);
  }
}
