package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.model.map.Drawable;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;
import org.sbml.jsbml.ext.render.RelAbsVector;
import org.sbml.jsbml.ext.render.RenderPoint;

import java.util.Collections;
import java.util.List;

public class CrossbarArrowShapeFactory extends AShapeFactory<Drawable> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {

    final Polygon polygon = new Polygon();
    createRelativePoint(polygon, 0, 50);

    final RenderPoint p1 = new RenderPoint();
    p1.setX(new RelAbsVector(0, 100));
    p1.setY(new RelAbsVector(0, 50));

    polygon.addElement(p1);
    final RenderPoint p2 = new RenderPoint();
    p2.setX(new RelAbsVector(0, 100));
    p2.setY(new RelAbsVector(-5, 50));

    polygon.addElement(p2);
    final RenderPoint p3 = new RenderPoint();
    p3.setX(new RelAbsVector(0, 100));
    p3.setY(new RelAbsVector(5, 50));

    polygon.addElement(p3);
    polygon.addElement(p1.clone());

    return Collections.singletonList(polygon);
  }

}
