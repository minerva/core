package lcsb.mapviewer.converter.model.sbml.units;

import org.sbml.jsbml.Unit.Kind;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;

public enum UnitMapping {
  AMPERE(SbmlUnitType.AMPERE, Kind.AMPERE),

  AVOGADRO(SbmlUnitType.AVOGADRO, Kind.AVOGADRO),

  BECQUEREL(SbmlUnitType.BECQUEREL, Kind.BECQUEREL),

  CANDELA(SbmlUnitType.CANDELA, Kind.CANDELA),

  COULUMB(SbmlUnitType.COULUMB, Kind.COULOMB),

  DIMENSIONLESS(SbmlUnitType.DIMENSIONLESS, Kind.DIMENSIONLESS),

  FARAD(SbmlUnitType.FARAD, Kind.FARAD),

  GRAM(SbmlUnitType.GRAM, Kind.GRAM),

  GRAY(SbmlUnitType.GRAY, Kind.GRAY),

  HENRY(SbmlUnitType.HENRY, Kind.HENRY),

  HERTZ(SbmlUnitType.HERTZ, Kind.HERTZ),

  ITEM(SbmlUnitType.ITEM, Kind.ITEM),

  JOULE(SbmlUnitType.JOULE, Kind.JOULE),

  KATAL(SbmlUnitType.KATAL, Kind.KATAL),

  KELVIN(SbmlUnitType.KELVIN, Kind.KELVIN),

  KILOGRAM(SbmlUnitType.KILOGRAM, Kind.KILOGRAM),

  LITRE(SbmlUnitType.LITRE, Kind.LITRE),

  LUMEN(SbmlUnitType.LUMEN, Kind.LUMEN),

  LUX(SbmlUnitType.LUX, Kind.LUX),

  METRE(SbmlUnitType.METRE, Kind.METRE),

  MOLE(SbmlUnitType.MOLE, Kind.MOLE),

  NEWTON(SbmlUnitType.NEWTON, Kind.NEWTON),

  OHM(SbmlUnitType.OHM, Kind.OHM),

  PASCAL(SbmlUnitType.PASCAL, Kind.PASCAL),

  RADIAN(SbmlUnitType.RADIAN, Kind.RADIAN),

  SECOND(SbmlUnitType.SECOND, Kind.SECOND),

  SIEMENS(SbmlUnitType.SIEMENS, Kind.SIEMENS),

  SIEVERT(SbmlUnitType.SIEVERT, Kind.SIEVERT),

  STERADIAN(SbmlUnitType.STERADIAN, Kind.STERADIAN),

  TESLA(SbmlUnitType.TESLA, Kind.TESLA),

  VOLT(SbmlUnitType.VOLT, Kind.VOLT),

  WATT(SbmlUnitType.WATT, Kind.WATT),

  WEBER(SbmlUnitType.WEBER, Kind.WEBER);

  private SbmlUnitType unitType;
  private Kind sbmlUnitType;

  UnitMapping(final SbmlUnitType unitType, final Kind sbmlUnitType) {
    this.unitType = unitType;
    this.sbmlUnitType = sbmlUnitType;
  }

  public static Kind unitTypeToKind(final SbmlUnitType unitType) {
    for (final UnitMapping mapping : UnitMapping.values()) {
      if (mapping.unitType == unitType) {
        return mapping.sbmlUnitType;
      }
    }
    throw new InvalidArgumentException("Unknown sbml unit type: " + unitType);
  }

  public static SbmlUnitType kindToUnitType(final Kind kind) {
    for (final UnitMapping mapping : UnitMapping.values()) {
      if (mapping.sbmlUnitType == kind) {
        return mapping.unitType;
      }
    }
    throw new InvalidArgumentException("Unknown sbml unit type: " + kind);
  }
}
