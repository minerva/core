package lcsb.mapviewer.converter.model.sbml;

import java.awt.Color;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.AbstractNamedSBase;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.SBasePlugin;
import org.sbml.jsbml.ext.layout.BoundingBox;
import org.sbml.jsbml.ext.layout.Dimensions;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.LayoutModelPlugin;
import org.sbml.jsbml.ext.layout.Point;
import org.sbml.jsbml.ext.render.ColorDefinition;
import org.sbml.jsbml.ext.render.Ellipse;
import org.sbml.jsbml.ext.render.LineEnding;
import org.sbml.jsbml.ext.render.LocalRenderInformation;
import org.sbml.jsbml.ext.render.LocalStyle;
import org.sbml.jsbml.ext.render.Polygon;
import org.sbml.jsbml.ext.render.RelAbsVector;
import org.sbml.jsbml.ext.render.RenderGroup;
import org.sbml.jsbml.ext.render.RenderLayoutPlugin;
import org.sbml.jsbml.ext.render.RenderPoint;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.graphics.ArrowType;

public class SbmlModelUtils {

  private Logger logger = LogManager.getLogger();

  private final Model sbmlModel;

  private Layout layout;

  public SbmlModelUtils(final Model model) {
    this.sbmlModel = model;
    this.layout = computeLayout();
  }

  private Layout computeLayout() {
    Layout layout = null;
    if (sbmlModel.getExtensionCount() > 0) {
      for (final SBasePlugin plugin : sbmlModel.getExtensionPackages().values()) {
        if (plugin.getClass().equals(org.sbml.jsbml.ext.layout.LayoutModelPlugin.class)) {
          LayoutModelPlugin layoutPlugin = (LayoutModelPlugin) plugin;
          if (layoutPlugin.getLayoutCount() == 0) {
            logger.warn("Layout plugin available but no layouts defined");
          } else if (layoutPlugin.getLayoutCount() > 1) {
            logger.warn(layoutPlugin.getLayoutCount() + " layouts defined. Using first one.");
            layout = layoutPlugin.getLayout(0);
          } else {
            layout = layoutPlugin.getLayout(0);
          }
        }
      }
    }
    return layout;
  }

  public Layout getLayout() {
    return layout;
  }

  public RenderLayoutPlugin getRenderPlugin() {
    if (getLayout().getExtensionCount() > 0) {
      return (RenderLayoutPlugin) getLayout().getExtension("render");
    }
    return null;
  }

  public ColorDefinition getColorDefinition(final Color color) {
    RenderLayoutPlugin renderPlugin = getRenderPlugin();

    LocalRenderInformation renderInformation = getRenderInformation(renderPlugin);

    for (final ColorDefinition cd : renderInformation.getListOfColorDefinitions()) {
      if (cd.getValue().equals(color)) {
        return cd;
      }
    }
    ColorDefinition colorDefinition = new ColorDefinition("color_" + XmlParser.colorToString(color), color);

    renderInformation.addColorDefinition(colorDefinition);
    return colorDefinition;
  }

  public LocalRenderInformation getRenderInformation() {
    return getRenderInformation(getRenderPlugin());
  }

  public LocalRenderInformation getRenderInformation(final RenderLayoutPlugin renderPlugin) {
    LocalRenderInformation renderInformation = null;
    for (final LocalRenderInformation lri : renderPlugin.getListOfLocalRenderInformation()) {
      if (lri.getId().equals("minerva_definitions")) {
        renderInformation = lri;
      }
    }
    if (renderInformation == null) {
      renderInformation = new LocalRenderInformation("minerva_definitions");
      renderPlugin.addLocalRenderInformation(renderInformation);
    }
    return renderInformation;
  }

  public LocalStyle createStyle() {
    LocalRenderInformation renderInformation = getRenderInformation(getRenderPlugin());
    LocalStyle style = new LocalStyle();
    style.setGroup(new RenderGroup());
    renderInformation.addLocalStyle(style);
    return style;
  }

  protected LineEnding createLineEndingStyle(final String endHead) {
    String arrowTypeId = endHead.replaceAll("line_ending_", "");
    LocalRenderInformation renderInformation = getRenderInformation();
    LineEnding result = renderInformation.getListOfLineEndings().get("line_ending_" + arrowTypeId);
    if (result != null) {
      return result;
    }
    ArrowType arrowType = ArrowType.valueOf(arrowTypeId);
    result = new LineEnding();
    result.setId("line_ending_" + arrowTypeId);
    result.setGroup(new RenderGroup());
    renderInformation.getListOfLineEndings().add(result);
    switch (arrowType) {
      case FULL:
        createFullLineEnding(result);
        break;
      case BLANK:
        createBlankLineEnding(result);
        break;
      case FULL_CROSSBAR:
        createFullCrossBarLineEnding(result);
        break;
      case BLANK_CROSSBAR:
        createBlankCrossBarLineEnding(result);
        break;
      case CROSSBAR:
        createCrossBarLineEnding(result);
        break;
      case DIAMOND:
        createDiamondLineEnding(result);
        break;
      case OPEN:
        createOpenLineEnding(result);
        break;
      case CIRCLE:
        createCircleLineEnding(result);
        break;
      case NONE:
        break;
      default:
        logger.warn("Unknown arrow type: " + arrowType);
        break;
    }

    return result;
  }

  private void createCircleLineEnding(final LineEnding result) {
    BoundingBox boundingBox = createBoundingBox(-2, 0, 0, 4, 4);
    result.getGroup().setFill(getColorDefinition(Color.WHITE).getId());
    result.setBoundingBox(boundingBox);
    Ellipse ellipse = new Ellipse();
    ellipse.setCx(new RelAbsVector(0, 0));
    ellipse.setCy(new RelAbsVector(0, 0));
    ellipse.setRx(new RelAbsVector(4, 0));
    ellipse.setRy(new RelAbsVector(4, 0));
    result.getGroup().addElement(ellipse);
  }

  private void createOpenLineEnding(final LineEnding result) {
    BoundingBox boundingBox = createBoundingBox(-12, -6, 0, 12, 12);
    result.setBoundingBox(boundingBox);
    Polygon polygon = new Polygon();
    polygon.addElement(createRenderPoint(0, 0));
    polygon.addElement(createRenderPoint(100, 50));
    polygon.addElement(createRenderPoint(0, 100));
    polygon.addElement(createRenderPoint(100, 50));
    result.getGroup().addElement(polygon);
  }

  private void createDiamondLineEnding(final LineEnding result) {
    BoundingBox boundingBox = createBoundingBox(-18, -6, 0, 18, 12);
    result.setBoundingBox(boundingBox);
    result.getGroup().setFill(getColorDefinition(Color.WHITE).getId());
    Polygon polygon = new Polygon();
    polygon.addElement(createRenderPoint(50, 0));
    polygon.addElement(createRenderPoint(100, 50));
    polygon.addElement(createRenderPoint(50, 100));
    polygon.addElement(createRenderPoint(0, 50));
    polygon.addElement(createRenderPoint(50, 0));
    result.getGroup().addElement(polygon);
  }

  private void createCrossBarLineEnding(final LineEnding result) {
    BoundingBox boundingBox = createBoundingBox(0, -6, 0, 1, 12);
    result.setBoundingBox(boundingBox);
    Polygon crossBar = new Polygon();
    crossBar.addElement(createRenderPoint(0, 0));
    crossBar.addElement(createRenderPoint(0, 100));
    result.getGroup().addElement(crossBar);
  }

  private void createBlankLineEnding(final LineEnding result) {
    createFullLineEnding(result);
    result.getGroup().setFill(getColorDefinition(Color.WHITE).getId());
  }

  private void createFullLineEnding(final LineEnding result) {
    BoundingBox boundingBox = createBoundingBox(-12, -6, 0, 12, 12);
    result.setBoundingBox(boundingBox);
    result.getGroup().setFill(getColorDefinition(Color.BLACK).getId());
    Polygon polygon = new Polygon();
    polygon.addElement(createRenderPoint(0, 0));
    polygon.addElement(createRenderPoint(100, 50));
    polygon.addElement(createRenderPoint(0, 100));
    polygon.addElement(createRenderPoint(0, 0));
    result.getGroup().addElement(polygon);
  }

  private void createBlankCrossBarLineEnding(final LineEnding result) {
    BoundingBox boundingBox = createBoundingBox(-18, -6, 0, 18, 12);
    result.setBoundingBox(boundingBox);
    result.getGroup().setFill(getColorDefinition(Color.WHITE).getId());
    Polygon polygon = new Polygon();
    polygon.addElement(createRenderPoint(33, 0));
    polygon.addElement(createRenderPoint(100, 50));
    polygon.addElement(createRenderPoint(33, 100));
    polygon.addElement(createRenderPoint(33, 0));
    result.getGroup().addElement(polygon);

    Polygon crossBar = new Polygon();
    crossBar.addElement(createRenderPoint(0, 0));
    crossBar.addElement(createRenderPoint(0, 100));
    result.getGroup().addElement(crossBar);
  }

  private void createFullCrossBarLineEnding(final LineEnding result) {
    createBlankCrossBarLineEnding(result);
    result.getGroup().setFill(getColorDefinition(Color.BLACK).getId());
  }

  protected BoundingBox createBoundingBox(final double x, final double y, final int z, final double w, final double h) {
    BoundingBox boundingBox = new BoundingBox();

    boundingBox.setPosition(new Point(x, y, z));
    Dimensions dimensions = new Dimensions();

    dimensions.setWidth(w);
    dimensions.setHeight(h);
    boundingBox.setDimensions(dimensions);
    return boundingBox;
  }

  protected RenderPoint createRenderPoint(final double percentX, final int percentY) {
    RenderPoint result = new RenderPoint();
    result.setX(new RelAbsVector(0, Math.round(percentX)));
    result.setY(new RelAbsVector(0, Math.round(percentY)));
    return result;
  }

  public static LogMarker createMarker(final ProjectLogEntryType type, final AbstractNamedSBase element) {
    if (element == null) {
      return new LogMarker(type, "", "N/A", "N/A");
    } else if (element.getModel() == null) {
      return new LogMarker(type, element.getClass().getSimpleName(), element.getId(), "");
    } else {
      return new LogMarker(type, element.getClass().getSimpleName(), element.getId(), element.getModel().getName());
    }
  }

}
