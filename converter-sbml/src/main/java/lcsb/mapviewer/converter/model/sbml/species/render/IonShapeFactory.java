package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.model.map.species.Ion;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.Collections;
import java.util.List;

public class IonShapeFactory extends AShapeFactory<Ion> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return Collections.singletonList(createEllipseShape());
  }
}
