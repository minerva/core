package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.converter.model.sbml.extension.render.CustomRenderPoint;
import lcsb.mapviewer.model.map.Drawable;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;
import org.sbml.jsbml.ext.render.RelAbsVector;

import java.util.Collections;
import java.util.List;

public class BlankArrowShapeFactory extends AShapeFactory<Drawable> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {

    final Polygon polygon = new Polygon();
    createRelativePoint(polygon, 0, 50);
    double s = Math.sin(0.875 * Math.PI);
    double c = Math.cos(0.875 * Math.PI);

    final CustomRenderPoint p0 = new CustomRenderPoint();
    p0.setX(new RelAbsVector(0, 100 * (1 + c)));
    p0.setY(new RelAbsVector(0, 50));

    final CustomRenderPoint p1 = new CustomRenderPoint();
    p1.setX(new RelAbsVector(0, 100 * (1 + c)));
    p1.setY(new RelAbsVector(0, 50));
    p1.setWidthRelative(s * 100);


    final CustomRenderPoint p2 = new CustomRenderPoint();
    p2.setX(new RelAbsVector(0, 100));
    p2.setY(new RelAbsVector(0, 50));

    final CustomRenderPoint p3 = new CustomRenderPoint();
    p3.setX(new RelAbsVector(0, 100 * (1 + c)));
    p3.setY(new RelAbsVector(0, 50));
    p3.setWidthRelative(-s * 100);

    polygon.addElement(p0);
    polygon.addElement(p1);
    polygon.addElement(p2);
    polygon.addElement(p3);
    polygon.addElement(p0.clone());
    polygon.addElement(p3.clone());
    polygon.addElement(p2.clone());
    polygon.addElement(p1.clone());
    polygon.addElement(p0.clone());

    return Collections.singletonList(polygon);
  }

}
