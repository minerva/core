package lcsb.mapviewer.converter.model.sbml.reaction.render;

import lcsb.mapviewer.model.map.reaction.type.ReducedModulationReaction;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.List;

public class ReducedModulationReactionShapeFactory extends ReactionShapeFactory<ReducedModulationReaction> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return super.getShapeDefinition(ReducedModulationReaction.class);
  }
}
