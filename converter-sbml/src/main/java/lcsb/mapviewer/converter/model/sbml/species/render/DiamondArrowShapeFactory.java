package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.converter.model.sbml.extension.render.CustomRenderPoint;
import lcsb.mapviewer.model.map.Drawable;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;
import org.sbml.jsbml.ext.render.RelAbsVector;

import java.util.Collections;
import java.util.List;

public class DiamondArrowShapeFactory extends AShapeFactory<Drawable> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {

    final Polygon polygon = new Polygon();
    createRelativePoint(polygon, 0, 50);

    final CustomRenderPoint p1 = new CustomRenderPoint();
    p1.setX(new RelAbsVector(0, 50));
    p1.setY(new RelAbsVector(0, 75));


    final CustomRenderPoint p2 = new CustomRenderPoint();
    p2.setX(new RelAbsVector(0, 100));
    p2.setY(new RelAbsVector(0, 50));

    final CustomRenderPoint p3 = new CustomRenderPoint();
    p3.setX(new RelAbsVector(0, 50));
    p3.setY(new RelAbsVector(0, 25));

    polygon.addElement(p1);
    polygon.addElement(p2);
    polygon.addElement(p3);

    createRelativePoint(polygon, 0, 50);

    polygon.addElement(p3.clone());
    polygon.addElement(p2.clone());
    polygon.addElement(p1.clone());

    return Collections.singletonList(polygon);
  }

}
