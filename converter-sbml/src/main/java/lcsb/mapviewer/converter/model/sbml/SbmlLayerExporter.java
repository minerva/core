package lcsb.mapviewer.converter.model.sbml;

import java.util.Set;

import org.sbml.jsbml.ext.layout.BoundingBox;
import org.sbml.jsbml.ext.layout.Dimensions;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.Point;
import org.sbml.jsbml.ext.layout.TextGlyph;
import org.sbml.jsbml.ext.render.HTextAnchor;
import org.sbml.jsbml.ext.render.LocalStyle;
import org.sbml.jsbml.ext.render.RenderGroup;
import org.sbml.jsbml.ext.render.VTextAnchor;

import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;

public class SbmlLayerExporter {

  private Model minervaModel;
  private Set<SbmlExtension> usedExtensions;
  private int idCounter = 0;

  public SbmlLayerExporter(final lcsb.mapviewer.model.map.model.Model minervaModel, final Set<SbmlExtension> usedExtensions) {
    this.minervaModel = minervaModel;
    this.usedExtensions = usedExtensions;
  }

  public void exportLayers(final org.sbml.jsbml.Model sbmlModel) {
    if (usedExtensions.contains(SbmlExtension.LAYOUT)) {
      SbmlModelUtils sbmlModelUtils = new SbmlModelUtils(sbmlModel);
      Layout layout = sbmlModelUtils.getLayout();
      for (final Layer layer : minervaModel.getLayers()) {
        for (LayerText text : layer.getTexts()) {
          TextGlyph glyph = createGlyph(text, sbmlModelUtils);
          layout.addTextGlyph(glyph);
        }
      }
    }
  }

  private TextGlyph createGlyph(final LayerText text, final SbmlModelUtils sbmlModelUtils) {
    TextGlyph glyph = new TextGlyph("layer_text_" + (idCounter++), SbmlExporter.SUPPORTED_VERSION, SbmlExporter.SUPPORTED_LEVEL);
    glyph.setText(text.getNotes());
    BoundingBox boundingBox = new BoundingBox();
    boundingBox.setPosition(new Point(text.getX(), text.getY(), text.getZ()));
    Dimensions dimensions = new Dimensions();
    dimensions.setWidth(text.getWidth());
    dimensions.setHeight(text.getHeight());
    boundingBox.setDimensions(dimensions);
    glyph.setBoundingBox(boundingBox);

    if (usedExtensions.contains(SbmlExtension.RENDER)) {
      LocalStyle style = sbmlModelUtils.createStyle();
      style.getIDList().add(glyph.getId());
      RenderGroup group = style.getGroup();
      group.setVTextAnchor(VTextAnchor.TOP);
      group.setTextAnchor(HTextAnchor.START);
      group.setStroke(sbmlModelUtils.getColorDefinition(text.getColor()).getId());
    }

    return glyph;
  }

}
