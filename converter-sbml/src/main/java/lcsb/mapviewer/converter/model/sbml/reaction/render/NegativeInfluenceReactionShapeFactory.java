package lcsb.mapviewer.converter.model.sbml.reaction.render;

import lcsb.mapviewer.model.map.reaction.type.NegativeInfluenceReaction;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.List;

public class NegativeInfluenceReactionShapeFactory extends ReactionShapeFactory<NegativeInfluenceReaction> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return super.getShapeDefinition(NegativeInfluenceReaction.class);
  }
}
