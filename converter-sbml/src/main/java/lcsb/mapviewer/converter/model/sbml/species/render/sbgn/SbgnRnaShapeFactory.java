package lcsb.mapviewer.converter.model.sbml.species.render.sbgn;

import lcsb.mapviewer.converter.model.sbml.species.render.AShapeFactory;
import lcsb.mapviewer.model.map.species.Rna;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.Collections;
import java.util.List;

public class SbgnRnaShapeFactory extends AShapeFactory<Rna> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return Collections.singletonList(createRoundedBottomRect(AShapeFactory.RECTANGLE_CORNER_ARC_SIZE));
  }

}
