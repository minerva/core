package lcsb.mapviewer.converter.model.sbml.reaction.render;

import lcsb.mapviewer.model.map.reaction.type.UnknownPositiveInfluenceReaction;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.List;

public class UnknownPositiveInfluenceReactionShapeFactory extends ReactionShapeFactory<UnknownPositiveInfluenceReaction> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return super.getShapeDefinition(UnknownPositiveInfluenceReaction.class);
  }
}
