package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.model.map.species.Gene;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;

import java.util.Collections;
import java.util.List;

public class GeneShapeFactory extends AShapeFactory<Gene> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    final Polygon polygon = new Polygon();
    createRelativePoint(polygon, 0, 0);
    createRelativePoint(polygon, 100, 0);
    createRelativePoint(polygon, 100, 100);
    createRelativePoint(polygon, 0, 100);
    return Collections.singletonList(polygon);
  }

}
