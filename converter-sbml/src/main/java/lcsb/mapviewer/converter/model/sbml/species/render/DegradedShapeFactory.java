package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.model.map.species.Degraded;
import org.sbml.jsbml.ext.render.Ellipse;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;
import org.sbml.jsbml.ext.render.RelAbsVector;

import java.util.ArrayList;
import java.util.List;

public class DegradedShapeFactory extends AShapeFactory<Degraded> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    final List<GraphicalPrimitive1D> result = new ArrayList<>();

    final Ellipse ellipse = new Ellipse();
    ellipse.setCx(new RelAbsVector(0, 50));
    ellipse.setCy(new RelAbsVector(0, 50));
    ellipse.setRx(new RelAbsVector(-DEGRADED_CROSS_LINE_EXTENDED_LENGTH, 50));
    ellipse.setRy(new RelAbsVector(-DEGRADED_CROSS_LINE_EXTENDED_LENGTH, 50));

    result.add(ellipse);

    final Polygon polygon = new Polygon();
    createAbsoluteRelativePoint(polygon, DEGRADED_CROSS_LINE_EXTENDED_LENGTH, 50, 0, 0);
    createAbsoluteRelativePoint(polygon, -DEGRADED_CROSS_LINE_EXTENDED_LENGTH, 50, 0, 100);

    result.add(polygon);

    return result;
  }

}
