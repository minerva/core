package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.model.map.Drawable;
import org.sbml.jsbml.ext.render.Ellipse;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;
import org.sbml.jsbml.ext.render.RelAbsVector;
import org.sbml.jsbml.ext.render.RenderCubicBezier;
import org.sbml.jsbml.ext.render.RenderPoint;

import java.util.List;

public abstract class AShapeFactory<T extends Drawable> {

  public static final double DRUG_OFFSET_BETWEEN_BORDERS = 4;

  public static final double RECTANGLE_CORNER_ARC_SIZE = 5;

  public static final double ION_CHANNEL_WIDTH = 20;

  public static final double COMPLEX_TRIMMED_CORNER_SIZE = 5;

  public static final double DEGRADED_CROSS_LINE_EXTENDED_LENGTH = 7;

  public abstract List<GraphicalPrimitive1D> getShapeDefinition();

  protected Ellipse createEllipseShape() {
    final Ellipse ellipse = new Ellipse();
    ellipse.setCx(new RelAbsVector(0, 50));
    ellipse.setCy(new RelAbsVector(0, 50));
    ellipse.setRx(new RelAbsVector(0, 50));
    ellipse.setRy(new RelAbsVector(0, 50));
    return ellipse;
  }

  protected void createRelativePoint(final Polygon polygon, final double x, final double y) {
    final RenderPoint point = polygon.createRenderPoint();
    point.setX(new RelAbsVector(0, Math.round(x)));
    point.setY(new RelAbsVector(0, Math.round(y)));
  }

  protected Polygon createRoundedRect(final double arcSize) {

    return createRoundedRect(
        new RelAbsVector(0, 0), new RelAbsVector(0, 0),
        new RelAbsVector(0, 100), new RelAbsVector(0, 100),
        arcSize);
  }

  protected Polygon createRoundedRect(
      final RelAbsVector topLeftX, final RelAbsVector topLeftY,
      final RelAbsVector width, final RelAbsVector height,
      final double arcSize) {

    // math taken from https://stackoverflow.com/a/44829356/1127920
    final double k2 = (4.0 / 3.0) * (Math.sqrt(2) - 1);

    final Polygon polygon = new Polygon();

    createAbsoluteRelativePoint(polygon,
        topLeftX.getAbsoluteValue() + arcSize, topLeftX.getRelativeValue(),
        topLeftY.getAbsoluteValue(), topLeftY.getRelativeValue());

    createAbsoluteRelativePoint(polygon,
        topLeftX.getAbsoluteValue() + width.getAbsoluteValue() - arcSize, topLeftX.getRelativeValue() + width.getRelativeValue(),
        topLeftY.getAbsoluteValue(), topLeftY.getRelativeValue());

    RenderCubicBezier result = polygon.createRenderCubicBezier();
    result.setX(new RelAbsVector(topLeftX.getAbsoluteValue() + width.getAbsoluteValue(), topLeftX.getRelativeValue() + width.getRelativeValue()));
    result.setY(new RelAbsVector(topLeftY.getAbsoluteValue() + Math.round(arcSize), topLeftY.getRelativeValue()));

    result.setX1(new RelAbsVector(topLeftX.getAbsoluteValue() + width.getAbsoluteValue() + Math.round(arcSize * (k2 - 1)),
        topLeftX.getRelativeValue() + width.getRelativeValue()));
    result.setY1(new RelAbsVector(topLeftY.getAbsoluteValue(), topLeftY.getRelativeValue()));

    result.setX2(new RelAbsVector(topLeftX.getAbsoluteValue() + width.getAbsoluteValue(), topLeftX.getRelativeValue() + width.getRelativeValue()));
    result.setY2(new RelAbsVector(topLeftY.getAbsoluteValue() + Math.round(arcSize * (1 - k2)), topLeftY.getRelativeValue()));

    createAbsoluteRelativePoint(polygon,
        topLeftX.getAbsoluteValue() + width.getAbsoluteValue(), topLeftX.getRelativeValue() + width.getRelativeValue(),
        topLeftY.getAbsoluteValue() + height.getAbsoluteValue() - arcSize, topLeftY.getRelativeValue() + height.getRelativeValue());

    result = polygon.createRenderCubicBezier();
    result.setX(new RelAbsVector(topLeftX.getAbsoluteValue() + width.getAbsoluteValue() - arcSize,
        topLeftX.getRelativeValue() + width.getRelativeValue()));
    result.setY(new RelAbsVector(topLeftY.getAbsoluteValue() + height.getAbsoluteValue(),
        topLeftY.getRelativeValue() + height.getRelativeValue()));

    result.setX1(new RelAbsVector(topLeftX.getAbsoluteValue() + width.getAbsoluteValue(),
        topLeftX.getRelativeValue() + width.getRelativeValue()));
    result.setY1(new RelAbsVector(topLeftY.getAbsoluteValue() + height.getAbsoluteValue() + Math.round(arcSize * (k2 - 1)),
        topLeftY.getRelativeValue() + height.getRelativeValue()));

    result.setX2(new RelAbsVector(topLeftX.getAbsoluteValue() + width.getAbsoluteValue() + Math.round(arcSize * (k2 - 1)),
        topLeftX.getRelativeValue() + width.getRelativeValue()));
    result.setY2(new RelAbsVector(topLeftY.getAbsoluteValue() + height.getAbsoluteValue(),
        topLeftY.getRelativeValue() + height.getRelativeValue()));

    createAbsoluteRelativePoint(polygon,
        topLeftX.getAbsoluteValue() + arcSize, topLeftX.getRelativeValue(),
        topLeftY.getAbsoluteValue() + height.getAbsoluteValue(), topLeftY.getRelativeValue() + height.getRelativeValue());

    result = polygon.createRenderCubicBezier();
    result.setX(new RelAbsVector(topLeftX.getAbsoluteValue(), topLeftX.getRelativeValue()));
    result.setY(new RelAbsVector(topLeftY.getAbsoluteValue() + height.getAbsoluteValue() - arcSize,
        topLeftY.getRelativeValue() + height.getRelativeValue()));

    result.setX1(new RelAbsVector(topLeftX.getAbsoluteValue() + Math.round(arcSize * (1 - k2)),
        topLeftX.getRelativeValue()));
    result.setY1(new RelAbsVector(topLeftY.getAbsoluteValue() + height.getAbsoluteValue(),
        topLeftY.getRelativeValue() + height.getRelativeValue()));

    result.setX2(new RelAbsVector(topLeftX.getAbsoluteValue(), topLeftX.getRelativeValue()));
    result.setY2(new RelAbsVector(topLeftY.getAbsoluteValue() + height.getAbsoluteValue() + Math.round(arcSize * (k2 - 1)),
        topLeftY.getRelativeValue() + height.getRelativeValue()));

    createAbsoluteRelativePoint(polygon,
        topLeftX.getAbsoluteValue(), topLeftX.getRelativeValue(),
        topLeftY.getAbsoluteValue() + arcSize, topLeftY.getRelativeValue());

    result = polygon.createRenderCubicBezier();
    result.setX(new RelAbsVector(topLeftX.getAbsoluteValue() + Math.round(arcSize), topLeftX.getRelativeValue()));
    result.setY(new RelAbsVector(topLeftY.getAbsoluteValue(), topLeftY.getRelativeValue()));

    result.setX1(new RelAbsVector(topLeftX.getAbsoluteValue(), topLeftX.getRelativeValue()));
    result.setY1(new RelAbsVector(topLeftY.getAbsoluteValue() + Math.round(arcSize * (1 - k2)), topLeftY.getRelativeValue()));

    result.setX2(new RelAbsVector(topLeftX.getAbsoluteValue() + Math.round(arcSize * (1 - k2)), topLeftX.getRelativeValue()));
    result.setY2(new RelAbsVector(topLeftY.getAbsoluteValue(), topLeftY.getRelativeValue()));
    return polygon;
  }

  protected void createAbsoluteRelativePoint(
      final Polygon polygon,
      final double absoluteX,
      final double relativeX,
      final double absoluteY,
      final double relativeY) {
    final RenderPoint p1 = polygon.createRenderPoint();
    p1.setX(new RelAbsVector(Math.round(absoluteX), Math.round(relativeX)));
    p1.setY(new RelAbsVector(Math.round(absoluteY), Math.round(relativeY)));
  }

  protected Polygon createRectangleShape(
      final RelAbsVector topLeftX, final RelAbsVector topLeftY,
      final RelAbsVector width, final RelAbsVector height) {

    final Polygon polygon = new Polygon();

    createAbsoluteRelativePoint(polygon,
        topLeftX.getAbsoluteValue(), topLeftX.getRelativeValue(),
        topLeftY.getAbsoluteValue(), topLeftY.getRelativeValue());

    createAbsoluteRelativePoint(polygon,
        topLeftX.getAbsoluteValue() + width.getAbsoluteValue(), topLeftX.getRelativeValue() + width.getRelativeValue(),
        topLeftY.getAbsoluteValue(), topLeftY.getRelativeValue());

    createAbsoluteRelativePoint(polygon,
        topLeftX.getAbsoluteValue() + width.getAbsoluteValue(), topLeftX.getRelativeValue() + width.getRelativeValue(),
        topLeftY.getAbsoluteValue() + height.getAbsoluteValue(), topLeftY.getRelativeValue() + height.getRelativeValue());


    createAbsoluteRelativePoint(polygon,
        topLeftX.getAbsoluteValue(), topLeftX.getRelativeValue(),
        topLeftY.getAbsoluteValue() + height.getAbsoluteValue(), topLeftY.getRelativeValue() + height.getRelativeValue());

    createAbsoluteRelativePoint(polygon,
        topLeftX.getAbsoluteValue(), topLeftX.getRelativeValue(),
        topLeftY.getAbsoluteValue(), topLeftY.getRelativeValue());
    return polygon;
  }

  protected Polygon createRoundedBottomRect(final double arcSize) {

    return createRoundedRectBottom(
        new RelAbsVector(0, 0), new RelAbsVector(0, 0),
        new RelAbsVector(0, 100), new RelAbsVector(0, 100),
        arcSize);
  }

  protected Polygon createRoundedRectBottom(
      final RelAbsVector topLeftX, final RelAbsVector topLeftY,
      final RelAbsVector width, final RelAbsVector height,
      final double arcSize) {

    // math taken from https://stackoverflow.com/a/44829356/1127920
    final double k2 = (4.0 / 3.0) * (Math.sqrt(2) - 1);

    final Polygon polygon = new Polygon();

    createAbsoluteRelativePoint(polygon,
        topLeftX.getAbsoluteValue(), topLeftX.getRelativeValue(),
        topLeftY.getAbsoluteValue(), topLeftY.getRelativeValue());

    createAbsoluteRelativePoint(polygon,
        topLeftX.getAbsoluteValue() + width.getAbsoluteValue(), topLeftX.getRelativeValue() + width.getRelativeValue(),
        topLeftY.getAbsoluteValue(), topLeftY.getRelativeValue());

    createAbsoluteRelativePoint(polygon,
        topLeftX.getAbsoluteValue() + width.getAbsoluteValue(), topLeftX.getRelativeValue() + width.getRelativeValue(),
        topLeftY.getAbsoluteValue() + height.getAbsoluteValue() - arcSize, topLeftY.getRelativeValue() + height.getRelativeValue());

    RenderCubicBezier result = polygon.createRenderCubicBezier();
    result.setX(new RelAbsVector(topLeftX.getAbsoluteValue() + width.getAbsoluteValue() - arcSize,
        topLeftX.getRelativeValue() + width.getRelativeValue()));
    result.setY(new RelAbsVector(topLeftY.getAbsoluteValue() + height.getAbsoluteValue(),
        topLeftY.getRelativeValue() + height.getRelativeValue()));

    result.setX1(new RelAbsVector(topLeftX.getAbsoluteValue() + width.getAbsoluteValue(),
        topLeftX.getRelativeValue() + width.getRelativeValue()));
    result.setY1(new RelAbsVector(topLeftY.getAbsoluteValue() + height.getAbsoluteValue() + Math.round(arcSize * (k2 - 1)),
        topLeftY.getRelativeValue() + height.getRelativeValue()));

    result.setX2(new RelAbsVector(topLeftX.getAbsoluteValue() + width.getAbsoluteValue() + Math.round(arcSize * (k2 - 1)),
        topLeftX.getRelativeValue() + width.getRelativeValue()));
    result.setY2(new RelAbsVector(topLeftY.getAbsoluteValue() + height.getAbsoluteValue(),
        topLeftY.getRelativeValue() + height.getRelativeValue()));

    createAbsoluteRelativePoint(polygon,
        topLeftX.getAbsoluteValue() + arcSize, topLeftX.getRelativeValue(),
        topLeftY.getAbsoluteValue() + height.getAbsoluteValue(), topLeftY.getRelativeValue() + height.getRelativeValue());

    result = polygon.createRenderCubicBezier();
    result.setX(new RelAbsVector(topLeftX.getAbsoluteValue(), topLeftX.getRelativeValue()));
    result.setY(new RelAbsVector(topLeftY.getAbsoluteValue() + height.getAbsoluteValue() - arcSize,
        topLeftY.getRelativeValue() + height.getRelativeValue()));

    result.setX1(new RelAbsVector(topLeftX.getAbsoluteValue() + Math.round(arcSize * (1 - k2)),
        topLeftX.getRelativeValue()));
    result.setY1(new RelAbsVector(topLeftY.getAbsoluteValue() + height.getAbsoluteValue(),
        topLeftY.getRelativeValue() + height.getRelativeValue()));

    result.setX2(new RelAbsVector(topLeftX.getAbsoluteValue(), topLeftX.getRelativeValue()));
    result.setY2(new RelAbsVector(topLeftY.getAbsoluteValue() + height.getAbsoluteValue() + Math.round(arcSize * (k2 - 1)),
        topLeftY.getRelativeValue() + height.getRelativeValue()));
    return polygon;
  }

}
