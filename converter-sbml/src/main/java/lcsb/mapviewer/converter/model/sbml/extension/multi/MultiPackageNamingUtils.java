package lcsb.mapviewer.converter.model.sbml.extension.multi;

import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.AbstractSiteModification;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.ext.multi.MultiSpeciesType;

/**
 * Class responsible for providing identifiers of structures inside multi
 * package.
 *
 * @author Piotr Gawron
 */
public final class MultiPackageNamingUtils {

  public static final String NULL_REPRESENTATION = "NULL";
  private static final String MINERVA_MODIFICATION_TYPE_PREFIX = "minerva_modification_type_";
  /**
   * Default logger.
   */
  @SuppressWarnings("unused")
  private static final Logger logger = LogManager.getLogger();

  /**
   * Returns id of the {@link MultiSpeciesType} for a given minerva class.
   *
   * @param speciesClass minerva {@link Element} class.
   * @return id of the {@link MultiSpeciesType} for a given minerva class
   */
  private static String getSpeciesTypeId(final Class<?> speciesClass) {
    return "minerva_species_type_" + speciesClass.getSimpleName();
  }

  /**
   * Returns id of the {@link MultiSpeciesType} for a given minerva element
   * object.
   *
   * @param element object for which we want to get id
   * @return id of the {@link MultiSpeciesType} for a given minerva object
   */
  public static String getSpeciesTypeId(final Element element) {
    String result = getSpeciesTypeId(element.getClass());

    if (element instanceof Complex) {
      result += "_" + element.getElementId().replaceAll("[^a-zA-Z0-9_]+", "_");
    } else if (element instanceof Species) {
      if (((Species) element).getComplex() != null) {
        result += "_" + element.getElementId().replaceAll("[^a-zA-Z0-9_]+", "_");
      }
    }
    return result;
  }

  public static String getFeatureId(final Element element, final BioEntityFeature feature) {
    return feature.getIdPrefix() + getSpeciesTypeId(element).replace("minerva_species_type_", "");
  }

  public static boolean isFeatureId(final String featureTypeId, final BioEntityFeature feature) {
    return featureTypeId.startsWith(feature.getIdPrefix());
  }

  public static String getModificationFeatureId(final ModificationResidue mr) {
    String stateSuffix = "";
    if (mr instanceof AbstractSiteModification) {
      if (((AbstractSiteModification) mr).getState() != null) {
        stateSuffix = "_" + ((AbstractSiteModification) mr).getState().name();
      } else {
        stateSuffix = "_null";
      }
    } else if (mr instanceof TranscriptionSite) {
      stateSuffix = "_" + ((TranscriptionSite) mr).getActive() + "_" + ((TranscriptionSite) mr).getDirection();
    }

    return MINERVA_MODIFICATION_TYPE_PREFIX + getSpeciesTypeId(mr.getSpecies()).replace("minerva_species_type_", "") + "_"
        + mr.getClass().getSimpleName() + stateSuffix;
  }

  public static boolean isModificationFeatureId(final String featureTypeString) {
    return featureTypeString.startsWith(MINERVA_MODIFICATION_TYPE_PREFIX);
  }

  public static boolean isModificationFeatureId(final String speciesFeatureType,
                                                final Class<? extends ModificationResidue> class1) {
    return isModificationFeatureId(speciesFeatureType) && speciesFeatureType.contains("_" + class1.getSimpleName());
  }

  public static ModificationState getModificationStateFromFeatureTypeName(final String featureTypeString) {
    for (final ModificationState state : ModificationState.values()) {
      if (featureTypeString.endsWith(state.name())) {
        return state;
      }
    }
    return null;
  }

  public static Boolean getTranscriptionFactorActiveStateFromFeatureTypeName(final String featureTypeString) {
    final String[] tmp = featureTypeString.split("_");
    Boolean result = null;
    if (tmp.length >= 2) {
      if (tmp[tmp.length - 2].equalsIgnoreCase("TRUE")) {
        result = true;
      } else if (tmp[tmp.length - 2].equalsIgnoreCase("FALSE")) {
        result = false;
      }
    }
    return result;
  }

  public static String getTranscriptionFactorDirectionStateFromFeatureTypeName(final String featureTypeString) {
    final String[] tmp = featureTypeString.split("_");
    String result = null;
    if (tmp.length >= 1) {
      result = tmp[tmp.length - 1];
    }
    return result;
  }

}
