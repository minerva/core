package lcsb.mapviewer.converter.model.sbml.species;

import lcsb.mapviewer.converter.model.sbml.SbmlElementExporter;
import lcsb.mapviewer.converter.model.sbml.SbmlExtension;
import lcsb.mapviewer.converter.model.sbml.compartment.SbmlCompartmentExporter;
import lcsb.mapviewer.converter.model.sbml.extension.multi.BioEntityFeature;
import lcsb.mapviewer.converter.model.sbml.extension.multi.MultiPackageNamingUtils;
import lcsb.mapviewer.converter.model.sbml.species.render.ShapeFactory;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.sbo.SBOTermSpeciesType;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.AbstractSiteModification;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;
import lcsb.mapviewer.model.map.species.field.RegulatoryRegion;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationResidue;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.AbstractReferenceGlyph;
import org.sbml.jsbml.ext.layout.GeneralGlyph;
import org.sbml.jsbml.ext.layout.ReferenceGlyph;
import org.sbml.jsbml.ext.layout.TextGlyph;
import org.sbml.jsbml.ext.multi.MultiModelPlugin;
import org.sbml.jsbml.ext.multi.MultiSpeciesPlugin;
import org.sbml.jsbml.ext.multi.MultiSpeciesType;
import org.sbml.jsbml.ext.multi.PossibleSpeciesFeatureValue;
import org.sbml.jsbml.ext.multi.SpeciesFeature;
import org.sbml.jsbml.ext.multi.SpeciesFeatureType;
import org.sbml.jsbml.ext.multi.SpeciesFeatureValue;
import org.sbml.jsbml.ext.multi.SpeciesTypeInstance;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.LocalStyle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SbmlSpeciesExporter extends SbmlElementExporter<Species, org.sbml.jsbml.Species> {

  private static int idCounter = 0;

  /**
   * Default class logger.
   */
  private static final Logger logger = LogManager.getLogger();

  private final SbmlCompartmentExporter compartmentExporter;
  private int modificationResidueCounter = 0;
  private final Map<String, String> modificationResidueIds = new HashMap<>();

  private final ModificationResidueExporter modificationResidueExporter;

  private final ShapeFactory shapeFactory = new ShapeFactory();

  public SbmlSpeciesExporter(final Model sbmlModel,
                             final lcsb.mapviewer.model.map.model.Model minervaModel,
                             final Collection<SbmlExtension> sbmlExtensions,
                             final SbmlCompartmentExporter compartmentExporter) {
    super(sbmlModel, minervaModel, sbmlExtensions);
    this.compartmentExporter = compartmentExporter;
    this.modificationResidueExporter = new ModificationResidueExporter(sbmlModel);
  }

  private void assignMultiExtensionData(final Species element, final org.sbml.jsbml.Species result) {
    final MultiSpeciesPlugin multiExtension = new MultiSpeciesPlugin(result);
    final MultiSpeciesType speciesType = getMultiSpeciesType(element);
    multiExtension.setSpeciesType(speciesType.getId());
    result.addExtension("multi", multiExtension);
    assignPostionToCompartmentToMulti(element, multiExtension, speciesType);
    assignElementModificationResiduesToMulti(element, multiExtension, speciesType);
    assignListOfSynonymsToMulti(element, multiExtension, speciesType);
    assignListOfFormerSymbolsToMulti(element, multiExtension, speciesType);
    assignSymbolToMulti(element, multiExtension, speciesType);
    assignStatePrefixToMulti(element, multiExtension, speciesType);
    assignStateSuffixToMulti(element, multiExtension, speciesType);
    assignFullNameToMulti(element, multiExtension, speciesType);
    assignFormulaToMulti(element, multiExtension, speciesType);
    assignDimerToMulti(element, multiExtension, speciesType);
    assignHypotheticalToMulti(element, multiExtension, speciesType);
    assignAbbreviationToMulti(element, multiExtension, speciesType);
    assignChargeToMulti(element, multiExtension, speciesType);
    assignActivityToMulti(element, multiExtension, speciesType);
  }

  private void assignElementModificationResiduesToMulti(final Species element, final MultiSpeciesPlugin multiExtension,
                                                        final MultiSpeciesType speciesType) {
    if (element instanceof SpeciesWithModificationResidue) {
      assignModificationResiduesToMulti(((SpeciesWithModificationResidue) element).getModificationResidues(), multiExtension, speciesType);
    }

  }

  private void assignModificationResiduesToMulti(final Collection<ModificationResidue> modificationResidues,
                                                 final MultiSpeciesPlugin multiExtension, final MultiSpeciesType speciesType) {
    for (final ModificationResidue mr : modificationResidues) {
      SpeciesFeatureType feature = null;
      final PossibleSpeciesFeatureValue value;

      if (mr instanceof BindingRegion) {
        feature = getBindingRegionFeature((BindingRegion) mr, speciesType);
      } else if (mr instanceof CodingRegion) {
        feature = getCodingRegionFeature((CodingRegion) mr, speciesType);
      } else if (mr instanceof ProteinBindingDomain) {
        feature = getProteinBindingDomainFeature((ProteinBindingDomain) mr, speciesType);
      } else if (mr instanceof RegulatoryRegion) {
        feature = getRegulatoryRegionFeature((RegulatoryRegion) mr, speciesType);
      } else if (mr instanceof TranscriptionSite) {
        feature = getTranscriptionSiteFeature((TranscriptionSite) mr, speciesType);
      } else if (mr instanceof ModificationSite) {
        feature = getModificationSiteFeature((ModificationSite) mr, speciesType);
      } else if (mr instanceof Residue) {
        feature = getResidueFeature((Residue) mr, speciesType);
      } else if (mr instanceof StructuralState) {
        feature = getStructuralStateFeature((StructuralState) mr, speciesType);
      } else {
        logger.warn("Don't know how to export modification: " + mr.getClass());
      }
      value = getPosibleFeatureIdByName(mr.getName(), feature);
      final SpeciesFeatureValue featureValue = addSpeciesFeatureValue(multiExtension, feature, value);
      featureValue.setId(getModificationResidueUniqueId(mr));
    }
  }

  private SpeciesFeatureType getStructuralStateFeature(final StructuralState mr, final MultiSpeciesType speciesType) {
    final String featureId = MultiPackageNamingUtils.getFeatureId(mr.getSpecies(), BioEntityFeature.STRUCTURAL_STATE);
    String featureName = "";
    if (mr.getName() != null) {
      featureName = mr.getName();
    }
    return getOrCreateFeatureById(featureId, featureName, speciesType);
  }

  private SpeciesFeatureType getCodingRegionFeature(final CodingRegion mr, final MultiSpeciesType speciesType) {
    final String featureId = MultiPackageNamingUtils.getModificationFeatureId(mr);
    final String featureName = "Coding region";
    return getOrCreateFeatureById(featureId, featureName, speciesType);
  }

  private SpeciesFeatureType getBindingRegionFeature(final BindingRegion bindingRegion, final MultiSpeciesType speciesType) {
    final String featureId = MultiPackageNamingUtils.getModificationFeatureId(bindingRegion);
    final String featureName = "Binding region";
    return getOrCreateFeatureById(featureId, featureName, speciesType);
  }

  private SpeciesFeatureType getProteinBindingDomainFeature(final ProteinBindingDomain proteinBindingDomain,
                                                            final MultiSpeciesType speciesType) {
    final String featureId = MultiPackageNamingUtils.getModificationFeatureId(proteinBindingDomain);
    final String featureName = "Protein binding domain";
    return getOrCreateFeatureById(featureId, featureName, speciesType);
  }

  private SpeciesFeatureType getRegulatoryRegionFeature(final RegulatoryRegion regulatoryRegion,
                                                        final MultiSpeciesType speciesType) {
    final String featureId = MultiPackageNamingUtils.getModificationFeatureId(regulatoryRegion);
    final String featureName = "Regulatory region";
    return getOrCreateFeatureById(featureId, featureName, speciesType);
  }

  private SpeciesFeatureType getTranscriptionSiteFeature(final TranscriptionSite transcriptionSite,
                                                         final MultiSpeciesType speciesType) {
    final String featureId = MultiPackageNamingUtils.getModificationFeatureId(transcriptionSite);
    final String featureName = "Transcription site";
    return getOrCreateFeatureById(featureId, featureName, speciesType);
  }

  private SpeciesFeatureType getModificationSiteFeature(final ModificationSite modificationSite,
                                                        final MultiSpeciesType speciesType) {
    final String featureId = MultiPackageNamingUtils.getModificationFeatureId(modificationSite);
    String featureName = "";
    if (modificationSite.getState() != null) {
      featureName = modificationSite.getState().getFullName();
    }
    return getOrCreateFeatureById(featureId, featureName, speciesType);
  }

  private SpeciesFeatureType getResidueFeature(final Residue residue, final MultiSpeciesType speciesType) {
    final String featureId = MultiPackageNamingUtils.getModificationFeatureId(residue);
    String featureName = "";
    if (residue.getState() != null) {
      featureName = residue.getState().getFullName();
    }
    return getOrCreateFeatureById(featureId, featureName, speciesType);
  }

  private SpeciesFeatureType getOrCreateFeatureById(final String featureId, final String featureName,
                                                    final MultiSpeciesType speciesType) {
    SpeciesFeatureType feature = speciesType.getSpeciesFeatureType(featureId);
    if (feature == null) {
      feature = new SpeciesFeatureType();
      feature.setName(featureName);
      feature.setId(featureId);
      feature.setOccur(1);
      speciesType.getListOfSpeciesFeatureTypes().add(feature);
    }
    return feature;
  }

  private SpeciesFeatureValue addSpeciesFeatureValue(final MultiSpeciesPlugin multiExtension, final SpeciesFeatureType fetureType,
                                                     final PossibleSpeciesFeatureValue possibleValue) {
    SpeciesFeature feature = null;
    for (final SpeciesFeature existingFeature : multiExtension.getListOfSpeciesFeatures()) {
      if (existingFeature.getSpeciesFeatureType().equals(fetureType.getId())) {
        feature = existingFeature;
      }
    }
    if (feature == null) {
      feature = multiExtension.createSpeciesFeature();
      feature.setSpeciesFeatureType(fetureType.getId());
      feature.setOccur(1);
    }
    final SpeciesFeatureValue value = new SpeciesFeatureValue();
    value.setValue(possibleValue.getId());
    feature.addSpeciesFeatureValue(value);
    return value;
  }

  private PossibleSpeciesFeatureValue getPosibleFeatureIdByName(final String featureValueName,
                                                                final SpeciesFeatureType speciesFeature) {
    PossibleSpeciesFeatureValue result = null;
    for (final PossibleSpeciesFeatureValue value : speciesFeature.getListOfPossibleSpeciesFeatureValues()) {
      if (value.getName().equals(featureValueName)) {
        result = value;
      }
    }
    if (result == null) {
      result = addPosibleValueToFeature(speciesFeature, featureValueName);
    }
    return result;
  }

  private void assignPostionToCompartmentToMulti(final Species element, final MultiSpeciesPlugin multiExtension,
                                                 final MultiSpeciesType speciesType) {
    String value = MultiPackageNamingUtils.NULL_REPRESENTATION;
    if (element.getPositionToCompartment() != null) {
      value = element.getPositionToCompartment().getStringName();
    }
    assignValueToFeature(element, multiExtension, speciesType, value, BioEntityFeature.POSITION_TO_COMPARTMENT);
  }

  private void assignListOfSynonymsToMulti(final Species element, final MultiSpeciesPlugin multiExtension,
                                           final MultiSpeciesType speciesType) {
    for (final String synonym : element.getSynonyms()) {
      assignValueToFeature(element, multiExtension, speciesType, synonym, BioEntityFeature.SYNONYM);
    }
  }

  private void assignListOfFormerSymbolsToMulti(final Species element, final MultiSpeciesPlugin multiExtension,
                                                final MultiSpeciesType speciesType) {
    for (final String formerSymbol : element.getFormerSymbols()) {
      assignValueToFeature(element, multiExtension, speciesType, formerSymbol, BioEntityFeature.FORMER_SYMBOL);
    }
  }

  private void assignSymbolToMulti(final Species element, final MultiSpeciesPlugin multiExtension,
                                   final MultiSpeciesType speciesType) {
    if (element.getSymbol() != null) {
      assignValueToFeature(element, multiExtension, speciesType, element.getSymbol(), BioEntityFeature.SYMBOL);
    }
  }

  private void assignStatePrefixToMulti(final Species element, final MultiSpeciesPlugin multiExtension,
                                        final MultiSpeciesType speciesType) {
    if (element.getStatePrefix() != null) {
      assignValueToFeature(element, multiExtension, speciesType, element.getStatePrefix(),
          BioEntityFeature.STATE_PREFIX);
    }
  }

  private void assignStateSuffixToMulti(final Species element, final MultiSpeciesPlugin multiExtension,
                                        final MultiSpeciesType speciesType) {
    if (element.getStateLabel() != null) {
      assignValueToFeature(element, multiExtension, speciesType, element.getStateLabel(),
          BioEntityFeature.STATE_SUFFIX);
    }
  }

  private void assignFullNameToMulti(final Species element, final MultiSpeciesPlugin multiExtension,
                                     final MultiSpeciesType speciesType) {
    if (element.getFullName() != null) {
      assignValueToFeature(element, multiExtension, speciesType, element.getFullName(), BioEntityFeature.FULL_NAME);
    }
  }

  private void assignFormulaToMulti(final Species element, final MultiSpeciesPlugin multiExtension,
                                    final MultiSpeciesType speciesType) {
    if (element.getFormula() != null) {
      assignValueToFeature(element, multiExtension, speciesType, element.getFormula(), BioEntityFeature.FORMULA);
    }
  }

  private void assignDimerToMulti(final Species element, final MultiSpeciesPlugin multiExtension,
                                  final MultiSpeciesType speciesType) {
    if (element.getHomodimer() != 1) {
      assignValueToFeature(element, multiExtension, speciesType, element.getHomodimer() + "", BioEntityFeature.DIMER);
    }
  }

  private void assignChargeToMulti(final Species element, final MultiSpeciesPlugin multiExtension,
                                   final MultiSpeciesType speciesType) {
    if (element.getCharge() != null) {
      assignValueToFeature(element, multiExtension, speciesType, element.getCharge().toString(),
          BioEntityFeature.CHARGE);
    }
  }

  private void assignHypotheticalToMulti(final Species element, final MultiSpeciesPlugin multiExtension,
                                         final MultiSpeciesType speciesType) {
    if (element.getHypothetical() != null) {
      assignValueToFeature(element, multiExtension, speciesType, element.getHypothetical().toString(),
          BioEntityFeature.HYPOTHETICAL);
    }
  }

  private void assignActivityToMulti(final Species element, final MultiSpeciesPlugin multiExtension,
                                     final MultiSpeciesType speciesType) {
    if (element.getActivity() != null) {
      assignValueToFeature(element, multiExtension, speciesType, element.getActivity().toString(),
          BioEntityFeature.ACTIVITY);
    }
  }

  private void assignAbbreviationToMulti(final Species element, final MultiSpeciesPlugin multiExtension,
                                         final MultiSpeciesType speciesType) {
    if (element.getAbbreviation() != null) {
      assignValueToFeature(element, multiExtension, speciesType, element.getAbbreviation(),
          BioEntityFeature.ABBREVIATION);
    }
  }

  private void assignValueToFeature(final Species element, final MultiSpeciesPlugin multiExtension, final MultiSpeciesType speciesType,
                                    final String value, final BioEntityFeature feature) {
    final SpeciesFeatureType structuralStateFeature = getFeature(element, speciesType, feature);
    final PossibleSpeciesFeatureValue structuralStateFeatureValue = getPosibleFeatureIdByName(value, structuralStateFeature);

    addSpeciesFeatureValue(multiExtension, structuralStateFeature, structuralStateFeatureValue);
  }

  private MultiSpeciesType getMultiSpeciesType(final Species element) {
    MultiSpeciesType speciesType = getMultiPlugin().getSpeciesType(MultiPackageNamingUtils.getSpeciesTypeId(element));
    if (speciesType == null) {
      speciesType = createSpeciesTypeForClass(getMultiPlugin(), element);
    }
    return speciesType;
  }

  @Override
  protected List<Species> getElementList() {
    return getMinervaModel().getSpeciesList();
  }

  @Override
  public org.sbml.jsbml.Species createSbmlElement(final Species element) throws InconsistentModelException {
    final org.sbml.jsbml.Species result = getSbmlModel().createSpecies("species_" + (getNextId()));
    result.setSBOTerm(SBOTermSpeciesType.getTermByType(element));
    result.setCompartment(compartmentExporter.getSbmlElement(element.getCompartment()));
    if (element.getInitialAmount() != null) {
      result.setInitialAmount(element.getInitialAmount());
    }
    if (element.getInitialConcentration() != null) {
      result.setInitialConcentration(element.getInitialConcentration());
    }
    if (element.getInitialAmount() == null && element.getInitialConcentration() == null) {
      result.setInitialConcentration(0);
    }
    if (element.hasOnlySubstanceUnits() != null) {
      result.setHasOnlySubstanceUnits(element.hasOnlySubstanceUnits());
    }
    if (element.getBoundaryCondition() != null) {
      result.setBoundaryCondition(element.getBoundaryCondition());
    } else {
      result.setBoundaryCondition(false);
    }
    if (element.getConstant() != null) {
      result.setConstant(element.getConstant());
    } else {
      result.setConstant(false);
    }
    if (isExtensionEnabled(SbmlExtension.MULTI)) {
      assignMultiExtensionData(element, result);
    }
    return result;
  }

  @Override
  protected String getSbmlIdKey(final Species element) {
    String compartmentName = null;
    String complexName = null;
    if (element.getCompartment() != null) {
      compartmentName = element.getCompartment().getName();
    }
    if (element.getComplex() != null) {
      // complexName = element.getComplex().getName();
      complexName = element.getElementId();
    }

    final List<String> annotations = new ArrayList<>();
    for (final MiriamData md : element.getMiriamData()) {
      annotations.add(md.toString());
    }
    Collections.sort(annotations);
    final String annotationNames = StringUtils.join(annotations, ",");

    String multiDistinguisher = "";
    if (isExtensionEnabled(SbmlExtension.MULTI)) {
      if (element instanceof SpeciesWithModificationResidue) {
        final List<String> modifications = new ArrayList<>();
        for (final ModificationResidue mr : ((SpeciesWithModificationResidue) element).getModificationResidues()) {
          String modificationId = mr.getName() + mr.getClass().getSimpleName();
          if (mr instanceof AbstractSiteModification) {
            modificationId += ((AbstractSiteModification) mr).getState();
          }
          modifications.add(modificationId);
        }
        multiDistinguisher += "\n" + StringUtils.join(modifications, ",");
      }
      multiDistinguisher += "\n" + element.getPositionToCompartment();
      if (element instanceof Complex) {
        final List<String> complexChildIds = new ArrayList<>();
        for (final Species child : ((Complex) element).getAllChildren()) {
          complexChildIds.add(getSbmlIdKey(child));
        }
        Collections.sort(complexChildIds);
        multiDistinguisher += "\n" + StringUtils.join(complexChildIds, "\n");
      }
      multiDistinguisher += "\n" + element.getActivity();
      multiDistinguisher += "\n" + element.getCharge();
      multiDistinguisher += "\n" + element.getHomodimer();
      if (element instanceof Complex) {
        multiDistinguisher += "\n" + element.getElementId();
      }
    }
    String result = element.getClass().getSimpleName() + "\n" + annotationNames + "\n" + element.getName() + "\n"
        + compartmentName + "\n"
        + complexName
        + "\n" + multiDistinguisher;
    if (element instanceof Degraded) {
      result += element.getElementId();
    }
    return result;
  }

  @Override
  protected AbstractReferenceGlyph createElementGlyph(final String sbmlElementId, final String glyphId) {
    final AbstractReferenceGlyph speciesGlyph = getLayout().createSpeciesGlyph(glyphId, sbmlElementId);
    return speciesGlyph;
  }

  @Override
  protected LocalStyle createStyle(final Species element) {
    final LocalStyle style = super.createStyle(element);
    style.getGroup().setStrokeWidth(element.getLineWidth());
    style.getGroup().setStroke(getColorDefinition(element.getBorderColor()).getId());

    final List<GraphicalPrimitive1D> shapes = shapeFactory.createSbmlShapeForElement(element);
    for (final GraphicalPrimitive1D shape : shapes) {
      style.getGroup().addElement(shape);
    }

    return style;
  }

  @Override
  protected void assignLayoutToGlyph(final Species element, final AbstractReferenceGlyph speciesGlyph) {
    super.assignLayoutToGlyph(element, speciesGlyph);
    if (isExtensionEnabled(SbmlExtension.MULTI)) {
      if (element instanceof SpeciesWithModificationResidue) {
        for (final ModificationResidue mr : ((SpeciesWithModificationResidue) element).getModificationResidues()) {
          if (mr instanceof StructuralState) {
            createStructuralStateGlyph((StructuralState) mr, speciesGlyph);
          } else {
            createModificationGlyph(mr, speciesGlyph);
          }
        }
      }
    }

  }

  private void createStructuralStateGlyph(final StructuralState structuralState, final AbstractReferenceGlyph speciesGlyph) {
    final Species element = structuralState.getSpecies();
    final org.sbml.jsbml.Species sbmlSpecies = getSbmlModel().getSpecies(speciesGlyph.getReference());

    final MultiSpeciesPlugin speciesExtension = (MultiSpeciesPlugin) sbmlSpecies.getExtension("multi");

    final SpeciesFeatureType structuralStateFeature = getFeature(element, getMultiSpeciesType(element), BioEntityFeature.STRUCTURAL_STATE);

    final PossibleSpeciesFeatureValue possibleSpeciesFeatureValue = getPosibleFeatureIdByName(
        structuralState.getName(), structuralStateFeature);

    SpeciesFeature feature = null;
    for (final SpeciesFeature existingFeature : speciesExtension.getListOfSpeciesFeatures()) {
      if (existingFeature.getSpeciesFeatureType().equals(structuralStateFeature.getId())) {
        feature = existingFeature;
      }
    }

    SpeciesFeatureValue modificationFeatureValue = null;
    for (final SpeciesFeatureValue featureValue : feature.getListOfSpeciesFeatureValues()) {
      if (possibleSpeciesFeatureValue.getId().equals(featureValue.getValue())) {
        modificationFeatureValue = featureValue;
      }
    }

    modificationFeatureValue.setId(getModificationResidueUniqueId(structuralState));

    final GeneralGlyph structuralStateGlyph = getLayout().createGeneralGlyph("structural_state_" + (idCounter++),
        modificationFeatureValue.getId());
    final ReferenceGlyph referenceGlyph = new ReferenceGlyph("structural_state_reference_" + (idCounter++));
    referenceGlyph.setGlyph(speciesGlyph.getId());
    referenceGlyph.setBoundingBox(modificationResidueExporter.createBoundingBoxForStructuralState(structuralState));

    structuralStateGlyph.addReferenceGlyph(referenceGlyph);
    structuralStateGlyph
        .setBoundingBox(modificationResidueExporter.createBoundingBoxForStructuralState(structuralState));

    final String str = structuralState.getName();
    final TextGlyph textGlyph = getLayout().createTextGlyph("modification_text_" + (idCounter++), str);
    textGlyph.setGraphicalObject(structuralStateGlyph.getId());

    textGlyph.setBoundingBox(modificationResidueExporter.createBoundingBoxForStructuralStateText(structuralState));
    if (isExtensionEnabled(SbmlExtension.RENDER)) {
      final LocalStyle structuralStateStyle = modificationResidueExporter.createStyle(structuralState);
      assignStyleToGlyph(structuralStateGlyph, structuralStateStyle);

      final LocalStyle structuralStateTextStyle = modificationResidueExporter.createTextStyle(structuralState);
      assignStyleToGlyph(textGlyph, structuralStateTextStyle);
    }

  }

  private MultiSpeciesType createSpeciesTypeForClass(final MultiModelPlugin multiPlugin, final Element element) {
    final Class<?> clazz = element.getClass();
    final MultiSpeciesType speciesType = new MultiSpeciesType();
    speciesType.setName(clazz.getSimpleName());
    speciesType.setId(MultiPackageNamingUtils.getSpeciesTypeId(element));
    if (element instanceof Complex) {
      for (final Species child : ((Complex) element).getElements()) {
        final MultiSpeciesType childType = getMultiSpeciesType(child);
        final SpeciesTypeInstance speciesTypeInstance = new SpeciesTypeInstance();
        speciesTypeInstance.setId("child_" + childType.getId());
        speciesTypeInstance.setSpeciesType(childType.getId());
        speciesType.addSpeciesTypeInstance(speciesTypeInstance);
      }
    }
    multiPlugin.addSpeciesType(speciesType);
    speciesType.setSBOTerm(SBOTermSpeciesType.getTermByType(element));
    return speciesType;
  }

  private SpeciesFeatureType getFeature(final Element element, final MultiSpeciesType speciesType,
                                        final BioEntityFeature bioEntityFeature) {
    SpeciesFeatureType feature = speciesType
        .getSpeciesFeatureType(MultiPackageNamingUtils.getFeatureId(element, bioEntityFeature));
    if (feature == null) {
      feature = new SpeciesFeatureType();
      feature.setName(bioEntityFeature.getFeatureName());
      feature.setId(MultiPackageNamingUtils.getFeatureId(element, bioEntityFeature));
      feature.setOccur(1);
      if (bioEntityFeature.getDefaultValue() != null) {
        addPosibleValueToFeature(feature, bioEntityFeature.getDefaultValue());
      }
      speciesType.getListOfSpeciesFeatureTypes().add(feature);
    }
    return feature;
  }

  private PossibleSpeciesFeatureValue addPosibleValueToFeature(final SpeciesFeatureType feature, final String value) {
    String name = value;
    if (name == null) {
      name = MultiPackageNamingUtils.NULL_REPRESENTATION;
    }

    PossibleSpeciesFeatureValue result = null;
    for (final PossibleSpeciesFeatureValue existingValue : feature.getListOfPossibleSpeciesFeatureValues()) {
      if (existingValue.getName().equals(name)) {
        result = existingValue;
      }
    }
    if (result == null) {
      result = new PossibleSpeciesFeatureValue();
      result.setId(feature.getId() + "_" + (idCounter++));
      result.setName(name);
      feature.getListOfPossibleSpeciesFeatureValues().add(result);
    }
    return result;
  }

  private void createModificationGlyph(final ModificationResidue mr, final AbstractReferenceGlyph speciesGlyph) {
    final org.sbml.jsbml.Species sbmlSpecies = getSbmlModel().getSpecies(speciesGlyph.getReference());

    final MultiSpeciesPlugin speciesExtension = (MultiSpeciesPlugin) sbmlSpecies.getExtension("multi");
    SpeciesFeature feature = null;
    final String featureTypeId = MultiPackageNamingUtils.getModificationFeatureId(mr);
    for (final SpeciesFeature existingFeature : speciesExtension.getListOfSpeciesFeatures()) {
      if (existingFeature.getSpeciesFeatureType().equals(featureTypeId)) {
        feature = existingFeature;
      }
    }

    SpeciesFeatureValue modificationFeatureValue = null;
    for (final SpeciesFeatureValue featureValue : feature.getListOfSpeciesFeatureValues()) {
      if (featureValue.getId().equals(getModificationResidueUniqueId(mr))) {
        modificationFeatureValue = featureValue;
      }
    }

    final GeneralGlyph modificationGlyph = getLayout().createGeneralGlyph("modification_" + (idCounter++),
        modificationFeatureValue.getId());
    final ReferenceGlyph referenceGlyph = new ReferenceGlyph("modification_reference_" + (idCounter++));
    referenceGlyph.setGlyph(speciesGlyph.getId());
    referenceGlyph.setBoundingBox(modificationResidueExporter.createBoundingBoxForModification(mr));

    modificationGlyph.addReferenceGlyph(referenceGlyph);
    modificationGlyph.setBoundingBox(modificationResidueExporter.createBoundingBoxForModification(mr));

    TextGlyph textGlyph = null;
    if (mr instanceof AbstractSiteModification) {
      final ModificationState state = ((AbstractSiteModification) mr).getState();
      if (state != null) {
        final String str = state.getAbbreviation();
        textGlyph = getLayout().createTextGlyph("modification_text_" + (idCounter++), str);
      }
    } else if (mr instanceof CodingRegion || mr instanceof RegulatoryRegion) {
      if (mr.getName() != null && !mr.getName().isEmpty()) {
        final String str = mr.getName();
        textGlyph = getLayout().createTextGlyph("modification_text_" + (idCounter++), str);
      }
    }

    if (textGlyph != null) {
      textGlyph.setGraphicalObject(modificationGlyph.getId());
      if (isExtensionEnabled(SbmlExtension.RENDER)) {
        final LocalStyle style = modificationResidueExporter.createTextStyle(mr);
        assignStyleToGlyph(textGlyph, style);
      }
      textGlyph.setBoundingBox(modificationResidueExporter.createBoundingBoxForModificationText(mr));
    }

    if (isExtensionEnabled(SbmlExtension.RENDER)) {

      final LocalStyle style = modificationResidueExporter.createStyle(mr);

      assignStyleToGlyph(modificationGlyph, style);
    }

  }

  private String getModificationResidueUniqueId(final ModificationResidue mr) {
    final SpeciesWithModificationResidue species = (SpeciesWithModificationResidue) mr.getSpecies();
    final String modificationId = "mod" + species.getModificationResidues().indexOf(mr);
    String result = modificationResidueIds.get(modificationId + "_" + getSbmlIdKey(mr.getSpecies()));

    if (result == null) {
      result = "modification_residue_" + (modificationResidueCounter++);
      modificationResidueIds.put(modificationId + "_" + getSbmlIdKey(mr.getSpecies()), result);
    }
    return result;
  }
}
