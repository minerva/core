package lcsb.mapviewer.converter.model.sbml.reaction.render;

import lcsb.mapviewer.model.map.reaction.type.CatalysisReaction;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.List;

public class CatalysisReactionShapeFactory extends ReactionShapeFactory<CatalysisReaction> {
  
  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return super.getShapeDefinition(CatalysisReaction.class);
  }
}
