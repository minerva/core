package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.model.map.species.Unknown;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.Collections;
import java.util.List;

public class UnknownShapeFactory extends AShapeFactory<Unknown> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return Collections.singletonList(createEllipseShape());
  }
}
