package lcsb.mapviewer.converter.model.sbml.compartment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.AbstractReferenceGlyph;
import org.sbml.jsbml.ext.layout.BoundingBox;
import org.sbml.jsbml.ext.layout.CompartmentGlyph;
import org.sbml.jsbml.ext.layout.Dimensions;
import org.sbml.jsbml.ext.layout.Point;
import org.sbml.jsbml.ext.multi.MultiCompartmentPlugin;
import org.sbml.jsbml.ext.render.Ellipse;
import org.sbml.jsbml.ext.render.LocalStyle;
import org.sbml.jsbml.ext.render.Rectangle;
import org.sbml.jsbml.ext.render.RelAbsVector;

import lcsb.mapviewer.converter.model.sbml.SbmlElementExporter;
import lcsb.mapviewer.converter.model.sbml.SbmlExtension;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.OvalCompartment;

public class SbmlCompartmentExporter extends SbmlElementExporter<Compartment, org.sbml.jsbml.Compartment> {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  public SbmlCompartmentExporter(final Model sbmlModel, final lcsb.mapviewer.model.map.model.Model minervaModel,
      final Collection<SbmlExtension> sbmlExtensions) {
    super(sbmlModel, minervaModel, sbmlExtensions);
  }

  @Override
  protected List<Compartment> getElementList() {
    List<Compartment> result = new ArrayList<>();
    result.addAll(getMinervaModel().getCompartments());
    boolean defaultFound = false;
    for (final Compartment compartment : result) {
      if (getElementId(compartment).equals("default")) {
        defaultFound = true;
      }
    }
    if (!defaultFound) {
      Compartment defaultCompartment = new Compartment("default");
      result.add(defaultCompartment);
    }
    return result;
  }

  @Override
  public org.sbml.jsbml.Compartment createSbmlElement(final Compartment element) throws InconsistentModelException {
    org.sbml.jsbml.Compartment result;
    if (element == null || getElementId(element).equals("default")) {
      result = getSbmlModel().createCompartment("default");
    } else {
      result = getSbmlModel().createCompartment("comp_" + (getNextId()));
    }
    // for now we don't have this information - needed for validation
    result.setConstant(false);
    result.setValue(1.0);
    result.setSpatialDimensions(3);
    if (isExtensionEnabled(SbmlExtension.MULTI)) {
      MultiCompartmentPlugin multiExtension = new MultiCompartmentPlugin(result);
      multiExtension.setIsType(false);
      result.addExtension("multi", multiExtension);
    }
    return result;
  }

  @Override
  public org.sbml.jsbml.Compartment getSbmlElement(final Compartment element) throws InconsistentModelException {
    String mapKey = getSbmlIdKey(element);
    if (element == null && sbmlElementByElementNameAndCompartmentName.get(mapKey) == null) {
      org.sbml.jsbml.Compartment sbmlElement = createSbmlElement(element);
      sbmlElementByElementNameAndCompartmentName.put(mapKey, sbmlElement);
    }
    return super.getSbmlElement(element);
  }

  @Override
  protected String getSbmlIdKey(final Compartment compartment) {
    if (compartment == null || getElementId(compartment).equals("default")) {
      return "default";
    }
    return compartment.getName();
  }

  @Override
  protected AbstractReferenceGlyph createElementGlyph(final String sbmlCompartmentId, final String glyphId) {
    String effectiveGlyphId = glyphId;
    if (sbmlCompartmentId.equals("default")) {
      effectiveGlyphId = "default_compartment";
      while (getMinervaModel().getElementByElementId(effectiveGlyphId) != null
          || getMinervaModel().getReactionByReactionId(effectiveGlyphId) != null) {
        effectiveGlyphId = "default_compartment_" + getNextId();
      }
    }
    CompartmentGlyph result = getLayout().createCompartmentGlyph(effectiveGlyphId, sbmlCompartmentId);
    return result;
  }

  @Override
  protected LocalStyle createStyle(final Compartment element) {
    LocalStyle style = super.createStyle(element);
    style.getGroup().setStrokeWidth(element.getThickness());
    style.getGroup().setFill(getColorDefinition(element.getFillColor()).getId());
    style.getGroup().setStroke(getColorDefinition(element.getBorderColor()).getId());
    if (element instanceof OvalCompartment) {
      Ellipse ellipse = new Ellipse();
      ellipse.setCx(new RelAbsVector(0, 50));
      ellipse.setCy(new RelAbsVector(0, 50));
      ellipse.setRx(new RelAbsVector(0, 50));
      ellipse.setRy(new RelAbsVector(0, 50));
      style.getGroup().addElement(ellipse);
    } else {
      Rectangle rectangle = new Rectangle();
      rectangle.setRx(new RelAbsVector(0, 0));
      rectangle.setRy(new RelAbsVector(0, 0));
      rectangle.setX(new RelAbsVector(0, 0));
      rectangle.setY(new RelAbsVector(0, 0));
      rectangle.setWidth(new RelAbsVector(0, 100));
      rectangle.setHeight(new RelAbsVector(0, 100));
      style.getGroup().addElement(rectangle);
    }

    return style;
  }

  @Override
  protected void assignLayoutToGlyph(final Compartment element, final AbstractReferenceGlyph speciesGlyph) {
    if (getElementId(element).equals("default")) {
      BoundingBox boundingBox = new BoundingBox();

      boundingBox.setPosition(new Point(element.getX(), element.getY(), 0));
      Dimensions dimensions = new Dimensions();

      if (getMinervaModel().getWidth() > 0) {
        dimensions.setWidth(getMinervaModel().getWidth());
        dimensions.setHeight(getMinervaModel().getHeight());
      }
      boundingBox.setDimensions(dimensions);
      speciesGlyph.setBoundingBox(boundingBox);
    } else {
      super.assignLayoutToGlyph(element, speciesGlyph);
    }
  }

}
