package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.converter.model.sbml.extension.render.CustomEllipse;
import lcsb.mapviewer.converter.model.sbml.extension.render.CustomRenderPoint;
import lcsb.mapviewer.model.map.species.field.Residue;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;
import org.sbml.jsbml.ext.render.RelAbsVector;
import org.sbml.jsbml.ext.render.RenderPoint;

import java.util.ArrayList;
import java.util.List;

public class ModificationSiteShapeFactory extends AShapeFactory<Residue> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    final List<GraphicalPrimitive1D> result = new ArrayList<>();
    final CustomEllipse ellipse = new CustomEllipse();
    ellipse.setCx(new RelAbsVector(0, 50));
    ellipse.setCy(new RelAbsVector(0, 0));
    ellipse.setCenterWidthRelative(50);

    ellipse.setRx(new RelAbsVector(0, 50));
    ellipse.setRy(new RelAbsVector(0, 0));
    ellipse.setRadiusWidthRelative(50);

    result.add(ellipse);

    final Polygon line = new Polygon();

    final CustomRenderPoint p1 = new CustomRenderPoint();
    p1.setX(new RelAbsVector(0, 50));
    p1.setY(new RelAbsVector(0, 0));
    p1.setWidthRelative(100);
    line.addElement(p1);

    final RenderPoint p2 = line.createRenderPoint();
    p2.setX(new RelAbsVector(0, 50));
    p2.setY(new RelAbsVector(0, 100));

    result.add(line);

    return result;
  }

}
