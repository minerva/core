package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.model.map.species.field.Residue;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.Collections;
import java.util.List;

public class ResidueShapeFactory extends AShapeFactory<Residue> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return Collections.singletonList(super.createEllipseShape());
  }

}
