package lcsb.mapviewer.converter.model.sbml.reaction.render;

import lcsb.mapviewer.model.map.reaction.type.UnknownNegativeInfluenceReaction;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.List;

public class UnknownNegativeInfluenceReactionShapeFactory extends ReactionShapeFactory<UnknownNegativeInfluenceReaction> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return super.getShapeDefinition(UnknownNegativeInfluenceReaction.class);
  }
}
