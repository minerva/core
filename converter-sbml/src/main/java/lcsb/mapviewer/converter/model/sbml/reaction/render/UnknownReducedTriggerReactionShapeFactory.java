package lcsb.mapviewer.converter.model.sbml.reaction.render;

import lcsb.mapviewer.model.map.reaction.type.UnknownReducedTriggerReaction;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.List;

public class UnknownReducedTriggerReactionShapeFactory extends ReactionShapeFactory<UnknownReducedTriggerReaction> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return super.getShapeDefinition(UnknownReducedTriggerReaction.class);
  }
}
