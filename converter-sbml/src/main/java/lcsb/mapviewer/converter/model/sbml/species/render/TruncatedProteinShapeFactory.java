package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.model.map.species.TruncatedProtein;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;
import org.sbml.jsbml.ext.render.RelAbsVector;
import org.sbml.jsbml.ext.render.RenderCubicBezier;

import java.util.Collections;
import java.util.List;

public class TruncatedProteinShapeFactory extends AShapeFactory<TruncatedProtein> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    final Polygon polygon = new Polygon();

    createAbsoluteRelativePoint(polygon, 10, 0, 0, 0);
    createAbsoluteRelativePoint(polygon, 0, 100, 0, 0);
    createAbsoluteRelativePoint(polygon, 0, 100, 0, 60);
    createAbsoluteRelativePoint(polygon, 0, 80, 0, 40);
    createAbsoluteRelativePoint(polygon, 0, 80, 0, 100);
    createAbsoluteRelativePoint(polygon, 10, 0, 0, 100);

    RenderCubicBezier result = polygon.createRenderCubicBezier();
    result.setX(new RelAbsVector(0, 0));
    result.setY(new RelAbsVector(-10, 100));
    result.setX1(new RelAbsVector(5, 0));
    result.setY1(new RelAbsVector(-2, 100));
    result.setX2(new RelAbsVector(2, 0));
    result.setY2(new RelAbsVector(-5, 100));

    createAbsoluteRelativePoint(polygon, 0, 0, 10, 0);

    result = polygon.createRenderCubicBezier();
    result.setX(new RelAbsVector(10, 0));
    result.setY(new RelAbsVector(0, 0));
    result.setX1(new RelAbsVector(2, 0));
    result.setY1(new RelAbsVector(5, 0));
    result.setX2(new RelAbsVector(5, 0));
    result.setY2(new RelAbsVector(2, 0));

    return Collections.singletonList(polygon);
  }
}
