package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.model.map.species.field.StructuralState;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.Collections;
import java.util.List;

public class StructuralStateShapeFactory extends AShapeFactory<StructuralState> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return Collections.singletonList(super.createEllipseShape());
  }

}
