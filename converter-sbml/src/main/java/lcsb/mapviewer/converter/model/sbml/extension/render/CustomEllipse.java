package lcsb.mapviewer.converter.model.sbml.extension.render;

import org.sbml.jsbml.ext.render.Ellipse;

public class CustomEllipse extends Ellipse implements CustomGraphicalPrimitive1D {

  private boolean fill = true;

  private Double centerHeightRelative;
  private Double centerWidthRelative;

  private Double radiusHeightRelative;
  private Double radiusWidthRelative;

  public Double getCenterHeightRelative() {
    return centerHeightRelative;
  }

  public Double getCenterWidthRelative() {
    return centerWidthRelative;
  }

  public void setCenterHeightRelative(final double centerHeightRelative) {
    this.centerHeightRelative = centerHeightRelative;
  }

  public void setCenterWidthRelative(final double centerWidthRelative) {
    this.centerWidthRelative = centerWidthRelative;
  }

  public Double getRadiusHeightRelative() {
    return radiusHeightRelative;
  }

  public void setRadiusHeightRelative(final double radiusHeightRelative) {
    this.radiusHeightRelative = radiusHeightRelative;
  }

  public Double getRadiusWidthRelative() {
    return radiusWidthRelative;
  }

  public void setRadiusWidthRelative(final double radiusWidthRelative) {
    this.radiusWidthRelative = radiusWidthRelative;
  }

  public boolean isFill() {
    return fill;
  }

  public void setFill(final boolean fill) {
    this.fill = fill;
  }
}
