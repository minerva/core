package lcsb.mapviewer.converter.model.sbml.units;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.Unit;

import lcsb.mapviewer.converter.model.sbml.SbmlBioEntityParser;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitTypeFactor;

public class SbmlUnitsParser extends SbmlBioEntityParser {

  public SbmlUnitsParser(final Model sbmlModel, final lcsb.mapviewer.model.map.model.Model minervaModel) {
    super(sbmlModel, minervaModel);
  }

  protected SbmlUnit parse(final org.sbml.jsbml.UnitDefinition unitDefinition, final Model sbmlModel) {
    SbmlUnit result = new SbmlUnit(unitDefinition.getId());
    result.setName(unitDefinition.getName());
    for (int i = 0; i < unitDefinition.getUnitCount(); i++) {
      Unit sbmlUnit = unitDefinition.getUnit(i);
      SbmlUnitTypeFactor factor = new SbmlUnitTypeFactor(UnitMapping.kindToUnitType(sbmlUnit.getKind()),
          (int) sbmlUnit.getExponent(), sbmlUnit.getScale(), sbmlUnit.getMultiplier());
      result.addUnitTypeFactor(factor);
    }
    return result;
  }

  protected ListOf<org.sbml.jsbml.UnitDefinition> getSbmlElementList(final Model sbmlModel) {
    return sbmlModel.getListOfUnitDefinitions();
  }

  public Collection<SbmlUnit> parseList(final Model sbmlModel) {
    List<SbmlUnit> result = new ArrayList<>();
    for (org.sbml.jsbml.UnitDefinition unitDefinition : getSbmlElementList(sbmlModel)) {
      result.add(parse(unitDefinition, sbmlModel));
    }
    return result;
  }
}
