package lcsb.mapviewer.converter.model.sbml;

import lcsb.mapviewer.commands.CreateHierarchyCommand;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.TextGlyph;
import org.sbml.jsbml.ext.render.LocalStyle;

import java.util.HashSet;
import java.util.Set;

public class SbmlLayerParser extends SbmlBioEntityParser {

  public SbmlLayerParser(final Model sbmlModel, final lcsb.mapviewer.model.map.model.Model minervaModel) {
    super(sbmlModel, minervaModel);
  }

  public Set<Layer> parseLayer(final Model sbmlModel) throws InvalidInputDataExecption {
    Layer layer = new Layer();
    layer.setLayerId(1);
    layer.setZ(1);
    layer.setName(CreateHierarchyCommand.TEXT_LAYER_NAME);
    SbmlModelUtils sbmlModelUtils = new SbmlModelUtils(sbmlModel);
    Layout layout = sbmlModelUtils.getLayout();
    if (layout != null) {
      for (int i = 0; i < layout.getTextGlyphCount(); i++) {
        TextGlyph glyph = layout.getTextGlyph(i);
        if ((glyph.getGraphicalObject() == null || glyph.getGraphicalObject().isEmpty())
            && (glyph.getOriginOfText() == null || glyph.getOriginOfText().isEmpty())) {
          if (glyph.getBoundingBox() != null && glyph.getBoundingBox().getDimensions() != null && glyph.getBoundingBox().getPosition() != null) {
            LocalStyle style = getStyleForGlyph(glyph);
            LayerText text = new LayerText();
            if (style != null && style.getGroup().isSetStroke()) {
              text.setColor(getColorByColorDefinition(style.getGroup().getStroke()));
            }
            text.setX(glyph.getBoundingBox().getPosition().getX());
            text.setY(glyph.getBoundingBox().getPosition().getY());
            text.setZ((int) glyph.getBoundingBox().getPosition().getZ());
            text.setWidth(glyph.getBoundingBox().getDimensions().getWidth());
            text.setHeight(glyph.getBoundingBox().getDimensions().getHeight());
            text.setNotes(glyph.getText());
            layer.addLayerText(text);
          }
        }
      }
    }
    Set<Layer> result = new HashSet<>();
    if (layer.getDrawables().size() > 0) {
      result.add(layer);
    }
    return result;
  }
}
