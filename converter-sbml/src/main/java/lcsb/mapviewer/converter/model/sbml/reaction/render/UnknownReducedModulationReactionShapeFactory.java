package lcsb.mapviewer.converter.model.sbml.reaction.render;

import lcsb.mapviewer.model.map.reaction.type.UnknownReducedModulationReaction;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.List;

public class UnknownReducedModulationReactionShapeFactory extends ReactionShapeFactory<UnknownReducedModulationReaction> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return super.getShapeDefinition(UnknownReducedModulationReaction.class);
  }
}
