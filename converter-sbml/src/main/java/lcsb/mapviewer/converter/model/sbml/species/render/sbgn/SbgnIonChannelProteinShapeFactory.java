package lcsb.mapviewer.converter.model.sbml.species.render.sbgn;

import lcsb.mapviewer.converter.model.sbml.species.render.AShapeFactory;
import lcsb.mapviewer.model.map.species.IonChannelProtein;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.Collections;
import java.util.List;

public class SbgnIonChannelProteinShapeFactory extends AShapeFactory<IonChannelProtein> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return Collections.singletonList(createRoundedRect(AShapeFactory.RECTANGLE_CORNER_ARC_SIZE));
  }

}
