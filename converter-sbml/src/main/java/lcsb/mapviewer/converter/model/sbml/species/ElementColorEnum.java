package lcsb.mapviewer.converter.model.sbml.species;

import java.awt.Color;

import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.model.map.compartment.OvalCompartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.IonChannelProtein;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.ReceptorProtein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.TruncatedProtein;
import lcsb.mapviewer.model.map.species.Unknown;

public enum ElementColorEnum {
  ANTISENSE_RNA(AntisenseRna.class, "#ff6666"),
  COMPLEX(Complex.class, "#f7f7f7"),
  DEGRADED(Degraded.class, "#ffcccc"),
  DRUG(Drug.class, "#ff00ff"),
  ELEMENT(Element.class, "#000000"),
  GENE(Gene.class, "#ffff66"),
  GENERIC_PROTEIN(GenericProtein.class, "#ccffcc"),
  TRUNCATED_PROTEIN(TruncatedProtein.class, "#ffcccc"),
  ION(Ion.class, "#9999ff"),
  ION_CHANNEL(IonChannelProtein.class, "#ccffff"),
  OVAL_COMPARTMENT(OvalCompartment.class, "#cccc00"),
  PHENOTYPE(Phenotype.class, "#cc99ff"),
  RECEPTOR(ReceptorProtein.class, "#ffffcc"),
  RNA(Rna.class, "#66ff66"),
  SIMPLE_MOLECULE(SimpleMolecule.class, "#ccff66"),
  SQUARE_COMPARTMENT(SquareCompartment.class, "#cccc00"),
  UNKNOWN(Unknown.class, "#cccccc"),
  ;

  private ColorParser colorParser = new ColorParser();
  private Class<? extends Element> clazz;
  private Color color;

  private ElementColorEnum(final Class<? extends Element> clazz, final String color) {
    this.clazz = clazz;
    if (color != null) {
      this.color = colorParser.parse(color);
    }
  }

  public static Color getColorByClass(final Class<? extends Element> clazz) {
    Color result = null;
    for (final ElementColorEnum type : ElementColorEnum.values()) {
      if (type.getClazz().equals(clazz)) {
        result = type.getColor();
      }
    }
    if (result == null) {
      for (final ElementColorEnum type : ElementColorEnum.values()) {
        if (type.getClazz().isAssignableFrom(clazz)) {
          result = type.getColor();
        }
      }
    }
    return result;
  }

  public Class<? extends Element> getClazz() {
    return clazz;
  }

  public Color getColor() {
    return color;
  }

}
