package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.RelAbsVector;

import java.util.Collections;
import java.util.List;

public class ProteinBindingDomainShapeFactory extends AShapeFactory<ProteinBindingDomain> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return Collections.singletonList(super.createRectangleShape(
        new RelAbsVector(0, 0), new RelAbsVector(0, 0),
        new RelAbsVector(0, 100), new RelAbsVector(0, 100)
    ));
  }

}
