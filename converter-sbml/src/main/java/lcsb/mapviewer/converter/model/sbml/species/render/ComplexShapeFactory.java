package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.model.map.species.Complex;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;

import java.util.Collections;
import java.util.List;

public class ComplexShapeFactory extends AShapeFactory<Complex> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    final Polygon polygon = new Polygon();
    createAbsoluteRelativePoint(polygon,
        COMPLEX_TRIMMED_CORNER_SIZE, 0,
        0, 0);
    createAbsoluteRelativePoint(polygon,
        -COMPLEX_TRIMMED_CORNER_SIZE, 100,
        0, 0);
    createAbsoluteRelativePoint(polygon,
        0, 100,
        COMPLEX_TRIMMED_CORNER_SIZE, 0);
    createAbsoluteRelativePoint(polygon,
        0, 100,
        -COMPLEX_TRIMMED_CORNER_SIZE, 100);
    createAbsoluteRelativePoint(polygon,
        -COMPLEX_TRIMMED_CORNER_SIZE, 100,
        0, 100);
    createAbsoluteRelativePoint(polygon,
        COMPLEX_TRIMMED_CORNER_SIZE, 0,
        0, 100);
    createAbsoluteRelativePoint(polygon,
        0, 0,
        -COMPLEX_TRIMMED_CORNER_SIZE, 100);
    createAbsoluteRelativePoint(polygon,
        0, 0,
        COMPLEX_TRIMMED_CORNER_SIZE, 0);

    return Collections.singletonList(polygon);
  }

}
