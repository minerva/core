package lcsb.mapviewer.converter.model.sbml;

import org.sbml.jsbml.Species;

import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.model.Model;

public class SbmlLogMarker extends LogMarker {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public SbmlLogMarker(final LogMarker marker) {
    super(marker);
  }

  public SbmlLogMarker(final ProjectLogEntryType type, final Species species, final Model model) {
    super(type, "Species", species.getId(), model.getName());
  }
}
