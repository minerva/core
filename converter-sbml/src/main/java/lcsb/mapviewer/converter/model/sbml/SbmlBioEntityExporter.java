package lcsb.mapviewer.converter.model.sbml;

import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.AbstractReferenceGlyph;
import org.sbml.jsbml.ext.layout.BoundingBox;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.multi.MultiModelPlugin;
import org.sbml.jsbml.ext.render.ColorDefinition;
import org.sbml.jsbml.ext.render.LineEnding;
import org.sbml.jsbml.ext.render.LocalStyle;
import org.sbml.jsbml.ext.render.Polygon;
import org.sbml.jsbml.ext.render.RelAbsVector;
import org.sbml.jsbml.ext.render.RenderConstants;
import org.sbml.jsbml.ext.render.RenderGraphicalObjectPlugin;
import org.sbml.jsbml.ext.render.RenderPoint;
import org.sbml.jsbml.xml.XMLNode;

import javax.xml.stream.XMLStreamException;
import java.awt.Color;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public abstract class SbmlBioEntityExporter<T extends BioEntity, S extends org.sbml.jsbml.AbstractNamedSBase> {
  /**
   * Default class logger.
   */
  private static final Logger logger = LogManager.getLogger();

  protected Map<String, S> sbmlElementByElementNameAndCompartmentName = new HashMap<>();
  private final Map<String, AbstractReferenceGlyph> sbmlGlyphByElementId = new HashMap<>();
  /**
   * SBML Layout used when exporting map.
   */
  private final Layout layout;
  /**
   * Map that we are exporting.
   */
  private final lcsb.mapviewer.model.map.model.Model minervaModel;
  /**
   * SBML model to which we are exporting.
   */
  private final Model sbmlModel;
  private final Map<String, S> sbmlElementByElementId = new HashMap<>();
  private int idCounter = 0;

  private final Set<SbmlExtension> sbmlExtensions = new HashSet<>();

  private boolean provideDefaults;

  private final Pattern sbmlValidIdMatcher;

  /**
   * Map with elementId mapping for identifiers that cannot be used in sbml.
   */
  private final Map<String, String> correctedElementId = new HashMap<>();

  private final SbmlModelUtils sbmlModelUtils;

  public SbmlBioEntityExporter(final Model sbmlModel, final lcsb.mapviewer.model.map.model.Model minervaModel,
                               final Collection<SbmlExtension> sbmlExtensions) {
    this.sbmlModel = sbmlModel;
    this.sbmlModelUtils = new SbmlModelUtils(sbmlModel);
    this.layout = sbmlModelUtils.getLayout();
    this.minervaModel = minervaModel;
    this.sbmlExtensions.addAll(sbmlExtensions);

    final String underscore = "_";
    final String letter = "a-zA-Z";
    final String digit = "0-9";
    final String idChar = '[' + letter + digit + underscore + ']';
    sbmlValidIdMatcher = Pattern.compile("^[" + letter + underscore + "]" + idChar + '*');

  }

  public void exportElements() throws InconsistentModelException {
    final Collection<T> speciesList = getElementList();
    for (final T bioEntity : speciesList) {
      try {
        final S sbmlElement = getSbmlElement(bioEntity);

        if (sbmlElementByElementId.get(getElementId(bioEntity)) != null) {
          throw new InconsistentModelException("More than one species with id: " + getElementId(bioEntity));
        }
        sbmlElementByElementId.put(getElementId(bioEntity), sbmlElement);
      } catch (final Exception e) {
        throw new InconsistentModelException(new ElementUtils().getElementTag(bioEntity)
            + "Problem with exporting bioEntity", e);
      }
    }
    if (isExtensionEnabled(SbmlExtension.LAYOUT)) {
      for (final T bioEntity : speciesList) {
        if (getSbmlElement(bioEntity) != null) {
          try {
            final AbstractReferenceGlyph elementGlyph = createGlyph(bioEntity);
            sbmlGlyphByElementId.put(getElementId(bioEntity), elementGlyph);
          } catch (final Exception e) {
            throw new InconsistentModelException(new ElementUtils().getElementTag(bioEntity)
                + "Problem with exporting bioEntity", e);
          }
        }
      }
    }
  }

  /**
   * Checks if exporter should used extension.
   *
   * @param sbmlExtension {@link SbmlExtension} to be checked
   * @return <code>true</code> if extension should be supported by exported file
   */
  protected boolean isExtensionEnabled(final SbmlExtension sbmlExtension) {
    return sbmlExtensions.contains(sbmlExtension);
  }

  protected abstract Collection<T> getElementList();

  public abstract S createSbmlElement(final T element) throws InconsistentModelException;

  public S getSbmlElement(final T element) throws InconsistentModelException {
    final String mapKey = getSbmlIdKey(element);
    if (!sbmlElementByElementNameAndCompartmentName.containsKey(mapKey)) {
      final S sbmlElement = createSbmlElement(element);
      if (sbmlElement != null) {
        try {
          final XMLNode rdfNode = NotesUtility.getRdfNode(element.getMiriamData(), sbmlElement.getId());
          sbmlElement.setAnnotation(rdfNode);
        } catch (final XMLStreamException e1) {
          throw new InconsistentModelException(e1);
        }
        sbmlElement.setName(element.getName());
        try {
          sbmlElement.setNotes(NotesUtility.prepareNotesNode(element.getNotes()));
        } catch (final XMLStreamException e) {
          throw new InvalidStateException(e);
        }
      }
      sbmlElementByElementNameAndCompartmentName.put(mapKey, sbmlElement);
    }
    return sbmlElementByElementNameAndCompartmentName.get(mapKey);
  }

  protected abstract String getSbmlIdKey(final T element);

  protected abstract void assignLayoutToGlyph(final T element, final AbstractReferenceGlyph compartmentGlyph);

  protected AbstractReferenceGlyph createGlyph(final T element) {
    final String sbmlElementId = sbmlElementByElementId.get(getElementId(element)).getId();
    final String glyphId = getElementId(element);
    final AbstractReferenceGlyph elementGlyph = createElementGlyph(sbmlElementId, glyphId);
    assignLayoutToGlyph(element, elementGlyph);
    return elementGlyph;
  }

  protected abstract AbstractReferenceGlyph createElementGlyph(final String sbmlCompartmentId, final String glyphId);

  protected String getNextId() {
    return (idCounter++) + "";
  }

  protected Layout getLayout() {
    return layout;
  }

  protected MultiModelPlugin getMultiPlugin() {
    return (MultiModelPlugin) sbmlModel.getExtension("multi");
  }

  protected lcsb.mapviewer.model.map.model.Model getMinervaModel() {
    return minervaModel;
  }

  protected Model getSbmlModel() {
    return sbmlModel;
  }

  public S getSbmlElementByElementId(final String id) {
    return sbmlElementByElementId.get(id);
  }

  public AbstractReferenceGlyph getSbmlGlyphByElementId(final String elementId) {
    return sbmlGlyphByElementId.get(elementId);
  }

  protected void assignStyleToGlyph(final AbstractReferenceGlyph speciesGlyph, final LocalStyle style) {
    final RenderGraphicalObjectPlugin rgop = new RenderGraphicalObjectPlugin(speciesGlyph);
    if (style.getGroup().isSetEndHead()) {
      final LineEnding lineEnding = sbmlModelUtils.createLineEndingStyle(style.getGroup().getEndHead());
      style.getGroup().setEndHead(lineEnding.getId());
    }
    style.getIDList().add(speciesGlyph.getId());
    speciesGlyph.addExtension(RenderConstants.shortLabel, rgop);
  }

  protected BoundingBox createBoundingBox(final double x, final double y, final int z, final double w, final double h) {
    return sbmlModelUtils.createBoundingBox(x, y, z, w, h);
  }

  protected abstract LocalStyle createStyle(final T element);

  protected LocalStyle createStyle() {
    return sbmlModelUtils.createStyle();
  }

  protected boolean isProvideDefaults() {
    return provideDefaults;
  }

  public void setProvideDefaults(final boolean provideDefaults) {
    this.provideDefaults = provideDefaults;
  }

  public String getElementId(final BioEntity bioEntity) {
    String result = bioEntity.getElementId();
    if (!sbmlValidIdMatcher.matcher(result).matches()) {
      if (correctedElementId.get(result) == null) {
        String newElementId = null;
        do {
          newElementId = this.getClass().getSimpleName() + "_" + getNextId();
        } while (minervaModel.getElementByElementId(newElementId) != null || minervaModel.getReactionByReactionId(newElementId) != null);
        logger.warn(new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, bioEntity),
            "Element id " + bioEntity.getElementId() + " cannot be used in sbml. Changing to: " + newElementId);
        correctedElementId.put(result, newElementId);
      }
      result = correctedElementId.get(result);
    }

    return result;
  }

  protected ColorDefinition getColorDefinition(final Color color) {
    return sbmlModelUtils.getColorDefinition(color);
  }

  protected void createAbsolutePoint(final Polygon polygon, final double x, final double y) {
    final RenderPoint p1 = polygon.createRenderPoint();
    p1.setX(new RelAbsVector(Math.round(x)));
    p1.setY(new RelAbsVector(Math.round(y)));
  }

}
