package lcsb.mapviewer.converter.model.sbml.reaction.render;

import lcsb.mapviewer.model.map.reaction.type.ReducedTriggerReaction;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.List;

public class ReducedTriggerReactionShapeFactory extends ReactionShapeFactory<ReducedTriggerReaction> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return super.getShapeDefinition(ReducedTriggerReaction.class);
  }
}
