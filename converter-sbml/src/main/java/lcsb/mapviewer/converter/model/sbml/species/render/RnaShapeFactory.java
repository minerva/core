package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.model.map.species.Rna;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;

import java.util.Collections;
import java.util.List;

public class RnaShapeFactory extends AShapeFactory<Rna> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    final Polygon polygon = new Polygon();
    createRelativePoint(polygon, 25, 0);
    createRelativePoint(polygon, 100, 0);
    createRelativePoint(polygon, 75, 100);
    createRelativePoint(polygon, 0, 100);
    return Collections.singletonList(polygon);
  }

}
