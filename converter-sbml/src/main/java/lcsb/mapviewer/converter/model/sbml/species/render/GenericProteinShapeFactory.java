package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.model.map.species.GenericProtein;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.Collections;
import java.util.List;

public class GenericProteinShapeFactory extends AShapeFactory<GenericProtein> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return Collections.singletonList(createRoundedRect(AShapeFactory.RECTANGLE_CORNER_ARC_SIZE));
  }

}
