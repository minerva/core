package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.model.map.species.field.TranscriptionSite;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;
import org.sbml.jsbml.ext.render.RelAbsVector;
import org.sbml.jsbml.ext.render.RenderPoint;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class TranscriptionSiteShapeFactory extends AShapeFactory<TranscriptionSite> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return getShapeDefinition("LEFT");
  }

  public List<GraphicalPrimitive1D> getShapeDefinition(final TranscriptionSite site) {
    return getShapeDefinition(site.getDirection());
  }

  private List<GraphicalPrimitive1D> getShapeDefinition(final String direction) {
    final double relativeWidthDelta;
    final double absoluteArrowDelta;

    if (Objects.equals(direction, "RIGHT")) {
      relativeWidthDelta = 0;
      absoluteArrowDelta = 6;
    } else {
      relativeWidthDelta = 100;
      absoluteArrowDelta = -6;
    }
    final Polygon line = new Polygon();

    final RenderPoint p1 = line.createRenderPoint();
    p1.setX(new RelAbsVector(0, 100 - relativeWidthDelta));
    p1.setY(new RelAbsVector(0, 100));

    final RenderPoint p2 = line.createRenderPoint();
    p2.setX(new RelAbsVector(0, 100 - relativeWidthDelta));
    p2.setY(new RelAbsVector(0, 0));

    final RenderPoint p3 = line.createRenderPoint();
    p3.setX(new RelAbsVector(0, relativeWidthDelta));
    p3.setY(new RelAbsVector(0, 0));

    final RenderPoint p4 = line.createRenderPoint();
    p4.setX(new RelAbsVector(absoluteArrowDelta, relativeWidthDelta));
    p4.setY(new RelAbsVector(-3, 0));

    final RenderPoint p5 = line.createRenderPoint();
    p5.setX(new RelAbsVector(absoluteArrowDelta, relativeWidthDelta));
    p5.setY(new RelAbsVector(3, 0));

    final RenderPoint p6 = line.createRenderPoint();
    p6.setX(new RelAbsVector(0, relativeWidthDelta));
    p6.setY(new RelAbsVector(0, 0));

    final RenderPoint p7 = line.createRenderPoint();
    p7.setX(new RelAbsVector(0, 100 - relativeWidthDelta));
    p7.setY(new RelAbsVector(0, 0));

    return Collections.singletonList(line);
  }

}
