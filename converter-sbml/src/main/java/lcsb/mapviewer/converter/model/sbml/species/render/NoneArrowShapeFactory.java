package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.model.map.Drawable;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;

import java.util.Collections;
import java.util.List;

public class NoneArrowShapeFactory extends AShapeFactory<Drawable> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {

    final Polygon polygon = new Polygon();
    
    createRelativePoint(polygon, 0, 50);
    createRelativePoint(polygon, 100, 50);

    return Collections.singletonList(polygon);
  }

}
