package lcsb.mapviewer.converter.model.sbml.species;

import lcsb.mapviewer.converter.model.sbml.SbmlModelUtils;
import lcsb.mapviewer.converter.model.sbml.species.render.ShapeFactory;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.BoundingBox;
import org.sbml.jsbml.ext.layout.Dimensions;
import org.sbml.jsbml.ext.layout.Point;
import org.sbml.jsbml.ext.render.ColorDefinition;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.HTextAnchor;
import org.sbml.jsbml.ext.render.LocalStyle;
import org.sbml.jsbml.ext.render.VTextAnchor;

import java.awt.Color;
import java.util.List;

public class ModificationResidueExporter {

  private static final Logger logger = LogManager.getLogger();

  private final SbmlModelUtils sbmlModelUtils;

  private final ShapeFactory shapeFactory = new ShapeFactory();

  public ModificationResidueExporter(final Model sbmlModel) {
    this.sbmlModelUtils = new SbmlModelUtils(sbmlModel);
  }

  public LocalStyle createStyle(final ModificationResidue element) {
    final LocalStyle style = createGenericStyle(element);

    final List<GraphicalPrimitive1D> shapes = shapeFactory.createSbmlShapeForElement(element);
    for (final GraphicalPrimitive1D shape : shapes) {
      style.getGroup().addElement(shape);
    }
    return style;
  }

  private LocalStyle createGenericStyle(final ModificationResidue element) {
    final LocalStyle style = createGenericStyle();
    style.getGroup().setStroke(sbmlModelUtils.getColorDefinition(element.getBorderColor()).getId());
    style.getGroup().setFontSize(element.getFontSize().shortValue());
    style.getGroup().setFill(sbmlModelUtils.getColorDefinition(element.getFillColor()).getId());
    return style;
  }

  private LocalStyle createGenericStyle() {
    final LocalStyle style = sbmlModelUtils.createStyle();
    final ColorDefinition color = sbmlModelUtils.getColorDefinition(Color.BLACK);
    final ColorDefinition background = sbmlModelUtils.getColorDefinition(Color.WHITE);
    style.getGroup().setStroke(color.getId());
    style.getGroup().setStrokeWidth(1.0);
    style.getGroup().setFill(background.getId());
    style.getGroup().setFontSize((short) 10);
    return style;
  }

  public LocalStyle createTextStyle(final ModificationResidue mr) {
    return createTextStyle();
  }

  private LocalStyle createTextStyle() {
    final LocalStyle style = createGenericStyle();

    style.getGroup().setVTextAnchor(VTextAnchor.MIDDLE);
    style.getGroup().setTextAnchor(HTextAnchor.MIDDLE);
    return style;
  }

  public LocalStyle createTextStyle(final StructuralState structuralState) {
    return createTextStyle();
  }

  public BoundingBox createBoundingBoxForModification(final ModificationResidue mr) {
    final double width = mr.getWidth();
    final double height = mr.getHeight();
    final double x = mr.getX();
    final double y = mr.getY();
    final double z = mr.getZ();
    final BoundingBox boundingBox = new BoundingBox();
    boundingBox.setPosition(new Point(x, y, z));
    final Dimensions dimensions = new Dimensions();
    dimensions.setWidth(width);
    dimensions.setHeight(height);
    boundingBox.setDimensions(dimensions);
    return boundingBox;
  }

  public BoundingBox createBoundingBoxForModificationText(final ModificationResidue mr) {
    final BoundingBox result = createBoundingBoxForModification(mr);
    result.getPosition().setX(result.getPosition().getX() + 2);
    result.getPosition().setY(result.getPosition().getY() + 2);
    result.getDimensions().setWidth(result.getDimensions().getWidth() - 4);
    if (mr instanceof ModificationSite) {
      result.getDimensions().setHeight(result.getDimensions().getWidth());
    } else {
      result.getDimensions().setHeight(result.getDimensions().getHeight() - 4);
    }
    result.getPosition().setZ(result.getPosition().getZ() + 1);
    return result;
  }

  public BoundingBox createBoundingBoxForStructuralState(final StructuralState structuralState) {
    final double width = structuralState.getWidth();
    final double height = structuralState.getHeight();
    final double x = structuralState.getX();
    final double y = structuralState.getY();
    final BoundingBox boundingBox = new BoundingBox();
    boundingBox.setPosition(new Point(x, y, structuralState.getZ()));
    final Dimensions dimensions = new Dimensions();
    dimensions.setWidth(width);
    dimensions.setHeight(height);
    boundingBox.setDimensions(dimensions);
    return boundingBox;
  }

  public BoundingBox createBoundingBoxForStructuralStateText(final StructuralState structuralState) {
    final BoundingBox result = createBoundingBoxForStructuralState(structuralState);
    result.getPosition().setZ(result.getPosition().getZ() + 1);
    return result;
  }

}
