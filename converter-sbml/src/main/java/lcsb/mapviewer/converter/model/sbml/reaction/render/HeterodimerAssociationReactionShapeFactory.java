package lcsb.mapviewer.converter.model.sbml.reaction.render;

import lcsb.mapviewer.model.map.reaction.type.HeterodimerAssociationReaction;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.List;

public class HeterodimerAssociationReactionShapeFactory extends ReactionShapeFactory<HeterodimerAssociationReaction> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return super.getShapeDefinition(HeterodimerAssociationReaction.class);
  }
}
