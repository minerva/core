package lcsb.mapviewer.converter.model.sbml.compartment;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.ext.layout.AbstractReferenceGlyph;
import org.sbml.jsbml.ext.layout.CompartmentGlyph;
import org.sbml.jsbml.ext.render.LocalStyle;

import lcsb.mapviewer.commands.layout.ApplySimpleLayoutModelCommand;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.sbml.SbmlElementParser;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.species.Element;

public class SbmlCompartmentParser extends SbmlElementParser<org.sbml.jsbml.Compartment> {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  public SbmlCompartmentParser(final Model sbmlModel, final lcsb.mapviewer.model.map.model.Model minervaModel) {
    super(sbmlModel, minervaModel);
  }

  @Override
  protected ListOf<org.sbml.jsbml.Compartment> getSbmlElementList() {
    return getSbmlModel().getListOfCompartments();
  }

  @Override
  public List<Element> mergeLayout(final List<? extends Element> elements)
      throws InvalidInputDataExecption {
    List<Element> result = super.mergeLayout(elements);

    for (final Element element : result) {
      Compartment parent = element.getCompartment();
      for (final Compartment compartment : getMinervaModel().getCompartments()) {
        if (compartment.getNameX() == null || compartment.getNameY() == null) {
          compartment.setNameX(compartment.getX() + ApplySimpleLayoutModelCommand.COMPARTMENT_BORDER);
          compartment.setNameY(compartment.getY() + ApplySimpleLayoutModelCommand.COMPARTMENT_BORDER);
        }
        if (compartment.getNameWidth() == null || compartment.getNameHeight() == null) {
          compartment.setNameWidth(compartment.getWidth() - (compartment.getNameX() - compartment.getX()));
          compartment.setNameHeight(compartment.getHeight() - (compartment.getNameY() - compartment.getY()));
        }
        if (compartment.getNameHorizontalAlign() == null) {
          compartment.setNameHorizontalAlign(HorizontalAlign.LEFT);
        }
        if (compartment.getNameVerticalAlign() == null) {
          compartment.setNameVerticalAlign(VerticalAlign.TOP);
        }

        if (parent == null || parent.getSize() > compartment.getSize()) {
          if (compartment.contains(element)) {
            parent = compartment;
          }
        }
      }
      if (parent != null) {
        parent.addElement(element);
      }
    }

    return result;
  }

  @Override
  protected void applyStyleToElement(final Element elementWithLayout, final LocalStyle style) {
    super.applyStyleToElement(elementWithLayout, style);
    if (!style.getGroup().isSetFill() && style.getGroup().isSetStroke()) {
      Color backgroundColor = getColorByColorDefinition(style.getGroup().getStroke());
      elementWithLayout.setFillColor(backgroundColor);
    }
  }

  @Override
  protected List<Pair<String, AbstractReferenceGlyph>> getGlyphs() {
    List<Pair<String, AbstractReferenceGlyph>> result = new ArrayList<>();
    for (final CompartmentGlyph glyph : getLayout().getListOfCompartmentGlyphs()) {
      if (!glyph.getCompartment().equals("default")) {
        result.add(new Pair<>(glyph.getCompartment(), glyph));
      }
    }
    return result;
  }

  @Override
  protected Compartment parse(final org.sbml.jsbml.Compartment compartment)
      throws InvalidInputDataExecption {
    if (compartment.getId().equals("default")) {
      return null;
    }
    Compartment result = new SquareCompartment(compartment.getId());
    assignBioEntityData(compartment, result);
    if (result.getName() == null || result.getName().isEmpty()) {
      result.setName(result.getElementId());
    }
    return result;
  }

}
