package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.model.map.species.AntisenseRna;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;

import java.util.Collections;
import java.util.List;

public class AntisenseRnaShapeFactory extends AShapeFactory<AntisenseRna> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    final Polygon polygon = new Polygon();
    createRelativePoint(polygon, 0, 0);
    createRelativePoint(polygon, 75, 0);
    createRelativePoint(polygon, 100, 100);
    createRelativePoint(polygon, 25, 100);
    return Collections.singletonList(polygon);
  }

}
