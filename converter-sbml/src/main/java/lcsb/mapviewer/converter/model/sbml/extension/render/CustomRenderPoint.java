package lcsb.mapviewer.converter.model.sbml.extension.render;

import org.sbml.jsbml.ext.render.RenderPoint;

public class CustomRenderPoint extends RenderPoint implements CustomGraphicalPrimitive1D {
  private Double heightRelative;
  private Double widthRelative;

  public CustomRenderPoint() {
    super();
  }

  public CustomRenderPoint(final CustomRenderPoint customRenderPoint) {
    super(customRenderPoint);
    this.heightRelative = customRenderPoint.heightRelative;
    this.widthRelative = customRenderPoint.widthRelative;
  }

  public Double getHeightRelative() {
    return heightRelative;
  }

  public void setHeightRelative(final double heightRelative) {
    this.heightRelative = heightRelative;
  }

  public Double getWidthRelative() {
    return widthRelative;
  }

  public void setWidthRelative(final double widthRelative) {
    this.widthRelative = widthRelative;
  }

  @Override
  public CustomRenderPoint clone() {
    return new CustomRenderPoint(this);
  }

}
