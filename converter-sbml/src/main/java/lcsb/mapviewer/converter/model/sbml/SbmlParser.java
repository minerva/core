package lcsb.mapviewer.converter.model.sbml;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.SBMLReader;
import org.sbml.jsbml.ext.SBasePlugin;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.LayoutModelPlugin;
import org.sbml.jsbml.ext.multi.MultiModelPlugin;
import org.springframework.stereotype.Component;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.commands.layout.ApplySimpleLayoutModelCommand;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.ZIndexPopulator;
import lcsb.mapviewer.converter.model.sbml.compartment.SbmlCompartmentParser;
import lcsb.mapviewer.converter.model.sbml.reaction.SbmlReactionParser;
import lcsb.mapviewer.converter.model.sbml.species.SbmlSpeciesParser;
import lcsb.mapviewer.converter.model.sbml.units.SbmlUnitsParser;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.modifier.Modulation;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationResidue;

@Component
public class SbmlParser extends Converter {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  private boolean provideDefaults = true;

  @Override
  public Model createModel(final ConverterParams params) throws InvalidInputDataExecption {
    Model model = new ModelFullIndexed(null);

    if (params.getFilename() != null) {
      model.setName(FilenameUtils.getBaseName(params.getFilename()));
    }
    SBMLDocument sbmlDocument = null;
    try {
      sbmlDocument = SBMLReader.read(params.getInputStream());
    } catch (XMLStreamException e) {
      throw new InvalidInputDataExecption(e);
    } catch (Exception e) {
      throw new InvalidInputDataExecption("JSbml library could not parse provided input.\n" + e.getMessage(), e);
    }
    org.sbml.jsbml.Model sbmlModel = sbmlDocument.getModel();
    model.setIdModel(sbmlModel.getId());
    if (model.getIdModel() != null && model.getIdModel().isEmpty()) {
      model.setIdModel(null);
    }
    model.setName(sbmlModel.getName());
    model.setNotes(NotesUtility.extractNotes(sbmlModel));

    checkAvailableExtensions(sbmlModel);
    Layout layout = getSbmlLayout(sbmlModel);
    boolean layoutExists = layout != null;

    SbmlCompartmentParser compartmentParser = new SbmlCompartmentParser(sbmlModel, model);
    SbmlSpeciesParser speciesParser = new SbmlSpeciesParser(sbmlModel, model);
    SbmlReactionParser reactionParser = new SbmlReactionParser(sbmlModel, model, speciesParser, compartmentParser);
    SbmlUnitsParser unitParser = new SbmlUnitsParser(sbmlModel, model);
    SbmlParameterParser parameterParser = new SbmlParameterParser(sbmlModel, model);
    SbmlFunctionParser functionParser = new SbmlFunctionParser(sbmlModel, model);
    SbmlLayerParser layerParser = new SbmlLayerParser(sbmlModel, model);

    model.addMiriamData(compartmentParser.extractMiriamDataFromAnnotation(sbmlModel.getAnnotation(), null));
    model.addAuthors(compartmentParser.extractAuthorsFromAnnotation(sbmlModel.getAnnotation()));
    model.addModificationDates(compartmentParser.extractModificationDatesFromAnnotation(sbmlModel.getAnnotation()));
    model.setCreationDate(compartmentParser.extractCreationDateFromAnnotation(sbmlModel.getAnnotation()));

    model.addUnits(unitParser.parseList(sbmlModel));
    model.addParameters(parameterParser.parseList(sbmlModel));
    model.addFunctions(functionParser.parseList(sbmlModel));
    model.addLayers(layerParser.parseLayer(sbmlModel));

    model.addElements(compartmentParser.parseList());
    model.addElements(speciesParser.parseList());
    model.addReactions(reactionParser.parseList());

    if (layoutExists) {
      if (layout.getDimensions() != null) {
        model.setWidth(layout.getDimensions().getWidth());
        model.setHeight(layout.getDimensions().getHeight());

      }
      compartmentParser.mergeLayout(model.getCompartments());
      speciesParser.mergeLayout(model.getSpeciesList());
      reactionParser.mergeLayout(model.getReactions());
    }

    reactionParser.validateReactions(model.getReactions());

    if (sbmlModel.getConstraintCount() > 0) {
      logger.warn("Constraints not implemented for model");
    }
    if (sbmlModel.getConversionFactorInstance() != null) {
      logger.warn("ConversionFactor not implemented for model");
    }
    if (sbmlModel.getEventCount() > 0 || sbmlModel.getEventAssignmentCount() > 0) {
      logger.warn("Handling of Events is not implemented for model");
    }
    if (sbmlModel.getInitialAssignmentCount() > 0) {
      logger.warn("InitialAssignment not implemented for model");
    }
    if (sbmlModel.getRuleCount() > 0) {
      logger.warn("Rule not implemented for model");
    }

    if (layoutExists) {
      addComplexRelationsBasedOnCoordinates(model);
    } else if (sbmlModel.getExtension("multi") != null) {
      speciesParser.addComplexRelationsBasedOnMulti();
    }
    createLayout(model, layout, params.isSizeAutoAdjust(), reactionParser);

    return model;
  }

  @Override
  public String model2String(final Model model) throws ConverterException {
    try {
      SbmlExporter sbmlExporter = new SbmlExporter();
      sbmlExporter.setProvideDefaults(provideDefaults);
      return sbmlExporter.toXml(model);
    } catch (final InconsistentModelException e) {
      throw new ConverterException(e.getMessage(), e);
    }
  }

  @Override
  public String getCommonName() {
    return "SBML";
  }

  @Override
  public MimeType getMimeType() {
    return MimeType.SBML;
  }

  @Override
  public List<String> getFileExtensions() {
    return Arrays.asList("xml", "sbml");
  }

  private void addComplexRelationsBasedOnCoordinates(final Model model) {
    for (Element element : model.getElements()) {
      if (element instanceof Species) {
        Species species = (Species) element;
        if (species.getComplex() == null && element.getWidth() != 0 || element.getHeight() != 0) {
          Complex complex = findComplexForElement(species, model);
          if (complex != null) {
            complex.addSpecies(species);
          }
        }
      }
    }
  }

  private Complex findComplexForElement(final Species species, final Model model) {
    Complex result = null;
    for (final Element element : model.getElements()) {
      if (element instanceof Complex && element != species) {
        Complex complex = (Complex) element;
        if (complex.getWidth() != 0 || complex.getHeight() != 0) {
          if (complex.contains(species) && complex.getSize() > species.getSize()) {
            if (result == null) {
              result = complex;
            } else if (result.getSize() > complex.getSize()) {
              result = complex;
            }
          }
        }
      }
    }
    return result;
  }

  private void createLayout(final Model model, final Layout layout, final boolean resize, final SbmlReactionParser parser)
      throws InvalidInputDataExecption {
    if (model.getWidth() <= Configuration.EPSILON) {
      double maxY = 1;
      double maxX = 1;
      for (final Element element : model.getElements()) {
        maxY = Math.max(maxY, element.getY() + element.getHeight() + 10);
        maxX = Math.max(maxX, element.getX() + element.getWidth() + 10);
      }
      if (resize) {
        model.setWidth(maxX);
        model.setHeight(maxY);
      } else {
        model.setWidth(256);
        model.setHeight(256);
      }
    }
    Collection<BioEntity> bioEntitesRequiringLayout = new HashSet<>();

    for (final Element element : model.getElements()) {
      if (element.getWidth() == 0 || element.getHeight() == 0) {
        bioEntitesRequiringLayout.add(element);
      } else if (element instanceof SpeciesWithModificationResidue) {
        for (final ModificationResidue mr : ((SpeciesWithModificationResidue) element).getModificationResidues()) {
          if (mr.getX() == null || mr.getY() == null) {
            bioEntitesRequiringLayout.add(element);
          }
        }
      }
    }
    for (final Reaction reaction : model.getReactions()) {
      if (!hasLayout(reaction)) {
        updateModifierTypes(reaction);
        bioEntitesRequiringLayout.add(reaction);
      }
    }
    try {
      if (bioEntitesRequiringLayout.size() > 0) {
        new ApplySimpleLayoutModelCommand(model, bioEntitesRequiringLayout, true).execute();
      }
    } catch (final CommandExecutionException e) {
      throw new InvalidInputDataExecption("Problem with generating layout", e);
    }
    new ZIndexPopulator().populateZIndex(model);
  }

  private void updateModifierTypes(final Reaction reaction) {
    Set<Modifier> modifiersToBeRemoved = new HashSet<>();
    Set<Modifier> modifiersToBeAdded = new HashSet<>();
    for (final Modifier modifier : reaction.getModifiers()) {
      if (modifier.getClass() == Modifier.class) {
        modifiersToBeRemoved.add(modifier);
        modifiersToBeAdded.add(new Modulation(modifier.getElement()));
      }
    }
    for (final Modifier modifier : modifiersToBeRemoved) {
      reaction.removeModifier(modifier);
    }
    for (final Modifier modifier : modifiersToBeAdded) {
      reaction.addModifier(modifier);
    }
  }

  private boolean hasLayout(final Reaction reaction) {
    for (AbstractNode node : reaction.getNodes()) {
      if (node.getLine() == null) {
        return false;
      } else if (node.getLine().length() == 0) {
        return false;
      }
    }

    return true;
  }

  private Layout getSbmlLayout(final org.sbml.jsbml.Model sbmlModel) {
    Layout layout = null;

    if (sbmlModel.getExtensionCount() > 0) {
      for (final SBasePlugin plugin : sbmlModel.getExtensionPackages().values()) {
        if (plugin.getClass().equals(org.sbml.jsbml.ext.layout.LayoutModelPlugin.class)) {
          LayoutModelPlugin layoutPlugin = (LayoutModelPlugin) plugin;
          if (layoutPlugin.getLayoutCount() == 0) {
            logger.warn("Layout plugin available but no layouts defined");
          } else if (layoutPlugin.getLayoutCount() > 1) {
            logger.warn(layoutPlugin.getLayoutCount() + " layouts defined. Using first one.");
            layout = layoutPlugin.getLayout(0);
          } else {
            layout = layoutPlugin.getLayout(0);
          }
        }
      }
    }
    return layout;
  }

  private void checkAvailableExtensions(final org.sbml.jsbml.Model sbmlModel) {
    if (sbmlModel.getExtensionCount() > 0) {
      for (final SBasePlugin plugin : sbmlModel.getExtensionPackages().values()) {
        if (!plugin.getClass().equals(LayoutModelPlugin.class)
            && !plugin.getClass().equals(MultiModelPlugin.class)) {
          logger.warn("Unknown sbml plugin: " + plugin);
        }
      }
    }
  }

  public void setProvideDefaults(final boolean b) {
    this.provideDefaults = b;
  }

}
