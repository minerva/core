package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.model.map.species.IonChannelProtein;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.RelAbsVector;

import java.util.ArrayList;
import java.util.List;

public class IonChannelProteinShapeFactory extends AShapeFactory<IonChannelProtein> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    final List<GraphicalPrimitive1D> result = new ArrayList<>();

    result.add(createRoundedRect(
        new RelAbsVector(0, 0), new RelAbsVector(0, 0),
        new RelAbsVector(-ION_CHANNEL_WIDTH - 1, 100), new RelAbsVector(0, 100),
        AShapeFactory.RECTANGLE_CORNER_ARC_SIZE));

    result.add(createRoundedRect(
        new RelAbsVector(-ION_CHANNEL_WIDTH, 100), new RelAbsVector(0, 0),
        new RelAbsVector(ION_CHANNEL_WIDTH, 0), new RelAbsVector(0, 100),
        AShapeFactory.RECTANGLE_CORNER_ARC_SIZE));

    return result;
  }

}
