package lcsb.mapviewer.converter.model.sbml.reaction;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.PolylineDataFactory;
import lcsb.mapviewer.converter.model.sbml.SbmlBioEntityExporter;
import lcsb.mapviewer.converter.model.sbml.SbmlExtension;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.modifier.Inhibition;
import lcsb.mapviewer.model.map.modifier.Trigger;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.sbo.SBOTermModifierType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.ASTNode;
import org.sbml.jsbml.KineticLaw;
import org.sbml.jsbml.LocalParameter;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.SimpleSpeciesReference;
import org.sbml.jsbml.Species;
import org.sbml.jsbml.SpeciesReference;
import org.sbml.jsbml.ext.layout.AbstractReferenceGlyph;
import org.sbml.jsbml.ext.layout.BoundingBox;
import org.sbml.jsbml.ext.layout.Curve;
import org.sbml.jsbml.ext.layout.CurveSegment;
import org.sbml.jsbml.ext.layout.Dimensions;
import org.sbml.jsbml.ext.layout.LineSegment;
import org.sbml.jsbml.ext.layout.Point;
import org.sbml.jsbml.ext.layout.ReactionGlyph;
import org.sbml.jsbml.ext.layout.SpeciesReferenceGlyph;
import org.sbml.jsbml.ext.layout.SpeciesReferenceRole;
import org.sbml.jsbml.ext.render.ColorDefinition;
import org.sbml.jsbml.ext.render.LocalStyle;
import org.sbml.jsbml.ext.render.Polygon;
import org.w3c.dom.Node;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Stroke;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SbmlReactionExporter extends SbmlBioEntityExporter<Reaction, org.sbml.jsbml.Reaction> {
  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  private static final double RECT_SIZE = 10;

  private SbmlBioEntityExporter<lcsb.mapviewer.model.map.species.Species, Species> speciesExporter;

  private SbmlBioEntityExporter<Compartment, org.sbml.jsbml.Compartment> compartmentExporter;

  private Map<ReactionNode, SimpleSpeciesReference> speciesReferenceByReactionNode = new HashMap<>();

  public SbmlReactionExporter(final Model sbmlModel, final lcsb.mapviewer.model.map.model.Model minervaModel,
                              final SbmlBioEntityExporter<lcsb.mapviewer.model.map.species.Species, Species> speciesExporter,
                              final Collection<SbmlExtension> sbmlExtensions,
                              final SbmlBioEntityExporter<Compartment, org.sbml.jsbml.Compartment> compartmentExporter) {
    super(sbmlModel, minervaModel, sbmlExtensions);
    this.speciesExporter = speciesExporter;
    this.compartmentExporter = compartmentExporter;
  }

  private KineticLaw createKineticLaw(final Reaction reaction) throws InconsistentModelException {
    SbmlKinetics kinetics = reaction.getKinetics();
    KineticLaw result = new KineticLaw();
    for (final SbmlParameter minervaParameter : kinetics.getParameters()) {
      if (getMinervaModel().getParameterById(minervaParameter.getElementId()) == null) {
        LocalParameter parameter = new LocalParameter();
        parameter.setId(minervaParameter.getElementId());
        parameter.setName(minervaParameter.getName());
        if (minervaParameter.getValue() != null) {
          parameter.setValue(minervaParameter.getValue());
        }
        if (minervaParameter.getUnits() != null) {
          parameter.setUnits(minervaParameter.getUnits().getUnitId());
        }
        result.addLocalParameter(parameter);
      }
    }

    try {
      Node node = XmlParser.getXmlDocumentFromString(kinetics.getDefinition());
      for (final Node ciNode : XmlParser.getAllNotNecessirellyDirectChild("ci", node)) {
        String id = XmlParser.getNodeValue(ciNode).trim();
        Element element = getMinervaModel().getElementByElementId(id);
        if (element != null) {
          String sbmlId = null;
          Species species = speciesExporter.getSbmlElementByElementId(id);
          if (species != null) {
            sbmlId = species.getId();
          } else {
            sbmlId = compartmentExporter.getSbmlElementByElementId(id).getId();
          }
          ciNode.setTextContent(sbmlId);
        }
      }
      String definition = XmlParser.nodeToString(node);
      definition = definition.replace(" xmlns=\"http://www.sbml.org/sbml/level2/version4\"", "");
      definition = definition.replace("<math>", "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">");
      result.setMath(ASTNode.parseMathML(definition));
      return result;
    } catch (final InvalidXmlSchemaException e) {
      throw new InconsistentModelException(e);
    }

  }

  private String getReactionId(final Reaction reaction) {
    String elementId = getElementId(reaction);
    int separatorIndex = elementId.indexOf("__");
    if (separatorIndex > 0) {
      return elementId.substring(0, separatorIndex);
    }
    return elementId;
  }

  @Override
  protected Collection<Reaction> getElementList() {
    return getMinervaModel().getReactions();
  }

  @Override
  public org.sbml.jsbml.Reaction createSbmlElement(final Reaction reaction) throws InconsistentModelException {
    String reactionId = getReactionId(reaction);
    for (final ReactionNode node : reaction.getReactionNodes()) {
      if (node.getElement() instanceof Compartment) {
        logger.warn(new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, reaction),
            "Reaction to compartment is not supported");
        return null;
      }
    }
    org.sbml.jsbml.Reaction result = super.getSbmlElementByElementId(reactionId);
    if (result != null) {
      return result;
    }
    result = getSbmlModel().createReaction(reactionId);
    result.setReversible(reaction.isReversible());
    result.setSBOTerm(reaction.getSboTerm());
    for (final Product product : reaction.getProducts()) {
      Species sbmlSymbol = speciesExporter
          .getSbmlElementByElementId(speciesExporter.getElementId(product.getElement()));
      SpeciesReference speciesReference = result.createProduct(sbmlSymbol);
      speciesReference.setConstant(false);
      speciesReferenceByReactionNode.put(product, speciesReference);
    }
    for (final Reactant reactant : reaction.getReactants()) {
      Species sbmlSymbol = speciesExporter
          .getSbmlElementByElementId(speciesExporter.getElementId(reactant.getElement()));
      SpeciesReference speciesReference = result.createReactant(sbmlSymbol);
      speciesReference.setConstant(false);
      speciesReferenceByReactionNode.put(reactant, speciesReference);
    }
    for (final Modifier modifier : reaction.getModifiers()) {
      Species sbmlSymbol = speciesExporter
          .getSbmlElementByElementId(speciesExporter.getElementId(modifier.getElement()));
      SimpleSpeciesReference speciesReference = result.createModifier(sbmlSymbol);
      speciesReferenceByReactionNode.put(modifier, speciesReference);
      String term = SBOTermModifierType.getTermByType(modifier.getClass());
      if (term != null) {
        speciesReference.setSBOTerm(term);
      }
    }
    if (reaction.getKinetics() != null) {
      result.setKineticLaw(createKineticLaw(reaction));
    }
    return result;
  }

  @Override
  protected String getSbmlIdKey(final Reaction reaction) {
    return reaction.getClass().getSimpleName() + "\n" + getElementId(reaction);
  }

  @Override
  protected void assignLayoutToGlyph(final Reaction reaction, final AbstractReferenceGlyph glyph) {
    ReactionGlyph reactionGlyph = (ReactionGlyph) glyph;
    boolean firstReactant = true;
    reactionGlyph.setCurve(new Curve());

    for (final Reactant reactant : reaction.getReactants()) {
      SpeciesReferenceGlyph reactantGlyph = createNodeGlyph(reactionGlyph, reactant);
      if (firstReactant) {
        reactantGlyph.setRole(SpeciesReferenceRole.SUBSTRATE);
        for (final NodeOperator operator : reaction.getOperators()) {
          if (operator.isReactantOperator()) {
            addOperatorLineToGlyph(reactionGlyph, operator, false);
            break;
          }
        }
      } else {
        reactantGlyph.setRole(SpeciesReferenceRole.SIDESUBSTRATE);
      }
      firstReactant = false;
      if (isExtensionEnabled(SbmlExtension.RENDER)) {
        LocalStyle style = createStyle(reactant.getLine());
        style.getGroup().setEndHead(reactant.getLine().getBeginAtd().getArrowType().name());

        assignStyleToGlyph(reactantGlyph, style);
      }
    }
    Curve curve = createCurve(reaction.getLine(), false, reaction.getZ());
    for (int i = 0; i < curve.getCurveSegmentCount(); i++) {
      reactionGlyph.getCurve().addCurveSegment(new LineSegment(curve.getCurveSegment(i)));
    }

    boolean firstProduct = true;
    for (final Product product : reaction.getProducts()) {
      SpeciesReferenceGlyph productGlyph = createNodeGlyph(reactionGlyph, product);
      if (firstProduct) {
        productGlyph.setRole(SpeciesReferenceRole.PRODUCT);
        for (final NodeOperator operator : reaction.getOperators()) {
          if (operator.isProductOperator()) {
            addOperatorLineToGlyph(reactionGlyph, operator, true);
            break;
          }
        }
      } else {
        productGlyph.setRole(SpeciesReferenceRole.SIDEPRODUCT);
      }
      firstProduct = false;

      if (isExtensionEnabled(SbmlExtension.RENDER)) {
        LocalStyle style = createStyle(product.getLine());
        style.getGroup().setEndHead(product.getLine().getEndAtd().getArrowType().name());

        assignStyleToGlyph(productGlyph, style);
      }

    }

    for (final Modifier modifier : reaction.getModifiers()) {
      SpeciesReferenceGlyph modifierGlyph = createNodeGlyph(reactionGlyph, modifier);
      for (final NodeOperator operator : reaction.getOperators()) {
        if (operator.getInputs().contains(modifier)) {
          modifierGlyph
              .setCurve(joinCurves(modifierGlyph.getCurve(), createCurve(operator.getLine(), false, reaction.getZ())));
        }
      }
      if (modifier instanceof Inhibition) {
        modifierGlyph.setRole(SpeciesReferenceRole.INHIBITOR);
      } else if (modifier instanceof Trigger) {
        modifierGlyph.setRole(SpeciesReferenceRole.ACTIVATOR);
      } else {
        modifierGlyph.setRole(SpeciesReferenceRole.MODIFIER);
      }
      LocalStyle style = createStyle(modifier.getLine());
      style.getGroup().setEndHead(modifier.getLine().getEndAtd().getArrowType().name());

      if (isExtensionEnabled(SbmlExtension.RENDER)) {
        assignStyleToGlyph(modifierGlyph, style);
      }
    }

    if (isExtensionEnabled(SbmlExtension.RENDER)) {
      assignStyleToGlyph(reactionGlyph, createStyle(reaction));
    }
    if (reaction.getProcessCoordinates() != null) {
      if (reactionGlyph.getBoundingBox() == null) {
        reactionGlyph.setBoundingBox(new BoundingBox());
      }
      if (reactionGlyph.getBoundingBox().getPosition() == null) {
        int rectSize = 10;
        double x = reaction.getProcessCoordinates().getX() - rectSize / 2;
        double y = reaction.getProcessCoordinates().getY() - rectSize / 2;
        reactionGlyph.getBoundingBox().setPosition(new Point(x, y, reaction.getZ()));
        reactionGlyph.getBoundingBox().setDimensions(
            new Dimensions(rectSize, rectSize, 0, getSbmlModel().getLevel(), getSbmlModel().getVersion()));
      }
      reactionGlyph.getBoundingBox().getPosition().setZ(reaction.getZ());
    }

    removeColinearPoints(reactionGlyph);
  }

  @Override
  protected AbstractReferenceGlyph createElementGlyph(final String sbmlElementId, final String glyphId) {
    String effectiveGlyphId = glyphId;
    int separatorIndex = effectiveGlyphId.indexOf("__");
    if (separatorIndex > 0) {
      effectiveGlyphId = effectiveGlyphId.substring(separatorIndex + 2);
    }
    ReactionGlyph reactionGlyph;
    try {
      // handle case when input data cannot doesn't come from SBML parser and contains
      // "__" that mess up identifier uniqueness
      reactionGlyph = getLayout().createReactionGlyph(effectiveGlyphId, sbmlElementId);
    } catch (final IllegalArgumentException e) {
      effectiveGlyphId += "_" + getNextId();
      reactionGlyph = getLayout().createReactionGlyph(effectiveGlyphId, sbmlElementId);
    }

    return reactionGlyph;
  }

  @Override
  protected LocalStyle createStyle(final Reaction reaction) {
    LocalStyle style = createStyle(reaction.getLine());
    ColorDefinition color = getColorDefinition(Color.WHITE);
    style.getGroup().setFill(color.getId());

    Point2D startPoint = reaction.getLine().getLines().get((reaction.getLine().getLines().size() - 1) / 2).getP1();
    Point2D endPoint = reaction.getLine().getLines().get((reaction.getLine().getLines().size() - 1) / 2).getP2();

    double pointX = (startPoint.getX() + endPoint.getX()) / 2;
    double pointY = (startPoint.getY() + endPoint.getY()) / 2;
    double dx = endPoint.getX() - startPoint.getX();
    double dy = endPoint.getY() - startPoint.getY();
    double angle = Math.atan2(dy, dx);

    if (reaction.getProcessCoordinates() != null) {
      angle = 0;
      pointX = reaction.getProcessCoordinates().getX();
      pointY = reaction.getProcessCoordinates().getY();
    }
    angle += Math.PI / 4;

    Polygon polygon = new Polygon();

    double dist = Math.sqrt(2) * RECT_SIZE / 2;

    createAbsolutePoint(polygon, pointX - dist * Math.cos(angle), pointY - dist * Math.sin(angle));
    createAbsolutePoint(polygon, pointX + dist * Math.sin(angle), pointY - dist * Math.cos(angle));
    createAbsolutePoint(polygon, pointX + dist * Math.cos(angle), pointY + dist * Math.sin(angle));
    createAbsolutePoint(polygon, pointX - dist * Math.sin(angle), pointY + dist * Math.cos(angle));
    style.getGroup().addElement(polygon);

    return style;

  }

  private LocalStyle createStyle(final PolylineData line) {
    LocalStyle style = createStyle();
    ColorDefinition color = getColorDefinition(line.getColor());
    style.getGroup().setStroke(color.getId());
    style.getGroup().setFill(color.getId());
    style.getGroup().setStrokeWidth(line.getWidth());
    style.getGroup().setStrokeDashArray(strokeToArray(line.getType().getStroke(line.getWidth())));

    return style;
  }

  void removeColinearPoints(final ReactionGlyph glyph) {
    if (glyph.getCurve().getCurveSegmentCount() > 0) {
      PolylineData line = createLine(glyph.getCurve());
      line = PolylineDataFactory.removeCollinearPoints(line);
      Curve curve = createCurve(line, false, (int) glyph.getCurve().getCurveSegment(0).getStart().getZ());
      glyph.setCurve(curve);
    }
  }

  private PolylineData createLine(final Curve curve) {
    PolylineData result = new PolylineData();
    for (int i = 0; i < curve.getCurveSegmentCount(); i++) {
      CurveSegment segment = curve.getCurveSegment(i);
      result.addLine(segment.getStart().getX(), segment.getStart().getY(),
          segment.getEnd().getX(), segment.getEnd().getY());

    }
    return result;
  }

  private List<Short> strokeToArray(final Stroke stroke) {
    if (stroke instanceof BasicStroke) {
      BasicStroke basicStroke = (BasicStroke) stroke;
      if (basicStroke.getDashArray() != null) {
        List<Short> result = new ArrayList<>();

        for (final float entry : basicStroke.getDashArray()) {
          result.add((short) entry);
        }
        return result;
      }
    }
    return null;
  }

  private void addOperatorLineToGlyph(final ReactionGlyph reactantGlyph, final NodeOperator operator, final boolean reverse) {
    Curve curve = reactantGlyph.getCurve();
    List<Line2D> lines = operator.getLine().getLines();
    if (reverse) {
      lines = operator.getLine().reverse().getLines();
    }
    for (final Line2D line : lines) {
      if (line.getP1().distance(line.getP2()) > Configuration.EPSILON) {
        LineSegment segment = new LineSegment();
        segment.setStart(new Point(line.getX1(), line.getY1()));
        segment.setEnd(new Point(line.getX2(), line.getY2()));
        curve.addCurveSegment(segment);
      }
    }
  }

  private SpeciesReferenceGlyph createNodeGlyph(final ReactionGlyph reactionGlyph, final ReactionNode node) {
    SpeciesReferenceGlyph reactantGlyph = reactionGlyph.createSpeciesReferenceGlyph("node_" + getNextId());
    reactantGlyph.setSpeciesGlyph(
        speciesExporter.getSbmlGlyphByElementId(speciesExporter.getElementId(node.getElement())).getId());
    Curve curve = createCurve(node.getLine(), node instanceof Reactant, node.getReaction().getZ());
    if (curve.getCurveSegmentCount() == 0) {
      logger.warn(new ElementUtils().getElementTag(node) + " Problematic line");
    }
    reactantGlyph.setCurve(curve);
    reactantGlyph.setSpeciesReference(speciesReferenceByReactionNode.get(node));
    return reactantGlyph;
  }

  private Curve createCurve(final PolylineData polyline, final boolean reverse, final int z) {
    Curve curve = new Curve();
    List<Line2D> lines = polyline.getLines();
    if (reverse) {
      lines = polyline.reverse().getLines();
    }
    for (final Line2D line : lines) {
      if (line.getP1().distance(line.getP2()) > Configuration.EPSILON) {
        LineSegment segment = new LineSegment();
        segment.setStart(new Point(line.getX1(), line.getY1(), z));
        segment.setEnd(new Point(line.getX2(), line.getY2(), z));
        curve.addCurveSegment(segment);
      }
    }
    return curve;
  }

  private Curve joinCurves(final Curve curve1, final Curve curve2) {
    Curve curve = new Curve();
    for (int i = 0; i < curve1.getCurveSegmentCount(); i++) {
      LineSegment source = ((LineSegment) curve1.getCurveSegment(i));
      LineSegment segment = new LineSegment();
      segment.setStart(new Point(source.getStart().getX(), source.getStart().getY()));
      segment.setEnd(new Point(source.getEnd().getX(), source.getEnd().getY()));
      curve.addCurveSegment(segment);
    }
    for (int i = 0; i < curve2.getCurveSegmentCount(); i++) {
      LineSegment source = ((LineSegment) curve2.getCurveSegment(i));
      LineSegment segment = new LineSegment();
      segment.setStart(new Point(source.getStart().getX(), source.getStart().getY()));
      segment.setEnd(new Point(source.getEnd().getX(), source.getEnd().getY()));
      curve.addCurveSegment(segment);
    }
    return curve;
  }

}
