package lcsb.mapviewer.converter.model.sbml.species.render.sbgn;

import lcsb.mapviewer.converter.model.sbml.extension.render.CustomRenderCubicBezier;
import lcsb.mapviewer.converter.model.sbml.extension.render.CustomRenderPoint;
import lcsb.mapviewer.converter.model.sbml.species.render.AShapeFactory;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;
import org.sbml.jsbml.ext.render.RelAbsVector;

import java.util.ArrayList;
import java.util.List;

public class SbgnSimpleMoleculeShapeFactory extends AShapeFactory<SimpleMolecule> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    final List<GraphicalPrimitive1D> result = new ArrayList<>();

    Polygon polygon = new Polygon();

    CustomRenderPoint p1 = new CustomRenderPoint();
    p1.setX(new RelAbsVector(0, 0));
    p1.setY(new RelAbsVector(0, 0));
    p1.setHeightRelative(50);

    CustomRenderPoint p2 = new CustomRenderPoint();
    p2.setX(new RelAbsVector(0, 100));
    p2.setY(new RelAbsVector(0, 0));
    p2.setHeightRelative(-50);

    CustomRenderCubicBezier p3 = new CustomRenderCubicBezier();
    p3.setX(new RelAbsVector(0, 100));
    p3.setHeightRelative(-50);
    p3.setY(new RelAbsVector(0, 100));

    p3.setX1(new RelAbsVector(0, 100));
    p3.setHeightRelative1(15);
    p3.setY1(new RelAbsVector(0, 0));

    p3.setX2(new RelAbsVector(new RelAbsVector(0, 100)));
    p3.setHeightRelative2(15);
    p3.setY2(new RelAbsVector(0, 100));

    CustomRenderPoint p4 = new CustomRenderPoint();
    p4.setX(new RelAbsVector(0, 0));
    p4.setY(new RelAbsVector(0, 100));
    p4.setHeightRelative(50);


    CustomRenderCubicBezier p5 = new CustomRenderCubicBezier();
    p5.setX(new RelAbsVector(0, 0));
    p5.setHeightRelative(50);
    p5.setY(new RelAbsVector(0, 0));

    p5.setX1(new RelAbsVector(0, 0));
    p5.setHeightRelative1(-15);
    p5.setY1(new RelAbsVector(0, 100));

    p5.setX2(new RelAbsVector(0, 0));
    p5.setHeightRelative2(-15);
    p5.setY2(new RelAbsVector(0, 0));

    polygon.addElement(p1);
    polygon.addElement(p2);
    polygon.addElement(p3);
    polygon.addElement(p4);
    polygon.addElement(p5);

    result.add(polygon);

    return result;

  }

}
