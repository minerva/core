package lcsb.mapviewer.converter.model.sbml;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.converter.model.sbml.compartment.SbmlCompartmentExporter;
import lcsb.mapviewer.converter.model.sbml.reaction.SbmlReactionExporter;
import lcsb.mapviewer.converter.model.sbml.species.SbmlSpeciesExporter;
import lcsb.mapviewer.converter.model.sbml.units.SbmlUnitExporter;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.species.Species;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.SBMLWriter;
import org.sbml.jsbml.ext.layout.Dimensions;
import org.sbml.jsbml.ext.layout.Layout;
import org.sbml.jsbml.ext.layout.LayoutModelPlugin;
import org.sbml.jsbml.ext.multi.MultiModelPlugin;
import org.sbml.jsbml.ext.render.GlobalRenderInformation;
import org.sbml.jsbml.ext.render.RenderLayoutPlugin;

import javax.xml.stream.XMLStreamException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class SbmlExporter {

  public static final int SUPPORTED_VERSION = 3;
  public static final int SUPPORTED_LEVEL = 2;

  private static final Logger logger = LogManager.getLogger();

  /**
   * Set of SBML extensions that should be used during export.
   */
  private final Set<SbmlExtension> usedExtensions = new HashSet<>(Arrays.asList(SbmlExtension.values()));

  private boolean provideDefaults = true;

  /**
   * Export input model into SBML xml.
   *
   * @param model input model
   * @return SBML xml string
   * @throws InconsistentModelException thrown when there is problem with input model
   */
  public String toXml(final lcsb.mapviewer.model.map.model.Model model)
      throws InconsistentModelException {
    try {
      SBMLDocument doc = toSbmlDocument(model);

      ByteArrayOutputStream stream = new ByteArrayOutputStream();
      SBMLWriter.write(doc, stream, "minerva", Configuration.getSystemVersion(null));
      return stream.toString("UTF-8")
          // COPASI does not accept double coordinates
          .replace(".0\"", "\"")
          .replace(".0+", "+")
          .replace(".0%\"", "%\"");
    } catch (final UnsupportedEncodingException | XMLStreamException e) {
      throw new InvalidStateException(e);
    }
  }

  /**
   * Translates input model into SBML model. {@link #usedExtensions} define which
   * SBML extensions should be enabled in the SBML model.
   *
   * @param model input model
   * @return SBML model
   * @throws InconsistentModelException thrown when there is problem with input model
   */
  protected SBMLDocument toSbmlDocument(final lcsb.mapviewer.model.map.model.Model model) throws InconsistentModelException {
    SBMLDocument doc = new SBMLDocument(SUPPORTED_VERSION, SUPPORTED_LEVEL);
    Model result = doc.createModel();
    try {
      result.setId(model.getIdModel());
    } catch (final IllegalArgumentException e) {
      logger.warn("Invalid model identifier: \"" + model.getIdModel() + "\". Ignoring.");
    }
    if (model.getIdModel() == null || model.getIdModel().isEmpty()) {
      model.setIdModel("unknown");
    }
    result.setName(model.getName());
    try {
      result.setNotes(NotesUtility.prepareNotesNode(model.getNotes()));
    } catch (final XMLStreamException e) {
      throw new InvalidStateException(e);
    }

    if (usedExtensions.contains(SbmlExtension.LAYOUT)) {
      createSbmlLayout(model, result);
    }

    if (usedExtensions.contains(SbmlExtension.MULTI)) {
      createSbmlMultiPlugin(result);
    }

    SbmlCompartmentExporter compartmentExporter = new SbmlCompartmentExporter(result, model, usedExtensions);
    compartmentExporter.setProvideDefaults(provideDefaults);

    XmlAnnotationParser parser = new XmlAnnotationParser(new ArrayList<>(), true);
    String rdf = parser.dataSetToXmlString(model.getMiriamData(), model.getAuthors(), model.getCreationDate(),
        model.getModificationDates(), model.getIdModel());
    try {
      result.setAnnotation(NotesUtility.getRdfNode(rdf));
    } catch (final XMLStreamException e1) {
      throw new InconsistentModelException(e1);
    }

    SbmlBioEntityExporter<Species, org.sbml.jsbml.Species> speciesExporter = new SbmlSpeciesExporter(result, model,
        usedExtensions,
        compartmentExporter);
    speciesExporter.setProvideDefaults(provideDefaults);
    SbmlReactionExporter reactionExporter = new SbmlReactionExporter(result, model, speciesExporter,
        usedExtensions, compartmentExporter);
    SbmlUnitExporter unitExporter = new SbmlUnitExporter(model);
    unitExporter.setProvideDefaults(provideDefaults);
    SbmlParameterExporter parameterExporter = new SbmlParameterExporter(model);
    SbmlFunctionExporter functionExporter = new SbmlFunctionExporter(model);
    SbmlLayerExporter layerExporter = new SbmlLayerExporter(model, usedExtensions);

    compartmentExporter.exportElements();
    speciesExporter.exportElements();
    reactionExporter.exportElements();
    unitExporter.exportUnits(result);
    layerExporter.exportLayers(result);

    parameterExporter.exportParameter(result);
    functionExporter.exportFunction(result);
    return doc;
  }

  /**
   * Create SBML layout for the given model.
   *
   * @param model  input model
   * @param result SBML model where layout should be embedded
   * @return SBML layout
   */
  public Layout createSbmlLayout(final lcsb.mapviewer.model.map.model.Model model, final Model result) {
    LayoutModelPlugin layoutPlugin = new LayoutModelPlugin(result);
    Layout layout = new Layout();
    layout.setId("minerva_layout");
    Dimensions dimensions = new Dimensions();
    if (model.getHeight() > 0) {
      dimensions.setHeight(model.getHeight());
    } else {
      dimensions.setHeight(640);
    }
    if (model.getWidth() > 0) {
      dimensions.setWidth(model.getWidth());
    } else {
      dimensions.setWidth(480);
    }
    layout.setDimensions(dimensions);
    layoutPlugin.add(layout);
    result.addExtension("layout", layoutPlugin);

    if (usedExtensions.contains(SbmlExtension.RENDER)) {
      createSbmlRenderPlugin(layout);
    }
    return layout;
  }

  protected MultiModelPlugin createSbmlMultiPlugin(final Model result) {
    MultiModelPlugin multiPlugin = new MultiModelPlugin(result);
    result.addExtension("multi", multiPlugin);
    return multiPlugin;
  }

  /**
   * Creates SBML render plugin for SBML model.
   *
   * @param layout SBML layout where render package will be used
   * @return render plugin
   */
  private RenderLayoutPlugin createSbmlRenderPlugin(final Layout layout) {
    RenderLayoutPlugin renderPlugin = new RenderLayoutPlugin(layout);
    renderPlugin.setRenderInformation(new GlobalRenderInformation());
    layout.addExtension("render", renderPlugin);
    return renderPlugin;
  }

  /**
   * Removes set of extensions from export.
   *
   * @param sbmlExtensions set of extensions that shouldn't be used during export
   */
  public void removeSbmlExtensions(final Collection<SbmlExtension> sbmlExtensions) {
    usedExtensions.removeAll(sbmlExtensions);
  }

  /**
   * Adds {@link SbmlExtension} to export
   *
   * @param sbmlExtension extension that should be used during export
   */
  public void addSbmlExtension(final SbmlExtension sbmlExtension) {
    usedExtensions.add(sbmlExtension);
  }

  /**
   * @return set of extensions that will be used during export
   */
  public Set<SbmlExtension> getSbmlExtensions() {
    return usedExtensions;
  }

  /**
   * Removes extension from export
   *
   * @param sbmlExtension {@link SbmlExtension} that shouldn't be used during export
   */
  public void removeSbmlExtension(final SbmlExtension sbmlExtension) {
    usedExtensions.remove(sbmlExtension);
  }

  public void setProvideDefaults(final boolean provideDefaults) {
    this.provideDefaults = provideDefaults;
  }
}
