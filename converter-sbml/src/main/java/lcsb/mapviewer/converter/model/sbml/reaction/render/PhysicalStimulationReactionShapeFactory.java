package lcsb.mapviewer.converter.model.sbml.reaction.render;

import lcsb.mapviewer.model.map.reaction.type.PhysicalStimulationReaction;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.List;

public class PhysicalStimulationReactionShapeFactory extends ReactionShapeFactory<PhysicalStimulationReaction> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return super.getShapeDefinition(PhysicalStimulationReaction.class);
  }
}
