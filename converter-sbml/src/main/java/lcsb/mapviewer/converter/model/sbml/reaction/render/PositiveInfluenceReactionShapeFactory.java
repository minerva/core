package lcsb.mapviewer.converter.model.sbml.reaction.render;

import lcsb.mapviewer.model.map.reaction.type.PositiveInfluenceReaction;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;

import java.util.List;

public class PositiveInfluenceReactionShapeFactory extends ReactionShapeFactory<PositiveInfluenceReaction> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    return super.getShapeDefinition(PositiveInfluenceReaction.class);
  }
}
