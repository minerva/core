package lcsb.mapviewer.converter.model.sbml.species.render;

import lcsb.mapviewer.converter.model.sbml.extension.render.CustomRenderPoint;
import lcsb.mapviewer.model.map.Drawable;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;
import org.sbml.jsbml.ext.render.RelAbsVector;

import java.util.ArrayList;
import java.util.List;

public class BlankCrossbarArrowShapeFactory extends AShapeFactory<Drawable> {

  @Override
  public List<GraphicalPrimitive1D> getShapeDefinition() {
    List<GraphicalPrimitive1D> result = new ArrayList<>(new BlankArrowShapeFactory().getShapeDefinition());
    final Polygon polygon = new Polygon();

    double s = Math.sin(0.875 * Math.PI);
    double c = Math.cos(0.875 * Math.PI);

    final CustomRenderPoint p1 = new CustomRenderPoint();
    p1.setX(new RelAbsVector(-5, 100 * (1 + c)));
    p1.setY(new RelAbsVector(0, 50));
    p1.setWidthRelative(s * 100);

    final CustomRenderPoint p2 = new CustomRenderPoint();
    p2.setX(new RelAbsVector(-5, 100 * (1 + c)));
    p2.setY(new RelAbsVector(0, 50));
    p2.setWidthRelative(-s * 100);

    polygon.addElement(p1);
    polygon.addElement(p2);

    result.add(polygon);

    return result;
  }

}
