<?xml version="1.0" encoding="UTF-8"?>
<Pathway xmlns="http://pathvisio.org/GPML/2013a" Name="Sphingolipid Metabolism" Organism="Saccharomyces cerevisiae">
  <Comment Source="GenMAPP remarks">Based on http://pathway.yeastgenome.org/biocyc/</Comment>
  <Comment Source="WikiPathways-category">Metabolic Process</Comment>
  <Comment Source="WikiPathways-description">Sphingolipids are essential components of the plasma membrane in all eukaryotic cells.  S. cerevisiae cells make three complex sphingolipids: inositol-phosphoceramide (IPC), mannose-inositol-phosphoceramide (MIPC), and mannose-(inositol phosphate)2-ceramide (M(IP)2C)(CITS: [12069845]).  In the yeast plasma membrane sphingolipids concentrate with ergosterol to form lipid rafts, specialized membrane microdomains implicated in a variety of cellular processes, including sorting of membrane proteins and lipids, as well as organizing and regulating signaling cascades (CITS: [12452424]).  Intermediates in sphingolipid biosynthesis have been shown to play important roles as signaling molecules and growth regulators.  Sphingolipid long chain bases (LCBs), dihydrosphingosine (DHS) and phytosphingosine (PHS), have been implicated as secondary messengers in signaling pathways that regulate heat stress response (CITS: [9405471])(CITS: [11967828]).  Other intermediates, phytoceramide and long-chain base phosphates (LCBPs), have been shown to be components of the tightly-controlled ceramide/LCBP rheostat, which regulates cell growth (CITS: [12684378]).  Since phosphoinositol-containing sphingolipids are unique to fungi, the sphingolipid biosynthesis pathway is considered a target for antifungal drugs (CITS: [9092515])(CITS: [15578972]). 
SOURCE: SGD pathways, http://pathway.yeastgenome.org/server.html</Comment>
  <BiopaxRef>nnb</BiopaxRef>
  <BiopaxRef>fkn</BiopaxRef>
  <BiopaxRef>o80</BiopaxRef>
  <BiopaxRef>sk6</BiopaxRef>
  <Graphics BoardWidth="1180.1122448979595" BoardHeight="886.4885954381756" />
  <DataNode TextLabel="palmityl-CoA" GraphId="ea5" Type="Metabolite">
    <Graphics CenterX="444.2692507002786" CenterY="75.77413865546218" Width="102.33333333333333" Height="19.0" ZOrder="32768" FontSize="10" Valign="Middle" Color="0000ff" />
    <Xref Database="CAS" ID="1763-10-6" />
  </DataNode>
  <DataNode TextLabel="L-serine" GraphId="a7f" Type="Metabolite">
    <Graphics CenterX="679.4145658263305" CenterY="40.54621848739496" Width="67.33333333333333" Height="19.0" ZOrder="32768" FontSize="10" Valign="Middle" Color="0000ff" />
    <Xref Database="CAS" ID="56-45-1" />
  </DataNode>
  <DataNode TextLabel="3-ketodihydrosphoingosine" GraphId="bcfa9" Type="Metabolite">
    <Graphics CenterX="623.2580672268914" CenterY="291.73876050420205" Width="135.58823529411754" Height="20.0" ZOrder="32768" FontSize="10" Valign="Middle" Color="0000ff" />
    <Xref Database="" ID="" />
  </DataNode>
  <Interaction GraphId="idf8f078bd">
    <Graphics ZOrder="12288" LineThickness="1.0">
      <Point X="628.0" Y="75.56416666666667" />
      <Point X="623.2580672268914" Y="281.73876050420205" GraphRef="bcfa9" RelX="0.0" RelY="-1.0" ArrowHead="Arrow" />
      <Anchor Position="0.5168907563025209" Shape="None" />
      <Anchor Position="0.15764705882352933" Shape="None" />
      <Anchor Position="0.0" Shape="None" GraphId="eead0" />
    </Graphics>
    <Xref Database="" ID="" />
  </Interaction>
  <Interaction GraphId="id1089ed5e">
    <Graphics ZOrder="12288" LineThickness="1.0">
      <Point X="444.2692507002786" Y="85.27413865546218" GraphRef="ea5" RelX="0.0" RelY="1.0" />
      <Point X="628.0" Y="75.56416666666667" GraphRef="eead0" RelX="0.0" RelY="0.0" />
    </Graphics>
    <Xref Database="" ID="" />
  </Interaction>
  <Interaction GraphId="idc6d07c44">
    <Graphics ZOrder="12288" LineThickness="1.0">
      <Point X="679.4145658263305" Y="50.04621848739496" GraphRef="a7f" RelX="0.0" RelY="1.0" />
      <Point X="628.0" Y="75.56416666666667" GraphRef="eead0" RelX="0.0" RelY="0.0" />
    </Graphics>
    <Xref Database="" ID="" />
  </Interaction>
  <InfoBox CenterX="10.0" CenterY="13.333333333333334" />
  <Legend CenterX="0.0" CenterY="0.0" />
  <Biopax>
    <bp:openControlledVocabulary xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#">
      <bp:TERM xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:datatype="http://www.w3.org/2001/XMLSchema#string">sphingolipid metabolic pathway</bp:TERM>
      <bp:ID xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PW:0000197</bp:ID>
      <bp:Ontology xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Pathway Ontology</bp:Ontology>
    </bp:openControlledVocabulary>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="fkn">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">12069845</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Sphingolipid functions in Saccharomyces cerevisiae.</bp:TITLE>
      <bp:SOURCE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Biochim Biophys Acta</bp:SOURCE>
      <bp:YEAR rdf:datatype="http://www.w3.org/2001/XMLSchema#string">2002</bp:YEAR>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Dickson RC</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Lester RL</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="o80">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">11113186</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Tsc13p is required for fatty acid elongation and localizes to a novel structure at the nuclear-vacuolar interface in Saccharomyces cerevisiae.</bp:TITLE>
      <bp:SOURCE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Mol Cell Biol</bp:SOURCE>
      <bp:YEAR rdf:datatype="http://www.w3.org/2001/XMLSchema#string">2001</bp:YEAR>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Kohlwein SD</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Eder S</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Oh CS</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Martin CE</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Gable K</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Bacikova D</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Dunn T</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="nnb">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">12034738</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Identification and characterization of a Saccharomyces cerevisiae gene, RSB1, involved in sphingoid long-chain base release.</bp:TITLE>
      <bp:SOURCE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">J Biol Chem</bp:SOURCE>
      <bp:YEAR rdf:datatype="http://www.w3.org/2001/XMLSchema#string">2002</bp:YEAR>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Kihara A</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Igarashi Y</bp:AUTHORS>
    </bp:PublicationXref>
    <bp:PublicationXref xmlns:bp="http://www.biopax.org/release/biopax-level3.owl#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" rdf:id="sk6">
      <bp:ID rdf:datatype="http://www.w3.org/2001/XMLSchema#string">15692566</bp:ID>
      <bp:DB rdf:datatype="http://www.w3.org/2001/XMLSchema#string">PubMed</bp:DB>
      <bp:TITLE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Lip1p: a novel subunit of acyl-CoA ceramide synthase.</bp:TITLE>
      <bp:SOURCE rdf:datatype="http://www.w3.org/2001/XMLSchema#string">EMBO J</bp:SOURCE>
      <bp:YEAR rdf:datatype="http://www.w3.org/2001/XMLSchema#string">2005</bp:YEAR>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Vall√É¬©e B</bp:AUTHORS>
      <bp:AUTHORS rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Riezman H</bp:AUTHORS>
    </bp:PublicationXref>
  </Biopax>
</Pathway>

