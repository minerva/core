package lcsb.mapviewer.wikipathway.utils;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

import org.junit.Test;

public class GeoTest {

  @Test
  public void testClosestPointOnSegment() {
    Point2D result = Geo.getClosestPointOnSegment(new Line2D.Double(10, 10, 20, 20), new Point2D.Double(20, 10));
    assertEquals(result, new Point2D.Double(15, 15));
  }

  @Test
  public void testClosestPointOnSegmentEdge() {
    Point2D result = Geo.getClosestPointOnSegment(new Line2D.Double(10, 10, 20, 20), new Point2D.Double(20, 0));
    assertEquals(result, new Point2D.Double(10, 10));
  }

  @Test
  public void testClosestPointOnSegmentEdge2() {
    Point2D result = Geo.getClosestPointOnSegment(new Line2D.Double(10, 10, 20, 20), new Point2D.Double(20, 30));
    assertEquals(result, new Point2D.Double(20, 20));
  }

}
