package lcsb.mapviewer.wikipathway.xml;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.wikipathway.model.GpmlInteractionType;
import lcsb.mapviewer.wikipathway.model.GpmlLineConnectorType;
import lcsb.mapviewer.wikipathway.model.PointData;

public class EdgeParserTest {


  private EdgeParser parser;

  @Before
  public void setUp() {
    parser = new EdgeParser("mapName");
  }

  @Test
  public void testGetPointsWithoutTargetDirection() throws Exception {
    List<PointData> gpmlPoints = new ArrayList<>();
    PointData pointData = new PointData("mapName");
    pointData.setX(370.0);
    pointData.setY(506.9845153753351);
    pointData.setRelX("0.0");
    pointData.setRelY("1.0");
    pointData.setGraphRef("db29a");
    gpmlPoints.add(pointData);

    pointData = new PointData("mapName");
    pointData.setX(337.9360555506242);
    pointData.setY(558.2089552238806);
    gpmlPoints.add(pointData);

    pointData = new PointData("mapName");
    pointData.setX(305.8721111012484);
    pointData.setY(587.6687409348253);
    pointData.setRelX("0.0");
    pointData.setRelY("0.0");
    pointData.setGraphRef("ba36a");
    pointData.setType(GpmlInteractionType.CONVERSION);
    gpmlPoints.add(pointData);

    GpmlLineConnectorType type = GpmlLineConnectorType.ELBOW;

    List<Line2D> result = parser.getPoints(null, gpmlPoints, type);

    assertEquals(0.0, result.get(0).getP1().distance(new Point2D.Double(370.0, 506.9845153753351)), Configuration.EPSILON);
    assertEquals(0.0, result.get(0).getP2().distance(new Point2D.Double(370.0, 558.2089552238806)), Configuration.EPSILON);
    assertEquals(0.0, result.get(1).getP2().distance(new Point2D.Double(305.8721111012484, 558.2089552238806)),
        Configuration.EPSILON);
    assertEquals(0.0, result.get(2).getP2().distance(new Point2D.Double(305.8721111012484, 587.6687409348253)),
        Configuration.EPSILON);
  }

  @Test
  public void testGetPointsWithoutSourceDirection() throws Exception {
    List<PointData> gpmlPoints = new ArrayList<>();
    PointData pointData = new PointData("mapName");
    pointData.setX(418.1490729057133);
    pointData.setY(421.3813636363637);
    pointData.setRelX("0.0");
    pointData.setRelY("0.0");
    pointData.setGraphRef("f78da");
    gpmlPoints.add(pointData);

    pointData = new PointData("mapName");
    pointData.setX(506.83491062039946);
    pointData.setY(396.35686597839583);
    gpmlPoints.add(pointData);

    pointData = new PointData("mapName");
    pointData.setX(506.83491062039946);
    pointData.setY(369.3819137749735);
    gpmlPoints.add(pointData);

    pointData = new PointData("mapName");
    pointData.setX(718.8015247108307);
    pointData.setY(343.0727655099894);
    pointData.setRelX("0.0");
    pointData.setRelY("1.0");
    pointData.setGraphRef("ca8");
    pointData.setType(GpmlInteractionType.ARROW);
    gpmlPoints.add(pointData);

    GpmlLineConnectorType type = GpmlLineConnectorType.ELBOW;

    List<Line2D> result = parser.getPoints(null, gpmlPoints, type);

    assertEquals(0.0, result.get(0).getP1().distance(new Point2D.Double(418.1490729057133, 421.3813636363637)), Configuration.EPSILON);
    assertEquals(0.0, result.get(0).getP2().distance(new Point2D.Double(506.83491062039946, 421.3813636363637)), Configuration.EPSILON);
    assertEquals(0.0, result.get(1).getP2().distance(new Point2D.Double(506.83491062039946, 369.3819137749735)),
        Configuration.EPSILON);
    assertEquals(0.0, result.get(2).getP2().distance(new Point2D.Double(718.8015247108307, 369.3819137749735)),
        Configuration.EPSILON);
    assertEquals(0.0, result.get(3).getP2().distance(new Point2D.Double(718.8015247108307, 343.0727655099894)),
        Configuration.EPSILON);
  }

}
