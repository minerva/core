package lcsb.mapviewer.wikipathway.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;
import lcsb.mapviewer.wikipathway.model.DataNode;
import lcsb.mapviewer.wikipathway.model.GraphicalPathwayElement;
import lcsb.mapviewer.wikipathway.model.Group;
import lcsb.mapviewer.wikipathway.model.Label;
import lcsb.mapviewer.wikipathway.model.Shape;

public class ModelContructorTest extends WikipathwaysTestFunctions {

  private ModelComparator comparator = new ModelComparator();
  private ModelContructor mc;

  private int elementCounter = 0;

  private String mapName = "mapName";

  @Before
  public void setUp() throws Exception {
    mc = new ModelContructor(mapName);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testZIndexForElement() throws Exception {
    Model model = createEmptyModel();
    model.addElement(createProtein());
    ModelToGPML parser = new ModelToGPML(model.getName());
    String xml = parser.getGPML(model);
    assertNotNull(xml);

    Model model2 = new GPMLToModel().getModel(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));
    assertEquals(0, comparator.compare(model2, model));
  }

  @Test
  public void testUpdateAliasWithLowDimension() throws Exception {
    Element protein = createProtein();
    GraphicalPathwayElement gpmlElement = new DataNode("id", "id2");
    gpmlElement.setRectangle(new Rectangle2D.Double(10, 20, 0.3, 0.5));
    mc.updateAlias(gpmlElement, protein);
    assertNotNull(protein.getBorder());
  }

  @Test
  public void testUpdateAliasWithEmptyDimension() throws Exception {
    Element protein = createProtein();
    GraphicalPathwayElement gpmlElement = new DataNode("id", "id2");
    gpmlElement.setRectangle(new Rectangle2D.Double(10, 20, 0, 0));
    mc.updateAlias(gpmlElement, protein);
    assertNotNull(protein.getBorder());
  }

  @Test
  public void testUpdateAliasWithColor() throws Exception {
    Element protein = createProtein();
    GraphicalPathwayElement gpmlElement = new DataNode("id", "id2");
    gpmlElement.setRectangle(new Rectangle2D.Double(10, 20, 10, 110));
    gpmlElement.setColor(Color.GREEN);
    mc.updateAlias(gpmlElement, protein);
    assertEquals(Color.GREEN, protein.getFontColor());
    assertEquals(Color.GREEN, protein.getBorderColor());
  }

  @Test
  public void testRemoveSelfLoops() throws Exception {
    Model model = createEmptyModel();
    Complex c1 = createComplex();
    model.addElement(c1);
    Reaction reaction = new TransportReaction("re");
    reaction.addReactant(new Reactant(c1));
    reaction.addProduct(new Product(c1));
    model.addReaction(reaction);
    mc.removeSelfLoops(model);
    assertEquals(0, model.getReactions().size());
  }

  @Test
  public void testRemoveEmptyComplexes() throws Exception {
    Model model = createEmptyModel();
    model.addElement(new Complex("id"));
    mc.removeEmptyComplexes(model);
    assertEquals(0, model.getElements().size());
  }

  @Test
  public void testRemoveEmptyComplexesWithReactions() throws Exception {
    Model model = createEmptyModel();
    Complex c1 = new Complex("id");
    model.addElement(c1);
    Reaction reaction = new TransportReaction("re");
    reaction.addReactant(new Reactant(c1));
    reaction.addProduct(new Product(c1));
    model.addReaction(reaction);
    mc.removeEmptyComplexes(model);
    assertEquals(0, model.getBioEntities().size());
  }

  @Test
  public void testRemoveEmptyComplexesWithNotEmpmtyComplex() throws Exception {
    Model model = createEmptyModel();
    model.addElement(createComplex());
    mc.removeEmptyComplexes(model);
    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testShouldRotate90degreesWithNoAsAnswer() {
    assertFalse(mc.shouldRotate90degrees(0.0));
    assertFalse(mc.shouldRotate90degrees(Math.PI));
    assertFalse(mc.shouldRotate90degrees(15 * Math.PI));
    assertFalse(mc.shouldRotate90degrees(-Math.PI));
    assertFalse(mc.shouldRotate90degrees((Double) null));
  }

  @Test
  public void testShouldRotate90degrees() {
    assertTrue(mc.shouldRotate90degrees(Math.PI / 2));
    assertTrue(mc.shouldRotate90degrees(15 * Math.PI / 2));
    assertTrue(mc.shouldRotate90degrees(-Math.PI / 2));
  }

  @Test
  public void testShouldRotate90degreesWithWarnings() {
    assertFalse(mc.shouldRotate90degrees(0.0 + 0.5));
    assertEquals(1, super.getWarnings().size());
    assertFalse(mc.shouldRotate90degrees(Math.PI + 0.5));
    assertEquals(2, super.getWarnings().size());
    assertFalse(mc.shouldRotate90degrees(15 * Math.PI + 0.5));
    assertEquals(3, super.getWarnings().size());
    assertFalse(mc.shouldRotate90degrees(-Math.PI + 0.5));
    assertEquals(4, super.getWarnings().size());
    assertTrue(mc.shouldRotate90degrees(Math.PI / 2 + 0.5));
    assertEquals(5, super.getWarnings().size());
    assertTrue(mc.shouldRotate90degrees(15 * Math.PI / 2 + 0.5));
    assertEquals(6, super.getWarnings().size());
    assertTrue(mc.shouldRotate90degrees(-Math.PI / 2 + 0.5));
    assertEquals(7, super.getWarnings().size());
  }

  @Test
  public void testRotate90degrees() {
    Rectangle2D result = mc.rotate90degrees(new Rectangle2D.Double(10, 20, 30, 40));
    assertEquals(5, result.getX(), Configuration.EPSILON);
    assertEquals(25, result.getY(), Configuration.EPSILON);
    assertEquals(40, result.getWidth(), Configuration.EPSILON);
    assertEquals(30, result.getHeight(), Configuration.EPSILON);
  }

  @Test
  public void testCreateElementFromPathwayGroup() {
    Group group = new Group("1", "2", "3");
    group.setStyle("Pathway");
    List<Element> elements = mc.createElementsFromGroup(Arrays.asList(group), mc.new Data());
    assertEquals(1, elements.size());
    assertTrue(elements.get(0) instanceof PathwayCompartment);
  }

  @Test
  public void testCreateRectWithFill() {
    Shape shape = createShape();
    shape.setFillColor(Color.GREEN);
    LayerRect rect = mc.createRectangle(shape);
    assertEquals(shape.getFillColor(), rect.getFillColor());
  }

  @Test
  public void testCreateRectWithZIndex() {
    Shape shape = createShape();
    shape.setzOrder(143);
    LayerRect rect = mc.createRectangle(shape);
    assertEquals((Integer) 143, rect.getZ());
  }

  private Shape createShape() {
    Shape result = new Shape("id" + (elementCounter++), mapName);
    result.setRectangle(new Rectangle2D.Double(10, 20, 90, 80));
    return result;
  }

  @Test
  public void testCreateTextFromLabel() {
    Label label = createLabel();
    label.setTextLabel("first name\nsecond name");
    LayerText text = mc.createText(label);
    assertTrue(text.getNotes().contains("first name second name"));
  }

  private Label createLabel() {
    Label label = new Label("id" + (elementCounter++), mapName);
    label.setRectangle(new Rectangle2D.Double(10, 20, 90, 80));
    return label;
  }

}
