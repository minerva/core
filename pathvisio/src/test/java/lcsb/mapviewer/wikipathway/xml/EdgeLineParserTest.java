package lcsb.mapviewer.wikipathway.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Element;

import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;
import lcsb.mapviewer.wikipathway.model.Edge;

public class EdgeLineParserTest extends WikipathwaysTestFunctions {

  private EdgeLineParser parser;

  @Before
  public void setUp() throws Exception {
    parser = new EdgeLineParser("mapName");
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseLine() throws Exception {
    Element node = fileToNode("testFiles/elements/line.xml");
    Edge edge = parser.parse(node);
    assertNotNull(edge);

    assertEquals(1, edge.getBiopaxReferences().size());
    assertEquals("c64", edge.getBiopaxReferences().get(0));

    assertEquals((Integer) 12288, edge.getzOrder());
    assertEquals(1.0, edge.getLine().getWidth(), EPSILON);
    assertTrue(edge.getComments().contains("line comment"));
    assertTrue(edge.getComments().contains("Type your comment here"));

    assertTrue(new Point2D.Double(730, 265).distance(edge.getLine().getStartPoint()) < EPSILON);
    assertTrue(new Point2D.Double(931.0, 442.0).distance(edge.getLine().getEndPoint()) < EPSILON);

    assertEquals(0, getWarnings().size());
  }

}
