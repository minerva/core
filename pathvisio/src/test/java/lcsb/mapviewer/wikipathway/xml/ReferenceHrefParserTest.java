package lcsb.mapviewer.wikipathway.xml;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;

@RunWith(Parameterized.class)
public class ReferenceHrefParserTest extends WikipathwaysTestFunctions {

  private ReferenceParser mc = new ReferenceParser("mapName");

  private String href;
  private MiriamData result;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  public ReferenceHrefParserTest(final String href, final MiriamData result) {
    this.href = href;
    this.result = result;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<>();
    data.add(new Object[] { "https://www.omim.org/entry/264080", new MiriamData(MiriamType.OMIM, "264080") });
    data.add(new Object[] { "http://www.omim.org/entry/264081", new MiriamData(MiriamType.OMIM, "264081") });
    data.add(new Object[] { "https://www.omim.org/264082", new MiriamData(MiriamType.OMIM, "264082") });
    data.add(new Object[] { "http://www.omim.org/264083", new MiriamData(MiriamType.OMIM, "264083") });
    data.add(new Object[] { "https://omim.org/entry/264084", new MiriamData(MiriamType.OMIM, "264084") });
    data.add(new Object[] { "http://omim.org/entry/264085", new MiriamData(MiriamType.OMIM, "264085") });
    data.add(new Object[] { "https://omim.org/264086", new MiriamData(MiriamType.OMIM, "264086") });
    data.add(new Object[] { "http://omim.org/264087", new MiriamData(MiriamType.OMIM, "264087") });
    return data;
  }

  @Test
  public void testParseHref() throws Exception {
    assertEquals(result, mc.hrefToMiriamData(href));
  }
}
