package lcsb.mapviewer.wikipathway.xml;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.Color;
import java.awt.Shape;
import java.awt.geom.Point2D;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ModelToGPMLTest extends WikipathwaysTestFunctions {

  private int speciesCounter = 0;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGSTP() throws Exception {
    final CellDesignerXmlParser cellDesignerXmlParser = new CellDesignerXmlParser();
    final Model model = cellDesignerXmlParser
        .createModel(new ConverterParams().filename("testFiles/other_full/GSTP1 subnetwork_220214.xml"));
    final ModelToGPML parser = new ModelToGPML(model.getName());
    final String xml = parser.getGPML(model);
    assertNotNull(xml);
    assertEquals(13, getWarnings().size());

    final Model model2 = new GPMLToModel().getModel(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));
    assertNotNull(model2);
  }

  @Test
  public void testAnnotations() throws Exception {

    final Model model = createModel();

    final GenericProtein protein = createProtein();
    protein.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    protein.addMiriamData(new MiriamData(MiriamType.PUBMED, "123"));
    protein.addMiriamData(new MiriamData(MiriamType.PUBMED, "1234"));

    model.addElement(protein);

    final ModelToGPML parser = new ModelToGPML(model.getName());
    final String xml = parser.getGPML(model);
    assertNotNull(xml);
    assertEquals(0, getWarnings().size());

    final Model model2 = new GPMLToModel().getModel(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));

    final Element p = model2.getElementByElementId(protein.getElementId());

    assertEquals(protein.getMiriamData().size(), p.getMiriamData().size());
    for (final MiriamData md : p.getMiriamData()) {
      assertTrue(protein.getMiriamData().contains(md));
    }
  }

  @Test
  public void testHypoteticalComplex() throws Exception {

    final Model model = createModel();

    final Complex complex = createComplex();
    complex.addSpecies(createProtein());
    complex.setHypothetical(false);

    final Complex complex2 = createComplex();
    complex2.addSpecies(createProtein());
    complex2.setHypothetical(true);

    model.addElement(complex);
    model.addElement(complex2);
    model.addElements(complex.getElements());
    model.addElements(complex2.getElements());

    final ModelToGPML parser = new ModelToGPML(model.getName());
    final String xml = parser.getGPML(model);
    assertNotNull(xml);
    assertEquals(0, getWarnings().size());

    final Model model2 = new GPMLToModel().getModel(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));

    final Complex newComplex = model2.getElementByElementId(complex.getElementId());
    final Complex newComplex2 = model2.getElementByElementId(complex2.getElementId());

    assertEquals(complex.isHypothetical(), newComplex.isHypothetical());
    assertEquals(complex2.isHypothetical(), newComplex2.isHypothetical());
  }

  @Test
  public void testComplexName() throws Exception {

    final Model model = createModel();

    final Complex complex = createComplex();
    complex.addSpecies(createProtein());
    complex.setName("blabla");

    model.addElement(complex);
    model.addElements(complex.getElements());

    final ModelToGPML parser = new ModelToGPML(model.getName());
    final String xml = parser.getGPML(model);
    assertNotNull(xml);
    assertEquals(0, getWarnings().size());

    final Model model2 = new GPMLToModel().getModel(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));

    final Complex newComplex = model2.getElementByElementId(complex.getElementId());

    assertEquals(complex.getName(), newComplex.getName());
  }

  @Test
  public void testReactionColor() throws Exception {

    final Model model = createModel();

    final Protein p1 = createProtein();
    final Protein p2 = createProtein();

    model.addElement(p1);
    model.addElement(p2);

    final Reaction reaction = createReaction(p1, p2);

    model.addReaction(reaction);

    final ModelToGPML parser = new ModelToGPML(model.getName());
    final String xml = parser.getGPML(model);

    final Model model2 = new GPMLToModel().getModel(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));

    final Reaction newReaction = model2.getReactionByReactionId(reaction.getElementId());

    assertEquals(reaction.getLine().getColor(), newReaction.getLine().getColor());
  }

  @Test
  public void testReversibleReaction() throws Exception {

    final Model model = createModel();

    final Protein p1 = createProtein();
    final Protein p2 = createProtein();

    model.addElement(p1);
    model.addElement(p2);

    final Reaction reaction = createReaction(p1, p2);
    reaction.setReversible(true);
    reaction.getReactants().get(0).getLine().getBeginAtd().setArrowType(ArrowType.FULL);

    model.addReaction(reaction);

    final ModelToGPML parser = new ModelToGPML(model.getName());
    final String xml = parser.getGPML(model);

    final Model model2 = new GPMLToModel().getModel(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));

    final Reaction newReaction = model2.getReactionByReactionId(reaction.getElementId());

    assertEquals(reaction.isReversible(), newReaction.isReversible());
  }

  private Reaction createReaction(final Species p1, final Species p2) {
    final Reaction reaction = new StateTransitionReaction("re" + (speciesCounter++));
    final Reactant reactant = new Reactant(p1);
    final Product product = new Product(p2);
    reaction.addReactant(reactant);
    reaction.addProduct(product);

    final Point2D start = p1.getCenter();
    final Point2D end = p2.getCenter();

    final PointTransformation pt = new PointTransformation();
    reactant.setLine(new PolylineData(start, pt.getPointOnLine(start, end, 0.3)));
    reaction.setLine(new PolylineData(pt.getPointOnLine(start, end, 0.3), pt.getPointOnLine(start, end, 0.6)));
    product.setLine(new PolylineData(pt.getPointOnLine(start, end, 0.6), end));

    reaction.getLine().setColor(Color.BLUE);
    reactant.getLine().setColor(Color.BLUE);
    product.getLine().setColor(Color.BLUE);
    product.getLine().getEndAtd().setArrowType(ArrowType.FULL);

    reaction.setZ(20);
    return reaction;
  }

  @Test
  public void testProteinModification() throws Exception {

    final Model model = createModel();

    final Protein p1 = createProtein();
    p1.addResidue(createResidue(p1));

    model.addElement(p1);

    final ModelToGPML parser = new ModelToGPML(model.getName());
    final String xml = parser.getGPML(model);

    final Model model2 = new GPMLToModel().getModel(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));

    final Protein newProtein = model2.getElementByElementId(p1.getElementId());

    assertEquals(p1.getModificationResidues().size(), newProtein.getModificationResidues().size());
    assertEquals(p1.getModificationResidues().get(0).getX(), newProtein.getModificationResidues().get(0).getX());
    assertEquals(p1.getModificationResidues().get(0).getY(), newProtein.getModificationResidues().get(0).getY());
  }

  private Residue createResidue(final Protein p1) {
    final Residue residue = new Residue("mr" + speciesCounter++);
    residue.setState(ModificationState.PHOSPHORYLATED);
    residue.setPosition(new Point2D.Double(p1.getCenterX(), p1.getY()));
    residue.setZ(p1.getZ() + 1);
    return residue;
  }

  @Test
  public void testGetRectangle() throws Exception {
    final ModelToGPML parser = new ModelToGPML("xyz");
    final Compartment compartment = createCompartment();
    final Shape r = parser.getShape(compartment);
    assertNotNull(r);
  }

  @Test
  public void testExportUnknownSpecies() throws Exception {
    final ModelToGPML parser = new ModelToGPML("xyz");
    final String content = parser.speciesToDataNode(createUnknown());
    assertNotNull(content);
  }

  @Test
  public void testDegraded() throws Exception {
    final Model model = new ModelFullIndexed(null);
    model.addElement(createDegraded());

    final ModelToGPML parser = new ModelToGPML(model.getName());
    final String xml = parser.getGPML(model);
    final Model model2 = new GPMLToModel().getModel(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));

    assertEquals(0, getWarnings().size());
    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  private Unknown createUnknown() {
    final Unknown result = new Unknown("id" + (speciesCounter++));
    assignCoordinates(1, 1, 10, 10, result);
    return result;
  }

  private Degraded createDegraded() {
    final Degraded result = new Degraded("id" + (speciesCounter++));
    assignCoordinates(1, 1, 10, 10, result);
    result.setName("");

    return result;
  }

  private Compartment createCompartment() {
    final Compartment compartment = new SquareCompartment("ca" + (speciesCounter++));
    compartment.setName("s2");
    assignCoordinates(1, 1, 10, 10, compartment);
    compartment.setNameHorizontalAlign(HorizontalAlign.LEFT);
    compartment.setNameVerticalAlign(VerticalAlign.TOP);
    compartment.setThickness(0);
    compartment.setOuterWidth(0);
    compartment.setInnerWidth(0);
    return compartment;
  }

  @Test
  public void testExportHypothetical() throws Exception {
    final Model model = createModel();

    final Protein p1 = createProtein();
    p1.setHypothetical(true);
    p1.addResidue(createResidue(p1));

    model.addElement(p1);

    final lcsb.mapviewer.wikipathway.GpmlParser parser = new lcsb.mapviewer.wikipathway.GpmlParser();

    final String xml = parser.model2String(model);
    final Model model2 = parser.createModel(new ConverterParams().inputStream(new ByteArrayInputStream(xml.getBytes())));

    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void testExportUnknown() throws Exception {
    final Model model = createModel();

    final Species p1 = createUnknown();

    model.addElement(p1);

    final lcsb.mapviewer.wikipathway.GpmlParser parser = new lcsb.mapviewer.wikipathway.GpmlParser();

    final String xml = parser.model2String(model);
    final Model model2 = parser.createModel(new ConverterParams().inputStream(new ByteArrayInputStream(xml.getBytes())));

    model2.getSpeciesList().get(0).setBorderColor(Unknown.DEFAULT_BORDER_COLOR);
    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void testMapNotes() throws Exception {
    final Model model = createModel();
    model.setNotes("xyz");

    final lcsb.mapviewer.wikipathway.GpmlParser parser = new lcsb.mapviewer.wikipathway.GpmlParser();

    final String xml = parser.model2String(model);
    final Model model2 = parser.createModel(new ConverterParams().inputStream(new ByteArrayInputStream(xml.getBytes())));

    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void testHtmlTagInNotes() throws Exception {
    final Model model = createModel();
    model.setNotes("a < b");

    final Species p1 = createProtein();
    p1.setNotes("<a href='https://google.lu/'>link</a>");

    model.addElement(p1);

    final Species p2 = createProtein();
    model.addElement(p2);

    final Reaction r = createReaction(p1, p2);
    r.setNotes("<xml>tag</xml>");
    model.addReaction(r);

    final Model model2 = serializeModelOverGPML(model);

    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  private Model createModel() {
    final Model model = new ModelFullIndexed(null);
    model.setWidth(1000);
    model.setHeight(1000);
    return model;
  }

  @Test
  public void specialCharacterInCompartmentName() throws Exception {
    final Model model = createModel();
    final Compartment compartment = createCompartment();
    compartment.setName("hello&by");
    model.addElement(compartment);

    final Model model2 = serializeModelOverGPML(model);

    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void specialCharacterInComplexName() throws Exception {
    final Model model = createModel();
    final Element element = createComplex();
    element.setName("hello&by");
    model.addElement(element);

    final Model model2 = serializeModelOverGPML(model);

    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void specialCharacterInProteinName() throws Exception {
    final Model model = createModel();
    final Element element = createProtein();
    element.setName("hello&by");
    model.addElement(element);

    final Model model2 = serializeModelOverGPML(model);

    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void specialCharacterInAnnotation() throws Exception {
    final Model model = createModel();
    final Element element = createProtein();
    element.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "1&2"));
    model.addElement(element);

    final Model model2 = serializeOverGpml(model);

    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void specialCharacterInAuhtorName() throws Exception {
    final Model model = createModel();
    final Author author = new Author("Piotr\"", "Hack");
    model.addAuthor(author);

    final Model model2 = serializeOverGpml(model);

    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void specialCharacterInAuhtorEmail() throws Exception {
    final Model model = createModel();
    final Author author = new Author("", "");
    author.setEmail("hacky@email.com\\\"");
    model.addAuthor(author);

    final Model model2 = serializeOverGpml(model);

    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void specialCharacterInModificationName() throws Exception {
    final Model model = createModel();
    final Protein element = createProtein();
    final BindingRegion mr = createBindingRegion(element);
    mr.setName("hello&by");
    element.addBindingRegion(mr);
    model.addElement(element);

    serializeModelOverGPML(model);
  }

  @Test
  public void specialCharacterInNonEmptyComplexName() throws Exception {
    final Model model = createModel();
    final Complex complex = createComplex();
    final Protein protein = createProtein();
    complex.addSpecies(protein);
    model.addElement(complex);
    model.addElement(protein);

    serializeModelOverGPML(model);
  }

  private BindingRegion createBindingRegion(final Protein p1) {
    final BindingRegion residue = new BindingRegion();
    residue.setIdModificationResidue("mr" + speciesCounter++);
    residue.setPosition(new Point2D.Double(p1.getCenterX(), p1.getY()));
    residue.setName("m");
    return residue;
  }

  @Test
  public void getPolylineFarFromMainLine() throws Exception {
    final ModelToGPML converter = new ModelToGPML("xx");
    final ReactionNode product = new Product(createProtein());
    product.setLine(new PolylineData(new Point2D.Double(10, 0), new Point2D.Double(20, 0)));
    final PolylineData line = converter.getPolyline(product,
        new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(0, 100)));
    assertEquals(0, line.getStartPoint().distance(new Point2D.Double(0, 0)), Configuration.EPSILON);
  }

}
