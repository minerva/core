package lcsb.mapviewer.wikipathway.xml;

import static org.junit.Assert.assertEquals;

import java.io.FileInputStream;

import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.ElementComparator;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;

public class BugTest extends WikipathwaysTestFunctions {
  private ModelComparator mc;

  @Before
  public void setUp() {
    mc = new ModelComparator(1.0);
  }

  @Test
  public void testBug319() throws Exception {
    String fileName = "testFiles/bugs/error_319.gpml";
    FileInputStream fis = new FileInputStream(fileName);
    new GpmlParser().createGraph(fis);
    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testBug328() throws Exception {
    String filename = "testFiles/bugs/error_328.gpml";
    Model model1 = new GPMLToModel().getModel(filename);

    assertEquals(7, getWarnings().size());

    int complexes = 0;
    for (final Element alias : model1.getElements()) {
      if (alias instanceof Complex) {
        complexes++;
      }
    }
    assertEquals("Invalid number of complexes", 1, complexes);

    Model model2 = serializeModelOverCellDesignerParser(model1);
    model2.setNotes(model1.getNotes());

    // export to CellDesigner changes line width
    mc.setElementComparator(new ElementComparator(3));

    assertEquals("File " + filename + " different after transformation", 0, mc.compare(model1, model2));
  }

}
