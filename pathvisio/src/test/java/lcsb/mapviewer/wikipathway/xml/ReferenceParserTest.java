package lcsb.mapviewer.wikipathway.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;

public class ReferenceParserTest extends WikipathwaysTestFunctions {

  private ReferenceParser mc = new ReferenceParser("mapName");

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParse() throws Exception {
    Node node = super.getXmlDocumentFromFile("testFiles/elements/xref.xml");
    MiriamData md = mc.parse((Element) node.getFirstChild());
    assertNotNull(md);
    assertEquals(md, new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testParseUnkown() throws Exception {
    MiriamData md = mc.createMiriamData("unk", "own-");
    assertNull(md);
    assertEquals(1, super.getWarnings().size());
  }

  @Test
  public void testToXml() throws Exception {
    MiriamData md = new MiriamData(MiriamType.CHEBI, "CHEBI:123");
    String xml = mc.toXml(md, null);

    Node node = super.getNodeFromXmlString(xml);
    MiriamData md2 = mc.parse((Element) node);
    assertNotNull(md);
    assertEquals(md, md2);
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testToXmlUnknownType() throws Exception {
    @SuppressWarnings("deprecation")
    MiriamData md = new MiriamData(MiriamType.UNKNOWN, "xyz");
    String xml = mc.toXml(md, null);

    assertNotNull(xml);
    assertEquals(1, getWarnings().size());
  }
}
