package lcsb.mapviewer.wikipathway.xml;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;

@RunWith(Parameterized.class)
public class GenericReferenceParserTest extends WikipathwaysTestFunctions {

  private ReferenceParser mc = new ReferenceParser("mapName");

  private String database;

  private String resource;

  public GenericReferenceParserTest(final String resource, final String database) {
    this.database = database;
    this.resource = resource;
  }

  @Parameters(name = "{index} : {1} ({0})")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<>();
    data.add(new Object[] { "HMDB03791", "HMDB" });
    data.add(new Object[] { "WormBase", "WormBase" });
    data.add(new Object[] { "S000028457", "SGD" });
    data.add(new Object[] { "Q15623825", "Wikidata" });
    data.add(new Object[] { "AT3G18550", "Ensembl Plants" });
    data.add(new Object[] { "657-24-9", "CAS" });
    data.add(new Object[] { "123", "GenBank" });
    data.add(new Object[] { "39484", "pubchem.compound" });
    data.add(new Object[] { "Reactive_oxygen_species", "Wikipedia" });
    data.add(new Object[] { "K00114", "Kegg ortholog" });
    data.add(new Object[] { "110354", "Chemspider" });
    data.add(new Object[] { "PF00071", "Pfam" });
    data.add(new Object[] { "MI0000750", "miRBase" });
    data.add(new Object[] { "478507", "Entrez" });
    data.add(new Object[] { "FBgn0042693", "FlyBase" });
    data.add(new Object[] { "3265", "GeneDB" });
    data.add(new Object[] { "EBI-2339149", "IntAct" });
    data.add(new Object[] { "D11487", "KEGG Drug" });
    data.add(new Object[] { "C00007340", "KNApSAcK" });
    data.add(new Object[] { "LMSP01020002", "LIPID MAPS" });
    data.add(new Object[] { "AAQ75423.1", "NCBI Protein" });
    data.add(new Object[] { "188055", "OMIM" });
    data.add(new Object[] { "PATO:0002220", "pato" });
    data.add(new Object[] { "2244", "PubChem" });
    data.add(new Object[] { "14282017", "PubChem Compound" });
    data.add(new Object[] { "3389", "RGD" });
    data.add(new Object[] { "00006", "SPIKE" });
    data.add(new Object[] { "Q7098084", "TTD Drug" });
    data.add(new Object[] { "ZDB-GENE-030131-1904", "ZFIN" });
    return data;
  }

  @Test
  public void test() throws Exception {
    MiriamData md = mc.createMiriamData(resource, database);
    assertNotNull(md);
  }
}
