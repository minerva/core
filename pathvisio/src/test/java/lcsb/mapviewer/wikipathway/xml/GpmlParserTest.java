package lcsb.mapviewer.wikipathway.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;
import lcsb.mapviewer.wikipathway.model.Graph;
import lcsb.mapviewer.wikipathway.model.Interaction;

public class GpmlParserTest extends WikipathwaysTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEdgeAttributes() throws Exception {
    String fileName = "testFiles/small/missing_aliases_in_compartment.gpml";
    FileInputStream fis = new FileInputStream(fileName);
    Graph graph = new GpmlParser().createGraph(fis);
    assertNotNull(graph);
    assertEquals(3, getWarnings().size());
  }

  @Test
  public void testCyclicReactions() throws Exception {
    String fileName = "testFiles/small/cyclic_reactions.gpml";
    FileInputStream fis = new FileInputStream(fileName);
    Graph graph = new GpmlParser().createGraph(fis);
    assertNotNull(graph);
    assertEquals(3, getWarnings().size());
  }

  @Test
  public void testBiopaxVocabulary() throws Exception {
    String fileName = "testFiles/small/opencontrolledvocabulary.gpml";
    FileInputStream fis = new FileInputStream(fileName);
    Graph graph = new GpmlParser().createGraph(fis);

    assertEquals(1, graph.getBiopaxData().getOpenControlledVocabularies().size());
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testModelAttribute() throws Exception {
    String fileName = "testFiles/small/model_attribute.gpml";
    FileInputStream fis = new FileInputStream(fileName);
    Graph graph = new GpmlParser().createGraph(fis);

    assertEquals("428359", graph.getAttributes().get("reactome_id"));
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testModelAnnotations() throws Exception {
    String fileName = "testFiles/small/model_annotations.gpml";
    FileInputStream fis = new FileInputStream(fileName);
    Graph graph = new GpmlParser().createGraph(fis);

    assertEquals(6, graph.getBiopaxReferences().size());
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testModelLines() throws Exception {
    String fileName = "testFiles/small/model_with_line.gpml";
    FileInputStream fis = new FileInputStream(fileName);
    Graph graph = new GpmlParser().createGraph(fis);

    assertEquals(1, graph.getLines().size());
    PolylineData pd = graph.getLines().get(0);
    assertEquals(2, pd.getLines().size());
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testSimpleReaction() throws Exception {
    String fileName = "testFiles/small/simple_reaction.gpml";
    FileInputStream fis = new FileInputStream(fileName);
    Graph graph = new GpmlParser().createGraph(fis);

    Interaction interaction = graph.getInteractions().iterator().next();
    PolylineData pd = interaction.getLine();
    assertTrue(pd.getLines().get(0).getP1().distance(476.0, 351.0) < EPSILON);
    assertTrue(pd.getLines().get(0).getP2().distance(476.0, 286.0) < EPSILON);
    assertTrue(pd.getLines().get(1).getP2().distance(846.0, 287.0) < EPSILON);
    assertTrue(pd.getLines().get(2).getP2().distance(846.0, 354.0) < EPSILON);

    assertEquals(0, getWarnings().size());
  }

}
