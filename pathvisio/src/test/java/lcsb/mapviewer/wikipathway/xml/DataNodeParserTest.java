package lcsb.mapviewer.wikipathway.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Element;

import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;
import lcsb.mapviewer.wikipathway.model.DataNode;

public class DataNodeParserTest extends WikipathwaysTestFunctions {
  private DataNodeParser parser;

  @Before
  public void setUp() throws Exception {
    parser = new DataNodeParser("mapName");
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseGene() throws Exception {
    Element node = fileToNode("testFiles/elements/gene.xml");
    DataNode dataNode = parser.parse(node);

    assertNotNull(dataNode);
    assertEquals("GeneProduct", dataNode.getTextLabel());
    assertEquals("ab30b", dataNode.getGraphId());
    assertEquals(1, dataNode.getBiopaxReferences().size());
    assertEquals("aea", dataNode.getBiopaxReferences().get(0));
    assertEquals(1, dataNode.getReferences().size());
    assertEquals(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"), dataNode.getReferences().get(0));

    assertEquals((Integer) 32768, dataNode.getzOrder());
    assertEquals((Double) 10.0, dataNode.getFontSize());
    assertEquals("Middle", dataNode.getVerticalAlign());
    assertEquals("GeneProduct", dataNode.getType());
    assertTrue(dataNode.getComments().contains("comment 1"));
    assertTrue(dataNode.getComments().contains("Type your comment here"));

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testParseProteinWithOvalShape() throws Exception {
    Element node = fileToNode("testFiles/elements/protein_oval_shape.xml");
    DataNode dataNode = parser.parse(node);

    assertNotNull(dataNode);
    assertEquals("Protein", dataNode.getType());

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testParseWithNoGraphId() throws Exception {
    Element node = fileToNode("testFiles/elements/gene_no_id.xml");
    DataNode dataNode = parser.parse(node);

    assertNotNull(dataNode.getGraphId());
    assertFalse(dataNode.getGraphId().isEmpty());
  }

}
