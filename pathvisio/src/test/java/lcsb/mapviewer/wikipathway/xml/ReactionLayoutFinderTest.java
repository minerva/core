package lcsb.mapviewer.wikipathway.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.geometry.LineTransformation;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;
import lcsb.mapviewer.wikipathway.model.Edge;
import lcsb.mapviewer.wikipathway.model.Interaction;
import lcsb.mapviewer.wikipathway.xml.ReactionLayoutFinder.LineLocation;

public class ReactionLayoutFinderTest extends WikipathwaysTestFunctions {

  private LineTransformation lt = new LineTransformation();

  private ReactionLayoutFinder finder;

  @Before
  public void setUp() {
    finder = new ReactionLayoutFinder();
  }

  @Test
  public void testNodePointsInteractionWithoutAnything() {
    Interaction interaction = new Interaction(createEdge("id", new Point2D.Double(0, 0), new Point2D.Double(100, 0)));
    Map<Class<?>, Point2D> result = finder.getNodeStartPoints(interaction);
    validatePointsResult(interaction, result);
  }

  @Test
  public void testNodePointsInteractionWithAdditionalReactant() {
    Interaction interaction = new Interaction(createEdge("id", new Point2D.Double(0, 0), new Point2D.Double(100, 0)));

    interaction.addReactant(createEdge("react1", new Point2D.Double(10, 0), new Point2D.Double(200, 200)));

    Map<Class<?>, Point2D> result = finder.getNodeStartPoints(interaction);
    validatePointsResult(interaction, result);
    assertEquals(0, result.get(Reactant.class).distance(new Point2D.Double(10, 0)), Configuration.EPSILON);
  }

  @Test
  public void testNodePointsInteractionWithShiftedReactant() {
    Interaction interaction = new Interaction(createEdge("id", new Point2D.Double(0, 0), new Point2D.Double(100, 0)));

    interaction.addReactant(createEdge("react1", new Point2D.Double(60, 0), new Point2D.Double(200, 200)));

    Map<Class<?>, Point2D> result = finder.getNodeStartPoints(interaction);
    validatePointsResult(interaction, result);
    assertEquals(0, result.get(Reactant.class).distance(new Point2D.Double(60, 0)), Configuration.EPSILON);
  }

  @Test
  public void testNodePointsInteractionWithAdditionalProduct() {
    Interaction interaction = new Interaction(createEdge("id", new Point2D.Double(0, 0), new Point2D.Double(100, 0)));

    interaction.addProduct(createEdge("prod", new Point2D.Double(80, 0), new Point2D.Double(200, 200)));

    Map<Class<?>, Point2D> result = finder.getNodeStartPoints(interaction);
    validatePointsResult(interaction, result);
    assertEquals(0, result.get(Product.class).distance(new Point2D.Double(80, 0)), Configuration.EPSILON);
  }

  @Test
  public void testNodePointsInteractionWithAdditionalModifier() {
    Interaction interaction = new Interaction(createEdge("id", new Point2D.Double(0, 0), new Point2D.Double(100, 0)));

    interaction.addModifier(createEdge("modifier", new Point2D.Double(80, 0), new Point2D.Double(200, 200)));

    Map<Class<?>, Point2D> result = finder.getNodeStartPoints(interaction);
    validatePointsResult(interaction, result);
    assertEquals(0, result.get(Modifier.class).distance(new Point2D.Double(80, 0)), Configuration.EPSILON);
  }

  @Test
  public void testNodePointsInteractionWithMultiNodes() {
    Interaction interaction = new Interaction(createEdge("id", new Point2D.Double(0, 0), new Point2D.Double(100, 0)));

    interaction.addReactant(createEdge("react1", new Point2D.Double(10, 0), new Point2D.Double(200, 200)));
    interaction.addReactant(createEdge("react2", new Point2D.Double(20, 0), new Point2D.Double(200, 200)));
    interaction.addReactant(createEdge("react3", new Point2D.Double(30, 0), new Point2D.Double(200, 200)));
    interaction.addModifier(createEdge("modifier", new Point2D.Double(40, 0), new Point2D.Double(200, 200)));
    interaction.addProduct(createEdge("prod1", new Point2D.Double(50, 0), new Point2D.Double(200, 200)));
    interaction.addProduct(createEdge("prod2", new Point2D.Double(60, 0), new Point2D.Double(200, 200)));
    interaction.addProduct(createEdge("prod3", new Point2D.Double(70, 0), new Point2D.Double(200, 200)));

    Map<Class<?>, Point2D> result = finder.getNodeStartPoints(interaction);
    validatePointsResult(interaction, result);
    assertEquals(0, result.get(Modifier.class).distance(new Point2D.Double(40, 0)), Configuration.EPSILON);
  }

  @Test
  public void testNodePointsInteractionWithMultiModifiers() {
    Interaction interaction = new Interaction(createEdge("id", new Point2D.Double(0, 0), new Point2D.Double(100, 0)));

    interaction.addReactant(createEdge("react1", new Point2D.Double(10, 0), new Point2D.Double(200, 200)));
    interaction.addReactant(createEdge("react2", new Point2D.Double(20, 0), new Point2D.Double(200, 200)));
    interaction.addReactant(createEdge("react3", new Point2D.Double(30, 0), new Point2D.Double(200, 200)));
    interaction.addModifier(createEdge("modifier", new Point2D.Double(40, 0), new Point2D.Double(200, 200)));
    interaction.addProduct(createEdge("prod1", new Point2D.Double(50, 0), new Point2D.Double(200, 200)));
    interaction.addModifier(createEdge("modifier2", new Point2D.Double(60, 0), new Point2D.Double(200, 200)));
    interaction.addProduct(createEdge("prod3", new Point2D.Double(70, 0), new Point2D.Double(200, 200)));

    Map<Class<?>, Point2D> result = finder.getNodeStartPoints(interaction);
    validatePointsResult(interaction, result);
    assertEquals(0, result.get(Modifier.class).distance(new Point2D.Double(40, 0)), Configuration.EPSILON);
  }

  @Test
  public void testNodePointsInteractionWithMultiModifiers2() {
    Interaction interaction = new Interaction(createEdge("id", new Point2D.Double(0, 0), new Point2D.Double(100, 0)));

    interaction.addReactant(createEdge("react1", new Point2D.Double(10, 0), new Point2D.Double(200, 200)));
    interaction.addModifier(createEdge("modifier2", new Point2D.Double(20, 0), new Point2D.Double(200, 200)));
    interaction.addReactant(createEdge("react3", new Point2D.Double(30, 0), new Point2D.Double(200, 200)));
    interaction.addModifier(createEdge("modifier", new Point2D.Double(40, 0), new Point2D.Double(200, 200)));
    interaction.addProduct(createEdge("prod1", new Point2D.Double(50, 0), new Point2D.Double(200, 200)));
    interaction.addProduct(createEdge("prod3", new Point2D.Double(70, 0), new Point2D.Double(200, 200)));

    Map<Class<?>, Point2D> result = finder.getNodeStartPoints(interaction);
    validatePointsResult(interaction, result);
    assertEquals(0, result.get(Modifier.class).distance(new Point2D.Double(40, 0)), Configuration.EPSILON);
  }

  private Edge createEdge(final String id, final Point2D p1, final Point2D p2) {
    Edge modifierEdge = new Edge(id, null);
    modifierEdge.setLine(new PolylineData(p1, p2));
    return modifierEdge;
  }

  private void validatePointsResult(final Interaction interaction, final Map<Class<?>, Point2D> result) {
    PolylineData pd = interaction.getLine();
    Point2D reactantPoint = result.get(Reactant.class);
    Point2D productPoint = result.get(Product.class);
    Point2D modifierPoint = result.get(Modifier.class);
    assertNotNull("Reactant point is not defined", reactantPoint);
    assertNotNull("Product point is not defined", productPoint);
    assertNotNull("Modifier point is not defined", modifierPoint);

    // logger.debug(reactantPoint);
    // logger.debug(modifierPoint);
    // logger.debug(productPoint);
    //
    assertEquals(0, distanceFromLine(pd, reactantPoint), Configuration.EPSILON);
    assertEquals(0, distanceFromLine(pd, productPoint), Configuration.EPSILON);
    assertEquals(0, distanceFromLine(pd, modifierPoint), Configuration.EPSILON);

    assertTrue(
        finder.distanceFromPolylineStart(pd, reactantPoint) < finder.distanceFromPolylineStart(pd, modifierPoint));
    assertTrue(
        finder.distanceFromPolylineStart(pd, modifierPoint) < finder.distanceFromPolylineStart(pd, productPoint));
  }

  private double distanceFromLine(final PolylineData pd, final Point2D point) {
    double distance = Double.MAX_VALUE;
    for (final Line2D line : pd.getLines()) {
      distance = Math.min(distance, lt.distBetweenPointAndLineSegment(line, point));
    }
    return distance;
  }

  @Test
  public void testDistanceFromPolylineStartSimple() {
    PolylineData line = new PolylineData(new Point2D.Double(10, 20), new Point2D.Double(40, 60));
    assertEquals(0, finder.distanceFromPolylineStart(line, new Point2D.Double(10, 20)), Configuration.EPSILON);
    assertEquals(50, finder.distanceFromPolylineStart(line, new Point2D.Double(40, 60)), Configuration.EPSILON);
    assertEquals(25, finder.distanceFromPolylineStart(line, new Point2D.Double(25, 40)), Configuration.EPSILON);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testDistanceFromPolylineStartInvalid() {
    PolylineData line = new PolylineData(new Point2D.Double(10, 20), new Point2D.Double(40, 60));
    finder.distanceFromPolylineStart(line, new Point2D.Double(1, 2));
  }

  @Test
  public void testDistanceFromPolylineStartComplex() {
    PolylineData line = new PolylineData(new Point2D.Double(10, 20), new Point2D.Double(40, 60));
    line.addLine(new Point2D.Double(40, 60), new Point2D.Double(60, 60));
    assertEquals(50, finder.distanceFromPolylineStart(line, new Point2D.Double(40, 60)), Configuration.EPSILON);
    assertEquals(60, finder.distanceFromPolylineStart(line, new Point2D.Double(50, 60)), Configuration.EPSILON);
    assertEquals(70, finder.distanceFromPolylineStart(line, new Point2D.Double(60, 60)), Configuration.EPSILON);
  }

  @Test
  public void testNodeLinesInteractionWithoutAnything() {
    Interaction interaction = new Interaction(createEdge("id", new Point2D.Double(0, 0), new Point2D.Double(100, 0)));
    Map<LineLocation, PolylineData> result = finder.getNodeLines(interaction);
    validateLinesResult(interaction, result);
  }

  @Test
  public void testNodeLinesInteractionWithTwoReactants() {
    Edge edge = new Edge("id", null);
    PolylineData line = new PolylineData(Arrays.asList(
        new Point2D.Double(0, 0),
        new Point2D.Double(100, 0),
        new Point2D.Double(100, 100)));
    edge.setLine(line);
    Interaction interaction = new Interaction(edge);
    interaction.addReactant(createEdge("reactant", new Point2D.Double(200, 0), new Point2D.Double(100, 0)));

    Map<LineLocation, PolylineData> result = finder.getNodeLines(interaction);
    validateLinesResult(interaction, result);
  }

  @Test
  public void testNodeLinesInteractionWithTwoProducts() {
    Edge edge = new Edge("id", null);
    PolylineData line = new PolylineData(Arrays.asList(
        new Point2D.Double(0, 0),
        new Point2D.Double(100, 0),
        new Point2D.Double(100, 100)));
    edge.setLine(line);
    Interaction interaction = new Interaction(edge);
    interaction.addProduct(createEdge("product", new Point2D.Double(200, 0), new Point2D.Double(100, 0)));

    Map<LineLocation, PolylineData> result = finder.getNodeLines(interaction);
    validateLinesResult(interaction, result);
  }

  private void validateLinesResult(final Interaction interaction, final Map<LineLocation, PolylineData> result) {
    PolylineData pd = interaction.getLine();
    PolylineData reactantLine = result.get(LineLocation.REACTANT);
    PolylineData inputOperatorLine = result.get(LineLocation.INPUT_OPERATOR);
    PolylineData outputOperatorLine = result.get(LineLocation.OUTPUT_OPERATOR);
    PolylineData productLine = result.get(LineLocation.PRODUCT);
    PolylineData modifierLine = result.get(LineLocation.MODIFIER);

    // logger.debug(reactantLine);
    // logger.debug(inputOperatorLine);
    // logger.debug(modifierLine);
    // logger.debug(outputOperatorLine);
    // logger.debug(productLine);
    //
    assertNotNull(reactantLine);
    assertTrue(reactantLine.length() > Configuration.EPSILON);
    assertNotNull(productLine);
    assertTrue(productLine.length() > Configuration.EPSILON);
    assertNotNull(modifierLine);
    assertTrue("Modifier line length must be > 0", modifierLine.length() > Configuration.EPSILON);
    assertNotNull(inputOperatorLine);
    assertTrue(inputOperatorLine.length() > Configuration.EPSILON);
    assertNotNull(outputOperatorLine);
    assertTrue(outputOperatorLine.length() > Configuration.EPSILON);

    assertEquals(0, pd.getStartPoint().distance(reactantLine.getStartPoint()), Configuration.EPSILON);
    assertEquals(0, reactantLine.getEndPoint().distance(inputOperatorLine.getStartPoint()), Configuration.EPSILON);
    assertEquals(0, inputOperatorLine.getEndPoint().distance(modifierLine.getStartPoint()), Configuration.EPSILON);
    assertEquals(0, modifierLine.getEndPoint().distance(outputOperatorLine.getStartPoint()), Configuration.EPSILON);
    assertEquals(0, outputOperatorLine.getEndPoint().distance(productLine.getStartPoint()), Configuration.EPSILON);
    assertEquals(0, productLine.getEndPoint().distance(pd.getEndPoint()), Configuration.EPSILON);
  }

  @Test
  public void testNodePointsInteractionWithWeirdProduct() {
    Interaction interaction = new Interaction(createEdge("id", new Point2D.Double(0, 0), new Point2D.Double(100, 0)));

    interaction.addProduct(createEdge("prod1", new Point2D.Double(00, 0), new Point2D.Double(0, 200)));

    Map<Class<?>, Point2D> result = finder.getNodeStartPoints(interaction);
    validatePointsResult(interaction, result);
  }

  @Test
  public void testNodePointsInteractionWithColinearProduct() {
    Interaction interaction = new Interaction(createEdge("id", new Point2D.Double(0, 0), new Point2D.Double(100, 0)));

    interaction.addProduct(createEdge("prod1", new Point2D.Double(10, 0), new Point2D.Double(20, 0)));

    Map<Class<?>, Point2D> result = finder.getNodeStartPoints(interaction);
    assertEquals(0.0, result.get(Product.class).distance(new Point2D.Double(10, 0)), Configuration.EPSILON);
    validatePointsResult(interaction, result);
  }

  @Test
  public void testNodePointsInteractionWithReversedProductLine() {
    Interaction interaction = new Interaction(createEdge("id", new Point2D.Double(0, 0), new Point2D.Double(100, 0)));

    Edge product = createEdge("prod1", new Point2D.Double(10, 10), new Point2D.Double(10, 0));
    interaction.addProduct(product);

    Map<Class<?>, Point2D> result = finder.getNodeStartPoints(interaction);

    assertEquals(result.get(Product.class), product.getLine().getStartPoint());
    validatePointsResult(interaction, result);
  }

}
