package lcsb.mapviewer.wikipathway.model;

import static org.junit.Assert.assertNotNull;

import java.util.Set;

import org.junit.Test;
import org.reflections.Reflections;

import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.Reaction;

public class InteractionMappingTest {


  @Test
  public void testGetGpmlInteractionTypeForMinervaModifierClass() {
    Reflections reflections = new Reflections("lcsb.mapviewer.model.map");
    Set<Class<? extends Modifier>> classes = reflections.getSubTypesOf(Modifier.class);
    for (final Class<? extends Modifier> clazz : classes) {
      assertNotNull("Gpml interaction type not defined for: " + clazz.getSimpleName(),
          InteractionMapping.getGpmlInteractionTypeForMinervaModifierClass(clazz));
    }
  }

  @Test
  public void testGetGpmlInteractionTypeForMinervaReactionClass() {
    Reflections reflections = new Reflections("lcsb.mapviewer.model.map");
    Set<Class<? extends Reaction>> classes = reflections.getSubTypesOf(Reaction.class);
    for (final Class<? extends Reaction> clazz : classes) {
      assertNotNull("Gpml interaction type not defined for: " + clazz.getSimpleName(),
          InteractionMapping.getGpmlInteractionTypeForMinervaReactionClass(clazz));
    }
  }

}
