package lcsb.mapviewer.wikipathway.model;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;

public class ShapeMappingTest extends WikipathwaysTestFunctions {

  @Test
  public void testMappingExistsForEveryElement() {
    for (final Class<?> clazz : new ElementUtils().getAvailableElementSubclasses()) {
      assertNotNull(ShapeMapping.getShape(clazz));
    }
  }

  @Test
  public void testMappingExistsModification() {
    assertNotNull(ShapeMapping.getShape(ModificationResidue.class));
  }

}
