package lcsb.mapviewer.wikipathway.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;

public class ShapeTest extends WikipathwaysTestFunctions {


  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSeialization() {
    SerializationUtils.serialize(new Shape());
  }

  @Test
  public void getNameForSpecialShapes() {
    List<GpmlShape> specialShapeNames = Arrays.asList(
        GpmlShape.SACROPLASMIC_RETICULUM,
        GpmlShape.GOLGI_APPARATUS,
        GpmlShape.ENDOPLASMIC_RETICULUM,
        GpmlShape.MITOCHONDRIA);

    for (final GpmlShape gpmlShape : specialShapeNames) {
      Shape shape = new Shape();
      shape.setShape(gpmlShape);
      assertEquals(gpmlShape.getStringRepresentation(), shape.getName());
      shape.setTextLabel("bla");
      assertTrue(shape.getName().contains(gpmlShape.getStringRepresentation()));
    }
  }

  @Test
  public void getNameForGenericShapes() {
    List<GpmlShape> otherShapes = Arrays.asList(
        GpmlShape.RECTANGLE,
        GpmlShape.TRIANGLE,
        GpmlShape.OVAL);
    for (final GpmlShape string : otherShapes) {
      Shape shape = new Shape();
      shape.setShape(string);
      assertTrue(shape.getName().isEmpty());
      shape.setTextLabel("bla");
      assertEquals("bla", shape.getName());
    }
  }

}
