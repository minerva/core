package lcsb.mapviewer.wikipathway.model;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Point2D;
import java.util.Arrays;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.wikipathway.WikipathwaysTestFunctions;

public class EdgeTest extends WikipathwaysTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSeialization() {
    SerializationUtils.serialize(new Edge());
  }

  @Test
  public void testExtend() {

    double length = 0;
    Edge e1 = new Edge();
    e1.setLine(new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 100)));
    length += e1.getLine().length();

    Edge e2 = new Edge();
    e2.setLine(new PolylineData(new Point2D.Double(10, 100), new Point2D.Double(20, 100)));
    length += e2.getLine().length();

    e1.extend(e2);

    assertEquals(length, e1.getLine().length(), Configuration.EPSILON);
  }

  @Test
  public void testExtendInvalid() {
    //original line has too many points (real case example)
    Edge e1 = new Edge();
    e1.setLine(new PolylineData(
        Arrays.asList(new Point2D.Double(0, 0), new Point2D.Double(10, 100), new Point2D.Double(50, 100))));

    Edge e2 = new Edge();
    e2.setLine(new PolylineData(new Point2D.Double(10, 100), new Point2D.Double(20, 100)));

    e1.extend(e2);

    assertEquals(2, e1.getLine().getLines().size());
  }

}
