package lcsb.mapviewer.wikipathway.model;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ReferenceMappingTest {

  // CHECKSTYLE.OFF
  @Parameter(0)
  public String gpmlType;
  // CHECKSTYLE.ON

  @Parameters(name = "{0}")
  public static Collection<Object[]> data() throws IOException {

    List<Object[]> result = new ArrayList<>();

    result.add(new Object[] { "BRENDA" });
    result.add(new Object[] { "CAS" });
    result.add(new Object[] { "ChEBI" });
    result.add(new Object[] { "ChEMBL compound" });
    result.add(new Object[] { "ChemIDplus" });
    result.add(new Object[] { "Chemspider" });
    result.add(new Object[] { "DrugBank" });
    result.add(new Object[] { "EcoGene" });
    result.add(new Object[] { "Ensembl" });
    result.add(new Object[] { "Ensembl Plants" });

    result.add(new Object[] { "Entrez" });
    result.add(new Object[] { "Entrez Gene" });
    result.add(new Object[] { "Enzyme Nomenclature" });
    result.add(new Object[] { "FlyBase" });
    result.add(new Object[] { "GenBank" });
    result.add(new Object[] { "GeneDB" });
    result.add(new Object[] { "GeneOntology" });
    result.add(new Object[] { "HGNC" });
    result.add(new Object[] { "HGNC Accession number" });
    result.add(new Object[] { "HMDB" });
    result.add(new Object[] { "IntAct" });
    result.add(new Object[] { "InterPro" });
    result.add(new Object[] { "Kegg Compound" });
    result.add(new Object[] { "KEGG Compound" });
    result.add(new Object[] { "KEGG Drug" });
    result.add(new Object[] { "KEGG Genes" });
    result.add(new Object[] { "Kegg ortholog" });
    result.add(new Object[] { "KEGG Pathway" });
    result.add(new Object[] { "KEGG Reaction" });
    result.add(new Object[] { "KNApSAcK" });
    result.add(new Object[] { "MGI" });
    result.add(new Object[] { "miRBase mature sequence" });
    result.add(new Object[] { "miRBase Sequence" });
    result.add(new Object[] { "NCBI Protein" });
    result.add(new Object[] { "OMIM" });
    result.add(new Object[] { "pato" });
    result.add(new Object[] { "Pfam" });
    result.add(new Object[] { "PubChem" });
    result.add(new Object[] { "PubChem-compound" });
    result.add(new Object[] { "PubChem Compound" });
    result.add(new Object[] { "PubChem-substance" });
    result.add(new Object[] { "Reactome" });
    result.add(new Object[] { "RefSeq" });
    result.add(new Object[] { "RGD" });
    result.add(new Object[] { "Rhea" });
    result.add(new Object[] { "SGD" });
    result.add(new Object[] { "SPIKE" });
    result.add(new Object[] { "STRING" });
    result.add(new Object[] { "TAIR" });
    result.add(new Object[] { "TTD Drug" });
    result.add(new Object[] { "UniGene" });
    result.add(new Object[] { "Uniprot-TrEMBL" });
    result.add(new Object[] { "Wikidata" });
    result.add(new Object[] { "WikiPathways" });
    result.add(new Object[] { "WormBase" });
    result.add(new Object[] { "ZFIN" });

    return result;
  }

  @Test
  public void testMiriamTypeMappipping() {

    assertNotNull(ReferenceMapping.getMappingByGpmlString(gpmlType));
  }

}
