package lcsb.mapviewer.wikipathway;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.geom.Line2D;

import org.junit.Test;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.modifier.Inhibition;
import lcsb.mapviewer.model.map.modifier.Modulation;
import lcsb.mapviewer.model.map.modifier.PhysicalStimulation;
import lcsb.mapviewer.model.map.modifier.UnknownCatalysis;
import lcsb.mapviewer.model.map.modifier.UnknownInhibition;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.wikipathway.xml.GPMLToModel;

public class ReactionGpmlInputToModelTest extends WikipathwaysTestFunctions {


  private ModelComparator mc = new ModelComparator(1.0);

  @Test
  public void LineInteractionInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_line_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modifier);
    }

    assertNotNull(reactant);
    assertNull(product);
    assertNull(modifier);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void ArrowInteractionInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_arrow_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNotNull(reactant);
    assertNull(product);
    assertNull(modifier);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void DashedLineInteractionInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_line_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modifier);
    }

    assertNotNull(reactant);
    assertNull(product);
    assertNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void DashedArrowInteractionInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_arrow_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void TBarInteractionInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_tbar_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Inhibition);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void NecessaryStimulationInteractionInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_necessary_stimulation_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof PhysicalStimulation);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void BindingInteractionInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_binding_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void ConversionInteractionInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_conversion_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNotNull(reactant);
    assertNull(product);
    assertNull(modifier);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void StimulationInteractionInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_stimulation_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof PhysicalStimulation);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void ModificationInteractionInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_modification_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void CatalystInteractionInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_catalyst_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Catalysis);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void InhibitionInteractionInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_inhibition_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Inhibition);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void CleavageInteractionInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_cleavage_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void CovalentBondInteractionInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_covalent_bond_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modifier);
    }

    assertNotNull(reactant);
    assertNull(product);
    assertNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void BranchingLeftInteractionInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_branching_left_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modifier);
    }

    assertNotNull(reactant);
    assertNull(product);
    assertNull(modifier);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void BranchingRightInteractionInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_branching_right_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modifier);
    }

    assertNotNull(reactant);
    assertNull(product);
    assertNull(modifier);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void TranscriptionTranslationInteractionInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_transcription_translation_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void GapInteractionInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_gap_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void TBarInteractionDashedInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_tbar_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof UnknownInhibition);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void NecessaryStimulationInteractionDashedInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_necessary_stimulation_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void BindingInteractionDashedInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_binding_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void ConversionInteractionDashedInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_conversion_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void StimulationInteractionDashedInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_stimulation_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void ModificationInteractionDashedInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_modification_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void CatalystInteractionDashedInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_catalyst_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof UnknownCatalysis);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void InhibitionInteractionDashedInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_inhibition_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof UnknownInhibition);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void CleavageInteractionDashedInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_cleavage_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void CovalentBondInteractionDashedInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_covalent_bond_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modifier);
    }

    assertNotNull(reactant);
    assertNull(product);
    assertNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void BranchingLeftInteractionDashedInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_branching_left_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modifier);
    }

    assertNotNull(reactant);
    assertNull(product);
    assertNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void BranchingRightInteractionDashedInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_branching_right_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modifier);
    }

    assertNotNull(reactant);
    assertNull(product);
    assertNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void TranscriptionTranslationInteractionDashedInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_transcription_translation_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void GapInteractionDashedInputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_gap_input.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNull(product);
    assertNotNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void testProblematicInhibition() throws Exception {
    String fileName = "testFiles/reactions/inhibition_with_problematic_line.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);
    Reaction r = model1.getReactionByReactionId("eff08");
    r.getLine().setColor(Color.BLUE);

    Line2D line = r.getProducts().get(0).getLine().getLines().get(r.getProducts().get(0).getLine().getLines().size() - 1);
    assertTrue("End points shouldn't be too close",
        line.getP1().distance(line.getP2()) > 0);
  }

}
