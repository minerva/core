package lcsb.mapviewer.wikipathway;

import lcsb.mapviewer.common.tests.TestUtils;
import lcsb.mapviewer.common.tests.UnitTestFailedWatcher;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.ZIndexPopulator;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public abstract class WikipathwaysTestFunctions extends TestUtils {

  protected static Logger logger = LogManager.getLogger();

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();

  protected Model serializeModelOverCellDesignerParser(final Model model1)
      throws InconsistentModelException, InvalidInputDataExecption {
    model1.setName("Unknown");
    for (final Drawable bioEntity : model1.getDrawables()) {
      if (!(bioEntity instanceof PolylineData)) {
        bioEntity.setZ(null);
      }
    }
    for (final lcsb.mapviewer.model.map.species.Element bioEntity : model1.getElements()) {
      bioEntity.setFontColor(Color.BLACK);
    }

    // CellDesigner doesn't allow to store fill color for rectangles
    for (final Layer layer : model1.getLayers()) {
      for (final LayerRect rect : layer.getRectangles()) {
        rect.setFillColor(Color.LIGHT_GRAY);
      }
    }
    // CellDesigner doesn't allow to store compartment font size
    for (final Compartment bioEntity : model1.getCompartments()) {
      bioEntity.setFontSize(Compartment.DEFAULT_COMPARTMENT_THICKNESS);
    }
    new ZIndexPopulator().populateZIndex(model1);
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final String xml = parser.model2String(model1);
    final InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

    for (final lcsb.mapviewer.model.map.species.Element bioEntity : model1.getElements()) {
      // CellDesigner doesn't allow to store border color
      if (bioEntity instanceof Species) {
        bioEntity.setBorderColor(Color.BLACK);
      }
      if (bioEntity instanceof Unknown) {
        bioEntity.setBorderColor(Unknown.DEFAULT_BORDER_COLOR);
      }
      // CellDesigner doesn't allow to store name point position
      bioEntity.setNameX(0);
      bioEntity.setNameY(0);
      bioEntity.setNameWidth(0);
      bioEntity.setNameHeight(0);
      bioEntity.setNameHorizontalAlign(null);
      bioEntity.setNameVerticalAlign(null);
    }

    final Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));
    for (final lcsb.mapviewer.model.map.species.Element bioEntity : model2.getElements()) {
      // CellDesigner doesn't allow to store name point position
      bioEntity.setNameX(0);
      bioEntity.setNameY(0);
      bioEntity.setNameWidth(0);
      bioEntity.setNameHeight(0);
      bioEntity.setNameHorizontalAlign(null);
      bioEntity.setNameVerticalAlign(null);
    }
    return model2;
  }

  protected Model serializeModelOverGPML(final Model model1)
      throws InconsistentModelException, InvalidInputDataExecption, ConverterException {
    final GpmlParser parser = new GpmlParser();
    final String xml = parser.model2String(model1);
    // logger.debug(xml);
    final InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
    final Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));
    return model2;
  }

  private static int z = 10;
  private static int counter = 0;

  protected static void assignCoordinates(final double x, final double y, final double width, final double height,
                                          final lcsb.mapviewer.model.map.species.Element element) {
    element.setX(x);
    element.setY(y);
    element.setZ(z++);
    element.setWidth(width);
    element.setHeight(height);
    element.setNameX(x);
    element.setNameY(y);
    element.setNameWidth(width);
    element.setNameHeight(height);
    element.setNameVerticalAlign(VerticalAlign.MIDDLE);
    element.setNameHorizontalAlign(HorizontalAlign.CENTER);
  }

  protected static Complex createComplex() {
    final Complex complex = new Complex("id" + (counter++));
    assignCoordinates(1, 1, 10, 10, complex);
    complex.setNameHeight(complex.getHeight() - 2);
    complex.setNameVerticalAlign(VerticalAlign.MIDDLE);
    complex.setHypothetical(true);
    return complex;
  }

  protected Model serializeOverGpml(final Model model)
      throws InconsistentModelException, ConverterException, InvalidInputDataExecption {
    final lcsb.mapviewer.wikipathway.GpmlParser parser = new lcsb.mapviewer.wikipathway.GpmlParser();

    final String xml = parser.model2String(model);
    final Model model2 = parser.createModel(new ConverterParams().inputStream(new ByteArrayInputStream(xml.getBytes())));
    return model2;
  }

  protected static GenericProtein createProtein() {
    final GenericProtein protein = new GenericProtein("id" + (counter++));
    assignCoordinates(100 * counter, 10 + 100 * counter * Math.random(), 10, 10, protein);
    return protein;
  }

  protected static StructuralState createStructuralState(final Element element) {
    final StructuralState structuralState = new StructuralState("state" + (counter++));
    structuralState.setName("xxx" + (counter++));
    structuralState.setPosition(new Point2D.Double(element.getX(), element.getY() - 10));
    structuralState.setWidth(element.getWidth());
    structuralState.setHeight(20.0);
    structuralState.setFontSize(10.0);
    structuralState.setZ(element.getZ() + 1);
    structuralState.setBorderColor(Color.GREEN);
    return structuralState;
  }

  protected static Model createEmptyModel() {
    final Model result = new ModelFullIndexed(null);
    result.setWidth(640);
    result.setHeight(480);
    return result;
  }

}
