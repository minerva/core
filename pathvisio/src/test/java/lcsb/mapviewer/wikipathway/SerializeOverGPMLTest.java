package lcsb.mapviewer.wikipathway;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Protein;

@RunWith(Parameterized.class)
public class SerializeOverGPMLTest extends WikipathwaysTestFunctions {

  private Model model;

  public SerializeOverGPMLTest(final String name, final Model model) {
    this.model = model;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<Object[]>();
    Model model = proteinWithTwoState();
    data.add(new Object[] { model.getName(), model });
    model = emptyMapWithName();
    data.add(new Object[] { model.getName(), model });
    return data;
  }

  private static Model emptyMapWithName() {
    Model model = createEmptyModel();
    model.setName("Empty map");
    return model;
  }

  private static Model proteinWithTwoState() {
    Model model = new ModelFullIndexed(null);
    model.setName("Protein with 2 structural states");
    model.setWidth(640);
    model.setHeight(480);
    Protein protein = createProtein();
    protein.addStructuralState(createStructuralState(protein));
    protein.addStructuralState(createStructuralState(protein));
    model.addElement(protein);

    Complex complex = createComplex();
    complex.addStructuralState(createStructuralState(complex));
    complex.addStructuralState(createStructuralState(complex));
    model.addElement(complex);

    return model;
  }

  @Test
  public void test() throws Exception {
    ModelComparator comparator = new ModelComparator();
    Model model2 = serializeModelOverGPML(model);

    assertEquals(0, comparator.compare(model, model2));
  }

}
