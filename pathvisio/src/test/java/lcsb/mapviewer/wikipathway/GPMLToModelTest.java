package lcsb.mapviewer.wikipathway;

import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.converter.model.celldesigner.geometry.ReactionCellDesignerConverter;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.ElementComparator;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import lcsb.mapviewer.wikipathway.xml.GPMLToModel;
import org.junit.Before;
import org.junit.Test;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class GPMLToModelTest extends WikipathwaysTestFunctions {

  private ModelComparator mc;

  private PointTransformation pt = new PointTransformation();

  @Before
  public void setUp() {
    mc = new ModelComparator(1.0);
  }

  @Test
  public void DopamineTest() throws Exception {
    Model model1 = new GPMLToModel().getModel("testFiles/wikipathways/Dopamine.gpml");

    assertEquals(20, getWarnings().size());
    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void testAuthors() throws Exception {
    Model model = new GPMLToModel().getModel("testFiles/small/model_with_author.gpml");

    assertEquals(1, model.getAuthors().size());
    Author author = model.getAuthors().get(0);
    assertEquals("Piotr", author.getFirstName());
    assertEquals("Marcin Gawron", author.getLastName());

    Model model2 = super.serializeOverGpml(model);
    assertEquals(0, mc.compare(model, model2));
  }

  @Test
  public void testMaintainer() throws Exception {
    Model model = new GPMLToModel().getModel("testFiles/small/model_with_author_and_maintainer.gpml");
    assertEquals(2, model.getAuthors().size());

    Model model2 = super.serializeOverGpml(model);
    assertEquals(0, mc.compare(model, model2));
  }

  @Test
  public void testModifyDate() throws Exception {
    Model model = new GPMLToModel().getModel("testFiles/small/model_with_modified_date.gpml");

    assertEquals(1, model.getModificationDates().size());
    Calendar calendar = model.getModificationDates().get(0);
    assertEquals(2013, calendar.get(Calendar.YEAR));
    assertEquals(1, calendar.get(Calendar.MONTH));
    assertEquals(22, calendar.get(Calendar.DATE));

    Model model2 = super.serializeOverGpml(model);
    assertEquals(0, mc.compare(model, model2));
  }

  @Test
  public void testComplexWithName() throws Exception {
    Model model1 = new GPMLToModel().getModel("testFiles/small/complex_with_name.gpml");
    Complex complex = model1.getElementByElementId("ac7f7");
    assertEquals("complex name", complex.getName());
    assertEquals(2, complex.getElements().size());
    assertEquals(VerticalAlign.BOTTOM, complex.getNameVerticalAlign());
  }

  @Test
  public void testZIndex() throws Exception {
    Model model1 = new GPMLToModel().getModel("testFiles/wikipathways/Dopamine.gpml");

    for (final Drawable bioEntity : model1.getDrawables()) {
      assertNotNull(bioEntity.getElementId() + " has null z-index", bioEntity.getZ());
    }
  }

  @Test
  public void WP1403_75220Test() throws Exception {
    Model model1 = new GPMLToModel().getModel("testFiles/wikipathways/WP1403_75220.gpml");

    assertEquals(20, getWarnings().size());
    Model model2 = serializeModelOverCellDesignerParser(model1);

    // export to CellDesigner changes line width
    mc.setElementComparator(new ElementComparator(3));
    model2.setNotes(model1.getNotes());
    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void WP528_76269Test() throws Exception {
    Model model1 = new GPMLToModel().getModel("testFiles/wikipathways/WP528_76269.gpml");

    assertEquals(12, getWarnings().size());
    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void WP550_73391Test() throws Exception {
    Model model1 = new GPMLToModel().getModel("testFiles/wikipathways/WP550_73391.gpml");

    assertEquals(12, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void WP197_69902Test() throws Exception {
    Model model1 = new GPMLToModel().getModel("testFiles/wikipathways/WP197_69902.gpml");

    assertEquals(9, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void WP306_71714Test() throws Exception {
    Model model1 = new GPMLToModel().getModel("testFiles/wikipathways/WP306_71714.gpml");

    assertEquals(40, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void WP481_72080Test() throws Exception {
    Model model1 = new GPMLToModel().getModel("testFiles/wikipathways/WP481_72080.gpml");

    assertEquals(21, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void testTwoReactants() throws Exception {
    Model model1 = new GPMLToModel().getModel("testFiles/small/two_reactant.gpml");

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void testTwoPubmeds() throws Exception {
    Model model1 = new GPMLToModel().getModel("testFiles/small/reaction_with_two_pubmeds.gpml");
    assertEquals(2, model1.getReactions().iterator().next().getMiriamData().size());

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void testComment() throws Exception {
    Model model1 = new GPMLToModel().getModel("testFiles/small/comment.gpml");
    assertTrue(model1.getNotes().contains("Metabolic Process"));

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void testMissingAliasesInCompartment() throws Exception {
    Model model1 = new GPMLToModel().getModel("testFiles/small/missing_aliases_in_compartment.gpml");

    assertEquals(3, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    // export to CellDesigner changes line width
    mc.setElementComparator(new ElementComparator(3));
    model2.setNotes(model1.getNotes());
    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void testModelLines() throws Exception {
    Model model1 = new GPMLToModel().getModel("testFiles/small/model_with_line.gpml");

    assertEquals(0, getWarnings().size());

    assertEquals(1, model1.getLayers().iterator().next().getLines().size());
  }

  @Test
  public void testModelShapes() throws Exception {
    new GPMLToModel().getModel("testFiles/small/shapes.gpml");

    assertEquals(11, getWarnings().size());
  }

  @Test
  public void testHypotheticalComplex() throws Exception {
    Model model = new GPMLToModel().getModel("testFiles/complex/hypothetical_complex.gpml");
    for (final Complex species : model.getComplexList()) {
      assertTrue("Complex parsed from gpml should be hypothetical", species.isHypothetical());
    }

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testNonHypotheticlaComplex() throws Exception {
    String fileName = "testFiles/complex/nonhypothetical_complex.gpml";
    Model model = new GPMLToModel().getModel(fileName);
    for (final Complex complex : model.getComplexList()) {
      assertFalse("Complex parsed from gpml should be hypothetical", complex.isHypothetical());
      for (final Species species : complex.getElements()) {
        assertTrue("Complex z index should be lower than children x index", species.getZ() > complex.getZ());
      }
    }
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testUnknownElementType() throws Exception {
    String fileName = "testFiles/complex/unknown_element.gpml";
    new GPMLToModel().getModel(fileName);
    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testComplexName() throws Exception {
    String fileName = "testFiles/complex/complex_with_name.gpml";
    Model model = new GPMLToModel().getModel(fileName);
    boolean nameFound = false;
    for (final Complex species : model.getComplexList()) {
      if ("p70 S6 Kinases".equals(species.getName())) {
        nameFound = true;
      }
    }
    assertTrue("Complex parsed from gpml should have a valid name", nameFound);
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testComplexNameAndReaction() throws Exception {
    String fileName = "testFiles/small/complex_with_name_and_reaction.gpml";
    Model model = new GPMLToModel().getModel(fileName);
    Set<String> names = new HashSet<>();
    for (Complex species : model.getComplexList()) {
      names.add(species.getName());
    }
    assertTrue("Complex parsed from gpml should have a valid name", names.contains("complex name"));
    assertTrue("Generic complex name should also be included", names.contains("ac7f7"));
    assertEquals(0, getWarnings().size());

  }

  @Test
  public void testComplexNameFromLabel() throws Exception {
    String fileName = "testFiles/complex/complex_with_label_name.gpml";
    Model model = new GPMLToModel().getModel(fileName);
    for (final Complex complex : model.getComplexList()) {
      assertEquals("p53 Related", complex.getName());
      assertEquals(VerticalAlign.BOTTOM, complex.getNameVerticalAlign());
      assertEquals(HorizontalAlign.CENTER, complex.getNameHorizontalAlign());
    }
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testCompartmentName() throws Exception {
    String fileName = "testFiles/compartment/compartment_name.gpml";
    Model model = new GPMLToModel().getModel(fileName);
    for (final Compartment compartment : model.getCompartments()) {
      assertEquals("Compartment parsed from gpml should have a valid name", "Label", compartment.getName());
    }
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testComplexInCompartment() throws Exception {
    String fileName = "testFiles/compartment/complex_in_compartment.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);
    assertEquals(3, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    // export to CellDesigner changes line width
    mc.setElementComparator(new ElementComparator(3));
    model2.setNotes(model1.getNotes());
    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void testAuthorEmail() throws Exception {
    String fileName = "testFiles/small/model_with_email.gpml";
    Model model = new GPMLToModel().getModel(fileName);
    assertEquals(2, model.getAuthors().size());

    Model model2 = super.serializeOverGpml(model);
    assertEquals(0, mc.compare(model, model2));
  }

  @Test
  public void testReactionToComplex() throws Exception {
    String fileName = "testFiles/small/reaction_to_complex.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));

    assertEquals(53, getWarnings().size());
  }

  @Test
  public void testColorfullComplexReaction() throws Exception {
    String fileName = "testFiles/reactions/complex_color_reaction.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void testColorfullComplexReaction2() throws Exception {
    String fileName = "testFiles/reactions/complex_color_reaction2.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void testProteinWithModification() throws Exception {
    String fileName = "testFiles/small/protein_with_modification.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    Gene gene = (Gene) model1.getElementByElementId("be3de");
    assertNotNull(gene);
    assertEquals(2, gene.getModificationResidues().size());
    for (ModificationResidue mr : gene.getModificationResidues()) {
      assertNotNull(mr.getBorderColor());
    }

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void testProteinWithModification2() throws Exception {
    String fileName = "testFiles/small/protein_with_modification_2.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    Gene protein = (Gene) model1.getElementByElementId("be3de");
    assertNotNull(protein);
    assertEquals(2, protein.getModificationResidues().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void testModifierPosition() throws Exception {
    String fileName = "testFiles/small/modifier_misaligned.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    Reaction reaction = model1.getReactions().iterator().next();

    Line2D centerLine = reaction.getLine().getLines().get(reaction.getLine().getLines().size() / 2);
    double distance = pt.getPointOnLine(centerLine.getP1(), centerLine.getP2(), 0.5)
        .distance(reaction.getModifiers().get(0).getLine().getEndPoint());
    assertTrue("Modifier is too far from center point: " + distance,
        distance < ReactionCellDesignerConverter.RECT_SIZE);

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testReactionToLabel() throws Exception {
    String fileName = "testFiles/small/reaction_to_label.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, getWarnings().size());

    assertEquals(1, model1.getReactions().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void testModifierAsLabel() throws Exception {
    String fileName = "testFiles/small/modifier_to_label.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());
    assertEquals(3, model1.getReactions().iterator().next().getNodes().size());

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void testModifierAdsLabel() throws Exception {
    String fileName = "testFiles/small/protein_with_state.gpml";
    Model model1 = new GPMLToModel().getModel(fileName);

    Protein protein = (Protein) model1.getElementsByName("Protein").get(0);
    assertEquals(1, protein.getModificationResidues().size());
    assertEquals(StructuralState.class, protein.getModificationResidues().get(0).getClass());
  }

  @Test
  public void parseModelAnnotation() throws Exception {
    String fileName = "testFiles/small/model_annotation.gpml";
    Model model = new GPMLToModel().getModel(fileName);
    assertEquals(1, model.getMiriamData().size());
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void parseInvalidModelAnnotation() throws Exception {
    String fileName = "testFiles/small/model_invalid_annotation.gpml";
    Model model = new GPMLToModel().getModel(fileName);
    assertEquals(0, model.getMiriamData().size());
    assertEquals(2, getWarnings().size());
  }

  @Test
  public void parseElementWithComment() throws Exception {
    String fileName = "testFiles/small/element_with_comment.gpml";
    Model model = new GPMLToModel().getModel(fileName);
    Element element = model.getElementByElementId("a12");
    assertTrue(element.getNotes().length() > 0);
  }

  @Test
  public void parseReversibleReaction() throws Exception {
    String fileName = "testFiles/small/reversible_reaction.gpml";
    Model model = new GPMLToModel().getModel(fileName);
    Reaction reaction = model.getReactionByReactionId("e5d42");
    assertTrue(reaction.isReversible());
  }

  @Test
  public void parseGraphicallyReversibleReaction() throws Exception {
    String fileName = "testFiles/small/reversible_reaction_2.gpml";
    Model model = new GPMLToModel().getModel(fileName);
    Reaction reaction = model.getReactionByReactionId("idd62d2e40");
    assertTrue(reaction.isReversible());
  }

  @Test
  public void parseReactionWithLabelAsSideProduct() throws Exception {
    String fileName = "testFiles/small/reaction_with_label_as_side_product.gpml";
    Model model = new GPMLToModel().getModel(fileName);
    Reaction reaction = model.getReactionByReactionId("id6f2bfa37");
    assertEquals(2, reaction.getProducts().size());
  }

  @Test
  public void parseNodeWithMissingGroup() throws Exception {
    String fileName = "testFiles/small/node_with_missing_group.gpml";
    Model model = new GPMLToModel().getModel(fileName);
    assertEquals(1, getWarnings().size());
    assertEquals(1, model.getElements().size());
  }

  @Test
  public void parseMoleculeWithColor() throws Exception {
    String fileName = "testFiles/small/molecule_with_color.gpml";
    Model model = new GPMLToModel().getModel(fileName);
    Element element = model.getElementByElementId("d9620");
    logger.debug(element.getFontColor());
  }

  @Test
  public void parseComplexName() throws Exception {
    String fileName = "testFiles/small/compartment_without_name_and_with_complex_with_name.gpml";
    Model model = new GPMLToModel().getModel(fileName);
    Complex complex = model.getElementByElementId("b33da");
    assertFalse(complex.getName().isEmpty());
    Compartment compartment = model.getElementByElementId("a3e41");
    assertTrue(compartment.getName().isEmpty());
  }

  @Test
  public void parseCompartmentFromShape() throws Exception {
    String fileName = "testFiles/small/compartment_from_shape.gpml";
    Model model = new GPMLToModel().getModel(fileName);
    Compartment compartment = model.getElementByElementId("d5695");
    assertFalse(compartment.getName().isEmpty());
    assertTrue(compartment.contains(new Point2D.Double(compartment.getNameX(), compartment.getNameY())));
  }

  @Test
  public void lineLineCombinedReactionsTest() throws Exception {
    String fileName = "testFiles/reactions/line_line_combined_reaction.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(4, model1.getReactions().size());

    assertEquals(4, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

}
