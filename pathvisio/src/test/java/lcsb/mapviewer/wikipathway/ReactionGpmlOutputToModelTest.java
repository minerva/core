package lcsb.mapviewer.wikipathway;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.modifier.Inhibition;
import lcsb.mapviewer.model.map.modifier.Modulation;
import lcsb.mapviewer.model.map.modifier.PhysicalStimulation;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.wikipathway.xml.GPMLToModel;

public class ReactionGpmlOutputToModelTest extends WikipathwaysTestFunctions {


  private ModelComparator mc = new ModelComparator(1.0);

  @Test
  public void LineInteractionOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_line_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modifier);
    }

    assertNotNull(reactant);
    assertNull(product);
    assertNull(modifier);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void ArrowInteractionOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_arrow_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));

  }

  @Test
  public void DashedLineInteractionOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_line_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modifier);
    }

    assertNotNull(reactant);
    assertNull(product);
    assertNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));

  }

  @Test
  public void DashedArrowInteractionOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_arrow_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void TBarInteractionOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_tbar_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Inhibition);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void NecessaryStimulationInteractionOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_necessary_stimulation_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof PhysicalStimulation);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void BindingInteractionOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_binding_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void ConversionInteractionOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_conversion_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void StimulationInteractionOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_stimulation_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof PhysicalStimulation);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void ModificationInteractionOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_modification_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void CatalystInteractionOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_catalyst_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Catalysis);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void InhibitionInteractionOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_inhibition_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Inhibition);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void CleavageInteractionOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_cleavage_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void CovalentBondInteractionOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_covalent_bond_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modifier);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void BranchingLeftInteractionOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_branching_left_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modifier);
    }

    assertNotNull(reactant);
    assertNull(product);
    assertNull(modifier);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void BranchingRightInteractionOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_branching_right_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modifier);
    }

    assertNotNull(reactant);
    assertNull(product);
    assertNull(modifier);

    assertEquals(0, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void TranscriptionTranslationInteractionOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_transcription_translation_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void GapInteractionOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_mim_gap_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(1, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void TBarInteractionDashedOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_tbar_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Inhibition);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void NecessaryStimulationInteractionDashedOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_necessary_stimulation_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof PhysicalStimulation);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void BindingInteractionDashedOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_binding_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void ConversionInteractionDashedOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_conversion_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void StimulationInteractionDashedOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_stimulation_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof PhysicalStimulation);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void ModificationInteractionDashedOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_modification_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void CatalystInteractionDashedOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_catalyst_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Catalysis);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void InhibitionInteractionDashedOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_inhibition_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Inhibition);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void CleavageInteractionDashedOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_cleavage_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void CovalentBondInteractionDashedOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_covalent_bond_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modifier);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void BranchingLeftInteractionDashedOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_branching_left_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modifier);
    }

    assertNotNull(reactant);
    assertNull(product);
    assertNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void BranchingRightInteractionDashedOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_branching_right_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modifier);
    }

    assertNotNull(reactant);
    assertNull(product);
    assertNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void TranscriptionTranslationInteractionDashedOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_transcription_translation_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

  @Test
  public void GapInteractionDashedOutputTest() throws Exception {
    String fileName = "testFiles/reactions/interaction_dashed_mim_gap_output.gpml";

    Model model1 = new GPMLToModel().getModel(fileName);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    Product product = null;
    if (reaction.getProducts().size() > 1) {
      product = reaction.getProducts().get(1);
    }
    Reactant reactant = null;
    if (reaction.getReactants().size() > 1) {
      reactant = reaction.getReactants().get(1);
    }
    Modifier modifier = null;
    if (reaction.getModifiers().size() > 0) {
      modifier = reaction.getModifiers().get(0);
      assertTrue(modifier instanceof Modulation);
    }

    assertNull(reactant);
    assertNotNull(product);
    assertNull(modifier);

    assertEquals(2, getWarnings().size());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }
}
