package lcsb.mapviewer.wikipathway;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.HeterodimerAssociationReaction;
import lcsb.mapviewer.model.map.reaction.type.NegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.PositiveInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedPhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownNegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownPositiveInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownTransitionReaction;
import lcsb.mapviewer.wikipathway.xml.GPMLToModel;

@RunWith(Parameterized.class)
public class ReactionGpmlToModelTest extends WikipathwaysTestFunctions {


  private ModelComparator mc = new ModelComparator(1.0);

  private String filename;

  private boolean isWarning;

  private Class<? extends Reaction> reactionClass;

  private boolean reversible;

  public ReactionGpmlToModelTest(final String name, final String filename, final boolean isWarning,
      final Class<? extends Reaction> reactionClass, final boolean reversible) {
    this.filename = filename;
    this.isWarning = isWarning;
    this.reactionClass = reactionClass;
    this.reversible = reversible;
  }

  @Parameters(name = "{0}")
  public static Collection<Object[]> data() {
    List<Object[]> result = new ArrayList<>();

    result.add(new Object[] { "Line", "testFiles/reactions/interaction_line.gpml", true,
        UnknownTransitionReaction.class, true });
    result.add(new Object[] { "Arrow", "testFiles/reactions/interaction_arrow.gpml", false,
        StateTransitionReaction.class, false });
    result.add(new Object[] { "Dashed Arrow", "testFiles/reactions/interaction_dashed_arrow.gpml", false,
        UnknownPositiveInfluenceReaction.class, false });
    result.add(new Object[] { "Dashed Line", "testFiles/reactions/interaction_dashed_line.gpml", true,
        UnknownTransitionReaction.class, true });
    // invalid mim_binding (contains only one reactant)
    result.add(new Object[] { "Invalid Binding", "testFiles/reactions/interaction_mim_binding.gpml", true,
        UnknownTransitionReaction.class, false });
    // proper mim_binding (contains two reactants)
    result.add(new Object[] { "Binding", "testFiles/reactions/interaction_mim_binding2.gpml", false,
        HeterodimerAssociationReaction.class, false });
    result.add(new Object[] { "Branching Left", "testFiles/reactions/interaction_mim_branching_left.gpml", true,
        UnknownTransitionReaction.class, true });
    result.add(new Object[] { "Branching Right", "testFiles/reactions/interaction_mim_branching_right.gpml", true,
        UnknownTransitionReaction.class, true });
    result.add(new Object[] { "Catalyst", "testFiles/reactions/interaction_mim_catalyst.gpml", true,
        PositiveInfluenceReaction.class, false });
    result.add(new Object[] { "Cleavage", "testFiles/reactions/interaction_mim_cleavage.gpml", false,
        StateTransitionReaction.class, false });
    result.add(new Object[] { "Conversion", "testFiles/reactions/interaction_mim_conversion.gpml", false,
        StateTransitionReaction.class, false });
    result.add(new Object[] { "Covalent Bond", "testFiles/reactions/interaction_mim_covalent_bond.gpml", true,
        UnknownTransitionReaction.class, true });
    result.add(new Object[] { "Gap", "testFiles/reactions/interaction_mim_gap.gpml", true,
        UnknownTransitionReaction.class, true });
    result.add(new Object[] { "Inhibition", "testFiles/reactions/interaction_mim_inhibition.gpml", false,
        NegativeInfluenceReaction.class, false });
    result.add(new Object[] { "Modification", "testFiles/reactions/interaction_mim_modification.gpml", true,
        PositiveInfluenceReaction.class, false });
    result.add(new Object[] { "Necessary Stimulation", "testFiles/reactions/interaction_mim_necessary_stimulation.gpml", false,
        ReducedPhysicalStimulationReaction.class, false });
    result.add(new Object[] { "Stimulation", "testFiles/reactions/interaction_mim_stimulation.gpml", true,
        ReducedPhysicalStimulationReaction.class, false });
    result.add(new Object[] { "Transcrption/Translation", "testFiles/reactions/interaction_mim_transcription_translation.gpml", true,
        StateTransitionReaction.class, false });
    result.add(new Object[] { "Tbar", "testFiles/reactions/interaction_tbar.gpml", true,
        NegativeInfluenceReaction.class, false });
    result.add(new Object[] { "Line as interaction", "testFiles/reactions/line_as_interaction.gpml", true,
        UnknownTransitionReaction.class, true });
    result.add(new Object[] { "Dashed Binding", "testFiles/reactions/interaction_dashed_mim_binding.gpml", false,
        UnknownPositiveInfluenceReaction.class, false });
    result.add(new Object[] { "Dashed Branching Left", "testFiles/reactions/interaction_dashed_mim_branching_left.gpml", true,
        UnknownTransitionReaction.class, true });
    result.add(new Object[] { "Dashed Branching Right", "testFiles/reactions/interaction_dashed_mim_branching_right.gpml", true,
        UnknownTransitionReaction.class, true });
    result.add(new Object[] { "Dashed Catalyst", "testFiles/reactions/interaction_dashed_mim_catalyst.gpml", true,
        UnknownPositiveInfluenceReaction.class, false });
    result.add(new Object[] { "Dashed Cleavage", "testFiles/reactions/interaction_dashed_mim_cleavage.gpml", true,
        UnknownTransitionReaction.class, false });
    result.add(new Object[] { "Dashed Conversion", "testFiles/reactions/interaction_dashed_mim_conversion.gpml", false,
        UnknownPositiveInfluenceReaction.class, false });
    result.add(new Object[] { "Dashed Covalent Bond", "testFiles/reactions/interaction_dashed_mim_covalent_bond.gpml", true,
        UnknownTransitionReaction.class, true });
    result.add(new Object[] { "Dashed Gap", "testFiles/reactions/interaction_dashed_mim_gap.gpml", true,
        UnknownTransitionReaction.class, true });
    result.add(new Object[] { "Dashed Inhibition", "testFiles/reactions/interaction_dashed_mim_inhibition.gpml", true,
        UnknownNegativeInfluenceReaction.class, false });
    result.add(new Object[] { "Dashed Modification", "testFiles/reactions/interaction_dashed_mim_modification.gpml", false,
        UnknownPositiveInfluenceReaction.class, false });
    result.add(new Object[] { "Dashed Necessary Stimulation", "testFiles/reactions/interaction_dashed_mim_necessary_stimulation.gpml", true,
        UnknownPositiveInfluenceReaction.class, false });
    result.add(new Object[] { "Dashed Stimulation", "testFiles/reactions/interaction_dashed_mim_stimulation.gpml", true,
        UnknownPositiveInfluenceReaction.class, false });
    result.add(new Object[] { "Dashed Transcription/Translation", "testFiles/reactions/interaction_dashed_mim_transcription_translation.gpml", true,
        StateTransitionReaction.class, false });
    result.add(new Object[] { "Dashed Tbar", "testFiles/reactions/interaction_dashed_tbar.gpml", true,
        UnknownNegativeInfluenceReaction.class, false });
    return result;
  }

  @Test
  public void test() throws Exception {
    Model model1 = new GPMLToModel().getModel(filename);

    assertEquals(1, model1.getReactions().size());

    Reaction reaction = model1.getReactions().iterator().next();

    if (isWarning) {
      assertEquals(1, getWarnings().size());
    }
    assertTrue(reactionClass.isAssignableFrom(reaction.getClass()));
    assertEquals(reversible, reaction.isReversible());

    Model model2 = serializeModelOverCellDesignerParser(model1);

    assertEquals(0, mc.compare(model1, model2));
  }

}
