package lcsb.mapviewer.wikipathway;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.wikipathway.xml.GPMLToModel;
import lcsb.mapviewer.wikipathway.xml.ModelToGPML;

@Component
public class GpmlParser extends Converter {

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  @Override
  public Model createModel(final ConverterParams params) throws InvalidInputDataExecption, ConverterException {
    GPMLToModel gpmlToModel = new GPMLToModel();
    File inputFile = null;
    try {
      Model result = gpmlToModel.getModel(params.getInputStream());
      return result;

    } catch (final IOException e) {
      throw new ConverterException(e);
    } finally {
      assert inputFile == null || inputFile.delete();
    }
  }

  @Override
  public String model2String(final Model model) throws InconsistentModelException, ConverterException {
    ModelToGPML modelToGPML = new ModelToGPML(model.getName());
    return modelToGPML.getGPML(model);
  }

  @Override
  public String getCommonName() {
    return "GPML";
  }

  @Override
  public MimeType getMimeType() {
    return MimeType.XML;
  }

  @Override
  public List<String> getFileExtensions() {
    return Arrays.asList("gpml");
  }

}
