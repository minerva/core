package lcsb.mapviewer.wikipathway.utils;

import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.geometry.EllipseTransformation;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.model.graphics.PolylineData;

/**
 * This class has some geometric utils.
 * 
 * @author Jan Badura
 * 
 */
public final class Geo {

  /**
   * Max distance between point and line that still allows to match intersection.
   */
  private static final int DISTANCE_PROXIMITY = 10;

  /**
   * Default class constructor. Prevents instantiation.
   */
  private Geo() {

  }

  /**
   * This function return value from <0,1>, that is the position of given point on
   * given Polyline.
   * 
   * @param mainLine
   *          line on which we are looking for a point
   * @param point
   *          point
   * @return double position, 0 <= position <= 1
   */
  public static double distanceOnPolyline(final PolylineData mainLine, final Point2D point) {
    double length = 0;
    double tmp = 0;
    for (final Line2D line : mainLine.getLines()) {
      length += lineLen(line);
    }

    for (final Line2D line : mainLine.getLines()) {
      if (line.ptSegDist(point) < DISTANCE_PROXIMITY) {
        tmp += line.getP1().distance(point);
        break;
      } else {
        tmp += lineLen(line);
      }
    }
    double position = tmp / length;
    if (position < 0 || position > 1) {
      throw new InvalidStateException();
    }
    return position;
  }

  /**
   * Returns length of the line.
   * 
   * @param line
   *          line
   * @return length of the line
   */
  public static double lineLen(final Line2D line) {
    return line.getP1().distance(line.getP2());
  }

  public static double distance(final Point2D point, final Shape shape) {
    if (shape instanceof Rectangle2D || shape instanceof Polygon) {
      return new PointTransformation()
          .getClosestPointOnPathIterator(point, shape.getPathIterator(new AffineTransform()))
          .distance(point);
    } else if (shape instanceof Ellipse2D) {
      return new EllipseTransformation().getClosestPoint(point, (Ellipse2D) shape).distance(point);
    } else {
      throw new InvalidArgumentException("Unknown shape: " + shape.getClass());
    }
  }

  public static Point2D closestPoint(final Shape shape, final Point2D point) {
    if (shape instanceof Rectangle2D || shape instanceof Polygon) {
      return new PointTransformation().getClosestPointOnPathIterator(point,
          shape.getPathIterator(new AffineTransform()));
    } else if (shape instanceof Ellipse2D) {
      return new EllipseTransformation().getClosestPoint(point, (Ellipse2D) shape);
    } else {
      throw new InvalidArgumentException("Unknown shape: " + shape.getClass());
    }
  }

  /**
   * Returns distance between point and line.
   * 
   * @param point
   *          point
   * @param mainLine
   *          line
   * @return distance between point and line
   */
  public static double distanceFromPolyline(final Point2D point, final PolylineData mainLine) {
    double min = Double.MAX_VALUE;
    for (final Line2D line : mainLine.getLines()) {
      min = Math.min(min, line.ptSegDist(point));
    }
    return min;
  }

  /**
   * Returns point on line that is as close as possible to point.
   * 
   * @param point
   *          point
   * @param mainLine
   *          line
   * @return point on line that is as close as possible to point
   */
  public static Point2D closestPointOnPolyline(final PolylineData mainLine, final Point2D point) {
    Point2D res = mainLine.getStartPoint();
    for (final Line2D line : mainLine.getLines()) {
      Point2D newPoint = getClosestPointOnSegment(line, point);
      if (res.distanceSq(point) > newPoint.distanceSq(point)) {
        res = newPoint;
      }
    }
    return res;
  }

  static Point2D getClosestPointOnSegment(final Line2D line, final Point2D point) {
    double dx = line.getX2() - line.getX1();
    double dy = line.getY2() - line.getY1();

    if ((dx == 0) && (dy == 0)) {
      return new Point2D.Double(line.getX1(), line.getY1());
    }

    double u = ((point.getX() - line.getX1()) * dx + (point.getY() - line.getY1()) * dy)
        / (dx * dx + dy * dy);

    if (u < 0) {
      return new Point2D.Double(line.getX1(), line.getY1());
    } else if (u > 1) {
      return new Point2D.Double(line.getX2(), line.getY2());
    } else {
      return new Point2D.Double((int) Math.round(line.getX1() + u * dx),
          (int) Math.round(line.getY1() + u * dy));
    }
  }

}
