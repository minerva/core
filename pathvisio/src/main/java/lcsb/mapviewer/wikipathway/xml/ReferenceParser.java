package lcsb.mapviewer.wikipathway.xml;

import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.wikipathway.model.ReferenceMapping;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Collection;

/**
 * Parser used to extract {@link MiriamData references} in miriam format from
 * GPML data.
 *
 * @author Piotr Gawron
 */
public class ReferenceParser extends ElementGpmlParser<MiriamData> {

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  public ReferenceParser(final String mapName) {
    super(mapName);
  }

  /**
   * This function creates MiriamData from database name and id.
   *
   * @param id - resource identifier
   * @param db -database type
   * @return {@link MiriamData} object referencing to the resource
   */
  protected MiriamData createMiriamData(final String id, final String db) {
    if (db == null || db.equals("")) {
      throw new InvalidArgumentException("Invalid db type: " + db);
    } else if (id == null || id.trim().equals("")) {
      throw new InvalidArgumentException("Invalid db resource value: " + id);
    }
    ReferenceMapping mapping = ReferenceMapping.getMappingByGpmlString(db);
    MiriamType type = null;
    if (mapping != null) {
      type = mapping.getType();
    } else {
      for (final MiriamType t : MiriamType.values()) {
        if (t.getCommonName().equalsIgnoreCase(db)) {
          type = t;
        } else {
          for (final String uri : t.getUris()) {
            if (uri.endsWith(db)) {
              type = t;
              break;
            }
          }
        }
      }
    }
    if (type != null) {
      return new MiriamData(type, id);
    } else {
      logger.warn("This database type is not implemented yet: " + db + "; resource: " + id);
      return null;
    }
  }

  public MiriamData hrefToMiriamData(final String href) {
    if (href == null) {
      return null;
    }
    if (href.startsWith("https://omim.org/entry/")) {
      return new MiriamData(MiriamType.OMIM, href.replaceAll("https://omim.org/entry/", ""));
    }
    if (href.startsWith("http://omim.org/entry/")) {
      return new MiriamData(MiriamType.OMIM, href.replaceAll("http://omim.org/entry/", ""));
    }
    if (href.startsWith("https://omim.org/")) {
      return new MiriamData(MiriamType.OMIM, href.replaceAll("https://omim.org/", ""));
    }
    if (href.startsWith("http://omim.org/")) {
      return new MiriamData(MiriamType.OMIM, href.replaceAll("http://omim.org/", ""));
    }
    if (href.startsWith("https://www.omim.org/entry/")) {
      return new MiriamData(MiriamType.OMIM, href.replaceAll("https://www.omim.org/entry/", ""));
    }
    if (href.startsWith("http://www.omim.org/entry/")) {
      return new MiriamData(MiriamType.OMIM, href.replaceAll("http://www.omim.org/entry/", ""));
    }
    if (href.startsWith("https://www.omim.org/")) {
      return new MiriamData(MiriamType.OMIM, href.replaceAll("https://www.omim.org/", ""));
    }
    if (href.startsWith("http://www.omim.org/")) {
      return new MiriamData(MiriamType.OMIM, href.replaceAll("http://www.omim.org/", ""));
    }
    return null;
  }

  @Override
  public MiriamData parse(final Element node) {
    return parse(node, null);
  }

  public MiriamData parse(final Element node, final LogMarker logMarker) {
    String id = null;
    String db = null;
    for (Pair<String, String> entry : getAttributes(node)) {
      switch (entry.getLeft()) {
        case ("ID"):
          id = entry.getRight();
          break;
        case ("Database"):
          db = entry.getRight();
          break;
        default:
          logger.warn(logMarker, "Unknown attribute of " + node.getNodeName() + " node: " + entry.getLeft());
          break;
      }
    }

    NodeList tmpList = node.getChildNodes();
    for (int j = 0; j < tmpList.getLength(); j++) {
      Node childNode = tmpList.item(j);
      if (childNode.getNodeType() == Node.ELEMENT_NODE) {
        Element childElement = (Element) childNode;
        switch (childElement.getNodeName()) {
          default:
            logger.warn(logMarker, "Unknown sub-node of " + node.getNodeName() + " node: " + childElement.getNodeName());
            break;
        }
      }
    }

    if (id != null && !id.isEmpty()) {
      if (db == null || db.isEmpty()) {
        logger.warn(logMarker,
            "Reference is invalid. Database identifier exists (" + id + "), but no database type is set.");
      } else {
        try {
          return createMiriamData(id, db);
        } catch (final InvalidArgumentException e) {
          logger.warn(logMarker, e.getMessage());
        } catch (final NotImplementedException e) {
          logger.warn(logMarker, "Unknown database type: " + db + ". Identifier: " + id);
        }
      }
    }
    return null;
  }

  @Override
  public String toXml(final MiriamData md, final LogMarker logMarker) throws ConverterException {
    if (md == null) {
      return "<Xref ID=\"\" Database=\"\"/>\n";
    } else if (MiriamType.PUBMED.equals(md.getDataType())) {
      throw new InvalidArgumentException("Pubmed cannot be exported to XRef node.");
    } else {
      ReferenceMapping mapping = ReferenceMapping.getMappingByMiriamType(md.getDataType());
      if (mapping == null) {
        logger.warn("Don't know how to export " + md.getDataType() + " annotation.");
        return "";
      } else if (mapping.getGpmlString() == null) {
        logger.warn(md.getDataType().getCommonName() + " annotation is not supported by GPML");
        return "";
      } else {
        return "<Xref ID=\"" + StringEscapeUtils.escapeXml11(md.getResource()) + "\" Database=\"" + mapping.getGpmlString() + "\"/>\n";
      }
    }
  }

  @Override
  public String toXml(final Collection<MiriamData> miriamData, final LogMarker logMarker) throws ConverterException {
    StringBuilder result = new StringBuilder();
    int counter = 0;
    for (final MiriamData md : miriamData) {
      if (!md.getDataType().equals(MiriamType.PUBMED)) {
        counter++;
        if (counter == 1) {
          MinervaLoggerAppender appender = MinervaLoggerAppender.createAppender();
          try {
            result.append(toXml(md, logMarker));
            if (appender.getWarnings().size() > 0) {
              counter--;
            }
          } finally {
            MinervaLoggerAppender.unregisterLogEventStorage(appender);
          }
        } else {
          logger.warn(logMarker, "Annotation ommited - gpml support only one annotation per element: " + md.getDataType() + ": "
              + md.getResource());
        }
      }
    }
    if (counter == 0) {
      return toXml((MiriamData) null, null);
    }
    return result.toString();
  }
}
