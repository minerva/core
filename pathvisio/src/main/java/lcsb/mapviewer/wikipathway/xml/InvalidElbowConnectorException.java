package lcsb.mapviewer.wikipathway.xml;

import lcsb.mapviewer.converter.ConverterException;

/**
 * Exception that shold be thrown when lines described as
 * {@link lcsb.mapviewer.wikipathway.model.GpmlLineConnectorType#ELBOW} cannot
 * be transformed into standard x,y coordinates.
 * 
 * @author Piotr Gawron
 * 
 */
public class InvalidElbowConnectorException extends ConverterException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor with message passed in the argument.
   * 
   * @param string
   *          message of this exception
   */
  public InvalidElbowConnectorException(final String string) {
    super(string);
  }

}
