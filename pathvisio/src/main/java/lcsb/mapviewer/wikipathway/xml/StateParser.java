package lcsb.mapviewer.wikipathway.xml;

import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.wikipathway.model.GpmlModificationType;
import lcsb.mapviewer.wikipathway.model.State;
import lcsb.mapviewer.wikipathway.model.UnknownTypeException;

/**
 * Parser class that creates {@link State} objects from Xml {@link Element node}
 * .
 * 
 * @author Piotr Gawron
 *
 */
public class StateParser extends ElementGpmlParser<State> {

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  /**
   * Parser used for extracting {@link lcsb.mapviewer.model.map.MiriamData
   * references} from GPML model.
   */
  private ReferenceParser referenceParser;

  public StateParser(final String mapName) {
    super(mapName);
    referenceParser = new ReferenceParser(mapName);
  }

  @Override
  public State parse(final Element element) throws ConverterException {
    if (!element.getNodeName().equals("State")) {
      throw new InvalidArgumentException(StateParser.class.getSimpleName() + " can parse only State xml nodes");
    }
    State state = new State(element.getAttribute("GraphId"), getMapName());
    for (Pair<String, String> entry : getAttributes(element)) {
      switch (entry.getLeft()) {
        case ("GraphId"):
          break;
        case ("TextLabel"):
          try {
            GpmlModificationType type = GpmlModificationType.getByGpmlName(entry.getRight());
            state.setType(type.getCorrespondingModificationState());
          } catch (final UnknownTypeException e) {
            if (isModificationPosition(entry.getRight())) {
              state.setType(ModificationState.UNKNOWN);
              state.setTypeName(entry.getRight());
            } else {
              state.setStructuralState(entry.getRight());
            }
          }
          break;
        case ("GraphRef"):
          state.setGraphRef(entry.getRight());
          break;
        default:
          logger.warn("Unknown attribute of " + element.getNodeName() + " node: " + entry.getLeft());
          break;
      }
    }

    NodeList tmpList = element.getChildNodes();
    for (int j = 0; j < tmpList.getLength(); j++) {
      Node childNode = tmpList.item(j);
      if (childNode.getNodeType() == Node.ELEMENT_NODE) {
        Element childElement = (Element) childNode;
        switch (childElement.getNodeName()) {
          case ("Comment"):
            state.addComment(childElement.getTextContent());
            break;
          case ("Graphics"):
            parseGraphics(childElement, state);
            break;
          case ("Attribute"):
            parseAttribute(childElement, state);
            break;
          case ("BiopaxRef"):
            state.addBiopaxReference(childElement.getTextContent());
            break;
          case ("Xref"):
            MiriamData md = referenceParser.parse(childElement);
            if (md != null) {
              state.addReference(md);
            }
            break;
          default:
            logger.warn("Unknown sub-node of " + element.getNodeName() + " node: " + childElement.getNodeName());
            break;
        }
      }
    }
    return state;
  }

  boolean isModificationPosition(final String right) {
    return right.matches("^[a-zA-Z][0-9]+$");
  }

  @Override
  public String toXml(final State node, final LogMarker logMarker) throws ConverterException {
    throw new NotImplementedException();
  }

  @Override
  public String toXml(final Collection<State> list, final LogMarker logMarker) throws ConverterException {
    throw new NotImplementedException();
  }

  /**
   * Parse graphics xml node in the state node.
   *
   * @param element
   *          xml node with graphics
   * @param state
   *          state where data should be added
   * @throws UnknownTypeException
   *           thrown when some types in the xml node are unknown
   */
  private void parseGraphics(final Element element, final State state) throws UnknownTypeException {
    for (Pair<String, String> entry : getAttributes(element)) {
      switch (entry.getLeft()) {
        case ("RelX"):
          state.setRelX(Double.valueOf(entry.getRight()));
          break;
        case ("RelY"):
          state.setRelY(Double.valueOf(entry.getRight()));
          break;
        case ("Width"):
          state.setWidth(Double.valueOf(entry.getRight()));
          break;
        case ("Height"):
          state.setHeight(Double.valueOf(entry.getRight()));
          break;
        case ("ShapeType"):
          state.setShape(entry.getRight());
          break;
        case ("FillColor"):
          state.setFillColor(hexStringToColor(entry.getRight()));
          break;
        default:
          logger.warn("Unknown attribute of " + element.getNodeName() + " node: " + entry.getLeft());
          break;
      }
    }
  }

  /**
   * Method that parses {@link State} xml attribute.
   *
   * @param element
   *          xml node with attribute
   * @param state
   *          state where data should be added
   */
  private void parseAttribute(final Element element, final State state) {
    String key = element.getAttribute("Key");
    String value = element.getAttribute("Value");
    switch (key) {
      default:
        logger.warn(state.getLogMarker() + "Unknown attribute of node. Key:" + key + "; value: " + value);
        break;
    }
  }

}
