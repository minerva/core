package lcsb.mapviewer.wikipathway.xml;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.model.LogMarker;

/**
 * Generic parser for GPML xml structures.
 * 
 * @author Piotr Gawron
 *
 * @param <T>
 *          class for which this parser applies
 */
public abstract class ElementGpmlParser<T> {

  /**
   * Color that should be used when we have transparent color.
   */
  private static final Color TRANSPARENT_COLOR = new Color(0, 0, 0, 0);
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger();

  private String mapName;

  public ElementGpmlParser(final String mapName) {
    this.mapName = mapName;
  }

  /**
   * Creates object from xml node given in parameter.
   * 
   * @param node
   *          xml node from which object will be created
   * @return object created from xml node
   * @throws ConverterException
   *           thrown when there is severe problem with parsing xml node
   */
  public abstract T parse(final Element node) throws ConverterException;

  /**
   * Converts object into xml string (in GPML format).
   * 
   * @param object
   *          object to transform
   * @return xml string representing object
   * @throws ConverterException
   *           thrown when there is a problem with converting into xml
   */
  public abstract String toXml(final T object, final LogMarker logMarker) throws ConverterException;

  /**
   * Converts list of objects into xml string (in GPML format) representing this
   * list.
   * 
   * @param list
   *          list objects to transform
   * @return xml string representing objects
   * @throws ConverterException
   *           thrown when there is a problem with converting into xml
   */
  public abstract String toXml(final Collection<T> list, final LogMarker logMarker) throws ConverterException;

  /**
   * Creates list of object from list of xml nodes.
   * 
   * @param nodes
   *          list of xml nodes to parse
   * @return list of object from list of xml nodes
   * @throws ConverterException
   *           thrown when there is severe problem with parsing xml node
   */
  public List<T> parseCollection(final Collection<Node> nodes) throws ConverterException {
    List<T> result = new ArrayList<>();
    for (final Node node : nodes) {
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        T element = parse((Element) node);
        // add only not null elements (null might appear when node is partially
        // invalid and warnings are set)
        if (element != null) {
          result.add(element);
        }
      }
    }
    return result;
  }

  /**
   * Transforms string representing color into {@link Color}.
   * 
   * @param hex
   *          string representing color
   * @return {@link Color} representation of input string
   */
  protected Color hexStringToColor(final String hex) {
    if (hex == null || hex.isEmpty()) {
      return null;
    }
    if ("Transparent".equalsIgnoreCase(hex)) {
      return TRANSPARENT_COLOR;
    }
    if (hex.length() != 6) {
      throw new InvalidArgumentException("Unknown color format: " + hex);
    }
    return new Color(Integer.valueOf(hex, 16));
  }

  /**
   * Returns list of {@link Node} attributes.
   * 
   * @param node
   *          xml node
   * @return set of pairs containing xml node attributes
   */
  protected Set<Pair<String, String>> getAttributes(final Node node) {
    Set<Pair<String, String>> result = new HashSet<>();
    NamedNodeMap map = node.getAttributes();
    for (int k = 0; k < map.getLength(); k++) {
      Node attribute = map.item(k);
      String name = attribute.getNodeName();
      String value = attribute.getNodeValue();
      result.add(new Pair<String, String>(name, value));
    }
    return result;
  }

  public String getMapName() {
    return mapName;
  }
}
