package lcsb.mapviewer.wikipathway.xml;

import lcsb.mapviewer.converter.ConverterException;

/**
 * Exception that shold be thrown when merging of two interactions cannot be
 * performed, because we don't know what should be the result of merging.
 * 
 * @author Piotr Gawron
 * 
 */
public class UnknownMergingMethodException extends ConverterException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor with message passed in the argument.
   * 
   * @param string
   *          message of this exception
   */
  public UnknownMergingMethodException(final String string) {
    super(string);
  }

}
