package lcsb.mapviewer.wikipathway.xml;

import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.wikipathway.model.Edge;

/**
 * Exception that shold be thrown when edges create a cycle. Consider an example
 * which has three edges:
 * <ul>
 * <li>edge_A - starts in edge_B and ends in some protein,</li>
 * <li>edge_B - starts in edge_C and ends in some protein,</li>
 * <li>edge_C - starts in edge_A and ends in some protein,</li>
 * </ul>
 * During resolving how it should be maintained there are two options - either
 * merge it (but in more complex example it's almost impossible to do it
 * properly) or report an errow. This exception will be thrown in such
 * situations.
 * 
 * @author Piotr Gawron
 * 
 */
public class CyclicEdgeException extends ConverterException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  private Edge edge;

  /**
   * Default constructor with message passed in the argument.
   * 
   * @param string
   *          message of this exception
   */
  public CyclicEdgeException(final String string, final Edge edge) {
    super(string);
    this.edge = edge;
  }

  public LogMarker getLogMarker() {
    if (edge != null) {
      return edge.getLogMarker();
    }
    return null;
  }

}
