package lcsb.mapviewer.wikipathway.xml;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.wikipathway.model.Graph;

/**
 * Class contains methods for GPMLToGraph conversion.
 * 
 * @author Jan Badura
 * 
 */
public class GPMLToModel {

  /**
   * This function creates model from GPML input stream.
   * 
   * @param stream
   *          input stream that contains GPML data
   * @return {@link Model} created from GPML file
   * @throws IOException
   *           thrown when there is a problem with input file
   * @throws ConverterException
   *           thrown when model couldn't be created
   */
  public Model getModel(final InputStream stream) throws IOException, ConverterException {
    Graph graph = new GpmlParser().createGraph(stream);
    Model model = new ModelContructor(graph.getMapName()).getModel(graph);
    model.addAuthors(graph.getAuthors());
    if (graph.getLastModified() != null) {
      model.addModificationDate(graph.getLastModified());
    }
    return model;
  }

  /**
   * This function creates model from gpml file.
   * 
   * @param fileName
   *          name of the gpml file
   * @return {@link Model} created from gpml file
   * @throws IOException
   *           thrown when there is a problem with input file
   * @throws ConverterException
   *           thrown when the was a problem with creating a model
   */
  public Model getModel(final String fileName) throws IOException, ConverterException {
    FileInputStream fis = new FileInputStream(fileName);
    return getModel(fis);
  }
}
