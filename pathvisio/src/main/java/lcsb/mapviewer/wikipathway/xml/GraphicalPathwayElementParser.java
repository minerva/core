package lcsb.mapviewer.wikipathway.xml;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.wikipathway.model.GpmlLineType;
import lcsb.mapviewer.wikipathway.model.GraphicalPathwayElement;
import lcsb.mapviewer.wikipathway.model.UnknownTypeException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.font.TextAttribute;

/**
 * Parser used for extracting common data (from gpml file) for all elements
 * extending {@link GraphicalPathwayElement} class.
 *
 * @author Piotr Gawron
 */
public abstract class GraphicalPathwayElementParser<T extends GraphicalPathwayElement> extends ElementGpmlParser<T> {

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  public GraphicalPathwayElementParser(final String mapName) {
    super(mapName);
  }

  /**
   * Parse font attributes that might appear.
   *
   * @param element   element where data should be put
   * @param attribute attribute to parse
   * @return true if element was parsed properly
   * @throws UnknownTypeException thrown when some elements contains unknown values
   */
  protected boolean parseCommonGraphicAttributes(final GraphicalPathwayElement element, final Pair<String, String> attribute)
      throws UnknownTypeException {
    switch (attribute.getLeft()) {
      case ("LineThickness"):
        try {
          Double value = Double.valueOf(attribute.getRight());
          element.setLineThickness(value);
        } catch (final NumberFormatException e) {
          logger.warn("Invalid LineThickness: " + attribute.getRight());
        }
        break;
      case ("LineStyle"):
        element.setLineType(GpmlLineType.getByGpmlName(attribute.getRight()).getCorrespondingGlobalLineType());
        break;
      case ("FontStyle"):
        element.addFontAttribute(TextAttribute.POSTURE, TextAttribute.POSTURE_OBLIQUE);
        break;
      case ("FontName"):
        element.setFontName(attribute.getRight());
        break;
      case ("Align"):
        switch (attribute.getRight()) {
          case ("Right"):
            element.setTextAlignment(HorizontalAlign.RIGHT);
            break;
          case ("Left"):
            element.setTextAlignment(HorizontalAlign.LEFT);
            break;
          default:
            logger.warn("Unknown value of attribute: " + attribute.getLeft() + " - " + attribute.getRight());
            break;
        }
        break;
      default:
        return false;
    }
    return true;
  }

}
