package lcsb.mapviewer.wikipathway.xml;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.PolylineDataFactory;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.wikipathway.model.Edge;
import lcsb.mapviewer.wikipathway.model.Graph;
import lcsb.mapviewer.wikipathway.model.Group;
import lcsb.mapviewer.wikipathway.model.Interaction;
import lcsb.mapviewer.wikipathway.model.InteractionMapping;
import lcsb.mapviewer.wikipathway.model.Label;
import lcsb.mapviewer.wikipathway.model.MergeMapping;
import lcsb.mapviewer.wikipathway.model.PointData;
import lcsb.mapviewer.wikipathway.model.Shape;
import lcsb.mapviewer.wikipathway.model.UnknownTypeException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class allows to parse
 * <a href="http://developers.pathvisio.org/wiki/EverythingGpml">gpml</a>.
 *
 * @author Jan Badura
 */
public class GpmlParser {

  static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger();

  /**
   * Parser used for creating {@link Shape shapes}.
   */
  private ShapeParser shapeParser;

  /**
   * Parser used for creating {@link lcsb.mapviewer.wikipathway.model.DataNode
   * data nodes}.
   */
  private DataNodeParser dataNodeParser;

  /**
   * Parser used for creating {@link Label labels}.
   */
  private LabelParser labelParser;

  /**
   * Parser used for creating {@link Edge edges}.
   */
  private EdgeParser edgeParser;

  /**
   * Parser used for creating {@link lcsb.mapviewer.wikipathway.model.State
   * states}.
   */
  private StateParser stateParser;

  /**
   * Parser used for creating {@link Edge edges} from line xml nodes.
   */
  private EdgeLineParser edgeLineParser;

  /**
   * Parser used for creating {@link PointData points} from line xml nodes.
   */
  private PointDataParser pointParser;

  /**
   * This function returns parent interaction for edge that has anchor at one
   * end.
   *
   * @param graph      model where data is stored
   * @param sourceEdge edge for which interaction is looked for
   * @return {@link Interaction} that is parent reaction for given edge.
   * @throws CyclicEdgeException thrown when parent interaction cannot be found because reactions
   *                             are cyclic
   */
  private Interaction getParentInteraction(final Graph graph, final Edge sourceEdge) throws CyclicEdgeException {
    // remember what we already processed (to detect cycles)
    Set<Edge> processedEdges = new HashSet<>();

    Edge edge = sourceEdge;
    processedEdges.add(edge);
    while (graph.getNodeByGraphId(edge.getEnd()) == null || graph.getNodeByGraphId(edge.getStart()) == null) {
      String anchor = null;
      if (graph.getEdgeByAnchor(edge.getEnd()) != null) {
        anchor = edge.getEnd();
      } else if (graph.getEdgeByAnchor(edge.getStart()) != null) {
        anchor = edge.getStart();
      } else {
        return null;
      }

      edge = graph.getEdgeByAnchor(anchor);
      // when we have cycle then return null
      if (processedEdges.contains(edge)) {
        throw new CyclicEdgeException("Edge is a part of invalid, cyclic edge", edge);
      }
      processedEdges.add(edge);
    }
    return graph.getInteractionByGraphId(edge.getGraphId());
  }

  /**
   * This function sets width and height in given graph.
   *
   * @param node  xml node where data is stored
   * @param graph object where data is stored
   */
  protected void setSize(final Node node, final Graph graph) {
    Element element = (Element) node;
    String boardWidth = element.getAttribute("BoardWidth");
    String boardHeight = element.getAttribute("BoardHeight");
    graph.setBoardWidth(Math.ceil(Double.parseDouble(boardWidth)));
    graph.setBoardHeight(Math.ceil(Double.parseDouble(boardHeight)));
  }

  /**
   * This function adds groups to graph and nest them.
   *
   * @param groups xml nodes
   * @param graph  object where data is stored
   * @throws UnknownAttributeValueException thrown when there is a problem with xml attributes
   */
  protected void addGroups(final List<Node> groups, final Graph graph) throws ConverterException {
    // Adding Groups to graph
    for (final Node node : groups) {
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element element = (Element) node;
        String graphId = element.getAttribute("GraphId");
        String groupId = element.getAttribute("GroupId");
        if (graphId.equals("") || graphId == null) {
          graphId = groupId;
        }

        Group group = new Group(graphId, groupId, graph.getMapName());

        NamedNodeMap attributes = element.getAttributes();
        for (int i = 0; i < attributes.getLength(); i++) {
          String value = attributes.item(i).getTextContent();
          switch (attributes.item(i).getNodeName()) {
            case ("GraphId"):
            case ("GroupId"):
              break;
            case ("GroupRef"):
              group.setGroupRef(value);
              break;
            case ("BiopaxRef"):
              group.addBiopaxReference(value);
              break;
            case ("TextLabel"):
              group.setTextLabel(value);
              break;
            case ("ZOrder"):
              if (!value.isEmpty()) {
                group.setzOrder(Integer.valueOf(value));
              }
              break;
            case ("Style"):
              if (!"".equals(value)) {
                if (!"Complex".equalsIgnoreCase(value)
                        && !"Pathway".equalsIgnoreCase(value)
                        && !"Group".equalsIgnoreCase(value)) {
                  throw new UnknownAttributeValueException(
                          "Unknown value of \"style\" attribute for group node: " + value
                                  + ". Only null, Complex, final Group are supported.");
                }
                group.setStyle(value);
              }
              break;
            default:
              logger.warn(group.getLogMarker(),
                      "Unknown sub-node of " + element.getNodeName() + " node: " + element.getNodeName());
              break;
          }
        }
        graph.addGroup(group);
      }
    }
    // Handling nested groups
    for (final Node node : groups) {
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element element = (Element) node;
        String groupRef = element.getAttribute("GroupRef");
        String groupId = element.getAttribute("GroupId");
        if (groupRef != null && !groupRef.equals("")) {
          Group gr1 = graph.getGroupByGroupId(groupRef);
          Group gr2 = graph.getGroupByGroupId(groupId);
          gr1.addNode(gr2);
        }
      }
    }
  }

  /**
   * This function adds edges to graph. It ignores edges that have no connection
   * at one end and edges that connects to shapes or labels.
   *
   * @param nodes xml nodes
   * @param graph object where data is sorted
   */
  private void prepareEdges(final List<Node> nodes, final Graph graph) {
    for (final Node node : nodes) {
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Edge edge;
        try {
          edge = edgeParser.parse((Element) node);

          if (graph.getLabelByGraphId(edge.getStart()) != null) {
            Label label = graph.getLabelByGraphId(edge.getStart());
            label.setTreatAsNode(true);
          }
          if (graph.getLabelByGraphId(edge.getEnd()) != null) {
            Label label = graph.getLabelByGraphId(edge.getEnd());
            label.setTreatAsNode(true);
          }

          if (graph.getShapeByGraphId(edge.getStart()) != null) {
            Shape shape = graph.getShapeByGraphId(edge.getStart());
            shape.setTreatAsNode(true);
          }
          if (graph.getShapeByGraphId(edge.getEnd()) != null) {
            Shape shape = graph.getShapeByGraphId(edge.getEnd());
            shape.setTreatAsNode(true);
          }
          if (edge.getStart() == null || edge.getEnd() == null) {
            logger.warn(edge.getLogMarker(), "Interaction is not connected");

          } else {
            graph.addEdge(edge);
          }
        } catch (final ConverterException e) {
          logger.warn(e, e);
        }
      }
    }
  }

  /**
   * This function transforms Edges into Interactions. First: Edges with
   * NodeToNode connection are transformed into Interactions. Second: Edges with
   * AnchorToNode connection are added to right interaction. Edges with
   * AnchorToAnchor connection are ignored.
   *
   * @param graph object where data is sorted
   * @throws InvalidXmlSchemaException thrown when the data in input file is invalid
   */
  protected void addInteractions(final Graph graph) throws InvalidXmlSchemaException {
    for (final Edge edge : graph.getEdges()) {
      if (graph.getNodeByGraphId(edge.getEnd()) != null && graph.getNodeByGraphId(edge.getStart()) != null) {
        markLabelsAndShapesToBecomeSpeciesForEdge(graph, edge);

        Interaction interaction = new Interaction(edge);
        graph.addInteraction(interaction);
        InteractionMapping mapping = InteractionMapping.getInteractionMapping(edge.getType(), edge.getLine().getType());
        if (mapping != null) {
          if (mapping.isInteractionWarning()) {
            logger.warn(edge.getLogMarker(), "Invalid interaction type.");
          }
        } else {
          throw new InvalidXmlSchemaException(
                  edge.getLogMarker() + "Unknown interaction type: " + edge.getType() + " and line type: "
                          + edge.getLine().getType());
        }
      }
    }
    for (final Edge edge : graph.getEdges()) {
      try {
        if (graph.getEdgeByAnchor(edge.getEnd()) != null && graph.getEdgeByAnchor(edge.getStart()) != null) {
          logger.warn(edge.getLogMarker(), "Interaction can not connect two anchors.");
        } else if (graph.getEdgeByAnchor(edge.getEnd()) != null && graph.getNodeByGraphId(edge.getStart()) != null) {
          Interaction tmp = getParentInteraction(graph, edge);
          if (tmp != null) {
            InteractionMapping mapping = InteractionMapping.getInteractionMapping(edge.getType(),
                    edge.getLine().getType());
            if (mapping != null) {
              if (mapping.isInputWarning()) {
                logger.warn(edge.getLogMarker(), "Invalid interaction type as an input to reaction.");
              }
              if (Modifier.class.isAssignableFrom(mapping.getModelInputReactionNodeType())) {
                tmp.addModifier(edge);
              } else if (Reactant.class.isAssignableFrom(mapping.getModelInputReactionNodeType())) {
                tmp.addReactant(edge);
              } else if (Product.class.isAssignableFrom(mapping.getModelInputReactionNodeType())) {
                tmp.addProduct(edge);
              } else {
                throw new InvalidStateException(
                        "Unknown internal model type: " + mapping.getModelInputReactionNodeType());
              }
              markLabelsAndShapesToBecomeSpeciesForEdge(graph, edge);
            } else {
              throw new InvalidXmlSchemaException("Unknown interaction type: " + edge.getType());
            }
          } else {
            logger.warn(edge.getLogMarker(), "Interaction is disconnected.");
          }
        } else if (graph.getEdgeByAnchor(edge.getStart()) != null && graph.getNodeByGraphId(edge.getEnd()) != null) {
          Interaction tmp = getParentInteraction(graph, edge);
          if (tmp != null) {
            InteractionMapping mapping = InteractionMapping.getInteractionMapping(edge.getType(),
                    edge.getLine().getType());
            if (mapping != null) {
              if (mapping.isOutputWarning()) {
                logger.warn(edge.getLogMarker(), "Invalid interaction type \"" + edge.getType()
                        + "\" as an output from reaction.");
              }
              if (Modifier.class.isAssignableFrom(mapping.getModelOutputReactionNodeType())) {
                tmp.addModifier(edge);
              } else if (Reactant.class.isAssignableFrom(mapping.getModelOutputReactionNodeType())) {
                tmp.addReactant(edge);
              } else if (Product.class.isAssignableFrom(mapping.getModelOutputReactionNodeType())) {
                tmp.addProduct(edge);
              } else {
                throw new InvalidStateException(
                        "Unknown internal model type: " + mapping.getModelOutputReactionNodeType());
              }
              markLabelsAndShapesToBecomeSpeciesForEdge(graph, edge);
            } else {
              throw new InvalidXmlSchemaException("Unknown interaction type: " + edge.getType());
            }
          } else {
            logger.warn(edge.getLogMarker(), "Interaction is disconnected.");
          }
        } else if (graph.getNodeByGraphId(edge.getEnd()) == null || graph.getNodeByGraphId(edge.getStart()) == null) {
          logger.warn(edge.getLogMarker(), "Interaction edge is invalid (one end is not connected).");
        }
      } catch (final CyclicEdgeException e) {
        logger.warn(e.getLogMarker(), e.getMessage(), e);
      }
    }
  }

  private void markLabelsAndShapesToBecomeSpeciesForEdge(final Graph graph, final Edge edge) {
    if (graph.getNodeByGraphId(edge.getEnd()) instanceof Label) {
      Label label = (Label) graph.getNodeByGraphId(edge.getEnd());
      label.setTreatAsNode(true);
    }
    if (graph.getNodeByGraphId(edge.getStart()) instanceof Label) {
      Label label = (Label) graph.getNodeByGraphId(edge.getStart());
      label.setTreatAsNode(true);
    }
    if (graph.getShapeByGraphId(edge.getEnd()) instanceof Shape) {
      Shape shape = graph.getShapeByGraphId(edge.getEnd());
      shape.setTreatAsNode(true);
    }
    if (graph.getShapeByGraphId(edge.getStart()) instanceof Shape) {
      Shape shape = graph.getShapeByGraphId(edge.getStart());
      shape.setTreatAsNode(true);
    }
  }

  /**
   * Creates gpml {@link Graph} model from gpml input stream.
   *
   * @param stream input stream with gpml model
   * @return {@link Graph} model
   * @throws IOException        thrown when there is a problem with input file
   * @throws ConverterException thrown when there is a problem with parsing input file
   */
  public Graph createGraph(final InputStream stream) throws IOException, ConverterException {
    try {
      DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      Document document = builder.parse(stream);

      // Get the document's root XML node
      NodeList root = document.getElementsByTagName("Pathway");
      Node pathwayNode = root.item(0);

      Graph graph = new Graph();
      for (Pair<String, String> entry : getAttributes(pathwayNode)) {
        String value = entry.getRight().trim();
        switch (entry.getLeft()) {
          case ("Version"):
          case ("License"):
          case ("Organism"):
          case ("Data-Source"):
            break;
          case ("xmlns"):
            if (!value.equalsIgnoreCase("http://pathvisio.org/GPML/2013a")
                    && !value.equalsIgnoreCase("http://genmapp.org/GPML/2010a")) {
              logger.warn("Unsupported xmlns: " + value);
            }
            break;
          case ("Name"):
            String name = entry.getRight();
            if (name.equals("")) {
              name = null;
            }
            graph.setMapName(name);
            break;
          case ("Maintainer"):
          case ("Author"): {
            for (String string : value.split(";")) {
              Author author = parseAuthor(string.trim());
              if (!graph.getAuthors().contains(author)) {
                graph.addAuthor(author);
              }
            }
            break;
          }
          case ("Email"): {
            for (String email : value.split(";")) {
              Author author = new Author("", "");
              author.setEmail(email);
              if (!graph.getAuthors().contains(author)) {
                graph.addAuthor(author);
              }
            }
            break;
          }
          case ("Last-Modified"):
            try {
              Calendar modificationDate = Calendar.getInstance();
              Date date = DATE_FORMAT.parse(entry.getRight());
              modificationDate.setTime(date);

              graph.setLastModified(modificationDate);
            } catch (ParseException e) {
              logger.warn("Invalid Last-Modified date. Expected format: " + DATE_FORMAT.toPattern() + ", but got: " + entry.getRight());
            }
            break;
          default:
            logger.warn("Unknown attribute of " + pathwayNode.getNodeName() + " node: " + entry.getLeft() + "; value: " + entry.getRight());
            break;
        }
      }

      NodeList nodes = root.item(0).getChildNodes();

      Node dimensionNode = null;
      List<Node> dataNodes = new ArrayList<Node>();
      List<Node> interactions = new ArrayList<Node>();
      List<Node> labels = new ArrayList<Node>();
      List<Node> groups = new ArrayList<Node>();
      List<Node> shapes = new ArrayList<Node>();
      List<Node> lines = new ArrayList<Node>();
      List<Node> states = new ArrayList<Node>();
      List<Element> attributes = new ArrayList<Element>();
      Node biopax = null;

      edgeLineParser = new EdgeLineParser(graph.getMapName());
      edgeParser = new EdgeParser(graph.getMapName());
      dataNodeParser = new DataNodeParser(graph.getMapName());
      labelParser = new LabelParser(graph.getMapName());
      shapeParser = new ShapeParser(graph.getMapName());
      stateParser = new StateParser(graph.getMapName());
      pointParser = new PointDataParser(graph.getMapName());

      for (int x = 0; x < nodes.getLength(); x++) {
        Node node = nodes.item(x);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          if (node.getNodeName().equalsIgnoreCase("Graphics")) {
            dimensionNode = node;
          } else if (node.getNodeName().equalsIgnoreCase("DataNode")) {
            dataNodes.add(node);
          } else if (node.getNodeName().equalsIgnoreCase("Interaction")) {
            interactions.add(node);
          } else if (node.getNodeName().equalsIgnoreCase("Label")) {
            labels.add(node);
          } else if (node.getNodeName().equalsIgnoreCase("Group")) {
            groups.add(node);
          } else if (node.getNodeName().equalsIgnoreCase("Attribute")) {
            attributes.add((Element) node);
          } else if (node.getNodeName().equalsIgnoreCase("Shape")) {
            shapes.add(node);
          } else if (node.getNodeName().equalsIgnoreCase("GraphicalLine")) {
            lines.add(node);
          } else if (node.getNodeName().equalsIgnoreCase("Comment")) {
            graph.addComment(node.getTextContent());
          } else if (node.getNodeName().equalsIgnoreCase("BiopaxRef")) {
            graph.addBiopaxReferences(node.getTextContent());
          } else if (node.getNodeName().equalsIgnoreCase("State")) {
            states.add(node);
          } else if (node.getNodeName().equalsIgnoreCase("Biopax")) {
            if (biopax != null) {
              throw new ConverterException("Biopax node should appear only once");
            }
            biopax = node;
          } else if (node.getNodeName().equalsIgnoreCase("InfoBox")) {
            // infobox can be skipped
            continue;
          } else if (node.getNodeName().equalsIgnoreCase("Legend")) {
            // legend can be skipped
            continue;
          } else {
            logger.warn("Unknown element of gpml file: " + node.getNodeName());
          }
        }
      }
      graph.addLines(parseLines(lines));
      graph.addEdges(parseEdgesFromLines(lines));

      if (dimensionNode != null) {
        setSize(dimensionNode, graph);
      }
      addGroups(groups, graph);

      resizeGroupsByIncludedLines(graph.getGroups(), lines);

      graph.addDataNodes(dataNodeParser.parseCollection(dataNodes));
      graph.addLabels(labelParser.parseCollection(labels));
      graph.addShapes(shapeParser.parseCollection(shapes));
      graph.addStates(stateParser.parseCollection(states));

      prepareEdges(interactions, graph);

      mergeEdges(graph);

      addInteractions(graph);
      if (biopax != null) {
        graph.setBiopaxData(new BiopaxParser(graph.getMapName()).parse(biopax));
      }
      Map<String, String> attributesMap = new HashMap<String, String>();
      for (final Element attribute : attributes) {
        String key = attribute.getAttribute("Key");
        String value = attribute.getAttribute("Value");
        if (attributesMap.get(key) != null) {
          logger.warn("Model xml contains duplicate attributes: " + key);
        } else {
          attributesMap.put(key, value);
        }
      }
      graph.setAttributes(attributesMap);
      return graph;
    } catch (final ParserConfigurationException e) {
      throw new ConverterException("Problem with input data", e);
    } catch (final SAXException e) {
      throw new ConverterException("Problem with input data", e);
    } catch (final InvalidXmlSchemaException e) {
      throw new ConverterException("Problem with input data", e);
    }
  }

  private Author parseAuthor(final String value) {
    String firstName = "";
    String lastName = value;
    int splitIndex = value.indexOf(" ");
    if (splitIndex > 0) {
      firstName = value.substring(0, splitIndex).trim();
      lastName = value.substring(splitIndex + 1).trim();
    }
    Author author = new Author(firstName, lastName);
    return author;
  }

  private void resizeGroupsByIncludedLines(final Collection<Group> groups, final List<Node> lines) throws UnknownTypeException {
    for (final Node node : lines) {
      String groupId = XmlParser.getNodeAttr("GroupRef", node);
      if (groupId != null) {
        for (final Group group : groups) {
          if (group.getGroupId().equals(groupId)) {
            PolylineData polyline = parseLine(node);
            if (polyline != null) {
              group.addBounds(PolylineDataFactory.getBounds(polyline));
            }
          }
        }
      }
    }
  }

  /**
   * This method merge edges that should be merged into single line.
   *
   * @param graph model where edges are stored
   */
  private void mergeEdges(final Graph graph) {
    List<Edge> toExtend = new ArrayList<>();
    List<Edge> toRemove = new ArrayList<>();
    List<Edge> toAdd = new ArrayList<>();
    Map<String, List<Edge>> extendable = new HashMap<>();
    for (final Edge edge : graph.getEdges()) {
      if (graph.getEdgeByAnchor(edge.getStart()) == edge) {
        toExtend.add(edge);
      } else if (graph.getEdgeByAnchor(edge.getEnd()) == edge) {
        toExtend.add(edge);
      } else if (graph.getEdgeByAnchor(edge.getEnd()) != null) {
        List<Edge> list = extendable.get(edge.getEnd());
        if (list == null) {
          list = new ArrayList<>();
          extendable.put(edge.getEnd(), list);
        }
        list.add(edge);
      } else if (graph.getEdgeByAnchor(edge.getStart()) != null) {
        List<Edge> list = extendable.get(edge.getStart());
        if (list == null) {
          list = new ArrayList<>();
          extendable.put(edge.getStart(), list);
        }
        list.add(edge);
      }

    }

    for (final Edge edge : toExtend) {
      String anchor = null;
      if (graph.getEdgeByAnchor(edge.getStart()) == edge) {
        anchor = edge.getStart();
      } else if (graph.getEdgeByAnchor(edge.getEnd()) == edge) {
        anchor = edge.getEnd();
      }
      List<Edge> howExtend = extendable.get(anchor);
      if (howExtend == null) {
        logger.warn(edge.getLogMarker(), " Should be connected with another element, but nothing found.");
        if (edge.getStart().equals(anchor)) {
          edge.setStart(null);
        } else if (edge.getEnd().equals(anchor)) {
          edge.setEnd(null);
        }
      } else if (howExtend.size() > 0) {
        try {
          Edge newEdge = mergeEdges(edge, howExtend.get(0));
          toRemove.add(edge);
          toRemove.add(howExtend.get(0));
          toAdd.add(newEdge);
        } catch (final UnknownMergingMethodException exception) {
          toRemove.add(edge);
          toRemove.add(howExtend.get(0));

          logger.warn(edge.getLogMarker(), exception.getMessage(), exception);
        }
      }
    }
    for (final Edge e : toRemove) {
      graph.removeEdge(e);
    }
    for (final Edge e : toAdd) {
      graph.addEdge(e);
    }
  }

  /**
   * Method that merge two {@link Edge edges}.
   *
   * @param edge1 first edge to merge
   * @param edge2 second edge to merge
   * @return new edge that merges two parameters
   * @throws UnknownMergingMethodException thrown when edges cannot be merged
   */
  private Edge mergeEdges(final Edge edge1, final Edge edge2) throws UnknownMergingMethodException {
    MergeMapping mapping = null;
    if (edge1.getStart() != null && edge1.getStart().equals(edge2.getStart())) {
      mapping = MergeMapping.getMergeMappingByInteractions(edge1.getType(), true, edge2.getType(), false);
    } else if (edge1.getStart() != null && edge1.getStart().equals(edge2.getEnd())) {
      mapping = MergeMapping.getMergeMappingByInteractions(edge1.getType(), true, edge2.getType(), true);
    } else if (edge1.getEnd() != null && edge1.getEnd().equals(edge2.getStart())) {
      mapping = MergeMapping.getMergeMappingByInteractions(edge1.getType(), false, edge2.getType(), false);
    } else if (edge1.getEnd() != null && edge1.getEnd().equals(edge2.getEnd())) {
      mapping = MergeMapping.getMergeMappingByInteractions(edge1.getType(), false, edge2.getType(), true);
    }
    if (mapping == null) {
      throw new UnknownMergingMethodException("Don't know how to merge interactions");
    }
    Edge first;
    if (mapping.isReversed1()) {
      first = edge1.reverse();
    } else {
      first = new Edge(edge1);
    }
    Edge second;
    if (mapping.isReversed2()) {
      second = edge2.reverse();
    } else {
      second = new Edge(edge2);
    }
    first.extend(second);

    if (!first.getLine().getType().equals(second.getLine().getType())) {
      logger.warn(first.getLogMarker(), "Merging edges with different line types: "
              + first.getLine().getType() + ", "
              + second.getLine().getType());
    }
    first.setType(mapping.getResultType());
    if (mapping.isResultReversed()) {
      return first.reverse();
    } else {
      return first;
    }
  }

  /**
   * Creates edges from lines when it's possible.
   *
   * @param lines xml nodes with lines
   * @return list of edges that could be created from xml nodes
   */
  private Collection<Edge> parseEdgesFromLines(final List<Node> lines) {
    List<Edge> result = new ArrayList<Edge>();
    for (final Node node : lines) {
      try {
        Edge line = edgeLineParser.parse((Element) node);
        if (line.getStart() != null && line.getEnd() != null) {
          result.add(line);
        }
      } catch (final ConverterException e) {
        logger.warn(e, e);

      }
    }
    return result;
  }

  /**
   * Creates lines from the list of gpml xml nodes.
   *
   * @param lines list of xml nodes
   * @return list of {@link PolylineData lines}
   * @throws UnknownTypeException thrown when the type of line defined in xml node is unknown
   */
  private Collection<PolylineData> parseLines(final List<Node> lines) throws UnknownTypeException {
    List<PolylineData> result = new ArrayList<>();
    for (final Node node : lines) {
      PolylineData line = parseLine(node);
      if (line != null) {
        result.add(line);
      }
    }
    return result;
  }

  private PolylineData parseLine(final Node parentNode) throws UnknownTypeException {
    int refs = 0;
    PolylineData line = new PolylineData();
    List<Point2D> points = new ArrayList<>();
    NodeList nodes = parentNode.getChildNodes();
    for (int i = 0; i < nodes.getLength(); i++) {
      Node node = nodes.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("Graphics")) {
          NodeList nodes2 = node.getChildNodes();
          for (int j = 0; j < nodes2.getLength(); j++) {
            Node node2 = nodes2.item(j);
            if (node2.getNodeType() == Node.ELEMENT_NODE) {
              Element element = (Element) node2;
              if (node2.getNodeName().equalsIgnoreCase("Point")) {
                PointData point = pointParser.parse(element);
                if (point.hasGraphRef()) {
                  refs++;
                }
                points.add(point.toPoint());
              } else {
                logger.warn("Unknown node in line: " + node2.getNodeName());
              }
            }
          }

        } else {
          logger.warn("Unknown node in line: " + node.getNodeName());
        }
      }
    }
    for (int i = 1; i < points.size(); i++) {
      line.addLine(points.get(i - 1), points.get(i));
    }
    if (refs >= 2) {
      line = null;
    }
    return line;
  }

  protected Set<Pair<String, String>> getAttributes(final Node node) {
    Set<Pair<String, String>> result = new HashSet<>();
    NamedNodeMap map = node.getAttributes();
    for (int k = 0; k < map.getLength(); k++) {
      Node attribute = map.item(k);
      String name = attribute.getNodeName();
      String value = attribute.getNodeValue();
      result.add(new Pair<String, String>(name, value));
    }
    return result;
  }

}
