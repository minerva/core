package lcsb.mapviewer.wikipathway.xml;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.PolylineDataFactory;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.model.map.species.field.AbstractRegionModification;
import lcsb.mapviewer.model.map.species.field.AbstractSiteModification;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationResidue;
import lcsb.mapviewer.wikipathway.model.GpmlLineType;
import lcsb.mapviewer.wikipathway.model.InteractionMapping;
import lcsb.mapviewer.wikipathway.model.ShapeMapping;
import lcsb.mapviewer.wikipathway.utils.Geo;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.Color;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class contains methods for ModelToGPML conversion.
 *
 * @author Jan Badura
 */
public class ModelToGPML {

  /**
   * Maximum distance between point and line. ???
   */
  private static final double DIS_FOR_LINE = 3.0;

  /**
   * This value define margin of the group border. Rectangle border will be
   * resized by this value in every direction.
   */
  private static final int GROUP_RECTANGLE_BORDER_MARGIN = 8;

  /**
   * First id value used for generating identifiers during conversion.
   */
  private static final int INITIAL_ID_VALUE = 10000;

  /**
   * Maximum distance between point and rectangle. ???
   */
  private static final double DIS_FOR_REP = 9.0;

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  /**
   * Counter used for generating identifiers if identifiers aren't provided by the
   * GPML model.
   */
  private int idCounter = INITIAL_ID_VALUE;

  /**
   * Parser used to convert from/to xml {@link MiriamData} annotations.
   */
  private final ReferenceParser referenceParser;

  /**
   * Parser used to convert from/to xml (in BioPAX format) {@link MiriamData}
   * annotations.
   */
  private final BiopaxParser biopaxParser;

  private final ColorParser colorParser = new ColorParser();

  public ModelToGPML(final String mapName) {
    referenceParser = new ReferenceParser(mapName);
    biopaxParser = new BiopaxParser(mapName);
  }

  /**
   * Returns new unique identifier for the model.
   *
   * @return new unique identifier for the model
   */
  private String getNewId() {
    idCounter++;
    return "id" + idCounter;
  }

  /**
   * This function returns GPML Species type based on class. If it can not
   * recognize one of (Rna,GeneProduct,Protein,Metabolite) it will throw
   * {@link InvalidArgumentException}.
   *
   * @param species species for which GPML type will be returned
   * @return String - type
   */
  private String getType(final Species species) {
    String res = "";
    if (species instanceof AntisenseRna) {
      res = "Rna";
    } else if (species instanceof Rna) {
      res = "Rna";
    } else if (species instanceof Gene) {
      res = "GeneProduct";
    } else if (species instanceof Protein) {
      res = "Protein";
    } else if (species instanceof SimpleMolecule) {
      res = "Metabolite";
    } else if (species instanceof Ion) {
      res = "Metabolite";
    } else if (species instanceof Drug) {
      res = "Metabolite";
    } else if (species instanceof Phenotype) {
      res = "Pathway";
    } else if (species instanceof Unknown) {
      res = null;
    } else if (species instanceof Degraded) {
      res = "Unknown";
    } else {
      throw new InvalidArgumentException("Unknown class type: " + species.getClass());
    }

    return res;
  }

  /**
   * Since in pathvisio it is impossible to set the size of group rectangle this
   * function calculates new rectangle for complex based on contents of this
   * complex.
   *
   * @param alias alias for which rectangle border is calculated
   * @return rectangle border for alias
   */
  Shape getShape(final Element alias) {

    if (alias instanceof Complex) {
      Shape res = null;
      Complex ca = (Complex) alias;
      if (ca.getElements().size() == 0) {
        res = (Rectangle2D) ca.getBorder().clone();
      } else {
        res = getShape(ca.getElements().get(0));
        for (final Element al : ca.getElements()) {
          res = res.getBounds2D().createUnion(getShape(al).getBounds2D());
        }
        Rectangle2D bounds = res.getBounds2D();
        res = new Rectangle2D.Double(
            bounds.getX() - GROUP_RECTANGLE_BORDER_MARGIN,
            bounds.getY() - GROUP_RECTANGLE_BORDER_MARGIN,
            bounds.getWidth() + 2 * GROUP_RECTANGLE_BORDER_MARGIN,
            bounds.getHeight() + 2 * GROUP_RECTANGLE_BORDER_MARGIN);
      }
      return res;
    } else {
      switch (ShapeMapping.getShape(alias.getClass())) {
        case DEGRADATION:
        case OVAL:
          return new Ellipse2D.Double(alias.getX(), alias.getY(), alias.getWidth(), alias.getHeight());
        case RECTANGLE:
          return (Rectangle2D) alias.getBorder().clone();
        case HEXAGON:
          Polygon polygon = new Polygon();

          Point2D topLeft = new Point2D.Double(alias.getX(), alias.getY());
          Point2D topRight = new Point2D.Double(alias.getX() + alias.getWidth(), alias.getY());
          Point2D bottomRight = new Point2D.Double(alias.getX() + alias.getWidth(), alias.getY() + alias.getHeight());
          Point2D bottomLeft = new Point2D.Double(alias.getX(), alias.getY() + alias.getHeight());

          PointTransformation pt = new PointTransformation();
          Point2D p1 = pt.getPointOnLine(topLeft, topRight, 0.25);
          Point2D p2 = pt.getPointOnLine(topLeft, topRight, 0.75);
          Point2D p3 = pt.getPointOnLine(topRight, bottomRight, 0.5);
          Point2D p4 = pt.getPointOnLine(bottomRight, bottomLeft, 0.25);
          Point2D p5 = pt.getPointOnLine(bottomRight, bottomLeft, 0.75);
          Point2D p6 = pt.getPointOnLine(bottomLeft, topLeft, 0.5);

          polygon.addPoint((int) p1.getX(), (int) p1.getY());
          polygon.addPoint((int) p2.getX(), (int) p2.getY());
          polygon.addPoint((int) p3.getX(), (int) p3.getY());
          polygon.addPoint((int) p4.getX(), (int) p4.getY());
          polygon.addPoint((int) p5.getX(), (int) p5.getY());
          polygon.addPoint((int) p6.getX(), (int) p6.getY());

          return polygon;
        default:
          throw new InvalidArgumentException("Unknown shape: " + ShapeMapping.getShape(alias.getClass()));
      }
    }
  }

  /**
   * This function calculates {@link PolylineData} based on start {@link Reactant}
   * and end {@link Product}. If end or start points are to far away from their
   * rectangles new points are added.
   *
   * @param start reactant from which polyline starts
   * @param end   product where polyline ends
   * @return {@link PolylineData} between start and end
   */
  private PolylineData getPolyline(final Reactant start, final Product end) {
    PolylineData res = new PolylineData();

    NodeOperator and = start.getNodeOperatorForInput();
    NodeOperator split = end.getNodeOperatorForOutput();

    PolylineData startLine = start.getLine();
    Point2D ps = startLine.getStartPoint();
    Shape rec1 = getShape(start.getElement());
    double dis1 = Geo.distance(ps, rec1);

    res.addLines(startLine.getLines());
    if (dis1 > DIS_FOR_REP) {
      res.setStartPoint(Geo.closestPoint(rec1, ps));
    }
    if (and != null) {
      res.addLines(and.getLine().getLines());
    }
    if (split != null) {
      res.addLines(split.getLine().reverse().getLines());
    }

    PolylineData endLine = end.getLine();
    Point2D pe = endLine.getEndPoint();
    Shape rec2 = getShape(end.getElement());
    double dis2 = Geo.distance(pe, rec2);

    res.addLines(endLine.getLines());
    if (dis2 > DIS_FOR_REP) {
      res.setEndPoint(Geo.closestPoint(rec2, pe));
    }
    return PolylineDataFactory.removeCollinearPoints(res);
  }

  /**
   * This function calculates PolylineData based on ReactionNode.
   *
   * @param rn       reaction node for which polyline is calculated
   * @param mainLine main line to which polyline is attached
   * @return polyline for {@link ReactionNode}
   */
  PolylineData getPolyline(final ReactionNode rn, final PolylineData mainLine) {
    PolylineData res = new PolylineData();

    PolylineData line = rn.getLine();
    Point2D ps = line.getStartPoint();
    Point2D pe = line.getEndPoint();
    Shape rec = getShape(rn.getElement());

    if (rn instanceof Reactant || rn instanceof Modifier) {
      double dis = Geo.distance(ps, rec);
      double dis2 = Geo.distanceFromPolyline(pe, mainLine);
      res.addLines(line.getLines());
      if (dis > DIS_FOR_REP) {
        res.setStartPoint(Geo.closestPoint(rec, ps));
      }
      if (dis2 > DIS_FOR_LINE) {
        Point2D tmpPoint = Geo.closestPointOnPolyline(mainLine, pe);
        if (tmpPoint != null) {
          res.setEndPoint(tmpPoint);
        }
      }
    } else if (rn instanceof Product) {
      double dis = Geo.distance(pe, rec);
      double dis2 = Geo.distanceFromPolyline(ps, mainLine);
      if (dis2 > DIS_FOR_LINE) {
        Point2D tmpPoint = Geo.closestPointOnPolyline(mainLine, ps);
        if (tmpPoint != null) {
          res.addLine(tmpPoint, line.getStartPoint());
        }
      }
      res.addLines(line.getLines());
      if (dis > DIS_FOR_REP) {
        res.setEndPoint(Geo.closestPoint(rec, pe));
      }
    }
    return PolylineDataFactory.removeCollinearPoints(res);
  }

  /**
   * This function transform Compartments into Shapes (final Oval or Rectangle)
   * from PathVisio.
   *
   * @param model model where compartments are placed
   * @return String that encodes all compartments in GPML format
   */
  private String getComparments(final Model model) {
    StringBuilder result = new StringBuilder();
    for (final Compartment compartment : model.getCompartments()) {
      result.append(compartmentToXml(compartment));
    }
    return result.toString();
  }

  String compartmentToXml(final Compartment ca) {
    StringBuilder comparments = new StringBuilder();
    double x = ca.getCenterX();
    double y = ca.getCenterY();
    double h = ca.getHeight();
    double w = ca.getWidth();
    String shape = ShapeMapping.getShape(ca.getClass()).getStringRepresentation();

    comparments.append(
        "  <Shape TextLabel=\"" + XmlParser.escapeXml(ca.getName()) + "\" GraphId=\"" + ca.getElementId() + "\">\n");
    comparments.append("    <Attribute Key=\"org.pathvisio.CellularComponentProperty\" Value=\"Cell\" />\n");
    comparments.append("    <Attribute Key=\"org.pathvisio.DoubleLineProperty\" Value=\"Double\" />\n");
    comparments.append(
        "    <Graphics CenterX=\"" + x + "\" "
            + "CenterY=\"" + y + "\" "
            + "Width=\"" + w + "\" "
            + "Height=\"" + h + "\" "
            + "ZOrder=\"" + ca.getZ() + "\" "
            + "FontSize=\"" + ca.getFontSize().intValue() + "\" "
            + "Valign=\"Bottom\" "
            + "ShapeType=\"" + shape + "\" "
            + "LineThickness=\"3.0\" "
            + "Color=\"" + colorToString(ca.getFontColor()) + "\" "
            + "FillColor=\"" + colorToString(ca.getFillColor()) + "\" "
            + "Rotation=\"0.0\" />\n");
    comparments.append("  </Shape>\n");
    return comparments.toString();
  }

  /**
   * This function creates Interaction for other product, reactant or modifier
   * from main reaction. This is support function for getInteractions(Model
   * model).
   *
   * @param rn       object representing reactant/product/modifier
   * @param anchId   identifier of the anchor where it will be connected
   * @param anchors  string builder where anchors are stored (it will be modified to add
   *                 anchor for this element)
   * @param mainLine line used for connecting this element
   * @return string representing connection of this element to reaction
   * @throws ConverterException thrown when there is a problem with conversion
   */
  private String getInteractionForAnchor(final ReactionNode rn, final String anchId, final StringBuilder anchors, final PolylineData mainLine)
      throws ConverterException {
    StringBuilder interaction = new StringBuilder();

    PolylineData line = getPolyline(rn, mainLine);
    Point2D ps = line.getStartPoint();
    Point2D pe = line.getEndPoint();
    double anchorPosition = -1;

    String lineStyle = GpmlLineType.getByLineType(rn.getLine().getType()).getGpmlString();

    interaction.append("  <Interaction GraphId=\"" + getNewId() + "\">\n");
    interaction.append("    <Attribute Key=\"org.pathvisio.core.ds\" Value=\"\"/>\n");
    interaction.append("    <Attribute Key=\"org.pathvisio.core.id\" Value=\"\"/>\n");
    interaction.append("    <Graphics "
        + "ConnectorType=\"Segmented\" "
        + "ZOrder=\"" + rn.getReaction().getZ() + "\" "
        + "Color=\"" + colorToString(rn.getLine().getColor()) + "\" ");
    if (lineStyle != null) {
      interaction.append("LineStyle=\"" + lineStyle + "\" ");
    }

    interaction.append("LineThickness=\"" + rn.getLine().getWidth() + "\">\n");

    String reactionArrowHead = InteractionMapping
        .getGpmlInteractionTypeForMinervaReactionClass(rn.getReaction().getClass())
        .getGpmlString();
    if (rn instanceof Reactant) {
      interaction.append("      <Point "
          + "X=\"" + ps.getX() + "\" "
          + "Y=\"" + ps.getY() + "\" ");

      if (rn.getReaction().isReversible()) {
        interaction.append("ArrowHead=\"" + reactionArrowHead + "\" ");
      }

      interaction.append("GraphRef=\"" + rn.getElement().getElementId() + "\"/>\n");
      for (final Line2D p2d : line.getLines()) {
        if (p2d.getP2().equals(pe)) {
          interaction
              .append("      <Point X=\"" + pe.getX() + "\" "
                  + "Y=\"" + pe.getY() + "\" "
                  + "GraphRef=\"" + anchId + "\"/>\n");
        } else {
          interaction.append("      <Point X=\"" + p2d.getX2() + "\" Y=\"" + p2d.getY2() + "\"/>\n");
        }
      }
    } else if (rn instanceof Product) {
      interaction
          .append("      <Point X=\"" + ps.getX() + "\" Y=\"" + ps.getY() + "\" GraphRef=\"" + anchId + "\"/>\n");
      for (final Line2D p2d : line.getLines()) {
        if (p2d.getP2().equals(pe)) {
          interaction.append(
              "      <Point X=\"" + pe.getX() + "\" "
                  + "Y=\"" + pe.getY() + "\" "
                  + "GraphRef=\"" + rn.getElement().getElementId() + "\" "
                  + "ArrowHead=\"" + reactionArrowHead + "\"/>\n");
        } else {
          interaction.append("      <Point X=\"" + p2d.getX2() + "\" Y=\"" + p2d.getY2() + "\"/>\n");
        }
      }
    } else if (rn instanceof Modifier) {
      Modifier modifier = (Modifier) rn;
      interaction.append("      <Point X=\"" + ps.getX() + "\" Y=\"" + ps.getY() + "\" GraphRef=\""
          + rn.getElement().getElementId() + "\"/>\n");
      for (final Line2D p2d : line.getLines()) {
        if (p2d.getP2().equals(pe)) {
          interaction.append("      <Point X=\"" + pe.getX() + "\" "
              + "Y=\"" + pe.getY() + "\" "
              + "GraphRef=\"" + anchId + "\" "
              + "ArrowHead=\""
              + InteractionMapping.getGpmlInteractionTypeForMinervaModifierClass(modifier.getClass()).getGpmlString()
              + "\"/>\n");
        } else {
          interaction.append("      <Point X=\"" + p2d.getX2() + "\" Y=\"" + p2d.getY2() + "\"/>\n");
        }
      }
    }
    if (rn instanceof Reactant) {
      anchorPosition = Geo.distanceOnPolyline(mainLine, pe);
    } else if (rn instanceof Product) {
      anchorPosition = Geo.distanceOnPolyline(mainLine, ps);
    } else if (rn instanceof Modifier) {
      anchorPosition = Geo.distanceOnPolyline(mainLine, pe);
    } else {
      throw new InvalidArgumentException("Invalid class type: " + rn.getClass());
    }
    anchors.append("      <Anchor Position=\"" + anchorPosition + "\" Shape=\"None\" GraphId=\"" + anchId + "\"/>\n");
    interaction.append("    </Graphics>\n");
    interaction.append(referenceParser.toXml((MiriamData) null, null));
    interaction.append("  </Interaction>\n");

    return interaction.toString();
  }

  /**
   * This function encode Species into DataNodes from GPML format. Empty complexes
   * are also transformed into DataNodes.
   *
   * @param model model where aliases are placed
   * @return string representing {@link lcsb.mapviewer.wikipathway.model.DataNode
   * data nodes}
   * @throws ConverterException thrown when there is a problem with conversion
   */
  protected String getDataNodes(final Model model) throws ConverterException {
    StringBuilder dataNodes = new StringBuilder();

    for (final Species species : model.getNotComplexSpeciesList()) {
      if (!(species instanceof Complex) && !(species instanceof Degraded)) {
        dataNodes.append(speciesToDataNode(species));
      }
    }

    // Special Case for empty Complexes
    for (final Complex ca : model.getComplexList()) {
      if (ca.getElements().size() == 0) {
        dataNodes.append(
            "  <DataNode TextLabel=\"" + XmlParser.escapeXml(ca.getName()) + "\" GraphId=\"" + ca.getElementId()
                + "\" Type=\"Complex\"");
        if (ca.getComplex() != null) {
          dataNodes.append(" GroupRef=\"" + ca.getComplex().getElementId() + "\"");
        }
        dataNodes.append(">\n");

        dataNodes.append(biopaxParser.toReferenceXml(ca.getMiriamData()));
        Rectangle2D rec = ca.getBorder();

        dataNodes.append(
            "    <Graphics CenterX=\"" + rec.getCenterX() + "\" "
                + "CenterY=\"" + rec.getCenterY() + "\" "
                + "Width=\"" + rec.getWidth() + "\" "
                + "Height=\"" + rec.getHeight() + "\" "
                + "ZOrder=\"" + ca.getZ() + "\" "
                + "FontSize=\"" + ca.getFontSize().intValue() + "\" "
                + "Valign=\"Middle\" "
                + "Color=\"" + colorToString(ca.getFontColor()) + "\" "
                + "FillColor=\"" + colorToString(ca.getFillColor()) + "\" "
                + "ShapeType=\"" + ShapeMapping.getShape(ca.getClass()).getStringRepresentation() + "\" "
                + "LineStyle=\"Broken\"/>\n");

        dataNodes
            .append(referenceParser.toXml(ca.getMiriamData(), new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, ca)));
        dataNodes.append("  </DataNode>\n");
      }
    }

    return dataNodes.toString();
  }

  String speciesToShape(final Species species) throws ConverterException {
    String shape = ShapeMapping.getShape(species.getClass()).getStringRepresentation();
    StringBuilder result = new StringBuilder();
    result.append(
        "  <Shape GraphId=\"" + species.getElementId() + "\"");
    if (species.getComplex() != null) {
      result.append(" GroupRef=\"" + species.getComplex().getElementId() + "\"");
    }
    result.append(">\n");

    result.append("    <Comment>" + StringEscapeUtils.escapeXml11(species.getNotes().trim()) + "</Comment>\n");

    Rectangle2D rec = species.getBorder();

    result.append(
        "    <Graphics CenterX=\"" + rec.getCenterX() + "\" "
            + "CenterY=\"" + rec.getCenterY() + "\" "
            + "Width=\"" + rec.getWidth() + "\" "
            + "Height=\"" + rec.getHeight() + "\" "
            + "ZOrder=\"" + species.getZ() + "\" "
            + "FontSize=\"" + species.getFontSize().intValue() + "\" "
            + "Valign=\"Middle\" "
            + "Color=\"" + colorToString(species.getFontColor()) + "\" "
            + "ShapeType=\"" + shape + "\" "
            + "FillColor=\"" + colorToString(species.getFillColor()) + "\"/>\n");

    if (species.getMiriamData().size() > 0) {
      logger.warn(new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, species), "Annotations cannot be exported to gpml");
    }
    result.append("  </Shape>\n");
    return result.toString();
  }

  String speciesToDataNode(final Species species) throws ConverterException {
    StringBuilder result = new StringBuilder();
    String lineStyle = null;
    if (species.isHypothetical()) {
      lineStyle = GpmlLineType.DASHED.getGpmlString();
    }

    String nodeType = getType(species);

    result.append(
        "  <DataNode TextLabel=\"" + XmlParser.escapeXml(species.getName()) + "\" "
            + "GraphId=\"" + species.getElementId() + "\"");
    if (nodeType != null) {
      result.append(" Type=\"" + nodeType + "\"");
    }
    if (species.getComplex() != null) {
      result.append(" GroupRef=\"" + species.getComplex().getElementId() + "\"");
    }
    result.append(">\n");

    result.append("    <Comment>" + StringEscapeUtils.escapeXml11(species.getNotes().trim()) + "</Comment>\n");
    result.append(biopaxParser.toReferenceXml(species.getMiriamData()));

    Rectangle2D rec = species.getBorder();

    result.append(
        "    <Graphics CenterX=\"" + rec.getCenterX() + "\" "
            + "CenterY=\"" + rec.getCenterY() + "\" "
            + "Width=\"" + rec.getWidth() + "\" "
            + "Height=\"" + rec.getHeight() + "\" "
            + "ZOrder=\"" + species.getZ() + "\" "
            + "FontSize=\"" + species.getFontSize().intValue() + "\" "
            + "Valign=\"Middle\" "
            + "Color=\"" + colorToString(species.getFontColor()) + "\" "
            + "ShapeType=\"" + ShapeMapping.getShape(species.getClass()).getStringRepresentation() + "\" ");
    if (lineStyle != null) {
      result.append("LineStyle=\"" + lineStyle + "\" ");

    }
    result.append("FillColor=\"" + colorToString(species.getFillColor()) + "\"/>\n");

    result.append(
        referenceParser.toXml(species.getMiriamData(), new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, species)));
    result.append("  </DataNode>\n");

    return result.toString();
  }

  private String modifictionResidueToState(final ModificationResidue mr) throws ConverterException {
    StringBuilder result = new StringBuilder();
    result.append("<State "
        + "GraphRef=\"" + mr.getSpecies().getElementId() + "\" "
        + "GraphId=\"" + mr.getSpecies().getElementId() + "-" + mr.getIdModificationResidue() + "\" ");
    String stateString = "";
    if (mr instanceof AbstractSiteModification) {
      ModificationState state = ((AbstractSiteModification) mr).getState();
      if (state != null) {
        stateString = state.getAbbreviation();
      }
    } else if (mr instanceof AbstractRegionModification) {
      if (mr.getName() != null) {
        stateString = mr.getName();
      }
    }
    result.append("TextLabel=\"" + XmlParser.escapeXml(stateString) + "\" ");
    result.append(">");
    double relX = ((mr.getCenter().getX() - mr.getSpecies().getX()) / mr.getSpecies().getWidth() - 0.5) * 2;
    double relY = ((mr.getCenter().getY() - mr.getSpecies().getY()) / mr.getSpecies().getHeight() - 0.5) * 2;

    String shape = ShapeMapping.getShape(mr.getClass()).getStringRepresentation();
    result.append("<Graphics "
        + "RelX=\"" + relX + "\" "
        + "RelY=\"" + relY + "\" "
        + "Width=\"" + mr.getWidth() + "\" "
        + "Height=\"" + mr.getHeight() + "\" "
        + "ShapeType=\"" + shape + "\" "
        + "/>\n");
    result.append(
        referenceParser.toXml((MiriamData) null, new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, mr.getSpecies())));
    result.append("</State>\n");
    return result.toString();
  }

  /**
   * This function encode ComplexAliases into Groups from GPML format.
   *
   * @param model model where aliases are placed
   * @return string representing groups in GPML format
   */
  protected String getGroups(final Model model) {
    StringBuilder groups = new StringBuilder();

    for (final Complex complex : model.getComplexList()) {
      if (complex.getElements().size() > 0) {
        groups.append("  <Group"
            + " GroupId=\"" + complex.getElementId() + "\""
            + " GraphId=\"" + complex.getElementId() + "\""
            + " TextLabel=\"" + XmlParser.escapeXml(complex.getName()) + "\"");
        if (complex.getComplex() != null) {
          groups.append(" GroupRef=\"" + complex.getComplex().getElementId() + "\"");
        }
        if (!complex.isHypothetical()) {
          groups.append(" Style=\"Complex\"");
        }
        groups.append("/>\n");
      }
    }
    return groups.toString();
  }

  /**
   * This function encode Reactions into Interactions from GPML format.
   *
   * @param model model where reactions are placed
   * @return string with interactions in GPML format
   * @throws ConverterException thrown when there is a problem with conversion
   */
  protected String getInteractions(final Model model) throws ConverterException {
    StringBuilder interactions = new StringBuilder();

    for (final Reaction reaction : model.getReactions()) {
      String reactionArrowHead = InteractionMapping.getGpmlInteractionTypeForMinervaReactionClass(reaction.getClass())
          .getGpmlString();

      String lineStyle = GpmlLineType.getByLineType(reaction.getLine().getType()).getGpmlString();

      StringBuilder anchors = new StringBuilder();
      StringBuilder tmp = new StringBuilder();

      interactions.append("  <Interaction GraphId=\"" + reaction.getIdReaction() + "\">\n");
      if (reaction.getNotes() != null) {
        interactions.append("  <Comment>" + StringEscapeUtils.escapeXml11(reaction.getNotes().trim()) + "</Comment>\n");
      }
      interactions.append(biopaxParser.toReferenceXml(reaction.getMiriamData()));
      interactions.append("    <Graphics "
          + "ConnectorType=\"Segmented\" "
          + "ZOrder=\"" + reaction.getZ() + "\" "
          + "Color=\"" + colorToString(reaction.getLine().getColor()) + "\" ");
      if (lineStyle != null) {
        interactions.append("LineStyle=\"" + lineStyle + "\" ");
      }
      interactions.append("LineThickness=\"" + reaction.getLine().getWidth() + "\">\n");

      Reactant start = reaction.getReactants().get(0);
      Product end = reaction.getProducts().get(0);

      PolylineData line = getPolyline(start, end);
      Point2D ps = line.getStartPoint();
      Point2D pe = line.getEndPoint();
      String sid = start.getElement().getElementId();
      String eid = end.getElement().getElementId();

      interactions.append("      <Point "
          + "X=\"" + ps.getX() + "\" "
          + "Y=\"" + ps.getY() + "\" ");

      if (reaction.isReversible()) {
        interactions.append("ArrowHead=\"" + reactionArrowHead + "\" ");
      }

      interactions.append("GraphRef=\"" + sid + "\"/>\n");
      for (final Line2D p2d : line.getLines()) {
        if (p2d.getP2().equals(pe)) {
          interactions.append("      <Point X=\"" + pe.getX() + "\" Y=\"" + pe.getY() + "\" GraphRef=\"" + eid
              + "\" ArrowHead=\"" + reactionArrowHead + "\"/>\n");
        } else {
          interactions.append("      <Point X=\"" + p2d.getX2() + "\" Y=\"" + p2d.getY2() + "\"/>\n");
        }
      }

      boolean first = true;
      for (final Reactant reactant : reaction.getReactants()) {
        if (first) {
          first = false;
        } else {
          tmp.append(getInteractionForAnchor(reactant, getNewId(), anchors, line));
        }
      }

      first = true;
      for (final Product product : reaction.getProducts()) {
        if (first) {
          first = false;
        } else {
          tmp.append(getInteractionForAnchor(product, getNewId(), anchors, line));
        }
      }

      for (final Modifier modifier : reaction.getModifiers()) {
        tmp.append(getInteractionForAnchor(modifier, getNewId(), anchors, line));
      }

      interactions.append(anchors);
      interactions.append("    </Graphics>\n");
      interactions.append(
          referenceParser.toXml(reaction.getMiriamData(), new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, reaction)));
      interactions.append("  </Interaction>\n");
      interactions.append(tmp);
    }

    return interactions.toString();
  }

  /**
   * This function returns Model in GPML format.
   *
   * @param model model to transform
   * @return string in GPML format representing model
   * @throws ConverterException thrown when there is a problem with conversion
   */
  public String getGPML(final Model model) throws ConverterException {
    StringBuilder gpml = new StringBuilder();
    gpml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    gpml.append("<Pathway xmlns=\"http://pathvisio.org/GPML/2013a\"");
    if (model.getName() != null) {
      gpml.append(" Name=\"" + StringEscapeUtils.escapeXml11(model.getName()) + "\" ");
    }
    if (model.getAuthors().size() > 0) {
      List<String> authors = new ArrayList<>();
      List<String> emails = new ArrayList<>();
      for (Author author : model.getAuthors()) {
        if ((author.getFirstName() != null && author.getFirstName().length() > 0)
            || (author.getLastName() != null && author.getLastName().length() > 0)) {
          authors.add((author.getFirstName() + " " + author.getLastName()).trim());
        }
        if (author.getEmail() != null && author.getEmail().length() > 0) {
          emails.add(author.getEmail().trim());
        }
      }
      if (authors.size() > 0) {
        gpml.append(" Author=\"" + StringEscapeUtils.escapeXml11(StringUtils.join(authors, "; ")) + "\"");
      }
      if (emails.size() > 0) {
        gpml.append(" Email=\"" + StringEscapeUtils.escapeXml11(StringUtils.join(emails, "; ")) + "\"");
      }
    }
    if (model.getModificationDates().size() > 0) {
      Calendar date = model.getModificationDates().get(model.getModificationDates().size() - 1);
      gpml.append(" Last-Modified=\"" + GpmlParser.DATE_FORMAT.format(date.getTime()) + "\"");
    }
    gpml.append(">\n");
    if (model.getNotes() != null) {
      gpml.append("  <Comment>" + StringEscapeUtils.escapeXml11(model.getNotes().trim()) + "</Comment>\n");
    }
    gpml.append("  <Graphics BoardWidth=\"" + model.getWidth() + "\" BoardHeight=\"" + model.getHeight() + "\"/>\n");

    gpml.append(getDataNodes(model));
    gpml.append(getStates(model));
    String groups = getGroups(model);
    gpml.append(getInteractions(model));
    gpml.append(getShapes(model));
    gpml.append(groups);
    Set<MiriamData> set = new HashSet<>();
    for (final BioEntity element : model.getBioEntities()) {
      set.addAll(element.getMiriamData());
    }

    // the order here is important...
    gpml.append("  <InfoBox CenterX=\"0.0\" CenterY=\"0.0\"/>\n");
    gpml.append(biopaxParser.toXml(set));

    gpml.append("</Pathway>");

    return gpml.toString();
  }

  private String getStates(final Model model) throws ConverterException {
    StringBuilder result = new StringBuilder();
    for (final Element element : model.getSortedElements()) {
      if (element instanceof SpeciesWithModificationResidue) {
        for (final ModificationResidue mr : ((SpeciesWithModificationResidue) element).getModificationResidues()) {
          result.append(modifictionResidueToState(mr));
        }
      }
    }
    return result.toString();
  }

  private String colorToString(final Color color) {
    return colorParser.colorToHtml(color).substring(1);
  }

  protected String getShapes(final Model model) throws ConverterException {
    StringBuilder result = new StringBuilder();
    result.append(getComparments(model));

    for (final Species species : model.getNotComplexSpeciesList()) {
      if (species instanceof Degraded) {
        result.append(speciesToShape(species));
      }
    }

    return result.toString();
  }

}
