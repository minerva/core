package lcsb.mapviewer.wikipathway.xml;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.geometry.LineTransformation;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.converter.model.celldesigner.geometry.ReactionCellDesignerConverter;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.PolylineDataFactory;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.wikipathway.model.Edge;
import lcsb.mapviewer.wikipathway.model.Interaction;
import lcsb.mapviewer.wikipathway.utils.Geo;

class ReactionLayoutFinder {

  private Logger logger = LogManager.getLogger();

  private LineTransformation lt = new LineTransformation();
  private PointTransformation pt = new PointTransformation();

  private Interaction interaction;

  public static enum LineLocation {
    PRODUCT,
    MODIFIER,
    REACTANT,
    INPUT_OPERATOR,
    OUTPUT_OPERATOR
  }

  /**
   * Return coordinates where all {@link Reactant} should end, all
   * {@link Modifier} should end, and all {@link Product} should start.
   * 
   * @param interaction
   *          GPML {@link Interaction}
   * @return map consisted of three entries for classes: {@link Product},
   *         {@link Reactant}, {@link Modifier}
   */
  public Map<Class<?>, Point2D> getNodeStartPoints(final Interaction interaction) {
    this.interaction = interaction;
    List<Pair<Class<?>, Point2D>> possiblePoints = getPointsInOrder(interaction);

    Point2D modifierPoint = getModifierPoint(possiblePoints);
    Point2D reactantPoint = getReactantPoint(possiblePoints, modifierPoint);
    Point2D productPoint = getProductPoint(possiblePoints, modifierPoint);

    Map<Class<?>, Point2D> result = new HashMap<>();
    result.put(Reactant.class, reactantPoint);
    result.put(Product.class, productPoint);
    result.put(Modifier.class, modifierPoint);

    return result;
  }

  public Map<LineLocation, PolylineData> getNodeLines(final Interaction interaction) {
    Map<Class<?>, Point2D> points = getNodeStartPoints(interaction);

    Map<LineLocation, PolylineData> result = new HashMap<>();
    PolylineData reactantLine = getSubline(interaction.getLine(), interaction.getLine().getStartPoint(),
        points.get(Reactant.class));
    PolylineData inputReactionLine = getSubline(interaction.getLine(), points.get(Reactant.class),
        points.get(Modifier.class));
    inputReactionLine.trimEnd(ReactionCellDesignerConverter.RECT_SIZE / 2);

    PolylineData outputReactionLine = getSubline(interaction.getLine(),
        points.get(Modifier.class), points.get(Product.class));

    outputReactionLine.trimBegin(ReactionCellDesignerConverter.RECT_SIZE / 2);
    PolylineData productLine = getSubline(interaction.getLine(),
        points.get(Product.class), interaction.getLine().getEndPoint());
    PolylineData modifierLine = new PolylineData(pt.copyPoint(inputReactionLine.getEndPoint()),
        pt.copyPoint(outputReactionLine.getStartPoint()));
    modifierLine.setColor(interaction.getColor());
    modifierLine.setWidth(productLine.getWidth());

    result.put(LineLocation.REACTANT, reactantLine);
    result.put(LineLocation.PRODUCT, productLine);
    result.put(LineLocation.MODIFIER, modifierLine);
    result.put(LineLocation.INPUT_OPERATOR, inputReactionLine);
    result.put(LineLocation.OUTPUT_OPERATOR, outputReactionLine);

    return result;
  }

  private List<Pair<Class<?>, Point2D>> getPointsInOrder(final Interaction interaction) {
    PolylineData pd = interaction.getLine();
    List<Pair<Class<?>, Point2D>> possiblePoints = new ArrayList<>();
    for (final Edge e : interaction.getReactants()) {
      if (pointOnPolyline(pd, e.getLine().getEndPoint())) {
        possiblePoints.add(new Pair<>(Reactant.class, e.getLine().getEndPoint()));
      } else if (pointOnPolyline(pd, e.getLine().getStartPoint())) {
        possiblePoints.add(new Pair<>(Reactant.class, e.getLine().getStartPoint()));
      }
    }
    for (final Edge e : interaction.getProducts()) {
      if (pointOnPolyline(pd, e.getLine().getStartPoint())) {
        possiblePoints.add(new Pair<>(Product.class, e.getLine().getStartPoint()));
      } else if (pointOnPolyline(pd, e.getLine().getEndPoint())) {
        possiblePoints.add(new Pair<>(Product.class, e.getLine().getEndPoint()));
        logger.warn("Product line is reversed");
        e.setLine(e.getLine().reverse());
      }
    }
    for (final Edge e : interaction.getModifiers()) {
      if (pointOnPolyline(pd, e.getLine().getEndPoint())) {
        possiblePoints.add(new Pair<>(Modifier.class, e.getLine().getEndPoint()));
      } else if (pointOnPolyline(pd, e.getLine().getStartPoint())) {
        possiblePoints.add(new Pair<>(Modifier.class, e.getLine().getStartPoint()));
      }
    }
    Comparator<Pair<Class<?>, Point2D>> comparator = new Comparator<Pair<Class<?>, Point2D>>() {

      @Override
      public int compare(final Pair<Class<?>, Point2D> o1, final Pair<Class<?>, Point2D> o2) {
        double dist1 = distanceFromPolylineStart(pd, o1.getRight());
        double dist2 = distanceFromPolylineStart(pd, o2.getRight());
        return new DoubleComparator(Configuration.EPSILON).compare(dist1, dist2);
      }
    };

    Collections.sort(possiblePoints, comparator);
    return possiblePoints;
  }

  private Point2D getProductPoint(final List<Pair<Class<?>, Point2D>> possiblePoints, final Point2D modifierPoint) {
    Point2D result = null;
    for (Pair<Class<?>, Point2D> pair : possiblePoints) {
      if (pair.getLeft().equals(Product.class)) {
        if (distanceFromPolylineStart(interaction.getLine(),
            modifierPoint) < distanceFromPolylineStart(interaction.getLine(), pair.getRight())) {
          result = pair.getRight();
          break;
        }
      }
    }
    if (result == null) {
      for (final Line2D line : interaction.getLine().getLines()) {
        if (distanceFromPolylineStart(interaction.getLine(),
            modifierPoint) < distanceFromPolylineStart(interaction.getLine(), line.getP1())) {
          double x = modifierPoint.getX() + (line.getX1() - modifierPoint.getX()) / 2;
          double y = modifierPoint.getY() + (line.getY1() - modifierPoint.getY()) / 2;
          result = new Point2D.Double(x, y);
          break;
        }
        if (distanceFromPolylineStart(interaction.getLine(),
            modifierPoint) < distanceFromPolylineStart(interaction.getLine(), line.getP2())) {
          double x = modifierPoint.getX() + (line.getX2() - modifierPoint.getX()) / 2;
          double y = modifierPoint.getY() + (line.getY2() - modifierPoint.getY()) / 2;
          result = new Point2D.Double(x, y);
          break;
        }
      }
    }
    return result;
  }

  private Point2D getReactantPoint(final List<Pair<Class<?>, Point2D>> possiblePoints, final Point2D modifierPoint) {
    Point2D result = null;
    for (Pair<Class<?>, Point2D> pair : possiblePoints) {
      if (pair.getLeft().equals(Reactant.class)) {
        if (distanceFromPolylineStart(interaction.getLine(),
            modifierPoint) > distanceFromPolylineStart(interaction.getLine(), pair.getRight())) {
          result = pair.getRight();
        }
      }
    }
    if (result == null) {
      for (final Line2D line : interaction.getLine().getLines()) {
        if (distanceFromPolylineStart(interaction.getLine(),
            modifierPoint) > distanceFromPolylineStart(interaction.getLine(), line.getP1())) {
          double x = modifierPoint.getX() + (line.getX1() - modifierPoint.getX()) / 2;
          double y = modifierPoint.getY() + (line.getY1() - modifierPoint.getY()) / 2;
          result = new Point2D.Double(x, y);
        }
        if (distanceFromPolylineStart(interaction.getLine(),
            modifierPoint) > distanceFromPolylineStart(interaction.getLine(), line.getP2())) {
          double x = modifierPoint.getX() + (line.getX2() - modifierPoint.getX()) / 2;
          double y = modifierPoint.getY() + (line.getY2() - modifierPoint.getY()) / 2;
          result = new Point2D.Double(x, y);
        }
      }
    }
    return result;
  }

  private Point2D getModifierPoint(final List<Pair<Class<?>, Point2D>> points) {
    List<Pair<Class<?>, Point2D>> possiblePoints = new ArrayList<>(points);
    int productPoints = 0;
    int reactantPoints = 0;
    for (Pair<Class<?>, Point2D> pair : possiblePoints) {
      if (pair.getLeft().equals(Product.class)) {
        productPoints++;
      } else if (pair.getLeft().equals(Reactant.class)) {
        reactantPoints++;
      }
    }
    int countedReactants = 0;
    int countedProducts = 0;
    int score = Integer.MIN_VALUE;
    Point2D point = null;
    for (Pair<Class<?>, Point2D> pair : possiblePoints) {
      if (pair.getLeft().equals(Product.class)) {
        countedProducts++;
      } else if (pair.getLeft().equals(Reactant.class)) {
        countedReactants++;
      } else if (pair.getLeft().equals(Modifier.class)) {
        int currentScore = countedReactants + (productPoints - countedProducts);
        if (point == null || score < currentScore) {
          score = currentScore;
          point = pair.getRight();
        }
      }
    }

    if (point == null) {
      possiblePoints.add(0, new Pair<Class<?>, Point2D>(null, interaction.getLine().getStartPoint()));
      possiblePoints.add(new Pair<Class<?>, Point2D>(null, interaction.getLine().getEndPoint()));
      countedReactants = 0;
      countedProducts = 0;

      Point2D previousPoint = null;
      for (Pair<Class<?>, Point2D> pair : possiblePoints) {
        if (previousPoint != null) {
          int currentScore = countedReactants + (productPoints - countedProducts);
          if (point == null || score < currentScore) {
            score = currentScore;
            if (reactantPoints == 0) {
              // shift to most right possible line when
              // there are no other reactants
              point = getRightCenterPoint(interaction.getLine(), previousPoint, pair.getRight());
            } else if (productPoints == 0) {
              point = getLeftCenterPoint(interaction.getLine(), previousPoint, pair.getRight());
            } else {
              point = getCenterPoint(interaction.getLine(), previousPoint, pair.getRight());
            }
          }
        }
        if (pair.getLeft() == Product.class) {
          countedProducts++;
        } else if (pair.getLeft() == Reactant.class) {
          countedReactants++;
        }
        previousPoint = pair.getRight();
      }
    }
    return point;
  }

  private Point2D getCenterPoint(final PolylineData originalPolylineData, final Point2D startPoint, final Point2D endPoint) {
    PolylineData pd = getSubline(originalPolylineData, startPoint, endPoint);
    return getCenterPoint(pd);
  }

  private Point2D getCenterPoint(final PolylineData pd) {
    List<Line2D> lines = pd.getLines();
    double lenght = 0.0;
    for (final Line2D line : lines) {
      lenght += Geo.lineLen(line);
    }
    double tmp = 0.0;
    for (final Line2D line : lines) {
      if (tmp + Geo.lineLen(line) > lenght / 2) {
        double x = line.getX1() + (line.getX2() - line.getX1()) / 2;
        double y = line.getY1() + (line.getY2() - line.getY1()) / 2;
        return new Point2D.Double(x, y);
      } else {
        tmp += Geo.lineLen(line);
      }
    }
    return pt.copyPoint(pd.getEndPoint());
  }

  private Point2D getLeftCenterPoint(final PolylineData originalPolylineData, final Point2D startPoint, final Point2D endPoint) {
    PolylineData pd = getSubline(originalPolylineData, startPoint, endPoint);

    double x = (pd.getLines().get(0).getX1() + pd.getLines().get(0).getX2()) / 2;
    double y = (pd.getLines().get(0).getY1() + pd.getLines().get(0).getY2()) / 2;
    return new Point2D.Double(x, y);
  }

  private Point2D getRightCenterPoint(final PolylineData originalPolylineData, final Point2D startPoint, final Point2D endPoint) {
    PolylineData pd = getSubline(originalPolylineData, startPoint, endPoint);
    int lastLine = pd.getLines().size() - 1;
    double x = (pd.getLines().get(lastLine).getX1() + pd.getLines().get(lastLine).getX2()) / 2;
    double y = (pd.getLines().get(lastLine).getY1() + pd.getLines().get(lastLine).getY2()) / 2;
    return new Point2D.Double(x, y);
  }

  private PolylineData getSubline(final PolylineData originalPolylineData, final Point2D startPoint, final Point2D endPoint) {
    int start = 0;
    int end = originalPolylineData.getLines().size() - 1;
    for (int i = 0; i < originalPolylineData.getLines().size(); i++) {
      if (distanceFromPolylineStart(originalPolylineData, startPoint) > distanceFromPolylineStart(originalPolylineData,
          originalPolylineData.getLines().get(i).getP1())) {
        start = i;
      }
      if (distanceFromPolylineStart(originalPolylineData, endPoint) < distanceFromPolylineStart(originalPolylineData,
          originalPolylineData.getLines().get(i).getP2())) {
        end = Math.min(end, i);
      }
    }
    PolylineData result = new PolylineData(originalPolylineData);
    List<Line2D> lines = new ArrayList<>(result.getLines().subList(start, end + 1));
    result.removeLines();
    ;
    result.addLines(lines);

    result.setStartPoint(pt.copyPoint(startPoint));
    result.setEndPoint(pt.copyPoint(endPoint));
    return PolylineDataFactory.removeCollinearPoints(result);
  }

  double distanceFromPolylineStart(final PolylineData line, final Point2D point) {
    double distance = 0;
    for (final Line2D l : line.getLines()) {
      if (lt.distBetweenPointAndLineSegment(l, point) <= Configuration.EPSILON) {
        return distance + point.distance(l.getP1());
      }
      distance += l.getP1().distance(l.getP2());
    }
    throw new InvalidArgumentException("Point doesn't lay on the line");
  }

  private boolean pointOnPolyline(final PolylineData line, final Point2D point) {
    // point cannot be on the edge
    if (line.getStartPoint().distance(point) <= Configuration.EPSILON
        || line.getEndPoint().distance(point) <= Configuration.EPSILON) {
      return false;
    }

    for (final Line2D l : line.getLines()) {
      if (lt.distBetweenPointAndLineSegment(l, point) <= Configuration.EPSILON) {
        return true;
      }
    }
    return false;
  }
}
