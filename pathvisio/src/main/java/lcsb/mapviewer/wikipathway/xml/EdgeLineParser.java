package lcsb.mapviewer.wikipathway.xml;

import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.wikipathway.model.Edge;

/**
 * Parser class that creates {@link Edge} objects from Xml {@link Element node}.
 * However the xml node is not typical edge node , but line node.
 * 
 * @author Piotr Gawron
 *
 */
public class EdgeLineParser extends ElementGpmlParser<Edge> {

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  /**
   * Parser used to parse typical {@link Edge} xml nodes.
   */
  private final EdgeParser edgeParser;

  public EdgeLineParser(final String mapName) {
    super(mapName);
    this.edgeParser = new EdgeParser(mapName);
  }

  /**
   * Creates {@link Edge} from xml line node.
   * 
   * @return {@link Edge} from xml node
   */
  @Override
  public Edge parse(final Element element) throws ConverterException {
    if (!element.getNodeName().equals("GraphicalLine")) {
      throw new InvalidArgumentException(
          ShapeParser.class.getSimpleName() + " can parse only GraphicalLine xml nodes. But " + element.getNodeName());
    }
    Edge line = edgeParser.createEmpty();
    NodeList nodes = element.getChildNodes();
    for (Pair<String, String> entry : getAttributes(element)) {
      switch (entry.getLeft()) {
        case ("GraphId"):
          line.setGraphId(entry.getRight());
          break;
        case ("GroupRef"):
          line.setGroupRef(entry.getRight());
          break;
        default:
          logger.warn("Unknown attribute of " + element.getNodeName() + " node: " + entry.getLeft());
      }
    }

    for (int i = 0; i < nodes.getLength(); i++) {
      Node node = nodes.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element childElement = (Element) node;
        switch (node.getNodeName()) {
          case ("Graphics"):
            edgeParser.parseGraphics(line, (Element) node);
            break;
          case ("BiopaxRef"):
            line.addBiopaxReference(childElement.getTextContent());
            break;
          case ("Comment"):
            line.addComment(childElement.getTextContent());
            break;
          default:
            logger.warn("Unknown node in line: " + node.getNodeName());
        }
      }
    }
    return line;
  }

  @Override
  public String toXml(final Edge node, final LogMarker logMarker) throws ConverterException {
    throw new NotImplementedException();
  }

  @Override
  public String toXml(final Collection<Edge> list, final LogMarker logMarker) throws ConverterException {
    throw new NotImplementedException();
  }
}
