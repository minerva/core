package lcsb.mapviewer.wikipathway.xml;

import lcsb.mapviewer.commands.CreateHierarchyCommand;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.PolylineDataFactory;
import lcsb.mapviewer.converter.model.celldesigner.reaction.ReactionLineData;
import lcsb.mapviewer.converter.model.celldesigner.types.ModifierType;
import lcsb.mapviewer.converter.model.celldesigner.types.ModifierTypeUtils;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.OvalCompartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.reaction.type.ModifierReactionNotation;
import lcsb.mapviewer.model.map.reaction.type.ReducedNotation;
import lcsb.mapviewer.model.map.reaction.type.TwoReactantReactionInterface;
import lcsb.mapviewer.model.map.reaction.type.UnknownTransitionReaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.model.map.species.field.AbstractSiteModification;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithStructuralState;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import lcsb.mapviewer.wikipathway.model.DataNode;
import lcsb.mapviewer.wikipathway.model.Edge;
import lcsb.mapviewer.wikipathway.model.GpmlShape;
import lcsb.mapviewer.wikipathway.model.Graph;
import lcsb.mapviewer.wikipathway.model.GraphicalPathwayElement;
import lcsb.mapviewer.wikipathway.model.Group;
import lcsb.mapviewer.wikipathway.model.Interaction;
import lcsb.mapviewer.wikipathway.model.InteractionMapping;
import lcsb.mapviewer.wikipathway.model.Label;
import lcsb.mapviewer.wikipathway.model.PathwayElement;
import lcsb.mapviewer.wikipathway.model.Shape;
import lcsb.mapviewer.wikipathway.model.State;
import lcsb.mapviewer.wikipathway.model.biopax.BiopaxFactory;
import lcsb.mapviewer.wikipathway.xml.ReactionLayoutFinder.LineLocation;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.Color;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Class contains methods for GraphToModel conversion.
 *
 * @author Jan Badura
 */
public class ModelContructor {

  /**
   * Default color used by complexes.
   */
  static final Color DEFAULT_COMPLEX_ALIAS_COLOR = new Color(248, 247, 240);

  /**
   * Epsilon used for double comparison.
   */
  private static final double EPSILON = 1e-6;
  /**
   * List of {@link Shape#shape shapes} that are not supported to be part of a
   * {@link Complex complex}.
   */
  private static final Set<GpmlShape> INALID_COMPLEX_SHAPE_CHILDREN = new HashSet<>();

  static {
    INALID_COMPLEX_SHAPE_CHILDREN.add(GpmlShape.RECTANGLE);
    INALID_COMPLEX_SHAPE_CHILDREN.add(GpmlShape.OVAL);
    INALID_COMPLEX_SHAPE_CHILDREN.add(GpmlShape.ROUNDED_RECTANGLE);
    INALID_COMPLEX_SHAPE_CHILDREN.add(GpmlShape.GOLGI_APPARATUS);
    INALID_COMPLEX_SHAPE_CHILDREN.add(GpmlShape.BRACE);
    INALID_COMPLEX_SHAPE_CHILDREN.add(GpmlShape.SACROPLASMIC_RETICULUM);
    INALID_COMPLEX_SHAPE_CHILDREN.add(GpmlShape.MITOCHONDRIA);
    INALID_COMPLEX_SHAPE_CHILDREN.add(GpmlShape.ENDOPLASMIC_RETICULUM);
    INALID_COMPLEX_SHAPE_CHILDREN.add(GpmlShape.ARC);
    INALID_COMPLEX_SHAPE_CHILDREN.add(GpmlShape.TRIANGLE);
    INALID_COMPLEX_SHAPE_CHILDREN.add(GpmlShape.HEXAGON);
    INALID_COMPLEX_SHAPE_CHILDREN.add(GpmlShape.NONE);
    INALID_COMPLEX_SHAPE_CHILDREN.add(GpmlShape.PENTAGON);
    INALID_COMPLEX_SHAPE_CHILDREN.add(GpmlShape.PHOSPHORYLATED);
  }

  /**
   * CellDesigner util class used for retrieving information about modifier
   * graphics.
   */
  private final ModifierTypeUtils mtu = new ModifierTypeUtils();
  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  /**
   * Parser used for extracting {@link MiriamData references} from GPML model.
   */
  private final BiopaxFactory biopaxFactory;

  public ModelContructor(final String mapName) {
    biopaxFactory = new BiopaxFactory(mapName);
  }

  /**
   * This function creates Species from DataNode.
   *
   * @param dataNode object from which species is created
   * @return {@link Species} created from input {@link DataNode}
   */
  protected Species createSpecies(final DataNode dataNode) {
    Species species = null;
    String type = dataNode.getType();
    if (type == null || type.equals("")) {
      type = null;
    }

    if (type != null && type.equalsIgnoreCase("Metabolite")) {
      species = new SimpleMolecule(dataNode.getGraphId());
    } else if (type != null && type.equalsIgnoreCase("GeneProduct")) {
      species = new Gene(dataNode.getGraphId());
    } else if (type != null && type.equalsIgnoreCase("Pathway")) {
      species = new Phenotype(dataNode.getGraphId());
    } else if (type != null && type.equalsIgnoreCase("Rna")) {
      species = new Rna(dataNode.getGraphId());
    } else if (type != null && type.equalsIgnoreCase("Protein")) {
      species = new GenericProtein(dataNode.getGraphId());
    } else if (type != null && type.equalsIgnoreCase("Complex")) {
      species = new Complex(dataNode.getGraphId());
    } else if (type != null && type.equalsIgnoreCase("None")) {
      species = new Unknown(dataNode.getGraphId());
    } else if (type == null) {
      species = new Unknown(dataNode.getGraphId());
    } else {
      logger.warn(dataNode.getLogMarker(), "Unknown species type: " + type + ". Using Unknown");
      species = new Unknown(dataNode.getGraphId());
    }

    species.addMiriamData(dataNode.getReferences());
    if (dataNode.getLineType() == LineType.DASHED) {
      species.setHypothetical(true);
    }
    species.setName(dataNode.getName());
    species.setNotes(getComments(dataNode));
    return species;
  }

  /**
   * Creates {@link Unknown species} from {@link Label}.
   *
   * @param label original label from which output should be created
   * @return {@link Unknown} object created from input {@link Label}
   */
  private Species createSpecies(final Label label) {
    logger.warn(label.getLogMarker(), " Label cannot be part of reaction. Tranforming to Unknown");

    final Unknown unknown = new Unknown(label.getGraphId());
    unknown.addMiriamData(label.getReferences());
    unknown.setName(label.getName());
    final StringBuilder notes = new StringBuilder();
    for (final String comment : label.getComments()) {
      notes.append(comment + "\n\n");
    }
    unknown.setNotes(notes.toString());
    return unknown;
  }

  /**
   * Creates {@link Unknown species} from {@link Shape}.
   *
   * @param shape original label from which output should be created
   * @return {@link Unknown} object created from input {@link Label}
   */
  private Species createSpecies(final Shape shape) {
    Species result = null;
    if (shape.getShape() != null && shape.getShape().equals(GpmlShape.DEGRADATION)) {
      result = new Degraded(shape.getGraphId());
    } else {
      logger.warn(shape.getLogMarker(), " Shape can not be part of reaction. Tranforming to Unknown");

      result = new Unknown(shape.getGraphId());
    }
    final StringBuilder notes = new StringBuilder();
    for (final String comment : shape.getComments()) {
      notes.append(comment + "\n\n");
    }
    result.setNotes(notes.toString());
    result.setName(shape.getName());
    return result;
  }

  private String getComments(final PathwayElement dataNode) {
    final StringBuilder notes = new StringBuilder();
    for (final String comment : dataNode.getComments()) {
      notes.append(comment + "\n\n");
    }
    return notes.toString();
  }

  /**
   * This function adds {@link Complex} to model from graph. ComplexName is set as
   * groupId from GPML
   *
   * @param groups list of groups
   * @param data   ...
   */
  protected List<Element> createElementsFromGroup(final Collection<Group> groups, final Data data) {
    final List<Element> result = new ArrayList<>();
    for (final Group group : groups) {
      Element element = null;
      if ("Complex".equalsIgnoreCase(group.getStyle())) {
        element = new Complex(group.getGraphId());
        ((Complex) element).setHypothetical(false);
      } else if ("Pathway".equalsIgnoreCase(group.getStyle())) {
        element = new PathwayCompartment(group.getGraphId());
      } else {
        element = new Complex(group.getGraphId());
        ((Complex) element).setHypothetical(true);
      }

      element.setName(group.getName());

      Rectangle2D rec = group.getRectangle();
      if (rec == null) {
        rec = new Rectangle2D.Double(0, 0, 0, 0);
      }
      element.setX(rec.getX());
      element.setY(rec.getY());
      element.setWidth(rec.getWidth());
      element.setHeight(rec.getHeight());
      assignNameCoordinates(element);
      element.setZ(group.getzOrder());
      element.setFillColor(DEFAULT_COMPLEX_ALIAS_COLOR);

      data.id2alias.put(group.getGraphId(), element);
      result.add(element);
    }
    return result;
  }

  /**
   * This function adds Species, TextLabels, final Compartments and Shapes from
   * graph.
   *
   * @param model model to which species will be added
   * @param graph GPML data model
   * @param data  ...
   */
  protected void addElement(final Model model, final Graph graph, final Data data) {

    for (final DataNode dataNode : graph.getDataNodes()) {
      final Species species = createSpecies(dataNode);
      species.addMiriamData(biopaxFactory.getMiriamData(graph.getBiopaxData(), dataNode.getBiopaxReferences()));

      final Element alias = updateAlias(dataNode, species);

      data.id2alias.put(dataNode.getGraphId(), alias);
      model.addElement(alias);
    }
    for (final Label label : graph.getLabels()) {
      if (label.isTreatAsNode()) {
        final Species species = createSpecies(label);
        species.addMiriamData(biopaxFactory.getMiriamData(graph.getBiopaxData(), label.getBiopaxReferences()));

        final Element alias = updateAlias(label, species);

        data.id2alias.put(label.getGraphId(), alias);
        model.addElement(alias);
      } else if (label.getGroupRef() == null || label.getGroupRef().isEmpty()) {
        final LayerText text = createText(label);
        data.layer.addLayerText(text);
      }
    }
    for (final Shape shape : graph.getShapes()) {
      if (shape.isTreatAsNode()) {
        final Species species = createSpecies(shape);
        species.addMiriamData(biopaxFactory.getMiriamData(graph.getBiopaxData(), shape.getBiopaxReferences()));

        final Element alias = updateAlias(shape, species);

        data.id2alias.put(shape.getGraphId(), alias);
        model.addElement(alias);

      } else {
        if (!shape.isCompartment()) {
          if (Objects.equals(shape.getShape(), GpmlShape.OVAL)) {
            final LayerOval oval = createOval(shape);
            data.layer.addLayerOval(oval);
          } else if (shape.getComments().size() > 0 || shape.getTextLabel() != null) {
            data.layer.addLayerText(createText(shape));
          } else {
            final LayerRect rect = createRectangle(shape);
            data.layer.addLayerRect(rect);
          }
        } else {
          final Compartment compartment = new Compartment(shape.getGraphId());
          final Element cmpAl = updateAlias(shape, compartment);
          model.addElement(cmpAl);
        }
      }
    }

    for (final State state : graph.getStates()) {
      // TODO this might not work
      final Element species = data.id2alias.get(state.getGraphRef());
      if (state.getType() != null) {
        AbstractSiteModification mr = null;

        if (species instanceof Protein) {
          mr = new Residue();
          ((Protein) species).addResidue((Residue) mr);
        } else if (species instanceof Gene) {
          mr = new ModificationSite();
          ((Gene) species).addModificationSite((ModificationSite) mr);
        } else {
          logger.warn(
              state.getLogMarker(), "state for " + species.getClass().getSimpleName() + " is not supported.");
        }
        if (mr != null) {
          mr.setIdModificationResidue(state.getGraphId());
          mr.setState(state.getType());
          if (state.getTypeName() != null) {
            mr.setName(state.getTypeName());
          } else {
            mr.setName("");
          }

          final Double x = state.getRelX() * species.getWidth() / 2 + species.getCenterX() - mr.getWidth() / 2;
          Double y = state.getRelY() * species.getHeight() / 2 + species.getCenterY();
          if (mr instanceof ModificationSite) {
            y -= mr.getHeight();
          } else {
            y -= mr.getHeight() / 2;
          }
          mr.setPosition(new Point2D.Double(x, y));
          if (species.getZ() != null) {
            mr.setZ(species.getZ() + 1);
          }
          mr.setNameX(mr.getX());
          mr.setNameY(mr.getY());
          mr.setBorderColor(species.getBorderColor());
        }
      } else if (state.getStructuralState() != null) {
        if (species instanceof SpeciesWithStructuralState) {
          final SpeciesWithStructuralState speciesWithStructuralState = (SpeciesWithStructuralState) species;
          speciesWithStructuralState.addStructuralState(createStructuralState(state, species));
        } else {
          logger.warn(state.getLogMarker(), "structural state for " + species.getClass().getSimpleName()
              + " is not supported.");
        }
      }
    }
  }

  private StructuralState createStructuralState(final State state, final Element species) {
    final Double x = state.getRelX() * species.getWidth() + species.getX();
    final Double y = state.getRelY() * species.getHeight() + species.getY();
    final StructuralState structuralState = new StructuralState();
    structuralState.setPosition(new Point2D.Double(x, y));
    structuralState.setFontSize(10);
    structuralState.setHeight(state.getHeight());
    structuralState.setWidth(state.getWidth());
    structuralState.setName(state.getStructuralState());
    structuralState.setNameHeight(state.getHeight());
    structuralState.setNameWidth(state.getWidth());
    structuralState.setNameX(x);
    structuralState.setNameY(y);
    structuralState.setNameHorizontalAlign(HorizontalAlign.CENTER);
    structuralState.setNameVerticalAlign(VerticalAlign.MIDDLE);
    if (species.getZ() != null) {
      structuralState.setZ(species.getZ() + 1);
    }
    return structuralState;
  }

  /**
   * Creates {@link LayerRect} object from {@link Shape}.
   *
   * @param shape source GPML object to be transformed
   * @return {@link LayerRect} obtained from {@link Shape} object
   */
  LayerRect createRectangle(final Shape shape) {
    final LayerRect rect = new LayerRect();
    final Rectangle2D rec;
    if (shouldRotate90degrees(shape)) {
      rec = rotate90degrees(shape.getRectangle());
    } else {
      rec = shape.getRectangle();
    }
    rect.setZ(shape.getzOrder());
    rect.setX(rec.getX());
    rect.setY(rec.getY());
    rect.setHeight(rec.getHeight());
    rect.setWidth(rec.getWidth());
    rect.setBorderColor(shape.getColor());
    rect.setFillColor(shape.getFillColor());
    return rect;
  }

  Rectangle2D rotate90degrees(final Rectangle2D rectangle) {
    return new Rectangle2D.Double(rectangle.getCenterX() - rectangle.getHeight() / 2,
        rectangle.getCenterY() - rectangle.getWidth() / 2, rectangle.getHeight(), rectangle.getWidth());
  }

  boolean shouldRotate90degrees(final Shape shape) {
    return shouldRotate90degrees(shape.getRotation(), shape);
  }

  boolean shouldRotate90degrees(final Double rot) {
    return shouldRotate90degrees(rot, null);
  }

  boolean shouldRotate90degrees(final Double rot, final Shape shape) {
    if (rot == null) {
      return false;
    }
    double rotation = rot;
    while (rotation > Math.PI / 2) {
      rotation -= Math.PI;
    }
    while (rotation < -Math.PI / 2) {
      rotation += Math.PI;
    }
    if (Math.abs(rotation) < Math.PI / 4) {
      if (Math.abs(rotation) > Configuration.EPSILON) {
        LogMarker marker = null;
        if (shape != null) {
          marker = shape.getLogMarker();
        }
        logger.warn(marker, "Rotation of objects is not supported. Rotation=" + rot);
      }
      return false;
    }
    if ((Math.PI / 2 - Math.abs(rotation)) > Configuration.EPSILON) {
      LogMarker marker = null;
      if (shape != null) {
        marker = shape.getLogMarker();
      }
      logger.warn(marker, "Rotation of objects is not supported. Rotation=" + rot);
    }
    return true;
  }

  /**
   * Creates {@link LayerOval} object from {@link Shape}.
   *
   * @param shape source GPML object to be transformed
   * @return {@link LayerOval} obtained from {@link Shape} object
   */
  public LayerOval createOval(final Shape shape) {
    final LayerOval oval = new LayerOval();
    final Rectangle2D rec = shape.getRectangle();
    oval.setX(rec.getX());
    oval.setY(rec.getY());
    oval.setHeight(rec.getHeight());
    oval.setWidth(rec.getWidth());
    oval.setColor(shape.getColor());
    return oval;
  }

  /**
   * Creates {@link LayerText} from {@link Label}.
   *
   * @param label object from which result will be created
   * @return {@link LayerText} from {@link Label}
   */
  public LayerText createText(final Label label) {
    final Rectangle2D rec = label.getRectangle();

    final LayerText text = new LayerText();
    text.setX(rec.getX());
    text.setY(rec.getY());
    text.setHeight(rec.getHeight());
    text.setWidth(rec.getWidth());
    text.setColor(label.getColor());
    if (text.getColor().equals(Color.WHITE)) {
      text.setColor(Color.BLACK);
    }
    final StringBuilder notes = new StringBuilder(label.getName().replaceAll("[\n\r]+", " "));
    for (final String comment : label.getComments()) {
      notes.append("\n\n" + comment);
    }
    text.setNotes(notes.toString());
    return text;
  }

  public LayerText createText(final Shape shape) {
    final Rectangle2D rec = shape.getRectangle();

    final LayerText text = new LayerText();
    text.setX(rec.getX());
    text.setY(rec.getY());
    text.setHeight(rec.getHeight());
    text.setWidth(rec.getWidth());
    text.setColor(shape.getColor());
    if (text.getColor().equals(Color.WHITE)) {
      text.setColor(Color.BLACK);
    }
    text.setBorderColor(shape.getColor());
    text.setBackgroundColor(shape.getFillColor());

    String name = shape.getTextLabel();
    if (name == null) {
      name = "";
    }

    final StringBuilder notes = new StringBuilder(name);
    for (final String comment : shape.getComments()) {
      notes.append(comment + "\n\n");
    }
    text.setNotes(notes.toString());

    return text;
  }

  /**
   * Creates alias for {@link GraphicalPathwayElement}. Type of the alias is
   * defined by the parameter {@link Species}
   *
   * @param gpmlElement object from which alias will be create
   * @param element     element for which alias will be created
   * @return {@link Species} created from input {@link Label}
   */
  public Element updateAlias(final GraphicalPathwayElement gpmlElement, final Element element) {
    Element result = element;
    if (result instanceof Compartment) {
      if (((Shape) gpmlElement).getShape().equals(GpmlShape.OVAL)) {
        result = new OvalCompartment((Compartment) result);
      } else {
        result = new SquareCompartment((Compartment) result);
      }
      String name = ((Shape) gpmlElement).getTextLabel();
      if (name == null) {
        name = "";
      }
      result.setName(name);
    }

    final Rectangle2D rec = gpmlElement.getRectangle();
    result.setX(rec.getX());
    result.setY(rec.getY());
    result.setWidth(rec.getWidth());
    if (result.getWidth() <= 0) {
      logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, result),
          "Element width must be positive. Setting to 1.0");
      result.setWidth(1.0);
    }
    if (result instanceof Degraded) {
      result.setHeight(Math.min(rec.getHeight(), rec.getWidth()));
    } else {
      result.setHeight(rec.getHeight());
    }
    if (result.getHeight() <= 0) {
      logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, result),
          "Element height must be positive. Setting to 1.0");
      result.setHeight(1.0);
    }

    assignNameCoordinates(result);

    result.setFillColor(gpmlElement.getFillColor());
    if (result instanceof Compartment && !Objects.equals(Color.WHITE, gpmlElement.getFillColor())) {
      result.setBorderColor(gpmlElement.getFillColor());
    } else {
      result.setBorderColor(gpmlElement.getColor());
    }
    result.setZ(gpmlElement.getzOrder());
    result.setFontColor(gpmlElement.getColor());
    if (gpmlElement.getFontSize() != null) {
      result.setFontSize(gpmlElement.getFontSize());
    }
    return result;
  }

  private void assignNameCoordinates(final Element element) {
    element.setNameWidth(element.getWidth());
    element.setNameHeight(element.getHeight());
    if (element instanceof Compartment) {
      element.setNameX(element.getX());
      element.setNameY(element.getY());
      element.setNameHorizontalAlign(HorizontalAlign.LEFT);
      element.setNameVerticalAlign(VerticalAlign.TOP);
    } else if (element instanceof Complex) {
      element.setNameX(element.getX());
      element.setNameY(element.getY());
      element.setNameHeight(element.getHeight() - 2);
      element.setNameHorizontalAlign(HorizontalAlign.CENTER);
      element.setNameVerticalAlign(VerticalAlign.BOTTOM);
    } else {
      element.setNameX(element.getX());
      element.setNameY(element.getY());
      element.setNameHorizontalAlign(HorizontalAlign.CENTER);
      element.setNameVerticalAlign(VerticalAlign.MIDDLE);
    }
  }

  /**
   * This function add Species to right Complexes.
   *
   * @param graph GPML data model
   * @param data  ...
   */
  protected void putSpeciesIntoComplexes(final Graph graph, final Data data) throws ConverterException {
    for (final Group group : graph.getGroups()) {
      Complex complex = null;
      Compartment compartment = null;
      Element parent = null;
      if (data.id2alias.get(group.getGraphId()) instanceof Complex) {
        complex = (Complex) data.id2alias.get(group.getGraphId());
        parent = complex;
      } else if (data.id2alias.get(group.getGraphId()) instanceof Compartment) {
        compartment = (Compartment) data.id2alias.get(group.getGraphId());
        parent = compartment;
      }
      for (final PathwayElement pe : group.getNodes()) {
        final Element element = data.id2alias.get(pe.getGraphId());
        if (element != null) {
          if (complex != null) {
            if (element instanceof Species) {
              final Species species = (Species) element;
              complex.addSpecies(species);
            }
          } else if (compartment != null) {
            compartment.addElement(element);
          } else {
            throw new ConverterException("Parent doesn't exists: " + group.getGraphId());
          }
          // we have label
        } else if (graph.getLabelByGraphId(pe.getGraphId()) != null) {
          final Label label = graph.getLabelByGraphId(pe.getGraphId());
          // if complex has generic name then change it into label
          if (parent.getName().equals(group.getGraphId()) || parent.getName().trim().isEmpty()) {
            parent.setName(label.getTextLabel());
            parent.setNameX(label.getRectangle().getMinX());
            parent.setNameY(label.getRectangle().getMinY());
            parent.setNameWidth(label.getRectangle().getWidth());
            parent.setNameHeight(label.getRectangle().getHeight());
            parent.setNameHorizontalAlign(HorizontalAlign.CENTER);
            parent.setNameVerticalAlign(VerticalAlign.MIDDLE);
          } else {
            // if there are more labels than one then merge labels
            parent.setName(parent.getName() + "\n" + label.getTextLabel());
          }
        } else if (graph.getShapeByGraphId(pe.getGraphId()) != null) {
          final Shape shape = graph.getShapeByGraphId(pe.getGraphId());
          if (INALID_COMPLEX_SHAPE_CHILDREN.contains(shape.getShape())) {
            logger.warn(shape.getLogMarker(), shape.getShape() + " cannot be part of a complex. Skipping.");
          } else {
            throw new UnknownChildClassException("Unknown class type with id \"" + pe.getGraphId() + "\": "
                + shape.getShape() + ". Group id: " + group.getGraphId());
          }
        } else {
          throw new UnknownChildClassException(
              "Cannot find child with id: " + pe.getGraphId() + ". Group id: " + group.getGraphId());
        }
      }
    }
  }

  /**
   * This function creates {@link Reaction} from {@link Interaction} from graph.
   *
   * @param interaction GPML interaction
   * @param graph       GPML data model
   * @param data        ...
   * @return reaction created from GPML {@link Interaction}
   */
  protected Reaction createReactionFromInteraction(final Interaction interaction, final Graph graph, final Data data) {
    final InteractionMapping mapping = InteractionMapping.getInteractionMapping(interaction.getType(),
        interaction.getLine().getType());
    if (mapping == null) {
      throw new InvalidArgumentException("Unknown interaction type: " + interaction.getType());
    }
    Reaction reaction = createReactionByType(mapping.getModelReactionType(), interaction.getGraphId());
    if (mapping.isReversible() || interaction.isReversible()) {
      if (reaction instanceof ReducedNotation || reaction instanceof ModifierReactionNotation) {
        logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, reaction),
            "Reacion was marked as reversible but it cannot be reversible");
      } else {
        reaction.setReversible(true);
      }
    }

    reaction.setNotes(getComments(interaction));

    reaction.addMiriamData(interaction.getReferences());
    reaction.setZ(interaction.getzOrder());
    reaction.addMiriamData(biopaxFactory.getMiriamData(graph.getBiopaxData(), interaction.getBiopaxReferences()));

    final ReactionLayoutFinder layoutFinder = new ReactionLayoutFinder();
    final Map<LineLocation, PolylineData> lines = layoutFinder.getNodeLines(interaction);
    final PolylineData reactantLine = lines.get(LineLocation.REACTANT);
    final PolylineData productLine = lines.get(LineLocation.PRODUCT);

    final Reactant reactant = new Reactant(data.id2alias.get(interaction.getStart()));
    reactant.setLine(reactantLine);
    reaction.addReactant(reactant);

    final Product product = new Product(data.id2alias.get(interaction.getEnd()));
    product.setLine(productLine);
    reaction.addProduct(product);

    for (final Edge e : interaction.getReactants()) {
      final Reactant reac = createReactantFromEdge(data, e);
      reaction.addReactant(reac);
    }

    for (final Edge e : interaction.getProducts()) {
      final Product pro = createProductFromEdge(data, e);
      reaction.addProduct(pro);
    }

    if (reaction.getReactants().size() > 1) {
      final PolylineData operatorLine = lines.get(LineLocation.INPUT_OPERATOR);
      final NodeOperator andOperator = new AndOperator();
      andOperator.addInput(reactant);
      andOperator.setLine(operatorLine);
      for (int i = 1; i < reaction.getReactants().size(); i++) {
        final Reactant r = reaction.getReactants().get(i);
        r.getLine().setEndPoint(operatorLine.getStartPoint());
        andOperator.addInput(r);
      }
      reaction.addNode(andOperator);
    } else {
      final PolylineData operatorLine = lines.get(LineLocation.INPUT_OPERATOR);
      reactantLine.addLines(operatorLine.getLines());
      reactant.setLine(PolylineDataFactory.removeCollinearPoints(reactantLine));
    }

    if (reaction.getProducts().size() > 1) {
      final PolylineData operatorLine = lines.get(LineLocation.OUTPUT_OPERATOR);

      final AndOperator splitOperator = new AndOperator();
      splitOperator.addOutput(product);
      splitOperator.setLine(operatorLine.reverse());
      for (int i = 1; i < reaction.getProducts().size(); i++) {
        final Product r = reaction.getProducts().get(i);
        r.getLine().setStartPoint(operatorLine.getEndPoint());
        splitOperator.addOutput(r);
      }
      reaction.addNode(splitOperator);
    } else {
      final PolylineData operatorLine = lines.get(LineLocation.OUTPUT_OPERATOR);

      final List<Line2D> productLines = new ArrayList<>();
      productLines.addAll(operatorLine.getLines());
      productLines.addAll(productLine.getLines());
      productLine.removeLines();
      productLine.addLines(productLines);

      product.setLine(PolylineDataFactory.removeCollinearPoints(productLine));
    }

    for (final Edge e : interaction.getModifiers()) {
      final Modifier mod = createModifierFromEdge(data, e);
      reaction.addModifier(mod);
    }
    if (reaction instanceof TwoReactantReactionInterface && reaction.getReactants().size() < 2) {
      logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, reaction),
          "Reaction should contain at least 2 reactants. GraphId: " + interaction.getGraphId());
      reaction = new UnknownTransitionReaction(reaction);
    }
    if (reaction instanceof ReducedNotation && ((reaction.getReactants().size() > 1)
        || reaction.getProducts().size() > 1 || reaction.getModifiers().size() > 0)) {
      logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, reaction),
          "Reaction should contain only one reactant and one product.");
      reaction = new UnknownTransitionReaction(reaction);
    }
    final ReactionLineData rld = ReactionLineData.getByReactionType(reaction.getClass());
    if (rld == null) {
      throw new InvalidArgumentException("Don't know how to process reaction type: " + reaction.getClass());
    }
    for (final AbstractNode node : reaction.getNodes()) {
      if (!(node instanceof Modifier)) {
        node.getLine().setType(rld.getLineType());
      }
    }
    for (final Product p : reaction.getProducts()) {
      p.getLine().getEndAtd().setArrowType(rld.getProductArrowType());
      if (rld.getProductLineTrim() > 0) {
        p.getLine().trimEnd(rld.getProductLineTrim());
      }

    }
    if (reaction.isReversible()) {
      for (final Reactant r : reaction.getReactants()) {
        r.getLine().getBeginAtd().setArrowType(rld.getProductArrowType());
        if (rld.getProductLineTrim() > 0) {
          r.getLine().trimBegin(rld.getProductLineTrim());
        }
      }
    }

    final PolylineData modifierLine = lines.get(LineLocation.MODIFIER);
    modifierLine.setType(rld.getLineType());
    reaction.setLine(modifierLine);

    final ModifierTypeUtils mtu = new ModifierTypeUtils();
    for (final Modifier modifier : reaction.getModifiers()) {
      modifier.getLine()
          .setEndPoint(modifier.getLine().getLines().get(modifier.getLine().getLines().size() - 1).getP1());
      modifier.getLine()
          .setEndPoint(mtu.getAnchorPointOnReactionRect(reaction, mtu.getTargetLineIndexByModifier(modifier)));

      final ModifierType mt = mtu.getModifierTypeForClazz(modifier.getClass());
      modifier.getLine().trimEnd(mt.getTrimLength());
    }
    return reaction;
  }

  /**
   * Creates {@link Reactant} from GPML edge.
   *
   * @param data ...
   * @param e    edge
   * @return {@link Reactant} from gpml edge
   */
  protected Reactant createReactantFromEdge(final Data data, final Edge e) {
    String id = null;
    if (data.id2alias.containsKey(e.getStart())) {
      id = e.getStart();
    } else if (data.id2alias.containsKey(e.getEnd())) {
      id = e.getEnd();
    }
    if (id == null) {
      throw new InvalidStateException("This reactant is invalid");
    }

    final Reactant reac = new Reactant(data.id2alias.get(id));
    reac.setLine(e.getLine());

    // if somebody drew the reaction in reverse order (starting from reaction
    // and going to the product) then reverse the order of points: order should
    // be from reactant to the product
    if (id.equals(e.getEnd())) {
      reac.setLine(reac.getLine().reverse());
    }
    return reac;
  }

  /**
   * Creates {@link Product} from GPML edge.
   *
   * @param data ...
   * @param e    edge
   * @return {@link Product} from GPML edge
   */
  protected Product createProductFromEdge(final Data data, final Edge e) {
    String id = null;
    if (data.id2alias.containsKey(e.getStart())) {
      id = e.getStart();
    } else if (data.id2alias.containsKey(e.getEnd())) {
      id = e.getEnd();
    }
    if (id == null) {
      throw new InvalidStateException("[" + e.getGraphId() + "]\tThis product is invalid");
    }
    final Product pro = new Product(data.id2alias.get(id));
    pro.setLine(e.getLine());
    // if somebody drew the reaction in reverse order (starting from reaction
    // and going to the product) then reverse the order of points: order should
    // be from reactant to the product
    if (id.equals(e.getStart())) {
      pro.setLine(pro.getLine().reverse());
    }
    return pro;
  }

  /**
   * Creates {@link Modifier} from gpml edge.
   *
   * @param data ...
   * @param e    edge
   * @return {@link Modifier} from gpml edge
   */
  protected Modifier createModifierFromEdge(final Data data, final Edge e) {
    final InteractionMapping modifierMapping = InteractionMapping.getInteractionMapping(e.getType(), e.getLine().getType());
    if (modifierMapping == null) {
      throw new InvalidArgumentException("Unknown interaction type: " + e.getType());
    }

    Class<? extends ReactionNode> modifierType = null;
    String id = null;
    if (data.id2alias.containsKey(e.getStart())) {
      modifierType = modifierMapping.getModelInputReactionNodeType();
      id = e.getStart();
    } else if (data.id2alias.containsKey(e.getEnd())) {
      modifierType = modifierMapping.getModelOutputReactionNodeType();
      id = e.getEnd();
    }
    if (id == null) {
      throw new InvalidStateException("This modifier is invalid");
    }

    final Modifier mod = createModifierByType(modifierType, data.id2alias.get(id));

    final ModifierType mt = mtu.getModifierTypeForClazz(mod.getClass());

    e.getLine().setEndAtd(mt.getAtd());
    e.getLine().setType(mt.getLineType());
    mod.setLine(e.getLine());

    return mod;
  }

  /**
   * Creates {@link Reaction} for a given class type.
   *
   * @param reactionType type of reaction to create
   * @return new instance of the reactionType
   */
  private Reaction createReactionByType(final Class<? extends Reaction> reactionType, final String elementId) {
    try {
      final Reaction result = reactionType.getConstructor(String.class).newInstance(elementId);
      return result;
    } catch (final Exception e) {
      throw new InvalidArgumentException("Cannot create reaction.", e);
    }
  }

  /**
   * Creates {@link Modifier} for a given class type.
   *
   * @param modifierType type of modifier in reaction to create
   * @param alias        {@link Species alias } to which modifier is attached
   * @return new instance of the modifierType
   */
  private Modifier createModifierByType(final Class<? extends ReactionNode> modifierType, final Element alias) {
    try {
      final Modifier result = (Modifier) modifierType.getConstructor(Element.class).newInstance(alias);
      return result;
    } catch (final Exception e) {
      throw new InvalidArgumentException("Cannot create modifier.", e);
    }
  }

  /**
   * This function creates {@link Model} from {@link Graph}.
   *
   * @param graph object with data obtained from gpml
   * @return {@link Model} object representing gpml
   * @throws ConverterException exception thrown when conversion from graph couldn't be performed
   */
  public Model getModel(final Graph graph) throws ConverterException {
    try {
      final Data data = new Data();
      final Model model = new ModelFullIndexed(null);
      model.setWidth(graph.getBoardWidth());
      model.setHeight(graph.getBoardHeight());
      model.setName(graph.getMapName());

      for (final String key : graph.getAttributes().keySet()) {
        final String value = graph.getAttributes().get(key);
        switch (key) {
          case ("org.pathvisio.model.WindowWidth"):
          case ("org.pathvisio.model.WindowHeight"):
          case ("ConversionDate"):
          case ("SBML_Level"):
          case ("SBML_Version"):
          case ("org.pathvisio.sbgn.language"):
            break;
          case ("KeggId"):
            model.addMiriamData(new MiriamData(MiriamType.KEGG_PATHWAY, value));
            break;
          case ("reactome_id"):
            if (NumberUtils.isDigits(value)) {
              model.addMiriamData(new MiriamData(MiriamType.REACTOME, "R-HSA-" + value));
            } else {
              logger.warn("Invalid reactome_id: " + value);
            }
            break;
          default:
            logger.warn("Unknown pathway attribute: " + key + " - " + value);
            break;
        }
      }

      model.addElements(createElementsFromGroup(graph.getGroups(), data));
      addElement(model, graph, data);
      putSpeciesIntoComplexes(graph, data);

      for (final Interaction interaction : graph.getInteractions()) {
        final Reaction reaction = createReactionFromInteraction(interaction, graph, data);
        model.addReaction(reaction);
      }

      removeEmptyComplexes(model);

      removeSelfLoops(model);

      fixCompartmentAliases(model);

      final StringBuilder tmp = new StringBuilder();
      for (final String string : graph.getComments()) {
        tmp.append(string + "\n");
      }
      String notes = tmp.toString();
      boolean different = false;
      do {
        final String newNotes = StringEscapeUtils.unescapeHtml4(notes);
        different = newNotes.compareTo(notes) != 0;
        notes = newNotes;
      } while (different);
      if (notes != null) {
        notes = StringEscapeUtils.unescapeHtml4(notes);
      }
      model.setNotes(notes);
      model.addMiriamData(biopaxFactory.getMiriamData(graph.getBiopaxData(), graph.getBiopaxReferences()));

      data.layer.addLayerLines(createLines(graph));

      if (!data.layer.isEmpty()) {
        model.getLayers().add(data.layer);
      }

      assignNamesToComplexes(model);
      assignNamesToCompartments(model);

      putAliasesIntoCompartments(model);

      putMissingZIndexes(model.getDrawables());

      return model;
    } catch (final UnknownChildClassException e) {
      throw new ConverterException(e);
    }
  }

  void removeSelfLoops(final Model model) {
    final Set<Reaction> toRemove = new HashSet<>();
    for (final Reaction reaction : model.getReactions()) {
      if (reaction.getReactants().get(0).getElement().equals(reaction.getProducts().get(0).getElement())) {
        logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, reaction),
            "Self loops are not supported. Removing");
        toRemove.add(reaction);
      }
    }
    model.removeReactions(toRemove);
  }

  private void putMissingZIndexes(final Collection<Drawable> bioEntities) {
    int maxZIndex = 0;
    for (final Drawable bioEntity : bioEntities) {
      if (bioEntity.getZ() != null) {
        maxZIndex = Math.max(maxZIndex, bioEntity.getZ());
      }
    }

    for (final Drawable bioEntity : bioEntities) {
      if (bioEntity.getZ() == null) {
        Integer index = null;
        if (bioEntity instanceof Complex) {
          final Complex complex = (Complex) bioEntity;
          index = getMinZIndex(complex.getAllChildren());
          Integer maxReactionIndex = null;
          for (final Drawable reactionBioEntity : bioEntities) {
            if (reactionBioEntity instanceof Reaction) {
              final Reaction reaction = (Reaction) reactionBioEntity;
              if (reaction.containsElement(complex)) {
                if (maxReactionIndex == null) {
                  maxReactionIndex = reaction.getZ();
                } else if (reaction.getZ() != null) {
                  maxReactionIndex = Math.max(reaction.getZ(), maxReactionIndex);
                }
              }
            }
          }
          if (index == null) {
            index = maxReactionIndex;
          } else if (maxReactionIndex != null && index > maxReactionIndex) {
            index = maxReactionIndex + 1;
          }
        }

        if (index == null) {
          index = ++maxZIndex;
        } else {
          index--;
        }
        bioEntity.setZ(index);
      }
    }

  }

  private Integer getMinZIndex(final List<Species> allChildren) {
    Integer result = null;
    for (final Species species : allChildren) {
      if (species.getZ() != null) {
        if (result == null) {
          result = species.getZ();
        }
        result = Math.min(result, species.getZ());
      }
    }
    return result;
  }

  /**
   * Method that put {@link Species aliases} that are not assigned into any
   * {@link Compartment} into a proper compartment.
   *
   * @param model model where aliases will be modified
   */
  private void putAliasesIntoCompartments(final Model model) {
    for (final Element element : model.getElements()) {
      if (element instanceof Species) {
        if (((Species) element).getComplex() != null) {
          element.setCompartment(null);
        } else if (element.getCompartment() == null) {
          Compartment selectedAlias = null;
          for (final Compartment compartment : model.getCompartments()) {
            if (compartment.cross(element)) {
              if (selectedAlias == null) {
                selectedAlias = compartment;
              } else if (selectedAlias.getSize() > compartment.getSize()) {
                selectedAlias = compartment;
              }
            }
          }
          if (selectedAlias != null) {
            selectedAlias.addElement(element);
          }
        }
      }
    }
  }

  /**
   * Removes empty complexes (with size 0) from model.
   *
   * @param model model where operation is performed
   */
  void removeEmptyComplexes(final Model model) {
    final Set<Element> aliasesInReaction = new HashSet<>();
    for (final Reaction reaction : model.getReactions()) {
      for (final ReactionNode node : reaction.getReactionNodes()) {
        aliasesInReaction.add(node.getElement());
      }
    }
    final Set<BioEntity> toRemove = new HashSet<>();
    for (final Element element : model.getElements()) {
      if (element instanceof Complex) {
        final Complex complex = (Complex) element;
        if (complex.getSize() <= EPSILON && complex.getAllChildren().size() == 0) {
          toRemove.add(element);
          if (aliasesInReaction.contains(element)) {
            logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, element),
                "Empty element is invalid, but it's a part of reaction.");
            for (final Reaction reaction : model.getReactions()) {
              for (final ReactionNode node : reaction.getReactionNodes()) {
                if (node.getElement().equals(element)) {
                  toRemove.add(reaction);
                  logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, reaction),
                      "Reaction to empty element is invalid.");
                }
              }
            }
          } else {
            logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, element), "Empty element is invalid");
          }
        }
      }
    }
    for (final BioEntity bioEntitiy : toRemove) {
      model.removeBioEntity(bioEntitiy);
    }
  }

  /**
   * Tries to find a name to assign to complexes if complexes don't contain them.
   *
   * @param model model where complexes are placed
   */
  private void assignNamesToComplexes(final Model model) {
    final Set<Element> elementsToRemove = new HashSet<>();
    final Set<Element> elementsWithReaction = new HashSet<>();
    for (final Reaction reaction : model.getReactions()) {
      for (final ReactionNode reactionNode : reaction.getReactionNodes()) {
        elementsWithReaction.add(reactionNode.getElement());
      }
    }
    for (final Element complex : model.getElements()) {
      if (complex instanceof Complex) {
        if (complex.getName() == null || complex.getName().isEmpty()
            || complex.getName().equals(complex.getElementId())) {
          for (final Species child : ((Complex) complex).getElements()) {
            if (child instanceof Complex
                && ((Complex) child).getElements().size() == 0
                && !elementsWithReaction.contains(child)) {
              complex.setName(child.getName());
              ((Complex) complex).removeElement(child);
              if (child.getCompartment() != null) {
                child.getCompartment().removeElement(child);
              }
              elementsToRemove.add(child);

              complex.setNameX(child.getX());
              complex.setNameY(child.getY());
              complex.setNameWidth(child.getWidth());
              complex.setNameHeight(child.getHeight());
              complex.setNameHorizontalAlign(HorizontalAlign.CENTER);
              complex.setNameVerticalAlign(VerticalAlign.MIDDLE);

              break;
            }
          }
          if (complex.getName() == null || (complex.getName().isEmpty())) {
            for (final Layer layer : model.getLayers()) {
              if (layer.getName().equals(CreateHierarchyCommand.TEXT_LAYER_NAME)) {
                LayerText toRemove = null;
                for (final LayerText lt : layer.getTexts()) {
                  if (complex.contains(lt)) {
                    complex.setName(lt.getNotes());
                    complex.setNameX(lt.getX());
                    complex.setNameY(lt.getY());
                    complex.setNameWidth(lt.getWidth());
                    complex.setNameHeight(lt.getHeight());
                    complex.setNameHorizontalAlign(HorizontalAlign.LEFT);
                    complex.setNameVerticalAlign(VerticalAlign.TOP);
                    toRemove = lt;
                    break;
                  }
                }
                if (toRemove != null) {
                  layer.removeLayerText(toRemove);
                  break;
                }
              }
            }
          }
        }
        if (((Complex) complex).getElements().isEmpty()) {
          complex.setNameVerticalAlign(VerticalAlign.MIDDLE);
        } else {
          complex.setNameVerticalAlign(VerticalAlign.BOTTOM);
        }
      }
    }
    for (final Element element : elementsToRemove) {
      model.removeElement(element);
    }
  }

  /**
   * Tries to find a name to assign to compartments if compartments don't contain
   * them.
   *
   * @param model model where compartments are placed
   */
  private void assignNamesToCompartments(final Model model) {
    for (final Compartment compartment : model.getCompartments()) {
      if (compartment.getName() == null || compartment.getName().isEmpty()) {
        for (final Layer layer : model.getLayers()) {
          if (layer.getName().equals(CreateHierarchyCommand.TEXT_LAYER_NAME)) {
            LayerText toRemove = null;
            for (final LayerText lt : layer.getTexts()) {
              if (compartment.contains(lt)) {
                compartment.setName(lt.getNotes());
                compartment.setNameX(lt.getX());
                compartment.setNameY(lt.getY());
                compartment.setNameWidth(lt.getWidth());
                compartment.setNameHeight(lt.getHeight());
                compartment.setNameHorizontalAlign(HorizontalAlign.LEFT);
                compartment.setNameVerticalAlign(VerticalAlign.TOP);
                toRemove = lt;
                break;
              }
            }
            if (toRemove != null) {
              layer.removeLayerText(toRemove);
              break;
            }
          }
        }
      }
    }
  }

  /**
   * Creates list of {@link LayerLine} in the model from gpml model.
   *
   * @param graph gpml model
   * @return list of {@link LayerLine}
   */
  private Collection<PolylineData> createLines(final Graph graph) {
    final List<PolylineData> result = new ArrayList<PolylineData>();
    for (final PolylineData pd : graph.getLines()) {
      result.add(pd);
    }
    return result;
  }

  /**
   * By default gpml doesn't offer information about compartments structure. This
   * function fixes assignments to compartment and compartment aliases.
   *
   * @param model model where assignments are fixed.
   */
  private void fixCompartmentAliases(final Model model) {
    final List<Compartment> aliases = model.getCompartments();
    // clear all assignments
    for (final Compartment compartmentAlias : aliases) {
      compartmentAlias.getElements().clear();
    }

    for (final Element element : model.getElements()) {
      // elements inside complexes shouldn't be considered
      if (element instanceof Species) {
        if (((Species) element).getComplex() != null) {
          continue;
        }
      }
      Compartment parentAlias = null;
      for (final Compartment compartmentAlias : aliases) {
        if (compartmentAlias.contains(element)) {
          if (parentAlias == null) {
            parentAlias = compartmentAlias;
          } else if (parentAlias.getSize() > compartmentAlias.getSize()) {
            parentAlias = compartmentAlias;
          }
        }
      }
      if (parentAlias != null) {
        parentAlias.addElement(element);
        element.setCompartment(parentAlias);
      }
    }
  }

  /**
   * Support class to send fewer parameters in functions.
   *
   * @author Jan Badura
   */
  final class Data {
    /**
     * Map between graphId and aliases created from gpml elements.
     */
    private final Map<String, Element> id2alias;

    /**
     * Default layer.
     */
    private final Layer layer;

    /**
     * Default constructor.
     */
    Data() {
      id2alias = new HashMap<>();
      layer = new Layer();
      layer.setVisible(true);
      layer.setLayerId(1);
      layer.setZ(1);
      layer.setName(CreateHierarchyCommand.TEXT_LAYER_NAME);
    }
  }
}
