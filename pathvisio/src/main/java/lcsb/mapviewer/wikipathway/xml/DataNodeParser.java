package lcsb.mapviewer.wikipathway.xml;

import java.awt.geom.Rectangle2D;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.wikipathway.model.DataNode;
import lcsb.mapviewer.wikipathway.model.GpmlLineType;
import lcsb.mapviewer.wikipathway.model.UnknownTypeException;

/**
 * Parser class that creates {@link DataNode} objects from Xml {@link Element
 * node}.
 * 
 * @author Piotr Gawron
 *
 */
public class DataNodeParser extends GraphicalPathwayElementParser<DataNode> {

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  /**
   * PArser used to process references.
   */
  private final ReferenceParser referenceParser;

  private int counter = 0;

  public DataNodeParser(final String mapName) {
    super(mapName);
    referenceParser = new ReferenceParser(mapName);
  }

  @Override
  public DataNode parse(final Element element) throws UnknownTypeException {
    if (!element.getNodeName().equals("DataNode")) {
      throw new InvalidArgumentException(ShapeParser.class.getSimpleName() + " can parse only DataNode xml nodes");
    }

    String id = element.getAttribute("GraphId");
    if (id == null || id.isEmpty()) {
      id = "gen_dn_" + counter++;
      logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, "DataNode", null, getMapName()),
          "Empty GraphId found. Replacing with: " + id);
    }
    DataNode node = new DataNode(id, getMapName());

    for (Pair<String, String> entry : getAttributes(element)) {
      switch (entry.getLeft()) {
        case ("GraphId"):
          break;
        case ("TextLabel"):
          node.setTextLabel(entry.getRight());
          break;
        case ("GroupRef"):
          node.setGroupRef(entry.getRight());
          break;
        case ("Type"):
          node.setType(entry.getRight());
          break;
        default:
          logger.warn(node.getLogMarker(), "Unknown attribute: " + entry.getLeft());
          break;
      }
    }

    NodeList tmpList = element.getChildNodes();
    for (int j = 0; j < tmpList.getLength(); j++) {
      Node tmpNode = tmpList.item(j);
      if (tmpNode.getNodeType() == Node.ELEMENT_NODE) {
        Element childElement = (Element) tmpNode;
        switch (childElement.getNodeName()) {
          case ("Comment"):
            node.addComment(childElement.getTextContent());
            break;
          case ("BiopaxRef"):
            node.addBiopaxReference(childElement.getTextContent());
            break;
          case ("Xref"):
            MiriamData data = referenceParser.parse(childElement, node.getLogMarker());
            if (data != null) {
              node.addReference(data);
            }
            break;
          case ("Graphics"):
            parseGraphics(childElement, node);
            break;
          case ("Attribute"):
            parseAttribute(childElement, node);
            break;
          default:
            logger.warn(node.getLogMarker(),
                "Unknown sub-node of " + element.getNodeName() + " node: " + childElement.getNodeName());
            break;
        }
      }
    }

    return node;
  }

  @Override
  public String toXml(final DataNode node, final LogMarker logMarker) throws ConverterException {
    throw new NotImplementedException();
  }

  @Override
  public String toXml(final Collection<DataNode> list, final LogMarker logMarker) throws ConverterException {
    throw new NotImplementedException();
  }

  /**
   * Parse graphics xml node in the shape node.
   *
   * @param element
   *          xml node with graphics
   * @param shape
   *          shape where data should be added
   * @throws UnknownTypeException
   *           thrown when node contains unknown types
   */
  private void parseGraphics(final Element element, final DataNode shape) throws UnknownTypeException {
    Double centerX = null;
    Double centerY = null;
    Double width = null;
    Double height = null;
    for (Pair<String, String> entry : getAttributes(element)) {
      if (!parseCommonGraphicAttributes(shape, entry)) {
        switch (entry.getLeft()) {
          case ("CenterX"):
            centerX = Double.valueOf(entry.getRight());
            break;
          case ("CenterY"):
            centerY = Double.valueOf(entry.getRight());
            break;
          case ("Width"):
            width = Double.valueOf(entry.getRight());
            break;
          case ("Height"):
            height = Double.valueOf(entry.getRight());
            break;
          case ("ShapeType"):
            if (shape.getType() == null || shape.getType().trim().isEmpty()) {
              shape.setType(entry.getRight());
            }
            break;
          case ("Color"):
            shape.setColor(hexStringToColor(entry.getRight()));
            break;
          case ("FillColor"):
            shape.setFillColor(hexStringToColor(entry.getRight()));
            break;
          case ("ZOrder"):
            shape.setzOrder(Integer.valueOf(entry.getRight()));
            break;
          case ("LineStyle"):
            shape.setLineType(GpmlLineType.getByGpmlName(entry.getRight()).getCorrespondingGlobalLineType());
            break;
          case ("FontSize"):
            shape.setFontSize(Double.valueOf(entry.getRight()));
            break;
          case ("FontWeight"):
            shape.setFontWeight(entry.getRight());
            break;
          case ("Valign"):
            shape.setVerticalAlign(entry.getRight());
            break;
          default:
            logger.warn("Unknown attribute of " + element.getNodeName() + " node: " + entry.getLeft());
            break;
        }
      }
    }
    shape.setRectangle(new Rectangle2D.Double(centerX - width / 2, centerY - height / 2, width, height));
  }

  /**
   * Method that parses {@link DataNode} xml attribute.
   *
   * @param element
   *          xml node with attribute
   * @param shape
   *          shape where data should be added
   */
  private void parseAttribute(final Element element, final DataNode shape) {
    String key = element.getAttribute("Key");
    String value = element.getAttribute("Value");
    switch (key) {
      case ("org.pathvisio.model.BackpageHead"):
        // it's deprecated in PathVisio so we can skip it
        break;
      case ("org.pathvisio.model.GenMAPP-Xref"):
        // skip it when it's empty
        if (!value.isEmpty()) {
          logger.warn(shape.getLogMarker(), "Unknown attribute of node. Key: " + key + "; value: " + value);
          break;
        }
        break;
      default:
        logger.warn(shape.getLogMarker(), "Unknown attribute of node. Key: " + key + "; value: " + value);
        break;
    }
  }

}
