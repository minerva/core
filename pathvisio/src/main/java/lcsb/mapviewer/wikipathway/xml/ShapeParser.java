package lcsb.mapviewer.wikipathway.xml;

import java.awt.font.TextAttribute;
import java.awt.geom.Rectangle2D;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.wikipathway.model.GpmlShape;
import lcsb.mapviewer.wikipathway.model.Shape;
import lcsb.mapviewer.wikipathway.model.UnknownTypeException;

/**
 * Parser class that creates {@link Shape} objects from Xml {@link Element node}
 * .
 * 
 * @author Piotr Gawron
 *
 */
public class ShapeParser extends GraphicalPathwayElementParser<Shape> {

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  public ShapeParser(final String mapName) {
    super(mapName);
  }

  @Override
  public Shape parse(final Element element) throws ConverterException {
    if (!element.getNodeName().equals("Shape")) {
      throw new InvalidArgumentException(ShapeParser.class.getSimpleName() + " can parse only Shape xml nodes");
    }
    Shape shape = new Shape(element.getAttribute("GraphId"), getMapName());
    for (Pair<String, String> entry : getAttributes(element)) {
      switch (entry.getLeft()) {
        case ("GraphId"):
          break;
        case ("TextLabel"):
          shape.setTextLabel(entry.getRight());
          break;
        case ("GroupRef"):
          shape.setGroupRef(entry.getRight());
          break;
        default:
          logger.warn("Unknown attribute of " + element.getNodeName() + " node: " + entry.getLeft());
          break;
      }
    }

    NodeList tmpList = element.getChildNodes();
    for (int j = 0; j < tmpList.getLength(); j++) {
      Node childNode = tmpList.item(j);
      if (childNode.getNodeType() == Node.ELEMENT_NODE) {
        Element childElement = (Element) childNode;
        switch (childElement.getNodeName()) {
          case ("Graphics"):
            parseGraphics(childElement, shape);
            break;
          case ("Attribute"):
            parseAttribute(childElement, shape);
            break;
          case ("Comment"):
            shape.addComment(childElement.getTextContent());
            break;
          case ("BiopaxRef"):
            shape.addBiopaxReference(childElement.getTextContent());
            break;
          default:
            logger.warn("Unknown sub-node of " + element.getNodeName() + " node: " + childElement.getNodeName());
            break;
        }
      }
    }
    return shape;
  }

  @Override
  public String toXml(final Shape node, final LogMarker logMarker) throws ConverterException {
    throw new NotImplementedException();
  }

  @Override
  public String toXml(final Collection<Shape> list, final LogMarker logMarker) throws ConverterException {
    throw new NotImplementedException();
  }

  /**
   * Method that parses {@link Shape} xml attribute.
   *
   * @param element
   *          xml node with attribute
   * @param shape
   *          shape where data should be added
   */
  private void parseAttribute(final Element element, final Shape shape) {
    String key = element.getAttribute("Key");
    String value = element.getAttribute("Value");
    switch (key) {
      case ("org.pathvisio.CellularComponentProperty"):
        shape.setCompartment(true);
        break;
      case ("org.pathvisio.DoubleLineProperty"):
        switch (value) {
          case ("Double"):
            shape.setLineType(LineType.SOLID);
            shape.setCompartment(true);
            break;
          default:
            logger.warn(shape.getLogMarker() + "Unknown line type: " + value);
            break;
        }
        break;
      default:
        logger.warn(shape.getLogMarker() + "Unknown attribute of node. Key:" + key + "; value: " + value);
        break;
    }
  }

  /**
   * Parse graphics xml node in the shape node.
   *
   * @param element
   *          xml node with graphics
   * @param shape
   *          shape where data should be added
   * @throws UnknownTypeException
   *           thrown when some types in the xml node are unknown
   */
  private void parseGraphics(final Element element, final Shape shape) throws UnknownTypeException {
    Double centerX = null;
    Double centerY = null;
    Double width = null;
    Double height = null;
    for (Pair<String, String> entry : getAttributes(element)) {
      if (!parseCommonGraphicAttributes(shape, entry)) {
        switch (entry.getLeft()) {
          case ("CenterX"):
            centerX = Double.valueOf(entry.getRight());
            break;
          case ("CenterY"):
            centerY = Double.valueOf(entry.getRight());
            break;
          case ("Width"):
            width = Double.valueOf(entry.getRight());
            break;
          case ("Height"):
            height = Double.valueOf(entry.getRight());
            break;
          case ("ShapeType"):
            if (entry.getRight() != null && !entry.getRight().isEmpty()) {
              GpmlShape gpmlShape = GpmlShape.stringToGpmlShape(entry.getRight());
              if (gpmlShape != null) {
                shape.setShape(gpmlShape);
              } else {
                logger.warn("Unknown shape type: " + entry.getRight());
                shape.setShape(GpmlShape.UNKNOWN);
              }
            }
            break;
          case ("Color"):
            shape.setColor(hexStringToColor(entry.getRight()));
            break;
          case ("FillColor"):
            shape.setFillColor(hexStringToColor(entry.getRight()));
            break;
          case ("ZOrder"):
            shape.setzOrder(Integer.valueOf(entry.getRight()));
            break;
          case ("FontSize"):
            shape.setFontSize(Double.valueOf(entry.getRight()));
            break;
          case ("LineThickness"):
            shape.setLineThickness(Double.valueOf(entry.getRight()));
            break;
          case ("Valign"):
            shape.setVerticalAlign(entry.getRight());
            break;
          case ("FontWeight"):
            switch (entry.getRight()) {
              case ("Bold"):
                shape.addFontAttribute(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD);
                break;
              default:
                logger.warn("Unknown value of attribute: " + entry.getLeft() + " - " + entry.getRight());
                break;
            }
            break;
          case ("Rotation"):
            shape.setRotation(Double.valueOf(entry.getRight()));
            break;
          default:
            logger.warn("Unknown attribute of " + element.getNodeName() + " node: " + entry.getLeft());
            break;
        }
      }
    }
    shape.setRectangle(new Rectangle2D.Double(centerX - width / 2, centerY - height / 2, width, height));
  }
}
