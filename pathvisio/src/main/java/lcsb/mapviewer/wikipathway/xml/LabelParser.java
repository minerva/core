package lcsb.mapviewer.wikipathway.xml;

import java.awt.geom.Rectangle2D;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.wikipathway.model.Label;
import lcsb.mapviewer.wikipathway.model.UnknownTypeException;

/**
 * Parser class that creates {@link Label} objects from Xml {@link Element node}
 * .
 * 
 * @author Piotr Gawron
 *
 */
public class LabelParser extends GraphicalPathwayElementParser<Label> {

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  private ReferenceParser referenceParser;

  public LabelParser(final String mapName) {
    super(mapName);
    referenceParser = new ReferenceParser(mapName);
  }

  @Override
  public Label parse(final Element element) throws UnknownTypeException {
    if (!element.getNodeName().equals("Label")) {
      throw new InvalidArgumentException(ShapeParser.class.getSimpleName() + " can parse only Label xml nodes");
    }
    Label label = new Label(element.getAttribute("GraphId"), getMapName());

    for (Pair<String, String> entry : getAttributes(element)) {
      switch (entry.getLeft()) {
        case ("GraphId"):
          break;
        case ("TextLabel"):
          label.setTextLabel(entry.getRight());
          break;
        case ("Href"):
          if (entry.getRight() != null && !entry.getRight().isEmpty()) {
            MiriamData md = referenceParser.hrefToMiriamData(entry.getRight());
            if (md == null) {
              logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, "LABEL", label.getGraphId(), getMapName()),
                  "Don't know how to process href: " + entry.getRight());
              label.addComment(entry.getRight());
            } else {
              label.addReference(md);
            }
          }
          break;
        case ("GroupRef"):
          label.setGroupRef(entry.getRight());
          break;
        default:
          logger.warn("Unknown attribute of " + element.getNodeName() + " node: " + entry.getLeft());
          break;
      }
    }

    NodeList tmpList = element.getChildNodes();
    for (int j = 0; j < tmpList.getLength(); j++) {
      Node tmpNode = tmpList.item(j);
      if (tmpNode.getNodeType() == Node.ELEMENT_NODE) {
        Element childElement = (Element) tmpNode;
        switch (childElement.getNodeName()) {
          case ("Comment"):
            label.addComment(childElement.getTextContent());
            break;
          case ("BiopaxRef"):
            label.addBiopaxReference(childElement.getTextContent());
            break;
          case ("Graphics"):
            parseGraphics(childElement, label);
            break;
          case ("Attribute"):
            parseAttribute(childElement, label);
            break;
          default:
            logger.warn("Unknown sub-node of " + element.getNodeName() + " node: " + childElement.getNodeName());
            break;
        }
      }
    }

    return label;
  }

  @Override
  public String toXml(final Label node, final LogMarker logMarker) throws ConverterException {
    throw new NotImplementedException();
  }

  @Override
  public String toXml(final Collection<Label> list, final LogMarker logMarker) throws ConverterException {
    throw new NotImplementedException();
  }

  /**
   * Parse graphics xml node in the shape node.
   *
   * @param element
   *          xml node with graphics
   * @param shape
   *          shape where data should be added
   * @throws UnknownTypeException
   *           thrown when some elements contain unknown typevalues
   */
  protected void parseGraphics(final Element element, final Label shape) throws UnknownTypeException {
    Double centerX = null;
    Double centerY = null;
    Double width = null;
    Double height = null;
    for (Pair<String, String> entry : getAttributes(element)) {
      if (!parseCommonGraphicAttributes(shape, entry)) {
        switch (entry.getLeft()) {
          case ("CenterX"):
            centerX = Double.valueOf(entry.getRight());
            break;
          case ("CenterY"):
            centerY = Double.valueOf(entry.getRight());
            break;
          case ("Width"):
            width = Double.valueOf(entry.getRight());
            break;
          case ("Height"):
            height = Double.valueOf(entry.getRight());
            break;
          case ("Color"):
            shape.setColor(hexStringToColor(entry.getRight()));
            break;
          case ("FillColor"):
            shape.setFillColor(hexStringToColor(entry.getRight()));
            break;
          case ("ZOrder"):
            shape.setzOrder(Integer.valueOf(entry.getRight()));
            break;
          case ("FontSize"):
            shape.setFontSize(Double.valueOf(entry.getRight()));
            break;
          case ("FontWeight"):
            shape.setFontWeight(entry.getRight());
            break;
          case ("Valign"):
            shape.setVerticalAlign(entry.getRight());
            break;
          case ("ShapeType"):
            shape.setShape(entry.getRight());
            break;
          default:
            logger.warn("Unknown attribute of " + element.getNodeName() + " node: " + entry.getLeft() + "; value: "
                + entry.getRight());
            break;
        }
      }
    }
    shape.setRectangle(new Rectangle2D.Double(centerX - width / 2, centerY - height / 2, width, height));
  }

  /**
   * Method that parses {@link Label} xml attribute.
   *
   * @param element
   *          xml node with attribute
   * @param shape
   *          shape where data should be added
   */
  private void parseAttribute(final Element element, final Label shape) {
    String key = element.getAttribute("Key");
    String value = element.getAttribute("Value");
    switch (key) {
      case ("org.pathvisio.model.GenMAPP-Xref"):
        // skip it when it's empty
        if (!value.isEmpty()) {
          logger.warn(shape.getLogMarker() + "Unknown attribute of node. Key: " + key + "; value: " + value);
          break;
        }
        break;
      default:
        logger.warn(shape.getLogMarker() + "Unknown attribute of node. Key:" + key + "; value: " + value);
        break;
    }
  }

}
