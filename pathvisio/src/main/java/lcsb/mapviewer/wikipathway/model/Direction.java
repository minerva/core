package lcsb.mapviewer.wikipathway.model;

/**
 * Class defining directions of the world. They are listed in clockwise order
 * starting from north.
 * 
 * @author Piotr Gawron
 *
 */
public enum Direction {

  /**
   * North.
   */
  NORTH(Axis.NORTH_SOUTH),

  /**
   * East.
   */
  EAST(Axis.EAST_WEST),

  /**
   * South.
   */
  SOUTH(Axis.NORTH_SOUTH),

  /**
   * West.
   */
  WEST(Axis.EAST_WEST),

  /**
   * None.
   */
  NONE(null),
  ;

  /**
   * Defines axis in which direction lies (either {@link Axis#NORTH_SOUTH} or
   * {@link Axis#EAST_WEST}).
   */
  private Axis axis;

  /**
   * Default constructor.
   * 
   * @param axis
   *          {@link #axis}
   */
  Direction(final Axis axis) {
    this.axis = axis;
  }

  /**
   * @return the axis
   * @see #axis
   */
  public Axis getAxis() {
    return axis;
  }

  /**
   * Returns next direction (by 90 degrees) in clockwise order.
   *
   * @return next direction (by 90 degrees) in clockwise order
   */
  public Direction nextClockwiseDirection() {
    Direction result = values()[(this.ordinal() + 1) % values().length];
    if (result == NONE) {
      result = values()[(this.ordinal() + 2) % values().length];
    }
    return result;
  }

  /**
   * Defines axis in which {@link Direction} can lie (either
   * {@link Axis#NORTH_SOUTH} or {@link Axis#EAST_WEST}).
   */
  public enum Axis {
    /**
     * Axis going from NORTH to SOUTH (or from SOUTH to NORTH).
     */
    NORTH_SOUTH,
    /**
     * Axis going from WEST to EAST (or from EAST to WEST).
     */

    EAST_WEST;
  }
}
