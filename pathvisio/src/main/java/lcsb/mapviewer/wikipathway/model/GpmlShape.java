package lcsb.mapviewer.wikipathway.model;

public enum GpmlShape {
  HEXAGON("Hexagon"),
  OVAL("Oval"),
  RECTANGLE("Rectangle"),
  TRIANGLE("Triangle"),
  DEGRADATION("mim-degradation"),
  SACROPLASMIC_RETICULUM("Sarcoplasmic Reticulum"),
  GOLGI_APPARATUS("Golgi Apparatus"),
  ENDOPLASMIC_RETICULUM("Endoplasmic Reticulum"),
  MITOCHONDRIA("Mitochondria"),
  ROUNDED_RECTANGLE("RoundedRectangle"),
  BRACE("Brace"),
  ARC("Arc"),
  NONE("None"),
  PENTAGON("Pentagon"),
  PHOSPHORYLATED("mim-phosphorylated"),
  UNKNOWN(""),
  ;

  private String stringRepresentation;

  GpmlShape(final String stringRepresentation) {
    this.stringRepresentation = stringRepresentation;
  }

  public String getStringRepresentation() {
    return stringRepresentation;
  }

  public static GpmlShape stringToGpmlShape(final String string) {
    for (final GpmlShape shape : GpmlShape.values()) {
      if (shape.getStringRepresentation().equalsIgnoreCase(string)) {
        return shape;
      }
    }
    return null;
  }
}
