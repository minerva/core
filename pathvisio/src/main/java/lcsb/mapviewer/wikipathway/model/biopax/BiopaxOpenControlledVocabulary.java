package lcsb.mapviewer.wikipathway.model.biopax;

import java.io.Serializable;

/**
 * Describes biopax node with open controlled vocabulary (no idea of the
 * purpose).
 * 
 * @author Piotr Gawron
 * 
 */
public class BiopaxOpenControlledVocabulary implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * ???
   */
  private String id;

  /**
   * ???
   */
  private String term;

  /**
   * ???
   */
  private String ontology;

  /**
   * @return the id
   * @see #id
   */
  public String getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(final String id) {
    this.id = id;
  }

  /**
   * @return the term
   * @see #term
   */
  public String getTerm() {
    return term;
  }

  /**
   * @param term
   *          the term to set
   * @see #term
   */
  public void setTerm(final String term) {
    this.term = term;
  }

  /**
   * @return the ontology
   * @see #ontology
   */
  public String getOntology() {
    return ontology;
  }

  /**
   * @param ontology
   *          the ontology to set
   * @see #ontology
   */
  public void setOntology(final String ontology) {
    this.ontology = ontology;
  }
}
