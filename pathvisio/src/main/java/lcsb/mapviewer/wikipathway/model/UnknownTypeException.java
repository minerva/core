package lcsb.mapviewer.wikipathway.model;

import lcsb.mapviewer.converter.ConverterException;

/**
 * Exception that shold be thrown when unknown type was encountered.
 * 
 * @author Piotr Gawron
 * 
 */
public class UnknownTypeException extends ConverterException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor with message passed in the argument.
   * 
   * @param string
   *          message of this exception
   */
  public UnknownTypeException(final String string) {
    super(string);
  }

}
