package lcsb.mapviewer.wikipathway.model;

import java.awt.geom.Rectangle2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;

/**
 * Abstract class for pathway elements. It defines common functionalities for
 * all elements in the model. There are two known subclasses:
 * <ul>
 * <li>{@link GraphicalPathwayElement}, representing elements with some
 * graphical representation</li>
 * <li>{@link Group}, representing just groups of elements</li>
 * </ul>
 * 
 * @author Jan Badura
 * 
 */
public abstract class PathwayElement implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Comment of the element.
   */
  private List<String> comments = new ArrayList<>();

  /**
   * Z order of the element.
   */
  // CHECKSTYLE.OFF
  private Integer zOrder;
  // CHECKSTYLE.ON

  /**
   * Identifier of the element in model.
   */
  private String graphId;

  /**
   * Where this node belongs to.
   */
  private String groupRef;

  /**
   * Reference to BioPax node with references about this element.
   */
  private List<String> biopaxReferences = new ArrayList<>();

  private String mapName;

  /**
   * Default constructor.
   * 
   * @param graphId
   *          {@link #graphId} value
   */
  public PathwayElement(final String graphId, final String mapName) {
    this.graphId = graphId;
    this.mapName = mapName;
  }

  /**
   * Empty constructor that should be used only by serialization tools and
   * subclasses.
   */
  protected PathwayElement() {
  }

  /**
   * 
   * @return {@link #graphId}
   */
  public String getGraphId() {
    return this.graphId;
  }

  /**
   * Returns name of the element.
   *
   * @return name of the element
   */
  abstract String getName();

  /**
   * Return boundary of the element.
   *
   * @return boundary of the element
   */
  abstract Rectangle2D getRectangle();

  public LogMarker getLogMarker() {
    return new LogMarker(ProjectLogEntryType.PARSING_ISSUE, this.getClass().getSimpleName(), getGraphId(),
        getMapName());
  }

  /**
   * @return the biopaxReference
   * @see #biopaxReference
   */
  public List<String> getBiopaxReferences() {
    return biopaxReferences;
  }

  /**
   * @param biopaxReferences
   *          the biopaxReference to set
   * @see #biopaxReference
   */
  public void setBiopaxReferences(final List<String> biopaxReferences) {
    this.biopaxReferences = biopaxReferences;
  }

  /**
   * @return the zOrder
   * @see #zOrder
   */
  public Integer getzOrder() {
    return zOrder;
  }

  /**
   * @param z
   *          the zOrder to set
   * @see #zOrder
   */
  public void setzOrder(final Integer z) {
    this.zOrder = z;
  }

  /**
   * Adds reference to {@link #biopaxReferences}.
   * 
   * @param biopaxString
   *          reference to add
   */
  public void addBiopaxReference(final String biopaxString) {
    biopaxReferences.add(biopaxString);
  }

  public void addBiopaxReferences(final Collection<String> biopaxStrings) {
    biopaxReferences.addAll(biopaxStrings);
  }

  /**
   * @return the comments
   * @see #comments
   */
  public List<String> getComments() {
    return comments;
  }

  /**
   * @param comment
   *          the comment to set
   * @see #comment
   */
  public void addComment(final String comment) {
    this.comments.add(comment);
  }

  public void addComments(final Collection<String> comments) {
    this.comments.addAll(comments);
  }

  public String getMapName() {
    return mapName;
  }

  public void setMapName(final String mapName) {
    this.mapName = mapName;
  }

  public String getGroupRef() {
    return groupRef;
  }

  public void setGroupRef(final String groupRef) {
    this.groupRef = groupRef;
  }

}
