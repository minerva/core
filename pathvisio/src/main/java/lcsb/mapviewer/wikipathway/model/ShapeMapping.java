package lcsb.mapviewer.wikipathway.model;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.compartment.BottomSquareCompartment;
import lcsb.mapviewer.model.map.compartment.LeftSquareCompartment;
import lcsb.mapviewer.model.map.compartment.OvalCompartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.compartment.RightSquareCompartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.compartment.TopSquareCompartment;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

public enum ShapeMapping {
  ANTISENSE_RNA(AntisenseRna.class, GpmlShape.RECTANGLE),
  COMPLEX(Complex.class, GpmlShape.RECTANGLE),
  DEGRADED(Degraded.class, GpmlShape.DEGRADATION),
  DRUG(Drug.class, GpmlShape.OVAL),
  GENE(Gene.class, GpmlShape.RECTANGLE),
  ION(Ion.class, GpmlShape.OVAL),
  PHENOTYPE(Phenotype.class, GpmlShape.HEXAGON),
  PROTEIN(Protein.class, GpmlShape.RECTANGLE),
  RNA(Rna.class, GpmlShape.RECTANGLE),
  SIMPLE_MOLECULE(SimpleMolecule.class, GpmlShape.OVAL),
  UNKNOWN(Unknown.class, GpmlShape.OVAL),

  MODIFICATION_RESIDUE(ModificationResidue.class, GpmlShape.OVAL),

  BOTTOM_SQUARE_COMPARTMENT(BottomSquareCompartment.class, GpmlShape.RECTANGLE),
  TOP_SQUARE_COMPARTMENT(TopSquareCompartment.class, GpmlShape.RECTANGLE),
  RIGHT_SQUARE_COMPARTMENT(LeftSquareCompartment.class, GpmlShape.RECTANGLE),
  LEFT_SQUARE_COMPARTMENT(RightSquareCompartment.class, GpmlShape.RECTANGLE),
  PATHWAY_COMPARTMENT(PathwayCompartment.class, GpmlShape.RECTANGLE),
  SQUARE_COMPARTMENT(SquareCompartment.class, GpmlShape.RECTANGLE),
  
  OVAL_COMPARTMENT(OvalCompartment.class, GpmlShape.OVAL);

  private GpmlShape gpmlShape;

  private Class<?> minervaClass;

  ShapeMapping(final Class<?> clazz, final GpmlShape shape) {
    this.gpmlShape = shape;
    this.minervaClass = clazz;
  }

  public static GpmlShape getShape(final Class<?> clazz) {
    for (final ShapeMapping mapping: ShapeMapping.values()) {
      if (mapping.minervaClass.isAssignableFrom(clazz)) {
        return mapping.gpmlShape;
      }
    }
    throw new InvalidArgumentException("Don't know how to handle class: " + clazz);
  }
}
