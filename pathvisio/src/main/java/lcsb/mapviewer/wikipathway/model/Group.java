package lcsb.mapviewer.wikipathway.model;

import java.awt.geom.Rectangle2D;
import java.util.HashSet;
import java.util.Set;

/**
 * Class used to store data from Group from GPML.
 * 
 * @author Jan Badura
 * 
 */
public class Group extends PathwayElement {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Margin used when retrieving rectangle border of the group (result rectangle
   * will by wider and higher in every direction by this value).
   */
  private static final int RECTANGLE_BORDER_MARGIN = 8;

  /**
   * Identifier of the group.
   */
  private String groupId;

  /**
   * Style of the group.
   */
  private String style;

  /**
   * Style of the group.
   */
  private String textLabel;

  /**
   * Elements in the group.
   */
  private Set<PathwayElement> nodes;
  private Set<Rectangle2D> additionalBounds = new HashSet<>();

  /**
   * Default constructor.
   * 
   * @param graphId
   *          graph identifier in the model
   * @param groupId
   *          {@link #groupId}
   */
  public Group(final String graphId, final String groupId, final String mapName) {
    super(graphId, mapName);
    this.groupId = groupId;
    nodes = new HashSet<>();
  }

  /**
   * Empty constructor that should be used only by serialization tools and
   * subclasses.
   */
  protected Group() {
  }

  @Override
  public String getName() {
    if (getTextLabel() == null) {
      return getGraphId();
    }
    return getTextLabel();
  }

  @Override
  public Rectangle2D getRectangle() {

    Rectangle2D result = null;
    boolean first = true;

    for (final PathwayElement n : nodes) {
      if (first) {
        result = (Rectangle2D) n.getRectangle().clone();
        first = false;
      }
      Rectangle2D.union(result, n.getRectangle(), result);
    }
    for (final Rectangle2D bounds : additionalBounds) {
      if (first) {
        result = (Rectangle2D) bounds.clone();
        first = false;
      }
      Rectangle2D.union(result, bounds, result);
    }
    if (result == null) {
      return null;
    }
    result.setRect(
        result.getX() - RECTANGLE_BORDER_MARGIN, result.getY() - RECTANGLE_BORDER_MARGIN,
        result.getWidth() + 2 * RECTANGLE_BORDER_MARGIN, result.getHeight()
            + 2 * RECTANGLE_BORDER_MARGIN);
    return result;
  }

  /**
   * 
   * @return {@link #groupId}
   */
  public String getGroupId() {
    return groupId;
  }

  /**
   * Adds element to group.
   * 
   * @param pe
   *          object to add
   */
  public void addNode(final PathwayElement pe) {
    nodes.add(pe);
  }

  /**
   * 
   * @return {@link #nodes}
   */
  public Set<PathwayElement> getNodes() {
    return this.nodes;
  }

  /**
   * @return the style
   * @see #style
   */
  public String getStyle() {
    return style;
  }

  /**
   * @param style
   *          the style to set
   * @see #style
   */
  public void setStyle(final String style) {
    this.style = style;
  }

  public void addBounds(final Rectangle2D bounds) {
    this.additionalBounds.add(bounds);
  }

  public String getTextLabel() {
    return textLabel;
  }

  public void setTextLabel(final String textLabel) {
    this.textLabel = textLabel;
  }

}
