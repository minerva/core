package lcsb.mapviewer.wikipathway.model;

/**
 * Defines how lines are drawn in gpml format.
 * 
 * @author Piotr Gawron
 *
 */
public enum GpmlLineConnectorType {

  /**
   * Line is a curve.
   */
  CURVED("Curved"),

  /**
   * Line is constructed from list of perpendicular segments.
   */
  ELBOW("Elbow"),

  /**
   * It's a straight line from start point to the end point.
   */
  STRAIGHT("Straight"),

  /**
   * Line is constructoed from list of segement lines.
   */
  SEGMENTED("Segmented");

  /**
   * String used in gpml to identify this specific connection type.
   */
  private String gpmlName;

  /**
   * Default constructor.
   * 
   * @param gpmlName
   *          {@link #gpmlName}
   */
  GpmlLineConnectorType(final String gpmlName) {
    this.gpmlName = gpmlName;
  }

  /**
   * Returns {@link GpmlLineConnectorType type} identified by {@link #gpmlName
   * gpml string} identifing the type.
   *
   * @param gpmlName
   *          {@link #gpmlName}
   * @return {@link GpmlLineConnectorType type} identified by {@link #gpmlName
   *         gpml string} identifing the type
   * @throws UnknownTypeException
   *           thrown when type cannot be found
   */
  public static GpmlLineConnectorType getByGpmlName(final String gpmlName) throws UnknownTypeException {
    for (final GpmlLineConnectorType type : values()) {
      if (type.getGpmlName().equals(gpmlName)) {
        return type;
      }
    }
    throw new UnknownTypeException("Unknown type name: " + gpmlName);
  }

  /**
   * @return the gpmlName
   * @see #gpmlName
   */
  public String getGpmlName() {
    return gpmlName;
  }
}
