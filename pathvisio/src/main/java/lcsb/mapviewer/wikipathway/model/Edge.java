package lcsb.mapviewer.wikipathway.model;

import java.awt.Color;
import java.awt.geom.Line2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.MiriamData;

/**
 * Support class for Interaction between elements. It stores data parsed from
 * GPML.
 * 
 * @author Jan Badura
 * @author Piotr Gawron
 * 
 */
public class Edge implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Epsilon value used for comparison of doubles.
   */
  private static final Double EPSILON = 1e-6;

  /**
   * Default class logger.
   */
  private final transient Logger logger = LogManager.getLogger();

  /**
   * Identifier in GPML model.
   */
  private String graphId;

  /**
   * In which group this edge is located.
   */
  private String groupRef;

  /**
   * Where this {@link Edge} starts (in which {@link Edge}/{@link Interaction}).
   */
  private String start;

  /**
   * Where this {@link Edge} ends (in which {@link Edge}/{@link Interaction}).
   */
  private String end;

  /**
   * Z-order of the line (how much to front it should be located).
   */
  // CHECKSTYLE.OFF
  private Integer zOrder;
  // CHECKSTYLE.ON

  /**
   * Line representing this edge.
   */
  private PolylineData line = new PolylineData();

  /**
   * GPML interaction type (arrow).
   */
  private GpmlInteractionType type = GpmlInteractionType.LINE;

  private boolean reversibleReaction = false;

  /**
   * References for given edge.
   */
  private List<MiriamData> references = new ArrayList<>();

  /**
   * List of anchors placed on the edge.
   */
  private Set<String> anchors = new HashSet<>();

  /**
   * Comments.
   */
  private List<String> comments = new ArrayList<>();

  /**
   * List of identifiers used by BioPax nodes that are inside GPML. These BioPax
   * nodes contain annotations.
   */
  private List<String> biopaxReferences = new ArrayList<>();

  private String mapName;

  /**
   * Default constructor.
   * 
   * @param graphId
   *          {@link #graphId}
   */
  public Edge(final String graphId, final String mapName) {
    this.graphId = graphId;
    this.mapName = mapName;
  }

  /**
   * Creates new instance that contains the same data as the parameter object.
   * 
   * @param original
   *          original object from which data is being copied
   */
  public Edge(final Edge original) {
    this.graphId = original.getGraphId();
    this.groupRef = original.getGroupRef();
    this.start = original.getStart();
    this.end = original.getEnd();
    this.line = new PolylineData(original.getLine());
    this.type = original.getType();
    for (final MiriamData md : original.getReferences()) {
      addReference(new MiriamData(md));
    }
    this.anchors.addAll(original.getAnchors());
    this.comments.addAll(original.getComments());
    this.biopaxReferences.addAll(original.getBiopaxReferences());
    this.zOrder = original.getzOrder();
    this.reversibleReaction = original.isReversibleReaction();
    this.mapName = original.getMapName();
  }

  /**
   * Empty constructor that should be used only by serialization tools and
   * subclasses.
   */
  protected Edge() {
  }

  /**
   * Adds reference to the object.
   * 
   * @param reference
   *          reference to add
   */
  public void addReference(final MiriamData reference) {
    references.add(reference);
  }

  /**
   * Adds anchor to {@link #anchors}.
   * 
   * @param anchor
   *          object to add
   */
  public void addAnchor(final String anchor) {
    this.anchors.add(anchor);
  }

  /**
   * @return the graphId
   * @see #graphId
   */
  public String getGraphId() {
    return graphId;
  }

  /**
   * @param graphId
   *          the graphId to set
   * @see #graphId
   */
  public void setGraphId(final String graphId) {
    this.graphId = graphId;
  }

  /**
   * @return the start
   * @see #start
   */
  public String getStart() {
    return start;
  }

  /**
   * @param start
   *          the start to set
   * @see #start
   */
  public void setStart(final String start) {
    this.start = start;
  }

  /**
   * @return the end
   * @see #end
   */
  public String getEnd() {
    return end;
  }

  /**
   * @param end
   *          the end to set
   * @see #end
   */
  public void setEnd(final String end) {
    this.end = end;
  }

  /**
   * @return the line
   * @see #line
   */
  public PolylineData getLine() {
    return line;
  }

  /**
   * @param line
   *          the line to set
   * @see #line
   */
  public void setLine(final PolylineData line) {
    this.line = line;
  }

  /**
   * @return the anchors
   * @see #anchors
   */
  public Set<String> getAnchors() {
    return anchors;
  }

  /**
   * @param anchors
   *          the anchors to set
   * @see #anchors
   */
  public void setAnchors(final Set<String> anchors) {
    this.anchors = anchors;
  }

  /**
   * @return the comments
   * @see #comments
   */
  public List<String> getComments() {
    return comments;
  }

  /**
   * @param comment
   *          the comment to add
   * @see #comment
   */
  public void addComment(final String comment) {
    this.comments.add(comment);
  }

  /**
   * @return the biopaxReferences
   * @see #biopaxReferences
   */
  public List<String> getBiopaxReferences() {
    return biopaxReferences;
  }

  /**
   * @param biopaxReferences
   *          the biopaxReferences to set
   * @see #biopaxReferences
   */
  public void setBiopaxReferences(final List<String> biopaxReferences) {
    this.biopaxReferences = biopaxReferences;
  }

  /**
   * Returns a prefix that should be used in warnings.
   * 
   * @return string that identifies element in logs
   */
  public LogMarker getLogMarker() {
    return new LogMarker(ProjectLogEntryType.PARSING_ISSUE, getType().toString(), getGraphId(), getMapName());
  }

  private String getMapName() {
    return mapName;
  }

  /**
   * @return the type
   * @see #type
   */
  public GpmlInteractionType getType() {
    return type;
  }

  /**
   * @param type
   *          the type to set
   * @see #type
   */
  public void setType(final GpmlInteractionType type) {
    if (type != null) {
      this.type = type;
    }
  }

  /**
   * Reverse direction of edge (order of elements).
   * 
   * @return copy of the object that is reversed
   */
  public Edge reverse() {
    Edge result = new Edge(this);
    result.setLine(result.getLine().reverse());
    String tmp = result.getStart();
    result.setStart(result.getEnd());
    result.setEnd(tmp);

    return result;
  }

  /**
   * @return the references
   * @see #references
   */
  public List<MiriamData> getReferences() {
    return references;
  }

  /**
   * @param references
   *          the references to set
   * @see #references
   */
  public void setReferences(final List<MiriamData> references) {
    this.references = references;
  }

  /**
   * Adds set of references.
   * 
   * @param references
   *          references to add
   */
  public void addReferences(final List<MiriamData> references) {
    for (final MiriamData miriamData : references) {
      addReference(miriamData);
    }
  }

  /**
   * This method extend edge (the line) by adding data from the edge in parameter.
   * 
   * @param edge2
   *          edge that should be used for extension
   */
  public void extend(final Edge edge2) {
    // check if extension make sense (they should point to the same identifier)
    if (!Objects.equals(getEnd(), edge2.getStart())) {
      throw new InvalidArgumentException(" Cannot merge - anchor points doesn't match: " + getEnd()
          + "," + edge2.getStart());
    }

    int mergePoint = line.getLines().size() - 1;

    while (mergePoint >= 0) {
      if (line.getLines().get(mergePoint).getP2().distance(edge2.getLine().getStartPoint()) < EPSILON) {
        break;
      }
      mergePoint--;
    }
    if (mergePoint < 0) {
      throw new InvalidArgumentException(" Cannot merge - edges are far from each other: "
          + line + ","
          + edge2.getLine());
    }
    while (mergePoint < line.getLines().size() - 1) {
      line.removeLine(line.getLines().size() - 1);
    }

    // graphId should be the same

    // start should be taken from the original

    // end should be taken from the extension
    setEnd(edge2.getEnd());

    // add points
    for (final Line2D l : edge2.getLine().getLines()) {
      line.addLine(l.getP1(), l.getP2());
    }

    // skip type

    for (final MiriamData md : edge2.getReferences()) {
      addReference(new MiriamData(md));
    }
    anchors.addAll(edge2.getAnchors());
    List<String> toAdd = new ArrayList<>();
    for (final String comment : edge2.getComments()) {
      if (!comments.contains(comment)) {
        toAdd.add(comment);
      }
    }
    comments.addAll(toAdd);

    // color should be skipped
    if (!getColor().equals(edge2.getColor())) {
      logger.warn(getLogMarker(), "Problem with merging. Edges have different colors.");
    }

    // add biopax references
    biopaxReferences.addAll(edge2.getBiopaxReferences());

  }

  /**
   * @return the color
   * @see #color
   */
  public Color getColor() {
    return line.getColor();
  }

  /**
   * @param color
   *          the color to set
   * @see #color
   */
  public void setColor(final Color color) {
    if (color != null) {
      line.setColor(color);
    }
  }

  /**
   * @return the zOrder
   * @see #zOrder
   */
  public Integer getzOrder() {
    return zOrder;
  }

  /**
   * @param z
   *          the zOrder to set
   * @see #zOrder
   */
  public void setzOrder(final Integer z) {
    this.zOrder = z;
  }

  /**
   * Adds element to {@link #biopaxReferences}.
   * 
   * @param reference
   *          reference to add
   */
  public void addBiopaxReference(final String reference) {
    biopaxReferences.add(reference);

  }

  /**
   * @return the groupRef
   * @see #groupRef
   */
  public String getGroupRef() {
    return groupRef;
  }

  /**
   * @param groupRef
   *          the groupRef to set
   * @see #groupRef
   */
  public void setGroupRef(final String groupRef) {
    this.groupRef = groupRef;
  }

  public boolean isReversibleReaction() {
    return reversibleReaction;
  }

  public void setReversibleReaction(final boolean reversibleReaction) {
    this.reversibleReaction = reversibleReaction;
  }

}
