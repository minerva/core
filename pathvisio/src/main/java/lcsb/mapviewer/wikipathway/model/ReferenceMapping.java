package lcsb.mapviewer.wikipathway.model;

import lcsb.mapviewer.model.map.MiriamType;

/**
 * This enum lists mapping between strings representing annotations in gpml into
 * {@link MiriamType}.
 * 
 * @author Piotr Gawron
 *
 */
public enum ReferenceMapping {

  BRENDA("BRENDA", MiriamType.BRENDA),

  /**
   * {@link MiriamType#CAS}.
   */
  CAS("CAS", MiriamType.CAS),

  /**
   * {@link MiriamType#CHEBI}.
   */
  CHEBI("ChEBI", MiriamType.CHEBI),

  /**
   * {@link MiriamType#CHEMBL_COMPOUND}.
   */
  CHEMBL_COMPOUND("ChEMBL compound", MiriamType.CHEMBL_COMPOUND),

  /**
   * {@link MiriamType#CHEMBL_TARGET}.
   */
  CHEMBL_TARGET(null, MiriamType.CHEMBL_TARGET),

  CHEM_ID_PLUS("ChemIDplus", MiriamType.CHEM_ID_PLUS),

  /**
   * {@link MiriamType#CHEMSPIDER}.
   */
  CHEMSPIDER("ChemSpider", MiriamType.CHEMSPIDER),

  DRUGBANK("DrugBank", MiriamType.DRUGBANK),

  /**
   * {@link MiriamType#EC}.
   */
  EC("Enzyme Nomenclature", MiriamType.EC),
  ECOGENE("EcoGene", MiriamType.ECOGENE),

  /**
   * {@link MiriamType#ENSEMBL}.
   */
  ENSEMBL("Ensembl", MiriamType.ENSEMBL),
  ENSEMBL_PLANTS("Ensembl Plants", MiriamType.ENSEMBL_PLANTS),

  /**
   * {@link MiriamType#ENTREZ}.
   */
  ENTREZ_GENE("Entrez Gene", MiriamType.ENTREZ),
  ENTREZ("Entrez", MiriamType.ENTREZ),
  GEN_BANK("GenBank", MiriamType.ENTREZ),

  
  FLYBASE("FlyBase", MiriamType.FLYBASE),


  GENE_DB("GeneDB", MiriamType.GENE_DB),

  /**
   * {@link MiriamType#GO}.
   */
  GO("GeneOntology", MiriamType.GO),

  /**
   * {@link MiriamType#HGNC}.
   */
  HGNC("HGNC Accession number", MiriamType.HGNC),

  /**
   * {@link MiriamType#HGNC_SYMBOL}.
   */
  HGNC_SYMBOL("HGNC", MiriamType.HGNC_SYMBOL),

  /**
   * {@link MiriamType#HMDB}.
   */
  HMDB("HMDB", MiriamType.HMDB),

  INTACT("IntAct", MiriamType.INTACT),

  /**
   * {@link MiriamType#INTERPRO}.
   */
  INTERPRO("InterPro", MiriamType.INTERPRO),

  KNAPSACK("KNApSAcK", MiriamType.KNAPSACK),

  LIPID_MAPS("LIPID_MAPS", MiriamType.LIPID_MAPS),

  /**
   * {@link MiriamType#MESH_2012}.
   */
  MESH_2012(null, MiriamType.MESH_2012),

  /**
   * {@link MiriamType#MI_RBASE_SEQUENCE}.
   */
  MI_RBASE_SEQUENCE2("miRBase", MiriamType.MI_R_BASE_SEQUENCE),

  /**
   * {@link MiriamType#KEGG_COMPOUND}.
   */
  KEGG_COMPOUND("KEGG Compound", MiriamType.KEGG_COMPOUND),

  /**
   * {@link MiriamType#KEGG_COMPOUND}.
   */
  KEGG_DRUG("KEGG Drug", MiriamType.KEGG_DRUG),

  /**
   * {@link MiriamType#KEGG_GENES}.
   */
  KEGG_GENES("KEGG Genes", MiriamType.KEGG_GENES),

  /**
   * {@link MiriamType#KEGG_ORTHOLOGY}.
   */
  KEGG_ORTHOLOGY("Kegg ortholog", MiriamType.KEGG_ORTHOLOGY),

  /**
   * {@link MiriamType#KEGG_PATHWAY}.
   */
  KEGG_PATHWAY("KEGG Pathway", MiriamType.KEGG_PATHWAY),

  /**
   * {@link MiriamType#KEGG_REACTION}.
   */
  KEGG_REACTION("KEGG Reaction", MiriamType.KEGG_REACTION),

  /**
   * {@link MiriamType#MGD}.
   */
  MGD("MGI", MiriamType.MGD),

  /**
   * {@link MiriamType#MI_RBASE_SEQUENCE}.
   */
  MI_RBASE_SEQUENCE("miRBase Sequence", MiriamType.MI_R_BASE_SEQUENCE),
  
  NCBI_PROTEIN("NCBI Protein", MiriamType.NCBI_PROTEIN),

  
  /**
   * {@link MiriamType#MI_RBASE_SEQUENCE}.
   */
  MI_R_BASE_MATURE_SEQUENCE("miRBase mature sequence", MiriamType.MI_R_BASE_MATURE_SEQUENCE),

  OMIM("OMIM", MiriamType.OMIM),

  /**
   * {@link MiriamType#PANTHER}.
   */
  PANTHER(null, MiriamType.PANTHER),

  PATO("pato", MiriamType.PATO),

  /**
   * {@link MiriamType#PFAM}.
   */
  PFAM("Pfam", MiriamType.PFAM),

  /**
   * {@link MiriamType#PHARM}.
   */
  PHARM("PharmGKB Pathways", MiriamType.PHARM),

  /**
   * {@link MiriamType#PUBCHEM}.
   */
  PUBCHEM_COMPUND("PubChem-compound", MiriamType.PUBCHEM),

  PUBCHEM_COMPUND2("PubChem Compound", MiriamType.PUBCHEM),

  PUBCHEM_COMPUND_OLD("PubChem", MiriamType.PUBCHEM),

  /**
   * {@link MiriamType#PUBCHEM_SUBSTANCE}.
   */
  PUBCHEM_SUBSTANCE("PubChem-substance", MiriamType.PUBCHEM_SUBSTANCE),

  /**
   * {@link MiriamType#REACTOME}.
   */
  REACTOME("Reactome", MiriamType.REACTOME),

  /**
   * {@link MiriamType#REFSEQ}.
   */
  REFSEQ("RefSeq", MiriamType.REFSEQ),
  RHEA("Rhea", MiriamType.RHEA),

  RGD("RGD", MiriamType.RGD),

  /**
   * {@link MiriamType#SGD }.
   */
  SGD("SGD", MiriamType.SGD),

  SPIKE("SPIKE", MiriamType.SPIKE),
  STRING("STRING", MiriamType.STRING),

  /**
   * {@link MiriamType#TAIR_LOCUS}.
   */
  TAIR_LOCUS("TAIR", MiriamType.TAIR_LOCUS),

  TTD_DRUG("TTD Drug", MiriamType.TTD_Drug),

  UNIGENE("UniGene", MiriamType.UNIGENE),

  /**
   * {@link MiriamType#UNIPROT}.
   */
  UNIPROT("Uniprot-TrEMBL", MiriamType.UNIPROT),

  /**
   * {@link MiriamType#UNIPROT_ISOFORM}.
   */
  UNIPROT_ISOFORM(null, MiriamType.UNIPROT_ISOFORM),

  /**
   * {@link MiriamType#WIKIPATHWAYS}.
   */
  WIKIPATHWAYS("Wikipathways", MiriamType.WIKIPATHWAYS),

  /**
   * {@link MiriamType#WIKIPEDIA}.
   */
  WIKIDATA("Wikidata", MiriamType.WIKIDATA),
  WIKIPEDIA("Wikipedia", MiriamType.WIKIPEDIA),
  WORM_BASE("WormBase", MiriamType.WORM_BASE),
  ZFIN("ZFIN", MiriamType.ZFIN),
  ;

  /**
   * Gpml string representing specific database type.
   */
  private String gpmlString;

  /**
   * {@link MiriamType} corresponding to {@link #gpmlString}.
   */
  private MiriamType type;

  /**
   * Default constructor.
   * 
   * @param gpmlString
   *          {@link #getGpmlString()}
   * @param type
   *          {@link #type}
   */
  ReferenceMapping(final String gpmlString, final MiriamType type) {
    this.type = type;
    this.gpmlString = gpmlString;
  }

  /**
   * Returns {@link ReferenceMapping mapping} to {@link MiriamType} that should be
   * used for gpml resource type.
   *
   * @param gpmlString
   *          {@link #gpmlString}
   * @return {@link ReferenceMapping mapping} to {@link MiriamType} that should be
   *         used for gpml resource type
   */
  public static ReferenceMapping getMappingByGpmlString(final String gpmlString) {
    for (final ReferenceMapping mm : values()) {
      if (mm.getGpmlString() != null && mm.getGpmlString().equalsIgnoreCase(gpmlString)) {
        return mm;
      }
    }
    return null;
  }

  /**
   * Returns {@link ReferenceMapping mapping} that should be used for
   * {@link #gpmlString gpml string} representing type of annotation.
   *
   * @param dataType
   *          {@link #type}
   *
   * @return {@link ReferenceMapping mapping} that should be used for
   *         {@link #gpmlString gpml string} representing type of annotation
   */
  public static ReferenceMapping getMappingByMiriamType(final MiriamType dataType) {
    for (final ReferenceMapping mm : values()) {
      if (mm.getType().equals(dataType)) {
        return mm;
      }
    }
    return null;
  }

  /**
   * @return the gpmlString
   * @see #gpmlString
   */
  public String getGpmlString() {
    return gpmlString;
  }

  /**
   * @return the type
   * @see #type
   */
  public MiriamType getType() {
    return type;
  }

}
