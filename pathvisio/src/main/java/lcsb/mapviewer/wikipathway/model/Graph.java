package lcsb.mapviewer.wikipathway.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.wikipathway.model.biopax.BiopaxData;

/**
 * This class models nodes and reaction from PathVisio for easy(?) conversion to
 * Model.
 * 
 * @author Jan Badura
 * 
 */
public class Graph implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default width of the graphic model.
   */
  private static final double DEFAULT_MODEL_WIDTH = 1024.0;

  /**
   * Default height of the graphic model.
   */
  private static final double DEFAULT_MODEL_HEIGHT = 1024.0;

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Height of the model.
   */
  private double boardHeight;

  /**
   * Information about BioPax data in the GPML file.
   */
  private BiopaxData biopaxData;

  /**
   * Width of the model.
   */
  private double boardWidth;

  /**
   * All {@link PathwayElement nodes} in the model identified by the
   * {@link PathwayElement#graphId}.
   */
  private Map<String, PathwayElement> nodes;

  /**
   * All {@link Group groups} in the model identified by the
   * {@link PathwayElement#graphId}.
   */
  private Map<String, Group> groupsByGraphId;

  /**
   * All {@link Group groups} in the model identified by the
   * {@link Group#groupId}.
   */
  private Map<String, Group> groupsByGroupId;

  /**
   * All {@link DataNode data nodes} in the model identified by the
   * {@link PathwayElement#graphId}.
   */
  private Map<String, DataNode> dataNodes;

  /**
   * All {@link Label labvels} in the model identified by the
   * {@link PathwayElement#graphId}.
   */
  private Map<String, Label> labels;

  /**
   * All {@link Shape shapes} in the model identified by the
   * {@link PathwayElement#graphId}.
   */
  private Map<String, Shape> shapes;

  /**
   * All {@link Edge edges} in the model identified by the {@link Edge#graphId}.
   */
  private Map<String, Edge> edges;

  /**
   * {@link Edge Edges} in the model identified by anchors .
   */
  private Map<String, Edge> anchorToEdge;

  /**
   * All {@link Interaction interactions} in the model identified by the
   * {@link Interaction#graphId}.
   */
  private Map<String, Interaction> interactions;

  /**
   * Comments on the model.
   */
  private List<String> comments = new ArrayList<>();

  /**
   * Pathvisio attributes of the model.
   */
  private Map<String, String> attributes = new HashMap<String, String>();

  /**
   * List of identifiers used by biopax nodes that are inside gpml. These biopax
   * nodes contain annotations.
   */
  private List<String> biopaxReferences = new ArrayList<String>();

  /**
   * List of lines in gpml model.
   */
  private List<PolylineData> lines = new ArrayList<PolylineData>();

  /**
   * List of states in gpml model.
   */
  private List<State> states = new ArrayList<State>();

  private String mapName;

  private List<Author> authors = new ArrayList<>();

  private Calendar lastModified;

  /**
   * Default constructor.
   */
  public Graph() {
    boardHeight = DEFAULT_MODEL_HEIGHT;
    boardWidth = DEFAULT_MODEL_WIDTH;
    nodes = new HashMap<String, PathwayElement>();
    groupsByGraphId = new HashMap<String, Group>();
    groupsByGroupId = new HashMap<String, Group>();
    dataNodes = new HashMap<String, DataNode>();
    labels = new HashMap<String, Label>();
    shapes = new HashMap<String, Shape>();
    edges = new HashMap<String, Edge>();
    anchorToEdge = new HashMap<String, Edge>();
    interactions = new HashMap<String, Interaction>();
  }

  /**
   * Adds {@link Group} to model.
   * 
   * @param group
   *          object to add
   */
  public void addGroup(final Group group) {
    groupsByGraphId.put(group.getGraphId(), group);
    groupsByGroupId.put(group.getGroupId(), group);
    nodes.put(group.getGraphId(), group);
  }

  /**
   * Adds {@link DataNode} to model.
   * 
   * @param node
   *          object to add
   */
  public void addDataNode(final DataNode node) {
    dataNodes.put(node.getGraphId(), node);
    nodes.put(node.getGraphId(), node);
    if (node.getGroupRef() != null && !node.getGroupRef().equals("")) {
      if (groupsByGroupId.get(node.getGroupRef()) == null) {
        logger.warn(node.getLogMarker(), "Missing group: " + node.getGroupRef());
      } else {
        groupsByGroupId.get(node.getGroupRef()).addNode(node);
      }
    }
  }

  /**
   * Adds {@link Label} to model.
   * 
   * @param label
   *          object to add
   */
  public void addLabel(final Label label) {
    labels.put(label.getGraphId(), label);
    nodes.put(label.getGraphId(), label);
    if (label.getGroupRef() != null && !label.getGroupRef().equals("")) {
      groupsByGroupId.get(label.getGroupRef()).addNode(label);
    }
  }

  /**
   * Adds {@link Shape} to model.
   * 
   * @param shape
   *          object to add
   */
  public void addShape(final Shape shape) {
    shapes.put(shape.getGraphId(), shape);
    nodes.put(shape.getGraphId(), shape);
    if (shape.getGroupRef() != null && !shape.getGroupRef().equals("")) {
      groupsByGroupId.get(shape.getGroupRef()).addNode(shape);
    }
  }

  /**
   * Adds {@link Edge} to model.
   * 
   * @param edge
   *          object to add
   */
  public void addEdge(final Edge edge) {
    edges.put(edge.getGraphId(), edge);
    for (final String anchor : edge.getAnchors()) {
      anchorToEdge.put(anchor, edge);
    }
  }

  /**
   * Adds {@link Interaction} to model.
   * 
   * @param interaction
   *          object to add
   */
  public void addInteraction(final Interaction interaction) {
    interactions.put(interaction.getGraphId(), interaction);
  }

  /**
   * Returns {@link PathwayElement} identified by {@link PathwayElement#graphId}
   * .
   * 
   * @param graphId
   *          identifier
   * @return {@link PathwayElement} identified by {@link PathwayElement#graphId}
   */
  public PathwayElement getNodeByGraphId(final String graphId) {
    return nodes.get(graphId);
  }

  /**
   * Returns collection of {@link Group groups} in the model.
   * 
   * @return collection of {@link Group groups} in the model
   */
  public Collection<Group> getGroups() {
    return groupsByGraphId.values();
  }

  /**
   * Returns {@link Group} identified by {@link Group#groupId}.
   * 
   * @param groupId
   *          identifier
   * @return {@link Group} identified by {@link Group#groupId}
   */
  public Group getGroupByGroupId(final String groupId) {
    return groupsByGroupId.get(groupId);
  }

  /**
   * Returns set of {@link DataNode data nodes} in model.
   * 
   * @return set of {@link DataNode data nodes} in model
   */
  public Collection<DataNode> getDataNodes() {
    return dataNodes.values();
  }

  /**
   * Returns set of {@link Label labels} in model.
   * 
   * @return set of {@link Label labels} in model.
   */
  public Collection<Label> getLabels() {
    return labels.values();
  }

  /**
   * Returns {@link Label} identified by {@link PathwayElement#graphId}.
   * 
   * @param graphId
   *          identifier
   * @return {@link Label} identified by {@link PathwayElement#graphId}.
   */
  public Label getLabelByGraphId(final String graphId) {
    return labels.get(graphId);
  }

  /**
   * Returns set of {@link Shape shapes} in the model.
   * 
   * @return set of {@link Shape shapes} in the model.
   */
  public Collection<Shape> getShapes() {
    return shapes.values();
  }

  /**
   * Returns {@link Shape} identified by {@link PathwayElement#graphId}.
   * 
   * @param graphId
   *          identifier
   * @return {@link Shape} identified by {@link PathwayElement#graphId}
   */
  public Shape getShapeByGraphId(final String graphId) {
    return shapes.get(graphId);
  }

  /**
   * Returns set of {@link Edge edges} in the model.
   * 
   * @return set of {@link Edge edges} in the model
   */
  public Collection<Edge> getEdges() {
    return edges.values();
  }

  /**
   * Return {@link Edge} pointed by the anchor.
   * 
   * @param anchor
   *          anchor string identifier
   * @return {@link Edge} pointed by the anchor
   */
  public Edge getEdgeByAnchor(final String anchor) {
    return anchorToEdge.get(anchor);
  }

  /**
   * Returns set of {@link Interaction interactions} in the model.
   * 
   * @return set of {@link Interaction interactions} in the model
   */
  public Collection<Interaction> getInteractions() {
    return interactions.values();
  }

  /**
   * Returns {@link Interaction} identified by {@link Interaction#graphId}.
   * 
   * @param graphId
   *          identifier
   * @return {@link Interaction} identified by {@link Interaction#graphId}
   */
  public Interaction getInteractionByGraphId(final String graphId) {
    return interactions.get(graphId);
  }

  /**
   * @return the boardHeight
   * @see #boardHeight
   */
  public double getBoardHeight() {
    return boardHeight;
  }

  /**
   * @param boardHeight
   *          the boardHeight to set
   * @see #boardHeight
   */
  public void setBoardHeight(final double boardHeight) {
    this.boardHeight = boardHeight;
  }

  /**
   * @return the boardWidth
   * @see #boardWidth
   */
  public double getBoardWidth() {
    return boardWidth;
  }

  /**
   * @param boardWidth
   *          the boardWidth to set
   * @see #boardWidth
   */
  public void setBoardWidth(final double boardWidth) {
    this.boardWidth = boardWidth;
  }

  /**
   * @return the comments
   * @see #comments
   */
  public List<String> getComments() {
    return comments;
  }

  /**
   * @param comment
   *          the comment to add
   * @see #comments
   */
  public void addComment(final String comment) {
    this.comments.add(comment);
  }

  /**
   * @return the biopaxData
   * @see #biopaxData
   */
  public BiopaxData getBiopaxData() {
    return biopaxData;
  }

  /**
   * @param biopaxData
   *          the biopaxData to set
   * @see #biopaxData
   */
  public void setBiopaxData(final BiopaxData biopaxData) {
    this.biopaxData = biopaxData;
  }

  /**
   * @return the attributesMap
   * @see #attributes
   */
  public Map<String, String> getAttributes() {
    return attributes;
  }

  /**
   * @param attributesMap
   *          the attributesMap to set
   * @see #attributes
   */
  public void setAttributes(final Map<String, String> attributesMap) {
    this.attributes = attributesMap;
  }

  /**
   * @return the biopaxReferences
   * @see #biopaxReferences
   */
  public List<String> getBiopaxReferences() {
    return biopaxReferences;
  }

  /**
   * @param biopaxReference
   *          the biopaxReference to add
   * @see #biopaxReferences
   */
  public void addBiopaxReferences(final String biopaxReference) {
    this.biopaxReferences.add(biopaxReference);
  }

  /**
   * @return the lines
   * @see #lines
   */
  public List<PolylineData> getLines() {
    return lines;
  }

  /**
   * @param line
   *          the line to add
   * @see #lines
   */
  public void addLine(final PolylineData line) {
    this.lines.add(line);
  }

  /**
   * @param lines
   *          the lines to add
   * @see #lines
   */
  public void addLines(final Collection<PolylineData> lines) {
    this.lines.addAll(lines);
  }

  /**
   * Adds edges to the model.
   * 
   * @param parseEdgesFromLines
   *          set of edges to add
   */
  public void addEdges(final Collection<Edge> parseEdgesFromLines) {
    for (final Edge edge : parseEdgesFromLines) {
      addEdge(edge);
    }
  }

  /**
   * Removes edge from the model.
   * 
   * @param edge
   *          object to remove
   */
  public void removeEdge(final Edge edge) {
    edges.remove(edge.getGraphId(), edge);
    for (final String anchor : edge.getAnchors()) {
      anchorToEdge.remove(anchor, edge);
    }
  }

  /**
   * Adds {@link DataNode data nodes} to the graph.
   * 
   * @param nodes
   *          object to add
   */
  public void addDataNodes(final List<DataNode> nodes) {
    for (final DataNode dataNode : nodes) {
      addDataNode(dataNode);
    }
  }

  /**
   * Adds {@link Label labels} to the graph.
   * 
   * @param nodes
   *          object to add
   */
  public void addLabels(final List<Label> nodes) {
    for (final Label label : nodes) {
      addLabel(label);
    }

  }

  /**
   * Adds {@link Shape shapes} to the graph.
   * 
   * @param nodes
   *          object to add
   */
  public void addShapes(final List<Shape> nodes) {
    for (final Shape shape : nodes) {
      addShape(shape);
    }
  }

  /**
   * Adds {@link State states} to the graph.
   * 
   * @param parseCollection
   *          collections of objects to add
   */
  public void addStates(final List<State> parseCollection) {
    for (final State state : parseCollection) {
      addState(state);
    }

  }

  /**
   * Adds {@link State} to the graph.
   * 
   * @param state
   *          object to add
   */
  private void addState(final State state) {
    this.states.add(state);
  }

  /**
   * @return the states
   * @see #states
   */
  public List<State> getStates() {
    return states;
  }

  /**
   * @param states
   *          the states to set
   * @see #states
   */
  public void setStates(final List<State> states) {
    this.states = states;
  }

  public String getMapName() {
    return mapName;
  }

  public void setMapName(final String mapName) {
    this.mapName = mapName;
  }

  public void addAuthor(final Author author) {
    this.authors.add(author);
  }

  public Collection<Author> getAuthors() {
    return authors;
  }

  public Calendar getLastModified() {
    return lastModified;
  }

  public void setLastModified(final Calendar modificationDate) {
    if (lastModified != null) {
      throw new NotImplementedException("Only single lastModified date is supported");
    }
    this.lastModified = modificationDate;
  }

}
