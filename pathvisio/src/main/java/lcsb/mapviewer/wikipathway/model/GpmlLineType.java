package lcsb.mapviewer.wikipathway.model;

import lcsb.mapviewer.model.graphics.LineType;

/**
 * This enum represents type of line in GPML format.
 * 
 * @author Piotr Gawron
 *
 */
public enum GpmlLineType {

  /**
   * Sold line (global representation: {@link LineType#SOLID}).
   */
  SOLID(null, LineType.SOLID),

  /**
   * Dashed line (global representation: {@link LineType#DASHED}).
   */
  DASHED("Broken", LineType.DASHED);

  /**
   * String in GPML format representig this type.
   */
  private String gpmlString;

  /**
   * Which {@link LineType} in our model should be used for this GPML line type.
   */
  private LineType correspondingGlobalLineType;

  /**
   * Default constructor.
   * 
   * @param gpmlString
   *          {@link #gpmlString}
   * @param globalLineType
   *          {@link #correspondingGlobalLineType}
   */
  GpmlLineType(final String gpmlString, final LineType globalLineType) {
    this.gpmlString = gpmlString;
    this.correspondingGlobalLineType = globalLineType;
  }

  /**
   * Returns {@link GpmlLineType type} identified by {@link #gpmlName gpml string}
   * identifying the type.
   *
   * @param gpmlName
   *          {@link #gpmlString}
   * @return {@link GpmlLineType type} identified by {@link #gpmlName gpml string}
   *         identifying the type
   * @throws UnknownTypeException
   *           thrown when type cannot be found
   */
  public static GpmlLineType getByGpmlName(final String gpmlName) throws UnknownTypeException {
    for (final GpmlLineType type : values()) {
      if (type.getGpmlString() == null) {
        if (gpmlName == null) {
          return type;
        }
      } else if (type.getGpmlString().equals(gpmlName)) {
        return type;
      }
    }
    throw new UnknownTypeException("Unknown line type: " + gpmlName);
  }

  public static GpmlLineType getByLineType(final LineType lineType) throws UnknownTypeException {
    for (final GpmlLineType type : values()) {
      if (type.correspondingGlobalLineType == lineType) {
        return type;
      }
    }
    return GpmlLineType.SOLID;
  }

  /**
   * @return the gpmlString
   * @see #gpmlString
   */
  public String getGpmlString() {
    return gpmlString;
  }

  /**
   * @return the correspondingGlobalLineType
   * @see #correspondingGlobalLineType
   */
  public LineType getCorrespondingGlobalLineType() {
    return correspondingGlobalLineType;
  }
}
