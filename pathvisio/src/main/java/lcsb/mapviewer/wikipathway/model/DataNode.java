package lcsb.mapviewer.wikipathway.model;

import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.map.MiriamData;

/**
 * Class used to store data from DataNode from GPML.
 * 
 * @author Jan Badura
 * 
 */
public class DataNode extends GraphicalPathwayElement {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * References.
   */
  private List<MiriamData> references = new ArrayList<>();

  /**
   * ???
   */
  private String type;

  /**
   * Type o line used to draw element.
   */
  private LineType lineType = LineType.SOLID;

  /**
   * ???
   */
  private String textLabel;

  /**
   * Font weight (like italic, bold) that should be used.
   */
  private String fontWeight;

  /**
   * Align of the description.
   */
  private String verticalAlign;

  /**
   * Default constructor.
   * 
   * @param graphId
   *          {@link PathwayElement#graphId}
   */
  public DataNode(final String graphId, final String mapName) {
    super(graphId, mapName);
  }

  /**
   * Empty constructor that should be used only by serialization tools and
   * subclasses.
   */
  protected DataNode() {
    super();
  }

  @Override
  public String toString() {
    return textLabel + " " + getGraphId() + " " + getGroupRef() + " " + type;
  }

  @Override
  public String getName() {
    return getTextLabel();
  }

  /**
   * @return the type
   * @see #type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type
   *          the type to set
   * @see #type
   */
  public void setType(final String type) {
    this.type = type;
  }

  /**
   * @return the textLabel
   * @see #textLabel
   */
  public String getTextLabel() {
    return textLabel;
  }

  /**
   * @param textLabel
   *          the textLabel to set
   * @see #textLabel
   */
  public void setTextLabel(final String textLabel) {
    this.textLabel = textLabel;
  }

  /**
   * @return the references
   * @see #references
   */
  public List<MiriamData> getReferences() {
    return references;
  }

  /**
   * @param references
   *          the references to set
   * @see #references
   */
  public void setReferences(final List<MiriamData> references) {
    this.references = references;
  }

  /**
   * @return the vAlign
   * @see #verticalAlign
   */
  public String getVerticalAlign() {
    return verticalAlign;
  }

  /**
   * @param verticalAlign
   *          the vAlign to set
   * @see #verticalAlign
   */
  public void setVerticalAlign(final String verticalAlign) {
    this.verticalAlign = verticalAlign;
  }

  /**
   * Adds reference to node.
   * 
   * @param md
   *          objet to add
   */
  public void addReference(final MiriamData md) {
    references.add(md);
  }

  /**
   * @return the lineType
   * @see #lineType
   */
  public LineType getLineType() {
    return lineType;
  }

  /**
   * @param lineType
   *          the lineType to set
   * @see #lineType
   */
  public void setLineType(final LineType lineType) {
    this.lineType = lineType;
  }

  /**
   * @return the fontWeight
   * @see #fontWeight
   */
  public String getFontWeight() {
    return fontWeight;
  }

  /**
   * @param fontWeight
   *          the fontWeight to set
   * @see #fontWeight
   */
  public void setFontWeight(final String fontWeight) {
    this.fontWeight = fontWeight;
  }

}
