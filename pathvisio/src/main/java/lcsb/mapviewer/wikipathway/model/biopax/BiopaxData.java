package lcsb.mapviewer.wikipathway.model.biopax;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;

/**
 * Element containing all BioPax information parsed from GPML file.
 * 
 * @author Piotr Gawron
 * 
 */
public class BiopaxData implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final transient Logger logger = LogManager.getLogger();

  /**
   * List of {@link BiopaxPublication publications}.
   */
  private Map<String, BiopaxPublication> publications = new HashMap<>();

  /**
   * List of {@link BiopaxOpenControlledVocabulary BioPax vocabulary}.
   */
  private List<BiopaxOpenControlledVocabulary> openControlledVocabularies = new ArrayList<>();

  /**
   * @return the publications
   * @see #publications
   */
  public Collection<BiopaxPublication> getPublications() {
    return publications.values();
  }

  /**
   * @param publication
   *          the publication to add
   * @see #publications
   */
  public void addPublication(final BiopaxPublication publication) {
    String id = publication.getReferenceId();
    if (this.publications.get(publication.getReferenceId()) != null) {
      throw new InvalidArgumentException("Biopax publication with rdf:id=\'" + id + "\' already exists.");
    }
    this.publications.put(id, publication);
  }

  /**
   * Returns publication identified by reference id.
   * 
   * @param ref
   *          reference identifier
   * @return {@link BiopaxPublication} corresponding to the ref
   */
  public BiopaxPublication getPublicationByReference(final String ref) {
    return publications.get(ref);
  }

  /**
   * @return the openControlledVocabularies
   * @see #openControlledVocabularies
   */
  public List<BiopaxOpenControlledVocabulary> getOpenControlledVocabularies() {
    return openControlledVocabularies;
  }

  /**
   * @param openControlledVocabulary
   *          the openControlledVocabulary to add
   * @see #openControlledVocabularies
   */
  public void addOpenControlledVocabulary(final BiopaxOpenControlledVocabulary openControlledVocabulary) {
    this.openControlledVocabularies.add(openControlledVocabulary);
  }

}
