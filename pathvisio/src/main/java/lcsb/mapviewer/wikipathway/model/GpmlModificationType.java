package lcsb.mapviewer.wikipathway.model;

import lcsb.mapviewer.model.map.species.field.ModificationState;

/**
 * This enum represents type of line in GPML format.
 * 
 * @author Piotr Gawron
 *
 */
public enum GpmlModificationType {

  /**
   * Gpml type representing: {@link ModificationState#PHOSPHORYLATED}.
   */
  PHOSPHORYLATED("P", ModificationState.PHOSPHORYLATED),

  /**
   * Gpml type representing: {@link ModificationState#ACETYLATED}.
   */
  ACETYLATED("Ac", ModificationState.ACETYLATED),

  /**
   * Gpml type representing: {@link ModificationState#UBIQUITINATED}.
   */
  UBIQUITINATED("Ub", ModificationState.UBIQUITINATED),

  /**
   * Gpml type representing: {@link ModificationState#METHYLATED}.
   */
  METHYLATED("Me", ModificationState.METHYLATED),

  /**
   * Gpml type representing: {@link ModificationState#HYDROXYLATED}.
   */
  HYDROXYLATED("OH", ModificationState.HYDROXYLATED),

  /**
   * Gpml type representing: {@link ModificationState#MYRISTOYLATED}.
   */
  MYRISTOYLATED("My", ModificationState.MYRISTOYLATED),

  /**
   * Gpml type representing: {@link ModificationState#SULFATED}.
   */
  SULFATED("S", ModificationState.SULFATED),

  /**
   * Gpml type representing: {@link ModificationState#PRENYLATED}.
   */
  PRENYLATED("Pr", ModificationState.PRENYLATED),

  /**
   * Gpml type representing: {@link ModificationState#GLYCOSYLATED}.
   */
  GLYCOSYLATED("G", ModificationState.GLYCOSYLATED),

  /**
   * Gpml type representing: {@link ModificationState#PALMYTOYLATED}.
   */
  PALMYTOYLATED("Pa", ModificationState.PALMYTOYLATED),

  /**
   * Gpml type representing: {@link ModificationState#UNKNOWN}.
   */
  UNKOWN("", ModificationState.UNKNOWN),

  /**
   * Gpml type representing: {@link ModificationState#PROTONATED}.
   */
  PROTONATED("H", ModificationState.PROTONATED);

  /**
   * String in GPML format representig this type.
   */
  private String gpmlString;
  /**
   * Which {@link ModificationState} in our model should be used for this GPML
   * modification type.
   */
  private ModificationState correspondingModificationState;

  /**
   * Default constructor.
   *
   * @param gpmlString
   *          {@link #gpmlString}
   * @param correspondingModificationState
   *          {@link #correspondingModificationState}
   */
  GpmlModificationType(final String gpmlString, final ModificationState correspondingModificationState) {
    this.gpmlString = gpmlString;
    this.correspondingModificationState = correspondingModificationState;
  }

  /**
   * Returns {@link GpmlModificationType type} identified by {@link #gpmlName gpml
   * string} identifing the type.
   *
   * @param gpmlName
   *          {@link #gpmlString}
   * @return {@link GpmlModificationType type} identified by {@link #gpmlName gpml
   *         string} identifing the type
   * @throws UnknownTypeException
   *           thrown when type cannot be found
   */
  public static GpmlModificationType getByGpmlName(final String gpmlName) throws UnknownTypeException {
    for (final GpmlModificationType type : values()) {
      if (type.getGpmlString() == null) {
        if (gpmlName == null) {
          return type;
        }
      } else if (type.getGpmlString().equalsIgnoreCase(gpmlName)) {
        return type;
      }
    }
    throw new UnknownTypeException("Unknown state type: " + gpmlName);
  }

  /**
   * @return the gpmlString
   * @see #gpmlString
   */
  public String getGpmlString() {
    return gpmlString;
  }

  /**
   * @return the correspondingModificationState
   * @see #correspondingModificationState
   */
  public ModificationState getCorrespondingModificationState() {
    return correspondingModificationState;
  }
}
