package lcsb.mapviewer.wikipathway.model;

import java.awt.Color;
import java.awt.font.TextAttribute;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.Map;

import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.LineType;

/**
 * Class with common data for all GPML elements that have a graphical
 * representation on the map.
 * 
 * @author Piotr Gawron
 *
 */
public abstract class GraphicalPathwayElement extends PathwayElement {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Color.
   */
  private Color color = Color.BLACK;

  /**
   * Fill color.
   */
  private Color fillColor = Color.WHITE;

  /**
   * Name of the font used in drawing.
   */
  private String fontName;

  /**
   * Font size used for description.
   */
  private Double fontSize;

  /**
   * Rectangle border of the element.
   */
  private Rectangle2D rectangle;

  /**
   * {@link TextAttribute Font attributes}.
   */
  private Map<TextAttribute, Object> fontAttributes = new HashMap<>();

  /**
   * {@link TextAttribute Font attributes}.
   */
  private HorizontalAlign textAlignment = HorizontalAlign.LEFT;

  /**
   * Thickness of the line used to draw element.
   */
  private Double lineThickness = 1.0;

  /**
   * Type of line used to draw the element.
   */
  private LineType lineType = LineType.SOLID;

  /**
   * Default constructor.
   */
  public GraphicalPathwayElement() {
    super();
  }

  /**
   * Default constructor.
   * 
   * @param graphId
   *          {@link PathwayElement#graphId}
   */
  public GraphicalPathwayElement(final String graphId, final String mapName) {
    super(graphId, mapName);
  }

  /**
   * @return the fontAttributes
   * @see #fontAttributes
   */
  public Map<TextAttribute, Object> getFontAttributes() {
    return fontAttributes;
  }

  /**
   * @param fontAttributes
   *          the fontAttributes to set
   * @see #fontAttributes
   */
  public void setFontAttributes(final Map<TextAttribute, Object> fontAttributes) {
    this.fontAttributes = fontAttributes;
  }

  /**
   * Adds font attribute.
   * 
   * @param key
   *          type of the attribute
   * @param value
   *          value of the attribute
   */
  public void addFontAttribute(final TextAttribute key, final Object value) {
    fontAttributes.put(key, value);
  }

  /**
   * @return the textAlignment
   * @see #textAlignment
   */
  public HorizontalAlign getTextAlignment() {
    return textAlignment;
  }

  /**
   * @param textAlignment
   *          the textAlignment to set
   * @see #textAlignment
   */
  public void setTextAlignment(final HorizontalAlign textAlignment) {
    this.textAlignment = textAlignment;
  }

  /**
   * @return the fontSize
   * @see #fontSize
   */
  public Double getFontSize() {
    return fontSize;
  }

  /**
   * @param fontSize
   *          the fontSize to set
   * @see #fontSize
   */
  public void setFontSize(final Double fontSize) {
    this.fontSize = fontSize;
  }

  /**
   * @return the color
   * @see #color
   */
  public Color getColor() {
    return color;
  }

  /**
   * @param color
   *          the color to set
   * @see #color
   */
  public void setColor(final Color color) {
    if (color != null) {
      this.color = color;
    }
  }

  /**
   * @return the fillColor
   * @see #fillColor
   */
  public Color getFillColor() {
    return fillColor;
  }

  /**
   * @param fillColor
   *          the fillColor to set
   * @see #fillColor
   */
  public void setFillColor(final Color fillColor) {
    if (fillColor != null) {
      this.fillColor = fillColor;
    }
  }

  /**
   * @return the fontName
   * @see #fontName
   */
  public String getFontName() {
    return fontName;
  }

  /**
   * @param fontName
   *          the fontName to set
   * @see #fontName
   */
  public void setFontName(final String fontName) {
    this.fontName = fontName;
  }

  /**
   * @return the lineThickness
   * @see #lineThickness
   */
  public Double getLineThickness() {
    return lineThickness;
  }

  /**
   * @param lineThickness
   *          the lineThickness to set
   * @see #lineThickness
   */
  public void setLineThickness(final Double lineThickness) {
    this.lineThickness = lineThickness;
  }

  /**
   * @return the lineType
   * @see #lineType
   */
  public LineType getLineType() {
    return lineType;
  }

  /**
   * @param lineType
   *          the lineType to set
   * @see #lineType
   */
  public void setLineType(final LineType lineType) {
    this.lineType = lineType;
  }

  @Override
  public Rectangle2D getRectangle() {
    return rectangle;
  }

  /**
   * @param rectangle
   *          the rectangle to set
   * @see #rectangle
   */
  public void setRectangle(final Rectangle2D rectangle) {
    this.rectangle = rectangle;
  }

}
