package lcsb.mapviewer.wikipathway.model;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;

import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;

/**
 * Class used to store data from Shape from GPML.
 * 
 * @author Jan Badura
 * 
 */
public class Shape extends GraphicalPathwayElement {

  private static final List<GpmlShape> SPECIAL_SHAPE_NAMES = Arrays.asList(
      GpmlShape.SACROPLASMIC_RETICULUM,
      GpmlShape.GOLGI_APPARATUS,
      GpmlShape.ENDOPLASMIC_RETICULUM,
      GpmlShape.MITOCHONDRIA);

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Shape type of the node.
   */
  private GpmlShape shape;

  /**
   * ???
   */
  private String textLabel;

  /**
   * Is shape a compartment.
   */
  private Boolean compartment;

  /**
   * Thickness of the line used for drawing shape.
   */
  private Double lineThickness;

  /**
   * Align of the description.
   */
  private String verticalAlign;

  /**
   * Rotation of the element.
   */
  private Double rotation;

  /**
   * Sometimes {@link Shape labels} are connected to reactions.
   */
  private boolean treatAsNode = false;

  /**
   * Default constructor.
   * 
   * @param graphId
   *          {@link PathwayElement#graphId}
   */
  public Shape(final String graphId, final String mapName) {
    super(graphId, mapName);
    setRectangle(null);
    setShape(null);
    setTextLabel(null);
    setGroupRef(null);
    setCompartment(false);
    setFillColor(Color.GRAY);
  }

  /**
   * Empty constructor that should be used only by serialization tools and
   * subclasses.
   */
  protected Shape() {
  }

  @Override
  public String getName() {
    if (SPECIAL_SHAPE_NAMES.contains(shape)) {
      if (textLabel != null && !textLabel.isEmpty()) {
        return shape.getStringRepresentation() + ":" + textLabel;
      } else {
        return shape.getStringRepresentation();
      }
    }
    if (textLabel == null) {
      return "";
    } else {
      return textLabel;
    }
  }

  @Override
  public LogMarker getLogMarker() {
    return new LogMarker(ProjectLogEntryType.PARSING_ISSUE, getName(), getGraphId(), getMapName());
  }

  /**
   * @return the shape
   * @see #shape
   */
  public GpmlShape getShape() {
    return shape;
  }

  /**
   * @param shape
   *          the shape to set
   * @see #shape
   */
  public void setShape(final GpmlShape shape) {
    this.shape = shape;
    if (shape != null && shape.equals(GpmlShape.DEGRADATION)) {
      setTreatAsNode(true);
    }
  }

  /**
   * @return the textLabel
   * @see #textLabel
   */
  public String getTextLabel() {
    return textLabel;
  }

  /**
   * @param textLabel
   *          the textLabel to set
   * @see #textLabel
   */
  public void setTextLabel(final String textLabel) {
    this.textLabel = textLabel;
  }

  /**
   * @return the compartment
   * @see #compartment
   */
  public Boolean isCompartment() {
    return compartment;
  }

  /**
   * @param compartment
   *          the compartment to set
   * @see #compartment
   */
  public void setCompartment(final Boolean compartment) {
    this.compartment = compartment;
  }

  /**
   * @return the vAlign
   * @see #verticalAlign
   */
  public String getVerticalAlign() {
    return verticalAlign;
  }

  /**
   * @param verticalAlign
   *          the vAlign to set
   * @see #verticalAlign
   */
  public void setVerticalAlign(final String verticalAlign) {
    this.verticalAlign = verticalAlign;
  }

  /**
   * @return the rotation
   * @see #rotation
   */
  public Double getRotation() {
    return rotation;
  }

  /**
   * @param rotation
   *          the rotation to set
   * @see #rotation
   */
  public void setRotation(final Double rotation) {
    this.rotation = rotation;
  }

  /**
   * @return the lineThickness
   * @see #lineThickness
   */
  public Double getLineThickness() {
    return lineThickness;
  }

  /**
   * @param lineThickness
   *          the lineThickness to set
   * @see #lineThickness
   */
  public void setLineThickness(final Double lineThickness) {
    this.lineThickness = lineThickness;
  }

  /**
   * @return the treatAsNode
   * @see #treatAsNode
   */
  public boolean isTreatAsNode() {
    return treatAsNode;
  }

  /**
   * @param treatAsNode
   *          the treatAsNode to set
   * @see #treatAsNode
   */
  public void setTreatAsNode(final boolean treatAsNode) {
    this.treatAsNode = treatAsNode;
  }

}
