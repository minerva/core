package lcsb.mapviewer.wikipathway.model.biopax;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class BiopaxFactory {
  private final Logger logger = LogManager.getLogger();
  private String mapName;

  public BiopaxFactory(final String mapName) {
    this.mapName = mapName;
  }

  /**
   * Creates {@link MiriamData annotation} from {@link BiopaxPublication}.
   *
   * @param publication input BioPax structure
   * @return {@link MiriamData annotation}
   */
  public MiriamData createMiriamData(final BiopaxPublication publication) {
    if ("PubMed".equals(publication.getDb())) {
      if (publication.getId() == null || publication.getId().isEmpty()) {
        return null;
      } else {
        if (StringUtils.isNumeric(publication.getId().trim())) {
          return new MiriamData(MiriamType.PUBMED, publication.getId());
        } else {
          return null;
        }
      }
    } else {
      throw new NotImplementedException("Unknown biopax database: " + publication.getDb());
    }
  }

  /**
   * Returns list of {@link MiriamData} that are referenced in {@link BiopaxData}
   * with identifier given in biopaxReference.
   *
   * @param biopaxData      {@link BiopaxData} where annotations are stored
   * @param biopaxReference list of references (to data in {@link BiopaxData}) that we want to
   *                        convert into {@link MiriamData}
   * @return list of {@link MiriamData} that are referenced in {@link BiopaxData} with identifier given in biopaxReference.
   */
  public Collection<MiriamData> getMiriamData(final BiopaxData biopaxData, final Collection<String> biopaxReference) {
    List<MiriamData> result = new ArrayList<>();
    for (final String string : biopaxReference) {
      if (biopaxData != null) {
        BiopaxPublication bp = biopaxData.getPublicationByReference(string);
        if (bp != null) {
          try {
            MiriamData md = createMiriamData(bp);
            if (md != null) {
              result.add(md);
            } else {
              logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, "BiopaxReference", string, getMapName()),
                  "Publication is invalid.");
            }
          } catch (final NotImplementedException e) {
            logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, "BiopaxReference", string, getMapName()),
                "Publication type is not supported.");
          }
        } else {
          logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, "BiopaxReference", string, getMapName()),
              "Biopax publication doesn't exist.");
        }
      } else {
        logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, "BiopaxReference", string, getMapName()),
            "Invalid BioPax reference.");
      }
    }
    return result;
  }

  public String getMapName() {
    return mapName;
  }

  public void setMapName(final String mapName) {
    this.mapName = mapName;
  }
}
