package lcsb.mapviewer.wikipathway.model.biopax;

import java.io.Serializable;

/**
 * Describes BioPax node with publication.
 * 
 * @author Piotr Gawron
 * 
 */
public class BiopaxPublication implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Identifier of a publication in db.
   */
  private String id;

  /**
   * Database where publication is located.
   */
  private String db;

  /**
   * Title of publication.
   */
  private String title;

  /**
   * Source of the publication.
   */
  private String source;

  /**
   * Year when publication was issued.
   */
  private String year;

  /**
   * Authors of the publication.
   */
  private String authors;

  /**
   * Identifier of the object which this publication annotate.
   */
  private String referenceId;

  /**
   * @return the id
   * @see #id
   */
  public String getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(final String id) {
    this.id = id;
  }

  /**
   * @return the db
   * @see #db
   */
  public String getDb() {
    return db;
  }

  /**
   * @param db
   *          the db to set
   * @see #db
   */
  public void setDb(final String db) {
    this.db = db;
  }

  /**
   * @return the title
   * @see #title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title
   *          the title to set
   * @see #title
   */
  public void setTitle(final String title) {
    this.title = title;
  }

  /**
   * @return the source
   * @see #source
   */
  public String getSource() {
    return source;
  }

  /**
   * @param source
   *          the source to set
   * @see #source
   */
  public void setSource(final String source) {
    this.source = source;
  }

  /**
   * @return the year
   * @see #year
   */
  public String getYear() {
    return year;
  }

  /**
   * @param year
   *          the year to set
   * @see #year
   */
  public void setYear(final String year) {
    this.year = year;
  }

  /**
   * @return the authors
   * @see #authors
   */
  public String getAuthors() {
    return authors;
  }

  /**
   * @param authors
   *          the authors to set
   * @see #authors
   */
  public void setAuthors(final String authors) {
    this.authors = authors;
  }

  /**
   * @return the referenceId
   * @see #referenceId
   */
  public String getReferenceId() {
    return referenceId;
  }

  /**
   * @param referenceId
   *          the referenceId to set
   * @see #referenceId
   */
  public void setReferenceId(final String referenceId) {
    this.referenceId = referenceId;
  }
}
