package lcsb.mapviewer.wikipathway.model;

import java.util.HashSet;
import java.util.Set;

import lcsb.mapviewer.model.map.MiriamData;

/**
 * Class used to store data from Label from GPML.
 * 
 * @author Jan Badura
 * 
 */
public class Label extends GraphicalPathwayElement {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * ???
   */
  private String textLabel;

  /**
   * Font weight (like italic, bold) that should be used.
   */
  private String fontWeight;

  /**
   * Shape associated with the label border.
   */
  private String shape;

  /**
   * How description should be aligned.
   */
  private String verticalAlign;

  private Set<MiriamData> references = new HashSet<>();

  /**
   * Sometimes {@link Label labels} are connected to reactions. This field
   */
  private boolean treatAsNode = false;

  /**
   * Default constructor.
   * 
   * @param graphId
   *          {@link PathwayElement#graphId}
   */
  public Label(final String graphId, final String mapName) {
    super(graphId, mapName);
    setTextLabel(null);
    setGroupRef(null);
  }

  /**
   * Empty constructor that should be used only by serialization tools and
   * subclasses.
   */
  protected Label() {
  }

  @Override
  public String getName() {
    return getTextLabel();
  }

  /**
   * @return the textLabel
   * @see #textLabel
   */
  public String getTextLabel() {
    return textLabel;
  }

  /**
   * @param textLabel
   *          the textLabel to set
   * @see #textLabel
   */
  public void setTextLabel(final String textLabel) {
    this.textLabel = textLabel;
  }

  /**
   * @return the treatAsNode
   * @see #treatAsNode
   */
  public boolean isTreatAsNode() {
    return treatAsNode;
  }

  /**
   * @param treatAsNode
   *          the treatAsNode to set
   * @see #treatAsNode
   */
  public void setTreatAsNode(final boolean treatAsNode) {
    this.treatAsNode = treatAsNode;
  }

  /**
   * @return the vAlign
   * @see #verticalAlign
   */
  public String getVerticalAlign() {
    return verticalAlign;
  }

  /**
   * @param verticalAlign
   *          the vAlign to set
   * @see #verticalAlign
   */
  public void setVerticalAlign(final String verticalAlign) {
    this.verticalAlign = verticalAlign;
  }

  /**
   * @return the fontWeight
   * @see #fontWeight
   */
  public String getFontWeight() {
    return fontWeight;
  }

  /**
   * @param fontWeight
   *          the fontWeight to set
   * @see #fontWeight
   */
  public void setFontWeight(final String fontWeight) {
    this.fontWeight = fontWeight;
  }

  /**
   * @return the shape
   * @see #shape
   */
  public String getShape() {
    return shape;
  }

  /**
   * @param shape
   *          the shape to set
   * @see #shape
   */
  public void setShape(final String shape) {
    this.shape = shape;
  }

  public Set<MiriamData> getReferences() {
    return references;
  }

  public void setReferences(final Set<MiriamData> references) {
    this.references = references;
  }

  public void addReference(final MiriamData md) {
    this.references.add(md);
  }
}
