package lcsb.mapviewer.wikipathway.model;

import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.modifier.Inhibition;
import lcsb.mapviewer.model.map.modifier.Modulation;
import lcsb.mapviewer.model.map.modifier.PhysicalStimulation;
import lcsb.mapviewer.model.map.modifier.UnknownCatalysis;
import lcsb.mapviewer.model.map.modifier.UnknownInhibition;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.reaction.type.HeterodimerAssociationReaction;
import lcsb.mapviewer.model.map.reaction.type.NegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.PositiveInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedPhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownNegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownPositiveInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownTransitionReaction;

/**
 * This class contains mapping between GPML interactions and CellDesigner model
 * representation. Type is identified by two values:
 * {@link #gpmlInteractionType} and {@link #gpmlLineStyle}. For every type there
 * are three possible mappings:
 * <ul>
 * <li>reaction - defines to what {@link Reaction} GPML type should be
 * transformed</li>
 * <li>input - when interaction is just an input part of main reaction this
 * defines to what {@link ReactionNode} should it be transformed</li>
 * <li>output - when interaction is just an output part of main reaction this
 * defines to what {@link ReactionNode} should it be transformed</li>
 * </ul>
 * 
 * @author Piotr Gawron
 * 
 */
public enum InteractionMapping {

  /**
   * Default line.
   */
  LINE(GpmlInteractionType.LINE, GpmlLineType.SOLID,
      UnknownTransitionReaction.class, true,
      Reactant.class, false,
      Reactant.class, false,
      true),

  /**
   * Default arrow.
   */
  ARROW(GpmlInteractionType.ARROW, GpmlLineType.SOLID,
      StateTransitionReaction.class, false,
      Reactant.class, false,
      Product.class, false),

  /**
   * Dashed line.
   */
  LINE_DASHED(GpmlInteractionType.LINE, GpmlLineType.DASHED,
      UnknownTransitionReaction.class, true,
      Reactant.class, true,
      Reactant.class, true,
      true),

  /**
   * Dashed arrow.
   */
  ARROW_DASHED(GpmlInteractionType.ARROW, GpmlLineType.DASHED,
      UnknownPositiveInfluenceReaction.class, false,
      Modulation.class, true,
      Product.class, true),

  /**
   * Line ended with T-bar.
   */
  TBAR(GpmlInteractionType.TBAR, GpmlLineType.SOLID,
      NegativeInfluenceReaction.class, true,
      Inhibition.class, false,
      Product.class, true),

  /**
   * Line ended with T-bar dashed.
   */
  TBAR_DASHED(GpmlInteractionType.TBAR, GpmlLineType.DASHED,
      UnknownNegativeInfluenceReaction.class, true,
      UnknownInhibition.class, false,
      Product.class, true),

  /**
   * Necessary stimulation.
   */
  NECESSARY_STIMULATION(GpmlInteractionType.NECESSARY_STIMULATION, GpmlLineType.SOLID,
      ReducedPhysicalStimulationReaction.class, false,
      PhysicalStimulation.class, false,
      Product.class, true),

  /**
   * Necessary stimulation dashed.
   */
  NECESSARY_STIMULATION_DASHED(GpmlInteractionType.NECESSARY_STIMULATION, GpmlLineType.DASHED,
      UnknownPositiveInfluenceReaction.class, true,
      Modulation.class, true,
      Product.class, true),

  /**
   * Binding.
   */
  BINDING(GpmlInteractionType.BINDING, GpmlLineType.SOLID,
      HeterodimerAssociationReaction.class, false,
      Modulation.class, true,
      Product.class, true),

  /**
   * Binding dashed.
   */
  BINDING_DASHED(GpmlInteractionType.BINDING, GpmlLineType.DASHED,
      UnknownPositiveInfluenceReaction.class, false,
      Modulation.class, true,
      Product.class, true),

  /**
   * Conversion.
   */
  CONVERSION(GpmlInteractionType.CONVERSION, GpmlLineType.SOLID,
      StateTransitionReaction.class, false,
      Reactant.class, false,
      Product.class, false),

  /**
   * Dashed conversion.
   */
  CONVERSION_DASHED(GpmlInteractionType.CONVERSION, GpmlLineType.DASHED,
      UnknownPositiveInfluenceReaction.class, false,
      Modulation.class, true,
      Product.class, true),

  /**
   * Stimulation.
   */
  STIMULATION(GpmlInteractionType.STIMULATION, GpmlLineType.SOLID,
      ReducedPhysicalStimulationReaction.class, true,
      PhysicalStimulation.class, false,
      Product.class, true),

  /**
   * Stimulation dashed.
   */
  STIMULATION_DASHED(GpmlInteractionType.STIMULATION, GpmlLineType.DASHED,
      UnknownPositiveInfluenceReaction.class, true,
      Modulation.class, true,
      Product.class, true),

  /**
   * Catalysis.
   */
  CATALYSIS(GpmlInteractionType.CATALYSIS, GpmlLineType.SOLID,
      PositiveInfluenceReaction.class, true,
      Catalysis.class, false,
      Product.class, true),

  /**
   * Catalysis dashed.
   */
  CATALYSIS_DASHED(GpmlInteractionType.CATALYSIS, GpmlLineType.DASHED,
      UnknownPositiveInfluenceReaction.class, true,
      UnknownCatalysis.class, false,
      Product.class, true),

  /**
   * Modification.
   */
  MODIFICATION(GpmlInteractionType.MODIFICATION, GpmlLineType.SOLID,
      PositiveInfluenceReaction.class, true,
      Modulation.class, false,
      Product.class, true),

  /**
   * Modification dashed.
   */
  MODIFICATION_DASHED(GpmlInteractionType.MODIFICATION, GpmlLineType.DASHED,
      UnknownPositiveInfluenceReaction.class, false,
      Modulation.class, true,
      Product.class, true),

  /**
   * Inhibition.
   */
  INHIBITION(GpmlInteractionType.INHIBITION, GpmlLineType.SOLID,
      NegativeInfluenceReaction.class, false,
      Inhibition.class, false,
      Product.class, true),

  /**
   * Inhibition dashed.
   */
  INHIBITION_DASHED(GpmlInteractionType.INHIBITION, GpmlLineType.DASHED,
      UnknownNegativeInfluenceReaction.class, true,
      UnknownInhibition.class, true,
      Product.class, true),

  /**
   * Cleavage.
   */
  CLEAVAGE(GpmlInteractionType.CLEAVAGE, GpmlLineType.SOLID,
      StateTransitionReaction.class, false,
      Modulation.class, true,
      Product.class, false),

  /**
   * Cleavage dashed.
   */
  CLEAVAGE_DASHED(GpmlInteractionType.CLEAVAGE, GpmlLineType.DASHED,
      UnknownTransitionReaction.class, true,
      Modulation.class, true,
      Product.class, true),

  /**
   * Covalent bond.
   */
  COVALENT_BOND(GpmlInteractionType.COVALENT_BOND, GpmlLineType.SOLID,
      UnknownTransitionReaction.class, true,
      Reactant.class, true,
      Product.class, true,
      true),

  /**
   * Covalent bond dashed.
   */
  COVALENT_BOND_DASHED(GpmlInteractionType.COVALENT_BOND, GpmlLineType.DASHED,
      UnknownTransitionReaction.class, true,
      Reactant.class, true,
      Product.class, true,
      true),

  /**
   * Branching left.
   */
  BRANCHING_LEFT(GpmlInteractionType.BRANCHING_LEFT, GpmlLineType.SOLID,
      UnknownTransitionReaction.class, true,
      Reactant.class, false,
      Reactant.class, false,
      true),

  /**
   * Branching left dashed.
   */
  BRANCHING_LEFT_DASHED(GpmlInteractionType.BRANCHING_LEFT, GpmlLineType.DASHED,
      UnknownTransitionReaction.class, true,
      Reactant.class, true,
      Reactant.class, true,
      true),

  /**
   * Branching right.
   */
  BRANCHING_RIGHT(GpmlInteractionType.BRANCHING_RIGHT, GpmlLineType.SOLID,
      UnknownTransitionReaction.class, true,
      Reactant.class, false,
      Reactant.class, false,
      true),

  /**
   * Branching right dashed.
   */
  BRANCHING_RIGHT_DASHED(GpmlInteractionType.BRANCHING_RIGHT, GpmlLineType.DASHED,
      UnknownTransitionReaction.class, true,
      Reactant.class, true,
      Reactant.class, true,
      true),

  /**
   * Transcription-translation.
   */
  TRANSCRIPTION_TRANSLATION(GpmlInteractionType.TRANSCRIPTION_TRANSLATION, GpmlLineType.SOLID,
      StateTransitionReaction.class, true,
      Modulation.class, true,
      Product.class, true),

  /**
   * Transcription-translation dashed.
   */
  TRANSCRIPTION_TRANSLATION_DASHED(GpmlInteractionType.TRANSCRIPTION_TRANSLATION, GpmlLineType.DASHED,
      StateTransitionReaction.class, true,
      Modulation.class, true,
      Product.class, true),

  /**
   * Gap...
   */
  GAP(GpmlInteractionType.GAP, GpmlLineType.SOLID,
      UnknownTransitionReaction.class, true,
      Modulation.class, true,
      Product.class, true,
      true),

  /**
   * Gap dashed...
   */
  GAP_DASHED(GpmlInteractionType.GAP, GpmlLineType.DASHED,
      UnknownTransitionReaction.class, true,
      Modulation.class, true,
      Product.class, true,
      true);

  /**
   * Interaction type defined in GPML.
   */
  private GpmlInteractionType gpmlInteractionType;

  /**
   * Line type defined in GPML.
   */
  private GpmlLineType gpmlLineStyle;

  /**
   * Cell designer reaction to which GPML interaction should be transformed.
   */
  private Class<? extends Reaction> modelReactionType;

  /**
   * Cell designer reaction node to which interaction should be transformed when
   * it's an input to the reaction.
   */
  private Class<? extends ReactionNode> modelInputReactionNodeType;

  /**
   * Cell designer reaction node to which interaction should be transformed when
   * it's an output to the reaction.
   */
  private Class<? extends ReactionNode> modelOutputReactionNodeType;

  /**
   * Should CellDesigner reaction be reversible.
   */
  private boolean reversible = false;

  /**
   * When converting to reaction. Should the code warn.
   */
  private boolean interactionWarning = false;

  /**
   * When converting to input of the reaction should the code warn.
   */
  private boolean inputWarning = false;

  /**
   * When converting to output of the reaction should the code warn.
   */
  private boolean outputWarning = false;

  /**
   * Default constructor.
   * 
   * @param gpmlInteractionType
   *          {@link #gpmlInteractionType}
   * @param gpmlLineStyle
   *          {@link #gpmlLineStyle}
   * @param modelReactionType
   *          {@link #modelReactionType}
   * @param interactionWarning
   *          {@link #interactionWarning}
   * @param modelInputReactionNodeType
   *          {@link #modelInputReactionNodeType}
   * @param inputWarning
   *          {@link #inputWarning}
   * @param modelOutputReactionNodeType
   *          {@link #modelOutputReactionNodeType}
   * @param outputWarning
   *          {@link #outputWarning}
   * @param reversible
   *          {@link #reversible}
   */
  InteractionMapping(final GpmlInteractionType gpmlInteractionType, final GpmlLineType gpmlLineStyle,
      final Class<? extends Reaction> modelReactionType,
      final boolean interactionWarning, final Class<? extends ReactionNode> modelInputReactionNodeType, final boolean inputWarning,
      final Class<? extends ReactionNode> modelOutputReactionNodeType, final boolean outputWarning, final boolean reversible) {
    this.gpmlInteractionType = gpmlInteractionType;
    this.modelReactionType = modelReactionType;
    this.interactionWarning = interactionWarning;
    this.modelInputReactionNodeType = modelInputReactionNodeType;
    this.inputWarning = inputWarning;
    this.modelOutputReactionNodeType = modelOutputReactionNodeType;
    this.outputWarning = outputWarning;
    this.reversible = reversible;
    this.gpmlLineStyle = gpmlLineStyle;
  }

  /**
   * Default constructor.
   * 
   * @param gpmlInteractionType
   *          {@link #gpmlInteractionType}
   * @param gpmlLineStyle
   *          {@link #gpmlLineStyle}
   * @param modelReactionType
   *          {@link #modelReactionType}
   * @param interactionWarning
   *          {@link #interactionWarning}
   * @param modelInputReactionNodeType
   *          {@link #modelInputReactionNodeType}
   * @param inputWarning
   *          {@link #inputWarning}
   * @param modelOutputReactionNodeType
   *          {@link #modelOutputReactionNodeType}
   * @param outputWarning
   *          {@link #outputWarning}
   */
  InteractionMapping(final GpmlInteractionType gpmlInteractionType, final GpmlLineType gpmlLineStyle,
      final Class<? extends Reaction> modelReactionType,
      final boolean interactionWarning, final Class<? extends ReactionNode> modelInputReactionNodeType, final boolean inputWarning,
      final Class<? extends ReactionNode> modelOutputReactionNodeType, final boolean outputWarning) {
    this(
        gpmlInteractionType, gpmlLineStyle, modelReactionType, interactionWarning, modelInputReactionNodeType,
        inputWarning, modelOutputReactionNodeType,
        outputWarning, false);
  }

  /**
   * Method that is looking for the {@link InteractionMapping} that matches to
   * given GPML "arrow type" and "line type".
   *
   * @param gpmlType
   *          GPML arrow type
   * @param lineType
   *          GPML line type
   * @return {@link InteractionMapping} that matches to given gpml "arrow type"
   *         and "line type"
   */
  public static InteractionMapping getInteractionMapping(final GpmlInteractionType gpmlType, final LineType lineType) {
    for (final InteractionMapping mapping : InteractionMapping.values()) {
      boolean arrowMatch = mapping.getGpmlInteractionType().equals(gpmlType);
      boolean lineTypeMatch = lineType.equals(mapping.getGpmlLineStyle().getCorrespondingGlobalLineType());

      if (arrowMatch && lineTypeMatch) {
        return mapping;
      }
    }
    return null;
  }

  /**
   * @return the gpmlInteractionType
   * @see #gpmlInteractionType
   */
  public GpmlInteractionType getGpmlInteractionType() {
    return gpmlInteractionType;
  }

  /**
   * @return the modelReactionType
   * @see #modelReactionType
   */
  public Class<? extends Reaction> getModelReactionType() {
    return modelReactionType;
  }

  /**
   * @return the modelInputReactionNodeType
   * @see #modelInputReactionNodeType
   */
  public Class<? extends ReactionNode> getModelInputReactionNodeType() {
    return modelInputReactionNodeType;
  }

  /**
   * @return the modelOutputReactionNodeType
   * @see #modelOutputReactionNodeType
   */
  public Class<? extends ReactionNode> getModelOutputReactionNodeType() {
    return modelOutputReactionNodeType;
  }

  /**
   * @return the reversible
   * @see #reversible
   */
  public boolean isReversible() {
    return reversible;
  }

  /**
   * @return the interactionWarning
   * @see #interactionWarning
   */
  public boolean isInteractionWarning() {
    return interactionWarning;
  }

  /**
   * @return the inputWarning
   * @see #inputWarning
   */
  public boolean isInputWarning() {
    return inputWarning;
  }

  /**
   * @return the outputWarning
   * @see #outputWarning
   */
  public boolean isOutputWarning() {
    return outputWarning;
  }

  /**
   * @return the gpmlLineStyle
   * @see #gpmlLineStyle
   */
  public GpmlLineType getGpmlLineStyle() {
    return gpmlLineStyle;
  }

  public static GpmlInteractionType getGpmlInteractionTypeForMinervaModifierClass(final Class<? extends Modifier> clazz) {
    GpmlInteractionType result = GpmlInteractionType.MODIFICATION;
    for (final InteractionMapping mapping : InteractionMapping.values()) {
      if (mapping.getModelInputReactionNodeType() == clazz) {
        if (mapping.isInputWarning()) {
          result = mapping.getGpmlInteractionType();
        } else {
          return mapping.getGpmlInteractionType();
        }
      }
    }
    return result;
  }

  public static GpmlInteractionType getGpmlInteractionTypeForMinervaReactionClass(final Class<? extends Reaction> clazz) {
    GpmlInteractionType result = GpmlInteractionType.CONVERSION;
    for (final InteractionMapping mapping : InteractionMapping.values()) {
      if (mapping.getModelReactionType() == clazz) {
        if (mapping.isInteractionWarning()) {
          result = mapping.getGpmlInteractionType();
        } else {
          return mapping.getGpmlInteractionType();
        }
      }
    }

    if (clazz.isAssignableFrom(UnknownTransitionReaction.class)) {
      result = GpmlInteractionType.CONVERSION;
    }
    return result;
  }
}
