package lcsb.mapviewer.wikipathway.model;

import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.MiriamData;

/**
 * Class used to store data from Interaction from GPML.
 * 
 * @author Jan Badura
 * 
 */
public class Interaction extends PathwayElement implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Where this {@link Interaction} starts (in which {@link Edge}/
   * {@link Interaction}).
   */
  private String start;

  /**
   * Where this {@link Interaction} ends (in which {@link Edge}/
   * {@link Interaction}).
   */
  private String end;

  /**
   * Line representing this edge.
   */
  private PolylineData line;

  /**
   * Gpml interaction type (arrow).
   */
  private GpmlInteractionType type = GpmlInteractionType.LINE;

  /**
   * List of anchors placed on the edge.
   */
  private Set<String> anchors;

  /**
   * List of edges representing reactants in interaction.
   */
  private Set<Edge> reactants = new HashSet<>();

  /**
   * List of edges representing products in interaction.
   */
  private Set<Edge> products = new HashSet<>();

  /**
   * List of edges representing modifiers in interaction.
   */
  private Set<Edge> modifiers = new HashSet<>();

  /**
   * References for given edge.
   */
  private List<MiriamData> references = new ArrayList<>();

  private boolean reversible = false;

  /**
   * Default constructor.
   * 
   * @param edge
   *          object will be created from this {@link Edge}
   */
  public Interaction(final Edge edge) {
    super(edge.getGraphId(), null);
    setStart(edge.getStart());
    setEnd(edge.getEnd());
    setLine(edge.getLine());
    setType(edge.getType());
    setzOrder(edge.getzOrder());
    for (final MiriamData md : edge.getReferences()) {
      addReference(new MiriamData(md));
    }
    setColor(edge.getColor());
    addComments(edge.getComments());
    addBiopaxReferences(edge.getBiopaxReferences());
    reversible = edge.isReversibleReaction();
  }

  /**
   * Empty constructor that should be used only by serialization tools and
   * subclasses.
   */
  Interaction() {
  }

  /**
   * Adds reference.
   * 
   * @param reference
   *          object to add
   */
  private void addReference(final MiriamData reference) {
    references.add(reference);
  }

  /**
   * @return the start
   * @see #start
   */
  public String getStart() {
    return start;
  }

  /**
   * @param start
   *          the start to set
   * @see #start
   */
  public void setStart(final String start) {
    this.start = start;
  }

  /**
   * @return the end
   * @see #end
   */
  public String getEnd() {
    return end;
  }

  /**
   * @param end
   *          the end to set
   * @see #end
   */
  public void setEnd(final String end) {
    this.end = end;
  }

  /**
   * @return the line
   * @see #line
   */
  public PolylineData getLine() {
    return line;
  }

  /**
   * @param line
   *          the line to set
   * @see #line
   */
  public void setLine(final PolylineData line) {
    this.line = line;
  }

  /**
   * @return the anchors
   * @see #anchors
   */
  public Set<String> getAnchors() {
    return anchors;
  }

  /**
   * @param anchors
   *          the anchors to set
   * @see #anchors
   */
  public void setAnchors(final Set<String> anchors) {
    this.anchors = anchors;
  }

  /**
   * @return the reactants
   * @see #reactants
   */
  public Set<Edge> getReactants() {
    return reactants;
  }

  /**
   * @return the products
   * @see #products
   */
  public Set<Edge> getProducts() {
    return products;
  }

  /**
   * @return the modifiers
   * @see #modifiers
   */
  public Set<Edge> getModifiers() {
    return modifiers;
  }

  /**
   * Adds product.
   * 
   * @param interaction
   *          product to add
   */
  public void addProduct(final Edge interaction) {
    this.products.add(interaction);
    addBiopaxReferences(interaction.getBiopaxReferences());
  }

  /**
   * Add modifier.
   * 
   * @param interaction
   *          modifier to add
   */
  public void addModifier(final Edge interaction) {
    this.modifiers.add(interaction);
    addBiopaxReferences(interaction.getBiopaxReferences());
  }

  /**
   * Add modifier.
   * 
   * @param interaction
   *          reactant to add
   */
  public void addReactant(final Edge interaction) {
    this.reactants.add(interaction);
    addBiopaxReferences(interaction.getBiopaxReferences());
  }

  /**
   * @return the type
   * @see #type
   */
  public GpmlInteractionType getType() {
    return type;
  }

  /**
   * @param type
   *          the type to set
   * @see #type
   */
  public void setType(final GpmlInteractionType type) {
    this.type = type;
  }

  /**
   * @return the references
   * @see #references
   */
  public List<MiriamData> getReferences() {
    return references;
  }

  /**
   * @param references
   *          the references to set
   * @see #references
   */
  public void setReferences(final List<MiriamData> references) {
    this.references = references;
  }

  /**
   * @return the color
   * @see #color
   */
  public Color getColor() {
    return line.getColor();
  }

  /**
   * @param color
   *          the color to set
   * @see #color
   */
  public void setColor(final Color color) {
    if (color != null) {
      line.setColor(color);
      for (final Edge edge : reactants) {
        edge.setColor(color);
      }
      for (final Edge edge : products) {
        edge.setColor(color);
      }
      for (final Edge edge : modifiers) {
        edge.setColor(color);
      }
    }
  }

  public boolean isReversible() {
    return reversible;
  }

  public void setReversible(final boolean reversible) {
    this.reversible = reversible;
  }

  @Override
  String getName() {
    throw new NotImplementedException();
  }

  @Override
  Rectangle2D getRectangle() {
    throw new NotImplementedException();
  }

}
