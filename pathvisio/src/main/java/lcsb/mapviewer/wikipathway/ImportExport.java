package lcsb.mapviewer.wikipathway;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.LoggerContext;
import org.pathvisio.core.model.ConverterException;
import org.pathvisio.core.model.Pathway;
import org.pathvisio.core.model.PathwayExporter;
import org.pathvisio.core.model.PathwayImporter;
import org.pathvisio.desktop.PvDesktop;
import org.pathvisio.desktop.plugin.Plugin;

import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.modelutils.map.LogFormatter;
import lcsb.mapviewer.wikipathway.xml.GPMLToModel;
import lcsb.mapviewer.wikipathway.xml.ModelToGPML;

/**
 * PathVisio plugin that allows to import/export CellDesigner file.
 * 
 * @author Piotr Gawron
 * 
 */
public class ImportExport implements Plugin {
  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * List of extensions supported by import plugin.
   */
  private final String[] importExtensions = new String[] { "xml" };

  /**
   * List of extensions supported by export plugin.
   */
  private final String[] exportExtensions = new String[] { "cell" };

  @Override
  public void init(final PvDesktop desktop) {
    try {
      LoggerContext context = (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
      File file = new File("log4j2.properties");
      context.setConfigLocation(file.toURI());
    } catch (final Exception e) {
      e.printStackTrace();
    }
    ExportCellDesigner exportPlugin = new ExportCellDesigner();
    ImportCellDesigner importPlugin = new ImportCellDesigner();
    desktop.getSwingEngine().getEngine().addPathwayImporter(importPlugin);
    desktop.getSwingEngine().getEngine().addPathwayExporter(exportPlugin);
  }

  @Override
  public void done() {
  }

  /**
   * Implementation of {@link PathwayExporter} that allows PathVisio to import
   * from CellDesigner xml file.
   * 
   * @author Piotr Gawron
   * 
   */
  protected class ImportCellDesigner implements PathwayImporter {

    /**
     * List of warnings that occured during conversion.
     */
    private List<LogEvent> warnings = new ArrayList<>();

    /**
     * Default constructor.
     */
    public ImportCellDesigner() {
    }

    @Override
    public String getName() {
      return "CellDesigner";
    }

    @Override
    public String[] getExtensions() {
      return importExtensions;
    }

    @Override
    public List<String> getWarnings() {
      return new LogFormatter().createFormattedWarnings(warnings);
    }

    @Override
    public boolean isCorrectType(final File arg0) {
      return true;
    }

    @Override
    public Pathway doImport(final File file) throws ConverterException {
      MinervaLoggerAppender appender = MinervaLoggerAppender.createAppender();
      try {
        Pathway pathway = new Pathway();
        String fileName = file.getPath();

        CellDesignerXmlParser parser = new CellDesignerXmlParser();
        Model model = (Model) parser.createModel(new ConverterParams().filename(fileName).sizeAutoAdjust(false));
        String tmp = new ModelToGPML(model.getName()).getGPML(model);
        InputStream stream = new ByteArrayInputStream(tmp.getBytes(StandardCharsets.UTF_8));
        Boolean validate = false;
        pathway.readFromXml(stream, validate);

        MinervaLoggerAppender.unregisterLogEventStorage(appender);
        warnings.addAll(appender.getWarnings());
        return pathway;
      } catch (final Exception e) {
        logger.error(e, e);
        throw new ConverterException(e);
      } finally {
        MinervaLoggerAppender.unregisterLogEventStorage(appender);
      }
    }
  }

  /**
   * Implementation of {@link PathwayExporter} that allows PathVisio to export
   * into CellDesigner xml file.
   * 
   * @author Piotr Gawron
   * 
   */
  protected class ExportCellDesigner implements PathwayExporter {

    /**
     * {@link Model} that was created using this {@link PathwayExporter}.
     */
    private Model model = null;

    /**
     * List of export warnings.
     */
    private List<String> warnings = new ArrayList<>();

    @Override
    public String getName() {
      return "CellDesigner";
    }

    @Override
    public String[] getExtensions() {
      return exportExtensions;
    }

    @Override
    public List<String> getWarnings() {
      return warnings;
    }

    @Override
    public void doExport(final File file, final Pathway pathway) throws ConverterException {
      MinervaLoggerAppender appender = MinervaLoggerAppender.createAppender();
      try {
        pathway.writeToXml(new File("tmp.gpml"), false);
        model = new GPMLToModel().getModel("tmp.gpml");

        MinervaLoggerAppender.unregisterLogEventStorage(appender);
        warnings = new LogFormatter().createFormattedWarnings(appender.getWarnings());

        CellDesignerXmlParser parser = new CellDesignerXmlParser();
        String xml = parser.model2String(model);
        PrintWriter writer = new PrintWriter(file.getPath(), "UTF-8");
        writer.println(xml);
        writer.close();

        warnings.add("Please manually change extension of saved file from .cell to .xml");
      } catch (final Exception e) {
        logger.error(e.getMessage(), e);
        throw new ConverterException(e);
      } finally {
        MinervaLoggerAppender.unregisterLogEventStorage(appender);
      }
    }
  }
}
