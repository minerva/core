package lcsb.mapviewer.wikipathway;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.pathvisio.desktop.plugin.Plugin;

/**
 * Bundle activator of plugin. Responsible for registering bundle and cleaning
 * during finishing.
 * 
 * @author Piotr Gawron
 * 
 */

public class Activator implements BundleActivator {

  /**
   * CellDesigner plugin for PathVisio.
   */
  private ImportExport plugin;

  @Override
  public void start(final BundleContext context) throws Exception {
    plugin = new ImportExport();
    context.registerService(Plugin.class.getName(), plugin, null);
  }

  @Override
  public void stop(final BundleContext context) throws Exception {
    if (plugin != null) {
      plugin.done();
    }
  }
}
