package lcsb.mapviewer.cdplugin.info;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import jp.sbi.celldesigner.plugin.PluginSpeciesAlias;
import lcsb.mapviewer.cdplugin.CdPluginFunctions;

public class InfoPluginTest extends CdPluginFunctions {


  @Test
  public void testConstructor() {
    new InfoPlugin();
    assertEquals(0, super.getErrors().size());
  }

  @Test
  public void testThreadListener() throws InterruptedException {
    new InfoPlugin() {
      public List<PluginSpeciesAlias> getListOfAlias() {
        return Arrays.asList(createSpeciesAlias("id"));
      }
    };
    Thread.sleep(6 * InfoPlugin.DELAY_BETWEEN_ON_SELECT_LISTENER_CHECKS);
    assertEquals(1, InfoFrame.getInstance().getSpecies().size());
  }

}
