package lcsb.mapviewer.cdplugin.info;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import jp.sbi.celldesigner.plugin.PluginSpeciesAlias;
import lcsb.mapviewer.cdplugin.CdPluginFunctions;

public class InfoFrameTest extends CdPluginFunctions {

  @Test
  public void testSetEmptySpeciesList() {
    InfoFrame frame = InfoFrame.getInstance();
    List<PluginSpeciesAlias> list = new ArrayList<>();
    frame.setSpecies(list);
    assertEquals(list, frame.getSpecies());
  }

  @Test
  public void testSetNotEmptySpeciesList() {
    InfoFrame frame = InfoFrame.getInstance();
    frame.setSpecies(Arrays.asList(super.createSpeciesAlias("id"), super.createSpeciesAlias("id2")));
    assertEquals(2, frame.getSpecies().size());
  }

}
