package lcsb.mapviewer.cdplugin;

import java.util.List;

import org.apache.logging.log4j.core.LogEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.mockito.Mockito;

import jp.sbi.celldesigner.plugin.PluginListOf;
import jp.sbi.celldesigner.plugin.PluginReaction;
import jp.sbi.celldesigner.plugin.PluginSpecies;
import jp.sbi.celldesigner.plugin.PluginSpeciesAlias;
import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.common.tests.UnitTestFailedWatcher;

public class CdPluginFunctions {

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();

  private String rdfString = "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:dcterms=\"http://purl.org/dc/terms/\" xmlns:vCard=\"http://www.w3.org/2001/vcard-rdf/3.0#\" xmlns:bqbiol=\"http://biomodels.net/biology-qualifiers/\" xmlns:bqmodel=\"http://biomodels.net/model-qualifiers/\">\n"
      + "<rdf:Description rdf:about=\"#s3\">\n"
      + "<bqmodel:is>\n"
      + "<rdf:Bag>\n"
      + "<rdf:li rdf:resource=\"urn:miriam:wikipedia.en:1\"/>\n"
      + "</rdf:Bag>\n"
      + "</bqmodel:is>\n"
      + "</rdf:Description>\n"
      + "</rdf:RDF>\n";

  private MinervaLoggerAppender appender;

  @Before
  public final void _setUp() throws Exception {
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
    appender = MinervaLoggerAppender.createAppender(false);
  }

  @After
  public final void _tearDown() throws Exception {
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
  }

  protected List<LogEvent> getWarnings() {
    return appender.getWarnings();
  }

  protected List<LogEvent> getErrors() {
    return appender.getErrors();
  }

  protected List<LogEvent> getFatals() {
    return appender.getFatals();
  }

  protected PluginListOf createPluginListWithSpecies(final int size) {
    PluginListOf list = Mockito.mock(PluginListOf.class);

    Mockito.when(list.size()).thenReturn(size);
    for (int i = 0; i < size; i++) {
      PluginSpeciesAlias alias = createSpeciesAlias("id" + i);
      Mockito.when(list.get(i)).thenReturn(alias);
    }
    return list;
  }

  protected PluginListOf createPluginListWithReaction(final int size) {
    PluginListOf list = Mockito.mock(PluginListOf.class);

    Mockito.when(list.size()).thenReturn(size);
    for (int i = 0; i < size; i++) {
      PluginReaction reaction = createReaction("id" + i);
      Mockito.when(list.get(i)).thenReturn(reaction);
    }
    return list;
  }

  protected PluginSpeciesAlias createSpeciesAlias(final String id) {
    PluginSpecies species = Mockito.mock(PluginSpecies.class);
    Mockito.when(species.getId()).thenReturn(id);
    Mockito.when(species.getAnnotationString()).thenReturn(rdfString);
    Mockito.when(species.getNotesString()).thenReturn("spceies some notes" + id);

    PluginSpeciesAlias speciesAlias = Mockito.mock(PluginSpeciesAlias.class);
    Mockito.when(speciesAlias.getSpecies()).thenReturn(species);
    return speciesAlias;
  }

  private PluginReaction createReaction(final String id) {

    PluginReaction reaction = Mockito.mock(PluginReaction.class);
    Mockito.when(reaction.getId()).thenReturn("re" + id);
    Mockito.when(reaction.getAnnotationString()).thenReturn(rdfString);
    Mockito.when(reaction.getNotesString()).thenReturn("re notes" + id);

    return reaction;
  }

}