package lcsb.mapviewer.cdplugin.copypaste;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;

import java.awt.event.KeyEvent;

import org.junit.Test;
import org.mockito.Mockito;

import jp.sbi.celldesigner.plugin.PluginListOf;
import lcsb.mapviewer.cdplugin.CdPluginFunctions;

public class CopyPastePluginTest extends CdPluginFunctions {

  private SystemClipboard cp = new SystemClipboard();

  @Test
  public void testConstructor() {
    new CopyPastePlugin();
    assertEquals(0, getErrors().size());
  }

  @Test
  public void testCopyAction() {
    String data = cp.getClipboardContents();

    CopyPastePlugin plugin = new CopyPastePlugin();
    KeyEvent event = Mockito.mock(KeyEvent.class);
    when(event.getID()).thenReturn(KeyEvent.KEY_PRESSED);
    when(event.getKeyCode()).thenReturn(java.awt.event.KeyEvent.VK_C);
    when(event.isControlDown()).thenReturn(true);
    when(event.isAltDown()).thenReturn(true);

    CopyPastePlugin copyPastePlugin = Mockito.mock(CopyPastePlugin.class);
    PluginListOf list = createPluginListWithSpecies(1);
    when(copyPastePlugin.getSelectedAllNode()).thenReturn(list);
    plugin.getCopyPluginAction().setPlugin(copyPastePlugin);

    plugin.createKeyEventDispatcher().dispatchKeyEvent(event);

    String newData = cp.getClipboardContents();

    assertNotEquals("Data in clipboard didn't change", data, newData);

  }

  @Test
  public void testPasteAction() {
    CopyPastePlugin plugin = new CopyPastePlugin();
    KeyEvent event = Mockito.mock(KeyEvent.class);
    when(event.getID()).thenReturn(KeyEvent.KEY_PRESSED);
    when(event.getKeyCode()).thenReturn(java.awt.event.KeyEvent.VK_V);
    when(event.isControlDown()).thenReturn(true);
    when(event.isAltDown()).thenReturn(true);

    CopyPastePlugin copyPastePlugin = Mockito.mock(CopyPastePlugin.class);
    PluginListOf list = createPluginListWithSpecies(1);
    when(copyPastePlugin.getSelectedAllNode()).thenReturn(list);
    plugin.getPastePluginAction().setPlugin(copyPastePlugin);

    plugin.createKeyEventDispatcher().dispatchKeyEvent(event);
  }

}
