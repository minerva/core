package lcsb.mapviewer.cdplugin.copypaste;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import org.mockito.Mockito;

import jp.sbi.celldesigner.plugin.PluginListOf;
import lcsb.mapviewer.cdplugin.CdPluginFunctions;

public class CopyActionTest extends CdPluginFunctions {

  private SystemClipboard cp = new SystemClipboard();

  @Test
  public void testCopyFromSpecies() {
    CopyAction action = new CopyAction();
    String data = cp.getClipboardContents();

    PluginListOf list = createPluginListWithSpecies(2);
    action.performAnnotation(Mockito.mock(CopyPastePlugin.class), list);

    String newData = cp.getClipboardContents();

    assertNotEquals("Data in clipboard didn't change", data, newData);
  }

  @Test
  public void testCopyFromReaction() {
    CopyAction action = new CopyAction();
    String data = cp.getClipboardContents();

    PluginListOf list = createPluginListWithReaction(2);
    action.performAnnotation(Mockito.mock(CopyPastePlugin.class), list);

    String newData = cp.getClipboardContents();

    assertNotEquals("Data in clipboard didn't change", data, newData);
  }

  @Test
  public void testCopyFromEmptyList() {
    CopyAction action = new CopyAction();
    String data = cp.getClipboardContents();

    PluginListOf list = createPluginListWithSpecies(0);
    action.performAnnotation(Mockito.mock(CopyPastePlugin.class), list);

    String newData = cp.getClipboardContents();

    assertEquals("Data in clipboard didn't change", data, newData);
  }

}
