package lcsb.mapviewer.cdplugin.copypaste;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.times;

import org.junit.Test;
import org.mockito.Mockito;

import jp.sbi.celldesigner.plugin.PluginListOf;
import jp.sbi.celldesigner.plugin.PluginReaction;
import jp.sbi.celldesigner.plugin.PluginSpecies;
import jp.sbi.celldesigner.plugin.PluginSpeciesAlias;
import lcsb.mapviewer.cdplugin.CdPluginFunctions;

public class PasteActionTest extends CdPluginFunctions {

  private SystemClipboard cp = new SystemClipboard();

  @Test
  public void testPerformAnnotationOnSpecies() {
    cp.setClipboardContents("\n\n\n\n\n\n");
    PasteAction action = new PasteAction();
    PluginListOf speciesList = createPluginListWithSpecies(1);
    CopyPastePlugin plugin = Mockito.mock(CopyPastePlugin.class);

    action.performAnnotation(plugin, speciesList);
    Mockito.verify(plugin, times(2)).notifySBaseChanged(any());

    PluginSpeciesAlias alias = (PluginSpeciesAlias) speciesList.get(0);
    PluginSpecies species = alias.getSpecies();

    Mockito.verify(species, atLeast(1)).setNotes(anyString());
    Mockito.verify(species, atLeast(1)).setAnnotationString(anyString());
  }

  @Test
  public void testPerformAnnotationOnReaction() {
    cp.setClipboardContents("\n\n\n\n\n\n");
    PasteAction action = new PasteAction();
    PluginListOf speciesList = createPluginListWithReaction(1);
    CopyPastePlugin plugin = Mockito.mock(CopyPastePlugin.class);

    action.performAnnotation(plugin, speciesList);
    Mockito.verify(plugin, times(1)).notifySBaseChanged(any());

    PluginReaction reaction = (PluginReaction) speciesList.get(0);

    Mockito.verify(reaction, atLeast(1)).setNotes(anyString());
    Mockito.verify(reaction, atLeast(1)).setAnnotationString(anyString());
  }

}
