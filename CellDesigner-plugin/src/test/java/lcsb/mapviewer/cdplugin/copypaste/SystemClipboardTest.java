package lcsb.mapviewer.cdplugin.copypaste;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.cdplugin.CdPluginFunctions;

public class SystemClipboardTest extends CdPluginFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testClipboard() throws Exception {
    SystemClipboard sc = new SystemClipboard();
    sc.setClipboardContents("TEST");

    SystemClipboard sc2 = new SystemClipboard();
    assertEquals("TEST", sc2.getClipboardContents());
  }

  @Test
  public void testInvalidClipboard() throws Exception {
    SystemClipboard sc2 = new SystemClipboard();

    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    clipboard.setContents(new Transferable() {

      @Override
      public DataFlavor[] getTransferDataFlavors() {
        return null;
      }

      @Override
      public boolean isDataFlavorSupported(final DataFlavor flavor) {
        return true;
      }

      @Override
      public Object getTransferData(final DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        throw new UnsupportedFlavorException(flavor);
      }
    }, sc2);

    assertEquals(null, sc2.getClipboardContents());
  }

  @Test
  public void testLostOwnership() throws Exception {
    SystemClipboard sc = new SystemClipboard();
    sc.lostOwnership(null, null);
  }

  @Test
  public void testEmpty() throws Exception {
    SystemClipboard sc2 = new SystemClipboard();
    sc2.setClipboardContents(null);
    assertNull(sc2.getClipboardContents());
  }

}
