package lcsb.mapviewer.cdplugin.copypaste;

import java.awt.event.ActionEvent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jp.sbi.celldesigner.MainWindow;
import jp.sbi.celldesigner.plugin.PluginAction;
import jp.sbi.celldesigner.plugin.PluginListOf;

/**
 * This class represent action that paste annotations and notes from clipboard
 * into selected species/reactions.
 * 
 * @author Piotr Gawron
 * 
 */
public class PastePluginAction extends PluginAction {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger();
  /**
   * Plugin that access this action.
   */
  private CopyPastePlugin plugin = null;

  /**
   * Default constructor.
   * 
   * @param plugin
   *          {@link #plugin}
   * @param win
   *          {@link #window}
   */
  public PastePluginAction(final CopyPastePlugin plugin, final MainWindow win) {
    this.setPlugin(plugin);
  }

  @Override
  public void myActionPerformed(final ActionEvent e) {
    try {
      PasteAction annotateAction = new PasteAction();
      PluginListOf list = getPlugin().getSelectedAllNode();
      annotateAction.performAnnotation(plugin, list);
    } catch (final Exception ex) {
      logger.error(ex, ex);
    }
  }

  protected CopyPastePlugin getPlugin() {
    return plugin;
  }

  protected void setPlugin(final CopyPastePlugin plugin) {
    this.plugin = plugin;
  }

}