package lcsb.mapviewer.cdplugin.copypaste;

import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jp.sbi.celldesigner.plugin.PluginListOf;
import jp.sbi.celldesigner.plugin.PluginReaction;
import jp.sbi.celldesigner.plugin.PluginSBase;
import jp.sbi.celldesigner.plugin.PluginSpecies;
import jp.sbi.celldesigner.plugin.PluginSpeciesAlias;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.model.map.MiriamData;

/**
 * CD Plugin action responsible for translating annotation stored in notes to
 * xml format stored in annotations.
 * 
 * @author Piotr Gawron
 * 
 */
public class PasteAction extends CopyPasteAbstractAction {
  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  /**
   * Method that perform annotation of a CellDesigner species listed in
   * speciesList.
   * 
   * @param plug
   *          - a plugin object
   * @param speciesList
   *          - list of species to annotate
   */
  public void performAnnotation(final CopyPastePlugin plug, final PluginListOf speciesList) {
    XmlAnnotationParser xap = new XmlAnnotationParser();

    SystemClipboard sc = new SystemClipboard();
    String value = sc.getClipboardContents();

    Pair<Set<MiriamData>, String> data = getAnnotationDataFromClipboardString(value);
    String notes = data.getRight();
    Set<MiriamData> set = data.getLeft();

    int size = speciesList.size();
    for (int i = 0; i < size; i++) {
      PluginSBase element = speciesList.get(i);
      if (element instanceof PluginSpeciesAlias) {
        PluginSpeciesAlias alias = (PluginSpeciesAlias) element;
        PluginSpecies species = alias.getSpecies();
        species.setNotes(notes);
        species.setAnnotationString(xap.dataSetToXmlString(set, species.getId()));
        plug.notifySBaseChanged(element);
        plug.notifySBaseChanged(species);
      } else if (element instanceof PluginReaction) {
        PluginReaction reaction = (PluginReaction) element;
        reaction.setNotes(notes);
        reaction.setAnnotationString(xap.dataSetToXmlString(set, reaction.getId()));
        plug.notifySBaseChanged(element);
      } else {
        logger.warn("Unknown class type :" + element.getClass());
      }
    }
  }

}
