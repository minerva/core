package lcsb.mapviewer.cdplugin.copypaste;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class allowing access to system clipboard.
 * 
 * @author Piotr Gawron
 * 
 */
public class SystemClipboard implements ClipboardOwner {
  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  @Override
  public void lostOwnership(final Clipboard clipboard, final Transferable contents) {
  }

  /**
   * Get the String residing on the clipboard.
   *
   * @return any text found on the Clipboard; if none found, return an empty
   *         String.
   */
  public String getClipboardContents() {
    String result = null;
    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    // odd: the Object param of getContents is not currently used
    Transferable contents = clipboard.getContents(null);
    boolean hasTransferableText = contents.isDataFlavorSupported(DataFlavor.stringFlavor);
    if (hasTransferableText) {
      try {
        result = (String) contents.getTransferData(DataFlavor.stringFlavor);
      } catch (final Exception ex) {
        logger.error(ex, ex);
      }
    }
    return result;
  }

  /**
   * Place a String on the clipboard, and make this class the owner of the
   * Clipboard's contents.
   *
   * @param string
   *          what we want to put into clipboard
   */
  public void setClipboardContents(final String string) {
    StringSelection stringSelection = new StringSelection(string);
    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    clipboard.setContents(stringSelection, this);
  }

}
