package lcsb.mapviewer.cdplugin.copypaste;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;

/**
 * This abstract class defines common function for {@link CopyAction} and
 * {@link PasteAction}.
 * 
 * @author Piotr Gawron
 * 
 */
public class CopyPasteAbstractAction {
  /**
   * Prefix used for serialization/deserialization of {@link MiriamData} to
   * string stored in system clipboard.
   */
  private static final String PREFIX = "[MIRIAM]";
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger();

  /**
   * Serializes {@link MiriamData} into string that will be stored into system
   * clipboard.
   * 
   * @param md
   *          object to serialize
   * @return string representation of {@link MiriamData}
   */
  protected String serialize(final MiriamData md) {
    return PREFIX + "\t" + md.getRelationType().getStringRepresentation() + "\t" + md.getDataType().getUris().get(0)
        + "\t" + md.getResource();
  }

  /**
   * Deserialize {@link MiriamData} from string creted by
   * {@link #serialize(MiriamData)} method.
   * 
   * @param string
   *          string representation of {@link MiriamData}
   * @return {@link MiriamData} obtained from input string
   */
  protected MiriamData deserialize(final String string) {
    if (string == null) {
      return null;
    }
    String[] tmp = string.split("\t");

    if (tmp.length == 4) {
      if (PREFIX.equals(tmp[0])) {
        String relation = tmp[1];
        String uri = tmp[2];
        String resource = tmp[3];
        MiriamType mt = MiriamType.getTypeByUri(uri);
        MiriamRelationType mrt = MiriamRelationType.getTypeByStringRepresentation(relation);
        if (mt == null || mrt == null) {
          return null;
        }
        return new MiriamData(mrt, mt, resource);
      }
    }
    return null;
  }

  /**
   * Returns serialized string of annotations and notes.
   * 
   * @param annotationString
   *          xml string representing annotations in CellDesigner
   * @param notesString
   *          flat notes String
   * @return serialized string
   * @throws InvalidXmlSchemaException
   *           thrown when xmlString is invalid
   */
  protected String getCopyString(final String annotationString, final String notesString) throws InvalidXmlSchemaException {
    XmlAnnotationParser xap = new XmlAnnotationParser();
    Set<MiriamData> set = xap.parse(annotationString, null);
    StringBuilder result = new StringBuilder();
    for (final MiriamData md : set) {
      result.append(serialize(md) + "\n");
    }
    result.append(notesString);
    return result.toString();
  }

  /**
   * Deserialize string created by {@link #getCopyString(String, String)}
   * method. Used to create {@link MiriamData} set and notes from clipboard.
   * 
   * @param value
   *          string for deserialization
   * 
   * @return {@link Pair} of {@link MiriamData} set and notes string
   */
  protected Pair<Set<MiriamData>, String> getAnnotationDataFromClipboardString(final String value) {
    if (value == null) {
      return new Pair<Set<MiriamData>, String>(null, null);
    }
    String[] rows = value.split("\n");

    StringBuilder sb = new StringBuilder("");

    boolean miriam = true;

    Set<MiriamData> set = new HashSet<MiriamData>();
    for (int i = 0; i < rows.length; i++) {
      String row = rows[i];
      if (miriam) {
        MiriamData md = deserialize(row);
        if (md != null) {
          set.add(md);
        } else {
          miriam = false;
        }
      }
      if (!miriam) {
        sb.append(row);
        sb.append("\n");
      }
    }
    String notes = sb.toString().trim();

    return new Pair<Set<MiriamData>, String>(set, notes);
  }
}