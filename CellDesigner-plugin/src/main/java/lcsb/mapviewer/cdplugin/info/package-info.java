/**
 * In this package there is implementation of a simple plugin that presents
 * informations about CellDesigner elements. More information can be found in
 * {@link lcsb.mapviewer.cdplugin.info.InfoPlugin}.
 */
package lcsb.mapviewer.cdplugin.info;
