package lcsb.mapviewer.cdplugin.info;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Timer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jp.sbi.celldesigner.plugin.CellDesignerPlugin;
import jp.sbi.celldesigner.plugin.PluginListOf;
import jp.sbi.celldesigner.plugin.PluginSBase;
import jp.sbi.celldesigner.plugin.PluginSpeciesAlias;

/**
 * This class represent a plugin to CellDesigner that add info text box with
 * informations about species. Plugin run in pooling mode (there is no other way
 * in CellDesigner...).
 * <p>
 * It overrides <i>CTRL + T shortcut</i> - shows/hide info text window.
 * </p>
 * 
 * @author Piotr Gawron
 * 
 */
public class InfoPlugin extends CellDesignerPlugin {

  /**
   * What is the time between two consecutive checks if the set of selected
   * elements changed.
   */
  protected static final int DELAY_BETWEEN_ON_SELECT_LISTENER_CHECKS = 250;

  /**
   * Maximum number of elements that will be described in the text box.
   */
  private static final int MAX_VISIBLE_ELEMENTS = 5;

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger();

  /**
   * Listener called when set of selected elements in cell designer changed.
   */
  private ActionListener onChangeSelectListener;

  /**
   * Frame where visualization is generated.
   */
  private InfoFrame frame;

  /**
   * Default constructor. Creates menu and InfoFrame.
   */
  public InfoPlugin() {
    try {
      // load logger information

      // PropertyConfigurator.configure("D:/log4j.properties");
      logger.debug("Loading info plugin...");

      frame = InfoFrame.getInstance();

      // create keyboard listener for shortcuts
      KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
        @Override
        public boolean dispatchKeyEvent(final KeyEvent e) {
          if (KeyEvent.KEY_PRESSED == e.getID()) {
            // if someone hits CTRL+T then open/hide info window
            if (e.getKeyCode() == java.awt.event.KeyEvent.VK_T
                && e.getModifiers() == java.awt.event.InputEvent.CTRL_MASK) {
              frame.setVisible(!frame.isVisible());
              return true;
            }
          }
          return false;
        }
      });

      // create a listener for refreshing selected elements
      onChangeSelectListener = new ActionListener() {

        /**
         * This string contains a simple hash of all species shown in the last
         * run of this listener. Due to this information we don't have to
         * refresh this information at every run.
         */
        private String selectedElementsId = "";

        @Override
        public void actionPerformed(final ActionEvent arg0) {
          // if there is no open model then getSelectedAllNode() will throw an
          // exception...
          List<PluginSpeciesAlias> aliasList = getListOfAlias();
          try {
            ArrayList<PluginSpeciesAlias> v = new ArrayList<PluginSpeciesAlias>();
            String newId = "";
            // max five elements should be shown in infofrmae
            for (int i = 0; i < Math.min(aliasList.size(), MAX_VISIBLE_ELEMENTS); i++) {
              PluginSpeciesAlias alias = aliasList.get(i);
              v.add(alias);
              newId += alias.getAliasID();
            }
            // if the cache is exactly the same as in the previous run then
            // don't update infoframe
            if (!selectedElementsId.equals(newId)) {
              selectedElementsId = newId;
              frame.setSpecies(v);
            }
          } catch (final Exception exception) {
            logger.error(exception, exception);
          }

        }
      };

      Timer timer = new Timer(DELAY_BETWEEN_ON_SELECT_LISTENER_CHECKS, onChangeSelectListener);
      timer.start();

    } catch (final Exception exception) {
      logger.error("Unhandled exception. ", exception);
    }
  }

  @Override
  public void addPluginMenu() {
  }

  @Override
  public void SBaseAdded(final PluginSBase arg0) {

  }

  @Override
  public void SBaseChanged(final PluginSBase arg0) {

  }

  @Override
  public void SBaseDeleted(final PluginSBase arg0) {
  }

  @Override
  public void modelOpened(final PluginSBase arg0) {

  }

  @Override
  public void modelSelectChanged(final PluginSBase arg0) {

  }

  @Override
  public void modelClosed(final PluginSBase arg0) {

  }

  public List<PluginSpeciesAlias> getListOfAlias() {
    List<PluginSpeciesAlias> result = new ArrayList<>();
    PluginListOf list = new PluginListOf();
    try {
      list = getSelectedAllNode();
      for (int i = 0; i < list.size(); i++) {
        if (list.get(i) instanceof PluginSpeciesAlias) {
          PluginSpeciesAlias alias = (PluginSpeciesAlias) list.get(i);
          result.add(alias);
        }
      }
    } catch (final Exception e) {
    }
    return result;
  }

}