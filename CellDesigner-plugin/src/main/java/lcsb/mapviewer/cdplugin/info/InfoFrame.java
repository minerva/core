package lcsb.mapviewer.cdplugin.info;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jp.sbi.celldesigner.plugin.PluginSpeciesAlias;

/**
 * This class represent a frame that visualize MIRIAM data and notes for a
 * species. Only one instance of JFrame is stored in the class (only one window
 * of this type can be created).
 * 
 * @author Piotr Gawron
 * 
 */
public final class InfoFrame {

  /**
   * What should be the distance between description of different aliases.
   */
  private static final int DISTANCE_BETWEEN_PANELS = 5;

  /**
   * How many columns should be in the info panel grid.
   */
  private static final int GRID_INFO_COL_NUMBER = 2;

  /**
   * How many rows should be in the info panel grid.
   */
  private static final int GRID_INFO_ROW_NUMBER = 3;

  /**
   * Frame height.
   */
  private static final int FRAME_HEIGHT = 200;

  /**
   * Frame width.
   */
  private static final int FRAME_WIDTH = 200;

  /**
   * Font size of the caption label.
   */
  private static final int CAPTION_FONT_SIZE = 18;
  /**
   * Singleton instance of this class (there can be only one instance of the
   * frame).
   */
  private static InfoFrame instance = null;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger();
  /**
   * Instance of the JFrame representation of windows.
   */
  private JFrame frame = null;

  /**
   * List of selected aliases.
   */
  private List<PluginSpeciesAlias> species = new ArrayList<>();

  /**
   * Default constructor.
   */
  private InfoFrame() {
    int width = FRAME_WIDTH;
    int height = FRAME_HEIGHT;

    // check the resolution of the screen
    Toolkit tk = Toolkit.getDefaultToolkit();
    Dimension screenSize = tk.getScreenSize();
    int screenHeight = screenSize.height;
    int screenWidth = screenSize.width;

    // if the resultion of the screen changed in a meantime adjust position to
    // be
    // visible on the screen
    width = Math.min(width, screenWidth);
    height = Math.min(height, screenHeight);

    int posX = (screenWidth - width) / 2;
    int posY = (screenHeight - height) / 2;

    frame = new JFrame("Element information");
    frame.setSize(width, height);
    frame.setLocation(posX, posY);
    frame.setAlwaysOnTop(true);
    frame.setVisible(false);
  }

  /**
   * Returns singleton instance of the class.
   * 
   * @return singleton instance of this class
   */
  public static InfoFrame getInstance() {
    if (InfoFrame.instance == null) {
      instance = new InfoFrame();
    }
    return instance;
  }

  /**
   * Updates information about species in the form.
   */
  protected void updateSpecies() {
    int panels = species.size() + 1;

    JPanel controlArea = new JPanel(new GridLayout(panels, 1));
    controlArea.setLayout(new BoxLayout(controlArea, BoxLayout.Y_AXIS));
    // for every species create a panel and add it to general form
    for (final PluginSpeciesAlias sp : this.species) {
      JPanel panel = getPanelViewForSpecies(sp);
      controlArea.add(panel);
    }
    // after adding all species add a text area which is "invisible", the main
    // purpose of it is to fill the empty space in the form
    JTextArea text = new JTextArea();
    text.setEditable(false);
    text.setBackground(frame.getBackground());
    controlArea.add(text);

    Container content = frame.getContentPane();
    content.removeAll();

    // the whole panel should be scrollable
    JScrollPane scrollBar = new JScrollPane(controlArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
        JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    scrollBar.setPreferredSize(new Dimension(0, 0));
    content.add(scrollBar);

    frame.validate();
  }

  /**
   * This method create a panel for a species with all information to be
   * presented.
   *
   * @param species
   *          CellDesigner species alias
   * @return {@link JPanel} object with information about species
   */
  private JPanel getPanelViewForSpecies(final PluginSpeciesAlias species) {
    JPanel result = new JPanel();
    result.setLayout(new BoxLayout(result, BoxLayout.Y_AXIS));

    // create a caption
    JPanel tmpPanel = new JPanel(new GridLayout(GRID_INFO_ROW_NUMBER, GRID_INFO_COL_NUMBER));
    JLabel label = new JLabel("Element: ", JLabel.RIGHT);
    label.setFont(getCaptionFont());
    tmpPanel.add(label, Component.RIGHT_ALIGNMENT);
    JTextField textField = new JTextField(species.getSpecies().getName(), JLabel.LEFT);
    textField.setFont(getCaptionFont());
    textField.setEditable(false);
    tmpPanel.add(textField, Component.LEFT_ALIGNMENT);

    tmpPanel.add(new JLabel("Alias id: ", JLabel.RIGHT), Component.RIGHT_ALIGNMENT);
    textField = new JTextField(species.getAliasID(), JLabel.LEFT);
    textField.setEditable(false);
    tmpPanel.add(textField, Component.LEFT_ALIGNMENT);

    tmpPanel.add(new JLabel("Species id: ", JLabel.RIGHT), Component.RIGHT_ALIGNMENT);
    textField = new JTextField(species.getSpecies().getId(), JLabel.LEFT);
    textField.setEditable(false);
    tmpPanel.add(textField, Component.LEFT_ALIGNMENT);

    result.add(tmpPanel);

    // add empty space between labels
    result.add(Box.createRigidArea(new Dimension(DISTANCE_BETWEEN_PANELS, DISTANCE_BETWEEN_PANELS)));

    return result;
  }

  protected Font getCaptionFont() {
    return new Font("Default", Font.PLAIN, CAPTION_FONT_SIZE);
  }

  /**
   * Returns info if the frame is visible.
   *
   * @return info if the frame is visible
   */
  public boolean isVisible() {
    return frame.isVisible();
  }

  /**
   * Set visible flag for {@link #frame}.
   *
   * @param visible
   *          should the frame be visible or not
   */
  public void setVisible(final boolean visible) {
    frame.setVisible(visible);
  }

  /**
   * Sets frame title.
   *
   * @param title
   *          title of the frame
   */
  public void setTitle(final String title) {
    frame.setTitle(title);
  }

  /**
   * Returns frame title.
   *
   * @return frame title
   */
  public JFrame getFrame() {
    return frame;
  }

  /**
   * Set {@link JFrame#defaultCloseOperation}.
   *
   * @param type
   *          new {@link JFrame#defaultCloseOperation} value
   */
  public void setDefaultCloseOperation(final int type) {
    frame.setDefaultCloseOperation(type);
  }

  /**
   * Set {@link JFrame#setAlwaysOnTop(boolean)}.
   *
   * @param always
   *          new {@link JFrame#isAlwaysOnTop()} value
   */
  public void setAlwaysOnTop(final boolean always) {
    frame.setAlwaysOnTop(always);
  }

  public List<PluginSpeciesAlias> getSpecies() {
    return species;
  }

  /**
   * This method assign a set of species to this form.
   *
   * @param species
   *          - species to be shown in the form
   */
  public void setSpecies(final List<PluginSpeciesAlias> species) {
    this.species = species;
    updateSpecies();
  }

}
