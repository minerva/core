package lcsb.mapviewer.persist.dao;

import lcsb.mapviewer.model.Article;

public enum ArticleProperty implements MinervaEntityProperty<Article> {
  AUTHOR,
  IS_MISSING,
  JOURNAL,
  MODEL,
  PUBMED_ID,
  TEXT,
  TITLE,
  YEAR,
}
