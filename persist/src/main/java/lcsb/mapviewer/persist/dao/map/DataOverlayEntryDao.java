package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.BaseDao;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class DataOverlayEntryDao extends BaseDao<DataOverlayEntry, DataOverlayEntryProperty> {

  public DataOverlayEntryDao() {
    super(DataOverlayEntry.class);
  }

  @Override
  protected Predicate createPredicate(final Map<DataOverlayEntryProperty, Object> filterOptions, final Root<DataOverlayEntry> root) {
    final CriteriaBuilder builder = getSession().getCriteriaBuilder();
    final List<Predicate> predicates = new ArrayList<>();

    for (final DataOverlayEntryProperty key : filterOptions.keySet()) {
      if (key.equals(DataOverlayEntryProperty.PROJECT_ID)) {
        final Join<DataOverlay, Project> overlayJoin = root.join("dataOverlay");
        final Join<DataOverlay, Project> projectJoin = overlayJoin.join("project");

        final Predicate predicate = builder.and(
            builder.equal(projectJoin.get("projectId"), filterOptions.get(key)));
        predicates.add(predicate);
      } else if (key.equals(DataOverlayEntryProperty.USER_LOGIN)) {
        final Join<DataOverlay, Project> overlayJoin = root.join("dataOverlay");
        final Join<DataOverlay, User> userJoin = overlayJoin.join("creator");

        final List<Predicate> orPredicates = new ArrayList<>();

        final Predicate loginPredicate = builder.and(
            builder.equal(userJoin.get("login"), filterOptions.get(key)));

        final Predicate publicPredicate = builder.and(
            builder.isTrue(overlayJoin.get("isPublic")));
        orPredicates.add(loginPredicate);
        orPredicates.add(publicPredicate);

        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
      } else if (key.equals(DataOverlayEntryProperty.DATA_OVERLAY_ID)) {
        final Join<DataOverlay, Project> overlayJoin = root.join("dataOverlay");

        final Predicate predicate = builder.and(
            builder.equal(overlayJoin.get("id"), filterOptions.get(key)));
        predicates.add(predicate);
      } else if (key.equals(DataOverlayEntryProperty.ID)) {
        final Predicate predicate = builder.and(builder.equal(root.get("id"), filterOptions.get(key)));
        predicates.add(predicate);
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    final Predicate predicate = builder.and(predicates.toArray(new Predicate[0]));
    return predicate;
  }

}
