package lcsb.mapviewer.persist.dao.map;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.persist.dao.BaseDao;

@Repository
public class OverviewImageDao extends BaseDao<OverviewImage, OverviewImageProperty> {

  public OverviewImageDao() {
    super(OverviewImage.class);
  }

  @Override
  protected Predicate createPredicate(final Map<OverviewImageProperty, Object> filterOptions, final Root<OverviewImage> root) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    List<Predicate> predicates = new ArrayList<>();

    for (OverviewImageProperty key : filterOptions.keySet()) {
      if (key.equals(OverviewImageProperty.PROJECT_ID)) {
        Join<OverviewImage, Project> projectJoin = root.join("project");
        Predicate predicate = builder.and(
            builder.equal(projectJoin.get("projectId"), filterOptions.get(key)));
        predicates.add(predicate);
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    Predicate predicate = builder.and(predicates.toArray(new Predicate[0]));
    return predicate;

  }

}
