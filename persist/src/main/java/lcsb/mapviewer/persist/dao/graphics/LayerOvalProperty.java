package lcsb.mapviewer.persist.dao.graphics;

import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum LayerOvalProperty implements MinervaEntityProperty<LayerOval> {
  ID,
  LAYER_ID,
  MAP_ID,
  PROJECT_ID,
}
