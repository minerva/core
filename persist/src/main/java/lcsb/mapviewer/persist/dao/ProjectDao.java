package lcsb.mapviewer.persist.dao;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectLogEntry;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.OverviewLink;
import lcsb.mapviewer.model.map.layout.ProjectBackground;
import lcsb.mapviewer.model.map.layout.ProjectBackgroundImageLayer;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerImage;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.SubmodelConnection;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.overlay.DataOverlayGroup;
import lcsb.mapviewer.model.overlay.GeneVariant;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.dialect.Dialect;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Data access object class for Project objects.
 *
 * @author Piotr Gawron
 */
@Repository
public class ProjectDao extends BaseDao<Project, ProjectProperty> {

  /**
   * Default constructor.
   */
  public ProjectDao() {
    super(Project.class);
  }

  /**
   * Returns project with the given {@link Project#getProjectId() project identifier} .
   *
   * @param projectId {@link Project#getProjectId() project identifier}
   * @return project for the given name
   */
  public Project getProjectByProjectId(final String projectId) {
    final Project project = getByParameter("projectId", projectId);

    return project;
  }

  /**
   * Returns project that contains model with a given id.
   *
   * @param id id of the model
   * @return project that contains model with a given id
   */
  public Project getProjectForModelId(final Integer id) {
    final List<?> list = getSession()
        .createQuery("select project from " + ModelData.class.getSimpleName() + " model_t where model_t.id = :id")
        .setParameter("id", id).list();
    if (list.size() == 0) {
      return null;
    }
    return (Project) list.get(0);
  }

  /**
   * Returns information if the project with given name exists.
   *
   * @param projectName name of the project
   */
  public boolean isProjectExistsByName(final String projectName) {
    @SuppressWarnings("unchecked") final List<Long> list = getSession()
        .createQuery("select count(*) from " + getClazz().getSimpleName() + " where projectId = :projectId")
        .setParameter("projectId", projectName).list();
    return list.get(0) > 0;
  }

  public long getNextId() {
    final Dialect dialect = getDialect();
    final NativeQuery<?> sqlQuery = getSession().createNativeQuery(dialect.getSequenceNextValString("project_sequence"));
    final Object result = sqlQuery.getResultList().get(0);
    // postgres database returns BigInteger
    if (result instanceof BigInteger) {
      return ((BigInteger) sqlQuery.getResultList().get(0)).longValue();
    } else { // hsql database returns Integer
      return ((Integer) sqlQuery.getResultList().get(0)).longValue();
    }

  }

  @Override
  public void delete(final Project p) {
    final List<String> queries = new ArrayList<>();

    queries.add("DELETE FROM " + GeneVariant.class.getSimpleName() + " where data_overlay_entry_id in"
        + " (select id from " + DataOverlayEntry.class.getSimpleName() + " where data_overlay_id in "
        + " (select id from " + DataOverlay.class.getSimpleName() + " where project_id = :projectId))");

    queries.add("DELETE FROM " + DataOverlayEntry.class.getSimpleName() + " where data_overlay_id in"
        + " (select id from " + DataOverlay.class.getSimpleName() + " where project_id = :projectId)");

    queries.add("DELETE FROM " + DataOverlay.class.getSimpleName() + " where project_id = :projectId");

    queries.add("DELETE FROM " + DataOverlayGroup.class.getSimpleName() + " where project_id = :projectId");
    
    queries.add("DELETE FROM " + ProjectBackgroundImageLayer.class.getSimpleName() + " where model_id in "
        + " (select id from " + ModelData.class.getSimpleName() + " where project_id = :projectId)");

    queries.add("DELETE FROM " + ProjectBackground.class.getSimpleName() + " where project_id = :projectId");

    queries.add("update " + Element.class.getSimpleName() + " set submodel_id = null where model_id in "
        + " (select id from " + ModelData.class.getSimpleName() + " where project_id= :projectId)");
    queries.add("delete from " + SubmodelConnection.class.getSimpleName() + " where from_element_id in "
        + " (select id from " + Element.class.getSimpleName() + " where model_id in "
        + " (select id from " + ModelData.class.getSimpleName() + " where project_id = :projectId))");
    queries.add("delete from " + SubmodelConnection.class.getSimpleName() + " where submodel_id in "
        + "(select id from " + ModelData.class.getSimpleName() + " where project_id= :projectId)");
    queries.add("delete from " + SubmodelConnection.class.getSimpleName() + " where parent_model_id in "
        + "(select id from " + ModelData.class.getSimpleName() + " where project_id= :projectId)");
    queries.add("delete from " + ReactionNode.class.getSimpleName() + " where element_id in "
        + " (select id from " + Element.class.getSimpleName() + " where model_id in "
        + " (select id from " + ModelData.class.getSimpleName() + " where project_id= :projectId))");
    queries.add("delete from " + ModificationResidue.class.getSimpleName() + " where id_species_db in "
        + " (select id from " + Element.class.getSimpleName() + " where model_id in "
        + " (select id from " + ModelData.class.getSimpleName() + " where project_id= :projectId))");
    queries.add("delete from " + Element.class.getSimpleName() + " where model_id in "
        + " (select id from " + ModelData.class.getSimpleName() + " where project_id= :projectId)");

    queries.add("delete from " + AbstractNode.class.getSimpleName() + " where reaction_id in "
        + " (select id from " + Reaction.class.getSimpleName() + " where model_id in "
        + " (select id from " + ModelData.class.getSimpleName() + " where project_id= :projectId))");
    queries.add("delete from " + Reaction.class.getSimpleName() + " where model_id in "
        + " (select id from " + ModelData.class.getSimpleName() + " where project_id= :projectId)");

    queries.add("update " + LayerText.class.getSimpleName() + " set glyph_id =null where glyph_id in "
        + " (select id from " + Glyph.class.getSimpleName() + " where project_id= :projectId)");

    queries.add("delete from " + LayerImage.class.getSimpleName() + " where layer_id in "
        + " (select id from " + Layer.class.getSimpleName() + " where model_id in "
        + " (select id from " + ModelData.class.getSimpleName() + " where project_id= :projectId))");

    queries.add("delete from " + LayerText.class.getSimpleName() + " where layer_id in "
        + " (select id from " + Layer.class.getSimpleName() + " where model_id in "
        + " (select id from " + ModelData.class.getSimpleName() + " where project_id= :projectId))");

    queries.add("delete from " + LayerOval.class.getSimpleName() + " where layer_id in "
        + " (select id from " + Layer.class.getSimpleName() + " where model_id in "
        + " (select id from " + ModelData.class.getSimpleName() + " where project_id= :projectId))");

    queries.add("delete from " + LayerRect.class.getSimpleName() + " where layer_id in "
        + " (select id from " + Layer.class.getSimpleName() + " where model_id in "
        + " (select id from " + ModelData.class.getSimpleName() + " where project_id= :projectId))");

    queries.add("delete from " + Layer.class.getSimpleName() + " where model_id in "
        + " (select id from " + ModelData.class.getSimpleName() + " where project_id= :projectId)");

    queries.add("delete from " + OverviewLink.class.getSimpleName() + " where linked_model_id in "
        + " (select id from " + ModelData.class.getSimpleName() + "  where project_id= :projectId)");

    queries.add("delete from " + Comment.class.getSimpleName() + " where model_id in "
        + " (select id from " + ModelData.class.getSimpleName() + "  where project_id= :projectId)");

    queries.add("update " + Project.class.getSimpleName() + " set top_model_id = null where id = :projectId");
    queries.add("DELETE FROM " + ModelData.class.getSimpleName() + " where project_id = :projectId");

    queries.add("delete from " + Glyph.class.getSimpleName() + " where project_id= :projectId");

    queries.add("delete from " + ProjectLogEntry.class.getSimpleName() + " where project_id= :projectId");
    queries.add("DELETE FROM " + Project.class.getSimpleName() + " where id = :projectId");
    for (final String query : queries) {
      getSession().createQuery(query)
          .setParameter("projectId", p.getId())
          .executeUpdate();
    }

  }

  @Override
  protected Predicate createPredicate(final Map<ProjectProperty, Object> filterOptions, final Root<Project> root) {
    final CriteriaBuilder builder = getSession().getCriteriaBuilder();
    final List<Predicate> predicates = new ArrayList<>();

    for (final ProjectProperty key : filterOptions.keySet()) {
      if (key.equals(ProjectProperty.PROJECT_ID)) {
        final Predicate predicate = builder.and(root.get("projectId").in(filterOptions.get(key)));
        predicates.add(predicate);
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    final Predicate predicate = builder.and(predicates.toArray(new Predicate[0]));
    return predicate;

  }

  public Page<Project> getByFilter(final Pageable pageable, final Map<ProjectProperty, List<?>> filter) {

    final String queryString = "select project from " + this.getClazz().getSimpleName() + " project "
        + getJoinsForFilter(filter)
        + getWhereByFilter(filter);

    logger.debug(queryString);

    final Query<?> query = getSession().createQuery(queryString);

    if (pageable.isPaged()) {
      query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
      query.setMaxResults(pageable.getPageSize());
    }

    assignFilterOptions(query, filter);
    final List<Project> list = genericListToProjectList(query.list());

    if (pageable.getSort().isSorted()) {
      throw new NotImplementedException("Sorting is not implemented");
    }

    return new PageImpl<>(list, pageable, getCountByFilter(filter));
  }

  private String getJoinsForFilter(final Map<ProjectProperty, List<?>> filter) {
    final String result = "";
    for (final ProjectProperty property : filter.keySet()) {
      final List<?> list = filter.get(property);
      if (list != null && !list.isEmpty()) {
        switch (property) {
          case USER:
            break;
          default:
            throw new NotImplementedException("Search by " + property + " not implemented");
        }
      }
    }
    return result;
  }

  private void assignFilterOptions(final Query<?> query, final Map<ProjectProperty, List<?>> filter) {
    for (final ProjectProperty property : filter.keySet()) {
      final List<?> list = filter.get(property);
      if (list != null && !list.isEmpty()) {
        switch (property) {
          case USER:
            final List<String> projectIds = new ArrayList<>();
            boolean isAdmin = false;
            for (final Object o : list) {
              final User user = (User) o;
              for (final Privilege privilege : user.getPrivileges()) {
                if (privilege.getType().equals(PrivilegeType.READ_PROJECT)) {
                  projectIds.add(privilege.getObjectId());
                }
                if (privilege.getType().equals(PrivilegeType.IS_ADMIN)) {
                  isAdmin = true;
                }
              }
            }
            if (!isAdmin) {
              query.setParameter("projectIds", projectIds);
            }
            break;
          default:
            throw new NotImplementedException("Search by " + property + " not implemented");
        }
      }
    }
  }

  private String getWhereByFilter(final Map<ProjectProperty, List<?>> filter) {
    final List<String> statements = new ArrayList<>();

    for (final ProjectProperty property : filter.keySet()) {
      final List<?> list = filter.get(property);
      if (list != null && !list.isEmpty()) {
        switch (property) {
          case USER:
            boolean isAdmin = false;
            for (final Object o : list) {
              final User user = (User) o;
              for (final Privilege privilege : user.getPrivileges()) {
                if (privilege.getType().equals(PrivilegeType.IS_ADMIN)) {
                  isAdmin = true;
                  break;
                }
              }
            }
            if (!isAdmin) {
              statements.add(" projectId in :projectIds ");
            }
            break;
          default: {
            throw new NotImplementedException("Search by " + property + " not implemented");
          }
        }
      }
    }
    if (!statements.isEmpty()) {
      return " where " + StringUtils.join(statements, " AND ");
    } else {
      return "";
    }
  }

  public long getCountByFilter(final Map<ProjectProperty, List<?>> filter) {
    final String countQueryString = "select count(distinct project) from " + this.getClazz().getSimpleName() + " project "
        + getJoinsForFilter(filter)
        + getWhereByFilter(filter);
    final Query<?> countQuery = getSession().createQuery(countQueryString);
    assignFilterOptions(countQuery, filter);
    return (Long) countQuery.list().get(0);
  }

  private List<Project> genericListToProjectList(final List<?> list) {
    final List<Project> result = new ArrayList<>();

    for (final Object object : list) {
      result.add((Project) object);
    }
    return result;
  }

}