package lcsb.mapviewer.persist.dao.map.layout;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.map.layout.ReferenceGenomeGeneMapping;
import lcsb.mapviewer.persist.dao.BaseDao;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

/**
 * Data access object for {@link ReferenceGenomeGeneMapping} objects.
 * 
 * @author Piotr Gawron
 *
 */
@Repository
public class ReferenceGenomeGeneMappingDao extends BaseDao<ReferenceGenomeGeneMapping, MinervaEntityProperty<ReferenceGenomeGeneMapping>> {

  /**
   * Default constructor.
   */
  public ReferenceGenomeGeneMappingDao() {
    super(ReferenceGenomeGeneMapping.class);
  }

}
