package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum OverviewImageProperty implements MinervaEntityProperty<OverviewImage> {
  PROJECT_ID,
}
