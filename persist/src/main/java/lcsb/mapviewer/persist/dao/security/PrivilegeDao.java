package lcsb.mapviewer.persist.dao.security;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.persist.dao.BaseDao;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
public class PrivilegeDao extends BaseDao<Privilege, PrivilegeProperty> {

  public PrivilegeDao() {
    super(Privilege.class);
  }

  public Privilege getPrivilegeForTypeAndObjectId(final PrivilegeType type, final String objectId) {
    List<Privilege> privileges = getElementsByParameters(Arrays.asList(
        new Pair<>("type", type),
        new Pair<>("objectId", objectId)));
    if (privileges.size() > 1) {
      throw new IllegalStateException("Impossible DB state. Privileges are constrained to be unique.");
    } else if (privileges.size() == 1) {
      return privileges.get(0);
    } else {
      return new Privilege(type, objectId);
    }
  }

  @Override
  protected Predicate createPredicate(final Map<PrivilegeProperty, Object> filterOptions, final Root<Privilege> root) {
    final CriteriaBuilder builder = getSession().getCriteriaBuilder();
    final List<Predicate> predicates = new ArrayList<>();

    for (final PrivilegeProperty key : filterOptions.keySet()) {
      if (key.equals(PrivilegeProperty.OBJECT_ID)) {
        final Predicate predicate = builder.and(root.get("objectId").in(filterOptions.get(key)));
        predicates.add(predicate);
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    return builder.and(predicates.toArray(new Predicate[0]));
  }

}
