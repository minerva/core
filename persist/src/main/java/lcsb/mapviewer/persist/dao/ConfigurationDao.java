package lcsb.mapviewer.persist.dao;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.ConfigurationOption;

/**
 * Data access object class for Configuration objects.
 * 
 * @author Piotr Gawron
 * 
 */
@Repository
public class ConfigurationDao extends BaseDao<ConfigurationOption, MinervaEntityProperty<ConfigurationOption>> {

  /**
   * Default constructor.
   */
  public ConfigurationDao() {
    super(ConfigurationOption.class);
  }

  /**
   * Returns object with configuration value for the type given in the
   * parameter.
   * 
   * @param type
   *          type of the parameter that we are interested in
   * @return object with configuration value for the type given in the parameter
   */
  public ConfigurationOption getByType(final ConfigurationElementType type) {
    return getByParameter("type", type);
  }

  /**
   * Returns value by the type.
   * 
   * @param type
   *          type of configuration parameter
   * @return value of the specific configuration parameter
   */
  public String getValueByType(final ConfigurationElementType type) {
    ConfigurationOption val = getByParameter("type", type);
    if (val == null) {
      if (type == null) {
        return null;
      } else {
        return type.getDefaultValue();
      }
    } else {
      return val.getValue();
    }
  }
}
