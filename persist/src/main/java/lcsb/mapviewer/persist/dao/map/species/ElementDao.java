package lcsb.mapviewer.persist.dao.map.species;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.persist.dao.BaseDao;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.dialect.HSQLDialect;
import org.hibernate.dialect.PostgreSQL81Dialect;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Repository
public class ElementDao extends BaseDao<Element, ElementProperty> {

  public ElementDao() {
    super(Element.class);
  }

  public List<Element> getByCoordinates(final ModelData map, final Point2D point, final int numberOfElements,
                                        final List<Class<? extends BioEntity>> classes) {
    final String typeClause;
    if (classes.size() > 0) {
      typeClause = " type(element) in :classes ";
    } else {
      typeClause = " not element_type_db like '%COMPARTMENT' and not element_type_db like '%PATHWAY' ";
    }
    final String order = "z desc, width*height";
    final org.hibernate.query.Query<Element> query = getSession()
        .createQuery(" from " + this.getClazz().getSimpleName() + " element where "
            + "model = :map and "
            + "x <= :x and "
            + "y <= :y and "
            + "x + width >= :x and "
            + "y + height >= :y and "
            + typeClause
            + removableAndStatemant()
            + " order by " + order, Element.class)
        .setParameter("map", map)
        .setParameter("x", point.getX())
        .setParameter("y", point.getY())
        .setMaxResults(numberOfElements);

    if (classes.size() > 0) {
      query.setParameter("classes", classes);
    }

    final List<Element> result = query.list();

    if (numberOfElements > result.size()) {
      // first fetch the ids, because otherwise subselect does not work (there
      // is a
      // bug in hibernate that ads named parameters for subselects from order
      // clause
      // that are not used there)
      final org.hibernate.query.Query<Integer> idQuery = getSession()
          .createQuery("select element.id from " + this.getClazz().getSimpleName() + " element where "
              + "model = :map and"
              + typeClause
              + removableAndStatemant()
              + " order by "
              + "least("
              + "function('dist_to_segment', :x, :y, x, y, x, y+height),"
              + "function('dist_to_segment', :x, :y, x, y+height, x+width, y+height),"
              + "function('dist_to_segment', :x, :y, x+width, y+height, x+width, y),"
              + "function('dist_to_segment', :x, :y, x+width, y, x, y)"
              + " ), " + order, Integer.class)
          .setParameter("map", map)
          .setParameter("x", point.getX())
          .setParameter("y", point.getY())
          .setMaxResults(numberOfElements);

      if (classes.size() > 0) {
        idQuery.setParameter("classes", classes);
      }

      final List<Integer> orderedList = idQuery.list();
      if (orderedList.size() > 0) {
        final Map<Integer, Integer> orderId = new HashMap<>();
        for (int i = 0; i < orderedList.size(); i++) {
          orderId.put(orderedList.get(i), i);
        }

        // and now fetch the elements
        final List<Element> elements = getSession()
            .createQuery("from " + this.getClazz().getSimpleName() + " element where element.id in :ids", Element.class)
            .setParameter("ids", orderedList).list();
        elements.sort(new Comparator<Element>() {
          @Override
          public int compare(final Element o1, final Element o2) {
            return orderId.get(o1.getId()) - orderId.get(o2.getId());
          }
        });
        for (final Element element : elements) {
          if (!result.contains(element) && numberOfElements > result.size()) {
            result.add(element);
          }
        }
      }
    }

    return result;
  }

  public List<String> getSuggestedQueries(final List<ModelData> maps) {
    if (maps.size() == 0) {
      return new ArrayList<>();
    }
    final Set<String> entries = new HashSet<>();
    List<String> list = getSession()
        .createQuery(
            "select distinct(trim(both ' ' from lower(name))) from " + this.getClazz().getSimpleName() + " element "
                + " where element.model in :maps",
            String.class)
        .setParameter("maps", maps).list();
    for (final String object : list) {
      if (object != null) {
        entries.add(object);
      }
    }

    list = getSession()
        .createQuery(
            "select distinct(trim(both ' ' from lower(fullName))) from " + this.getClazz().getSimpleName() + " element "
                + " where element.model in :maps",
            String.class)
        .setParameter("maps", maps).list();
    for (final String object : list) {
      if (object != null) {
        entries.add(object);
      }
    }

    list = getSession()
        .createQuery(
            "select distinct(trim(both ' ' from lower(synonyms))) from " + this.getClazz().getSimpleName() + " element "
                + " inner join element.synonyms synonyms"
                + " where element.model in :maps",
            String.class)
        .setParameter("maps", maps).list();
    for (final String object : list) {
      if (object != null) {
        entries.add(object);
      }
    }

    list = getSession()
        .createQuery("select distinct(trim(both ' ' from lower(formerSymbols))) from " + this.getClazz().getSimpleName()
            + " element "
            + " inner join element.formerSymbols formerSymbols"
            + " where element.model in :maps", String.class)
        .setParameter("maps", maps).list();
    for (final String object : list) {
      if (object != null) {
        entries.add(object);
      }
    }

    final List<String> result = new ArrayList<>(entries);
    Collections.sort(result);
    return result;

  }

  public Map<MiriamType, Integer> getAnnotationStatistics(final String projectId) {
    final Map<MiriamType, Integer> result = new HashMap<>();
    final List<?> list = getSession()
        .createQuery(
            "select md.dataType, count(*) as counter from " + getClazz().getSimpleName()
                + " element_t join element_t.miriamData md "
                + " where element_t.model.project.projectId = :projectId"
                + " group by md.dataType")
        .setParameter("projectId", projectId).list();
    for (final Object object : list) {
      final Object[] row = (Object[]) object;
      result.put((MiriamType) row[0], ((Long) row[1]).intValue());
    }
    return result;
  }

  @Override
  protected Predicate createPredicate(final Map<ElementProperty, Object> filterOptions, final Root<Element> root, final CriteriaQuery<?> query) {
    final CriteriaBuilder builder = getSession().getCriteriaBuilder();
    final List<Predicate> predicates = new ArrayList<>();

    for (final ElementProperty key : filterOptions.keySet()) {
      if (key.equals(ElementProperty.MAP)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        if (values.size() > 0) {
          final Join<Element, ModelData> modelJoin = root.join("model");
          final In<Integer> predicate = builder.in(modelJoin.get("id"));
          for (final Object object : values) {
            predicate.value(((ModelData) object).getId());
          }
          predicates.add(predicate);
        } else {
          predicates.add(builder.or());
        }
      } else if (key.equals(ElementProperty.ID)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        final In<Integer> predicate = builder.in(root.get("id"));
        for (final Object object : values) {
          predicate.value((Integer) object);
        }
        predicates.add(predicate);
      } else if (key.equals(ElementProperty.ANNOTATION)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        final Join<Element, MiriamData> miriamJoin = root.join("miriamData");
        final List<Predicate> orPredicates = new ArrayList<>();

        for (final Object object : values) {
          final MiriamData md = (MiriamData) object;
          final Predicate predicate = builder.and(
              builder.like(builder.lower(miriamJoin.get("resource")), md.getResource().toLowerCase()),
              builder.equal(miriamJoin.get("dataType"), md.getDataType()));
          orPredicates.add(predicate);
        }
        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
      } else if (key.equals(ElementProperty.PROJECT_ID)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        final List<Predicate> orPredicates = new ArrayList<>();

        final Join<Element, Project> projectJoin = root.join("model").join("project");

        for (final Object object : values) {
          orPredicates.add(builder.equal(projectJoin.get("projectId"), object));
        }
        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
      } else if (key.equals(ElementProperty.COMPARTMENT_ID) || key.equals(ElementProperty.INCLUDED_IN_COMPARTMENT)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        final List<Predicate> orPredicates = new ArrayList<>();

        final Join<Element, Compartment> compartmentJoin = root.join("compartment");

        for (final Object object : values) {
          orPredicates.add(builder.equal(compartmentJoin.get("id"), object));
        }
        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
      } else if (key.equals(ElementProperty.COMPLEX_ID)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        final List<Predicate> orPredicates = new ArrayList<>();

        final Join<Species, Complex> complexJoin = builder.treat(root, Species.class).join("complex");

        for (final Object object : values) {
          orPredicates.add(builder.equal(complexJoin.get("id"), object));
        }
        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
      } else if (key.equals(ElementProperty.EXCLUDED_FROM_COMPARTMENT)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        final List<Predicate> orPredicates = new ArrayList<>();

        final Join<Species, Complex> compartmentJoin = builder.treat(root, Species.class).join("compartment", JoinType.LEFT);

        orPredicates.add(builder.not(compartmentJoin.get("id").in(values)));
        orPredicates.add(builder.isNull(root.get("compartment")));

        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));

      } else if (key.equals(ElementProperty.MAP_ID)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        final List<Predicate> orPredicates = new ArrayList<>();

        final Join<Element, ModelData> complexJoin = root.join("model");

        for (final Object object : values) {
          orPredicates.add(builder.equal(complexJoin.get("id"), object));
        }
        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
      } else if (key.equals(ElementProperty.NAME)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        final List<Predicate> orPredicates = new ArrayList<>();

        for (final Object object : values) {
          orPredicates.add(builder.equal(builder.lower(root.get("name")), ((String) object).toLowerCase()));
        }
        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
      } else if (key.equals(ElementProperty.PROJECT)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        final List<Predicate> orPredicates = new ArrayList<>();

        final Join<Element, ModelData> modelJoin = root.join("model");

        for (final Object object : values) {
          orPredicates.add(builder.equal(modelJoin.get("project"), object));
        }
        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
      } else if (key.equals(ElementProperty.SUBMAP_CONNECTION)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;

        if (values.size() != 1) {
          throw new NotImplementedException("Only single true false value was expected");
        }

        if (values.contains(Boolean.TRUE)) {
          predicates.add(builder.isNotNull(root.get("submodel")));
        } else if (values.contains(Boolean.FALSE)) {
          predicates.add(builder.isNull(root.get("submodel")));
        } else {
          throw new NotImplementedException("Only single true false value was expected");
        }
      } else if (key.equals(ElementProperty.CLASS)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        final List<Predicate> orPredicates = new ArrayList<>();

        for (final Object object : values) {
          final Class<?> clazz = (Class<?>) object;
          orPredicates.add(builder.equal(root.type(), clazz));
        }
        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
      } else if (key.equals(ElementProperty.TEXT)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;

        if (values.size() != 1) {
          throw new NotImplementedException("Only single query was expected");
        }

        final String text = '%' + ((String) values.get(0)).toLowerCase().trim().replaceAll("[^a-z0-9]", "") + '%';

        final List<Predicate> orPredicates = new ArrayList<>();

        final Join<Element, String> formerSymbolJoin = root.join("formerSymbols", JoinType.LEFT);
        final Join<Element, String> synonym = root.join("synonyms", JoinType.LEFT);

        orPredicates.add(builder.like(getSearchTextByRegex(root.get("elementId"), builder), text));
        orPredicates.add(builder.like(getSearchTextByRegex(root.get("name"), builder), text));
        orPredicates.add(builder.like(getSearchTextByRegex(root.get("fullName"), builder), text));
        orPredicates.add(builder.like(getSearchTextByRegex(formerSymbolJoin, builder), text));
        orPredicates.add(builder.like(getSearchTextByRegex(synonym, builder), text));

        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));

      } else if (key.equals(ElementProperty.PERFECT_TEXT)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;

        if (values.size() != 1) {
          throw new NotImplementedException("Only single query was expected");
        }

        final String text = ((String) values.get(0)).toLowerCase().trim().replaceAll("[^a-z0-9]", "");

        final List<Predicate> orPredicates = new ArrayList<>();
        final Join<Element, String> formerSymbolJoin = root.join("formerSymbols", JoinType.LEFT);
        final Join<Element, String> synonym = root.join("synonyms", JoinType.LEFT);

        orPredicates.add(builder.equal(getSearchTextByRegex(root.get("elementId"), builder), text));
        orPredicates.add(builder.equal(getSearchTextByRegex(root.get("name"), builder), text));
        orPredicates.add(builder.equal(getSearchTextByRegex(root.get("fullName"), builder), text));
        orPredicates.add(builder.equal(getSearchTextByRegex(formerSymbolJoin, builder), text));
        orPredicates.add(builder.equal(getSearchTextByRegex(synonym, builder), text));

        if (StringUtils.isNumeric(text)) {
          orPredicates.add(builder.equal(root.get("id"), Integer.valueOf(text)));
        }

        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));

      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    final Predicate predicate = builder.and(predicates.toArray(new Predicate[0]));
    return predicate;

  }

  private Expression<String> getSearchTextByRegex(final Path<String> path, final CriteriaBuilder builder) {
    if (getDialect() instanceof PostgreSQL81Dialect) {
      return builder.function("regexp_replace", String.class, builder.lower(path),
          builder.literal("[^a-z0-9]"), builder.literal(""), builder.literal("g"));
    } else if (getDialect() instanceof HSQLDialect) {
      return builder.function("regexp_replace", String.class, builder.lower(path),
          builder.literal("[^a-z0-9]"));
    } else {
      throw new NotImplementedException("Unsupported dialect: " + getDialect().getClass());
    }
  }

  public boolean isPerfectMatch(final Element element, final String query) {
    if (query == null) {
      return true;
    }
    final String textQuery = stripString(query);

    return isPerfectName(element, textQuery)
        || isPerfectSynonym(element, textQuery)
        || isPerfectFullName(element, textQuery)
        || isPerfectSymbol(element, textQuery)
        || isPerfectElementId(element, textQuery);
  }

  private boolean isPerfectElementId(final Element element, final String textQuery) {
    return stripString(element.getElementId()).equals(textQuery);
  }

  private boolean isPerfectFullName(final Element element, final String textQuery) {
    return stripString(element.getFullName()).equals(textQuery);
  }

  private boolean isPerfectSynonym(final Element element, final String textQuery) {
    for (final String synonym : element.getSynonyms()) {
      if (stripString(synonym).equals(textQuery)) {
        return true;
      }
    }
    return false;
  }

  private boolean isPerfectName(final Element element, final String textQuery) {
    return stripString(element.getName()).equals(textQuery);
  }

  private String stripString(final String name) {
    if (name == null) {
      return "";
    }
    return name.toLowerCase().replaceAll("[^a-z0-9]", "");
  }

  private boolean isPerfectSymbol(final Element element, final String textQuery) {
    for (final String formerSymbol : element.getFormerSymbols()) {
      if (stripString(formerSymbol).equals(textQuery)) {
        return true;
      }
    }
    return false;
  }

}
