package lcsb.mapviewer.persist.dao.map;

public enum BioEntityProperty {
  ANNOTATION,
  CLASS,
  MAP_ID,
  PERFECT_TEXT,
  PROJECT_ID,
  TEXT,
}
