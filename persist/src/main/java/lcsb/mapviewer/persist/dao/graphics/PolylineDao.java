package lcsb.mapviewer.persist.dao.graphics;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.BaseDao;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Data access object for {@link PolylineDao} class.
 *
 * @author Piotr Gawron
 */
@Repository
public class PolylineDao extends BaseDao<PolylineData, PolylineDataProperty> {

  /**
   * Default constructor.
   */
  public PolylineDao() {
    super(PolylineData.class);
  }

  @Override
  protected Predicate createPredicate(final Map<PolylineDataProperty, Object> filterOptions, final Root<PolylineData> root) {
    final CriteriaBuilder builder = getSession().getCriteriaBuilder();
    final List<Predicate> predicates = new ArrayList<>();

    for (final PolylineDataProperty key : filterOptions.keySet()) {
      if (key.equals(PolylineDataProperty.PROJECT_ID)) {
        final Join<PolylineData, Layer> layerJoin = root.join("layer");
        final Join<Layer, ModelData> modelJoin = layerJoin.join("model");
        final Join<ModelData, Project> projectJoin = modelJoin.join("project");

        final Predicate predicate = builder.and(
            builder.equal(projectJoin.get("projectId"), filterOptions.get(key)));
        predicates.add(predicate);
      } else if (key.equals(PolylineDataProperty.MAP_ID)) {
        final List<?> values = (List<?>) filterOptions.get(key);

        if (!values.isEmpty()) {
          final Join<PolylineData, Layer> layerJoin = root.join("layer");
          final Join<Layer, ModelData> modelJoin = layerJoin.join("model");
          final CriteriaBuilder.In<Integer> predicate = builder.in(modelJoin.get("id"));
          for (final Object object : values) {
            predicate.value((Integer) object);
          }
          predicates.add(predicate);
        } else {
          predicates.add(builder.or());
        }
      } else if (key.equals(PolylineDataProperty.LAYER_ID)) {
        final List<?> values = (List<?>) filterOptions.get(key);

        if (!values.isEmpty()) {
          final Join<PolylineData, Layer> layerJoin = root.join("layer");
          final CriteriaBuilder.In<Integer> predicate = builder.in(layerJoin.get("id"));
          for (final Object object : values) {
            predicate.value((Integer) object);
          }
          predicates.add(predicate);
        } else {
          predicates.add(builder.or());
        }
      } else if (key.equals(PolylineDataProperty.ID)) {
        final List<?> values = (List<?>) filterOptions.get(key);
        final CriteriaBuilder.In<Integer> predicate = builder.in(root.get("id"));
        for (final Object object : values) {
          predicate.value((Integer) object);
        }
        predicates.add(predicate);
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    return builder.and(predicates.toArray(new Predicate[0]));
  }

}
