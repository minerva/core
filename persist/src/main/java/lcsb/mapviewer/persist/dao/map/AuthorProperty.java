package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum AuthorProperty implements MinervaEntityProperty<Author> {
  PROJECT_ID,
  MAP_ID,
  ID
}
