package lcsb.mapviewer.persist.dao.user;

import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.BaseDao;
import org.springframework.stereotype.Repository;

@Repository
public class UserDao extends BaseDao<User, UserProperty> {

  public UserDao() {
    super(User.class, "removed");
  }

  public User getUserByLogin(final String login) {
    return getByParameter("login", login);
  }

  public User getUserByEmail(final String email) {
    return getByParameter("email", email);
  }

  @Override
  public void delete(final User object) {
    object.setRemoved(true);
    object.setLogin("[REMOVED]_" + object.getId() + "_" + object.getLogin());
    update(object);
  }

  public User getUserByOrcidId(final String orcidId) {
    return getByParameter("orcidId", orcidId);
  }
}
