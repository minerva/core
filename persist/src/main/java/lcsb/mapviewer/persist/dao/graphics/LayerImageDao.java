package lcsb.mapviewer.persist.dao.graphics;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerImage;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.BaseDao;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class LayerImageDao extends BaseDao<LayerImage, LayerImageProperty> {

  public LayerImageDao() {
    super(LayerImage.class);
  }

  @Override
  protected Predicate createPredicate(final Map<LayerImageProperty, Object> filterOptions, final Root<LayerImage> root) {
    final CriteriaBuilder builder = getSession().getCriteriaBuilder();
    final List<Predicate> predicates = new ArrayList<>();

    for (final LayerImageProperty key : filterOptions.keySet()) {
      if (key.equals(LayerImageProperty.PROJECT_ID)) {
        final Join<LayerImage, Layer> layerJoin = root.join("layer");
        final Join<Layer, ModelData> modelJoin = layerJoin.join("model");
        final Join<ModelData, Project> projectJoin = modelJoin.join("project");

        final Predicate predicate = builder.and(
            builder.equal(projectJoin.get("projectId"), filterOptions.get(key)));
        predicates.add(predicate);
      } else if (key.equals(LayerImageProperty.MAP_ID)) {
        final List<?> values = (List<?>) filterOptions.get(key);

        if (!values.isEmpty()) {
          final Join<LayerImage, Layer> layerJoin = root.join("layer");
          final Join<Layer, ModelData> modelJoin = layerJoin.join("model");
          final CriteriaBuilder.In<Integer> predicate = builder.in(modelJoin.get("id"));
          for (final Object object : values) {
            predicate.value((Integer) object);
          }
          predicates.add(predicate);
        } else {
          predicates.add(builder.or());
        }
      } else if (key.equals(LayerImageProperty.LAYER_ID)) {
        final List<?> values = (List<?>) filterOptions.get(key);

        if (!values.isEmpty()) {
          final Join<LayerOval, Layer> layerJoin = root.join("layer");
          final CriteriaBuilder.In<Integer> predicate = builder.in(layerJoin.get("id"));
          for (final Object object : values) {
            predicate.value((Integer) object);
          }
          predicates.add(predicate);
        } else {
          predicates.add(builder.or());
        }
      } else if (key.equals(LayerImageProperty.ID)) {
        final List<?> values = (List<?>) filterOptions.get(key);
        final CriteriaBuilder.In<Integer> predicate = builder.in(root.get("id"));
        for (final Object object : values) {
          predicate.value((Integer) object);
        }
        predicates.add(predicate);
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    return builder.and(predicates.toArray(new Predicate[0]));
  }
}
