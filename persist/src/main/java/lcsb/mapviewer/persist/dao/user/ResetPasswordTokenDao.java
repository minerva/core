package lcsb.mapviewer.persist.dao.user;

import java.util.List;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.user.ResetPasswordToken;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.BaseDao;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

@Repository
public class ResetPasswordTokenDao extends BaseDao<ResetPasswordToken, MinervaEntityProperty<ResetPasswordToken>> {

  public ResetPasswordTokenDao() {
    super(ResetPasswordToken.class);
  }

  public ResetPasswordToken getByToken(final String token) {
    return getByParameter("token", token);
  }

  public List<ResetPasswordToken> getByUser(final User user) {
    return getElementsByParameter("user", user);
  }

}
