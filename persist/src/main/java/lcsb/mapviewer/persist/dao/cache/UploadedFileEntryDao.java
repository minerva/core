package lcsb.mapviewer.persist.dao.cache;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.persist.dao.BaseDao;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

/**
 * Data access object for cached values.
 * 
 * @author Piotr Gawron
 * 
 */
@Repository
public class UploadedFileEntryDao extends BaseDao<UploadedFileEntry, MinervaEntityProperty<UploadedFileEntry>> {

  public UploadedFileEntryDao() {
    super(UploadedFileEntry.class, "removed");
  }

  @Override
  public void delete(final UploadedFileEntry entry) {
    entry.setRemoved(true);
    update(entry);
  }

  public void removeOrphans() {
    String query = "DELETE from " + this.getClazz().getSimpleName() + " "
        + " where id not in (select inputData.id from " + DataOverlay.class.getSimpleName() + " where inputData.id is not null) "
        + " and id not in (select file.id from " + Glyph.class.getSimpleName() + " where file.id is not null) "
        + " and id not in (select inputData.id from " + Project.class.getSimpleName() + " where inputData.id is not null)";
    getSession().createQuery(query).executeUpdate();
  }

}
