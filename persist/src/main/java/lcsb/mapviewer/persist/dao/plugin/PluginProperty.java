package lcsb.mapviewer.persist.dao.plugin;

import lcsb.mapviewer.model.plugin.Plugin;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum PluginProperty implements MinervaEntityProperty<Plugin> {
  DEFAULT_VALUE,
  PUBLIC_VALUE,
}
