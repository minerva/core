/**
 * Data access objects for layout substructures.
 */
package lcsb.mapviewer.persist.dao.map.layout;
