package lcsb.mapviewer.persist.dao.map.species;

import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum ModificationResidueProperty implements MinervaEntityProperty<ModificationResidue> {
  ID, PROJECT_ID, MAP_ID, ELEMENT_ID,
}
