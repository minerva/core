package lcsb.mapviewer.persist.dao.map.kinetics;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.BaseDao;

@Transactional
@Service
public class SbmlParameterDao extends BaseDao<SbmlParameter, SbmlParameterProperty> {

  public SbmlParameterDao() {
    super(SbmlParameter.class);
  }

  @Override
  protected Predicate createPredicate(final Map<SbmlParameterProperty, Object> filterOptions, final Root<SbmlParameter> root,
      final CriteriaQuery<?> query) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    List<Predicate> predicates = new ArrayList<>();

    for (SbmlParameterProperty key : filterOptions.keySet()) {
      if (key.equals(SbmlParameterProperty.PROJECT_ID)) {
        Subquery<Integer> projectSubquery = query.subquery(Integer.class);
        Root<Project> projectRoot = projectSubquery.from(Project.class);
        Join<Project, SbmlParameter> modelParameterJoin = projectRoot.join("models").join("parameters");

        Predicate parameterIdMatch = builder.equal(root.get("id"), modelParameterJoin.get("id"));
        Predicate modelIdMatch = builder.equal(projectRoot.get("projectId"), filterOptions.get(key));

        projectSubquery.select(modelParameterJoin.get("id")).where(parameterIdMatch, modelIdMatch);

        Subquery<Integer> mapReactionSubquery = query.subquery(Integer.class);
        Root<Project> projectReactionRoot = mapReactionSubquery.from(Project.class);
        Join<Project, SbmlParameter> projectReactionParameterJoin = projectReactionRoot.join("models").join("reactions").join("kinetics")
            .join("parameters");

        Predicate reactionParameterIdMatch = builder.equal(root.get("id"), projectReactionParameterJoin.get("id"));
        Predicate reactionProjectIdMatch = builder.equal(projectReactionRoot.get("projectId"), filterOptions.get(key));

        mapReactionSubquery.select(projectReactionParameterJoin.get("id")).where(reactionParameterIdMatch, reactionProjectIdMatch);

        predicates.add(builder.or(builder.exists(mapReactionSubquery), builder.exists(projectSubquery)));
      } else if (key.equals(SbmlParameterProperty.MAP_ID)) {
        Subquery<Integer> mapSubquery = query.subquery(Integer.class);
        Root<ModelData> modelRoot = mapSubquery.from(ModelData.class);
        Join<ModelData, SbmlParameter> modelParameterJoin = modelRoot.join("parameters");

        Predicate parameterIdMatch = builder.equal(root.get("id"), modelParameterJoin.get("id"));
        Predicate modelIdMatch = builder.equal(modelRoot.get("id"), filterOptions.get(key));

        mapSubquery.select(modelParameterJoin.get("id")).where(parameterIdMatch, modelIdMatch);

        Subquery<Integer> mapReactionSubquery = query.subquery(Integer.class);
        Root<ModelData> modelReactionRoot = mapReactionSubquery.from(ModelData.class);
        Join<ModelData, SbmlParameter> reactionParameterJoin = modelReactionRoot.join("reactions").join("kinetics").join("parameters");

        Predicate reactionParameterIdMatch = builder.equal(root.get("id"), reactionParameterJoin.get("id"));
        Predicate reactionModelIdMatch = builder.equal(modelReactionRoot.get("id"), filterOptions.get(key));

        mapReactionSubquery.select(reactionParameterJoin.get("id")).where(reactionParameterIdMatch, reactionModelIdMatch);

        predicates.add(builder.or(builder.exists(mapReactionSubquery), builder.exists(mapSubquery)));
      } else if (key.equals(SbmlParameterProperty.ID)) {
        Predicate predicate = builder.and(builder.equal(root.get("id"), filterOptions.get(key)));
        predicates.add(predicate);
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    Predicate predicate = builder.and(predicates.toArray(new Predicate[0]));
    return predicate;

  }

}
