package lcsb.mapviewer.persist.dao.user;

import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum UserProperty implements MinervaEntityProperty<User> {
  ID,
}
