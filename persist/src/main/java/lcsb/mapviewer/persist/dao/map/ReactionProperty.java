package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum ReactionProperty implements MinervaEntityProperty<Reaction> {
  ANNOTATION,
  CLASS,
  MAP,
  ID,
  MAP_ID,
  PARTICIPANT_ID,
  PERFECT_TEXT,
  TEXT,
  PROJECT,
  PROJECT_ID,
}
