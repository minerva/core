package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum DataOverlayEntryProperty implements MinervaEntityProperty<DataOverlayEntry> {
  PROJECT_ID,
  DATA_OVERLAY_ID,
  USER_LOGIN,
  ID
}
