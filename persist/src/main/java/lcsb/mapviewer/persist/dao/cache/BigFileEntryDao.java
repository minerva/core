package lcsb.mapviewer.persist.dao.cache;

import java.util.List;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.cache.BigFileEntry;
import lcsb.mapviewer.persist.dao.BaseDao;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

/**
 * Data access object for cached values.
 * 
 * @author Piotr Gawron
 * 
 */
@Repository
public class BigFileEntryDao extends BaseDao<BigFileEntry, MinervaEntityProperty<BigFileEntry>> {

  /**
   * Default constructor.
   */
  public BigFileEntryDao() {
    super(BigFileEntry.class, "removed");
  }

  /**
   * Return {@link BigFileEntry} identified by remote url.
   * 
   * @param url
   *          url of the file
   * @return {@link BigFileEntry} identified by remote url
   */
  public BigFileEntry getByUrl(final String url) {
    List<?> list = getElementsByParameter("url", url);
    if (list.size() == 0) {
      return null;
    }
    return (BigFileEntry) list.get(0);
  }

  public List<BigFileEntry> getAllByUrl(final String url) {
    return getElementsByParameter("url", url);
  }

  @Override
  public void delete(final BigFileEntry entry) {
    entry.setRemoved(true);
    update(entry);
  }

}
