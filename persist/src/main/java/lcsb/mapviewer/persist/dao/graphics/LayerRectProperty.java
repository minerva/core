package lcsb.mapviewer.persist.dao.graphics;

import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum LayerRectProperty implements MinervaEntityProperty<LayerRect> {
  ID,
  LAYER_ID,
  MAP_ID,
  PROJECT_ID,
}
