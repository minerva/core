package lcsb.mapviewer.persist.dao.plugin;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.plugin.Plugin;
import lcsb.mapviewer.model.plugin.PluginDataEntry;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.BaseDao;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

/**
 * Data access object class for {@link PluginDataEntry} objects.
 * 
 * @author Piotr Gawron
 * 
 */
@Repository
public class PluginDataEntryDao extends BaseDao<PluginDataEntry, MinervaEntityProperty<PluginDataEntry>> {
  /**
   * Default constructor.
   */
  public PluginDataEntryDao() {
    super(PluginDataEntry.class);
  }

  public PluginDataEntry getByKey(final Plugin plugin, final String key, final User user) {
    List<Pair<String, Object>> params = new ArrayList<>();
    params.add(new Pair<>("plugin_id", plugin.getId()));
    params.add(new Pair<>("key", key));
    params.add(new Pair<>("user", user));
    List<PluginDataEntry> entries = getElementsByParameters(params);
    if (entries.size() > 0) {
      return entries.get(0);
    } else {
      return null;
    }
  }

  public List<PluginDataEntry> getByPlugin(final Plugin plugin) {
    List<Pair<String, Object>> params = new ArrayList<>();
    params.add(new Pair<>("plugin_id", plugin.getId()));
    return getElementsByParameters(params);
  }

}
