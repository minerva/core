package lcsb.mapviewer.persist.dao.map.kinetics;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.stereotype.Service;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.persist.dao.BaseDao;

@Service
public class SbmlUnitDao extends BaseDao<SbmlUnit, SbmlUnitProperty> {

  public SbmlUnitDao() {
    super(SbmlUnit.class);
  }

  public SbmlUnit getById(final int id, final ModelData map) {
    List<?> list = getSession()
        .createQuery(
            "select unit_t from " + ModelData.class.getSimpleName()
                + " model inner join model.units unit_t "
                + " where model = :map")
        .setParameter("map", map).list();
    if (list.size() == 0) {
      return null;
    } else {
      return (SbmlUnit) list.get(0);
    }
  }

  @Override
  protected Predicate createPredicate(final Map<SbmlUnitProperty, Object> filterOptions, final Root<SbmlUnit> root, final CriteriaQuery<?> query) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    List<Predicate> predicates = new ArrayList<>();

    for (SbmlUnitProperty key : filterOptions.keySet()) {
      if (key.equals(SbmlUnitProperty.PROJECT_ID)) {
        Join<SbmlFunction, Project> projectJoin = root.join("model").join("project");
        Predicate predicate = builder.and(builder.equal(projectJoin.get("projectId"), filterOptions.get(key)));
        predicates.add(predicate);
      } else if (key.equals(SbmlUnitProperty.MAP_ID)) {
        Join<SbmlFunction, Project> modelJoin = root.join("model");
        Predicate predicate = builder.and(builder.equal(modelJoin.get("id"), filterOptions.get(key)));
        predicates.add(predicate);
      } else if (key.equals(SbmlUnitProperty.ID)) {
        Predicate predicate = builder.and(builder.equal(root.get("id"), filterOptions.get(key)));
        predicates.add(predicate);
      } else if (key.equals(SbmlUnitProperty.IN_USE)) {
        Subquery<Integer> elementSubquery = query.subquery(Integer.class);
        Join<Species, SbmlUnit> elementUnitJoin = elementSubquery.from(Species.class).join("substanceUnits");

        Predicate unitIdMatch = builder.equal(root.get("id"), elementUnitJoin.get("id"));

        elementSubquery.select(elementUnitJoin.get("id"))
            .where(unitIdMatch);

        if (((Boolean) filterOptions.get(key)) == true) {
          predicates.add(builder.exists(elementSubquery));
        } else {
          predicates.add(builder.exists(elementSubquery).not());
        }
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    Predicate predicate = builder.and(predicates.toArray(new Predicate[0]));
    return predicate;

  }

}
