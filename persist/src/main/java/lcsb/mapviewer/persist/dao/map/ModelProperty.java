package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum ModelProperty implements MinervaEntityProperty<ModelData> {
  MAP_ID,
  PROJECT_ID,
}
