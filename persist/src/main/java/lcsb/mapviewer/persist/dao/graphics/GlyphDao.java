package lcsb.mapviewer.persist.dao.graphics;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.persist.dao.BaseDao;

@Repository
public class GlyphDao extends BaseDao<Glyph, GlyphProperty> {

  public GlyphDao() {
    super(Glyph.class);
  }

  @Override
  protected Predicate createPredicate(final Map<GlyphProperty, Object> filterOptions, final Root<Glyph> root) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    List<Predicate> predicates = new ArrayList<>();

    for (GlyphProperty key : filterOptions.keySet()) {
      if (key.equals(GlyphProperty.PROJECT_ID)) {
        Join<Glyph, Project> projectJoin = root.join("project");
        Predicate predicate = builder.and(builder.equal(projectJoin.get("projectId"), filterOptions.get(key)));
        predicates.add(predicate);
      } else if (key.equals(GlyphProperty.ID)) {
        Predicate predicate = builder.and(builder.equal(root.get("id"), filterOptions.get(key)));
        predicates.add(predicate);
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    Predicate predicate = builder.and(predicates.toArray(new Predicate[0]));
    return predicate;

  }

}
