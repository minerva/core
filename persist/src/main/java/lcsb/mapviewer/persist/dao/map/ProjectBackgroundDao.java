package lcsb.mapviewer.persist.dao.map;

import java.util.List;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.ProjectBackground;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.BaseDao;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

@Repository
public class ProjectBackgroundDao extends BaseDao<ProjectBackground, MinervaEntityProperty<ProjectBackground>> {

  public ProjectBackgroundDao() {
    super(ProjectBackground.class);
  }

  public List<ProjectBackground> getProjectBackgroundsByProject(final Project project) {
    List<ProjectBackground> layouts = getElementsByParameter("project_id", project.getId());
    for (final ProjectBackground layout : layouts) {
      refresh(layout);
    }
    return layouts;
  }

  @SuppressWarnings("unchecked")
  public List<ProjectBackground> getProjectBackgroundsByProject(final String projectId) {
    List<?> list = getSession()
        .createQuery(
            "select background from " + this.getClazz().getSimpleName()
                + " background inner join background.project where background.project.projectId = :param_val "
                + removableAndStatemant())
        .setParameter("param_val", projectId).list();
    return (List<ProjectBackground>) list;
  }

  public List<ProjectBackground> getProjectBackgroundsByUser(final User user) {
    List<ProjectBackground> layouts = getElementsByParameter("creator_id", user.getId());
    for (final ProjectBackground layout : layouts) {
      refresh(layout);
    }
    return layouts;
  }
}
