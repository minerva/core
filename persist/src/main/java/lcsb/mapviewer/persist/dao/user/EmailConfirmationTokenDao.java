package lcsb.mapviewer.persist.dao.user;

import java.util.List;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.user.EmailConfirmationToken;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.BaseDao;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

@Repository
public class EmailConfirmationTokenDao extends BaseDao<EmailConfirmationToken, MinervaEntityProperty<EmailConfirmationToken>> {

  public EmailConfirmationTokenDao() {
    super(EmailConfirmationToken.class);
  }

  public EmailConfirmationToken getByToken(final String token) {
    return getByParameter("token", token);
  }

  public List<EmailConfirmationToken> getByUser(final User user) {
    return getElementsByParameter("user", user);
  }

}
