package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.BaseDao;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Data access object class for Comment objects.
 *
 * @author Piotr Gawron
 */
@Repository
public class CommentDao extends BaseDao<Comment, CommentProperty> {

  /**
   * Default constructor.
   */
  public CommentDao() {
    super(Comment.class);
  }

  /**
   * Returns list of comments for a model given in the parameter. The comments
   * are limited by the parameters.
   *
   * @param model   in which model we want to find comments
   * @param pinned  this parameter defines what kind of comments we want to get:
   *                <ul>
   *                <li>null - all comments</li>
   *                <li>false - comments that are not visible on the map</li>
   *                <li>true - comments that are visible on the map</li>
   *                </ul>
   * @param deleted this parameter defines what kind of comments we want to get:
   *                <ul>
   *                <li>null - all comments</li>
   *                <li>false - comments that are not deleted</li>
   *                <li>true - comments that are deleted</li>
   *                </ul>
   * @return list of comments that fulfill parameters constraints
   */
  public List<Comment> getCommentByModel(final Model model, final Boolean pinned, final Boolean deleted) {
    return getCommentByModel(model.getId(), pinned, deleted);
  }

  /**
   * Returns list of comments for a model given in the parameter. The comments
   * are limited by the parameters.
   *
   * @param modelId in which model we want to find comments
   * @param pinned  this parameter defines what kind of comments we want to get:
   *                <ul>
   *                <li>null - all comments</li>
   *                <li>false - comments that are not visible on the map</li>
   *                <li>true - comments that are visible on the map</li>
   *                </ul>
   * @param deleted this parameter defines what kind of comments we want to get:
   *                <ul>
   *                <li>null - all comments</li>
   *                <li>false - comments that are not deleted</li>
   *                <li>true - comments that are deleted</li>
   *                </ul>
   * @return list of comments that fulfill parameters constraints
   */
  private List<Comment> getCommentByModel(final int modelId, final Boolean pinned, final Boolean deleted) {
    List<Comment> comments = getElementsByParameter("model_id", modelId);
    List<Comment> result = new ArrayList<>();
    if (deleted != null) {
      for (final Comment comment : comments) {
        if (comment.isDeleted() == deleted) {
          result.add(comment);
        }
      }
    } else {
      result = comments;
    }

    if (pinned == null) {
      return result;
    }
    comments = result;
    result = new ArrayList<>();
    for (final Comment comment : comments) {
      if (comment.isPinned() == pinned) {
        result.add(comment);
      }
    }
    return result;
  }

  /**
   * Returns list of comments for a model given in the parameter. The comments
   * are limited by the parameters.
   *
   * @param model   in which model we want to find comments
   * @param pinned  this parameter defines what kind of comments we want to get:
   *                <ul>
   *                <li>null - all comments</li>
   *                <li>false - comments that are not visible on the map</li>
   *                <li>true - comments that are visible on the map</li>
   *                </ul>
   * @param deleted this parameter defines what kind of comments we want to get:
   *                <ul>
   *                <li>null - all comments</li>
   *                <li>false - comments that are not deleted</li>
   *                <li>true - comments that are deleted</li>
   *                </ul>
   * @return list of comments that fulfill parameters constraints
   */
  public List<Comment> getCommentByModel(final ModelData model, final Boolean pinned, final Boolean deleted) {
    return getCommentByModel(model.getId(), pinned, deleted);
  }

  public Comment getByIdAndProjectId(final int id, final String projectId) {

    @SuppressWarnings("unchecked")
    List<Comment> list = getSession()
        .createQuery(
            "select comment_t from " + getClazz().getSimpleName()
                + " comment_t join comment_t.model.project project_t where project_t.projectId = :project_id and comment_t.id = :id")
        .setParameter("project_id", projectId)
        .setParameter("id", id)
        .list();
    if (!list.isEmpty()) {
      return list.get(0);
    }
    return null;
  }

  protected Predicate createPredicate(final Map<CommentProperty, Object> filterOptions, final Root<Comment> root) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    List<Predicate> predicates = new ArrayList<>();

    for (CommentProperty key : filterOptions.keySet()) {
      if (key.equals(CommentProperty.PROJECT_ID)) {
        Join<Author, ModelData> modelJoin = root.join("model");
        Join<ModelData, Project> projectJoin = modelJoin.join("project");

        Predicate predicate = builder.and(
            builder.equal(projectJoin.get("projectId"), filterOptions.get(key)));
        predicates.add(predicate);
      } else if (key.equals(CommentProperty.MAP_ID)) {
        Join<Author, ModelData> modelJoin = root.join("model");

        Predicate predicate = builder.and(
            builder.equal(modelJoin.get("id"), filterOptions.get(key)));
        predicates.add(predicate);
      } else if (key.equals(CommentProperty.ID)) {

        Predicate predicate = builder.and(
            builder.equal(root.get("id"), filterOptions.get(key)));
        predicates.add(predicate);
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    return builder.and(predicates.toArray(new Predicate[0]));

  }
}
