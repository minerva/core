package lcsb.mapviewer.persist.dao.graphics;

import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum PolylineDataProperty implements MinervaEntityProperty<PolylineData> {
  ID,
  LAYER_ID,
  MAP_ID,
  PROJECT_ID,
}
