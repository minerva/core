package lcsb.mapviewer.persist.dao.graphics;

import lcsb.mapviewer.model.map.layout.graphics.LayerImage;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum LayerImageProperty implements MinervaEntityProperty<LayerImage> {
  ID,
  LAYER_ID,
  MAP_ID,
  PROJECT_ID,
}
