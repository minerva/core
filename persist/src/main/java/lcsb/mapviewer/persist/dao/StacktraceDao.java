package lcsb.mapviewer.persist.dao;

import java.util.Calendar;
import java.util.List;

import javax.persistence.TemporalType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.Stacktrace;
import lcsb.mapviewer.persist.DbUtils;

@Repository
public class StacktraceDao {

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  private Class<?> clazz = Stacktrace.class;

  @Autowired
  private DbUtils dbUtils;

  public Stacktrace getById(final Integer id) {
    return getById(id + "");
  }

  public Stacktrace getById(final String id) {
    if (id == null) {
      return null;
    }
    List<?> list = getSession()
        .createQuery(" from " + this.getClazz().getSimpleName() + " where id=:id ")
        .setParameter("id", id).list();
    if (list.size() == 0) {
      return null;
    } else {
      return (Stacktrace) list.get(0);
    }
  }

  public void removeBeforeDate(final Calendar cleanupTimestamp) {
    getSession().createQuery("delete from " + this.getClazz().getSimpleName() + " where createdAt < :cleanupTimestamp ")
        .setParameter("cleanupTimestamp", cleanupTimestamp, TemporalType.TIMESTAMP)
        .executeUpdate();
  }

  public void removeOld() {
    Calendar cleanupTimestamp = Calendar.getInstance();
    cleanupTimestamp.add(Calendar.MINUTE, -10); // remove everything except the last 10 minutes

    removeBeforeDate(cleanupTimestamp);
  }

  protected Session getSession() {
    return dbUtils.getSessionFactory().getCurrentSession();
  }

  public Class<?> getClazz() {
    return clazz;
  }

  public void setClazz(final Class<?> clazz) {
    this.clazz = clazz;
  }

  public Stacktrace add(final Stacktrace object) {
    getSession().save(object);
    if (dbUtils.isAutoFlush()) {
      getSession().flush();
    }
    return object;
  }

  public long getCount() {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
    Root<?> root = criteria.from(this.clazz);

    criteria.select(builder.count(root));
    return getSession().createQuery(criteria).getSingleResult();
  }
}
