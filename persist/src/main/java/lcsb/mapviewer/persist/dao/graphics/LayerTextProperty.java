package lcsb.mapviewer.persist.dao.graphics;

import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum LayerTextProperty implements MinervaEntityProperty<LayerText> {
  ID,
  LAYER_ID,
  MAP_ID,
  PROJECT_ID,
}
