package lcsb.mapviewer.persist.dao.map;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.BaseDao;

@Repository
public class AuthorDao extends BaseDao<Author, AuthorProperty> {

  public AuthorDao() {
    super(Author.class);
  }

  @Override
  protected Predicate createPredicate(final Map<AuthorProperty, Object> filterOptions, final Root<Author> root) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    List<Predicate> predicates = new ArrayList<>();

    for (AuthorProperty key : filterOptions.keySet()) {
      if (key.equals(AuthorProperty.PROJECT_ID)) {
        Join<Author, ModelData> modelJoin = root.join("modelData");
        Join<ModelData, Project> projectJoin = modelJoin.join("project");

        Predicate predicate = builder.and(
            builder.equal(projectJoin.get("projectId"), filterOptions.get(key)));
        predicates.add(predicate);
      } else if (key.equals(AuthorProperty.MAP_ID)) {
        Join<Author, ModelData> modelJoin = root.join("modelData");

        Predicate predicate = builder.and(
            builder.equal(modelJoin.get("id"), filterOptions.get(key)));
        predicates.add(predicate);
      } else if (key.equals(AuthorProperty.ID)) {

        Predicate predicate = builder.and(
            builder.equal(root.get("id"), filterOptions.get(key)));
        predicates.add(predicate);
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    Predicate predicate = builder.and(predicates.toArray(new Predicate[0]));
    return predicate;

  }

}
