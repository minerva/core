package lcsb.mapviewer.persist.dao.map.kinetics;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.persist.dao.BaseDao;

@Transactional
@Service
public class SbmlFunctionDao extends BaseDao<SbmlFunction, SbmlFunctionProperty> {

  public SbmlFunctionDao() {
    super(SbmlFunction.class);
  }

  @Override
  protected Predicate createPredicate(final Map<SbmlFunctionProperty, Object> filterOptions, final Root<SbmlFunction> root) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    List<Predicate> predicates = new ArrayList<>();

    for (SbmlFunctionProperty key : filterOptions.keySet()) {
      if (key.equals(SbmlFunctionProperty.PROJECT_ID)) {
        Join<SbmlFunction, Project> projectJoin = root.join("model").join("project");
        Predicate predicate = builder.and(builder.equal(projectJoin.get("projectId"), filterOptions.get(key)));
        predicates.add(predicate);
      } else if (key.equals(SbmlFunctionProperty.MAP_ID)) {
        Join<SbmlFunction, Project> modelJoin = root.join("model");
        Predicate predicate = builder.and(builder.equal(modelJoin.get("id"), filterOptions.get(key)));
        predicates.add(predicate);
      } else if (key.equals(SbmlFunctionProperty.ID)) {
        Predicate predicate = builder.and(builder.equal(root.get("id"), filterOptions.get(key)));
        predicates.add(predicate);
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    Predicate predicate = builder.and(predicates.toArray(new Predicate[0]));
    return predicate;

  }

}
