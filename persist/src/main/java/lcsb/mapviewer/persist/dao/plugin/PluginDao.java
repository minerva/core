package lcsb.mapviewer.persist.dao.plugin;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.plugin.Plugin;
import lcsb.mapviewer.persist.dao.BaseDao;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Data access object class for {@link Plugin} objects.
 *
 * @author Piotr Gawron
 */
@Repository
public class PluginDao extends BaseDao<Plugin, PluginProperty> {

  public PluginDao() {
    super(Plugin.class);
  }

  public Plugin getByHash(final String hash) {
    return getByParameter("hash", hash);
  }

  @Override
  protected Predicate createPredicate(final Map<PluginProperty, Object> filterOptions, final Root<Plugin> root) {
    final CriteriaBuilder builder = getSession().getCriteriaBuilder();
    final List<Predicate> predicates = new ArrayList<>();

    for (final PluginProperty key : filterOptions.keySet()) {
      if (key.equals(PluginProperty.DEFAULT_VALUE)) {
        final Predicate predicate = builder.and(root.get("isDefault").in(filterOptions.get(key)));
        predicates.add(predicate);
      } else if (key.equals(PluginProperty.PUBLIC_VALUE)) {
        final Predicate predicate = builder.and(root.get("isPublic").in(filterOptions.get(key)));
        predicates.add(predicate);
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    return builder.and(predicates.toArray(new Predicate[0]));
  }
}
