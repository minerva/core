package lcsb.mapviewer.persist.dao.security;

import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum PrivilegeProperty implements MinervaEntityProperty<Privilege> {
  OBJECT_ID,
  PRIVILEGE_TYPE,
}
