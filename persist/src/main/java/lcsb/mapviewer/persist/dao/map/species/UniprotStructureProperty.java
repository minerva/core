package lcsb.mapviewer.persist.dao.map.species;

import lcsb.mapviewer.model.map.species.field.Structure;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum UniprotStructureProperty implements MinervaEntityProperty<Structure> {
  ID,
  PROJECT_ID,
  MAP_ID,
  ELEMENT_ID,
  UNIPROT_ID,
}
