package lcsb.mapviewer.persist.dao.map.species;

import lcsb.mapviewer.model.map.species.field.UniprotRecord;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum UniprotRecordProperty implements MinervaEntityProperty<UniprotRecord> {
  ID,
  PROJECT_ID,
  MAP_ID,
  ELEMENT_ID,
}
