package lcsb.mapviewer.persist.dao;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.persist.DbUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.dialect.Dialect;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Abstract interface for data access object in the system. Contains the basic
 * generic functionality.
 *
 * @param <T> type for which data access object is created
 * @author Piotr Gawron
 */
@Repository
public abstract class BaseDao<T extends MinervaEntity, S extends MinervaEntityProperty<T>> {

  protected Logger logger = LogManager.getLogger();

  /**
   * Sometimes objects have a flag that indicate that the object was removed from
   * the system (but is still there because of foreign keys etc.). This column
   * determines the name of the column. If it's set to null then such column
   * doesn't exist.
   */
  private String removableColumn = null;

  /**
   * Class of the object that DAO works on.
   */
  private final Class<T> clazz;
  /**
   * Utility that helps to manage the sessions in custom multithreaded
   * implementation.
   */
  @Autowired
  private DbUtils dbUtils;

  /**
   * Default constructor.
   *
   * @param theClass class of the object that DAO will work on
   */
  public BaseDao(final Class<T> theClass) {
    this.clazz = theClass;
  }

  /**
   * Default constructor.
   *
   * @param theClass        class of the object that DAO will work on
   * @param removableColumn determines the column in the object table that indicates if object
   *                        should be considered as removed or not (see also:
   *                        {@link #removableColumn}).
   */
  public BaseDao(final Class<T> theClass, final String removableColumn) {
    this(theClass);
    this.removableColumn = removableColumn;
  }

  /**
   * Adds object to the database.
   *
   * @param object object to add to database
   */
  public T add(final T object) {
    getSession().save(object);
    if (dbUtils.isAutoFlush()) {
      getSession().flush();
    }
    return object;
  }

  /**
   * Flush connection with database.
   */
  public void flush() {
    getSession().flush();
  }

  /**
   * Returns current session.
   *
   * @return session for current thread
   */
  protected Session getSession() {
    return dbUtils.getSessionFactory().getCurrentSession();
  }

  /**
   * Update object in the database.
   *
   * @param object object to be updated
   */
  public void update(final T object) {
    getSession().update(object);
    if (dbUtils.isAutoFlush()) {
      getSession().flush();
    }
  }

  /**
   * Removes object from the database.
   *
   * @param object object to be removed from database
   */
  public void delete(final T object) {
    getSession().delete(object);
    if (dbUtils.isAutoFlush()) {
      getSession().flush();
    }
  }

  /**
   * "Disconnects" object with the element in database. From this point on we
   * cannot update/delete it in the database.
   *
   * @param object object that should be evicted
   */
  public void evict(final T object) {
    getSession().evict(object);
    if (dbUtils.isAutoFlush()) {
      getSession().flush();
    }
  }

  /**
   * Returns number of elements in the table for this object.
   *
   * @return number of all elements in database
   */
  public long getCount() {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
    Root<T> root = criteria.from(this.clazz);

    if (removableColumn != null) {
      criteria.where(builder.equal(root.get(removableColumn), false));
    }
    criteria.select(builder.count(root));
    return getSession().createQuery(criteria).getSingleResult();
  }

  public long getCount(final Map<S, Object> filterOptions) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    CriteriaQuery<Long> countQuery = builder.createQuery(Long.class);

    Root<T> root = countQuery.from(clazz);
    Predicate predicate = createPredicate(filterOptions, root, countQuery);

    countQuery.select(builder.count(root));
    countQuery.where(predicate).distinct(true);

    return getSession().createQuery(countQuery).getSingleResult();
  }

  /**
   * Returns an element that has a key parameter equal to value. If there is more
   * than one element than one of them will be returned. If there is no such
   * element then <code>null</code> is returned.
   *
   * @param key   which parameter will filter the data
   * @param value what must be the value of parameter key
   * @return element that fulfill T.key=value criteria
   */
  protected T getByParameter(final String key, final Object value) {
    List<T> list = getSession()
        .createQuery(
            " from " + this.clazz.getSimpleName() + " where " + key + " = :param_val " + removableAndStatemant(), this.clazz)
        .setParameter("param_val", value).list();
    if (list.isEmpty()) {
      return null;
    } else {
      return list.get(0);
    }
  }

  /**
   * Returns the list of elements that have a key parameter equal to value.
   *
   * @param key   which parameter will filter the data
   * @param value what must be the value of parameter key
   * @return list of elements that fulfill T.key=value criteria
   */
  protected List<T> getElementsByParameter(final String key, final Object value) {
    List<T> list = getSession()
        .createQuery(
            " from " + this.clazz.getSimpleName() + " where " + key + " = :param_val " + removableAndStatemant(), this.clazz)
        .setParameter("param_val", value).list();
    return list;
  }

  protected List<T> getElementsByParameters(final List<Pair<String, Object>> params) {
    String queryString = " from " + this.clazz.getSimpleName() + " where ";
    boolean firstParam = true;
    for (Pair<String, Object> param : params) {
      String key = param.getLeft();
      if (!firstParam) {
        queryString += " AND ";
      }
      if (param.getRight() == null) {
        queryString += key + " is null ";
      } else {
        queryString += key + " = :param_val_" + key + " ";
      }

      firstParam = false;
    }
    queryString += removableAndStatemant();
    Query<T> query = getSession().createQuery(queryString, clazz);
    for (Pair<String, Object> param : params) {
      String key = param.getLeft();
      Object value = param.getRight();
      if (value != null) {
        query = query.setParameter("param_val_" + key, value);
      }
    }
    List<T> list = query.list();
    return list;
  }

  /**
   * Returns element with the id given as parameter.
   *
   * @param id database identifier
   * @return object width identifier given as parameter
   */
  public T getById(final Integer id) {
    if (id == null) {
      return null;
    }
    List<T> list = getSession()
        .createQuery(" from " + this.clazz.getSimpleName() + " where id=:id " + removableAndStatemant(), this.clazz)
        .setParameter("id", id).list();
    if (list.isEmpty()) {
      return null;
    } else {
      return list.get(0);
    }
  }

  /**
   * Removes all elements from the database.
   */
  public void clearTable() {
    String stringQuery = "DELETE FROM " + this.clazz.getSimpleName();
    getSession().createQuery(stringQuery).executeUpdate();
    if (dbUtils.isAutoFlush()) {
      getSession().flush();
    }
  }

  /**
   * Refresh object with the new data taken from database.
   *
   * @param object object to be refreshed
   */
  public void refresh(final Object object) {
    getSession().refresh(object);
  }

  /**
   * @return the clazz
   */
  protected Class<? extends T> getClazz() {
    return clazz;
  }

  /**
   * Returns part of HQL statement responsible for {@link #removableColumn}.
   *
   * @return part of HQL statement responsible for {@link #removableColumn}.
   */
  protected String removableAndStatemant() {
    if (removableColumn == null) {
      return "";
    } else {
      return " AND " + removableColumn + " = false ";
    }
  }

  /**
   * Returns part of HQL statement responsible for {@link #removableColumn}
   * (without sql AND).
   *
   * @return part of HQL statement responsible for {@link #removableColumn} (without sql AND).
   */
  protected String removableStatemant() {
    if (removableColumn == null) {
      return "";
    } else {
      return " WHERE " + removableColumn + " = false ";
    }
  }

  protected Dialect getDialect() {
    return dbUtils.getDialect();
  }

  public void saveOrUpdate(final T object) {
    getSession().saveOrUpdate(object);
  }

  /**
   * Returns list of all object in db.
   *
   * @return list of all object in db
   */
  public List<T> getAll() {
    List<T> list = getSession().createQuery(" from " + this.clazz.getSimpleName() + " " + removableStatemant(), this.clazz).list();
    return list;
  }

  public Page<T> getAll(final Map<S, Object> filterOptions, final Pageable pageable) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    CriteriaQuery<T> criteria = builder.createQuery(clazz);
    Root<T> root = criteria.from(clazz);

    Predicate predicate = createPredicate(filterOptions, root, criteria);

    criteria.where(predicate);
    criteria.distinct(true);

    if (pageable.getSort().isSorted()) {
      criteria.orderBy(getOrder(pageable.getSort(), builder, root));
    }

    TypedQuery<T> typedQuery = getSession().createQuery(criteria);
    if (pageable.isPaged()) {
      typedQuery.setFirstResult(pageable.getPageNumber()
          * pageable.getPageSize());
      typedQuery.setMaxResults(pageable.getPageSize());
    }

    List<T> sublist = typedQuery.getResultList();

    long count = getCount(filterOptions);
    return new PageImpl<>(sublist, pageable, count);
  }

  public List<T> getAll(final Map<S, Object> filterOptions) {
    return getAll(filterOptions, Pageable.unpaged()).getContent();
  }


  protected List<Order> getOrder(final Sort sort, final CriteriaBuilder criteriaBuilder, final Root<T> root) {
    for (Iterator<Sort.Order> iterator = sort.iterator(); iterator.hasNext(); ) {
      Sort.Order order = iterator.next();
      logger.warn("Unsupported order column: " + order.getProperty());
    }
    return new ArrayList<>();
  }

  protected Predicate createPredicate(final Map<S, Object> filterOptions, final Root<T> root, final CriteriaQuery<?> query) {
    return createPredicate(filterOptions, root);
  }

  protected Predicate createPredicate(final Map<S, Object> filterOptions, final Root<T> root) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    for (MinervaEntityProperty<T> key : filterOptions.keySet()) {
      throw new InvalidArgumentException("Unknown property: " + key);
    }

    return builder.and();
  }
}
