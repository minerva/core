package lcsb.mapviewer.persist.dao;

import lcsb.mapviewer.model.Project;

public enum ProjectProperty implements MinervaEntityProperty<Project> {
  PROJECT_ID,
  USER
}
