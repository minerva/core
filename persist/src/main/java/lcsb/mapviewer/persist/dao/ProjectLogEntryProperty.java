package lcsb.mapviewer.persist.dao;

import lcsb.mapviewer.model.ProjectLogEntry;

public enum ProjectLogEntryProperty implements MinervaEntityProperty<ProjectLogEntry> {
  LEVEL,
  CONTENT,
  PROJECT_ID,
  ID,
  TYPE,
  OBJECT_IDENTIFIER,
  OBJECT_CLASS,
  MAP_NAME,
  SOURCE,

}
