package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.BaseDao;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class DataOverlayDao extends BaseDao<DataOverlay, DataOverlayProperty> {

  public DataOverlayDao() {
    super(DataOverlay.class);
  }

  public List<DataOverlay> getByProject(final Project project) {
    final List<DataOverlay> layouts = getElementsByParameter("project_id", project.getId());
    for (final DataOverlay layout : layouts) {
      refresh(layout);
    }
    return layouts;
  }

  public List<Pair<Element, DataOverlayEntry>> getElementsForModels(final int overlayId, final List<ModelData> maps) {
    return getElementsForModels(overlayId, maps, new ArrayList<>());
  }

  public List<Pair<Element, DataOverlayEntry>> getElementsForModels(final int overlayId, final List<ModelData> maps,
                                                                    final List<Integer> elementIdentifiers) {
    final List<?> list = getElementsFromDatabase(overlayId, maps, elementIdentifiers, "entry, element");

    final List<Pair<Element, DataOverlayEntry>> result = new ArrayList<>();
    for (final Object object : list) {
      final Object[] objects = (Object[]) object;
      final Element element = (Element) objects[1];
      final DataOverlayEntry entry = (DataOverlayEntry) objects[0];
      if (matchType(element, entry)) {
        result.add(new Pair<>(element, entry));
      }
    }
    return result;
  }

  public List<Pair<Element, DataOverlayEntry>> getElementReferencesForModels(final int overlayId, final List<ModelData> maps) {
    return getElementReferencesForModels(overlayId, maps, new ArrayList<>());
  }

  public List<Pair<Element, DataOverlayEntry>> getElementReferencesForModels(final int overlayId, final List<ModelData> maps,
                                                                             final List<Integer> elementIdentifiers) {
    final List<?> list = getElementsFromDatabase(overlayId, maps, elementIdentifiers, "entry, element.id");

    final List<Pair<Element, DataOverlayEntry>> result = new ArrayList<>();
    for (final Object object : list) {
      final Object[] objects = (Object[]) object;
      final DataOverlayEntry entry = (DataOverlayEntry) objects[0];
      final Element element = getSession().load(Element.class, (Integer) (objects[1]));

      if (matchType(element, entry)) {
        result.add(new Pair<>(element, entry));
      }
    }
    return result;
  }

  public List<Pair<Reaction, DataOverlayEntry>> getReactionsForModels(final int overlayId, final List<ModelData> maps) {
    return getReactionsForModels(overlayId, maps, new ArrayList<>());
  }

  public List<Pair<Reaction, DataOverlayEntry>> getReactionsForModels(final int overlayId, final List<ModelData> maps,
                                                                      final List<Integer> reactionIdentifiers) {
    final List<?> list = getReactionsFromDatabase(overlayId, maps, reactionIdentifiers, "entry, reaction");
    final List<Pair<Reaction, DataOverlayEntry>> result = new ArrayList<>();
    for (final Object object : list) {
      final Object[] objects = (Object[]) object;
      final Reaction element = (Reaction) objects[1];
      final DataOverlayEntry entry = (DataOverlayEntry) objects[0];
      if (matchType(element, entry)) {
        result.add(new Pair<>(element, entry));
      }
    }
    return result;
  }

  public List<Pair<Reaction, DataOverlayEntry>> getReactionReferencesForModels(final int overlayId, final List<ModelData> maps) {
    return getReactionReferencesForModels(overlayId, maps, new ArrayList<>());
  }

  public List<Pair<Reaction, DataOverlayEntry>> getReactionReferencesForModels(final int overlayId, final List<ModelData> maps,
                                                                               final List<Integer> reactionIdentifiers) {
    final List<?> list = getReactionsFromDatabase(overlayId, maps, reactionIdentifiers, "entry, reaction.id");
    final List<Pair<Reaction, DataOverlayEntry>> result = new ArrayList<>();
    for (final Object object : list) {
      final Object[] objects = (Object[]) object;
      final Reaction element = getSession().load(Reaction.class, (Integer) (objects[1]));
      final DataOverlayEntry entry = (DataOverlayEntry) objects[0];
      if (matchType(element, entry)) {
        result.add(new Pair<>(element, entry));
      }
    }
    return result;
  }

  private boolean matchType(final BioEntity element, final DataOverlayEntry entry) {
    boolean add = entry.getTypes().isEmpty();
    for (final Class<?> clazz : entry.getTypes()) {
      if (clazz.isAssignableFrom(element.getClass())) {
        add = true;
        break;
      }
    }
    return add;
  }

  private List<?> getElementsFromDatabase(final int overlayId, final List<ModelData> maps, final List<Integer> elementIdentifiers,
                                          final String columnNames) {
    String queryString = "select " + columnNames + " from " + DataOverlayEntry.class.getSimpleName()
        + " entry left join entry.compartments compartment left join entry.miriamData entry_miriam, "
        + Element.class.getSimpleName() + " element left join element.miriamData element_miriam "
        + " left join entry.types types"
        + " where element.model in :maps and entry.dataOverlay.id = :overlayId "
        + " and (%ELEMENT_NAME_PART%) "
        + " and (lower(element.model.name) = lower(entry.modelName) or entry.modelName='' or entry.modelName is null) "
        + " and (entry_miriam is null or "
        + "      (entry_miriam.dataType = element_miriam.dataType and lower(entry_miriam.resource) = lower(element_miriam.resource)) ) "
        + " and (compartment is null or"
        + "      element.compartment.id in (select id from " + Element.class.getSimpleName()
        + "      compartment_element where lower(compartment_element.name) = lower(compartment) and compartment_element.model in :maps)) "
        + " and (lower(element.elementId) = lower(entry.elementId) or entry.elementId='' or entry.elementId is null) ";
    if (!elementIdentifiers.isEmpty()) {
      queryString += " and (element.id in :elementId) ";
    } else { // if there is no element identifier provided don't allow to have empty filter
      queryString += " and ((entry.name <>'' and entry.name is not null) ";
      queryString += " or (entry_miriam is not null) ";
      queryString += " or (compartment is not null) ";
      queryString += " or (entry.elementId is not null and entry.elementId is not null) ";
      queryString += " or (types is not null) ";
      queryString += " or (entry.modelName is not null and entry.modelName is not null)) ";
    }
    queryString += " group by entry, element";

    final org.hibernate.query.Query<?> query1 = getSession()
        .createQuery(queryString.replace("%ELEMENT_NAME_PART%", "lower(element.name) = lower(entry.name) and entry.name <>''"))
        .setParameter("maps", maps)
        .setParameter("overlayId", overlayId);
    if (!elementIdentifiers.isEmpty()) {
      query1.setParameter("elementId", elementIdentifiers);
    }

    final org.hibernate.query.Query<?> query2 = getSession()
        .createQuery(queryString.replace("%ELEMENT_NAME_PART%", "entry.name =''"))
        .setParameter("maps", maps)
        .setParameter("overlayId", overlayId);
    if (!elementIdentifiers.isEmpty()) {
      query2.setParameter("elementId", elementIdentifiers);
    }

    final List<Object> list = new ArrayList<>(query1.list());
    list.addAll(query2.list());
    return list;
  }

  private List<?> getReactionsFromDatabase(final int overlayId, final List<ModelData> maps, final List<Integer> reactionIdentifiers,
                                           final String columnNames) {
    String queryString = "select " + columnNames + " from " + DataOverlayEntry.class.getSimpleName()
        + " entry left join entry.compartments compartment left join entry.miriamData entry_miriam, "
        + Reaction.class.getSimpleName() + " reaction left join reaction.miriamData reaction_miriam"
        + " left join entry.types types"
        + " where reaction.model in :maps and entry.dataOverlay.id = :overlayId "
        + " and (%REACTION_NAME_PART%) "
        + " and (lower(reaction.model.name) = lower(entry.modelName) or entry.modelName='' or entry.modelName is null) "
        + " and (entry_miriam is null or "
        + "      (entry_miriam.dataType = reaction_miriam.dataType and lower(entry_miriam.resource) = lower(reaction_miriam.resource)) ) "
        + " and (lower(reaction.idReaction) = lower(entry.elementId) or entry.elementId='' or entry.elementId is null) ";
    if (!reactionIdentifiers.isEmpty()) {
      queryString += " and (reaction.id in :reactionId) ";
    } else { // if there is no element identifier provided don't allow to have empty filter
      queryString += " and ((entry.name <>'' and entry.name is not null) ";
      queryString += " or (entry_miriam is not null) ";
      queryString += " or (entry.elementId is not null and entry.elementId is not null) ";
      queryString += " or (types is not null) ";
      queryString += " or (entry.modelName is not null and entry.modelName is not null)) ";
    }

    queryString += " group by entry, reaction";

    final org.hibernate.query.Query<?> query1 = getSession()
        .createQuery(queryString.replace("%REACTION_NAME_PART%", "lower(reaction.name) = lower(entry.name) and entry.name <>''"))
        .setParameter("maps", maps)
        .setParameter("overlayId", overlayId);

    if (!reactionIdentifiers.isEmpty()) {
      query1.setParameter("reactionId", reactionIdentifiers);
    }

    final org.hibernate.query.Query<?> query2 = getSession()
        .createQuery(queryString.replace("%REACTION_NAME_PART%", "entry.name =''"))
        .setParameter("maps", maps)
        .setParameter("overlayId", overlayId);

    if (!reactionIdentifiers.isEmpty()) {
      query2.setParameter("reactionId", reactionIdentifiers);
    }

    final List<Object> list = new ArrayList<>(query1.list());
    list.addAll(query2.list());
    return list;
  }

  public List<DataOverlay> getDataOverlayByOwner(final User user) {
    return getElementsByParameter("creator_id", user.getId());
  }

  public void deleteDataOverlays(final List<DataOverlay> dataOverlays) {
    for (final DataOverlay dataOverlay : dataOverlays) {
      delete(dataOverlay);
    }
  }

  @Override
  protected Predicate createPredicate(final Map<DataOverlayProperty, Object> filterOptions, final Root<DataOverlay> root) {
    final CriteriaBuilder builder = getSession().getCriteriaBuilder();
    final List<Predicate> predicates = new ArrayList<>();

    for (final DataOverlayProperty key : filterOptions.keySet()) {
      if (key.equals(DataOverlayProperty.PROJECT_ID)) {
        final Join<DataOverlay, Project> projectJoin = root.join("project");

        final Predicate predicate = builder.and(
            builder.equal(projectJoin.get("projectId"), filterOptions.get(key)));
        predicates.add(predicate);
      } else if (key.equals(DataOverlayProperty.USER_LOGIN)) {
        final Join<DataOverlay, User> userJoin = root.join("creator");

        final Predicate loginPredicate = builder.and(
            builder.equal(userJoin.get("login"), filterOptions.get(key)));

        predicates.add(loginPredicate);
      } else if (key.equals(DataOverlayProperty.IS_PUBLIC)) {

        final Predicate publicPredicate = builder.and(
            builder.equal(root.get("isPublic"), filterOptions.get(key)));

        predicates.add(publicPredicate);
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    return builder.and(predicates.toArray(new Predicate[0]));
  }
}
