package lcsb.mapviewer.persist.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectLogEntry;

@Repository
public class ProjectLogEntryDao extends BaseDao<ProjectLogEntry, ProjectLogEntryProperty> {

  public ProjectLogEntryDao() {
    super(ProjectLogEntry.class);
  }

  @Override
  protected List<Order> getOrder(final Sort sort, final CriteriaBuilder criteriaBuilder, final Root<ProjectLogEntry> root) {
    List<Order> result = new ArrayList<>();
    for (Iterator<Sort.Order> iterator = sort.iterator(); iterator.hasNext();) {
      Sort.Order order = iterator.next();
      Expression<?> column = null;
      if (Objects.equals(order.getProperty(), ProjectLogEntryProperty.CONTENT.name())) {
        column = root.get("content");
      } else if (Objects.equals(order.getProperty(), ProjectLogEntryProperty.LEVEL.name())) {
        column = root.get("severity");
      } else if (Objects.equals(order.getProperty(), ProjectLogEntryProperty.ID.name())) {
        column = root.get("id");
      } else if (Objects.equals(order.getProperty(), ProjectLogEntryProperty.TYPE.name())) {
        column = root.get("type");
      } else if (Objects.equals(order.getProperty(), ProjectLogEntryProperty.OBJECT_IDENTIFIER.name())) {
        column = root.get("objectIdentifier");
      } else if (Objects.equals(order.getProperty(), ProjectLogEntryProperty.OBJECT_CLASS.name())) {
        column = root.get("objectClass");
      } else if (Objects.equals(order.getProperty(), ProjectLogEntryProperty.MAP_NAME.name())) {
        column = root.get("mapName");
      } else if (Objects.equals(order.getProperty(), ProjectLogEntryProperty.SOURCE.name())) {
        column = root.get("source");
      }

      if (column == null) {
        logger.warn("Unsupported order column: " + order.getProperty());
      } else {
        if (order.isAscending()) {
          result.add(criteriaBuilder.asc(column));
        } else {
          result.add(criteriaBuilder.desc(column));
        }
      }

    }
    return result;
  }

  @Override
  protected Predicate createPredicate(final Map<ProjectLogEntryProperty, Object> filterOptions, final Root<ProjectLogEntry> root) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    List<Predicate> predicates = new ArrayList<>();

    for (ProjectLogEntryProperty key : filterOptions.keySet()) {
      if (key.equals(ProjectLogEntryProperty.PROJECT_ID)) {
        Join<ProjectLogEntry, Project> projectJoin = root.join("project");
        Predicate predicate = builder.and(
            builder.equal(projectJoin.get("projectId"), filterOptions.get(key)));
        predicates.add(predicate);

      } else if (key.equals(ProjectLogEntryProperty.LEVEL)) {
        Predicate predicate = builder.like(builder.lower(root.get("severity")), ("%" + filterOptions.get(key) + "%").toLowerCase());
        predicates.add(predicate);
      } else if (key.equals(ProjectLogEntryProperty.CONTENT)) {
        Predicate predicate = builder.like(builder.lower(root.get("content")), ("%" + filterOptions.get(key) + "%").toLowerCase());
        predicates.add(predicate);
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    Predicate predicate = builder.and(predicates.toArray(new Predicate[0]));
    return predicate;

  }

}
