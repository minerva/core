/**
 * Package with data access object for user, privileges, etc.
 */
package lcsb.mapviewer.persist.dao.user;
