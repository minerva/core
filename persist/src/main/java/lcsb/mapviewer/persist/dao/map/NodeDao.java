package lcsb.mapviewer.persist.dao.map;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.persist.dao.BaseDao;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

@Repository
public class NodeDao extends BaseDao<AbstractNode, MinervaEntityProperty<AbstractNode>> {

  public NodeDao() {
    super(AbstractNode.class);
  }
}
