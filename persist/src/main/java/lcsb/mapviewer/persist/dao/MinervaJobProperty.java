package lcsb.mapviewer.persist.dao;

import lcsb.mapviewer.model.job.MinervaJob;

public enum MinervaJobProperty implements MinervaEntityProperty<MinervaJob> {
  EXTERNAL_OBJECT_CLASS,
  EXTERNAL_OBJECT_ID,
}
