package lcsb.mapviewer.persist.dao;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.job.MinervaJob;
import lcsb.mapviewer.model.job.MinervaJobStatus;
import lcsb.mapviewer.model.job.MinervaJobType;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Repository
public class MinervaJobDao extends BaseDao<MinervaJob, MinervaJobProperty> {

  public MinervaJobDao() {
    super(MinervaJob.class);
  }

  public List<MinervaJob> getRunning() {
    return getElementsByParameter("jobStatus", MinervaJobStatus.RUNNING);
  }

  public long getCount(final MinervaJobStatus status) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
    Root<MinervaJob> root = criteria.from(MinervaJob.class);
    criteria.where(builder.equal(root.get("jobStatus"), status));
    criteria.select(builder.count(root));
    return getSession().createQuery(criteria).getSingleResult();
  }

  public List<MinervaJob> getPending() {
    return getPending(Integer.MAX_VALUE);
  }

  public List<MinervaJob> getPending(final int max) {

    return getSession()
        .createQuery(" from " + MinervaJob.class.getSimpleName() + " where jobStatus = :job_status "
                + " order by priority, id",
            MinervaJob.class)
        .setMaxResults(max)
        .setParameter("job_status", MinervaJobStatus.PENDING).list();
  }

  public void deleteBefore(final Calendar beforeDate) {
    String query = "DELETE from " + this.getClazz().getSimpleName() + " where jobFinished < :before_date";
    getSession().createQuery(query)
        .setParameter("before_date", beforeDate)
        .executeUpdate();

  }

  public void deleteBefore(final MinervaJobStatus status, final Calendar beforeDate) {
    String query = "DELETE from " + this.getClazz().getSimpleName() + " where jobStatus  = :job_status and jobFinished < :before_date";
    getSession().createQuery(query)
        .setParameter("job_status", status)
        .setParameter("before_date", beforeDate)
        .executeUpdate();

  }

  public void deleteBefore(final MinervaJobType type, final MinervaJobStatus status, final Calendar beforeDate) {
    String query = "DELETE from " + MinervaJob.class.getSimpleName()
        + " where jobStatus  = :job_status and jobFinished < :before_date and jobType = :job_type";
    getSession().createQuery(query)
        .setParameter("job_status", status)
        .setParameter("job_type", type)
        .setParameter("before_date", beforeDate)
        .executeUpdate();

  }

  public List<MinervaJob> getUnfinishedByJobParameters(final MinervaJobType jobType, final Map<String, Object> jobParameters) {
    return getSession()
        .createQuery(" from " + MinervaJob.class.getSimpleName()
                + " where jobStatus in(:job_statuses) "
                + " and jobParameters = :job_parameters "
                + " and jobType = :job_type "
                + " order by priority, id",
            MinervaJob.class)
        .setParameter("job_statuses", Arrays.asList(MinervaJobStatus.PENDING, MinervaJobStatus.RUNNING))
        .setParameter("job_parameters", jobParameters)
        .setParameter("job_type", jobType)
        .list();
  }

  @Override
  protected Predicate createPredicate(final Map<MinervaJobProperty, Object> filterOptions, final Root<MinervaJob> root) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    List<Predicate> predicates = new ArrayList<>();

    for (MinervaJobProperty key : filterOptions.keySet()) {
      if (key.equals(MinervaJobProperty.EXTERNAL_OBJECT_CLASS)) {
        Collection<Class<?>> values = (Collection<Class<?>>) filterOptions.get(key);

        final CriteriaBuilder.In<Class<?>> predicate = builder.in(root.get("externalObjectClass"));
        for (final Class<?> object : values) {
          predicate.value(object);
        }
        predicates.add(predicate);
      } else if (key.equals(MinervaJobProperty.EXTERNAL_OBJECT_ID)) {
        Collection<Integer> values = (Collection<Integer>) filterOptions.get(key);

        final CriteriaBuilder.In<Integer> predicate = builder.in(root.get("externalObjectId"));
        for (final Integer object : values) {
          predicate.value(object);
        }
        predicates.add(predicate);
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    Predicate predicate = builder.and(predicates.toArray(new Predicate[0]));
    return predicate;

  }
}
