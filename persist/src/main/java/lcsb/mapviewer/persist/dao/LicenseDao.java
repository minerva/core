package lcsb.mapviewer.persist.dao;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.License;
import org.hibernate.query.Query;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LicenseDao extends BaseDao<License, MinervaEntityProperty<License>> {

  public LicenseDao() {
    super(License.class);
  }

  @SuppressWarnings("unchecked")
  public Page<License> getByFilter(final Pageable pageable) {
    String queryString = "from " + this.getClazz().getSimpleName();
    Query<?> query = getSession().createQuery(queryString);

    if (pageable.getSort().isUnsorted()) {
      queryString += " order by id";
    } else {
      throw new NotImplementedException("Sorting is not implemented");
    }

    if (pageable.isPaged()) {
      query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
      query.setMaxResults(pageable.getPageSize());
    }
    return new PageImpl<License>((List<License>) query.list(), pageable, getCount());
  }

}
