package lcsb.mapviewer.persist.dao.map.kinetics;

import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum SbmlUnitProperty implements MinervaEntityProperty<SbmlUnit> {
  ID,
  MAP_ID,
  PROJECT_ID,
  IN_USE,
}
