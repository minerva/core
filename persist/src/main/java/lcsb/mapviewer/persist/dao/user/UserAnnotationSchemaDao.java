package lcsb.mapviewer.persist.dao.user;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.user.UserAnnotationSchema;
import lcsb.mapviewer.persist.dao.BaseDao;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

@Repository
public class UserAnnotationSchemaDao extends BaseDao<UserAnnotationSchema, MinervaEntityProperty<UserAnnotationSchema>> {

  public UserAnnotationSchemaDao() {
    super(UserAnnotationSchema.class);
  }
}
