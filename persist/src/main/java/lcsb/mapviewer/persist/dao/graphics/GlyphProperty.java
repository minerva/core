package lcsb.mapviewer.persist.dao.graphics;

import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum GlyphProperty implements MinervaEntityProperty<Glyph> {
  ID,
  PROJECT_ID,
}
