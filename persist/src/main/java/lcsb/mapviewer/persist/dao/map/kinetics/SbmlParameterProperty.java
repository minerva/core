package lcsb.mapviewer.persist.dao.map.kinetics;

import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum SbmlParameterProperty implements MinervaEntityProperty<SbmlParameter> {
  ID,
  MAP_ID,
  PROJECT_ID,
}
