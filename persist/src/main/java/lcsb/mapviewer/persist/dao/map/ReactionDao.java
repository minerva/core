package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.BaseDao;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.dialect.HSQLDialect;
import org.hibernate.dialect.PostgreSQL81Dialect;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Data access object for {@link Reaction} class.
 *
 * @author Piotr Gawron
 */
@Repository
public class ReactionDao extends BaseDao<Reaction, ReactionProperty> {

  public ReactionDao() {
    super(Reaction.class);
  }

  /**
   * Fetch proxy of reaction objects together with distance from point
   *
   * @param map              map on which the search is performed
   * @param point            point to which reaction should be as close as possible
   * @param numberOfElements number of reactions to return
   * @param classes          type of reactions, empty list is treated as wild card
   * @return list of reaction closest to the point
   */
  public List<Pair<Reaction, Double>> getByCoordinatesWithDistance(final ModelData map, final Point2D point, final int numberOfElements,
                                                                   final List<Class<? extends BioEntity>> classes) {
    final String typeClause = getTypeClause(classes);

    final org.hibernate.query.Query<?> query = getSession()
        .createQuery("select reaction.id, "
            + " cast(min(function('dist_to_segment', :x, :y, node_segment.x1, node_segment.y1, node_segment.x2, node_segment.y2)) as double), "
            + " cast(min(function('dist_to_segment', :x, :y, "
            + "                   reaction_segment.x1, reaction_segment.y1, reaction_segment.x2, reaction_segment.y2)) as double)"
            + " from " + getClazz().getSimpleName() + " as reaction "
            + " inner join reaction.line inner join reaction.line.segments reaction_segment "
            + " inner join reaction.nodes node "
            + " inner join node.line inner join node.line.segments node_segment"
            + " where reaction.model = :map"
            + typeClause
            + " group by reaction"
            + " order by least("
            + " min(function('dist_to_segment', :x, :y, node_segment.x1, node_segment.y1, node_segment.x2, node_segment.y2)),"
            + " min(function('dist_to_segment', :x, :y, reaction_segment.x1, reaction_segment.y1, reaction_segment.x2, reaction_segment.y2))"
            + " )")
        .setMaxResults(numberOfElements)
        .setParameter("map", map)
        .setParameter("x", point.getX())
        .setParameter("y", point.getY());

    if (!classes.isEmpty()) {
      query.setParameter("classes", classes);
    }

    @SuppressWarnings("unchecked") final List<Object[]> orderedList = (List<Object[]>) query.list();
    final List<Pair<Reaction, Double>> result = new ArrayList<>();
    for (final Object[] row : orderedList) {
      final Reaction reaction = getSession().load(Reaction.class, (Integer) row[0]);
      final Double dist = Math.min((Double) row[1], (Double) row[2]);
      result.add(new Pair<>(reaction, dist));
    }
    return result;

  }

  public List<Reaction> getByCoordinates(final ModelData map, final Point2D point, final int numberOfElements,
                                         final List<Class<? extends BioEntity>> classes) {
    final List<Pair<Reaction, Double>> data = getByCoordinatesWithDistance(map, point, numberOfElements, classes);
    final List<Reaction> result = new ArrayList<>();
    for (final Pair<Reaction, Double> pair : data) {
      result.add(pair.getLeft());
    }
    return result;
  }

  private String getTypeClause(final List<Class<? extends BioEntity>> classes) {
    String typeClause = "";
    if (!classes.isEmpty()) {
      typeClause = " and type(reaction) in :classes ";
    }
    return typeClause;
  }

  public Map<MiriamType, Integer> getAnnotationStatistics(final String projectId) {
    final Map<MiriamType, Integer> result = new HashMap<>();
    final List<?> list = getSession()
        .createQuery(
            "select md.dataType, count(*) as counter from " + getClazz().getSimpleName()
                + " element_t join element_t.miriamData md "
                + " where element_t.model.project.projectId = :projectId "
                + " group by md.dataType")
        .setParameter("projectId", projectId).list();
    for (final Object object : list) {
      final Object[] row = (Object[]) object;
      result.put((MiriamType) row[0], ((Long) row[1]).intValue());
    }
    return result;
  }

  @Override
  protected Predicate createPredicate(final Map<ReactionProperty, Object> filterOptions, final Root<Reaction> root, final CriteriaQuery<?> query) {
    final CriteriaBuilder builder = getSession().getCriteriaBuilder();
    final List<Predicate> predicates = new ArrayList<>();

    for (final ReactionProperty key : filterOptions.keySet()) {
      if (key.equals(ReactionProperty.PERFECT_TEXT) || key.equals(ReactionProperty.TEXT)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        Expression<String> regexResult;

        if (getDialect() instanceof PostgreSQL81Dialect) {
          regexResult = builder.function("regexp_replace", String.class, builder.lower(root.get("idReaction")),
              builder.literal("[^a-z0-9]"), builder.literal(""), builder.literal("g"));
        } else if (getDialect() instanceof HSQLDialect) {
          regexResult = builder.function("regexp_replace", String.class, builder.lower(root.get("idReaction")),
              builder.literal("[^a-z0-9]"));
        } else {
          throw new NotImplementedException("Unsupported dialect: " + getDialect().getClass());
        }

        final List<Predicate> orPredicates = new ArrayList<>();
        boolean intValues = true;

        final In<String> predicate = builder.in(regexResult);
        for (final Object object : values) {
          String stringValue = ((String) object);
          predicate.value(stringValue.toLowerCase());
          if (!StringUtils.isNumeric(stringValue)) {
            intValues = false;
          }
        }
        orPredicates.add(predicate);

        if (key.equals(ReactionProperty.PERFECT_TEXT) && intValues) {
          final CriteriaBuilder.In<Integer> idPredicate = builder.in(root.get("id"));
          for (final Object object : values) {
            String stringValue = ((String) object);
            idPredicate.value(Integer.parseInt(stringValue));
          }
          orPredicates.add(idPredicate);
        }

        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
      } else if (key.equals(ReactionProperty.MAP)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        if (!values.isEmpty()) {
          final Join<Reaction, ModelData> modelJoin = root.join("model");
          final In<Integer> predicate = builder.in(modelJoin.get("id"));
          for (final Object object : values) {
            predicate.value(((ModelData) object).getId());
          }
          predicates.add(predicate);
        } else {
          predicates.add(builder.or());
        }
      } else if (key.equals(ReactionProperty.ID)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        final In<Integer> predicate = builder.in(root.get("id"));
        for (final Object object : values) {
          predicate.value((Integer) object);
        }
        predicates.add(predicate);
      } else if (key.equals(ReactionProperty.ANNOTATION)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        final Join<Reaction, MiriamData> miriamJoin = root.join("miriamData");
        final List<Predicate> orPredicates = new ArrayList<>();

        for (final Object object : values) {
          final MiriamData md = (MiriamData) object;
          final Predicate predicate = builder.and(
              builder.like(builder.lower(miriamJoin.get("resource")), md.getResource()),
              builder.equal(miriamJoin.get("dataType"), md.getDataType()));
          orPredicates.add(predicate);
        }
        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
      } else if (key.equals(ReactionProperty.CLASS)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        final List<Predicate> orPredicates = new ArrayList<>();

        for (final Object object : values) {
          final Class<?> clazz = (Class<?>) object;
          orPredicates.add(builder.equal(root.type(), clazz));
        }
        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
      } else if (key.equals(ReactionProperty.PROJECT_ID)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        final List<Predicate> orPredicates = new ArrayList<>();

        final Join<Reaction, Project> projectJoin = root.join("model").join("project");

        for (final Object object : values) {
          orPredicates.add(builder.equal(projectJoin.get("projectId"), object));
        }
        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
      } else if (key.equals(ReactionProperty.PROJECT)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        final List<Predicate> orPredicates = new ArrayList<>();

        final Join<Reaction, Project> projectJoin = root.join("model").join("project");

        for (final Object object : values) {
          orPredicates.add(builder.equal(projectJoin.get("projectId"), ((Project) object).getProjectId()));
        }
        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
      } else if (key.equals(ReactionProperty.MAP_ID)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        final List<Predicate> orPredicates = new ArrayList<>();

        final Join<Reaction, ModelData> modelJoin = root.join("model");

        for (final Object object : values) {
          orPredicates.add(builder.equal(modelJoin.get("id"), object));
        }
        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
      } else if (key.equals(ReactionProperty.PARTICIPANT_ID)) {
        final Object value = filterOptions.get(key);

        final Subquery<Integer> reactionNodeSubquery = query.subquery(Integer.class);
        final Root<ReactionNode> reactionNodeRoot = reactionNodeSubquery.from(ReactionNode.class);
        final Join<ReactionNode, Element> elementNodeJoin = reactionNodeRoot.join("element");
        final Join<ReactionNode, Reaction> reactionNodeJoin = reactionNodeRoot.join("reaction");

        final In<Integer> elementIdMatch = builder.in(elementNodeJoin.get("id"));
        for (final Object object : (List<?>) value) {
          elementIdMatch.value((Integer) object);
        }
        final Predicate reactionIdMatch = builder.equal(reactionNodeJoin.get("id"), root.get("id"));

        reactionNodeSubquery.select(reactionNodeRoot.get("id"))
            .where(elementIdMatch, reactionIdMatch);

        predicates.add(builder.exists(reactionNodeSubquery));
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    return builder.and(predicates.toArray(new Predicate[0]));

  }

  public boolean isPerfectMatch(final Reaction reaction, final String originalQuery) {
    return stripString(reaction.getIdReaction()).equals(stripString(originalQuery));
  }

  private String stripString(final String name) {
    if (name == null) {
      return "";
    }
    return name.toLowerCase().replaceAll("[^a-z0-9]", "");
  }

}
