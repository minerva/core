package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum CommentProperty implements MinervaEntityProperty<Comment> {
  PROJECT_ID,
  MAP_ID,
  ID,
  DELETED,
  VISIBLE,
}
