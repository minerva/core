package lcsb.mapviewer.persist.dao;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Article;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.ModelData;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Repository
public class ArticleDao extends BaseDao<Article, ArticleProperty> {

  public ArticleDao() {
    super(Article.class);
  }

  @Override
  protected Predicate createPredicate(final Map<ArticleProperty, Object> filterOptions, final Root<Article> root, final CriteriaQuery<?> query) {
    CriteriaBuilder builder = getSession().getCriteriaBuilder();
    List<Predicate> predicates = new ArrayList<>();

    for (ArticleProperty key : filterOptions.keySet()) {
      if (key.equals(ArticleProperty.TEXT)) {
        Object value = filterOptions.get(key);

        String text = "%" + ((String) value).toLowerCase() + "%";

        List<Predicate> orPredicates = new ArrayList<>();
        orPredicates.add(builder.like(builder.lower(root.get("pubmedId")), text));
        orPredicates.add(builder.like(builder.lower(root.get("title")), text));
        orPredicates.add(builder.like(builder.lower(root.get("journal")), text));
        if (StringUtils.isNumeric(((String) value))) {
          orPredicates.add(builder.equal(root.get("year"), Integer.valueOf(((String) value))));
          orPredicates.add(builder.equal(root.get("citationCount"), Integer.valueOf(((String) value))));
        }

        Subquery<String> elementSubquery = query.subquery(String.class);
        Root<ModelData> modelFromElementSubquery = elementSubquery.from(ModelData.class);
        Join<MiriamData, ModelData> elementMiriamJoin = modelFromElementSubquery.join("elements").join("miriamData");

        Predicate pubmedIdMatchFromElementSubquery = builder.equal(root.get("pubmedId"), elementMiriamJoin.get("resource"));
        Predicate modelNameMatchElementSubquery = builder.like(builder.lower(modelFromElementSubquery.get("name")), text);
        Predicate typeMatchElementSubquery = builder.equal(elementMiriamJoin.get("dataType"), MiriamType.PUBMED);

        elementSubquery.select(elementMiriamJoin.get("resource"))
            .where(pubmedIdMatchFromElementSubquery, modelNameMatchElementSubquery, typeMatchElementSubquery);

        orPredicates.add(builder.exists(elementSubquery));

        Subquery<String> reactionSubquery = query.subquery(String.class);
        Root<ModelData> modelFromReactionSubquery = reactionSubquery.from(ModelData.class);
        Join<MiriamData, ModelData> reactionMiriamJoin = modelFromReactionSubquery.join("reactions").join("miriamData");

        Predicate pubmedIdMatchFromReactionSubquery = builder.equal(root.get("pubmedId"), reactionMiriamJoin.get("resource"));
        Predicate modelNameMatchReactionSubquery = builder.like(builder.lower(modelFromReactionSubquery.get("name")), text);
        Predicate typeMatchReactionSubquery = builder.equal(reactionMiriamJoin.get("dataType"), MiriamType.PUBMED);
        reactionSubquery.select(reactionMiriamJoin.get("resource"))
            .where(pubmedIdMatchFromReactionSubquery, modelNameMatchReactionSubquery, typeMatchReactionSubquery);

        orPredicates.add(builder.exists(reactionSubquery));

        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
      } else if (key.equals(ArticleProperty.MODEL)) {
        @SuppressWarnings("unchecked")
        List<ModelData> values = (List<ModelData>) filterOptions.get(key);

        List<Predicate> orPredicates = new ArrayList<>();

        Subquery<String> elementSubquery = query.subquery(String.class);
        Root<ModelData> modelFromElementSubquery = elementSubquery.from(ModelData.class);
        Join<MiriamData, ModelData> elementMiriamJoin = modelFromElementSubquery.join("elements").join("miriamData");

        Predicate pubmedIdMatchFromElementSubquery = builder.equal(root.get("pubmedId"), elementMiriamJoin.get("resource"));
        In<Integer> modelIdMatchElementSubquery = builder.in(modelFromElementSubquery.get("id"));
        for (ModelData model : values) {
          modelIdMatchElementSubquery.value(model.getId());
        }

        Predicate typeMatchElementSubquery = builder.equal(elementMiriamJoin.get("dataType"), MiriamType.PUBMED);
        elementSubquery.select(elementMiriamJoin.get("resource"))
            .where(pubmedIdMatchFromElementSubquery, modelIdMatchElementSubquery, typeMatchElementSubquery);

        orPredicates.add(builder.exists(elementSubquery));

        Subquery<String> reactionSubquery = query.subquery(String.class);
        Root<ModelData> modelFromReactionSubquery = reactionSubquery.from(ModelData.class);
        Join<MiriamData, ModelData> reactionMiriamJoin = modelFromReactionSubquery.join("reactions").join("miriamData");

        Predicate pubmedIdMatchFromReactionSubquery = builder.equal(root.get("pubmedId"), reactionMiriamJoin.get("resource"));
        In<Integer> modelIdMatchReactionSubquery = builder.in(modelFromReactionSubquery.get("id"));
        for (ModelData model : values) {
          modelIdMatchReactionSubquery.value(model.getId());
        }
        Predicate typeMatchReactionSubquery = builder.equal(reactionMiriamJoin.get("dataType"), MiriamType.PUBMED);
        reactionSubquery.select(reactionMiriamJoin.get("resource"))
            .where(pubmedIdMatchFromReactionSubquery, modelIdMatchReactionSubquery, typeMatchReactionSubquery);

        orPredicates.add(builder.exists(reactionSubquery));

        predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
      } else if (key.equals(ArticleProperty.PUBMED_ID)) {
        Object value = filterOptions.get(key);

        String pubmedId;
        if (value instanceof String) {
          pubmedId = (String) value;
        } else if (value instanceof Integer) {
          pubmedId = ((Integer) value).toString();
        } else {
          throw new InvalidArgumentException("PubmedId should be an Integer or a String");
        }

        Predicate predicate = builder.and(root.get("pubmedId").in(pubmedId));
        predicates.add(predicate);
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    return builder.and(predicates.toArray(new Predicate[0]));

  }

  @Override
  protected List<Order> getOrder(final Sort sort, final CriteriaBuilder criteriaBuilder, final Root<Article> root) {
    List<Order> result = new ArrayList<>();
    for (Sort.Order order : sort) {
      Expression<?> column = null;
      if (Objects.equals(order.getProperty(), ArticleProperty.TITLE.name())) {
        column = root.get("title");
      } else if (Objects.equals(order.getProperty(), ArticleProperty.JOURNAL.name())) {
        column = root.get("journal");
      } else if (Objects.equals(order.getProperty(), ArticleProperty.PUBMED_ID.name()) || Objects.equals(order.getProperty(), "pubmedId")) {
        column = root.get("pubmedId");
      } else if (Objects.equals(order.getProperty(), ArticleProperty.YEAR.name())) {
        column = root.get("year");
      }

      if (column == null) {
        logger.warn("Unsupported order column: {}", order.getProperty());
      } else {
        if (order.isAscending()) {
          result.add(criteriaBuilder.asc(column));
        } else {
          result.add(criteriaBuilder.desc(column));
        }
      }

    }
    return result;
  }

}
