package lcsb.mapviewer.persist.dao.map.species;

import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum ElementProperty implements MinervaEntityProperty<Element> {
  ANNOTATION,
  COMPARTMENT_ID,
  COMPLEX_ID,
  CLASS,
  EXCLUDED_FROM_COMPARTMENT,
  ID,
  INCLUDED_IN_COMPARTMENT,
  MAP,
  MAP_ID,
  NAME,
  PERFECT_TEXT,
  PROJECT,
  PROJECT_ID,
  SUBMAP_CONNECTION,
  TEXT,
}
