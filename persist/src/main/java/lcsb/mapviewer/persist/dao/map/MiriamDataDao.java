package lcsb.mapviewer.persist.dao.map;

import org.springframework.stereotype.Repository;

import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.persist.dao.BaseDao;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

@Repository
public class MiriamDataDao extends BaseDao<MiriamData, MinervaEntityProperty<MiriamData>> {

  public MiriamDataDao() {
    super(MiriamData.class);
  }
}
