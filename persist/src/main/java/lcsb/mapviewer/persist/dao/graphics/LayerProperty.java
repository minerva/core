package lcsb.mapviewer.persist.dao.graphics;

import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum LayerProperty implements MinervaEntityProperty<Layer> {
  PROJECT_ID,
  MAP_ID,
  ID
}
