package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayGroup;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.BaseDao;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class DataOverlayGroupDao extends BaseDao<DataOverlayGroup, DataOverlayGroupProperty> {

  public DataOverlayGroupDao() {
    super(DataOverlayGroup.class);
  }

  @Override
  protected Predicate createPredicate(final Map<DataOverlayGroupProperty, Object> filterOptions, final Root<DataOverlayGroup> root) {
    final CriteriaBuilder builder = getSession().getCriteriaBuilder();
    final List<Predicate> predicates = new ArrayList<>();

    for (final DataOverlayGroupProperty key : filterOptions.keySet()) {
      if (key.equals(DataOverlayGroupProperty.PROJECT_ID)) {
        final Join<DataOverlay, Project> projectJoin = root.join("project");

        final Predicate predicate = builder.and(
            builder.equal(projectJoin.get("projectId"), filterOptions.get(key)));
        predicates.add(predicate);
      } else if (key.equals(DataOverlayGroupProperty.USER_LOGIN)) {
        final Join<DataOverlay, User> userJoin = root.join("owner");

        final Predicate loginPredicate = builder.and(
            builder.equal(userJoin.get("login"), filterOptions.get(key)));

        predicates.add(loginPredicate);
      } else if (key.equals(DataOverlayGroupProperty.ID)) {
        final Predicate idPredicate = builder.and(
            builder.equal(root.get("id"), filterOptions.get(key)));

        predicates.add(idPredicate);
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    return builder.and(predicates.toArray(new Predicate[0]));
  }
}
