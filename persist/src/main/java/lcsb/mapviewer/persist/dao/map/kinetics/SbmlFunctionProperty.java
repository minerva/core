package lcsb.mapviewer.persist.dao.map.kinetics;

import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum SbmlFunctionProperty implements MinervaEntityProperty<SbmlFunction> {
  ID,
  MAP_ID,
  PROJECT_ID,
}
