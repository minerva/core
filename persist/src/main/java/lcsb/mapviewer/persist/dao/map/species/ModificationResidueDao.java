package lcsb.mapviewer.persist.dao.map.species;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.persist.dao.BaseDao;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class ModificationResidueDao extends BaseDao<ModificationResidue, ModificationResidueProperty> {

  public ModificationResidueDao() {
    super(ModificationResidue.class);
  }

  @Override
  protected Predicate createPredicate(
      final Map<ModificationResidueProperty, Object> filterOptions,
      final Root<ModificationResidue> root,
      final CriteriaQuery<?> query) {

    final CriteriaBuilder builder = getSession().getCriteriaBuilder();
    final List<Predicate> predicates = new ArrayList<>();

    for (final ModificationResidueProperty key : filterOptions.keySet()) {
      if (key.equals(ModificationResidueProperty.MAP_ID)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        if (!values.isEmpty()) {
          final List<Predicate> orPredicates = new ArrayList<>();

          final Join<Element, ModelData> modelJoin = root.join("species").join("model");

          for (final Object object : values) {
            orPredicates.add(builder.equal(modelJoin.get("id"), object));
          }
          predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
        } else {
          predicates.add(builder.or());
        }
      } else if (key.equals(ModificationResidueProperty.PROJECT_ID)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        if (!values.isEmpty()) {
          final List<Predicate> orPredicates = new ArrayList<>();

          final Join<Element, Project> projectJoin = root.join("species").join("model").join("project");

          for (final Object object : values) {
            orPredicates.add(builder.equal(projectJoin.get("projectId"), object));
          }
          predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
        } else {
          predicates.add(builder.or());
        }
      } else if (key.equals(ModificationResidueProperty.ID)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;
        final CriteriaBuilder.In<Integer> predicate = builder.in(root.get("id"));
        for (final Object object : values) {
          predicate.value((Integer) object);
        }
        predicates.add(predicate);
      } else if (key.equals(ModificationResidueProperty.ELEMENT_ID)) {
        final Object value = filterOptions.get(key);

        final List<?> values = (List<?>) value;

        if (!values.isEmpty()) {
          final List<Predicate> orPredicates = new ArrayList<>();

          final Join<Element, Project> speciesJoin = root.join("species");

          for (final Object object : values) {
            orPredicates.add(builder.equal(speciesJoin.get("id"), object));
          }
          predicates.add(builder.or(orPredicates.toArray(new Predicate[]{})));
        } else {
          predicates.add(builder.or());
        }
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    return builder.and(predicates.toArray(new Predicate[0]));
  }

}
