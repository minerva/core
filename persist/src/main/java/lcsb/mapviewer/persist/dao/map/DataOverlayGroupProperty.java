package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.model.overlay.DataOverlayGroup;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum DataOverlayGroupProperty implements MinervaEntityProperty<DataOverlayGroup> {
  ID,
  PROJECT_ID,
  USER_LOGIN,
}
