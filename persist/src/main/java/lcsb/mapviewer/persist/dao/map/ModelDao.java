package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.BaseDao;
import org.hibernate.Hibernate;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Data access object for {@link Model} class.
 *
 * @author Piotr Gawron
 */
@Repository
public class ModelDao extends BaseDao<ModelData, ModelProperty> {

  public ModelDao() {
    super(ModelData.class);
  }

  @Override
  public void delete(final ModelData model) {
    if (model.getProject() != null && Hibernate.isInitialized(model.getProject())) {
      model.getProject().removeModel(model);
    }
    model.setProject(null);
    super.delete(model);
  }

  /**
   * Removes model data from the database.
   *
   * @param model object containing model data
   */
  public void delete(final Model model) {
    delete(model.getModelData());
  }

  /**
   * Adds model data to the database.
   *
   * @param model object containing model data to add to database
   */
  public void add(final Model model) {
    add(model.getModelData());
  }

  /**
   * "Disconnects" model data from database. From this point on we cannot
   * update/delete it in the database.
   *
   * @param model model containing model data to disconnect
   */
  public void evict(final Model model) {
    evict(model.getModelData());
  }

  public int getAnnotationCount(final List<ModelData> maps, final MiriamType type) {
    final Query<?> query = getSession()
        .createQuery(
            "select count(distinct md.resource) from " + MiriamData.class.getSimpleName() + " md "
                + " where md.id in ( "
                + " select md1.id from " + getClazz().getSimpleName()
                + " model inner join model.elements element "
                + " inner join element.miriamData md1 "
                + " where model in :maps "
                + " and md1.dataType = :type)"
                + " or md.id in ("
                + " select md2.id from " + getClazz().getSimpleName()
                + " model2 inner join model2.reactions reaction "
                + " inner join reaction.miriamData md2 "
                + " where model2 in :maps "
                + " and md2.dataType = :type)")
        .setParameter("maps", maps)
        .setParameter("type", type);
    return ((Long) query.list().get(0)).intValue();
  }

  @Override
  protected Predicate createPredicate(final Map<ModelProperty, Object> filterOptions, final Root<ModelData> root) {
    final CriteriaBuilder builder = getSession().getCriteriaBuilder();
    final List<Predicate> predicates = new ArrayList<>();

    for (final ModelProperty key : filterOptions.keySet()) {
      if (key.equals(ModelProperty.PROJECT_ID)) {
        final Join<ModelData, Project> projectJoin = root.join("project");
        final Predicate predicate = builder.and(
            builder.equal(projectJoin.get("projectId"), filterOptions.get(key)));
        predicates.add(predicate);
      } else if (key.equals(ModelProperty.MAP_ID)) {
        final Predicate predicate = builder.and(builder.equal(root.get("id"), filterOptions.get(key)));
        predicates.add(predicate);
      } else {
        throw new InvalidArgumentException("Unknown property: " + key);
      }
    }
    final Predicate predicate = builder.and(predicates.toArray(new Predicate[0]));
    return predicate;

  }

}
