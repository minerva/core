package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public enum DataOverlayProperty implements MinervaEntityProperty<DataOverlay> {
  PROJECT_ID,
  USER_LOGIN,
  IS_PUBLIC,
  ID
}
