package lcsb.mapviewer.persist.dao.cache;

import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.persist.dao.BaseDao;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Data access object for cached values.
 *
 * @author Piotr Gawron
 */
@Repository
public class CacheTypeDao extends BaseDao<CacheType, MinervaEntityProperty<CacheType>> {

  /**
   * Default constructor.
   */
  public CacheTypeDao() {
    super(CacheType.class);
  }

  /**
   * Returns the cache type for the class name identified by a canonical name.
   *
   * @param className canonical name of the class
   * @return type of cache that should be used by this cacheable interface
   */
  public CacheType getByClassName(final String className) {
    List<?> list = getElementsByParameter("className", className);
    if (list.isEmpty()) {
      return null;
    }
    return (CacheType) list.get(0);
  }

}
