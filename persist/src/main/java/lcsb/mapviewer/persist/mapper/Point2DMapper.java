package lcsb.mapviewer.persist.mapper;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;
import org.hibernate.usertype.CompositeUserType;

/**
 * This class allows to put {@link Point2D} objects into hibernate based models.
 * 
 * @author Piotr Gawron
 * 
 */
public class Point2DMapper implements CompositeUserType {

  @Override
  public String[] getPropertyNames() {
    return new String[] { "val" };
  }

  @Override
  public Type[] getPropertyTypes() {
    return new Type[] { StringType.INSTANCE };
  }

  @Override
  public Object getPropertyValue(final Object component, final int property) {
    Object returnValue = null;
    final Point2D point = (Point2D) component;
    if (0 == property) {
      returnValue = point.getX() + "," + point.getY();
    }
    return returnValue;
  }

  @Override
  public void setPropertyValue(final Object component, final int property, final Object value) {
    final Point2D point = (Point2D) component;
    if (0 == property) {
      final String[] values = ((String) value).split(",");
      point.setLocation(Double.parseDouble(values[0]), Double.parseDouble(values[1]));
    }
  }

  @Override
  public Class<?> returnedClass() {
    return Point2D.class;
  }

  @Override
  public boolean equals(final Object o1, final Object o2) {
    boolean isEqual = false;
    if (o1 == o2) {
      isEqual = true;
    } else if (null == o1 || null == o2) {
      isEqual = false;
    } else {
      isEqual = o1.equals(o2);
    }
    return isEqual;
  }

  @Override
  public int hashCode(final Object x) {
    return x.hashCode();
  }

  @Override
  public Object nullSafeGet(final ResultSet rs, final String[] names, final SharedSessionContractImplementor session, final Object owner)
      throws HibernateException, SQLException {
    Point2D point = null;
    final String val = rs.getString(names[0]);
    if (!rs.wasNull()) {
      final String[] values = val.split(",");
      point = new Point2D.Double(Double.parseDouble(values[0]), Double.parseDouble(values[1]));
    }
    return point;
  }

  @Override
  public void nullSafeSet(final PreparedStatement st, final Object value, final int index, final SharedSessionContractImplementor session)
      throws HibernateException, SQLException {
    if (value == null) {
      st.setNull(index, StringType.INSTANCE.sqlType());
    } else {
      final Point2D point = (Point2D) value;
      st.setString(index, point.getX() + "," + point.getY());
    }
  }

  @Override
  public Object deepCopy(final Object value) {
    if (value == null) {
      return null;
    }
    final Point2D recievedParam = (Point2D) value;
    final Point2D point = new Point2D.Double(recievedParam.getX(), recievedParam.getY());
    return point;
  }

  @Override
  public boolean isMutable() {
    return true;
  }

  @Override
  public Serializable disassemble(final Object value, final SharedSessionContractImplementor session) throws HibernateException {
    return (Serializable) value;
  }

  @Override
  public Object assemble(final Serializable cached, final SharedSessionContractImplementor session, final Object owner)
      throws HibernateException {
    return cached;
  }

  @Override
  public Object replace(final Object original, final Object target, final SharedSessionContractImplementor session, final Object owner)
      throws HibernateException {
    return this.deepCopy(original);
  }

}
