package lcsb.mapviewer.persist.mapper;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;
import org.hibernate.usertype.CompositeUserType;

import lcsb.mapviewer.common.geometry.ColorParser;

/**
 * This class allows to put {@link Point2D} objects into hibernate based models.
 * 
 * @author Piotr Gawron
 * 
 */
public class ColorMapper implements CompositeUserType {

  private static ColorParser colorParser = new ColorParser();

  @Override
  public String[] getPropertyNames() {
    return new String[] { "color" };
  }

  @Override
  public Type[] getPropertyTypes() {
    return new Type[] { StringType.INSTANCE };
  }

  @Override
  public Object getPropertyValue(final Object component, final int property) {
    Object returnValue = null;
    final Color color = (Color) component;
    if (0 == property) {
      returnValue = colorParser.serializeColor(color);
    }
    return returnValue;
  }

  @Override
  public void setPropertyValue(final Object component, final int property, final Object value) {
    throw new UnsupportedOperationException("Object is immutable");
  }

  @Override
  public Class<?> returnedClass() {
    return Color.class;
  }

  @Override
  public boolean equals(final Object o1, final Object o2) {
    boolean isEqual = false;
    if (o1 == o2) {
      isEqual = true;
    } else if (null == o1 || null == o2) {
      isEqual = false;
    } else {
      isEqual = o1.equals(o2);
    }
    return isEqual;
  }

  @Override
  public int hashCode(final Object x) {
    return x.hashCode();
  }

  @Override
  public Object nullSafeGet(final ResultSet rs, final String[] names, final SharedSessionContractImplementor session, final Object owner)
      throws HibernateException, SQLException {
    Color color = null;
    final String val = rs.getString(names[0]);
    if (!rs.wasNull()) {
      color = colorParser.deserializeColor(val);
    }
    return color;
  }

  @Override
  public void nullSafeSet(final PreparedStatement st, final Object value, final int index, final SharedSessionContractImplementor session)
      throws HibernateException, SQLException {
    if (value == null) {
      st.setNull(index, StringType.INSTANCE.sqlType());
    } else {
      final Color color = (Color) value;
      st.setString(index, colorParser.serializeColor(color));
    }
  }

  @Override
  public Object deepCopy(final Object value) {
    if (value == null) {
      return null;
    }
    final Color color = (Color) value;
    final Color result = new Color(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
    return result;
  }

  @Override
  public boolean isMutable() {
    return false;
  }

  @Override
  public Serializable disassemble(final Object value, final SharedSessionContractImplementor session) throws HibernateException {
    return (Serializable) value;
  }

  @Override
  public Object assemble(final Serializable cached, final SharedSessionContractImplementor session, final Object owner)
      throws HibernateException {
    return cached;
  }

  @Override
  public Object replace(final Object original, final Object target, final SharedSessionContractImplementor session, final Object owner)
      throws HibernateException {
    return this.deepCopy(original);
  }

}
