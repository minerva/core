package lcsb.mapviewer.persist.mapper;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.DoubleType;
import org.hibernate.type.Type;
import org.hibernate.usertype.CompositeUserType;

/**
 * This class allows to put {@link Line2D} objects into hibernate based models.
 * 
 * @author Piotr Gawron
 * 
 */
public class Line2DMapper implements CompositeUserType {

  private static Logger logger = LogManager.getLogger();

  @Override
  public String[] getPropertyNames() {
    return new String[] { "x1", "y1", "x2", "y2" };
  }

  @Override
  public Type[] getPropertyTypes() {
    return new Type[] { DoubleType.INSTANCE, DoubleType.INSTANCE, DoubleType.INSTANCE, DoubleType.INSTANCE };
  }

  @Override
  public Object getPropertyValue(final Object component, final int property) {
    final Line2D line = (Line2D) component;
    switch (property) {
      case 0:
        return line.getX1();
      case 1:
        return line.getY1();
      case 2:
        return line.getX2();
      case 3:
        return line.getY2();
      default:
        return null;
    }
  }

  @Override
  public void setPropertyValue(final Object component, final int property, final Object value) {
    final Line2D line = (Line2D) component;
    Point2D p1 = line.getP1();
    Point2D p2 = line.getP2();
    Double val = (Double) value;
    switch (property) {
      case 0:
        p1.setLocation(val, p1.getY());
        break;
      case 1:
        p1.setLocation(p1.getX(), val);
        break;
      case 2:
        p2.setLocation(val, p2.getY());
        break;
      case 3:
        p2.setLocation(p2.getX(), val);
        break;
      default:
        logger.warn("Unknown property for line mapper: " + property);
    }
    line.setLine(p1, p2);
  }

  @Override
  public Class<?> returnedClass() {
    return Line2D.class;
  }

  @Override
  public boolean equals(final Object o1, final Object o2) {
    boolean isEqual = false;
    if (o1 == o2) {
      isEqual = true;
    } else if (null == o1 || null == o2) {
      isEqual = false;
    } else {
      Line2D l1 = (Line2D) o1;
      Line2D l2 = (Line2D) o2;
      isEqual = l1.getX1() == l2.getX1()
          && l1.getX2() == l2.getX2()
          && l1.getY1() == l2.getY1()
          && l1.getY2() == l2.getY2();
    }
    return isEqual;
  }

  @Override
  public int hashCode(final Object x) {
    return x.hashCode();
  }

  @Override
  public Object nullSafeGet(final ResultSet rs, final String[] names, final SharedSessionContractImplementor session, final Object owner)
      throws HibernateException, SQLException {
    final Double x1 = rs.getDouble(names[0]);
    final Double y1 = rs.getDouble(names[1]);
    final Double x2 = rs.getDouble(names[2]);
    final Double y2 = rs.getDouble(names[3]);
    return new Line2D.Double(x1, y1, x2, y2);
  }

  @Override
  public void nullSafeSet(final PreparedStatement st, final Object value, final int index, final SharedSessionContractImplementor session)
      throws HibernateException, SQLException {
    final Line2D line = (Line2D) value;
    if (value == null) {
      st.setNull(index, DoubleType.INSTANCE.sqlType());
    } else {
      st.setDouble(index + 0, line.getX1());
      st.setDouble(index + 1, line.getY1());
      st.setDouble(index + 2, line.getX2());
      st.setDouble(index + 3, line.getY2());
    }
  }

  @Override
  public Object deepCopy(final Object value) {
    if (value == null) {
      return null;
    }
    final Line2D line = (Line2D) value;
    return new Line2D.Double(line.getP1(), line.getP2());
  }

  @Override
  public boolean isMutable() {
    return true;
  }

  @Override
  public Serializable disassemble(final Object value, final SharedSessionContractImplementor session) throws HibernateException {
    return (Serializable) value;
  }

  @Override
  public Object assemble(final Serializable cached, final SharedSessionContractImplementor session, final Object owner)
      throws HibernateException {
    return cached;
  }

  @Override
  public Object replace(final Object original, final Object target, final SharedSessionContractImplementor session, final Object owner)
      throws HibernateException {
    return this.deepCopy(original);
  }

}
