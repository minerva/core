package lcsb.mapviewer.persist.migration;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.IonChannelProtein;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.ReceptorProtein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.TruncatedProtein;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.overlay.GeneVariant;
import lcsb.mapviewer.model.overlay.GeneVariantDataOverlayEntry;
import lcsb.mapviewer.model.overlay.GenericDataOverlayEntry;

//CHECKSTYLE.OFF
public abstract class V16_0_0_20200930_2__generic_data_overlay_entry_data extends BaseJavaMigration {
  // CHECKSTYLE.ON

  private static final String DEFAULT_GENOME_VERSION = "hg38";

  private ColorParser colorParser = new ColorParser();

  private Logger logger = LogManager.getLogger();

  @Override
  public void migrate(final Context context) throws Exception {
    try (final Statement select = context.getConnection().createStatement()) {
      try (final ResultSet rows = select
          .executeQuery("select data_overlay_table.id, data_overlay_table.color_schema_type, file_content "
              + "from data_overlay_table , file_entry_table "
              + "where data_overlay_table.file_entry_id = file_entry_table.id")) {
        while (rows.next()) {
          Integer overlayId = rows.getInt(1);
          String schemaType = rows.getString(2);
          byte[] fileContent = rows.getBytes(3);

          Set<? extends DataOverlayEntry> entries = extractEntries(fileContent, schemaType);
          addEntries(overlayId, entries, context);
          if (Objects.equals("GENETIC_VARIANT", schemaType)) {
            String version = extractGenomeVersion(fileContent);
            PreparedStatement updateGenome = context.getConnection()
                .prepareStatement("update data_overlay_table set genome_version = ? where id = ?");
            updateGenome.setString(1, version);
            updateGenome.setInt(2, overlayId);
            updateGenome.executeUpdate();
          }
        }
      }
    }
  }

  private String extractGenomeVersion(final byte[] fileContent) throws IOException {
    InputStream bais = new ByteArrayInputStream(fileContent);

    BufferedReader br = new BufferedReader(new InputStreamReader(bais));

    try {
      String line = br.readLine();
      while (line != null && line.startsWith("#")) {
        if (line.startsWith("#GENOME_VERSION")) {
          return line.replace("#GENOME_VERSION=", "").trim();
        }
        line = br.readLine();
      }
      if (line == null) {
        return DEFAULT_GENOME_VERSION;
      }
      String[] columns = line.toLowerCase().split("\t");
      Integer versionColumn = null;
      for (int i = 0; i < columns.length; i++) {
        if (columns[i].equals("reference_genome_version")) {
          versionColumn = i;
        }
      }
      if (versionColumn == null) {
        return DEFAULT_GENOME_VERSION;
      }

      line = br.readLine();
      while (line != null) {
        if (!line.trim().isEmpty()) {
          String[] values = line.split("\t", -1);
          if (!values[versionColumn].trim().isEmpty()) {
            return values[versionColumn].trim();
          }
        }
        line = br.readLine();
      }
    } finally {
      br.close();
    }
    return DEFAULT_GENOME_VERSION;
  }

  private void addEntries(final Integer overlayId, final Set<? extends DataOverlayEntry> entries, final Context context)
      throws SQLException {
    for (final DataOverlayEntry dataOverlayEntry : entries) {
      if (dataOverlayEntry.getName() != null && dataOverlayEntry.getName().length() > 255) {
        logger.warn("Data overlay entry with too long name. Skipping: \"" + dataOverlayEntry.getName() + "\"");
        continue;
      }
      String distinguisher = "GENERIC_DATA_OVERLAY";
      if (dataOverlayEntry instanceof GeneVariantDataOverlayEntry) {
        distinguisher = "GENE_VARIANT_DATA_OVERLAY";
      }
      PreparedStatement insert = context.getConnection().prepareStatement(
          "insert into public.data_overlay_entry_table (name, model_name, element_id, reverse_reaction, line_width, value, color,"
              + "description, data_overlay_type, data_overlay_id) values (?,?,?,?,?,?,?,?,?,?)",
          Statement.RETURN_GENERATED_KEYS);
      insert.setString(1, dataOverlayEntry.getName());
      insert.setString(2, dataOverlayEntry.getModelName());
      insert.setString(3, dataOverlayEntry.getElementId());
      if (dataOverlayEntry.getReverseReaction() != null) {
        insert.setBoolean(4, dataOverlayEntry.getReverseReaction());
      } else {
        insert.setNull(4, java.sql.Types.BOOLEAN);
      }
      if (dataOverlayEntry.getLineWidth() != null) {
        insert.setDouble(5, dataOverlayEntry.getLineWidth());
      } else {
        insert.setNull(5, java.sql.Types.DOUBLE);
      }
      if (dataOverlayEntry.getValue() != null) {
        insert.setDouble(6, dataOverlayEntry.getValue());
      } else {
        insert.setNull(6, java.sql.Types.DOUBLE);
      }
      if (dataOverlayEntry.getColor() != null) {
        insert.setString(7, colorParser.serializeColor(dataOverlayEntry.getColor()));
      } else {
        insert.setNull(7, java.sql.Types.VARCHAR);
      }
      insert.setString(8, dataOverlayEntry.getDescription());
      insert.setString(9, distinguisher);
      insert.setInt(10, overlayId);
      insert.executeUpdate();
      try (final ResultSet generatedKeys = insert.getGeneratedKeys()) {
        if (generatedKeys.next()) {
          int entryId = ((int) generatedKeys.getLong(1));
          if (dataOverlayEntry.getCompartments().size() > 0) {
            for (final String compartment : dataOverlayEntry.getCompartments()) {
              try (final PreparedStatement insertCompartment = context.getConnection().prepareStatement(
                  "insert into public.data_overlay_entry_compartment_table (data_overlay_entry_id, compartment) values (?,?)")) {
                insertCompartment.setInt(1, entryId);
                insertCompartment.setString(2, compartment);
                insertCompartment.executeUpdate();
              }
            }
          }
          for (final Class<?> type : dataOverlayEntry.getTypes()) {
            try (final PreparedStatement insertType = context.getConnection().prepareStatement(
                "insert into public.data_overlay_entry_type_table (data_overlay_entry_id, type) values (?,?)")) {
              insertType.setInt(1, entryId);
              insertType.setString(2, type.getCanonicalName());
              insertType.executeUpdate();
            }
          }
          for (final MiriamData md : dataOverlayEntry.getMiriamData()) {
            try (final PreparedStatement insertMiriam = context.getConnection().prepareStatement(
                "insert into public.miriam_data_table (resource, data_type, relation_type) values (?,?,?)",
                Statement.RETURN_GENERATED_KEYS)) {
              insertMiriam.setString(1, md.getResource());
              insertMiriam.setString(2, md.getDataType().name());
              insertMiriam.setInt(3, md.getRelationType().ordinal());
              insertMiriam.executeUpdate();
              ResultSet miriamGeneratedKeys = insertMiriam.getGeneratedKeys();
              if (miriamGeneratedKeys.next()) {
                int miriamId = ((int) miriamGeneratedKeys.getLong(1));
                try (final PreparedStatement insertRelationMiriam = context.getConnection().prepareStatement(
                    "insert into public.data_overlay_entry_miriam_table (data_overlay_entry_id, miriam_id) values (?,?)")) {
                  insertRelationMiriam.setInt(1, entryId);
                  insertRelationMiriam.setInt(2, miriamId);
                  insertRelationMiriam.executeUpdate();
                }
              } else {
                logger.error("Problem with adding miriam data: " + md);
              }

            }
          }
          if (dataOverlayEntry instanceof GeneVariantDataOverlayEntry) {
            for (final GeneVariant gv : ((GeneVariantDataOverlayEntry) dataOverlayEntry).getGeneVariants()) {
              try (final PreparedStatement insertGeneVariant = context.getConnection().prepareStatement(
                  "insert into public.gene_variant_table (position, allel_frequency,"
                      + "original_dna,modified_dna,contig,amino_acid_change,variant_identifier,"
                      + "data_overlay_entry_id) values (?,?,?,?,?,?,?,?)")) {
                insertGeneVariant.setLong(1, gv.getPosition());
                if (gv.getAllelFrequency() != null) {
                  insertGeneVariant.setDouble(2, gv.getAllelFrequency());
                } else {
                  insertGeneVariant.setNull(2, java.sql.Types.DOUBLE);
                }
                insertGeneVariant.setString(3, gv.getOriginalDna());
                insertGeneVariant.setString(4, gv.getModifiedDna());
                insertGeneVariant.setString(5, gv.getContig());
                if (gv.getAminoAcidChange() != null) {
                  insertGeneVariant.setString(6, gv.getAminoAcidChange());
                } else {
                  insertGeneVariant.setNull(6, java.sql.Types.VARCHAR);

                }
                if (gv.getVariantIdentifier() != null) {
                  insertGeneVariant.setString(7, gv.getVariantIdentifier());
                } else {
                  insertGeneVariant.setNull(7, java.sql.Types.VARCHAR);
                }
                insertGeneVariant.setInt(8, entryId);
                insertGeneVariant.executeUpdate();
              }
            }
          }
        } else {
          logger.error("Problem with migrating entry data for overlay: " + overlayId);
        }
      }
    }
  }

  private Set<? extends DataOverlayEntry> extractEntries(final byte[] content, final String type) throws IOException {
    if (Objects.equals("GENETIC_VARIANT", type)) {
      return extractGeneVariantsEntries(content);
    } else {
      return extractGenericEntries(content);
    }
  }

  private Set<? extends DataOverlayEntry> extractGeneVariantsEntries(final byte[] content) throws IOException {
    InputStream bais = new ByteArrayInputStream(content);

    Set<GeneVariantDataOverlayEntry> result = new HashSet<>();

    BufferedReader br = new BufferedReader(new InputStreamReader(bais));

    try {
      String line = br.readLine();
      while (line != null && line.startsWith("#")) {
        line = br.readLine();
      }
      if (line == null) {
        return result;
      }
      String[] columns = line.split("\t");
      int columnCount = columns.length;

      if (columnCount == 1) {
        return extractSimpleEntries(content);
      }

      line = br.readLine();
      while (line != null) {
        GeneVariantDataOverlayEntry entry = new GeneVariantDataOverlayEntry();
        extractGenericEntry(entry, columns, line.split("\t", -1));
        if (entry.getName() != null && entry.getName().contains(";")) {
          String[] names = entry.getName().split(";");
          for (final String string : names) {
            if (!string.trim().isEmpty()) {
              GeneVariantDataOverlayEntry schemaCopy = entry.copy();
              schemaCopy.setName(string);
              result.add(schemaCopy);
            }
          }
        } else {
          result.add(entry);
        }

        line = br.readLine();
      }
    } finally {
      br.close();
    }

    return mergeSchemas(result);
  }

  private Set<GeneVariantDataOverlayEntry> mergeSchemas(final Collection<GeneVariantDataOverlayEntry> schemas) {
    Map<String, GeneVariantDataOverlayEntry> schemasByName = new HashMap<>();
    for (final GeneVariantDataOverlayEntry colorSchema : schemas) {
      GeneVariantDataOverlayEntry mergedSchema = schemasByName.get(getColorSchemaIdentifiablePart(colorSchema));
      if (mergedSchema == null) {
        mergedSchema = colorSchema.copy();
        schemasByName.put(getColorSchemaIdentifiablePart(colorSchema), mergedSchema);
      } else {
        mergedSchema.addGeneVariants(colorSchema.getGeneVariants());
      }
    }
    return new HashSet<>(schemasByName.values());
  }

  private String getColorSchemaIdentifiablePart(final DataOverlayEntry colorSchema) {
    List<String> identifiers = new ArrayList<>();
    for (final MiriamData md : colorSchema.getMiriamData()) {
      identifiers.add(md.toString());
    }
    Collections.sort(identifiers);

    return StringUtils.join(identifiers, "\n") + "\n" + colorSchema.getName();
  }

  private Set<DataOverlayEntry> extractGenericEntries(final byte[] content) throws IOException {
    InputStream bais = new ByteArrayInputStream(content);

    Set<DataOverlayEntry> result = new HashSet<>();

    BufferedReader br = new BufferedReader(new InputStreamReader(bais));

    try {
      String line = br.readLine();
      while (line != null && line.startsWith("#")) {
        line = br.readLine();
      }
      if (line == null) {
        return result;
      }
      String[] columns = line.split("\t");
      int columnCount = columns.length;

      if (columnCount == 1) {
        return extractSimpleEntries(content);
      }

      line = br.readLine();
      while (line != null) {
        if (!line.trim().isEmpty()) {
          DataOverlayEntry entry = new GenericDataOverlayEntry();
          try {
            result.add(extractGenericEntry(entry, columns, line.split("\t", -1)));
          } catch (final Exception e) {
            logger.error("Problem with creating entry. Line:\n" + line + "\nColumns: "
                + StringUtils.join(Arrays.asList(columns), "\t"), e);
          }
        }
        line = br.readLine();
      }
    } finally {
      br.close();
    }
    return result;
  }

  private DataOverlayEntry extractGenericEntry(final DataOverlayEntry entry, final String[] columns, final String[] data) {
    GeneVariant gv = new GeneVariant();

    for (int i = 0; i < columns.length; i++) {
      String column = columns[i].trim().toLowerCase();
      String value = null;
      // there might be some broken entries
      if (data.length > i) {
        value = data[i];
      }

      if (value != null && !value.trim().isEmpty()) {

        boolean found = false;
        for (final MiriamType miriamType : MiriamType.values()) {
          if (column.equalsIgnoreCase("identifier_" + miriamType.name())
              || column.equalsIgnoreCase(miriamType.getCommonName())) {
            entry.addMiriamData(new MiriamData(miriamType, value.trim()));
            found = true;
          }
        }

        switch (column) {
          case "name":
          case "gene_name":
            entry.setName(value);
            break;
          case "map_name":
          case "model_name":
            entry.setModelName(value);
            break;
          case "value":
            Double number = parseDouble(value);
            entry.setValue(number);
            break;
          case "compartment":
            for (final String string : value.split(",")) {
              if (!string.trim().isEmpty()) {
                entry.addCompartment(string);
              }
            }
            break;
          case "type":
            for (final String string : value.split(",")) {
              if (!string.isEmpty()) {
                switch (string.toLowerCase()) {
                  case "protein":
                    entry.addType(GenericProtein.class);
                    entry.addType(IonChannelProtein.class);
                    entry.addType(ReceptorProtein.class);
                    entry.addType(TruncatedProtein.class);
                    break;
                  case "gene":
                    entry.addType(Gene.class);
                    break;
                  case "complex":
                    entry.addType(Complex.class);
                    break;
                  case "simple_molecule":
                    entry.addType(SimpleMolecule.class);
                    break;
                  case "ion":
                    entry.addType(Ion.class);
                    break;
                  case "phenotype":
                    entry.addType(Phenotype.class);
                    break;
                  case "drug":
                    entry.addType(Drug.class);
                    break;
                  case "rna":
                    entry.addType(Rna.class);
                    break;
                  case "antisense_rna":
                    entry.addType(AntisenseRna.class);
                    break;
                  case "unknown":
                    entry.addType(Unknown.class);
                    break;
                  case "degraded":
                    entry.addType(Degraded.class);
                    break;
                  default:
                    throw new NotImplementedException("class type is unknown: " + string);
                }
              }
            }
            break;
          case "color":
            entry.setColor(colorParser.parse(value.trim()));
            break;
          case "references":
          case "identifier":
            entry.addMiriamData(MiriamType.getMiriamDataFromIdentifier(value));
            break;
          case "element_identifier":
          case "elementidentifier":
          case "reaction_identifier":
          case "reactionidentifier":
            entry.setElementId(value);
            break;
          case "line_width":
          case "linewidth":
            entry.setLineWidth(parseDouble(value));
            break;
          case "description":
            entry.setDescription(value);
            break;
          case "reverse_reaction":
          case "reversereaction":
            entry.setReverseReaction(value.equalsIgnoreCase("true"));
            break;
          default:
            if (!found) {
              assignGeneVariantData(gv, column, value);
            }
            break;
        }
      }
    }
    if (gv.getPosition() != null) {
      ((GeneVariantDataOverlayEntry) entry).addGeneVariant(gv);
    }
    return entry;
  }

  private Double parseDouble(final String value) {
    Double number = Double.parseDouble(value.replace(",", "."));
    return number;
  }

  private void assignGeneVariantData(final GeneVariant gv, final String column, final String value) {
    switch (column) {

      case "position":
        gv.setPosition(Long.valueOf(value));
        break;
      case "original_dna":
        gv.setOriginalDna(value);
        break;
      case "alternative_dna":
        gv.setModifiedDna(value);
        break;
      case "chromosome":
      case "contig":
        gv.setContig(value);
        break;
      case "allel_frequency":
      case "allele_frequency":
        gv.setAllelFrequency(parseDouble(value));
        break;
      case "variant_identifier":
        gv.setVariantIdentifier(value);
        break;
      case "amino_acid_change":
        gv.setAminoAcidChange(value);
        break;
      case "reference_genome_type":
      case "reference_genome_version":
        break;
      default:
        throw new NotImplementedException("Column type is unknown: " + column);
    }
  }

  private Set<DataOverlayEntry> extractSimpleEntries(final byte[] content) throws IOException {
    InputStream bais = new ByteArrayInputStream(content);

    Set<DataOverlayEntry> result = new HashSet<>();

    BufferedReader br = new BufferedReader(new InputStreamReader(bais));

    try {
      String line = br.readLine();
      while (line != null && line.startsWith("#")) {
        line = br.readLine();
      }
      while (line != null) {
        DataOverlayEntry entry = new GenericDataOverlayEntry();
        entry.setName(line.trim());
        result.add(entry);
        line = br.readLine();
      }
    } finally {
      br.close();
    }
    return result;
  }
}
