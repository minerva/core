package lcsb.mapviewer.persist.migration;

import java.io.ByteArrayInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.Objects;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.model.Article;

//CHECKSTYLE.OFF
public abstract class V16_0_0_20200922_2__generic_article_data_migration extends BaseJavaMigration {
  // CHECKSTYLE.ON

  private static Logger logger = LogManager.getLogger();

  @Override
  public void migrate(final Context context) throws Exception {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();

    try (final Statement select = context.getConnection().createStatement()) {
      try (final ResultSet rows = select.executeQuery("select value from cache_query_table where query like 'pubmed:%'")) {
        while (rows.next()) {
          String xmlArticle = rows.getString(1);
          try {

            ByteArrayInputStream input = new ByteArrayInputStream(
                xmlArticle.toString().getBytes("UTF-8"));
            Document doc = builder.parse(input);

            Element root = doc.getDocumentElement();

            Article article = new Article("");
            NodeList children = root.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
              Node child = children.item(i);
              if (child instanceof Element) {
                String content = child.getTextContent();
                if (content != null) {
                  content = content.trim();
                }
                if (Objects.equals("authors", child.getNodeName())) {
                  article.addAuthor(content);
                } else if (Objects.equals("citationCount", child.getNodeName())) {
                  if (StringUtils.isNumeric(content)) {
                    article.setCitationCount(Integer.valueOf(content));
                  }
                } else if (Objects.equals("id", child.getNodeName())) {
                  article.setPubmedId(content);
                } else if (Objects.equals("journal", child.getNodeName())) {
                  article.setJournal(content);
                } else if (Objects.equals("link", child.getNodeName())) {
                  article.setLink(content);
                } else if (Objects.equals("title", child.getNodeName())) {
                  article.setTitle(content);
                } else if (Objects.equals("year", child.getNodeName())) {
                  if (StringUtils.isNumeric(content)) {
                    article.setYear(Integer.valueOf(content));
                  }
                } else {
                  logger.warn("Unknown article field: " + child.getNodeName());
                }

              }
            }
            try (final PreparedStatement insert = context.getConnection().prepareStatement(
                "insert into public.article_table (citation_count, pubmed_id, journal, link, title, year) values (?,?,?,?,?,?)",
                Statement.RETURN_GENERATED_KEYS)) {
              insert.setInt(1, article.getCitationCount());
              insert.setString(2, article.getPubmedId());
              insert.setString(3, article.getJournal());
              insert.setString(4, article.getLink());
              insert.setString(5, article.getTitle());
              if (article.getYear() == null) {
                insert.setNull(6, Types.INTEGER);
              } else {
                insert.setInt(6, article.getYear());
              }
              if (insert.executeUpdate() == 0) {
                logger.error("Problem with migrating cached article data: " + xmlArticle);
              } else {
                try (final ResultSet generatedKeys = insert.getGeneratedKeys()) {
                  if (generatedKeys.next()) {
                    int articleId = ((int) generatedKeys.getLong(1));

                    int idx = 0;
                    for (final String author : article.getAuthors()) {
                      try (final PreparedStatement insertAuthor = context.getConnection().prepareStatement(
                          "insert into public.article_author (idx, article_id, author) values (?,?,?)")) {
                        insertAuthor.setInt(1, idx++);
                        insertAuthor.setInt(2, articleId);
                        insertAuthor.setString(3, author);
                        insertAuthor.executeUpdate();
                      }
                    }

                  } else {
                    logger.error("Problem with migrating cached article data: " + xmlArticle);
                  }
                }
              }
            }
          } catch (final Exception e) {
            logger.error("Problem with migrating cached article data: " + xmlArticle, e);
          }
        }
      }
    } catch (final Exception e) {
      logger.error("Problem with migrating cached article data", e);
    }

    try (final Statement select = context.getConnection().createStatement()) {
      select.executeUpdate("delete from cache_query_table where query like 'pubmed:%'");
    } catch (final Exception e) {
      logger.error("Problem with purgining cached article data", e);
    }

  }

}
