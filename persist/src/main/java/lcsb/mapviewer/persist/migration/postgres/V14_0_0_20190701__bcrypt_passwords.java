package lcsb.mapviewer.persist.migration.postgres;

import java.sql.ResultSet;
import java.sql.Statement;

import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.springframework.security.crypto.bcrypt.BCrypt;

//CHECKSTYLE.OFF
public class V14_0_0_20190701__bcrypt_passwords extends BaseJavaMigration {
  // CHECKSTYLE.ON

  @Override
  public void migrate(final Context context) throws Exception {
    try (final Statement select = context.getConnection().createStatement()) {
      try (final ResultSet rows = select.executeQuery("select id, crypted_password from user_table")) {
        while (rows.next()) {
          int id = rows.getInt(1);
          String md5Password = rows.getString(2);
          String bcryptMd5Password = BCrypt.hashpw(md5Password, BCrypt.gensalt());
          try (final Statement update = context.getConnection().createStatement()) {
            update.execute("update user_table set crypted_password = '" + bcryptMd5Password + "' where id = " + id);
          }
        }
      }
    }
  }

}
