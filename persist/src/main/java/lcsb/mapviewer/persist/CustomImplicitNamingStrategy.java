package lcsb.mapviewer.persist;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.ImplicitEntityNameSource;
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy;

/**
 * This naming strategy adds "Table" suffix to table names when there is no
 * table name defined.
 * 
 * @author Piotr Gawron
 *
 */
public class CustomImplicitNamingStrategy extends SpringImplicitNamingStrategy {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private static final String TABLE_NAME_SUFFIX = "Table";

  @Override
  public Identifier determinePrimaryTableName(final ImplicitEntityNameSource source) {
    Identifier newIdentifier = super.determinePrimaryTableName(source);
    return new Identifier(newIdentifier.getText() + TABLE_NAME_SUFFIX, newIdentifier.isQuoted());
  }
}
