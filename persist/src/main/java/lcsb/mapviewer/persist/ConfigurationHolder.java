package lcsb.mapviewer.persist;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.stereotype.Service;

@Service
@PropertySources({
    @PropertySource("classpath:db.properties"),
    @PropertySource(value = "file:/etc/minerva/db.properties", ignoreResourceNotFound = true)
})
public class ConfigurationHolder {

  @Value("${database.uri}")
  private String dbUri;

  @Value("${database.username}")
  private String dbUsername;

  @Value("${database.password}")
  private String dbPassword;

  @Value("${database.driver:org.postgresql.Driver}")
  private String dbDriver;

  @Value("${database.dialect:org.hibernate.dialect.PostgreSQL95Dialect}")
  private String dbDialect;

  @Value("${database.connectionQuery:select 1}")
  private String dbConnectionQuery;

  @Value("${database.pathToDataScripts:#{null}}")
  private String pathToDataScripts;

  public String getDbUri() {
    return dbUri;
  }

  public void setDbUri(final String dbUri) {
    this.dbUri = dbUri;
  }

  public String getDbUsername() {
    return dbUsername;
  }

  public void setDbUsername(final String dbUsername) {
    this.dbUsername = dbUsername;
  }

  public String getDbPassword() {
    return dbPassword;
  }

  public void setDbPassword(final String dbPassword) {
    this.dbPassword = dbPassword;
  }

  public String getDbDriver() {
    return dbDriver;
  }

  public void setDbDriver(final String dbDriver) {
    this.dbDriver = dbDriver;
  }

  public String getDbDialect() {
    return dbDialect;
  }

  public void setDbDialect(final String dbDialect) {
    this.dbDialect = dbDialect;
  }

  public String getDbConnectionQuery() {
    return dbConnectionQuery;
  }

  public void setDbConnectionQuery(final String dbConnectionQuery) {
    this.dbConnectionQuery = dbConnectionQuery;
  }

  public String getPathToDataScripts() {
    return pathToDataScripts;
  }

  public void setPathToDataScripts(final String pathToDataScripts) {
    this.pathToDataScripts = pathToDataScripts;
  }
}
