package lcsb.mapviewer.persist;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.OneToOne;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.ReflectionUtils;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.user.User;

public class ObjectValidator {
  private Logger logger = LogManager.getLogger();

  private static final Set<Class<?>> SKIPPED_CLASSES = new HashSet<>(Arrays.asList(
      int.class,
      double.class,
      boolean.class,
      long.class,
      float.class,
      char.class,
      short.class,
      byte.class,
      User.class,
      ModelFullIndexed.class));

  private interface ProcessIssue {
    void accept(final Object object, final Field field, final Object subjectUnderInvestigation);
  }

  public List<Pair<Object, String>> getValidationIssues(final Object o) {
    final List<Pair<Object, String>> result = new ArrayList<>();
    processValidationIssues(o, new ProcessIssue() {
      @Override
      public void accept(final Object t, final Field u, final Object objectUnderInvestigation) {
        try {
          Column column = getAnnotationColumn(u);
          if (u.getType() == String.class) {
            u.setAccessible(true);

            String newObject = (String) u.get(t);
            if (column != null) {
              if (!column.nullable()) {
                if (newObject == null) {
                  result.add(new Pair<>(objectUnderInvestigation, u.getName() + " is null but shouldn't"));
                }
              }
              if (newObject != null && column.length() <= newObject.length()
                  && !column.columnDefinition().equals("TEXT")) {
                result.add(new Pair<>(objectUnderInvestigation,
                    u.getName() + " is too long (limit: " + column.length() + "; found: " + newObject.length() + ")"
                        + newObject));
              }
            }
          } else if (u.getType() == Double.class || u.getType() == Integer.class || u.getType() == Class.class
              || u.getType() == Color.class || u.getType() == Calendar.class || u.getType().isEnum()) {
            Object newObject = u.get(t);
            if (column != null && !column.nullable()) {
              if (newObject == null) {
                result.add(new Pair<>(objectUnderInvestigation, u.getName() + " is null but shouldn't"));
              }
            }
          } else if (Collection.class.isAssignableFrom(u.getType())) {
            Collection<?> newObjectSet = (Collection<?>) u.get(t);
            for (final Object o : newObjectSet) {
              if (isProblematicProperty(o, u)) {
                result.add(new Pair<>(objectUnderInvestigation, u.getName() + " has invalid entry: " + o));
              }
            }
          } else {
            Object newObject = u.get(t);
            if (isProblematicProperty(newObject, u)) {
              result.add(new Pair<>(objectUnderInvestigation, u.getName() + " is invalid:  " + newObject));
            }
          }
        } catch (final IllegalArgumentException | IllegalAccessException e) {
          logger.error("Problem with validating property", e);
        }
      }

    });
    return result;
  }

  private Column getAnnotationColumn(final Field u) {
    for (final Annotation a : u.getAnnotations()) {
      if (a instanceof Column) {
        return (Column) a;
      }
    }
    return null;
  }

  private OneToOne getOneToOne(final Field u) {
    for (final Annotation a : u.getAnnotations()) {
      if (a instanceof OneToOne) {
        return (OneToOne) a;
      }
    }
    return null;
  }

  public void fixValidationIssues(final Object object) {
    processValidationIssues(object, new ProcessIssue() {
      @Override
      public void accept(final Object t, final Field u, final Object objectUnderInvestigation) {
        try {
          u.setAccessible(true);
          Column column = getAnnotationColumn(u);
          Object newObject = u.get(t);
          if (column != null) {
            if (u.getType() == String.class) {
              String newString = (String) u.get(t);
              if (!column.nullable()) {
                if (newString == null) {
                  u.set(t, "");
                }
              }
              if (newString != null && column.length() <= newString.length()
                  && !column.columnDefinition().equals("TEXT")) {
                u.set(t, newString.substring(0, column.length() - 1));
              }

            } else if (u.getType() == Double.class) {
              if (!column.nullable() && newObject == null) {
                u.set(t, 0.0);
              }
            } else if (u.getType() == Integer.class) {
              if (!column.nullable() && newObject == null) {
                u.set(t, 0);
              }
            } else if (u.getType() == Color.class) {
              if (!column.nullable() && newObject == null) {
                u.set(t, Color.BLACK);
              }
            } else if (u.getType() == Class.class) {
              if (!column.nullable() && newObject == null) {
                u.set(t, Object.class);
              }
            } else if (u.getType() == Calendar.class) {
              if (!column.nullable() && newObject == null) {
                u.set(t, Calendar.getInstance());
              }
            } else if (u.getType().isEnum()) {
              if (!column.nullable() && newObject == null) {
                u.set(t, u.getType().getEnumConstants()[0]);
              }
            } else if (u.getType() == Point2D.class) {
              if (!column.nullable() && newObject == null) {
                u.set(t, new Point2D.Double());
              }
            } else if (Collection.class.isAssignableFrom(u.getType())) {
              Collection<?> newObjectSet = (Collection<?>) newObject;
              Set<Object> toRemove = new HashSet<>();

              for (final Object o : newObjectSet) {
                if (isProblematicProperty(o, u)) {
                  toRemove.add(o);
                }
              }
              newObjectSet.removeAll(toRemove);
            } else {
              logger.error("Don't know how to handle class: " + u.getType());
            }
          }
        } catch (final IllegalArgumentException | IllegalAccessException e) {
          logger.error("Problem with validating property", e);
        }
      }
    });

  }

  private Set<Object> processed = new HashSet<>();
  private Queue<Pair<Object, Object>> objects = new LinkedList<>();

  private synchronized void processValidationIssues(final Object o, final ProcessIssue processIssue) {
    initQueue();
    addToQueue(o, null);

    while (objects.size() > 0) {

      Pair<Object, Object> pair = objects.poll();
      Object object = pair.getLeft();

      ReflectionUtils.doWithFields(object.getClass(), new ReflectionUtils.FieldCallback() {
        @Override
        public void doWith(final Field arg0) {
          try {
            // skip static
            if (java.lang.reflect.Modifier.isStatic(arg0.getModifiers())) {
              return;
            }
            arg0.setAccessible(true);
            Object property = arg0.get(object);
            if (isIgnoredClass(arg0.getType())) {
              // skip
            } else if (property == null
                || property instanceof String
                || property instanceof Double
                || property instanceof Integer
                || property instanceof Color
                || property instanceof Calendar
                || property instanceof Class
                || property instanceof Calendar
                || arg0.getType().isEnum()) {
              if (isProblematicProperty(property, arg0)) {
                processIssue.accept(object, arg0, pair.getRight());
              }
            } else if (property instanceof Collection) {
              Collection<?> newObjectSet = (Collection<?>) property;

              List<Object> problematicEntries = new ArrayList<>();
              if (newObjectSet != null) {
                for (final Object newObject : newObjectSet) {
                  if (isProblematicProperty(newObject, arg0)) {
                    problematicEntries.add(newObject);
                  }
                  addToQueue(newObject, pair.getRight());
                }

                if (problematicEntries.size() > 0) {
                  processIssue.accept(object, arg0, pair.getRight());
                }
              }

            } else if (Object.class.isAssignableFrom(arg0.getType())) {
              arg0.setAccessible(true);

              if (isProblematicProperty(property, arg0)) {
                processIssue.accept(object, arg0, pair.getRight());
              }

              addToQueue(property, pair.getRight());
            } else {
              logger.error("Don't know how to process: " + arg0.getType());
            }
          } catch (final Exception e) {
            logger.error("Problem with checking property: " + arg0, e);
          }
        }

      });
    }

  }

  private void addToQueue(final Object object, final Object objectUnderInvestigation) {
    if (object != null && !isIgnoredClass(object.getClass())) {
      if (!processed.contains(object)) {
        if (object instanceof BioEntity) {
          objects.add(new Pair<>(object, object));
        } else if (object instanceof ModelData && objectUnderInvestigation == null) {
          objects.add(new Pair<>(object, object));
        } else {
          objects.add(new Pair<>(object, objectUnderInvestigation));
        }
        processed.add(object);
      }
    }
  }

  private void initQueue() {
    processed = new HashSet<>();
    objects = new LinkedList<>();
  }

  private boolean isProblematicProperty(final Object property, final Field field)
      throws IllegalArgumentException, IllegalAccessException {
    Column column = getAnnotationColumn(field);
    if (column != null) {
      if (property instanceof String) {
        String stringProperty = (String) property;
        if (stringProperty != null && column.length() <= stringProperty.length()
            && !column.columnDefinition().equals("TEXT")) {
          return true;
        }
      }
      if (!column.nullable()) {
        if (property == null) {
          return true;
        }
      }
    }
    OneToOne oneToOne = getOneToOne(field);
    if (oneToOne != null && !oneToOne.optional()) {
      if (property == null) {
        return true;
      }
    }
    return false;
  }

  private boolean isIgnoredClass(final Class<?> type) {
    for (final Class<?> clazz : SKIPPED_CLASSES) {
      if (clazz.isAssignableFrom(type)) {
        return true;
      }
    }
    if (type.getPackage() != null && type.getPackage().getName().contains("hibernate")) {
      return true;
    }
    return false;
  }

}
