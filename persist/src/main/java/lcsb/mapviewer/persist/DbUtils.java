package lcsb.mapviewer.persist;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.PostgreSQL81Dialect;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.function.Supplier;

/**
 * This class contains a set of methods that allow to manage hibernate sessions
 * in multi-thread approach. There are two types of sessions used in the system:
 * <ul>
 * <li>default - created and managed by sessionFactory</li>
 * <li>custom - created and managed by this class, used only when new thread is
 * created manually.</li>
 * </ul>
 * Class extends {@link Observable}. Whenever session is created or closed
 * observers are notified.
 *
 * @author Piotr Gawron
 */
@SuppressWarnings("deprecation")
@Service
public class DbUtils extends Observable {

  /**
   * Default class logger.
   */
  private static final Logger logger = LogManager.getLogger();

  /**
   * This determines if every add/update/delete operation should be flushed (for
   * specific {@link Thread}). There are few drawbacks of this approach:
   * <ol>
   * <li>when autoflushing is set to false, then the data consistency could be
   * broken (even in the same thread/transaction)</li>,
   * <li>we have to automatically take care of the flushing, therefore there
   * might be some problems with long transactions.</li>
   * </ol>
   */
  private static final Map<Long, Boolean> autoFlushForThread = new HashMap<>();

  /**
   * Hibernate session factory.
   */
  private SessionFactory sessionFactory;

  @Autowired
  public DbUtils(final SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  /**
   * @return the sessionFactory
   * @see #sessionFactory
   */
  public SessionFactory getSessionFactory() {
    return sessionFactory;
  }

  /**
   * @param sessionFactory the sessionFactory to set
   * @see #sessionFactory
   */
  public void setSessionFactory(final SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  /**
   * Returns info if the flush is automatically done or not in current
   * {@link Thread}.
   *
   * @return <code>true</code> if flush is automatically done,
   * <code>false</code> otherwise
   * @see #autoFlushForThread
   */
  public boolean isAutoFlush() {
    Long id = Thread.currentThread().getId();
    Boolean result = true;
    synchronized (autoFlushForThread) {
      result = autoFlushForThread.get(id);
    }
    if (result == null) {
      logger.debug("autoFlush not set for thread: " + id + ". Setting true");
      synchronized (autoFlushForThread) {
        autoFlushForThread.put(id, true);
      }
      result = true;
    }
    return result;
  }

  /**
   * Set autoFlush for current {@link Thread}.
   *
   * @param autoFlush the new autoflush value
   * @see #autoFlushForThread
   */
  public void setAutoFlush(final boolean autoFlush) {
    Long id = Thread.currentThread().getId();
    synchronized (autoFlushForThread) {
      autoFlushForThread.put(id, autoFlush);
    }
  }

  public Dialect getDialect() {
    return ((SessionFactoryImpl) sessionFactory).getDialect();
  }

  public void detach(final Object entity) {
    sessionFactory.getCurrentSession().evict(entity);
  }

  public void vaccuum() throws SQLException {
    if (getDialect() instanceof PostgreSQL81Dialect) {
      getSessionFactory().getCurrentSession().doWork(new Work() {
        @Override
        public void execute(final Connection connection) throws SQLException {
          boolean autoCommit = connection.getAutoCommit();
          connection.setAutoCommit(true);
          connection.prepareStatement("VACUUM ANALYZE").execute();
          connection.setAutoCommit(autoCommit);
        }
      });
    } else {
      logger.warn("Vacuum is not supported for dialect: " + getDialect().getClass());
    }
  }

  public void dumpDbToFile(final String filename) {
    sessionFactory.getCurrentSession().createNativeQuery("SCRIPT '" + filename + "'").executeUpdate();
  }

  @Transactional
  public void callInTransaction(final Supplier<Void> function) {
    function.get();
  }

  @Transactional
  public <T> T callInTransactionWithResult(final Supplier<T> function) {
    return function.get();
  }

}
