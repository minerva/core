package lcsb.mapviewer.persist;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.ScriptException;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class CustomDatabasePopulator implements DatabasePopulator {

  private final Logger logger = LogManager.getLogger();

  private final ConfigurationHolder config;

  private DataSource dataSource;

  @Autowired
  public CustomDatabasePopulator(final ConfigurationHolder config, final DataSource dataSource) {
    this.config = config;
    this.dataSource = dataSource;
  }

  @Override
  public void populate(final Connection connection) throws ScriptException {
    String migrationFolder = "postgres";
    if (config.getDbDriver().equals("org.hsqldb.jdbcDriver")) {
      migrationFolder = "db.migration.hsql";
    }
    final List<String> locations = new ArrayList<>(
        Arrays.asList("classpath:lcsb.mapviewer.persist/migration/" + migrationFolder,
            "classpath:db.migration." + migrationFolder,
            "classpath:" + migrationFolder
            ));
    if (config.getPathToDataScripts() != null) {
      logger.info("Additional SQL scripts located at: " + config.getPathToDataScripts());
      locations.add("filesystem:" + config.getPathToDataScripts());
    }
    Flyway.configure().dataSource(config.getDbUri(), config.getDbUsername(), config.getDbPassword())
        .baselineVersion("12.1.0")
        .locations(locations.toArray(new String[0]))
        .outOfOrder(true)
        .baselineOnMigrate(true)
        .validateOnMigrate(false)
        .load()
        .migrate();
  }

  public DataSource getDataSource() {
    return dataSource;
  }

  public void setDataSource(final DataSource dataSource) {
    this.dataSource = dataSource;
  }

}
