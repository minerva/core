alter table modification_residue_table add column border_color varchar(20);

update modification_residue_table set border_color = '#ff0000ff' where active;
update modification_residue_table set border_color = '#000000ff' where not active;

update modification_residue_table set border_color = (select border_color from element_table where element_table.id=modification_residue_table.id_species_db) where border_color is null;
update modification_residue_table set border_color = '#000000ff' where border_color is null;
 
alter table modification_residue_table alter column border_color set not null;
 