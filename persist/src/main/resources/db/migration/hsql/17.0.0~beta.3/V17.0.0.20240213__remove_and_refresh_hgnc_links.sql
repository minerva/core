delete from cache_query_table where value like '%genenames%';

--refresh all links in miriam_data_table    
insert into minerva_job_table (job_type, job_status, job_parameters) 
select 'REFRESH_MIRIAM_INFO', 'PENDING', concat('{"id":',id, ' }') from miriam_data_table where link like '%genenames%';

update miriam_data_table set link = null where link like '%genenames%';
