alter table project_table
    add column custom_license_name character varying(256) default '';

alter table project_table
    add column custom_license_url character varying(1024) default '';
