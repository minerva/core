update element_table
set name_vertical_align='MIDDLE'
where element_type_db = 'COMPLEX'
  and id not in (select idcomplexdb from element_table where idcomplexdb is not null);
