alter table layer_table
    add column z integer;

update layer_table
set z = id;

alter table layer_table
    alter column z set not null;
