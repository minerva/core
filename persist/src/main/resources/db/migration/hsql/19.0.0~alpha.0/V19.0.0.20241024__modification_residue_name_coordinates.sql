alter table modification_residue_table
    add column name_horizontal_align character varying(255);
alter table modification_residue_table
    add column name_vertical_align character varying(255);

update modification_residue_table
set name_horizontal_align='CENTER'
where true;

update modification_residue_table
set name_vertical_align='MIDDLE'
where true;

alter table modification_residue_table
    alter column name_horizontal_align set not null;
alter table modification_residue_table
    alter column name_vertical_align set not null;

alter table modification_residue_table
    add column name_x double precision;
alter table modification_residue_table
    add column name_y double precision;
alter table modification_residue_table
    add column name_width double precision;
alter table modification_residue_table
    add column name_height double precision;

update modification_residue_table
set name_x      = x,
    name_y      = y,
    name_height = height,
    name_width  = width
where true;

update modification_residue_table
set name_height = width
where modification_type = 'MODIFICATION_SITE';

update modification_residue_table
set name_vertical_align   = 'TOP',
    name_horizontal_align = 'LEFT',
    name_y                = y - 12
where modification_type = 'TRANSCRIPTION_SITE';

update modification_residue_table
set name_horizontal_align = 'RIGHT'
where modification_type = 'TRANSCRIPTION_SITE'
  and direction = 'RIGHT';

alter table modification_residue_table
    alter column name_x set not null;
alter table modification_residue_table
    alter column name_y set not null;
alter table modification_residue_table
    alter column name_width set not null;
alter table modification_residue_table
    alter column name_height set not null;
