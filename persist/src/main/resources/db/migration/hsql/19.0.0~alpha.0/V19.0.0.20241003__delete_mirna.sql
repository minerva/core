delete
from cache_query_table
where type in (select id from cache_type_table where class_name = 'lcsb.mapviewer.annotation.services.MiRNAParser');
delete
from cache_type_table
where class_name = 'lcsb.mapviewer.annotation.services.MiRNAParser';

delete
from minerva_job_table
where job_type = 'REFRESH_MI_RNA_INFO';

update project_table
set status ='FAIL'
where status = 'CACHING_MI_RNA';