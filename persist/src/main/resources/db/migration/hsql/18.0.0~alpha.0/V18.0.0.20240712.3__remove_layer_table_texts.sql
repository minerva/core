delete
from layer_oval_table
where id not in (select ovals_id from layer_table_ovals);

alter table layer_oval_table
    add column layer_id integer;

ALTER TABLE layer_oval_table
    ADD CONSTRAINT layer_oval_table_layer_fk FOREIGN KEY (layer_id) REFERENCES public.layer_table (id)
        ON DELETE SET NULL;

update layer_oval_table
set layer_id = (select layer_table_id from layer_table_ovals where ovals_id = layer_oval_table.id)
where layer_id is null;

drop table layer_table_ovals;