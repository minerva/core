ALTER TABLE public.layer_table_lines
    drop CONSTRAINT layer_table_layerline_pkey;

ALTER TABLE layer_table_lines
    drop column idx;

ALTER TABLE public.layer_table_lines
    ADD CONSTRAINT layer_table_layerline_pkey PRIMARY KEY (lines_id);
