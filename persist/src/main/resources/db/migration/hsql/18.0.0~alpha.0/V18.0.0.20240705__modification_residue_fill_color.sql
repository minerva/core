alter table modification_residue_table
    add column fill_color character varying(10);

update modification_residue_table
set fill_color = '#ffffff';

update modification_residue_table
set fill_color = border_color
where modification_type = 'TRANSCRIPTION_SITE';

alter table modification_residue_table
    alter column fill_color set not null;
