CREATE SEQUENCE reset_password_token_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE reset_password_token_table
(
  id integer DEFAULT nextval('public.reset_password_token_sequence') NOT NULL,
  token character varying(255) NOT NULL,
  user_id integer not null,
  expires timestamp without time zone,

  CONSTRAINT reset_password_token_pk PRIMARY KEY (id),
  CONSTRAINT reset_password_token_user_fk FOREIGN KEY (user_id)
      REFERENCES user_table (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);
