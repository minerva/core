SET DATABASE SQL SYNTAX PGS TRUE;

CREATE TABLE public.line_segment (
    x1 numeric NOT NULL,
    y1 numeric NOT NULL,
    x2 numeric NOT NULL,
    y2 numeric NOT NULL
);

CREATE FUNCTION dist_to_segment(px double, py double, vx double, vy double, wx double, wy double)
  RETURNS double
BEGIN atomic
   declare l2 double;
   declare t double;
   declare nx double;
   declare ny double;
   set l2 =(vx - wx)*(vx - wx) + (vy - wy)*(vy - wy);
   IF l2 = 0 THEN
     RETURN sqrt((vx - px)*(vx - px) + (vy - py)*(vy - py));
   ELSE
     set t = ((px - vx) * (wx - vx) + (py - vy) * (wy - vy)) / l2;
     set t = GREATEST(0, LEAST(1, t));
     set nx=vx + t * (wx - vx);
     set ny=vy + t * (wy - vy);
     RETURN sqrt((nx - px)*(nx - px) + (ny - py)*(ny - py));
   END IF;
END;

alter table point_table add column end_point character varying(255) ;
update point_table p1 set end_point = (select point_val from point_table p2 where p1.idx+1=p2.idx and p2.id=p1.id);

delete from point_table where end_point is null;

alter table point_table add column x1 double;
alter table point_table add column y1 double;
alter table point_table add column x2 double;
alter table point_table add column y2 double;

update point_table set x1 = cast (REGEXP_SUBSTRING_ARRAY(point_val, '[0-9\.\-]+')[1] as double precision);
update point_table set y1 = cast (REGEXP_SUBSTRING_ARRAY(point_val, '[0-9\.\-]+')[2] as double precision);

update point_table set x2 = cast (REGEXP_SUBSTRING_ARRAY(end_point, '[0-9\.\-]+')[1] as double precision);
update point_table set y2 = cast (REGEXP_SUBSTRING_ARRAY(end_point, '[0-9\.\-]+')[2] as double precision);

alter table point_table alter column x1 set not null;
alter table point_table alter column y1 set not null;
alter table point_table alter column x2 set not null;
alter table point_table alter column y2 set not null;

alter table point_table drop column point_val;
alter table point_table drop column end_point;
