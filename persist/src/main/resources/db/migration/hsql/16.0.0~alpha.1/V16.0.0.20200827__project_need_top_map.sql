-- top map
alter table project_table add column top_model_id integer;
ALTER TABLE public.project_table
    ADD CONSTRAINT top_modelproject_table_fk FOREIGN KEY (top_model_id) REFERENCES public.model_data_table(id);
update public.project_table set top_model_id = (select id from model_data_table where project_id=project_table.id);
