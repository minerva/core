alter table public.data_overlay_image_layer_table alter column data_overlay_id rename to project_background_id;

alter table public.data_overlay_image_layer_table rename to project_background_image_layer_table;

alter table public.layout_table alter column title rename to name;

alter table public.layout_table rename to project_background_table;

alter table public.user_annotation_schema_table alter column network_layout_as_default rename to network_background_as_default;
