SET DATABASE SQL SYNTAX PGS TRUE;

alter table public.polyline_data_table
   add column tmp_font_color character varying(10);

update polyline_data_table set tmp_font_color = concat('#',left(right(RAWTOHEX(color),12),6),left(right(RAWTOHEX(color),14),2)) where color is not null;
update polyline_data_table set tmp_font_color = '#000000ff' where color is null;

alter table public.polyline_data_table drop column color;

alter table public.polyline_data_table rename tmp_font_color to color;
   
alter table polyline_data_table alter column color set not null;


alter table public.layer_oval_table
   add column tmp_font_color character varying(10);

update layer_oval_table set tmp_font_color = concat('#',left(right(RAWTOHEX(color),12),6),left(right(RAWTOHEX(color),14),2)) where color is not null;
update layer_oval_table set tmp_font_color = '#000000ff' where color is null;

alter table public.layer_oval_table drop column color;

alter table public.layer_oval_table rename tmp_font_color to color;
   
alter table layer_oval_table alter column color set not null;


alter table public.layer_rect_table
   add column tmp_fill_color character varying(10);
alter table public.layer_rect_table
   add column tmp_border_color character varying(10);

update layer_rect_table set tmp_fill_color = concat('#',left(right(RAWTOHEX(fill_color),12),6),left(right(RAWTOHEX(fill_color),14),2)) where fill_color is not null;
update layer_rect_table set tmp_fill_color = '#c0c0c0ff' where fill_color is null;
update layer_rect_table set tmp_border_color = concat('#',left(right(RAWTOHEX(border_color),12),6),left(right(RAWTOHEX(border_color),14),2)) where border_color is not null;
update layer_rect_table set tmp_border_color = '#000000ff' where border_color is null;

alter table public.layer_rect_table drop column fill_color;
alter table public.layer_rect_table drop column border_color;

alter table public.layer_rect_table rename column tmp_fill_color to fill_color;
alter table public.layer_rect_table rename column tmp_border_color to border_color;
   
alter table layer_rect_table alter column fill_color set not null;
alter table layer_rect_table alter column border_color set not null;


alter table public.layer_text_table
   add column tmp_background_color character varying(10);
alter table public.layer_text_table
   add column tmp_border_color character varying(10);
alter table public.layer_text_table
   add column tmp_font_color character varying(10);

update layer_text_table set tmp_font_color = concat('#',left(right(RAWTOHEX(color),12),6),left(right(RAWTOHEX(color),14),2)) where color is not null;
update layer_text_table set tmp_font_color = '#000000ff' where color is null;
update layer_text_table set tmp_background_color = concat('#',left(right(RAWTOHEX(background_color),12),6),left(right(RAWTOHEX(background_color),14),2)) where background_color is not null;
update layer_text_table set tmp_background_color = '#c0c0c0ff' where background_color is null;
update layer_text_table set tmp_border_color = concat('#',left(right(RAWTOHEX(border_color),12),6),left(right(RAWTOHEX(border_color),14),2)) where border_color is not null;
update layer_text_table set tmp_border_color = '#c0c0c0ff' where border_color is null;

alter table public.layer_text_table drop column color;
alter table public.layer_text_table drop column background_color;
alter table public.layer_text_table drop column border_color;

alter table public.layer_text_table rename tmp_font_color to color;
alter table public.layer_text_table rename tmp_background_color to background_color;
alter table public.layer_text_table rename column tmp_border_color to border_color;
   
alter table layer_text_table alter column color set not null;
alter table layer_text_table alter column background_color set not null;
alter table layer_text_table alter column border_color set not null;


alter table public.element_table
   add column tmp_fill_color character varying(10);
alter table public.element_table
   add column tmp_border_color character varying(10);
alter table public.element_table
   add column tmp_font_color character varying(10);

update element_table set tmp_font_color = concat('#',left(right(RAWTOHEX(font_color),12),6),left(right(RAWTOHEX(font_color),14),2)) where font_color is not null;
update element_table set tmp_font_color = '#000000ff' where font_color is null;
update element_table set tmp_fill_color = concat('#',left(right(RAWTOHEX(fill_color),12),6),left(right(RAWTOHEX(fill_color),14),2)) where fill_color is not null;
update element_table set tmp_fill_color = '#c0c0c0ff' where fill_color is null;
update element_table set tmp_border_color = concat('#',left(right(RAWTOHEX(border_color),12),6),left(right(RAWTOHEX(border_color),14),2)) where border_color is not null;
update element_table set tmp_border_color = '#000000ff' where border_color is null;

alter table public.element_table drop column font_color;
alter table public.element_table drop column fill_color;
alter table public.element_table drop column border_color;

alter table public.element_table rename tmp_font_color to font_color;
alter table public.element_table rename tmp_fill_color to fill_color;
alter table public.element_table rename tmp_border_color to border_color;
   
alter table element_table alter column font_color set not null;
alter table element_table alter column fill_color set not null;
alter table element_table alter column border_color set not null;


alter table public.user_table
   add column tmp_min_color character varying(10);
alter table public.user_table
   add column tmp_max_color character varying(10);
alter table public.user_table
   add column tmp_neutral_color character varying(10);
alter table public.user_table
   add column tmp_simple_color character varying(10);

update user_table set tmp_neutral_color = concat('#',left(right(RAWTOHEX(neutral_color),12),6),left(right(RAWTOHEX(neutral_color),14),2)) where neutral_color is not null;
update user_table set tmp_simple_color = concat('#',left(right(RAWTOHEX(simple_color),12),6),left(right(RAWTOHEX(simple_color),14),2)) where simple_color is not null;
update user_table set tmp_min_color = concat('#',left(right(RAWTOHEX(min_color),12),6),left(right(RAWTOHEX(min_color),14),2)) where min_color is not null;
update user_table set tmp_max_color = concat('#',left(right(RAWTOHEX(max_color),12),6),left(right(RAWTOHEX(max_color),14),2)) where max_color is not null;

alter table public.user_table drop column neutral_color;
alter table public.user_table drop column simple_color;
alter table public.user_table drop column min_color;
alter table public.user_table drop column max_color;

alter table public.user_table rename tmp_neutral_color to neutral_color;
alter table public.user_table rename tmp_simple_color to simple_color;
alter table public.user_table rename tmp_min_color to min_color;
alter table public.user_table rename tmp_max_color to max_color;
