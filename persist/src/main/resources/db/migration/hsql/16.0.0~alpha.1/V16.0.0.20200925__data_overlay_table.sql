update public.layout_table set creator_id = (select owner_id from project_table where project_table.id= layout_table.project_id) where creator_id is null;
alter table public.layout_table alter column creator_id set not null;

CREATE SEQUENCE public.data_overlay_sequence
    START WITH 1000000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.data_overlay_table (
    id integer DEFAULT nextval('public.data_overlay_sequence') NOT NULL,
    name character varying(255),
    color_schema_type character varying(255),
    creator_id integer NOT NULL,
    description text,
    google_license_consent boolean NOT NULL,
    file_entry_id integer NOT NULL,
    order_index integer NOT NULL,
    project_id integer NOT NULL,
    is_public boolean NOT NULL,
);

ALTER TABLE public.data_overlay_table
    ADD CONSTRAINT data_overlay_table_pk PRIMARY KEY (id);

ALTER TABLE public.data_overlay_table
    ADD CONSTRAINT data_overlay_table_creator_fk FOREIGN KEY (creator_id) REFERENCES public.user_table(id);
    
ALTER TABLE public.data_overlay_table
    ADD CONSTRAINT data_overlay_table_file_fk FOREIGN KEY (file_entry_id) REFERENCES public.file_entry_table(id);

ALTER TABLE public.data_overlay_table
    ADD CONSTRAINT data_overlay_table_project_fk FOREIGN KEY (project_id) REFERENCES public.project_table(id);

insert into public.data_overlay_table (id, name, color_schema_type, creator_id, description, google_license_consent, file_entry_id, order_index, project_id, is_public)
select id, title, color_schema_type, creator_id, description, google_license_consent, file_entry_id, order_index, project_id, public_layout from layout_table where file_entry_id is not null; 

delete from layout_table where file_entry_id is not null;

alter table layout_table drop column color_schema_type;
alter table layout_table drop column google_license_consent;
alter table layout_table drop column file_entry_id;
alter table layout_table drop column public_layout;

