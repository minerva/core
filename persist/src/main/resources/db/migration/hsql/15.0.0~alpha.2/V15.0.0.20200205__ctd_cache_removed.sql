SET DATABASE SQL SYNTAX PGS TRUE;
SET DATABASE TRANSACTION CONTROL MVCC;

CREATE SEQUENCE public.annotator_data_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.annotator_data_table (
    id integer DEFAULT nextval('public.annotator_data_sequence') NOT NULL,
    order_index integer NOT NULL,
    annotator_class_name character varying(255),
    user_class_annotators_id integer NOT NULL
);

CREATE SEQUENCE public.user_annotators_param_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.annotator_parameter_table (
    id integer DEFAULT nextval('public.user_annotators_param_sequence') NOT NULL,
    type character varying(255),
    value character varying(255),
    type_distinguisher character varying(255) NOT NULL,
    annotator_data_id integer NOT NULL,
    field character varying(255),
    identifier_type character varying(255),
    order_position integer
);

CREATE SEQUENCE public.author_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.author_table (
    id integer DEFAULT nextval('public.author_sequence') NOT NULL,
    first_name character varying(255),
    last_name character varying(255),
    email character varying(255),
    organisation character varying(255)
);

CREATE SEQUENCE public.cache_query_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.cache_query_table (
    id integer DEFAULT nextval('public.cache_query_sequence') NOT NULL,
    accessed timestamp without time zone,
    query text,
    value text,
    type integer NOT NULL,
    expires timestamp without time zone
);

CREATE SEQUENCE public.cache_type_sequence
    START WITH 29
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.cache_type_table (
    id integer DEFAULT nextval('public.cache_type_sequence') NOT NULL,
    validity integer,
    class_name character varying (1024)
);

CREATE TABLE public.class_required_annotation_miriam_type_table (
    class_required_annotation_id integer NOT NULL,
    idx integer NOT NULL,
    miriam_type_name character varying(255)
);

CREATE TABLE public.class_valid_annotation_miriam_type_table (
    class_valid_annotation_id integer NOT NULL,
    idx integer NOT NULL,
    miriam_type_name character varying(255)
);

CREATE SEQUENCE public.comment_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.comment_table (
    id integer DEFAULT nextval('public.comment_sequence') NOT NULL,
    content text,
    coordinates character varying(255),
    deleted boolean NOT NULL,
    email character varying(255),
    name character varying(255),
    remove_reason character varying(255),
    table_id integer,
    table_name character varying(255),
    pinned boolean NOT NULL,
    model_id integer NOT NULL,
    user_id integer
);

CREATE SEQUENCE public.configuration_option_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.configuration_option_table (
    id integer DEFAULT nextval('public.configuration_option_sequence') NOT NULL,
    value character varying(255),
    type character varying(255)
);

CREATE SEQUENCE public.data_overlay_image_layer_sequence
    START WITH 6
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.data_overlay_image_layer_table (
    id integer DEFAULT nextval('public.data_overlay_image_layer_sequence') NOT NULL,
    data_overlay_id integer NOT NULL,
    model_id integer NOT NULL,
    directory character varying(255)
);

CREATE TABLE public.element_former_symbols (
    id integer NOT NULL,
    idx integer NOT NULL,
    symbol character varying(255)
);

CREATE TABLE public.element_miriam (
    element_id integer NOT NULL,
    miriam_id integer NOT NULL
);

CREATE SEQUENCE public.element_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.element_table (
    element_type_db character varying(31) NOT NULL,
    id integer DEFAULT nextval('public.element_sequence') NOT NULL,
    element_id character varying(255),
    fill_color VARBINARY(1G) NOT NULL,
    font_size double precision NOT NULL,
    height double precision,
    visibility_level character varying(255) NOT NULL,
    width double precision,
    x double precision,
    y double precision,
    inner_width double precision,
    name_point character varying(255),
    outer_width double precision,
    thickness double precision,
    name character varying(255),
    activity boolean,
    line_width double precision,
    state character varying(255),
    compartment_id integer,
    model_id integer,
    idcomplexdb integer,
    transparency_level character varying(255),
    submodel_id integer,
    state_label character varying(255),
    state_prefix character varying(255),
    abbreviation character varying(255),
    formula character varying(255),
    full_name character varying(255),
    notes text,
    symbol character varying(255),
    charge integer,
    homodimer integer,
    hypothetical boolean,
    initial_amount integer,
    initial_concentration integer,
    only_substance_units boolean,
    position_to_compartment character varying(255),
    in_chi text,
    in_chikey character varying(255),
    smiles text,
    boundary_condition boolean,
    constant boolean,
    substance_unit_raw_type character varying(255),
    font_color VARBINARY(1G) DEFAULT HEXTORAW('aced00057372000e6a6176612e6177742e436f6c6f7201a51783108f337502000546000666616c70686149000576616c75654c0002637374001b4c6a6176612f6177742f636f6c6f722f436f6c6f7253706163653b5b00096672676276616c75657400025b465b00066676616c756571007e0002787000000000ff000000707070') NOT NULL,
    z integer NOT NULL,
    glyph_id integer,
    substance_unit_comlex_type_id integer,
    structural_state_id integer,
    border_color VARBINARY(1G) DEFAULT HEXTORAW('aced00057372000e6a6176612e6177742e436f6c6f7201a51783108f337502000546000666616c70686149000576616c75654c0002637374001b4c6a6176612f6177742f636f6c6f722f436f6c6f7253706163653b5b00096672676276616c75657400025b465b00066676616c756571007e0002787000000000ff010101707070') NOT NULL
);

CREATE TABLE public.element_synonyms (
    id integer NOT NULL,
    idx integer NOT NULL,
    synonym character varying(512)
);

CREATE SEQUENCE public.file_entry_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.file_entry_table (
    id integer DEFAULT nextval('public.file_entry_sequence') NOT NULL,
    url text,
    local_path text,
    download_date timestamp without time zone,
    download_progress double precision,
    download_thread_id integer,
    removed boolean DEFAULT false,
    file_content VARBINARY(1G),
    file_type_db character varying(255),
    original_file_name character varying(255),
    length bigint,
    owner_id integer
);

CREATE SEQUENCE public.glyph_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.glyph_table (
    id integer DEFAULT nextval('public.glyph_sequence') NOT NULL,
    file_entry_id integer NOT NULL,
    project_id integer NOT NULL
);

CREATE TABLE public.kinetic_law_elements (
    kinetic_law_id integer NOT NULL,
    element_id integer NOT NULL
);

CREATE TABLE public.kinetic_law_functions (
    kinetic_law_id integer NOT NULL,
    function_id integer NOT NULL
);

CREATE TABLE public.kinetic_law_parameters (
    kinetic_law_id integer NOT NULL,
    parameter_id integer NOT NULL
);

CREATE SEQUENCE public.layer_oval_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.layer_oval_table (
    id integer DEFAULT nextval('public.layer_oval_sequence') NOT NULL,
    color VARBINARY(1G),
    height double precision,
    width double precision,
    x double precision,
    y double precision,
    z integer NOT NULL
);

CREATE SEQUENCE public.layer_rect_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.layer_rect_table (
    id integer DEFAULT nextval('public.layer_rect_sequence') NOT NULL,
    color VARBINARY(1G),
    height double precision,
    width double precision,
    x double precision,
    y double precision,
    z integer NOT NULL
);

CREATE SEQUENCE public.layer_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.layer_table (
    id integer DEFAULT nextval('public.layer_sequence') NOT NULL,
    layer_id character varying(255),
    locked boolean NOT NULL,
    name character varying(255),
    visible boolean NOT NULL,
    model_id integer
);

CREATE TABLE public.layer_table_lines (
    layer_table_id integer NOT NULL,
    lines_id integer NOT NULL,
    idx integer NOT NULL
);

CREATE TABLE public.layer_table_ovals (
    layer_table_id integer NOT NULL,
    ovals_id integer NOT NULL,
    idx integer NOT NULL
);

CREATE TABLE public.layer_table_rectangles (
    layer_table_id integer NOT NULL,
    rectangles_id integer NOT NULL,
    idx integer NOT NULL
);

CREATE TABLE public.layer_table_texts (
    layer_table_id integer NOT NULL,
    texts_id integer NOT NULL,
    idx integer NOT NULL
);

CREATE SEQUENCE public.layer_text_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.layer_text_table (
    id integer DEFAULT nextval('public.layer_text_sequence') NOT NULL,
    color VARBINARY(1G),
    font_size double precision,
    height double precision,
    notes text,
    width double precision,
    x double precision,
    y double precision,
    background_color VARBINARY(1G) DEFAULT HEXTORAW('aced00057372000e6a6176612e6177742e436f6c6f7201a51783108f337502000546000666616c70686149000576616c75654c0002637374001b4c6a6176612f6177742f636f6c6f722f436f6c6f7253706163653b5b00096672676276616c75657400025b465b00066676616c756571007e0002787000000000ffc0c0c0707070'),
    z integer NOT NULL,
    glyph_id integer,
    border_color VARBINARY(1G) DEFAULT  HEXTORAW('aced00057372000e6a6176612e6177742e436f6c6f7201a51783108f337502000546000666616c70686149000576616c75654c0002637374001b4c6a6176612f6177742f636f6c6f722f436f6c6f7253706163653b5b00096672676276616c75657400025b465b00066676616c756571007e0002787000000000ffc0c0c0707070') NOT NULL
);

CREATE SEQUENCE public.layout_sequence
    START WITH 10
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.layout_table (
    id integer DEFAULT nextval('public.layout_sequence') NOT NULL,
    title character varying(255),
    creator_id integer,
    public_layout boolean,
    progress double precision,
    description text,
    hierarchical_view boolean DEFAULT false,
    file_entry_id integer,
    hierarchy_view_level integer,
    google_license_consent boolean DEFAULT false,
    default_overlay boolean DEFAULT false,
    order_index integer DEFAULT 0,
    status character varying(31),
    project_id integer,
    color_schema_type character varying(255) NOT NULL
);

CREATE SEQUENCE public.miriam_data_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.miriam_data_table (
    id integer DEFAULT nextval('public.miriam_data_sequence') NOT NULL,
    resource character varying(255),
    data_type character varying(255),
    relation_type integer,
    annotator character varying(256)
);

CREATE TABLE public.model_data_modification_dates (
    idx integer NOT NULL,
    model_data_id integer NOT NULL,
    modification_date timestamp without time zone
);

CREATE SEQUENCE public.model_data_sequence
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.model_data_table (
    id integer DEFAULT nextval('public.model_data_sequence') NOT NULL,
    height double precision NOT NULL,
    notes text,
    tile_size integer NOT NULL,
    width double precision NOT NULL,
    zoom_levels integer NOT NULL,
    project_id integer,
    id_model character varying(255),
    name character varying(255),
    default_zoom_level integer,
    default_centerx double precision,
    default_centery double precision,
    creation_date timestamp without time zone
);

CREATE TABLE public.model_data_table_authors (
    model_data_id integer NOT NULL,
    author_id integer NOT NULL,
    idx integer NOT NULL
);

CREATE TABLE public.model_miriam_data_table (
    model_id integer NOT NULL,
    miriam_data_id integer NOT NULL
);

CREATE TABLE public.model_parameters_table (
    model_id integer NOT NULL,
    parameter_id integer NOT NULL
);

CREATE SEQUENCE public.modification_residue_sequence
    START WITH 51016
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.modification_residue_table (
    id integer DEFAULT nextval('public.modification_residue_sequence') NOT NULL,
    id_modification_residue character varying(255),
    name character varying(255),
    id_species_db integer NOT NULL,
    position character varying(255),
    state character varying(255),
    modification_type character varying(255),
    width numeric(6,2),
    active boolean,
    height numeric(6,2),
    direction character varying(255)
);

CREATE SEQUENCE public.overview_image_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.overview_image_table (
    id integer DEFAULT nextval('public.overview_image_sequence') NOT NULL,
    filename character varying(255),
    width integer,
    height integer,
    project_id integer
);

CREATE SEQUENCE public.overview_link_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.overview_link_table (
    id integer DEFAULT nextval('public.overview_link_sequence') NOT NULL,
    overview_image_id integer,
    polygon character varying(2048),
    zoom_level integer,
    y_coord integer,
    x_coord integer,
    link_type character varying(31) NOT NULL,
    linked_overview_image_id integer,
    linked_model_id integer,
    query character varying(1024)
);

CREATE SEQUENCE public.plugin_data_entry_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE SEQUENCE public.plugin_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.plugin_data_entry_table (
    id integer DEFAULT nextval('public.plugin_sequence') NOT NULL,
    plugin_id integer NOT NULL,
    user_id integer,
    key character varying(1024) NOT NULL,
    value character varying(1048576) NOT NULL
);

CREATE TABLE public.plugin_table (
    id integer DEFAULT nextval('public.plugin_sequence') NOT NULL,
    hash character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    version character varying(255) NOT NULL,
    is_public boolean DEFAULT false,
    is_default boolean DEFAULT false NOT NULL
);

CREATE SEQUENCE public.plugin_url_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.plugin_urls (
    plugin_id integer NOT NULL,
    url character varying(1024) NOT NULL,
    idx integer DEFAULT nextval('public.plugin_url_sequence') NOT NULL
);

CREATE TABLE public.point_table (
    id integer NOT NULL,
    point_val character varying(255) NOT NULL,
    idx integer NOT NULL
);

CREATE SEQUENCE public.polyline_data_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.polyline_data_table (
    id integer DEFAULT nextval('public.polyline_data_sequence') NOT NULL,
    color VARBINARY(1G),
    width double precision NOT NULL,
    type character varying(31),
    begin_atd character varying(255),
    end_atd character varying(255)
);

CREATE SEQUENCE public.privilege_sequence
    START WITH 177
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.privilege_table (
    id integer DEFAULT nextval('public.privilege_sequence') NOT NULL,
    object_id character varying(255),
    type character varying(255)
);

CREATE SEQUENCE public.project_log_entry_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.project_log_entry_table (
    id integer DEFAULT nextval('public.project_log_entry_sequence') NOT NULL,
    severity character varying(255) NOT NULL,
    type character varying(255) NOT NULL,
    object_identifier character varying(255),
    object_class character varying(1024),
    map_name character varying(255),
    project_id integer NOT NULL,
    content text,
    source character varying(255)
);

CREATE SEQUENCE public.project_sequence
    START WITH 5
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.project_table (
    id integer DEFAULT nextval('public.project_sequence') NOT NULL,
    project_id character varying(255),
    progress double precision NOT NULL,
    directory character varying(1024),
    name character varying(255),
    disease_id integer,
    organism_id integer,
    sbgn_format boolean DEFAULT false,
    version character varying(255) DEFAULT '',
    file_entry_id integer,
    notify_email character varying(255) DEFAULT '',
    status character varying(255) DEFAULT 'UNKNOWN',
    google_map_license_accept_date timestamp without time zone,
    map_canvas_type character varying (255) DEFAULT 'OPEN_LAYERS',
    creation_date timestamp without time zone,
    owner_id integer
);

CREATE TABLE public.reaction_miriam (
    reaction_id integer NOT NULL,
    miriam_id integer NOT NULL
);

CREATE SEQUENCE public.reaction_node_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.reaction_node_table (
    node_type_db character varying(31) NOT NULL,
    id integer DEFAULT nextval('public.reaction_node_sequence') NOT NULL,
    node_operator_for_input_id integer,
    node_operator_for_output_id integer,
    line_id integer,
    reaction_id integer,
    element_id integer,
    stoichiometry numeric(9,2)
);

CREATE SEQUENCE public.reaction_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.reaction_table (
    reaction_type_db character varying(31) NOT NULL,
    id integer DEFAULT nextval('public.reaction_sequence') NOT NULL,
    id_reaction character varying(255),
    name character varying(255),
    notes text,
    reversible boolean NOT NULL,
    model_id integer,
    symbol character varying(255),
    abbreviation character varying(255),
    formula character varying(255),
    subsystem character varying(255),
    gene_protein_reaction character varying(255),
    mechanical_confidence_score integer,
    upper_bound double precision,
    lower_bound double precision,
    visibility_level character varying(255),
    kinetics_id integer,
    z integer NOT NULL,
    line_id integer NOT NULL
);

CREATE TABLE public.reaction_synonyms (
    id integer NOT NULL,
    idx integer NOT NULL,
    synonym character varying(255)
);

CREATE SEQUENCE public.reference_genome_gene_mapping_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.reference_genome_gene_mapping_table (
    id integer DEFAULT nextval('public.reference_genome_gene_mapping_sequence') NOT NULL,
    reference_genome_id integer NOT NULL,
    name character varying(255) NOT NULL,
    source_url text NOT NULL,
    download_progress double precision
);

CREATE SEQUENCE public.reference_genome_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.reference_genome_table (
    id integer DEFAULT nextval('public.reference_genome_sequence') NOT NULL,
    organism_id integer NOT NULL,
    type character varying(255) NOT NULL,
    version character varying(255) NOT NULL,
    source_url text,
    download_progress double precision
);

CREATE TABLE public.sbml_function_arguments (
    idx integer NOT NULL,
    sbml_function_id integer NOT NULL,
    argument_name character varying(255)
);

CREATE SEQUENCE public.sbml_function_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.sbml_function_table (
    id integer DEFAULT nextval('public.sbml_function_sequence') NOT NULL,
    function_id character varying(255),
    name character varying(255),
    definition text,
    model_id integer NOT NULL
);

CREATE SEQUENCE public.sbml_kinetics_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.sbml_kinetics_table (
    id integer DEFAULT nextval('public.sbml_function_sequence') NOT NULL,
    definition text
);

CREATE SEQUENCE public.sbml_parameter_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.sbml_parameter_table (
    id integer DEFAULT nextval('public.sbml_parameter_sequence') NOT NULL,
    parameter_id character varying(255),
    name character varying(255),
    value numeric,
    units_id integer
);

CREATE SEQUENCE public.sbml_unit_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.sbml_unit_table (
    id integer DEFAULT nextval('public.sbml_unit_sequence') NOT NULL,
    unit_id character varying(255),
    name character varying(255),
    model_id integer NOT NULL
);

CREATE SEQUENCE public.sbml_unit_type_factor_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.sbml_unit_type_factor_table (
    id integer DEFAULT nextval('public.sbml_unit_type_factor_sequence') NOT NULL,
    unit_id integer NOT NULL,
    exponent integer DEFAULT 1 NOT NULL,
    scale integer DEFAULT 0 NOT NULL,
    multiplier numeric DEFAULT 1.0 NOT NULL,
    unit_type character varying(255)
);

CREATE SEQUENCE public.search_history_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.search_history_table (
    id integer DEFAULT nextval('public.search_history_sequence') NOT NULL,
    query character varying(1024),
    map character varying(255),
    timestamp timestamp without time zone,
    ip_address character varying(255),
    type character varying(31)
);

CREATE SEQUENCE public.search_index_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.search_index_table (
    id integer DEFAULT nextval('public.search_index_sequence') NOT NULL,
    value character varying(255),
    weight integer,
    source_id integer
);

CREATE SEQUENCE public.structural_state_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.structural_state_table (
    id integer DEFAULT nextval('public.structural_state_sequence') NOT NULL,
    value character varying(255) NOT NULL,
    position character varying(255) NOT NULL,
    width numeric(6,2) NOT NULL,
    height numeric(6,2) NOT NULL,
    font_size numeric(6,2) NOT NULL
);

CREATE SEQUENCE public.structure_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.structure_table (
    id integer DEFAULT nextval('public.structure_sequence') NOT NULL,
    uniprot_id integer NOT NULL,
    pdb_id character varying(255) NOT NULL,
    chain_id character varying(255) NOT NULL,
    struct_start integer,
    struct_end integer,
    unp_start integer,
    unp_end integer,
    experimental_method character varying(255),
    coverage double precision,
    resolution double precision,
    tax_id integer
);

CREATE SEQUENCE public.submodel_connection_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.submodel_connection_table (
    submodel_connections_type_db character varying(31) NOT NULL,
    id integer DEFAULT nextval('public.submodel_connection_sequence') NOT NULL,
    name character varying(255),
    submodel_id integer,
    parent_model_id integer,
    from_element_id integer,
    to_element_id integer,
    type character varying(31)
);

CREATE SEQUENCE public.uniprot_record_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.uniprot_record_table (
    id integer DEFAULT nextval('public.uniprot_record_sequence') NOT NULL,
    element_id integer NOT NULL,
    uniprot_id character varying(255) NOT NULL
);

CREATE SEQUENCE public.user_annotation_schema_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.user_annotation_schema_table (
    id integer DEFAULT nextval('public.user_annotation_schema_sequence') NOT NULL,
    user_id integer,
    validate_miriam_types boolean,
    network_layout_as_default boolean DEFAULT false,
    sbgn_format boolean DEFAULT false,
    annotate_model boolean DEFAULT false,
    auto_resize_map boolean DEFAULT true,
    cache_data boolean DEFAULT true,
    semantic_zooming boolean DEFAULT false,
    semantic_zoom_contains_multiple_overlays boolean DEFAULT false
);

CREATE SEQUENCE public.user_class_annotators_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.user_class_annotators_table (
    id integer DEFAULT nextval('public.user_class_annotators_sequence') NOT NULL,
    annotation_schema_id integer,
    class_name character varying(255)
);

CREATE SEQUENCE public.user_class_required_annotations_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.user_class_required_annotations_table (
    id integer DEFAULT nextval('public.user_class_required_annotations_sequence') NOT NULL,
    annotation_schema_id integer,
    require_at_least_one_annotation boolean,
    class_name character varying(255)
);

CREATE SEQUENCE public.user_class_valid_annotations_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.user_class_valid_annotations_table (
    id integer DEFAULT nextval('public.user_class_valid_annotations_sequence') NOT NULL,
    annotation_schema_id integer,
    class_name character varying(255)
);

CREATE SEQUENCE public.user_gui_preference_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.user_gui_preference_table (
    id integer DEFAULT nextval('public.user_gui_preference_sequence') NOT NULL,
    key character varying(255) NOT NULL,
    value character varying(255) NOT NULL,
    annotation_schema_id integer NOT NULL
);

CREATE TABLE public.user_privilege_map_table (
    user_id integer NOT NULL,
    privilege_id integer NOT NULL
);

CREATE SEQUENCE public.user_sequence
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.user_table (
    id integer DEFAULT nextval('public.user_sequence') NOT NULL,
    crypted_password character varying(255) NOT NULL,
    email character varying(255),
    login character varying(255),
    name character varying(255),
    surname character varying(255),
    removed boolean DEFAULT false,
    annotation_schema_id integer,
    max_color VARBINARY(1G),
    min_color VARBINARY(1G),
    simple_color VARBINARY(1G),
    terms_of_use_consent boolean DEFAULT false,
    neutral_color VARBINARY(1G),
    connected_to_ldap boolean DEFAULT false
);

CREATE TABLE public.user_terms_of_use_consent (
    user_id integer NOT NULL,
    date timestamp without time zone
);

INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (0, 28, 'lcsb.mapviewer.reactome.utils.ReactomeConnector');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (2, 28, 'lcsb.mapviewer.annotation.services.ChEMBLParser');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (6, -1, 'lcsb.mapviewer.annotation.cache.MockCacheInterface');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (9, 365, 'lcsb.mapviewer.annotation.services.MiriamConnector');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (10, 365, 'lcsb.mapviewer.annotation.services.TaxonomyBackend');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (7, 28, 'lcsb.mapviewer.annotation.services.annotators.ChebiAnnotator');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (8, 28, 'lcsb.mapviewer.annotation.services.annotators.GoAnnotator');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (11, 28, 'lcsb.mapviewer.annotation.services.annotators.UniprotAnnotator');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (12, 28, 'lcsb.mapviewer.annotation.services.annotators.ReconAnnotator');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (13, 365, 'lcsb.mapviewer.annotation.services.annotators.HgncAnnotator');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (14, 365, 'lcsb.mapviewer.annotation.services.annotators.EntrezAnnotator');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (15, 365, 'lcsb.mapviewer.annotation.services.annotators.EnsemblAnnotator');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (17, 28, 'lcsb.mapviewer.annotation.services.MeSHParser');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (18, 28, 'lcsb.mapviewer.annotation.services.MiRNAParser');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (3, 365, 'lcsb.mapviewer.annotation.services.PubmedParser');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (19, 28, 'lcsb.mapviewer.annotation.services.genome.UcscReferenceGenomeConnector');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (21, 365, 'lcsb.mapviewer.annotation.services.annotators.TairAnnotator');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (22, 365, 'lcsb.mapviewer.annotation.services.annotators.CazyAnnotator');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (23, 365, 'lcsb.mapviewer.annotation.services.annotators.BrendaAnnotator');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (24, 365, 'lcsb.mapviewer.annotation.services.annotators.StitchAnnotator');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (25, 365, 'lcsb.mapviewer.annotation.services.annotators.StringAnnotator');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (26, 365, 'lcsb.mapviewer.annotation.services.annotators.PdbAnnotator');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (27, 365, 'lcsb.mapviewer.annotation.services.annotators.KeggAnnotator');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (28, 28, 'lcsb.mapviewer.annotation.services.dapi.DapiConnectorImpl');
INSERT INTO public.cache_type_table (id, validity, class_name) VALUES (4, 28, 'lcsb.mapviewer.annotation.services.dapi.DrugBankParser');

INSERT INTO public.data_overlay_image_layer_table (id, data_overlay_id, model_id, directory) VALUES (1, 5, 3, 'normal');

INSERT INTO public.layout_table (id, title, creator_id, public_layout, progress, description, hierarchical_view, file_entry_id, hierarchy_view_level, google_license_consent, default_overlay, order_index, status, project_id, color_schema_type) VALUES (5, 'Normal', NULL, true, 0, NULL, false, NULL, NULL, false, false, 1, 'UNKNOWN', 4, 'GENERIC');

INSERT INTO public.model_data_table (id, height, notes, tile_size, width, zoom_levels, project_id, id_model, name, default_zoom_level, default_centerx, default_centery, creation_date) VALUES (3, 14285, NULL, 256, 25195, 7, 4, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO public.privilege_table (id, object_id, type) VALUES (169, NULL, 'IS_ADMIN');
INSERT INTO public.privilege_table (id, object_id, type) VALUES (170, NULL, 'IS_CURATOR');
INSERT INTO public.privilege_table (id, object_id, type) VALUES (171, NULL, 'CAN_CREATE_OVERLAYS');
INSERT INTO public.privilege_table (id, object_id, type) VALUES (172, 'empty', 'READ_PROJECT');
INSERT INTO public.privilege_table (id, object_id, type) VALUES (173, 'empty', 'WRITE_PROJECT');
INSERT INTO public.privilege_table (id, object_id, type) VALUES (174, '*', 'WRITE_PROJECT');
INSERT INTO public.privilege_table (id, object_id, type) VALUES (175, '*', 'READ_PROJECT');

INSERT INTO public.project_table (id, project_id, progress, directory, name, disease_id, organism_id, sbgn_format, version, file_entry_id, notify_email, status, google_map_license_accept_date, map_canvas_type, creation_date, owner_id) VALUES (4, 'empty', 100, 'a2e4822a98337283e39f7b60acf85ec9', NULL, NULL, NULL, false, 'empty DISEASE MAP', NULL, NULL, 'DONE', NULL, 'OPEN_LAYERS', '2014-03-27 15:59:31.321', 1);

INSERT INTO public.user_privilege_map_table (user_id, privilege_id) VALUES (1, 172);
INSERT INTO public.user_privilege_map_table (user_id, privilege_id) VALUES (3, 172);
INSERT INTO public.user_privilege_map_table (user_id, privilege_id) VALUES (1, 173);
INSERT INTO public.user_privilege_map_table (user_id, privilege_id) VALUES (1, 169);
INSERT INTO public.user_privilege_map_table (user_id, privilege_id) VALUES (1, 170);

INSERT INTO public.user_table (id, crypted_password, email, login, name, surname, removed, annotation_schema_id, max_color, min_color, simple_color, terms_of_use_consent, neutral_color, connected_to_ldap) VALUES (3, '$2a$10$gyuDgRZ1wwuFe67DHnkMruhOCKJp1KBzQLeQSJ57WAehJXmhkreRi', NULL, 'anonymous', NULL, NULL, false, NULL, NULL, NULL, NULL, false, NULL, false);
INSERT INTO public.user_table (id, crypted_password, email, login, name, surname, removed, annotation_schema_id, max_color, min_color, simple_color, terms_of_use_consent, neutral_color, connected_to_ldap) VALUES (1, '$2a$10$AGem45sdWGs2xJ1sU.J3eOcaLwvLIIm3.D9DbRY2T0vUpyouNw8iW', NULL, 'admin', NULL, NULL, false, NULL, NULL, NULL, NULL, false, NULL, false);

ALTER TABLE public.element_table
    ADD CONSTRAINT alias_table_pkey PRIMARY KEY (id);

ALTER TABLE public.annotator_data_table
    ADD CONSTRAINT annotator_data_table_pk PRIMARY KEY (id);

ALTER TABLE public.annotator_parameter_table
    ADD CONSTRAINT annotators_params_pkey PRIMARY KEY (id);

ALTER TABLE public.author_table
    ADD CONSTRAINT author_table_pkey PRIMARY KEY (id);

ALTER TABLE public.file_entry_table
    ADD CONSTRAINT big_file_entry_pkey PRIMARY KEY (id);

ALTER TABLE public.cache_type_table
    ADD CONSTRAINT cache_type_pkey PRIMARY KEY (id);

ALTER TABLE public.cache_query_table
    ADD CONSTRAINT cachequery_pkey PRIMARY KEY (id);

ALTER TABLE public.user_class_annotators_table
    ADD CONSTRAINT class_annotator_pkey PRIMARY KEY (id);

ALTER TABLE public.user_class_required_annotations_table
    ADD CONSTRAINT class_required_annotation_pkey PRIMARY KEY (id);

ALTER TABLE public.user_class_valid_annotations_table
    ADD CONSTRAINT class_valid_annotation_pkey PRIMARY KEY (id);

ALTER TABLE public.configuration_option_table
    ADD CONSTRAINT configuration_table_pkey PRIMARY KEY (id);

ALTER TABLE public.data_overlay_image_layer_table
    ADD CONSTRAINT data_overlay_image_layer_pkey PRIMARY KEY (id);

ALTER TABLE public.element_former_symbols
    ADD CONSTRAINT element_former_symbols_pk PRIMARY KEY (id, idx);

ALTER TABLE public.element_miriam
    ADD CONSTRAINT element_miriam_pkey PRIMARY KEY (element_id, miriam_id);

ALTER TABLE public.element_synonyms
    ADD CONSTRAINT element_synonyms_pk PRIMARY KEY (id, idx);

ALTER TABLE public.comment_table
    ADD CONSTRAINT feedback_pkey PRIMARY KEY (id);

ALTER TABLE public.glyph_table
    ADD CONSTRAINT glyph_table_pk PRIMARY KEY (id);

ALTER TABLE public.kinetic_law_elements
    ADD CONSTRAINT kinetic_law_elements_unique UNIQUE (kinetic_law_id, element_id);

ALTER TABLE public.kinetic_law_functions
    ADD CONSTRAINT kinetic_law_functions_unique UNIQUE (kinetic_law_id, function_id);

ALTER TABLE public.kinetic_law_parameters
    ADD CONSTRAINT kinetic_law_parameters_unique UNIQUE (kinetic_law_id, parameter_id);

ALTER TABLE public.layer_table_lines
    ADD CONSTRAINT layer_table_layerline_lines_iddb_key UNIQUE (lines_id);

ALTER TABLE public.layer_table_lines
    ADD CONSTRAINT layer_table_layerline_pkey PRIMARY KEY (layer_table_id, idx);

ALTER TABLE public.layer_table_ovals
    ADD CONSTRAINT layer_table_layeroval_ovals_iddb_key UNIQUE (ovals_id);

ALTER TABLE public.layer_table_ovals
    ADD CONSTRAINT layer_table_layeroval_pkey PRIMARY KEY (layer_table_id, idx);

ALTER TABLE public.layer_table_rectangles
    ADD CONSTRAINT layer_table_layerrect_pkey PRIMARY KEY (layer_table_id, idx);

ALTER TABLE public.layer_table_rectangles
    ADD CONSTRAINT layer_table_layerrect_rectangles_iddb_key UNIQUE (rectangles_id);

ALTER TABLE public.layer_table_texts
    ADD CONSTRAINT layer_table_layertext_pkey PRIMARY KEY (layer_table_id, idx);

ALTER TABLE public.layer_table_texts
    ADD CONSTRAINT layer_table_layertext_texts_iddb_key UNIQUE (texts_id);

ALTER TABLE public.layer_table
    ADD CONSTRAINT layer_table_pkey PRIMARY KEY (id);

ALTER TABLE public.layer_oval_table
    ADD CONSTRAINT layeroval_pkey PRIMARY KEY (id);

ALTER TABLE public.layer_rect_table
    ADD CONSTRAINT layerrect_pkey PRIMARY KEY (id);

ALTER TABLE public.layer_text_table
    ADD CONSTRAINT layertext_pkey PRIMARY KEY (id);

ALTER TABLE public.layout_table
    ADD CONSTRAINT layout_pkey PRIMARY KEY (id);

ALTER TABLE public.miriam_data_table
    ADD CONSTRAINT miriam_data_table_pkey PRIMARY KEY (id);

ALTER TABLE public.model_data_table_authors
    ADD CONSTRAINT model_data_table_authors_pkey PRIMARY KEY (model_data_id, idx);

ALTER TABLE public.model_parameters_table
    ADD CONSTRAINT model_parameters_unique UNIQUE (model_id, parameter_id);

ALTER TABLE public.model_data_table
    ADD CONSTRAINT model_table_pkey PRIMARY KEY (id);

ALTER TABLE public.modification_residue_table
    ADD CONSTRAINT modification_residue_pkey PRIMARY KEY (id);

ALTER TABLE public.reaction_node_table
    ADD CONSTRAINT node_table_pkey PRIMARY KEY (id);

ALTER TABLE public.overview_image_table
    ADD CONSTRAINT overview_image_pkey PRIMARY KEY (id);

ALTER TABLE public.overview_link_table
    ADD CONSTRAINT overview_link_pkey PRIMARY KEY (id);

ALTER TABLE public.plugin_data_entry_table
    ADD CONSTRAINT plugin_data_entry_pkey PRIMARY KEY (id);

ALTER TABLE public.plugin_table
    ADD CONSTRAINT plugin_hash_unique UNIQUE (hash);

ALTER TABLE public.plugin_table
    ADD CONSTRAINT plugin_pkey PRIMARY KEY (id);

ALTER TABLE public.plugin_urls
    ADD CONSTRAINT plugin_urls_pkey PRIMARY KEY (plugin_id, url);

ALTER TABLE public.point_table
    ADD CONSTRAINT point_table_pkey PRIMARY KEY (id, idx);

ALTER TABLE public.polyline_data_table
    ADD CONSTRAINT polylinedata_pkey PRIMARY KEY (id);

ALTER TABLE public.privilege_table
    ADD CONSTRAINT privilege_table_pkey PRIMARY KEY (id);

ALTER TABLE public.project_log_entry_table
    ADD CONSTRAINT project_log_entry_pk PRIMARY KEY (id);

ALTER TABLE public.project_table
    ADD CONSTRAINT project_table_pkey PRIMARY KEY (id);

ALTER TABLE public.reaction_miriam
    ADD CONSTRAINT reaction_miriam_pkey PRIMARY KEY (reaction_id, miriam_id);

ALTER TABLE public.reaction_synonyms
    ADD CONSTRAINT reaction_synonyms_pkey PRIMARY KEY (id, idx);

ALTER TABLE public.reaction_table
    ADD CONSTRAINT reaction_table_pkey PRIMARY KEY (id);

ALTER TABLE public.reference_genome_gene_mapping_table
    ADD CONSTRAINT reference_genome_gene_mapping_pkey PRIMARY KEY (id);

ALTER TABLE public.reference_genome_gene_mapping_table
    ADD CONSTRAINT reference_genome_gene_mapping_unique_name UNIQUE (reference_genome_id, name);

ALTER TABLE public.reference_genome_table
    ADD CONSTRAINT reference_genome_pkey PRIMARY KEY (id);

ALTER TABLE public.sbml_function_table
    ADD CONSTRAINT sbml_function_pk PRIMARY KEY (id);

ALTER TABLE public.sbml_kinetics_table
    ADD CONSTRAINT sbml_kinetics_pk PRIMARY KEY (id);

ALTER TABLE public.sbml_parameter_table
    ADD CONSTRAINT sbml_parameter_pk PRIMARY KEY (id);

ALTER TABLE public.sbml_unit_type_factor_table
    ADD CONSTRAINT sbml_unit_factor_pk PRIMARY KEY (id);

ALTER TABLE public.sbml_unit_table
    ADD CONSTRAINT sbml_unit_pk PRIMARY KEY (id);

ALTER TABLE public.search_index_table
    ADD CONSTRAINT search_index_table_pkey PRIMARY KEY (id);

ALTER TABLE public.structural_state_table
    ADD CONSTRAINT structural_state_pk PRIMARY KEY (id);

ALTER TABLE public.structure_table
    ADD CONSTRAINT structure_pkey PRIMARY KEY (id);

ALTER TABLE public.submodel_connection_table
    ADD CONSTRAINT submodel_connection_table_pkey PRIMARY KEY (id);

ALTER TABLE public.uniprot_record_table
    ADD CONSTRAINT uniprot_pkey PRIMARY KEY (id);

ALTER TABLE public.project_table
    ADD CONSTRAINT unique_project_id UNIQUE (project_id);

ALTER TABLE public.privilege_table
    ADD CONSTRAINT unique_rows UNIQUE (type, object_id);

ALTER TABLE public.user_table
    ADD CONSTRAINT unique_user_login UNIQUE (login);

ALTER TABLE public.user_annotation_schema_table
    ADD CONSTRAINT user_annotation_schema_pkey PRIMARY KEY (id);

ALTER TABLE public.user_gui_preference_table
    ADD CONSTRAINT user_gui_preferences_key_unique UNIQUE (key, annotation_schema_id);

ALTER TABLE public.user_privilege_map_table
    ADD CONSTRAINT user_privilege_map_table_pkey PRIMARY KEY (user_id, privilege_id);

ALTER TABLE public.user_table
    ADD CONSTRAINT user_table_pkey PRIMARY KEY (id);

CREATE INDEX query_index ON public.cache_query_table(query);

ALTER TABLE public.annotator_data_table
    ADD CONSTRAINT annotator_data_table_user_class_annotators_fk FOREIGN KEY (user_class_annotators_id) REFERENCES public.user_class_annotators_table(id);

ALTER TABLE public.annotator_parameter_table
    ADD CONSTRAINT annotator_parameter_table_annotator_data_fk FOREIGN KEY (annotator_data_id) REFERENCES public.annotator_data_table(id);

ALTER TABLE public.cache_query_table
    ADD CONSTRAINT cachequery_type_fk FOREIGN KEY (type) REFERENCES public.cache_type_table(id);

ALTER TABLE public.user_class_annotators_table
    ADD CONSTRAINT class_annotator_user_annotation_schema_fk FOREIGN KEY (annotation_schema_id) REFERENCES public.user_annotation_schema_table(id);

ALTER TABLE public.class_required_annotation_miriam_type_table
    ADD CONSTRAINT class_required_annotation_miriam_type_fk FOREIGN KEY (class_required_annotation_id) REFERENCES public.user_class_required_annotations_table(id);

ALTER TABLE public.user_class_required_annotations_table
    ADD CONSTRAINT class_required_annotation_user_annotation_schema_fk FOREIGN KEY (annotation_schema_id) REFERENCES public.user_annotation_schema_table(id);

ALTER TABLE public.class_valid_annotation_miriam_type_table
    ADD CONSTRAINT class_valid_annotation_miriam_type_fk FOREIGN KEY (class_valid_annotation_id) REFERENCES public.user_class_valid_annotations_table(id);

ALTER TABLE public.user_class_valid_annotations_table
    ADD CONSTRAINT class_valid_annotation_user_annotation_schema_fk FOREIGN KEY (annotation_schema_id) REFERENCES public.user_annotation_schema_table(id);

ALTER TABLE public.data_overlay_image_layer_table
    ADD CONSTRAINT data_overlay_image_layer__data_overlay_fkey FOREIGN KEY (model_id) REFERENCES public.model_data_table(id);

ALTER TABLE public.data_overlay_image_layer_table
    ADD CONSTRAINT data_overlay_image_layer__layout_fkey FOREIGN KEY (data_overlay_id) REFERENCES public.layout_table(id);

ALTER TABLE public.element_former_symbols
    ADD CONSTRAINT element_former_symbols_fk FOREIGN KEY (id) REFERENCES public.element_table(id);

ALTER TABLE public.element_miriam
    ADD CONSTRAINT element_miriam_element_fk FOREIGN KEY (element_id) REFERENCES public.element_table(id);

ALTER TABLE public.element_miriam
    ADD CONSTRAINT element_miriam_miriam_fk FOREIGN KEY (miriam_id) REFERENCES public.miriam_data_table(id);

ALTER TABLE public.element_synonyms
    ADD CONSTRAINT element_synonyms_fk FOREIGN KEY (id) REFERENCES public.element_table(id);

ALTER TABLE public.element_table
    ADD CONSTRAINT element_table_glyph_fk FOREIGN KEY (glyph_id) REFERENCES public.glyph_table(id);

ALTER TABLE public.element_table
    ADD CONSTRAINT element_table_structural_state_fk FOREIGN KEY (structural_state_id) REFERENCES public.structural_state_table(id);

ALTER TABLE public.element_table
    ADD CONSTRAINT element_table_substance_unit_comlex_type_id_fkey FOREIGN KEY (substance_unit_comlex_type_id) REFERENCES public.sbml_unit_table(id);

ALTER TABLE public.file_entry_table
    ADD CONSTRAINT file_entry_owner_constraint FOREIGN KEY (owner_id) REFERENCES public.user_table(id);

ALTER TABLE public.element_table
    ADD CONSTRAINT fk1220391f28cbc89c FOREIGN KEY (idcomplexdb) REFERENCES public.element_table(id);

ALTER TABLE public.element_table
    ADD CONSTRAINT fk1220391f746527fb FOREIGN KEY (compartment_id) REFERENCES public.element_table(id);

ALTER TABLE public.element_table
    ADD CONSTRAINT fk1220391fd07dda99 FOREIGN KEY (model_id) REFERENCES public.model_data_table(id);

ALTER TABLE public.search_index_table
    ADD CONSTRAINT fk19f9ea0a4f8f9d0a FOREIGN KEY (source_id) REFERENCES public.element_table(id);

ALTER TABLE public.reaction_table
    ADD CONSTRAINT fk1ccb7638d07dda99 FOREIGN KEY (model_id) REFERENCES public.model_data_table(id);

ALTER TABLE public.point_table
    ADD CONSTRAINT fk23b8885f4910e2f9 FOREIGN KEY (id) REFERENCES public.polyline_data_table(id);

ALTER TABLE public.model_data_table
    ADD CONSTRAINT fk3197d738b8043c6b FOREIGN KEY (project_id) REFERENCES public.project_table(id);

ALTER TABLE public.reaction_node_table
    ADD CONSTRAINT fk5cb2435132fc04bd FOREIGN KEY (node_operator_for_input_id) REFERENCES public.reaction_node_table(id);

ALTER TABLE public.reaction_node_table
    ADD CONSTRAINT fk5cb243516a806500 FOREIGN KEY (node_operator_for_output_id) REFERENCES public.reaction_node_table(id);

ALTER TABLE public.reaction_node_table
    ADD CONSTRAINT fk5cb2435185f7f0ff FOREIGN KEY (element_id) REFERENCES public.element_table(id);

ALTER TABLE public.reaction_node_table
    ADD CONSTRAINT fk5cb24351ddccc464 FOREIGN KEY (line_id) REFERENCES public.polyline_data_table(id);

ALTER TABLE public.reaction_node_table
    ADD CONSTRAINT fk5cb24351f8b3fb7e FOREIGN KEY (reaction_id) REFERENCES public.reaction_table(id);

ALTER TABLE public.reaction_miriam
    ADD CONSTRAINT fk71295b151586e740 FOREIGN KEY (reaction_id) REFERENCES public.reaction_table(id);

ALTER TABLE public.reaction_miriam
    ADD CONSTRAINT fk71295b1567cc9103 FOREIGN KEY (miriam_id) REFERENCES public.miriam_data_table(id);

ALTER TABLE public.element_table
    ADD CONSTRAINT fk_alias_submodel FOREIGN KEY (submodel_id) REFERENCES public.submodel_connection_table(id);

ALTER TABLE public.submodel_connection_table
    ADD CONSTRAINT fk_parent_model_connection_model FOREIGN KEY (parent_model_id) REFERENCES public.model_data_table(id);

ALTER TABLE public.reaction_table
    ADD CONSTRAINT fk_reaction_line FOREIGN KEY (line_id) REFERENCES public.polyline_data_table(id);

ALTER TABLE public.submodel_connection_table
    ADD CONSTRAINT fk_submodel_connection_from_alias FOREIGN KEY (from_element_id) REFERENCES public.element_table(id);

ALTER TABLE public.submodel_connection_table
    ADD CONSTRAINT fk_submodel_connection_model FOREIGN KEY (submodel_id) REFERENCES public.model_data_table(id);

ALTER TABLE public.submodel_connection_table
    ADD CONSTRAINT fk_submodel_connection_to_alias FOREIGN KEY (to_element_id) REFERENCES public.element_table(id);

ALTER TABLE public.layer_table
    ADD CONSTRAINT fka7b17360d07dda99 FOREIGN KEY (model_id) REFERENCES public.model_data_table(id);

ALTER TABLE public.layer_table_lines
    ADD CONSTRAINT fkcbfabea647d24b0b FOREIGN KEY (layer_table_id) REFERENCES public.layer_table(id);

ALTER TABLE public.layer_table_lines
    ADD CONSTRAINT fkcbfabea6573d6d60 FOREIGN KEY (lines_id) REFERENCES public.polyline_data_table(id);

ALTER TABLE public.layer_table_ovals
    ADD CONSTRAINT fkcbfc4b0447d24b0b FOREIGN KEY (layer_table_id) REFERENCES public.layer_table(id);

ALTER TABLE public.layer_table_ovals
    ADD CONSTRAINT fkcbfc4b04e771479c FOREIGN KEY (ovals_id) REFERENCES public.layer_oval_table(id);

ALTER TABLE public.layer_table_rectangles
    ADD CONSTRAINT fkcbfd689647d24b0b FOREIGN KEY (layer_table_id) REFERENCES public.layer_table(id);

ALTER TABLE public.layer_table_rectangles
    ADD CONSTRAINT fkcbfd68968b8fcc8b FOREIGN KEY (rectangles_id) REFERENCES public.layer_rect_table(id);

ALTER TABLE public.layer_table_texts
    ADD CONSTRAINT fkcbfe53df47d24b0b FOREIGN KEY (layer_table_id) REFERENCES public.layer_table(id);

ALTER TABLE public.layer_table_texts
    ADD CONSTRAINT fkcbfe53dfd0c40912 FOREIGN KEY (texts_id) REFERENCES public.layer_text_table(id);

ALTER TABLE public.comment_table
    ADD CONSTRAINT fkf8704fa5839f2b6e FOREIGN KEY (user_id) REFERENCES public.user_table(id);

ALTER TABLE public.comment_table
    ADD CONSTRAINT fkf8704fa5d07dda99 FOREIGN KEY (model_id) REFERENCES public.model_data_table(id);

ALTER TABLE public.glyph_table
    ADD CONSTRAINT glyph_table_file_entry_fk FOREIGN KEY (file_entry_id) REFERENCES public.file_entry_table(id);

ALTER TABLE public.glyph_table
    ADD CONSTRAINT glyph_table_project FOREIGN KEY (project_id) REFERENCES public.project_table(id);

ALTER TABLE public.kinetic_law_elements
    ADD CONSTRAINT kinetic_law_elements_element_fk FOREIGN KEY (element_id) REFERENCES public.element_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.kinetic_law_elements
    ADD CONSTRAINT kinetic_law_elements_sbml_kinetics_fk FOREIGN KEY (kinetic_law_id) REFERENCES public.sbml_kinetics_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.kinetic_law_functions
    ADD CONSTRAINT kinetic_law_functions_function_fk FOREIGN KEY (function_id) REFERENCES public.sbml_function_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.kinetic_law_functions
    ADD CONSTRAINT kinetic_law_functions_sbml_kinetics_fk FOREIGN KEY (kinetic_law_id) REFERENCES public.sbml_kinetics_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.kinetic_law_parameters
    ADD CONSTRAINT kinetic_law_parameters_parameter_fk FOREIGN KEY (parameter_id) REFERENCES public.sbml_parameter_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.kinetic_law_parameters
    ADD CONSTRAINT kinetic_law_parameters_sbml_kinetics_fk FOREIGN KEY (kinetic_law_id) REFERENCES public.sbml_kinetics_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.layer_text_table
    ADD CONSTRAINT layer_text_table_glyph_fk FOREIGN KEY (glyph_id) REFERENCES public.glyph_table(id);

ALTER TABLE public.layout_table
    ADD CONSTRAINT layout_file_constraint FOREIGN KEY (file_entry_id) REFERENCES public.file_entry_table(id);

ALTER TABLE public.layout_table
    ADD CONSTRAINT layout_table_project FOREIGN KEY (project_id) REFERENCES public.project_table(id);

ALTER TABLE public.model_data_modification_dates
    ADD CONSTRAINT model_data_modification_dates_model_fk FOREIGN KEY (model_data_id) REFERENCES public.model_data_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.model_data_table_authors
    ADD CONSTRAINT model_data_table_authors_author_fk FOREIGN KEY (author_id) REFERENCES public.author_table(id);

ALTER TABLE public.model_data_table_authors
    ADD CONSTRAINT model_data_table_authors_model_fk FOREIGN KEY (model_data_id) REFERENCES public.model_data_table(id);

ALTER TABLE public.model_miriam_data_table
    ADD CONSTRAINT model_miriam_data_table_miriam_data_fk FOREIGN KEY (miriam_data_id) REFERENCES public.miriam_data_table(id);

ALTER TABLE public.model_miriam_data_table
    ADD CONSTRAINT model_miriam_data_table_modle_fk FOREIGN KEY (model_id) REFERENCES public.model_data_table(id);

ALTER TABLE public.model_parameters_table
    ADD CONSTRAINT model_parameters_model_fk FOREIGN KEY (model_id) REFERENCES public.model_data_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.model_parameters_table
    ADD CONSTRAINT model_parameters_parameter_fk FOREIGN KEY (parameter_id) REFERENCES public.sbml_parameter_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.modification_residue_table
    ADD CONSTRAINT modification_residue_species_fk FOREIGN KEY (id_species_db) REFERENCES public.element_table(id);

ALTER TABLE public.overview_link_table
    ADD CONSTRAINT overview_image_model_fk FOREIGN KEY (linked_model_id) REFERENCES public.model_data_table(id);

ALTER TABLE public.overview_link_table
    ADD CONSTRAINT overview_link_linked_overview_image_fk FOREIGN KEY (linked_overview_image_id) REFERENCES public.overview_image_table(id);

ALTER TABLE public.overview_link_table
    ADD CONSTRAINT overview_link_overview_image_fk FOREIGN KEY (overview_image_id) REFERENCES public.overview_image_table(id);

ALTER TABLE public.plugin_data_entry_table
    ADD CONSTRAINT plugin_data_entry_plugin_fk FOREIGN KEY (plugin_id) REFERENCES public.plugin_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.plugin_data_entry_table
    ADD CONSTRAINT plugin_data_entry_user_fk FOREIGN KEY (user_id) REFERENCES public.user_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.plugin_urls
    ADD CONSTRAINT plugin_urls_fk FOREIGN KEY (plugin_id) REFERENCES public.plugin_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.project_table
    ADD CONSTRAINT project_file_constraint FOREIGN KEY (file_entry_id) REFERENCES public.file_entry_table(id);

ALTER TABLE public.project_log_entry_table
    ADD CONSTRAINT project_log_entry_project_fk FOREIGN KEY (project_id) REFERENCES public.project_table(id);

ALTER TABLE public.project_table
    ADD CONSTRAINT project_organism_constraint FOREIGN KEY (organism_id) REFERENCES public.miriam_data_table(id);

ALTER TABLE public.project_table
    ADD CONSTRAINT project_table_miriam FOREIGN KEY (disease_id) REFERENCES public.miriam_data_table(id);

ALTER TABLE public.reaction_synonyms
    ADD CONSTRAINT reaction_synonyms_fkey FOREIGN KEY (id) REFERENCES public.reaction_table(id);

ALTER TABLE public.reaction_table
    ADD CONSTRAINT reaction_table_kinetics_iddb_fkey FOREIGN KEY (kinetics_id) REFERENCES public.sbml_kinetics_table(id);

ALTER TABLE public.reference_genome_gene_mapping_table
    ADD CONSTRAINT reference_genome_gene_mapping_reference_genome_fk FOREIGN KEY (reference_genome_id) REFERENCES public.reference_genome_table(id);

ALTER TABLE public.reference_genome_table
    ADD CONSTRAINT reference_genome_organism_fk FOREIGN KEY (organism_id) REFERENCES public.miriam_data_table(id);

ALTER TABLE public.sbml_function_arguments
    ADD CONSTRAINT sbml_function_arguments_sbml_function_fk FOREIGN KEY (sbml_function_id) REFERENCES public.sbml_function_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.sbml_function_table
    ADD CONSTRAINT sbml_function_model_fk FOREIGN KEY (model_id) REFERENCES public.model_data_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.sbml_parameter_table
    ADD CONSTRAINT sbml_parameter_units_fk FOREIGN KEY (units_id) REFERENCES public.sbml_unit_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.sbml_unit_type_factor_table
    ADD CONSTRAINT sbml_unit_factor_unit_fk FOREIGN KEY (unit_id) REFERENCES public.sbml_unit_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.sbml_unit_table
    ADD CONSTRAINT sbml_unit_model_fk FOREIGN KEY (model_id) REFERENCES public.model_data_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.structure_table
    ADD CONSTRAINT structure_uniprot_fk FOREIGN KEY (uniprot_id) REFERENCES public.uniprot_record_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.uniprot_record_table
    ADD CONSTRAINT uniprot_element_fk FOREIGN KEY (element_id) REFERENCES public.element_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.user_annotation_schema_table
    ADD CONSTRAINT user_annotation_schema_user_fk FOREIGN KEY (user_id) REFERENCES public.user_table(id);

ALTER TABLE public.user_gui_preference_table
    ADD CONSTRAINT user_gui_preferences_fk FOREIGN KEY (annotation_schema_id) REFERENCES public.user_annotation_schema_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.user_privilege_map_table
    ADD CONSTRAINT user_privilege_map_table_privilege_id_fkey FOREIGN KEY (privilege_id) REFERENCES public.privilege_table(id);

ALTER TABLE public.user_privilege_map_table
    ADD CONSTRAINT user_privilege_map_table_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.user_table(id);

ALTER TABLE public.user_terms_of_use_consent
    ADD CONSTRAINT user_terms_of_use_consent_user_fk FOREIGN KEY (user_id) REFERENCES public.user_table(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE public.user_table
    ADD CONSTRAINT user_user_annotation_schema_fk FOREIGN KEY (annotation_schema_id) REFERENCES public.user_annotation_schema_table(id);

