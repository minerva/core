ALTER TABLE modification_residue_table add COLUMN font_size integer default 10;

insert into modification_residue_table 
(id_modification_residue, 
	name, 
	id_species_db, 
	position, 
	state, 
	modification_type, 
	width, 
	active, 
	height, 
	direction, 
	z, 
	border_color,
	font_size) 
select 'state', 
	value, 
	element_table.id, 
	position, 
	null, 
	'STRUCTURAL_STATE', 
	structural_state_table.width, 
	null, 
	structural_state_table.height, 
	null, 
	structural_state_table.z, 
	'#000000ff',
	structural_state_table.font_size
	from  element_table ,structural_state_table where structural_state_id =structural_state_table.id;
	
alter table element_table drop column structural_state_id;
drop table structural_state_table;
drop sequence structural_state_sequence;

alter table modification_residue_table add column x double precision;
alter table modification_residue_table add column y double precision;

update modification_residue_table set x = cast (REGEXP_SUBSTRING_ARRAY(position, '[0-9\.\-]+')[1] as double precision);
update modification_residue_table set y = cast (REGEXP_SUBSTRING_ARRAY(position, '[0-9\.\-]+')[2] as double precision);

update modification_residue_table set width=15, height=15, x=x-7.5, y=y-7.5 where modification_type='RESIDUE';
update modification_residue_table set width=15, height=22.5, x=x-7.5, y=y-22.5 where modification_type='MODIFICATION_SITE';
update modification_residue_table set x=x-7.5, y=y-7.5 where modification_type in('BINDING_REGION','PROTEIN_BINDING_DOMAIN','REGULATORY_REGION', 'CODING_REGION');

alter table modification_residue_table drop column position;
