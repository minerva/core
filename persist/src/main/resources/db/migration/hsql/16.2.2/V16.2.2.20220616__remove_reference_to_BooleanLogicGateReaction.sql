delete from class_valid_annotation_miriam_type_table where class_valid_annotation_id in (select id from user_class_valid_annotations_table where class_name = 'lcsb.mapviewer.model.map.reaction.type.BooleanLogicGateReaction');
delete from user_class_valid_annotations_table where class_name = 'lcsb.mapviewer.model.map.reaction.type.BooleanLogicGateReaction';

delete from annotator_parameter_table where annotator_data_id in (select id from annotator_data_table where user_class_annotators_id in (select id from user_class_annotators_table where class_name = 'lcsb.mapviewer.model.map.reaction.type.BooleanLogicGateReaction'));
delete from annotator_data_table where user_class_annotators_id in (select id from user_class_annotators_table where class_name = 'lcsb.mapviewer.model.map.reaction.type.BooleanLogicGateReaction');
delete from user_class_annotators_table where class_name = 'lcsb.mapviewer.model.map.reaction.type.BooleanLogicGateReaction';
