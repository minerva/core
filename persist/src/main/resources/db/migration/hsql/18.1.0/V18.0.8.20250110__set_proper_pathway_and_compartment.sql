update element_table child
set compartment_id = null
where element_type_db = 'PATHWAY';

update element_table child
set compartment_id =
        (select id
         from element_table parent
         where parent.element_type_db like '%COMPARTMENT'
           and parent.model_id = child.model_id
           and parent.x < child.x + child.width
           and parent.x + parent.width > child.x
           and parent.y < child.y + child.height
           and parent.y + parent.height > child.y
           and id <> child.id
         order by parent.width * parent.height
         limit 1)
where element_type_db <> 'PATHWAY'
  and not element_type_db like '%COMPARTMENT';
;

update element_table child
set compartment_id =
        (select id
         from element_table parent
         where parent.element_type_db like '%COMPARTMENT'
           and parent.model_id = child.model_id
           and parent.x < child.x
           and parent.x + parent.width > child.x + child.width
           and parent.y < child.y
           and parent.y + parent.height > child.y + child.height
           and id <> child.id
         order by parent.width * parent.height
         limit 1)
where element_type_db like '%COMPARTMENT';
;

update element_table child
set pathway_id =
        (select id
         from element_table parent
         where parent.element_type_db = 'PATHWAY'
           and parent.model_id = child.model_id
           and parent.x < child.x + child.width
           and parent.x + parent.width > child.x
           and parent.y < child.y + child.height
           and parent.y + parent.height > child.y
           and id <> child.id
         order by parent.width * parent.height
         limit 1)
where element_type_db <> 'PATHWAY'
  and not element_type_db like '%COMPARTMENT';
;

update element_table child
set pathway_id =
        (select id
         from element_table parent
         where parent.element_type_db = 'PATHWAY'
           and parent.model_id = child.model_id
           and parent.x < child.x
           and parent.x + parent.width > child.x + child.width
           and parent.y < child.y
           and parent.y + parent.height > child.y + child.height
           and id <> child.id
         order by parent.width * parent.height
         limit 1)
where element_type_db = 'PATHWAY'
   or element_type_db like '%COMPARTMENT';
;
