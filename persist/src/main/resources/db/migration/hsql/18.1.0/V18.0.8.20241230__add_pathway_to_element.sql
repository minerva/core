alter table element_table
    add column pathway_id integer;

ALTER TABLE element_table
    ADD CONSTRAINT element_table_pathway_fk FOREIGN KEY (pathway_id) REFERENCES public.element_table (id);
