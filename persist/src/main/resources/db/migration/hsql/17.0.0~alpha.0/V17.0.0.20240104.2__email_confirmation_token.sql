CREATE SEQUENCE email_confirmation_token_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE email_confirmation_token_table
(
  id integer DEFAULT nextval('public.email_confirmation_token_sequence') NOT NULL,
  token character varying(255) NOT NULL,
  user_id integer not null,
  expires timestamp without time zone,

  CONSTRAINT email_confirmation_token_pk PRIMARY KEY (id),
  CONSTRAINT email_confirmation_token_user_fk FOREIGN KEY (user_id)
      REFERENCES user_table (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);
