ALTER TABLE PROJECT_BACKGROUND_IMAGE_LAYER_TABLE DROP CONSTRAINT data_overlay_image_layer__data_overlay_fkey;

ALTER TABLE PROJECT_BACKGROUND_IMAGE_LAYER_TABLE
    ADD CONSTRAINT data_overlay_image_layer__data_overlay_fkey FOREIGN KEY (model_id) REFERENCES public.model_data_table(id)
    ON DELETE CASCADE;


ALTER TABLE overview_link_table DROP CONSTRAINT overview_image_model_fk;

ALTER TABLE public.overview_link_table
    ADD CONSTRAINT overview_image_model_fk FOREIGN KEY (linked_model_id) REFERENCES public.model_data_table(id)
    ON DELETE CASCADE;

ALTER TABLE project_table DROP CONSTRAINT top_modelproject_table_fk;

ALTER TABLE public.project_table
    ADD CONSTRAINT top_modelproject_table_fk FOREIGN KEY (top_model_id) REFERENCES public.model_data_table(id)
    ON DELETE SET NULL;
    
    