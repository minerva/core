delete from user_privilege_map_table where privilege_id in (select id from privilege_table where type = 'CAN_CREATE_OVERLAYS');

delete from privilege_table where type = 'CAN_CREATE_OVERLAYS';

delete from configuration_option_table where type = 'DEFAULT_CAN_CREATE_OVERLAYS';