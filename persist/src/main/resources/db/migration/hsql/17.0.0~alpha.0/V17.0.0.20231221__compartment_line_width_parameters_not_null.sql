update element_table set inner_width=1 where inner_width is null and element_type_db in 
(
'COMPARTMENT',
'BOTTOM_SQUARE_COMPARTMENT',
'LEFT_SQUARE_COMPARTMENT',
'OVAL_COMPARTMENT',
'PATHWAY',
'RIGHT_SQUARE_COMPARTMENT',
'SQUARE_COMPARTMENT',
'TOP_SQUARE_COMPARTMENT'
);

update element_table set outer_width=2 where outer_width is null and element_type_db in 
(
'COMPARTMENT',
'BOTTOM_SQUARE_COMPARTMENT',
'LEFT_SQUARE_COMPARTMENT',
'OVAL_COMPARTMENT',
'PATHWAY',
'RIGHT_SQUARE_COMPARTMENT',
'SQUARE_COMPARTMENT',
'TOP_SQUARE_COMPARTMENT'
);

update element_table set thickness=12 where thickness is null and element_type_db in 
(
'COMPARTMENT',
'BOTTOM_SQUARE_COMPARTMENT',
'LEFT_SQUARE_COMPARTMENT',
'OVAL_COMPARTMENT',
'PATHWAY',
'RIGHT_SQUARE_COMPARTMENT',
'SQUARE_COMPARTMENT',
'TOP_SQUARE_COMPARTMENT'
);

