alter table comment_table add column element_id integer;
alter table comment_table add column reaction_id integer;

ALTER TABLE comment_table
    ADD CONSTRAINT comment_table_element_fk FOREIGN KEY (element_id) REFERENCES public.element_table(id)
    ON DELETE SET NULL;
ALTER TABLE comment_table
    ADD CONSTRAINT comment_table_reaction_fk FOREIGN KEY (reaction_id) REFERENCES public.reaction_table(id)
    ON DELETE SET NULL;

update comment_table set element_id = table_id 
where table_name like 'lcsb.mapviewer.model.map.species%' 
  and table_id in (select id from element_table);
  
update comment_table set reaction_id = table_id 
where table_name like 'lcsb.mapviewer.model.map.reaction%' 
  and table_id in (select id from reaction_table);
 
alter table comment_table drop column table_id;
alter table comment_table drop column table_name;
