CREATE SEQUENCE public.minerva_job_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE minerva_job_table (
    id integer DEFAULT nextval('public.minerva_job_sequence') NOT NULL,
    job_type character varying(255) not null,
    job_status character varying(255) not null,
    job_parameters text
);

alter table miriam_data_table add column article_id integer;

ALTER TABLE miriam_data_table
    ADD CONSTRAINT miriam_data_table_article_fk FOREIGN KEY (article_id) REFERENCES public.article_table(id)
    ON DELETE SET NULL;

alter table miriam_data_table add column link character varying(1024);

--refresh all links in miriam_data_table    
insert into minerva_job_table (job_type, job_status, job_parameters) 
select 'REFRESH_MIRIAM_INFO', 'PENDING', concat('{"id":',id, ' }') from miriam_data_table;

--delete unused cache
delete from cache_query_table where query like 'pubmed:%';
