ALTER TABLE model_miriam_data_table DROP CONSTRAINT model_miriam_data_table_miriam_data_fk;

ALTER TABLE model_miriam_data_table
    ADD CONSTRAINT model_miriam_data_table_miriam_data_fk FOREIGN KEY (miriam_data_id) REFERENCES miriam_data_table(id)
    ON DELETE CASCADE;
    

ALTER TABLE element_miriam DROP CONSTRAINT element_miriam_miriam_fk;
    
ALTER TABLE element_miriam
    ADD CONSTRAINT element_miriam_miriam_fk FOREIGN KEY (miriam_id) REFERENCES public.miriam_data_table(id)
    ON DELETE CASCADE;

ALTER TABLE reaction_miriam DROP CONSTRAINT fk71295b1567cc9103;
    
ALTER TABLE reaction_miriam
    ADD CONSTRAINT reaction_miriam_fk FOREIGN KEY (miriam_id) REFERENCES public.miriam_data_table(id)
    ON DELETE CASCADE;
    