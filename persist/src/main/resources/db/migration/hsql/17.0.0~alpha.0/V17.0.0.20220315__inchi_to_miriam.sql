ALTER TABLE miriam_data_table ALTER COLUMN resource TYPE text;

INSERT INTO miriam_data_table (resource, data_type, relation_type, link)
    SELECT in_chi, 'INCHI_x',  0,  id as varchar
    FROM element_table where in_chi is not null and in_chi <> '';

insert into element_miriam (element_id, miriam_id) SELECT link as integer, id from miriam_data_table where data_type = 'INCHI_x';
update miriam_data_table set data_type='INCHI', link = null where data_type='INCHI_x';

INSERT INTO miriam_data_table (resource, data_type, relation_type, link)
    SELECT in_chikey, 'INCHIKEY_x',  0,  id as varchar
    FROM element_table where in_chikey is not null and in_chikey <> '';

insert into element_miriam (element_id, miriam_id) SELECT link as integer, id from miriam_data_table where data_type = 'INCHIKEY_x';
update miriam_data_table set data_type='INCHIKEY', link = null where data_type='INCHIKEY_x';

alter table element_table drop column in_chi;
alter table element_table drop column in_chikey;

update annotator_parameter_table set identifier_type = 'INCHI', field = null where field = 'INCHI';
update annotator_parameter_table set identifier_type = 'INCHIKEY', field = null where field = 'INCHI_KEY';
