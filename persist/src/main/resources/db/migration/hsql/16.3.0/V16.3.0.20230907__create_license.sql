SET DATABASE SQL SYNTAX PGS TRUE;

CREATE SEQUENCE public.license_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.license_table (
    id integer DEFAULT nextval('public.license_sequence') NOT NULL,
    name character varying(255),
    content text,
    CONSTRAINT license_pk PRIMARY KEY (id),
    
);

ALTER TABLE project_table add column license_id integer;

ALTER TABLE project_table
    ADD CONSTRAINT project_table_license_fk FOREIGN KEY (license_id) REFERENCES license_table(id);
