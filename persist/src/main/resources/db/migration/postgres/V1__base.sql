--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

-- Started on 2017-09-21 13:13:48

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 1 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 181 (class 1259 OID 1746678)
-- Name: alias_table; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE alias_table (
    alias_type_db character varying(31) NOT NULL,
    iddb integer NOT NULL,
    aliasid character varying(255),
    color bytea,
    fontsize double precision,
    height double precision,
    maxvisibilitylevel integer,
    minvisibilitylevel integer,
    visibilitylevel integer NOT NULL,
    width double precision,
    x double precision,
    y double precision,
    innerwidth double precision,
    namepoint character varying(255),
    outerwidth double precision,
    thickness double precision,
    title character varying(255),
    activity boolean,
    linewidth double precision,
    state character varying(255),
    compartmentalias_iddb integer,
    model_iddb integer,
    parent_iddb integer,
    idcompartmentdb integer,
    idcomplexaliasdb integer,
    idspeciesdb integer
);


--
-- TOC entry 182 (class 1259 OID 1746684)
-- Name: alias_table_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE alias_table_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2549 (class 0 OID 0)
-- Dependencies: 182
-- Name: alias_table_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE alias_table_iddb_seq OWNED BY alias_table.iddb;


--
-- TOC entry 183 (class 1259 OID 1746686)
-- Name: antisense_rna_region_table; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE antisense_rna_region_table (
    idantisensernaregiondb integer NOT NULL,
    id character varying(255),
    name character varying(255),
    pos double precision,
    size double precision,
    type character varying(255),
    idrnaregiondb integer NOT NULL,
    state character varying(255),
    idspeciesdb integer
);

--
-- TOC entry 184 (class 1259 OID 1746692)
-- Name: antisense_rna_region_table_idrnaregiondb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE antisense_rna_region_table_idrnaregiondb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2550 (class 0 OID 0)
-- Dependencies: 184
-- Name: antisense_rna_region_table_idrnaregiondb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE antisense_rna_region_table_idrnaregiondb_seq OWNED BY antisense_rna_region_table.idrnaregiondb;


--
-- TOC entry 185 (class 1259 OID 1746694)
-- Name: arrowtypedata; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE arrowtypedata (
    iddb integer NOT NULL,
    angle double precision NOT NULL,
    arrowlinetype integer,
    arrowtype integer,
    len double precision NOT NULL,
    linetype integer
);

--
-- TOC entry 186 (class 1259 OID 1746697)
-- Name: arrowtypedata_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE arrowtypedata_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2551 (class 0 OID 0)
-- Dependencies: 186
-- Name: arrowtypedata_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE arrowtypedata_iddb_seq OWNED BY arrowtypedata.iddb;


--
-- TOC entry 187 (class 1259 OID 1746699)
-- Name: cacheentry; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE cacheentry (
    iddb integer NOT NULL,
    accessed timestamp without time zone,
    value text
);

--
-- TOC entry 188 (class 1259 OID 1746705)
-- Name: cachequery; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE cachequery (
    iddb integer NOT NULL,
    accessed timestamp without time zone,
    query text,
    value text
);

--
-- TOC entry 189 (class 1259 OID 1746711)
-- Name: cachequery_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE cachequery_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2552 (class 0 OID 0)
-- Dependencies: 189
-- Name: cachequery_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE cachequery_iddb_seq OWNED BY cachequery.iddb;


--
-- TOC entry 190 (class 1259 OID 1746713)
-- Name: feedback; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE feedback (
    feedback_type character varying(31) NOT NULL,
    iddb integer NOT NULL,
    content text,
    coordinates character varying(255),
    deleted boolean NOT NULL,
    email character varying(255),
    name character varying(255),
    removereason character varying(255),
    tableid integer,
    tablename character varying(255),
    pinned boolean,
    model_iddb integer,
    user_iddb integer
);

--
-- TOC entry 191 (class 1259 OID 1746719)
-- Name: feedback_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE feedback_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2553 (class 0 OID 0)
-- Dependencies: 191
-- Name: feedback_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE feedback_iddb_seq OWNED BY feedback.iddb;


--
-- TOC entry 192 (class 1259 OID 1746721)
-- Name: layer_table; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE layer_table (
    iddb integer NOT NULL,
    layerid character varying(255),
    locked boolean NOT NULL,
    name character varying(255),
    visible boolean NOT NULL,
    model_iddb integer
);

--
-- TOC entry 193 (class 1259 OID 1746727)
-- Name: layer_table_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE layer_table_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2554 (class 0 OID 0)
-- Dependencies: 193
-- Name: layer_table_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE layer_table_iddb_seq OWNED BY layer_table.iddb;


--
-- TOC entry 194 (class 1259 OID 1746729)
-- Name: layer_table_layerline; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE layer_table_layerline (
    layer_table_iddb integer NOT NULL,
    lines_iddb integer NOT NULL,
    idx integer NOT NULL
);

--
-- TOC entry 195 (class 1259 OID 1746732)
-- Name: layer_table_layeroval; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE layer_table_layeroval (
    layer_table_iddb integer NOT NULL,
    ovals_iddb integer NOT NULL,
    idx integer NOT NULL
);

--
-- TOC entry 196 (class 1259 OID 1746735)
-- Name: layer_table_layerrect; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE layer_table_layerrect (
    layer_table_iddb integer NOT NULL,
    rectangles_iddb integer NOT NULL,
    idx integer NOT NULL
);

--
-- TOC entry 197 (class 1259 OID 1746738)
-- Name: layer_table_layertext; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE layer_table_layertext (
    layer_table_iddb integer NOT NULL,
    texts_iddb integer NOT NULL,
    idx integer NOT NULL
);

--
-- TOC entry 198 (class 1259 OID 1746741)
-- Name: layerline; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE layerline (
    iddb integer NOT NULL,
    color bytea,
    linewdith double precision NOT NULL,
    atd_iddb integer,
    line_iddb integer
);

--
-- TOC entry 199 (class 1259 OID 1746747)
-- Name: layerline_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE layerline_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2555 (class 0 OID 0)
-- Dependencies: 199
-- Name: layerline_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE layerline_iddb_seq OWNED BY layerline.iddb;


--
-- TOC entry 200 (class 1259 OID 1746749)
-- Name: layeroval; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE layeroval (
    iddb integer NOT NULL,
    color bytea,
    height double precision,
    width double precision,
    x double precision,
    y double precision
);

--
-- TOC entry 201 (class 1259 OID 1746755)
-- Name: layeroval_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE layeroval_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2556 (class 0 OID 0)
-- Dependencies: 201
-- Name: layeroval_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE layeroval_iddb_seq OWNED BY layeroval.iddb;


--
-- TOC entry 202 (class 1259 OID 1746757)
-- Name: layerrect; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE layerrect (
    iddb integer NOT NULL,
    color bytea,
    height double precision,
    width double precision,
    x double precision,
    y double precision
);

--
-- TOC entry 203 (class 1259 OID 1746763)
-- Name: layerrect_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE layerrect_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2557 (class 0 OID 0)
-- Dependencies: 203
-- Name: layerrect_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE layerrect_iddb_seq OWNED BY layerrect.iddb;


--
-- TOC entry 204 (class 1259 OID 1746765)
-- Name: layertext; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE layertext (
    iddb integer NOT NULL,
    color bytea,
    fontsize double precision,
    height double precision,
    notes character varying(255),
    width double precision,
    x double precision,
    y double precision
);

--
-- TOC entry 205 (class 1259 OID 1746771)
-- Name: layertext_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE layertext_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2558 (class 0 OID 0)
-- Dependencies: 205
-- Name: layertext_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE layertext_iddb_seq OWNED BY layertext.iddb;


--
-- TOC entry 206 (class 1259 OID 1746773)
-- Name: layout; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE layout (
    iddb integer NOT NULL,
    directory character varying(255),
    title character varying(255),
    model_iddb integer
);

--
-- TOC entry 207 (class 1259 OID 1746779)
-- Name: layout_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE layout_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 208 (class 1259 OID 1746781)
-- Name: layout_iddb_seq1; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE layout_iddb_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2559 (class 0 OID 0)
-- Dependencies: 208
-- Name: layout_iddb_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE layout_iddb_seq1 OWNED BY layout.iddb;


--
-- TOC entry 209 (class 1259 OID 1746783)
-- Name: log_table; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE log_table (
    log_type_db character varying(31) NOT NULL,
    iddb integer NOT NULL,
    description character varying(255),
    "time" timestamp without time zone,
    type integer,
    objectid integer,
    tablename character varying(255),
    user_iddb integer
);

--
-- TOC entry 210 (class 1259 OID 1746789)
-- Name: log_table_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE log_table_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2560 (class 0 OID 0)
-- Dependencies: 210
-- Name: log_table_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE log_table_iddb_seq OWNED BY log_table.iddb;


--
-- TOC entry 211 (class 1259 OID 1746791)
-- Name: mappath; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE mappath (
    iddb integer NOT NULL,
    length double precision,
    beginnode_iddb integer NOT NULL,
    endnode_iddb integer NOT NULL
);

--
-- TOC entry 212 (class 1259 OID 1746794)
-- Name: mappath_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE mappath_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2561 (class 0 OID 0)
-- Dependencies: 212
-- Name: mappath_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE mappath_iddb_seq OWNED BY mappath.iddb;


--
-- TOC entry 213 (class 1259 OID 1746796)
-- Name: mappathnode; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE mappathnode (
    iddb integer NOT NULL,
    element_iddb integer,
    mappath_iddb integer NOT NULL,
    reaction_iddb integer
);

--
-- TOC entry 214 (class 1259 OID 1746799)
-- Name: mappathnode_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE mappathnode_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2562 (class 0 OID 0)
-- Dependencies: 214
-- Name: mappathnode_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE mappathnode_iddb_seq OWNED BY mappathnode.iddb;


--
-- TOC entry 215 (class 1259 OID 1746801)
-- Name: miriam_data_table; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE miriam_data_table (
    iddb integer NOT NULL,
    datatypeuri character varying(255),
    relationtype character varying(255),
    resource character varying(255),
    species_iddb integer
);

--
-- TOC entry 216 (class 1259 OID 1746807)
-- Name: miriam_data_table_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE miriam_data_table_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2563 (class 0 OID 0)
-- Dependencies: 216
-- Name: miriam_data_table_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE miriam_data_table_iddb_seq OWNED BY miriam_data_table.iddb;


--
-- TOC entry 217 (class 1259 OID 1746809)
-- Name: missingconnection; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE missingconnection (
    iddb integer NOT NULL,
    description text,
    link text,
    name character varying(255),
    score double precision NOT NULL,
    type integer,
    element_iddb integer NOT NULL
);

--
-- TOC entry 218 (class 1259 OID 1746815)
-- Name: missingconnection_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE missingconnection_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2564 (class 0 OID 0)
-- Dependencies: 218
-- Name: missingconnection_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE missingconnection_iddb_seq OWNED BY missingconnection.iddb;


--
-- TOC entry 219 (class 1259 OID 1746817)
-- Name: model_table; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE model_table (
    iddb integer NOT NULL,
    creationdate timestamp without time zone,
    height double precision,
    mapversion character varying(255),
    notes character varying(255),
    shortdescription character varying(255),
    tilesize integer NOT NULL,
    width double precision,
    zoomlevels integer NOT NULL,
    project_iddb integer NOT NULL
);

--
-- TOC entry 220 (class 1259 OID 1746823)
-- Name: model_table_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE model_table_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2565 (class 0 OID 0)
-- Dependencies: 220
-- Name: model_table_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE model_table_iddb_seq OWNED BY model_table.iddb;


--
-- TOC entry 221 (class 1259 OID 1746825)
-- Name: modification_residue_table; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE modification_residue_table (
    idmodificationresiduedb integer NOT NULL,
    angle double precision,
    id character varying(255),
    name character varying(255),
    side character varying(255),
    size double precision,
    state character varying(255),
    idspeciesdb integer NOT NULL
);

--
-- TOC entry 222 (class 1259 OID 1746831)
-- Name: modification_residue_table_idmodificationresiduedb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE modification_residue_table_idmodificationresiduedb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2566 (class 0 OID 0)
-- Dependencies: 222
-- Name: modification_residue_table_idmodificationresiduedb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE modification_residue_table_idmodificationresiduedb_seq OWNED BY modification_residue_table.idmodificationresiduedb;


--
-- TOC entry 223 (class 1259 OID 1746833)
-- Name: node_table; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE node_table (
    node_type_db character varying(31) NOT NULL,
    iddb integer NOT NULL,
    nodeoperatorforinput_iddb integer,
    nodeoperatorforoutput_iddb integer,
    line_iddb integer,
    reaction_iddb integer,
    alias_iddb integer,
    element_iddb integer,
    parent_iddb integer
);

--
-- TOC entry 224 (class 1259 OID 1746836)
-- Name: node_table_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE node_table_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2567 (class 0 OID 0)
-- Dependencies: 224
-- Name: node_table_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE node_table_iddb_seq OWNED BY node_table.iddb;


--
-- TOC entry 225 (class 1259 OID 1746838)
-- Name: point_table; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE point_table (
    iddb integer NOT NULL,
    point_val character varying(255),
    idx integer NOT NULL
);

--
-- TOC entry 226 (class 1259 OID 1746841)
-- Name: polylinedata; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE polylinedata (
    iddb integer NOT NULL,
    color bytea,
    type integer,
    width double precision NOT NULL,
    beginatd_iddb integer,
    endatd_iddb integer
);

--
-- TOC entry 227 (class 1259 OID 1746847)
-- Name: polylinedata_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE polylinedata_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2568 (class 0 OID 0)
-- Dependencies: 227
-- Name: polylinedata_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE polylinedata_iddb_seq OWNED BY polylinedata.iddb;


--
-- TOC entry 228 (class 1259 OID 1746849)
-- Name: privilege_table; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE privilege_table (
    privilege_class_type_db character varying(31) NOT NULL,
    iddb integer NOT NULL,
    level integer NOT NULL,
    type integer,
    idobject integer,
    user_iddb integer
);

--
-- TOC entry 229 (class 1259 OID 1746852)
-- Name: privilege_table_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE privilege_table_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2569 (class 0 OID 0)
-- Dependencies: 229
-- Name: privilege_table_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE privilege_table_iddb_seq OWNED BY privilege_table.iddb;


--
-- TOC entry 230 (class 1259 OID 1746854)
-- Name: project_table; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE project_table (
    iddb integer NOT NULL,
    name character varying(255),
    progress double precision NOT NULL,
    status integer,
    visible boolean NOT NULL
);

--
-- TOC entry 231 (class 1259 OID 1746857)
-- Name: project_table_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE project_table_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2570 (class 0 OID 0)
-- Dependencies: 231
-- Name: project_table_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE project_table_iddb_seq OWNED BY project_table.iddb;


--
-- TOC entry 232 (class 1259 OID 1746859)
-- Name: reaction_miriam; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE reaction_miriam (
    reaction_id integer NOT NULL,
    miriam_id integer NOT NULL
);

--
-- TOC entry 233 (class 1259 OID 1746862)
-- Name: reaction_table; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE reaction_table (
    reaction_type_db character varying(31) NOT NULL,
    iddb integer NOT NULL,
    idreaction character varying(255),
    kineticlaw boolean NOT NULL,
    metaid character varying(255),
    name character varying(255),
    notes text,
    reversible boolean NOT NULL,
    type character varying(255),
    model_iddb integer
);

--
-- TOC entry 234 (class 1259 OID 1746868)
-- Name: reaction_table_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE reaction_table_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2571 (class 0 OID 0)
-- Dependencies: 234
-- Name: reaction_table_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE reaction_table_iddb_seq OWNED BY reaction_table.iddb;


--
-- TOC entry 235 (class 1259 OID 1746870)
-- Name: references_table; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE references_table (
    reference_id integer NOT NULL,
    reference text
);

--
-- TOC entry 236 (class 1259 OID 1746876)
-- Name: search_index_table; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE search_index_table (
    iddb integer NOT NULL,
    value character varying(255),
    weight integer,
    source_iddb integer
);

--
-- TOC entry 237 (class 1259 OID 1746879)
-- Name: search_index_table_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE search_index_table_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 238 (class 1259 OID 1746881)
-- Name: search_index_table_iddb_seq1; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE search_index_table_iddb_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2572 (class 0 OID 0)
-- Dependencies: 238
-- Name: search_index_table_iddb_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE search_index_table_iddb_seq1 OWNED BY search_index_table.iddb;


--
-- TOC entry 239 (class 1259 OID 1746883)
-- Name: species_table; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE species_table (
    species_type_db character varying(31) NOT NULL,
    iddb integer NOT NULL,
    compartmentid character varying(255),
    metaid character varying(255),
    name character varying(255),
    charge integer,
    idspecies character varying(255),
    initialamount integer,
    initialconcentration integer,
    notes text,
    onlysubstanceunits boolean,
    positiontocompartment character varying(255),
    type character varying(255),
    antisensernaid character varying(255),
    homodimer integer,
    hypothetical boolean,
    degradedid character varying(255),
    drugid character varying(255),
    geneid character varying(255),
    structuralstate character varying(255),
    ionid character varying(255),
    phenotypeid character varying(255),
    idprotein character varying(255),
    rnaid character varying(255),
    simplemoleculeid character varying(255),
    unknownid character varying(255),
    complex_iddb integer,
    model_iddb integer,
    parent_iddb integer
);

--
-- TOC entry 240 (class 1259 OID 1746889)
-- Name: species_table_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE species_table_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2573 (class 0 OID 0)
-- Dependencies: 240
-- Name: species_table_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE species_table_iddb_seq OWNED BY species_table.iddb;


--
-- TOC entry 241 (class 1259 OID 1746891)
-- Name: unified_node_elements; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE unified_node_elements (
    iddb_node integer NOT NULL,
    iddb_element integer NOT NULL
);

--
-- TOC entry 242 (class 1259 OID 1746894)
-- Name: unifiednode; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE unifiednode (
    iddb integer NOT NULL
);

--
-- TOC entry 243 (class 1259 OID 1746897)
-- Name: unifiednode_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE unifiednode_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2574 (class 0 OID 0)
-- Dependencies: 243
-- Name: unifiednode_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE unifiednode_iddb_seq OWNED BY unifiednode.iddb;


--
-- TOC entry 244 (class 1259 OID 1746899)
-- Name: user_table; Type: TABLE; Schema: public; Owner: map_viewer
--

CREATE TABLE user_table (
    iddb integer NOT NULL,
    cryptedpassword character varying(255),
    email character varying(255),
    login character varying(255),
    name character varying(255),
    surname character varying(255)
);

--
-- TOC entry 245 (class 1259 OID 1746905)
-- Name: user_table_iddb_seq; Type: SEQUENCE; Schema: public; Owner: map_viewer
--

CREATE SEQUENCE user_table_iddb_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- TOC entry 2575 (class 0 OID 0)
-- Dependencies: 245
-- Name: user_table_iddb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: map_viewer
--

ALTER SEQUENCE user_table_iddb_seq OWNED BY user_table.iddb;


--
-- TOC entry 2198 (class 2604 OID 1746907)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY alias_table ALTER COLUMN iddb SET DEFAULT nextval('alias_table_iddb_seq'::regclass);


--
-- TOC entry 2199 (class 2604 OID 1746908)
-- Name: idrnaregiondb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY antisense_rna_region_table ALTER COLUMN idrnaregiondb SET DEFAULT nextval('antisense_rna_region_table_idrnaregiondb_seq'::regclass);


--
-- TOC entry 2200 (class 2604 OID 1746909)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY arrowtypedata ALTER COLUMN iddb SET DEFAULT nextval('arrowtypedata_iddb_seq'::regclass);


--
-- TOC entry 2201 (class 2604 OID 1746910)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY cachequery ALTER COLUMN iddb SET DEFAULT nextval('cachequery_iddb_seq'::regclass);


--
-- TOC entry 2202 (class 2604 OID 1746911)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY feedback ALTER COLUMN iddb SET DEFAULT nextval('feedback_iddb_seq'::regclass);


--
-- TOC entry 2203 (class 2604 OID 1746912)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layer_table ALTER COLUMN iddb SET DEFAULT nextval('layer_table_iddb_seq'::regclass);


--
-- TOC entry 2204 (class 2604 OID 1746913)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layerline ALTER COLUMN iddb SET DEFAULT nextval('layerline_iddb_seq'::regclass);


--
-- TOC entry 2205 (class 2604 OID 1746914)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layeroval ALTER COLUMN iddb SET DEFAULT nextval('layeroval_iddb_seq'::regclass);


--
-- TOC entry 2206 (class 2604 OID 1746915)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layerrect ALTER COLUMN iddb SET DEFAULT nextval('layerrect_iddb_seq'::regclass);


--
-- TOC entry 2207 (class 2604 OID 1746916)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layertext ALTER COLUMN iddb SET DEFAULT nextval('layertext_iddb_seq'::regclass);


--
-- TOC entry 2208 (class 2604 OID 1746917)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layout ALTER COLUMN iddb SET DEFAULT nextval('layout_iddb_seq1'::regclass);


--
-- TOC entry 2209 (class 2604 OID 1746918)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY log_table ALTER COLUMN iddb SET DEFAULT nextval('log_table_iddb_seq'::regclass);


--
-- TOC entry 2210 (class 2604 OID 1746919)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY mappath ALTER COLUMN iddb SET DEFAULT nextval('mappath_iddb_seq'::regclass);


--
-- TOC entry 2211 (class 2604 OID 1746920)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY mappathnode ALTER COLUMN iddb SET DEFAULT nextval('mappathnode_iddb_seq'::regclass);


--
-- TOC entry 2212 (class 2604 OID 1746921)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY miriam_data_table ALTER COLUMN iddb SET DEFAULT nextval('miriam_data_table_iddb_seq'::regclass);


--
-- TOC entry 2213 (class 2604 OID 1746922)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY missingconnection ALTER COLUMN iddb SET DEFAULT nextval('missingconnection_iddb_seq'::regclass);


--
-- TOC entry 2214 (class 2604 OID 1746923)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY model_table ALTER COLUMN iddb SET DEFAULT nextval('model_table_iddb_seq'::regclass);


--
-- TOC entry 2215 (class 2604 OID 1746924)
-- Name: idmodificationresiduedb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY modification_residue_table ALTER COLUMN idmodificationresiduedb SET DEFAULT nextval('modification_residue_table_idmodificationresiduedb_seq'::regclass);


--
-- TOC entry 2216 (class 2604 OID 1746925)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY node_table ALTER COLUMN iddb SET DEFAULT nextval('node_table_iddb_seq'::regclass);


--
-- TOC entry 2217 (class 2604 OID 1746926)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY polylinedata ALTER COLUMN iddb SET DEFAULT nextval('polylinedata_iddb_seq'::regclass);


--
-- TOC entry 2218 (class 2604 OID 1746927)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY privilege_table ALTER COLUMN iddb SET DEFAULT nextval('privilege_table_iddb_seq'::regclass);


--
-- TOC entry 2219 (class 2604 OID 1746928)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY project_table ALTER COLUMN iddb SET DEFAULT nextval('project_table_iddb_seq'::regclass);


--
-- TOC entry 2220 (class 2604 OID 1746929)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY reaction_table ALTER COLUMN iddb SET DEFAULT nextval('reaction_table_iddb_seq'::regclass);


--
-- TOC entry 2221 (class 2604 OID 1746930)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY search_index_table ALTER COLUMN iddb SET DEFAULT nextval('search_index_table_iddb_seq1'::regclass);


--
-- TOC entry 2222 (class 2604 OID 1746931)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY species_table ALTER COLUMN iddb SET DEFAULT nextval('species_table_iddb_seq'::regclass);


--
-- TOC entry 2223 (class 2604 OID 1746932)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY unifiednode ALTER COLUMN iddb SET DEFAULT nextval('unifiednode_iddb_seq'::regclass);


--
-- TOC entry 2224 (class 2604 OID 1746933)
-- Name: iddb; Type: DEFAULT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY user_table ALTER COLUMN iddb SET DEFAULT nextval('user_table_iddb_seq'::regclass);


--
-- TOC entry 2476 (class 0 OID 1746678)
-- Dependencies: 181
-- Data for Name: alias_table; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2576 (class 0 OID 0)
-- Dependencies: 182
-- Name: alias_table_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('alias_table_iddb_seq', 18198, true);


--
-- TOC entry 2478 (class 0 OID 1746686)
-- Dependencies: 183
-- Data for Name: antisense_rna_region_table; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2577 (class 0 OID 0)
-- Dependencies: 184
-- Name: antisense_rna_region_table_idrnaregiondb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('antisense_rna_region_table_idrnaregiondb_seq', 1, false);


--
-- TOC entry 2480 (class 0 OID 1746694)
-- Dependencies: 185
-- Data for Name: arrowtypedata; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2578 (class 0 OID 0)
-- Dependencies: 186
-- Name: arrowtypedata_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('arrowtypedata_iddb_seq', 45802, true);


--
-- TOC entry 2482 (class 0 OID 1746699)
-- Dependencies: 187
-- Data for Name: cacheentry; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2483 (class 0 OID 1746705)
-- Dependencies: 188
-- Data for Name: cachequery; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2579 (class 0 OID 0)
-- Dependencies: 189
-- Name: cachequery_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('cachequery_iddb_seq', 3717, true);


--
-- TOC entry 2485 (class 0 OID 1746713)
-- Dependencies: 190
-- Data for Name: feedback; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2580 (class 0 OID 0)
-- Dependencies: 191
-- Name: feedback_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('feedback_iddb_seq', 27, true);


--
-- TOC entry 2487 (class 0 OID 1746721)
-- Dependencies: 192
-- Data for Name: layer_table; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2581 (class 0 OID 0)
-- Dependencies: 193
-- Name: layer_table_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('layer_table_iddb_seq', 4, true);


--
-- TOC entry 2489 (class 0 OID 1746729)
-- Dependencies: 194
-- Data for Name: layer_table_layerline; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2490 (class 0 OID 1746732)
-- Dependencies: 195
-- Data for Name: layer_table_layeroval; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2491 (class 0 OID 1746735)
-- Dependencies: 196
-- Data for Name: layer_table_layerrect; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2492 (class 0 OID 1746738)
-- Dependencies: 197
-- Data for Name: layer_table_layertext; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2493 (class 0 OID 1746741)
-- Dependencies: 198
-- Data for Name: layerline; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2582 (class 0 OID 0)
-- Dependencies: 199
-- Name: layerline_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('layerline_iddb_seq', 1, false);


--
-- TOC entry 2495 (class 0 OID 1746749)
-- Dependencies: 200
-- Data for Name: layeroval; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2583 (class 0 OID 0)
-- Dependencies: 201
-- Name: layeroval_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('layeroval_iddb_seq', 1, false);


--
-- TOC entry 2497 (class 0 OID 1746757)
-- Dependencies: 202
-- Data for Name: layerrect; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2584 (class 0 OID 0)
-- Dependencies: 203
-- Name: layerrect_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('layerrect_iddb_seq', 1, false);


--
-- TOC entry 2499 (class 0 OID 1746765)
-- Dependencies: 204
-- Data for Name: layertext; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2585 (class 0 OID 0)
-- Dependencies: 205
-- Name: layertext_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('layertext_iddb_seq', 104, true);


--
-- TOC entry 2501 (class 0 OID 1746773)
-- Dependencies: 206
-- Data for Name: layout; Type: TABLE DATA; Schema: public; Owner: map_viewer
--

INSERT INTO layout VALUES (5, 'normal', 'Normal', 3);


--
-- TOC entry 2586 (class 0 OID 0)
-- Dependencies: 207
-- Name: layout_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('layout_iddb_seq', 164, true);


--
-- TOC entry 2587 (class 0 OID 0)
-- Dependencies: 208
-- Name: layout_iddb_seq1; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('layout_iddb_seq1', 90, true);


--
-- TOC entry 2504 (class 0 OID 1746783)
-- Dependencies: 209
-- Data for Name: log_table; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2588 (class 0 OID 0)
-- Dependencies: 210
-- Name: log_table_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('log_table_iddb_seq', 1, false);


--
-- TOC entry 2506 (class 0 OID 1746791)
-- Dependencies: 211
-- Data for Name: mappath; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2589 (class 0 OID 0)
-- Dependencies: 212
-- Name: mappath_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('mappath_iddb_seq', 1, false);


--
-- TOC entry 2508 (class 0 OID 1746796)
-- Dependencies: 213
-- Data for Name: mappathnode; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2590 (class 0 OID 0)
-- Dependencies: 214
-- Name: mappathnode_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('mappathnode_iddb_seq', 1, false);


--
-- TOC entry 2510 (class 0 OID 1746801)
-- Dependencies: 215
-- Data for Name: miriam_data_table; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2591 (class 0 OID 0)
-- Dependencies: 216
-- Name: miriam_data_table_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('miriam_data_table_iddb_seq', 98614, true);


--
-- TOC entry 2512 (class 0 OID 1746809)
-- Dependencies: 217
-- Data for Name: missingconnection; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2592 (class 0 OID 0)
-- Dependencies: 218
-- Name: missingconnection_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('missingconnection_iddb_seq', 23235, true);


--
-- TOC entry 2514 (class 0 OID 1746817)
-- Dependencies: 219
-- Data for Name: model_table; Type: TABLE DATA; Schema: public; Owner: map_viewer
--

INSERT INTO model_table VALUES (3, '2014-03-27 15:59:31.321', 14285, 'empty DISEASE MAP', NULL, 'For information on content, functionalities and referencing the Parkinson''s disease map, click <a target="_blank" href = "http://minerva.uni.lu/pd_map/">here</a>.', 256, 25195, 7, 4);


--
-- TOC entry 2593 (class 0 OID 0)
-- Dependencies: 220
-- Name: model_table_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('model_table_iddb_seq', 24, true);


--
-- TOC entry 2516 (class 0 OID 1746825)
-- Dependencies: 221
-- Data for Name: modification_residue_table; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2594 (class 0 OID 0)
-- Dependencies: 222
-- Name: modification_residue_table_idmodificationresiduedb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('modification_residue_table_idmodificationresiduedb_seq', 1465, true);


--
-- TOC entry 2518 (class 0 OID 1746833)
-- Dependencies: 223
-- Data for Name: node_table; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2595 (class 0 OID 0)
-- Dependencies: 224
-- Name: node_table_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('node_table_iddb_seq', 22901, true);


--
-- TOC entry 2520 (class 0 OID 1746838)
-- Dependencies: 225
-- Data for Name: point_table; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2521 (class 0 OID 1746841)
-- Dependencies: 226
-- Data for Name: polylinedata; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2596 (class 0 OID 0)
-- Dependencies: 227
-- Name: polylinedata_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('polylinedata_iddb_seq', 22901, true);


--
-- TOC entry 2523 (class 0 OID 1746849)
-- Dependencies: 228
-- Data for Name: privilege_table; Type: TABLE DATA; Schema: public; Owner: map_viewer
--

INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 71, 1, 0, 1, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 72, 1, 2, 1, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 73, 1, 3, 1, 1);
INSERT INTO privilege_table VALUES ('BASIC_PRIVILEGE', 162, 1, 5, 0, 1);
INSERT INTO privilege_table VALUES ('BASIC_PRIVILEGE', 163, 1, 6, 0, 1);
INSERT INTO privilege_table VALUES ('BASIC_PRIVILEGE', 164, 1, 9, 0, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 77, 1, 0, 1, 3);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 78, 1, 0, 6, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 79, 1, 2, 6, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 80, 1, 3, 6, 1);
INSERT INTO privilege_table VALUES ('BASIC_PRIVILEGE', 165, 1, 10, 0, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 84, 1, 0, 6, 3);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 15, 1, 0, 4, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 16, 1, 2, 4, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 17, 1, 3, 4, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 21, 1, 0, 4, 3);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 85, 1, 0, 7, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 86, 1, 2, 7, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 87, 1, 3, 7, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 91, 1, 0, 7, 3);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 92, 1, 0, 8, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 93, 1, 2, 8, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 94, 1, 3, 8, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 98, 1, 0, 8, 3);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 99, 1, 0, 9, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 100, 1, 2, 9, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 101, 1, 3, 9, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 105, 1, 0, 9, 3);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 106, 1, 0, 10, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 107, 1, 2, 10, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 108, 1, 3, 10, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 112, 1, 0, 10, 3);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 113, 1, 0, 11, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 114, 1, 2, 11, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 115, 1, 3, 11, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 119, 1, 0, 11, 3);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 120, 1, 0, 17, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 121, 1, 2, 17, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 122, 1, 3, 17, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 126, 1, 0, 17, 3);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 127, 1, 0, 18, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 128, 1, 2, 18, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 129, 1, 3, 18, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 133, 1, 0, 18, 3);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 134, 1, 0, 19, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 135, 1, 2, 19, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 136, 1, 3, 19, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 140, 1, 0, 19, 3);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 141, 1, 0, 20, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 142, 1, 2, 20, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 143, 1, 3, 20, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 147, 1, 0, 20, 3);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 148, 1, 0, 21, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 149, 1, 2, 21, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 150, 1, 3, 21, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 154, 1, 0, 21, 3);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 155, 1, 0, 22, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 156, 1, 2, 22, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 157, 1, 3, 22, 1);
INSERT INTO privilege_table VALUES ('OBJECT_PRIVILEGE', 161, 1, 0, 22, 3);


--
-- TOC entry 2597 (class 0 OID 0)
-- Dependencies: 229
-- Name: privilege_table_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('privilege_table_iddb_seq', 165, true);


--
-- TOC entry 2525 (class 0 OID 1746854)
-- Dependencies: 230
-- Data for Name: project_table; Type: TABLE DATA; Schema: public; Owner: map_viewer
--

INSERT INTO project_table VALUES (4, 'empty', 0, 0, true);


--
-- TOC entry 2598 (class 0 OID 0)
-- Dependencies: 231
-- Name: project_table_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('project_table_iddb_seq', 22, true);


--
-- TOC entry 2527 (class 0 OID 1746859)
-- Dependencies: 232
-- Data for Name: reaction_miriam; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2528 (class 0 OID 1746862)
-- Dependencies: 233
-- Data for Name: reaction_table; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2599 (class 0 OID 0)
-- Dependencies: 234
-- Name: reaction_table_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('reaction_table_iddb_seq', 6640, true);


--
-- TOC entry 2530 (class 0 OID 1746870)
-- Dependencies: 235
-- Data for Name: references_table; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2531 (class 0 OID 1746876)
-- Dependencies: 236
-- Data for Name: search_index_table; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2600 (class 0 OID 0)
-- Dependencies: 237
-- Name: search_index_table_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('search_index_table_iddb_seq', 15, true);


--
-- TOC entry 2601 (class 0 OID 0)
-- Dependencies: 238
-- Name: search_index_table_iddb_seq1; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('search_index_table_iddb_seq1', 1, false);


--
-- TOC entry 2534 (class 0 OID 1746883)
-- Dependencies: 239
-- Data for Name: species_table; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2602 (class 0 OID 0)
-- Dependencies: 240
-- Name: species_table_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('species_table_iddb_seq', 15168, true);


--
-- TOC entry 2536 (class 0 OID 1746891)
-- Dependencies: 241
-- Data for Name: unified_node_elements; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2537 (class 0 OID 1746894)
-- Dependencies: 242
-- Data for Name: unifiednode; Type: TABLE DATA; Schema: public; Owner: map_viewer
--



--
-- TOC entry 2603 (class 0 OID 0)
-- Dependencies: 243
-- Name: unifiednode_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('unifiednode_iddb_seq', 9891, true);


--
-- TOC entry 2539 (class 0 OID 1746899)
-- Dependencies: 244
-- Data for Name: user_table; Type: TABLE DATA; Schema: public; Owner: map_viewer
--

INSERT INTO user_table VALUES (3, 'd41d8cd98f00b204e9800998ecf8427e', NULL, 'anonymous', NULL, NULL);
INSERT INTO user_table VALUES (1, '21232f297a57a5a743894a0e4a801fc3', NULL, 'admin', NULL, NULL);


--
-- TOC entry 2604 (class 0 OID 0)
-- Dependencies: 245
-- Name: user_table_iddb_seq; Type: SEQUENCE SET; Schema: public; Owner: map_viewer
--

SELECT pg_catalog.setval('user_table_iddb_seq', 3, true);


--
-- TOC entry 2226 (class 2606 OID 1746935)
-- Name: alias_table_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY alias_table
    ADD CONSTRAINT alias_table_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2228 (class 2606 OID 1746937)
-- Name: antisense_rna_region_table_idantisensernaregiondb_key; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY antisense_rna_region_table
    ADD CONSTRAINT antisense_rna_region_table_idantisensernaregiondb_key UNIQUE (idantisensernaregiondb);


--
-- TOC entry 2230 (class 2606 OID 1746939)
-- Name: antisense_rna_region_table_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY antisense_rna_region_table
    ADD CONSTRAINT antisense_rna_region_table_pkey PRIMARY KEY (idrnaregiondb);


--
-- TOC entry 2232 (class 2606 OID 1746941)
-- Name: arrowtypedata_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY arrowtypedata
    ADD CONSTRAINT arrowtypedata_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2234 (class 2606 OID 1746943)
-- Name: cacheentry_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY cacheentry
    ADD CONSTRAINT cacheentry_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2236 (class 2606 OID 1746945)
-- Name: cachequery_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY cachequery
    ADD CONSTRAINT cachequery_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2238 (class 2606 OID 1746947)
-- Name: feedback_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY feedback
    ADD CONSTRAINT feedback_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2242 (class 2606 OID 1746949)
-- Name: layer_table_layerline_lines_iddb_key; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layer_table_layerline
    ADD CONSTRAINT layer_table_layerline_lines_iddb_key UNIQUE (lines_iddb);


--
-- TOC entry 2244 (class 2606 OID 1746951)
-- Name: layer_table_layerline_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layer_table_layerline
    ADD CONSTRAINT layer_table_layerline_pkey PRIMARY KEY (layer_table_iddb, idx);


--
-- TOC entry 2246 (class 2606 OID 1746953)
-- Name: layer_table_layeroval_ovals_iddb_key; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layer_table_layeroval
    ADD CONSTRAINT layer_table_layeroval_ovals_iddb_key UNIQUE (ovals_iddb);


--
-- TOC entry 2248 (class 2606 OID 1746955)
-- Name: layer_table_layeroval_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layer_table_layeroval
    ADD CONSTRAINT layer_table_layeroval_pkey PRIMARY KEY (layer_table_iddb, idx);


--
-- TOC entry 2250 (class 2606 OID 1746957)
-- Name: layer_table_layerrect_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layer_table_layerrect
    ADD CONSTRAINT layer_table_layerrect_pkey PRIMARY KEY (layer_table_iddb, idx);


--
-- TOC entry 2252 (class 2606 OID 1746959)
-- Name: layer_table_layerrect_rectangles_iddb_key; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layer_table_layerrect
    ADD CONSTRAINT layer_table_layerrect_rectangles_iddb_key UNIQUE (rectangles_iddb);


--
-- TOC entry 2254 (class 2606 OID 1746961)
-- Name: layer_table_layertext_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layer_table_layertext
    ADD CONSTRAINT layer_table_layertext_pkey PRIMARY KEY (layer_table_iddb, idx);


--
-- TOC entry 2256 (class 2606 OID 1746963)
-- Name: layer_table_layertext_texts_iddb_key; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layer_table_layertext
    ADD CONSTRAINT layer_table_layertext_texts_iddb_key UNIQUE (texts_iddb);


--
-- TOC entry 2240 (class 2606 OID 1746965)
-- Name: layer_table_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layer_table
    ADD CONSTRAINT layer_table_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2258 (class 2606 OID 1746967)
-- Name: layerline_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layerline
    ADD CONSTRAINT layerline_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2260 (class 2606 OID 1746969)
-- Name: layeroval_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layeroval
    ADD CONSTRAINT layeroval_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2262 (class 2606 OID 1746971)
-- Name: layerrect_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layerrect
    ADD CONSTRAINT layerrect_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2264 (class 2606 OID 1746973)
-- Name: layertext_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layertext
    ADD CONSTRAINT layertext_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2266 (class 2606 OID 1746975)
-- Name: layout_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layout
    ADD CONSTRAINT layout_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2268 (class 2606 OID 1746977)
-- Name: log_table_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY log_table
    ADD CONSTRAINT log_table_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2270 (class 2606 OID 1746979)
-- Name: mappath_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY mappath
    ADD CONSTRAINT mappath_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2272 (class 2606 OID 1746981)
-- Name: mappathnode_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY mappathnode
    ADD CONSTRAINT mappathnode_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2274 (class 2606 OID 1746983)
-- Name: miriam_data_table_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY miriam_data_table
    ADD CONSTRAINT miriam_data_table_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2276 (class 2606 OID 1746985)
-- Name: missingconnection_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY missingconnection
    ADD CONSTRAINT missingconnection_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2278 (class 2606 OID 1746987)
-- Name: model_table_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY model_table
    ADD CONSTRAINT model_table_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2280 (class 2606 OID 1746989)
-- Name: modification_residue_table_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY modification_residue_table
    ADD CONSTRAINT modification_residue_table_pkey PRIMARY KEY (idmodificationresiduedb);


--
-- TOC entry 2282 (class 2606 OID 1746991)
-- Name: node_table_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY node_table
    ADD CONSTRAINT node_table_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2284 (class 2606 OID 1746993)
-- Name: point_table_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY point_table
    ADD CONSTRAINT point_table_pkey PRIMARY KEY (iddb, idx);


--
-- TOC entry 2286 (class 2606 OID 1746995)
-- Name: polylinedata_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY polylinedata
    ADD CONSTRAINT polylinedata_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2288 (class 2606 OID 1746997)
-- Name: privilege_table_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY privilege_table
    ADD CONSTRAINT privilege_table_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2290 (class 2606 OID 1746999)
-- Name: project_table_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY project_table
    ADD CONSTRAINT project_table_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2292 (class 2606 OID 1747001)
-- Name: reaction_miriam_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY reaction_miriam
    ADD CONSTRAINT reaction_miriam_pkey PRIMARY KEY (reaction_id, miriam_id);


--
-- TOC entry 2294 (class 2606 OID 1747003)
-- Name: reaction_table_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY reaction_table
    ADD CONSTRAINT reaction_table_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2296 (class 2606 OID 1747005)
-- Name: search_index_table_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY search_index_table
    ADD CONSTRAINT search_index_table_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2298 (class 2606 OID 1747007)
-- Name: species_table_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY species_table
    ADD CONSTRAINT species_table_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2300 (class 2606 OID 1747009)
-- Name: unified_node_elements_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY unified_node_elements
    ADD CONSTRAINT unified_node_elements_pkey PRIMARY KEY (iddb_node, iddb_element);


--
-- TOC entry 2302 (class 2606 OID 1747011)
-- Name: unifiednode_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY unifiednode
    ADD CONSTRAINT unifiednode_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2304 (class 2606 OID 1747013)
-- Name: user_table_pkey; Type: CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY user_table
    ADD CONSTRAINT user_table_pkey PRIMARY KEY (iddb);


--
-- TOC entry 2305 (class 2606 OID 1747014)
-- Name: fk1220391f1e595a8a; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY alias_table
    ADD CONSTRAINT fk1220391f1e595a8a FOREIGN KEY (idspeciesdb) REFERENCES species_table(iddb);


--
-- TOC entry 2306 (class 2606 OID 1747019)
-- Name: fk1220391f28cbc89c; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY alias_table
    ADD CONSTRAINT fk1220391f28cbc89c FOREIGN KEY (idcomplexaliasdb) REFERENCES alias_table(iddb);


--
-- TOC entry 2307 (class 2606 OID 1747024)
-- Name: fk1220391f5720a4be; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY alias_table
    ADD CONSTRAINT fk1220391f5720a4be FOREIGN KEY (idcompartmentdb) REFERENCES species_table(iddb);


--
-- TOC entry 2308 (class 2606 OID 1747029)
-- Name: fk1220391f746527fb; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY alias_table
    ADD CONSTRAINT fk1220391f746527fb FOREIGN KEY (compartmentalias_iddb) REFERENCES alias_table(iddb);


--
-- TOC entry 2309 (class 2606 OID 1747034)
-- Name: fk1220391f911dfddb; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY alias_table
    ADD CONSTRAINT fk1220391f911dfddb FOREIGN KEY (parent_iddb) REFERENCES alias_table(iddb);


--
-- TOC entry 2310 (class 2606 OID 1747039)
-- Name: fk1220391fd07dda99; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY alias_table
    ADD CONSTRAINT fk1220391fd07dda99 FOREIGN KEY (model_iddb) REFERENCES model_table(iddb);


--
-- TOC entry 2325 (class 2606 OID 1747044)
-- Name: fk143908c51b95291e; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layerline
    ADD CONSTRAINT fk143908c51b95291e FOREIGN KEY (atd_iddb) REFERENCES arrowtypedata(iddb);


--
-- TOC entry 2326 (class 2606 OID 1747049)
-- Name: fk143908c5ddccc464; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layerline
    ADD CONSTRAINT fk143908c5ddccc464 FOREIGN KEY (line_iddb) REFERENCES polylinedata(iddb);


--
-- TOC entry 2355 (class 2606 OID 1747054)
-- Name: fk19f9ea0a4f8f9d0a; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY search_index_table
    ADD CONSTRAINT fk19f9ea0a4f8f9d0a FOREIGN KEY (source_iddb) REFERENCES alias_table(iddb);


--
-- TOC entry 2356 (class 2606 OID 1747059)
-- Name: fk19f9ea0af3f38614; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY search_index_table
    ADD CONSTRAINT fk19f9ea0af3f38614 FOREIGN KEY (source_iddb) REFERENCES alias_table(iddb);


--
-- TOC entry 2353 (class 2606 OID 1747064)
-- Name: fk1ccb7638d07dda99; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY reaction_table
    ADD CONSTRAINT fk1ccb7638d07dda99 FOREIGN KEY (model_iddb) REFERENCES model_table(iddb);


--
-- TOC entry 2347 (class 2606 OID 1747069)
-- Name: fk23b8885f4910e2f9; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY point_table
    ADD CONSTRAINT fk23b8885f4910e2f9 FOREIGN KEY (iddb) REFERENCES polylinedata(iddb);


--
-- TOC entry 2336 (class 2606 OID 1747074)
-- Name: fk3197d738b8043c6b; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY model_table
    ADD CONSTRAINT fk3197d738b8043c6b FOREIGN KEY (project_iddb) REFERENCES project_table(iddb);


--
-- TOC entry 2350 (class 2606 OID 1747079)
-- Name: fk37460cc0839f2b6e; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY privilege_table
    ADD CONSTRAINT fk37460cc0839f2b6e FOREIGN KEY (user_iddb) REFERENCES user_table(iddb);


--
-- TOC entry 2360 (class 2606 OID 1747084)
-- Name: fk573952a79b59257d; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY unified_node_elements
    ADD CONSTRAINT fk573952a79b59257d FOREIGN KEY (iddb_node) REFERENCES unifiednode(iddb);


--
-- TOC entry 2361 (class 2606 OID 1747089)
-- Name: fk573952a7b6f23f93; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY unified_node_elements
    ADD CONSTRAINT fk573952a7b6f23f93 FOREIGN KEY (iddb_element) REFERENCES species_table(iddb);


--
-- TOC entry 2340 (class 2606 OID 1747094)
-- Name: fk5cb2435132fc04bd; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY node_table
    ADD CONSTRAINT fk5cb2435132fc04bd FOREIGN KEY (nodeoperatorforinput_iddb) REFERENCES node_table(iddb);


--
-- TOC entry 2341 (class 2606 OID 1747099)
-- Name: fk5cb243516a806500; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY node_table
    ADD CONSTRAINT fk5cb243516a806500 FOREIGN KEY (nodeoperatorforoutput_iddb) REFERENCES node_table(iddb);


--
-- TOC entry 2342 (class 2606 OID 1747104)
-- Name: fk5cb2435185f7f0ff; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY node_table
    ADD CONSTRAINT fk5cb2435185f7f0ff FOREIGN KEY (alias_iddb) REFERENCES alias_table(iddb);


--
-- TOC entry 2343 (class 2606 OID 1747109)
-- Name: fk5cb243519761b59d; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY node_table
    ADD CONSTRAINT fk5cb243519761b59d FOREIGN KEY (parent_iddb) REFERENCES reaction_table(iddb);


--
-- TOC entry 2344 (class 2606 OID 1747114)
-- Name: fk5cb24351ddccc464; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY node_table
    ADD CONSTRAINT fk5cb24351ddccc464 FOREIGN KEY (line_iddb) REFERENCES polylinedata(iddb);


--
-- TOC entry 2345 (class 2606 OID 1747119)
-- Name: fk5cb24351eb3afa39; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY node_table
    ADD CONSTRAINT fk5cb24351eb3afa39 FOREIGN KEY (element_iddb) REFERENCES species_table(iddb);


--
-- TOC entry 2346 (class 2606 OID 1747124)
-- Name: fk5cb24351f8b3fb7e; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY node_table
    ADD CONSTRAINT fk5cb24351f8b3fb7e FOREIGN KEY (reaction_iddb) REFERENCES reaction_table(iddb);


--
-- TOC entry 2348 (class 2606 OID 1747129)
-- Name: fk60d359aa425ee8c7; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY polylinedata
    ADD CONSTRAINT fk60d359aa425ee8c7 FOREIGN KEY (beginatd_iddb) REFERENCES arrowtypedata(iddb);


--
-- TOC entry 2349 (class 2606 OID 1747134)
-- Name: fk60d359aabc19a6f9; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY polylinedata
    ADD CONSTRAINT fk60d359aabc19a6f9 FOREIGN KEY (endatd_iddb) REFERENCES arrowtypedata(iddb);


--
-- TOC entry 2351 (class 2606 OID 1747139)
-- Name: fk71295b151586e740; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY reaction_miriam
    ADD CONSTRAINT fk71295b151586e740 FOREIGN KEY (reaction_id) REFERENCES reaction_table(iddb);


--
-- TOC entry 2352 (class 2606 OID 1747144)
-- Name: fk71295b1567cc9103; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY reaction_miriam
    ADD CONSTRAINT fk71295b1567cc9103 FOREIGN KEY (miriam_id) REFERENCES miriam_data_table(iddb);


--
-- TOC entry 2331 (class 2606 OID 1747149)
-- Name: fk83f98123be8a7411; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY mappathnode
    ADD CONSTRAINT fk83f98123be8a7411 FOREIGN KEY (element_iddb) REFERENCES unifiednode(iddb);


--
-- TOC entry 2332 (class 2606 OID 1747154)
-- Name: fk83f98123d8920019; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY mappathnode
    ADD CONSTRAINT fk83f98123d8920019 FOREIGN KEY (mappath_iddb) REFERENCES mappath(iddb);


--
-- TOC entry 2333 (class 2606 OID 1747159)
-- Name: fk83f98123f8b3fb7e; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY mappathnode
    ADD CONSTRAINT fk83f98123f8b3fb7e FOREIGN KEY (reaction_iddb) REFERENCES reaction_table(iddb);


--
-- TOC entry 2327 (class 2606 OID 1747164)
-- Name: fk873fe74ad07dda99; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layout
    ADD CONSTRAINT fk873fe74ad07dda99 FOREIGN KEY (model_iddb) REFERENCES model_table(iddb);


--
-- TOC entry 2328 (class 2606 OID 1747169)
-- Name: fk88cfbf13839f2b6e; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY log_table
    ADD CONSTRAINT fk88cfbf13839f2b6e FOREIGN KEY (user_iddb) REFERENCES user_table(iddb);


--
-- TOC entry 2329 (class 2606 OID 1747174)
-- Name: fk951607816c839650; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY mappath
    ADD CONSTRAINT fk951607816c839650 FOREIGN KEY (endnode_iddb) REFERENCES unifiednode(iddb);


--
-- TOC entry 2330 (class 2606 OID 1747179)
-- Name: fk95160781aee68e42; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY mappath
    ADD CONSTRAINT fk95160781aee68e42 FOREIGN KEY (beginnode_iddb) REFERENCES unifiednode(iddb);


--
-- TOC entry 2316 (class 2606 OID 1747184)
-- Name: fka7b17360d07dda99; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layer_table
    ADD CONSTRAINT fka7b17360d07dda99 FOREIGN KEY (model_iddb) REFERENCES model_table(iddb);


--
-- TOC entry 2311 (class 2606 OID 1747189)
-- Name: fkc75381701e595a8a; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY antisense_rna_region_table
    ADD CONSTRAINT fkc75381701e595a8a FOREIGN KEY (idspeciesdb) REFERENCES species_table(iddb);


--
-- TOC entry 2312 (class 2606 OID 1747194)
-- Name: fkc7538170a4c0d273; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY antisense_rna_region_table
    ADD CONSTRAINT fkc7538170a4c0d273 FOREIGN KEY (idspeciesdb) REFERENCES species_table(iddb);


--
-- TOC entry 2313 (class 2606 OID 1747199)
-- Name: fkc7538170f459d889; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY antisense_rna_region_table
    ADD CONSTRAINT fkc7538170f459d889 FOREIGN KEY (idspeciesdb) REFERENCES species_table(iddb);


--
-- TOC entry 2317 (class 2606 OID 1747204)
-- Name: fkcbfabea647d24b0b; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layer_table_layerline
    ADD CONSTRAINT fkcbfabea647d24b0b FOREIGN KEY (layer_table_iddb) REFERENCES layer_table(iddb);


--
-- TOC entry 2318 (class 2606 OID 1747209)
-- Name: fkcbfabea6573d6d60; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layer_table_layerline
    ADD CONSTRAINT fkcbfabea6573d6d60 FOREIGN KEY (lines_iddb) REFERENCES layerline(iddb);


--
-- TOC entry 2319 (class 2606 OID 1747214)
-- Name: fkcbfc4b0447d24b0b; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layer_table_layeroval
    ADD CONSTRAINT fkcbfc4b0447d24b0b FOREIGN KEY (layer_table_iddb) REFERENCES layer_table(iddb);


--
-- TOC entry 2320 (class 2606 OID 1747219)
-- Name: fkcbfc4b04e771479c; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layer_table_layeroval
    ADD CONSTRAINT fkcbfc4b04e771479c FOREIGN KEY (ovals_iddb) REFERENCES layeroval(iddb);


--
-- TOC entry 2321 (class 2606 OID 1747224)
-- Name: fkcbfd689647d24b0b; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layer_table_layerrect
    ADD CONSTRAINT fkcbfd689647d24b0b FOREIGN KEY (layer_table_iddb) REFERENCES layer_table(iddb);


--
-- TOC entry 2322 (class 2606 OID 1747229)
-- Name: fkcbfd68968b8fcc8b; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layer_table_layerrect
    ADD CONSTRAINT fkcbfd68968b8fcc8b FOREIGN KEY (rectangles_iddb) REFERENCES layerrect(iddb);


--
-- TOC entry 2323 (class 2606 OID 1747234)
-- Name: fkcbfe53df47d24b0b; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layer_table_layertext
    ADD CONSTRAINT fkcbfe53df47d24b0b FOREIGN KEY (layer_table_iddb) REFERENCES layer_table(iddb);


--
-- TOC entry 2324 (class 2606 OID 1747239)
-- Name: fkcbfe53dfd0c40912; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY layer_table_layertext
    ADD CONSTRAINT fkcbfe53dfd0c40912 FOREIGN KEY (texts_iddb) REFERENCES layertext(iddb);


--
-- TOC entry 2334 (class 2606 OID 1747244)
-- Name: fkcc8b66f9a1903ff9; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY miriam_data_table
    ADD CONSTRAINT fkcc8b66f9a1903ff9 FOREIGN KEY (species_iddb) REFERENCES species_table(iddb);


--
-- TOC entry 2354 (class 2606 OID 1747249)
-- Name: fkdfe14357f943f5d4; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY references_table
    ADD CONSTRAINT fkdfe14357f943f5d4 FOREIGN KEY (reference_id) REFERENCES missingconnection(iddb);


--
-- TOC entry 2337 (class 2606 OID 1747254)
-- Name: fke2a456b71e595a8a; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY modification_residue_table
    ADD CONSTRAINT fke2a456b71e595a8a FOREIGN KEY (idspeciesdb) REFERENCES species_table(iddb);


--
-- TOC entry 2338 (class 2606 OID 1747259)
-- Name: fke2a456b783a4f0d1; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY modification_residue_table
    ADD CONSTRAINT fke2a456b783a4f0d1 FOREIGN KEY (idspeciesdb) REFERENCES species_table(iddb);


--
-- TOC entry 2339 (class 2606 OID 1747264)
-- Name: fke2a456b7f6dd3c25; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY modification_residue_table
    ADD CONSTRAINT fke2a456b7f6dd3c25 FOREIGN KEY (idspeciesdb) REFERENCES species_table(iddb);


--
-- TOC entry 2335 (class 2606 OID 1747269)
-- Name: fkeecb14c4be8a7411; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY missingconnection
    ADD CONSTRAINT fkeecb14c4be8a7411 FOREIGN KEY (element_iddb) REFERENCES unifiednode(iddb);


--
-- TOC entry 2357 (class 2606 OID 1747274)
-- Name: fkf137030bb54fb17f; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY species_table
    ADD CONSTRAINT fkf137030bb54fb17f FOREIGN KEY (parent_iddb) REFERENCES species_table(iddb);


--
-- TOC entry 2358 (class 2606 OID 1747279)
-- Name: fkf137030bd07dda99; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY species_table
    ADD CONSTRAINT fkf137030bd07dda99 FOREIGN KEY (model_iddb) REFERENCES model_table(iddb);


--
-- TOC entry 2359 (class 2606 OID 1747284)
-- Name: fkf137030bf170df45; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY species_table
    ADD CONSTRAINT fkf137030bf170df45 FOREIGN KEY (complex_iddb) REFERENCES species_table(iddb);


--
-- TOC entry 2314 (class 2606 OID 1747289)
-- Name: fkf8704fa5839f2b6e; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY feedback
    ADD CONSTRAINT fkf8704fa5839f2b6e FOREIGN KEY (user_iddb) REFERENCES user_table(iddb);


--
-- TOC entry 2315 (class 2606 OID 1747294)
-- Name: fkf8704fa5d07dda99; Type: FK CONSTRAINT; Schema: public; Owner: map_viewer
--

ALTER TABLE ONLY feedback
    ADD CONSTRAINT fkf8704fa5d07dda99 FOREIGN KEY (model_iddb) REFERENCES model_table(iddb);


--
-- TOC entry 2547 (class 0 OID 0)
-- Dependencies: 7
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--
--
--REVOKE ALL ON SCHEMA public FROM PUBLIC;
--REVOKE ALL ON SCHEMA public FROM postgres;
--GRANT ALL ON SCHEMA public TO postgres;
--GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2017-09-21 13:13:48

--
-- PostgreSQL database dump complete
--

