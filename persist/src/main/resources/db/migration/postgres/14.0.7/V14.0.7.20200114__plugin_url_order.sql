alter table plugin_urls add column idx integer;
alter table plugin_urls add column idx_c varchar;
update plugin_urls set idx_c = ctid;
update plugin_urls p1 set idx = (select count(*) from plugin_urls p2 where p2.idx_c < p1.idx_c);
alter table plugin_urls drop column idx_c;
alter table plugin_urls alter COLUMN idx set not null;
CREATE SEQUENCE plugin_url_sequence;
alter table plugin_urls alter COLUMN idx set default nextval('plugin_url_sequence');
SELECT setval('plugin_url_sequence', (select max(idx)+1 from plugin_urls));