--alter table element_table drop column point_x;
--alter table element_table drop column point_y;
--alter table element_table drop column name_x;
--alter table element_table drop column name_y;
--alter table element_table drop column name_width;
--alter table element_table drop column name_height;

alter table element_table add column point_x double precision;
alter table element_table add column point_y double precision;

update element_table set point_x = cast (split_part(name_point, ',',1) as double precision);
update element_table set point_y = cast (split_part(name_point, ',',2) as double precision);

alter table element_table add column name_x double precision;
alter table element_table add column name_y double precision;
alter table element_table add column name_width double precision;
alter table element_table add column name_height double precision;

update element_table set name_x = point_x where name_horizontal_align='LEFT';
update element_table set name_x = point_x-least(point_x-x,width-(point_x-x)) where name_horizontal_align='CENTER';
update element_table set name_x = x where name_horizontal_align='RIGHT';

update element_table set name_width = width-(name_x-x) where name_horizontal_align='LEFT';
update element_table set name_width = 2*(point_x-name_x) where name_horizontal_align='CENTER';
update element_table set name_width = point_x-x where name_horizontal_align='RIGHT';

update element_table set name_y = point_y where name_vertical_align='TOP';
update element_table set name_y = point_y-least(point_y-y,height-(point_y-y)) where name_vertical_align='MIDDLE';
update element_table set name_y = y where name_vertical_align='BOTTOM';

update element_table set name_height = height-(name_y-y) where name_vertical_align='TOP';
update element_table set name_height = 2*(point_y-name_y) where name_vertical_align='MIDDLE';
update element_table set name_height = point_y-y where name_vertical_align='BOTTOM';

update element_table set name_width=1 where name_width<=0;
update element_table set name_height=1 where name_height<=0;

update element_table set name_x = x where name_x is null;
update element_table set name_y = y where name_y is null;
update element_table set name_width = width where name_width is null;
update element_table set name_height = height where name_height is null;

alter table element_table alter column name_x set not null;
alter table element_table alter column name_y set not null;
alter table element_table alter column name_width set not null;
alter table element_table alter column name_height set not null;

alter table element_table drop column name_point;
alter table element_table drop column point_x;
alter table element_table drop column point_y;
