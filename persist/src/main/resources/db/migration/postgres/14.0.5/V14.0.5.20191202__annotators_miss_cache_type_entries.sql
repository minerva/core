INSERT INTO cache_type_table (validity, class_name) 
select 365, 'lcsb.mapviewer.annotation.services.annotators.TairAnnotator' 
where not exists (select 1 from cache_type_table where class_name = 'lcsb.mapviewer.annotation.services.annotators.TairAnnotator');

INSERT INTO cache_type_table (validity, class_name) 
select 365, 'lcsb.mapviewer.annotation.services.annotators.CazyAnnotator' 
where not exists (select 1 from cache_type_table where class_name = 'lcsb.mapviewer.annotation.services.annotators.CazyAnnotator');

INSERT INTO cache_type_table (validity, class_name) 
select 365, 'lcsb.mapviewer.annotation.services.annotators.BrendaAnnotator' 
where not exists (select 1 from cache_type_table where class_name = 'lcsb.mapviewer.annotation.services.annotators.BrendaAnnotator');

INSERT INTO cache_type_table (validity, class_name) 
select 365, 'lcsb.mapviewer.annotation.services.annotators.StitchAnnotator' 
where not exists (select 1 from cache_type_table where class_name = 'lcsb.mapviewer.annotation.services.annotators.StitchAnnotator');

INSERT INTO cache_type_table (validity, class_name) 
select 365, 'lcsb.mapviewer.annotation.services.annotators.StringAnnotator' 
where not exists (select 1 from cache_type_table where class_name = 'lcsb.mapviewer.annotation.services.annotators.StringAnnotator');
