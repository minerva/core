-- coordinates for antisense rna changed into absolute x,y

alter table antisense_rna_region_table add column position varchar(255);
update antisense_rna_region_table set position=concat(element_table.x+(element_table.width*3/4*antisense_rna_region_table.pos),',',element_table.y) from  element_table where element_table.iddb = antisense_rna_region_table.idspeciesdb and not pos is null;
update antisense_rna_region_table set position=concat(element_table.x,',',element_table.y) from  element_table where element_table.iddb = antisense_rna_region_table.idspeciesdb and pos is null;
alter table antisense_rna_region_table drop column pos;

