--size is changed to width for rna region
alter table rna_region_table add column width numeric(6,2);
update rna_region_table set width=element_table.width*rna_region_table.size from  element_table where element_table.iddb = rna_region_table.idspeciesdb and not size is null;
alter table rna_region_table drop column size;

--modifications includes different type of modification (that will be resolved to different classes)
alter table modification_residue_table add column modification_type varchar(255);
update modification_residue_table set modification_type='MODIFICATION_SITE' from element_table where modification_residue_table.idspeciesdb=element_table.iddb and element_type_db='GENE_ALIAS';
update modification_residue_table set modification_type='RESIDUE' from element_table where modification_residue_table.idspeciesdb=element_table.iddb and modification_type is null;

-- move antisense rna regions into modification table
alter table modification_residue_table add column width numeric(6,2);
insert into modification_residue_table (idmodificationresidue, name, idspeciesdb, position, state, modification_type, width) select idantisensernaregion, name, idspeciesdb, position, null, type, width from antisense_rna_region_table where antisense_rna_region_table is not null;
drop table antisense_rna_region_table;

-- move rna regions into modification table
insert into modification_residue_table (idmodificationresidue, name, idspeciesdb, position, state, modification_type, width) select idrnaregion, name, idspeciesdb, position, state, type, width from rna_region_table;
update modification_residue_table set modification_type='MODIFICATION_SITE' where modification_type='Modification Site';
update modification_residue_table set modification_type='PROTEIN_BINDING_DOMAIN' where modification_type='proteinBindingDomain';
update modification_residue_table set modification_type='CODING_REGION' where modification_type='CodingRegion';
drop table rna_region_table;

--  modification can be active
alter table modification_residue_table add column active boolean;