-- add unique constraints

ALTER TABLE project_table ADD CONSTRAINT unique_project_id UNIQUE (project_id);

update user_table set login = login || '_' || cast (iddb as text) where removed = true;

ALTER TABLE user_table ADD CONSTRAINT unique_user_login UNIQUE (login);
