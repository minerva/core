--sometimes when exporting celldesigner file to cml converter produced corrupted output
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (480, 'Export to CellDesigner rarely produced corrupted CellDEsigner file', 'BUG_FIX', null, 
  (select iddb from external_user where email ='piotr.gawron@uni.lu'));

-- longfei
insert into external_user (name, surname, email) values ('Longfei', 'Mao', 'longfei.mao@uni.lu');

-- bugfix in recon
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (480, 'Recon annotations of reactions', 'BUG_FIX', 
  (select iddb from external_project where name ='RECON'), 
  (select iddb from external_user where email ='longfei.mao@uni.lu'));
