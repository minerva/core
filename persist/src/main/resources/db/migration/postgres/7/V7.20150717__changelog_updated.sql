--when user logged in and was browsing non-standard map then he was redirected to the standard map
insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (480, 'Login problem when browsing not default map', 'BUG_FIX', null, 
  (select iddb from external_user where email ='stephan.gebel@uni.lu'));
