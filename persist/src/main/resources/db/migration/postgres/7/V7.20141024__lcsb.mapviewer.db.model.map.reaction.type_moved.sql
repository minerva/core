-- fix comments table
update feedback set tablename='lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction' where tablename='lcsb.mapviewer.db.model.map.reaction.type.StateTransitionReaction';
update feedback set tablename='lcsb.mapviewer.model.map.reaction.type.TransportReaction' where tablename='lcsb.mapviewer.db.model.map.reaction.type.TransportReaction';
update feedback set tablename='lcsb.mapviewer.model.map.layout.alias.SpeciesAlias' where tablename='lcsb.mapviewer.db.model.map.layout.alias.SpeciesAlias';
update feedback set tablename='lcsb.mapviewer.model.map.reaction.type.UnknownTransitionReaction' where tablename='lcsb.mapviewer.db.model.map.reaction.type.UnknownTransitionReaction';
