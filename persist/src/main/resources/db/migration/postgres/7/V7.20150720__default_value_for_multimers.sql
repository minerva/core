--set default value for multimers from previus version of the system
update species_table set homodimer = 1 where homodimer is null;

-- alexander
insert into external_user (name, surname, email) values ('Alexander', 'Mazein', 'amazein@eisbm.org');

insert into changelog (svnversion, description, type, requestproject_iddb, requestuser_iddb) values 
  (480, 'Multimers are available for every element', 'BUG_FIX', null, 
  (select iddb from external_user where email ='amazein@eisbm.org'));
  
