-- change cache query

ALTER TABLE cachequery ADD COLUMN type integer;

update cachequery set type =0 where substring (query from 0 for position(chr(10) in query)) = 'lcsb.mapviewer.reactome.model.ReactomeCandidateSet';
update cachequery set type =0 where substring (query from 0 for position(chr(10) in query)) = 'lcsb.mapviewer.reactome.model.ReactomeCatalystActivity';
update cachequery set type =0 where substring (query from 0 for position(chr(10) in query)) = 'lcsb.mapviewer.reactome.model.ReactomeComplex';
update cachequery set type =0 where substring (query from 0 for position(chr(10) in query)) = 'lcsb.mapviewer.reactome.model.ReactomeDatabaseObject';
update cachequery set type =0 where substring (query from 0 for position(chr(10) in query)) = 'lcsb.mapviewer.reactome.model.ReactomeEntitySet';
update cachequery set type =0 where substring (query from 0 for position(chr(10) in query)) = 'lcsb.mapviewer.reactome.model.ReactomeEntityWithAccessionedSequence';
update cachequery set type =0 where substring (query from 0 for position(chr(10) in query)) = 'lcsb.mapviewer.reactome.model.ReactomeEvent';
update cachequery set type =0 where substring (query from 0 for position(chr(10) in query)) = 'lcsb.mapviewer.reactome.model.ReactomeLiteratureReference';
update cachequery set type =0 where substring (query from 0 for position(chr(10) in query)) = 'lcsb.mapviewer.reactome.model.ReactomePhysicalEntity';
update cachequery set type =0 where substring (query from 0 for position(chr(10) in query)) = 'lcsb.mapviewer.reactome.model.ReactomeReactionlikeEvent';
update cachequery set type =0 where substring (query from 0 for position(chr(10) in query)) = 'lcsb.mapviewer.reactome.model.ReactomeReferenceMolecule';
update cachequery set type =0 where substring (query from 0 for position(chr(10) in query)) = 'lcsb.mapviewer.reactome.model.ReactomeReferenceSequence';
update cachequery set type =0 where substring (query from 0 for position(chr(10) in query)) = 'lcsb.mapviewer.reactome.model.ReactomeSimpleEntity';
update cachequery set type =0 where substring (query from 0 for position(chr(10) in query)) = 'lcsb.mapviewer.reactome.model.ReactomeStableIdentifier';
update cachequery set type =1 where substring (query from 0 for position(chr(10) in query)) = 'lcsb.mapviewer.utils.annotation.AnnotationRestService';
update cachequery set type =2 where substring (query from 0 for position(chr(10) in query)) = 'lcsb.mapviewer.utils.annotation.ChEMBLParser';
update cachequery set type =3 where substring (query from 0 for position(chr(10) in query)) = 'lcsb.mapviewer.utils.annotation.data.Article';
update cachequery set type =4 where substring (query from 0 for position(chr(10) in query)) = 'lcsb.mapviewer.utils.annotation.DrugbankHTMLParser';
update cachequery set type =5 where substring (query from 0 for position(chr(10) in query)) = 'lcsb.mapviewer.utils.annotation.IdConverter';

ALTER TABLE cachequery ALTER COLUMN type set not null;

update cachequery set query= substring(query from position(chr(10) in query)+1)  where position(chr(10) in query)>0;

ALTER TABLE cachequery ADD COLUMN expires timestamp without time zone;

update cachequery set expires = accessed + interval '42 days';

-- and now entry

ALTER TABLE cacheentry ADD COLUMN type integer;

update cacheentry set type =0;

ALTER TABLE cacheentry ALTER COLUMN type set not null;

ALTER TABLE cacheentry ADD COLUMN expires timestamp without time zone;

update cacheentry set expires = accessed + interval '42 days';

ALTER TABLE cacheentry ADD COLUMN customid integer;
update cacheentry set customid = iddb;

-- set auto increment id
CREATE SEQUENCE cacheentry_iddb_seq  INCREMENT 1  MINVALUE 1  MAXVALUE 9223372036854775807  START 1  CACHE 1;
delete from cacheentry;
ALTER TABLE cacheentry ALTER COLUMN iddb SET DEFAULT nextval('cacheentry_iddb_seq'::regclass);

--recreate search index
drop index if exists query_index;
CREATE INDEX query_index
  ON cachequery
  USING btree
  (type, query COLLATE pg_catalog."default");
