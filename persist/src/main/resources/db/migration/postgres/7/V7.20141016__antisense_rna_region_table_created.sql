-- fix type in antisense rna region
alter table antisense_rna_region_table ADD COLUMN type_int integer;

update  antisense_rna_region_table set type_int = 0 where type='Modification Site';
update  antisense_rna_region_table set type_int = 1 where type='CodingRegion';
update  antisense_rna_region_table set type_int = 2 where type='proteinBindingDomain';

alter table antisense_rna_region_table drop column type;
alter table antisense_rna_region_table rename type_int to type;

--change names in modification residue db identifiers
alter table modification_residue_table rename idModificationResidueDb to iddb;
alter table modification_residue_table rename id to idModificationResidue;
