--postion to compartment changed into enum
update species_table set positiontocompartment='INSIDE' where positiontocompartment = 'inside';
update species_table set positiontocompartment='INSIDE_OF_MEMBRANE' where positiontocompartment = 'insideOfMembrane';
update species_table set positiontocompartment='TRANSMEMBRANE' where positiontocompartment = 'transmembrane';
update species_table set positiontocompartment='OUTER_SURFACE' where positiontocompartment = 'outerSurface';
update species_table set positiontocompartment='INNER_SURFACE' where positiontocompartment = 'innerSurface';
