alter table layout add column layout_iddb integer;
alter table layout add constraint layout_layout_fk FOREIGN KEY (layout_iddb) REFERENCES layout (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
alter table layout rename column layout_iddb to parentlayout_iddb;