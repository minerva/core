-- remove old IdConverter class
delete from cache_type where classname ='lcsb.mapviewer.annotation.services.IdConverter';

-- and data that was sotred for this converter
delete from cachequery where type = 5;

-- add foreign key that was missing
alter table cachequery add constraint cachequery_type_fk foreign key(type) 
	references cache_type(iddb) match simple
	ON UPDATE NO ACTION ON DELETE NO ACTION;

