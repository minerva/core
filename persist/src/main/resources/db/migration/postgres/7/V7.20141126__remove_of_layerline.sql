--remove of layerline class
alter table layer_table_layerline rename to layer_table_polylinedata;
alter table layer_table_polylinedata drop CONSTRAINT fkcbfabea6573d6d60;
alter table layer_table_polylinedata add CONSTRAINT fkcbfabea6573d6d60 FOREIGN KEY (lines_iddb)
      REFERENCES polylinedata (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
drop table layerline;

--change in data mining interface

alter table missingconnection rename to datamining;
alter table missingconnection_miriam_data_table rename to datamining_miriam_data_table;
alter table datamining drop column name;
alter table datamining drop column link;
alter table datamining drop column score;
delete from datamining_miriam_data_table;
delete from datamining;
alter table datamining drop CONSTRAINT fkeecb14c4be8a7411;
alter table datamining add CONSTRAINT fkeecb14c4be8a7411 FOREIGN KEY (element_iddb)
      REFERENCES species_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
alter table datamining_miriam_data_table rename column missingconnection_iddb to datamining_iddb;
CREATE TABLE datamining_suggested_connections
(
  idx integer NOT NULL,
  datamining_iddb integer NOT NULL,
  suggestedconnections_iddb integer NOT NULL,
  CONSTRAINT datamining_suggested_connections_table_miriam_pkey PRIMARY KEY (datamining_iddb, idx),
  CONSTRAINT datamining_suggested_connections_table_foreign_key FOREIGN KEY (datamining_iddb)
      REFERENCES datamining (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT datamining_suggested_connections_table_foreign_miriam_key FOREIGN KEY (suggestedconnections_iddb)
      REFERENCES miriam_data_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT datamining_suggested_connections_table_unique_miriam UNIQUE (suggestedconnections_iddb)
  );  
drop table unified_node_elements;
drop table unifiednode;