--simplify model by removing CD specific identifiers
alter table species_table drop column antisensernaid;
alter table species_table drop column degradedid;
alter table species_table drop column drugid; 
alter table species_table drop column geneid; 
alter table species_table drop column ionid;
alter table species_table drop column phenotypeid;
alter table species_table drop column idprotein; 
alter table species_table drop column rnaid;
alter table species_table drop column simplemoleculeid;
alter table species_table drop column unknownid;