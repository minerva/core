-- removing cacheentry
drop table cacheentry;
drop sequence cacheentry_iddb_seq;

-- merging feedback and comments
alter table feedback drop column feedback_type;

-- species alias and compartment alias contain common element
alter table alias_table ADD COLUMN idelementdb integer;
update alias_table set idelementdb=idspeciesdb where idspeciesdb is not null;
update alias_table set idelementdb=idcompartmentdb where idcompartmentdb is not null;

ALTER TABLE alias_table DROP CONSTRAINT fk1220391f1e595a8a;
ALTER TABLE alias_table DROP CONSTRAINT fk1220391f5720a4be;

ALTER TABLE alias_table
  ADD CONSTRAINT fkalias_element FOREIGN KEY (idelementdb)
      REFERENCES species_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

alter table alias_table drop COLUMN idspeciesdb;
alter table alias_table drop COLUMN idcompartmentdb;

--rename column name in miriam data

alter table miriam_data_table rename species_iddb to element_iddb;
 