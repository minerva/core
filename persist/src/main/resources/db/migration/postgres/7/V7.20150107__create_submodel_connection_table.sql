-- create submodel connection table 
create table submodel_connection_table
(
	submodel_connections_type_db character varying(31) NOT NULL,
	iddb serial NOT NULL,
	name character varying(255),
	submodel_iddb integer,
	type character varying(31) NOT NULL,
	parentmodel_iddb integer,
	fromalias_iddb integer,
	toalias_iddb integer,
	CONSTRAINT submodel_connection_table_pkey PRIMARY KEY (iddb),
	CONSTRAINT fk_submodel_connection_model FOREIGN KEY (submodel_iddb)
      REFERENCES model_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_parent_model_connection_model FOREIGN KEY (parentmodel_iddb)
      REFERENCES model_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_submodel_connection_from_alias FOREIGN KEY (fromalias_iddb)
      REFERENCES alias_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT fk_submodel_connection_to_alias FOREIGN KEY (toalias_iddb)
      REFERENCES alias_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

-- create submodel connection in alias table
alter table alias_table add column submodel_iddb integer;
-- add foreign key
alter table alias_table add constraint fk_alias_submodel FOREIGN KEY (submodel_iddb)
    REFERENCES submodel_connection_table (iddb) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION;
   
-- modify data in log table due to reorganization of model package structure
update log_table set tablename='lcsb.mapviewer.model.map.model.ModelFullIndexed' where tablename like 'lcsb.mapviewer.model.map.ModelFullIndexed';
