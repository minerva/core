alter TABLE minerva_job_table add column job_finished timestamp without time zone;
alter TABLE minerva_job_table add column job_started timestamp without time zone;
alter TABLE minerva_job_table add column logs text;
alter TABLE minerva_job_table add column priority integer default 10 not null;
