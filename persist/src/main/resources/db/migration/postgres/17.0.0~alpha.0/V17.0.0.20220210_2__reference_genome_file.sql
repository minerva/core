alter table reference_genome_table add column file_id integer;

ALTER TABLE reference_genome_table
    ADD CONSTRAINT reference_genome_table_file_fk FOREIGN KEY (file_id) REFERENCES public.file_entry_table(id)
    ON DELETE SET NULL;

update reference_genome_table set file_id = 
(select max(id) from file_entry_table where 
  file_entry_table.url=reference_genome_table.source_url and 
  file_entry_table.download_progress>90); 

alter table reference_genome_gene_mapping_table add column file_id integer;

ALTER TABLE reference_genome_gene_mapping_table
    ADD CONSTRAINT reference_genome_gene_mapping_table_file_fk FOREIGN KEY (file_id) REFERENCES public.file_entry_table(id)
    ON DELETE SET NULL;

update reference_genome_gene_mapping_table set file_id = 
(select max(id) from file_entry_table where 
  file_entry_table.url=reference_genome_gene_mapping_table.source_url and 
  file_entry_table.download_progress>90); 

  