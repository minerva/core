update polyline_data_table set end_atd=replace (end_atd,';SOLID_BOLD',';SOLID') where end_atd like '%;SOLID_BOLD' ;
update polyline_data_table set begin_atd=replace (begin_atd,';SOLID_BOLD',';SOLID') where begin_atd like '%;SOLID_BOLD' ;
update polyline_data_table set type='SOLID' where type ='SOLID_BOLD';

update polyline_data_table set end_atd=replace (end_atd,';DASHED_BOLD',';DASHED') where end_atd like '%;DASHED_BOLD' ;
update polyline_data_table set begin_atd=replace (begin_atd,';DASHED_BOLD',';DASHED') where begin_atd like '%;DASHED_BOLD' ;
update polyline_data_table set type='DASHED' where type ='DASHED_BOLD';
