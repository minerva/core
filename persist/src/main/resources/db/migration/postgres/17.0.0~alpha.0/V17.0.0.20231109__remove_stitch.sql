delete from cache_query_table where type in (select id from cache_type_table where class_name ='lcsb.mapviewer.annotation.services.annotators.StitchAnnotator');
delete from cache_type_table where class_name ='lcsb.mapviewer.annotation.services.annotators.StitchAnnotator';
delete from annotator_parameter_table where annotator_data_id in (select id from annotator_data_table where annotator_class_name ='lcsb.mapviewer.annotation.services.annotators.StitchAnnotator');
delete from annotator_data_table where annotator_class_name ='lcsb.mapviewer.annotation.services.annotators.StitchAnnotator';