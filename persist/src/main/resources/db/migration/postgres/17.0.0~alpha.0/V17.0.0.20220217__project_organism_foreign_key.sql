ALTER TABLE public.project_table DROP CONSTRAINT project_table_miriam;

ALTER TABLE public.project_table
    ADD CONSTRAINT project_table_miriam FOREIGN KEY (disease_id) REFERENCES public.miriam_data_table(id)
    ON DELETE SET NULL;

ALTER TABLE public.project_table DROP CONSTRAINT project_organism_constraint;
    
ALTER TABLE public.project_table
    ADD CONSTRAINT project_organism_constraint FOREIGN KEY (organism_id) REFERENCES public.miriam_data_table(id)
    ON DELETE SET NULL;
    
alter table project_log_entry_table add column entity_version bigint default 0 not null;
alter table overview_image_table add column entity_version bigint default 0 not null;
alter table user_table add column entity_version bigint default 0 not null;
alter table privilege_table add column entity_version bigint default 0 not null;
alter table model_data_table add column entity_version bigint default 0 not null;
alter table project_background_table add column entity_version bigint default 0 not null;
alter table element_table add column entity_version bigint default 0 not null;
alter table user_annotation_schema_table add column entity_version bigint default 0 not null;
alter table sbml_parameter_table add column entity_version bigint default 0 not null;
alter table sbml_unit_table add column entity_version bigint default 0 not null;
alter table article_table add column entity_version bigint default 0 not null;
alter table reaction_table add column entity_version bigint default 0 not null;
alter table polyline_data_table add column entity_version bigint default 0 not null;
alter table data_overlay_table add column entity_version bigint default 0 not null;
alter table sbml_function_table add column entity_version bigint default 0 not null;
alter table overview_link_table add column entity_version bigint default 0 not null;
alter table plugin_table add column entity_version bigint default 0 not null;
alter table plugin_data_entry_table add column entity_version bigint default 0 not null;
alter table minerva_job_table add column entity_version bigint default 0 not null;
alter table configuration_option_table add column entity_version bigint default 0 not null;
alter table file_entry_table add column entity_version bigint default 0 not null;
alter table comment_table add column entity_version bigint default 0 not null;
alter table reference_genome_table add column entity_version bigint default 0 not null;
alter table reference_genome_gene_mapping_table add column entity_version bigint default 0 not null;
alter table cache_type_table add column entity_version bigint default 0 not null;
alter table cache_query_table add column entity_version bigint default 0 not null;
alter table glyph_table add column entity_version bigint default 0 not null;
alter table reset_password_token_table add column entity_version bigint default 0 not null;
alter table reaction_node_table add column entity_version bigint default 0 not null;
