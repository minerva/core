CREATE SEQUENCE structural_state_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE structural_state_table
(
  id integer NOT NULL DEFAULT nextval('structural_state_sequence'::regclass),
  value varchar NOT NULL,
  position varchar NOT NULL,
  width numeric(6,2) NOT NULL,
  height numeric(6,2) NOT NULL,
  font_size numeric(6,2) NOT NULL,
  species_id integer NOT NULL,

  CONSTRAINT structural_state_pk PRIMARY KEY (id),
  CONSTRAINT structural_state_table_species_fk FOREIGN KEY (species_id)
      REFERENCES element_table (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

alter table element_table add column structural_state_id integer;
alter table element_table add constraint element_table_structural_state_fk FOREIGN KEY (structural_state_id)
      REFERENCES structural_state_table (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

insert into structural_state_table (position, width, height, value, font_size, species_id) select
  concat(x+width/2-least(greatest(60, 60+4*(length(structural_state)-7)), width)/2,',', y-10), --position 
  least(greatest(60, 60+4*(length(structural_state)-7)), width), --width
  20, --height
  structural_state, --state
  10, --font_size
  id
  from element_table where structural_state is not null;

update element_table set structural_state_id = (select id from structural_state_table where species_id = element_table.id);

alter table element_table drop column structural_state;
