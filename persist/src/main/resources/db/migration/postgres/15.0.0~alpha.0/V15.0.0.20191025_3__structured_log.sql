CREATE SEQUENCE project_log_entry_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE project_log_entry_table
(
  id integer NOT NULL DEFAULT nextval('project_log_entry_sequence'::regclass),
  severity varchar NOT NULL,
  type varchar NOT NULL,
  object_identifier varchar,
  object_class varchar,
  map_name varchar,
  project_id integer not null,
  content text,

  CONSTRAINT project_log_entry_pk PRIMARY KEY (id),
  CONSTRAINT project_log_entry_project_fk FOREIGN KEY (project_id)
      REFERENCES project_table (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

insert into project_log_entry_table (content, project_id, severity, type) SELECT s.token, id, 'WARNING', 'OTHER' from project_table t, unnest(string_to_array(t.warnings, E'\n')) s(token);
insert into project_log_entry_table (content, project_id, severity, type) SELECT s.token, id, 'ERROR', 'OTHER' from project_table t, unnest(string_to_array(t.errors, E'\n')) s(token);
alter table project_table drop column warnings;
alter table project_table drop column errors;
alter table project_log_entry_table add column source varchar;
