-- alignment columns
alter table element_table add column name_horizontal_align varchar;
alter table element_table add column name_vertical_align varchar;

update element_table set name_horizontal_align='LEFT' where name_point is not null;
update element_table set name_vertical_align='TOP' where name_point is not null;

update element_table set name_horizontal_align='CENTER' where name_point is null;
update element_table set name_vertical_align='MIDDLE' where name_point is null;

--temporary coordinates
alter table element_table add column name_x double precision;
alter table element_table add column name_y double precision;

--coordinates of complex text
update element_table set name_x = x+(width/2) where element_type_db='COMPLEX';
update element_table set name_y = y+height - font_size where element_type_db='COMPLEX';

--coordinates of other species
update element_table set name_x = x+(width/2) where name_x is null and name_point is null;
update element_table set name_y = y+(height/2) where name_y is null and name_point is null;

update element_table set name_point = concat(name_x,',', name_y) where name_point is null;

alter table element_table drop column name_x;
alter table element_table drop column name_y;

--not null
alter table element_table alter column name_point set not null;
alter table element_table alter column name_horizontal_align set not null;
alter table element_table alter column name_vertical_align set not null;
