-- column which indicates that this overlay should be shown on startup 
alter table layout add column defaultoverlay boolean default false;

-- default positioning of map when session data is not available
alter table model_table add column defaultzoomlevel integer default null;
alter table model_table add column defaultcenterx double precision default null;
alter table model_table add column defaultcentery double precision default null;
