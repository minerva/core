-- version moved from model into project
alter table project_table add column version character varying default '';
update project_table set version = model_table.mapversion from model_table where project_table.iddb = model_table.project_iddb;
alter table model_table drop column mapversion;
