-- images moved to project
alter table overview_image_table add column project_iddb integer;
update overview_image_table set project_iddb = model_table.project_iddb from model_table where model_table.iddb = overview_image_table.model_iddb;
delete from overview_image_table where project_iddb is null;
alter table overview_image_table drop column model_iddb;

-- project file moved to downloaded files
alter table project_table add column file_entry_iddb integer;
DO $$ 
DECLARE 
	project 	RECORD;
	last_id 	INT;
BEGIN
  FOR project IN SELECT * FROM project_table where inputData is not null LOOP
	insert into file_entry (filecontent, file_type_db, originalfilename) values
	  (project.inputdata, 'UPLOADED_FILE_ENTRY',project.inputfilename) returning iddb into last_id;
	
  	update project_table set file_entry_iddb = last_id where iddb = project.iddb ;
  	RAISE NOTICE 'Calling cs_create_job(%, %)', project.iddb, last_id ;
  END LOOP;
END $$;

alter table project_table drop column inputdata;
alter table project_table drop column inputfilename;
