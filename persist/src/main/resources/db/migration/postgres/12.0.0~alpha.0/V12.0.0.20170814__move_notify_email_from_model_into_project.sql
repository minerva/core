-- notify email from model into project
alter table project_table add column notifyemail character varying default '';
update project_table set notifyemail = model_table.notifyemail from model_table where project_table.iddb = model_table.project_iddb;
alter table model_table drop column notifyemail;
