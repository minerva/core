alter table structural_state_table add column z integer;
update structural_state_table set z = (select z+1 from element_table where structural_state_id=structural_state_table.id);
update structural_state_table set z = 0 where z is null;
alter table structural_state_table alter column z set not null;

alter table modification_residue_table add column z integer;
update modification_residue_table set z = (select z+1 from element_table where element_table.id=modification_residue_table.id_species_db);
update modification_residue_table set z = 0 where z is null;
alter table modification_residue_table alter column z set not null;
