alter table model_data_table add column creation_date timestamp without time zone;

CREATE SEQUENCE author_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE author_table (
  id integer NOT NULL DEFAULT nextval('author_sequence'::regclass),
  first_name varchar(255),
  last_name varchar(255),
  email varchar(255),
  organisation varchar(255),
  CONSTRAINT author_table_pkey PRIMARY KEY (id)
);


CREATE TABLE model_data_table_authors (
    model_data_id integer NOT NULL,
    author_id integer NOT NULL,
    idx integer NOT NULL
);

ALTER TABLE ONLY model_data_table_authors
    ADD CONSTRAINT model_data_table_authors_pkey PRIMARY KEY (model_data_id, idx);

ALTER TABLE ONLY model_data_table_authors
    ADD CONSTRAINT model_data_table_authors_model_fk FOREIGN KEY (model_data_id) REFERENCES model_data_table(id);

ALTER TABLE ONLY model_data_table_authors
    ADD CONSTRAINT model_data_table_authors_author_fk FOREIGN KEY (author_id) REFERENCES author_table(id);

    
CREATE TABLE model_data_modification_dates
(
  idx integer NOT NULL,
  model_data_id integer not null,
  modification_date timestamp without time zone, 
  
  CONSTRAINT model_data_modification_dates_model_fk FOREIGN KEY (model_data_id)
      REFERENCES model_data_table (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
  
);
