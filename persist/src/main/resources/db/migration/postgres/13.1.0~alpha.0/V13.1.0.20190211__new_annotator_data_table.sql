CREATE SEQUENCE annotator_data_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE annotator_data_table
(
  id integer NOT NULL DEFAULT nextval('annotator_data_sequence'::regclass),
  order_index integer not null,
  annotator_class_name character varying(255),
  user_class_annotators_id integer,

  CONSTRAINT annotator_data_table_pk PRIMARY KEY (id),
  CONSTRAINT annotator_data_table_user_class_annotators_fk FOREIGN KEY (user_class_annotators_id)
      REFERENCES user_class_annotators_table (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
