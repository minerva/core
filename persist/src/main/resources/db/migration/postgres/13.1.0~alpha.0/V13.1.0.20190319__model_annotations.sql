CREATE TABLE model_miriam_data_table
(
  model_id integer NOT NULL,
  miriam_data_id integer not null,

  CONSTRAINT model_miriam_data_table_modle_fk FOREIGN KEY (model_id)
      REFERENCES model_data_table (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT model_miriam_data_table_miriam_data_fk FOREIGN KEY (miriam_data_id)
      REFERENCES miriam_data_table (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
