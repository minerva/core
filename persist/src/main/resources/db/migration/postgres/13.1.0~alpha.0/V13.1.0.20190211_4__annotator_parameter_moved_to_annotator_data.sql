ALTER TABLE annotator_parameter_table drop COLUMN annotation_schema_id;
delete from annotator_parameter_table;
alter table annotator_parameter_table add column annotator_data_id integer not null;

ALTER TABLE annotator_parameter_table 
ADD CONSTRAINT annotator_parameter_table_annotator_data_fk FOREIGN KEY (annotator_data_id) REFERENCES annotator_data_table (id);
