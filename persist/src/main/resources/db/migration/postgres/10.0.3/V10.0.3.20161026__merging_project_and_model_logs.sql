--merging project and model logs
alter table project_table add column warnings text;
update project_table set warnings = (select creationwarnings from model_table  where project_iddb = project_table.iddb);

alter table model_table drop column creationwarnings;
