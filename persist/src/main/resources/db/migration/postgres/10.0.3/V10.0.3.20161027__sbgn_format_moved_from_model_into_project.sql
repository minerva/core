--sbgn format moved from model into project
alter table project_table add column sbgnformat boolean default false ;
update project_table set sbgnformat = (select sbgnformat from model_table  where project_iddb = project_table.iddb);

alter table model_table drop column sbgnformat;

--PrivilegeType should be mapped in database by name not by index
alter table privilege_table add column type_string character varying;
update privilege_table set type_string = 'VIEW_PROJECT' where type = 0;
update privilege_table set type_string = 'ADD_MAP' where type = 1;
update privilege_table set type_string = 'EDIT_MISSING_CONNECTIONS_PROJECT' where type = 2;
update privilege_table set type_string = 'EDIT_COMMENTS_PROJECT' where type = 3;
update privilege_table set type_string = 'DRUG_TARGETING_ADVANCED_VIEW_PROJECT' where type = 4;
update privilege_table set type_string = 'PROJECT_MANAGEMENT' where type = 5;
update privilege_table set type_string = 'USER_MANAGEMENT' where type = 6;
update privilege_table set type_string = 'CUSTOM_LAYOUTS' where type = 7;
update privilege_table set type_string = 'LAYOUT_VIEW' where type = 8;
update privilege_table set type_string = 'CONFIGURATION_MANAGE' where type = 9;
update privilege_table set type_string = 'LAYOUT_MANAGEMENT' where type = 10;
update privilege_table set type_string = 'MANAGE_GENOMES' where type = 11;
alter table privilege_table drop column type;
alter table privilege_table rename type_string   TO type;
