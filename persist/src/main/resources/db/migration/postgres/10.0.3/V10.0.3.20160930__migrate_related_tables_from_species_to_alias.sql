-- modification residues
	CREATE SEQUENCE modification_residue_table_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 51016
  CACHE 1;

CREATE TABLE modification_residue_table_new
(
  iddb integer NOT NULL DEFAULT nextval('modification_residue_table_iddb_seq'::regclass),
  angle double precision,
  idmodificationresidue character varying(255),
  name character varying(255),
  side character varying(255),
  size double precision,
  idspeciesdb integer NOT NULL,
  state integer,
  CONSTRAINT modification_residue_pkey PRIMARY KEY (iddb),
  CONSTRAINT modification_residue_species_fk FOREIGN KEY (idspeciesdb)
      REFERENCES public.alias_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

INSERT INTO modification_residue_table_new (angle, idmodificationresidue, name, side, size, idspeciesdb,state) select modification_residue_table.angle, modification_residue_table.idmodificationresidue, modification_residue_table.name, modification_residue_table.side, modification_residue_table.size, alias_table.iddb, modification_residue_table.state from 
alias_table inner join species_table on alias_table.idelementdb = species_table.iddb inner join modification_residue_table on species_table.iddb = modification_residue_table.idspeciesdb;

drop table modification_residue_table;

-- mapping between miriam data and element
CREATE TABLE public.element_miriam
(
  element_id integer NOT NULL,
  miriam_id integer NOT NULL,
  CONSTRAINT element_miriam_pkey PRIMARY KEY (element_id, miriam_id),
  CONSTRAINT element_miriam_element_fk FOREIGN KEY (element_id)
      REFERENCES public.alias_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT element_miriam_miriam_fk FOREIGN KEY (miriam_id)
      REFERENCES public.miriam_data_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

-- migrate data
insert into element_miriam (miriam_id, element_id) select miriam_data_table.iddb, alias_table.iddb from 
alias_table inner join species_table on alias_table.idelementdb = species_table.iddb inner join miriam_data_table on species_table.iddb = miriam_data_table.element_iddb;

alter table miriam_data_table drop column element_iddb;

-- former symbol list
CREATE TABLE element_former_symbols_new
(
  iddb integer NOT NULL,
  idx integer NOT NULL,
  symbol character varying(255),
  CONSTRAINT element_former_symbols_pk PRIMARY KEY (iddb, idx),
  CONSTRAINT element_former_symbols_fk FOREIGN KEY (iddb)
      REFERENCES alias_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

insert into element_former_symbols_new (iddb, idx, symbol) 
select alias_table.iddb, element_former_symbols.idx, element_former_symbols.symbol from 
alias_table inner join species_table on alias_table.idelementdb = species_table.iddb inner join element_former_symbols on species_table.iddb = element_former_symbols.iddb;

drop table element_former_symbols;

---synonyms list
CREATE TABLE element_synonyms_new
(
  iddb integer NOT NULL,
  idx integer NOT NULL,
  synonym character varying(255),
  CONSTRAINT element_synonyms_pk PRIMARY KEY (iddb, idx),
  CONSTRAINT element_synonyms_fk FOREIGN KEY (iddb)
      REFERENCES alias_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

insert into element_synonyms_new (iddb, idx, synonym) 
select alias_table.iddb, element_synonyms.idx, element_synonyms.synonym from 
alias_table inner join species_table on alias_table.idelementdb = species_table.iddb inner join element_synonyms on species_table.iddb = element_synonyms.iddb;

drop table element_synonyms;

--- antisense rna regions
CREATE TABLE antisense_rna_region_table_new
(
  iddb integer NOT NULL DEFAULT nextval('antisense_rna_region_table_iddb_seq'::regclass),
  idantisensernaregion character varying(255),
  name character varying(255),
  pos double precision,
  size double precision,
  idspeciesdb integer,
  state integer,
  type integer,
  CONSTRAINT antisense_rna_region_table_pk PRIMARY KEY (iddb),
  CONSTRAINT antisense_rna_region_table_fk FOREIGN KEY (idspeciesdb)
      REFERENCES alias_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

INSERT INTO antisense_rna_region_table_new ( idantisensernaregion, name, pos, size, idspeciesdb,state,type) 
select antisense_rna_region_table.idantisensernaregion, antisense_rna_region_table.name, antisense_rna_region_table.pos, antisense_rna_region_table.size, alias_table.iddb, antisense_rna_region_table.state, antisense_rna_region_table.type from 
alias_table inner join species_table on alias_table.idelementdb = species_table.iddb inner join antisense_rna_region_table on species_table.iddb = antisense_rna_region_table.idspeciesdb;

drop table antisense_rna_region_table;

-- rna
-- modification residues
CREATE SEQUENCE rna_region_table_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 51016
  CACHE 1;

CREATE TABLE rna_region_table_new
(
  iddb integer NOT NULL DEFAULT nextval('rna_region_table_iddb_seq'::regclass),
  idrnaregion character varying(255),
  name character varying(255),
  pos double precision,
  size double precision,
  idspeciesdb integer,
  state integer,
  type character varying(255),
  CONSTRAINT rna_region_table_pk PRIMARY KEY (iddb),
  CONSTRAINT rna_region_table_fk FOREIGN KEY (idspeciesdb)
      REFERENCES alias_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

INSERT INTO rna_region_table_new ( idrnaregion, name, pos, size, idspeciesdb,state) 
select rna_region_table.idrnaregion, rna_region_table.name, rna_region_table.pos, rna_region_table.size, alias_table.iddb, rna_region_table.state from 
alias_table inner join species_table on alias_table.idelementdb = species_table.iddb inner join rna_region_table on species_table.iddb = rna_region_table.idspeciesdb;

drop table rna_region_table;
drop sequence rna_region_table_idrnaregiondb_seq;
