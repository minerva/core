-- data mining
CREATE SEQUENCE datamining_table_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 51016
  CACHE 1;

CREATE TABLE datamining_table
(
  iddb integer NOT NULL DEFAULT nextval('datamining_table_iddb_seq'::regclass),
  old_idbb integer,
  description text,
  type integer,
  element_iddb integer NOT NULL,
  CONSTRAINT missingconnection_pk PRIMARY KEY (iddb),
  CONSTRAINT missingconnection_fk FOREIGN KEY (element_iddb)
      REFERENCES alias_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

INSERT INTO datamining_table ( old_idbb, description, type, element_iddb) 
select datamining.iddb, datamining.description, datamining.type, alias_table.iddb as element_iddb from 
alias_table inner join species_table on alias_table.idelementdb = species_table.iddb inner join datamining on species_table.iddb = datamining.element_iddb;

CREATE TABLE datamining_references
(
  idx integer NOT NULL,
  datamining_iddb integer NOT NULL,
  references_iddb integer NOT NULL,
  CONSTRAINT datamining_references_pk PRIMARY KEY (datamining_iddb, idx),
  CONSTRAINT datamining_references_fk_data_mining FOREIGN KEY (datamining_iddb)
      REFERENCES datamining_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT datamining_references_fk_miriam FOREIGN KEY (references_iddb)
      REFERENCES miriam_data_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

INSERT INTO datamining_references ( idx, datamining_iddb, references_iddb) 
select datamining_miriam_data_table.idx, datamining_table.iddb, datamining_miriam_data_table.references_iddb from datamining_table inner join datamining_miriam_data_table on datamining_table.old_idbb = datamining_miriam_data_table.datamining_iddb ;

drop table datamining_miriam_data_table;
alter table datamining_references rename column datamining_iddb  to datamining_table_iddb ;


drop table if exists datamining_connections;
CREATE TABLE datamining_connections
(
  idx integer NOT NULL,
  datamining_table_iddb integer NOT NULL,
  suggestedconnections_iddb integer NOT NULL,
  CONSTRAINT datamining_connections_pk PRIMARY KEY (datamining_table_iddb, idx),
  CONSTRAINT datamining_connections_fk_data_mining FOREIGN KEY (datamining_table_iddb)
      REFERENCES datamining_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT datamining_connections_fk_miriam FOREIGN KEY (suggestedconnections_iddb)
      REFERENCES miriam_data_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

INSERT INTO datamining_connections ( idx, datamining_table_iddb, suggestedconnections_iddb) 
select datamining_suggested_connections.idx, datamining_table.iddb, datamining_suggested_connections.suggestedconnections_iddb from datamining_table inner join datamining_suggested_connections on datamining_table.old_idbb = datamining_suggested_connections.datamining_iddb ;

drop table datamining_suggested_connections;

alter table datamining_table drop column old_idbb;

drop table datamining;

drop table associated_phenotype_element_table;

drop table external_project;
drop table external_user;

-- clean info about annotations
delete from class_annotator_annotators_table  ;
delete from class_annotator_table  ;
delete from class_required_annotation_miriam_type_table  ;
delete from class_required_annotation_table  ;
delete from class_valid_annotation_miriam_type_table  ;
delete from class_valid_annotation_table  ;
update user_table  set annotationschema_iddb =null;
delete from user_annotation_schema_table;
