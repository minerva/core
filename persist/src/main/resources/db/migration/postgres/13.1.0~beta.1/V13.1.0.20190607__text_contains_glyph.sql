alter table layer_text_table add column glyph_id integer;
alter table layer_text_table add constraint layer_text_table_glyph_fk FOREIGN KEY (glyph_id)
      REFERENCES glyph_table (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

