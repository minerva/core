-- drop data mining structures
drop table datamining_references ;
drop table datamining_connections;
drop table datamining_table;
drop table data_mining_set_table ;
drop sequence datamining_table_iddb_seq;
drop sequence data_mining_set_iddb_seq;

-- change project status from int to string

alter table project_table add column status_string varchar(255) default 'UNKNOWN';
update project_table set status_string = 'PARSING_DATA' where status = 1;
update project_table set status_string = 'ANNOTATING' where status = 2;
update project_table set status_string = 'UPLOADING_TO_DB' where status = 3;
update project_table set status_string = 'EXTENDING_MODEL' where status = 4;
update project_table set status_string = 'GENERATING_IMAGES' where status = 5;
update project_table set status_string = 'CACHING' where status = 6;
update project_table set status_string = 'DONE' where status = 7;
update project_table set status_string = 'FAIL' where status = 8;
update project_table set status_string = 'CACHING_MIRIAM' where status = 9;
update project_table set status_string = 'VALIDATING_MIRIAM' where status = 10;
update project_table set status_string = 'REMOVING' where status = 11;
update project_table set status_string = 'CACHING_CHEMICAL' where status = 12;
update project_table set status_string = 'CACHING_DRUG' where status = 13;
update project_table set status_string = 'CACHING_MI_RNA' where status = 14;
alter table project_table drop column status ;
alter table project_table rename COLUMN status_string to status;

-- remove DATA MINING status
update project_table set status = 'UNKNOWN' where status = 'EXTENDING_MODEL';
