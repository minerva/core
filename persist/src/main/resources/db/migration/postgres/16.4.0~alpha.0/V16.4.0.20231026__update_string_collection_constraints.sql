alter table data_overlay_entry_compartment_table
  drop constraint data_overlay_entry_compartment_table_entry_fk;
alter table data_overlay_entry_compartment_table
  add constraint data_overlay_entry_compartment_table_entry_fk
    FOREIGN KEY (data_overlay_entry_id) 
    REFERENCES data_overlay_entry_table(id)
    on delete cascade;  
    

ALTER TABLE data_overlay_entry_type_table
  drop CONSTRAINT data_overlay_entry_type_table_entry_fk;
ALTER TABLE data_overlay_entry_type_table
  ADD CONSTRAINT data_overlay_entry_type_table_entry_fk 
    FOREIGN KEY (data_overlay_entry_id) 
    REFERENCES data_overlay_entry_table(id)
    on delete cascade;
     
     
ALTER TABLE element_miriam
  drop CONSTRAINT element_miriam_element_fk;
ALTER TABLE element_miriam
  ADD CONSTRAINT element_miriam_element_fk 
    FOREIGN KEY (element_id) 
    REFERENCES public.element_table(id)
    on delete cascade;
    
ALTER TABLE public.element_synonyms
  drop CONSTRAINT element_synonyms_fk;
ALTER TABLE public.element_synonyms
  ADD CONSTRAINT element_synonyms_fk
    FOREIGN KEY (id) 
    REFERENCES public.element_table(id)
    on delete cascade;
    
ALTER TABLE public.element_former_symbols
  DROP CONSTRAINT element_former_symbols_fk;
ALTER TABLE public.element_former_symbols
  ADD CONSTRAINT element_former_symbols_fk
    FOREIGN KEY (id) REFERENCES public.element_table(id)
    on delete cascade;

ALTER TABLE reaction_miriam
  drop CONSTRAINT fk71295b151586e740;
ALTER TABLE reaction_miriam
  ADD CONSTRAINT reaction_miriam_reaction_fk
    FOREIGN KEY (reaction_id)
    REFERENCES reaction_table(id)
    on delete cascade;

ALTER TABLE reaction_synonyms
  drop CONSTRAINT reaction_synonyms_fkey;
ALTER TABLE reaction_synonyms
  ADD CONSTRAINT reaction_synonyms_fkey 
    FOREIGN KEY (id)
    REFERENCES reaction_table(id)
    on delete cascade;
    
    