-- change in layout
alter table history_table add column type_string varchar(31);
update history_table set type_string='GENERAL' where type=0;
update history_table set type_string='DRUG' where type=1;
update history_table set type_string='CHEMICAL' where type=2;
update history_table set type_string='MI_RNA' where type=3;

alter table history_table drop column type;
alter table history_table rename column type_string to type;
