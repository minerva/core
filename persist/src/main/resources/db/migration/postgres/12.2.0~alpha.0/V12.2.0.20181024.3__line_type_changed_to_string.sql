-- change in arrow_type_data
alter table arrow_type_data add column arrow_line_type_string varchar(31);
update arrow_type_data set arrow_line_type_string='SOLID' where arrow_line_type=0;
update arrow_type_data set arrow_line_type_string='SOLID_BOLD' where arrow_line_type=1;
update arrow_type_data set arrow_line_type_string='DASH_DOT_DOT' where arrow_line_type=2;
update arrow_type_data set arrow_line_type_string='DASH_DOT' where arrow_line_type=3;
update arrow_type_data set arrow_line_type_string='DASHED' where arrow_line_type=4;
update arrow_type_data set arrow_line_type_string='DOTTED' where arrow_line_type=5;
update arrow_type_data set arrow_line_type_string='DASHED_BOLD' where arrow_line_type=6;
update arrow_type_data set arrow_line_type_string='DOUBLE' where arrow_line_type=7;

alter table arrow_type_data drop column arrow_line_type;
alter table arrow_type_data rename column arrow_line_type_string to arrow_line_type;

-- change in polyline_data
alter table polyline_data add column type_string varchar(31);
update polyline_data set type_string='SOLID' where type=0;
update polyline_data set type_string='SOLID_BOLD' where type=1;
update polyline_data set type_string='DASH_DOT_DOT' where type=2;
update polyline_data set type_string='DASH_DOT' where type=3;
update polyline_data set type_string='DASHED' where type=4;
update polyline_data set type_string='DOTTED' where type=5;
update polyline_data set type_string='DASHED_BOLD' where type=6;
update polyline_data set type_string='DOUBLE' where type=7;

alter table polyline_data drop column type;
alter table polyline_data rename column type_string to type;

