CREATE SEQUENCE public.data_overlay_image_layer_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.data_overlay_image_layer_table
(
  id integer NOT NULL DEFAULT nextval('data_overlay_image_layer_sequence'::regclass),
  data_overlay_id integer NOT NULL ,
  model_id integer NOT NULL ,
  directory varchar(255),
  CONSTRAINT data_overlay_image_layer_pkey PRIMARY KEY (id),
  CONSTRAINT data_overlay_image_layer__layout_fkey FOREIGN KEY (data_overlay_id)
      REFERENCES public.layout_table (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT data_overlay_image_layer__data_overlay_fkey FOREIGN KEY (model_id)
      REFERENCES public.model_data_table (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

-- top data overlays
insert into data_overlay_image_layer_table (data_overlay_id, model_id, directory) select id, model_id, directory from layout_table where parent_layout_id is null;
-- child data overlays
insert into data_overlay_image_layer_table (data_overlay_id, model_id, directory) select parent_layout_id, model_id, directory from layout_table where parent_layout_id is not null;

-- remove old child data overlays 
delete from layout_table where parent_layout_id is not null;
alter table layout_table drop column parent_layout_id;