alter table layout_table add column project_id integer;
update layout_table set project_id = (select project_id from model_data_table where model_data_table.id = layout_table.model_id);
update layout_table child set project_id = (select project_id from model_data_table where model_data_table.id = (select model_id from layout_table parent where child.parent_layout_id = parent.id)) where child.parent_layout_id is not null;
-- just in case we have some artifacts in database
delete from layout_table where project_id is null;
-- and now we can add foreign key
ALTER TABLE layout_table ADD CONSTRAINT layout_table_project FOREIGN KEY (project_id)
      REFERENCES public.project_table (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
