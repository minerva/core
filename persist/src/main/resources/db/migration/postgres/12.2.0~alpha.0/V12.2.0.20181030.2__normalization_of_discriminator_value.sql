update element_table set element_type_db = 'BOTTOM_SQUARE_COMPARTMENT' where element_type_db = 'Bottom square Compartment';
update element_table set element_type_db = 'COMPARTMENT' where element_type_db = 'Compartment';
update element_table set element_type_db = 'LEFT_SQUARE_COMPARTMENT' where element_type_db = 'Left square Compartment';
update element_table set element_type_db = 'OVAL_COMPARTMENT' where element_type_db = 'Oval Compartment';
update element_table set element_type_db = 'PATHWAY' where element_type_db = 'Pathway Compartment';
update element_table set element_type_db = 'RIGHT_SQUARE_COMPARTMENT' where element_type_db = 'Right square Compartment';
update element_table set element_type_db = 'SQUARE_COMPARTMENT' where element_type_db = 'Square Compartment';
update element_table set element_type_db = 'TOP_SQUARE_COMPARTMENT' where element_type_db = 'Top square Compartment';

update submodel_connection_table set submodel_connections_type_db = 'ELEMENT_SUBMODEL_LINK' where submodel_connections_type_db = 'ALIAS_SUBMODEL_LINK';

update element_table set element_type_db = 'ANTISENSE_RNA' where element_type_db = 'ANTISENSE_RNA_ALIAS';
update element_table set element_type_db = 'CHEMICAL' where element_type_db = 'CHEMICAL_ALIAS';
update element_table set element_type_db = 'PHENTOYPE' where element_type_db = 'PHENTOYPE_ALIAS';
update element_table set element_type_db = 'ION_CHANNEL_PROTEIN' where element_type_db = 'ION_CHANNEL_PROTEIN_ALIAS';
update element_table set element_type_db = 'ION' where element_type_db = 'ION_ALIAS';
update element_table set element_type_db = 'GENERIC_PROTEIN' where element_type_db = 'GENERIC_PROTEIN_ALIAS';
update element_table set element_type_db = 'GENE' where element_type_db = 'GENE_ALIAS';
update element_table set element_type_db = 'GENERIC' where element_type_db = 'GENERIC_ALIAS';
update element_table set element_type_db = 'DRUG' where element_type_db = 'DRUG_ALIAS';
update element_table set element_type_db = 'DEGRADED' where element_type_db = 'DEGRADED_ALIAS';
update element_table set element_type_db = 'COMPLEX' where element_type_db = 'Complex';
update element_table set element_type_db = 'PROTEIN' where element_type_db = 'PROTEIN_ALIAS';
update element_table set element_type_db = 'RECEPTOR_PROTEIN' where element_type_db = 'RECEPTOR_PROTEIN_ALIAS';
update element_table set element_type_db = 'RNA' where element_type_db = 'RNA_ALIAS';
update element_table set element_type_db = 'SIMPLE_MOLECULE' where element_type_db = 'SIMPLE_MOLECULE_ALIAS';
update element_table set element_type_db = 'SPECIES' where element_type_db = 'Species';
update element_table set element_type_db = 'TRUNCATED_PROTEIN' where element_type_db = 'TRUNCATED_PROTEIN_ALIAS';
update element_table set element_type_db = 'UNKNOWN' where element_type_db = 'UNKNOWN_ALIAS';

