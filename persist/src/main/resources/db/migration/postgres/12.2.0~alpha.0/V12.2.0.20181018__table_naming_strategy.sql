-- rename arrowtypedata
alter table arrowtypedata rename to arrow_type_data;
alter table arrow_type_data rename linetype to line_type;
alter table arrow_type_data rename arrowtype to arrow_type;
alter table arrow_type_data rename arrowlinetype to arrow_line_type;
alter table arrow_type_data rename iddb to id;

-- rename polylinedata
alter table polylinedata rename to polyline_data;
alter table polyline_data rename iddb to id;
alter table polyline_data rename endatd_iddb to end_atd_id;
alter table polyline_data rename beginatd_iddb to begin_atd_id;

-- rename point_table
alter table point_table rename iddb to id;

-- rename project_table properties
alter table project_table rename COLUMN iddb to id;
alter table project_table rename COLUMN disease_iddb to disease_id;
alter table project_table rename COLUMN notifyemail to notify_email;
alter table project_table rename COLUMN organism_iddb to organism_id;
alter table project_table rename COLUMN sbgnformat to sbgn_format;
alter table project_table rename COLUMN file_entry_iddb to file_entry_id;

-- rename model_table properties
alter table model_table rename COLUMN iddb to id;
alter table model_table rename COLUMN project_iddb to project_id;
alter table model_table rename COLUMN creationdate to creation_date;
alter table model_table rename column defaultcenterx to default_centerx;
alter table model_table rename column defaultcentery to default_centery;
alter table model_table rename column defaultzoomlevel to default_zoom_level;
alter table model_table rename column idmodel to id_model;
alter table model_table rename column tilesize to tile_size;
alter table model_table rename column zoomlevels to zoom_levels;

-- rename element_table properties
alter table element_table rename column iddb to id;
alter table element_table rename column compartment_iddb to compartment_id;
alter table element_table rename column elementid to element_id;
alter table element_table rename column fontsize to font_size;
alter table element_table rename column fullname to full_name;
alter table element_table rename column model_iddb to model_id;
alter table element_table rename column submodel_iddb to submodel_id;
alter table element_table rename column transparencylevel to transparency_level;
alter table element_table rename column visibilitylevel to visibility_level;
alter table element_table rename column boundarycondition to boundary_condition;
alter table element_table rename column initialamount to initial_amount;
alter table element_table rename column initialconcentration to initial_concentration;
alter table element_table rename column linewidth to line_width;
alter table element_table rename column onlysubstanceunits to only_substance_units;
alter table element_table rename column positiontocompartment to position_to_compartment;
alter table element_table rename column statelabel to state_label;
alter table element_table rename column stateprefix to state_prefix;
alter table element_table rename column substanceunits to substance_units;
alter table element_table rename column inchikey to in_chikey;
alter table element_table rename column structuralstate to structural_state;
alter table element_table rename column inchi to in_chi;
alter table element_table rename column innerwidth to inner_width;
alter table element_table rename column namepoint to name_point;
alter table element_table rename column outerwidth to outer_width;

-- rename submodel_connection_table properties
alter table submodel_connection_table rename COLUMN iddb to id;
alter table submodel_connection_table rename COLUMN parentmodel_iddb to parent_model_id;
alter table submodel_connection_table rename COLUMN submodel_iddb to submodel_id;
alter table submodel_connection_table rename toelement_iddb to to_element_id;
alter table submodel_connection_table rename fromelement_iddb to from_element_id;

-- rename overview_image_table properties
alter table overview_image_table rename COLUMN iddb to id;
alter table overview_image_table rename COLUMN project_iddb to project_id;

-- rename sbml_parameter properties
alter table sbml_parameter rename iddb to id;
alter table sbml_parameter rename units_iddb to units_id;
alter table sbml_parameter rename parameterid to parameter_id;

-- rename sbml_function properties
alter table sbml_function rename model_iddb to model_id;
alter table sbml_function rename iddb to id;
alter table sbml_function rename functionid to function_id;

-- rename sbml_function_arguments properties
alter table sbml_function_arguments rename sbml_function_iddb to sbml_function_id;

-- rename sbml_unit properties
alter table sbml_unit rename iddb to id;
alter table sbml_unit rename model_iddb to model_id;
alter table sbml_unit rename unitid to unit_id;

-- rename sbml_unit_factor properties
alter table sbml_unit_factor rename iddb to id;
alter table sbml_unit_factor rename unittype to unit_type;
alter table sbml_unit_factor rename unit_iddb to unit_id;

-- rename user_table properties
alter table user_table rename iddb to id;
alter table user_table rename connectedtoldap to connected_to_ldap;
alter table user_table rename neutralcolor to neutral_color;
alter table user_table rename simplecolor to simple_color;
alter table user_table rename mincolor to min_color;
alter table user_table rename maxcolor to max_color;
alter table user_table rename annotationschema_iddb to annotation_schema_id;
alter table user_table rename cryptedpassword to crypted_password;

-- rename user_annotation_schema_table properties
alter table user_annotation_schema_table rename iddb to id;
alter table user_annotation_schema_table rename semanticzoomcontainsmultipleoverlays to semantic_zoom_contains_multiple_overlays;
alter table user_annotation_schema_table rename semanticzooming to semantic_zooming;
alter table user_annotation_schema_table rename cachedata to cache_data;
alter table user_annotation_schema_table rename autoresizemap to auto_resize_map;
alter table user_annotation_schema_table rename annotatemodel to annotate_model;
alter table user_annotation_schema_table rename sbgnformat to sbgn_format;
alter table user_annotation_schema_table rename networklayoutasdefault to network_layout_as_default;
alter table user_annotation_schema_table rename validatemiriamtypes to validate_miriam_types;
alter table user_annotation_schema_table rename user_iddb to user_id;

-- rename privilege_table properties
alter table privilege_table rename iddb to id;
alter table privilege_table rename user_iddb to user_id;
alter table privilege_table rename idobject to id_object;

-- rename user_terms_of_use_consent properties
alter table user_terms_of_use_consent rename user_iddb to user_id;

-- rename class_annotator_table properties
alter table class_annotator_table rename iddb to id;
alter table class_annotator_table rename classname to class_name;
alter table class_annotator_table rename annotationschema_iddb to annotation_schema_id;

-- rename class_valid_annotation_table properties
alter table class_valid_annotation_table rename iddb to id;
alter table class_valid_annotation_table rename classname to class_name;
alter table class_valid_annotation_table rename annotationschema_iddb to annotation_schema_id;

-- rename class_required_annotation_table properties
alter table class_required_annotation_table rename iddb to id;
alter table class_required_annotation_table rename classname to class_name;
alter table class_required_annotation_table rename annotationschema_iddb to annotation_schema_id;
alter table class_required_annotation_table rename requireatleastoneannotation to require_at_least_one_annotation;

-- rename class_required_annotation_miriam_type_table properties
alter table class_required_annotation_miriam_type_table rename class_required_annotation_iddb to class_required_annotation_id;

-- rename class_annotator_annotators_table properties
alter table class_annotator_annotators_table rename class_annotator_iddb to class_annotator_id;

-- rename class_valid_annotation_miriam_type_table properties
alter table class_valid_annotation_miriam_type_table rename class_valid_annotation_iddb to class_valid_annotation_id;

-- rename user_gui_preferences properties
alter table user_gui_preferences rename annotation_schema_iddb to annotation_schema_id;
alter table user_gui_preferences rename iddb to id;

-- rename configuration_table properties
alter table configuration_table rename iddb to id;

-- rename miriam_data_table 
alter table miriam_data_table rename iddb to id;
alter table miriam_data_table rename datatype to data_type;
alter table miriam_data_table rename relationtype to relation_type;

-- rename layout
alter table layout rename model_iddb to model_id;
alter table layout rename iddb to id;
alter table layout rename creator_iddb to creator_id;
alter table layout rename defaultoverlay to default_overlay;
alter table layout rename hierarchicalview to hierarchical_view;
alter table layout rename hierarchyviewlevel to hierarchy_view_level;
alter table layout rename file_entry_iddb to file_entry_id;
alter table layout rename parentlayout_iddb to parent_layout_id;
alter table layout rename publiclayout to public_layout;

-- rename uniprot_table
alter table uniprot_table rename iddb to id;

-- rename element_synonyms
alter table element_synonyms rename iddb to id;

-- rename reaction_synonyms
alter table reaction_synonyms rename iddb to id;

-- rename element_former_symbols
alter table element_former_symbols rename iddb to id;

-- rename search_index_table
alter table search_index_table rename source_iddb to source_id;
alter table search_index_table rename iddb to id;

-- rename reaction_table
alter table reaction_table rename iddb to id;
alter table reaction_table rename kinetics_iddb to kinetics_id;
alter table reaction_table rename model_iddb to model_id;
alter table reaction_table rename geneproteinreaction to gene_protein_reaction;
alter table reaction_table rename idreaction to id_reaction;
alter table reaction_table rename lowerbound to lower_bound;
alter table reaction_table rename mechanicalconfidencescore to mechanical_confidence_score;
alter table reaction_table rename upperbound to upper_bound;
alter table reaction_table rename visibilitylevel to visibility_level;

-- rename node_table
alter table node_table rename iddb to id;
alter table node_table rename element_iddb to element_id;
alter table node_table rename reaction_iddb to reaction_id;
alter table node_table rename line_iddb to line_id;
alter table node_table rename nodeoperatorforoutput_iddb to node_operator_for_output_id;
alter table node_table rename nodeoperatorforinput_iddb to node_operator_for_input_id;

-- rename sbml_kinetics
alter table sbml_kinetics rename iddb to id;

-- rename layer_table
alter table layer_table rename model_iddb to model_id;
alter table layer_table rename iddb to id;
alter table layer_table rename layerid to layer_id;

-- rename layer_table_layerrect
alter table layer_table_layerrect rename to layer_table_rectangles;
alter table layer_table_rectangles rename layer_table_iddb to layer_table_id;
alter table layer_table_rectangles rename rectangles_iddb to rectangles_id;

-- rename layer_table_layertext
alter table layer_table_layertext rename to layer_table_texts;
alter table layer_table_texts rename layer_table_iddb to layer_table_id;
alter table layer_table_texts rename texts_iddb to texts_id;

-- rename layer_table_layeroval
alter table layer_table_layeroval rename to layer_table_ovals;
alter table layer_table_ovals rename layer_table_iddb to layer_table_id;
alter table layer_table_ovals rename ovals_iddb to ovals_id;

-- rename layer_table_polylinedata
alter table layer_table_polylinedata rename to layer_table_lines;
alter table layer_table_lines rename layer_table_iddb to layer_table_id;
alter table layer_table_lines rename lines_iddb to lines_id;

-- rename layerrect
alter table layerrect rename to layer_rect;
alter table layer_rect rename iddb to id;

-- rename layeroval
alter table layeroval rename to layer_oval;
alter table layer_oval rename iddb to id;

-- rename layertext
alter table layertext rename to layer_text;
alter table layer_text rename iddb to id;
alter table layer_text rename fontsize to font_size;

-- rename overview_link_table
alter table overview_link_table rename linkedmodel_iddb to linked_model_id;
alter table overview_link_table rename linkedoverviewimage_iddb to linked_overview_image_id;
alter table overview_link_table rename zoomlevel to zoom_level;
alter table overview_link_table rename overviewimage_iddb to overview_image_id;
alter table overview_link_table rename iddb to id;
alter table overview_link_table rename xcoord to x_coord;
alter table overview_link_table rename ycoord to y_coord;

-- rename file_entry
alter table file_entry rename owner_iddb to owner_id;
alter table file_entry rename originalfilename to original_file_name;
alter table file_entry rename filecontent to file_content;
alter table file_entry rename downloadthreadid to download_thread_id;
alter table file_entry rename downloadprogress to download_progress;
alter table file_entry rename downloaddate to download_date;
alter table file_entry rename localpath to local_path;
alter table file_entry rename iddb to id;

-- rename cache_type
alter table cache_type rename iddb to id;
alter table cache_type rename classname to class_name;

-- rename cachequery
alter table cachequery rename to cache_query; 
alter table cache_query rename iddb to id;

-- rename log_table
alter table log_table rename iddb to id;
alter table log_table rename tablename to table_name;
alter table log_table rename objectid to object_id;
alter table log_table rename user_iddb to user_id;

-- rename modification_residue_table
alter table modification_residue_table rename iddb to id;
alter table modification_residue_table rename idspeciesdb to id_species_db;
alter table modification_residue_table rename idmodificationresidue to id_modification_residue;

-- rename reference_genome
alter table reference_genome rename iddb to id;
alter table reference_genome rename downloadprogress to download_progress;
alter table reference_genome rename sourceurl to source_url;
alter table reference_genome rename organism_iddb to organism_id;

-- rename reference_genome_gene_mapping
alter table reference_genome_gene_mapping rename iddb to id;
alter table reference_genome_gene_mapping rename downloadprogress to download_progress;
alter table reference_genome_gene_mapping rename sourceurl to source_url;
alter table reference_genome_gene_mapping rename referencegenome_iddb to reference_genome_id;

-- rename feedback
alter table feedback rename iddb to id;
alter table feedback rename submodel_iddb to submodel_id;
alter table feedback rename user_iddb to user_id;
alter table feedback rename model_iddb to model_id;
alter table feedback rename tablename to table_name;
alter table feedback rename tableid to table_id;
alter table feedback rename removereason to remove_reason;

-- rename history_table
alter table history_table rename iddb to id;
alter table history_table rename ipaddress to ip_address;

-- rename plugin_table
alter table plugin_table rename iddb to id;

-- rename plugin_urls
alter table plugin_urls rename plugin_iddb to plugin_id;

-- rename plugin_data_entry
alter table plugin_data_entry rename iddb to id;
alter table plugin_data_entry rename user_iddb to user_id;
alter table plugin_data_entry rename plugin_iddb to plugin_id;

-- rename annotators_params_table
alter table annotators_params_table rename annotationschema_iddb to annotation_schema_id;
alter table annotators_params_table rename iddb to id;

-- rename structure_table
alter table structure_table rename iddb to id;

-- rename project sequence
alter sequence project_table_iddb_seq rename to project_table_id_seq;