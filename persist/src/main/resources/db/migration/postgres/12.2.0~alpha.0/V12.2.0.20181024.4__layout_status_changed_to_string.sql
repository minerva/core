-- change in layout
alter table layout add column status_string varchar(31);
update layout set status_string='UNKNOWN' where status=0;
update layout set status_string='NA' where status=1;
update layout set status_string='GENERATING' where status=2;
update layout set status_string='OK' where status=3;
update layout set status_string='FAILURE' where status=4;

alter table layout drop column status;
alter table layout rename column status_string to status;
