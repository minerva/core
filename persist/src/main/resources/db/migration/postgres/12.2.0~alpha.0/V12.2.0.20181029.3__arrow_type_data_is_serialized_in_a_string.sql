alter table polyline_data add column begin_atd varchar(255);
alter table polyline_data add column end_atd varchar(255);

update polyline_data set begin_atd = (select concat(angle,';',len,';',arrow_type,';',arrow_line_type) from arrow_type_data where id=polyline_data.begin_atd_id);
update polyline_data set end_atd = (select concat(angle,';',len,';',arrow_type,';',arrow_line_type) from arrow_type_data where id=polyline_data.end_atd_id);

alter table polyline_data drop column begin_atd_id;
alter table polyline_data drop column end_atd_id;

drop table arrow_type_data;