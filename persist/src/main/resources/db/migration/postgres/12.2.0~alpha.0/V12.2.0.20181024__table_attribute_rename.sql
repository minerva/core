-- rename attributes for annotators_params_table

alter table annotators_params_table rename annotator_classname to annotator_class_name;
alter table annotators_params_table rename name to param_name;
alter table annotators_params_table rename value to param_value;
