update user_table set crypted_password = '' where crypted_password is null;
ALTER TABLE user_table ALTER COLUMN crypted_password SET NOT NULL;
