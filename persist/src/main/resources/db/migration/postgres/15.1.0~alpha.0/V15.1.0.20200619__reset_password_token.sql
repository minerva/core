CREATE SEQUENCE reset_password_token_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE reset_password_token_table
(
  id integer NOT NULL DEFAULT nextval('reset_password_token_sequence'::regclass),
  token varchar NOT NULL,
  user_id integer not null,
  expires timestamp without time zone,

  CONSTRAINT reset_password_token_pk PRIMARY KEY (id),
  CONSTRAINT reset_password_token_user_fk FOREIGN KEY (user_id)
      REFERENCES user_table (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
