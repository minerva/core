update layout_table set color_schema_type ='GENETIC_VARIANT' where file_entry_id in (select id from file_entry_table where  file_content like '%TYPE=GENE_%') and color_schema_type is null;
update layout_table set color_schema_type ='GENERIC' where color_schema_type is null;

ALTER TABLE layout_table ALTER COLUMN color_schema_type SET NOT NULL;
