CREATE INDEX minerva_job_table_job_status_index
    ON minerva_job_table (job_status);

CREATE INDEX cache_query_table_expires_index ON cache_query_table (expires);
