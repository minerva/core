update project_table set file_entry_id = null where not file_entry_id in (select id from file_entry_table);

ALTER TABLE project_table ADD CONSTRAINT project_file_constraint FOREIGN KEY (file_entry_id)
      REFERENCES public.file_entry_table (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
