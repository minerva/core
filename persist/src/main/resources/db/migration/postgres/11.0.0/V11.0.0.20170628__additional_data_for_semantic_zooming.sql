-- semantic zooming
alter table element_table drop column zoomlevelvisibility ;
alter table reaction_table drop column zoomlevelvisibility ;

ALTER TABLE element_table ALTER COLUMN visibilitylevel TYPE character varying;
ALTER TABLE element_table ALTER COLUMN transparencylevel TYPE character varying;

alter table reaction_table add column visibilitylevel character varying default null;