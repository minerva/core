alter table uniprot_record_table
    add column entity_version bigint default 0 not null;
alter table structure_table
    add column entity_version bigint default 0 not null;
