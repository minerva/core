CREATE TABLE public.stacktrace_table (
    id varchar(255) NOT NULL,
    content text NOT NULL,
    PRIMARY KEY (ID),
    created_at timestamp without time zone
);
