CREATE SEQUENCE public.data_overlay_entry_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.data_overlay_entry_table (
    id integer DEFAULT nextval('public.data_overlay_entry_sequence') NOT NULL,
    name character varying(255),
    model_name character varying(255),
    element_id character varying(255),
    reverse_reaction boolean,
    line_width double precision,
    value double precision,
    color character varying(10),
    description character varying(255),
    data_overlay_type character varying(255) not null,
    data_overlay_id integer NOT NULL
);

ALTER TABLE public.data_overlay_entry_table
    ADD CONSTRAINT data_overlay_entry_table_pk PRIMARY KEY (id);

ALTER TABLE public.data_overlay_entry_table
    ADD CONSTRAINT data_overlay_entry_table_data_overlay_fk FOREIGN KEY (data_overlay_id) REFERENCES public.data_overlay_table(id);

CREATE TABLE public.data_overlay_entry_compartment_table (
    data_overlay_entry_id integer NOT NULL,
    compartment character varying(255)
);

ALTER TABLE public.data_overlay_entry_compartment_table
    ADD CONSTRAINT data_overlay_entry_compartment_table_unique unique (data_overlay_entry_id, compartment);

ALTER TABLE public.data_overlay_entry_compartment_table
    ADD CONSTRAINT data_overlay_entry_compartment_table_entry_fk FOREIGN KEY (data_overlay_entry_id) REFERENCES public.data_overlay_entry_table(id);

CREATE TABLE public.data_overlay_entry_miriam_table (
    data_overlay_entry_id integer NOT NULL,
    miriam_id integer NOT NULL
);

ALTER TABLE public.data_overlay_entry_miriam_table
    ADD CONSTRAINT data_overlay_entry_miriam_table_unique unique (data_overlay_entry_id, miriam_id);

ALTER TABLE public.data_overlay_entry_miriam_table
    ADD CONSTRAINT data_overlay_entry_miriam_table_entry_fk FOREIGN KEY (data_overlay_entry_id) REFERENCES public.data_overlay_entry_table(id);

ALTER TABLE public.data_overlay_entry_miriam_table
    ADD CONSTRAINT data_overlay_entry_miriam_table_miriam_fk FOREIGN KEY (miriam_id) REFERENCES public.miriam_data_table(id);


CREATE TABLE public.data_overlay_entry_type_table (
    data_overlay_entry_id integer NOT NULL,
    type character varying(255)
);

ALTER TABLE public.data_overlay_entry_type_table
    ADD CONSTRAINT data_overlay_entry_type_table_unique unique (data_overlay_entry_id, type);

ALTER TABLE public.data_overlay_entry_type_table
    ADD CONSTRAINT data_overlay_entry_type_table_entry_fk FOREIGN KEY (data_overlay_entry_id) REFERENCES public.data_overlay_entry_table(id);

CREATE SEQUENCE public.gene_variant_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.gene_variant_table (
    id integer DEFAULT nextval('public.gene_variant_sequence') NOT NULL,
    position BIGINT not null,
    allel_frequency double precision,
    original_dna character varying(255) not null,
    modified_dna character varying(255) not null,
    contig character varying(255) not null,
    amino_acid_change character varying(255),
    variant_identifier character varying(255),
    data_overlay_entry_id integer not null
);

ALTER TABLE public.gene_variant_table
    ADD CONSTRAINT gene_variant_table_data_overlay_fk FOREIGN KEY (data_overlay_entry_id) REFERENCES public.data_overlay_entry_table(id);

ALTER TABLE public.gene_variant_table
    ADD CONSTRAINT gene_variant_table_pk PRIMARY KEY (id);

alter table public.data_overlay_table
   add column genome_type character varying(255);
   
alter table public.data_overlay_table
   add column genome_version character varying(255);

update public.data_overlay_table set genome_type = 'UCSC' where color_schema_type='GENETIC_VARIANT';
