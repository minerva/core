update layout_table set creator_id = (select owner_id from project_table where project_table.id= layout_table.project_id) where creator_id is null;
alter table layout_table alter column creator_id set not null;

CREATE SEQUENCE data_overlay_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

select setval('data_overlay_sequence',  (SELECT MAX(id)+1 FROM layout_table));

CREATE TABLE data_overlay_table (
    id integer DEFAULT nextval('data_overlay_sequence') NOT NULL,
    name character varying(255),
    color_schema_type character varying(255),
    creator_id integer NOT NULL,
    description text,
    google_license_consent boolean NOT NULL,
    file_entry_id integer NOT NULL,
    order_index integer NOT NULL,
    project_id integer NOT NULL,
    is_public boolean NOT NULL
);

ALTER TABLE data_overlay_table
    ADD CONSTRAINT data_overlay_table_pk PRIMARY KEY (id);

ALTER TABLE data_overlay_table
    ADD CONSTRAINT data_overlay_table_creator_fk FOREIGN KEY (creator_id) REFERENCES user_table(id);
    
ALTER TABLE data_overlay_table
    ADD CONSTRAINT data_overlay_table_file_fk FOREIGN KEY (file_entry_id) REFERENCES file_entry_table(id);

ALTER TABLE data_overlay_table
    ADD CONSTRAINT data_overlay_table_project_fk FOREIGN KEY (project_id) REFERENCES project_table(id);

insert into data_overlay_table (id, name, color_schema_type, creator_id, description, google_license_consent, file_entry_id, order_index, project_id, is_public)
select id, title, color_schema_type, creator_id, description, google_license_consent, file_entry_id, order_index, project_id, public_layout from layout_table where file_entry_id is not null; 

delete from data_overlay_image_layer_table where data_overlay_id in (select id from layout_table where file_entry_id is not null); 
delete from layout_table where file_entry_id is not null;

alter table layout_table drop column color_schema_type;
alter table layout_table drop column google_license_consent;
alter table layout_table drop column file_entry_id;
alter table layout_table drop column public_layout;

