CREATE SEQUENCE public.article_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE public.article_table (
    id integer DEFAULT nextval('public.article_sequence') NOT NULL,
    title character varying(1024),
    journal character varying(1024),
    link character varying(1024),
    pubmed_id character varying(255) not null,
    year integer,
    citation_count integer
);

ALTER TABLE public.article_table
    ADD CONSTRAINT article_table_pk PRIMARY KEY (id);


ALTER TABLE public.article_table
    ADD CONSTRAINT article_table_pubmed_id_unique UNIQUE (pubmed_id);

CREATE TABLE public.article_author (
    author character varying(1024),
    article_id integer not null,
    idx integer not null
);

ALTER TABLE public.article_author
    ADD CONSTRAINT article_author_unique_order UNIQUE (article_id,idx );

ALTER TABLE public.article_author
    ADD CONSTRAINT article_author_fk FOREIGN KEY (article_id) REFERENCES public.article_table(id);
