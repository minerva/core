CREATE TABLE public.line_segment (
    x1 numeric NOT NULL,
    y1 numeric NOT NULL,
    x2 numeric NOT NULL,
    y2 numeric NOT NULL
);

CREATE FUNCTION dist_to_segment(px double precision, py double precision,      														vx double precision, vy double precision,  															wx double precision, wy double precision)
  RETURNS double precision
as $$
   declare l2 double precision;
   declare t double precision;
   declare nx double precision;
   declare ny double precision;
BEGIN 
   l2 := (vx - wx)*(vx - wx) + (vy - wy)*(vy - wy);
   IF l2 = 0 THEN
     RETURN sqrt((vx - px)*(vx - px) + (vy - py)*(vy - py));
   ELSE
     t := ((px - vx) * (wx - vx) + (py - vy) * (wy - vy)) / l2;
     t := GREATEST(0, LEAST(1, t));
     nx := vx + t * (wx - vx);
     ny := vy + t * (wy - vy);
     RETURN sqrt((nx - px)*(nx - px) + (ny - py)*(ny - py));
   END IF;
END;
$$ LANGUAGE plpgsql;

alter table point_table add column end_point character varying(255) ;
update point_table p1 set end_point = (select point_val from point_table p2 where p1.idx+1=p2.idx and p2.id=p1.id);

delete from point_table where end_point is null;

alter table point_table add column x1 double precision;
alter table point_table add column y1 double precision;
alter table point_table add column x2 double precision;
alter table point_table add column y2 double precision;

update point_table set x1 = cast (split_part(point_val, ',',1) as double precision);
update point_table set y1 = cast (split_part(point_val, ',',2) as double precision);

update point_table set x2 = cast (split_part(end_point, ',',1) as double precision);
update point_table set y2 = cast (split_part(end_point, ',',2) as double precision);

alter table point_table alter column x1 set not null;
alter table point_table alter column y1 set not null;
alter table point_table alter column x2 set not null;
alter table point_table alter column y2 set not null;

alter table point_table drop column point_val;
alter table point_table drop column end_point;
