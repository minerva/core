alter table data_overlay_image_layer_table rename column data_overlay_id to project_background_id;

alter table data_overlay_image_layer_table rename to project_background_image_layer_table;

alter table layout_table rename column title to name;

alter table layout_table rename to project_background_table;

alter table user_annotation_schema_table rename column network_layout_as_default to network_background_as_default;
