CREATE INDEX element_name_lower_case_idx ON public.element_table USING btree (lower((name)::text) varchar_pattern_ops);
CREATE INDEX element_name_idx ON public.element_table USING btree (name);

CREATE INDEX data_overlay_entry_name_lower_case_idx ON public.data_overlay_entry_table USING btree (lower((name)::text) varchar_pattern_ops);
CREATE INDEX data_overlay_entry_name_idx ON public.data_overlay_entry_table USING btree (name);
