alter table polyline_data_table
    add column z integer;

update polyline_data_table
set z = 0;

alter table polyline_data_table
    alter column z set not null;
