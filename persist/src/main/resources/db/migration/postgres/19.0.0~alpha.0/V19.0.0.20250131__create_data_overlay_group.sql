CREATE SEQUENCE public.data_overlay_group_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

create table public.data_overlay_group_table
(
    id             integer DEFAULT nextval('public.data_overlay_group_sequence') NOT NULL,
    order_index    integer                                                       not null,
    name           varchar(255)                                                  not null,
    owner_id       integer                                                       not null,
    project_id     integer                                                       not null,
    entity_version bigint  default 0                                             not null,

    CONSTRAINT data_overlay_group_pkey PRIMARY KEY (id),
    CONSTRAINT data_overlay_group_owner_fk FOREIGN KEY (owner_id)
        REFERENCES user_table (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,

    CONSTRAINT data_overlay_group_project_fk FOREIGN KEY (project_id)
        REFERENCES project_table (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
);


alter table public.data_overlay_table
    add column group_id integer;

ALTER TABLE public.data_overlay_table
    ADD CONSTRAINT data_overlay_table_group_fk FOREIGN KEY (group_id)
        REFERENCES data_overlay_group_table (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
