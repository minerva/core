alter table minerva_job_table
    add column external_object_id integer;

alter table minerva_job_table
    add column external_object_class character varying(512);
