update element_table
set name_height = height - (homodimer - 1) * 6,
    name_width  = width - (homodimer - 1) * 6
where homodimer > 1;
