-- alignment columns
alter table layer_text_table
    add column horizontal_align character varying(255);
alter table layer_text_table
    add column vertical_align character varying(255);

update layer_text_table
set horizontal_align='LEFT'
where horizontal_align is null;
update layer_text_table
set vertical_align='TOP'
where vertical_align is null;


alter table layer_text_table
    alter column horizontal_align set not null;
alter table layer_text_table
    alter column vertical_align set not null;
