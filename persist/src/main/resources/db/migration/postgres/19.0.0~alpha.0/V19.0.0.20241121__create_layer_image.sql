CREATE SEQUENCE layer_image_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE layer_image_table
(
    id             integer DEFAULT nextval('public.layer_image_sequence') NOT NULL,
    entity_version bigint  default 0                                      not null,
    glyph_id       integer,
    x              double PRECISION                                       not null,
    y              double PRECISION                                       not null,
    z              integer                                                not null,
    height         double PRECISION                                       not null,
    width          double PRECISION                                       not null,
    layer_id       integer                                                not null,

    CONSTRAINT layer_image_table_layer_fk FOREIGN KEY (layer_id) REFERENCES public.layer_table (id)
        ON UPDATE NO ACTION ON DELETE NO ACTION,
    constraint layer_image_table_glyph_fk FOREIGN KEY (glyph_id)
        REFERENCES glyph_table (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
);
