INSERT INTO public.cache_type_table (validity, class_name)
VALUES (365, 'lcsb.mapviewer.annotation.services.annotators.CopyMissingCellDesignerMiriamDataAnnotator');

insert into annotator_data_table (order_index, annotator_class_name, user_class_annotators_id)
select 0, 'lcsb.mapviewer.annotation.services.annotators.CopyMissingCellDesignerMiriamDataAnnotator', id
from user_class_annotators_table
where class_name like '%species%';

insert into annotator_parameter_table (type_distinguisher, annotator_data_id, field, order_position)
select 'INPUT_PARAMETER', id, 'NAME', 0
from annotator_data_table
where annotator_class_name = 'lcsb.mapviewer.annotation.services.annotators.CopyMissingCellDesignerMiriamDataAnnotator';
