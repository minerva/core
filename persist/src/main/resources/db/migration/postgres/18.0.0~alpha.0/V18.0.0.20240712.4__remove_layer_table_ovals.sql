delete
from layer_text_table
where id not in (select texts_id from layer_table_texts);

alter table layer_text_table
    add column layer_id integer;

alter table layer_text_table
    add column entity_version bigint default 0 not null;

ALTER TABLE layer_text_table
    ADD CONSTRAINT layer_text_table_layer_fk FOREIGN KEY (layer_id) REFERENCES public.layer_table (id)
        ON DELETE SET NULL;

update layer_text_table
set layer_id = (select layer_table_id from layer_table_texts where texts_id = layer_text_table.id)
where layer_id is null;

drop table layer_table_texts;