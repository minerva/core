delete
from layer_rect_table
where id not in (select rectangles_id from layer_table_rectangles);

alter table layer_rect_table
    add column layer_id integer;

alter table layer_rect_table
    add column entity_version bigint default 0 not null;

ALTER TABLE layer_rect_table
    ADD CONSTRAINT layer_rect_table_layer_fk FOREIGN KEY (layer_id) REFERENCES public.layer_table (id)
        ON DELETE SET NULL;

update layer_rect_table
set layer_id = (select layer_table_id from layer_table_rectangles where rectangles_id = layer_rect_table.id)
where layer_id is null;

drop table layer_table_rectangles;