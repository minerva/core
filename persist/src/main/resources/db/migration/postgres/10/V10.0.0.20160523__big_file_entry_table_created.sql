CREATE SEQUENCE public.big_file_entry_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
CREATE TABLE public.big_file_entry
(
  iddb integer NOT NULL DEFAULT nextval('big_file_entry_iddb_seq'::regclass),
  url text,
  localpath text,
  downloaddate timestamp without time zone,
  downloadprogress double precision,
  downloadthreadid integer,
  removed boolean default false,
  CONSTRAINT big_file_entry_pkey PRIMARY KEY (iddb)
)
WITH (
  OIDS=FALSE
);
