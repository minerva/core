-- project should have organism id
alter table project_table  add column organism_iddb integer;
ALTER TABLE project_table ADD CONSTRAINT project_organism_constraint FOREIGN KEY (organism_iddb)
      REFERENCES public.miriam_data_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;