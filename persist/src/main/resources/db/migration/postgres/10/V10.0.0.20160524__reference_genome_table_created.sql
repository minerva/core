drop table if exists public.reference_genome;
drop SEQUENCE if exists public.reference_genome_iddb_seq;
CREATE SEQUENCE public.reference_genome_iddb_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
CREATE TABLE public.reference_genome
(
  iddb integer NOT NULL DEFAULT nextval('reference_genome_iddb_seq'::regclass),
  organism_iddb integer NOT NULL ,
  "type" varchar(255),
  version varchar(255),
  sourceurl text,
  downloadprogress double precision,
  CONSTRAINT reference_genome_pkey PRIMARY KEY (iddb),
  CONSTRAINT reference_genome_organism_fk FOREIGN KEY (organism_iddb)
      REFERENCES public.miriam_data_table (iddb) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
