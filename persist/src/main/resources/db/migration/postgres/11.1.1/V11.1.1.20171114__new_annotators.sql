DELETE FROM cache_type WHERE classname = 'lcsb.mapviewer.annotation.services.annotators.TairAnnotator';
INSERT INTO cache_type(validity, classname) VALUES (365, 'lcsb.mapviewer.annotation.services.annotators.TairAnnotator');

DELETE FROM cache_type WHERE classname = 'lcsb.mapviewer.annotation.services.annotators.CazyAnnotator';
INSERT INTO cache_type(validity, classname) VALUES (365, 'lcsb.mapviewer.annotation.services.annotators.CazyAnnotator');

DELETE FROM cache_type WHERE classname = 'lcsb.mapviewer.annotation.services.annotators.BrendaAnnotator';
INSERT INTO cache_type(validity, classname) VALUES (365, 'lcsb.mapviewer.annotation.services.annotators.BrendaAnnotator');

DELETE FROM cache_type WHERE classname = 'lcsb.mapviewer.annotation.services.annotators.StitchAnnotator';
INSERT INTO cache_type(validity, classname) VALUES (365, 'lcsb.mapviewer.annotation.services.annotators.StitchAnnotator');

DELETE FROM cache_type WHERE classname = 'lcsb.mapviewer.annotation.services.annotators.StringAnnotator';
INSERT INTO cache_type(validity, classname) VALUES (365, 'lcsb.mapviewer.annotation.services.annotators.StringAnnotator');

