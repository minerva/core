alter table license_table
    add column url character varying(1024);

update license_table
set url ='https://creativecommons.org/publicdomain/zero/1.0/'
where name = 'CC0 1.0 Universal';

update license_table
set url ='https://creativecommons.org/licenses/by/4.0/'
where name = 'Creative Commons Attribution 4.0 International Public License';

update license_table
set url ='https://creativecommons.org/licenses/by-sa/4.0/'
where name = 'Creative Commons Attribution-ShareAlike 4.0 International Public License';

update license_table
set url ='https://creativecommons.org/licenses/by-nc/4.0/'
where name = 'Creative Commons Attribution-NonCommercial 4.0 International';

update license_table
set url ='https://creativecommons.org/licenses/by-nc-sa/4.0/'
where name = 'Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License';

update license_table
set url ='https://creativecommons.org/licenses/by-nd/4.0/'
where name = 'Creative Commons Attribution-NoDerivatives 4.0 International Public License';

update license_table
set url ='https://creativecommons.org/licenses/by-nc-nd/4.0/'
where name = 'Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International Public License';
