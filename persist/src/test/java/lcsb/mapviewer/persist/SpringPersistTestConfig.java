package lcsb.mapviewer.persist;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ SpringPersistConfig.class })
public class SpringPersistTestConfig {

}
