package lcsb.mapviewer.persist;

import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.common.tests.TestUtils;
import lcsb.mapviewer.common.tests.UnitTestFailedWatcher;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerImage;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.ModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.Residue;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.awt.Color;
import java.awt.geom.Point2D;

@ContextConfiguration(classes = SpringPersistTestConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class PersistTestFunctionsNoTransaction extends TestUtils {

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();

  private static int identifierCounter = 0;

  private final PointTransformation pt = new PointTransformation();

  private static int z = 0;

  protected static GenericProtein createProtein() {
    return createProtein(0, 0, 100, 200, "s" + (identifierCounter++));
  }

  protected static GenericProtein createProtein(final String proteinId) {
    return createProtein(0, 0, 100, 200, proteinId);
  }

  protected static GenericProtein createProtein(final double x, final double y, final double width, final double height, final String proteinId) {
    final GenericProtein protein = new GenericProtein("s" + identifierCounter++);
    protein.setElementId(proteinId);
    assignCoordinates(x, y, width, height, protein);
    return protein;
  }

  protected Compartment createCompartment(final double x, final double y, final double width, final double height, final String compartmentId) {
    final Compartment compartment = new Compartment(compartmentId);
    assignCoordinates(x, y, width, height, compartment);
    return compartment;
  }

  protected Compartment createCompartment(final String compartmentId) {
    return createCompartment(10, 20, 100, 200, compartmentId);
  }

  protected Compartment createCompartment() {
    return createCompartment("c" + (identifierCounter++));
  }

  private static void assignCoordinates(final double x, final double y, final double width, final double height, final Element protein) {
    protein.setX(x);
    protein.setY(y);
    protein.setZ(z++);
    protein.setWidth(width);
    protein.setHeight(height);
    protein.setNameX(x);
    protein.setNameY(y);
    protein.setNameWidth(width);
    protein.setNameHeight(height);
    protein.setNameVerticalAlign(VerticalAlign.MIDDLE);
    protein.setNameHorizontalAlign(HorizontalAlign.CENTER);
  }

  protected static Ion createIon(final String ionId) {
    final Ion ion = new Ion(ionId);
    assignCoordinates(1, 2, 10, 20, ion);
    return ion;
  }

  protected Complex createComplex(final double x, final double y, final double width, final double height, final String complexId) {
    final Complex complex = new Complex(complexId);
    assignCoordinates(x, y, width, height, complex);
    return complex;
  }

  protected Complex createComplex() {
    return createComplex(10, 20, 100, 200, "comp" + (identifierCounter++));
  }

  protected AntisenseRna createAntisenseRna(final String antisenseRnaId) {
    final AntisenseRna antisenseRna = new AntisenseRna(antisenseRnaId);
    assignCoordinates(1, 2, 10, 20, antisenseRna);
    return antisenseRna;
  }

  public PersistTestFunctionsNoTransaction() {
    super();
  }

  protected Phenotype createPhenotype(final String phenotypeId) {
    final Phenotype phenotype = new Phenotype(phenotypeId);
    assignCoordinates(1, 2, 10, 20, phenotype);
    return phenotype;
  }

  protected Rna createRna(final String rnaId) {
    final Rna rna = new Rna(rnaId);
    assignCoordinates(1, 2, 10, 20, rna);

    return rna;
  }

  protected TransportReaction createReaction(final Element reactantElement, final Element productElement) {
    final TransportReaction result = new TransportReaction("re" + identifierCounter++);
    addLines(reactantElement, productElement, result);
    return result;
  }

  private void addLines(final Element reactantElement, final Element productElement, final Reaction result) {
    final Product product = new Product(productElement);
    product.setLine(new PolylineData(productElement.getCenter(),
        pt.getPointOnLine(reactantElement.getCenter(), productElement.getCenter(), 0.6)));
    result.addProduct(product);
    final Reactant reactant = new Reactant(reactantElement);
    reactant.setLine(new PolylineData(productElement.getCenter(),
        pt.getPointOnLine(reactantElement.getCenter(), productElement.getCenter(), 0.4)));
    result.addReactant(reactant);
    result.setZ(z++);
    result.setLine(new PolylineData(pt.getPointOnLine(reactantElement.getCenter(), productElement.getCenter(), 0.4),
        pt.getPointOnLine(reactantElement.getCenter(), productElement.getCenter(), 0.6)));
  }

  protected ModulationReaction createModulationReaction(final Element reactantElement, final Element productElement) {
    final ModulationReaction result = new ModulationReaction("re" + identifierCounter++);
    addLines(reactantElement, productElement, result);
    result.setZ(z++);
    return result;
  }

  protected LayerRect createRect() {
    final LayerRect lr = new LayerRect();
    lr.setBorderColor(Color.YELLOW);
    lr.setZ(0);
    return lr;
  }

  protected LayerOval createOval() {
    final LayerOval oval = new LayerOval();
    oval.setColor(Color.GREEN);
    oval.setZ(0);
    return oval;
  }

  protected LayerText createText() {
    final LayerText text = new LayerText();
    text.setColor(Color.GREEN);
    text.setZ(0);
    return text;
  }

  protected LayerImage createImage() {
    final LayerImage image = new LayerImage();
    image.setZ(0);
    return image;
  }

  protected ModificationSite createModificationSite() {
    final ModificationSite mr = new ModificationSite();
    initModificationResidue(mr);
    mr.setName("name");
    mr.setState(ModificationState.DONT_CARE);
    return mr;
  }

  private void initModificationResidue(final ModificationResidue mr) {
    mr.setX(10.0);
    mr.setY(10.0);
    mr.setZ(z++);
    mr.setBorderColor(Color.BLACK);
    mr.setNameX(mr.getX());
    mr.setNameY(mr.getY());
    mr.setNameWidth(mr.getWidth());
    mr.setNameHeight(mr.getHeight());
    mr.setNameHorizontalAlign(HorizontalAlign.CENTER);
    mr.setNameVerticalAlign(VerticalAlign.MIDDLE);
  }

  protected Residue createResidue() {
    final Residue mr = new Residue();
    mr.setPosition(new Point2D.Double(10, 20));
    mr.setName("name");
    mr.setState(ModificationState.GLYCOSYLATED);
    initModificationResidue(mr);
    return mr;
  }

  protected CodingRegion createCodingRegion() {
    final CodingRegion region = new CodingRegion();
    initModificationResidue(region);
    return region;
  }

  protected Comment createComment(final Model model) {
    final Comment result = new Comment();
    result.setModel(model);
    result.setUser(model.getProject().getOwner());
    result.setContent("test content");
    return result;
  }

  protected UploadedFileEntry createUploadedFile() {
    final byte[] data = "test".getBytes();
    final UploadedFileEntry fileEntry = new UploadedFileEntry();
    fileEntry.setFileContent(data);
    return fileEntry;
  }

  protected Layer createLayer() {
    final Layer result = new Layer();
    result.setLayerId("l" + identifierCounter++);
    result.setZ(faker.number().numberBetween(1, 100));
    return result;
  }

}