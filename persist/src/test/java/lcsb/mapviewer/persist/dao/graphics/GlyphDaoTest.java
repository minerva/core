package lcsb.mapviewer.persist.dao.graphics;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.ProjectDao;

public class GlyphDaoTest extends PersistTestFunctions {

  @Autowired
  private ProjectDao projectDao;

  @Autowired
  private GlyphDao glyphDao;

  @Test
  public void testFilterByProject() {
    Project project = createProject();

    Map<GlyphProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(GlyphProperty.PROJECT_ID, project.getProjectId());
    Page<Glyph> page = glyphDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(GlyphProperty.PROJECT_ID, TEST_PROJECT_2);
    page = glyphDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

  @Test
  public void testFilterByMapId() {
    Project project = createProject();

    Map<GlyphProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(GlyphProperty.ID, project.getGlyphs().get(0).getId());
    Page<Glyph> page = glyphDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(GlyphProperty.ID, -1);
    page = glyphDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

  public Project createProject() {
    Project project = new Project(TEST_PROJECT_ID);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    Glyph glyph = new Glyph();
    glyph.setFile(new UploadedFileEntry());
    project.addGlyph(glyph);
    projectDao.add(project);
    return project;
  }

}
