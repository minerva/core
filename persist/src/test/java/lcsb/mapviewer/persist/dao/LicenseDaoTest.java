package lcsb.mapviewer.persist.dao;

import lcsb.mapviewer.model.License;
import lcsb.mapviewer.persist.PersistTestFunctions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;

public class LicenseDaoTest extends PersistTestFunctions {

  @Autowired
  private LicenseDao licenseDao;

  @Test
  public void testAdd() {
    final License license = new License("12345", "cont", "google.com");
    licenseDao.add(license);

    assertEquals(license, licenseDao.getById(license.getId()));
  }

}
