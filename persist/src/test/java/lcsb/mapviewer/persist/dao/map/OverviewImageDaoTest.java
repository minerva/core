package lcsb.mapviewer.persist.dao.map;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.user.UserDao;

public class OverviewImageDaoTest extends PersistTestFunctions {

  @Autowired
  private ProjectDao projectDao;

  @Autowired
  private UserDao userDao;

  @Autowired
  private OverviewImageDao overviewImageDao;

  @Test
  public void testGetByProjectId() {
    Project project = new Project(TEST_PROJECT);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));

    OverviewImage image = new OverviewImage();
    project.addOverviewImage(image);

    image = new OverviewImage();
    project.addOverviewImage(image);

    projectDao.add(project);

    Map<OverviewImageProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(OverviewImageProperty.PROJECT_ID, TEST_PROJECT);

    Page<OverviewImage> page = overviewImageDao.getAll(filterOptions, PageRequest.of(0, 20));
    assertEquals(2, page.getTotalElements());

    filterOptions.put(OverviewImageProperty.PROJECT_ID, TEST_PROJECT_2);

    page = overviewImageDao.getAll(filterOptions, PageRequest.of(0, 20));
    assertEquals(0, page.getTotalElements());

  }
}
