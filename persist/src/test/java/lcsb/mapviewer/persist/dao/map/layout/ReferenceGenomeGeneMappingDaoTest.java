package lcsb.mapviewer.persist.dao.map.layout;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.ReferenceGenome;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeGeneMapping;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class ReferenceGenomeGeneMappingDaoTest extends PersistTestFunctions {

  @Autowired
  private ReferenceGenomeGeneMappingDao referenceGenomeGeneMappingDao;

  @Autowired
  private ReferenceGenomeDao referenceGenomeDao;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void test() {
    ReferenceGenome genome = new ReferenceGenome();
    genome.setOrganism(new MiriamData(MiriamType.TAXONOMY, "9606"));
    genome.setType(ReferenceGenomeType.UCSC);
    genome.setVersion("x");
    referenceGenomeDao.add(genome);

    ReferenceGenomeGeneMapping mapping = new ReferenceGenomeGeneMapping();
    mapping.setName("Xxx");
    mapping.setReferenceGenome(genome);
    mapping.setSourceUrl("http://xxx.xx/");
    long count = referenceGenomeGeneMappingDao.getCount();
    referenceGenomeGeneMappingDao.add(mapping);
    long count2 = referenceGenomeGeneMappingDao.getCount();
    referenceGenomeGeneMappingDao.delete(mapping);
    long count3 = referenceGenomeGeneMappingDao.getCount();

    referenceGenomeDao.delete(genome);

    assertEquals(count + 1, count2);
    assertEquals(count, count3);
  }
}
