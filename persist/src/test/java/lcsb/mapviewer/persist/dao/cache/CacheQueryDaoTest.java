package lcsb.mapviewer.persist.dao.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Calendar;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.cache.CacheQuery;
import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class CacheQueryDaoTest extends PersistTestFunctions {

  @Autowired
  protected CacheTypeDao cacheTypeDao;

  @Autowired
  protected CacheQueryDao cacheQueryDao;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetByQuery() {
    CacheType type = cacheTypeDao.getByClassName("lcsb.mapviewer.reactome.utils.ReactomeConnector");
    CacheQuery query = new CacheQuery();
    query.setType(type);
    query.setQuery("test");
    cacheQueryDao.add(query);
    assertEquals(query, cacheQueryDao.getByQuery("test", type));
    assertNull(cacheQueryDao.getByQuery("another_test_bla", type));
  }

  @Test
  public void testGetExpired() {
    CacheType type = cacheTypeDao.getByClassName("lcsb.mapviewer.reactome.utils.ReactomeConnector");

    CacheQuery query = new CacheQuery();
    query.setType(type);
    query.setQuery("test");
    Calendar yesterday = Calendar.getInstance();
    yesterday.add(Calendar.DATE, -1);
    query.setExpires(yesterday);
    cacheQueryDao.add(query);

    query = new CacheQuery();
    query.setType(type);
    query.setQuery("test2");
    Calendar tomorrow = Calendar.getInstance();
    tomorrow.add(Calendar.DATE, 1);
    query.setExpires(tomorrow);
    cacheQueryDao.add(query);

    assertEquals(1, cacheQueryDao.getExpired(10).size());
  }

}
