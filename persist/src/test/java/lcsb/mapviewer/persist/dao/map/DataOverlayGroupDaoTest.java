package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.overlay.DataOverlayGroup;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.ProjectDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertNotNull;

public class DataOverlayGroupDaoTest extends PersistTestFunctions {

  @Autowired
  private DataOverlayGroupDao dataOverlayGroupDao;

  @Autowired
  private ProjectDao projectDao;

  private Project project;

  private User user;

  @Before
  public void setUp() throws Exception {
    project = projectDao.getProjectByProjectId(BUILT_IN_PROJECT);
    user = userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAddDataOverlayGroup() throws Exception {

    DataOverlayGroup dataOverlay = createDataOverlayGroup(project, user);
    dataOverlay.setProject(project);

    dataOverlayGroupDao.add(dataOverlay);

    assertNotNull(dataOverlayGroupDao.getById(dataOverlay.getId()));
  }

}
