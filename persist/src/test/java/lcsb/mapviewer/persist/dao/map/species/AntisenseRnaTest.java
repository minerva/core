package lcsb.mapviewer.persist.dao.map.species;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.ProjectDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class AntisenseRnaTest extends PersistTestFunctions {

  private User admin;

  @Autowired
  private ProjectDao projectDao;

  @Before
  public void setUp() throws Exception {
    admin = userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAntisenseRnaRegionInDb() throws Exception {
    Project project = new Project(TEST_PROJECT_ID);
    project.setOwner(admin);

    Model model = createModel();

    project.addModel(model);
    projectDao.add(project);
    projectDao.evict(project);

    Project project2 = projectDao.getProjectByProjectId(TEST_PROJECT_ID);
    assertNotNull(project2);
    assertNotEquals(project2, project);
    assertEquals(project.getId(), project2.getId());

    Model model2 = new ModelFullIndexed(project2.getModels().iterator().next());

    Element sp = model.getElements().iterator().next();
    AntisenseRna ar = (AntisenseRna) sp;

    Element sp2 = model2.getElements().iterator().next();
    AntisenseRna ar2 = (AntisenseRna) sp2;

    projectDao.delete(project2);

    assertEquals(ar.getModificationResidues().size(), ar2.getModificationResidues().size());
  }

  private Model createModel() {
    Model model = new ModelFullIndexed(null);

    AntisenseRna species = createAntisenseRna("As");
    CodingRegion region = createCodingRegion();
    species.addCodingRegion(region);
    model.addElement(species);

    return model;
  }

}
