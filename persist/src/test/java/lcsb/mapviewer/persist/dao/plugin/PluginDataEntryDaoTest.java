package lcsb.mapviewer.persist.dao.plugin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.plugin.Plugin;
import lcsb.mapviewer.model.plugin.PluginDataEntry;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class PluginDataEntryDaoTest extends PersistTestFunctions {

  @Autowired
  private PluginDataEntryDao pluginDataEntryDao;

  @Autowired
  private PluginDao pluginDao;

  @Test
  public void testGetByKeyForGlobalParam() {
    Plugin plugin = new Plugin();
    plugin.setHash("x");
    plugin.setName("Plugin name");
    plugin.setVersion("0.0.1");
    pluginDao.add(plugin);

    PluginDataEntry entry = new PluginDataEntry();
    entry.setPlugin(plugin);
    entry.setKey("x");
    entry.setValue("y");

    pluginDataEntryDao.add(entry);

    PluginDataEntry entry2 = pluginDataEntryDao.getByKey(entry.getPlugin(), entry.getKey(), entry.getUser());
    assertEquals(entry, entry2);
    assertNull(pluginDataEntryDao.getByKey(entry.getPlugin(), "blabla", entry.getUser()));
  }

  @Test
  public void testGetByKeyForUserParam() {
    User user = createUser();
    Plugin plugin = new Plugin();
    plugin.setHash("x");
    plugin.setName("Plugin name");
    plugin.setVersion("0.0.1");
    pluginDao.add(plugin);

    PluginDataEntry entry = new PluginDataEntry();
    entry.setPlugin(plugin);
    entry.setUser(user);
    entry.setKey("x");
    entry.setValue("y");

    pluginDataEntryDao.add(entry);

    PluginDataEntry entry2 = pluginDataEntryDao.getByKey(entry.getPlugin(), entry.getKey(), user);
    assertEquals(entry, entry2);

    assertNull(pluginDataEntryDao.getByKey(entry.getPlugin(), "blabla", user));
    assertNull(pluginDataEntryDao.getByKey(entry.getPlugin(), entry.getKey(), null));
  }

}
