package lcsb.mapviewer.persist.dao.user;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.user.ResetPasswordToken;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class ResetPasswordTokenDaoTest extends PersistTestFunctions {

  @Autowired
  private ResetPasswordTokenDao resetPasswordTokenDao;

  @Autowired
  private UserDao userDao;

  @Test
  public void test() {
    User user = userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    List<ResetPasswordToken> tokens = resetPasswordTokenDao.getByUser(user);
    assertEquals(0, tokens.size());
  }

}
