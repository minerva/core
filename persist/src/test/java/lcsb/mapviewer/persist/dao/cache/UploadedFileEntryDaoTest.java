package lcsb.mapviewer.persist.dao.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class UploadedFileEntryDaoTest extends PersistTestFunctions {

  @Autowired
  private UploadedFileEntryDao uploadedFileEntryDao;

  @Autowired
  private DbUtils dbUtils;

  private boolean flush;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
    flush = dbUtils.isAutoFlush();
    dbUtils.setAutoFlush(true);
  }

  @After
  public void tearDown() throws Exception {
    dbUtils.setAutoFlush(flush);
  }

  @Test
  public void testAdd() {
    UploadedFileEntry entry = createUploadedFile();
    long count = uploadedFileEntryDao.getCount();
    uploadedFileEntryDao.add(entry);
    long count2 = uploadedFileEntryDao.getCount();
    uploadedFileEntryDao.delete(entry);
    long count3 = uploadedFileEntryDao.getCount();

    assertEquals(count + 1, count2);
    assertEquals(count2 - 1, count3);
  }

  @Test
  public void testRemoveOrphans() {

    UploadedFileEntry entry = new UploadedFileEntry();
    uploadedFileEntryDao.add(entry);

    uploadedFileEntryDao.removeOrphans();

    assertNull(uploadedFileEntryDao.getById(entry.getId()));
  }

}
