package lcsb.mapviewer.persist.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Calendar;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.Stacktrace;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class StacktraceDaoTest extends PersistTestFunctions {

  @Autowired
  private StacktraceDao stacktraceDao;

  @Test
  public void testAdd() {
    assertEquals(0, stacktraceDao.getCount());
    Stacktrace st = new Stacktrace(new Exception());
    stacktraceDao.add(st);
    assertEquals(1, stacktraceDao.getCount());
  }

  @Test
  public void testGetByUuid() {
    Stacktrace st = new Stacktrace(new Exception());
    stacktraceDao.add(st);

    assertNotNull(stacktraceDao.getById(st.getId()));
  }

  @Test
  public void testGetByInvalidUid() {
    assertNull(stacktraceDao.getById("blah"));
  }

  @Test
  public void testCleanup() {
    Stacktrace st = new Stacktrace(new Exception());
    stacktraceDao.add(st);
    assertEquals(1, stacktraceDao.getCount());

    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DATE, 1);
    stacktraceDao.removeBeforeDate(calendar);
    assertEquals(0, stacktraceDao.getCount());
  }

  @Test
  public void testCleanupLeaveNew() {
    Stacktrace st = new Stacktrace(new Exception());
    stacktraceDao.add(st);
    assertEquals(1, stacktraceDao.getCount());

    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.MINUTE, -60);
    stacktraceDao.removeBeforeDate(calendar);

    assertEquals(1, stacktraceDao.getCount());
  }
}
