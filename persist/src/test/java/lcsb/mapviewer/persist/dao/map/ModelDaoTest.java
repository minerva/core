package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitTypeFactor;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.ModulationReaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.ProjectDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

public class ModelDaoTest extends PersistTestFunctions {
  private final ModelComparator modelComparator = new ModelComparator();

  @Autowired
  private ModelDao modelDao;

  @Autowired
  private ProjectDao projectDao;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testLoadFromDb() throws Exception {
    Project project = createProject();
    Model model = project.getTopModel();
    projectDao.evict(project);

    modelDao.evict(model);
    ModelData model2 = modelDao.getById(model.getId());
    assertNotNull(model2);
    assertNotSame(model2, model);

    assertEquals(model.getElements().size(), model2.getElements().size());
    assertEquals(model.getLayers().size(), model2.getLayers().size());
    assertEquals(model.getReactions().size(), model2.getReactions().size());

    ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(model, new ModelFullIndexed(model2)));

  }

  public Project createProject() {
    Project project = new Project(TEST_PROJECT_ID);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    project.addModel(createModel());
    projectDao.add(project);
    return project;
  }

  @Test
  public void testModelWithAuthor() throws Exception {
    Project project = createProject();
    ModelData model = project.getTopModelData();
    model.addAuthor(new Author("Piotr", "G"));
    modelDao.update(model);
    projectDao.evict(project);

    modelDao.evict(model);
    ModelData model2 = modelDao.getById(model.getId());

    ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(new ModelFullIndexed(model), new ModelFullIndexed(model2)));

    project = projectDao.getById(project.getId());
  }

  @Test
  public void testIndexesReload() throws Exception {
    Project project = createProject();
    ModelData model = project.getTopModelData();
    modelDao.evict(model);
    ModelData model2 = modelDao.getById(model.getId());
    Model fullModel = new ModelFullIndexed(model2);
    assertNotNull(model2);

    assertEquals(model.getElements().size(), model2.getElements().size());
    assertEquals(model.getLayers().size(), model2.getLayers().size());
    assertEquals(model.getReactions().size(), model2.getReactions().size());

    // check if we really performed a test
    boolean test = false;

    for (final Element alias : model.getElements()) {
      assertNotNull(fullModel.getElementByElementId(alias.getElementId()));
      test = true;
    }
    assertTrue(test);

    test = false;
    for (final Element alias : model.getElements()) {
      if (alias instanceof Compartment) {
        assertNotNull(fullModel.getElementByElementId(alias.getElementId()));
        test = true;
      }
    }
    assertTrue(test);

    model2.setHeight(32);
    modelDao.update(model2);

  }

  @Test
  public void testReactionInModelAfterReload() throws Exception {
    Project project = createProject();
    Model model = project.getTopModel();

    projectDao.evict(project);

    Reaction reaction = model.getReactions().iterator().next();

    ModelData model2 = modelDao.getById(model.getId());

    Reaction reaction2 = null;
    for (final Reaction r : model2.getReactions()) {
      if (r.getIdReaction().equals(reaction.getIdReaction())) {
        reaction2 = r;
      }
    }
    assertNotNull(reaction2);
    assertNotEquals(reaction, reaction2);

    assertEquals(reaction.getNodes().size(), reaction2.getNodes().size());

  }

  @Test
  public void testReactionInWithKinetics() throws Exception {
    Project project = createProject();
    Model model = project.getTopModel();

    Reaction reaction = model.getReactions().iterator().next();
    SbmlKinetics kinetics = new SbmlKinetics();
    kinetics.addElement(reaction.getReactants().get(0).getElement());
    kinetics.addFunction(createFunction());
    model.addFunctions(kinetics.getFunctions());
    kinetics.addParameter(createParameter());
    model.addUnit(kinetics.getParameters().iterator().next().getUnits());
    reaction.setKinetics(kinetics);

    projectDao.update(project);
    ModelData model2 = modelDao.getById(model.getId());

    assertEquals(0, modelComparator.compare(model, new ModelFullIndexed(model2)));

    project = projectDao.getById(project.getId());
  }

  @Test
  public void testModulationReaction() throws Exception {
    Project project = createProject();
    Model model = project.getTopModel();

    ModulationReaction reaction = createModulationReaction(model.getElementByElementId("sa1117"),
        model.getElementByElementId("sa2"));
    model.addReaction(reaction);

    projectDao.update(project);
    projectDao.evict(project);
    modelDao.evict(model);
    ModelData model2 = modelDao.getById(model.getId());

    assertEquals(0, modelComparator.compare(model, new ModelFullIndexed(model2)));

    project = projectDao.getById(project.getId());
  }

  @Test
  public void testModelWithParameters() throws Exception {
    Project project = createProject();
    Model model = project.getTopModel();

    model.addParameter(createParameter());
    model.addUnit(model.getParameters().iterator().next().getUnits());

    projectDao.update(project);
    ModelData model2 = modelDao.getById(model.getId());

    assertEquals(0, modelComparator.compare(model, new ModelFullIndexed(model2)));

    project = projectDao.getById(project.getId());
  }

  private SbmlParameter createParameter() {
    SbmlParameter parameter = new SbmlParameter("param_id");
    parameter.setName("X");
    parameter.setValue(4.7);
    parameter.setUnits(createUnits());
    return parameter;
  }

  private SbmlUnit createUnits() {
    SbmlUnit unit = new SbmlUnit("unit_id");
    unit.setName("u name");
    unit.addUnitTypeFactor(new SbmlUnitTypeFactor(SbmlUnitType.AMPERE, 1, 2, 3));
    return unit;
  }

  private SbmlFunction createFunction() throws InvalidXmlSchemaException {
    SbmlFunction result = new SbmlFunction("fun_id");
    result.setDefinition("<lambda><bvar><ci> x </ci></bvar></lambda>");
    result.setName("fun name");
    return result;
  }

  @Test
  public void testModificationsInProteins() throws Exception {
    Model model = createModel();
    Project project = new Project("test_project_id");
    project.addModel(model);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    projectDao.add(project);

    modelDao.evict(model);
    projectDao.evict(project);
    Model model2 = new ModelFullIndexed(modelDao.getById(model.getId()));

    Protein originalSpecies = model.getElementByElementId("pr1");
    Protein fromDbSpecies = model2.getElementByElementId("pr1");

    assertNotEquals(originalSpecies, fromDbSpecies);
    assertEquals(originalSpecies.getModificationResidues().size(), fromDbSpecies.getModificationResidues().size());

    project = projectDao.getById(project.getId());
    projectDao.delete(project);
  }

  @Test
  public void testMiriamInSpecies() throws Exception {
    Model model = createModel();
    Project project = new Project("test_project_id");
    project.addModel(model);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    projectDao.add(project);

    modelDao.evict(model);
    projectDao.evict(project);
    Model model2 = new ModelFullIndexed(modelDao.getById(model.getId()));

    Protein originalSpecies = model.getElementByElementId("pr1");
    Protein fromDbSpecies = model2.getElementByElementId("pr1");

    assertNotEquals(originalSpecies, fromDbSpecies);
    assertEquals(originalSpecies.getMiriamData().size(), fromDbSpecies.getMiriamData().size());

    project = projectDao.getById(project.getId());
    projectDao.delete(project);
  }

  private Model createModel() {
    Model model = new ModelFullIndexed(null);

    GenericProtein alias = createProtein(264.8333333333335, 517.75, 86.0, 46.0, "sa2");
    model.addElement(alias);
    alias = createProtein(267.6666666666665, 438.75, 80.0, 40.0, "sa1117");
    model.addElement(alias);
    alias = createProtein(261.6666666666665, 600.75, 92.0, 52.0, "sa1119");
    model.addElement(alias);
    alias = createProtein(203.666666666667, 687.75, 98.0, 58.0, "sa1121");
    model.addElement(alias);

    alias = createProtein(817.714285714286, 287.642857142859, 80.0, 40.0, "sa1422");
    Species alias2 = createProtein(224.964285714286, 241.392857142859, 80.0, 40.0, "sa1419");
    Complex alias3 = createComplex(804.714285714286, 182.642857142859, 112.0, 172.0, "csa152");
    alias3.addSpecies(alias);
    alias3.addSpecies(alias2);
    alias.setComplex(alias3);
    alias2.setComplex(alias3);

    model.addElement(alias);
    model.addElement(alias2);
    model.addElement(alias3);

    Compartment compartment = createCompartment(380.0, 416.0, 1893.0, 1866.0, "ca1");
    model.addElement(compartment);
    model.setWidth(2000);
    model.setHeight(2000);

    Layer layer = createLayer();
    model.addLayer(layer);

    layer.addLayerRect(createRect());

    Reaction reaction = createReaction(alias2, alias);
    model.addReaction(reaction);

    alias = createProtein(264.8333333333335, 517.75, 86.0, 46.0, "pr1");
    model.addElement(alias);

    alias.addResidue(createResidue());

    alias.addMiriamData(new MiriamData(MiriamType.CHEBI, "c"));
    return model;
  }

  @Test
  public void testModelWithLayer() throws Exception {
    Project project = createProject();
    ModelData model = project.getTopModelData();
    Layer layer = model.getLayers().iterator().next();
    layer.addLayerOval(createOval());
    layer.addLayerLine(new PolylineData());
    layer.addLayerText(createText());
    modelDao.update(model);
    projectDao.evict(project);

    modelDao.evict(model);
    ModelData model2 = modelDao.getById(model.getId());

    ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(new ModelFullIndexed(model2), new ModelFullIndexed(model2)));

    project = projectDao.getById(project.getId());
  }

  @Test
  public void testFilterByProject() {
    Project project = createProject();

    Map<ModelProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(ModelProperty.PROJECT_ID, project.getProjectId());
    Page<ModelData> page = modelDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(ModelProperty.PROJECT_ID, TEST_PROJECT_2);
    page = modelDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

  @Test
  public void testFilterByMapId() {
    Project project = createProject();

    Map<ModelProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(ModelProperty.MAP_ID, project.getTopModelData().getId());
    Page<ModelData> page = modelDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(ModelProperty.MAP_ID, -1);
    page = modelDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

}
