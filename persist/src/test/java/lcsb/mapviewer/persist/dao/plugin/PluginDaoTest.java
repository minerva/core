package lcsb.mapviewer.persist.dao.plugin;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.plugin.Plugin;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class PluginDaoTest extends PersistTestFunctions {

  @Autowired
  private PluginDao pluginDao;

  @Test
  public void testGetPlugins() {
    assertTrue(pluginDao.getAll().size() >= 0);
  }

  @Test
  public void testAddPlugin() {
    Plugin plugin = new Plugin();
    plugin.setHash("x");
    plugin.setName("Plugin name");
    plugin.setVersion("0.0.1");
    plugin.addUrl("htpp://google.pl/");
    pluginDao.add(plugin);
    assertTrue(pluginDao.getAll().size() > 0);
  }

  @Test
  public void testAddPluginWithUrls() {
    Plugin plugin = new Plugin();
    plugin.setHash("x");
    plugin.setName("Plugin name");
    plugin.setVersion("0.0.1");
    plugin.addUrl("htpp://google.pl/");
    plugin.addUrl("htpp://google.pl/");
    pluginDao.add(plugin);
    pluginDao.flush();
    assertTrue(pluginDao.getAll().size() > 0);
  }

}
