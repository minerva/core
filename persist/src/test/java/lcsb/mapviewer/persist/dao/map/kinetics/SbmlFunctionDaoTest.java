package lcsb.mapviewer.persist.dao.map.kinetics;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.ProjectDao;

public class SbmlFunctionDaoTest extends PersistTestFunctions {

  @Autowired
  private ProjectDao projectDao;

  @Autowired
  private SbmlFunctionDao sbmlFunctionDao;

  @Test
  public void testFilterByProject() {
    Project project = createProject();

    Map<SbmlFunctionProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(SbmlFunctionProperty.PROJECT_ID, project.getProjectId());
    Page<SbmlFunction> page = sbmlFunctionDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(SbmlFunctionProperty.PROJECT_ID, TEST_PROJECT_2);
    page = sbmlFunctionDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

  @Test
  public void testFilterById() {
    Project project = createProject();

    Map<SbmlFunctionProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(SbmlFunctionProperty.ID, project.getTopModelData().getFunctions().iterator().next().getId());
    Page<SbmlFunction> page = sbmlFunctionDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(SbmlFunctionProperty.ID, -1);
    page = sbmlFunctionDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

  @Test
  public void testFilterByMapId() {
    Project project = createProject();

    Map<SbmlFunctionProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(SbmlFunctionProperty.MAP_ID, project.getTopModelData().getId());
    Page<SbmlFunction> page = sbmlFunctionDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(SbmlFunctionProperty.MAP_ID, -1);
    page = sbmlFunctionDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

  public Project createProject() {
    Project project = new Project(TEST_PROJECT_ID);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    ModelData model = new ModelData();
    project.addModel(model);
    SbmlFunction sbmlFunction = new SbmlFunction("x");
    model.addFunction(sbmlFunction);

    projectDao.add(project);
    return project;
  }

}
