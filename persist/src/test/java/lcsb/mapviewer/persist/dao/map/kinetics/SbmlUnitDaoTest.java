package lcsb.mapviewer.persist.dao.map.kinetics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.map.ModelDao;

public class SbmlUnitDaoTest extends PersistTestFunctions {

  @Autowired
  private ModelDao modelDao;

  @Autowired
  private ProjectDao projectDao;

  @Autowired
  private SbmlUnitDao sbmlUnitDao;

  @Test
  public void testGetById() {
    Model model = new ModelFullIndexed(null);
    SbmlUnit unit = new SbmlUnit("id");
    model.addUnit(unit);

    modelDao.add(model);

    SbmlUnit result = sbmlUnitDao.getById(unit.getId(), model.getModelData());
    assertEquals(unit, result);
  }

  @Test
  public void testFilterByProject() {
    Project project = createProject();

    Map<SbmlUnitProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(SbmlUnitProperty.PROJECT_ID, project.getProjectId());
    Page<SbmlUnit> page = sbmlUnitDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(2, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(SbmlUnitProperty.PROJECT_ID, TEST_PROJECT_2);
    page = sbmlUnitDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

  @Test
  public void testFilterById() {
    Project project = createProject();

    Map<SbmlUnitProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(SbmlUnitProperty.ID, project.getTopModelData().getUnits().iterator().next().getId());
    Page<SbmlUnit> page = sbmlUnitDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(SbmlUnitProperty.ID, -1);
    page = sbmlUnitDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

  @Test
  public void testFilterByMapId() {
    Project project = createProject();

    Map<SbmlUnitProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(SbmlUnitProperty.MAP_ID, project.getTopModelData().getId());
    Page<SbmlUnit> page = sbmlUnitDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(2, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(SbmlUnitProperty.MAP_ID, -1);
    page = sbmlUnitDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

  @Test
  public void testFilterByInUse() {
    createProject();

    Map<SbmlUnitProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(SbmlUnitProperty.IN_USE, false);
    Page<SbmlUnit> page = sbmlUnitDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());
    assertEquals("unused", page.getContent().get(0).getUnitId());

    filterOptions = new HashMap<>();
    filterOptions.put(SbmlUnitProperty.IN_USE, true);
    page = sbmlUnitDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());
    assertNotEquals("unused", page.getContent().get(0).getUnitId());

  }

  public Project createProject() {
    Project project = new Project(TEST_PROJECT_ID);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    ModelData model = new ModelData();
    project.addModel(model);
    SbmlUnit sbmlUnit = new SbmlUnit("x");
    model.addUnit(sbmlUnit);
    model.addUnit(new SbmlUnit("unused"));
    Protein protein = createProtein();
    protein.setSubstanceUnits(sbmlUnit);
    model.addElement(protein);

    projectDao.add(project);
    return project;
  }

}
