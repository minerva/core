package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.overlay.DataOverlayType;
import lcsb.mapviewer.model.overlay.GeneVariant;
import lcsb.mapviewer.model.overlay.GeneVariantDataOverlayEntry;
import lcsb.mapviewer.model.overlay.GenericDataOverlayEntry;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.ProjectDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DataOverlayDaoTest extends PersistTestFunctions {

  @Autowired
  private DataOverlayDao dataOverlayDao;

  @Autowired
  private ModelDao modelDao;

  @Autowired
  private ProjectDao projectDao;

  private Project project;

  @Before
  public void setUp() throws Exception {
    project = new Project(TEST_PROJECT_ID);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    projectDao.add(project);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAddDataOverlay() throws Exception {

    DataOverlay dataOverlay = createDataOverlay();
    dataOverlay.setProject(project);

    dataOverlayDao.add(dataOverlay);

    assertNotNull(dataOverlayDao.getById(dataOverlay.getId()));
  }

  @Test
  public void testDataOverlayWithEntries() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    GenericDataOverlayEntry genericEntry = new GenericDataOverlayEntry();
    genericEntry.addCompartment("tmp");
    genericEntry.setColor(Color.GREEN);
    genericEntry.addMiriamData(new MiriamData(MiriamType.BiGG_COMPARTMENT, "1234"));
    genericEntry.addType(Ion.class);
    dataOverlay.addEntry(genericEntry);

    dataOverlayDao.add(dataOverlay);

    assertNotNull(dataOverlayDao.getById(dataOverlay.getId()));
  }

  @Test
  public void testDataOverlayWithGeneVariantEntry() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    GeneVariantDataOverlayEntry geneVariantEntry = new GeneVariantDataOverlayEntry();
    GeneVariant gv = new GeneVariant();
    gv.setContig("ch1");
    gv.setOriginalDna("A");
    gv.setModifiedDna("C");
    gv.setPosition(12);
    geneVariantEntry.addGeneVariant(gv);
    dataOverlay.addEntry(geneVariantEntry);

    dataOverlayDao.add(dataOverlay);

    assertNotNull(dataOverlayDao.getById(dataOverlay.getId()));
  }

  @Test
  public void testGetElementByName() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    protein.setName("XYZ");
    model.addElement(protein);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.setName("XYZ");
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Element, DataOverlayEntry>> elements = dataOverlayDao.getElementsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(1, elements.size());
  }

  @Test
  public void testGetElementByInvalidName() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    protein.setName("XYZ");
    model.addElement(protein);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.setName("bla");
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Element, DataOverlayEntry>> elements = dataOverlayDao.getElementsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(0, elements.size());
  }

  @Test
  public void testGetElementByModelName() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    model.setName("mName");
    model.addElement(protein);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.setModelName("mName");
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Element, DataOverlayEntry>> elements = dataOverlayDao.getElementsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(1, elements.size());
  }

  @Test
  public void testGetElementByInvalidModelName() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    model.setName("mName");
    model.addElement(protein);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.setModelName("mxxx");
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Element, DataOverlayEntry>> elements = dataOverlayDao.getElementsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(0, elements.size());
  }

  @Test
  public void testGetElementByElementId() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    model.addElement(protein);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.setElementId(protein.getElementId());
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Element, DataOverlayEntry>> elements = dataOverlayDao.getElementsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(1, elements.size());
  }

  @Test
  public void testGetElementByUnknownElementId() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    model.addElement(protein);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.setElementId("bla");
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Element, DataOverlayEntry>> elements = dataOverlayDao.getElementsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(0, elements.size());
  }

  @Test
  public void testGetElementByCompartment() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Compartment compartment = createCompartment();
    compartment.setName("bla");
    Protein protein = createProtein();
    protein.setCompartment(compartment);
    model.addElement(compartment);
    model.addElement(protein);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.addCompartment("bla");
    entry.addCompartment("xyz");
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Element, DataOverlayEntry>> elements = dataOverlayDao.getElementsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(1, elements.size());
  }

  @Test
  public void testGetElementByUnknownCompartment() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Compartment compartment = createCompartment();
    compartment.setName("bla");
    Protein protein = createProtein();
    protein.setCompartment(compartment);
    model.addElement(compartment);
    model.addElement(protein);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.addCompartment("xyz");
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Element, DataOverlayEntry>> elements = dataOverlayDao.getElementsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(0, elements.size());
  }

  @Test
  public void testGetElementByMiram() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    protein.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "scna"));
    model.addElement(protein);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "Scna"));
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Element, DataOverlayEntry>> elements = dataOverlayDao.getElementsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(1, elements.size());
  }

  @Test
  public void testGetElementByUnknownMiram() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    protein.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "scna"));
    model.addElement(protein);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "Scnb"));
    entry.addMiriamData(new MiriamData(MiriamType.HGNC, "Scna"));
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Element, DataOverlayEntry>> elements = dataOverlayDao.getElementsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(0, elements.size());
  }

  @Test
  public void testGetElementByType() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    model.addElement(protein);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.addType(protein.getClass());
    entry.addType(protein.getClass());
    entry.addType(Species.class);
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Element, DataOverlayEntry>> elements = dataOverlayDao.getElementsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(1, elements.size());
  }

  @Test
  public void testGetElementByUnknownType() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    model.addElement(protein);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.addType(Ion.class);
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Element, DataOverlayEntry>> elements = dataOverlayDao.getElementsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(0, elements.size());
  }

  @Test
  public void testGetElementByUnknownId() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    model.addElement(protein);
    model.setProject(project);
    modelDao.add(model);

    dataOverlay.addEntry(new GenericDataOverlayEntry());
    dataOverlayDao.add(dataOverlay);

    List<Pair<Element, DataOverlayEntry>> elements = dataOverlayDao.getElementsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()), Collections.singletonList(-1));
    assertEquals(0, elements.size());
  }

  @Test
  public void testGetElementById() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    model.addElement(protein);
    model.setProject(project);
    modelDao.add(model);

    dataOverlay.addEntry(new GenericDataOverlayEntry());
    dataOverlayDao.add(dataOverlay);

    List<Pair<Element, DataOverlayEntry>> elements = dataOverlayDao.getElementsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()), Collections.singletonList(protein.getId()));
    assertEquals(1, elements.size());
  }

  @Test
  public void testGetElementReferenceById() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    model.addElement(protein);
    model.setProject(project);
    modelDao.add(model);

    dataOverlay.addEntry(new GenericDataOverlayEntry());
    dataOverlayDao.add(dataOverlay);

    List<Pair<Element, DataOverlayEntry>> elements = dataOverlayDao.getElementReferencesForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()), Collections.singletonList(protein.getId()));
    assertEquals(1, elements.size());
  }

  @Test
  public void testGetElementByMultiCriteria() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    protein.setName("XXX");
    protein.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "scna"));
    model.addElement(protein);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "Scna"));
    entry.addMiriamData(new MiriamData(MiriamType.HGNC, "Scna"));
    entry.setName("xxx");
    entry.setElementId(protein.getElementId());
    dataOverlay.addEntry(entry);
    GenericDataOverlayEntry entry2 = new GenericDataOverlayEntry();
    entry2.setName("ble");
    dataOverlay.addEntry(entry2);
    dataOverlayDao.add(dataOverlay);
    GenericDataOverlayEntry entry3 = new GenericDataOverlayEntry();
    entry3.setName("xxx");
    dataOverlay.addEntry(entry3);

    List<Pair<Element, DataOverlayEntry>> elements = dataOverlayDao.getElementsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(2, elements.size());

    logger.debug(elements);
  }

  @Test
  public void testGetElementByCriteriaWithMultiColelctionMatch() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    protein.setName("XXX");
    protein.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "scna"));
    protein.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "scnb"));
    model.addElement(protein);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "Scna"));
    entry.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "Scnb"));
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Element, DataOverlayEntry>> elements = dataOverlayDao.getElementsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(1, elements.size());
  }

  private DataOverlay createDataOverlay() {
    DataOverlay dataOverlay = new DataOverlay();
    dataOverlay.setName("test");
    dataOverlay.setColorSchemaType(DataOverlayType.GENERIC);
    dataOverlay.setCreator(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));

    dataOverlay.setInputData(createUploadedFile());
    dataOverlay.setProject(project);
    return dataOverlay;
  }

  @Test
  public void testGetReactionByName() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein1 = createProtein();
    Protein protein2 = createProtein();
    Reaction reaction = createReaction(protein1, protein2);
    reaction.setName("XYZ");
    model.addElement(protein1);
    model.addElement(protein2);
    model.addReaction(reaction);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.setName("XYZ");
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Reaction, DataOverlayEntry>> elements = dataOverlayDao.getReactionsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(1, elements.size());
  }

  @Test
  public void testGetReactionByInvalidName() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein1 = createProtein();
    Protein protein2 = createProtein();
    Reaction reaction = createReaction(protein1, protein2);
    reaction.setName("XYZ");
    model.addElement(protein1);
    model.addElement(protein2);
    model.addReaction(reaction);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.setName("bla");
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Reaction, DataOverlayEntry>> elements = dataOverlayDao.getReactionsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(0, elements.size());
  }

  @Test
  public void testGetReactionByEmptyName() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein1 = createProtein();
    Protein protein2 = createProtein();
    Reaction reaction = createReaction(protein1, protein2);
    reaction.setName("XYZ");
    model.addElement(protein1);
    model.addElement(protein2);
    model.addReaction(reaction);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Reaction, DataOverlayEntry>> elements = dataOverlayDao.getReactionsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(0, elements.size());
  }

  @Test
  public void testGetReactionByModelName() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein1 = createProtein();
    Protein protein2 = createProtein();
    Reaction reaction = createReaction(protein1, protein2);
    model.setName("mName");
    model.addElement(protein1);
    model.addElement(protein2);
    model.addReaction(reaction);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.setModelName("mName");
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Reaction, DataOverlayEntry>> elements = dataOverlayDao.getReactionsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(1, elements.size());
  }

  @Test
  public void testGetReactionByInvalidModelName() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein1 = createProtein();
    Protein protein2 = createProtein();
    Reaction reaction = createReaction(protein1, protein2);
    model.setName("mName");
    model.addElement(protein1);
    model.addElement(protein2);
    model.addReaction(reaction);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.setModelName("mxxx");
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Reaction, DataOverlayEntry>> elements = dataOverlayDao.getReactionsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(0, elements.size());
  }

  @Test
  public void testGetReactionByReactionId() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein1 = createProtein();
    Protein protein2 = createProtein();
    Reaction reaction = createReaction(protein1, protein2);
    model.addElement(protein1);
    model.addElement(protein2);
    model.addReaction(reaction);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.setElementId(reaction.getElementId());
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Reaction, DataOverlayEntry>> elements = dataOverlayDao.getReactionsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(1, elements.size());
  }

  @Test
  public void testGetReactionByUnknownReactionId() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein1 = createProtein();
    Protein protein2 = createProtein();
    Reaction reaction = createReaction(protein1, protein2);
    model.addElement(protein1);
    model.addElement(protein2);
    model.addReaction(reaction);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.setElementId("bla");
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Reaction, DataOverlayEntry>> elements = dataOverlayDao.getReactionsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(0, elements.size());
  }

  @Test
  public void testGetReactionByMiram() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein1 = createProtein();
    Protein protein2 = createProtein();
    Reaction reaction = createReaction(protein1, protein2);
    reaction.addMiriamData(new MiriamData(MiriamType.PUBMED, "132"));
    model.addElement(protein1);
    model.addElement(protein2);
    model.addReaction(reaction);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.addMiriamData(new MiriamData(MiriamType.PUBMED, "132"));
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Reaction, DataOverlayEntry>> elements = dataOverlayDao.getReactionsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(1, elements.size());
  }

  @Test
  public void testGetReactionByUnknownMiram() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein1 = createProtein();
    Protein protein2 = createProtein();
    Reaction reaction = createReaction(protein1, protein2);
    reaction.addMiriamData(new MiriamData(MiriamType.PUBMED, "132"));
    model.addElement(protein1);
    model.addElement(protein2);
    model.addReaction(reaction);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "Scnb"));
    entry.addMiriamData(new MiriamData(MiriamType.HGNC, "Scna"));
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Reaction, DataOverlayEntry>> elements = dataOverlayDao.getReactionsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(0, elements.size());
  }

  @Test
  public void testGetReactionByType() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein1 = createProtein();
    Protein protein2 = createProtein();
    Reaction reaction = createReaction(protein1, protein2);
    model.addElement(protein1);
    model.addElement(protein2);
    model.addReaction(reaction);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.addType(reaction.getClass());
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Reaction, DataOverlayEntry>> elements = dataOverlayDao.getReactionsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(1, elements.size());
  }

  @Test
  public void testGetReactionByUnknownType() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein1 = createProtein();
    Protein protein2 = createProtein();
    Reaction reaction = createReaction(protein1, protein2);
    model.addElement(protein1);
    model.addElement(protein2);
    model.addReaction(reaction);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.addType(Ion.class);
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Reaction, DataOverlayEntry>> elements = dataOverlayDao.getReactionsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(0, elements.size());
  }

  @Test
  public void testGetInvalidReactionId() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein1 = createProtein();
    Protein protein2 = createProtein();
    Reaction reaction = createReaction(protein1, protein2);
    model.addElement(protein1);
    model.addElement(protein2);
    model.addReaction(reaction);
    model.setProject(project);
    modelDao.add(model);

    dataOverlay.addEntry(new GenericDataOverlayEntry());
    dataOverlayDao.add(dataOverlay);

    List<Pair<Reaction, DataOverlayEntry>> elements = dataOverlayDao.getReactionsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()), Collections.singletonList(-1));
    assertEquals(0, elements.size());
  }

  @Test
  public void testGetReactionId() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein1 = createProtein();
    Protein protein2 = createProtein();
    Reaction reaction = createReaction(protein1, protein2);
    model.addElement(protein1);
    model.addElement(protein2);
    model.addReaction(reaction);
    model.setProject(project);
    modelDao.add(model);

    dataOverlay.addEntry(new GenericDataOverlayEntry());
    dataOverlayDao.add(dataOverlay);

    List<Pair<Reaction, DataOverlayEntry>> elements = dataOverlayDao.getReactionsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()), Collections.singletonList(reaction.getId()));
    assertEquals(1, elements.size());
  }

  @Test
  public void testGetReactionReferencesId() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein1 = createProtein();
    Protein protein2 = createProtein();
    Reaction reaction = createReaction(protein1, protein2);
    model.addElement(protein1);
    model.addElement(protein2);
    model.addReaction(reaction);
    model.setProject(project);
    modelDao.add(model);

    dataOverlay.addEntry(new GenericDataOverlayEntry());
    dataOverlayDao.add(dataOverlay);

    List<Pair<Reaction, DataOverlayEntry>> elements = dataOverlayDao.getReactionReferencesForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()), Collections.singletonList(reaction.getId()));
    assertEquals(1, elements.size());
  }

  @Test
  public void testGetReactionByMultiCriteria() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein1 = createProtein();
    Protein protein2 = createProtein();
    Reaction reaction = createReaction(protein1, protein2);
    reaction.addMiriamData(new MiriamData(MiriamType.PUBMED, "132"));
    reaction.setName("666");
    model.addElement(protein1);
    model.addElement(protein2);
    model.addReaction(reaction);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.addMiriamData(new MiriamData(MiriamType.PUBMED, "132"));
    entry.addMiriamData(new MiriamData(MiriamType.PUBMED, "123"));
    entry.setName("666");
    entry.setElementId(reaction.getElementId());
    dataOverlay.addEntry(entry);
    dataOverlay.addEntry(new GenericDataOverlayEntry());
    GenericDataOverlayEntry entry2 = new GenericDataOverlayEntry();
    entry2.setName("ble");
    dataOverlay.addEntry(entry2);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Reaction, DataOverlayEntry>> elements = dataOverlayDao.getReactionsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(1, elements.size());
  }

  @Test
  public void testGetReactionByCriteriaWithMultiColelctionMatch() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein1 = createProtein();
    Protein protein2 = createProtein();
    Reaction reaction = createReaction(protein1, protein2);
    reaction.addMiriamData(new MiriamData(MiriamType.PUBMED, "132"));
    reaction.addMiriamData(new MiriamData(MiriamType.PUBMED, "123"));
    reaction.setName("666");
    model.addElement(protein1);
    model.addElement(protein2);
    model.addReaction(reaction);
    model.setProject(project);
    modelDao.add(model);

    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.addMiriamData(new MiriamData(MiriamType.PUBMED, "123"));
    entry.addMiriamData(new MiriamData(MiriamType.PUBMED, "132"));
    dataOverlay.addEntry(entry);
    dataOverlayDao.add(dataOverlay);

    List<Pair<Reaction, DataOverlayEntry>> elements = dataOverlayDao.getReactionsForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()));
    assertEquals(1, elements.size());
  }

  @Test
  public void testGetElementReferenceWithEmptyDataOverlay() throws Exception {
    DataOverlay dataOverlay = createDataOverlay();

    Model model = new ModelFullIndexed(null);
    Protein protein = createProtein();
    model.addElement(protein);
    model.setProject(project);
    modelDao.add(model);

    dataOverlay.addEntry(new GenericDataOverlayEntry());
    dataOverlayDao.add(dataOverlay);

    List<Pair<Element, DataOverlayEntry>> elements = dataOverlayDao.getElementReferencesForModels(dataOverlay.getId(),
        Collections.singletonList(model.getModelData()), new ArrayList<>());
    assertEquals(0, elements.size());
  }

}
