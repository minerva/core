package lcsb.mapviewer.persist.dao.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.cache.BigFileEntry;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class BigFileEntryDaoTest extends PersistTestFunctions {

  @Autowired
  private BigFileEntryDao bigFileEntryDao;

  @Autowired
  private DbUtils dbUtils;

  private boolean flush;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
    flush = dbUtils.isAutoFlush();
    dbUtils.setAutoFlush(true);
  }

  @After
  public void tearDown() throws Exception {
    dbUtils.setAutoFlush(flush);
  }

  @Test
  public void testAdd() {
    BigFileEntry entry = new BigFileEntry();
    long count = bigFileEntryDao.getCount();
    bigFileEntryDao.add(entry);
    long count2 = bigFileEntryDao.getCount();
    bigFileEntryDao.delete(entry);
    long count3 = bigFileEntryDao.getCount();

    assertEquals(count + 1, count2);
    assertEquals(count2 - 1, count3);
  }

  @Test
  public void testGetByUrl() {
    String url = "xxx.yyy";
    BigFileEntry entry = new BigFileEntry();
    entry.setUrl(url);

    BigFileEntry result = bigFileEntryDao.getByUrl(url);
    assertNull(result);
    bigFileEntryDao.add(entry);

    result = bigFileEntryDao.getByUrl(url);
    assertEquals(entry, result);

    bigFileEntryDao.delete(entry);
  }

}
