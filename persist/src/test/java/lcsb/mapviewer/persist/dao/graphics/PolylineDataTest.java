package lcsb.mapviewer.persist.dao.graphics;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.ProjectDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class PolylineDataTest extends PersistTestFunctions {

  @Autowired
  protected PolylineDao polylineDao;

  @Autowired
  protected ProjectDao projectDao;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAdd() {
    final PolylineData pd = new PolylineData(new Point2D.Double(1, 1), new Point2D.Double(2, 3));
    pd.getBeginAtd().setArrowType(ArrowType.BLANK_CROSSBAR);
    pd.getBeginAtd().setArrowLineType(LineType.DASHED);
    pd.getBeginAtd().setLen(102);
    pd.setColor(Color.CYAN);
    pd.setType(LineType.SOLID);
    pd.setWidth(2);
    pd.addLine(new Point2D.Double(2, 3), new Point2D.Double(10, 11));
    polylineDao.add(pd);
    PolylineData pd2 = polylineDao.getById(pd.getId());
    assertNotNull(pd2);

    assertEquals(ArrowType.BLANK_CROSSBAR, pd2.getBeginAtd().getArrowType());
    assertEquals(LineType.DASHED, pd2.getBeginAtd().getArrowLineType());
    assertEquals(102, pd2.getBeginAtd().getLen(), Configuration.EPSILON);
    assertEquals(Color.CYAN, pd2.getColor());
    assertEquals(LineType.SOLID, pd2.getType());
    assertEquals(2, pd2.getWidth(), Configuration.EPSILON);
    assertEquals(pd2.getLines().get(0).getP1(), new Point2D.Double(1, 1));
    assertEquals(pd2.getLines().get(0).getP2(), new Point2D.Double(2, 3));
    assertEquals(pd2.getLines().get(1).getP1(), new Point2D.Double(2, 3));
    assertEquals(pd2.getLines().get(1).getP2(), new Point2D.Double(10, 11));

    polylineDao.delete(pd);
    pd2 = polylineDao.getById(pd.getId());
    assertNull(pd2);
  }

  @Test
  public void testFilterByProject() {
    final Project project = createProject();

    Map<PolylineDataProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(PolylineDataProperty.PROJECT_ID, project.getProjectId());
    Page<PolylineData> page = polylineDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(PolylineDataProperty.PROJECT_ID, TEST_PROJECT_2);
    page = polylineDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

  @Test
  public void testFilterByPolylineDataId() {
    final Project project = createProject();
    final Layer layer = project.getTopModel().getLayers().iterator().next();

    Map<PolylineDataProperty, Object> filterOptions = new HashMap<>();
    final PolylineData polylineData = layer.getLines().iterator().next();
    final int polylineDataId = polylineData.getId();
    filterOptions.put(PolylineDataProperty.ID, Collections.singletonList(polylineDataId));
    Page<PolylineData> page = polylineDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(PolylineDataProperty.ID, Collections.singletonList(-1));
    page = polylineDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

  public Project createProject() {
    final Project project = new Project(TEST_PROJECT_ID);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));

    final Model model = new ModelFullIndexed(null);

    final Layer layer = createLayer();
    final PolylineData rect = createLine();
    layer.addLayerLine(rect);
    model.addLayer(layer);

    project.addModel(model);

    projectDao.add(project);
    return project;
  }

  @Test
  public void testFilterByLayer() {
    final Project project = createProject();

    Layer layer = project.getTopModel().getLayers().iterator().next();

    Map<PolylineDataProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(PolylineDataProperty.PROJECT_ID, project.getProjectId());
    filterOptions.put(PolylineDataProperty.LAYER_ID, Collections.singletonList(layer.getId()));
    Page<PolylineData> page = polylineDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(PolylineDataProperty.PROJECT_ID, project.getProjectId());
    filterOptions.put(PolylineDataProperty.LAYER_ID, Collections.singletonList(-1));
    page = polylineDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

}
