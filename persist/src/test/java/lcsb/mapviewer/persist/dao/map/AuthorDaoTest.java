package lcsb.mapviewer.persist.dao.map;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.ProjectDao;

public class AuthorDaoTest extends PersistTestFunctions {

  @Autowired
  private ProjectDao projectDao;

  @Autowired
  private AuthorDao authorDao;

  @Test
  public void testFilterByProject() {
    createProject();

    Map<AuthorProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(AuthorProperty.PROJECT_ID, TEST_PROJECT);
    Page<Author> page = authorDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(AuthorProperty.PROJECT_ID, TEST_PROJECT_2);
    page = authorDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

  @Test
  public void testFilterByMapId() {
    Project project = createProject();

    Map<AuthorProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(AuthorProperty.MAP_ID, project.getTopModelData().getId());
    Page<Author> page = authorDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(AuthorProperty.MAP_ID, -1);
    page = authorDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

  @Test
  public void testFilterById() {
    Project project = createProject();

    Map<AuthorProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(AuthorProperty.ID, project.getTopModelData().getAuthors().get(0).getId());
    Page<Author> page = authorDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(AuthorProperty.ID, -1);
    page = authorDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

  private Project createProject() {
    Project project = new Project(TEST_PROJECT);
    project.setOwner(super.createUser());
    ModelData map = new ModelData();
    Author author = new Author("John", "Doe");
    map.addAuthor(author);
    project.addModel(map);
    projectDao.add(project);
    return project;
  }

}
