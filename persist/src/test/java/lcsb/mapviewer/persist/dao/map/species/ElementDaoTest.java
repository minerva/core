package lcsb.mapviewer.persist.dao.map.species;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.ElementSubmodelConnection;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Chemical;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.Structure;
import lcsb.mapviewer.model.map.species.field.UniprotRecord;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.map.ModelDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class ElementDaoTest extends PersistTestFunctions {

  @Autowired
  private ElementDao elementDao;

  @Autowired
  private ModelDao modelDao;

  @Autowired
  private DbUtils dbUtils;

  @Autowired
  private ProjectDao projectDao;

  private final Integer testChargeVal = 1;
  private final String testIdAlias = "a";
  private final Double testInitialAmount = 2.0;
  private final Double testInitialConcentration = 3.0;
  private final String testName = "d";
  private final String testNotes = "e";
  private final Boolean testOnlySubstanceunits = true;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  private Project createProject() {
    final Project project = new Project(TEST_PROJECT_ID);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    project.addModel(createModel());
    projectDao.add(project);
    return project;
  }

  private Model createModel() {
    final Model model = new ModelFullIndexed(null);
    model.addElement(createProtein(264.8333333333335, 517.75, 86.0, 46.0, "sa2"));
    model.addElement(createProtein(267.6666666666665, 438.75, 80.0, 40.0, "sa1117"));
    model.addElement(createProtein(261.6666666666665, 600.75, 92.0, 52.0, "sa1119"));
    model.addElement(createProtein(203.666666666667, 687.75, 98.0, 58.0, "sa1121"));

    final Species alias = createProtein(817.714285714286, 287.642857142859, 80.0, 40.0, "sa1422");
    final Species alias2 = createProtein(224.964285714286, 241.392857142859, 80.0, 40.0, "sa1419");
    final Complex alias3 = createComplex(804.714285714286, 182.642857142859, 112.0, 172.0, "csa152");
    alias3.addSpecies(alias);
    alias3.addSpecies(alias2);
    alias.setComplex(alias3);
    alias2.setComplex(alias3);

    model.addElement(alias);
    model.addElement(alias2);
    model.addElement(alias3);

    model.addElement(createCompartment(380.0, 416.0, 1893.0, 1866.0, "ca1"));
    model.setWidth(2000);
    model.setHeight(2000);
    return model;
  }

  @Test
  public void saveAliasWithSubmodelTest() throws Exception {
    final Project project = createProject();

    final long count = modelDao.getCount();
    final Model model = createModel();
    final Model model1 = createModel();
    final Element element = model.getElementByElementId("sa2");
    final ElementSubmodelConnection submodel = new ElementSubmodelConnection(model1, SubmodelType.UNKNOWN);
    element.setSubmodel(submodel);
    project.addModel(model);

    projectDao.add(project);

    final long count2 = modelDao.getCount();
    assertEquals(count + 2, count2);
  }

  @Test
  public void saveWithUniprotData() throws Exception {
    final Project project = createProject();
    final Model model = project.getTopModel();

    final Species element = model.getElementByElementId("sa2");
    final UniprotRecord uniprotRecord = new UniprotRecord();
    final Structure s = new Structure();
    s.setPdbId("x");
    s.setChainId("y");
    final List<Structure> list = new ArrayList<>();
    list.add(s);
    s.setUniprot(uniprotRecord);
    uniprotRecord.addStructures(list);
    uniprotRecord.setSpecies(element);
    element.getUniprots().add(uniprotRecord);

    modelDao.add(model);
  }

  @Test
  public void testAdd() throws Exception {
    final GenericProtein sp = createProtein(testIdAlias);
    sp.setCharge(testChargeVal);
    sp.setInitialAmount(testInitialAmount);
    sp.setInitialConcentration(testInitialConcentration);
    sp.setName(testName);
    sp.setNotes(testNotes);
    sp.setOnlySubstanceUnits(testOnlySubstanceunits);

    final Compartment parent = createCompartment("comp id");
    sp.setCompartment(parent);

    final MiriamData md = new MiriamData(MiriamRelationType.BQ_BIOL_IS_DESCRIBED_BY, MiriamType.HGNC_SYMBOL, "SNCA");
    sp.addMiriamData(md);

    elementDao.add(sp);

    Species sp2 = (Species) elementDao.getById(sp.getId());
    assertNotNull(sp2);
    assertEquals(sp.getCharge(), sp2.getCharge());
    assertEquals(sp.getElementId(), sp2.getElementId());
    assertEquals(sp.getInitialAmount(), sp2.getInitialAmount());
    assertEquals(sp.getInitialConcentration(), sp2.getInitialConcentration());
    assertEquals(sp.getName(), sp2.getName());
    assertEquals(sp.getNotes(), sp2.getNotes());
    assertEquals(sp.hasOnlySubstanceUnits(), sp2.hasOnlySubstanceUnits());

    final Compartment parent2 = sp2.getCompartment();
    assertNotNull(parent2);
    assertEquals("comp id", parent2.getElementId());

    assertNotNull(sp2.getMiriamData());

    final MiriamData md2 = sp2.getMiriamData().iterator().next();
    assertNotNull(md2);
    assertEquals(md.getDataType(), md2.getDataType());
    assertEquals(md.getRelationType(), md2.getRelationType());
    assertEquals(md.getResource(), md2.getResource());

    elementDao.delete(sp);
    sp2 = (Species) elementDao.getById(sp.getId());
    assertNull(sp2);
  }

  @Test
  public void testProtein() throws Exception {
    final Protein protein = createProtein(testIdAlias);
    final Residue mr = createResidue();
    protein.addResidue(mr);

    elementDao.add(protein);

    Protein sp2 = (Protein) elementDao.getById(protein.getId());
    assertNotNull(sp2);
    assertEquals(protein.getElementId(), sp2.getElementId());

    assertNotNull(sp2.getModificationResidues());
    assertEquals(1, sp2.getModificationResidues().size());

    final Residue copy = (Residue) sp2.getModificationResidues().get(0);
    assertEquals(copy.getX(), mr.getX());
    assertEquals(copy.getY(), mr.getY());
    assertEquals(copy.getIdModificationResidue(), mr.getIdModificationResidue());
    assertEquals(copy.getName(), mr.getName());
    assertEquals(copy.getState(), mr.getState());

    elementDao.delete(sp2);
    sp2 = (Protein) elementDao.getById(protein.getId());
    assertNull(sp2);
  }

  @Test
  public void testRna() throws Exception {
    final Rna sp = createRna(testIdAlias);
    final ModificationSite mr = createModificationSite();
    sp.addModificationSite(mr);

    elementDao.add(sp);
    elementDao.evict(sp);

    Rna sp2 = (Rna) elementDao.getById(sp.getId());
    assertNotNull(sp2);
    assertEquals(sp.getElementId(), sp2.getElementId());

    assertNotNull(sp2.getModificationResidues());
    assertEquals(1, sp2.getModificationResidues().size());

    final ModificationSite copy = (ModificationSite) sp2.getModificationResidues().get(0);
    assertEquals(copy.getId(), mr.getId());
    assertEquals(copy.getName(), mr.getName());
    assertEquals(copy.getState(), mr.getState());

    elementDao.delete(sp2);
    sp2 = (Rna) elementDao.getById(sp.getId());
    assertNull(sp2);
  }

  @Test
  public void testAntisenseRna() throws Exception {
    final AntisenseRna sp = createAntisenseRna(testIdAlias);

    final ModificationSite mr = createModificationSite();
    sp.addModificationSite(mr);

    elementDao.add(sp);
    elementDao.evict(sp);

    AntisenseRna sp2 = (AntisenseRna) elementDao.getById(sp.getId());
    assertNotNull(sp2);
    assertEquals(sp.getElementId(), sp2.getElementId());

    assertNotNull(sp2.getModificationResidues());
    assertEquals(1, sp2.getModificationResidues().size());

    assertEquals(sp2.getModificationResidues().get(0).getIdModificationResidue(), mr.getIdModificationResidue());
    assertEquals(sp2.getModificationResidues().get(0).getName(), mr.getName());

    elementDao.delete(sp2);
    sp2 = (AntisenseRna) elementDao.getById(sp.getId());
    assertNull(sp2);
  }

  @Test
  public void testSynonymsInAlias() throws Exception {
    final Protein protein = createProtein(testIdAlias);
    protein.addSynonym("Synonym");
    protein.addSynonym("Synonym A");
    protein.addSynonym("A");

    protein.addFormerSymbol("Sym");
    protein.addFormerSymbol("Sym A");
    protein.addFormerSymbol("DD");

    elementDao.add(protein);
    elementDao.flush();

    elementDao.evict(protein);
    Protein sp2 = (Protein) elementDao.getById(protein.getId());

    assertNotNull(sp2.getSynonyms());
    assertEquals(protein.getSynonyms().size(), sp2.getSynonyms().size());

    for (int i = 0; i < protein.getSynonyms().size(); i++) {
      assertEquals(protein.getSynonyms().get(i), sp2.getSynonyms().get(i));
    }

    assertNotNull(sp2.getFormerSymbols());
    assertEquals(protein.getFormerSymbols().size(), sp2.getFormerSymbols().size());

    for (int i = 0; i < protein.getFormerSymbols().size(); i++) {
      assertEquals(protein.getFormerSymbols().get(i), sp2.getFormerSymbols().get(i));
    }

    elementDao.delete(sp2);
    sp2 = (Protein) elementDao.getById(protein.getId());
    assertNull(sp2);
  }

  @Test
  public void testChemicals() throws Exception {
    final Chemical ion = createIon(testIdAlias);
    ion.setSmiles("smile");

    elementDao.add(ion);
    elementDao.flush();

    elementDao.evict(ion);
    Ion sp2 = (Ion) elementDao.getById(ion.getId());

    assertNotNull(sp2.getSynonyms());
    assertEquals(ion.getSynonyms().size(), sp2.getSynonyms().size());

    assertEquals(ion.getSmiles(), sp2.getSmiles());

    elementDao.delete(sp2);
    sp2 = (Ion) elementDao.getById(ion.getId());
    assertNull(sp2);
  }

  @Test
  public void testPhenotype() throws Exception {
    final Phenotype phenotype = createPhenotype(testIdAlias);

    elementDao.add(phenotype);
    elementDao.flush();

    elementDao.evict(phenotype);
    Phenotype sp2 = (Phenotype) elementDao.getById(phenotype.getId());

    assertNotNull(sp2.getSynonyms());
    assertEquals(phenotype.getSynonyms().size(), sp2.getSynonyms().size());

    elementDao.delete(sp2);
    sp2 = (Phenotype) elementDao.getById(phenotype.getId());
    assertNull(sp2);
  }

  @Test
  public void testSearchClosest() throws Exception {
    final Model map = new ModelFullIndexed(null);
    map.addElement(createProtein(645.0, 220.5, 70.0, 25.0, "sa6"));
    map.addElement(createCompartment(488.0, 20.0, 275.0, 549.0, "ca2"));
    map.addElement(createCompartment(14.0, 13.0, 184.0, 377.0, "ca1"));
    map.addElement(createProtein(374.0, 314.0, 80.0, 40.0, "sa8"));
    map.addElement(createProtein(580.0, 484.0, 80.0, 40.0, "sa10"));
    map.addElement(createProtein(254.0, 203.0, 80.0, 40.0, "sa4"));
    map.addElement(createProtein(50.0, 468.0, 80.0, 40.0, "sa9"));
    map.addElement(createProtein(52.0, 310.0, 80.0, 40.0, "sa7"));
    map.addElement(createProtein(54.0, 57.0, 80.0, 40.0, "sa1"));
    map.addElement(createProtein(506.0, 51.0, 80.0, 30.0, "sa5"));
    map.addElement(createProtein(245.0, 39.0, 80.0, 40.0, "sa2"));
    map.addElement(createProtein(43.0, 169.0, 80.0, 40.0, "sa3"));

    modelDao.add(map);

    final List<Element> elements = elementDao.getByCoordinates(map.getModelData(), new Point2D.Double(0, 0), 5,
        new ArrayList<>());

    assertNotNull(elements);
    assertEquals(5, elements.size());
    assertEquals("sa1", elements.get(0).getElementId());
    assertEquals("sa3", elements.get(1).getElementId());
    assertEquals("sa2", elements.get(2).getElementId());

  }

  @Test
  public void testSearchWithInsideMatch() throws Exception {
    final Model map = new ModelFullIndexed(null);
    map.addElement(createProtein(645.0, 220.5, 70.0, 25.0, "sa6"));
    map.addElement(createCompartment(488.0, 20.0, 275.0, 549.0, "ca2"));
    map.addElement(createCompartment(14.0, 13.0, 184.0, 377.0, "ca1"));
    map.addElement(createProtein(374.0, 314.0, 80.0, 40.0, "sa8"));
    map.addElement(createProtein(580.0, 484.0, 80.0, 40.0, "sa10"));
    map.addElement(createProtein(254.0, 203.0, 80.0, 40.0, "sa4"));
    map.addElement(createProtein(50.0, 468.0, 80.0, 40.0, "sa9"));
    map.addElement(createProtein(52.0, 310.0, 80.0, 40.0, "sa7"));
    map.addElement(createProtein(54.0, 57.0, 80.0, 40.0, "sa1"));
    map.addElement(createProtein(506.0, 51.0, 80.0, 30.0, "sa5"));
    map.addElement(createProtein(245.0, 39.0, 80.0, 40.0, "sa2"));
    map.addElement(createProtein(-1000.0, -1000.0, 2000.0, 2000.0, "sa3"));

    modelDao.add(map);

    final List<Element> elements = elementDao.getByCoordinates(map.getModelData(), new Point2D.Double(10, 10), 5,
        new ArrayList<>());

    assertNotNull(elements);
    assertEquals(5, elements.size());
    assertEquals("sa3", elements.get(0).getElementId());

  }

  @Test
  public void testSearchClosestWithModifications() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Protein protein = createProtein(645.0, 220.5, 70.0, 25.0, "sa1");
    protein.addResidue(createResidue());
    map.addElement(protein);

    modelDao.add(map);

    final List<Element> elements = elementDao.getByCoordinates(map.getModelData(), new Point2D.Double(0, 0), 5,
        new ArrayList<>());

    assertNotNull(elements);
    assertEquals(1, elements.size());
    assertEquals("sa1", elements.get(0).getElementId());

  }

  @Test
  public void testSearchInside() throws Exception {
    final Model map = new ModelFullIndexed(null);
    map.addElement(createProtein(110.0, 110, 80.0, 80.0, "sa2"));
    map.addElement(createProtein(100.0, 100, 100.0, 100.0, "sa1"));
    map.addElement(createProtein(130.0, 130, 40.0, 40.0, "sa4"));
    map.addElement(createProtein(120.0, 120, 60.0, 60.0, "sa3"));
    for (final Element element : map.getElements()) {
      element.setZ(0);
    }

    modelDao.add(map);

    final List<Element> elements = elementDao.getByCoordinates(map.getModelData(), new Point2D.Double(150, 150), 1,
        new ArrayList<>());

    assertNotNull(elements);
    assertEquals(1, elements.size());
    assertEquals("sa4", elements.get(0).getElementId());

  }

  @Test
  public void testSearchByCoordinatesWithZIndex() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Protein p1 = createProtein(110.0, 110, 80.0, 80.0, "sa1");
    p1.setZ(1);
    final Protein p2 = createProtein(110.0, 110, 80.0, 80.0, "sa2");
    p2.setZ(-1);
    final Protein p3 = createProtein(110.0, 110, 80.0, 80.0, "sa3");
    p3.setZ(0);
    map.addElement(p1);
    map.addElement(p2);
    map.addElement(p3);

    modelDao.add(map);

    final List<Element> elements = elementDao.getByCoordinates(map.getModelData(), new Point2D.Double(150, 150), 3,
        new ArrayList<>());

    assertNotNull(elements);
    assertEquals(p1, elements.get(0));

  }

  @Test
  public void testSearchByCoordinatesWithType() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Ion p2 = createIon("sa2");
    map.addElement(createProtein(110.0, 110, 80.0, 80.0, "sa1"));
    map.addElement(p2);

    modelDao.add(map);

    List<Element> elements = elementDao.getByCoordinates(map.getModelData(), new Point2D.Double(150, 150), 3,
        Collections.singletonList(Ion.class));

    assertNotNull(elements);
    assertEquals(1, elements.size());
    assertEquals(p2, elements.get(0));

  }

  @Test
  public void testSearchEmptyMapByCoordinates() throws Exception {
    final Model map = new ModelFullIndexed(null);
    modelDao.add(map);

    final List<Element> elements = elementDao.getByCoordinates(map.getModelData(), new Point2D.Double(150, 150), 1,
        new ArrayList<>());

    assertNotNull(elements);
    assertEquals(0, elements.size());
  }

  @Test
  public void testSearchByAnnotation() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Protein p2 = createProtein();
    p2.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    map.addElement(p2);

    modelDao.add(map);

    Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.MAP, Collections.singletonList(map.getModelData()));
    properties.put(ElementProperty.ANNOTATION, Collections.singletonList(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA")));

    final Page<Element> elements = elementDao.getAll(properties, PageRequest.of(0, 10));

    assertNotNull(elements);
    assertEquals(1, elements.getNumberOfElements());
    assertEquals(p2, elements.getContent().get(0));
  }

  @Test
  public void testSearchByAnnotationCaseInsensitive() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Protein p2 = createProtein();
    p2.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    map.addElement(p2);

    modelDao.add(map);

    Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.MAP, Collections.singletonList(map.getModelData()));
    properties.put(ElementProperty.ANNOTATION, Collections.singletonList(new MiriamData(MiriamType.HGNC_SYMBOL, "snca")));

    final Page<Element> elements = elementDao.getAll(properties, PageRequest.of(0, 10));

    assertNotNull(elements);
    assertEquals(1, elements.getNumberOfElements());
    assertEquals(p2, elements.getContent().get(0));
  }

  @Test
  public void testSearchByAnnotationInFewMaps() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Protein p2 = createProtein();
    p2.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    map.addElement(p2);

    modelDao.add(map);
    final Model map2 = new ModelFullIndexed(null);
    modelDao.add(map2);

    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.MAP, Arrays.asList(map.getModelData(), map2.getModelData()));
    properties.put(ElementProperty.ANNOTATION, Collections.singletonList(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA")));

    final Page<Element> elements = elementDao.getAll(properties, PageRequest.of(0, 10));

    assertNotNull(elements);
    assertEquals(1, elements.getNumberOfElements());
    assertEquals(p2, elements.getContent().get(0));
  }

  @Test
  public void testSearchByNonExistingAnnotation() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Protein p2 = createProtein();
    p2.addMiriamData(new MiriamData(MiriamType.HGNC, "SNCA"));
    map.addElement(p2);

    modelDao.add(map);

    Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.MAP, Collections.singletonList(map.getModelData()));
    properties.put(ElementProperty.ANNOTATION, Collections.singletonList(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA")));

    final Page<Element> elements = elementDao.getAll(properties, PageRequest.of(0, 10));

    assertNotNull(elements);
    assertEquals(0, elements.getNumberOfElements());
  }

  @Test
  public void testSearchByFilterAnnotation() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Protein p2 = createProtein();
    p2.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    map.addElement(p2);

    modelDao.add(map);

    Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.MAP, Collections.singletonList(map.getModelData()));
    properties.put(ElementProperty.ANNOTATION, Collections.singletonList(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA")));

    final Page<Element> elements = elementDao.getAll(properties, PageRequest.of(0, 10));

    assertNotNull(elements);
    assertEquals(1, elements.getNumberOfElements());
    assertEquals(p2, elements.getContent().get(0));
  }

  @Test
  public void testSearchByFilterAnnotationCaseInsensitive() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Protein p2 = createProtein();
    p2.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    map.addElement(p2);

    modelDao.add(map);

    Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.MAP, Collections.singletonList(map.getModelData()));
    properties.put(ElementProperty.ANNOTATION, Collections.singletonList(new MiriamData(MiriamType.HGNC_SYMBOL, "snca")));

    final Page<Element> elements = elementDao.getAll(properties, PageRequest.of(0, 10));

    assertNotNull(elements);
    assertEquals(1, elements.getNumberOfElements());
    assertEquals(p2, elements.getContent().get(0));
  }

  @Test
  public void testSearchByFilterAnnotationInFewMaps() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Protein p2 = createProtein();
    p2.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    map.addElement(p2);

    modelDao.add(map);
    final Model map2 = new ModelFullIndexed(null);
    modelDao.add(map2);

    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.MAP, Arrays.asList(map.getModelData(), map2.getModelData()));
    properties.put(ElementProperty.ANNOTATION, Collections.singletonList(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA")));

    final Page<Element> elements = elementDao.getAll(properties, PageRequest.of(0, 10));

    assertNotNull(elements);
    assertEquals(1, elements.getNumberOfElements());
    assertEquals(p2, elements.getContent().get(0));
  }

  @Test
  public void testSearchByFilterNonExistingAnnotation() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Protein p2 = createProtein();
    p2.addMiriamData(new MiriamData(MiriamType.HGNC, "SNCA"));
    map.addElement(p2);

    modelDao.add(map);

    Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.MAP, Collections.singletonList(map.getModelData()));
    properties.put(ElementProperty.ANNOTATION, Collections.singletonList(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA")));

    final Page<Element> elements = elementDao.getAll(properties, PageRequest.of(0, 10));

    assertNotNull(elements);
    assertEquals(0, elements.getNumberOfElements());
  }

  @Test
  public void testSearchByFilterName() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Protein p2 = createProtein();
    p2.setName("blA");
    map.addElement(p2);

    modelDao.add(map);

    Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.MAP, Collections.singletonList(map.getModelData()));
    properties.put(ElementProperty.NAME, Collections.singletonList("BLA"));

    final Page<Element> elements = elementDao.getAll(properties, PageRequest.of(0, 10));

    assertNotNull(elements);
    assertEquals(1, elements.getNumberOfElements());
  }

  @Test
  public void testSearchByFilterSubmapConnection() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Protein protein = createProtein();
    protein.setName("blA");
    protein.setSubmodel(new ElementSubmodelConnection(map, SubmodelType.UNKNOWN));
    map.addElement(protein);
    map.addElement(createProtein());
    map.addElement(createProtein());
    map.addElement(createProtein());

    modelDao.add(map);

    Map<ElementProperty, Object> properties = new HashMap<>();

    properties.put(ElementProperty.MAP, Collections.singletonList(map.getModelData()));
    properties.put(ElementProperty.SUBMAP_CONNECTION, Collections.singletonList(Boolean.TRUE));

    final Page<Element> elements = elementDao.getAll(properties, PageRequest.of(0, 10));

    assertEquals(Collections.singletonList(protein), elements.getContent());
  }

  @Test
  public void testSearchByFilterInvalidName() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Protein p2 = createProtein();
    p2.setName("blA");
    map.addElement(p2);

    modelDao.add(map);

    Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.MAP, Collections.singletonList(map.getModelData()));
    properties.put(ElementProperty.NAME, Collections.singletonList("BL"));

    final Page<Element> elements = elementDao.getAll(properties, PageRequest.of(0, 10));

    assertNotNull(elements);
    assertEquals(0, elements.getNumberOfElements());
  }

  @Test
  public void testGetSuggestedQueryList() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Protein p1 = createProtein();
    p1.setName("Name1");
    map.addElement(p1);
    final Protein p2 = createProtein();
    p2.setName("name1");
    p2.addSynonym("syn1");
    p2.addSynonym("syn2");
    p2.addFormerSymbol("symb1");
    p2.addFormerSymbol("symb2");
    p2.setFullName("fname");
    map.addElement(p2);

    modelDao.add(map);

    List<String> elements = elementDao.getSuggestedQueries(Collections.singletonList(map.getModelData()));

    assertEquals(Arrays.asList("fname", "name1", "symb1", "symb2", "syn1", "syn2"), elements);
  }

  @Test
  public void testSearchByFilterProject() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Protein p2 = createProtein();
    map.addElement(p2);
    final Project project = new Project("x");
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    project.addModel(map);

    projectDao.add(project);

    final Model map2 = new ModelFullIndexed(null);
    map2.addElement(createProtein());
    final Project project2 = new Project("y");
    project2.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    project2.addModel(map2);

    projectDao.add(project2);

    Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.PROJECT, Collections.singletonList(map.getProject()));

    final Page<Element> elements = elementDao.getAll(properties, PageRequest.of(0, 10));

    assertNotNull(elements);
    assertEquals(1, elements.getNumberOfElements());
    assertEquals(p2, elements.getContent().get(0));
  }

  @Test
  public void testSearchByFilterClass() throws Exception {
    final Model map = new ModelFullIndexed(null);
    map.addElement(createProtein());
    map.addElement(createIon("id"));

    modelDao.add(map);

    Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.CLASS, Collections.singletonList(Ion.class));

    final Page<Element> elements = elementDao.getAll(properties, PageRequest.of(0, 10));

    assertNotNull(elements);
    assertEquals(1, elements.getNumberOfElements());
    assertTrue(elements.getContent().get(0) instanceof Ion);
  }

  @Test
  public void testGetById() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Ion ion = createIon("sa2");
    map.addElement(ion);

    modelDao.add(map);

    Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.ID, Collections.singletonList(ion.getId()));
    properties.put(ElementProperty.MAP, Collections.singletonList(map.getModelData()));

    final Page<Element> elements = elementDao.getAll(properties, PageRequest.of(0, 10));

    assertEquals(ion, elements.getContent().get(0));

  }

  @Test
  public void testGetByIdWithProjectId() throws Exception {
    final Project project = createProject();
    final Model model = project.getTopModel();

    final Ion ion = createIon("sa111");
    model.addElement(ion);

    modelDao.add(model);

    Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.ID, Collections.singletonList(ion.getId()));
    properties.put(ElementProperty.MAP, Collections.singletonList(model.getModelData()));
    properties.put(ElementProperty.PROJECT, Collections.singletonList(project));

    final Page<Element> element = elementDao.getAll(properties, Pageable.unpaged());

    assertEquals(ion, element.getContent().get(0));

  }

  @Test
  public void testGetByFilterTextId() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Ion ion = createIon("sa2");
    map.addElement(ion);

    modelDao.add(map);

    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.PERFECT_TEXT, Collections.singletonList(ion.getId() + ""));
    properties.put(ElementProperty.MAP, Collections.singletonList(map.getModelData()));

    final Page<Element> elements = elementDao.getAll(properties, PageRequest.of(0, 10));

    assertEquals(1, elements.getNumberOfElements());
    assertEquals(ion, elements.getContent().get(0));

  }

  @Test
  public void testGetByFilterId() throws Exception {
    final Project project = createProject();
    final Model map = project.getTopModel();
    final Protein protein = map.getElementByElementId("sa2");

    modelDao.update(map.getModelData());

    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.PROJECT_ID, Collections.singletonList(project.getProjectId()));
    properties.put(ElementProperty.ID, Collections.singletonList(protein.getId()));

    final Page<Element> elements = elementDao.getAll(properties, PageRequest.of(0, 10));

    assertEquals(protein, elements.getContent().get(0));

  }

  @Test
  public void testGetByIncludedCompartmentId() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Ion ion = createIon("sa2");
    map.addElement(ion);
    map.addElement(createProtein());
    final Compartment compartment = createCompartment();
    compartment.addElement(ion);
    map.addElement(compartment);

    modelDao.add(map);

    Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.MAP, Collections.singletonList(map.getModelData()));
    properties.put(ElementProperty.INCLUDED_IN_COMPARTMENT, Collections.singletonList(compartment.getId()));

    final Page<Element> elements = elementDao.getAll(properties, Pageable.unpaged());
    assertEquals(1, elements.getContent().size());
    assertEquals(ion, elements.getContent().get(0));

  }

  @Test
  public void testGetByExcludedCompartmentId() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Ion ion = createIon("sa2");
    map.addElement(ion);
    final Protein protein = createProtein();
    map.addElement(protein);
    final Compartment compartment = createCompartment();
    compartment.addElement(ion);
    map.addElement(compartment);

    final Ion ion2 = createIon("sa3");
    map.addElement(ion2);
    final Compartment compartment2 = createCompartment();
    compartment2.addElement(ion2);
    map.addElement(compartment2);

    modelDao.add(map);

    Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.MAP, Collections.singletonList(map.getModelData()));
    properties.put(ElementProperty.EXCLUDED_FROM_COMPARTMENT, Collections.singletonList(compartment.getId()));

    final Page<Element> elements = elementDao.getAll(properties, Pageable.unpaged());
    assertEquals(4, elements.getContent().size());
    assertTrue(elements.getContent().contains(protein));
    assertFalse(elements.getContent().contains(ion));
  }

  @Test
  public void testGetByIdEmptyMapList() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Ion ion = createIon("sa2");
    map.addElement(ion);

    modelDao.add(map);

    Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.ID, Collections.singletonList(ion.getId()));

    final Page<Element> elements = elementDao.getAll(properties, PageRequest.of(0, 10));

    assertEquals(ion, elements.getContent().get(0));

  }

  @Test
  public void testGetAnnotationStatistics() throws Exception {
    final Model map = new ModelFullIndexed(null);
    final Ion ion = createIon("sa1");
    ion.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    ion.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCB"));
    ion.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "BLA"));
    ion.addMiriamData(new MiriamData(MiriamType.HGNC, "123"));
    map.addElement(ion);

    final Ion ion2 = createIon("sa2");
    ion2.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    map.addElement(ion2);

    final Project project = new Project(TEST_PROJECT_ID);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    project.addModel(map);
    projectDao.add(project);

    Map<MiriamType, Integer> result = elementDao.getAnnotationStatistics(TEST_PROJECT_ID);

    assertEquals((Integer) 4, result.get(MiriamType.HGNC_SYMBOL));
    assertEquals((Integer) 1, result.get(MiriamType.HGNC));

  }

  @Test
  public void testFilterByCompartment() throws Exception {
    final GenericProtein protein = createProtein();
    final Compartment parent = createCompartment();
    protein.setCompartment(parent);

    elementDao.add(protein);
    elementDao.add(createProtein());

    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.COMPARTMENT_ID, Collections.singletonList(parent.getId()));

    assertEquals(1, elementDao.getAll(properties, PageRequest.of(0, 10)).getNumberOfElements());
  }

  @Test
  public void testFilterByComplex() throws Exception {
    final GenericProtein protein = createProtein();
    final Complex parent = createComplex();
    protein.setComplex(parent);

    elementDao.add(protein);
    elementDao.add(createProtein());

    elementDao.add(createCompartment());

    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.COMPLEX_ID, Collections.singletonList(parent.getId()));

    assertEquals(1, elementDao.getAll(properties, PageRequest.of(0, 10)).getNumberOfElements());
  }

  @Test
  public void testGetPredicate() throws Exception {

    final CriteriaBuilder builder = dbUtils.getSessionFactory().getCurrentSession().getCriteriaBuilder();

    final CriteriaQuery<Element> criteria = builder.createQuery(Element.class);
    final Root<Element> root = criteria.from(Element.class);

    for (final ElementProperty property : ElementProperty.values()) {
      final List<Object> list = new ArrayList<>();

      if (property == ElementProperty.PERFECT_TEXT || property == ElementProperty.TEXT) {
        list.add("");
      } else if (property == ElementProperty.SUBMAP_CONNECTION) {
        list.add(Boolean.TRUE);
      }
      final Map<ElementProperty, Object> properties = new HashMap<>();
      properties.put(property, list);
      assertNotNull(elementDao.createPredicate(properties, root, criteria));
    }

  }
}
