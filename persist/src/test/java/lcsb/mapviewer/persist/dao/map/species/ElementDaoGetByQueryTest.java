package lcsb.mapviewer.persist.dao.map.species;

import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.ElementComparator;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.SpringPersistTestConfig;
import lcsb.mapviewer.persist.utils.TransactionalElementDao;
import lcsb.mapviewer.persist.utils.TransactionalModelDao;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.domain.PageRequest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(Parameterized.class)
public class ElementDaoGetByQueryTest extends PersistTestFunctions {

  private static ApplicationContext applicationContext;

  private final TransactionalElementDao elementDao;

  private static List<ModelData> testMaps;

  private final String query;

  private final boolean perfect;

  private final List<Element> expectedResult;

  private final List<Class<? extends BioEntity>> types;

  public ElementDaoGetByQueryTest(final String testName, final Model map, final String query, final boolean perfect, final Object result) {
    this.query = query;
    this.perfect = perfect;
    this.expectedResult = new ArrayList<>();
    if (result == null) {
      this.types = new ElementUtils().getClassesByStringTypes(new ElementUtils().getBioEntityStringTypes());
    } else if (result instanceof Element) {
      Element bioEntity = (Element) result;
      this.types = Collections.singletonList(bioEntity.getClass());
      this.expectedResult.add(bioEntity);
    } else {
      this.types = new ArrayList<>();
      final List<?> bioEntities = (List<?>) result;
      for (final Object object : bioEntities) {
        final Element bioEntity = (Element) object;
        expectedResult.add(bioEntity);
        types.add(bioEntity.getClass());
      }
    }

    elementDao = applicationContext.getBean(TransactionalElementDao.class);
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    final Model map = new ModelFullIndexed(null);
    final Protein protein = createProtein();
    protein.setName("SNN-1");
    protein.setFullName("FULL BTR-5");
    protein.addSynonym("fancy synonym");
    protein.addSynonym("Fantastic ");
    protein.addFormerSymbol("former symbol2");
    map.addElement(protein);

    final Ion ion = createIon("ion1");
    ion.setName("X");
    map.addElement(ion);

    final Protein falseIon = createProtein("ion2");
    falseIon.setName("Ion");
    map.addElement(falseIon);

    final Protein falseIon2 = createProtein("ion");
    falseIon2.setFullName("Ion");
    map.addElement(falseIon2);

    applicationContext = new AnnotationConfigApplicationContext(SpringPersistTestConfig.class);

    Collection<Object[]> data = new ArrayList<>();
    data.add(new Object[]{"existing name perfect match", map, protein.getName(), true, protein});
    data.add(new Object[]{"existing name not-perfect match", map, "snn", false, protein});
    data.add(new Object[]{"not existing name perfect match", map, "snn", true, null});
    data.add(new Object[]{"not existing name not-perfect match", map, "bla", false, null});

    data.add(new Object[]{"existing full name perfect match", map, protein.getFullName(), true, protein});
    data.add(new Object[]{"existing full name not-perfect match", map, "BTR5", false, protein});
    data.add(new Object[]{"not existing full name perfect match", map, "BTR5", true, null});

    data.add(new Object[]{"existing element id perfect match", map, protein.getElementId(), true, protein});
    data.add(new Object[]{"existing element id not-perfect match", map, protein.getElementId().substring(1), false,
        protein});

    data.add(new Object[]{"existing synonym perfect match", map, "fancy synonym", true, protein});
    data.add(new Object[]{"existing synonym not-perfect match", map, "synonym", false, protein});
    data.add(new Object[]{"not existing synonym perfect match", map, "synonym", true, null});

    data.add(new Object[]{"existing former symbol perfect match", map, "former symbol2", true, protein});
    data.add(new Object[]{"existing former symbol not-perfect match", map, "symbol2", false, protein});
    data.add(new Object[]{"not existing former symbol perfect match", map, "symbol2", true, null});

    data.add(new Object[]{"element without synonym and former symbol ", map, ion.getElementId(), false, ion});

    data.add(new Object[]{"type selection", map, "ion", false, ion});

    testMaps = Collections.singletonList(map.getModelData());
    return data;
  }

  @BeforeClass
  public static void initialize() {
    final TransactionalModelDao modelDao = applicationContext.getBean(TransactionalModelDao.class);
    for (final ModelData map : testMaps) {
      modelDao.add(map);
    }
  }

  @AfterClass
  public static void shutdown() {
    TransactionalModelDao modelDao = applicationContext.getBean(TransactionalModelDao.class);
    for (final ModelData map : testMaps) {
      modelDao.delete(map);
    }
  }

  @Test
  public void test() throws Exception {
    Map<ElementProperty, Object> filter = new HashMap<>();
    filter.put(ElementProperty.MAP, testMaps);
    if (perfect) {
      filter.put(ElementProperty.PERFECT_TEXT, Collections.singletonList(query));
    } else {
      filter.put(ElementProperty.TEXT, Collections.singletonList(query));
    }
    filter.put(ElementProperty.CLASS, types);
    final List<Element> elements = elementDao.getByFilter(PageRequest.of(0, 10), filter).getContent();

    assertNotNull(elements);
    for (int i = 0; i < elements.size(); i++) {
      assertEquals(0, new ElementComparator().compare(elements.get(i), expectedResult.get(i)));
    }
  }

}
