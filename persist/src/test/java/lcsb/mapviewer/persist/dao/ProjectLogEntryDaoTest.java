package lcsb.mapviewer.persist.dao;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectLogEntry;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.user.UserDao;

public class ProjectLogEntryDaoTest extends PersistTestFunctions {

  @Autowired
  private ProjectDao projectDao;

  @Autowired
  private UserDao userDao;

  @Autowired
  private ProjectLogEntryDao projectLogEntryDao;

  @Test
  public void testGetByProjectId() {
    Project project = new Project(TEST_PROJECT);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    ProjectLogEntry entry = new ProjectLogEntry();
    entry.setContent("hello world");
    entry.setSeverity("DEBUG");
    entry.setType(ProjectLogEntryType.OTHER);

    project.addLogEntry(entry);
    projectDao.add(project);

    Map<ProjectLogEntryProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(ProjectLogEntryProperty.PROJECT_ID, TEST_PROJECT);

    Page<ProjectLogEntry> page = projectLogEntryDao.getAll(filterOptions, PageRequest.of(0, 20));
    assertEquals(1, page.getTotalElements());

    filterOptions.put(ProjectLogEntryProperty.PROJECT_ID, TEST_PROJECT_2);

    page = projectLogEntryDao.getAll(filterOptions, PageRequest.of(0, 20));
    assertEquals(0, page.getTotalElements());

  }

  @Test
  public void testGetByLevel() {
    Project project = new Project(TEST_PROJECT);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));

    ProjectLogEntry entry = new ProjectLogEntry();
    entry.setContent("hello world");
    entry.setSeverity("DEBUG");
    entry.setType(ProjectLogEntryType.OTHER);
    project.addLogEntry(entry);

    entry = new ProjectLogEntry();
    entry.setContent("hello world 2");
    entry.setSeverity("FATAL");
    entry.setType(ProjectLogEntryType.OTHER);
    project.addLogEntry(entry);

    projectDao.add(project);

    Map<ProjectLogEntryProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(ProjectLogEntryProperty.LEVEL, "DEBUG");

    Page<ProjectLogEntry> page = projectLogEntryDao.getAll(filterOptions, PageRequest.of(0, 20));
    assertEquals(1, page.getTotalElements());

    filterOptions = new HashMap<>();

    page = projectLogEntryDao.getAll(filterOptions, PageRequest.of(0, 20));
    assertEquals(2, page.getTotalElements());

  }

  @Test
  public void testGetByText() {
    Project project = new Project(TEST_PROJECT);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));

    ProjectLogEntry entry = new ProjectLogEntry();
    entry.setContent("hello world");
    entry.setSeverity("DEBUG");
    entry.setType(ProjectLogEntryType.OTHER);
    project.addLogEntry(entry);

    entry = new ProjectLogEntry();
    entry.setContent("hello world 2");
    entry.setSeverity("FATAL");
    entry.setType(ProjectLogEntryType.OTHER);
    project.addLogEntry(entry);

    projectDao.add(project);

    Map<ProjectLogEntryProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(ProjectLogEntryProperty.CONTENT, "WOR");

    Page<ProjectLogEntry> page = projectLogEntryDao.getAll(filterOptions, PageRequest.of(0, 20));
    assertEquals(2, page.getTotalElements());

    filterOptions.put(ProjectLogEntryProperty.CONTENT, "2");

    page = projectLogEntryDao.getAll(filterOptions, PageRequest.of(0, 20));
    assertEquals(1, page.getTotalElements());

  }

  @Test
  public void testOrderByText() {
    Project project = new Project(TEST_PROJECT);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));

    ProjectLogEntry entry1 = new ProjectLogEntry();
    entry1.setContent("ecd");
    entry1.setSeverity("DEBUG");
    entry1.setType(ProjectLogEntryType.OTHER);
    project.addLogEntry(entry1);

    ProjectLogEntry entry2 = new ProjectLogEntry();
    entry2.setContent("abc");
    entry2.setSeverity("FATAL");
    entry2.setType(ProjectLogEntryType.OTHER);
    project.addLogEntry(entry2);

    ProjectLogEntry entry3 = new ProjectLogEntry();
    entry3.setContent("def");
    entry3.setSeverity("FATAL");
    entry3.setType(ProjectLogEntryType.OTHER);
    project.addLogEntry(entry3);

    projectDao.add(project);

    Map<ProjectLogEntryProperty, Object> filterOptions = new HashMap<>();
    Page<ProjectLogEntry> page = projectLogEntryDao.getAll(filterOptions,
        PageRequest.of(0, 20, Direction.ASC, ProjectLogEntryProperty.CONTENT.name()));

    assertEquals(entry2, page.getContent().get(0));
    assertEquals(entry3, page.getContent().get(1));
    assertEquals(entry1, page.getContent().get(2));

    page = projectLogEntryDao.getAll(filterOptions,
        PageRequest.of(0, 20, Direction.DESC, ProjectLogEntryProperty.CONTENT.name()));

    assertEquals(entry1, page.getContent().get(0));
    assertEquals(entry3, page.getContent().get(1));
    assertEquals(entry2, page.getContent().get(2));

  }

  @Test
  public void testGenericSorting() {
    Project project = new Project(TEST_PROJECT);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));

    ProjectLogEntry entry1 = new ProjectLogEntry();
    entry1.setContent("ecd");
    entry1.setSeverity("DEBUG");
    entry1.setType(ProjectLogEntryType.OTHER);
    project.addLogEntry(entry1);

    ProjectLogEntry entry2 = new ProjectLogEntry();
    entry2.setContent("abc");
    entry2.setSeverity("FATAL");
    entry2.setType(ProjectLogEntryType.OTHER);
    project.addLogEntry(entry2);

    projectDao.add(project);

    for (ProjectLogEntryProperty property : ProjectLogEntryProperty.values()) {
      if (!ProjectLogEntryProperty.PROJECT_ID.equals(property)) {
        Map<ProjectLogEntryProperty, Object> filterOptions = new HashMap<>();
        projectLogEntryDao.getAll(filterOptions, PageRequest.of(0, 20, Direction.ASC, property.name()));
        assertEquals(0, super.getWarnings().size());
      }
    }
  }

}
