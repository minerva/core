package lcsb.mapviewer.persist.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.ConfigurationOption;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class ConfigurationDaoTest extends PersistTestFunctions {
  @Autowired
  protected ConfigurationDao configurationDao;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetNull() {
    assertNull(configurationDao.getByType(null));
  }

  @Test
  public void testGetValueByType() {
    // check data for null input (failsafe test)
    assertNull(configurationDao.getValueByType(null));

    // check data for something that is in database
    String val = configurationDao.getValueByType(ConfigurationElementType.LEFT_LOGO_TEXT);
    assertNotNull(val);

    // remove data from database
    ConfigurationOption obj = configurationDao.getByType(ConfigurationElementType.LEFT_LOGO_TEXT);
    if (obj != null) {
      configurationDao.delete(obj);
    }

    // and now check data for something that is not in database
    assertNotNull(configurationDao.getValueByType(ConfigurationElementType.LEFT_LOGO_TEXT));

  }

}
