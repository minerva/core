package lcsb.mapviewer.persist.dao.user;

import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.AnnotatorParamDefinition;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.model.user.UserAnnotationSchema;
import lcsb.mapviewer.model.user.UserClassAnnotators;
import lcsb.mapviewer.model.user.UserGuiPreference;
import lcsb.mapviewer.model.user.annotator.AnnotatorConfigParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.persist.PersistTestFunctions;
import org.hibernate.PropertyValueException;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.Color;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class UserDaoTest extends PersistTestFunctions {

  private final String testEmail = "a@a.pl";
  private final String testLogin2 = "test_login_tmp";

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAddUpdateDelete() {
    long counter = userDao.getCount();

    User user = createTestUser();
    user.addPrivilege(new Privilege(PrivilegeType.IS_CURATOR));
    userDao.add(user);

    long counter2 = userDao.getCount();
    assertEquals(counter + 1, counter2);

    user.setLogin(testLogin2);
    userDao.update(user);

    User user2 = userDao.getUserByLogin(testLogin2);
    assertNotNull(user2);

    userDao.delete(user);

    user2 = userDao.getUserByLogin(testLogin2);
    assertNull(user2);
    counter2 = userDao.getCount();
    assertEquals(counter, counter2);
  }

  @Test(expected = PropertyValueException.class)
  public void testTryUserWithNullPassword() {
    User user = new User();
    user.setLogin(TEST_USER_LOGIN);
    userDao.add(user);
  }

  @Test(expected = PropertyValueException.class)
  public void testTryUserWithNullLogin() {
    User user = new User();
    user.setCryptedPassword("ZX");
    userDao.add(user);
  }

  @Test(expected = ConstraintViolationException.class)
  public void testTryUserWithExistingLogin() {
    User user = createTestUser();
    userDao.add(user);

    User user2 = createTestUser();
    userDao.add(user2);
  }

  @Test
  public void testAddDeleteAdd() {
    long counter = userDao.getCount();

    User user = createTestUser();
    userDao.add(user);

    long counter2 = userDao.getCount();
    assertEquals(counter + 1, counter2);

    userDao.delete(user);

    User user2 = userDao.getUserByLogin(TEST_USER_LOGIN);
    assertNull(user2);

    user2 = createTestUser();
    userDao.add(user2);

    assertNotNull(userDao.getUserByLogin(TEST_USER_LOGIN));

    userDao.delete(user2);
  }

  @Test
  public void testGetUserByLogin() throws Exception {
    User user = createTestUser();
    userDao.add(user);
    User user2 = userDao.getUserByLogin(TEST_USER_LOGIN);
    assertNotNull(user2);
    assertEquals(user2.getId(), user.getId());
    assertEquals(user2.getLogin(), user.getLogin());
    assertEquals(user2.getCryptedPassword(), user.getCryptedPassword());
    user2 = userDao.getUserByLogin(testLogin2);
    assertNull(user2);
    userDao.delete(user);
  }

  @Test
  public void testGetUserByEmail() throws Exception {
    User user = createTestUser();
    user.addPrivilege(new Privilege(PrivilegeType.IS_CURATOR));
    userDao.add(user);
    User user2 = userDao.getUserByEmail(testEmail);
    assertNotNull(user2);
    assertEquals(user2.getId(), user.getId());
    assertEquals(user2.getLogin(), user.getLogin());
    assertEquals(user2.getCryptedPassword(), user.getCryptedPassword());
    user2 = userDao.getUserByEmail(testEmail + "sadas");
    assertNull(user2);
    userDao.delete(user);
  }

  @Test
  public void testGetUserByOrcid() throws Exception {
    User user = createTestUser();
    user.addPrivilege(new Privilege(PrivilegeType.IS_CURATOR));
    user.setOrcidId("123");
    userDao.add(user);
    User user2 = userDao.getUserByOrcidId("123");
    assertNotNull(user2);
    user2 = userDao.getUserByOrcidId("1234");
    assertNull(user2);
    userDao.delete(user);
  }

  private User createTestUser() {
    User user = new User();
    user.setName("John");
    user.setSurname("Doe");
    user.setCryptedPassword(TEST_USER_PASSWORD);
    user.setLogin(TEST_USER_LOGIN);
    user.setEmail(testEmail);
    return user;
  }

  @Test
  public void testGetAll() throws Exception {
    assertTrue(userDao.getAll().size() > 0);
  }

  @Test
  public void testUserWithAnnotatorSchema() throws Exception {
    User user = createTestUser();
    UserAnnotationSchema uas = new UserAnnotationSchema();
    uas.setValidateMiriamTypes(true);
    UserClassAnnotators ca = new UserClassAnnotators();
    ca.setClassName(Species.class);
    AnnotatorData annotatorData = new AnnotatorData(String.class);
    ca.addAnnotator(annotatorData);
    uas.addClassAnnotator(ca);
    uas.addClassAnnotator(new UserClassAnnotators(String.class, new ArrayList<>()));
    user.setAnnotationSchema(uas);
    userDao.add(user);
    userDao.evict(user);
    User user2 = userDao.getById(user.getId());
    assertNotNull(user2);
    UserAnnotationSchema uas2 = user2.getAnnotationSchema();
    assertNotNull(uas2);
    assertEquals(2, uas2.getClassAnnotators().size());
    assertEquals(Species.class.getCanonicalName(), uas.getClassAnnotators().get(0).getClassName());
    assertEquals(annotatorData, uas.getClassAnnotators().get(0).getAnnotators().get(0));

    userDao.delete(user2);
  }

  @Test
  public void testUserWithAnnotatorSchemaGuiPreferences() throws Exception {
    User user = createTestUser();
    UserAnnotationSchema uas = new UserAnnotationSchema();
    user.setAnnotationSchema(uas);
    UserGuiPreference option = new UserGuiPreference();
    option.setKey("key");
    option.setValue("val");
    uas.addGuiPreference(option);
    userDao.add(user);
    userDao.evict(user);
    User user2 = userDao.getById(user.getId());
    assertNotNull(user2);
    UserAnnotationSchema uas2 = user2.getAnnotationSchema();
    assertNotNull(uas2);
    assertEquals(1, uas2.getGuiPreferences().size());

    userDao.delete(user2);
  }

  @Test
  public void testUserWithAnnotatorParams() throws Exception {
    User user = createTestUser();
    UserAnnotationSchema uas = new UserAnnotationSchema();
    user.setAnnotationSchema(uas);
    UserClassAnnotators classAnnotators = new UserClassAnnotators(Integer.class);
    AnnotatorData annotatorData = new AnnotatorData(String.class);
    classAnnotators.addAnnotator(annotatorData);
    uas.addClassAnnotator(classAnnotators);
    AnnotatorConfigParameter param = new AnnotatorConfigParameter(AnnotatorParamDefinition.KEGG_ORGANISM_IDENTIFIER,
        "val");
    annotatorData.addAnnotatorParameter(param);
    userDao.add(user);
    userDao.evict(user);
    User user2 = userDao.getById(user.getId());
    assertNotNull(user2);
    UserAnnotationSchema uas2 = user2.getAnnotationSchema();
    assertNotNull(uas2);
    assertEquals(1, uas2.getClassAnnotators().get(0).getAnnotators().get(0).getAnnotatorParams().size());

    userDao.delete(user2);
  }

  @Test
  public void testAddWithColors() {
    User user = createTestUser();
    user.setMaxColor(Color.BLACK);
    user.setMinColor(Color.BLUE);
    user.setNeutralColor(Color.YELLOW);
    user.setSimpleColor(Color.GREEN);
    userDao.add(user);

    assertNotNull(userDao.getUserByLogin(TEST_USER_LOGIN));
  }

}
