package lcsb.mapviewer.persist.dao.map.kinetics;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.ProjectDao;

public class SbmlParameterDaoTest extends PersistTestFunctions {

  @Autowired
  private ProjectDao projectDao;

  @Autowired
  private SbmlParameterDao sbmlParameterDao;

  @Test
  public void testFilterByProject() {
    Project project = createProject();

    Map<SbmlParameterProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(SbmlParameterProperty.PROJECT_ID, project.getProjectId());
    Page<SbmlParameter> page = sbmlParameterDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(SbmlParameterProperty.PROJECT_ID, TEST_PROJECT_2);
    page = sbmlParameterDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

  @Test
  public void testFilterById() {
    Project project = createProject();

    Map<SbmlParameterProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(SbmlParameterProperty.ID, project.getTopModelData().getParameters().iterator().next().getId());
    Page<SbmlParameter> page = sbmlParameterDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(SbmlParameterProperty.ID, -1);
    page = sbmlParameterDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

  @Test
  public void testFilterByMapId() {
    Project project = createProject();

    Map<SbmlParameterProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(SbmlParameterProperty.MAP_ID, project.getTopModelData().getId());
    Page<SbmlParameter> page = sbmlParameterDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(SbmlParameterProperty.MAP_ID, -1);
    page = sbmlParameterDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

  public Project createProject() {
    Project project = new Project(TEST_PROJECT_ID);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    ModelData model = new ModelData();
    project.addModel(model);
    SbmlParameter sbmlParameter = new SbmlParameter("x");
    model.addParameter(sbmlParameter);

    projectDao.add(project);
    return project;
  }

  @Test
  public void testGetReactionParameters() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    map.addElement(p1);
    map.addElement(p2);

    SbmlParameter parameter = new SbmlParameter("id");
    SbmlKinetics kinetics = new SbmlKinetics();
    kinetics.addParameter(parameter);

    Reaction r1 = createReaction(p1, p2);
    r1.setKinetics(kinetics);
    map.addReaction(r1);

    Project project = new Project(TEST_PROJECT_ID);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    project.addModel(map);

    projectDao.add(project);

    Map<SbmlParameterProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(SbmlParameterProperty.MAP_ID, map.getId());
    Page<SbmlParameter> page = sbmlParameterDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(SbmlParameterProperty.PROJECT_ID, project.getProjectId());
    page = sbmlParameterDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

  }
}
