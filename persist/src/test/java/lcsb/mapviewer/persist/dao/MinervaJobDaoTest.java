package lcsb.mapviewer.persist.dao;

import lcsb.mapviewer.model.job.MinervaJob;
import lcsb.mapviewer.model.job.MinervaJobParameters;
import lcsb.mapviewer.model.job.MinervaJobPriority;
import lcsb.mapviewer.model.job.MinervaJobStatus;
import lcsb.mapviewer.model.job.MinervaJobType;
import lcsb.mapviewer.persist.PersistTestFunctions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class MinervaJobDaoTest extends PersistTestFunctions {

  private static class MockMinervaJob extends MinervaJobParameters {

    private Map<String, Object> jobParameters;

    public MockMinervaJob(final Map<String, Object> parameters) {
      this.jobParameters = parameters;
    }

    public MockMinervaJob() {
      this(new HashMap<>());
    }

    @Override
    protected void assignData(final Map<String, Object> jobParameters) {
      this.jobParameters = jobParameters;
    }

    @Override
    protected Map<String, Object> getJobParameters() {
      return jobParameters;
    }

  }

  @Autowired
  private MinervaJobDao minervaJobDao;

  @Test
  public void testGetPending() {
    MinervaJob job = new MinervaJob(MinervaJobType.REFRESH_CACHE, MinervaJobPriority.LOWEST, new MockMinervaJob());
    job.setJobStatus(MinervaJobStatus.PENDING);
    minervaJobDao.add(job);

    job = new MinervaJob(MinervaJobType.REFRESH_CACHE, MinervaJobPriority.LOWEST, new MockMinervaJob());
    job.setJobStatus(MinervaJobStatus.FAILED);
    minervaJobDao.add(job);

    assertEquals(1, minervaJobDao.getPending().size());
    assertEquals(1, minervaJobDao.getCount(MinervaJobStatus.PENDING));
    assertEquals(0, minervaJobDao.getCount(MinervaJobStatus.INTERRUPTED));
  }

  @Test
  public void testGetCount() {
    MinervaJob job = new MinervaJob(MinervaJobType.REFRESH_CACHE, MinervaJobPriority.LOWEST, new MockMinervaJob());
    job.setJobStatus(MinervaJobStatus.PENDING);
    minervaJobDao.add(job);

    job = new MinervaJob(MinervaJobType.REFRESH_CACHE, MinervaJobPriority.LOWEST, new MockMinervaJob());
    job.setJobStatus(MinervaJobStatus.FAILED);
    minervaJobDao.add(job);

    assertEquals(1, minervaJobDao.getCount(MinervaJobStatus.PENDING));
    assertEquals(0, minervaJobDao.getCount(MinervaJobStatus.INTERRUPTED));
  }

  @Test
  public void testRemoveBefore() {
    MinervaJob job = new MinervaJob(MinervaJobType.REFRESH_CACHE, MinervaJobPriority.LOWEST, new MockMinervaJob());
    job.setJobStatus(MinervaJobStatus.DONE);
    minervaJobDao.add(job);

    assertEquals(1, minervaJobDao.getCount(MinervaJobStatus.DONE));

    Calendar yesterday = Calendar.getInstance();
    yesterday.add(Calendar.DATE, -1);

    minervaJobDao.deleteBefore(MinervaJobStatus.DONE, yesterday);
    assertEquals(1, minervaJobDao.getCount(MinervaJobStatus.DONE));

    Calendar tomorrow = Calendar.getInstance();
    tomorrow.add(Calendar.DATE, 1);

    minervaJobDao.deleteBefore(MinervaJobStatus.DONE, tomorrow);
    assertEquals(0, minervaJobDao.getCount(MinervaJobStatus.DONE));

  }

  @Test
  public void testGetByParameters() {
    Map<String, Object> parameters = new HashMap<>();
    parameters.put("x", "y");
    Map<String, Object> parameters2 = new HashMap<>();
    parameters2.put("bla", "blah");
    parameters.put("parameters", parameters2);

    MinervaJob job = new MinervaJob(MinervaJobType.REFRESH_CACHE, MinervaJobPriority.LOWEST, new MockMinervaJob(parameters));
    job.setJobStatus(MinervaJobStatus.PENDING);
    minervaJobDao.add(job);

    MinervaJob job2 = new MinervaJob(MinervaJobType.REFRESH_CACHE, MinervaJobPriority.LOWEST, new MockMinervaJob());
    job2.setJobStatus(MinervaJobStatus.PENDING);
    minervaJobDao.add(job2);

    MinervaJob job3 = new MinervaJob(MinervaJobType.REFRESH_CACHE, MinervaJobPriority.LOWEST, new MockMinervaJob(parameters));
    job3.setJobStatus(MinervaJobStatus.DONE);
    minervaJobDao.add(job3);

    List<MinervaJob> jobs = minervaJobDao.getUnfinishedByJobParameters(MinervaJobType.REFRESH_CACHE, parameters);
    assertEquals(1, jobs.size());
  }

}
