package lcsb.mapviewer.persist.dao.map.layout;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.ReferenceGenome;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class ReferenceGenomeDaoTest extends PersistTestFunctions {

  @Autowired
  private ReferenceGenomeDao referenceGenomeDao;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void test() {
    ReferenceGenome genome = new ReferenceGenome();
    genome.setOrganism(new MiriamData(MiriamType.TAXONOMY, "9606"));
    genome.setType(ReferenceGenomeType.UCSC);
    genome.setVersion("x");
    long count = referenceGenomeDao.getCount();
    referenceGenomeDao.add(genome);
    long count2 = referenceGenomeDao.getCount();
    referenceGenomeDao.delete(genome);
    long count3 = referenceGenomeDao.getCount();

    assertEquals(count + 1, count2);
    assertEquals(count, count3);
  }

  @Test
  public void testGetByType() {
    ReferenceGenome genome = new ReferenceGenome();
    genome.setOrganism(new MiriamData(MiriamType.TAXONOMY, "111111"));
    genome.setType(ReferenceGenomeType.UCSC);
    genome.setVersion("unknown");
    long count = referenceGenomeDao.getByType(ReferenceGenomeType.UCSC).size();
    referenceGenomeDao.add(genome);
    long count2 = referenceGenomeDao.getByType(ReferenceGenomeType.UCSC).size();
    referenceGenomeDao.delete(genome);
    long count3 = referenceGenomeDao.getByType(ReferenceGenomeType.UCSC).size();

    assertEquals(count + 1, count2);
    assertEquals(count, count3);
  }

}
