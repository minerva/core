package lcsb.mapviewer.persist.dao;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectLogEntry;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.model.map.OverviewModelLink;
import lcsb.mapviewer.model.map.layout.ProjectBackground;
import lcsb.mapviewer.model.map.layout.ProjectBackgroundImageLayer;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayType;
import lcsb.mapviewer.model.overlay.GeneVariant;
import lcsb.mapviewer.model.overlay.GeneVariantDataOverlayEntry;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.ObjectValidator;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.graphics.GlyphDao;
import lcsb.mapviewer.persist.dao.map.CommentDao;
import lcsb.mapviewer.persist.dao.map.DataOverlayDao;
import lcsb.mapviewer.persist.dao.map.ModelDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class ProjectDaoTest extends PersistTestFunctions {

  @Autowired
  protected ModelDao modelDao;
  @Autowired
  private ProjectDao projectDao;
  @Autowired
  private DataOverlayDao dataOverlayDao;

  @Autowired
  private CommentDao commentDao;

  @Autowired
  private ProjectLogEntryDao logEntryDao;

  @Autowired
  private GlyphDao glyphDao;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetProjectByName() throws Exception {
    final Project project = createAndPersistProject(TEST_PROJECT_ID);
    projectDao.evict(project);

    final Project project2 = projectDao.getProjectByProjectId(TEST_PROJECT_ID);
    assertNotNull(project2);
    assertNotEquals(project2, project);
    assertEquals(project.getId(), project2.getId());

    projectDao.delete(project2);
  }

  private Project createAndPersistProject(final String projectId) {
    final Project project = new Project(projectId);
    project.setTopModel(createModel());
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    projectDao.add(project);
    return project;
  }

  @Test
  public void testGetProjectForModelId() throws Exception {
    final Project project = new Project(TEST_PROJECT_ID);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    final Model model = new ModelFullIndexed(null);
    project.addModel(model);
    projectDao.add(project);
    projectDao.evict(project);

    final Project project2 = projectDao.getProjectForModelId(model.getId());
    assertNotNull(project2);
    assertNotEquals(project2, project);
    assertEquals(project.getId(), project2.getId());

    projectDao.delete(project2);

    assertNull(projectDao.getProjectForModelId(model.getId()));
  }

  @Test
  public void testProjectExists() throws Exception {
    assertFalse(projectDao.isProjectExistsByName(TEST_PROJECT_ID));

    final Project project = createAndPersistProject(TEST_PROJECT_ID);
    projectDao.evict(project);

    assertTrue(projectDao.isProjectExistsByName(TEST_PROJECT_ID));

    final Project project2 = projectDao.getProjectByProjectId(TEST_PROJECT_ID);

    projectDao.delete(project2);
  }

  @Test
  public void testCheckEqualityAfterReload() throws Exception {
    final Project project = createAndPersistProject(TEST_PROJECT_ID);
    projectDao.flush();

    projectDao.evict(project);

    final ModelComparator comparator = new ModelComparator();

    final Model model2 = new ModelFullIndexed(modelDao.getById(project.getTopModel().getId()));

    assertEquals(0, comparator.compare(project.getTopModel(), model2));

    projectDao.delete(projectDao.getById(project.getId()));
  }

  @Test
  public void testCheckEqualityAfterReload2() throws Exception {
    final Project project = createAndPersistProject(TEST_PROJECT_ID);
    projectDao.evict(project);

    final Project project2 = projectDao.getProjectByProjectId(TEST_PROJECT_ID);

    final ModelComparator comparator = new ModelComparator();

    final Model fullModel1 = new ModelFullIndexed(project.getModels().iterator().next());
    final Model fullModel2 = new ModelFullIndexed(project2.getModels().iterator().next());
    assertEquals(0, comparator.compare(fullModel1, fullModel2));

    projectDao.delete(project2);
  }

  private Model createModel() {
    final Model model = new ModelFullIndexed(null);

    model.addElement(createProtein(264.8333333333335, 517.75, 86.0, 46.0, "sa2"));
    model.addElement(createProtein(267.6666666666665, 438.75, 80.0, 40.0, "sa1117"));
    model.addElement(createProtein(261.6666666666665, 600.75, 92.0, 52.0, "sa1119"));
    model.addElement(createProtein(203.666666666667, 687.75, 98.0, 58.0, "sa1121"));

    final Species element1 = createProtein(817.714285714286, 287.642857142859, 80.0, 40.0, "sa1422");
    final Species element2 = createProtein(224.964285714286, 241.392857142859, 80.0, 40.0, "sa1419");
    final Complex complex = createComplex(804.714285714286, 182.642857142859, 112.0, 172.0, "csa152");
    complex.addSpecies(element1);
    complex.addSpecies(element2);
    element1.setComplex(complex);
    element2.setComplex(complex);

    model.addElement(element1);
    model.addElement(element2);
    model.addElement(complex);

    model.addElement(createCompartment(380.0, 416.0, 1893.0, 1866.0, "ca1"));
    model.setWidth(2000);
    model.setHeight(2000);

    return model;
  }

  @Test
  public void testAddGetProjectWithOverviewImage() throws Exception {
    final Project project = new Project(TEST_PROJECT_ID);
    final Model model = new ModelFullIndexed(null);
    final OverviewImage oi = new OverviewImage();
    oi.setFilename("test");
    final OverviewModelLink oml = new OverviewModelLink();
    oml.setPolygon("10,10 20,20 20,100");
    oml.setxCoord(1);
    oml.setyCoord(2);
    oml.setZoomLevel(3);
    oml.setLinkedModel(model);
    oi.addLink(oml);
    project.addOverviewImage(oi);
    project.addModel(model);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));

    projectDao.add(project);
    projectDao.evict(project);

    final Project project2 = projectDao.getProjectByProjectId(TEST_PROJECT_ID);
    assertNotNull(project2);

    final OverviewImage oi2 = project2.getOverviewImages().get(0);
    final OverviewModelLink oml2 = (OverviewModelLink) oi2.getLinks().get(0);

    assertEquals(oi.getFilename(), oi2.getFilename());
    assertEquals(oml.getPolygon(), oml2.getPolygon());
    assertEquals(oml.getxCoord(), oml2.getxCoord());
    assertEquals(oml.getyCoord(), oml2.getyCoord());
    assertEquals(oml.getZoomLevel(), oml2.getZoomLevel());
    assertNotNull(oml2.getPolygonCoordinates());

    projectDao.delete(project2);
  }

  @Test
  public void testCreationWarnings() throws Exception {
    final Project project = new Project("test_project_id");
    project.setTopModel(createModel());
    final ProjectLogEntry entry = new ProjectLogEntry();
    entry.setContent("content");
    entry.setSeverity("WARN");
    entry.setType(ProjectLogEntryType.OTHER);
    project.addLogEntry(entry);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    assertEquals(1, project.getLogEntries().size());

    projectDao.add(project);

    projectDao.evict(project);
    final Project project2 = projectDao.getById(project.getId());

    assertEquals(1, project2.getLogEntries().size());

    projectDao.delete(project2);
  }

  @Test
  public void testGetAll() throws Exception {
    final long startTime = System.currentTimeMillis();
    final double max = 10;

    logger.debug("---");
    for (int i = 0; i < max; i++) {
      projectDao.getAll();
    }
    final long estimatedTime = System.currentTimeMillis() - startTime;
    logger.debug(estimatedTime / max);
  }

  @Test
  public void testBackgroundInProject() throws Exception {
    Project project = createAndPersistProject(TEST_PROJECT_ID);
    final ModelData model = project.getTopModelData();

    final ProjectBackground background = new ProjectBackground();
    background.addProjectBackgroundImageLayer(new ProjectBackgroundImageLayer(model, "tmp"));
    background.setName("temporary name");
    background.setCreator(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    project.addProjectBackground(background);

    modelDao.evict(model);
    projectDao.evict(project);
    final Project project2 = projectDao.getById(project.getId());

    assertEquals(1, project2.getProjectBackgrounds().size());
    assertEquals("tmp",
        project2.getProjectBackgrounds().iterator().next().getProjectBackgroundImageLayer().iterator().next().getDirectory());
    assertEquals("temporary name", project2.getProjectBackgrounds().iterator().next().getName());

    project = projectDao.getById(project.getId());
    projectDao.delete(project);
  }

  @Test
  public void testRemoveProjectWithReaction() throws Exception {
    final Project project = createAndPersistProject(TEST_PROJECT_ID);
    final ModelData model = project.getTopModelData();

    final Reaction reaction = createReaction(model.getModel().getSpeciesList().get(0), model.getModel().getSpeciesList().get(1));
    reaction.addSynonym("sym");
    reaction.addMiriamData(new MiriamData(MiriamType.PUBMED, "123"));
    model.addReaction(reaction);
    modelDao.saveOrUpdate(model);

    modelDao.evict(model);
    projectDao.evict(project);

    projectDao.delete(project);
  }

  @Test
  public void testRemoveProjectWithLayer() throws Exception {
    final Project project = createAndPersistProject(TEST_PROJECT_ID);
    final ModelData model = project.getTopModelData();

    final Layer layer = createLayer();
    layer.addLayerLine(new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 100)));
    layer.addLayerOval(createOval());
    layer.addLayerRect(createRect());
    layer.addLayerText(createText());

    model.addLayer(layer);
    modelDao.saveOrUpdate(model);

    modelDao.evict(model);
    projectDao.evict(project);

    projectDao.delete(project);
  }

  @Test
  public void testGetNextId() throws Exception {
    final long id = projectDao.getNextId();
    createAndPersistProject(TEST_PROJECT_ID);

    final long id2 = projectDao.getNextId();
    assertNotEquals(id, id2);
  }

  @Test
  public void validateHibernateObject() {
    final Project project = projectDao.getProjectByProjectId("empty");
    assertEquals(0, new ObjectValidator().getValidationIssues(project).size());
  }

  @Test
  public void validateLongInchi() throws Exception {
    MiriamData md = new MiriamData(MiriamType.INCHI,
        "InChI=1S/C27H35N9O15P2/c1-10-3-12-13(4-11(10)2)35(24-18(32-12)25(42)34-27(43)33-24)5-14(37)19(39)15(38)6-48-52"
            + "(44,45)51-53(46,47)49-7-16-20(40)21(41)26(50-16)36-9-31-17-22(28)29-8-30-23(17)36/h3-4,8-9,14-16,18-21,26,32,37-41"
            + "H,5-7H2,1-2H3,(H,44,45)(H,46,47)(H2,28,29,30)(H,34,42,43)/t14-,15+,16+,18-,19-,20+,21+,26+/m0/s1");
    assertEquals(0, new ObjectValidator().getValidationIssues(md).size());
  }


  @Test
  public void testGetAnnotationCount() throws Exception {
    final Model model = new ModelFullIndexed(null);
    final Protein p1 = createProtein();
    p1.addMiriamData(new MiriamData(MiriamType.PUBMED, "12345"));
    p1.addMiriamData(new MiriamData(MiriamType.PUBMED, "12346"));
    p1.addMiriamData(new MiriamData(MiriamType.HGNC, "1"));
    model.addElement(p1);
    final Protein p2 = createProtein();
    p2.addMiriamData(new MiriamData(MiriamType.PUBMED, "12345"));
    model.addElement(p2);

    final Protein p3 = createProtein();
    p3.addMiriamData(new MiriamData(MiriamType.PUBMED, "12345"));
    model.addElement(p3);

    final Reaction r = createReaction(p1, p2);
    r.addMiriamData(new MiriamData(MiriamType.PUBMED, "12346"));
    r.addMiriamData(new MiriamData(MiriamType.PUBMED, "12347"));
    model.addReaction(r);

    final Project project = new Project(TEST_PROJECT_ID);
    project.setTopModel(model);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    projectDao.add(project);

    final int count = modelDao.getAnnotationCount(Collections.singletonList(model.getModelData()), MiriamType.PUBMED);
    assertEquals(3, count);
  }

  @Test
  public void testRemoveProjectWithBackground() throws Exception {
    final Project project = new Project(TEST_PROJECT_ID);
    final Model model = new ModelFullIndexed(null);
    final Model submodel = new ModelFullIndexed(null);
    final ModelSubmodelConnection msc = new ModelSubmodelConnection(submodel, SubmodelType.DOWNSTREAM_TARGETS);
    model.addSubmodelConnection(msc);
    project.addModel(model);
    project.addModel(submodel);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    final ProjectBackground background = new ProjectBackground();
    background.addProjectBackgroundImageLayer(new ProjectBackgroundImageLayer(model, "."));
    background.addProjectBackgroundImageLayer(new ProjectBackgroundImageLayer(submodel, "."));
    background.setCreator(project.getOwner());
    project.addProjectBackground(background);
    projectDao.add(project);
    projectDao.flush();

    projectDao.delete(project);
  }

  @Test
  public void testGetAllWithFiltering() throws Exception {
    final Map<ProjectProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(ProjectProperty.PROJECT_ID, Collections.singletonList("unknown"));
    Page<Project> projects = projectDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, projects.getTotalElements());

    filterOptions.put(ProjectProperty.PROJECT_ID, Collections.singletonList(BUILT_IN_PROJECT));
    projects = projectDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, projects.getTotalElements());

  }

  @Test
  public void testRemoveWithDataOverlay() throws Exception {
    final Project project = createAndPersistProject(TEST_PROJECT_ID);

    final DataOverlay overlay = new DataOverlay();
    overlay.setProject(project);
    overlay.setColorSchemaType(DataOverlayType.GENERIC);
    overlay.setCreator(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    final UploadedFileEntry fileEntry = createUploadedFile();

    overlay.setInputData(fileEntry);
    overlay.setName("test");

    final GeneVariantDataOverlayEntry entry = new GeneVariantDataOverlayEntry();
    entry.setColor(Color.BLACK);
    entry.addCompartment("comp");
    entry.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    entry.addType(Protein.class);
    final GeneVariant geneVariant = new GeneVariant();
    geneVariant.setContig("chr1");
    geneVariant.setModifiedDna("AC");
    geneVariant.setOriginalDna("A");
    geneVariant.setPosition(1);
    entry.addGeneVariant(geneVariant);
    overlay.addEntry(entry);

    dataOverlayDao.add(overlay);

    projectDao.delete(project);

    assertNull(dataOverlayDao.getById(overlay.getId()));
  }

  @Test
  public void testRemoveWithComment() throws Exception {
    final Project project = createAndPersistProject(TEST_PROJECT_ID);
    final Comment comment = commentDao.add(createComment(project.getTopModel()));

    projectDao.delete(project);
    assertNull(commentDao.getById(comment.getId()));
  }

  @Test
  public void testRemoveWithLogEntries() throws Exception {
    final Project project = createAndPersistProject(TEST_PROJECT_ID);
    final ProjectLogEntry entry = new ProjectLogEntry();
    entry.setContent("test");
    entry.setSeverity("DEBUG");
    entry.setType(ProjectLogEntryType.OTHER);
    project.addLogEntry(entry);
    logEntryDao.add(entry);

    projectDao.delete(project);
    assertNull(logEntryDao.getById(entry.getId()));
  }

  @Test
  public void testRemoveWithGlyph() throws Exception {
    final Project project = createAndPersistProject(TEST_PROJECT_ID);

    final Glyph glyph = new Glyph();
    glyph.setFile(createUploadedFile());
    project.addGlyph(glyph);
    final Species species = project.getTopModel().getSpeciesList().get(0);
    species.setGlyph(glyph);

    final Layer layer = createLayer();
    final LayerText text = createText();
    text.setGlyph(glyph);
    layer.addLayerText(text);
    project.getTopModel().addLayer(layer);
    projectDao.saveOrUpdate(project);
    projectDao.flush();

    projectDao.delete(project);
    assertNull(glyphDao.getById(glyph.getId()));
  }

  @Test
  public void testFilterProjectsNoFilter() throws Exception {

    final Project project = projectDao.getProjectByProjectId(BUILT_IN_PROJECT);

    final Map<ProjectProperty, List<?>> properties = new HashMap<>();

    final Page<Project> elements = projectDao.getByFilter(PageRequest.of(0, 10), properties);

    assertNotNull(elements);
    assertEquals(1, elements.getNumberOfElements());
    assertEquals(project, elements.getContent().get(0));
  }

  @Test
  public void testFilterProjectsAdminFilter() throws Exception {

    final Project project = projectDao.getProjectByProjectId(BUILT_IN_PROJECT);
    final User admin = userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    final Map<ProjectProperty, List<?>> properties = new HashMap<>();
    properties.put(ProjectProperty.USER, Collections.singletonList(admin));

    final Page<Project> elements = projectDao.getByFilter(PageRequest.of(0, 10), properties);

    assertNotNull(elements);
    assertEquals(1, elements.getNumberOfElements());
    assertEquals(project, elements.getContent().get(0));
  }

  public void testFilterProjectsUserNoAccessFilter() throws Exception {

    final Project project = projectDao.getProjectByProjectId(BUILT_IN_PROJECT);
    final User user = super.createUser();

    final Map<ProjectProperty, List<?>> properties = new HashMap<>();
    properties.put(ProjectProperty.USER, Collections.singletonList(user));

    final Page<Project> elements = projectDao.getByFilter(PageRequest.of(0, 10), properties);

    assertNotNull(elements);
    assertEquals(0, elements.getNumberOfElements());
  }

  @Test
  public void testFilterProjectsUserAccessFilter() throws Exception {

    final Project project = projectDao.getProjectByProjectId(BUILT_IN_PROJECT);
    final User user = super.createUser();
    grantAccess(user, PrivilegeType.READ_PROJECT, BUILT_IN_PROJECT);

    final Map<ProjectProperty, List<?>> properties = new HashMap<>();
    properties.put(ProjectProperty.USER, Collections.singletonList(user));

    final Page<Project> elements = projectDao.getByFilter(PageRequest.of(0, 10), properties);

    assertNotNull(elements);
    assertEquals(1, elements.getNumberOfElements());
    assertEquals(project, elements.getContent().get(0));
  }

}
