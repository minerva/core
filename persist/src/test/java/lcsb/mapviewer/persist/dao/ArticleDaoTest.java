package lcsb.mapviewer.persist.dao;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

import lcsb.mapviewer.model.Article;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.map.MiriamDataDao;
import lcsb.mapviewer.persist.dao.map.ModelDao;

public class ArticleDaoTest extends PersistTestFunctions {

  @Autowired
  private ArticleDao articleDao;

  @Autowired
  private ModelDao modelDao;

  @Autowired
  private MiriamDataDao miriamDataDao;

  @Test
  public void testAdd() {
    Article article = new Article("12345");
    article.addAuthor("Piotr G");
    articleDao.add(article);

    assertEquals(article, articleDao.getById(article.getId()));
  }

  @Test
  public void testGetByFilterPagination() {
    Article article = null;
    for (int i = 0; i < 19; i++) {
      Article a = new Article("" + i);
      articleDao.add(a);
      if (i == 10) {
        article = a;
      }
    }

    Map<ArticleProperty, Object> filterOptions = new HashMap<>();

    Pageable pageable = PageRequest.of(1, 10);

    Page<Article> data = articleDao.getAll(filterOptions, pageable);
    assertEquals(19, data.getTotalElements());
    assertEquals(9, data.getNumberOfElements());
  }

  @Test
  public void testGetByFilterNoPagination() {
    for (int i = 18; i >= 0; i--) {
      articleDao.add(new Article("" + i));
    }

    Map<ArticleProperty, Object> filterOptions = new HashMap<>();

    Pageable pageable = Pageable.unpaged();

    Page<Article> data = articleDao.getAll(filterOptions, pageable);
    assertEquals(19, data.getTotalElements());
    assertEquals(19, data.getNumberOfElements());
  }

  @Test
  public void testGetByFilterSortOrder() {
    for (int i = 188; i >= 100; i--) {
      articleDao.add(new Article("" + i));
    }

    Map<ArticleProperty, Object> filterOptions = new HashMap<>();

    Pageable pageable = PageRequest.of(1, 10, Direction.DESC, ArticleProperty.PUBMED_ID.name());

    Page<Article> data = articleDao.getAll(filterOptions, pageable);
    assertEquals("Sort order violated", "178", data.getContent().get(0).getPubmedId());
  }

  @Test
  public void testGetByFilterSortColumn() {
    for (int i = 188; i >= 100; i--) {
      articleDao.add(new Article("" + i));
    }

    Map<ArticleProperty, Object> filterOptions = new HashMap<>();

    for (final ArticleProperty property : ArticleProperty.values()) {
      if (property != ArticleProperty.MODEL && property != ArticleProperty.TEXT && property != ArticleProperty.AUTHOR) {
        Pageable pageable = PageRequest.of(1, 10, Direction.ASC, property.name());

        Page<Article> data = articleDao.getAll(filterOptions, pageable);
        assertEquals(10, data.getSize());
      }
    }
  }

  @Test
  public void testGetByFilterTextPubmedId() {
    Article article = new Article("1111");
    article.setYear(2020);
    article.setCitationCount(122);
    article.setJournal("Journey");
    article.setLink("http://ilove.you");
    article.setTitle("tle");
    articleDao.add(article);

    Map<ArticleProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(ArticleProperty.TEXT, "11");

    Pageable pageable = PageRequest.of(0, 10);

    Page<Article> data = articleDao.getAll(filterOptions, pageable);
    assertEquals(1, data.getNumberOfElements());
    assertEquals(article, data.getContent().get(0));
  }

  @Test
  public void testGetByFilterPubmedIdNotExisting() {
    Article article = new Article("1111");
    article.setYear(2020);
    article.setCitationCount(122);
    article.setJournal("Journey");
    article.setLink("http://ilove.you");
    article.setTitle("tle");
    articleDao.add(article);

    Map<ArticleProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(ArticleProperty.PUBMED_ID, "11");

    Pageable pageable = PageRequest.of(0, 10);

    Page<Article> data = articleDao.getAll(filterOptions, pageable);
    assertEquals(0, data.getNumberOfElements());
  }

  @Test
  public void testGetByFilterPubmedIdString() {
    Article article = new Article("1111");
    article.setYear(2020);
    article.setCitationCount(122);
    article.setJournal("Journey");
    article.setLink("http://ilove.you");
    article.setTitle("tle");
    articleDao.add(article);

    Map<ArticleProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(ArticleProperty.PUBMED_ID, "1111");

    Pageable pageable = PageRequest.of(0, 10);

    Page<Article> data = articleDao.getAll(filterOptions, pageable);
    assertEquals(1, data.getNumberOfElements());
  }

  @Test
  public void testGetByFilterPubmedIdInteger() {
    Article article = new Article("1111");
    article.setYear(2020);
    article.setCitationCount(122);
    article.setJournal("Journey");
    article.setLink("http://ilove.you");
    article.setTitle("tle");
    articleDao.add(article);

    Map<ArticleProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(ArticleProperty.PUBMED_ID, 1111);

    Pageable pageable = PageRequest.of(0, 10);

    Page<Article> data = articleDao.getAll(filterOptions, pageable);
    assertEquals(1, data.getNumberOfElements());
  }

  @Test
  public void testGetByFilterTextUnknownText() {
    Article article = new Article("1111");
    article.setYear(2020);
    article.setCitationCount(122);
    article.setJournal("Journey");
    article.setLink("http://ilove.you");
    article.setTitle("tle");
    articleDao.add(article);

    Map<ArticleProperty, Object> filterOptions = new HashMap<>();

    Pageable pageable = PageRequest.of(0, 10);

    Page<Article> data = articleDao.getAll(filterOptions, pageable);
    filterOptions.put(ArticleProperty.TEXT, "112");
    data = articleDao.getAll(filterOptions, pageable);
    assertEquals(0, data.getNumberOfElements());
  }

  @Test
  public void testGetByFilterTextYear() {
    Article article = new Article("1111");
    article.setYear(2020);
    article.setCitationCount(122);
    article.setJournal("Journey");
    article.setLink("http://ilove.you");
    article.setTitle("tle");
    articleDao.add(article);

    Map<ArticleProperty, Object> filterOptions = new HashMap<>();

    Pageable pageable = PageRequest.of(0, 10);

    filterOptions.put(ArticleProperty.TEXT, "2020");
    Page<Article> data = articleDao.getAll(filterOptions, pageable);
    assertEquals(1, data.getNumberOfElements());
  }

  @Test
  public void testGetByFilterTextCitationCount() {
    Article article = new Article("1111");
    article.setYear(2020);
    article.setCitationCount(122);
    article.setJournal("Journey");
    article.setLink("http://ilove.you");
    article.setTitle("tle");
    articleDao.add(article);

    Map<ArticleProperty, Object> filterOptions = new HashMap<>();

    Pageable pageable = PageRequest.of(0, 10);

    filterOptions.put(ArticleProperty.TEXT, "122");
    Page<Article> data = articleDao.getAll(filterOptions, pageable);
    assertEquals(1, data.getNumberOfElements());
  }

  @Test
  public void testGetByFilterTextJournal() {
    Article article = new Article("1111");
    article.setYear(2020);
    article.setCitationCount(122);
    article.setJournal("Journey");
    article.setLink("http://ilove.you");
    article.setTitle("tle");
    articleDao.add(article);

    Map<ArticleProperty, Object> filterOptions = new HashMap<>();

    Pageable pageable = PageRequest.of(0, 10);

    filterOptions.put(ArticleProperty.TEXT, "jour");
    Page<Article> data = articleDao.getAll(filterOptions, pageable);
    assertEquals(1, data.getNumberOfElements());
  }

  @Test
  public void testGetByFilterTextLink() {
    Article article = new Article("1111");
    article.setYear(2020);
    article.setCitationCount(122);
    article.setJournal("Journey");
    article.setLink("http://ilove.you");
    article.setTitle("tle");
    articleDao.add(article);

    Map<ArticleProperty, Object> filterOptions = new HashMap<>();

    Pageable pageable = PageRequest.of(0, 10);

    filterOptions.put(ArticleProperty.TEXT, "love");
    Page<Article> data = articleDao.getAll(filterOptions, pageable);
    assertEquals(0, data.getNumberOfElements());
  }

  @Test
  public void testGetByFilterTextTitle() {
    Article article = new Article("1111");
    article.setYear(2020);
    article.setCitationCount(122);
    article.setJournal("Journey");
    article.setLink("http://ilove.you");
    article.setTitle("tle");
    articleDao.add(article);

    Map<ArticleProperty, Object> filterOptions = new HashMap<>();

    Pageable pageable = PageRequest.of(0, 10);

    filterOptions.put(ArticleProperty.TEXT, "LE");
    Page<Article> data = articleDao.getAll(filterOptions, pageable);
    assertEquals(1, data.getNumberOfElements());

  }

  @Test
  public void testGetByFilterInSpeciesMapName() {
    Article article = new Article("1111");
    articleDao.add(article);

    Model map = new ModelFullIndexed(null);
    Protein p2 = createProtein();
    p2.addMiriamData(new MiriamData(MiriamType.PUBMED, "1111"));
    map.addElement(p2);
    map.setName("cool name");

    modelDao.add(map);

    Map<ArticleProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(ArticleProperty.TEXT, "cool name");

    Pageable pageable = PageRequest.of(0, 10);

    Page<Article> data = articleDao.getAll(filterOptions, pageable);
    assertEquals(1, data.getNumberOfElements());
    assertEquals("Default sort order violated", article, data.getContent().get(0));

    filterOptions = new HashMap<>();
    filterOptions.put(ArticleProperty.TEXT, "unknown");

    data = articleDao.getAll(filterOptions, pageable);
    assertEquals(0, data.getNumberOfElements());
  }

  @Test
  public void testGetByFilterInReactionMapName() {
    Article article = new Article("1111");
    articleDao.add(article);

    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    Reaction r1 = createReaction(p1, p2);
    r1.addMiriamData(new MiriamData(MiriamType.PUBMED, "1111"));
    map.addElement(p1);
    map.addElement(p2);
    map.addReaction(r1);
    map.setName("cool name");

    modelDao.add(map);

    Map<ArticleProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(ArticleProperty.TEXT, "cool name");

    Pageable pageable = PageRequest.of(0, 10);

    Page<Article> data = articleDao.getAll(filterOptions, pageable);
    assertEquals(1, data.getNumberOfElements());
    assertEquals("Default sort order violated", article, data.getContent().get(0));

    filterOptions = new HashMap<>();
    filterOptions.put(ArticleProperty.TEXT, "unknown");

    data = articleDao.getAll(filterOptions, pageable);
    assertEquals(0, data.getNumberOfElements());
  }

  @Test
  public void testGetByFilterMap() {
    Article article = new Article("1111");
    articleDao.add(article);
    articleDao.add(new Article("112"));
    articleDao.add(new Article("113"));

    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    p1.addMiriamData(new MiriamData(MiriamType.PUBMED, "1111"));
    p1.addMiriamData(new MiriamData(MiriamType.HGNC, "113"));
    map.addElement(p1);

    modelDao.add(map);

    Model map2 = new ModelFullIndexed(null);
    Protein p2 = createProtein();
    p2.addMiriamData(new MiriamData(MiriamType.PUBMED, "112"));
    map2.addElement(p2);

    modelDao.add(map2);

    Map<ArticleProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(ArticleProperty.MODEL, Arrays.asList(map.getModelData()));

    Pageable pageable = PageRequest.of(0, 10);

    Page<Article> data = articleDao.getAll(filterOptions, pageable);
    assertEquals(1, data.getNumberOfElements());
    assertEquals(article, data.getContent().get(0));
  }

  @Test
  public void testRemoveReferencedByMiriam() {
    Article article = new Article("12345");
    article.addAuthor("Piotr G");
    articleDao.add(article);

    MiriamData md = new MiriamData(MiriamType.TAXONOMY, "9606");
    md.setArticle(article);
    miriamDataDao.add(md);

    articleDao.delete(article);
  }

}
