package lcsb.mapviewer.persist.dao.graphics;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerImage;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.ProjectDao;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ImageDaoTest extends PersistTestFunctions {

  @Autowired
  private ProjectDao projectDao;

  @Autowired
  private LayerImageDao layerImageDao;

  @Test
  public void testFilterByProject() {
    final Project project = createProject();

    Map<LayerImageProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(LayerImageProperty.PROJECT_ID, project.getProjectId());
    Page<LayerImage> page = layerImageDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(LayerImageProperty.PROJECT_ID, TEST_PROJECT_2);
    page = layerImageDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

  @Test
  public void testFilterByLayerImageId() {
    final Project project = createProject();
    final Layer layer = project.getTopModel().getLayers().iterator().next();

    Map<LayerImageProperty, Object> filterOptions = new HashMap<>();
    final LayerImage text = layer.getImages().iterator().next();
    final int LayerImageId = text.getId();
    filterOptions.put(LayerImageProperty.ID, Collections.singletonList(LayerImageId));
    Page<LayerImage> page = layerImageDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(1, page.getNumberOfElements());

    filterOptions = new HashMap<>();
    filterOptions.put(LayerImageProperty.ID, Collections.singletonList(-1));
    page = layerImageDao.getAll(filterOptions, Pageable.unpaged());
    assertEquals(0, page.getNumberOfElements());

  }

  public Project createProject() {
    final Project project = new Project(TEST_PROJECT_ID);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));

    final Model model = new ModelFullIndexed(null);

    final Layer layer = createLayer();
    final LayerImage layerImage = createImage();
    layer.addLayerImage(layerImage);
    model.addLayer(layer);

    project.addModel(model);

    projectDao.add(project);
    return project;
  }

}
