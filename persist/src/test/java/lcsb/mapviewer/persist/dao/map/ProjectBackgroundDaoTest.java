package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.ProjectBackground;
import lcsb.mapviewer.model.map.layout.ProjectBackgroundImageLayer;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.ProjectDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ProjectBackgroundDaoTest extends PersistTestFunctions {

  @Autowired
  private ProjectBackgroundDao backgroundDao;

  @Autowired
  private ProjectDao projectDao;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  private Model createModel() {
    Model model = new ModelFullIndexed(null);

    Species alias = createProtein(264.8333333333335, 517.75, 86.0, 46.0, "sa2");
    model.addElement(alias);
    alias = createProtein(267.6666666666665, 438.75, 80.0, 40.0, "sa1117");
    model.addElement(alias);
    alias = createProtein(261.6666666666665, 600.75, 92.0, 52.0, "sa1119");
    model.addElement(alias);
    alias = createProtein(203.666666666667, 687.75, 98.0, 58.0, "sa1121");
    model.addElement(alias);

    alias = createProtein(817.714285714286, 287.642857142859, 80.0, 40.0, "sa1422");
    Species alias2 = createProtein(224.964285714286, 241.392857142859, 80.0, 40.0, "sa1419");
    Complex alias3 = createComplex(804.714285714286, 182.642857142859, 112.0, 172.0, "csa152");
    alias3.addSpecies(alias);
    alias3.addSpecies(alias2);
    alias.setComplex(alias3);
    alias2.setComplex(alias3);

    model.addElement(alias);
    model.addElement(alias2);
    model.addElement(alias3);

    Compartment compartment = createCompartment(380.0, 416.0, 1893.0, 1866.0, "ca1");
    model.addElement(compartment);
    model.setWidth(2000);
    model.setHeight(2000);

    Layer layer = createLayer();
    model.addLayer(layer);

    layer.addLayerRect(createRect());

    Reaction reaction = createReaction(alias2, alias);
    model.addReaction(reaction);

    Protein protein = createProtein(264.8333333333335, 517.75, 86.0, 46.0, "pr1");
    model.addElement(protein);

    protein.addResidue(createResidue());

    protein.addMiriamData(new MiriamData(MiriamType.CHEBI, "c"));
    return model;
  }

  @Test
  public void testGetBackgroundsByProject() throws Exception {
    Model model = createModel();

    Project project = new Project("test_project_id");
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    project.addModel(model);
    projectDao.add(project);

    List<ProjectBackground> result = backgroundDao.getProjectBackgroundsByProject(project);
    assertEquals(0, result.size());

    ProjectBackground background = new ProjectBackground();
    background.addProjectBackgroundImageLayer(new ProjectBackgroundImageLayer(model, "tmp"));
    background.setName("temporary name");
    background.setCreator(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));

    project.addProjectBackground(background);
    backgroundDao.add(background);

    result = backgroundDao.getProjectBackgroundsByProject(project);
    assertEquals(1, result.size());

    project = projectDao.getById(project.getId());
    projectDao.delete(project);
  }

  @Test
  public void testGetBackgroundsByProjectId() throws Exception {
    Model model = createModel();

    Project project = new Project("test_project_id");
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    project.addModel(model);
    projectDao.add(project);

    List<ProjectBackground> result = backgroundDao.getProjectBackgroundsByProject(project.getProjectId());
    assertEquals(0, result.size());

    ProjectBackground background = new ProjectBackground();
    background.addProjectBackgroundImageLayer(new ProjectBackgroundImageLayer(model, "tmp"));
    background.setName("temporary name");
    background.setCreator(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));

    project.addProjectBackground(background);
    backgroundDao.add(background);

    result = backgroundDao.getProjectBackgroundsByProject(project.getProjectId());
    assertEquals(1, result.size());
    assertTrue(result.get(0) instanceof ProjectBackground);

    project = projectDao.getById(project.getId());
    projectDao.delete(project);
  }

}
