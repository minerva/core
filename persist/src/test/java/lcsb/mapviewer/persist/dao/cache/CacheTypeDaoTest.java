package lcsb.mapviewer.persist.dao.cache;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.persist.PersistTestFunctions;

public class CacheTypeDaoTest extends PersistTestFunctions {

  @Autowired
  protected CacheTypeDao cacheTypeDao;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEmpty() throws Exception {
    assertNull(cacheTypeDao.getByClassName("blablabla"));
  }

  @Test
  public void testArticleCacheData() throws Exception {
    CacheType cacheType = cacheTypeDao.getByClassName("lcsb.mapviewer.annotation.services.PubmedParser");
    assertNotNull(cacheType);
  }
}
