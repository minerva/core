package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.user.UserDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CommentDaoTest extends PersistTestFunctions {

  @Autowired
  protected CommentDao commentDao;

  @Autowired
  protected UserDao userDao;

  @Autowired
  private ProjectDao projectDao;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  public Project createProject() {
    Project project = new Project(TEST_PROJECT_ID);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    project.addModel(createModel());
    projectDao.add(project);
    return project;
  }

  @Test
  public void testGetById() {
    Project project = createProject();


    int counter = (int) commentDao.getCount();
    Comment comment = createComment(project.getTopModel());
    commentDao.add(comment);
    int counter2 = (int) commentDao.getCount();
    assertEquals(counter + 1, counter2);
    Comment comment2 = commentDao.getById(comment.getId());
    assertNotNull(comment2);
    commentDao.delete(comment);
  }

  @Test
  public void testGetComments() {
    Project project = createProject();

    Comment comment = createComment(project.getTopModel());
    comment.setDeleted(true);
    commentDao.add(comment);

    Comment comment2 = createComment(project.getTopModel());
    comment2.setPinned(true);
    commentDao.add(comment2);

    Model model = project.getTopModel();

    assertEquals(0, commentDao.getCommentByModel(model, true, true).size());
    assertEquals(1, commentDao.getCommentByModel(model, false, true).size());
    assertEquals(1, commentDao.getCommentByModel(model, null, true).size());
    assertEquals(1, commentDao.getCommentByModel(model, true, false).size());
    assertEquals(0, commentDao.getCommentByModel(model, false, false).size());
    assertEquals(1, commentDao.getCommentByModel(model, null, false).size());
    assertEquals(1, commentDao.getCommentByModel(model, true, null).size());
    assertEquals(1, commentDao.getCommentByModel(model, false, null).size());
    assertEquals(2, commentDao.getCommentByModel(model, null, null).size());

    assertEquals(0, commentDao.getCommentByModel(model.getModelData(), true, true).size());
    assertEquals(1, commentDao.getCommentByModel(model.getModelData(), false, true).size());
    assertEquals(1, commentDao.getCommentByModel(model.getModelData(), null, true).size());
    assertEquals(1, commentDao.getCommentByModel(model.getModelData(), true, false).size());
    assertEquals(0, commentDao.getCommentByModel(model.getModelData(), false, false).size());
    assertEquals(1, commentDao.getCommentByModel(model.getModelData(), null, false).size());
    assertEquals(1, commentDao.getCommentByModel(model.getModelData(), true, null).size());
    assertEquals(1, commentDao.getCommentByModel(model.getModelData(), false, null).size());
    assertEquals(2, commentDao.getCommentByModel(model.getModelData(), null, null).size());

    commentDao.delete(comment);
    commentDao.delete(comment2);
  }

  private Model createModel() {
    Model model = new ModelFullIndexed(null);

    GenericProtein alias = createProtein(264.8333333333335, 517.75, 86.0, 46.0, "sa2");
    model.addElement(alias);
    alias = createProtein(267.6666666666665, 438.75, 80.0, 40.0, "sa1117");
    model.addElement(alias);
    alias = createProtein(261.6666666666665, 600.75, 92.0, 52.0, "sa1119");
    model.addElement(alias);
    alias = createProtein(203.666666666667, 687.75, 98.0, 58.0, "sa1121");
    model.addElement(alias);

    alias = createProtein(817.714285714286, 287.642857142859, 80.0, 40.0, "sa1422");
    Species alias2 = createProtein(224.964285714286, 241.392857142859, 80.0, 40.0, "sa1419");
    Complex alias3 = createComplex(804.714285714286, 182.642857142859, 112.0, 172.0, "csa152");
    alias3.addSpecies(alias);
    alias3.addSpecies(alias2);
    alias.setComplex(alias3);
    alias2.setComplex(alias3);

    model.addElement(alias);
    model.addElement(alias2);
    model.addElement(alias3);

    Compartment compartment = createCompartment(380.0, 416.0, 1893.0, 1866.0, "ca1");
    model.addElement(compartment);
    model.setWidth(2000);
    model.setHeight(2000);

    Layer layer = createLayer();
    model.addLayer(layer);

    LayerRect lr = createRect();
    layer.addLayerRect(lr);

    Reaction reaction = createReaction(alias2, alias);
    model.addReaction(reaction);

    alias = createProtein(264.8333333333335, 517.75, 86.0, 46.0, "pr1");
    model.addElement(alias);

    Residue mr = createResidue();
    alias.addResidue(mr);

    alias.addMiriamData(new MiriamData(MiriamType.CHEBI, "c"));
    return model;
  }

}
