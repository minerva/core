package lcsb.mapviewer.persist.dao.map;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.HeterodimerAssociationReaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.reaction.type.TruncationReaction;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.persist.PersistTestFunctions;
import lcsb.mapviewer.persist.dao.ProjectDao;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ReactionDaoTest extends PersistTestFunctions {

  @Autowired
  private ReactionDao reactionDao;

  @Autowired
  private ModelDao modelDao;

  @Autowired
  private ProjectDao projectDao;

  @Test
  public void testGetByCoordinatesWithNotEnoughResults() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    map.addElement(p1);
    map.addElement(p2);

    map.addReaction(createReaction(p1, p2));

    modelDao.add(map);

    List<Reaction> reactions = reactionDao.getByCoordinates(map.getModelData(), new Point2D.Double(0, 0), 100,
        new ArrayList<>());

    assertEquals(1, reactions.size());
  }

  @Test
  public void testGetByCoordinates() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    map.addElement(p1);
    map.addElement(p2);

    Reaction r1 = createReaction(p1, p2);
    r1.getReactants().get(0).getLine().setLine(0, 100, 100, 200, 200);
    r1.getProducts().get(0).getLine().setLine(0, 200, 200, 200, 300);
    r1.getLine().setLine(0, 150, 150, 250, 250);
    Reaction r2 = createReaction(p1, p2);
    r2.getReactants().get(0).getLine().setLine(0, 50, 50, 50, 0);
    r2.getProducts().get(0).getLine().setLine(0, 50, 0, 30, 0);
    r2.getLine().setLine(0, 50, 25, 40, 0);
    Reaction r3 = createReaction(p1, p2);
    r3.getReactants().get(0).getLine().setLine(0, 500, 500, 500, 600);
    r3.getProducts().get(0).getLine().setLine(0, 500, 600, 500, 700);
    r3.getLine().setLine(0, 500, 550, 500, 650);
    map.addReaction(r1);
    map.addReaction(r2);
    map.addReaction(r3);

    modelDao.add(map);

    List<Reaction> reactions = reactionDao.getByCoordinates(map.getModelData(), new Point2D.Double(0, 0), 100,
        new ArrayList<>());

    assertEquals(r2, reactions.get(0));
    assertEquals(r1, reactions.get(1));
    assertEquals(r3, reactions.get(2));
  }

  @Test
  public void testGetByCentralLineCoordinates() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    map.addElement(p1);
    map.addElement(p2);

    Reaction r1 = createReaction(p1, p2);
    r1.getReactants().get(0).getLine().setLine(0, 0, 0, 10, 10);
    r1.getProducts().get(0).getLine().setLine(0, 0, 0, 10, 10);
    r1.getLine().setLine(0, 150, 150, 250, 250);
    Reaction r2 = createReaction(p1, p2);
    r2.getReactants().get(0).getLine().setLine(0, 0, 0, 10, 10);
    r2.getProducts().get(0).getLine().setLine(0, 0, 0, 10, 10);
    r2.getLine().setLine(0, 50, 25, 40, 0);
    Reaction r3 = createReaction(p1, p2);
    r3.getReactants().get(0).getLine().setLine(0, 0, 0, 10, 10);
    r3.getProducts().get(0).getLine().setLine(0, 0, 0, 10, 10);
    r3.getLine().setLine(0, 500, 550, 500, 650);
    map.addReaction(r1);
    map.addReaction(r2);
    map.addReaction(r3);

    modelDao.add(map);

    List<Reaction> reactions = reactionDao.getByCoordinates(map.getModelData(), new Point2D.Double(600, 600), 100,
        new ArrayList<>());

    assertEquals(r3, reactions.get(0));
    assertEquals(r1, reactions.get(1));
    assertEquals(r2, reactions.get(2));
  }

  @Test
  public void testGetByCoordinatesWithClassFilter() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    map.addElement(p1);
    map.addElement(p2);

    map.addReaction(createReaction(p1, p2));

    modelDao.add(map);

    List<Reaction> reactions = reactionDao.getByCoordinates(map.getModelData(), new Point2D.Double(0, 0), 100,
        Collections.singletonList(TruncationReaction.class));

    assertEquals(0, reactions.size());
  }

  @Test
  public void testGetByCoordinatesWithExistingClassFilter() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    map.addElement(p1);
    map.addElement(p2);

    map.addReaction(createReaction(p1, p2));

    modelDao.add(map);

    List<Reaction> reactions = reactionDao.getByCoordinates(map.getModelData(), new Point2D.Double(0, 0), 100,
        Collections.singletonList(TransportReaction.class));

    assertEquals(1, reactions.size());
  }

  @Test
  public void testSearchByAnnotation() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    map.addElement(p1);
    map.addElement(p2);

    Reaction r1 = createReaction(p1, p2);
    r1.addMiriamData(new MiriamData(MiriamType.PUBMED, "12345"));

    map.addReaction(r1);

    modelDao.add(map);

    Map<ReactionProperty, Object> filter = new HashMap<>();
    filter.put(ReactionProperty.MAP, Collections.singletonList(map.getModelData()));
    filter.put(ReactionProperty.ANNOTATION, Collections.singletonList(new MiriamData(MiriamType.PUBMED, "12345")));

    Page<Reaction> reactions = reactionDao.getAll(filter, PageRequest.of(0, 1));

    assertEquals(1, reactions.getNumberOfElements());
  }

  @Test
  public void testSearchByClass() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    map.addElement(p1);
    map.addElement(p2);

    Reaction r1 = createReaction(p1, p2);
    map.addReaction(r1);

    modelDao.add(map);

    Map<ReactionProperty, Object> filter = new HashMap<>();
    filter.put(ReactionProperty.CLASS, Collections.singletonList(r1.getClass()));

    Page<Reaction> reactions = reactionDao.getAll(filter, Pageable.unpaged());
    assertEquals(1, reactions.getNumberOfElements());

    filter.put(ReactionProperty.CLASS, Collections.singletonList(HeterodimerAssociationReaction.class));
    reactions = reactionDao.getAll(filter, Pageable.unpaged());
    assertEquals(0, reactions.getNumberOfElements());
  }

  @Test
  public void testSearchByAnnotationCaseInsensitive() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    map.addElement(p1);
    map.addElement(p2);

    Reaction r1 = createReaction(p1, p2);
    r1.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "BLA"));

    map.addReaction(r1);

    modelDao.add(map);

    Map<ReactionProperty, Object> filter = new HashMap<>();
    filter.put(ReactionProperty.MAP, Collections.singletonList(map.getModelData()));
    filter.put(ReactionProperty.ANNOTATION, Collections.singletonList(new MiriamData(MiriamType.HGNC_SYMBOL, "bla")));

    Page<Reaction> reactions = reactionDao.getAll(filter, PageRequest.of(0, 1));

    assertEquals(1, reactions.getNumberOfElements());
  }

  @Test
  public void testSearchByAnnotationInFewMaps() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    map.addElement(p1);
    map.addElement(p2);

    Reaction r1 = createReaction(p1, p2);
    r1.addMiriamData(new MiriamData(MiriamType.PUBMED, "12345"));

    map.addReaction(r1);

    modelDao.add(map);

    Model map2 = new ModelFullIndexed(null);
    modelDao.add(map2);

    Map<ReactionProperty, Object> filter = new HashMap<>();
    filter.put(ReactionProperty.MAP, Arrays.asList(map.getModelData(), map2.getModelData()));
    filter.put(ReactionProperty.ANNOTATION, Collections.singletonList(new MiriamData(MiriamType.PUBMED, "12345")));

    Page<Reaction> reactions = reactionDao.getAll(filter, PageRequest.of(0, 1));

    assertEquals(1, reactions.getNumberOfElements());
  }

  @Test
  public void testSearchByNonExistingAnnotation() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    map.addElement(p1);
    map.addElement(p2);

    Reaction r1 = createReaction(p1, p2);
    r1.addMiriamData(new MiriamData(MiriamType.PUBMED, "12345"));

    map.addReaction(r1);

    modelDao.add(map);

    Map<ReactionProperty, Object> filter = new HashMap<>();
    filter.put(ReactionProperty.MAP, Collections.singletonList(map.getModelData()));
    filter.put(ReactionProperty.ANNOTATION, Collections.singletonList(new MiriamData(MiriamType.PUBMED, "654321")));

    Page<Reaction> reactions = reactionDao.getAll(filter, PageRequest.of(0, 1));

    assertEquals(0, reactions.getNumberOfElements());
  }

  @Test
  public void testGetByQuery() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    map.addElement(p1);
    map.addElement(p2);

    Reaction r1 = createReaction(p1, p2);

    map.addReaction(r1);

    modelDao.add(map);

    Pageable pageable = PageRequest.of(0, 1);

    Map<ReactionProperty, Object> filter = new HashMap<>();
    filter.put(ReactionProperty.PERFECT_TEXT, Collections.singletonList(r1.getElementId()));
    filter.put(ReactionProperty.MAP, Collections.singletonList(map.getModelData()));

    Page<Reaction> reactions = reactionDao.getAll(filter, pageable);

    assertEquals(1, reactions.getNumberOfElements());
  }

  @Test
  public void testGetByText() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    map.addElement(p1);
    map.addElement(p2);

    Reaction r1 = createReaction(p1, p2);

    map.addReaction(r1);

    modelDao.add(map);

    Pageable pageable = PageRequest.of(0, 1);

    Map<ReactionProperty, Object> filter = new HashMap<>();
    filter.put(ReactionProperty.TEXT, Collections.singletonList(r1.getElementId()));
    filter.put(ReactionProperty.MAP, Collections.singletonList(map.getModelData()));

    Page<Reaction> reactions = reactionDao.getAll(filter, pageable);

    assertEquals(1, reactions.getNumberOfElements());
  }

  @Test
  public void testGetByNonExistingQuery() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    map.addElement(p1);
    map.addElement(p2);

    map.addReaction(createReaction(p1, p2));

    modelDao.add(map);

    Map<ReactionProperty, Object> filter = new HashMap<>();
    filter.put(ReactionProperty.PERFECT_TEXT, Collections.singletonList("bla"));
    filter.put(ReactionProperty.MAP, Collections.singletonList(map.getModelData()));

    Page<Reaction> reactions = reactionDao.getAll(filter, PageRequest.of(0, 1));

    assertEquals(0, reactions.getNumberOfElements());
  }

  @Test
  public void testGetByQueryDatabaseId() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    map.addElement(p1);
    map.addElement(p2);

    Reaction r1 = createReaction(p1, p2);

    map.addReaction(r1);

    modelDao.add(map);

    Map<ReactionProperty, Object> filter = new HashMap<>();
    filter.put(ReactionProperty.ID, Collections.singletonList(r1.getId()));
    filter.put(ReactionProperty.MAP, Collections.singletonList(map.getModelData()));

    Page<Reaction> reactions = reactionDao.getAll(filter, PageRequest.of(0, 1));
    assertEquals(1, reactions.getNumberOfElements());
  }

  @Test
  public void testGetByIdsByReactionId() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    Protein p3 = createProtein();
    map.addElement(p1);
    map.addElement(p2);
    map.addElement(p3);

    Reaction r1 = createReaction(p1, p2);
    map.addReaction(r1);
    Reaction r2 = createReaction(p1, p3);
    map.addReaction(r2);

    modelDao.add(map);

    Map<ReactionProperty, Object> filter = new HashMap<>();
    filter.put(ReactionProperty.ID, Collections.singletonList(r1.getId()));
    filter.put(ReactionProperty.MAP, Collections.singletonList(map.getModelData()));

    Page<Reaction> reactions = reactionDao.getAll(filter, Pageable.unpaged());
    assertEquals(Collections.singletonList(r1), reactions.getContent());

    filter.remove(ReactionProperty.ID);

    reactions = reactionDao.getAll(filter, Pageable.unpaged());
    assertEquals(2, reactions.getNumberOfElements());

  }

  @Test
  public void testGetReactionIdWithDash() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    Protein p3 = createProtein();
    map.addElement(p1);
    map.addElement(p2);
    map.addElement(p3);

    Reaction r1 = createReaction(p1, p2);
    r1.setIdReaction("as__ds");
    map.addReaction(r1);

    modelDao.add(map);

    Map<ReactionProperty, Object> filter = new HashMap<>();
    filter.put(ReactionProperty.MAP, Collections.singletonList(map.getModelData()));
    filter.put(ReactionProperty.TEXT, Collections.singletonList("asDS"));

    Page<Reaction> reactions = reactionDao.getAll(filter, PageRequest.of(0, 1));

    assertEquals(Collections.singletonList(r1), reactions.getContent());

    filter.put(ReactionProperty.TEXT, Collections.singletonList("asDSx"));

    reactions = reactionDao.getAll(filter, PageRequest.of(0, 1));

    assertEquals(Collections.emptyList(), reactions.getContent());

  }

  @Test
  public void testGetByIdsByParticipantId() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    Protein p3 = createProtein();

    map.addElement(p1);
    map.addElement(p2);
    map.addElement(p3);

    Reaction r1 = createReaction(p1, p2);
    map.addReaction(r1);
    Reaction r2 = createReaction(p1, p3);
    map.addReaction(r2);

    modelDao.add(map);

    Map<ReactionProperty, Object> filter = new HashMap<>();
    filter.put(ReactionProperty.MAP, Collections.singletonList(map.getModelData()));
    filter.put(ReactionProperty.PARTICIPANT_ID, Collections.singletonList(p1.getId()));

    Page<Reaction> reactions = reactionDao.getAll(filter,
        Pageable.unpaged());
    assertEquals(2, reactions.getNumberOfElements());

    filter.put(ReactionProperty.PARTICIPANT_ID, Collections.singletonList(p2.getId()));
    reactions = reactionDao.getAll(filter, Pageable.unpaged());
    assertEquals(Collections.singletonList(r1), reactions.getContent());

    filter.put(ReactionProperty.PARTICIPANT_ID, Arrays.asList(p1.getId(),
        p2.getId(), p3.getId()));
    reactions = reactionDao.getAll(filter, Pageable.unpaged());
    assertEquals(2, reactions.getNumberOfElements());

    filter.put(ReactionProperty.PARTICIPANT_ID, Collections.singletonList(p1.getId()));
    filter.put(ReactionProperty.ID, Collections.singletonList(r1.getId()));
    reactions = reactionDao.getAll(filter, Pageable.unpaged());
    assertEquals(Collections.singletonList(r1), reactions.getContent());

  }

  @Test
  public void testGetAnnotationStatistics() {
    Model map = new ModelFullIndexed(null);

    Protein p1 = createProtein();
    Protein p2 = createProtein();
    p1.addMiriamData(new MiriamData(MiriamType.HGNC, "12345"));
    map.addElement(p1);
    map.addElement(p2);

    Reaction r1 = createReaction(p1, p2);
    Reaction r2 = createReaction(p1, p2);
    map.addReaction(r1);
    map.addReaction(r2);

    r1.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    r1.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCB"));
    r2.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "BLA"));
    r2.addMiriamData(new MiriamData(MiriamType.HGNC, "123"));
    r2.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));

    Project project = new Project(TEST_PROJECT);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    project.addModel(map);
    projectDao.add(project);

    Map<MiriamType, Integer> result = reactionDao.getAnnotationStatistics(TEST_PROJECT);

    assertEquals((Integer) 4, result.get(MiriamType.HGNC_SYMBOL));
    assertEquals((Integer) 1, result.get(MiriamType.HGNC));

  }

  @Test
  public void testGetDistByCoordinates() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    map.addElement(p1);
    map.addElement(p2);

    Reaction r1 = createReaction(p1, p2);
    r1.getReactants().get(0).getLine().setLine(0, 100, 100, 200, 200);
    r1.getProducts().get(0).getLine().setLine(0, 200, 200, 200, 300);
    r1.getLine().setLine(0, 150, 150, 250, 250);
    map.addReaction(r1);

    modelDao.add(map);

    Point2D point = new Point2D.Double(0, 0);

    List<Pair<Reaction, Double>> reactions = reactionDao.getByCoordinatesWithDistance(map.getModelData(), point, 100,
        new ArrayList<>());

    Pair<Reaction, Double> result = reactions.get(0);
    double distance = result.getRight();

    Reaction reaction = result.getLeft();

    double minDistance = reaction.getDistanceFromPoint(point);

    assertEquals(minDistance, distance, Configuration.EPSILON);
  }

  @Test
  public void testFilterByMapId() {
    Model map = createMap();

    Map<ReactionProperty, Object> filter = new HashMap<>();
    filter.put(ReactionProperty.MAP_ID, Collections.singletonList(map.getId()));

    Page<Reaction> reactions = reactionDao.getAll(filter, Pageable.unpaged());
    assertEquals(2, reactions.getNumberOfElements());

    filter.put(ReactionProperty.MAP_ID, Collections.singletonList(-1));

    reactions = reactionDao.getAll(filter, Pageable.unpaged());
    assertEquals(0, reactions.getNumberOfElements());
  }

  @Test
  public void testFilterByProjectId() {
    Project project = createProject();

    Map<ReactionProperty, Object> filter = new HashMap<>();
    filter.put(ReactionProperty.PROJECT_ID, Collections.singletonList(project.getProjectId()));

    Page<Reaction> reactions = reactionDao.getAll(filter, Pageable.unpaged());
    assertEquals(2, reactions.getNumberOfElements());

    filter.put(ReactionProperty.PROJECT_ID, Collections.singletonList("unknown"));

    reactions = reactionDao.getAll(filter, Pageable.unpaged());
    assertEquals(0, reactions.getNumberOfElements());
  }

  private Project createProject() {
    Project project = new Project(TEST_PROJECT);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    project.addModel(createMap());
    projectDao.add(project);
    return project;
  }

  private Model createMap() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    Protein p3 = createProtein();
    map.addElement(p1);
    map.addElement(p2);
    map.addElement(p3);

    Reaction r1 = createReaction(p1, p2);
    map.addReaction(r1);
    Reaction r2 = createReaction(p1, p3);
    map.addReaction(r2);

    modelDao.add(map);
    return map;
  }

  @Test
  public void testGetByPerfectReactionId() {
    Model map = new ModelFullIndexed(null);
    Protein p1 = createProtein();
    Protein p2 = createProtein();
    map.addElement(p1);
    map.addElement(p2);

    Reaction r1 = createReaction(p1, p2);

    map.addReaction(r1);

    modelDao.add(map);

    Pageable pageable = PageRequest.of(0, 1);

    Map<ReactionProperty, Object> filter = new HashMap<>();
    filter.put(ReactionProperty.PERFECT_TEXT, Collections.singletonList(r1.getId() + ""));

    Page<Reaction> reactions = reactionDao.getAll(filter, pageable);

    assertEquals(1, reactions.getNumberOfElements());
  }

}
