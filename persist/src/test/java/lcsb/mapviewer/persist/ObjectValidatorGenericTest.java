package lcsb.mapviewer.persist;

import static org.junit.Assert.assertEquals;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.model.map.MiriamType;

@RunWith(Parameterized.class)
public class ObjectValidatorGenericTest extends PersistTestFunctions {

  private ObjectValidator objectValidator = new ObjectValidator();

  static class NullString {
    @Column(nullable = false)
    private String val = null;
  }

  static class NullElementInStringList {
    @Column(nullable = false)
    private List<String> val = new ArrayList<>(Arrays.asList("str", null));
  }

  static class NullElementInStringSet {
    @Column(nullable = false)
    private Set<String> val = new HashSet<>(Arrays.asList("str", null));
  }

  static class NullDouble {
    @Column(nullable = false)
    private Double val = null;
  }

  static class NullInteger {
    @Column(nullable = false)
    private Integer val = null;
  }

  static class NullColor {
    @Column(nullable = false)
    private Color val = null;
  }

  static class NullEnum {
    @Column(nullable = false)
    private MiriamType val = null;
  }

  static class NullCalendar {
    @Column(nullable = false)
    private Calendar val = null;
  }

  static class NullClass {
    @Column(nullable = false)
    private Class<?> val = null;
  }

  static class NullPoint {
    @Column(nullable = false)
    private Point2D val = null;
  }

  static class ToLongString {
    @Column(length = 3)
    private String val = "xxxx";
  }

  private Object testedObject;

  public ObjectValidatorGenericTest(final String testName, final Class<?> testedClass) throws Exception {
    this.testedObject = testedClass.newInstance();
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {

    Collection<Object[]> data = new ArrayList<Object[]>();
    data.add(new Object[] { "Too long String", ToLongString.class });
    data.add(new Object[] { "Null String for not nullable", NullString.class });
    data.add(new Object[] { "Null Double for not nullable", NullDouble.class });
    data.add(new Object[] { "Null Integer for not nullable", NullInteger.class });
    data.add(new Object[] { "Null Color for not nullable", NullColor.class });
    data.add(new Object[] { "Null Enum for not nullable", NullEnum.class });
    data.add(new Object[] { "Null Calendar for not nullable", NullCalendar.class });
    data.add(new Object[] { "Null Class for not nullable", NullClass.class });
    data.add(new Object[] { "Null Point for not nullable", NullPoint.class });
    data.add(new Object[] { "Null String in List for not nullable", NullElementInStringList.class });
    data.add(new Object[] { "Null String in Set for not nullable", NullElementInStringSet.class });
    return data;
  }

  @Test
  public void testGetValidation() throws Exception {
    assertEquals(1, objectValidator.getValidationIssues(testedObject).size());
  }

  @Test
  public void testFixValidation() throws Exception {
    objectValidator.fixValidationIssues(testedObject);
    assertEquals(0, objectValidator.getValidationIssues(testedObject).size());
  }

}
