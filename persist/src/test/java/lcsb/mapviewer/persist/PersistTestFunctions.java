package lcsb.mapviewer.persist;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.overlay.DataOverlayGroup;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.security.PrivilegeDao;
import lcsb.mapviewer.persist.dao.user.UserDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.awt.Color;
import java.awt.geom.Point2D;

@Transactional
@Rollback(true)
public abstract class PersistTestFunctions extends PersistTestFunctionsNoTransaction {

  protected static Logger logger = LogManager.getLogger();

  @Autowired
  protected UserDao userDao;

  @Autowired
  protected PrivilegeDao privilegeDao;

  protected User createUser() {
    final User user = new User();
    user.setName("John");
    user.setSurname("Doe");
    user.setEmail("john.doe@uni.lu");
    user.setLogin("john.doe");
    user.setCryptedPassword("passwd");
    userDao.add(user);
    return user;
  }

  protected void grantAccess(final User user, final PrivilegeType type, final String objectId) {
    final Privilege privilege = privilegeDao.getPrivilegeForTypeAndObjectId(type, objectId);
    user.addPrivilege(privilege);
    userDao.update(user);
  }

  protected PolylineData createLine() {
    final PolylineData pd = new PolylineData(new Point2D.Double(1, 1), new Point2D.Double(2, 3));

    pd.getBeginAtd().setArrowType(ArrowType.BLANK_CROSSBAR);
    pd.getBeginAtd().setArrowLineType(LineType.DASHED);
    pd.getBeginAtd().setLen(102);
    pd.setColor(Color.CYAN);
    pd.setType(LineType.SOLID);
    pd.setWidth(2);
    pd.addLine(new Point2D.Double(2, 3), new Point2D.Double(10, 11));
    return pd;
  }

  protected DataOverlayGroup createDataOverlayGroup(final Project project, final User user) {
    DataOverlayGroup dataOverlay = new DataOverlayGroup();
    dataOverlay.setName(faker.name().name());
    dataOverlay.setProject(project);
    dataOverlay.setOwner(user);
    dataOverlay.setOrderIndex(faker.number().numberBetween(1, 10));
    return dataOverlay;
  }


}
