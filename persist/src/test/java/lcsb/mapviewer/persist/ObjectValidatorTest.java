package lcsb.mapviewer.persist;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.awt.geom.Line2D;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.Protein;

public class ObjectValidatorTest extends PersistTestFunctions {

  private ObjectValidator objectValidator = new ObjectValidator();

  @Test
  public void testGetValidationSubjectForSpeciesIssue() throws Exception {
    Protein protein = createProtein("pid");
    protein.setFontSize((Double) null);

    Project project = new Project("proj");
    Model model = new ModelFullIndexed(null);

    project.addModel(model);
    model.addElement(protein);
    List<Pair<Object, String>> issues = objectValidator.getValidationIssues(project);
    assertEquals(1, issues.size());
    assertEquals(protein, issues.get(0).getLeft());
  }

  @Test
  public void testGetValidationSubjectForReactionIssue() throws Exception {
    Reaction reaction = new TransportReaction("id");
    reaction.setLine(new PolylineData());
    List<Pair<Object, String>> issues = objectValidator.getValidationIssues(reaction);
    assertEquals(1, issues.size());
    assertEquals(reaction, issues.get(0).getLeft());
  }

  @Test
  public void testGetValidationSubjectForModelIssue() throws Exception {
    Model m = new ModelFullIndexed(null);
    m.setProject(new Project());
    List<Pair<Object, String>> issues = objectValidator.getValidationIssues(m.getModelData());
    assertEquals(1, issues.size());
    assertEquals(m.getModelData(), issues.get(0).getLeft());
  }

  @Test
  public void testGetValidationSubjectForProjectIssue() throws Exception {
    Project p = new Project();
    List<Pair<Object, String>> issues = objectValidator.getValidationIssues(p);
    assertEquals(1, issues.size());
    assertNull(issues.get(0).getLeft());
  }

  @Test
  public void testLine2DValidation() throws Exception {
    Line2D line = new Line2D.Double(1, 2, 3, 4);
    List<Pair<Object, String>> issues = objectValidator.getValidationIssues(line);
    assertEquals(0, issues.size());
    assertEquals(0, getErrors().size());
  }

  @Test
  public void testStringListInJava17() throws Exception {
    objectValidator.getValidationIssues(Arrays.asList("string"));
    assertEquals(0, getErrors().size());
  }

}
