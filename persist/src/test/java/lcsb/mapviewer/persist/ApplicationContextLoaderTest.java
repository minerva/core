package lcsb.mapviewer.persist;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Constructor;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import lcsb.mapviewer.common.tests.UnitTestFailedWatcher;
import lcsb.mapviewer.persist.dao.user.UserDao;

public class ApplicationContextLoaderTest {

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();


  private ConfigurableApplicationContext originalContext;

  @Before
  public void setUp() throws Exception {
    originalContext = ApplicationContextLoader.getApplicationContext();
    ApplicationContextLoader.setApplicationContext(null);
  }

  @After
  public void tearDown() throws Exception {
    ApplicationContextLoader.setApplicationContext(originalContext);
  }

  @Test
  public void testGetters() {
    ConfigurableApplicationContext ctx = new AnnotationConfigApplicationContext(SpringPersistTestConfig.class);
    ApplicationContextLoader.setApplicationContext(ctx);
    assertEquals(ctx, ApplicationContextLoader.getApplicationContext());
    ctx.close();
  }

  @Test
  public void testLoadApplicationContext() {
    ConfigurableApplicationContext ctx = new AnnotationConfigApplicationContext(SpringPersistTestConfig.class);
    ApplicationContextLoader.setApplicationContext(ctx);
    assertNotNull(ApplicationContextLoader.getApplicationContext());
    ctx.close();
  }

  @Test
  public void testInject() throws Exception {
    class Tmp {
      @Autowired
      private UserDao userDao;

    }
    
    Tmp obj = new Tmp();
    ConfigurableApplicationContext ctx = new AnnotationConfigApplicationContext(SpringPersistTestConfig.class);
    ApplicationContextLoader.setApplicationContext(ctx);
    ApplicationContextLoader.injectDependencies(obj);
    assertNotNull(obj.userDao);
    ctx.close();
  }

  @Test
  public void testPrivateConstructor() throws Exception {
    for (Constructor<?> constructor : ApplicationContextLoader.class.getDeclaredConstructors()) {
      constructor.setAccessible(true);
      ApplicationContextLoader obj = (ApplicationContextLoader) constructor.newInstance(new Object[] {});
      assertNotNull(obj);
    }
  }

}
