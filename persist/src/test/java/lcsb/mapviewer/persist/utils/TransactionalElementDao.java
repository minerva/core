package lcsb.mapviewer.persist.utils;

import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.map.species.ElementDao;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Component
@Transactional
public class TransactionalElementDao {

  @Autowired
  private ElementDao elementDao;

  public Page<Element> getByFilter(final PageRequest pageable, final Map<ElementProperty, Object> filter) {
    return elementDao.getAll(filter, pageable);
  }
}
