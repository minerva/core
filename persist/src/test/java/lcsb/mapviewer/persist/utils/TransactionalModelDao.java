package lcsb.mapviewer.persist.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.map.ModelDao;

@Component
@Transactional
public class TransactionalModelDao {

  @Autowired
  private ModelDao modelDao;

  public void add(final ModelData map) {
    modelDao.add(map);

  }

  public void delete(final ModelData map) {
    modelDao.delete(map);
  }
}
