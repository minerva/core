package lcsb.mapviewer.persist.mapper;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.Serializable;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.ArrowTypeData;
import lcsb.mapviewer.model.graphics.LineType;

public class ArrowTypeDataMapperTest {

  private ArrowTypeDataMapper mapper = new ArrowTypeDataMapper();

  private ArrowTypeData atd;
  private ArrowTypeData atd2;

  @Before
  public void setUp() throws Exception {
    atd = new ArrowTypeData();
    atd.setAngle(12);
    atd.setArrowLineType(LineType.DASH_DOT);
    atd.setArrowType(ArrowType.CIRCLE);
    atd.setLen(43.3);

    atd2 = new ArrowTypeData();
    atd2.setAngle(18);
    atd2.setArrowLineType(LineType.DASHED);
    atd2.setArrowType(ArrowType.BLANK);
    atd2.setLen(33.3);

  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSetPropertyValue() {
    Object val = mapper.getPropertyValue(atd, 0);
    mapper.setPropertyValue(atd2, 0, val);
    assertTrue(atd.equals(atd2));
  }

  @Test
  public void testReturnedClass() {
    assertNotNull(mapper.returnedClass());
  }

  @Test
  public void testHashCode() {
    assertTrue(mapper.hashCode(atd) != mapper.hashCode(atd2));
  }

  @Test
  public void testDisassemble() {
    Serializable obj = mapper.disassemble(atd, null);
    ArrowTypeData object2 = (ArrowTypeData) mapper.assemble(obj, null, null);

    assertTrue(atd.equals(object2));
  }

  @Test
  public void testReplace() {
    assertNotNull(mapper.replace(atd, atd2, null, null));
  }

}
