package lcsb.mapviewer.persist.mapper;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;
import java.io.Serializable;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Point2DMapperTest {

  private Point2DMapper mapper = new Point2DMapper();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSetPropertyValue() {
    Point2D component = new Point2D.Double(0, 3);
    Point2D component2 = new Point2D.Double(0, 0);
    Object val = mapper.getPropertyValue(component, 0);
    mapper.setPropertyValue(component2, 0, val);
    assertTrue(component.equals(component2));
  }

  @Test
  public void testReturnedClass() {
    assertNotNull(mapper.returnedClass());
  }

  @Test
  public void testEqualsIdentical() {
    Point2D point = new Point2D.Double(1, 2);
    assertTrue(mapper.equals(point, point));
  }

  @Test
  public void testEquals() {
    Point2D point = new Point2D.Double(1, 2);
    Point2D point2 = new Point2D.Double(1, 2);
    assertTrue(mapper.equals(point, point2));
  }

  @Test
  public void testEqualsDifferent() {
    Point2D point = new Point2D.Double(1, 2);
    Point2D point2 = new Point2D.Double(1, 2.2);
    assertFalse(mapper.equals(point, point2));
  }

  @Test
  public void testHashCode() {
    Point2D component = new Point2D.Double(0, 3);
    Point2D component2 = new Point2D.Double(0, 0);
    assertTrue(mapper.hashCode(component) != mapper.hashCode(component2));
  }

  @Test
  public void testDisassemble() {
    Point2D object = new Point2D.Double(0, 3);
    Serializable obj = mapper.disassemble(object, null);
    Point2D object2 = (Point2D) mapper.assemble(obj, null, null);

    assertTrue(object.equals(object2));
  }

  @Test
  public void testReplace() {
    assertNotNull(mapper.replace(new Point2D.Double(0, 3), new Point2D.Double(0, 9), null, null));
  }

}
