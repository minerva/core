package lcsb.mapviewer.persist.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Line2D;

import org.junit.Test;

import lcsb.mapviewer.common.comparator.LineComparator;

public class Line2DMapperTest {

  private Line2DMapper mapper = new Line2DMapper();

  @Test
  public void testIdenticalEquals() {
    Line2D line = new Line2D.Double(1, 2, 3, 4);
    assertTrue(mapper.equals(line, line));
  }

  @Test
  public void testEquals() {
    Line2D line = new Line2D.Double(1, 2, 3, 4);
    Line2D line2 = new Line2D.Double(1, 2, 3, 4);
    assertTrue(mapper.equals(line, line2));
  }

  @Test
  public void testEqualsDifferent() {
    Line2D line = new Line2D.Double(1, 2, 3, 4);
    Line2D line2 = new Line2D.Double(1, 2, 3, 4.2);
    assertFalse(mapper.equals(line, line2));
  }

  @Test
  public void testSetPropertyValue() {
    Line2D line = new Line2D.Double(1, 2, 3, 4);
    mapper.setPropertyValue(line, 0, 100.0);
    assertEquals(0, new LineComparator().compare(new Line2D.Double(100, 2, 3, 4), line));
  }

}
