package lcsb.mapviewer.persist;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.user.UserDao;

/**
 * Make sure that the state after all migration scripts is what we expect to be.
 *
 */
public class InitialStateTest extends PersistTestFunctions {

  @Autowired
  private UserDao userDao;

  @Autowired
  private ProjectDao projectDao;

  @Test
  public void testAdminHasAdminPrivileges() throws Exception {
    User admin = userDao.getUserByLogin("admin");
    assertNotNull(admin);
    assertTrue(admin.getPrivileges().contains(new Privilege(PrivilegeType.IS_ADMIN)));
  }

  @Test
  public void testUserHasPrivilegeToExistingProjects() throws Exception {
    for (final User user : userDao.getAll()) {
      for (final Privilege privilege : user.getPrivileges()) {
        if (privilege.getObjectId() != null && privilege.getType().getPrivilegeObjectType().equals(Project.class)) {
          assertNotNull(user.getLogin() + " has privilege to a project that doesn't exist: " + privilege.getObjectId(),
              projectDao.getProjectByProjectId(privilege.getObjectId()));
        }
      }
    }
  }

}
