package lcsb.mapviewer.persist;

import static org.junit.Assert.assertFalse;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class DbUtilsTest extends PersistTestFunctions {
  @Autowired
  protected DbUtils dbUtils;

  @Test
  public void testAutoFlush() {
    boolean autoflush = dbUtils.isAutoFlush();
    dbUtils.setAutoFlush(false);
    assertFalse(dbUtils.isAutoFlush());
    dbUtils.setAutoFlush(autoflush);
  }

  @Test
  public void testVacuumDontCrash() throws Exception {
    dbUtils.vaccuum();
  }
}
