package lcsb.mapviewer.converter;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipFile;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.converter.zip.ZipEntryFile;
import lcsb.mapviewer.model.cache.UploadedFileEntry;

/**
 * Class that contains information about input data used for creation of the
 * complex {@link lcsb.mapviewer.model.map.model.Model Model} with submaps.
 * 
 * @author Piotr Gawron
 * 
 */
public class ComplexZipConverterParams {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger();

  /**
   * Zip file where all maps are stored.
   */
  private ZipFile zipFile;

  /**
   * Mapping between filename and information provided by the user about the file.
   */
  private Map<String, ZipEntryFile> entries = new HashMap<>();

  /**
   * Directory where additional visualization files should be stored.
   */
  private String visualizationDir = null;

  /**
   * Sets {@link #zipFile}.
   * 
   * @param zipFile
   *          {@link #zipFile}
   * @return object with all parameters
   */
  public ComplexZipConverterParams zipFile(final ZipFile zipFile) {
    this.zipFile = zipFile;
    return this;
  }

  public ComplexZipConverterParams zipFile(final UploadedFileEntry file) throws IOException {
    File temp = File.createTempFile("model", ".xml");
    IOUtils.copy(new ByteArrayInputStream(file.getFileContent()), new FileOutputStream(temp));
    return zipFile(temp.getAbsolutePath());
  }

  /**
   * Sets {@link #zipFile}.
   * 
   * @param filename
   *          name of the {@link #zipFile}
   * @return object with all parameters
   * @throws IOException
   *           throw when there is a problem with a file
   */
  public ComplexZipConverterParams zipFile(final String filename) throws IOException {
    this.zipFile = new ZipFile(filename);
    return this;
  }

  /**
   * Sets information about single entry in the zip file.
   * 
   * @param entry
   *          set of information about single zip entry
   * @return object with all parameters
   */
  public ComplexZipConverterParams entry(final ZipEntryFile entry) {
    this.entries.put(entry.getFilename().toLowerCase(), entry);
    return this;
  }

  /**
   * @return the zipFile
   * @see #zipFile
   */
  public ZipFile getZipFile() {
    return zipFile;
  }

  /**
   * Returns set of filenames put in this param object.
   * 
   * @return set of filenames put in this param object
   */
  public Set<String> getFilenames() {
    return entries.keySet();
  }

  /**
   * Sets {@link #visualizationDir} value.
   * 
   * @param string
   *          new value
   * @return instance of this class
   */
  public ComplexZipConverterParams visualizationDir(final String string) {
    this.visualizationDir = string;
    return this;
  }

  /**
   * Returns {@link #visualizationDir}.
   * 
   * @return {@link #visualizationDir}
   */
  public String getVisualizationDir() {
    return visualizationDir;
  }

  /**
   * Return {@link ZipEntryFile entry} with information about single file in the
   * zip archive identified by file name (file name is case insensitive).
   * 
   * @param name
   *          name of the file in zip archive
   * @return {@link ZipEntryFile entry} with information about file
   */
  public ZipEntryFile getEntry(final String name) {
    return entries.get(name.toLowerCase());
  }
}
