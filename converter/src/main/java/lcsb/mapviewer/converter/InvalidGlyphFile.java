package lcsb.mapviewer.converter;

/**
 * Exception thrown when the zip file containing data about
 * {@link lcsb.mapviewer.model.map.layout.graphics.Glyph} is invalid.
 * 
 * @author Piotr Gawron
 * 
 */
public class InvalidGlyphFile extends InvalidInputDataExecption {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   * 
   * @param e
   *          parent exception (reason)
   */
  public InvalidGlyphFile(final Exception e) {
    super(e);
  }

  /**
   * Default constructor.
   * 
   * @param message
   *          exception message
   * @param e
   *          parent exception (reason)
   */
  public InvalidGlyphFile(final String message, final Exception e) {
    super(message, e);
  }

  /**
   * Default constructor.
   * 
   * @param message
   *          exception message
   */
  public InvalidGlyphFile(final String message) {
    super(message);
  }
}
