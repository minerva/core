package lcsb.mapviewer.converter;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipFile;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.converter.zip.GlyphZipEntryFile;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;

/**
 * Parser used to extract data about {@link Glyph} from zip file.
 * 
 * @author Piotr Gawron
 * 
 */
public class GlyphParser {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger();

  /**
   * Method that parse zip entry and creates a {@link Glyph} from it.
   *
   * @param entry
   *          zip entry to parse
   * @param zipFile
   *          original file where zip entry comes from
   * @throws IOException
   *           thrown when there is a problem with zip file
   * @throws InvalidGlyphFile
   *           thrown when the zip file contains invalid data
   */
  public Glyph parseGlyph(final GlyphZipEntryFile entry, final ZipFile zipFile) throws InvalidGlyphFile, IOException {

    String filename = FilenameUtils.getName(entry.getFilename());
    if (filename.toLowerCase().endsWith("png")) {
      InputStream is = zipFile.getInputStream(zipFile.getEntry(entry.getFilename()));

      UploadedFileEntry file = new UploadedFileEntry();
      file.setFileContent(IOUtils.toByteArray(is));
      file.setOriginalFileName(entry.getFilename());
      file.setLength(file.getFileContent().length);

      Glyph result = new Glyph();
      result.setFile(file);
      return result;
    } else {
      throw new InvalidGlyphFile("Unknown file in overview images zip archive: " + filename);
    }
  }

}
