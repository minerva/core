package lcsb.mapviewer.converter;

import lcsb.mapviewer.common.TextFileUtils;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidClassException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.zip.GlyphZipEntryFile;
import lcsb.mapviewer.converter.zip.ImageZipEntryFile;
import lcsb.mapviewer.converter.zip.LayoutZipEntryFile;
import lcsb.mapviewer.converter.zip.ModelZipEntryFile;
import lcsb.mapviewer.converter.zip.ZipEntryFile;
import lcsb.mapviewer.converter.zip.ZipEntryFileFactory;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.model.map.model.ElementSubmodelConnection;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayType;
import lcsb.mapviewer.model.overlay.InvalidDataOverlayException;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Modifier;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * This class allows to create complex {@link Model} that contains submaps. It's
 * written in generic way and use {@link Converter} as a class to parse single
 * submap.
 *
 * @author Piotr Gawron
 */
public class ComplexZipConverter {

  /**
   * Size of the buffer used for accessing single chunk of data from input stream.
   */
  private static final int BUFFER_SIZE = 1024;

  /**
   * Default class logger.
   */
  private static final Logger logger = LogManager.getLogger();
  private static final String IMMEDIATE_LINK_PREFIX = "IMMEDIATE_LINK:";

  /**
   * Class used to create single submap from a file.
   */
  private final Class<? extends Converter> converterClazz;

  /**
   * Default constructor. Checks if the class given in the parameter is proper
   * {@link Converter} implementation.
   *
   * @param clazz {@link Converter} class used for creation of the single submap
   */
  public ComplexZipConverter(final Class<? extends Converter> clazz) {
    if (Modifier.isAbstract(clazz.getModifiers())) {
      throw new InvalidClassException("Param class cannot be abstract");
    }

    if (Modifier.isInterface(clazz.getModifiers())) {
      throw new InvalidClassException("Param class cannot be an interface");
    }

    converterClazz = clazz;
  }

  /**
   * Creates complex {@link Model} that contains submaps.
   *
   * @param params {@link ComplexZipConverterParams object} with information about data
   *               from which result is going to be created
   * @return complex {@link Model} created from input data
   * @throws InvalidInputDataExecption thrown when there is a problem with accessing input data
   */
  public Model createModel(final ComplexZipConverterParams params) throws InvalidInputDataExecption, ConverterException {
    try {
      ZipFile zipFile = params.getZipFile();
      Enumeration<? extends ZipEntry> entries;
      String mapping = validateSubmodelInformation(params, zipFile);

      Converter converter = createConverterInstance();
      Map<String, Model> filenameModelMap = new HashMap<>();
      Map<String, Model> nameModelMap = new HashMap<>();

      entries = zipFile.entries();
      Model result = null;
      while (entries.hasMoreElements()) {
        ZipEntry entry = entries.nextElement();
        if (!entry.isDirectory()) {
          ZipEntryFile zef = params.getEntry(entry.getName());
          if (zef instanceof ModelZipEntryFile) {
            ModelZipEntryFile modelEntryFile = (ModelZipEntryFile) zef;
            InputStream is = zipFile.getInputStream(entry);
            Model model = converter.createModel(new ConverterParams().inputStream(is, entry.getName()));
            model.setName(modelEntryFile.getName());
            filenameModelMap.put(entry.getName(), model);
            nameModelMap.put(FilenameUtils.getBaseName(modelEntryFile.getFilename()).toLowerCase(), model);
            if (modelEntryFile.isRoot()) {
              result = model;
            }
          } else if (zef instanceof LayoutZipEntryFile) {
            continue;
          } else if (zef instanceof ImageZipEntryFile) {
            continue;
          } else if (zef instanceof GlyphZipEntryFile) {
            continue;
          } else if (!isIgnoredFile(entry.getName())) {
            throw new NotImplementedException("Unknown entry type: " + zef.getClass());
          }
        }
      }

      for (Entry<String, Model> entry : filenameModelMap.entrySet()) {
        String filename = entry.getKey();
        Model model = entry.getValue();
        ZipEntryFile zef = params.getEntry(filename);
        if (zef instanceof ModelZipEntryFile) {
          ModelZipEntryFile modelEntryFile = (ModelZipEntryFile) zef;
          if (!modelEntryFile.isRoot() && !modelEntryFile.isMappingFile()) {
            ModelSubmodelConnection submodel = new ModelSubmodelConnection(model, modelEntryFile.getType());
            submodel.setName(modelEntryFile.getName());
            result.addSubmodelConnection(submodel);
          }
        }
      }
      Model mappingModel = filenameModelMap.get(mapping);
      if (mappingModel != null) {
        for (final Reaction reaction : mappingModel.getReactions()) {
          processReaction(mapping, nameModelMap, result, reaction);
        }
        for (final Species species : mappingModel.getSpeciesList()) {
          processSpecies(species, nameModelMap);
        }
      }
      return result;
    } catch (final IOException e) {
      throw new InvalidArgumentException(e);
    }
  }

  private void processSpecies(final Species species, final Map<String, Model> nameModelMap) {
    String notes = species.getNotes();
    if (notes != null) {
      String[] lines = notes.split("\n");
      for (String line : lines) {
        line = line.trim();
        if (line.startsWith(IMMEDIATE_LINK_PREFIX)) {
          String link = line.replace(IMMEDIATE_LINK_PREFIX, "").trim();

          Compartment compartment = species.getCompartment();
          if (compartment == null) {
            logger.warn("[SUBMODEL MAPPING] Species {} in mapping file doesn't start inside compartment. Skipped. Link skipped",
                species.getElementId());
          } else {
            String modelName = compartment.getName().toLowerCase();
            Model model = nameModelMap.get(modelName);
            if (model == null) {
              throw new InvalidArgumentException("Mapping file references to " + modelName + " submodel. But such model doesn't exist");
            }
            Element elementToChange = model.getElementByElementId(species.getName());
            if (elementToChange == null) {
              throw new InvalidArgumentException("Mapping file references to element with alias: " + species.getName()
                  + ". But such element doesn't exist");
            }
            elementToChange.setImmediateLink(link);
          }
        }
      }
    }
  }

  protected boolean isIgnoredFile(final String name) {
    if (name == null) {
      return true;
    } else if (name.isEmpty()) {
      return true;
    } else if (name.startsWith(".DS_Store") || name.endsWith(".DS_Store")) {
      return true;
    } else {
      return name.startsWith("__MACOSX");
    }
  }

  /**
   * Process a single reaction in mapping file (transforming reaction into
   * connection between submodels).
   *
   * @param mapping      name of the mapping file
   * @param nameModelMap mapping between file names and models
   * @param topModel     top model
   * @param reaction     reaction to transform into connection
   */
  public void processReaction(final String mapping, final Map<String, Model> nameModelMap, final Model topModel, final Reaction reaction) {
    if (reaction.getReactants().size() > 1) {
      logger.warn("[SUBMODEL MAPPING] Reaction {} in mapping file ({}) contains too many reactants. Skipped",
          reaction.getIdReaction(), mapping);
    } else if (reaction.getProducts().size() > 1) {
      logger.warn("[SUBMODEL MAPPING] Reaction {} in mapping file ({}) contains too many products. Skipped",
          reaction.getIdReaction(), mapping);
    } else if (!reaction.getModifiers().isEmpty()) {
      logger.warn("[SUBMODEL MAPPING] Reaction {} in mapping file ({}) contains modifiers. Skipped",
          reaction.getIdReaction(), mapping);
    } else {
      Element fromAlias = reaction.getReactants().get(0).getElement();
      Element toAlias = reaction.getProducts().get(0).getElement();
      if (!(fromAlias instanceof Species)) {
        logger.warn("[SUBMODEL MAPPING] Reaction {} in mapping file ({}) contains doesn't start in species. Skipped",
            reaction.getIdReaction(), mapping);
      } else if (!(toAlias instanceof Species)) {
        logger.warn("[SUBMODEL MAPPING] Reaction {} in mapping file ({}) contains doesn't end in species. Skipped",
            reaction.getIdReaction(), mapping);
      } else {
        Complex complexFrom = ((Species) fromAlias).getComplex();
        Complex complexTo = ((Species) toAlias).getComplex();
        if (complexFrom == null) {
          logger.warn("[SUBMODEL MAPPING] Reaction {} in mapping file ({}) contains doesn't start inside complex. Skipped",
              reaction.getIdReaction(), mapping);
        } else if (complexTo == null && (!(toAlias instanceof Complex))) {
          logger.warn("[SUBMODEL MAPPING] Reaction {} in mapping file ({}) contains doesn't end inside complex. Skipped",
              reaction.getIdReaction(), mapping);
        } else {
          if (complexTo == null) {
            complexTo = (Complex) toAlias;
          }
          String fromName = complexFrom.getName().toLowerCase();
          String toName = complexTo.getName().toLowerCase();
          Model fromModel = nameModelMap.get(fromName);
          Model toModel = nameModelMap.get(toName);
          if (fromModel == null) {
            throw new InvalidArgumentException("Mapping file references to " + fromName + " submodel. But such model doesn't exist");
          } else if (toModel == null) {
            throw new InvalidArgumentException("Mapping file references to " + toName + " submodel. But such model doesn't exist");
          }
          Element source = fromModel.getElementByElementId(fromAlias.getName());
          if (source == null) {
            throw new InvalidArgumentException("Mapping file references to element with alias: " + fromAlias.getName()
                + ". But such element doesn't exist");
          }
          Element dest = null;
          if (!(toAlias instanceof Complex)) {
            dest = fromModel.getElementByElementId(toAlias.getName());
          }
          SubmodelType type = SubmodelType.UNKNOWN;
          if (fromAlias instanceof Protein) {
            type = SubmodelType.DOWNSTREAM_TARGETS;
          } else if (fromAlias instanceof Phenotype) {
            type = SubmodelType.PATHWAY;
          }
          ElementSubmodelConnection connection = new ElementSubmodelConnection(toModel, type);
          connection.setFromElement(source);
          connection.setToElement(dest);
          connection.setName(toModel.getName());
          source.setSubmodel(connection);
        }
      }
    }
  }

  /**
   * This method validates if information about model and submodels in the params
   * are sufficient. If not then appropriate exception will be thrown.
   *
   * @param params  parameters to validate
   * @param zipFile original {@link ZipFile}
   * @return name of the file containing mapping between submodels
   */
  protected String validateSubmodelInformation(final ComplexZipConverterParams params, final ZipFile zipFile) {
    Enumeration<? extends ZipEntry> entries = zipFile.entries();
    Set<String> processed = new HashSet<>();

    String root = null;
    String mapping = null;
    while (entries.hasMoreElements()) {
      ZipEntry entry = entries.nextElement();
      if (!entry.isDirectory()) {
        String name = entry.getName();
        ZipEntryFile zef = params.getEntry(name);
        if (zef == null && !isIgnoredFile(name)) {
          throw new InvalidArgumentException("No information found in params about file: " + name);
        }
        if (zef instanceof ModelZipEntryFile) {
          ModelZipEntryFile modelEntryFile = (ModelZipEntryFile) zef;
          if (modelEntryFile.isRoot()) {
            if (root != null) {
              throw new InvalidArgumentException("Two roots found: " + name + ", " + root + ". There can be only one.");
            }
            root = name;
          }
          if (modelEntryFile.isMappingFile()) {
            if (mapping != null) {
              throw new InvalidArgumentException(
                  "Two mapping files found: " + name + ", " + mapping + ". There can be only one.");
            }
            mapping = name;
          }
        }
        processed.add(name.toLowerCase());
      }
    }
    if (root == null) {
      throw new InvalidArgumentException("No root map found.");
    }
    for (final String entryName : params.getFilenames()) {
      if (!processed.contains(entryName)) {
        throw new InvalidArgumentException("Entry " + entryName + " doesn't exists in the zip file.");
      }
    }
    return mapping;
  }

  /**
   * Transforms {@link LayoutZipEntryFile} into {@link DataOverlay}.
   *
   * @param zipFile     original {@link ZipFile}
   * @param entry       entry in a zip file
   * @param params      parameters used to make general conversion (we use directory where
   *                    layout should be stored)
   * @param layoutEntry {@link LayoutZipEntryFile} to transform
   * @return {@link DataOverlay} for a given {@link LayoutZipEntryFile}
   * @throws IOException thrown when there is a problem with accessing {@link ZipFile}
   */
  public DataOverlay layoutZipEntryFileToLayout(final ComplexZipConverterParams params, final ZipFile zipFile, final ZipEntry entry,
                                                final LayoutZipEntryFile layoutEntry, final int order) throws IOException, InvalidInputDataExecption {
    DataOverlay layout = new DataOverlay();
    layout.setDescription(layoutEntry.getDescription());
    UploadedFileEntry fileEntry = new UploadedFileEntry();
    fileEntry.setFileContent(IOUtils.toByteArray(zipFile.getInputStream(entry)));
    fileEntry.setOriginalFileName(entry.getName());
    fileEntry.setLength(fileEntry.getFileContent().length);

    Map<String, String> parameters = TextFileUtils.getHeaderParametersFromFile(zipFile.getInputStream(entry));
    DataOverlayType colorSchemaType = null;
    String type = parameters.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE);
    if (type != null) {
      colorSchemaType = DataOverlayType.valueOf(type);
    }
    if (colorSchemaType == null) {
      colorSchemaType = DataOverlayType.GENERIC;
    }

    layout.setColorSchemaType(colorSchemaType);
    layout.setInputData(fileEntry);
    layout.setPublic(true);
    layout.setName(layoutEntry.getName());
    layout.setOrderIndex(order);

    ColorSchemaReader reader = new ColorSchemaReader();
    ByteArrayInputStream baos = new ByteArrayInputStream(fileEntry.getFileContent());
    try {
      layout.addEntries(reader.readColorSchema(baos, parameters));

      if (colorSchemaType == DataOverlayType.GENETIC_VARIANT) {
        String genomeVersion = parameters.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_GENOME_VERSION);
        ReferenceGenomeType genomeType = ReferenceGenomeType.UCSC;
        if (parameters.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_GENOME_TYPE) != null) {
          genomeType = ReferenceGenomeType
              .valueOf(parameters.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_GENOME_TYPE));
        }
        layout.setGenomeType(genomeType);
        layout.setGenomeVersion(genomeVersion);
      }

    } catch (final InvalidDataOverlayException e) {
      throw new InvalidInputDataExecption("Data overlay is invalid", e);
    }
    return layout;
  }

  /**
   * Copy file from zip entry into temporary file.
   *
   * @param zipFile input zip file
   * @param entry   entry in the zip file
   * @return {@link File} with a copy of data from zip entry
   * @throws IOException thrown when there is a problem with input or output file
   */
  protected File saveFileFromZipFile(final ZipFile zipFile, final ZipEntry entry) throws IOException {
    InputStream is = zipFile.getInputStream(entry);
    File result = File.createTempFile("temp-file-name", ".png");
    FileOutputStream fos = new FileOutputStream(result);
    byte[] bytes = new byte[BUFFER_SIZE];
    int length;
    while ((length = is.read(bytes)) >= 0) {
      fos.write(bytes, 0, length);
    }
    fos.close();
    return result;
  }

  /**
   * Creates instance of {@link Converter} used as a template parameter for this
   * class instantiation.
   *
   * @return instance of {@link Converter}
   */
  protected Converter createConverterInstance() {
    Converter converter;
    try {
      converter = converterClazz.newInstance();
    } catch (final InstantiationException | IllegalAccessException e) {
      throw new InvalidClassException("Problem with instantiation of the class: " + converterClazz, e);
    }
    return converter;
  }
}
