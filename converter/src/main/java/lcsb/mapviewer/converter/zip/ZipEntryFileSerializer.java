package lcsb.mapviewer.converter.zip;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.zip.ZipEntryFileDeserializer.ZipEntryFileType;

public class ZipEntryFileSerializer extends StdSerializer<ZipEntryFile> {

  protected ZipEntryFileSerializer() {
    super(ZipEntryFile.class);
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Override
  public void serialize(final ZipEntryFile schema, final JsonGenerator gen, final SerializerProvider serializers) throws IOException {
    gen.writeStartObject();

    gen.writeStringField("_filename", schema.getFilename());
    ZipEntryFileType type = null;

    gen.writeObjectFieldStart("_data");
    if (schema instanceof GlyphZipEntryFile) {
      type = ZipEntryFileType.GLYPH;
    } else if (schema instanceof ImageZipEntryFile) {
      type = ZipEntryFileType.IMAGE;
    } else if (schema instanceof LayoutZipEntryFile) {
      type = ZipEntryFileType.OVERLAY;
      gen.writeStringField("name", ((LayoutZipEntryFile) schema).getName());
      gen.writeStringField("description", ((LayoutZipEntryFile) schema).getDescription());
    } else if (schema instanceof ModelZipEntryFile) {
      type = ZipEntryFileType.MAP;
      gen.writeStringField("name", ((ModelZipEntryFile) schema).getName());
      gen.writeObjectFieldStart("type");
      gen.writeObjectField("id", ((ModelZipEntryFile) schema).getType());
      gen.writeEndObject();
      gen.writeStringField("root", ((ModelZipEntryFile) schema).getRoot());
      gen.writeStringField("mapping", ((ModelZipEntryFile) schema).getMapping());
    } else {
      throw new NotImplementedException("Type not imlemented: " + schema);
    }

    gen.writeEndObject();

    gen.writeObjectField("_type", type);
    gen.writeEndObject();
  }
}
