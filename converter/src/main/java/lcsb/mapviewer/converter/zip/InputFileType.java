package lcsb.mapviewer.converter.zip;

/**
 * This enum identifies type of input file inside zip archive given as the input
 * to MINERVa framework.
 * 
 * @author Piotr Gawron
 * 
 */
public enum InputFileType {
  /**
   * Identifies file which is CellDesigner file.
   */
  CELLDESIGNER("CellDesigner (*.xml)"),

  /**
   * Unknown file type.
   */
  UNKNOWN("Unknown (*.*)");

  /**
   * Common name used for the type.
   */
  private String commonName;

  /**
   * Default constructor.
   * 
   * @param commonName
   *          {@link #commonName}
   */
  InputFileType(final String commonName) {
    this.commonName = commonName;
  }

  /**
   * @return the commonName
   * @see #commonName
   */
  public String getCommonName() {
    return commonName;
  }
}
