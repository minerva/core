package lcsb.mapviewer.converter.zip;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.model.SubmodelType;

public class ZipEntryFileDeserializer extends StdDeserializer<ZipEntryFile> {

  public static enum ZipEntryFileType {
    MAP,
    OVERLAY,
    IMAGE,
    GLYPH;
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public ZipEntryFileDeserializer() {
    this(null);
  }

  public ZipEntryFileDeserializer(final Class<?> vc) {
    super(vc);
  }

  @Override
  public ZipEntryFile deserialize(final JsonParser parser, final DeserializationContext ctxt) throws IOException, JsonProcessingException {
    ObjectMapper mapper = (ObjectMapper) parser.getCodec();
    ObjectNode rootNode = mapper.readTree(parser);

    String filename = rootNode.get("_filename").asText();
    String name = getValue(rootNode, "_data.name", "");

    switch (ZipEntryFileType.valueOf(getValue(rootNode,"_type",null))) {
      case MAP: {
        String submodelTypeKey = getValue(rootNode, "_data.type.id", SubmodelType.UNKNOWN.name());
        String rootKey = getValue(rootNode, "_data.root", "false");
        String mappingKey = getValue(rootNode, "_data.mapping", "false");

        SubmodelType mapType = SubmodelType.valueOf(submodelTypeKey);

        Boolean root = "true".equalsIgnoreCase(rootKey);
        Boolean mapping = "true".equalsIgnoreCase(mappingKey);

        return new ModelZipEntryFile(filename, name, root, mapping, mapType);
      }
      case OVERLAY: {
        String description = getValue(rootNode, "_data.description", "");
        if (name.trim().isEmpty()) {
          throw new DeserializationException("overlay name cannot be empty");
        }
        return new LayoutZipEntryFile(filename, name, description);
      }
      case IMAGE: {
        return new ImageZipEntryFile(filename);
      }
      case GLYPH: {
        return new GlyphZipEntryFile(filename);
      }
      default:
        throw new NotImplementedException("Unknown type: " + rootNode.get("_type"));
    }
  }

  private String getValue(final JsonNode rootNode, final String path, final String defaultValue) {
    JsonNode node = rootNode;
    for (final String segment : path.split("\\.")) {
      node = node.get(segment);
      if (node == null) {
        return defaultValue;
      }
    }
    return node.asText(defaultValue);
  }
}
