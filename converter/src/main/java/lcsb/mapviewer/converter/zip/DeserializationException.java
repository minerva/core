package lcsb.mapviewer.converter.zip;

import com.fasterxml.jackson.core.JsonProcessingException;

public class DeserializationException extends JsonProcessingException {

  protected DeserializationException(final String msg) {
    super(msg);
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

}
