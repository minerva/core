package lcsb.mapviewer.converter.zip;

import java.io.IOException;
import java.io.Serializable;
import java.util.Objects;

/**
 * Structure used to describe a file in a zip archive with single entry about
 * {@link lcsb.mapviewer.model.map.OverviewImage}.
 * 
 * @author Piotr Gawron
 * 
 */
public class ImageZipEntryFile extends ZipEntryFile implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public ImageZipEntryFile() {

  }

  /**
   * Default constructor.
   * 
   * @param filename
   *          {@link ZipEntryFile#filename}
   * @see #baos
   * @throws IOException
   *           thrown when there is a problem with accessing input stream
   */
  public ImageZipEntryFile(final String filename) {
    super(filename);
  }

  @Override
  public boolean equals(final Object arg) {
    if (!(arg instanceof ImageZipEntryFile)) {
      return false;
    }
    ImageZipEntryFile entryFile = (ImageZipEntryFile) arg;
    return Objects.equals(getFilename(), entryFile.getFilename());
  }

}
