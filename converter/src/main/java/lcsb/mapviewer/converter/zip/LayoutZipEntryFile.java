package lcsb.mapviewer.converter.zip;

import java.io.Serializable;
import java.util.Objects;

/**
 * Structure used to describe a file in a zip archive with single entry about
 * {@link lcsb.mapviewer.model.map.layout.Layout Layout}.
 * 
 * @author Piotr Gawron
 * 
 */
public class LayoutZipEntryFile extends ZipEntryFile implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Name of the layout.
   */
  private String name = "";

  /**
   * Description of the layout.
   */
  private String description = "";

  /**
   * Default constructor.
   */
  public LayoutZipEntryFile() {

  }

  /**
   * Default constructor.
   * 
   * @param filename
   *          {@link ZipEntryFile#filename}
   */
  public LayoutZipEntryFile(final String filename, final String name, final String description) {
    super(filename);
    this.name = name;
    this.description = description;
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * @return the description
   * @see #description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description
   *          the description to set
   * @see #description
   */
  public void setDescription(final String description) {
    this.description = description;
  }

  @Override
  public boolean equals(final Object arg) {
    if (!(arg instanceof LayoutZipEntryFile)) {
      return false;
    }
    LayoutZipEntryFile entryFile = (LayoutZipEntryFile) arg;
    return Objects.equals(getFilename(), entryFile.getFilename())
        && Objects.equals(getName(), entryFile.getName())
        && Objects.equals(getDescription(), entryFile.getDescription());
  }

}
