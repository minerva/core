package lcsb.mapviewer.converter.zip;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;

import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.model.map.OverviewImageLink;
import lcsb.mapviewer.model.map.OverviewLink;
import lcsb.mapviewer.model.map.OverviewModelLink;
import lcsb.mapviewer.model.map.OverviewSearchLink;
import lcsb.mapviewer.model.map.model.ModelData;

public class OverviewLinkDeserializer extends StdDeserializer<OverviewLink> {

  public OverviewLinkDeserializer() {
    super(OverviewLink.class);
  }

  /**
  * 
  */
  private static final long serialVersionUID = 1L;

  @Override
  public OverviewLink deserialize(final JsonParser parser, final DeserializationContext ctxt) throws IOException, JsonProcessingException {
    ObjectMapper mapper = (ObjectMapper) parser.getCodec();
    ObjectNode rootNode = mapper.readTree(parser);
    OverviewLink result;
    if (rootNode.has("modelLinkFilename")) {
      result = new OverviewModelLink();
      ModelData model = new ModelData();
      String name = rootNode.get("modelLinkFilename").asText();
      if (name.lastIndexOf(".") > 0) {
        name = name.substring(0, name.lastIndexOf("."));
      }
      model.setName(name);
      ((OverviewModelLink) result).setLinkedModel(model);

      ((OverviewModelLink) result).setZoomLevel(rootNode.get("zoomLevel").asInt());
      ((OverviewModelLink) result).setxCoord(rootNode.get("modelPoint").get("x").asInt());
      ((OverviewModelLink) result).setyCoord(rootNode.get("modelPoint").get("y").asInt());
    } else if (rootNode.has("imageLinkFilename")) {
      result = new OverviewImageLink();
      OverviewImage image = new OverviewImage();
      image.setFilename(rootNode.get("imageLinkFilename").asText());
      ((OverviewImageLink) result).setLinkedOverviewImage(image);
    } else if (rootNode.has("query")) {
      result = new OverviewSearchLink();
      ((OverviewSearchLink) result).setQuery(rootNode.get("query").asText());
    } else {
      throw new DeserializationException("Cannot deserialize OverviewLink.");
    }
    List<Point2D.Double> polygon = mapper.readValue(mapper.treeAsTokens(rootNode.get("polygon")), new TypeReference<List<Point2D.Double>>() {
    });
    result.setPolygon(polygon);

    return result;
  }
}
