package lcsb.mapviewer.converter.zip;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.TextFileUtils;
import lcsb.mapviewer.common.exception.InvalidArgumentException;

/**
 * Factory class used to create {@link ZipEntryFile} objects.
 * 
 * @author Piotr Gawron
 * 
 */
public class ZipEntryFileFactory {
  /**
   * Name of the parameter in {@link LayoutZipEntryFile file describing layout}
   * corresponding to the {@link LayoutZipEntryFile#type type}.
   */
  public static final String LAYOUT_HEADER_PARAM_TYPE = "TYPE";
  /**
   * Name of the parameter in {@link LayoutZipEntryFile file describing layout}
   * corresponding to the {@link LayoutZipEntryFile#genomeType genome type}.
   */
  public static final String LAYOUT_HEADER_PARAM_GENOME_TYPE = "GENOME_TYPE";
  /**
   * Name of the parameter in {@link LayoutZipEntryFile file describing layout}
   * corresponding to the {@link LayoutZipEntryFile#genomeVersion genome version}.
   */
  public static final String LAYOUT_HEADER_PARAM_GENOME_VERSION = "GENOME_VERSION";
  /**
   * Directory in a zip file where information about submodels is stored. These
   * entries should be by default transformed into {@link ModelZipEntryFile}.
   */
  private static final String SUBMODEL_DIRECTORY = "submaps/";
  /**
   * Directory in a zip file where information about
   * {@link lcsb.mapviewer.model.map.OverviewImage OverviewImage} is stored. These
   * entries should be by default transformed into {@link ImageZipEntryFile}.
   */
  private static final String IMAGES_DIRECTORY = "images/";
  /**
   * Directory in a zip file where information about
   * {@link lcsb.mapviewer.model.map.layout.graphics.Glyph} is stored. These
   * entries should be by default transformed into {@link GlyphZipEntryFile}.
   */
  private static final String GLYPHS_DIRECTORY = "glyphs/";
  /**
   * Directory in a zip file where information about
   * {@link lcsb.mapviewer.model.map.layout.Layout Layout} is stored. These
   * entries should be by default transformed into {@link LayoutZipEntryFile}.
   */
  private static final String LAYOUT_DIRECTORY = "layouts/";
  /**
   * Name of the parameter in {@link LayoutZipEntryFile file describing layout}
   * corresponding to the {@link LayoutZipEntryFile#name layout name}.
   */
  private static final String LAYOUT_HEADER_PARAM_NAME = "NAME";
  /**
   * Name of the parameter in {@link LayoutZipEntryFile file describing layout}
   * corresponding to the {@link LayoutZipEntryFile#description layout
   * description}.
   */
  private static final String LAYOUT_HEADER_PARAM_DESCRIPTION = "DESCRIPTION";
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger();

  /**
   * Generates instance of {@link ZipEntryFile} representing entry in the zip file
   * with pre-parsed structural data (like: type, name, description, etc).
   * 
   * @param entry
   *          {@link ZipEntry entry} in the {@link ZipFile}
   * @param zipFile
   *          original {@link ZipFile}
   * @return {@link ZipEntryFile} for the given {@link ZipEntry}
   * @throws IOException
   *           thrown when there is a problem with accessing zip file
   */
  public ZipEntryFile createZipEntryFile(final ZipEntry entry, final ZipFile zipFile) throws IOException {
    if (entry.isDirectory()) {
      return null;
    } else {
      String directory = null;
      String[] tmp = entry.getName().split("/");
      String name = tmp[tmp.length - 1];
      if (tmp.length > 1) {
        directory = entry.getName().replace(name, "");
      }

      if (directory == null) {
        ModelZipEntryFile zesf = new ModelZipEntryFile();
        zesf.setFilename(entry.getName());
        zesf.setName(FilenameUtils.getBaseName(entry.getName()));
        zesf.setRoot(true);
        if ("xml".equalsIgnoreCase(FilenameUtils.getExtension(entry.getName()))) {
          zesf.setFileType(InputFileType.CELLDESIGNER);
        } else {
          throw new InvalidArgumentException("Don't know what to do with entry: " + entry.getName());
        }
        return zesf;
      } else if (directory.equals(SUBMODEL_DIRECTORY)) {
        ModelZipEntryFile zesf = new ModelZipEntryFile();
        zesf.setFilename(entry.getName());
        zesf.setName(FilenameUtils.getBaseName(entry.getName()));
        if ("mapping".equalsIgnoreCase(zesf.getName())) {
          zesf.setMapping("true");
        } else {
          zesf.setMapping("false");
        }
        if ("xml".equalsIgnoreCase(FilenameUtils.getExtension(entry.getName()))) {
          zesf.setFileType(InputFileType.CELLDESIGNER);
        } else {
          throw new InvalidArgumentException("Don't know what to do with entry: " + entry.getName());
        }
        return zesf;
      } else if (directory.equals(IMAGES_DIRECTORY)) {
        ImageZipEntryFile result = new ImageZipEntryFile(entry.getName());
        return result;
      } else if (directory.equals(GLYPHS_DIRECTORY)) {
        GlyphZipEntryFile result = new GlyphZipEntryFile(entry.getName());
        return result;
      } else if (directory.equals(LAYOUT_DIRECTORY)) {
        LayoutZipEntryFile result = createLayoutZipEntryFile(entry.getName(), zipFile.getInputStream(entry));
        return result;
      } else {
        throw new InvalidArgumentException("Don't know what to do with entry: " + entry.getName());
      }
    }
  }

  /**
   * Creates {@link LayoutZipEntryFile layout entry} from input stream and given
   * name of the layout.
   * 
   * @param name
   *          name of the layout
   * @param inputStream
   *          stream where data is stored
   * @return {@link LayoutZipEntryFile} processed from input data
   * @throws IOException
   *           thrown when there is problem with accessing input data
   */
  public LayoutZipEntryFile createLayoutZipEntryFile(final String name, final InputStream inputStream) throws IOException {
    LayoutZipEntryFile result = new LayoutZipEntryFile();
    result.setFilename(name);
    result.setName(FilenameUtils.getBaseName(name));
    for (Map.Entry<String, String> mapEntry : TextFileUtils.getHeaderParametersFromFile(inputStream).entrySet()) {
      result.setHeaderParameter(mapEntry.getKey(), mapEntry.getValue());
    }
    if (result.getHeaderParameter(LAYOUT_HEADER_PARAM_NAME) != null) {
      result.setName(result.getHeaderParameter(LAYOUT_HEADER_PARAM_NAME));
    }
    if (result.getHeaderParameter(LAYOUT_HEADER_PARAM_DESCRIPTION) != null) {
      result.setDescription(result.getHeaderParameter(LAYOUT_HEADER_PARAM_DESCRIPTION));
    }
    return result;
  }

}
