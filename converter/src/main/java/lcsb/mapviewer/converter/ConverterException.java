package lcsb.mapviewer.converter;

/**
 * Generic exception thrown when implementation of {@link Converter} encounter a
 * problem.
 * 
 * @author Piotr Gawron
 * 
 */
public class ConverterException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor with exception message.
   * 
   * @param message
   *          error message
   */
  public ConverterException(final String message) {
    super(message);
  }

  /**
   * Default constructor with super exception as a source.
   * 
   * @param e
   *          super exception
   */
  public ConverterException(final Exception e) {
    super(e.getMessage(), e);
  }

  /**
   * Default constructor - initializes instance variable to unknown.
   */

  public ConverterException() {
    super(); // call superclass constructor
  }

  /**
   * Default constructor.
   * 
   * @param message
   *          exception message
   * @param e
   *          super exception
   */
  public ConverterException(final String message, final Exception e) {
    super(message, e);
  }
}