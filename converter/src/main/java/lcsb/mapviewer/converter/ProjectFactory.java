package lcsb.mapviewer.converter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.zip.GlyphZipEntryFile;
import lcsb.mapviewer.converter.zip.ImageZipEntryFile;
import lcsb.mapviewer.converter.zip.LayoutZipEntryFile;
import lcsb.mapviewer.converter.zip.ZipEntryFile;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.species.Element;

public class ProjectFactory {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger();

  private ComplexZipConverter converter;

  private GlyphParser glyphParser = new GlyphParser();

  public ProjectFactory(final ComplexZipConverter converter) {
    this.converter = converter;
  }

  public Project create(final ComplexZipConverterParams params) throws InvalidInputDataExecption, ConverterException {
    return create(params, new Project());
  }

  public Project create(final ComplexZipConverterParams params, final Project project)
      throws InvalidInputDataExecption, ConverterException {
    try {
      Model model = converter.createModel(params);

      Set<Model> models = new HashSet<>();
      models.add(model);
      models.addAll(model.getSubmodels());

      project.addModel(model);
      for (final Model m : model.getSubmodels()) {
        project.addModel(m);
      }

      ZipFile zipFile = params.getZipFile();
      Enumeration<? extends ZipEntry> entries;

      entries = zipFile.entries();
      List<ImageZipEntryFile> imageEntries = new ArrayList<>();
      int overlayOrder = 1;
      while (entries.hasMoreElements()) {
        ZipEntry entry = entries.nextElement();
        if (!entry.isDirectory()) {
          ZipEntryFile zef = params.getEntry(entry.getName());
          if (zef instanceof ImageZipEntryFile) {
            imageEntries.add((ImageZipEntryFile) zef);
          } else if (zef instanceof GlyphZipEntryFile) {
            project.addGlyph(glyphParser.parseGlyph((GlyphZipEntryFile) zef, zipFile));
          } else if (zef instanceof LayoutZipEntryFile) {
            project.addDataOverlay(
                converter.layoutZipEntryFileToLayout(params, zipFile, entry, (LayoutZipEntryFile) zef, overlayOrder++));
          }
        }
      }
      if (imageEntries.size() > 0) {
        OverviewParser parser = new OverviewParser();
        project
            .addOverviewImages(parser.parseOverviewLinks(models, imageEntries, params.getVisualizationDir(), zipFile));
      }
      if (project.getGlyphs().size() > 0) {
        assignGlyphsToElements(project);
      }
      return project;
    } catch (final IOException e) {
      throw new InvalidArgumentException(e);
    } catch (final InvalidCoordinatesFile e) {
      throw new InvalidInputDataExecption("Invalid coordinates file. " + e.getMessage(), e);
    }
  }

  private void assignGlyphsToElements(final Project project) throws InvalidGlyphFile {
    Set<ModelData> models = new HashSet<>();
    models.addAll(project.getModels());

    for (final ModelData model : project.getModels()) {
      for (final ModelSubmodelConnection connection : model.getSubmodels()) {
        models.add(connection.getSubmodel());
      }
    }
    for (final ModelData model : models) {
      for (final Element element : model.getElements()) {
        Glyph glyph = extractGlyph(project, element.getNotes());
        if (glyph != null) {
          element.setNotes(removeGlyph(element.getNotes()));
          element.setGlyph(glyph);
        }
      }
      for (final Layer layer : model.getLayers()) {
        for (final LayerText text : layer.getTexts()) {
          Glyph glyph = extractGlyph(project, text.getNotes());
          if (glyph != null) {
            text.setNotes(removeGlyph(text.getNotes()));
            text.setGlyph(glyph);
          }
        }
      }
    }
  }

  String removeGlyph(final String notes) {
    String[] lines = notes.split("[\n\r]+");
    StringBuilder result = new StringBuilder("");
    for (final String line : lines) {
      if (!line.startsWith("Glyph:")) {
        result.append(line + "\n");
      }
    }
    return result.toString();
  }

  Glyph extractGlyph(final Project project, final String notes) throws InvalidGlyphFile {
    String[] lines = notes.split("[\n\r]+");
    for (final String line : lines) {
      if (line.startsWith("Glyph:")) {
        String glyphString = line.replace("Glyph:", "").trim().toLowerCase();
        for (final Glyph glyph : project.getGlyphs()) {
          if (glyph.getFile().getOriginalFileName().toLowerCase().equalsIgnoreCase(glyphString)) {
            return glyph;
          }
        }
        throw new InvalidGlyphFile("Glyph file doesn't exist: " + glyphString);
      }
    }
    return null;
  }

}
