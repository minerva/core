package lcsb.mapviewer.converter;

import java.util.HashSet;
import java.util.Set;

import lcsb.mapviewer.model.overlay.DataOverlayType;

/**
 * This enum defines which columns are available for defining
 * {@link lcsb.mapviewer.model.map.layout.ColorSchema}.
 * 
 * @author Piotr Gawron
 * 
 */
public enum ColorSchemaColumn {

  /**
   * Name of the element.
   */
  NAME(new DataOverlayType[] { DataOverlayType.GENERIC }),

  GENE_NAME(new DataOverlayType[] { DataOverlayType.GENETIC_VARIANT }),

  /**
   * Name of the map.
   */
  MAP_NAME(new DataOverlayType[] { DataOverlayType.GENERIC, DataOverlayType.GENETIC_VARIANT }),

  /**
   * Value that will be transformed into new color.
   * 
   * @see ColorSchemaColumn#COLOR
   */
  VALUE(new DataOverlayType[] { DataOverlayType.GENERIC }),

  /**
   * In which compartment the element should be located.
   */
  COMPARTMENT(new DataOverlayType[] { DataOverlayType.GENERIC, DataOverlayType.GENETIC_VARIANT }),

  /**
   * Class type of the element.
   */
  TYPE(new DataOverlayType[] { DataOverlayType.GENERIC }),

  /**
   * New element/reaction color.
   */
  COLOR(new DataOverlayType[] { DataOverlayType.GENERIC, DataOverlayType.GENETIC_VARIANT }),

  /**
   * Identifier of the element.
   */
  IDENTIFIER(new DataOverlayType[] { DataOverlayType.GENERIC, DataOverlayType.GENETIC_VARIANT }),

  /**
   * Element identifier.
   */
  ELEMENT_IDENTIFIER(new DataOverlayType[] { DataOverlayType.GENERIC }),

  /**
   * New line width of the reaction.
   */
  LINE_WIDTH(new DataOverlayType[] { DataOverlayType.GENERIC }),

  /**
   * Position where gene variants starts.
   */
  POSITION(new DataOverlayType[] { DataOverlayType.GENETIC_VARIANT }),

  /**
   * Original DNA of the variant.
   */
  ORIGINAL_DNA(new DataOverlayType[] { DataOverlayType.GENETIC_VARIANT }),

  /**
   * Alternative DNA of the variant.
   */
  ALTERNATIVE_DNA(new DataOverlayType[] { DataOverlayType.GENETIC_VARIANT }),

  /**
   * Short description of the entry.
   */
  DESCRIPTION(new DataOverlayType[] { DataOverlayType.GENETIC_VARIANT, DataOverlayType.GENERIC }),

  /**
   * Contig where variant was observed.
   */
  CONTIG(new DataOverlayType[] { DataOverlayType.GENETIC_VARIANT }),

  CHROMOSOME(new DataOverlayType[] { DataOverlayType.GENETIC_VARIANT }),

  ALLELE_FREQUENCY(new DataOverlayType[] { DataOverlayType.GENETIC_VARIANT }),

  VARIANT_IDENTIFIER(new DataOverlayType[] { DataOverlayType.GENETIC_VARIANT }),

  /**
   * Should the direction of reaction be reversed.
   */
  REVERSE_REACTION(new DataOverlayType[] { DataOverlayType.GENERIC }),

  /**
   * Optional amino acid change in the variant.
   */
  AMINO_ACID_CHANGE(new DataOverlayType[] { DataOverlayType.GENETIC_VARIANT });

  /**
   * Set of types where column is allowed.
   */
  private Set<DataOverlayType> types = new HashSet<>();

  /**
   * Default constructor that creates enum entry.
   *
   * @param types
   *          list of {@link DataOverlayType types} where this column is allowed
   */
  ColorSchemaColumn(final DataOverlayType[] types) {
    for (final DataOverlayType colorSchemaType : types) {
      this.types.add(colorSchemaType);
    }
  }

  public String getColumnName() {
    return this.name().toLowerCase();
  }

  /**
   * @return the types
   * @see #types
   */
  public Set<DataOverlayType> getTypes() {
    return types;
  }

}
