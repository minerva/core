package lcsb.mapviewer.converter;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.FileUtils;

import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.model.Model;

/**
 * Interface used for reading data from file and putting it into {@link Model}
 * object.
 * 
 * @author Piotr Gawron
 * 
 */
public abstract class Converter {

  /**
   * Parse input source and transforms it into a model object.
   * 
   * @param params
   *          input params used for reading data
   * @return model obtained from input source
   * @throws InvalidInputDataExecption
   *           thrown when input parameters are invalid
   */
  public abstract Model createModel(final ConverterParams params) throws InvalidInputDataExecption, ConverterException;

  /**
   * Generate String representation of the model. Traditionally often XML but can
   * be any other format as well.
   *
   * @param model
   *          The MINERVA ${@link Model} to be serialized
   * @return The translated Model
   */
  public abstract String model2String(final Model model) throws InconsistentModelException, ConverterException;

  /**
   * Returns a common name of data format used in the converter.
   *
   * @return common name of data format used in the converter
   */
  public abstract String getCommonName();

  /**
   * Returns {@link MimeType} of the exported data.
   *
   * @return {@link MimeType} of the exported data
   */
  public abstract MimeType getMimeType();

  /**
   * Returns list of extensions provided by the converter.
   *
   * @return list of extensions
   */
  public abstract List<String> getFileExtensions();

  /**
   * Export model to {@link InputStream}.
   * 
   * @param model
   *          model to be exported
   * @return {@link InputStream} with exported data
   * @throws InconsistentModelException
   *           thrown when given model is inconsistent and unable to be exported
   */
  public InputStream model2InputStream(final Model model)
      throws InconsistentModelException, ConverterException {
    String modelString = model2String(model);
    return new ByteArrayInputStream(modelString.getBytes(StandardCharsets.UTF_8));
  }

  /**
   * Export model to {@link File}.
   * 
   * @param model
   *          model to be exported
   * @param filePath
   *          exported file path
   * @return {@link File} with exported data
   * @throws InconsistentModelException
   *           thrown when given model is inconsistent and unable to be exported
   */
  public File model2File(final Model model, final String filePath)
      throws InconsistentModelException, IOException, ConverterException {
    String modelString = model2String(model);
    File file = new File(filePath);
    File parentFile = file.getParentFile();
    if (parentFile != null) {
      parentFile.mkdirs();
    }
    try (final PrintWriter out = new PrintWriter(file)) {
      out.println(modelString);
    }
    return new File(filePath);
  }

  public File inputStream2File(final InputStream inputStream) throws IOException {
    File tempFile = File.createTempFile(UUID.randomUUID().toString(), ".tmp");
    tempFile.deleteOnExit();
    FileUtils.copyInputStreamToFile(inputStream, tempFile);
    return tempFile;
  }

}
