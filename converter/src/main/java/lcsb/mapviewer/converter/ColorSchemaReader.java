package lcsb.mapviewer.converter;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.TextFileUtils;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.converter.zip.ZipEntryFileFactory;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.overlay.DataOverlayType;
import lcsb.mapviewer.model.overlay.GeneVariant;
import lcsb.mapviewer.model.overlay.GeneVariantDataOverlayEntry;
import lcsb.mapviewer.model.overlay.GenericDataOverlayEntry;
import lcsb.mapviewer.model.overlay.InvalidDataOverlayException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class that reads information about set of {@link DataOverlayEntry color
 * schemas} from the input file.
 *
 * @author Piotr Gawron
 */
public class ColorSchemaReader {

  /**
   * Defines number of gene variants per element that is a threshold for
   * changing color.
   */
  private static final int SATURATION_SIZE_OF_GENE_VARIANTS = 10;

  /**
   * Max value of red in a color.
   */
  private static final int MAX_RED_VALUE = 255;

  /**
   * Min value of red in a color when computing color for gene variants.
   */
  private static final int MIN_GV_RED_VALUE = 128;

  private static final Map<String, Class<? extends BioEntity>> speciesMapping;

  static {
    speciesMapping = new HashMap<>();
    speciesMapping.put("protein", Protein.class);
    speciesMapping.put("gene", Gene.class);
    speciesMapping.put("complex", Complex.class);
    speciesMapping.put("simple_molecule", SimpleMolecule.class);
    speciesMapping.put("ion", Ion.class);
    speciesMapping.put("phenotype", Phenotype.class);
    speciesMapping.put("drug", Drug.class);
    speciesMapping.put("rna", Rna.class);
    speciesMapping.put("antisense_rna", AntisenseRna.class);
    speciesMapping.put("unknown", Unknown.class);
    speciesMapping.put("degraded", Degraded.class);
  }

  /**
   * Object that parses colors from string.
   */
  private final ColorParser colorParser = new ColorParser();

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  /**
   * Reads information about set of {@link DataOverlayEntry color schemas} from
   * the input stream.
   *
   * @param colorInputStream input stream with {@link DataOverlayEntry}
   * @param params           list of parameters that were parsed from file header (lines
   *                         starting with '#')
   * @return list of coloring schemas
   * @throws IOException                 thrown when there is a problem with input stream
   * @throws InvalidDataOverlayException thrown when color schema is invalid
   */
  public Collection<DataOverlayEntry> readColorSchema(final InputStream colorInputStream, final Map<String, String> params)
      throws IOException, InvalidDataOverlayException {
    if (Integer.valueOf(params.get(TextFileUtils.COLUMN_COUNT_PARAM)) == 1) {
      return readSimpleNameColorSchema(colorInputStream);
    } else if (params.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE) == null) {
      return readGenericColorSchema(colorInputStream);
    } else {
      DataOverlayType type = null;
      try {
        type = DataOverlayType.valueOf(params.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE));
      } catch (final IllegalArgumentException e) {
        final String options = StringUtils.join(DataOverlayType.values(), ", ");
        throw new InvalidDataOverlayException("Invalid overlay type: "
            + params.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE) + ". Allowed options: " + options);
      }
      if (type == null) {
        logger.warn("Unknown type of layout file: " + params.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE));
        return readGenericColorSchema(colorInputStream);
      }
      switch (type) {
        case GENERIC:
          return readGenericColorSchema(colorInputStream);
        case GENETIC_VARIANT:
          return readGeneticVariantColorSchema(colorInputStream, params);
        default:
          logger.warn("Data overlay type not implemented: " + type);
          return readGenericColorSchema(colorInputStream);
      }
    }
  }

  /**
   * Reads information about set of {@link DataOverlayEntry color schemas} from
   * the filename.
   *
   * @param filename file with {@link DataOverlayEntry}
   * @return list of coloring schemas
   * @throws IOException                 thrown when there is a problem with input stream
   * @throws InvalidDataOverlayException thrown when color schema is invalid
   */
  public Collection<DataOverlayEntry> readColorSchema(final String filename, final DataOverlayType colorSchemaType)
      throws IOException, InvalidDataOverlayException {
    final Map<String, String> params = TextFileUtils.getHeaderParametersFromFile(new FileInputStream(filename));
    if (colorSchemaType != null) {
      params.put(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE, colorSchemaType.name());
    }

    return readColorSchema(new FileInputStream(filename), params);
  }

  /**
   * Reads information about set of {@link DataOverlayEntry color schemas} from
   * the input stream represented as byte array.
   *
   * @param inputData source in form of byte array
   * @return list of coloring schemas
   * @throws IOException                 thrown when there is a problem with input stream
   * @throws InvalidDataOverlayException thrown when color schema is invalid
   */
  public Collection<DataOverlayEntry> readColorSchema(final byte[] inputData, final DataOverlayType colorSchemaType)
      throws IOException, InvalidDataOverlayException {
    final Map<String, String> params = TextFileUtils.getHeaderParametersFromFile(new ByteArrayInputStream(inputData));
    if (colorSchemaType != null && params.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE) == null) {
      params.put(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE, colorSchemaType.name());
    }
    return readColorSchema(new ByteArrayInputStream(inputData), params);
  }

  Collection<DataOverlayEntry> readColorSchema(final String filename)
      throws IOException, InvalidDataOverlayException {
    return readColorSchema(filename, null);
  }

  /**
   * Reads information about set of {@link GeneVariantDataOverlayEntry gene
   * variant color schemas} from the input stream.
   *
   * @param colorInputStream input stream with {@link GeneVariantDataOverlayEntry}
   * @param params           list of parameters that were parsed from file header (lines
   *                         starting with '#')
   * @return list of coloring schemas
   * @throws IOException                 thrown when there is a problem with input stream
   * @throws InvalidDataOverlayException thrown when color schema is invalid
   */
  protected Collection<DataOverlayEntry> readGeneticVariantColorSchema(final InputStream colorInputStream,
                                                                       final Map<String, String> params)
      throws IOException, InvalidDataOverlayException {
    final List<DataOverlayEntry> result = new ArrayList<>();
    final BufferedReader br = new BufferedReader(new InputStreamReader(colorInputStream));
    try {
      String line = br.readLine();
      int lineIndex = 1;
      while (line != null && line.startsWith("#")) {
        lineIndex++;
        line = br.readLine();
      }
      final String[] columns = line.split("\t");

      final Map<ColorSchemaColumn, Integer> schemaColumns = new HashMap<>();
      final List<Pair<MiriamType, Integer>> customIdentifiers = parseColumns(columns, schemaColumns,
          DataOverlayType.GENETIC_VARIANT);
      final Integer colorColumn = schemaColumns.get(ColorSchemaColumn.COLOR);
      Integer contigColumn = schemaColumns.get(ColorSchemaColumn.CONTIG);
      if (contigColumn == null) {
        contigColumn = schemaColumns.get(ColorSchemaColumn.CHROMOSOME);
      }
      Integer nameColumn = schemaColumns.get(ColorSchemaColumn.NAME);
      if (nameColumn == null) {
        nameColumn = schemaColumns.get(ColorSchemaColumn.GENE_NAME);
      } else if (schemaColumns.get(ColorSchemaColumn.GENE_NAME) != null) {
        throw new InvalidDataOverlayException(ColorSchemaColumn.GENE_NAME + " and " + ColorSchemaColumn.NAME
            + " column cannot appear in the same dataset");
      }
      final Integer modelNameColumn = schemaColumns.get(ColorSchemaColumn.MAP_NAME);
      final Integer identifierColumn = schemaColumns.get(ColorSchemaColumn.IDENTIFIER);
      final Integer variantIdentifierColumn = schemaColumns.get(ColorSchemaColumn.VARIANT_IDENTIFIER);
      final Integer allelFrequencyColumn = schemaColumns.get(ColorSchemaColumn.ALLELE_FREQUENCY);
      final Integer compartmentColumn = schemaColumns.get(ColorSchemaColumn.COMPARTMENT);
      final Integer typeColumn = schemaColumns.get(ColorSchemaColumn.TYPE);
      final Integer positionColumn = schemaColumns.get(ColorSchemaColumn.POSITION);
      final Integer originalDnaColumn = schemaColumns.get(ColorSchemaColumn.ORIGINAL_DNA);
      final Integer alternativeDnaColumn = schemaColumns.get(ColorSchemaColumn.ALTERNATIVE_DNA);
      final Integer descriptionColumn = schemaColumns.get(ColorSchemaColumn.DESCRIPTION);
      final Integer aminoAcidChangeColumn = schemaColumns.get(ColorSchemaColumn.AMINO_ACID_CHANGE);
      if (nameColumn == null && identifierColumn == null && customIdentifiers.size() == 0) {
        throw new InvalidDataOverlayException("One of these columns is obligatory: name, identifier");
      }
      if (contigColumn == null) {
        throw new InvalidDataOverlayException(ColorSchemaColumn.CONTIG.getColumnName() + " column is obligatory");
      }
      if (positionColumn == null) {
        throw new InvalidDataOverlayException(ColorSchemaColumn.POSITION.getColumnName() + " column is obligatory");
      }
      if (originalDnaColumn == null) {
        throw new InvalidDataOverlayException(ColorSchemaColumn.ORIGINAL_DNA.getColumnName() + " column is obligatory");
      }
      lineIndex++;
      line = br.readLine();
      while (line != null) {
        lineIndex++;
        final String errorPrefix = "[Line " + lineIndex + "]\t";
        if (!line.trim().equals("")) {
          final String[] values = line.split("\t", -1);
          if (values.length != columns.length) {
            throw new InvalidDataOverlayException("[Line " + lineIndex + "] Wrong number of cells");
          }
          final GeneVariantDataOverlayEntry schema = new GeneVariantDataOverlayEntry();
          if (nameColumn != null) {
            processNameColumn(schema, values[nameColumn]);
          }
          if (modelNameColumn != null) {
            processModelNameColumn(schema, values[modelNameColumn]);
          }
          if (compartmentColumn != null) {
            processCompartmentColumn(schema, values[compartmentColumn]);
          }
          if (typeColumn != null) {
            final String[] types = values[typeColumn].split(",");
            for (final String string : types) {
              final Class<? extends BioEntity> clazz = speciesMapping.get(string.toLowerCase());
              if (clazz != null) {
                schema.addType(clazz);
              } else {
                throw new InvalidDataOverlayException("[Line " + lineIndex + "] Unknown class type: " + string + ".");
              }
            }
          }
          if (colorColumn != null) {
            try {
              schema.setColor(colorParser.parse(values[colorColumn]));
            } catch (final InvalidArgumentException e) {
              throw new InvalidDataOverlayException(errorPrefix + e.getMessage(), e);
            }
          }
          if (identifierColumn != null && !values[identifierColumn].equals("")) {
            processGeneralIdentifier(values[identifierColumn], schema, errorPrefix);
          }
          if (descriptionColumn != null) {
            schema.setDescription(values[descriptionColumn]);
          }
          for (final Pair<MiriamType, Integer> pair : customIdentifiers) {
            processIdentifier(values[pair.getRight()], pair.getLeft(), schema);
          }

          final GeneVariant gv = new GeneVariant();
          if (positionColumn != null) {
            try {
              gv.setPosition(Long.parseLong(values[positionColumn]));
            } catch (final NumberFormatException e) {
              throw new InvalidDataOverlayException(
                  "[Line " + lineIndex + "] Invalid position: " + values[positionColumn], e);
            }
          }
          if (originalDnaColumn != null) {
            gv.setOriginalDna(values[originalDnaColumn]);
          }
          if (allelFrequencyColumn != null) {
            final String value = values[allelFrequencyColumn];
            if (value != null && !value.trim().isEmpty()) {
              gv.setAllelFrequency(Double.parseDouble(value));
            }
          }
          if (variantIdentifierColumn != null) {
            gv.setVariantIdentifier(values[variantIdentifierColumn]);
          }
          if (alternativeDnaColumn != null) {
            gv.setModifiedDna(values[alternativeDnaColumn]);
          }
          if (aminoAcidChangeColumn != null) {
            gv.setAminoAcidChange(values[aminoAcidChangeColumn]);
          }
          gv.setContig(values[contigColumn]);

          schema.addGeneVariant(gv);
          if (schema.getName() != null && schema.getName().contains(";")) {
            final String[] names = schema.getName().split(";");
            for (final String string : names) {
              if (!string.trim().isEmpty()) {
                final GeneVariantDataOverlayEntry schemaCopy = schema.copy();
                schemaCopy.setName(string);
                result.add(schemaCopy);
              }
            }
          } else {
            result.add(schema);
          }
        }
        line = br.readLine();
      }
    } finally {
      br.close();
    }
    return mergeSchemas(result);
  }

  private void processGeneralIdentifier(final String string, final DataOverlayEntry schema, final String errorPrefix)
      throws InvalidDataOverlayException {
    try {
      schema.addMiriamData(MiriamType.getMiriamDataFromIdentifier(string));
    } catch (final InvalidArgumentException e) {
      throw new InvalidDataOverlayException(errorPrefix + " Invalid identifier: " + string, e);
    }
  }

  /**
   * Sets proper value of identifier to {@link DataOverlayEntry} from cell
   * content.
   *
   * @param schema  {@link DataOverlayEntry} where name should be set
   * @param type    {@link MiriamType} type of the identifier
   * @param content content of the cell where identifier of given type is stored
   */
  private void processIdentifier(final String content, final MiriamType type, final DataOverlayEntry schema) {
    if (!content.isEmpty()) {
      schema.addMiriamData(new MiriamData(type, content));
    }
  }

  /**
   * Sets proper name to {@link DataOverlayEntry} from cell content.
   *
   * @param schema  {@link DataOverlayEntry} where name should be set
   * @param content content of the cell where name is stored
   */
  private void processNameColumn(final DataOverlayEntry schema, final String content) {
    if (!content.isEmpty()) {
      schema.setName(content);
    }
  }

  private void processModelNameColumn(final DataOverlayEntry schema, final String content) {
    if (!content.isEmpty()) {
      schema.setModelName(content);
    }
  }

  /**
   * Sets proper compartment names to {@link DataOverlayEntry} from cell
   * content.
   *
   * @param schema  {@link DataOverlayEntry} where name should be set
   * @param content content of the cell where compartments are stored
   */
  private void processCompartmentColumn(final DataOverlayEntry schema, final String content) {
    for (final String string : content.split(",")) {
      if (!string.isEmpty()) {
        schema.addCompartment(string);
      }
    }
  }

  /**
   * Transforms string into {@link ReferenceGenomeType}.
   *
   * @param referenceGenomeStr type as a string
   * @return {@link ReferenceGenomeType} obtained from input string
   * @throws InvalidDataOverlayException thrown when input string cannot be resolved into
   *                                     {@link ReferenceGenomeType}
   */
  protected ReferenceGenomeType extractReferenceGenomeType(final String referenceGenomeStr)
      throws InvalidDataOverlayException {
    ReferenceGenomeType genomeType = null;
    if (referenceGenomeStr != null) {
      try {
        genomeType = ReferenceGenomeType.valueOf(referenceGenomeStr);
      } catch (final IllegalArgumentException e) {
        throw new InvalidDataOverlayException("Unknown genome type: " + referenceGenomeStr + ". Acceptable values: "
            + StringUtils.join(ReferenceGenomeType.values(), ","), e);
      }
    }
    return genomeType;
  }

  /**
   * Merges collection of {@link DataOverlayEntry} that might contain duplicate
   * names into collection that doesn't have duplicate names.
   *
   * @param schemas {@link Collection} of {@link DataOverlayEntry} that might contain
   *                duplicate name
   * @return {@link Collection} of {@link DataOverlayEntry} that doesn't contain duplicate names
   */
  private Collection<DataOverlayEntry> mergeSchemas(final Collection<DataOverlayEntry> schemas) {
    final Map<String, DataOverlayEntry> schemasByName = new HashMap<>();
    for (final DataOverlayEntry colorSchema : schemas) {
      DataOverlayEntry mergedSchema = schemasByName.get(getColorSchemaIdentifiablePart(colorSchema));
      if (mergedSchema == null) {
        mergedSchema = colorSchema.copy();
        schemasByName.put(getColorSchemaIdentifiablePart(colorSchema), mergedSchema);
      } else {
        if (mergedSchema instanceof GeneVariantDataOverlayEntry) {
          if (colorSchema instanceof GeneVariantDataOverlayEntry) {
            ((GeneVariantDataOverlayEntry) mergedSchema)
                .addGeneVariants(((GeneVariantDataOverlayEntry) colorSchema).getGeneVariants());
          } else {
            throw new NotImplementedException(
                "Merge between classes not imeplemented:" + mergedSchema.getClass() + "," + colorSchema.getClass());
          }
        } else {
          throw new NotImplementedException(
              "Merge between classes not imeplemented:" + mergedSchema.getClass() + "," + colorSchema.getClass());
        }
      }
    }
    for (final DataOverlayEntry colorSchema : schemasByName.values()) {
      if (colorSchema instanceof GeneVariantDataOverlayEntry && colorSchema.getColor() == null) {
        colorSchema.setColor(getGeneVariantsColor(((GeneVariantDataOverlayEntry) colorSchema).getGeneVariants()));
      }
    }
    return schemasByName.values();
  }

  private String getColorSchemaIdentifiablePart(final DataOverlayEntry colorSchema) {
    final List<String> identifiers = new ArrayList<>();
    for (final MiriamData md : colorSchema.getMiriamData()) {
      identifiers.add(md.toString());
    }
    Collections.sort(identifiers);

    return StringUtils.join(identifiers, "\n") + "\n" + colorSchema.getName();
  }

  /**
   * Gets color that should be assigned to {@link GeneVariantDataOverlayEntry}.
   *
   * @param geneVariations list of variants
   * @return {@link Color} that should be assigned to {@link GeneVariantDataOverlayEntry}
   */
  private Color getGeneVariantsColor(final Collection<GeneVariant> geneVariations) {
    final int size = geneVariations.size();
    if (size >= SATURATION_SIZE_OF_GENE_VARIANTS) {
      return Color.RED;
    } else {
      final double ratio = (double) size / (double) SATURATION_SIZE_OF_GENE_VARIANTS;
      return new Color((int) (MIN_GV_RED_VALUE + ratio * (MAX_RED_VALUE - MIN_GV_RED_VALUE)), 0, 0);
    }
  }

  /**
   * Reads information about set of {@link GenericDataOverlayEntry generic color
   * schemas} from the input stream.
   *
   * @param colorInputStream input stream with {@link GenericDataOverlayEntry}
   * @return list of coloring schemas
   * @throws IOException                 thrown when there is a problem with input stream
   * @throws InvalidDataOverlayException thrown when color schema is invalid
   */
  protected Collection<DataOverlayEntry> readGenericColorSchema(final InputStream colorInputStream)
      throws IOException, InvalidDataOverlayException {
    final List<DataOverlayEntry> result = new ArrayList<>();

    final BufferedReader br = new BufferedReader(new InputStreamReader(colorInputStream));

    try {
      String line = br.readLine();
      int lineIndex = 1;
      while (line != null && line.startsWith("#")) {
        lineIndex++;
        line = br.readLine();
      }
      if (line == null) {
        throw new InvalidDataOverlayException("File does not contain any data");
      }
      final String[] columns = line.split("\t");

      final Map<ColorSchemaColumn, Integer> schemaColumns = new HashMap<>();
      final List<Pair<MiriamType, Integer>> customIdentifiers = parseColumns(columns, schemaColumns, DataOverlayType.GENERIC);

      final Integer valueColumn = schemaColumns.get(ColorSchemaColumn.VALUE);
      final Integer colorColumn = schemaColumns.get(ColorSchemaColumn.COLOR);
      final Integer nameColumn = schemaColumns.get(ColorSchemaColumn.NAME);
      final Integer modelNameColumn = schemaColumns.get(ColorSchemaColumn.MAP_NAME);
      final Integer identifierColumn = schemaColumns.get(ColorSchemaColumn.IDENTIFIER);
      final Integer elementIdentifierColumn = schemaColumns.get(ColorSchemaColumn.ELEMENT_IDENTIFIER);
      final Integer compartmentColumn = schemaColumns.get(ColorSchemaColumn.COMPARTMENT);
      final Integer typeColumn = schemaColumns.get(ColorSchemaColumn.TYPE);
      final Integer lineWidthColumn = schemaColumns.get(ColorSchemaColumn.LINE_WIDTH);
      final Integer reverseReactionColumn = schemaColumns.get(ColorSchemaColumn.REVERSE_REACTION);
      final Integer descriptionColumn = schemaColumns.get(ColorSchemaColumn.DESCRIPTION);

      if (nameColumn == null && identifierColumn == null && customIdentifiers.size() == 0
          && elementIdentifierColumn == null) {
        throw new InvalidDataOverlayException(
            "One of these columns is obligatory: " + ColorSchemaColumn.NAME.getColumnName()
                + "," + ColorSchemaColumn.IDENTIFIER.getColumnName() + ","
                + ColorSchemaColumn.ELEMENT_IDENTIFIER.getColumnName());
      }

      if (valueColumn == null && colorColumn == null) {
        throw new InvalidDataOverlayException("Schema must contain one of these two columns: value, color");
      }

      lineIndex++;
      line = br.readLine();
      while (line != null) {
        final String errorPrefix = "[Line " + lineIndex + "]\t";
        lineIndex++;

        if (line.trim().equals("")) {
          line = br.readLine();
          continue;
        }
        final String[] values = line.split("\t", -1);
        if (values.length != columns.length) {
          throw new InvalidDataOverlayException(errorPrefix + "Wrong number of cells");
        }
        final DataOverlayEntry schema = new GenericDataOverlayEntry();
        if (nameColumn != null) {
          processNameColumn(schema, values[nameColumn]);
        }
        if (modelNameColumn != null) {
          processModelNameColumn(schema, values[modelNameColumn]);
        }
        if (valueColumn != null) {
          schema.setValue(parseValueColumn(values[valueColumn], errorPrefix));
        }
        if (colorColumn != null && !values[colorColumn].isEmpty()) {
          try {
            schema.setColor(colorParser.parse(values[colorColumn]));
          } catch (final InvalidArgumentException e) {
            throw new InvalidDataOverlayException(errorPrefix + e.getMessage(), e);
          }
        }
        if (schema.getValue() != null && schema.getColor() != null) {
          throw new InvalidDataOverlayException(errorPrefix + "Either color or value can be defined but found both");
        }
        if (compartmentColumn != null) {
          processCompartmentColumn(schema, values[compartmentColumn]);
        }
        if (typeColumn != null) {
          schema.addTypes(parseSpeciesTypes(values[typeColumn], errorPrefix));
        }
        if (descriptionColumn != null) {
          schema.setDescription(values[descriptionColumn]);
        }
        if (elementIdentifierColumn != null) {
          processReactionIdentifier(schema, values[elementIdentifierColumn]);
        }
        if (lineWidthColumn != null) {
          if (!values[lineWidthColumn].trim().equals("")) {
            try {
              schema.setLineWidth(Double.parseDouble(values[lineWidthColumn].replace(",", ".")));
            } catch (final NumberFormatException e) {
              throw new InvalidDataOverlayException(
                  errorPrefix + "Problem with parsing value: \"" + values[lineWidthColumn] + "\"");
            }
          }
        }
        if (reverseReactionColumn != null) {
          schema.setReverseReaction("true".equalsIgnoreCase(values[reverseReactionColumn]));
        }
        if (identifierColumn != null && !values[identifierColumn].equals("")) {
          processGeneralIdentifier(values[identifierColumn], schema, errorPrefix);
        }
        for (final Pair<MiriamType, Integer> pair : customIdentifiers) {
          processIdentifier(values[pair.getRight()], pair.getLeft(), schema);
        }
        result.add(schema);
        line = br.readLine();
      }
    } finally {
      br.close();
    }
    return result;
  }

  List<Class<? extends BioEntity>> parseSpeciesTypes(final String typesString, final String errorPrefix)
      throws InvalidDataOverlayException {
    final List<Class<? extends BioEntity>> result = new ArrayList<>();
    final String[] types = typesString.split(",");
    for (final String string : types) {
      if (!string.isEmpty()) {
        final Class<? extends BioEntity> clazz = speciesMapping.get(string.toLowerCase());
        if (clazz != null) {
          result.add(clazz);
        } else {
          throw new InvalidDataOverlayException(
              errorPrefix + "Unknown class type: " + string + ". Valid values are: "
                  + StringUtils.join(speciesMapping.keySet(), ","));
        }
      }
    }
    return result;
  }

  private Double parseValueColumn(final String string, final String errorPrefix) throws InvalidDataOverlayException {
    Double result = null;
    if (!string.isEmpty()) {
      try {
        result = Double.parseDouble(string.replace(",", "."));
      } catch (final NumberFormatException e) {
        throw new InvalidDataOverlayException(errorPrefix + "Problem with parsing value: \"" + string + "\"");
      }
      if (result > 1 + Configuration.EPSILON || result < -1 - Configuration.EPSILON) {
        throw new InvalidDataOverlayException(
            errorPrefix + "Value " + result + " out of range. Only values between -1 and 1 are allowed.");
      }
    }
    return result;
  }

  protected Collection<DataOverlayEntry> readSimpleNameColorSchema(final InputStream colorInputStream)
      throws IOException, InvalidDataOverlayException {
    final List<DataOverlayEntry> result = new ArrayList<>();

    final BufferedReader br = new BufferedReader(new InputStreamReader(colorInputStream));

    try {
      String line = br.readLine();
      while (line != null && line.startsWith("#")) {
        line = br.readLine();
      }

      while (line != null) {
        if (line.trim().equals("")) {
          line = br.readLine();
          continue;
        }
        final String[] values = line.split("\t", -1);
        final DataOverlayEntry schema = new GenericDataOverlayEntry();
        processNameColumn(schema, values[0]);
        schema.setDescription("");
        result.add(schema);
        line = br.readLine();
      }
    } finally {
      br.close();
    }
    return result;
  }

  /**
   * Sets proper reaction identifier to {@link DataOverlayEntry} from cell
   * content.
   *
   * @param schema  {@link DataOverlayEntry} where name should be set
   * @param content content of the cell where reaction identifiers stored
   */

  private void processReactionIdentifier(final DataOverlayEntry schema, final String content) {
    if (!content.isEmpty()) {
      schema.setElementId(content);
    }
  }

  /**
   * Transform headers of columns into map with {@link ColorSchemaColumn column
   * types} to column number.
   *
   * @param columns       headers of columns
   * @param schemaColumns map with {@link ColorSchemaColumn column types} to column number
   *                      where result will be returned
   * @param type          type of the color schema (for instance
   *                      {@link DataOverlayType#GENETIC_VARIANT gene variants})
   * @throws InvalidDataOverlayException thrown when the list of column headers contain invalid value
   */
  public List<Pair<MiriamType, Integer>> parseColumns(final String[] columns, final Map<ColorSchemaColumn, Integer> schemaColumns,
                                                      final DataOverlayType type) throws InvalidDataOverlayException {
    final List<Pair<MiriamType, Integer>> result = new ArrayList<>();
    final Map<String, MiriamType> acceptableIdentifiers = new HashMap<>();
    final Map<String, MiriamType> deprecatedIdentifiers = new HashMap<>();
    for (final MiriamType miriamType : MiriamType.values()) {
      acceptableIdentifiers.put("identifier_" + miriamType.name().toLowerCase(), miriamType);
      deprecatedIdentifiers.put(miriamType.getCommonName().toLowerCase(), miriamType);
    }

    for (int i = 0; i < columns.length; i++) {
      boolean found = false;
      for (final ColorSchemaColumn schemaColumn : ColorSchemaColumn.values()) {
        if (columns[i].trim().equalsIgnoreCase(schemaColumn.getColumnName())
            && schemaColumn.getTypes().contains(type)) {
          schemaColumns.put(schemaColumn, i);
          found = true;
        }
      }
      if (!found) {
        if (acceptableIdentifiers.containsKey(columns[i].toLowerCase())) {
          result.add(new Pair<>(acceptableIdentifiers.get(columns[i].toLowerCase()), i));
        } else if (deprecatedIdentifiers.containsKey(columns[i].toLowerCase())) {
          result.add(new Pair<>(deprecatedIdentifiers.get(columns[i].toLowerCase()), i));
        } else {
          String columnNames = "";
          for (final ColorSchemaColumn schemaColumn : ColorSchemaColumn.values()) {
            if (schemaColumn.getTypes().contains(type)) {
              columnNames += schemaColumn.getColumnName() + ", ";
            }
          }
          for (final String string : acceptableIdentifiers.keySet()) {
            columnNames += string + ", ";
          }
          throw new InvalidDataOverlayException(
              "Unknown column type: " + columns[i] + ". Acceptable column name: " + columnNames);
        }
      }
    }
    return result;
  }

  /**
   * Returns list of columns that should be printed for given coloring schemas.
   *
   * @param schemas list of schemas
   * @return list of columns that should be printed (were set in the coloring schemas)
   */
  public Collection<ColorSchemaColumn> getSetColorSchemaColumns(final Collection<DataOverlayEntry> schemas) {
    final Set<ColorSchemaColumn> result = new HashSet<>();
    for (final DataOverlayEntry schema : schemas) {
      if (schema.getColor() != null) {
        result.add(ColorSchemaColumn.COLOR);
      }
      if (schema.getCompartments().size() > 0) {
        result.add(ColorSchemaColumn.COMPARTMENT);
      }
      if (schema.getMiriamData().size() > 0) {
        result.add(ColorSchemaColumn.IDENTIFIER);
      }
      if (schema.getLineWidth() != null) {
        result.add(ColorSchemaColumn.LINE_WIDTH);
      }
      if (getColorSchemaIdentifiablePart(schema) != null) {
        result.add(ColorSchemaColumn.NAME);
      }
      if (schema.getModelName() != null) {
        result.add(ColorSchemaColumn.MAP_NAME);
      }
      if (schema.getElementId() != null) {
        result.add(ColorSchemaColumn.ELEMENT_IDENTIFIER);
      }
      if (schema.getReverseReaction() != null) {
        result.add(ColorSchemaColumn.REVERSE_REACTION);
      }
      if (schema.getTypes().size() > 0) {
        result.add(ColorSchemaColumn.TYPE);
      }
      if (schema.getValue() != null) {
        result.add(ColorSchemaColumn.VALUE);
      }
    }
    return result;
  }

}
