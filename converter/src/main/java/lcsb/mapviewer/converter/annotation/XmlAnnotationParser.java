package lcsb.mapviewer.converter.annotation;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Author;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class allow to parse xml nodes responsible for species annotations in
 * CellDesigner.
 *
 * @author Piotr Gawron
 */
public class XmlAnnotationParser {

  /**
   * Default logger.
   */
  private static final Logger logger = LogManager.getLogger();

  private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  private final Set<MiriamRelationType> supportedRelationTypes = new HashSet<>();

  private boolean useIdentifiersOrgFormat = false;

  /**
   * Default constructor.
   */
  public XmlAnnotationParser() {
    this(new ArrayList<>());
  }

  public XmlAnnotationParser(final Collection<MiriamRelationType> supportedRelationTypes) {
    if (supportedRelationTypes == null || supportedRelationTypes.isEmpty()) {
      this.supportedRelationTypes.addAll(Arrays.asList(MiriamRelationType.values()));
    } else {
      this.supportedRelationTypes.addAll(supportedRelationTypes);
    }
  }

  public XmlAnnotationParser(final Collection<MiriamRelationType> supportedRelationTypes, final boolean useIdentifiersOrgFormat) {
    this(supportedRelationTypes);
    this.useIdentifiersOrgFormat = useIdentifiersOrgFormat;
  }

  /**
   * This method parse the xml string passed as an argument. All information
   * obtained by the method are stored in local variables: miriamDataSet,
   * speciesId and can be accessed later on.
   *
   * @param data - xml string to be parsed
   * @return collection of miriam objects that were obtained from the xml node
   * @throws InvalidXmlSchemaException thrown when there is a problem with xml
   */
  public Set<MiriamData> parse(final String data, final LogMarker marker) throws InvalidXmlSchemaException {
    // start from creating a DOM parser and parse the whole document
    Document doc = XmlParser.getXmlDocumentFromString(data);

    NodeList root = doc.getChildNodes();

    // process the whole schema
    return parseRdfNode(root, marker);
  }

  /**
   * Retrieves set of Miriam annotation from xml rdf node.
   *
   * @param root xml node list
   * @return set of miriam annotations.
   * @throws InvalidXmlSchemaException thrown when there is a problem with xml
   */
  private Set<MiriamData> parseRdfNode(final NodeList root, final LogMarker marker) throws InvalidXmlSchemaException {
    Node rdf = XmlParser.getNode("rdf:RDF", root);
    return parseRdfNode(rdf, marker);
  }

  /**
   * Retrieves set of Miriam annotation from xml rdf node.
   *
   * @param rdf xml node
   * @return set of miriam annotations.
   * @throws InvalidXmlSchemaException thrown when there is a problem with xml
   */
  public Set<MiriamData> parseRdfNode(final Node rdf, final LogMarker marker) throws InvalidXmlSchemaException {
    Set<MiriamData> miriamDataSet = new HashSet<>();
    if (rdf != null) {
      Node description = XmlParser.getNode("rdf:Description", rdf.getChildNodes());
      if (description != null) {
        NodeList list = description.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
          Node node = list.item(i);
          if (node.getNodeType() == Node.ELEMENT_NODE) {
            String relationTypeString = node.getNodeName();
            MiriamRelationType relationType = MiriamRelationType.getTypeByStringRepresentation(relationTypeString);
            if (relationType != null) {
              miriamDataSet.addAll(parseMiriamNode(node, marker));
            } else if (relationTypeString.equals("dc:creator")) {
              continue;
            } else if (relationTypeString.equals("dcterms:created")) {
              continue;
            } else if (relationTypeString.equals("dcterms:modified")) {
              continue;
            } else {
              logger.warn("RDF relation type is not supported: " + relationTypeString);
            }
          }
        }
      }
    } else {
      throw new InvalidXmlSchemaException("rdf:Rdf node not found");
    }
    return miriamDataSet;
  }

  public List<Author> getAuthorsFromRdf(final Node rdf) throws InvalidXmlSchemaException {
    List<Author> authors = new ArrayList<>();

    if (rdf != null) {
      Node description = XmlParser.getNode("rdf:Description", rdf.getChildNodes());
      if (description != null) {
        NodeList list = description.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
          Node node = list.item(i);
          if (node.getNodeType() == Node.ELEMENT_NODE) {
            String relationTypeString = node.getNodeName();
            if (relationTypeString.equals("dc:creator")) {
              Node bag = XmlParser.getNode("rdf:Bag", node.getChildNodes());
              if (bag == null) {
                throw new InvalidXmlSchemaException("No rdf:Bag node found");
              }
              NodeList bagChildren = bag.getChildNodes();
              List<Node> nodes = XmlParser.getNodes("rdf:li", bagChildren);
              for (final Node li : nodes) {
                Author author = parseAuthor(li);
                authors.add(author);
              }
            }
          }
        }
      }
    } else {
      throw new InvalidXmlSchemaException("rdf:Rdf node not found");
    }
    return authors;
  }

  public Calendar getCreateDateFromRdf(final Node rdf) throws InvalidXmlSchemaException {
    if (rdf != null) {
      Node description = XmlParser.getNode("rdf:Description", rdf.getChildNodes());
      if (description != null) {
        NodeList list = description.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
          Node node = list.item(i);
          if (node.getNodeType() == Node.ELEMENT_NODE) {
            String relationTypeString = node.getNodeName();
            if (relationTypeString.equals("dcterms:created")) {
              Node dateNode = XmlParser.getNode("dcterms:W3CDTF", node);
              if (dateNode != null) {
                return getDateFromNode(dateNode);
              }
            }
          }
        }
      }
    } else {
      throw new InvalidXmlSchemaException("rdf:Rdf node not found");
    }
    return null;
  }

  private Calendar getDateFromNode(final Node dateNode) {
    try {
      Date date = dateFormat.parse(dateNode.getTextContent().replaceAll("[T]", " "));
      Calendar result = Calendar.getInstance();
      result.setTime(date);
      return result;
    } catch (final ParseException | DOMException e) {
      logger.warn("Problem with parsing date: ", e);
    }
    return null;
  }

  private Author parseAuthor(final Node li) {
    NodeList list = li.getChildNodes();
    String firstName = null;
    String lastName = null;
    String organization = null;
    String email = null;
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equals("vCard:EMAIL")) {
          email = node.getTextContent().trim();
        } else if (node.getNodeName().equals("vCard:ORG")) {
          organization = node.getTextContent().trim();
        } else if (node.getNodeName().equals("vCard:N")) {
          Node familyNode = XmlParser.getNode("vCard:Family", node);
          if (familyNode != null) {
            lastName = familyNode.getTextContent().trim();
          }
          Node givenNode = XmlParser.getNode("vCard:Given", node);
          if (givenNode != null) {
            firstName = givenNode.getTextContent().trim();
          }
        } else {
          logger.warn("Unknown data node in vcard: " + node.getNodeName());
        }
      }
    }
    Author author = new Author(firstName, lastName);
    author.setEmail(email);
    author.setOrganisation(organization);
    return author;
  }

  /**
   * This method converts a xml node into MiriamData object (annotation of a
   * species).
   *
   * @param node - xml node that contains representation of a single annotation of
   *             a species
   * @return MiriamData object that represents annotation of a species
   * @throws InvalidXmlSchemaException thrown when there is a problem with xml
   */
  public Set<MiriamData> parseMiriamNode(final Node node, final LogMarker logMarker) throws InvalidXmlSchemaException {
    Set<MiriamData> result = new HashSet<>();
    NodeList list = node.getChildNodes();
    String relationTypeString = node.getNodeName();
    MiriamRelationType relationType = MiriamRelationType.getTypeByStringRepresentation(relationTypeString);
    Node bag = XmlParser.getNode("rdf:Bag", list);
    if (bag == null) {
      throw new InvalidXmlSchemaException("No rdf:Bag node found");
    }
    list = bag.getChildNodes();
    List<Node> nodes = XmlParser.getNodes("rdf:li", list);
    for (final Node li : nodes) {
      String dataTypeUri = XmlParser.getNodeAttr("rdf:resource", li);
      if (dataTypeUri == null || dataTypeUri.isEmpty()) {
        throw new InvalidXmlSchemaException("rdf:li does not have a rdf:resource attribute");
      }

      try {
        MiriamData md = MiriamType.getMiriamByUri(dataTypeUri);
        if (relationType == null) {
          logger.warn(logMarker,
              "Unknown relation type: " + relationTypeString + ". For miriam uri: " + dataTypeUri + ".");
        } else {
          if (!MiriamType.PUBMED.equals(md.getDataType())) {
            md.setRelationType(relationType);
          }
          result.add(md);
        }
      } catch (final InvalidArgumentException e) {
        logger.warn(logMarker, e.getMessage());
      }
    }
    return result;
  }

  /**
   * This method converts a set of MiriamData into xml string that can be put to
   * CellDesigner schema.
   *
   * @param data - a set of MiriamData to be converted.
   * @return xml string representation of the input data
   */
  public String dataSetToXmlString(final Collection<MiriamData> data, final String metaid) {
    return dataSetToXmlString(data, new ArrayList<>(), null, new ArrayList<>(), metaid);
  }

  /**
   * This method converts a set of MiriamData into xml string that can be put to
   * CellDesigner schema.
   *
   * @param data - a set of MiriamData to be converted.
   * @return xml string representation of the input data
   */
  public String dataSetToXmlString(final Collection<MiriamData> data, final Collection<Author> authors, final Calendar createDate,
                                   final List<Calendar> modificationDates, final String metaid) {
    StringBuilder result = new StringBuilder();
    result.append(
        "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" "
            + "xmlns:dc=\"http://purl.org/dc/elements/1.1/\" "
            + "xmlns:dcterms=\"http://purl.org/dc/terms/\" " + "xmlns:vCard=\"http://www.w3.org/2001/vcard-rdf/3.0#\" "
            + "xmlns:bqbiol=\"http://biomodels.net/biology-qualifiers/\" "
            + "xmlns:bqmodel=\"http://biomodels.net/model-qualifiers/\">\n");
    result.append("<rdf:Description rdf:about=\"#" + metaid + "\">\n");
    for (final MiriamData miriamData : data) {
      result.append(miriamDataToXmlString(miriamData));
    }
    for (final Author author : authors) {
      result.append(authorToXmlString(author));
    }
    if (createDate != null) {
      result.append("<dcterms:created rdf:parseType=\"Resource\">");
      result.append(dateToXmlString(createDate));
      result.append("</dcterms:created>\n");
    }
    result.append("<dcterms:modified rdf:parseType=\"Resource\">");
    for (final Calendar modificationDate : modificationDates) {
      result.append(dateToXmlString(modificationDate));
    }
    result.append("</dcterms:modified>\n");
    result.append("</rdf:Description>\n");
    result.append("</rdf:RDF>\n");
    return result.toString();
  }

  private Object dateToXmlString(final Calendar createDate) {
    return "<dcterms:W3CDTF>"
        + dateFormat.format(createDate.getTime()).replaceAll(" ", "T") + "Z"
        + "</dcterms:W3CDTF>\n";
  }

  private String authorToXmlString(final Author author) {
    StringBuilder result = new StringBuilder();
    result.append("<dc:creator>\n");
    result.append("<rdf:Bag>\n");
    result.append("<rdf:li rdf:parseType=\"Resource\">\n");
    result.append("<vCard:N rdf:parseType=\"Resource\">\n");
    if (author.getLastName() != null) {
      result.append("<vCard:Family>" + XmlParser.escapeXml(author.getLastName()) + "</vCard:Family>\n");
    }
    if (author.getFirstName() != null) {
      result.append("<vCard:Given>" + XmlParser.escapeXml(author.getFirstName()) + "</vCard:Given>\n");
    }
    result.append("</vCard:N>\n");

    if (author.getEmail() != null && !author.getEmail().trim().isEmpty()) {
      result.append("<vCard:EMAIL>" + XmlParser.escapeXml(author.getEmail()) + "</vCard:EMAIL>\n");
    }
    if (author.getOrganisation() != null && !author.getOrganisation().trim().isEmpty()) {
      result.append("<vCard:ORG rdf:parseType=\"Resource\">\n");
      result.append("<vCard:Orgname>" + XmlParser.escapeXml(author.getOrganisation()) + "</vCard:Orgname>\n");
      result.append("</vCard:ORG>\n");
    }

    result.append("</rdf:li>\n");
    result.append("</rdf:Bag>\n");
    result.append("</dc:creator>\n");
    return result.toString();
  }

  /**
   * This method converts a single MiriamData into xml string that can be put to
   * CellDesigner schema.
   *
   * @param data - a MiriamData to be converted.
   * @return xml string representation of the input data
   */
  public String miriamDataToXmlString(final MiriamData data) {
    StringBuilder result = new StringBuilder();
    MiriamRelationType relationType;
    if (supportedRelationTypes.contains(data.getRelationType())) {
      relationType = data.getRelationType();
    } else {
      relationType = supportedRelationTypes.iterator().next();
      logger.warn(data.getRelationType().getStringRepresentation() + " is not supported. Replacing with: "
          + relationType.getStringRepresentation());
    }
    result.append("<" + relationType.getStringRepresentation() + ">\n");
    result.append("<rdf:Bag>\n");
    String uri;
    if (useIdentifiersOrgFormat) {
      uri = getIdentifiersOrgUrl(data);
    } else {
      uri = data.getDataType().getUris().get(0) + ":" + data.getResource().replaceAll(":", "%3A");
    }
    result.append("<rdf:li rdf:resource=\"" + uri + "\"/>\n");
    result.append("</rdf:Bag>\n");
    result.append("</" + relationType.getStringRepresentation() + ">\n");

    return result.toString();
  }

  public List<Calendar> getModificationDatesFromRdf(final Node rdf) throws InvalidXmlSchemaException {
    List<Calendar> result = new ArrayList<>();

    if (rdf != null) {
      Node description = XmlParser.getNode("rdf:Description", rdf.getChildNodes());
      if (description != null) {
        NodeList list = description.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
          Node node = list.item(i);
          if (node.getNodeType() == Node.ELEMENT_NODE) {
            String relationTypeString = node.getNodeName();
            if (relationTypeString.equals("dcterms:modified")) {
              for (final Node dateNode : XmlParser.getNodes("dcterms:W3CDTF", node.getChildNodes())) {
                result.add(getDateFromNode(dateNode));
              }
            }
          }
        }
      } else {
        throw new InvalidXmlSchemaException("rdf:Description node not found");
      }
    } else {
      throw new InvalidXmlSchemaException("rdf:Rdf node not found");
    }

    return result;
  }

  public String getIdentifiersOrgUrl(final MiriamData miriamData) {
    String id;
    if (miriamData.getDataType().getNamespace().isEmpty()) {
      id = miriamData.getResource();
    } else {
      id = miriamData.getDataType().getNamespace() + ":" + miriamData.getResource();
    }
    return "https://identifiers.org/" + id;
  }

}
