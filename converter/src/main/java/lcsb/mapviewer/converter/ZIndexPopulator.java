package lcsb.mapviewer.converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationResidue;
import lcsb.mapviewer.model.map.species.field.StructuralState;

/**
 * This util class populate with z-index data if necessary.
 * 
 * @author Piotr Gawron
 *
 */
public class ZIndexPopulator {

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  private static DoubleComparator DOUBLE_COMPARATOR = new DoubleComparator(Configuration.EPSILON);
  private static StringComparator STRING_COMPARATOR = new StringComparator();

  private static Comparator<Drawable> COMPARATOR = new Comparator<Drawable>() {
    @Override
    public int compare(final Drawable o1, final Drawable o2) {
      if (o1 instanceof StructuralState) {
        if (o2 instanceof StructuralState) {
          return compareStructuralStates((StructuralState) o1, (StructuralState) o2);
        }
        return 1;
      }
      if (o2 instanceof StructuralState) {
        return -1;
      }

      if (o1 instanceof Reaction) {
        if (o2 instanceof Reaction) {
          return compareReactions((Reaction) o1, (Reaction) o2);
        }
        return 1;
      }
      if (o2 instanceof Reaction) {
        return -1;
      }

      if (o1 instanceof Element && o2 instanceof Element) {
        return compareElements((Element) o1, (Element) o2);
      }
      int result = -DOUBLE_COMPARATOR.compare(o1.getSize(), o2.getSize());
      if (result == 0) {
        result = -STRING_COMPARATOR.compare(o1.getElementId(), o2.getElementId());
      }
      return result;
    }

    private int compareElements(final Element e1, final Element e2) {
      if (isChildren(e1, e2)) {
        return -1;
      }
      if (isChildren(e2, e1)) {
        return 1;
      }
      int result = -DOUBLE_COMPARATOR.compare(e1.getSize(), e2.getSize());
      if (result == 0) {
        return STRING_COMPARATOR.compare(e1.getElementId(), e2.getElementId());
      }
      return result;
    }

    private int compareStructuralStates(final StructuralState o1, final StructuralState o2) {
      return -STRING_COMPARATOR.compare(o1.getSpecies().getElementId(), o2.getSpecies().getElementId());
    }

    private int compareReactions(final Reaction o1, final Reaction o2) {
      return STRING_COMPARATOR.compare(o1.getElementId(), o2.getElementId());
    }

    private boolean isChildren(final Element e1, final Element e2) {
      if (e1 instanceof Complex && e2 instanceof Species) {
        Complex parent = (Complex) e1;
        Species child = (Species) e2;
        return parent.getAllSimpleChildren().contains(child);
      }
      if (e1 instanceof Compartment) {
        Compartment parent = (Compartment) e1;
        return parent.getAllSubElements().contains(e2);
      }
      return false;
    }
  };

  /**
   * Populate all elements that don't have z-index with valid values. Elements
   * without z-index would have bigger z-values than elements with existing
   * z-values.
   */
  public void populateZIndex(final Model model) {
    Set<Drawable> drawables = model.getDrawables();

    populateZIndex(drawables);

  }

  public void populateZIndex(final Collection<? extends Drawable> drawables) {
    List<? extends Drawable> sortedDrawables = new ArrayList<>(drawables);
    int maxZIndex = 0;
    sortedDrawables.sort(COMPARATOR);

    for (final Drawable drawable : sortedDrawables) {
      if (drawable.getZ() == null) {
        drawable.setZ(++maxZIndex);
        if (drawable instanceof SpeciesWithModificationResidue) {
          for (ModificationResidue mr : ((SpeciesWithModificationResidue) drawable).getModificationResidues()) {
            mr.setZ(++maxZIndex);
          }
        }
      }
    }
  }
}
