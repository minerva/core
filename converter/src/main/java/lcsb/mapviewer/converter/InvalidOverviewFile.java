package lcsb.mapviewer.converter;

/**
 * Exception thrown when the
 * {@link lcsb.mapviewer.converter.zip.InputFileType#OVERLAY_IMAGE_DATA input
 * file * } containing data about {@link lcsb.mapviewer.model.map.OverviewImage
 * overview images} is invalid.
 * 
 * @author Piotr Gawron
 * 
 */
public class InvalidOverviewFile extends InvalidInputDataExecption {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   *
   * @param e
   *          parent exception (reason)
   */
  public InvalidOverviewFile(final Exception e) {
    super(e);
  }

  /**
   * Default constructor.
   *
   * @param message
   *          exception message
   * @param e
   *          parent exception (reason)
   */
  public InvalidOverviewFile(final String message, final Exception e) {
    super(message, e);
  }

  /**
   * Default constructor.
   *
   * @param message
   *          exception message
   */
  public InvalidOverviewFile(final String message) {
    super(message);
  }

}
