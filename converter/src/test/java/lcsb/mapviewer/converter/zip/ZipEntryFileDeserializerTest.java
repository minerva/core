package lcsb.mapviewer.converter.zip;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import lcsb.mapviewer.model.map.model.SubmodelType;

public class ZipEntryFileDeserializerTest {

  private ObjectMapper objectMapper = new ObjectMapper();

  @Test
  public void testGlyphDeserializeFromSerialized() throws Exception {
    ZipEntryFile entry = new GlyphZipEntryFile("glyph.png");

    ZipEntryFile deserialized = objectMapper.readValue(objectMapper.writeValueAsString(entry), ZipEntryFile.class);

    assertEquals(entry, deserialized);
  }

  @Test
  public void testImageDeserializeFromSerialized() throws Exception {
    ZipEntryFile entry = new ImageZipEntryFile("image.png");

    ZipEntryFile deserialized = objectMapper.readValue(objectMapper.writeValueAsString(entry), ZipEntryFile.class);

    assertEquals(entry, deserialized);
  }

  @Test
  public void testModelDeserializeFromSerialized() throws Exception {
    ZipEntryFile entry = new ModelZipEntryFile("model.xml", "m", true, false, SubmodelType.PATHWAY);

    ZipEntryFile deserialized = objectMapper.readValue(objectMapper.writeValueAsString(entry), ZipEntryFile.class);

    assertEquals(entry, deserialized);
  }

  @Test
  public void testOverlayDeserializeFromSerialized() throws Exception {
    ZipEntryFile entry = new LayoutZipEntryFile("overlay.txt", "o", "desc");

    ZipEntryFile deserialized = objectMapper.readValue(objectMapper.writeValueAsString(entry), ZipEntryFile.class);

    assertEquals(entry, deserialized);
  }

}
