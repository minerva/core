package lcsb.mapviewer.converter.zip;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Point2D;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import lcsb.mapviewer.converter.ConverterTestFunctions;
import lcsb.mapviewer.model.map.OverviewImageLink;
import lcsb.mapviewer.model.map.OverviewLink;
import lcsb.mapviewer.model.map.OverviewModelLink;
import lcsb.mapviewer.model.map.OverviewSearchLink;

public class OverviewLinkDeserializerTest extends ConverterTestFunctions {

  @Test
  public void testDeserializeOverviewModelLink() throws Exception {
    ObjectMapper mapper = new ObjectMapper();
    final SimpleModule module = new SimpleModule();
    module.addDeserializer(OverviewLink.class, new OverviewLinkDeserializer());
    mapper.registerModule(module);

    OverviewLink link = mapper.readValue(
        FileUtils.readFileToString(new File("testFiles/json/overview_model_link.json"), StandardCharsets.UTF_8),
        OverviewLink.class);

    OverviewModelLink result = (OverviewModelLink) link;
    assertEquals(Arrays.asList(
        new Point2D.Double(10.0, 10.0),
        new Point2D.Double(100.0, 10.0),
        new Point2D.Double(100.0, 100.0),
        new Point2D.Double(100.0, 10.0)),
        result.getPolygonCoordinates());

    assertEquals((Integer) 3, result.getZoomLevel());
    assertEquals((Integer) 10, result.getxCoord());
    assertEquals("main", result.getLinkedModel().getName());
  }

  @Test
  public void testDeserializeOverviewImageLink() throws Exception {
    ObjectMapper mapper = new ObjectMapper();
    final SimpleModule module = new SimpleModule();
    module.addDeserializer(OverviewLink.class, new OverviewLinkDeserializer());
    mapper.registerModule(module);

    OverviewLink link = mapper.readValue(
        FileUtils.readFileToString(new File("testFiles/json/overview_image_link.json"), StandardCharsets.UTF_8),
        OverviewLink.class);

    OverviewImageLink result = (OverviewImageLink) link;

    assertEquals("test2.png", result.getLinkedOverviewImage().getFilename());
  }

  @Test
  public void testDeserializeOverviewSearchLink() throws Exception {
    ObjectMapper mapper = new ObjectMapper();
    final SimpleModule module = new SimpleModule();
    module.addDeserializer(OverviewLink.class, new OverviewLinkDeserializer());
    mapper.registerModule(module);

    OverviewLink link = mapper.readValue(
        FileUtils.readFileToString(new File("testFiles/json/overview_query_link.json"), StandardCharsets.UTF_8),
        OverviewLink.class);

    OverviewSearchLink result = (OverviewSearchLink) link;

    assertEquals("SNCA", result.getQuery());
  }

}
