package lcsb.mapviewer.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.TextFileUtils;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.overlay.GeneVariant;
import lcsb.mapviewer.model.overlay.GeneVariantDataOverlayEntry;
import lcsb.mapviewer.model.overlay.InvalidDataOverlayException;

public class ColorSchemaReaderTest extends ConverterTestFunctions {

  private ColorSchemaReader reader;

  @Before
  public void setUp() throws Exception {
    reader = new ColorSchemaReader();
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testReadSchema() throws Exception {
    Collection<DataOverlayEntry> schemas = reader.readColorSchema("testFiles/coloring/ageing.txt");

    assertNotNull(schemas);
    assertEquals(412, schemas.size());

    schemas = reader.readColorSchema("testFiles/coloring/ge001.txt");

    assertNotNull(schemas);
    assertEquals(3057, schemas.size());

    schemas = reader.readColorSchema("testFiles/coloring/ge005.txt");

    assertNotNull(schemas);
    assertEquals(4338, schemas.size());
  }

  @Test
  public void testReadGeneVariantsSchema() throws Exception {
    File f = new File("testFiles/coloring/gene_variants.txt");
    byte[] data = fileToByteArray(f);

    ByteArrayInputStream bin = new ByteArrayInputStream(data);

    Collection<DataOverlayEntry> schemas = reader.readColorSchema(bin,
        TextFileUtils.getHeaderParametersFromFile(new ByteArrayInputStream(data)));
    assertNotNull(schemas);
    assertEquals(3, schemas.size());
  }

  @Test
  public void testReadGeneVariantByIdSchema() throws Exception {
    File f = new File("testFiles/coloring/gene_variants_id.txt");
    byte[] data = fileToByteArray(f);

    ByteArrayInputStream bin = new ByteArrayInputStream(data);

    Collection<DataOverlayEntry> schemas = reader.readColorSchema(bin,
        TextFileUtils.getHeaderParametersFromFile(new ByteArrayInputStream(data)));
    assertNotNull(schemas);
    assertEquals(1, schemas.size());
  }

  private byte[] fileToByteArray(final File f) throws FileNotFoundException, IOException {
    InputStream in = new FileInputStream(f);

    byte[] buff = new byte[8000];

    int bytesRead = 0;

    ByteArrayOutputStream bao = new ByteArrayOutputStream();

    while ((bytesRead = in.read(buff)) != -1) {
      bao.write(buff, 0, bytesRead);
    }
    in.close();
    bao.close();

    byte[] data = bao.toByteArray();
    return data;
  }

  @Test
  public void testReadGeneVariantsWithAminoAcidChange() throws Exception {
    File f = new File("testFiles/coloring/gene_variants_aa_change.txt");
    byte[] data = fileToByteArray(f);

    ByteArrayInputStream bin = new ByteArrayInputStream(data);

    Collection<DataOverlayEntry> schemas = reader.readColorSchema(bin,
        TextFileUtils.getHeaderParametersFromFile(new ByteArrayInputStream(data)));
    assertNotNull(schemas);
    assertEquals(2, schemas.size());
    GeneVariantDataOverlayEntry schema = (GeneVariantDataOverlayEntry) schemas.iterator().next();
    boolean aaChangeFound = false;
    for (final GeneVariant geneVariant : schema.getGeneVariants()) {
      if (geneVariant.getAminoAcidChange() != null && !geneVariant.getAminoAcidChange().trim().isEmpty()) {
        aaChangeFound = true;
      }
    }
    assertTrue("Amino acid are not present in parsed data", aaChangeFound);
  }

  @Test
  public void testReadGeneVariantsWithNewNamingSchema() throws Exception {
    File f = new File("testFiles/coloring/gene_variants_new_naming.txt");
    byte[] data = fileToByteArray(f);

    ByteArrayInputStream bin = new ByteArrayInputStream(data);

    Collection<DataOverlayEntry> schemas = reader.readColorSchema(bin,
        TextFileUtils.getHeaderParametersFromFile(new ByteArrayInputStream(data)));
    assertNotNull(schemas);
    assertEquals(3, schemas.size());
  }

  @Test
  public void testReadGeneVariantsSchemaWithAF() throws Exception {
    File f = new File("testFiles/coloring/gene_variants_all_freq.txt");
    byte[] data = fileToByteArray(f);

    ByteArrayInputStream bin = new ByteArrayInputStream(data);

    Collection<DataOverlayEntry> schemas = reader.readColorSchema(bin,
        TextFileUtils.getHeaderParametersFromFile(new ByteArrayInputStream(data)));
    assertNotNull(schemas);
    assertEquals(2, schemas.size());
  }

  @Test(expected = InvalidDataOverlayException.class)
  public void testReadInvalidGeneVariantsSchema() throws Exception {
    reader.readColorSchema("testFiles/coloring/gene_variants_invalid_genome.txt");
  }

  @Test
  public void testReadSchema2() throws Exception {
    Collection<DataOverlayEntry> schemas = reader.readColorSchema("testFiles/coloring/goodSchema.txt");

    assertNotNull(schemas);
    assertEquals(3, schemas.size());
  }

  @Test(expected = InvalidDataOverlayException.class)
  public void testReadSchema3() throws Exception {
    reader.readColorSchema("testFiles/coloring/wrongSchema.txt");
  }

  @Test
  public void testProblematicStephanSchema3() throws Exception {
    reader.readColorSchema("testFiles/coloring/problematicSchema.txt");
  }

  @Test
  public void testReadReactionSchema() throws Exception {
    Collection<DataOverlayEntry> collection = reader.readColorSchema("testFiles/coloring/reactionSchema.txt");
    assertEquals(1, collection.size());
    DataOverlayEntry schema = collection.iterator().next();
    assertEquals("re1", schema.getElementId());
    assertEquals(3.0, schema.getLineWidth(), Configuration.EPSILON);
    assertEquals(Color.RED, schema.getColor());
  }

  @Test(timeout = 15000)
  public void testNextVersionReadSchema() throws Exception {
    Collection<DataOverlayEntry> schemas = reader.readColorSchema("testFiles/coloring/goodLayout.v=1.0.txt");

    assertNotNull(schemas);
    assertEquals(3, schemas.size());
  }

  @Test
  public void testColoringWithValueOrColor() throws Exception {
    Map<String, String> params = new HashMap<>();
    params.put(TextFileUtils.COLUMN_COUNT_PARAM, "3");

    String input = "name\tcolor\tvalue\n"
        + "s1\t#ff0000\t\n"
        + "s2\t\t1.0\n";
    Collection<DataOverlayEntry> schemas = reader
        .readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
    assertEquals(2, schemas.size());
  }

  @Test
  public void testElementsByType() throws Exception {
    Map<String, String> params = new HashMap<>();
    params.put(TextFileUtils.COLUMN_COUNT_PARAM, "3");

    String input = "type\tname\tvalue\n"
        + "protein\t\t1.0\n";
    Collection<DataOverlayEntry> schemas = reader
        .readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
    assertEquals(1, schemas.size());
  }

  @Test(expected = InvalidDataOverlayException.class)
  public void testColoringWithInvalidValueAndColor() throws Exception {
    Map<String, String> params = new HashMap<>();
    params.put(TextFileUtils.COLUMN_COUNT_PARAM, "3");

    String input = "name\tcolor\tvalue\ns1\t#ff0000\t1.0";
    reader.readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
  }

  @Test(expected = InvalidDataOverlayException.class)
  public void testColoringWithEmptyColor() throws Exception {
    Map<String, String> params = new HashMap<>();
    params.put(TextFileUtils.COLUMN_COUNT_PARAM, "3");

    String input = "name\tcolor\ns1\tx";
    reader.readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
  }

  @Test(expected = InvalidDataOverlayException.class)
  public void testEmptyFile() throws Exception {
    Map<String, String> params = new HashMap<>();
    params.put(TextFileUtils.COLUMN_COUNT_PARAM, "0");

    String input = "";
    reader.readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
  }

  @Test(expected = InvalidDataOverlayException.class)
  public void testFileWithHeaderOnly() throws Exception {
    Map<String, String> params = new HashMap<>();
    params.put(TextFileUtils.COLUMN_COUNT_PARAM, "0");

    String input = "#xyz=aaaa\nabc=ttt";
    reader.readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
  }

  @Test
  public void testColoringWithInvalidValueAndColor2() throws Exception {
    Map<String, String> params = new HashMap<>();
    params.put(TextFileUtils.COLUMN_COUNT_PARAM, "3");

    String input = "name\tcolor\tvalue\ns1\t\t";
    reader.readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
  }

  @Test
  public void testSchemasWithId() throws Exception {
    Collection<DataOverlayEntry> schemas = reader.readColorSchema("testFiles/coloring/schemaWithIdentifiers.txt");
    for (final DataOverlayEntry colorSchema : schemas) {
      for (final MiriamData md : colorSchema.getMiriamData()) {
        assertNotNull(md.getResource());
        assertFalse(md.getResource().isEmpty());
      }
    }
  }

  @Test
  public void testSchemasWithIdSnakeCase() throws Exception {
    Collection<DataOverlayEntry> schemas = reader
        .readColorSchema("testFiles/coloring/schema_with_identifiers_in_snake_case.txt");
    for (final DataOverlayEntry colorSchema : schemas) {
      for (final MiriamData md : colorSchema.getMiriamData()) {
        assertNotNull(md.getResource());
        assertFalse(md.getResource().isEmpty());
      }
    }
  }

  @Test
  public void testSchemasWithEmptyNameAndId() throws Exception {
    Collection<DataOverlayEntry> schemas = reader.readColorSchema("testFiles/coloring/schemaIdWithoutName.txt");
    for (final DataOverlayEntry colorSchema : schemas) {
      for (final MiriamData md : colorSchema.getMiriamData()) {
        assertFalse(md.getResource().isEmpty());
        assertEquals("", colorSchema.getName());
      }
    }
  }

  @Test
  public void testSimpleNameSchemas() throws Exception {
    String input = "#header\ns1\ns2\n";
    Collection<DataOverlayEntry> schemas = reader
        .readSimpleNameColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)));
    assertEquals(2, schemas.size());
  }

  @Test
  public void testParseSpeciesTypesForEmptyString() throws Exception {
    List<Class<? extends BioEntity>> classes = reader.parseSpeciesTypes("", null);
    assertEquals(0, classes.size());
  }

  @Test
  public void testColoringWithModelName() throws Exception {
    String input = "name\tcolor\tmap_name\n"
        + "s1\t#ff0000\txxx\n";
    Map<String, String> params = new HashMap<>();
    params.put(TextFileUtils.COLUMN_COUNT_PARAM, "3");

    Collection<DataOverlayEntry> schemas = reader
        .readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
    assertEquals(1, schemas.size());
    assertNotNull(schemas.iterator().next().getModelName());
  }

  @Test
  public void testColoringWithMapName() throws Exception {
    String input = "name\tcolor\tmap_name\n"
        + "s1\t#ff0000\txxx\n";
    Map<String, String> params = new HashMap<>();
    params.put(TextFileUtils.COLUMN_COUNT_PARAM, "3");

    Collection<DataOverlayEntry> schemas = reader
        .readColorSchema(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), params);
    assertEquals(1, schemas.size());
    assertNotNull(schemas.iterator().next().getModelName());
  }

  @Test(expected = InvalidDataOverlayException.class)
  public void testReadColorSchemaWithInvalidType() throws Exception {
    FileInputStream fis = new FileInputStream("testFiles/coloring/invalidType.txt");
    FileInputStream fis2 = new FileInputStream("testFiles/coloring/invalidType.txt");
    ColorSchemaReader reader = new ColorSchemaReader();
    reader.readColorSchema(fis, TextFileUtils.getHeaderParametersFromFile(fis2));

  }

  @Test
  public void testGeneticVariantByUniprotIdentifier() throws Exception {
    File f = new File("testFiles/coloring/uniprot-identifiers.txt");
    byte[] data = fileToByteArray(f);

    ByteArrayInputStream bin = new ByteArrayInputStream(data);

    Collection<DataOverlayEntry> schemas = reader.readColorSchema(bin,
        TextFileUtils.getHeaderParametersFromFile(new ByteArrayInputStream(data)));
    assertNotNull(schemas);
    assertEquals(2, schemas.size());
  }

}
