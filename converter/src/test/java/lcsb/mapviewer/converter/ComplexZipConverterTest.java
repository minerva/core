package lcsb.mapviewer.converter;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidClassException;
import lcsb.mapviewer.converter.zip.LayoutZipEntryFile;
import lcsb.mapviewer.converter.zip.ModelZipEntryFile;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.overlay.DataOverlay;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.ByteArrayInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class ComplexZipConverterTest extends ConverterTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = InvalidClassException.class)
  public void testConstructor() throws Exception {
    new ComplexZipConverter(Converter.class);
  }

  @Test
  public void testConstructor3() throws Exception {
    new ComplexZipConverter(MockConverter.class);
  }

  @Test
  public void testParamsOk() throws Exception {
    ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(new ZipFile("testFiles/complex_model.zip"));
    params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));
    params.entry(new ModelZipEntryFile("s1.xml", "s1", false, false, SubmodelType.DOWNSTREAM_TARGETS));
    params.entry(new ModelZipEntryFile("s2.xml", "s2", false, false, SubmodelType.PATHWAY));
    params.entry(new ModelZipEntryFile("s3.xml", "s3", false, false, SubmodelType.UNKNOWN));
    params.entry(new ModelZipEntryFile("mapping.xml", null, false, true, null));
    Model model = converter.createModel(params);
    assertNotNull(model);
    assertEquals("main", model.getName());
    assertEquals(3, model.getSubmodelConnections().size());

    Model s1Model = null;
    Model s2Model = null;
    Model s3Model = null;

    for (final ModelSubmodelConnection submodel : model.getSubmodelConnections()) {
      if (submodel.getName().equals("s1")) {
        s1Model = submodel.getSubmodel().getModel();
      }
      if (submodel.getName().equals("s2")) {
        s2Model = submodel.getSubmodel().getModel();
      }
      if (submodel.getName().equals("s3")) {
        s3Model = submodel.getSubmodel().getModel();
      }
    }
    assertNotNull(s1Model);
    assertNotNull(s2Model);
    assertNotNull(s3Model);

    Element al1 = model.getElementByElementId("sa1");
    assertNotNull(al1.getSubmodel());
    assertEquals(SubmodelType.DOWNSTREAM_TARGETS, al1.getSubmodel().getType());
    assertEquals(s1Model, al1.getSubmodel().getSubmodel().getModel());
    assertNotNull(al1.getImmediateLink());

    Element al2 = model.getElementByElementId("sa2");
    assertNull(al2.getSubmodel());

    Element al4 = model.getElementByElementId("sa4");
    assertNotNull(al4.getSubmodel());
    assertEquals(SubmodelType.PATHWAY, al4.getSubmodel().getType());
    assertEquals(s2Model, al4.getSubmodel().getSubmodel().getModel());

    Element submap1Element1 = s1Model.getElementByElementId("sa1");
    assertNotNull(submap1Element1.getSubmodel());
    assertEquals(SubmodelType.DOWNSTREAM_TARGETS, submap1Element1.getSubmodel().getType());
    assertEquals(s3Model, submap1Element1.getSubmodel().getSubmodel().getModel());
  }

  @Test
  public void testChangeModelZipEntry() throws Exception {
    ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(new ZipFile("testFiles/complex_model.zip"));
    params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));
    params.entry(new ModelZipEntryFile("s1.xml", "s12", false, false, SubmodelType.DOWNSTREAM_TARGETS));
    params.entry(new ModelZipEntryFile("s2.xml", "s2", false, false, SubmodelType.PATHWAY));
    params.entry(new ModelZipEntryFile("s3.xml", "s3", false, false, SubmodelType.UNKNOWN));
    params.entry(new ModelZipEntryFile("mapping.xml", null, false, true, null));
    Model model = converter.createModel(params);
    assertNotNull(model);
    assertEquals("main", model.getName());
    assertEquals(3, model.getSubmodelConnections().size());

    Model s1Model = null;
    Model s2Model = null;
    Model s3Model = null;

    for (final ModelSubmodelConnection submodel : model.getSubmodelConnections()) {
      if (submodel.getName().equals("s12")) {
        s1Model = submodel.getSubmodel().getModel();
      }
      if (submodel.getName().equals("s2")) {
        s2Model = submodel.getSubmodel().getModel();
      }
      if (submodel.getName().equals("s3")) {
        s3Model = submodel.getSubmodel().getModel();
      }
    }
    assertNotNull(s1Model);
    assertNotNull(s2Model);
    assertNotNull(s3Model);

    Element al1 = model.getElementByElementId("sa1");
    assertNotNull(al1.getSubmodel());
    assertEquals(SubmodelType.DOWNSTREAM_TARGETS, al1.getSubmodel().getType());
    assertEquals(s1Model, al1.getSubmodel().getSubmodel().getModel());

    Element al2 = model.getElementByElementId("sa2");
    assertNull(al2.getSubmodel());

    Element al4 = model.getElementByElementId("sa4");
    assertNotNull(al4.getSubmodel());
    assertEquals(SubmodelType.PATHWAY, al4.getSubmodel().getType());
    assertEquals(s2Model, al4.getSubmodel().getSubmodel().getModel());

    Element submodel1Element1 = s1Model.getElementByElementId("sa1");
    assertNotNull(submodel1Element1.getSubmodel());
    assertEquals(SubmodelType.DOWNSTREAM_TARGETS, submodel1Element1.getSubmodel().getType());
    assertEquals(s3Model, submodel1Element1.getSubmodel().getSubmodel().getModel());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testParamsMissing() throws Exception {
    ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(new ZipFile("testFiles/complex_model.zip"));
    params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));
    params.entry(new ModelZipEntryFile("s1.xml", "s1", false, false, SubmodelType.DOWNSTREAM_TARGETS));
    params.entry(new ModelZipEntryFile("s3.xml", "s3", false, false, SubmodelType.UNKNOWN));
    params.entry(new ModelZipEntryFile("mapping.xml", null, false, true, null));
    converter.createModel(params);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testParamsInvalid1() throws Exception {
    // two mains
    ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(new ZipFile("testFiles/complex_model.zip"));
    params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));
    params.entry(new ModelZipEntryFile("s1.xml", "s1", true, false, SubmodelType.DOWNSTREAM_TARGETS));
    params.entry(new ModelZipEntryFile("s2.xml", "s2", false, false, SubmodelType.PATHWAY));
    params.entry(new ModelZipEntryFile("s3.xml", "s3", false, false, SubmodelType.UNKNOWN));
    params.entry(new ModelZipEntryFile("mapping.xml", null, false, true, null));
    converter.createModel(params);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testParamsInvalid2() throws Exception {
    // zero mains
    ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(new ZipFile("testFiles/complex_model.zip"));
    params.entry(new ModelZipEntryFile("main.xml", "main", false, false, null));
    params.entry(new ModelZipEntryFile("s1.xml", "s1", false, false, SubmodelType.DOWNSTREAM_TARGETS));
    params.entry(new ModelZipEntryFile("s2.xml", "s2", false, false, SubmodelType.PATHWAY));
    params.entry(new ModelZipEntryFile("s3.xml", "s3", false, false, SubmodelType.UNKNOWN));
    params.entry(new ModelZipEntryFile("mapping.xml", null, false, true, null));
    converter.createModel(params);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testParamsInvalid3() throws Exception {
    // two mappings
    ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(new ZipFile("testFiles/complex_model.zip"));
    params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));
    params.entry(new ModelZipEntryFile("s1.xml", "s1", false, true, SubmodelType.DOWNSTREAM_TARGETS));
    params.entry(new ModelZipEntryFile("s2.xml", "s2", false, false, SubmodelType.PATHWAY));
    params.entry(new ModelZipEntryFile("s3.xml", "s3", false, false, SubmodelType.UNKNOWN));
    params.entry(new ModelZipEntryFile("mapping.xml", null, false, true, null));
    converter.createModel(params);
  }

  @Test
  public void testParamsMissingMapping() throws Exception {
    // zero mappings (should be ok)
    ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(new ZipFile("testFiles/complex_model.zip"));
    params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));
    params.entry(new ModelZipEntryFile("s1.xml", "s1", false, false, SubmodelType.DOWNSTREAM_TARGETS));
    params.entry(new ModelZipEntryFile("s2.xml", "s2", false, false, SubmodelType.PATHWAY));
    params.entry(new ModelZipEntryFile("s3.xml", "s3", false, false, SubmodelType.UNKNOWN));
    params.entry(new ModelZipEntryFile("mapping.xml", "", false, false, null));
    Model model = converter.createModel(params);

    assertNotNull(model);

    assertEquals(4, model.getSubmodelConnections().size());

    for (final ModelSubmodelConnection submodel : model.getSubmodelConnections()) {
      if (submodel.getName().equals("s1")) {
        assertEquals(SubmodelType.DOWNSTREAM_TARGETS, submodel.getType());
      } else if (submodel.getName().equals("s2")) {
        assertEquals(SubmodelType.PATHWAY, submodel.getType());
      } else if (submodel.getName().equals("s3")) {
        assertEquals(SubmodelType.UNKNOWN, submodel.getType());
      } else if (submodel.getName().equals("mapping")) {
        assertEquals(SubmodelType.UNKNOWN, submodel.getType());
      }
    }
  }

  @Test(expected = InvalidArgumentException.class)
  public void testTooManyParams() throws Exception {
    // zero mappings (should be ok)
    ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(new ZipFile("testFiles/complex_model.zip"));
    params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));
    params.entry(new ModelZipEntryFile("s1.xml", "s1", false, false, SubmodelType.DOWNSTREAM_TARGETS));
    params.entry(new ModelZipEntryFile("s2.xml", "s2", false, false, SubmodelType.PATHWAY));
    params.entry(new ModelZipEntryFile("s3.xml", "s3", false, false, SubmodelType.UNKNOWN));
    params.entry(new ModelZipEntryFile("mapping.xml", "", false, false, null));
    params.entry(new ModelZipEntryFile("blabla.xml", "s3", false, false, SubmodelType.UNKNOWN));
    converter.createModel(params);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidMappingFile() throws Exception {
    // zero mappings (should be ok)
    ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(new ZipFile("testFiles/invalid_mapping.zip"));
    params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));
    params.entry(new ModelZipEntryFile("mapping.xml", null, false, true, null));
    converter.createModel(params);
  }

  @Test
  public void testIsIgnoredFileForMac() throws Exception {
    ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    assertTrue(converter.isIgnoredFile("__MACOSX/.desc"));
    assertTrue(converter.isIgnoredFile(".DS_Store"));
    assertTrue(converter.isIgnoredFile("images/.DS_Store"));
    assertTrue(converter.isIgnoredFile("layouts/.DS_Store"));
    assertTrue(converter.isIgnoredFile("submaps/.DS_Store"));
  }

  @Test
  public void testIsIgnoredFileForOldMacEntries() throws Exception {
    ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    assertTrue(converter.isIgnoredFile(".DS_Store/.desc"));
  }

  @Test
  public void testIsIgnoredFileForValidFiles() throws Exception {
    ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    assertFalse(converter.isIgnoredFile("mapping.xml"));
  }

  @Test
  public void testLayoutZipEntryFileToLayoutOrder() throws Exception {
    ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    ComplexZipConverterParams params = new ComplexZipConverterParams();
    ZipFile file1 = Mockito.mock(ZipFile.class);
    ZipFile file2 = Mockito.mock(ZipFile.class);
    LayoutZipEntryFile entry1 = new LayoutZipEntryFile("overlay1.txt", "name", "desc1");
    LayoutZipEntryFile entry2 = new LayoutZipEntryFile("overlay2.txt", "name", "desc1");
    params.entry(entry1);
    params.entry(entry2);
    ZipEntry entry = Mockito.mock(ZipEntry.class);
    Mockito.when(file1.getInputStream(entry)).thenReturn(new ByteArrayInputStream("name\tvalue\n".getBytes()));
    Mockito.when(file2.getInputStream(entry)).thenReturn(new ByteArrayInputStream("name\tvalue\n".getBytes()));

    DataOverlay overlay1 = converter.layoutZipEntryFileToLayout(params, file1, entry, entry1, 1);
    DataOverlay overlay2 = converter.layoutZipEntryFileToLayout(params, file2, entry, entry2, 2);
    assertTrue(overlay1.getOrderIndex() > 0);
    assertTrue(overlay2.getOrderIndex() > overlay1.getOrderIndex());
  }

}
