package lcsb.mapviewer.converter;

import lcsb.mapviewer.common.tests.TestUtils;
import lcsb.mapviewer.common.tests.UnitTestFailedWatcher;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.GenericProtein;
import org.junit.Rule;

public abstract class ConverterTestFunctions extends TestUtils {

  private int identifierCounter = 0;

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();

  protected GenericProtein createProtein() {
    GenericProtein result = new GenericProtein("s" + identifierCounter++);
    return result;
  }

  protected Complex createComplex() {
    Complex result = new Complex("s" + identifierCounter++);
    return result;
  }

  protected Compartment createCompartment() {
    Compartment result = new Compartment("s" + identifierCounter++);
    return result;
  }

  protected Reaction createReaction() {
    Reaction result = new TransportReaction("s" + identifierCounter++);
    return result;
  }

  protected LayerText createText() {
    return new LayerText();
  }

  protected Layer createLayer() {
    final Layer result = new Layer();
    result.setLayerId(identifierCounter++);
    result.setName(faker.name().name());
    result.setZ(faker.number().numberBetween(1, 100));
    return result;
  }

}
