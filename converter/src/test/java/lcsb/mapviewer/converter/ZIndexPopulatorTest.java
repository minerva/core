package lcsb.mapviewer.converter;

import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ZIndexPopulatorTest extends ConverterTestFunctions {


  @Test
  public void testMixZIndexValuesInSpecies() {
    Model model = new ModelFullIndexed(null);
    Species speciesWithoutZIndex = createProtein();
    model.addElement(speciesWithoutZIndex);

    Species speciesWithZIndex = createProtein();
    speciesWithZIndex.setZ(100);
    model.addElement(speciesWithZIndex);

    ZIndexPopulator populator = new ZIndexPopulator();
    populator.populateZIndex(model);

    assertNotNull(speciesWithoutZIndex.getZ());

    assertTrue(speciesWithoutZIndex.getZ() < speciesWithZIndex.getZ());
  }

  @Test
  public void testZIndexBySpeciesSize() {
    Model model = new ModelFullIndexed(null);
    Species microSpecies = createProtein();
    microSpecies.setWidth(1);
    microSpecies.setHeight(1);
    model.addElement(microSpecies);

    Species biggerSpecies = createProtein();
    biggerSpecies.setWidth(100);
    biggerSpecies.setHeight(100);
    model.addElement(biggerSpecies);

    Species smallerSpecies = createProtein();
    smallerSpecies.setWidth(10);
    smallerSpecies.setHeight(10);
    model.addElement(smallerSpecies);

    ZIndexPopulator populator = new ZIndexPopulator();
    populator.populateZIndex(model);

    assertTrue(biggerSpecies.getZ() < smallerSpecies.getZ());
    assertTrue(smallerSpecies.getZ() < microSpecies.getZ());
  }

  @Test
  public void testZIndexSpeciesAndText() {
    Model model = new ModelFullIndexed(null);
    Species microSpecies = createProtein();
    microSpecies.setWidth(1);
    microSpecies.setHeight(1);
    model.addElement(microSpecies);

    Layer layer = createLayer();
    model.addLayer(layer);
    LayerText biggerText = createText();
    biggerText.setWidth(10.0);
    biggerText.setHeight(10.0);
    layer.addLayerText(biggerText);

    LayerText smallerText = createText();
    smallerText.setWidth(5.0);
    smallerText.setHeight(5.0);
    layer.addLayerText(smallerText);

    ZIndexPopulator populator = new ZIndexPopulator();
    populator.populateZIndex(model);

    assertTrue(biggerText.getZ() < smallerText.getZ());
    assertTrue(smallerText.getZ() < microSpecies.getZ());
  }

  @Test
  public void testZIndexReactionShouldBeAfterStructuralState() {
    Model model = new ModelFullIndexed(null);
    Protein species = createProtein();
    StructuralState state = new StructuralState();
    state.setWidth(100);
    state.setHeight(100);
    species.setWidth(1);
    species.setHeight(1);
    species.addStructuralState(state);
    model.addElement(species);

    Reaction reaction = createReaction();
    model.addReaction(reaction);

    ZIndexPopulator populator = new ZIndexPopulator();
    populator.populateZIndex(model);

    assertTrue(reaction.getZ() > species.getZ());
    // structural state should be below reaction and above species
    assertTrue(species.getModificationResidues().get(0).getZ() < reaction.getZ());
    assertTrue(species.getModificationResidues().get(0).getZ() > species.getZ());
  }

  @Test
  public void testZIndexEqualSizeByElementId() {
    Model model = new ModelFullIndexed(null);
    Species firstSpecies = createProtein();
    firstSpecies.setWidth(1);
    firstSpecies.setHeight(1);
    model.addElement(firstSpecies);

    Species secondSpecies = createProtein();
    secondSpecies.setWidth(1);
    secondSpecies.setHeight(1);
    model.addElement(secondSpecies);

    Species thirdSpecies = createProtein();
    thirdSpecies.setWidth(1);
    thirdSpecies.setHeight(1);
    model.addElement(thirdSpecies);

    ZIndexPopulator populator = new ZIndexPopulator();
    populator.populateZIndex(model);

    assertTrue(firstSpecies.getZ() < secondSpecies.getZ());
    assertTrue(secondSpecies.getZ() < thirdSpecies.getZ());
  }

  @Test
  public void testZIndexComplexChildren() {
    Model model = new ModelFullIndexed(null);
    Complex complex = createComplex();
    complex.setWidth(1);
    complex.setHeight(1);
    model.addElement(complex);

    Species child = createProtein();
    child.setWidth(10);
    child.setHeight(10);
    model.addElement(child);

    complex.addSpecies(child);

    ZIndexPopulator populator = new ZIndexPopulator();
    populator.populateZIndex(model);

    assertTrue(complex.getZ() < child.getZ());
  }

  @Test
  public void testZIndexCompartmentChildren() {
    Model model = new ModelFullIndexed(null);
    Compartment compartment = createCompartment();
    compartment.setWidth(1);
    compartment.setHeight(1);
    model.addElement(compartment);

    Compartment childCompartment = createCompartment();
    childCompartment.setWidth(2);
    childCompartment.setHeight(2);
    model.addElement(childCompartment);
    compartment.addElement(childCompartment);

    Species child = createProtein();
    child.setWidth(10);
    child.setHeight(10);
    model.addElement(child);

    childCompartment.addElement(child);

    ZIndexPopulator populator = new ZIndexPopulator();
    populator.populateZIndex(model);

    assertTrue(compartment.getZ() < childCompartment.getZ());
    assertTrue(childCompartment.getZ() < child.getZ());
  }


}
