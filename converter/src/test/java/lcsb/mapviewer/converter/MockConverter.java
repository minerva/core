package lcsb.mapviewer.converter;

import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Species;

import java.io.File;
import java.io.InputStream;
import java.util.List;

public class MockConverter extends Converter {

  protected static Model modelToBeReturned = null;

  @Override
  public Model createModel(final ConverterParams params) {
    if (modelToBeReturned != null) {
      return modelToBeReturned;
    }
    Model result = new ModelFullIndexed(null);

    Species sa1 = new GenericProtein("sa1");
    result.addElement(sa1);

    Species sa2 = new GenericProtein("sa2");
    result.addElement(sa2);

    Species sa3 = new GenericProtein("sa3");
    result.addElement(sa3);

    Species sa4 = new Phenotype("sa4");
    result.addElement(sa4);

    Complex ca1 = new Complex("ca1");
    ca1.setName("main");
    result.addElement(ca1);
    Species sa5 = new GenericProtein("sa5");
    sa5.setName("sa1");
    result.addElement(sa5);
    ca1.addSpecies(sa5);
    Species sa6 = new Phenotype("sa6");
    sa6.setName("sa4");
    result.addElement(sa6);
    ca1.addSpecies(sa6);

    Complex ca2 = new Complex("ca2");
    ca2.setName("s1");
    result.addElement(ca2);
    Species sa7 = new GenericProtein("sa7");
    sa7.setName("sa1");
    result.addElement(sa7);
    ca2.addSpecies(sa7);

    Complex ca3 = new Complex("cs3");
    ca3.setName("s2");
    result.addElement(ca3);

    Complex ca4 = new Complex("cs4");
    ca4.setName("s3");
    result.addElement(ca4);

    Reaction r1 = new TransportReaction("re1");
    r1.addReactant(new Reactant(sa5));
    r1.addProduct(new Product(sa7));
    result.addReaction(r1);

    Reaction r2 = new TransportReaction("re2");
    r2.addReactant(new Reactant(sa6));
    r2.addProduct(new Product(ca3));
    result.addReaction(r2);

    Reaction r3 = new TransportReaction("re3");
    r3.addReactant(new Reactant(sa7));
    r3.addProduct(new Product(ca4));
    result.addReaction(r3);

    Species sa8 = new Phenotype("sa8");
    sa8.setName("sa1");
    sa8.setNotes("IMMEDIATE_LINK:https://minerva.pages.uni.lu/doc/");
    result.addElement(sa8);

    Compartment c1 = new Compartment("comp1");
    c1.setName("main");
    c1.addElement(sa8);
    result.addElement(c1);

    return result;
  }

  @Override
  public String model2String(final Model model) throws InconsistentModelException, ConverterException {
    return null;
  }

  @Override
  public String getCommonName() {
    return null;
  }

  @Override
  public MimeType getMimeType() {
    return null;
  }

  @Override
  public List<String> getFileExtensions() {
    return null;
  }

  @Override
  public InputStream model2InputStream(final Model model) throws InconsistentModelException, ConverterException {
    return null;
  }

  @Override
  public File model2File(final Model model, final String filePath) throws InconsistentModelException {
    return null;
  }

}