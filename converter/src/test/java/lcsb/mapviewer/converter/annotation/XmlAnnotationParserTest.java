package lcsb.mapviewer.converter.annotation;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.ConverterTestFunctions;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class XmlAnnotationParserTest extends ConverterTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseRdf() throws Exception {
    XmlAnnotationParser xap = new XmlAnnotationParser();

    String xml = readFile("testFiles/annotation/rdf.xml");

    Set<MiriamData> set = xap.parse(xml, null);
    assertEquals(2, set.size());
  }

  @Test
  public void testParseRdfWithIsProperty() throws Exception {
    XmlAnnotationParser xap = new XmlAnnotationParser();

    String xml = readFile("testFiles/annotation/rdf_has_property.xml");

    Set<MiriamData> set = xap.parse(xml, null);
    assertEquals(0, super.getWarnings().size());
    assertEquals(7, set.size());
  }

  @Test
  public void testMiriamDataToXmlAndBack() throws Exception {
    MiriamData md = new MiriamData(MiriamType.CHEBI, "e:f");
    XmlAnnotationParser parser = new XmlAnnotationParser();
    String xml = parser.miriamDataToXmlString(md);
    assertNotNull(xml);
    Set<MiriamData> set = parser.parseMiriamNode(super.getNodeFromXmlString(xml), null);
    assertNotNull(set);
    assertEquals(1, set.size());
    MiriamData md1 = set.iterator().next();
    assertNotNull(md1);
    assertEquals(md.getDataType(), md1.getDataType());
    assertEquals(md.getRelationType(), md1.getRelationType());
    assertEquals(md.getResource(), md1.getResource());
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidRdf() throws Exception {
    XmlAnnotationParser xap = new XmlAnnotationParser();

    xap.parseRdfNode(null, null);
  }

  @Test
  public void testParseRdfWithCreatorTag() throws Exception {
    XmlAnnotationParser xap = new XmlAnnotationParser();
    String xml = readFile("testFiles/annotation/rdf_with_creator_tag.xml");

    Set<MiriamData> set = xap.parse(xml, null);
    assertTrue(set.size() > 0);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidRdf3() throws Exception {
    XmlAnnotationParser xap = new XmlAnnotationParser();

    String xml = readFile("testFiles/annotation/invalid_rdf2.xml");

    xap.parse(xml, null);
  }

  @Test
  public void testParseInvalidRdf4() throws Exception {
    XmlAnnotationParser xap = new XmlAnnotationParser();

    String xml = readFile("testFiles/annotation/invalid_rdf3.xml");

    xap.parse(xml, null);

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testParseInvalidRdf5() throws Exception {
    XmlAnnotationParser xap = new XmlAnnotationParser();

    String xml = readFile("testFiles/annotation/invalid_rdf4.xml");

    xap.parse(xml, null);

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testMiriamDataToXmlWithUnsupportedType() throws Exception {
    MiriamData md = new MiriamData(MiriamType.CHEBI, "e:f");
    XmlAnnotationParser parser = new XmlAnnotationParser(Collections.singletonList(MiriamRelationType.BQ_BIOL_ENCODES));
    String xml = parser.miriamDataToXmlString(md);
    Set<MiriamData> set = parser.parseMiriamNode(super.getNodeFromXmlString(xml), null);
    assertNotNull(set);
    MiriamData md1 = set.iterator().next();
    assertNotEquals(md.getRelationType(), md1.getRelationType());
  }

}
