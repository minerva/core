package lcsb.mapviewer.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.converter.zip.ImageZipEntryFile;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.model.map.OverviewLink;
import lcsb.mapviewer.model.map.OverviewModelLink;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class OverviewParserTest extends ConverterTestFunctions {

  private static final String TEST_FILES_VALID_OVERVIEW_ZIP = "testFiles/valid_overview.zip";
  private static final String TEST_FILES_VALID_OVERVIEW_CASE_SENSITIVE_ZIP = "testFiles/valid_overview_case_sensitive.zip";
  private OverviewParser parser = new OverviewParser();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParsingValidFile() throws Exception {
    Set<Model> models = createValidTestMapModel();
    List<ImageZipEntryFile> imageEntries = createImageEntries(TEST_FILES_VALID_OVERVIEW_ZIP);
    List<OverviewImage> result = parser.parseOverviewLinks(models, imageEntries, null,
        new ZipFile(TEST_FILES_VALID_OVERVIEW_ZIP));

    assertNotNull(result);
    assertEquals(1, result.size());

    OverviewImage img = result.get(0);

    assertEquals("test.png", img.getFilename());
    assertEquals((Integer) 639, img.getHeight());
    assertEquals((Integer) 963, img.getWidth());
    assertEquals(2, img.getLinks().size());

    OverviewLink link = img.getLinks().get(0);
    List<Point2D> polygon = link.getPolygonCoordinates();
    assertEquals(4, polygon.size());

    assertTrue(link instanceof OverviewModelLink);

    OverviewModelLink modelLink = (OverviewModelLink) link;
    Model mainModel = models.iterator().next();
    assertEquals(mainModel.getModelData(), modelLink.getLinkedModel());
    assertEquals((Integer) 10, modelLink.getxCoord());
    assertEquals((Integer) 10, modelLink.getyCoord());
    assertEquals((Integer) 3, modelLink.getZoomLevel());
  }

  @Test
  public void testParsingValidCaseSensitiveFile() throws Exception {
    Set<Model> models = createValidTestMapModel();
    List<ImageZipEntryFile> imageEntries = createImageEntries(TEST_FILES_VALID_OVERVIEW_CASE_SENSITIVE_ZIP);
    for (final ImageZipEntryFile imageZipEntryFile : imageEntries) {
      imageZipEntryFile.setFilename(imageZipEntryFile.getFilename().toLowerCase());
    }
    List<OverviewImage> result = parser.parseOverviewLinks(models, imageEntries, null,
        new ZipFile(TEST_FILES_VALID_OVERVIEW_CASE_SENSITIVE_ZIP));
    assertNotNull(result);
    assertEquals(1, result.size());

    OverviewImage img = result.get(0);

    assertEquals("test.png", img.getFilename());
    assertEquals((Integer) 639, img.getHeight());
    assertEquals((Integer) 963, img.getWidth());
    assertEquals(2, img.getLinks().size());

    OverviewLink link = img.getLinks().get(0);
    List<Point2D> polygon = link.getPolygonCoordinates();
    assertEquals(4, polygon.size());

    assertTrue(link instanceof OverviewModelLink);

    OverviewModelLink modelLink = (OverviewModelLink) link;
    Model mainModel = models.iterator().next();
    assertEquals(mainModel.getModelData(), modelLink.getLinkedModel());
    assertEquals((Integer) 10, modelLink.getxCoord());
    assertEquals((Integer) 10, modelLink.getyCoord());
    assertEquals((Integer) 3, modelLink.getZoomLevel());
  }

  private List<ImageZipEntryFile> createImageEntries(final String string) throws IOException {
    List<ImageZipEntryFile> result = new ArrayList<>();

    ZipFile zipFile = new ZipFile(string);
    try {
      Enumeration<? extends ZipEntry> entries = zipFile.entries();
      while (entries.hasMoreElements()) {
        ZipEntry entry = entries.nextElement();
        if (!entry.isDirectory()) {
          result.add(new ImageZipEntryFile(entry.getName()));
        }
      }
      return result;
    } finally {
      zipFile.close();
    }
  }

  @Test
  public void testParsingValidFile2() throws Exception {
    Set<Model> models = createValidTestMapModel();

    String tmpDir = Files.createTempDirectory("tmp").toFile().getAbsolutePath();

    List<ImageZipEntryFile> imageEntries = createImageEntries(TEST_FILES_VALID_OVERVIEW_ZIP);
    List<OverviewImage> result = parser.parseOverviewLinks(models, imageEntries, tmpDir,
        new ZipFile(TEST_FILES_VALID_OVERVIEW_ZIP));

    assertTrue(new File(tmpDir + "/test.png").exists());

    assertNotNull(result);
    assertEquals(1, result.size());
    OverviewImage img = result.get(0);
    assertEquals("test.png", img.getFilename());
  }

  @Test(expected = InvalidOverviewFile.class)
  public void testParsingInvalidFile1() throws Exception {
    List<ImageZipEntryFile> imageEntries = createImageEntries("testFiles/invalid_overview_1.zip");
    Set<Model> models = createValidTestMapModel();

    parser.parseOverviewLinks(models, imageEntries, null, new ZipFile("testFiles/invalid_overview_1.zip"));
  }

  @Test(expected = InvalidOverviewFile.class)
  public void testParsingInvalidFile2() throws Exception {
    List<ImageZipEntryFile> imageEntries = createImageEntries("testFiles/invalid_overview_2.zip");
    Set<Model> models = createValidTestMapModel();

    parser.parseOverviewLinks(models, imageEntries, null, new ZipFile("testFiles/invalid_overview_2.zip"));
  }

  @Test(expected = InvalidOverviewFile.class)
  public void testParsingInvalidFile3() throws Exception {
    List<ImageZipEntryFile> imageEntries = createImageEntries("testFiles/invalid_overview_3.zip");
    Set<Model> models = createValidTestMapModel();

    parser.parseOverviewLinks(models, imageEntries, null, new ZipFile("testFiles/invalid_overview_3.zip"));
  }

  private Set<Model> createValidTestMapModel() {
    Set<Model> result = new HashSet<>();
    Model model = new ModelFullIndexed(null);
    model.setName("main");
    result.add(model);
    return result;
  }

  /**
   * Test coordinates that overlap (exception is expected).
   */
  @Test(expected = InvalidOverviewFile.class)
  public void testParseInvalidCoordinates() throws Exception {
    String invalidCoordinates = "test.png\t10,10 100,10 100,100 10,10\tmain.xml\t10,10\t3\n"
        + "test.png\t10,10 10,400 400,400 400,10\tmain.xml\t10,10\t4";
    Set<Model> models = createValidTestMapModel();

    List<OverviewImage> images = new ArrayList<OverviewImage>();
    OverviewImage oi = new OverviewImage();
    oi.setFilename("test.png");
    oi.setWidth(1000);
    oi.setHeight(1000);
    images.add(oi);

    parser.processCoordinates(models, images, invalidCoordinates);
  }

  @Test
  public void testParseValidCoordinates() throws Exception {
    String invalidCoordinates = "FILE\tPOLYGON\tLINK_TARGET\tMODEL_COORDINATES\tMODEL_ZOOM_LEVEL\tLINK_TYPE\n"
        + "test.png\t10,10 100,10 100,100 10,10\tmain.xml\t10,10\t3\tMODEL\n"
        + "test.png\t200,200 200,400 400,400 400,200\tmain.xml\t10,10\t4\tMODEL";
    Set<Model> models = createValidTestMapModel();

    List<OverviewImage> images = new ArrayList<OverviewImage>();
    OverviewImage oi = new OverviewImage();
    oi.setFilename("test.png");
    oi.setWidth(1000);
    oi.setHeight(1000);
    images.add(oi);

    parser.processCoordinates(models, images, invalidCoordinates);

    assertEquals(2, oi.getLinks().size());
  }

  @Test
  public void testParseValidComplexCoordinates() throws Exception {
    String invalidCoordinates = FileUtils.readFileToString(new File("testFiles/coordinates.txt"), "UTF-8");
    Set<Model> models = createValidTestMapModel();

    List<OverviewImage> images = new ArrayList<>();
    OverviewImage oi = new OverviewImage();
    oi.setFilename("test.png");
    oi.setWidth(1000);
    oi.setHeight(1000);
    images.add(oi);

    OverviewImage oi2 = new OverviewImage();
    oi2.setFilename("test2.png");
    oi2.setWidth(1000);
    oi2.setHeight(1000);
    images.add(oi2);

    parser.processCoordinates(models, images, invalidCoordinates);

    assertEquals(2, oi.getLinks().size());
    assertEquals(1, oi2.getLinks().size());
  }

  @Test
  public void testParseValidComplexCoordinatesWithExtraImages() throws Exception {
    String invalidCoordinates = FileUtils.readFileToString(new File("testFiles/coordinates.txt"), "UTF-8");
    Set<Model> models = createValidTestMapModel();

    List<OverviewImage> images = new ArrayList<>();
    OverviewImage oi = new OverviewImage();
    oi.setFilename("test.png");
    oi.setWidth(1000);
    oi.setHeight(1000);
    images.add(oi);

    OverviewImage oi2 = new OverviewImage();
    oi2.setFilename("test2.png");
    oi2.setWidth(1000);
    oi2.setHeight(1000);
    images.add(oi2);

    OverviewImage oi3 = new OverviewImage();
    oi3.setFilename("test2.png");
    oi3.setWidth(1000);
    oi3.setHeight(1000);
    images.add(oi3);

    parser.processCoordinates(models, images, invalidCoordinates);

    assertEquals(2, super.getWarnings().size());
  }

  @Test
  public void testJsonCoordinates() throws Exception {
    Set<Model> models = createValidTestMapModel();
    Model child = new ModelFullIndexed(null);
    child.setName("child");
    models.add(child);

    List<ImageZipEntryFile> imageEntries = createImageEntries("testFiles/valid_overview_with_json.zip");
    List<OverviewImage> result = parser.parseOverviewLinks(models, imageEntries, null, new ZipFile("testFiles/valid_overview_with_json.zip"));
    assertEquals(2, result.size());
    assertEquals(4, result.get(0).getLinks().size());
    assertNotNull(result.get(0).getHeight());
  }

}
