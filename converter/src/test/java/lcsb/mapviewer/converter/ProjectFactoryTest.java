package lcsb.mapviewer.converter;

import lcsb.mapviewer.converter.zip.ModelZipEntryFile;
import lcsb.mapviewer.converter.zip.ZipEntryFileFactory;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.model.map.OverviewLink;
import lcsb.mapviewer.model.map.OverviewModelLink;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.overlay.DataOverlay;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import java.awt.geom.Point2D;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ProjectFactoryTest extends ConverterTestFunctions {

  private final int elementCounter = 0;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
    MockConverter.modelToBeReturned = null;
  }

  @Test
  public void testOverviewImageLink() throws Exception {
    final ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    final ProjectFactory projectFactory = new ProjectFactory(converter);
    final ZipFile zipFile = new ZipFile("testFiles/complex_model_with_img.zip");
    final ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(zipFile);

    final ZipEntryFileFactory factory = new ZipEntryFileFactory();
    final Enumeration<? extends ZipEntry> entries = zipFile.entries();
    while (entries.hasMoreElements()) {
      final ZipEntry entry = entries.nextElement();
      if (!entry.isDirectory()) {
        params.entry(factory.createZipEntryFile(entry, zipFile));
      }
    }

    final Project project = projectFactory.create(params);
    assertNotNull(project);
    final Model model = project.getTopModel();
    assertEquals("main", model.getName());

    final List<OverviewImage> result = project.getOverviewImages();

    assertNotNull(result);
    assertEquals(1, result.size());

    final OverviewImage img = result.get(0);

    assertEquals("test.png", img.getFilename());
    assertEquals((Integer) 639, img.getHeight());
    assertEquals((Integer) 963, img.getWidth());
    assertEquals(2, img.getLinks().size());

    final OverviewLink link = img.getLinks().get(0);
    final List<Point2D> polygon = link.getPolygonCoordinates();
    assertEquals(4, polygon.size());

    assertTrue(link instanceof OverviewModelLink);

    final OverviewModelLink modelLink = (OverviewModelLink) link;
    assertEquals((Integer) 10, modelLink.getxCoord());
    assertEquals((Integer) 10, modelLink.getyCoord());
    assertEquals((Integer) 3, modelLink.getZoomLevel());
    assertEquals(model.getModelData(), modelLink.getLinkedModel());
  }

  @Test
  public void testOverviewImageLinkToSubmapPath() throws Exception {
    final ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    final ProjectFactory projectFactory = new ProjectFactory(converter);
    final ZipFile zipFile = new ZipFile("testFiles/complex_model_with_images_path.zip");
    final ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(zipFile);

    final ZipEntryFileFactory factory = new ZipEntryFileFactory();
    final Enumeration<? extends ZipEntry> entries = zipFile.entries();
    while (entries.hasMoreElements()) {
      final ZipEntry entry = entries.nextElement();
      if (!entry.isDirectory()) {
        params.entry(factory.createZipEntryFile(entry, zipFile));
      }
    }

    final Project project = projectFactory.create(params);
    assertNotNull(project);
    final Model model = project.getTopModel();
    assertEquals("main", model.getName());

    final List<OverviewImage> result = project.getOverviewImages();

    assertNotNull(result);
    assertEquals(2, result.size());
  }

  @Test
  public void testParseGlyphs() throws Exception {
    final ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    final ProjectFactory projectFactory = new ProjectFactory(converter);

    final ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(new ZipFile("testFiles/complex_with_glyphs.zip"));
    params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));

    final ZipFile zipFile = new ZipFile("testFiles/complex_with_glyphs.zip");

    final ZipEntryFileFactory factory = new ZipEntryFileFactory();
    final Enumeration<? extends ZipEntry> entries = zipFile.entries();
    while (entries.hasMoreElements()) {
      final ZipEntry entry = entries.nextElement();
      if (!entry.isDirectory()) {
        params.entry(factory.createZipEntryFile(entry, zipFile));
      }
    }

    final Project project = projectFactory.create(params);
    assertNotNull(project);
    assertEquals(1, project.getGlyphs().size());
  }

  @Test
  public void testParseGlyphsAndPutThemAsElementGlyphs() throws Exception {
    Model model = new ModelFullIndexed(null);
    final GenericProtein protein = createProtein();
    protein.setNotes("Glyph: glyphs/g1.png");
    model.addElement(protein);

    MockConverter.modelToBeReturned = model;

    final ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    final ProjectFactory projectFactory = new ProjectFactory(converter);

    final ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(new ZipFile("testFiles/complex_with_glyphs.zip"));
    params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));

    final ZipFile zipFile = new ZipFile("testFiles/complex_with_glyphs.zip");

    final ZipEntryFileFactory factory = new ZipEntryFileFactory();
    final Enumeration<? extends ZipEntry> entries = zipFile.entries();
    while (entries.hasMoreElements()) {
      final ZipEntry entry = entries.nextElement();
      if (!entry.isDirectory()) {
        params.entry(factory.createZipEntryFile(entry, zipFile));
      }
    }

    final Project project = projectFactory.create(params);
    assertNotNull(project);
    model = project.getModels().iterator().next().getModel();

    final GenericProtein fetchedProtein = (GenericProtein) model.getElements().iterator().next();

    assertFalse("Glyph field should be removed from notes", fetchedProtein.getNotes().contains("Glyph"));
    assertNotNull("Glyph in the protein is not defined but should", fetchedProtein.getGlyph());
  }

  @Test
  public void testParseGlyphsAndPutThemAsTextGlyphs() throws Exception {
    Model model = new ModelFullIndexed(null);
    final Layer layer = new Layer();

    final LayerText text = new LayerText();
    text.setNotes("Glyph: glyphs/g1.png");
    layer.addLayerText(text);
    model.addLayer(layer);

    MockConverter.modelToBeReturned = model;

    final ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    final ProjectFactory projectFactory = new ProjectFactory(converter);

    final ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(new ZipFile("testFiles/complex_with_glyphs.zip"));
    params.entry(new ModelZipEntryFile("main.xml", "main", true, false, null));

    final ZipFile zipFile = new ZipFile("testFiles/complex_with_glyphs.zip");

    final ZipEntryFileFactory factory = new ZipEntryFileFactory();
    final Enumeration<? extends ZipEntry> entries = zipFile.entries();
    while (entries.hasMoreElements()) {
      final ZipEntry entry = entries.nextElement();
      if (!entry.isDirectory()) {
        params.entry(factory.createZipEntryFile(entry, zipFile));
      }
    }

    final Project project = projectFactory.create(params);
    assertNotNull(project);
    model = project.getModels().iterator().next().getModel();

    final LayerText fetchedProtein = model.getLayers().iterator().next().getTexts().iterator().next();

    assertFalse("Glyph field should be removed from notes", fetchedProtein.getNotes().contains("Glyph"));
    assertNotNull("Glyph in the protein is not defined but should", fetchedProtein.getGlyph());
  }

  @Test
  public void testWithOverlay() throws Exception {
    final ComplexZipConverter converter = new ComplexZipConverter(MockConverter.class);
    final ProjectFactory projectFactory = new ProjectFactory(converter);
    final ZipFile zipFile = new ZipFile("testFiles/complex_model_with_layouts.zip");
    final ComplexZipConverterParams params = new ComplexZipConverterParams();
    params.zipFile(zipFile);

    final ZipEntryFileFactory factory = new ZipEntryFileFactory();
    final Enumeration<? extends ZipEntry> entries = zipFile.entries();
    while (entries.hasMoreElements()) {
      final ZipEntry entry = entries.nextElement();
      if (!entry.isDirectory()) {
        params.entry(factory.createZipEntryFile(entry, zipFile));
      }
    }

    final Project project = projectFactory.create(params);
    assertNotNull(project);
    assertEquals(1, project.getDataOverlays().size());
    final DataOverlay overlay = project.getDataOverlays().get(0);
    assertTrue(overlay.getEntries().size() > 0);
  }

}
