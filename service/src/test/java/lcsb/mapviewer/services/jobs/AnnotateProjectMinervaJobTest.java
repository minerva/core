package lcsb.mapviewer.services.jobs;

import lcsb.mapviewer.annotation.services.ModelAnnotator;
import lcsb.mapviewer.model.user.UserAnnotationSchema;
import lcsb.mapviewer.services.ServiceTestFunctions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;

public class AnnotateProjectMinervaJobTest extends ServiceTestFunctions {

  @Autowired
  private ModelAnnotator modelAnnotator;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testJobSerialization() {
    UserAnnotationSchema schema = modelAnnotator.createDefaultAnnotatorSchema();
    AnnotateProjectMinervaJob job = new AnnotateProjectMinervaJob("test", schema);

    AnnotateProjectMinervaJob job2 = new AnnotateProjectMinervaJob("test", new UserAnnotationSchema());

    job2.assignData(job.getJobParameters());

    UserAnnotationSchema serializedSchema = job2.getSchema();

    assertEquals(schema.getClassAnnotators().size(), serializedSchema.getClassAnnotators().size());
    assertEquals(job.getJobParameters(), job2.getJobParameters());
  }
}
