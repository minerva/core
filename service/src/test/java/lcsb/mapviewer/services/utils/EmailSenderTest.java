package lcsb.mapviewer.services.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.icegreen.greenmail.util.GreenMail;

import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.services.ServiceTestFunctions;

public class EmailSenderTest extends ServiceTestFunctions {

  private GreenMail mailServer;

  @Before
  public void setUp() throws Exception {
    mailServer = new GreenMail();
    mailServer.start();
    configurationService.setConfigurationValue(ConfigurationElementType.EMAIL_SMTP_SERVER, "127.0.0.1");
    configurationService.setConfigurationValue(ConfigurationElementType.EMAIL_SMTP_PORT, mailServer.getSmtp().getPort() + "");
    mailServer.setUser(configurationService.getConfigurationValue(ConfigurationElementType.EMAIL_LOGIN),
        configurationService.getConfigurationValue(ConfigurationElementType.EMAIL_PASSWORD));
  }

  @After
  public void tearDown() throws Exception {
    mailServer.stop();
    configurationService.setConfigurationValue(ConfigurationElementType.EMAIL_SMTP_SERVER, "");
  }

  @Test
  @Ignore("This is just a showcase")
  public void test() throws Exception {
    EmailSender emailSender = new EmailSender(configurationService);
    List<String> recipients = new ArrayList<>();
    recipients.add("piotr.gawron@uni.lu");
    emailSender.sendEmail(recipients, new ArrayList<>(), "Test subject", "Test content");

    MimeMessage[] emails = mailServer.getReceivedMessages();
    assertEquals(1, emails.length);
  }

  @Test
  public void testTooBigContent() throws Exception {
    EmailSender emailSender = new EmailSender(configurationService);
    List<String> recipients = new ArrayList<>();
    recipients.add("piotr.gawron@uni.lu");
    String content = StringUtils.repeat('x', 1024 * 64);

    emailSender.sendEmail(recipients, new ArrayList<>(), "Test subject", content);

    MimeMessage[] emails = mailServer.getReceivedMessages();
    assertEquals(1, emails.length);
    assertTrue(emails[0].getContent() instanceof MimeMultipart);
  }

  @Test
  public void testCc() throws Exception {
    EmailSender emailSender = new EmailSender(configurationService);
    List<String> recipients = new ArrayList<>();
    recipients.add("piotr.gawron@uni.lu");

    emailSender.sendEmail(recipients, Arrays.asList("p.gawron@atcomp.pl", "piotr.gawron@ext.uni.lu"), "Test subject", "xx");

    MimeMessage[] emails = mailServer.getReceivedMessages();
    assertEquals(3, emails.length);
  }
}
