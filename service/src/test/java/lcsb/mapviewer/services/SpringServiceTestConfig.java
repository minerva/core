package lcsb.mapviewer.services;

import lcsb.mapviewer.annotation.SpringAnnotationConfig;
import lcsb.mapviewer.persist.SpringPersistConfig;
import lcsb.mapviewer.services.websockets.IWebSocketMessenger;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.core.session.SessionRegistryImpl;

@Configuration
@Import({SpringServiceConfig.class, SpringPersistConfig.class, SpringAnnotationConfig.class})
public class SpringServiceTestConfig {

  @Bean
  public SessionRegistryImpl sessionRegistry() {
    return new SessionRegistryImpl();
  }

  @Bean
  public IWebSocketMessenger webSocketMessenger() {
    return Mockito.mock(IWebSocketMessenger.class);
  }

}
