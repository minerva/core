package lcsb.mapviewer.services;

import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.common.tests.UnitTestFailedWatcher;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.user.UserDao;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.ISearchService;
import lcsb.mapviewer.services.search.chemical.IChemicalService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LogEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Rollback(false)
@ContextConfiguration(classes = SpringServiceTestConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class ServiceTestFunctions {

  protected static Logger logger = LogManager.getLogger();

  public static final String ADMIN_BUILT_IN_LOGIN = "admin";
  public static final String BUILT_IN_MAP = "empty";

  public static final String TEST_PROJECT_ID = "Some_id";

  protected static String TEST_PROJECT_ID_2 = "Some_id2";

  protected static double EPSILON = 1e-6;

  private static final Map<String, Model> models = new HashMap<>();

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();

  @Autowired
  protected IConfigurationService configurationService;

  @Autowired
  protected PasswordEncoder passwordEncoder;

  @Autowired
  protected ISearchService searchService;

  @Autowired
  protected IChemicalService chemicalService;

  @Autowired
  protected IProjectService projectService;

  @Autowired
  protected ProjectDao projectDao;

  @Autowired
  protected IMinervaJobService minervaJobService;

  @Autowired
  protected UserDao userDao;

  @Autowired
  protected DbUtils dbUtils;
  private MinervaLoggerAppender appender;

  private String dapiLogin;
  private String dapiPassword;

  @Before
  public final void _setUp() throws Exception {
    minervaJobService.enableQueue(false);
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
    appender = MinervaLoggerAppender.createAppender();
    dbUtils.setAutoFlush(false);

    dapiLogin = System.getenv("DAPI_TEST_LOGIN");
    dapiPassword = System.getenv("DAPI_TEST_PASSWORD");
  }

  protected boolean isDapiConfigurationAvailable() {
    return dapiLogin != null && dapiPassword != null;
  }

  @After
  public final void _tearDown() throws Exception {
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
  }

  protected List<LogEvent> getWarnings() {
    return appender.getWarnings();
  }

  protected List<LogEvent> getErrors() {
    return appender.getErrors();
  }

  protected List<LogEvent> getInfos() {
    return appender.getInfos();
  }

  protected User createUser() {
    return createUser("john.doe");
  }

  protected User createUser(final String login) {
    User user = userDao.getUserByLogin(login);
    if (user != null) {
      logger.debug("remove user");
      userDao.delete(user);
      userDao.flush();
    }
    user = new User();
    user.setName("John");
    user.setSurname("Doe");
    user.setEmail("john.doe@uni.lu");
    user.setLogin(login);
    user.setCryptedPassword(passwordEncoder.encode("passwd"));
    userDao.add(user);
    return user;
  }

  protected Model getModelForFile(final String fileName, final boolean fromCache) throws Exception {
    if (!fromCache) {
      logger.debug("File without cache: " + fileName);
      return new CellDesignerXmlParser().createModel(new ConverterParams().filename(fileName));
    }
    Model result = ServiceTestFunctions.models.get(fileName);
    if (result == null) {
      logger.debug("File to cache: " + fileName);

      final CellDesignerXmlParser parser = new CellDesignerXmlParser();
      result = parser.createModel(new ConverterParams().filename(fileName).sizeAutoAdjust(false));
      ServiceTestFunctions.models.put(fileName, result);
    }
    return result;
  }

  public File createTempDirectory() throws IOException {
    final File temp;

    temp = File.createTempFile("temp", Long.toString(System.nanoTime()));

    if (!(temp.delete())) {
      throw new IOException("Could not delete temp file: " + temp.getAbsolutePath());
    }

    if (!(temp.mkdir())) {
      throw new IOException("Could not create temp directory: " + temp.getAbsolutePath());
    }

    return (temp);
  }

  private static int z = 0;

  protected static void assignCoordinates(final double x, final double y, final double width, final double height, final Element protein) {
    protein.setX(x);
    protein.setY(y);
    protein.setZ(z++);
    protein.setWidth(width);
    protein.setHeight(height);
    protein.setNameX(x);
    protein.setNameY(y);
    protein.setNameWidth(width);
    protein.setNameHeight(height);
    protein.setNameVerticalAlign(VerticalAlign.MIDDLE);
    protein.setNameHorizontalAlign(HorizontalAlign.CENTER);
  }

}
