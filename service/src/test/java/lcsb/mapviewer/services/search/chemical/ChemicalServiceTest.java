package lcsb.mapviewer.services.search.chemical;

import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.search.DbSearchCriteria;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

@Rollback(true)
@ActiveProfiles("ctdTestProfile")
public class ChemicalServiceTest extends ServiceTestFunctions {

  @Before
  public void setUp() throws Exception {
    assumeTrue("DAPI credentials are not provided", isDapiConfigurationAvailable());
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testgetChemicalByName() throws Exception {
    MiriamData parkinsonDisease = new MiriamData(MiriamType.MESH_2012, "D010300");
    String name = "GSSG";
    Chemical result = chemicalService.getByName(name, new DbSearchCriteria().disease(parkinsonDisease));
    assertNotNull(result);
    assertEquals("D019803", result.getChemicalId().getResource());

    for (final Target t : result.getTargets()) {
      assertEquals(t.getAssociatedDisease(), parkinsonDisease);
    }
    assertEquals("No warnings expected.", 0, getWarnings().size());
  }

  @Test
  public void testSearchByElements() throws Exception {
    List<Element> elements = new ArrayList<>();
    Rna protein = new Rna("id");
    protein.setName("RAB40AL");
    MiriamData retardationSyndrome = new MiriamData(MiriamType.MESH_2012, "C564495");

    elements.add(protein);

    List<Chemical> chemicals = chemicalService.getForTargets(elements,
        new DbSearchCriteria().disease(retardationSyndrome));
    assertNotNull(chemicals);
    assertTrue(chemicals.size() > 0);

    assertEquals("No warnings expected.", 0, getWarnings().size());
  }

  @Test
  public void testSearchByElementWithWeirdAnnotations() throws Exception {
    List<Element> elements = new ArrayList<>();
    Rna protein = new Rna("id");
    protein.setName("RAB40AL");
    for (final MiriamType mt : MiriamType.values()) {
      protein.addMiriamData(new MiriamData(mt, "RAB40AL"));
    }
    MiriamData retardationSyndrome = new MiriamData(MiriamType.MESH_2012, "C564495");

    elements.add(protein);

    List<Chemical> chemicals = chemicalService.getForTargets(elements,
        new DbSearchCriteria().disease(retardationSyndrome));
    assertNotNull(chemicals);
    assertTrue(chemicals.size() > 0);

    assertEquals("No warnings expected.", 0, getWarnings().size());
  }

}
