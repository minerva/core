package lcsb.mapviewer.services.search;

import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.annotation.data.Chemical;

public class DbSearchServiceTest {

  @Test
  public void testCacheMiriamData() throws Exception {
    Chemical chemical = new Chemical();
    DbSearchService service = Mockito.mock(DbSearchService.class, Mockito.CALLS_REAL_METHODS);
    service.cacheMiriamData(chemical);

  }

}
