package lcsb.mapviewer.services.search.drug;

import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.annotation.services.dapi.DrugBankParser;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.search.DbSearchCriteria;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

@ActiveProfiles("drugBankTestProfile")
public class DrugServiceTest extends ServiceTestFunctions {

  @Autowired
  private DrugBankParser drugBankParser;

  @Autowired
  private IDrugService drugService;

  @Before
  public void setUp() throws Exception {
    assumeTrue("DAPI credentials are not provided", isDapiConfigurationAvailable());
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetEmpty() {
    final Drug drug = drugService.getByName("blablablabla", new DbSearchCriteria());
    assertNull(drug);
  }

  @Test
  public void testGetIstodax() {
    final Drug drug = drugService.getByName("istodax", new DbSearchCriteria());
    assertEquals(2, drug.getSources().size());
    assertNotNull(drug.getSources().get(0).getLink());
    assertNotNull(drug.getTargets().get(0).getGenes().get(0).getLink());
  }

  @Test
  public void testGetTargetsBySynonym() {
    // search by synonym
    final Drug drug = drugService.getByName("Amantidine", new DbSearchCriteria().ipAddress("ip"));
    // search by name
    final Drug drug2 = drugService.getByName("Amantadine", new DbSearchCriteria().ipAddress("ip"));
    assertNotNull(drug);
    assertNotNull(drug.getName());
    assertNotEquals("", drug.getName().trim());
    assertNotNull(drug.getDescription());
    assertNotEquals("", drug.getDescription().trim());

    // number of targets should be the same
    assertEquals(drug.getTargets().size(), drug2.getTargets().size());
    assertFalse(drug.getTargets().isEmpty());

  }

  @Test
  public void testFindDrugSelegiline() {
    final Drug selegilineDrug = drugService.getByName("Selegiline",
        new DbSearchCriteria().ipAddress("ip").organisms(TaxonomyBackend.HUMAN_TAXONOMY));
    assertNotNull(selegilineDrug);

    assertNotNull(selegilineDrug.getTargets().get(0).getReferences().get(0).getLink());

  }

  @Test
  public void testAnnotationsInDrug() {
    final Drug test = drugService.getByName("Selegiline", new DbSearchCriteria().organisms(TaxonomyBackend.HUMAN_TAXONOMY));
    assertNotNull(test);
    for (final Target target : test.getTargets()) {
      for (final MiriamData md : target.getGenes()) {
        assertNull(md.getAnnotator());
      }
    }
  }

  @Test
  public void testDornaseAlpha() {
    final Drug drug = drugService.getByName("Dornase alpha", new DbSearchCriteria());
    assertNotNull(drug.getName());
    assertEquals("N/A", drug.getBloodBrainBarrier());
  }

  @Test
  public void testSearchByElements() {
    final List<Element> elements = new ArrayList<>();
    List<Drug> drugs = drugService.getForTargets(elements, new DbSearchCriteria());
    assertNotNull(drugs);
    assertEquals(0, drugs.size());

    final Protein protein = new GenericProtein("id");
    protein.setName("DRD2");
    protein.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SIRT3"));
    elements.add(protein);

    drugs = drugService.getForTargets(elements, new DbSearchCriteria());
    assertNotNull(drugs);
    assertFalse(drugs.isEmpty());
  }

  @Test
  public void testSearchByElements3() {
    final List<Element> elements = new ArrayList<>();
    final Protein protein = new GenericProtein("id");
    protein.setName("SIRT3");
    protein.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SIRT3"));
    protein.addMiriamData(new MiriamData(MiriamType.ENTREZ, "23410"));
    elements.add(protein);
    final List<Drug> drugs = drugService.getForTargets(elements, new DbSearchCriteria());

    assertNotNull(drugs);
    assertFalse(drugs.isEmpty());
  }

  @Test
  public void testObjectToDrugTargetList() throws Exception {
    final Drug drug = drugService.getByName("AMANTADINE", new DbSearchCriteria());
    final Drug drug2 = drugBankParser.findDrug("AMANTADINE");

    assertTrue(drug2.getTargets().size() <= drug.getTargets().size());
  }

  @Test
  public void testAspirinToDrugTargetList() throws Exception {
    final Drug drug = drugService.getByName("Aspirin", new DbSearchCriteria());
    final Drug drug2 = drugBankParser.findDrug("Aspirin");

    assertTrue(drug2.getTargets().size() <= drug.getTargets().size());
  }

  @Test
  public void testAspirinSynonyms() {
    final Drug drug = drugService.getByName("Aspirin", new DbSearchCriteria());

    final Set<String> synonyms = new HashSet<>();
    for (final String string : drug.getSynonyms()) {
      assertFalse("Duplicate entry in drug synonym: " + string, synonyms.contains(string));
      synonyms.add(string);
    }
  }

  @Test
  public void testGetSuggestedQueryList() throws Exception {
    final List<String> result = drugService.getSuggestedQueryList(new Project(), null);
    assertEquals(0, result.size());
  }

}
