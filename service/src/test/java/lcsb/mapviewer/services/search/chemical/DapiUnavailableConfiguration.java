package lcsb.mapviewer.services.search.chemical;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import lcsb.mapviewer.annotation.services.dapi.DapiConnectionException;
import lcsb.mapviewer.annotation.services.dapi.DapiConnector;
import lcsb.mapviewer.annotation.services.dapi.DapiConnectorImpl;
import lcsb.mapviewer.persist.dao.ConfigurationDao;

@Profile("dapiUnavailableTestProfile")
@Configuration
public class DapiUnavailableConfiguration {

  @Autowired
  private ConfigurationDao configurationDao;

  public DapiUnavailableConfiguration() {
  }

  @Bean
  @Primary
  public DapiConnector dapiConnector() throws DapiConnectionException {
    DapiConnectorImpl mock = Mockito.spy(new DapiConnectorImpl(configurationDao));

    Mockito.doReturn(false).when(mock).isValidConnection();

    Mockito.doReturn("").when(mock).getDapiLogin();
    Mockito.doReturn("").when(mock).getDapiPassword();
    return mock;
  }

}
