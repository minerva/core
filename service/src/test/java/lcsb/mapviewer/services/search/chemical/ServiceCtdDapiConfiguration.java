package lcsb.mapviewer.services.search.chemical;

import static org.mockito.ArgumentMatchers.anyString;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import lcsb.mapviewer.annotation.services.dapi.DapiConnectionException;
import lcsb.mapviewer.annotation.services.dapi.DapiConnector;
import lcsb.mapviewer.annotation.services.dapi.DapiConnectorImpl;
import lcsb.mapviewer.persist.dao.ConfigurationDao;

@Profile("ctdTestProfile")
@Configuration
public class ServiceCtdDapiConfiguration {

  @Autowired
  private ConfigurationDao configurationDao;

  private String dapiLogin;
  private String dapiPassword;

  public ServiceCtdDapiConfiguration() {
    dapiLogin = System.getenv("DAPI_TEST_LOGIN");
    dapiPassword = System.getenv("DAPI_TEST_PASSWORD");
  }

  @Bean
  @Primary
  public DapiConnector dapiConnector() throws DapiConnectionException {
    DapiConnectorImpl mock = Mockito.spy(new DapiConnectorImpl(configurationDao));
    Mockito.doReturn(dapiLogin).when(mock).getDapiLogin();
    Mockito.doReturn(dapiPassword).when(mock).getDapiPassword();

    Mockito.doReturn("https://dapi.lcsb.uni.lu/api/database/CTD/releases/2019.02/").when(mock)
        .getLatestReleaseUrl(anyString());

    Mockito.doReturn(true).when(mock).isValidConnection();

    return mock;
  }

}
