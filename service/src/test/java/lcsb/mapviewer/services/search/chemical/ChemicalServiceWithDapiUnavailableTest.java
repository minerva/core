package lcsb.mapviewer.services.search.chemical;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;

import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.search.DbSearchCriteria;

@Rollback(true)
@ActiveProfiles("dapiUnavailableTestProfile")
public class ChemicalServiceWithDapiUnavailableTest extends ServiceTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testgetChemicalByName() throws Exception {
    MiriamData pdDiseaseID = new MiriamData(MiriamType.MESH_2012, "D010300");
    String name = "GSSG";
    Chemical result = chemicalService.getByName(name, new DbSearchCriteria().disease(pdDiseaseID));

    assertNull(result);
  }

  @Test
  public void testSearchByElements() throws Exception {
    List<Element> elements = new ArrayList<>();
    Rna protein = new Rna("id");
    protein.setName("RAB40AL");
    MiriamData retardationSyndrome = new MiriamData(MiriamType.MESH_2012, "C564495");

    elements.add(protein);

    List<Chemical> chemicals = chemicalService.getForTargets(elements,
        new DbSearchCriteria().disease(retardationSyndrome));
    assertNotNull(chemicals);
    assertEquals(0, chemicals.size());
  }

}
