package lcsb.mapviewer.services;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import lcsb.mapviewer.services.utils.EmailSender;

@Profile("emailTestProfile")
@Configuration
public class EmailMockProfile {

  public EmailMockProfile() {
  }

  private static EmailSender sender;

  @Bean
  @Primary
  public EmailSender emailSender() {
    if (sender == null) {
      sender = Mockito.mock(EmailSender.class);
    }
    return sender;
  }

}
