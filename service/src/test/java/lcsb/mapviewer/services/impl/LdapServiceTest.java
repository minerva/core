package lcsb.mapviewer.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.test.annotation.Rollback;

import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.sdk.Filter;
import com.unboundid.ldap.sdk.LDAPConnection;

import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.UserDTO;

@Rollback(true)
public class LdapServiceTest extends ServiceTestFunctions {

  private LdapService ldapService;

  @Before
  public void setUp() throws Exception {
    configurationService.setConfigurationValue(ConfigurationElementType.LDAP_BASE_DN, "dc=uni,dc=lu");
    configurationService.setConfigurationValue(ConfigurationElementType.LDAP_UID, "uid");
    configurationService.setConfigurationValue(ConfigurationElementType.LDAP_OBJECT_CLASS, "person");
    configurationService.setConfigurationValue(ConfigurationElementType.LDAP_FILTER,
        "memberof=cn=gitlab,cn=groups,cn=accounts,dc=uni,dc=lu");

    ldapService = Mockito.spy(new LdapService(null));
    ldapService.setConfigurationService(configurationService);
    Mockito.when(ldapService.getConnection()).thenAnswer(new Answer<LDAPConnection>() {

      @Override
      public LDAPConnection answer(final InvocationOnMock invocation) throws Throwable {
        // Create the configuration to use for the server.
        InMemoryDirectoryServerConfig config = new InMemoryDirectoryServerConfig("dc=uni,dc=lu");
        config.addAdditionalBindCredentials("uid=piotr.gawron,cn=users,cn=accounts,dc=uni,dc=lu", "test_passwd");
        config.addAdditionalBindCredentials("uid=UPPER,cn=users,cn=accounts,dc=uni,dc=lu", "test_passwd");
        config.setSchema(null);

        // Create the directory server instance, populate it with data from the
        // "test-data.ldif" file, and start listening for client connections.
        InMemoryDirectoryServer ds = new InMemoryDirectoryServer(config);
        ds.importFromLDIF(true, "testFiles/ldap/testdata.ldif");
        ds.startListening();
        return ds.getConnection();
      }
    });
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testIsValidConfiguration() throws Exception {
    assertTrue(ldapService.isValidConfiguration());
    configurationService.setConfigurationValue(ConfigurationElementType.LDAP_BASE_DN, "");
    assertFalse(ldapService.isValidConfiguration());
  }

  @Test
  public void testCreateAttributeFilterForEmpty() throws Exception {
    configurationService.setConfigurationValue(ConfigurationElementType.LDAP_FILTER, "");
    Filter filter = ldapService.createAttributeFilter();
    assertNotNull(filter);
  }

  @Test
  public void testLogin() throws Exception {
    assertTrue(ldapService.login("piotr.gawron", "test_passwd"));
    assertFalse(ldapService.login("piotr.gawron", "invalid_password"));
  }

  @Test
  public void testLoginCaseInsensitive() throws Exception {
    assertTrue(ldapService.login("upper", "test_passwd"));
    UserDTO user = ldapService.getUserByLogin("upper");
    assertEquals("upper", user.getLogin());
  }

  @Test
  public void testGetUsernames() throws Exception {
    List<String> list = ldapService.getUsernames();
    assertEquals(3, list.size());
    assertTrue(list.contains("piotr.gawron"));
    assertFalse(list.contains("john.doe"));
  }

  @Test
  public void testGetUsernamesWithFiltering() throws Exception {
    configurationService.setConfigurationValue(ConfigurationElementType.LDAP_FILTER,
        "(memberof=cn=owncloud,cn=groups,cn=accounts,dc=uni,dc=lu)");

    List<String> list = ldapService.getUsernames();
    assertEquals(1, list.size());
    assertTrue(list.contains("piotr.gawron"));
  }

  @Test
  public void testGetUserByLogin() throws Exception {
    UserDTO user = ldapService.getUserByLogin("piotr.gawron");
    assertNotNull(user);
    assertEquals("Piotr", user.getFirstName());
    assertEquals("Gawron", user.getLastName());
    assertEquals("piotr.gawron", user.getLogin());
    assertEquals("piotr.gawron@uni.lu", user.getEmail());
    assertNotNull(user.getBindDn());
  }

}
