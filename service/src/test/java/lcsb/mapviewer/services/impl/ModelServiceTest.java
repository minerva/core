package lcsb.mapviewer.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.interfaces.IModelService;

public class ModelServiceTest extends ServiceTestFunctions {

  @Autowired
  private IModelService modelService;

  @Test
  public void testGetModelsByMapIdForAllMapId() throws Exception {
    List<ModelData> models = modelService.getModelsByMapId(BUILT_IN_MAP, "*");
    for (final ModelData modelData : models) {
      assertNotNull(modelData);
    }
    assertEquals(1, models.size());
  }

}
