package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.model.License;
import lcsb.mapviewer.persist.dao.LicenseDao;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.interfaces.ILicenseService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.Rollback;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@Rollback(true)
public class LicenseServiceTest extends ServiceTestFunctions {

  @Autowired
  private ILicenseService licenseServce;

  @Autowired
  private LicenseDao licenseDao;

  @Test
  public void testGetAll() throws Exception {
    final Page<License> page = licenseServce.getByFilter(Pageable.unpaged());

    assertTrue(page.getContent().size() > 1);
    assertTrue(page.getTotalElements() >= page.getContent().size());

  }

  @Test
  public void testGetById() throws Exception {
    final License license = licenseDao.add(new License("x", "y", "google.com"));
    assertNotNull(licenseServce.getById(license.getId()));
  }

  @Test
  public void testGetByInvalidId() throws Exception {
    assertNull(licenseServce.getById(-1));

  }

}
