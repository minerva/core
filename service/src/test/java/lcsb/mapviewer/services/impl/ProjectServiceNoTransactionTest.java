package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.commands.CopyCommand;
import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.common.tests.TestUtils;
import lcsb.mapviewer.common.tests.UnitTestFailedWatcher;
import lcsb.mapviewer.converter.ComplexZipConverter;
import lcsb.mapviewer.converter.ComplexZipConverterParams;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.graphics.MapGenerator;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.converter.zip.ModelZipEntryFile;
import lcsb.mapviewer.converter.zip.ZipEntryFile;
import lcsb.mapviewer.converter.zip.ZipEntryFileFactory;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectLogEntry;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.ProjectStatus;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.job.MinervaJobStatus;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.model.map.OverviewImageLink;
import lcsb.mapviewer.model.map.OverviewLink;
import lcsb.mapviewer.model.map.layout.ProjectBackground;
import lcsb.mapviewer.model.map.layout.ProjectBackgroundImageLayer;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.ArticleProperty;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.ProjectLogEntryDao;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.SpringServiceTestConfig;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IDataOverlayService;
import lcsb.mapviewer.services.interfaces.IFileService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IPublicationService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.services.utils.CreateProjectParams;
import lcsb.mapviewer.services.utils.data.BuildInBackgrounds;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.impl.Log4jLogEvent;
import org.apache.logging.log4j.message.SimpleMessage;
import org.apache.poi.util.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.CALLS_REAL_METHODS;

@ContextConfiguration(classes = SpringServiceTestConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class ProjectServiceNoTransactionTest extends TestUtils {

  private final String tmpResultDir = "tmp/";
  private final ZipEntryFileFactory zefFactory = new ZipEntryFileFactory();
  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();
  @Autowired
  protected IFileService fileService;
  @Autowired
  private IProjectService projectService;
  @Autowired
  private IModelService modelService;
  @Autowired
  private IDataOverlayService dataOverlayService;
  @Autowired
  private IMinervaJobService minervaJobService;
  @Autowired
  private IConfigurationService configurationService;
  @Autowired
  private IUserService userService;
  @Autowired
  private IPublicationService publicationService;

  @Before
  public void setUp() throws Exception {
    minervaJobService.enableQueue(false);
  }

  @After
  public void tearDown() throws Exception {
    if (projectService.getProjectByProjectId(TEST_PROJECT_ID) != null) {
      projectService.removeProject(TEST_PROJECT_ID, tmpResultDir);
    }
    minervaJobService.waitForTasksToFinish();
  }

  @Test
  public void testUpdater() throws Exception {
    projectService.submitCreateProjectJob(createProjectParams("testFiles/centeredAnchorInModifier.xml"));
    minervaJobService.waitForTasksToFinish();

    Project project = projectService.getProjectByProjectId(TEST_PROJECT_ID);
    assertNotNull(project);
    assertEquals(ProjectStatus.DONE, project.getStatus());
  }

  @Test
  public void testSubmitCreateProjectJob() throws Exception {
    projectService.submitCreateProjectJob(createProjectParams("testFiles/centeredAnchorInModifier.xml"));
    minervaJobService.waitForTasksToFinish();

    Project project = projectService.getProjectByProjectId(TEST_PROJECT_ID);

    assertNotNull(project);
    assertEquals(ProjectStatus.DONE, project.getStatus());
  }

  @Test
  public void testCreateWithAutoResize() throws Exception {
    projectService.submitCreateProjectJob(createProjectParams("testFiles/centeredAnchorInModifier.xml")
        .autoResize(false));
    minervaJobService.waitForTasksToFinish();

    Project project = projectService.getProjectByProjectId(TEST_PROJECT_ID, true);

    Model model = modelService.getAndFetchModelByMapId(TEST_PROJECT_ID, project.getTopModelData().getId());
    assertEquals(600, model.getWidth(), EPSILON);
  }

  @Test
  public void testRemoveProject() throws Exception {
    int initCount = projectService.getAllProjects().size();

    projectService.submitCreateProjectJob(createProjectParams("testFiles/centeredAnchorInModifier.xml"));
    minervaJobService.waitForTasksToFinish();

    assertEquals(initCount + 1, projectService.getAllProjects().size());

    projectService.removeProject(TEST_PROJECT_ID, null);
    assertEquals(initCount, projectService.getAllProjects().size());
  }

  @Test
  public void testSubmitRemoveProject() throws Exception {
    int initCount = projectService.getAllProjects().size();

    projectService.submitCreateProjectJob(createProjectParams("testFiles/centeredAnchorInModifier.xml"));
    minervaJobService.waitForTasksToFinish();

    assertEquals(initCount + 1, projectService.getAllProjects().size());

    projectService.submitRemoveProjectJob(projectService.getProjectByProjectId(TEST_PROJECT_ID));

    minervaJobService.waitForTasksToFinish();
    assertEquals(initCount, projectService.getAllProjects().size());

  }

  private CreateProjectParams createProjectParams(final String filename) throws IOException {
    InputStream is = Files.newInputStream(Paths.get(filename));
    return createProjectParams(filename, is);
  }

  private CreateProjectParams createProjectParams(final String filename, final InputStream is) throws IOException {
    UploadedFileEntry fileEntry = new UploadedFileEntry();
    fileEntry.setLocalPath(filename);
    byte[] data = IOUtils.toByteArray(is);
    fileEntry.setLength(data.length);
    fileEntry.setFileContent(data);

    fileService.add(fileEntry);

    return new CreateProjectParams()
        .setUser(userService.getUserByLogin(ServiceTestFunctions.ADMIN_BUILT_IN_LOGIN))
        .projectId(TEST_PROJECT_ID)
        .version("")
        .complex(filename.endsWith("zip"))
        .projectFile(fileEntry.getId())
        .annotations(false)
        .images(false)
        .projectDir(tmpResultDir)
        .parser(new CellDesignerXmlParser());
  }

  @Test
  public void testProjectFailure() {
    Project project = new Project(TEST_PROJECT_ID);
    project.setOwner(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    projectService.add(project);

    projectService.projectFailure(TEST_PROJECT_ID, ProjectLogEntryType.CANNOT_FIND_INFORMATION, "problem", new Exception());
    minervaJobService.waitForTasksToFinish();

    project = projectService.getProjectByProjectId(TEST_PROJECT_ID, true);
    assertEquals(1, project.getLogEntries().size());

    assertEquals(ProjectLogEntryType.CANNOT_FIND_INFORMATION, project.getLogEntries().iterator().next().getType());
  }

  @Test
  public void uploadTooBigFile() throws Exception {
    int value = Integer
        .parseInt(configurationService.getValue(ConfigurationElementType.MAX_NUMBER_OF_MAP_LEVELS).getValue());
    double size = MapGenerator.TILE_SIZE * Math.pow(2, value) + 1;
    Model model = new ModelFullIndexed(null);
    model.setWidth(size);
    model.setHeight(size);
    String xml = new CellDesignerXmlParser().model2String(model);
    InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

    CreateProjectParams params = createProjectParams("sample.xml", is);

    projectService.submitCreateProjectJob(params);
    minervaJobService.waitForTasksToFinish();

    Project project = projectService.getProjectByProjectId(TEST_PROJECT_ID, true);
    assertEquals(ProjectStatus.FAIL, project.getStatus());
    assertTrue(project.getLogEntries().stream().anyMatch(projectLogEntry -> projectLogEntry.getSeverity().equalsIgnoreCase("ERROR")));
  }

  @Test(timeout = 30000)
  public void testCreateComplexWithLayouts() throws Exception {
    String filename = "testFiles/complexModel/complex_model_with_layouts.zip";
    int overlaysInZipFile = 1;
    Project project = createComplexProject(TEST_PROJECT_ID, filename);
    List<ProjectBackground> backgrounds = projectService.getBackgrounds(TEST_PROJECT_ID, true);
    List<DataOverlay> dataOverlays = dataOverlayService.getDataOverlaysByProject(project, true);

    ModelData model = modelService.getModelByMapId(TEST_PROJECT_ID, project.getTopModelData().getId());
    assertNotNull(model);
    assertEquals("main", model.getName());
    assertEquals(ProjectStatus.DONE, project.getStatus());
    assertEquals("Number of backgrounds doesn't match", BuildInBackgrounds.values().length, backgrounds.size());
    assertEquals("Number of data overlays doesn't match", overlaysInZipFile, dataOverlays.size());

    boolean additionalFound = false;

    for (final DataOverlay layout : dataOverlays) {
      if ("example name".equals(layout.getName())) {
        additionalFound = true;
        assertEquals("layout description", layout.getDescription());
      }
    }
    assertTrue("Cannot find layout with predefined name from file", additionalFound);
  }

  protected Project createComplexProject(final String projectId, final String filename) throws IOException, SecurityException {
    CreateProjectParams params = createProjectParams(filename);

    ZipFile zipFile = new ZipFile(filename);
    Enumeration<? extends ZipEntry> entries = zipFile.entries();
    while (entries.hasMoreElements()) {
      ZipEntry entry = entries.nextElement();
      ZipEntryFile e = zefFactory.createZipEntryFile(entry, zipFile);
      params.addZipEntry(e);
    }
    zipFile.close();

    projectService.submitCreateProjectJob(params);
    minervaJobService.waitForTasksToFinish();
    return projectService.getProjectByProjectId(projectId, true);
  }

  @Test
  public void testCreateComplexWithLayoutOrder() throws Exception {
    String filename = "testFiles/complexModel/complex_model_with_images.zip";
    CreateProjectParams params = createProjectParams(filename);

    ZipFile zipFile = new ZipFile(filename);
    Enumeration<? extends ZipEntry> entries = zipFile.entries();
    while (entries.hasMoreElements()) {
      ZipEntry entry = entries.nextElement();
      params.addZipEntry(zefFactory.createZipEntryFile(entry, zipFile));
    }
    zipFile.close();

    projectService.submitCreateProjectJob(params.networkBackgroundAsDefault(true));
    minervaJobService.waitForTasksToFinish();

    Project project = projectService.getProjectByProjectId(TEST_PROJECT_ID);

    List<ProjectBackground> backgrounds = projectService.getBackgrounds(TEST_PROJECT_ID, true);

    ModelData model = modelService.getModelByMapId(TEST_PROJECT_ID, project.getTopModelData().getId());
    for (OverviewImage oi : project.getOverviewImages()) {
      for (OverviewLink ol : oi.getLinks()) {
        if (ol instanceof OverviewImageLink) {
          logger.debug(((OverviewImageLink) ol).getLinkedOverviewImage());
        }
      }
    }
    assertNotNull(model);
    assertEquals("main", model.getName());
    assertEquals(ProjectStatus.DONE, project.getStatus());
    assertEquals("Cannot find overview images the model", 2, project.getOverviewImages().size());
    assertEquals("Number of layouts doesn't match", BuildInBackgrounds.values().length,
        backgrounds.size());
    assertEquals(BuildInBackgrounds.NESTED.getName(), backgrounds.get(1).getName());
    assertEquals(BuildInBackgrounds.NORMAL.getName(), backgrounds.get(0).getName());
  }

  @Test
  public void testAsyncRefresh() throws Exception {
    long initialCount = minervaJobService.getCount(MinervaJobStatus.DONE);

    projectService.submitCreateProjectJob(createProjectParams("testFiles/centeredAnchorInModifier.xml"));
    projectService.getProjectByProjectId(TEST_PROJECT_ID);
    minervaJobService.waitForTasksToFinish();

    assertTrue(minervaJobService.getCount(MinervaJobStatus.DONE) > initialCount);
  }

  @Test
  public void testCreateEmptyComplex2() throws Exception {
    String filename = "testFiles/complexModel/empty_complex_model2.zip";
    Project project = createComplexProject(TEST_PROJECT_ID, filename);

    ModelData model = modelService.getModelByMapId(TEST_PROJECT_ID, project.getTopModelData().getId());
    assertNotNull(model);
    assertEquals(ProjectStatus.DONE, project.getStatus());
  }

  @Test
  public void testCreateEmptyComplex3() throws Exception {
    String filename = "testFiles/complexModel/empty_complex_model3.zip";
    Project project = createComplexProject(TEST_PROJECT_ID, filename);

    ModelData model = modelService.getModelByMapId(TEST_PROJECT_ID, project.getTopModelData().getId());
    assertNotNull(model);
    assertEquals(ProjectStatus.DONE, project.getStatus());
  }

  @Test
  public void testUpdateProjectWithoutModel() {
    Project project = new Project();
    project.setProjectId(TEST_PROJECT_ID);
    project.setOwner(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    projectService.add(project);
    projectService.update(project);
  }

  @Test
  public void testCreateComplexWithImages() throws Exception {
    String filename = "testFiles/complexModel/complex_model_with_images.zip";
    Project project = createComplexProject(TEST_PROJECT_ID, filename);

    List<ProjectBackground> backgrounds = projectService.getBackgrounds(TEST_PROJECT_ID, true);

    ModelData model = modelService.getModelByMapId(TEST_PROJECT_ID, project.getTopModelData().getId());
    assertNotNull(model);
    assertEquals("main", model.getName());
    assertEquals(ProjectStatus.DONE, project.getStatus());
    assertEquals("Cannot find overview images the model", 2, project.getOverviewImages().size());
    assertEquals("Number of layouts doesn't match", BuildInBackgrounds.values().length, backgrounds.size());
    assertEquals(BuildInBackgrounds.NESTED.getName(), backgrounds.get(0).getName());
    assertEquals(BuildInBackgrounds.NORMAL.getName(), backgrounds.get(1).getName());
  }

  @Test
  public void testGetAllProjects() {
    List<Project> projects = projectService.getAllProjects();

    Project project = new Project();
    project.setProjectId(TEST_PROJECT_ID);
    project.setOwner(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));

    projectService.add(project);

    List<Project> projects2 = projectService.getAllProjects();

    assertEquals(projects.size() + 1, projects2.size());

    Project project2 = projectService.getProjectByProjectId(TEST_PROJECT_ID);
    assertNotNull(project2);
    assertNotEquals(project2, project);
    assertEquals(project.getId(), project2.getId());
  }

  @Test
  public void testCreateComplexWithSubMaps() throws Exception {
    String filename = "testFiles/complexModel/complex_model_with_submaps.zip";
    Project project = createComplexProject(TEST_PROJECT_ID, filename);

    List<ProjectBackground> backgrounds = projectService.getBackgrounds(TEST_PROJECT_ID, true);

    ModelData model = modelService.getModelByMapId(TEST_PROJECT_ID, project.getTopModelData().getId());
    assertNotNull(model);
    assertEquals("main", model.getName());
    assertEquals(ProjectStatus.DONE, project.getStatus());
    assertEquals("Number of layouts doesn't match", BuildInBackgrounds.values().length, backgrounds.size());
  }

  @Test
  public void testCreateEmptyComplex() throws Exception {
    String filename = "testFiles/complexModel/empty_complex_model.zip";
    Project project = createComplexProject(TEST_PROJECT_ID, filename);

    ModelData model = modelService.getModelByMapId(TEST_PROJECT_ID, project.getTopModelData().getId());
    assertNotNull(model);
    assertEquals(ProjectStatus.DONE, project.getStatus());
  }

  @Test
  public void testGenerateImages() throws Exception {
    File f = createTempDirectory();
    String path = f.getAbsolutePath() + "/" + tmpResultDir;
    try {
      CreateProjectParams params = createProjectParams("testFiles/sample.xml")
          .images(true)
          .projectDir(path);

      projectService.submitCreateProjectJob(params);

      minervaJobService.waitForTasksToFinish();

      List<ProjectBackground> backgrounds = projectService.getBackgrounds(TEST_PROJECT_ID, true);
      for (ProjectBackground projectBackground : backgrounds) {
        for (ProjectBackgroundImageLayer layer : projectBackground.getProjectBackgroundImageLayer()) {
          File tmp = new File(path + "/" + layer.getDirectory());
          assertTrue(tmp.exists());
        }
      }
    } finally {
      //noinspection ResultOfMethodCallIgnored
      f.delete();
    }
  }

  public File createTempDirectory() throws IOException {
    final File temp;

    temp = File.createTempFile("temp", Long.toString(System.nanoTime()));

    if (!(temp.delete())) {
      throw new IOException("Could not delete temp file: " + temp.getAbsolutePath());
    }

    if (!(temp.mkdir())) {
      throw new IOException("Could not create temp directory: " + temp.getAbsolutePath());
    }

    return (temp);
  }

  @Test
  public void testAnnotations() throws Exception {
    projectService.submitCreateProjectJob(
        createProjectParams("testFiles/sample.xml")
            .annotations(true));
    minervaJobService.waitForTasksToFinish();

    Project project = projectService.getProjectByProjectId(TEST_PROJECT_ID);

    Model model = modelService.getAndFetchModelByMapId(TEST_PROJECT_ID, project.getTopModelData().getId());

    int count = 0;
    boolean linksFound = false;
    for (BioEntity bioEntity : model.getBioEntities()) {
      for (MiriamData miriamData : bioEntity.getMiriamData()) {
        linksFound |= miriamData.getLink() != null;
        if (miriamData.getDataType() == MiriamType.PUBMED) {
          Map<ArticleProperty, Object> filter = new HashMap<>();
          filter.put(ArticleProperty.PUBMED_ID, miriamData.getResource());
          assertTrue(publicationService.getCount(filter) > 0);
        }
      }
      count += bioEntity.getMiriamData().size();
    }
    assertTrue(count > 16);
    assertTrue(linksFound);
  }

  @Test
  public void testGenerateImagesInSubmodels() throws Exception {
    User user = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    File f = createTempDirectory();
    String path = f.getAbsolutePath() + "/" + tmpResultDir;
    CreateProjectParams params = createProjectParams("testFiles/sample.xml").images(true).projectDir(tmpResultDir);

    Project project = new Project(params.getProjectId());
    project.setOwner(user);
    try {
      Model model = new CellDesignerXmlParser().createModel(new ConverterParams().filename("testFiles/sample.xml"));
      Model model2 = new CellDesignerXmlParser().createModel(new ConverterParams().filename("testFiles/sample.xml"));

      project.addModel(model);
      project.addModel(model2);
      project.setTopModel(model);

      ProjectBackground background1 = new ProjectBackground(BuildInBackgrounds.NORMAL.getName());
      background1.setCreator(user);
      project.addProjectBackground(background1);
      ProjectBackground background2 = new ProjectBackground(BuildInBackgrounds.NESTED.getName());
      background2.setCreator(user);
      project.addProjectBackground(background2);

      model.addSubmodelConnection(new ModelSubmodelConnection(model2, SubmodelType.UNKNOWN, "name"));

      background1.addProjectBackgroundImageLayer(new ProjectBackgroundImageLayer(model, path + "/normal_A"));
      background2.addProjectBackgroundImageLayer(new ProjectBackgroundImageLayer(model2, path + "/nested_B"));

      ProjectDao mockProjectDao = Mockito.mock(ProjectDao.class);
      Mockito.when(mockProjectDao.getProjectByProjectId(anyString())).thenReturn(project);

      ProjectService projectService = new ProjectService(null, mockProjectDao,
          Mockito.mock(ProjectLogEntryDao.class),
          null, null, null, null, null, null, null, null, Mockito.mock(ProjectBackgroundService.class), null, null);
      ReflectionTestUtils.setField(projectService, "self", projectService);

      projectService.createImages(params);

      File tmp = new File(path + "/normal_A");
      assertTrue(tmp.exists());

      tmp = new File(path + "/nested_B");
      assertTrue(tmp.exists());
    } finally {
      //noinspection ResultOfMethodCallIgnored
      f.delete();
    }

  }


  @Test
  public void testCopyComplexWithCompartments() throws Exception {
    ZipEntryFile entry1 = new ModelZipEntryFile("main.xml", "main", true, false, SubmodelType.UNKNOWN);
    ZipEntryFile entry2 = new ModelZipEntryFile("s1.xml", "s1", false, false, SubmodelType.UNKNOWN);
    ZipEntryFile entry3 = new ModelZipEntryFile("s2.xml", "s2", false, false, SubmodelType.UNKNOWN);
    ZipEntryFile entry4 = new ModelZipEntryFile("s3.xml", "s3", false, false, SubmodelType.UNKNOWN);
    ZipEntryFile entry5 = new ModelZipEntryFile("mapping.xml", "mapping", false, true, SubmodelType.UNKNOWN);

    CreateProjectParams params = createProjectParams("testFiles/complexModel/complex_model_with_compartment.zip")
        .addZipEntry(entry1).addZipEntry(entry2).addZipEntry(entry3).addZipEntry(entry4).addZipEntry(entry5);

    ComplexZipConverter parser = new ComplexZipConverter(CellDesignerXmlParser.class);
    ComplexZipConverterParams complexParams = new ComplexZipConverterParams().zipFile(fileService.getById(params.getProjectFileId()));
    for (final ZipEntryFile entry : params.getZipEntries()) {
      complexParams.entry(entry);
    }
    Model model = parser.createModel(complexParams);

    assertNotNull(model);
    assertEquals("main", model.getName());
    assertEquals(3, model.getSubmodelConnections().size());

    int oldSize = 0;
    for (final Model m : model.getSubmodels()) {
      oldSize += m.getParentModels().size();
    }

    new CopyCommand(model).execute();

    int newSize = 0;
    for (final Model m : model.getSubmodels()) {
      newSize += m.getParentModels().size();
    }
    assertEquals("Submodels doesn't match after copying", oldSize, newSize);
  }

  @Test
  public void testCreateLogEntries() {
    Protein protein = new GenericProtein("id");
    MinervaLoggerAppender appender = MinervaLoggerAppender.createAppender();
    appender.append(Log4jLogEvent.newBuilder()
        .setLevel(Level.WARN)
        .setMessage(new SimpleMessage("Hello world"))
        .setMarker(new LogMarker(ProjectLogEntryType.OTHER, protein))
        .build());
    appender.append(Log4jLogEvent.newBuilder()
        .setLevel(Level.ERROR)
        .setMessage(new SimpleMessage("Hello world 2"))
        .setMarker(new LogMarker(ProjectLogEntryType.OTHER, protein))
        .build());
    appender.append(Log4jLogEvent.newBuilder()
        .setLevel(Level.ERROR)
        .setMessage(new SimpleMessage("Hello world 3"))
        .build());

    ProjectService service = Mockito.mock(ProjectService.class, Mockito.CALLS_REAL_METHODS);
    Set<ProjectLogEntry> entries = service.createLogEntries(appender);
    assertEquals(3, entries.size());
  }

  @Test
  public void testCreateIgnoredLogEntries() {
    MinervaLoggerAppender appender = MinervaLoggerAppender.createAppender();
    appender.append(Log4jLogEvent.newBuilder()
        .setLevel(Level.WARN)
        .setMessage(new SimpleMessage("Hello world"))
        .setMarker(LogMarker.IGNORE)
        .build());
    ProjectService service = Mockito.mock(ProjectService.class, Mockito.CALLS_REAL_METHODS);
    Set<ProjectLogEntry> entries = service.createLogEntries(appender);
    assertEquals(0, entries.size());
  }

  @Test
  public void testFixProjectIssuesWithElementWithoutModel() {
    ProjectService service = Mockito.mock(ProjectService.class, CALLS_REAL_METHODS);
    Project project = new Project();
    ModelData model = new ModelData();
    GenericProtein protein = new GenericProtein("1");
    project.addModel(model);
    model.addElement(protein);
    List<ProjectLogEntry> entries = service.fixModelIssues(model);
    assertFalse(entries.isEmpty());
  }


}
