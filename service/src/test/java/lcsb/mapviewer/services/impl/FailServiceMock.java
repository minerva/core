package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.IExternalService;

public class FailServiceMock implements IExternalService {

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus("Test service", "unknown");
    status.setStatus(ExternalServiceStatusType.DOWN);
    return status;
  }

}
