package lcsb.mapviewer.services.impl;

import com.icegreen.greenmail.util.GreenMail;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.security.PrivilegeDao;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.internet.MimeMessage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@Transactional
@Rollback
public class UserServiceTest extends ServiceTestFunctions {

  private static final String TEST_LOGIN = "test_user";

  private static final String TEST_LOGIN_2 = "test_user_2";

  private static final String TEST_LOGIN_1 = "test_user_1";

  @Autowired
  private IUserService userService;

  @Autowired
  private PrivilegeDao privilegeDao;

  private GreenMail mailServer;

  @Before
  public void setUp() throws Exception {
    mailServer = new GreenMail();
    mailServer.start();
    configurationService.setConfigurationValue(ConfigurationElementType.EMAIL_SMTP_SERVER, "127.0.0.1");
    configurationService.setConfigurationValue(ConfigurationElementType.EMAIL_SMTP_PORT, mailServer.getSmtp().getPort() + "");
    mailServer.setUser(configurationService.getConfigurationValue(ConfigurationElementType.EMAIL_LOGIN),
        configurationService.getConfigurationValue(ConfigurationElementType.EMAIL_PASSWORD));
  }

  @After
  public void tearDown() throws Exception {
    mailServer.stop();
    configurationService.setConfigurationValue(ConfigurationElementType.EMAIL_SMTP_SERVER, "");
  }

  @Test
  public void testSendActivationEmail() throws Exception {
    userService.sendUserActivatedEmail(userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN));

    MimeMessage[] emails = mailServer.getReceivedMessages();
    assertEquals(0, emails.length);
    assertEquals(0, super.getErrors().size());
  }

  @Test
  public void grantExistingPrivilegeDoesNotProduceDuplicates() {
    User user1 = createTestUser(TEST_LOGIN_1);
    userService.add(user1);

    User user2 = createTestUser(TEST_LOGIN_2);
    userService.add(user2);

    Privilege privilege = new Privilege(PrivilegeType.WRITE_PROJECT, "foo");
    privilegeDao.add(privilege);

    long count = privilegeDao.getCount();

    assertFalse(user1.getPrivileges().contains(privilege));
    assertFalse(user2.getPrivileges().contains(privilege));

    userService.grantUserPrivilege(user1, PrivilegeType.WRITE_PROJECT, "foo");
    userService.grantUserPrivilege(user2, PrivilegeType.WRITE_PROJECT, "foo");

    assertTrue(user1.getPrivileges().contains(privilege));
    assertTrue(user2.getPrivileges().contains(privilege));
    assertEquals(count, privilegeDao.getCount());
  }

  private User createTestUser(final String login) {
    User user = new User();
    user.setName("Jane");
    user.setSurname("Doe");
    user.setLogin(login);
    user.setCryptedPassword("***");
    return user;
  }

  @Test
  public void grantNonYetExistingPrivilegeTwiceDoesNotConflict() {
    User user1 = createTestUser(TEST_LOGIN_1);
    userService.add(user1);

    User user2 = createTestUser(TEST_LOGIN_2);
    userService.add(user2);

    Privilege privilege = new Privilege(PrivilegeType.WRITE_PROJECT, "foo");

    long count = privilegeDao.getCount();

    assertFalse(user1.getPrivileges().contains(privilege));
    assertFalse(user2.getPrivileges().contains(privilege));

    userService.grantUserPrivilege(user1, PrivilegeType.WRITE_PROJECT, "foo");
    userService.grantUserPrivilege(user2, PrivilegeType.WRITE_PROJECT, "foo");

    assertTrue(user1.getPrivileges().contains(privilege));
    assertTrue(user2.getPrivileges().contains(privilege));
    assertEquals(count + 1, privilegeDao.getCount());
  }

  @Test
  public void revokePrivilegesWorks() {
    User user = createTestUser(TEST_LOGIN);
    userService.add(user);

    Privilege privilege = new Privilege(PrivilegeType.WRITE_PROJECT, "foo");
    userService.grantUserPrivilege(user, PrivilegeType.WRITE_PROJECT, "foo");
    assertTrue(user.getPrivileges().contains(privilege));

    userService.revokeUserPrivilege(user, PrivilegeType.WRITE_PROJECT, "foo");
    assertFalse(user.getPrivileges().contains(privilege));
  }

  @Test
  public void revokePrivilegesDoesNotThrowIfPrivilegeNotExists() {
    User user = createTestUser(TEST_LOGIN);
    userService.add(user);

    Privilege privilege = new Privilege(PrivilegeType.WRITE_PROJECT, "foo");
    assertFalse(user.getPrivileges().contains(privilege));

    userService.revokeUserPrivilege(user, PrivilegeType.WRITE_PROJECT, "foo");
  }

  @Test
  public void revokePrivilegeGloballyWorks() {
    User user1 = createTestUser(TEST_LOGIN_1);
    userService.add(user1);

    User user2 = createTestUser(TEST_LOGIN_2);
    userService.add(user2);

    Privilege privilege = new Privilege(PrivilegeType.WRITE_PROJECT, "foo");

    long count = privilegeDao.getCount();

    assertFalse(user1.getPrivileges().contains(privilege));
    assertFalse(user2.getPrivileges().contains(privilege));

    userService.grantUserPrivilege(user1, PrivilegeType.WRITE_PROJECT, "foo");
    userService.grantUserPrivilege(user2, PrivilegeType.WRITE_PROJECT, "foo");

    assertEquals(count + 1, privilegeDao.getCount());
    assertTrue(user1.getPrivileges().contains(privilege));
    assertTrue(user2.getPrivileges().contains(privilege));

    userService.revokeObjectDomainPrivilegesForAllUsers(PrivilegeType.WRITE_PROJECT, "foo");

    assertFalse(user1.getPrivileges().contains(privilege));
    assertFalse(user2.getPrivileges().contains(privilege));
  }

  @Test
  public void testAddUser() throws Exception {
    long logCount = getInfos().size();
    User user2 = createTestUser("login");
    userService.add(user2);
    assertNotNull(user2.getId());
    long logCount2 = getInfos().size();
    assertEquals("Log entry is missing for add user event", logCount + 1, logCount2);
    userDao.evict(user2);
    User user3 = userService.getUserByLogin(user2.getLogin());
    assertNotNull(user3);
    userService.remove(user3);
    user3 = userService.getUserByLogin(user2.getLogin());
    assertNull(user3);

    long logCount3 = getInfos().size();
    assertEquals("Log entry is missing for remove user event", logCount2 + 1, logCount3);
  }

}
