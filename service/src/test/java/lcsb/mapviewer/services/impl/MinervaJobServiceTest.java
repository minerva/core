package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.annotation.cache.CacheQueryMinervaJob;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.common.tests.TestUtils;
import lcsb.mapviewer.common.tests.UnitTestFailedWatcher;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.CacheQuery;
import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.model.job.MinervaJob;
import lcsb.mapviewer.model.job.MinervaJobParameters;
import lcsb.mapviewer.model.job.MinervaJobPriority;
import lcsb.mapviewer.model.job.MinervaJobStatus;
import lcsb.mapviewer.model.job.MinervaJobType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.SpringServiceTestConfig;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IMiriamService;
import lcsb.mapviewer.services.jobs.RefreshChemicalInfoMinervaJob;
import lcsb.mapviewer.services.jobs.RefreshDrugInfoMinervaJob;
import lcsb.mapviewer.services.jobs.RefreshMiriamInfoMinervaJob;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


@ContextConfiguration(classes = SpringServiceTestConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class MinervaJobServiceTest extends TestUtils {

  private final String query = "https://google.com/";
  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();
  @Autowired
  private IMinervaJobService minervaJobService;
  @Autowired
  private IMiriamService miriamService;
  @Autowired
  private GeneralCacheInterface cache;
  @Autowired
  private HgncAnnotator hgncAnnotator;

  private CacheType type;

  @Before
  public void setUp() throws Exception {
    minervaJobService.enableQueue(false);
    type = hgncAnnotator.getCacheType();
  }

  @After
  public final void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    cache.clearCache();
  }

  @Test
  public void testRefreshTask() {
    for (int i = 0; i < 5; i++) {
      CacheQuery entry = cache.getByQuery(query + "?" + i, type);
      if (entry != null) {
        cache.delete(entry);
      }
    }

    minervaJobService.addJob(
        new MinervaJob(MinervaJobType.REFRESH_CACHE, MinervaJobPriority.MEDIUM, new CacheQueryMinervaJob(query + "?0", type.getId())));
    minervaJobService.addJob(
        new MinervaJob(MinervaJobType.REFRESH_CACHE, MinervaJobPriority.MEDIUM, new CacheQueryMinervaJob(query + "?1", type.getId())));
    minervaJobService.addJob(
        new MinervaJob(MinervaJobType.REFRESH_CACHE, MinervaJobPriority.MEDIUM, new CacheQueryMinervaJob(query + "?2", type.getId())));
    minervaJobService.addJob(
        new MinervaJob(MinervaJobType.REFRESH_CACHE, MinervaJobPriority.LOWEST, new CacheQueryMinervaJob(query + "?3", type.getId())));
    minervaJobService.addJob(
        new MinervaJob(MinervaJobType.REFRESH_CACHE, MinervaJobPriority.HIGH, new CacheQueryMinervaJob(query + "?4", type.getId())));

    minervaJobService.waitForTasksToFinish();

    for (int i = 0; i < 5; i++) {
      CacheQuery entry = cache.getByQuery(query + "?" + i, type);

      assertNotNull(entry);

      assertTrue(Calendar.getInstance().before(entry.getExpires()));
    }
    List<MinervaJob> jobs = minervaJobService.getAllJobs();
    MinervaJob low = null;
    MinervaJob high = null;
    for (final MinervaJob minervaJob : jobs) {
      if (minervaJob.getPriority() == MinervaJobPriority.HIGH.getValue()) {
        high = minervaJob;
      }
      if (minervaJob.getPriority() == MinervaJobPriority.LOWEST.getValue()) {
        low = minervaJob;
      }
    }
    assertNotNull(high);
    assertNotNull(low);
    assertTrue(high.getJobStarted().before(low.getJobStarted()));
  }

  @Test
  public void testFailedRefreshTaskRemoveFromCache() {
    final String invalidQuery = "invalidQuery";
    cache.setCachedQuery(invalidQuery, type, "some val");

    final MinervaJobParameters params = new CacheQueryMinervaJob(invalidQuery, type.getId());

    assertNotNull(cache.getByQuery(invalidQuery, type));

    minervaJobService.addJob(new MinervaJob(MinervaJobType.REFRESH_CACHE, MinervaJobPriority.MEDIUM, params));

    minervaJobService.waitForTasksToFinish();

    assertNull(cache.getByQuery(query, type));

  }

  @Test
  public void testCreateRefreshTaskForExpiredCache() {
    cache.setCachedQuery(query, type, "invalidated");
    cache.invalidateByQuery(query, type);

    minervaJobService.waitForTasksToFinish();

    final CacheQuery entry = cache.getByQuery(query, type);

    assertNotEquals("invalidated", entry.getValue());
  }

  @Test
  public void testRefreshMiriamInfoTask() {
    MiriamData md = new MiriamData(MiriamType.PUBMED, "12345");
    miriamService.add(md);

    final RefreshMiriamInfoMinervaJob params = new RefreshMiriamInfoMinervaJob(md.getId());
    final MinervaJob job = new MinervaJob(MinervaJobType.REFRESH_MIRIAM_INFO, MinervaJobPriority.MEDIUM, params);

    minervaJobService.addJob(job);
    minervaJobService.waitForTasksToFinish();

    md = miriamService.getById(md.getId());
    assertNotNull(md.getArticle());
    assertNotNull(md.getLink());
  }

  @Test
  public void testRefreshDrugInfoTask() {

    final RefreshDrugInfoMinervaJob params = new RefreshDrugInfoMinervaJob(MiriamType.HGNC_SYMBOL, "SNCA");
    final MinervaJob job = new MinervaJob(MinervaJobType.REFRESH_DRUG_INFO, MinervaJobPriority.MEDIUM, params);

    minervaJobService.addJob(job);
    minervaJobService.waitForTasksToFinish();

    assertEquals(0, getErrors().size());
  }

  @Test
  public void testRefreshChemicalInfoTask() {
    final MiriamData parkinsonDisease = new MiriamData(MiriamType.MESH_2012, "D010300");
    final MiriamData md = new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA");

    final RefreshChemicalInfoMinervaJob params = new RefreshChemicalInfoMinervaJob(md, parkinsonDisease);
    final MinervaJob job = new MinervaJob(MinervaJobType.REFRESH_CHEMICAL_INFO, MinervaJobPriority.MEDIUM, params);

    minervaJobService.addJob(job);
    minervaJobService.waitForTasksToFinish();

    assertEquals(0, getErrors().size());
  }

  @Test
  public void testAddDuplicates() {
    minervaJobService.disableQueue();
    try {
      long originalCount = minervaJobService.getCount(MinervaJobStatus.PENDING);

      RefreshMiriamInfoMinervaJob params = new RefreshMiriamInfoMinervaJob(-1);
      MinervaJob job = new MinervaJob(MinervaJobType.REFRESH_MIRIAM_INFO, MinervaJobPriority.MEDIUM, params);
      assertTrue(minervaJobService.addJob(job));
      assertEquals(originalCount + 1, minervaJobService.getCount(MinervaJobStatus.PENDING));

      MinervaJob job2 = new MinervaJob(MinervaJobType.REFRESH_MIRIAM_INFO, MinervaJobPriority.MEDIUM, params);
      assertFalse(minervaJobService.addJob(job2));
      assertEquals(originalCount + 1, minervaJobService.getCount(MinervaJobStatus.PENDING));
    } finally {
      minervaJobService.enableQueue(false);
    }
  }

  @Test
  public void testUpdateProgress() {
    minervaJobService.disableQueue();
    try {
      RefreshMiriamInfoMinervaJob params = new RefreshMiriamInfoMinervaJob(-1);
      MinervaJob job = new MinervaJob(MinervaJobType.REFRESH_MIRIAM_INFO, MinervaJobPriority.MEDIUM, params);
      minervaJobService.addJob(job);

      minervaJobService.updateJobProgress(job, 50.0);

      job = minervaJobService.getJobById(job.getId());
      assertEquals(50.0, job.getProgress(), EPSILON);
    } finally {
      minervaJobService.enableQueue(false);
    }
  }

  @Test
  public void testUpdateProgressInvalidJob() {
    minervaJobService.disableQueue();
    try {
      RefreshMiriamInfoMinervaJob params = new RefreshMiriamInfoMinervaJob(-1);
      MinervaJob job = new MinervaJob(MinervaJobType.REFRESH_MIRIAM_INFO, MinervaJobPriority.MEDIUM, params);

      minervaJobService.updateJobProgress(job, 50.0);

      assertNull(minervaJobService.getJobById(job.getId()));
    } finally {
      minervaJobService.enableQueue(false);
    }
  }

  @Test
  public void testAddJobWithExternalObject() {
    Project project = createProject();
    minervaJobService.disableQueue();
    try {
      RefreshMiriamInfoMinervaJob params = new RefreshMiriamInfoMinervaJob(-1);
      MinervaJob job = new MinervaJob(MinervaJobType.REFRESH_MIRIAM_INFO, MinervaJobPriority.MEDIUM, params);
      job.setExternalObject(project);

      minervaJobService.addJob(job);

      job = minervaJobService.getJobById(job.getId());

      assertNotNull(job.getExternalObjectClass());
      assertNotNull(job.getExternalObjectId());
    } finally {
      minervaJobService.enableQueue(false);
    }
  }

  public Project createProject() {
    User admin = new User();
    admin.setId(BUILT_IN_TEST_ADMIN_ID);

    Project project = new Project();
    project.setProjectId(faker.numerify("T######"));
    project.setOwner(admin);

    return project;
  }

}
