package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.commands.ColorModelCommand;
import lcsb.mapviewer.common.TextFileUtils;
import lcsb.mapviewer.converter.ColorSchemaReader;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.interfaces.IDataOverlayService.CreateDataOverlayParams;
import lcsb.mapviewer.services.interfaces.IProjectBackgroundService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.services.utils.EmailSender;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import java.awt.Color;
import java.io.FileInputStream;
import java.util.Collection;

public class ProjectBackgroundTest extends ServiceTestFunctions {

  private Project project = null;
  private Model model = null;

  private EmailSender originalSender;

  @Autowired
  private IProjectBackgroundService backgroundService;

  private final ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE, Color.WHITE);

  private User user;
  private User user2;

  @Autowired
  protected IUserService userService;

  @Before
  public void setUp() throws Exception {
    // we use custom threads because in layout service there is commit method
    // called, and because of that hibernate session injected by spring cannot
    // commit at the end of the test case

    project = projectDao.getProjectByProjectId(TEST_PROJECT_ID);
    if (project != null) {
      projectDao.delete(project);
    }

    project = projectDao.getProjectByProjectId(TEST_PROJECT_ID_2);
    if (project != null) {
      projectDao.delete(project);
    }

    project = new Project();
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));

    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    model = parser.createModel(new ConverterParams().filename("testFiles/sample.xml"));

    project.addModel(model);
    project.setProjectId(TEST_PROJECT_ID);

    projectDao.add(project);

    user = createUser();
    user2 = createUser("john.doe.bis");

    originalSender = backgroundService.getEmailSender();
    backgroundService.setEmailSender(Mockito.mock(EmailSender.class));
  }

  @After
  public void tearDown() throws Exception {
    userService.remove(user2);
    userService.remove(user);
    projectDao.delete(project);

    backgroundService.setEmailSender(originalSender);
  }

  @Test
  public void testGetLayoutAliasesForInvalidAlias() throws Exception {
    Project project = new Project("p_id");
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    try {
      Model model = new CellDesignerXmlParser()
          .createModel(new ConverterParams().filename("testFiles/coloring/problematicModel2.xml"));

      project.addModel(model);
      projectDao.add(project);

      FileInputStream fis = new FileInputStream("testFiles/coloring/problematicSchema2.txt");
      CreateDataOverlayParams params = new CreateDataOverlayParams().name("Test")
          .project(project)
          .colorInputStream(fis);
      ColorSchemaReader reader = new ColorSchemaReader();
      final Collection<DataOverlayEntry> schemas = reader.readColorSchema(params.getColorInputStream(),
          TextFileUtils.getHeaderParametersFromFile(params.getColorInputStream()));

      ColorModelCommand command = new ColorModelCommand(model, schemas, colorExtractor);
      command.execute();

      command.getModifiedElements();
    } finally {
      projectDao.delete(project);
    }
  }

}
