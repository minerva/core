package lcsb.mapviewer.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.services.ServiceTestFunctions;

public class CustomMd5PasswordEncoderTest extends ServiceTestFunctions {
  private CustomMd5PasswordEncoder passwordEncoder = new CustomMd5PasswordEncoder();

  private String passwd = "test";
  private String hash = "098f6bcd4621d373cade4e832627b4f6";

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEncode() {
    assertEquals(hash, passwordEncoder.encode(passwd));
  }

  @Test
  public void testMatch() {
    assertTrue(passwordEncoder.matches(passwd, hash));
  }

}
