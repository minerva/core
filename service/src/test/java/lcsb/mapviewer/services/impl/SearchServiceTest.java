package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.user.UserDao;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.interfaces.ISearchService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@Rollback(true)
public class SearchServiceTest extends ServiceTestFunctions {

  @Autowired
  private ProjectDao projectDao;

  @Autowired
  private ISearchService searchService;

  @Autowired
  private UserDao userDao;

  private Model model;

  private Project project;

  @Before
  public void setUp() throws Exception {
    model = createFullModel();
    project = model.getProject();
    projectDao.add(project);
  }

  protected Model createFullModel() throws Exception {
    Model model = getModelForFile("testFiles/searchModel.xml", false);
    MiriamData md = new MiriamData(MiriamRelationType.BQ_BIOL_HAS_VERSION, MiriamType.CAS, "123");
    model.getElements().iterator().next().addMiriamData(md);

    Project project = new Project("unknown project");
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    project.addModel(model);
    return model;
  }

  @After
  public void tearDown() throws Exception {
  }

  private SearchService createSearchService() {
    return new SearchService(null, null, null, null);
  }

  @Test
  public void testSearchByName2() throws Exception {
    List<BioEntity> result = searchService.searchByQuery(project.getProjectId(), model.getId() + "", "BAD:BCL-2", 50,
        false, "127.0.0.1");

    assertNotNull(result);
    assertTrue(result.size() > 0);
    Species alias = (Species) result.get(0);
    assertEquals("BAD:BCL-2", alias.getName());
  }

  @Test
  public void testSearchById() throws Exception {
    List<BioEntity> global = searchService.searchByQuery(project.getProjectId(), model.getId() + "", "CAS:123", 50,
        false, "127.0.0.1");
    assertNotNull(global);
    assertEquals(1, global.size());
  }

  @Test
  public void testSearchByElementId() throws Exception {
    Element element = model.getElements().iterator().next();
    List<BioEntity> global = searchService.searchByQuery(project.getProjectId(), model.getId() + "",
        "element:" + element.getId(), 50, true, "127.0.0.1");
    assertNotNull(global);
    assertEquals(1, global.size());
  }

  @Test
  public void testSearchByUnknwonElementId() throws Exception {
    List<BioEntity> global = searchService.searchByQuery(project.getProjectId(), model.getId() + "", "species:90111117",
        50, false, "127.0.0.1");
    assertEquals(0, global.size());
  }

  @Test
  public void testSearchByName4() throws Exception {
    List<BioEntity> result = searchService.searchByQuery(project.getProjectId(), model.getId() + "", "BC", 50, true,
        "127.0.0.1");

    assertNotNull(result);
    assertEquals(0, result.size());

    result = searchService.searchByQuery(project.getProjectId(), model.getId() + "", "BC", 50, false, "127.0.0.1");
    assertNotNull(result);
    assertTrue(result.size() > 0);

    result = searchService.searchByQuery(project.getProjectId(), model.getId() + "", "BAD:BCL-2", 50, true,
        "127.0.0.1");
    assertNotNull(result);
    assertEquals(1, result.size());
    Species alias = (Species) result.get(0);
    assertEquals("BAD:BCL-2", alias.getName());
  }

  @Test
  public void testQueryName() throws Exception {
    List<BioEntity> result = searchService.searchByQuery(project.getProjectId(), model.getId() + "",
        "reaction:re21", 50, false, "127.0.0.1");

    assertNotNull(result);
    assertEquals(4, result.size());
    Reaction reaction = (Reaction) result.get(0);
    assertEquals("re21", reaction.getIdReaction());
    assertTrue(result.get(1) instanceof Species);
    assertTrue(result.get(2) instanceof Species);
    assertTrue(result.get(3) instanceof Species);
  }

  @Test
  public void testQueryReactionId() throws Exception {
    Reaction reaction = model.getReactions().iterator().next();
    List<BioEntity> result = searchService.searchByQuery(project.getProjectId(), model.getId() + "",
        "reaction:" + reaction.getIdReaction(), 50, false, "127.0.0.1");

    assertNotNull(result);
    assertTrue(result.size() > 0);
    assertEquals(reaction, result.get(0));
  }

  @Test
  public void testQueryName2() throws Exception {
    List<BioEntity> result = searchService.searchByQuery(project.getProjectId(), model.getId() + "",
        "BCL-2", 50, false, "127.0.0.1");
    assertNotNull(result);
    assertTrue(result.size() > 0);
    assertTrue("Invalid alias: " + result.get(0).getName(), result.get(0).getName().toUpperCase().contains("BCL"));
  }

  @Test
  public void testQueryName3() throws Exception {
    List<BioEntity> result = searchService.searchByQuery(project.getProjectId(), model.getId() + "",
        "fdhgjkhdsfsdhfgfhsd", 50, false, "127.0.0.1");

    assertNotNull(result);
    assertEquals(0, result.size());
  }

  @Test
  public void testQueryName4() throws Exception {
    List<BioEntity> result = searchService.searchByQuery(project.getProjectId(), model.getId() + "", "protein:BCL-2",
        50, false, "127.0.0.1");
    assertNotNull(result);
    assertTrue(result.size() > 0);
  }

  @Test
  public void testGetMiriamTypeForQuery() throws Exception {
    MiriamData md = createSearchService().getMiriamTypeForQuery("hgnc:SNCA");
    assertNotNull(md);
    assertEquals(MiriamType.HGNC, md.getDataType());
    assertEquals("SNCA", md.getResource());

    md = createSearchService().getMiriamTypeForQuery("reactome:REACT_15128");
    assertNotNull(md);
    assertEquals(MiriamType.REACTOME, md.getDataType());
    assertEquals("REACT_15128", md.getResource());
  }

  @Test
  public void testSearchByMiriamInSubmap() throws Exception {
    String query = "HGNC_SYMBOL:SNCA";
    Model model = new ModelFullIndexed(null);
    Model submodel = new ModelFullIndexed(null);
    GenericProtein protein = new GenericProtein("s1");
    assignCoordinates(10, 10, 100, 100, protein);
    protein.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    submodel.addElement(protein);
    model.addSubmodelConnection(new ModelSubmodelConnection(submodel, SubmodelType.UNKNOWN));

    Project project = new Project("xyz");
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    project.addModel(model);
    project.addModel(submodel);

    projectDao.add(project);

    List<BioEntity> result = searchService.searchByQuery(project.getProjectId(), "" + submodel.getId(), query, 50,
        false,
        "127.0.0.1");

    assertEquals(1, result.size());
  }

  @Test
  public void testGetMiriamTypeForQuery2() throws Exception {
    MiriamData md = createSearchService().getMiriamTypeForQuery("UNKNOWN_HGNC:SNCA");
    assertNull(md);
  }

}
