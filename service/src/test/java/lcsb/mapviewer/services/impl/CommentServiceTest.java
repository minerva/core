package lcsb.mapviewer.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.map.CommentDao;
import lcsb.mapviewer.persist.dao.map.ModelDao;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.interfaces.ICommentService;

@Rollback(true)
public class CommentServiceTest extends ServiceTestFunctions {
  private Model model;
  private Project project;

  @Autowired
  protected ModelDao modelDao;

  @Autowired
  protected ICommentService commentService;

  @Autowired
  protected CommentDao commentDao;

  @Before
  public void setUp() throws Exception {
    dbUtils.setAutoFlush(true);
    project = projectDao.getProjectByProjectId(TEST_PROJECT_ID);
    if (project != null) {
      projectDao.delete(project);
    }
    project = new Project();
    project.setOwner(userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));

    project.setProjectId(TEST_PROJECT_ID);
    model = getModelForFile("testFiles/centeredAnchorInModifier.xml", false);
    model.setTileSize(128);
    project.addModel(model);
    projectDao.add(project);

    projectDao.evict(project);
    modelDao.evict(model);

    project = projectDao.getById(project.getId());
    model = project.getTopModel();
  }

  @After
  public void tearDown() throws Exception {
    projectDao.delete(project);
  }

  @Test
  public void testAddComment() throws Exception {
    long counter = commentService.getCommentCount();
    Comment feedback = commentService.addComment("b", "c", new Point2D.Double(0, 0), null, false, model.getId(),
        userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    long counter2 = commentService.getCommentCount();
    assertEquals(counter + 1, counter2);
    commentDao.delete(feedback);
  }

  @Test
  public void testAddCommentForReaction() throws Exception {
    long counter = commentService.getCommentCount();
    Comment feedback = commentService.addComment("b", "c", new Point2D.Double(0, 0),
        model.getReactions().iterator().next(), false, model.getId(), userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    long counter2 = commentService.getCommentCount();
    assertEquals(counter + 1, counter2);
    commentDao.delete(feedback);
  }

  @Test
  public void testAddCommentForAlias() throws Exception {
    Element alias = model.getElementByElementId("sa1");
    long counter = commentService.getCommentCount();
    Comment feedback = commentService.addComment("b", "c", new Point2D.Double(0, 0), alias, false, model.getId(),
        userDao.getUserByLogin(ADMIN_BUILT_IN_LOGIN));
    long counter2 = commentService.getCommentCount();
    assertEquals(counter + 1, counter2);
    commentDao.delete(feedback);
  }

  @Test
  public void testComparePointCommentId() throws Exception {
    CommentService cs = new CommentService(null, null, null, null, null);
    assertTrue(cs.equalPoints("Point2D.Double[117.685546875, 204.6923828125001]", "(117.69, 204.69)"));
  }

  @Test
  public void testComparePointCommentId2() throws Exception {
    CommentService cs = new CommentService(null, null, null, null, null);
    assertFalse(cs.equalPoints("Point2D.Double[118.685546875, 204.6923828125001]", "(117.69, 204.69)"));
  }

  @Test
  public void testComparePointCommentId3() throws Exception {
    CommentService cs = new CommentService(null, null, null, null, null);
    assertFalse(cs.equalPoints("Point2D.Double[117.685546875, 205.6923828125001]", "(117.69, 204.69)"));
  }

}
