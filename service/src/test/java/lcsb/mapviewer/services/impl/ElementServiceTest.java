package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IModelService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.Rollback;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@Rollback(true)
public class ElementServiceTest extends ServiceTestFunctions {

  @Autowired
  private IElementService elementService;

  @Autowired
  private IModelService modelService;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetElementsByExistingProject() throws Exception {
    final List<ModelData> maps = modelService.getModelsByMapId(BUILT_IN_MAP, "*");
    final Map<ElementProperty, Object> filter = new HashMap<>();
    filter.put(ElementProperty.MAP, maps);

    final Page<Element> elements = elementService.getAll(filter, Pageable.unpaged());
    assertEquals(0, elements.getNumberOfElements());
  }

  @Test
  public void testGetElementsByNonExistingMapId() throws Exception {
    final List<ModelData> maps = modelService.getModelsByMapId(BUILT_IN_MAP, "0");
    final Map<ElementProperty, Object> filter = new HashMap<>();
    filter.put(ElementProperty.MAP, maps);

    final Page<Element> elements = elementService.getAll(filter, Pageable.unpaged());
    assertEquals(0, elements.getNumberOfElements());
  }

  @Test
  public void testGetElementsByInvalidMapId() throws Exception {
    final Map<ElementProperty, Object> filter = new HashMap<>();
    filter.put(ElementProperty.MAP_ID, Collections.singletonList(-1));

    final Page<Element> elements = elementService.getAll(filter, Pageable.unpaged());
    assertEquals(0, elements.getNumberOfElements());
  }

}
