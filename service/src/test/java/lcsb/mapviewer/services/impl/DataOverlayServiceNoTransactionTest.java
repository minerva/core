package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.tests.TestUtils;
import lcsb.mapviewer.converter.ColorSchemaColumn;
import lcsb.mapviewer.converter.ColorSchemaReader;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.overlay.DataOverlayType;
import lcsb.mapviewer.model.overlay.GeneVariantDataOverlayEntry;
import lcsb.mapviewer.services.SpringServiceTestConfig;
import lcsb.mapviewer.services.interfaces.IDataOverlayService;
import lcsb.mapviewer.services.interfaces.IDataOverlayService.CreateDataOverlayParams;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@ContextConfiguration(classes = SpringServiceTestConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("emailTestProfile")
public class DataOverlayServiceNoTransactionTest extends TestUtils {

  @Autowired
  private IDataOverlayService dataOverlayService;

  @Autowired
  private IProjectService projectService;

  @Autowired
  private IUserService userService;

  @Autowired
  private IMinervaJobService minervaJobService;

  private Project project;

  private Model map;


  @Before
  public void setUp() throws Exception {
    minervaJobService.enableQueue(false);
    project = new Project();
    project.setOwner(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));

    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    map = parser.createModel(new ConverterParams().filename("testFiles/sample.xml"));

    project.addModel(map);
    project.setProjectId(TEST_PROJECT_ID);
    projectService.add(project);
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    projectService.remove(project);
    if (projectService.getProjectByProjectId(TEST_PROJECT) != null) {
      projectService.remove(projectService.getProjectByProjectId(TEST_PROJECT));
    }
  }

  @Test
  public void testGetOverlayElements() throws Exception {
    ByteArrayInputStream bais = new ByteArrayInputStream(("name\telement_identifier\tColor\n"
        + "s1\t\t#CC0000\n"
        + "s2\t\t#CCCC00\n"
        + "\tre1\t#CCCC00\n")
        .getBytes(StandardCharsets.UTF_8));
    CreateDataOverlayParams params = new CreateDataOverlayParams().name("Test")
        .project(project)
        .colorInputStream(bais)
        .user(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));

    DataOverlay row = dataOverlayService.createDataOverlay(params);

    List<?> result = dataOverlayService.getElementReferencesForDataOverlay(row.getId(), Collections.singletonList(map.getModelData()));

    assertTrue(result.size() > 0);

    List<?> result2 = dataOverlayService.getReactionReferencesForDataOverlay(row.getId(), Collections.singletonList(map.getModelData()));

    assertEquals(1, result2.size());
  }

  @Test
  public void testGetCustomDataOverlays() throws Exception {
    List<DataOverlay> dataOverlays = dataOverlayService.getDataOverlaysByProject(project);
    assertNotNull(dataOverlays);
    assertEquals(0, dataOverlays.size());

    CreateDataOverlayParams params = new CreateDataOverlayParams().name("Test").project(project)
        .colorInputStream(new FileInputStream("testFiles/coloring/ge001.txt"))
        .user(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    DataOverlay row = dataOverlayService.createDataOverlay(params);

    assertNotNull(row);
    assertTrue(row.getId() > 0);

    dataOverlays = dataOverlayService.getDataOverlaysByProject(project);
    assertEquals(1, dataOverlays.size());
  }

  @Test
  public void testUpdateDataOverlay() throws Exception {
    CreateDataOverlayParams params = new CreateDataOverlayParams().name("Test").project(project)
        .colorInputStream(new FileInputStream("testFiles/coloring/ge001.txt"))
        .user(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    DataOverlay row = dataOverlayService.createDataOverlay(params);

    List<DataOverlay> dataOverlays = dataOverlayService.getDataOverlaysByProject(project);
    assertEquals(1, dataOverlays.size());
    assertEquals("Test", dataOverlays.get(0).getName());

    row.setName("New name");

    dataOverlayService.updateDataOverlay(row);

    dataOverlays = dataOverlayService.getDataOverlaysByProject(project);
    assertEquals(1, dataOverlays.size());
    assertEquals("New name", dataOverlays.get(0).getName());
  }

  @Test
  public void testAddOverlayToComplexModel() throws Exception {
    Project localProject = new Project();
    localProject.setOwner(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model localModel = parser.createModel(new ConverterParams().filename("testFiles/sample.xml"));
    Model localSubmodelModel = parser.createModel(new ConverterParams().filename("testFiles/sample.xml"));

    ModelSubmodelConnection connection = new ModelSubmodelConnection(localSubmodelModel, SubmodelType.UNKNOWN);
    connection.setName("AS");
    localModel.addSubmodelConnection(connection);

    localProject.addModel(localModel);
    localProject.setProjectId(TEST_PROJECT);

    projectService.add(localProject);

    CreateDataOverlayParams params = new CreateDataOverlayParams().name("Test")
        .project(localProject)
        .colorInputStream(new FileInputStream("testFiles/coloring/ge001.txt"))
        .user(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));

    dataOverlayService.createDataOverlay(params);

    Collection<DataOverlay> dataOverlays = dataOverlayService.getDataOverlaysByProject(localProject);

    assertTrue(dataOverlays.size() > 0);

  }

  @Test
  public void testInputDataInOverlay() throws Exception {
    CreateDataOverlayParams params = new CreateDataOverlayParams().name("Test").project(project)
        .colorInputStream(new FileInputStream("testFiles/coloring/ge001.txt"))
        .user(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));

    DataOverlay row = dataOverlayService.createDataOverlay(params);

    assertNotNull(row);
    assertTrue(row.getId() > 0);
    assertNotNull(row.getInputData());
    byte[] inputData = row.getInputData().getFileContent();
    assertNotNull(inputData);
    byte[] originalData = IOUtils.toByteArray(new FileInputStream("testFiles/coloring/ge001.txt"));
    assertEquals(new String(originalData, StandardCharsets.UTF_8), new String(inputData, StandardCharsets.UTF_8));
  }

  @Test
  public void testDataOverlayWithType() throws Exception {
    String overlayId = "Test";
    CreateDataOverlayParams params = new CreateDataOverlayParams().name(overlayId).project(project)
        .colorInputStream(new FileInputStream("testFiles/coloring/gene_variants_without_type.txt"))
        .user(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN))
        .colorSchemaType(DataOverlayType.GENETIC_VARIANT);

    DataOverlay overlay = dataOverlayService.createDataOverlay(params);
    List<Pair<? extends BioEntity, DataOverlayEntry>> result = dataOverlayService.getBioEntitiesForDataOverlay(
        TEST_PROJECT_ID,
        map.getId(), overlay.getId(), false);
    assertNotNull(result);
  }

  @Test
  public void testDataOverlayWithGenomeVersion() throws Exception {
    String overlayId = "Test";
    CreateDataOverlayParams params = new CreateDataOverlayParams().name(overlayId).project(project)
        .colorInputStream(new FileInputStream("testFiles/coloring/gene_variants_aa_change.txt"))
        .user(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN))
        .colorSchemaType(DataOverlayType.GENETIC_VARIANT);

    DataOverlay overlay = dataOverlayService.createDataOverlay(params);
    assertEquals("hg38", overlay.getGenomeVersion());
    assertEquals(ReferenceGenomeType.UCSC, overlay.getGenomeType());

  }

  @Test
  public void testDataOverlayWithConflictingType() throws Exception {
    String overlayId = "Test";
    CreateDataOverlayParams params = new CreateDataOverlayParams().name(overlayId).project(project)
        .colorInputStream(new FileInputStream("testFiles/coloring/gene_variants.txt"))
        .user(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN))
        .colorSchemaType(DataOverlayType.GENERIC);

    DataOverlay overlay = dataOverlayService.createDataOverlay(params);
    List<Pair<? extends BioEntity, DataOverlayEntry>> result = dataOverlayService.getBioEntitiesForDataOverlay(
        TEST_PROJECT_ID,
        map.getId(), overlay.getId(), false);
    assertNotNull(result);
  }

  @Test
  public void testEmptyPreprareTableResultForGeneVariant() throws Exception {
    class CSR extends ColorSchemaReader {
      @Override
      public Collection<ColorSchemaColumn> getSetColorSchemaColumns(final Collection<DataOverlayEntry> schemas) {
        List<ColorSchemaColumn> result = new ArrayList<ColorSchemaColumn>();
        for (final ColorSchemaColumn csc : ColorSchemaColumn.values()) {
          if (csc.getTypes().contains(DataOverlayType.GENETIC_VARIANT)) {
            result.add(csc);
          }
        }
        return result;
      }
    }

    Map<DataOverlayEntry, Integer> matches = new HashMap<>();
    DataOverlayEntry cs = new GeneVariantDataOverlayEntry();
    matches.put(cs, 1);

    DataOverlayService ls = new DataOverlayService();
    String result = ls.prepareTableResult(matches, new CSR());
    assertNotNull(result);
  }

}
