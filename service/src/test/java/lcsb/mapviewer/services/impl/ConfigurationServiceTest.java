package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.ConfigurationOption;
import lcsb.mapviewer.services.ServiceTestFunctions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import java.util.List;

import static org.junit.Assert.assertEquals;

@Rollback(true)
public class ConfigurationServiceTest extends ServiceTestFunctions {

  @Before
  public void setUp() throws Exception {
    // clear information about git version
    Configuration.getSystemBuildVersion(null, true);
  }

  @After
  public void tearDown() throws Exception {
    // clear information about git version
    Configuration.getSystemBuildVersion(null, true);
  }

  @Test
  public void testGetUpdate() throws Exception {
    String address = configurationService.getConfigurationValue(ConfigurationElementType.EMAIL_ADDRESS);
    configurationService.deleteConfigurationValue(ConfigurationElementType.EMAIL_ADDRESS);
    String newAddress = configurationService.getConfigurationValue(ConfigurationElementType.EMAIL_ADDRESS);
    assertEquals(ConfigurationElementType.EMAIL_ADDRESS.getDefaultValue(), newAddress);
    configurationService.setConfigurationValue(ConfigurationElementType.EMAIL_ADDRESS, "hello@world");
    newAddress = configurationService.getConfigurationValue(ConfigurationElementType.EMAIL_ADDRESS);
    assertEquals("hello@world", newAddress);

    configurationService.setConfigurationValue(ConfigurationElementType.EMAIL_ADDRESS, address);
  }

  @Test
  public void testList() throws Exception {
    List<ConfigurationOption> list = configurationService.getAllValues();
    assertEquals(ConfigurationElementType.values().length, list.size());
  }
}
