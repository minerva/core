package lcsb.mapviewer.services.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import lcsb.mapviewer.model.Stacktrace;
import lcsb.mapviewer.services.ServiceTestFunctions;
import lcsb.mapviewer.services.interfaces.IStacktraceService;

@Rollback(true)
public class StacktraceServiceTest extends ServiceTestFunctions {

  @Autowired
  private IStacktraceService stacktraceService;

  @Before
  public void setUp() throws Exception {
  }

  @Test
  public void testCreateEntry() throws Exception {
    Stacktrace st = stacktraceService.createFromStackTrace(new Exception());
    stacktraceService.createFromStackTrace(new Exception());

    assertEquals(st, stacktraceService.getById(st.getId()));
  }

}
