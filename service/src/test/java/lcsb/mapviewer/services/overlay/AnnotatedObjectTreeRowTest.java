package lcsb.mapviewer.services.overlay;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.annotation.services.annotators.IElementAnnotator;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.modelutils.map.ClassTreeNode;
import lcsb.mapviewer.services.ServiceTestFunctions;

public class AnnotatedObjectTreeRowTest extends ServiceTestFunctions {

  @Autowired
  private HgncAnnotator hgncAnnotator;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new AnnotatedObjectTreeRow(new ClassTreeNode(Object.class)));
  }

  @Test
  public void testAnnotatorData() {
    List<IElementAnnotator> list = new ArrayList<>();
    list.add(hgncAnnotator);
    AnnotatedObjectTreeRow row = new AnnotatedObjectTreeRow(new ClassTreeNode(Object.class), list, list, new ArrayList<MiriamType>(),
        new ArrayList<MiriamType>());
    for (AnnotatorData data : row.getUsedAnnotators()) {
      assertTrue(data.getAnnotatorClassName() == HgncAnnotator.class);
    }

  }

}
