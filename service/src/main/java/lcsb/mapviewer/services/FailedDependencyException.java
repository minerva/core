package lcsb.mapviewer.services;

public class FailedDependencyException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public FailedDependencyException(final String message) {
    super(message);
  }

  public FailedDependencyException(final String message, final Exception reason) {
    super(message, reason);
  }

  public FailedDependencyException(final Exception reason) {
    super(reason);
  }

}
