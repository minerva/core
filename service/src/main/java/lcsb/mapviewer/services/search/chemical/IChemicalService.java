package lcsb.mapviewer.services.search.chemical;

import java.util.List;

import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.annotation.services.dapi.ChemicalSearchException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.job.MinervaJobExecutor;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.services.search.IDbSearchService;

/**
 * Service for accessing information about chemical.
 * 
 * @author Ayan Rota
 * 
 */
public interface IChemicalService extends IDbSearchService<Chemical>, MinervaJobExecutor {

  List<String> getSuggestedQueryList(final Project project, final MiriamData disease) throws ChemicalSearchException;

}
