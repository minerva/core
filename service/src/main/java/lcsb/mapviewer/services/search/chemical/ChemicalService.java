package lcsb.mapviewer.services.search.chemical;

import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.annotation.services.dapi.ChemicalParser;
import lcsb.mapviewer.annotation.services.dapi.ChemicalSearchException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.job.MinervaJob;
import lcsb.mapviewer.model.job.MinervaJobPriority;
import lcsb.mapviewer.model.job.MinervaJobType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.jobs.RefreshChemicalInfoMinervaJob;
import lcsb.mapviewer.services.jobs.RefreshChemicalSuggestedQueryListMinervaJob;
import lcsb.mapviewer.services.search.DbSearchCriteria;
import lcsb.mapviewer.services.search.DbSearchService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Implementation of the service that allows access information about chemicals.
 *
 * @author Ayan Rota
 */
@Transactional
@Service
public class ChemicalService extends DbSearchService implements IChemicalService {

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  /**
   * Access point and parser for the online ctd database.
   */
  private final ChemicalParser chemicalParser;

  /**
   * Object used to perform operations on {@link MiriamType#HGNC} annotations.
   */
  private final HgncAnnotator hgncAnnotator;

  private final ProjectDao projectDao;

  private final IMinervaJobService minervaJobService;

  @Autowired
  private IChemicalService self;

  @Autowired
  public ChemicalService(final ChemicalParser chemicalParser,
                         final HgncAnnotator hgncAnnotator,
                         final ProjectDao projectDao,
                         final IMinervaJobService minervaJobService) {
    super();
    this.chemicalParser = chemicalParser;
    this.hgncAnnotator = hgncAnnotator;
    this.minervaJobService = minervaJobService;
    this.projectDao = projectDao;
  }

  @Override
  public Chemical getByName(final String name, final DbSearchCriteria searchCriteria) {
    if (searchCriteria.getDisease() == null) {
      throw new InvalidArgumentException("diseaseID cannot be null");
    }
    if (name == null || name.isEmpty()) {
      return null;
    }
    try {
      List<Chemical> result = chemicalParser.getChemicalsByName(searchCriteria.getDisease(), name);
      if (result == null) {
        return null;
      }
      if (result.isEmpty()) {
        return null;
      } else {
        if (result.size() > 1) {
          logger.warn("More than one chemical found by name: {}", name);
        }
        Chemical chemical = result.get(0);
        addProjectTargets(searchCriteria.getProject(), chemical);
        return chemical;
      }
    } catch (final ChemicalSearchException e) {
      logger.error("Problem with fetch chemical data", e);
      return null;
    }
  }

  @Override
  public List<Chemical> getForTargets(final Collection<Element> targets, final DbSearchCriteria searchCriteria) {
    List<Chemical> chemicalList = new ArrayList<>();
    Set<MiriamData> targetsMiriam = new HashSet<>();
    for (final Element element : targets) {
      if (element instanceof Protein || element instanceof Gene || element instanceof Rna) {
        boolean hgncFound = false;
        for (final MiriamData md : element.getMiriamData()) {
          if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
            targetsMiriam.add(md);
            hgncFound = true;
          }
        }
        if (!hgncFound) {
          MiriamData md = new MiriamData(MiriamType.HGNC_SYMBOL, element.getName());
          try {
            if (hgncAnnotator.isValidHgncMiriam(md)) {
              targetsMiriam.add(md);
            }
          } catch (final AnnotatorException e) {
            logger.error("Problem with accessing HGNC database", e);
          }
        }
      }
    }
    try {
      chemicalList = chemicalParser.getChemicalListByTargets(targetsMiriam, searchCriteria.getDisease());
    } catch (final ChemicalSearchException e) {
      logger.error("Problem with accessing chemical database", e);
    }

    chemicalList.sort(new Chemical.NameComparator());
    for (final Chemical chemical : chemicalList) {
      addProjectTargets(searchCriteria.getProject(), chemical);
    }
    return chemicalList;
  }

  @PostConstruct
  public void init() {
    minervaJobService.registerExecutor(MinervaJobType.REFRESH_CHEMICAL_INFO, self);
    minervaJobService.registerExecutor(MinervaJobType.REFRESH_CHEMICAL_SUGGESTED_QUERY_LIST, self);
  }

  private void refreshChemical(final MiriamData disease, final MiriamData md) {
    try {
      List<Chemical> chemicalList = chemicalParser.getChemicalListByTarget(md, disease);
      for (final Chemical chemical : chemicalList) {
        cacheMiriamData(chemical);
      }
    } catch (final ChemicalSearchException | AnnotatorException e) {
      logger.error("Problem with accessing info about chemical for target: {}", md, e);
    }
  }

  @Override
  public List<String> getSuggestedQueryList(final Project project, final MiriamData disease) throws ChemicalSearchException {
    List<String> result = chemicalParser.getSuggestedQueryList(projectDao.getProjectByProjectId(project.getProjectId()), disease);
    if (result == null) {
      chemicalParser.setChemicalSuggestedQueryList(project.getId(), disease, Collections.singletonList(" "));
      RefreshChemicalSuggestedQueryListMinervaJob parameters = new RefreshChemicalSuggestedQueryListMinervaJob(disease, project.getId());
      MinervaJob job = new MinervaJob(MinervaJobType.REFRESH_CHEMICAL_SUGGESTED_QUERY_LIST, MinervaJobPriority.LOWEST, parameters);
      job.setExternalObject(project);
      minervaJobService.addJob(job);
      result = new ArrayList<>();
    }
    return result;
  }

  @Override
  public void execute(final MinervaJob job) throws Exception {
    if (job.getJobType() == MinervaJobType.REFRESH_CHEMICAL_INFO) {
      RefreshChemicalInfoMinervaJob parameters = new RefreshChemicalInfoMinervaJob(job.getJobParameters());
      MiriamData md = new MiriamData(parameters.getType(), parameters.getId());
      MiriamData disease = new MiriamData(parameters.getDiseaseType(), parameters.getDiseaseId());
      if (Objects.equals(MiriamType.HGNC_SYMBOL, md.getDataType())) {
        refreshChemical(disease, md);
      }
    } else if (job.getJobType() == MinervaJobType.REFRESH_CHEMICAL_SUGGESTED_QUERY_LIST) {
      RefreshChemicalSuggestedQueryListMinervaJob parameters = new RefreshChemicalSuggestedQueryListMinervaJob(job.getJobParameters());
      MiriamData organism = new MiriamData(parameters.getOrganismType(), parameters.getOrganismId());
      Integer projectId = parameters.getProjectId();
      refreshChemicalSuggestedQueryList(organism, projectId);
    } else {
      throw new NotImplementedException(job.getJobType() + " not implemented");
    }
  }

  private void refreshChemicalSuggestedQueryList(final MiriamData organism, final Integer projectId) {
    chemicalParser.refreshChemicalSuggestedQueryList(projectId, organism);
  }

}
