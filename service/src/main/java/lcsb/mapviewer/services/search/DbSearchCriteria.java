package lcsb.mapviewer.services.search;

import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.model.Model;

/**
 * Class with search criteria used by {@link IDbSearchService}. Contains
 * information about {@link lcsb.mapviewer.model.map.MiriamType#TAXONOMY
 * organisms}, {@link Model} where the search is performed, etc.
 * 
 * @author Piotr Gawron
 *
 */
public class DbSearchCriteria {

  /**
   * Ip address of the client that requested this search.
   */
  private String ipAddress;

  /**
   * Id of disease to which results should be contextualized.
   * 
   */
  private MiriamData disease;

  /**
   * Project where client is looking for the results.
   */
  private Project project;

  /**
   * List of organisms used for filtering results. If the list is null or empty
   * then filtering shouldn't be applied. Organisms must be represented as
   * {@link lcsb.mapviewer.model.map.MiriamType#TAXONOMY} MiriamData objects.
   * 
   */
  private List<MiriamData> organisms = new ArrayList<>();

  /**
   * Which set of icons should be used for representing results.
   * 
   */
  private int colorSet = 0;

  /**
   * @return the ipAddress
   * @see #ipAddress
   */
  public String getIpAddress() {
    return ipAddress;
  }

  /**
   * @param ipAddress
   *          the ipAddress to set
   * @see #ipAddress
   * @return this object after changes
   */
  public DbSearchCriteria ipAddress(final String ipAddress) {
    this.ipAddress = ipAddress;
    return this;
  }

  /**
   * @return the project
   * @see #model
   */
  public Project getProject() {
    return project;
  }

  /**
   * @param project
   *          the project to set
   * @see #project
   * @return this object after changes
   */
  public DbSearchCriteria project(final Project project) {
    this.project = project;
    return this;
  }

  /**
   * @return the organisms
   * @see #organisms
   */
  public List<MiriamData> getOrganisms() {
    return organisms;
  }

  /**
   * @param organisms
   *          the organisms to add
   * @see #organisms
   * @return this object after changes
   */
  public DbSearchCriteria organisms(final List<MiriamData> organisms) {
    this.organisms.addAll(organisms);
    return this;
  }

  /**
   * @param organism
   *          the organism to add
   * @see #organisms
   * @return this object after changes
   */
  public DbSearchCriteria organisms(final MiriamData organism) {
    this.organisms.add(organism);
    return this;
  }

  /**
   * @param organisms
   *          the organisms to add
   * @see #organisms
   * @return this object after changes
   */
  public DbSearchCriteria organisms(final MiriamData[] organisms) {
    for (final MiriamData miriamData : organisms) {
      this.organisms.add(miriamData);
    }
    return this;
  }

  /**
   * @return the colorSet
   * @see #colorSet
   */
  public int getColorSet() {
    return colorSet;
  }

  /**
   * @param colorSet
   *          the colorSet to set
   * @see #colorSet
   * @return this object after changes
   */
  public DbSearchCriteria colorSet(final int colorSet) {
    this.colorSet = colorSet;
    return this;
  }

  /**
   * @return the disease
   * @see #disease
   */
  public MiriamData getDisease() {
    return disease;
  }

  /**
   * @param disease
   *          the disease to set
   * @see #disease
   * @return this object after changes
   */
  public DbSearchCriteria disease(final MiriamData disease) {
    this.disease = disease;
    return this;
  }
}
