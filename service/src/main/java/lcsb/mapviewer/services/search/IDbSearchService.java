package lcsb.mapviewer.services.search;

import lcsb.mapviewer.model.map.species.Element;

import java.util.Collection;
import java.util.List;

/**
 * Service that allows to retrieve information of type T from external
 * resources/databases.
 *
 * @param <T> type of returned results
 * @author Piotr Gawron
 */
public interface IDbSearchService<T> {

  /**
   * Returns the drugs found by drug name.
   *
   * @param name           name of the drug
   * @param searchCriteria set of {@link DbSearchCriteria} used for searching (like:
   *                       {@link lcsb.mapviewer.model.map.model.Model Model})
   * @return list of drugs for drug names
   */
  T getByName(final String name, final DbSearchCriteria searchCriteria);

  /**
   * Returns the list of drugs that target at least one of the element in the
   * parameter.
   *
   * @param targetElements list of elements that should be targeted by drug
   * @param searchCriteria set of {@link DbSearchCriteria} used for searching (like:
   *                       {@link lcsb.mapviewer.model.map.model.Model Model})
   * @return list of drugs that targets something from the elements collection
   */
  List<T> getForTargets(final Collection<Element> targetElements, final DbSearchCriteria searchCriteria);

}
