package lcsb.mapviewer.services.search.drug;

import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.services.DrugSearchException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.job.MinervaJobExecutor;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.services.search.IDbSearchService;

import java.util.List;

/**
 * Service for accessing information about drugs.
 *
 * @author Piotr Gawron
 */
public interface IDrugService extends IDbSearchService<Drug>, MinervaJobExecutor {

  List<String> getSuggestedQueryList(final Project project, final MiriamData disease) throws DrugSearchException;

  void submitRefreshChemblSuggestedQueryList(final MiriamData organism, final Project project);

  void submitRefreshDrugBankSuggestedQueryList(final MiriamData organism, final Project project);
}
