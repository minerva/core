package lcsb.mapviewer.services.search.drug;

import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.services.ChEMBLParser;
import lcsb.mapviewer.annotation.services.DrugAnnotation;
import lcsb.mapviewer.annotation.services.DrugSearchException;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.dapi.DrugBankParser;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.job.MinervaJob;
import lcsb.mapviewer.model.job.MinervaJobPriority;
import lcsb.mapviewer.model.job.MinervaJobType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.jobs.RefreshDrugInfoMinervaJob;
import lcsb.mapviewer.services.jobs.RefreshDrugSuggestedQueryListMinervaJob;
import lcsb.mapviewer.services.search.DbSearchCriteria;
import lcsb.mapviewer.services.search.DbSearchService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Implementation of the service that allows access information about drugs.
 *
 * @author Piotr Gawron
 */
@Transactional
@Service
public class DrugService extends DbSearchService implements IDrugService {

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  /**
   * Access point and parser for the online DrugBank database.
   */
  private final DrugBankParser drugBankParser;

  /**
   * Access point and parser for the online ChEMBL database.
   */
  private final ChEMBLParser chEMBLParser;

  private final ProjectDao projectDao;

  private final IMinervaJobService minervaJobService;

  @Autowired
  private IDrugService self;

  @Autowired
  public DrugService(final DrugBankParser drugBankParser,
                     final ProjectDao projectDao,
                     final ChEMBLParser chEMBLParser,
                     final IMinervaJobService minervaJobService) {
    this.drugBankParser = drugBankParser;
    this.chEMBLParser = chEMBLParser;
    this.projectDao = projectDao;
    this.minervaJobService = minervaJobService;
  }

  @PostConstruct
  public void init() {
    minervaJobService.registerExecutor(MinervaJobType.REFRESH_DRUG_INFO, self);
    minervaJobService.registerExecutor(MinervaJobType.REFRESH_CHEMBL_SUGGESTED_QUERY_LIST, self);
    minervaJobService.registerExecutor(MinervaJobType.REFRESH_DRUG_BANK_SUGGESTED_QUERY_LIST, self);
  }

  /**
   * Removes targets for unknown organisms from the drug.
   *
   * @param drug      drug from which we want to remove targets
   * @param organisms organisms that should be kept
   */
  private void removeUnknownOrganisms(final Drug drug, final List<MiriamData> organisms) {
    if (!organisms.isEmpty()) {
      List<Target> toRemove = new ArrayList<Target>();
      for (final Target target : drug.getTargets()) {
        boolean remove = true;
        for (final MiriamData organism : organisms) {
          if (target.getOrganism() == null) {
            remove = false;
          } else if (target.getOrganism().equals(organism)) {
            remove = false;
          }
        }
        if (remove) {
          logger.debug("Target {} removed from list because results are limited to organisms: {}", drug.getName(), organisms);
          toRemove.add(target);
        }
      }
      drug.getTargets().removeAll(toRemove);
    }
  }

  /**
   * Merges two drugs (typically from different databases) into one
   * representation.
   *
   * @param drug  first drug to merge
   * @param drug2 second drug to merge
   * @return merged drug
   */
  private Drug mergeDrugs(final Drug drug, final Drug drug2) {
    if (drug == null) {
      return drug2;
    }
    if (drug2 == null) {
      return drug;
    }

    Drug result = new Drug(drug);
    result.getSources().addAll(drug2.getSources());
    if (result.getName() == null || result.getName().trim().isEmpty()) {
      result.setName(drug2.getName());
    }
    if (result.getDescription() == null || result.getDescription().trim().isEmpty()) {
      result.setDescription(drug2.getDescription());
    }
    for (final String synonym : drug2.getSynonyms()) {
      if (!result.getSynonyms().contains(synonym)) {
        result.addSynonym(synonym);
      }
    }
    for (final String brandName : drug2.getBrandNames()) {
      if (!result.getBrandNames().contains(brandName)) {
        result.addBrandName(brandName);
      }
    }

    if (result.getBloodBrainBarrier().equals("YES")) {
      if (drug2.getBloodBrainBarrier().equals("NO")) {
        logger.warn("Problem with merging drugs. Blood Brain Barrier contains opposite values: "
            + result.getBloodBrainBarrier() + ", " + drug2.getBloodBrainBarrier());
        result.setBloodBrainBarrier("N/A");
      }
    } else if (result.getBloodBrainBarrier().equals("NO")) {
      if (drug2.getBloodBrainBarrier().equals("YES")) {
        logger.warn("Problem with merging drugs. Blood Brain Barrier contains opposite values: "
            + result.getBloodBrainBarrier() + ", " + drug2.getBloodBrainBarrier());
        result.setBloodBrainBarrier("N/A");
      }
    } else if (result.getBloodBrainBarrier().equals("N/A")) {
      result.setBloodBrainBarrier(drug.getBloodBrainBarrier());
    } else {
      logger.warn("Unknown status of drug BBB:" + result.getBloodBrainBarrier());
    }

    if (result.getApproved() == null) {
      result.setApproved(drug2.getApproved());
    } else if (drug2.getApproved() != null) {
      if (!drug2.getApproved().equals(result.getApproved())) {
        logger.warn("Problem with merging drugs. Approved contains opposite values: " + result.getApproved() + ", "
            + drug2.getApproved());
        result.setApproved(null);
      }
    }

    result.getTargets().addAll(drug2.getTargets());
    return result;
  }

  @Override
  public Drug getByName(final String name, final DbSearchCriteria searchCriteria) {
    if (name.trim().isEmpty()) {
      return null;
    }
    DrugAnnotation secondParser = chEMBLParser;
    Drug drug = null;
    try {
      drug = drugBankParser.findDrug(name);

      if (drug == null) {
        drug = chEMBLParser.findDrug(name);
        secondParser = drugBankParser;
      }
      if (drug != null) {
        removeUnknownOrganisms(drug, searchCriteria.getOrganisms());
      }
    } catch (final DrugSearchException e) {
      logger.error("Problem with accessing drugBank parser", e);
    }
    try {
      Set<String> foundDrugs = new HashSet<>();
      Set<String> searchNames = new HashSet<>();
      // two-step searching in Chembl:
      // * search for name
      searchNames.add(name);
      if (drug != null && drug.getName() != null) {
        // * search for name of a drug in DrugBank
        searchNames.add(drug.getName());
        // * search for all synonyms found in DrugBank
        // searchNames.addAll(drug.getSynonyms());
      }
      for (final String string : searchNames) {
        Drug drug2 = secondParser.findDrug(string);
        if (drug2 != null) {
          removeUnknownOrganisms(drug2, searchCriteria.getOrganisms());
          // don't add drugs that were already found
          if (!foundDrugs.contains(drug2.getSources().get(0).getResource())) {
            drug = mergeDrugs(drug, drug2);
            foundDrugs.add(drug2.getSources().get(0).getResource());
          }
        }
      }
    } catch (final DrugSearchException e) {
      logger.error("Problem with accessing ChEMBL parser", e);
    }
    addProjectTargets(searchCriteria.getProject(), drug);

    return drug;
  }

  @Override
  public List<Drug> getForTargets(final Collection<Element> targets, final DbSearchCriteria searchCriteria) {
    List<Drug> drugList = new ArrayList<>();
    Set<MiriamData> targetsMiriam = getHgncIdentifiers(targets);
    try {
      List<Drug> drugs = drugBankParser.getDrugListByTargets(targetsMiriam, searchCriteria.getOrganisms());
      drugList.addAll(drugs);
    } catch (final DrugSearchException e) {
      logger.error("Problem with accessing drugBank parser", e);
    }

    try {
      List<Drug> drugs = chEMBLParser.getDrugListByTargets(targetsMiriam, searchCriteria.getOrganisms());
      drugList.addAll(drugs);
    } catch (final DrugSearchException e) {
      logger.error("Problem with accessing chembl parser", e);
    }

    for (final Drug drug : drugList) {
      removeUnknownOrganisms(drug, searchCriteria.getOrganisms());
      addProjectTargets(searchCriteria.getProject(), drug);
    }

    drugList.sort(new Drug.NameComparator());
    return drugList;
  }

  private void refreshDrugsForMiriamData(final List<MiriamData> organisms, final MiriamData md) {
    try {
      List<Drug> drugList = drugBankParser.getDrugListByTarget(md, organisms);
      for (final Drug drug : drugList) {
        cacheMiriamData(drug);
      }
    } catch (final DrugSearchException | AnnotatorException e) {
      logger.error("Problem with accessing info about DrugBank for target: {}", md, e);
    }
    try {
      List<Drug> drugList = chEMBLParser.getDrugListByTarget(md, organisms);
      for (final Drug drug : drugList) {
        cacheMiriamData(drug);
      }
    } catch (final DrugSearchException | AnnotatorException e) {
      logger.error("Problem with accessing info about chembl for target: {}", md, e);
    }
  }

  @Override
  public List<String> getSuggestedQueryList(final Project queryProject, final MiriamData organism) throws DrugSearchException {
    Project project = projectDao.getProjectByProjectId(queryProject.getProjectId());

    Set<String> resultSet = new HashSet<>();
    List<String> drugBankList = drugBankParser.getSuggestedQueryList(project, organism);
    List<String> chemblList = chEMBLParser.getSuggestedQueryList(project, organism);
    if (drugBankList == null) {
      drugBankParser.setDrugSuggestedQueryList(project.getId(), organism, Collections.singletonList(" "));
      submitRefreshDrugBankSuggestedQueryList(organism, project);
    } else {
      for (final String string : drugBankList) {
        resultSet.add(string.toLowerCase());
      }
    }

    if (chemblList == null) {
      chEMBLParser.setDrugSuggestedQueryList(project.getId(), organism, Collections.singletonList(" "));
      submitRefreshChemblSuggestedQueryList(organism, project);
    } else {
      for (final String string : chemblList) {
        resultSet.add(string.toLowerCase());
      }
    }

    List<String> result = new ArrayList<>(resultSet);
    Collections.sort(result);
    return result;
  }

  @Override
  public void submitRefreshChemblSuggestedQueryList(final MiriamData organism, final Project project) {
    RefreshDrugSuggestedQueryListMinervaJob parameters = new RefreshDrugSuggestedQueryListMinervaJob(organism, project.getId());
    MinervaJob job = new MinervaJob(MinervaJobType.REFRESH_CHEMBL_SUGGESTED_QUERY_LIST, MinervaJobPriority.LOWEST, parameters);
    job.setExternalObject(project);
    minervaJobService.addJob(job);
  }

  @Override
  public void submitRefreshDrugBankSuggestedQueryList(final MiriamData organism, final Project project) {
    RefreshDrugSuggestedQueryListMinervaJob parameters = new RefreshDrugSuggestedQueryListMinervaJob(organism, project.getId());
    MinervaJob job = new MinervaJob(MinervaJobType.REFRESH_DRUG_BANK_SUGGESTED_QUERY_LIST, MinervaJobPriority.LOWEST, parameters);
    job.setExternalObject(project);
    minervaJobService.addJob(job);
  }

  @Override
  public void execute(final MinervaJob job) throws Exception {
    if (job.getJobType() == MinervaJobType.REFRESH_DRUG_INFO) {
      RefreshDrugInfoMinervaJob parameters = new RefreshDrugInfoMinervaJob(job.getJobParameters());
      MiriamData md = new MiriamData(parameters.getType(), parameters.getId());
      if (Objects.equals(MiriamType.HGNC_SYMBOL, md.getDataType()) || Objects.equals(MiriamType.UNIPROT, md.getDataType())) {
        refreshDrugsForMiriamData(Collections.emptyList(), md);
      }
    } else if (job.getJobType() == MinervaJobType.REFRESH_CHEMBL_SUGGESTED_QUERY_LIST) {
      RefreshDrugSuggestedQueryListMinervaJob parameters = new RefreshDrugSuggestedQueryListMinervaJob(job.getJobParameters());
      MiriamData organism = new MiriamData(parameters.getOrganismType(), parameters.getOrganismId());
      Integer projectId = parameters.getProjectId();
      refreshChemblSuggestedQueryList(organism, projectId);
    } else if (job.getJobType() == MinervaJobType.REFRESH_DRUG_BANK_SUGGESTED_QUERY_LIST) {
      RefreshDrugSuggestedQueryListMinervaJob parameters = new RefreshDrugSuggestedQueryListMinervaJob(job.getJobParameters());
      MiriamData organism = new MiriamData(parameters.getOrganismType(), parameters.getOrganismId());
      Integer projectId = parameters.getProjectId();
      refreshDrugBankSuggestedQueryList(organism, projectId);
    } else {
      throw new NotImplementedException(job.getJobType() + " not implemented");
    }
  }

  private void refreshChemblSuggestedQueryList(final MiriamData organism, final Integer projectId) {
    chEMBLParser.refreshDrugSuggestedQueryList(projectId, organism);
  }

  private void refreshDrugBankSuggestedQueryList(final MiriamData organism, final Integer projectId) {
    drugBankParser.refreshDrugSuggestedQueryList(projectId, organism);
  }
}
