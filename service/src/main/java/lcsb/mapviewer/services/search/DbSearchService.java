package lcsb.mapviewer.services.search;

import lcsb.mapviewer.annotation.data.ProjectTarget;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.data.TargettingStructure;
import lcsb.mapviewer.annotation.services.MiriamConnector;
import lcsb.mapviewer.annotation.services.PubmedParser;
import lcsb.mapviewer.annotation.services.PubmedSearchException;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.interfaces.IElementService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Transactional
public abstract class DbSearchService {

  private static final Logger logger = LogManager.getLogger();

  @Autowired
  private HgncAnnotator hgncAnnotator;

  @Autowired
  private PubmedParser pubmedParser;

  @Autowired
  private MiriamConnector miriamConnector;

  @Autowired
  private IElementService elementService;

  protected void cacheMiriamData(final TargettingStructure targettingStructure) throws AnnotatorException {
    final Set<MiriamData> result = new HashSet<>();
    result.addAll(targettingStructure.getSources());
    for (final Target target : targettingStructure.getTargets()) {
      result.addAll(target.getGenes());
      result.addAll(target.getReferences());

    }
    for (final MiriamData miriamData : result) {
      miriamConnector.getUrlString(miriamData);
      if (MiriamType.PUBMED.equals(miriamData.getDataType())) {
        try {
          pubmedParser.getPubmedArticleById(Integer.valueOf(miriamData.getResource()));
        } catch (final NumberFormatException | PubmedSearchException e) {
          throw new AnnotatorException(e);
        }
      }
    }
  }

  protected void assignMiriamLinks(final TargettingStructure targettingStructure) {
    final Set<MiriamData> result = new HashSet<>(targettingStructure.getSources());
    for (final Target target : targettingStructure.getTargets()) {
      result.addAll(target.getGenes());
      result.addAll(target.getReferences());
    }
    for (final MiriamData miriamData : result) {
      miriamData.setLink(miriamConnector.getUrlString(miriamData));
    }
  }


  protected Set<MiriamData> getHgncIdentifiers(final ModelData model) {
    final Set<BioEntity> targetElements = new HashSet<>();
    targetElements.addAll(model.getElements());
    targetElements.addAll(model.getReactions());
    return getHgncIdentifiers(targetElements);
  }

  protected Set<MiriamData> getHgncIdentifiers(final Collection<? extends BioEntity> bioEntities) {
    final Set<MiriamData> result = new HashSet<>();
    for (final BioEntity element : bioEntities) {
      for (final MiriamData md : element.getMiriamData()) {
        if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
          result.add(md);
        }
      }
      boolean validClass = false;
      for (final Class<?> clazz : MiriamType.HGNC_SYMBOL.getValidClass()) {
        if (clazz.isAssignableFrom(element.getClass())) {
          validClass = true;
        }
      }
      if (validClass) {
        final MiriamData md = new MiriamData(MiriamType.HGNC_SYMBOL, element.getName());
        try {
          if (hgncAnnotator.isValidHgncMiriam(md)) {
            result.add(md);
          }
        } catch (final AnnotatorException e) {
          logger.error("Problem with accessing HGNC database", e);
        }
      }

    }
    return result;
  }

  public ProjectTarget createTargetWithProjectData(final Target target, final Project project) {
    final List<BioEntity> bioEntities = new ArrayList<>();

    final List<Map<ElementProperty, Object>> searchPropertyList = new ArrayList<>();

    switch (target.getType()) {
      // case COMPLEX_PROTEIN: {
      // Map<ElementProperty, List<? extends Object>> properties = new
      // HashMap<>();
      // if (target.getSource() != null) {
      // properties.put(ElementProperty.ANNOTATION,
      // Arrays.asList(target.getSource()));
      // } else {
      // properties.put(ElementProperty.ANNOTATION, new ArrayList<>());
      // }
      // searchPropertyList.add(properties);
      // break;
      // }
      case SINGLE_PROTEIN: {
        Map<ElementProperty, Object> properties = getSearchByAnnotationsProperties(target);
        searchPropertyList.add(properties);

        properties = getSearchByProteinRnaGeneNameProperties(target);
        searchPropertyList.add(properties);
        break;
      }
      case COMPLEX_PROTEIN:
      case PROTEIN_FAMILY: {
        final Map<ElementProperty, Object> properties = getSearchByProteinRnaGeneNameProperties(target);

        searchPropertyList.add(properties);
        break;
      }
      case OTHER: {
        Map<ElementProperty, Object> properties = getSearchByAnnotationsProperties(target);
        searchPropertyList.add(properties);

        properties = getSearchByNameProperties(target);
        searchPropertyList.add(properties);
        break;
      }
      default:
        throw new InvalidArgumentException("Unknown drug target type: " + target.getType());
    }

    for (final Map<ElementProperty, Object> properties : searchPropertyList) {
      properties.put(ElementProperty.PROJECT, Collections.singletonList(project));
      bioEntities.addAll(elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent());
    }
    return new ProjectTarget(target, bioEntities);

  }

  protected void addProjectTargets(final Project project, final TargettingStructure structure) {
    if (project != null && structure != null) {
      final List<ProjectTarget> projectTargets = new ArrayList<>();
      for (final Target target : structure.getTargets()) {
        if (target != null) {
          projectTargets.add(createTargetWithProjectData(target, project));
        }
      }
      projectTargets.sort(ProjectTarget.COMPARATOR);

      structure.removeTargets(structure.getTargets());
      structure.addTargets(projectTargets);

    }
    if (structure != null) {
      assignMiriamLinks(structure);
    }
  }

  private Map<ElementProperty, Object> getSearchByAnnotationsProperties(final Target target) {
    final Map<ElementProperty, Object> properties = new HashMap<>();
    final List<MiriamData> annotations = new ArrayList<>();
    if (target.getSource() != null) {
      annotations.add(target.getSource());
    }
    annotations.addAll(target.getGenes());
    properties.put(ElementProperty.ANNOTATION, annotations);
    return properties;
  }

  private Map<ElementProperty, Object> getSearchByProteinRnaGeneNameProperties(final Target target) {
    final Map<ElementProperty, Object> properties = getSearchByNameProperties(target);
    final List<Class<? extends BioEntity>> classes = new ArrayList<>();
    classes.addAll(new ElementUtils().getClassesByStringTypes("Protein"));
    classes.addAll(new ElementUtils().getClassesByStringTypes("RNA"));
    classes.addAll(new ElementUtils().getClassesByStringTypes("Gene"));
    properties.put(ElementProperty.CLASS, classes);
    return properties;
  }

  private Map<ElementProperty, Object> getSearchByNameProperties(final Target target) {
    final Map<ElementProperty, Object> properties = new HashMap<>();
    final List<String> names = new ArrayList<>();
    if (target.getSource() != null) {
      names.add(target.getSource().getResource());
    }
    for (final MiriamData md : target.getGenes()) {
      names.add(md.getResource());
    }
    properties.put(ElementProperty.NAME, names);
    return properties;
  }

}
