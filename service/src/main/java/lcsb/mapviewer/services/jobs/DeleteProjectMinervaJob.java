package lcsb.mapviewer.services.jobs;

import java.util.HashMap;
import java.util.Map;

import lcsb.mapviewer.model.job.InvalidMinervaJobParameters;
import lcsb.mapviewer.model.job.MinervaJobParameters;

public class DeleteProjectMinervaJob extends MinervaJobParameters {

  private String projectId;
  private String directory;

  public DeleteProjectMinervaJob(final String projectId, final String directory) {
    this.projectId = projectId;
    this.directory = directory;
  }

  public DeleteProjectMinervaJob(final Map<String, Object> jobParameters) throws InvalidMinervaJobParameters {
    super(jobParameters);
  }

  @Override
  protected void assignData(final Map<String, Object> jobParameters) {
    this.projectId = (String) jobParameters.get("projectId");
    this.directory = (String) jobParameters.get("directory");
  }

  @Override
  protected Map<String, Object> getJobParameters() {
    Map<String, Object> jobParameters = new HashMap<>();
    jobParameters.put("projectId", projectId);
    jobParameters.put("directory", directory);
    return jobParameters;
  }

  public String getProjectId() {
    return projectId;
  }

  public String getDirectory() {
    return directory;
  }

}
