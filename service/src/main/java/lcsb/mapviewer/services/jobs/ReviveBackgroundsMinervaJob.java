package lcsb.mapviewer.services.jobs;

import java.util.HashMap;
import java.util.Map;

import lcsb.mapviewer.model.job.InvalidMinervaJobParameters;
import lcsb.mapviewer.model.job.MinervaJobParameters;

public class ReviveBackgroundsMinervaJob extends MinervaJobParameters {

  private String projectId;

  public ReviveBackgroundsMinervaJob(final String projectId) {
    this.projectId = projectId;
  }

  public ReviveBackgroundsMinervaJob(final Map<String, Object> jobParameters) throws InvalidMinervaJobParameters {
    super(jobParameters);
  }

  @Override
  protected void assignData(final Map<String, Object> jobParameters) {
    this.projectId = (String) jobParameters.get("projectId");
  }

  @Override
  protected Map<String, Object> getJobParameters() {
    Map<String, Object> jobParameters = new HashMap<>();
    jobParameters.put("projectId", projectId);
    return jobParameters;
  }

  public String getProjectId() {
    return projectId;
  }

}
