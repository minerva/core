package lcsb.mapviewer.services.jobs;

import java.util.HashMap;
import java.util.Map;

import lcsb.mapviewer.model.job.InvalidMinervaJobParameters;
import lcsb.mapviewer.model.job.MinervaJobParameters;

public class DeleteBackgroundMinervaJob extends MinervaJobParameters {

  private String directory;
  private Integer backgroundId;

  public DeleteBackgroundMinervaJob(final Integer backgroundId, final String directory) {
    this.backgroundId = backgroundId;
    this.directory = directory;
  }

  public DeleteBackgroundMinervaJob(final Map<String, Object> jobParameters) throws InvalidMinervaJobParameters {
    super(jobParameters);
  }

  @Override
  protected void assignData(final Map<String, Object> jobParameters) {
    this.backgroundId = (Integer) jobParameters.get("backgroundId");
    this.directory = (String) jobParameters.get("directory");
  }

  @Override
  protected Map<String, Object> getJobParameters() {
    Map<String, Object> jobParameters = new HashMap<>();
    jobParameters.put("backgroundId", backgroundId);
    jobParameters.put("directory", directory);
    return jobParameters;
  }

  public String getDirectory() {
    return directory;
  }

  public int getBackgroundId() {
    return backgroundId;
  }

}
