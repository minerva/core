package lcsb.mapviewer.services.jobs;

import java.util.Map;

import lcsb.mapviewer.model.job.InvalidMinervaJobParameters;
import lcsb.mapviewer.model.map.MiriamType;

public class RefreshDrugInfoMinervaJob extends RefreshInfoFromMiriamMinervaJob {

  public RefreshDrugInfoMinervaJob(final MiriamType type, final String id) {
    super(type, id);
  }

  public RefreshDrugInfoMinervaJob(final Map<String, Object> jobParameters) throws InvalidMinervaJobParameters {
    super(jobParameters);
  }
}
