package lcsb.mapviewer.services.jobs;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.job.InvalidMinervaJobParameters;
import lcsb.mapviewer.model.job.MinervaJobParameters;
import lcsb.mapviewer.model.user.UserAnnotationSchema;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class AnnotateProjectMinervaJob extends MinervaJobParameters {

  private static final Logger logger = LogManager.getLogger();

  private String projectId;

  private UserAnnotationSchema schema;

  public AnnotateProjectMinervaJob(final String projectId, final UserAnnotationSchema schema) {
    this.projectId = projectId;
    this.schema = schema;
  }

  public AnnotateProjectMinervaJob(final Map<String, Object> jobParameters) throws InvalidMinervaJobParameters {
    super(jobParameters);
  }

  @Override
  protected void assignData(final Map<String, Object> jobParameters) {
    ObjectMapper mapper = new ObjectMapper();

    this.projectId = (String) jobParameters.get("projectId");
    Object schemaData = jobParameters.get("schema");
    if (schemaData != null) {
      try {
        String serializedSchema = mapper.writeValueAsString(schemaData);
        this.schema = mapper.readValue(serializedSchema, new TypeReference<UserAnnotationSchema>() {
        });
      } catch (Exception e) {
        logger.error("Problem with deserializing task", e);
        this.schema = null;
      }
    }
  }

  @Override
  protected Map<String, Object> getJobParameters() {
    ObjectMapper mapper = new ObjectMapper();
    Map<String, Object> jobParameters = new HashMap<>();
    jobParameters.put("projectId", projectId);
    try {
      String serializedSchema = mapper.writeValueAsString(schema);
      jobParameters.put("schema", mapper.readValue(serializedSchema, new TypeReference<Map<String, Object>>() {
      }));
    } catch (Exception e) {
      logger.error("Problem with serializing task", e);
      this.schema = null;
    }

    return jobParameters;
  }

  public String getProjectId() {
    return projectId;
  }

  public UserAnnotationSchema getSchema() {
    return schema;
  }
}
