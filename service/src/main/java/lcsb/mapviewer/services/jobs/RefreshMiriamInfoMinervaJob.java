package lcsb.mapviewer.services.jobs;

import java.util.HashMap;
import java.util.Map;

import lcsb.mapviewer.model.job.InvalidMinervaJobParameters;
import lcsb.mapviewer.model.job.MinervaJobParameters;

public class RefreshMiriamInfoMinervaJob extends MinervaJobParameters {

  private Integer id;

  public RefreshMiriamInfoMinervaJob(final Integer id) {
    this.id = id;
  }

  public RefreshMiriamInfoMinervaJob(final Map<String, Object> jobParameters) throws InvalidMinervaJobParameters {
    super(jobParameters);
  }

  @Override
  protected void assignData(final Map<String, Object> jobParameters) {
    this.id = (Integer) jobParameters.get("id");
  }

  @Override
  protected Map<String, Object> getJobParameters() {
    Map<String, Object> jobParameters = new HashMap<>();
    jobParameters.put("id", id);
    return jobParameters;
  }

  public Integer getId() {
    return id;
  }

}
