package lcsb.mapviewer.services.jobs;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.model.job.InvalidMinervaJobParameters;
import lcsb.mapviewer.model.job.MinervaJobParameters;
import lcsb.mapviewer.model.map.MiriamType;

public class RefreshInfoFromMiriamMinervaJob extends MinervaJobParameters {

  private static Logger logger = LogManager.getLogger();

  private String id;
  private MiriamType type;

  public RefreshInfoFromMiriamMinervaJob(final MiriamType type, final String id) {
    this.id = id;
    this.type = type;
  }

  public RefreshInfoFromMiriamMinervaJob(final Map<String, Object> jobParameters) throws InvalidMinervaJobParameters {
    super(jobParameters);
  }

  @Override
  protected void assignData(final Map<String, Object> jobParameters) {
    this.id = (String) jobParameters.get("id");
    String stringType = (String) jobParameters.get("type");
    if (stringType != null) {
      this.type = MiriamType.valueOf(stringType);
    }
  }

  @Override
  protected Map<String, Object> getJobParameters() {
    Map<String, Object> jobParameters = new HashMap<>();
    jobParameters.put("id", id);
    jobParameters.put("type", type);
    return jobParameters;
  }

  public String getId() {
    return id;
  }

  public MiriamType getType() {
    return type;
  }

}
