package lcsb.mapviewer.services.jobs;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import lcsb.mapviewer.model.job.InvalidMinervaJobParameters;
import lcsb.mapviewer.model.job.MinervaJobParameters;
import lcsb.mapviewer.services.utils.CreateProjectParams;

public class CreateProjectMinervaJob extends MinervaJobParameters {

  private CreateProjectParams parameters;

  public CreateProjectMinervaJob(final CreateProjectParams params) {
    parameters = params;
  }

  public CreateProjectMinervaJob(final Map<String, Object> jobParameters) throws InvalidMinervaJobParameters {
    super(jobParameters);
  }

  @Override
  protected void assignData(final Map<String, Object> jobParameters) throws InvalidMinervaJobParameters {
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      this.parameters = objectMapper.readValue(objectMapper.writeValueAsString(jobParameters.get("parameters")),
          CreateProjectParams.class);
    } catch (Exception e) {
      throw new InvalidMinervaJobParameters(e);
    }
  }

  @Override
  protected Map<String, Object> getJobParameters() {
    Map<String, Object> result = new HashMap<>();
    result.put("parameters", parameters);
    return result;
  }

  public CreateProjectParams getParameters() {
    return parameters;
  }

}
