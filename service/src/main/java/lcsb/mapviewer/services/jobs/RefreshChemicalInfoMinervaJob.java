package lcsb.mapviewer.services.jobs;

import java.util.Map;

import lcsb.mapviewer.model.job.InvalidMinervaJobParameters;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

public class RefreshChemicalInfoMinervaJob extends RefreshInfoFromMiriamMinervaJob {

  private MiriamType diseaseType;
  private String diseaseId;

  public RefreshChemicalInfoMinervaJob(final MiriamData identifier, final MiriamData disease) {
    super(identifier.getDataType(), identifier.getResource());
    this.diseaseType = disease.getDataType();
    this.diseaseId = disease.getResource();
  }

  public RefreshChemicalInfoMinervaJob(final Map<String, Object> jobParameters) throws InvalidMinervaJobParameters {
    super(jobParameters);
  }

  @Override
  protected void assignData(final Map<String, Object> jobParameters) {
    super.assignData(jobParameters);
    this.diseaseId = (String) jobParameters.get("diseaseId");
    String stringType = (String) jobParameters.get("diseaseType");
    if (stringType != null) {
      this.diseaseType = MiriamType.valueOf(stringType);
    }
  }

  @Override
  protected Map<String, Object> getJobParameters() {
    Map<String, Object> jobParameters = super.getJobParameters();
    jobParameters.put("diseaseId", diseaseId);
    jobParameters.put("diseaseType", diseaseType);
    return jobParameters;
  }

  public MiriamType getDiseaseType() {
    return diseaseType;
  }

  public String getDiseaseId() {
    return diseaseId;
  }

}
