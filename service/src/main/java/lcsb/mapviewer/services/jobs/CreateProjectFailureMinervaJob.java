package lcsb.mapviewer.services.jobs;

import java.util.HashMap;
import java.util.Map;

import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.job.InvalidMinervaJobParameters;
import lcsb.mapviewer.model.job.MinervaJobParameters;

public class CreateProjectFailureMinervaJob extends MinervaJobParameters {

  private String message;
  private String projectId;
  private String errorType;

  public CreateProjectFailureMinervaJob(final String message, final String projectId, final String errorType) {
    this.message = message;
    this.projectId = projectId;
    this.errorType = errorType;
  }

  public CreateProjectFailureMinervaJob(final Map<String, Object> jobParameters) throws InvalidMinervaJobParameters {
    super(jobParameters);
  }

  @Override
  protected void assignData(final Map<String, Object> jobParameters) {
    this.message = (String) jobParameters.get("message");
    this.projectId = (String) jobParameters.get("projectId");
    this.errorType = (String) jobParameters.get("errorType");
  }

  @Override
  protected Map<String, Object> getJobParameters() {
    Map<String, Object> jobParameters = new HashMap<>();
    jobParameters.put("message", message);
    jobParameters.put("projectId", projectId);
    jobParameters.put("errorType", errorType);

    return jobParameters;
  }

  public String getProjectId() {
    return projectId;
  }

  public String getMessage() {
    return message;
  }

  public ProjectLogEntryType getType() {
    return ProjectLogEntryType.valueOf(errorType);
  }

}
