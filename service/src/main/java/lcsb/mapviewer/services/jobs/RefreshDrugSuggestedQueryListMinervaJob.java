package lcsb.mapviewer.services.jobs;

import lcsb.mapviewer.model.job.InvalidMinervaJobParameters;
import lcsb.mapviewer.model.job.MinervaJobParameters;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

import java.util.HashMap;
import java.util.Map;

public class RefreshDrugSuggestedQueryListMinervaJob extends MinervaJobParameters {

  private MiriamType organismType;
  private String organismId;
  private Integer projectId;

  public RefreshDrugSuggestedQueryListMinervaJob(final MiriamData organism, final int projectId) {
    this.organismType = organism.getDataType();
    this.organismId = organism.getResource();
    this.projectId = projectId;
  }

  public RefreshDrugSuggestedQueryListMinervaJob(final Map<String, Object> jobParameters) throws InvalidMinervaJobParameters {
    super(jobParameters);
  }

  @Override
  protected void assignData(final Map<String, Object> jobParameters) {
    this.organismId = (String) jobParameters.get("organismId");
    String stringType = (String) jobParameters.get("organismType");
    if (stringType != null) {
      this.organismType = MiriamType.valueOf(stringType);
    }
    this.projectId = (Integer) jobParameters.get("projectId");
  }

  @Override
  protected Map<String, Object> getJobParameters() {
    Map<String, Object> jobParameters = new HashMap<>();
    jobParameters.put("organismId", organismId);
    jobParameters.put("organismType", organismType);
    jobParameters.put("projectId", projectId);
    return jobParameters;
  }

  public MiriamType getOrganismType() {
    return organismType;
  }

  public String getOrganismId() {
    return organismId;
  }

  public Integer getProjectId() {
    return projectId;
  }
}
