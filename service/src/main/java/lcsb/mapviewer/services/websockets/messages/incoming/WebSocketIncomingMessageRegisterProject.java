package lcsb.mapviewer.services.websockets.messages.incoming;

import javax.validation.constraints.NotNull;

public class WebSocketIncomingMessageRegisterProject extends WebSocketIncomingMessage {

  @NotNull
  private String projectId;

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(final String projectId) {
    this.projectId = projectId;
  }
}
