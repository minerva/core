package lcsb.mapviewer.services.websockets.messages.outgoing;

public class WebSocketMessageInvalidInput extends WebSocketMessage {

  private final String message;

  public WebSocketMessageInvalidInput(final String message) {
    super(WebSocketMessageType.ERROR_PROCESSING_INPUT);
    this.message = message;
  }

  public String getMessage() {
    return message;
  }
}
