package lcsb.mapviewer.services.websockets.messages.outgoing;

import lcsb.mapviewer.services.websockets.messages.incoming.WebSocketIncomingMessage;

public class WebSocketMessageAck extends WebSocketMessage {

  private final String message;

  private final String commandId;

  public WebSocketMessageAck(final String message, final String commandId) {
    super(WebSocketMessageType.MESSAGE_PROCESSED_SUCCESSFULLY);
    this.message = message;
    this.commandId = commandId;
  }

  public WebSocketMessageAck(final String message, final WebSocketIncomingMessage incomingMessage) {
    this(message, incomingMessage.getCommandId());
  }

  public String getMessage() {
    return message;
  }

  public String getCommandId() {
    return commandId;
  }
}
