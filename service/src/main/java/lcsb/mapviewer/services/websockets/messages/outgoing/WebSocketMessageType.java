package lcsb.mapviewer.services.websockets.messages.outgoing;

public enum WebSocketMessageType {
  ENTITY_UPDATED,
  ENTITY_DELETED,
  ENTITY_CREATED,
  ENTITY_CHILD_CHANGED,
  ERROR_PROCESSING_INPUT,
  ACCESS_DENIED,
  MESSAGE_PROCESSED_SUCCESSFULLY,
}
