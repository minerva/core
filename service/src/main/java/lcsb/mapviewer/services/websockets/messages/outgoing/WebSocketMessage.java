package lcsb.mapviewer.services.websockets.messages.outgoing;

public abstract class WebSocketMessage {

  private final WebSocketMessageType type;

  protected WebSocketMessage(final WebSocketMessageType type) {
    this.type = type;
  }

  public WebSocketMessageType getType() {
    return type;
  }
}
