package lcsb.mapviewer.services.websockets.messages.incoming;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY, property = "command")
@JsonSubTypes({
    @JsonSubTypes.Type(value = WebSocketIncomingMessageRegisterProject.class, name = "register-for-project-updates"),
    @JsonSubTypes.Type(value = WebSocketIncomingMessageLogin.class, name = "login"),
})
public abstract class WebSocketIncomingMessage {

  private String command;

  private String commandId;

  public String getCommand() {
    return command;
  }

  public String getCommandId() {
    return commandId;
  }
}
