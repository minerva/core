package lcsb.mapviewer.services.websockets.messages.outgoing;

import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;

public class WebSocketMessageEntityCreated extends WebSocketMessage {

  private final int entityId;

  private final long entityVersion;

  private final EntityType entityType;

  private final String projectId;

  private final Integer mapId;

  public WebSocketMessageEntityCreated(final MinervaEntity minervaEntity, final String projectId, final Integer mapId) {
    super(WebSocketMessageType.ENTITY_CREATED);
    this.entityId = minervaEntity.getId();
    this.entityVersion = minervaEntity.getEntityVersion();
    this.projectId = projectId;
    this.mapId = mapId;
    this.entityType = minervaEntity.getEntityType();
  }

  public WebSocketMessageEntityCreated(final MinervaEntity minervaEntity, final String projectId) {
    this(minervaEntity, projectId, null);
  }

  public int getEntityId() {
    return entityId;
  }

  public EntityType getEntityType() {
    return entityType;
  }

  public String getProjectId() {
    return projectId;
  }

  public int getMapId() {
    return mapId;
  }

  public long getEntityVersion() {
    return entityVersion;
  }
}
