package lcsb.mapviewer.services.websockets.messages.incoming;

import javax.validation.constraints.NotNull;

public class WebSocketIncomingMessageLogin extends WebSocketIncomingMessage {

  @NotNull
  private String token;

  public String getToken() {
    return token;
  }

  public void setToken(final String token) {
    this.token = token;
  }
}
