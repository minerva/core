package lcsb.mapviewer.services.websockets.messages.outgoing;

public class WebSocketMessageAccessDeniedInput extends WebSocketMessage {

  private final String message;

  public WebSocketMessageAccessDeniedInput(final String message) {
    super(WebSocketMessageType.ACCESS_DENIED);
    this.message = message;
  }

  public String getMessage() {
    return message;
  }
}
