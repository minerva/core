package lcsb.mapviewer.services.websockets.messages.outgoing;

import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.map.layout.graphics.Layer;

public class WebSocketMessageLayerChildUpdated extends WebSocketMessageEntityUpdated {

  private final Integer layerId;

  public WebSocketMessageLayerChildUpdated(final MinervaEntity minervaEntity, final String projectId, final Layer layer) {
    super(minervaEntity, projectId, layer.getModel().getId());
    this.layerId = layer.getId();
  }

  public Integer getLayerId() {
    return layerId;
  }
}
