package lcsb.mapviewer.services.websockets;

import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessage;
import org.springframework.web.socket.WebSocketHandler;

public interface IWebSocketMessenger extends WebSocketHandler {
  void broadcast(String projectId, WebSocketMessage message);
}
