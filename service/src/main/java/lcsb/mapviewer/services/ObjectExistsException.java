package lcsb.mapviewer.services;

/**
 * Thrown when object cannot be found via API.
 * 
 * @author Piotr Gawron
 *
 */
public class ObjectExistsException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   * 
   * @param message
   *          error message
   */
  public ObjectExistsException(final String message) {
    super(message);
  }

  /**
   * Constructor with error message and parent exception.
   * 
   * @param message
   *          error message
   * @param reason
   *          parent exception that caused this one
   */
  public ObjectExistsException(final String message, final Exception reason) {
    super(message, reason);
  }

  public ObjectExistsException(final Exception reason) {
    super(reason);
  }

}
