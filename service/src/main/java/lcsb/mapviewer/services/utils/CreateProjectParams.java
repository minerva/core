package lcsb.mapviewer.services.utils;

import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.zip.ZipEntryFile;
import lcsb.mapviewer.model.License;
import lcsb.mapviewer.model.user.User;

import java.util.ArrayList;
import java.util.List;


/**
 * Set of parameters used during creation of new project.
 *
 * @author Piotr Gawron
 */
public class CreateProjectParams {

  /**
   * User defined project identifier.
   */
  private String projectId;

  /**
   * Name of the project.
   */
  private String projectName;

  /**
   * Disease associated to the project.
   */
  private String disease;

  /**
   * Organism associated to the project.
   */
  private String organism;

  private String customLicenseName;

  private String customLicenseUrl;

  private int projectFileId;

  private Class<? extends Converter> parser;

  /**
   * Is the project a complex multi-file project.
   */
  private boolean complex = false;

  /**
   * List of zip entries in the complex model definition.
   */
  private final List<ZipEntryFile> zipEntries = new ArrayList<>();

  /**
   * Should the elements be annotated by the external resources.
   */
  private boolean updateAnnotations = false;

  /**
   * Do we want to generate images for all available layers.
   */
  private boolean images = false;

  /**
   * Should the map be auto-resized after processing (to trim unnecessary margins).
   */
  private boolean autoResize = true;

  /**
   * Should the map be displayed in SBGN format.
   */
  private boolean sbgnFormat = false;

  /**
   * Is the {@link lcsb.mapviewer.services.utils.data.BuildInBackgrounds#NORMAL}
   * default {@link lcsb.mapviewer.model.map.layout.ProjectBackground} when
   * generating new project.
   */
  private boolean networkBackgroundAsDefault = false;

  /**
   * Email address that should be notified after everything is done.
   */
  private String notifyEmail = null;

  /**
   * Description of the project.
   */
  private String description = "";

  /**
   * Version of the map.
   */
  private String version = "0";

  /**
   * Directory with the static images that will be stored on server. This
   * directory is relative and it's a simple unique name within folder with
   * images.
   */
  private String projectDir;

  private String userLogin;

  private License license;

  /**
   * @param projectId the projectId to set
   * @return object with all parameters
   * @see #projectId
   */
  public CreateProjectParams projectId(final String projectId) {
    this.projectId = projectId;
    return this;
  }

  public CreateProjectParams parser(final Converter parser) {
    this.parser = parser.getClass();
    return this;
  }

  public CreateProjectParams parser(final Class<? extends Converter> clazz) {
    this.parser = clazz;
    return this;
  }

  /**
   * @param notifyEmail the notifyEmail to set
   * @return object with all parameters
   * @see #notifyEmail
   */
  public CreateProjectParams notifyEmail(final String notifyEmail) {
    this.notifyEmail = notifyEmail;
    return this;
  }

  /**
   * @param autoResize the autoResize to set
   * @return object with all parameters
   * @see #autoResize
   */
  public CreateProjectParams autoResize(final boolean autoResize) {
    this.autoResize = autoResize;
    return this;
  }

  public CreateProjectParams autoResize(final String value) {
    return this.autoResize("true".equalsIgnoreCase(value));
  }

  /**
   * @param updateAnnotations the updateAnnotations to set
   * @return object with all parameters
   * @see #updateAnnotations
   */
  public CreateProjectParams annotations(final boolean updateAnnotations) {
    this.updateAnnotations = updateAnnotations;
    return this;
  }

  public CreateProjectParams annotations(final String value) {
    return this.annotations("true".equalsIgnoreCase(value));
  }

  /**
   * @param images the images to set
   * @return object with all parameters
   * @see #images
   */
  public CreateProjectParams images(final boolean images) {
    this.images = images;
    return this;
  }

  /**
   * @param description the description to set
   * @return object with all parameters
   * @see #description
   */
  public CreateProjectParams description(final String description) {
    this.description = description;
    return this;
  }

  /**
   * @param version the version to set
   * @return object with all parameters
   * @see #version
   */
  public CreateProjectParams version(final String version) {
    this.version = version;
    return this;
  }

  /**
   * @return the version
   * @see #version
   */
  public String getVersion() {
    return version;
  }

  /**
   * @return the description
   * @see #description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @return the notifyEmail
   * @see #notifyEmail
   */
  public String getNotifyEmail() {
    return notifyEmail;
  }

  /**
   * @return the autoResize
   * @see #autoResize
   */
  public boolean isAutoResize() {
    return autoResize;
  }

  /**
   * @return the images
   * @see #images
   */
  public boolean isImages() {
    return images;
  }

  /**
   * @return the updateAnnotations
   * @see #updateAnnotations
   */
  public boolean isUpdateAnnotations() {
    return updateAnnotations;
  }

  /**
   * @return the projectFile
   * @see #projectFile
   */
  public int getProjectFileId() {
    return projectFileId;
  }

  /**
   * @return the projectId
   * @see #projectId
   */
  public String getProjectId() {
    return projectId;
  }

  /**
   * @return the submodels
   * @see #zipEntries
   */
  public List<ZipEntryFile> getZipEntries() {
    return zipEntries;
  }

  /**
   * @param entry the submodel to add
   * @return object with all parameters
   * @see #zipEntries
   */
  public CreateProjectParams addZipEntry(final ZipEntryFile entry) {
    if (entry != null) {
      this.zipEntries.add(entry);
    }
    return this;
  }

  /**
   * @return the complex
   * @see #complex
   */
  public boolean isComplex() {
    return complex;
  }

  /**
   * @return the sbgnFormat
   * @see #sbgnFormat
   */
  public boolean isSbgnFormat() {
    return sbgnFormat;
  }

  /**
   * @param sbgnFormat the sbgnFormat to set
   * @see #sbgnFormat
   */
  public CreateProjectParams sbgnFormat(final boolean sbgnFormat) {
    this.sbgnFormat = sbgnFormat;
    return this;
  }

  public CreateProjectParams sbgnFormat(final String value) {
    return this.sbgnFormat("true".equalsIgnoreCase(value));
  }

  /**
   * @param complex the complex to set
   * @return object with all parameters
   * @see #complex
   */
  public CreateProjectParams complex(final boolean complex) {
    this.complex = complex;
    return this;
  }

  /**
   * Sets {@link #projectDir}.
   *
   * @param directory new {@link #projectDir} value
   * @return instance of this class with new value set
   */
  public CreateProjectParams projectDir(final String directory) {
    this.projectDir = directory;
    return this;
  }

  /**
   * @return the projectDir
   * @see #projectDir
   */
  public String getProjectDir() {
    return projectDir;
  }

  /**
   * @param projectName new {@link #projectName}
   * @return instance of this class with new value set
   */
  public CreateProjectParams projectName(final String projectName) {
    this.projectName = projectName;
    return this;
  }

  /**
   * @return the projectName
   * @see #projectName
   */
  public String getProjectName() {
    return projectName;
  }

  public CreateProjectParams networkBackgroundAsDefault(final boolean value) {
    this.networkBackgroundAsDefault = value;
    return this;

  }

  public boolean isNetworkBackgroundAsDefault() {
    return networkBackgroundAsDefault;
  }

  /**
   * @return disease MESH code.
   */
  public String getDisease() {
    return disease;
  }

  /**
   * @param disease the code of the disease.
   * @return updated params object.
   */
  public CreateProjectParams projectDisease(final String disease) {
    this.disease = disease;
    return this;
  }

  /**
   * @return the organism
   * @see #organism
   */
  public String getOrganism() {
    return organism;
  }

  public String getCustomLicenseName() {
    return customLicenseName;
  }

  public String getCustomLicenseUrl() {
    return customLicenseUrl;
  }

  /**
   * @param organism the organism to set
   * @return updated params object.
   */
  public CreateProjectParams projectOrganism(final String organism) {
    this.organism = organism;
    return this;
  }

  public CreateProjectParams customLicenseName(final String customLicenseName) {
    this.customLicenseName = customLicenseName;
    return this;
  }

  public CreateProjectParams customLicenseUrl(final String customLicenseUrl) {
    this.customLicenseUrl = customLicenseUrl;
    return this;
  }

  public String getUserLogin() {
    return userLogin;
  }

  public CreateProjectParams setUser(final User user) {
    this.userLogin = user.getLogin();
    return this;
  }

  public Class<? extends Converter> getParser() {
    return this.parser;
  }

  public CreateProjectParams setUserLogin(final String userLogin) {
    this.userLogin = userLogin;
    return this;
  }

  public CreateProjectParams projectFile(final int fileId) {
    this.projectFileId = fileId;
    return this;
  }

  public CreateProjectParams license(final License license) {
    this.license = license;
    return this;
  }

  public License getLicense() {
    return license;
  }

}
