package lcsb.mapviewer.services.utils;

import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Class that allows to send emails. It requires access to email account from
 * which emails will be sent.
 *
 * @author Piotr Gawron
 */
@Transactional
@Service
public class EmailSender {

  /**
   * Max size of the content that is allowed as plain text. For emails with longer
   * content the content will be compressed and added as an attachment file.
   */
  private static final int MAX_EMAIL_CONTENT_SIZE = 32 * 1024;

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger();

  private IConfigurationService configurationService;

  private String getSender() {
    return configurationService.getConfigurationValue(ConfigurationElementType.EMAIL_ADDRESS);
  }

  private String getLogin() {
    return configurationService.getConfigurationValue(ConfigurationElementType.EMAIL_LOGIN);
  }

  private String getPassword() {
    return configurationService.getConfigurationValue(ConfigurationElementType.EMAIL_PASSWORD);
  }

  private String getSmtpHost() {
    return configurationService.getConfigurationValue(ConfigurationElementType.EMAIL_SMTP_SERVER);
  }

  private String getSmtpPort() {
    return configurationService.getConfigurationValue(ConfigurationElementType.EMAIL_SMTP_PORT);
  }

  /**
   * Creates email sender class from data taken from
   * {@link IConfigurationService}.
   *
   * @param configurationService configuration service that contains information required to access
   *                             email account
   */
  @Autowired
  public EmailSender(final IConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

  /**
   * Sends email.
   *
   * @param subject  subject of the email
   * @param message  content of the message
   * @param receiver user that should receive email
   */
  public void sendEmail(final String subject, final String message, final User receiver) throws EmailException {
    if (receiver.getEmail() == null || receiver.getEmail().equals("")) {
      logger.warn(
          "Cannot send email to user: " + receiver.getName() + " " + receiver.getSurname() + ". Email set to null");
      return;
    }
    List<String> recipients = new ArrayList<String>();
    recipients.add(receiver.getEmail());
    sendEmail(recipients, new ArrayList<String>(), subject, message);
  }

  /**
   * Sends email.
   *
   * @param recipients   list of email recipients
   * @param ccRecipients list of cc email recipients
   * @param subject      subject of the email
   * @param message      message content
   */
  public void sendEmail(final List<String> recipients, final List<String> ccRecipients, final String subject, final String message)
      throws EmailException {
    try {
      if (!canSendEmails()) {
        logger.warn("Cannot send email. No smpt server defined");
        return;
      }
      // set data of the mail account

      Session session;
      if (isSSL()) {
        Properties props = System.getProperties();
        props.setProperty("mail.smtp.host", getSmtpHost());
        props.setProperty("mail.smtp.port", getSmtpPort());

        props.put("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.smtp.ssl.enable", "true");
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");

        session = Session.getInstance(props, null);
      } else {
        Properties props = System.getProperties();
        props.setProperty("mail.smtp.host", getSmtpHost());
        props.setProperty("mail.smtp.port", getSmtpPort());
        // props.setProperty("mail.smtp.user", login);
        // props.setProperty("mail.smtp.password", password);
        // props.setProperty("mail.smtp.auth", "true");

        props.put("mail.smtps.quitwait", "false");
        session = Session.getInstance(props, null);
      }

      session.setDebug(true);

      final MimeMessage msg = new MimeMessage(session);

      // set from address

      msg.setFrom(new InternetAddress(getSender()));

      // add recipients
      InternetAddress[] rec = new InternetAddress[recipients.size()];
      for (int i = 0; i < recipients.size(); i++) {
        rec[i] = new InternetAddress(recipients.get(i));
      }

      msg.setRecipients(Message.RecipientType.TO, rec);

      // add cc recipients
      InternetAddress[] cc = new InternetAddress[ccRecipients.size()];
      for (int i = 0; i < ccRecipients.size(); i++) {
        cc[i] = new InternetAddress(ccRecipients.get(i));
      }

      msg.setRecipients(Message.RecipientType.CC, cc);

      // set subject
      msg.setSubject(subject);

      // set time stamp
      msg.setSentDate(new Date());

      File attachment = createAttachmentZipForBigMessage(message);

      if (attachment != null) {
        MimeBodyPart textBodyPart = new MimeBodyPart();
        textBodyPart.setText("Content zipped, because it's size exceeded max size: " + MAX_EMAIL_CONTENT_SIZE, "UTF-8");

        MimeBodyPart attachmentBodyPart = new MimeBodyPart();
        attachmentBodyPart = new MimeBodyPart();
        DataSource source = new FileDataSource(attachment);
        attachmentBodyPart.setDataHandler(new DataHandler(source));
        attachmentBodyPart.setFileName("content.zip");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(textBodyPart);
        multipart.addBodyPart(attachmentBodyPart);

        msg.setContent(multipart);
      } else {
        // and content
        msg.setContent(message, "text/html; charset=utf-8");
      }

      Transport transport;
      if (isSSL()) {
        transport = session.getTransport("smtps");
      } else {
        transport = session.getTransport("smtp");
      }
      transport.connect(getSmtpHost(), getLogin(), getPassword());
      transport.sendMessage(msg, msg.getAllRecipients());
      transport.close();

    } catch (MessagingException e) {
      throw new EmailException(e);
    }
  }

  /**
   * Sends email.
   *
   * @param subject      subject of the email
   * @param message      content of the message
   * @param emailAddress email address that should receive email
   */
  public void sendEmail(final String subject, final String message, final String emailAddress) throws EmailException {
    try {
      if (emailAddress == null || emailAddress.isEmpty()) {
        logger.warn("Cannot send email to user: " + emailAddress + ". Email set to null");
        return;
      }
      List<String> recipients = new ArrayList<String>();
      recipients.add(emailAddress);
      sendEmail(recipients, new ArrayList<>(), subject, message);
    } catch (EmailException e) {
      throw e;
    } catch (Exception e) {
      throw new EmailException(e);
    }
  }

  private File createAttachmentZipForBigMessage(final String message) {
    File attachment = null;
    if (message.length() > MAX_EMAIL_CONTENT_SIZE) {
      try {
        attachment = File.createTempFile("attachment", ".zip");

        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(attachment));
        ZipEntry e = new ZipEntry("content.txt");
        out.putNextEntry(e);

        byte[] data = message.getBytes();
        out.write(data, 0, data.length);
        out.closeEntry();

        out.close();
      } catch (final IOException e1) {
        logger.error("Problem with creating attachment. Fall back into putting everything into content");
        attachment = null;
      }
    }
    return attachment;
  }

  public boolean canSendEmails() {
    if (getSmtpHost().equals(ConfigurationElementType.EMAIL_SMTP_SERVER.getDefaultValue())) {
      return false;
    }
    if (getSmtpHost().trim().isEmpty()) {
      return false;
    }
    return true;
  }

  private boolean isSSL() {
    return "true".equalsIgnoreCase(configurationService.getConfigurationValue(ConfigurationElementType.EMAIL_SSL_SERVER));
  }
}
