package lcsb.mapviewer.services.utils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.map.BioEntity;

public class BioEntitySearchResult implements MinervaEntity {
  private static final long serialVersionUID = 1L;
  private final BioEntity bioEntity;
  private final boolean isPerfect;

  public BioEntitySearchResult(final BioEntity bioentity, final boolean isPerfect) {
    this.bioEntity = bioentity;
    this.isPerfect = isPerfect;
  }

  public BioEntity getBioEntity() {
    return bioEntity;
  }

  public boolean isPerfect() {
    return isPerfect;
  }

  @Override
  @JsonIgnore
  public int getId() {
    throw new NotImplementedException();
  }

  @Override
  @JsonIgnore
  public long getEntityVersion() {
    throw new NotImplementedException();
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
