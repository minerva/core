package lcsb.mapviewer.services.utils;

/**
 * Exception used when there are problems with privileges.
 * 
 * @author Piotr Gawron
 * 
 */
public class InvalidPrivilegeException extends RuntimeException {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Public constructor with message that should be reported.
   * 
   * @param string
   *          message
   */
  public InvalidPrivilegeException(final String string) {
    super(string);
  }
}
