package lcsb.mapviewer.services.utils;

import lcsb.mapviewer.annotation.services.genome.ReferenceGenomeConnectorException;

/**
 * Exception used when there are problems with privileges.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReferenceGenomeExistsException extends ReferenceGenomeConnectorException {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Public constructor with message that should be reported.
   * 
   * @param string
   *          message
   */
  public ReferenceGenomeExistsException(final String string) {
    super(string);
  }
}
