package lcsb.mapviewer.services.utils.data;

import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This enum defines which columns are available in export function.
 * 
 * @author Piotr Gawron
 * 
 */
public enum ExportColumn {

  /**
   * Name of the species.
   */
  NAME("Name", Species.class),

  /**
   * Name of compartment.
   */
  COMPARTMENT_NAME("Compartment", Species.class),

  /**
   * Name of compartment.
   */
  POSITION_TO_COMPARTMENT("Position to compartment", Species.class),

  /**
   * Name of pathway.
   */
  COMPONENT_NAME("Pathway", Species.class),

  /**
   * Type of the species.
   */
  TYPE("Type", Species.class),

  /**
   * Identifier.
   */
  ID("Id", Object.class),

  /**
   * Complex in which element is localized.
   */
  COMPLEX("Parent complex", Species.class),

  /**
   * Complex in which element is localized.
   */
  SUBMODEL("Submodel", Species.class),

  /**
   * Reaction identifier.
   */
  REACTION_ID("Reaction id", Reaction.class),

  /**
   * Reaction type.
   */
  REACTION_TYPE("Reaction type", Reaction.class),

  /**
   * Information if reaction is reversible.
   */
  REACTION_REVERSIBLE("Reversible", Reaction.class),

  /**
   * Every reaction is split into two elements interactions.
   */
  REACTION_TWO_ELEMENT("Split reaction to two element interaction", Reaction.class);

  /**
   * Human readable title.
   */
  private String title;
  /**
   * Class for which this column should be accessible.
   */
  private Class<?> clazz;

  /**
   * Default constructor that creates enum entry.
   *
   * @param title
   *          {@link #title}
   * @param clazz
   *          {@link #clazz}
   */
  ExportColumn(final String title, final Class<?> clazz) {
    this.title = title;
    this.clazz = clazz;
  }

  /**
   * 
   * @return {@link #title}
   */
  public String getTitle() {
    return title;
  }

  /**
   * 
   * 
   * @return {@link #clazz}
   */
  public Class<?> getClazz() {
    return clazz;
  }

}
