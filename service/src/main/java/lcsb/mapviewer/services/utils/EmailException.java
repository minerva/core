package lcsb.mapviewer.services.utils;

public class EmailException extends Exception {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  public EmailException(final Throwable e) {
    super(e);
  }
}
