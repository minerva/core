package lcsb.mapviewer.services.utils.data;

import lcsb.mapviewer.model.map.layout.ProjectBackground;

/**
 * This enum defines set of build-in {@link ProjectBackground}.
 * 
 * @author Piotr Gawron
 * 
 */
public enum BuildInBackgrounds {
  /**
   * Normal straight forward visualization.
   */
  NORMAL("Network", "_normal", false),
  /**
   * Standard visualization with hierarchical view.
   */
  NESTED("Pathways and compartments", "_nested", true),
  /**
   * Clean visualization (with colors reset to black and white).
   */
  CLEAN("Empty", "_empty", false);

  private String name;

  /**
   * Suffix used for the directory name during image generation.
   */
  private String directorySuffix;

  /**
   * Should visualization be hierarchical or simple.
   */
  private boolean nested;

  /**
   * Default constructor with all information needed by project background.
   * 
   * @param name
   *          name of the background
   * @param directorySuffix
   *          Suffix used for the directory name during image generation
   * @param nested
   *          Should visualization be hierarchical or simple
   */
  BuildInBackgrounds(final String name, final String directorySuffix, final boolean nested) {
    this.name = name;
    this.directorySuffix = directorySuffix;
    this.nested = nested;
  }

  /**
   * @return the title
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @return the directorySuffix
   * @see #directorySuffix
   */
  public String getDirectorySuffix() {
    return directorySuffix;
  }

  /**
   * @return the nested
   * @see #nested
   */
  public boolean isNested() {
    return nested;
  }

}
