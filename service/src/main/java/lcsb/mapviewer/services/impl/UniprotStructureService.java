package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.Structure;
import lcsb.mapviewer.persist.dao.map.species.UniprotStructureDao;
import lcsb.mapviewer.persist.dao.map.species.UniprotStructureProperty;
import lcsb.mapviewer.services.interfaces.IUniprotStructureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Transactional
@Service
public class UniprotStructureService implements IUniprotStructureService {

  private final UniprotStructureDao uniprotStructureDao;

  @Autowired
  public UniprotStructureService(final UniprotStructureDao uniprotRecordDao) {
    this.uniprotStructureDao = uniprotRecordDao;
  }

  @Override
  public Page<Structure> getAll(final Map<UniprotStructureProperty, Object> filter, final Pageable pageable) {
    return uniprotStructureDao.getAll(filter, pageable);
  }

  @Override
  public Structure getById(final int id) {
    return uniprotStructureDao.getById(id);
  }

  @Override
  public void update(final Structure object) {
    uniprotStructureDao.update(object);
  }

  @Override
  public void remove(final Structure object) {
    uniprotStructureDao.delete(object);
  }

  @Override
  public long getCount(final Map<UniprotStructureProperty, Object> filterOptions) {
    throw new NotImplementedException();
  }

  @Override
  public void add(final Structure mr) {
    uniprotStructureDao.add(mr);
  }
}
