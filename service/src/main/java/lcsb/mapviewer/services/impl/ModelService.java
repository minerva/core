package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.dao.map.ModelDao;
import lcsb.mapviewer.persist.dao.map.ModelProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IModelService;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Implementation of the service that manages models.
 *
 * @author Piotr Gawron
 */
@Transactional
@Service
public class ModelService implements IModelService {

  private final Logger logger = LogManager.getLogger();

  /**
   * Data access object for models.
   */
  private final ModelDao modelDao;

  private final DbUtils dbUtils;

  @Autowired
  public ModelService(final ModelDao modelDao,
                      final DbUtils dbUtils) {
    this.modelDao = modelDao;
    this.dbUtils = dbUtils;
  }

  @Override
  public void update(final ModelData model) {
    modelDao.update(model);

  }

  @Override
  public List<ModelData> getModelsByMapId(final String projectId, final String mapId) throws ObjectNotFoundException {
    final Map<ModelProperty, Object> filter = new HashMap<>();
    filter.put(ModelProperty.PROJECT_ID, projectId);
    if (!Objects.equals("*", mapId)) {
      if (!StringUtils.isNumeric(mapId)) {
        throw new ObjectNotFoundException("Map with given id does not exist: " + mapId);
      }
      filter.put(ModelProperty.MAP_ID, Integer.valueOf(mapId));
    }
    return getAll(filter, Pageable.unpaged()).getContent();
  }

  @Override
  public ModelData getModelByMapId(final String projectId, final int mapId) throws ObjectNotFoundException {
    final Map<ModelProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(ModelProperty.MAP_ID, mapId);
    filterOptions.put(ModelProperty.PROJECT_ID, projectId);
    final Page<ModelData> page = modelDao.getAll(filterOptions, Pageable.unpaged());

    if (page.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Map with given id does not exist: " + mapId);
    }
    return page.getContent().get(0);
  }

  @Override
  public Model getAndFetchModelByMapId(final String projectId, final int mapId) throws ObjectNotFoundException {
    final ModelData map = getModelByMapId(projectId, mapId);

    Hibernate.initialize(map.getElements());
    Hibernate.initialize(map.getReactions());
    Hibernate.initialize(map.getProject());
    Hibernate.initialize(map.getFunctions());
    Hibernate.initialize(map.getUnits());
    Hibernate.initialize(map.getLayers());

    map.getSubmodels().clear();
    map.getParentModels().clear();

    for (final Element element : map.getElements()) {
      if (element.getGlyph() != null) {
        Hibernate.initialize(element.getGlyph().getFile());
      }
      if (element instanceof Complex) {
        ((Complex) element).setElemets(new ArrayList<>());
      }
      if (element instanceof Compartment) {
        ((Compartment) element).setElements(new HashSet<>());
      }
      if (element.getGlyph() != null) {
        Hibernate.initialize(element.getGlyph().getFile());
      }
      element.setSubmodel(null);
    }
    for (final Element element : map.getElements()) {
      if (element instanceof Species) {
        if (((Species) element).getComplex() != null) {
          ((Species) element).getComplex().addSpecies((Species) element);
        }
      }
      if (element.getCompartment() != null) {
        element.getCompartment().addElement(element);
      }
    }
    dbUtils.getSessionFactory().getCurrentSession().clear();

    return new ModelFullIndexed(map);
  }

  @Override
  public int getPublicationCount(final String projectId) throws ObjectNotFoundException {
    final List<ModelData> models = getModelsByMapId(projectId, "*");
    if (models.isEmpty()) {
      throw new ObjectNotFoundException();
    }
    return modelDao.getAnnotationCount(models, MiriamType.PUBMED);
  }

  @Override
  public void add(final ModelData modelData) {
    modelDao.add(modelData);
  }

  @Override
  public void remove(final ModelData model) {
    modelDao.delete(model);
  }

  @Override
  public Page<ModelData> getAll(final Map<ModelProperty, Object> modelFilter, final Pageable pageable) {
    return modelDao.getAll(modelFilter, pageable);
  }

  @Override
  public ModelData getById(final int id) {
    return modelDao.getById(id);
  }

  @Override
  public long getCount(final Map<ModelProperty, Object> filterOptions) {
    return modelDao.getCount(filterOptions);
  }
}
