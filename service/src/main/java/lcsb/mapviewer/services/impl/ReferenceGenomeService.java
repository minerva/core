package lcsb.mapviewer.services.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.services.genome.FileNotAvailableException;
import lcsb.mapviewer.annotation.services.genome.ReferenceGenomeConnector;
import lcsb.mapviewer.annotation.services.genome.ReferenceGenomeConnectorException;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.layout.ReferenceGenome;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeGeneMapping;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.persist.dao.map.layout.ReferenceGenomeDao;
import lcsb.mapviewer.services.interfaces.IReferenceGenomeService;
import lcsb.mapviewer.services.utils.ReferenceGenomeExistsException;

/**
 * Service managing reference genomes.
 * 
 * @author Piotr Gawron
 *
 */
@Transactional
@Service
public class ReferenceGenomeService implements IReferenceGenomeService {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger();

  /**
   * Class responsible for connection to {@link ReferenceGenomeType#UCSC}
   * database.
   */
  private ReferenceGenomeConnector ucscReferenceGenomeConnector;

  /**
   * Data access object for {@link ReferenceGenome} objects.
   */
  private ReferenceGenomeDao referenceGenomeDao;

  @Autowired
  public ReferenceGenomeService(final ReferenceGenomeConnector ucscReferenceGenomeConnector,
      final ReferenceGenomeDao referenceGenomeDao) {
    this.ucscReferenceGenomeConnector = ucscReferenceGenomeConnector;
    this.referenceGenomeDao = referenceGenomeDao;
  }

  @Override
  public void addReferenceGenome(final ReferenceGenomeType type, final MiriamData organism, final String version, final String customUrl)
      throws IOException, URISyntaxException, ReferenceGenomeConnectorException {
    for (final ReferenceGenome genome : getDownloadedGenomes()) {
      if (genome.getType().equals(type) && genome.getOrganism().equals(organism)
          && genome.getVersion().equals(version)) {
        throw new ReferenceGenomeExistsException("Selected reference genome already downloaded");
      }
    }
    getReferenceGenomeConnector(type).downloadGenomeVersion(organism, version, new IProgressUpdater() {
      @Override
      public void setProgress(final double progress) {
      }
    }, true, customUrl);
  }

  @Override
  public List<MiriamData> getOrganismsByReferenceGenomeType(final ReferenceGenomeType type)
      throws ReferenceGenomeConnectorException {
    return getReferenceGenomeConnector(type).getAvailableOrganisms();
  }

  @Override
  public List<String> getAvailableGenomeVersions(final ReferenceGenomeType type, final MiriamData organism)
      throws ReferenceGenomeConnectorException {
    return getReferenceGenomeConnector(type).getAvailableGenomeVersion(organism);
  }

  @Override
  public String getUrlForGenomeVersion(final ReferenceGenomeType type, final MiriamData organism, final String version) throws IOException {
    try {
      return getReferenceGenomeConnector(type).getGenomeVersionFile(organism, version);
    } catch (final FileNotAvailableException e) {
      return null;
    }
  }

  @Override
  public List<ReferenceGenome> getDownloadedGenomes() {
    return referenceGenomeDao.getAll();
  }

  @Override
  public void removeGenome(final ReferenceGenome genome) throws IOException {
    getReferenceGenomeConnector(genome.getType()).removeGenomeVersion(genome.getOrganism(), genome.getVersion());
  }

  @Override
  public void addReferenceGenomeGeneMapping(final ReferenceGenome referenceGenome, final String name, final String url)
      throws IOException, URISyntaxException, ReferenceGenomeConnectorException {
    getReferenceGenomeConnector(referenceGenome.getType()).downloadGeneMappingGenomeVersion(referenceGenome, name,
        new IProgressUpdater() {
          @Override
          public void setProgress(final double progress) {
          }
        }, true, url);

  }

  @Override
  public void removeReferenceGenomeGeneMapping(final ReferenceGenomeGeneMapping genome) throws IOException {
    getReferenceGenomeConnector(genome.getReferenceGenome().getType()).removeGeneMapping(genome);
  }

  @Override
  public ReferenceGenome getReferenceGenomeViewByParams(
      final MiriamData miriamData, final ReferenceGenomeType genomeType, final String version) {
    List<ReferenceGenome> list = referenceGenomeDao.getByType(genomeType);
    for (final ReferenceGenome referenceGenome : list) {
      if (referenceGenome.getOrganism().equals(miriamData) && referenceGenome.getVersion().equals(version)) {
        return referenceGenome;
      }
    }
    return null;
  }

  @Override
  public ReferenceGenome getReferenceGenomeById(final int id) {
    return referenceGenomeDao.getById(id);
  }

  /**
   * Return {@link ReferenceGenomeConnector} implementation for given reference
   * genome type.
   *
   * @param type
   *          type of reference genome
   * @return {@link ReferenceGenomeConnector} implementation for given reference
   *         genome type
   */
  private ReferenceGenomeConnector getReferenceGenomeConnector(final ReferenceGenomeType type) {
    if (type == ReferenceGenomeType.UCSC) {
      return ucscReferenceGenomeConnector;
    } else {
      throw new InvalidArgumentException("Unknown reference genome type: " + type);
    }
  }

}
