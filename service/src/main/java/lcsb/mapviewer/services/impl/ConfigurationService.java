package lcsb.mapviewer.services.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.dapi.DapiConnector;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.ConfigurationElementEditType;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.ConfigurationOption;
import lcsb.mapviewer.persist.dao.ConfigurationDao;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.utils.EmailSender;

/**
 * Service implementation used for accessing and modifying configuration
 * parameters.
 * 
 * @author Piotr Gawron
 * 
 */
@Transactional
@Service
public class ConfigurationService implements IConfigurationService {

  /**
   * Number of bytes in a megabyte.
   */
  private static final int MEGABYTE_SIZE = 1024 * 1024;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger();

  /**
   * Data access object for configuration parameters.
   */
  private ConfigurationDao configurationDao;

  private DapiConnector dapiConnector;

  @Autowired
  private EmailSender emailSender;

  @Autowired
  public ConfigurationService(final ConfigurationDao configurationDao, final DapiConnector dapiConnector) {
    this.configurationDao = configurationDao;
    this.dapiConnector = dapiConnector;
  }

  @Override
  public String getConfigurationValue(final ConfigurationElementType type) {
    ConfigurationOption result = configurationDao.getByType(type);
    if (result == null) {
      result = new ConfigurationOption();
      result.setType(type);
      result.setValue(type.getDefaultValue());
      configurationDao.add(result);
    }
    return result.getValue();
  }

  @Override
  @Transactional(noRollbackFor = InvalidArgumentException.class)
  public void setConfigurationValue(final ConfigurationElementType type, final String value) {
    if (value == null) {
      throw new InvalidArgumentException("null is not a proper value");
    }
    if (type.getEditType() == ConfigurationElementEditType.INTEGER) {
      if (!value.matches("-?\\d+")) {
        throw new InvalidArgumentException("Expected integer value but got: " + value);
      }
    }
    if (type == ConfigurationElementType.MINERVA_ROOT && !value.isEmpty()) {
      try {
        String content = new WebPageDownloader().getFromNetwork(value + "/api/configuration/");
        Map<String, Object> data = new ObjectMapper().readValue(content, new TypeReference<Map<String, Object>>() {
        });
        if (!(data.get("version") instanceof String)) {
          throw new InvalidArgumentException("Root url is invalid: " + value);
        }
      } catch (IOException e) {
        throw new InvalidArgumentException("Root url is invalid: " + value, e);
      }
    } else if (type == ConfigurationElementType.ALLOW_AUTO_REGISTER && value.equalsIgnoreCase("true")) {
      if (!emailSender.canSendEmails()) {
        throw new InvalidArgumentException("Minerva is not configured to send emails");
      }
      String minervaRootUrl = getConfigurationValue(ConfigurationElementType.MINERVA_ROOT);
      if (minervaRootUrl.isEmpty()) {
        throw new InvalidArgumentException("Minerva is not configured properly . Root url is invalid: " + minervaRootUrl);
      }
    }
    ConfigurationOption configuration = configurationDao.getByType(type);
    if (configuration == null) {
      configuration = new ConfigurationOption();
      configuration.setType(type);
    }
    if (type.getEditType().equals(ConfigurationElementEditType.INTEGER)) {
      if (!StringUtils.isNumeric(value)) {
        throw new InvalidArgumentException(type + " must be an integer");
      }
    }
    configuration.setValue(value);
    configurationDao.add(configuration);

    if (type.equals(ConfigurationElementType.X_FRAME_DOMAIN)) {
      Configuration.getxFrameDomain().clear();
      for (final String domain : getConfigurationValue(ConfigurationElementType.X_FRAME_DOMAIN).split(";")) {
        Configuration.getxFrameDomain().add(domain);
      }
    } else if (type.equals(ConfigurationElementType.CORS_DOMAIN)) {
      Configuration.setDisableCors(value.equalsIgnoreCase("true"));
    } else if (type.equals(ConfigurationElementType.SESSION_LENGTH)) {
      Configuration.setSessionLength(Integer.valueOf(value));
    } else if (type.equals(ConfigurationElementType.DAPI_LOGIN)) {
      dapiConnector.resetConnection();
    } else if (type.equals(ConfigurationElementType.DAPI_PASSWORD)) {
      dapiConnector.resetConnection();
    }
  }

  @Override
  public void deleteConfigurationValue(final ConfigurationElementType type) {
    configurationDao.delete(configurationDao.getByType(type));
  }

  @Override
  public List<ConfigurationOption> getAllValues() {
    List<ConfigurationOption> result = new ArrayList<>();
    for (final ConfigurationElementType type : ConfigurationElementType.values()) {
      ConfigurationOption configuration = configurationDao.getByType(type);
      if (configuration == null) {
        getConfigurationValue(type);
        configuration = configurationDao.getByType(type);
      }
      result.add(configuration);
    }
    return result;
  }

  @Override
  public void updateConfiguration(final List<ConfigurationOption> values) {
    for (final ConfigurationOption configurationElement : values) {
      setConfigurationValue(configurationElement.getType(), configurationElement.getValue());
    }
  }

  @Override
  public String getSystemSvnVersion() {
    return lcsb.mapviewer.common.Configuration.getSystemVersion(null);
  }

  @Override
  public Long getMemoryUsage() {
    Runtime runtime = Runtime.getRuntime();

    return (runtime.totalMemory() - runtime.freeMemory()) / MEGABYTE_SIZE;
  }

  @Override
  public Long getMaxMemory() {
    Runtime runtime = Runtime.getRuntime();

    return runtime.maxMemory() / MEGABYTE_SIZE;
  }

  @Override
  public String getSystemBuild() {
    return lcsb.mapviewer.common.Configuration.getSystemBuild(null);
  }

  @Override
  public String getSystemGitVersion() {
    return lcsb.mapviewer.common.Configuration.getSystemBuildVersion(null);
  }

  @Override
  public ConfigurationOption getValue(final ConfigurationElementType type) {
    ConfigurationOption configuration = configurationDao.getByType(type);
    if (configuration == null) {
      getConfigurationValue(type);
      configuration = configurationDao.getByType(type);
    }
    return configuration;
  }

  @Override
  public ConfigurationOption getValue(final PrivilegeType type) {
    String name = "DEFAULT_" + type.name();
    if (EnumUtils.isValidEnum(ConfigurationElementType.class, name)) {
      return getValue(ConfigurationElementType.valueOf(name));
    } else {
      return null;
    }
  }

}
