package lcsb.mapviewer.services.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.persist.dao.map.kinetics.SbmlUnitDao;
import lcsb.mapviewer.persist.dao.map.kinetics.SbmlUnitProperty;
import lcsb.mapviewer.services.interfaces.ISbmlUnitService;

@Transactional
@Service
public class SbmlUnitService implements ISbmlUnitService {

  @Autowired
  private SbmlUnitDao unitDao;

  @Override
  public void update(final SbmlUnit unit) {
    unitDao.update(unit);
  }

  @Override
  public SbmlUnit getById(final int id) {
    return unitDao.getById(id);
  }

  @Override
  public void remove(final SbmlUnit unit) {
    unitDao.delete(unit);
  }

  @Override
  public void add(final SbmlUnit unit) {
    unitDao.add(unit);
  }

  @Override
  public Page<SbmlUnit> getAll(final Map<SbmlUnitProperty, Object> filter, final Pageable pageable) {
    return unitDao.getAll(filter, pageable);
  }

  @Override
  public long getCount(final Map<SbmlUnitProperty, Object> filterOptions) {
    return unitDao.getCount(filterOptions);
  }

}
