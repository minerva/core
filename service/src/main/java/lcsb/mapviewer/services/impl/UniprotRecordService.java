package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.UniprotRecord;
import lcsb.mapviewer.persist.dao.map.species.UniprotRecordDao;
import lcsb.mapviewer.persist.dao.map.species.UniprotRecordProperty;
import lcsb.mapviewer.services.interfaces.IUniprotRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Transactional
@Service
public class UniprotRecordService implements IUniprotRecordService {

  private final UniprotRecordDao uniprotRecordDao;

  @Autowired
  public UniprotRecordService(final UniprotRecordDao uniprotRecordDao) {
    this.uniprotRecordDao = uniprotRecordDao;
  }

  @Override
  public Page<UniprotRecord> getAll(final Map<UniprotRecordProperty, Object> filter, final Pageable pageable) {
    return uniprotRecordDao.getAll(filter, pageable);
  }

  @Override
  public UniprotRecord getById(final int id) {
    return uniprotRecordDao.getById(id);
  }

  @Override
  public void update(final UniprotRecord object) {
    uniprotRecordDao.update(object);
  }

  @Override
  public void remove(final UniprotRecord object) {
    uniprotRecordDao.delete(object);
  }

  @Override
  public long getCount(final Map<UniprotRecordProperty, Object> filterOptions) {
    throw new NotImplementedException();
  }

  @Override
  public void add(final UniprotRecord mr) {
    uniprotRecordDao.add(mr);
  }
}
