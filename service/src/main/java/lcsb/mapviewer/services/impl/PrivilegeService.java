package lcsb.mapviewer.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.persist.dao.security.PrivilegeDao;
import lcsb.mapviewer.services.interfaces.IPrivilegeService;

@Transactional
@Service
public class PrivilegeService implements IPrivilegeService {

  private PrivilegeDao privilegeDao;

  @Autowired
  public PrivilegeService(final PrivilegeDao privilegeDao) {
    this.privilegeDao = privilegeDao;
  }

  @Override
  public Privilege getPrivilege(final PrivilegeType type) {
    return getPrivilege(type, null);
  }

  @Override
  public Privilege getPrivilege(final PrivilegeType type, final String objectId) {
    return privilegeDao.getPrivilegeForTypeAndObjectId(type, objectId);
  }

  @Override
  public void updatePrivilege(final Privilege privilege) {
    privilegeDao.update(privilege);
  }

}
