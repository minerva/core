package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.graphics.LayerDao;
import lcsb.mapviewer.persist.dao.graphics.LayerProperty;
import lcsb.mapviewer.services.interfaces.ILayerService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.websockets.IWebSocketMessenger;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageEntityCreated;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageEntityDeleted;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageEntityUpdated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Transactional
@Service
public class LayerService implements ILayerService {

  private final IWebSocketMessenger webSocketMessenger;

  private final IModelService modelService;

  private final LayerDao layerDao;

  @Autowired
  public LayerService(final LayerDao layerDao, final IWebSocketMessenger webSocketMessenger, final IModelService modelService) {
    this.layerDao = layerDao;
    this.webSocketMessenger = webSocketMessenger;
    this.modelService = modelService;
  }

  @Override
  public Page<Layer> getAll(final Map<LayerProperty, Object> filter, final Pageable pageable) {
    return layerDao.getAll(filter, pageable);
  }

  @Override
  public Layer getById(final int id) {
    return layerDao.getById(id);
  }

  @Override
  public void update(final Layer layer) {
    layerDao.update(layer);
    String projectId = layer.getModel().getProject().getProjectId();
    Integer modelId = layer.getModel().getId();
    webSocketMessenger.broadcast(projectId, new WebSocketMessageEntityUpdated(layer, projectId, modelId));
  }

  @Override
  public void remove(final Layer layer) {
    layerDao.delete(layer);
    String projectId = layer.getModel().getProject().getProjectId();
    Integer modelId = layer.getModel().getId();
    webSocketMessenger.broadcast(projectId, new WebSocketMessageEntityDeleted(layer, projectId, modelId));
  }

  @Override
  public long getCount(final Map<LayerProperty, Object> filterOptions) {
    return layerDao.getCount(filterOptions);
  }

  @Override
  public void add(final Layer object) {
    layerDao.add(object);
    ModelData model = modelService.getById(object.getModel().getId());
    String projectId = model.getProject().getProjectId();
    Integer modelId = model.getId();
    webSocketMessenger.broadcast(projectId, new WebSocketMessageEntityCreated(object, projectId, modelId));
  }

}
