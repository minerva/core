package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.annotation.services.ModelAnnotator;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.job.MinervaJob;
import lcsb.mapviewer.model.job.MinervaJobType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.model.user.UserAnnotationSchema;
import lcsb.mapviewer.model.user.UserClassAnnotators;
import lcsb.mapviewer.modelutils.map.ClassTreeNode;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.services.interfaces.IAnnotationService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.services.jobs.AnnotateProjectMinervaJob;
import lcsb.mapviewer.services.search.drug.IDrugService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.LinkedList;
import java.util.Queue;
import java.util.function.Consumer;

@Service
public class AnnotationService implements IAnnotationService {

  private final Logger logger = LogManager.getLogger();

  private final ModelAnnotator modelAnnotator;

  private final IUserService userService;

  private final IMinervaJobService minervaJobService;

  @Autowired
  private IAnnotationService self;

  private final IProjectService projectService;
  private final IDrugService drugService;

  @Autowired
  public AnnotationService(final IMinervaJobService minervaJobService,
                           final ModelAnnotator modelAnnotator,
                           final IUserService userService,
                           final IProjectService projectService,
                           final IDrugService drugService) {
    this.minervaJobService = minervaJobService;
    this.modelAnnotator = modelAnnotator;
    this.userService = userService;
    this.projectService = projectService;
    this.drugService = drugService;
  }

  @PostConstruct
  public void init() {
    minervaJobService.registerExecutor(MinervaJobType.ANNOTATE_PROJECT, self);
  }


  @Override
  public void execute(final MinervaJob job) throws Exception {
    if (job.getJobType() == MinervaJobType.ANNOTATE_PROJECT) {
      AnnotateProjectMinervaJob parameters = new AnnotateProjectMinervaJob(job.getJobParameters());
      self.handleAnnotateProjectJob(parameters, (final Double progress) -> minervaJobService.updateJobProgress(job, progress));

      Project project = projectService.getProjectByProjectId(parameters.getProjectId());

      projectService.submitRefreshChemicalInfoJobs(parameters.getProjectId());
      drugService.submitRefreshChemblSuggestedQueryList(project.getOrganism(), project);
      drugService.submitRefreshDrugBankSuggestedQueryList(project.getOrganism(), project);

    } else {
      throw new NotImplementedException(job.getJobType() + " not implemented");
    }
  }


  /**
   * Retrieves (or creates) annotation schema for a given user.
   *
   * @return {@link UserAnnotationSchema} for {@link User}
   */
  @Override
  public UserAnnotationSchema prepareUserAnnotationSchema(final User user, final boolean initializeLazy) {
    UserAnnotationSchema annotationSchema = null;
    if (user != null) {
      annotationSchema = userService.getById(user.getId()).getAnnotationSchema();
      if (annotationSchema != null && annotationSchema.getClassAnnotators().isEmpty()) {
        for (final UserClassAnnotators uca : modelAnnotator.createDefaultAnnotatorSchema().getClassAnnotators()) {
          annotationSchema.addClassAnnotator(uca);
        }
      }
    }
    if (annotationSchema == null) {
      annotationSchema = modelAnnotator.createDefaultAnnotatorSchema();

      final ElementUtils elementUtils = new ElementUtils();

      final ClassTreeNode top = elementUtils.getAnnotatedElementClassTree();

      final Queue<ClassTreeNode> nodes = new LinkedList<>();
      nodes.add(top);

      while (!nodes.isEmpty()) {
        final ClassTreeNode element = nodes.poll();
        annotationSchema.addClassAnnotator(new UserClassAnnotators(element.getClazz(),
            modelAnnotator.getDefaultAnnotators(element.getClazz())));
        nodes.addAll(element.getChildren());
      }
      if (user != null) {
        final User dbUser = userService.getById(user.getId());
        dbUser.setAnnotationSchema(annotationSchema);
        userService.update(dbUser);
      }
    }
    if (user != null) {
      return userService.getUserByLogin(user.getLogin(), initializeLazy).getAnnotationSchema();
    }
    return annotationSchema;
  }

  @Override
  public void handleAnnotateProjectJob(final AnnotateProjectMinervaJob parameters, final Consumer<Double> updateProgress) {
    Project project = projectService.getProjectByProjectId(parameters.getProjectId());
    modelAnnotator.performAnnotations(project, updateProgress::accept, parameters.getSchema());
  }
}
