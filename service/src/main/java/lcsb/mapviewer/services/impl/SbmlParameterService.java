package lcsb.mapviewer.services.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.persist.dao.map.kinetics.SbmlParameterDao;
import lcsb.mapviewer.persist.dao.map.kinetics.SbmlParameterProperty;
import lcsb.mapviewer.services.interfaces.ISbmlParameterService;

@Transactional
@Service
public class SbmlParameterService implements ISbmlParameterService {

  @Autowired
  private SbmlParameterDao parameterDao;

  @Override
  public void update(final SbmlParameter parameter) {
    parameterDao.update(parameter);
  }

  @Override
  public SbmlParameter getById(final int parameterId) {
    return parameterDao.getById(parameterId);
  }

  @Override
  public void remove(final SbmlParameter parameter) {
    parameterDao.delete(parameter);
  }

  @Override
  public void add(final SbmlParameter parameter) {
    parameterDao.add(parameter);
  }

  @Override
  public Page<SbmlParameter> getAll(final Map<SbmlParameterProperty, Object> filter, final Pageable pageable) {
    return parameterDao.getAll(filter, pageable);
  }

  @Override
  public long getCount(final Map<SbmlParameterProperty, Object> filterOptions) {
    return parameterDao.getCount(filterOptions);
  }
}
