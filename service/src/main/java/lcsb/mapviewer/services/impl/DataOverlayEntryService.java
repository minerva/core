package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.persist.dao.map.DataOverlayEntryDao;
import lcsb.mapviewer.persist.dao.map.DataOverlayEntryProperty;
import lcsb.mapviewer.services.interfaces.IDataOverlayEntryService;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Transactional
@Service
public class DataOverlayEntryService implements IDataOverlayEntryService {
  @Autowired
  private DataOverlayEntryDao dataOverlayEntryDao;


  @Override
  public Page<DataOverlayEntry> getAll(final Map<DataOverlayEntryProperty, Object> filter, final Pageable pageable) {
    return dataOverlayEntryDao.getAll(filter, pageable);
  }

  @Override
  public Page<DataOverlayEntry> getAll(final Map<DataOverlayEntryProperty, Object> filter, final Pageable pageable, final boolean initLazy) {
    final Page<DataOverlayEntry> page = dataOverlayEntryDao.getAll(filter, pageable);
    if (initLazy) {
      for (final DataOverlayEntry entry : page.getContent()) {
        initializeEntry(entry);
      }
    }
    return page;
  }


  private void initializeEntry(final DataOverlayEntry entry) {
    Hibernate.initialize(entry.getCompartments());
    Hibernate.initialize(entry.getMiriamData());
    Hibernate.initialize(entry.getTypes());
  }

  @Override
  public DataOverlayEntry getById(final int id) {
    return dataOverlayEntryDao.getById(id);
  }

  @Override
  public DataOverlayEntry getById(final int id, final boolean initLazy) {
    final DataOverlayEntry entry = dataOverlayEntryDao.getById(id);
    if (initLazy) {
      initializeEntry(entry);
    }
    return entry;
  }

  @Override
  public void update(final DataOverlayEntry object) {
    dataOverlayEntryDao.update(object);
  }

  @Override
  public void remove(final DataOverlayEntry object) {
    dataOverlayEntryDao.delete(object);
  }

  @Override
  public long getCount(final Map<DataOverlayEntryProperty, Object> filterOptions) {
    return dataOverlayEntryDao.getCount(filterOptions);
  }

  @Override
  public void add(final DataOverlayEntry object) {
    dataOverlayEntryDao.add(object);
  }

}
