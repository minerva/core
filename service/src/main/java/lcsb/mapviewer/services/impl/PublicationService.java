package lcsb.mapviewer.services.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.Article;
import lcsb.mapviewer.persist.dao.ArticleDao;
import lcsb.mapviewer.persist.dao.ArticleProperty;
import lcsb.mapviewer.services.interfaces.IPublicationService;

@Transactional
@Service
public class PublicationService implements IPublicationService {

  @Autowired
  private ArticleDao articleDao;

  @Override
  public Page<Article> getAll(final Map<ArticleProperty, Object> filterOptions, final Pageable pageable) {
    return articleDao.getAll(filterOptions, pageable);
  }

  @Override
  public long getCount(final Map<ArticleProperty, Object> filterOptions) {
    return articleDao.getCount(filterOptions);
  }

  @Override
  public void add(final Article article) {
    articleDao.add(article);
  }

  @Override
  public Article getById(final int id) {
    throw new NotImplementedException();
  }

  @Override
  public void update(final Article object) {
    throw new NotImplementedException();
  }

  @Override
  public void remove(final Article object) {
    throw new NotImplementedException();
  }

}
