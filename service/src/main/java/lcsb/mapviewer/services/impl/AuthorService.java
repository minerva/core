package lcsb.mapviewer.services.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.persist.dao.map.AuthorDao;
import lcsb.mapviewer.persist.dao.map.AuthorProperty;
import lcsb.mapviewer.services.interfaces.IAuthorService;

@Transactional
@Service
public class AuthorService implements IAuthorService {

  private AuthorDao authorDao;

  @Autowired
  public AuthorService(final AuthorDao authorDao) {
    this.authorDao = authorDao;
  }

  @Override
  public Page<Author> getAll(final Map<AuthorProperty, Object> filter, final Pageable pageable) {
    return authorDao.getAll(filter, pageable);
  }

  @Override
  public Author getById(final int id) {
    return authorDao.getById(id);
  }

  @Override
  public void update(final Author author) {
    authorDao.update(author);
  }

  @Override
  public void remove(final Author author) {
    authorDao.delete(author);
  }

  @Override
  public long getCount(final Map<AuthorProperty, Object> filterOptions) {
    throw new NotImplementedException();
  }

  @Override
  public void add(final Author object) {
    throw new NotImplementedException();
  }

}
