package lcsb.mapviewer.services.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.persist.dao.map.OverviewImageDao;
import lcsb.mapviewer.persist.dao.map.OverviewImageProperty;
import lcsb.mapviewer.services.interfaces.IOverviewImageService;

@Transactional
@Service
public class OverviewImageService implements IOverviewImageService {

  private OverviewImageDao overviewImageDao;

  @Autowired
  public OverviewImageService(final OverviewImageDao overviewImageDao) {
    this.overviewImageDao = overviewImageDao;
  }

  @Override
  public Page<OverviewImage> getAll(final Map<OverviewImageProperty, Object> filterOptions, final Pageable pageable) {
    return overviewImageDao.getAll(filterOptions, pageable);
  }

  @Override
  public OverviewImage getById(final int id) {
    throw new NotImplementedException();
  }

  @Override
  public void update(final OverviewImage object) {
    throw new NotImplementedException();
  }

  @Override
  public void remove(final OverviewImage object) {
    throw new NotImplementedException();
  }

  @Override
  public long getCount(final Map<OverviewImageProperty, Object> filterOptions) {
    throw new NotImplementedException();
  }

  @Override
  public void add(final OverviewImage object) {
    throw new NotImplementedException();
  }
}
