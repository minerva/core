package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.persist.dao.map.species.ModificationResidueDao;
import lcsb.mapviewer.persist.dao.map.species.ModificationResidueProperty;
import lcsb.mapviewer.services.interfaces.IModificationResidueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Transactional
@Service
public class ModificationResidueService implements IModificationResidueService {

  private final ModificationResidueDao modificationResidueDao;

  @Autowired
  public ModificationResidueService(final ModificationResidueDao modificationResidueDao) {
    this.modificationResidueDao = modificationResidueDao;
  }

  @Override
  public Page<ModificationResidue> getAll(final Map<ModificationResidueProperty, Object> filter, final Pageable pageable) {
    return modificationResidueDao.getAll(filter, pageable);
  }

  @Override
  public ModificationResidue getById(final int id) {
    return modificationResidueDao.getById(id);
  }

  @Override
  public void update(final ModificationResidue object) {
    modificationResidueDao.update(object);
  }

  @Override
  public void remove(final ModificationResidue object) {
    modificationResidueDao.delete(object);
  }

  @Override
  public long getCount(final Map<ModificationResidueProperty, Object> filterOptions) {
    throw new NotImplementedException();
  }

  @Override
  public void add(final ModificationResidue mr) {
    modificationResidueDao.add(mr);
  }
}
