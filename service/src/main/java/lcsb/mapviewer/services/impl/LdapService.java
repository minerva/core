package lcsb.mapviewer.services.impl;

import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.BindResult;
import com.unboundid.ldap.sdk.Filter;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.ResultCode;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.SearchScope;
import com.unboundid.ldap.sdk.SimpleBindRequest;
import com.unboundid.util.ssl.SSLUtil;
import com.unboundid.util.ssl.TrustAllTrustManager;

import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.services.UserDTO;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.ILdapService;

@Transactional
@Service
public class LdapService implements ILdapService {
  private static Logger logger = LogManager.getLogger();

  private IConfigurationService configurationService;

  @Autowired
  public LdapService(final IConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

  public LDAPConnection getConnection() throws LDAPException {
    String address = configurationService.getConfigurationValue(ConfigurationElementType.LDAP_ADDRESS);
    if (address == null || address.trim().isEmpty()) {
      return null;
    }
    boolean ssl = "true"
        .equalsIgnoreCase(configurationService.getConfigurationValue(ConfigurationElementType.LDAP_SSL));

    String portString = configurationService.getConfigurationValue(ConfigurationElementType.LDAP_PORT);
    if (portString == null || address.trim().isEmpty()) {
      if (ssl) {
        portString = "636";
      } else {
        portString = "389";
      }
    }
    int port = Integer.parseInt(portString);
    LDAPConnection connection;
    if (ssl) {
      SSLUtil sslUtil = new SSLUtil(new TrustAllTrustManager());
      try {
        connection = new LDAPConnection(sslUtil.createSSLSocketFactory());
      } catch (final GeneralSecurityException e) {
        throw new InvalidStateException(e);
      }
    } else {
      connection = new LDAPConnection();
    }
    connection.connect(address, port);

    String bindDn = configurationService.getConfigurationValue(ConfigurationElementType.LDAP_BIND_DN);
    String password = configurationService.getConfigurationValue(ConfigurationElementType.LDAP_PASSWORD);

    if (bindDn == null || bindDn.trim().isEmpty()) {
      connection.bind(new SimpleBindRequest());
    } else {
      connection.bind(bindDn, password);
    }

    return connection;
  }

  @Override
  public boolean login(final String login, final String password) throws LDAPException {
    if (!isValidConfiguration()) {
      logger.warn("Invalid LDAP configuration");
      return false;
    }
    LDAPConnection connection = getConnection();
    UserDTO user = getUserByLogin(login);
    if (user != null) {
      try {
        BindResult result = connection.bind(user.getBindDn(), password);
        logger.debug("LDAP login status for user " + login + ": " + result.toString());
        return result.getResultCode().equals(ResultCode.SUCCESS);
      } catch (final Exception e) {
        logger.debug("LDAP login for user " + login + " failed", e);
        return false;
      }
    }

    return false;
  }

  @Override
  public List<String> getUsernames() throws LDAPException {
    if (!isValidConfiguration()) {
      logger.warn("Invalid LDAP configuration");
      return new ArrayList<>();
    }
    List<String> result = new ArrayList<>();
    LDAPConnection connection = getConnection();

    Filter f2 = createObjectClassFilter();
    Filter f3 = createAttributeFilter();

    Filter filter = Filter.createANDFilter(f2, f3);

    String baseDn = configurationService.getConfigurationValue(ConfigurationElementType.LDAP_BASE_DN);
    String uidProperty = configurationService.getConfigurationValue(ConfigurationElementType.LDAP_UID);
    SearchResult searchResult = connection.search(baseDn, SearchScope.SUB, filter);

    for (final SearchResultEntry entry : searchResult.getSearchEntries()) {
      Attribute uid = entry.getAttribute(uidProperty);
      if (uid != null) {
        result.add(uid.getValue());
      } else {
        logger.warn("Invalid ldap entry: " + entry);
      }
    }
    connection.close();

    return result;
  }

  @Override
  public UserDTO getUserByLogin(final String login) throws LDAPException {
    if (!isValidConfiguration()) {
      logger.warn("Invalid LDAP configuration");
      return null;
    }
    LDAPConnection connection = getConnection();
    try {
      String baseDn = configurationService.getConfigurationValue(ConfigurationElementType.LDAP_BASE_DN);
      String uidAttribute = configurationService.getConfigurationValue(ConfigurationElementType.LDAP_UID);

      String firstNameAttribute = configurationService
          .getConfigurationValue(ConfigurationElementType.LDAP_FIRST_NAME_ATTRIBUTE);
      String lastNameAttribute = configurationService
          .getConfigurationValue(ConfigurationElementType.LDAP_LAST_NAME_ATTRIBUTE);
      String emailAttribute = configurationService.getConfigurationValue(ConfigurationElementType.LDAP_EMAIL_ATTRIBUTE);

      Filter loginFilter = createLoginFilter(login);
      Filter f2 = createObjectClassFilter();
      Filter f3 = createAttributeFilter();

      Filter filter = Filter.createANDFilter(loginFilter, f2, f3);
      SearchResult searchResult = connection.search(baseDn, SearchScope.SUB, filter);

      for (final SearchResultEntry entry : searchResult.getSearchEntries()) {
        UserDTO result = new UserDTO();
        result.setBindDn(entry.getDN());

        Attribute uid = entry.getAttribute(uidAttribute);
        if (uid != null) {
          String remoteLogin = uid.getValue();
          if (remoteLogin != null) {
            remoteLogin = remoteLogin.toLowerCase();
          }
          result.setLogin(remoteLogin);
        } else {
          logger.warn("Invalid ldap entry: " + entry);
        }
        if (!firstNameAttribute.trim().isEmpty()) {
          Attribute firstName = entry.getAttribute(firstNameAttribute);
          if (firstName != null) {
            result.setFirstName(firstName.getValue());
          }
        }

        if (!lastNameAttribute.trim().isEmpty()) {
          Attribute lastName = entry.getAttribute(lastNameAttribute);
          if (lastName != null) {
            result.setLastName(lastName.getValue());
          }
        }

        if (!emailAttribute.trim().isEmpty()) {
          Attribute emailName = entry.getAttribute(emailAttribute);
          if (emailName != null) {
            result.setEmail(emailName.getValue());
          }
        }

        return result;
      }
      searchResult = connection.search(baseDn, SearchScope.SUB, loginFilter);
      if (searchResult.getSearchEntries().size() > 0) {
        logger.debug("User '" + login + "' exists, but was filtered out [filter=" + filter.toString() + "]");
      }
      return null;
    } finally {
      connection.close();
    }
  }

  @Override
  public boolean isValidConfiguration() {
    try {
      String baseDn = configurationService.getConfigurationValue(ConfigurationElementType.LDAP_BASE_DN);
      if (baseDn == null || baseDn.trim().isEmpty()) {
        return false;
      }
      LDAPConnection connection = getConnection();
      if (connection != null) {
        connection.close();
        return true;
      }
      return false;
    } catch (final Exception e) {
      logger.error(e, e);
      return false;
    }
  }

  private Filter createObjectClassFilter() throws LDAPException {
    String objectClass = configurationService.getConfigurationValue(ConfigurationElementType.LDAP_OBJECT_CLASS);

    if (objectClass == null || objectClass.trim().isEmpty() || objectClass.equals("*")) {
      return Filter.create("objectClass=*");
    }

    return Filter.createEqualityFilter("objectClass", objectClass);
  }

  Filter createAttributeFilter() throws LDAPException {
    String ldapStringFilter = configurationService.getConfigurationValue(ConfigurationElementType.LDAP_FILTER);

    if (ldapStringFilter == null || ldapStringFilter.trim().isEmpty()) {
      return Filter.create("objectClass=*");
    }

    return Filter.create(ldapStringFilter);
  }

  private Filter createLoginFilter(final String login) {
    String uidProperty = configurationService.getConfigurationValue(ConfigurationElementType.LDAP_UID);
    return Filter.createEqualityFilter(uidProperty, login);
  }

  public IConfigurationService getConfigurationService() {
    return configurationService;
  }

  public void setConfigurationService(final IConfigurationService configurationService) {
    this.configurationService = configurationService;
  }
}
