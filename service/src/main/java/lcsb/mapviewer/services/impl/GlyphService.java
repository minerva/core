package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.persist.dao.graphics.GlyphDao;
import lcsb.mapviewer.persist.dao.graphics.GlyphProperty;
import lcsb.mapviewer.services.interfaces.IGlyphService;
import lcsb.mapviewer.services.websockets.IWebSocketMessenger;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageEntityCreated;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Transactional
@Service
public class GlyphService implements IGlyphService {

  @Autowired
  private IWebSocketMessenger webSocketMessenger;

  private final GlyphDao glyphDao;

  @Autowired
  public GlyphService(final GlyphDao glyphDao) {
    this.glyphDao = glyphDao;
  }

  @Override
  public Page<Glyph> getAll(final Map<GlyphProperty, Object> filterOptions, final Pageable pageable) {
    return glyphDao.getAll(filterOptions, pageable);
  }

  @Override
  public Page<Glyph> getAll(final Map<GlyphProperty, Object> filterOptions, final Pageable pageable, final boolean initializeLazy) {
    Page<Glyph> glyphs = glyphDao.getAll(filterOptions, pageable);
    if (initializeLazy) {
      for (Glyph glyph : glyphs.getContent()) {
        Hibernate.initialize(glyph.getFile());
      }
    }
    return glyphs;
  }

  @Override
  public Glyph getById(final int id) {
    throw new NotImplementedException();
  }

  @Override
  public void update(final Glyph author) {
    throw new NotImplementedException();
  }

  @Override
  public void remove(final Glyph author) {
    throw new NotImplementedException();
  }

  @Override
  public long getCount(final Map<GlyphProperty, Object> filterOptions) {
    throw new NotImplementedException();
  }

  @Override
  public void add(final Glyph object) {
    glyphDao.add(object);
    webSocketMessenger.broadcast(object.getProject().getProjectId(), new WebSocketMessageEntityCreated(object, object.getProject().getProjectId()));
  }
}
