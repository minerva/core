package lcsb.mapviewer.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.annotation.services.TaxonomySearchException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.services.interfaces.ITaxonomyService;

@Transactional
@Service
public class TaxonomyService implements ITaxonomyService {

  private TaxonomyBackend taxonomyBackend;

  @Autowired
  public TaxonomyService(final TaxonomyBackend taxonomyBackend) {
    this.taxonomyBackend = taxonomyBackend;
  }

  @Override
  public String getNameForTaxonomy(final MiriamData miriamData) throws TaxonomySearchException {
    return taxonomyBackend.getNameForTaxonomy(miriamData);
  }

}
