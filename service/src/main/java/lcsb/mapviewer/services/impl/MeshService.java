package lcsb.mapviewer.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.data.MeSH;
import lcsb.mapviewer.annotation.services.MeSHParser;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.services.interfaces.IMeshService;

@Transactional
@Service
public class MeshService implements IMeshService {

  private MeSHParser meSHParser;

  @Autowired
  public MeshService(final MeSHParser meSHParser) {
    this.meSHParser = meSHParser;
  }


  @Override
  public MeSH getMesh(final MiriamData meshID) throws AnnotatorException {
    return meSHParser.getMeSH(meshID);
  }


  @Override
  public boolean isValidMeshId(final MiriamData meshId) throws AnnotatorException {
    return meSHParser.isValidMeshId(meshId);
  }
}
