package lcsb.mapviewer.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.persist.dao.map.NodeDao;
import lcsb.mapviewer.services.interfaces.INodeService;

@Transactional
@Service
public class NodeService implements INodeService {

  @Autowired
  private NodeDao nodeDao;

  @Override
  public void remove(final AbstractNode object) {
    nodeDao.delete(object);
  }

  @Override
  public void update(final AbstractNode object) {
    nodeDao.update(object);
  }

  @Override
  public AbstractNode getById(final int id) {
    return nodeDao.getById(id);
  }

}
