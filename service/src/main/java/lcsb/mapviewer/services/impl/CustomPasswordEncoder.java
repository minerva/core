package lcsb.mapviewer.services.impl;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class CustomPasswordEncoder implements PasswordEncoder {

  // CHECKSTYLE.OFF
  private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
  // CHECKSTYLE.ON

  private CustomMd5PasswordEncoder md5PasswordEncoder = new CustomMd5PasswordEncoder();

  @Override
  public String encode(final CharSequence rawPassword) {
    return bCryptPasswordEncoder.encode(md5PasswordEncoder.encode(rawPassword));
  }

  @Override
  public boolean matches(final CharSequence rawPassword, final String encodedPassword) {
    return bCryptPasswordEncoder.matches(md5PasswordEncoder.encode(rawPassword), encodedPassword);
  }

}
