package lcsb.mapviewer.services.impl;

import com.unboundid.ldap.sdk.LDAPException;
import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.model.map.layout.ProjectBackground;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.EmailConfirmationToken;
import lcsb.mapviewer.model.user.ResetPasswordToken;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.model.user.UserClassAnnotators;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.persist.dao.map.DataOverlayDao;
import lcsb.mapviewer.persist.dao.map.ProjectBackgroundDao;
import lcsb.mapviewer.persist.dao.security.PrivilegeDao;
import lcsb.mapviewer.persist.dao.security.PrivilegeProperty;
import lcsb.mapviewer.persist.dao.user.EmailConfirmationTokenDao;
import lcsb.mapviewer.persist.dao.user.ResetPasswordTokenDao;
import lcsb.mapviewer.persist.dao.user.UserDao;
import lcsb.mapviewer.persist.dao.user.UserProperty;
import lcsb.mapviewer.services.InvalidTokenException;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.ILdapService;
import lcsb.mapviewer.services.interfaces.IPrivilegeService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.services.utils.EmailException;
import lcsb.mapviewer.services.utils.EmailSender;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Transactional
@Service
public class UserService implements IUserService {

  private static final Logger logger = LogManager.getLogger();

  private final UserDao userDao;
  private final DataOverlayDao dataOverlayDao;
  private final ILdapService ldapService;
  private final IConfigurationService configurationService;
  private final IPrivilegeService privilegeService;
  private final ProjectBackgroundDao projectBackgroundDao;
  private final ResetPasswordTokenDao resetPasswordTokenDao;
  private final EmailConfirmationTokenDao emailConfirmationTokenDao;
  private final EmailSender emailSender;
  private final PrivilegeDao privilegeDao;

  @Autowired
  public UserService(final UserDao userDao,
                     final ILdapService ldapService,
                     final IConfigurationService configurationService,
                     final IPrivilegeService privilegeService,
                     final ResetPasswordTokenDao resetPasswordTokenDao,
                     final EmailSender emailSender,
                     final DataOverlayDao dataOverlayDao,
                     final ProjectBackgroundDao projectBackgroundDao,
                     final EmailConfirmationTokenDao emailConfirmationTokenDao, final PrivilegeDao privilegeDao) {
    this.userDao = userDao;
    this.ldapService = ldapService;
    this.configurationService = configurationService;
    this.privilegeService = privilegeService;
    this.resetPasswordTokenDao = resetPasswordTokenDao;
    this.emailConfirmationTokenDao = emailConfirmationTokenDao;
    this.emailSender = emailSender;
    this.dataOverlayDao = dataOverlayDao;
    this.projectBackgroundDao = projectBackgroundDao;
    this.privilegeDao = privilegeDao;
  }

  @Override
  public void update(final User user) {
    userDao.update(user);
  }

  @Override
  public void remove(final User user) {
    String login = user.getLogin();
    User userFromDb = getUserByLogin(login);
    List<ProjectBackground> backgrounds = projectBackgroundDao.getProjectBackgroundsByUser(userFromDb);
    for (final ProjectBackground background : backgrounds) {
      projectBackgroundDao.delete(background);
    }
    dataOverlayDao.deleteDataOverlays(dataOverlayDao.getDataOverlayByOwner(userFromDb));
    userDao.delete(userFromDb);
    logger.info("User {} removed successfully", login);
  }

  @Override
  public User getUserByLogin(final String login, final boolean loadLazy) {
    User user = getUserByLogin(login);
    if (loadLazy) {
      initializeLazy(user);
    }
    return user;
  }

  @Override
  public User getUserByLogin(final String login) {
    return userDao.getUserByLogin(login);
  }


  private static void initializeLazy(final User user) {
    if (user != null) {
      Hibernate.initialize(user.getPrivileges());
      if (user.getAnnotationSchema() != null) {
        for (final UserClassAnnotators annotator : user.getAnnotationSchema().getClassAnnotators()) {
          for (final AnnotatorData data : annotator.getAnnotators()) {
            Hibernate.initialize(data.getAnnotatorParams());
          }
        }
        Hibernate.initialize(user.getAnnotationSchema().getGuiPreferences());
      }
    }
  }

  @Override
  public List<User> getUsers(final boolean loadLazy) {
    List<User> users = userDao.getAll();
    if (loadLazy) {
      for (final User user : users) {
        Hibernate.initialize(user.getPrivileges());
      }
    }
    return users;
  }

  @Override
  public void grantUserPrivilege(final User user, final PrivilegeType type) {
    User dbUser = getUserByLogin(user.getLogin());
    dbUser.addPrivilege(privilegeService.getPrivilege(type));
    userDao.update(dbUser);
  }

  @Override
  public void grantUserPrivilege(final User user, final PrivilegeType type, final String objectId) {
    User dbUser = getUserByLogin(user.getLogin());
    dbUser.addPrivilege(privilegeService.getPrivilege(type, objectId));
    userDao.update(dbUser);
  }

  @Override
  public void revokeUserPrivilege(final User user, final PrivilegeType type) {
    User dbUser = getUserByLogin(user.getLogin());
    dbUser.removePrivilege(privilegeService.getPrivilege(type));
    userDao.update(dbUser);
  }

  @Override
  public void revokeUserPrivilege(final User user, final PrivilegeType type, final String objectId) {
    User dbUser = getUserByLogin(user.getLogin());
    dbUser.removePrivilege(privilegeService.getPrivilege(type, objectId));
    userDao.update(dbUser);
  }

  @Override
  public void grantDefaultPrivileges(final User user) {
    User anonymous = getUserByLogin(Configuration.ANONYMOUS_LOGIN);
    for (Privilege privilege : anonymous.getPrivileges()) {
      grantUserPrivilege(user, privilege.getType(), privilege.getObjectId());
    }
  }

  @Override
  public void grantPrivilegeToAllUsersWithDefaultAccess(final PrivilegeType type, final String objectId) {
    getUsers(false).stream()
        .filter(u -> u.getPrivileges().contains(new Privilege(type, "*")))
        .forEach(u -> grantUserPrivilege(u, type, objectId));
  }

  @Override
  public void revokeObjectDomainPrivilegesForAllUsers(final PrivilegeType privilegeType, final String objectId) {
    userDao.getAll().forEach(user -> revokeUserPrivilege(user, privilegeType, objectId));
  }

  @Override
  public ColorExtractor getColorExtractorForUser(final User loggedUser) {
    Color colorMin = null;
    Color colorMax = null;
    Color colorSimple = null;
    Color colorNeutral = null;
    if (loggedUser != null) {
      User dbUser = getUserByLogin(loggedUser.getLogin());
      if (dbUser != null) {
        colorMin = dbUser.getMinColor();
        colorMax = dbUser.getMaxColor();
        colorSimple = dbUser.getSimpleColor();
        colorNeutral = dbUser.getNeutralColor();
      }
    }
    ColorParser parser = new ColorParser();

    if (colorMin == null) {
      colorMin = parser.parse(configurationService.getConfigurationValue(ConfigurationElementType.MIN_COLOR_VAL));
    }
    if (colorMax == null) {
      colorMax = parser.parse(configurationService.getConfigurationValue(ConfigurationElementType.MAX_COLOR_VAL));
    }
    if (colorSimple == null) {
      colorSimple = parser.parse(configurationService.getConfigurationValue(ConfigurationElementType.SIMPLE_COLOR_VAL));
    }
    if (colorNeutral == null) {
      colorNeutral = parser
          .parse(configurationService.getConfigurationValue(ConfigurationElementType.NEUTRAL_COLOR_VAL));
    }
    return new ColorExtractor(colorMin, colorMax, colorSimple, colorNeutral);
  }

  @Override
  public Map<String, Boolean> ldapAccountExistsForLogin(final Collection<User> logins) {
    Map<String, Boolean> result = new HashMap<>();
    for (final User user : logins) {
      result.put(user.getLogin(), false);
    }

    if (ldapService.isValidConfiguration()) {
      try {
        List<String> ldapUserNames = ldapService.getUsernames();
        for (final String string : ldapUserNames) {
          String ldapLogin = string.toLowerCase();
          if (result.containsKey(ldapLogin)) {
            result.put(ldapLogin, true);
          }
        }
      } catch (final LDAPException e) {
        logger.error("Problem with accessing LDAP directory", e);
      }
    }
    return result;
  }

  @Override
  public void createResetPasswordToken(final User user) throws EmailException {
    Calendar expires = Calendar.getInstance();
    expires.add(Calendar.DAY_OF_MONTH, 1);
    ResetPasswordToken token = new ResetPasswordToken(user, UUID.randomUUID().toString(), expires);
    resetPasswordTokenDao.add(token);

    String content = "Dear " + user.getName() + " " + user.getSurname() + " (" + user.getLogin() + "),<br/><br/>"
        + "We received request to reset your password. You can reset the password using this <a href=\""
        + getRootUrl()
        + "login.xhtml?resetPasswordToken=" + token.getToken()
        + "\">link</a>. Link will be valid for the next 24 hours.<br/><br/>"
        + "If you did not perform this action you can ignore the email.<br/>";
    try {
      emailSender.sendEmail("Minerva - Reset password", content, user.getEmail());
    } catch (final EmailException e) {
      resetPasswordTokenDao.delete(token);
      throw e;
    }

  }

  @Override
  public void resetPassword(final String token, final String encryptedPassword) throws InvalidTokenException {
    ResetPasswordToken resetToken = resetPasswordTokenDao.getByToken(token);
    if (resetToken != null && resetToken.getExpires().before(Calendar.getInstance())) {
      resetPasswordTokenDao.delete(resetToken);
      resetToken = null;
    }
    if (resetToken == null) {
      throw new InvalidTokenException("Invalid token");
    }
    resetToken.getUser().setCryptedPassword(encryptedPassword);
    userDao.update(resetToken.getUser());
    resetPasswordTokenDao.delete(resetToken);
  }

  @Override
  public long getPasswordTokenCount() {
    return resetPasswordTokenDao.getCount();
  }

  @Override
  public void add(final ResetPasswordToken token) {
    resetPasswordTokenDao.add(token);
  }

  @Override
  public void add(final EmailConfirmationToken token) {
    emailConfirmationTokenDao.add(token);
  }

  @Override
  public void add(final User user) {
    if (userDao.getUserByLogin(user.getLogin()) != null) {
      throw new InvalidArgumentException("Login already in use");
    }
    if (user.getEmail() != null && !user.getEmail().isEmpty() && userDao.getUserByEmail(user.getEmail()) != null) {
      throw new InvalidArgumentException("Email already in use");
    }
    userDao.add(user);
    logger.info("User {} created successfully", user.getLogin());
  }

  @Override
  public List<ResetPasswordToken> getPasswordTokens(final String login) {
    return resetPasswordTokenDao.getByUser(getUserByLogin(login));
  }

  @Override
  public User getUserByOrcidId(final String orcidId) {
    return userDao.getUserByOrcidId(orcidId);
  }

  @Override
  public User registerUser(final User user) throws EmailException {
    user.setActive(false);
    user.setConfirmed(false);
    add(user);
    createRegisterUserToken(user);

    return user;
  }

  @Override
  public void createRegisterUserToken(final User user) throws EmailException {
    Calendar expires = Calendar.getInstance();
    expires.add(Calendar.DAY_OF_MONTH, 1);
    EmailConfirmationToken token = new EmailConfirmationToken(user, UUID.randomUUID().toString(), expires);
    emailConfirmationTokenDao.add(token);

    String content = "Dear " + user.getName() + " " + user.getSurname() + " (" + user.getLogin() + "),<br/><br/>"
        + "We received request to create an account. You can confirm this request using this <a href=\""
        + getRootUrl()
        + "login.xhtml?confirmEmailToken=" + token.getToken() + "&login=" + user.getEmail()
        + "\">link</a>. Link will be valid for the next 24 hours.<br/><br/>"
        + "If you did not perform this action you can ignore the email.<br/>";
    try {
      emailSender.sendEmail("Minerva - Confirmation of account creation", content, user.getEmail());
    } catch (final EmailException e) {
      emailConfirmationTokenDao.delete(token);
      throw e;
    }

  }

  private String getRootUrl() {
    String url = configurationService.getConfigurationValue(ConfigurationElementType.MINERVA_ROOT);
    if (!url.endsWith("/")) {
      url += "/";
    }
    return url;
  }

  @Override
  public User confirmEmail(final String login, final String token) throws InvalidTokenException {
    EmailConfirmationToken confirmationToken = emailConfirmationTokenDao.getByToken(token);
    if (confirmationToken != null && confirmationToken.getExpires().before(Calendar.getInstance())) {
      emailConfirmationTokenDao.delete(confirmationToken);
      confirmationToken = null;
    }
    if (confirmationToken == null) {
      throw new InvalidTokenException("Invalid token");
    }
    if (!confirmationToken.getUser().getLogin().equalsIgnoreCase(login)) {
      throw new InvalidTokenException("Invalid token");
    }
    User user = confirmationToken.getUser();
    user.setConfirmed(true);
    if (!"true".equalsIgnoreCase(configurationService.getValue(ConfigurationElementType.REQUIRE_APPROVAL_FOR_AUTO_REGISTERED_USERS).getValue())) {
      user.setActive(true);
    } else {
      user.setActive(false);
      if (emailSender.canSendEmails()) {
        List<String> recipients = getAdminEmailAddresses();
        try {
          String url = getRootUrl();
          String subject = "[MINERVA] There is new account waiting for approval";
          String content = "Dear admin,<br/>"
              + "There is a new account (" + user.getLogin() + ") waiting for your approval on <a href=\"" + url + "\">" + url + "</a>.";
          emailSender.sendEmail(recipients, new ArrayList<>(), subject, content);
        } catch (Exception e) {
          logger.error("Problem with sending email", e);
        }
      }
    }
    userDao.update(user);

    emailConfirmationTokenDao.delete(confirmationToken);
    return user;
  }

  private List<String> getAdminEmailAddresses() {
    String[] array = configurationService.getValue(ConfigurationElementType.REQUEST_ACCOUNT_EMAIL).getValue().split(";");
    List<String> recipients = new ArrayList<>();
    for (String string : array) {
      if (!string.trim().isEmpty()) {
        recipients.add(string);
      }
    }
    return recipients;
  }

  @Override
  public void sendUserActivatedEmail(final User user) {
    try {
      List<String> recipients = getAdminEmailAddresses();
      String url = getRootUrl();
      String subject = "[MINERVA] Your account has been approved";
      String content = "Dear " + user.getName() + " " + user.getSurname() + ",<br/>"
          + "Your account (" + user.getLogin() + ") has been approved. You can login using this url: <a href=\"" + url + "\">" + url + "</a>.";
      String email = user.getEmail();
      if (email == null || email.trim().isEmpty()) {
        logger.warn("Cannot send email to {}. Invalid email", user.getLogin());
      } else {
        emailSender.sendEmail(Collections.singletonList(email), recipients, subject, content);
      }
    } catch (Exception e) {
      logger.error("Problem with sending email", e);
    }
  }

  @Override
  public void moveProjectPrivileges(final String oldProjectId, final String newProjectId) {

    Map<PrivilegeProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(PrivilegeProperty.OBJECT_ID, oldProjectId);
    List<Privilege> privileges = privilegeDao.getAll(filterOptions, Pageable.unpaged()).getContent();
    for (Privilege privilege : privileges) {
      privilege.setObjectId(newProjectId);
      privilegeDao.update(privilege);
    }
  }

  @Override
  public Page<User> getAll(final Map<UserProperty, Object> filter, final Pageable pageable) {
    Page<User> result = userDao.getAll(filter, pageable);
    for (User user : result.getContent()) {
      initializeLazy(user);
    }
    return result;
  }

  @Override
  public User getById(final int id) {
    User user = userDao.getById(id);
    initializeLazy(user);
    return user;

  }

  @Override
  public long getCount(final Map<UserProperty, Object> filterOptions) {
    throw new NotImplementedException();
  }
}
