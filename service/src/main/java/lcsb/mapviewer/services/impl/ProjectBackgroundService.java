package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.ProjectBackground;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.dao.map.ProjectBackgroundDao;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IProjectBackgroundService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.services.utils.EmailException;
import lcsb.mapviewer.services.utils.EmailSender;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class ProjectBackgroundService implements IProjectBackgroundService {

  private ProjectBackgroundDao projectBackgroundDao;

  private IUserService userService;

  private IConfigurationService configurationService;

  private EmailSender emailSender;

  @Autowired
  public ProjectBackgroundService(final ProjectBackgroundDao projectBackgroundDao,
                                  final IUserService userService,
                                  final IConfigurationService configurationService,
                                  final DbUtils dbUtils) {
    this.projectBackgroundDao = projectBackgroundDao;
    this.userService = userService;
    this.configurationService = configurationService;
  }

  @Override
  public void updateProjectBackground(final ProjectBackground projectBackground) {
    projectBackgroundDao.update(projectBackground);
  }

  @Override
  public EmailSender getEmailSender() {
    if (emailSender == null) {
      emailSender = new EmailSender(configurationService);
    }
    return emailSender;
  }

  @Override
  public void setEmailSender(final EmailSender emailSender) {
    this.emailSender = emailSender;
  }

  @Override
  public List<ProjectBackground> getProjectBackgroundsByProject(final String projectId) {
    return projectBackgroundDao.getProjectBackgroundsByProject(projectId);
  }

  @Override
  public List<ProjectBackground> getProjectBackgroundsByProject(final Project project) {
    List<ProjectBackground> result = new ArrayList<>();
    List<ProjectBackground> backgrounds = projectBackgroundDao.getProjectBackgroundsByProject(project);
    for (final ProjectBackground projectBackground : backgrounds) {
      Hibernate.initialize(projectBackground.getProjectBackgroundImageLayer());
      Hibernate.initialize(projectBackground.getProject());
    }
    result.addAll(backgrounds);
    result.sort(ProjectBackground.ID_COMPARATOR);
    return result;
  }

  @Override
  public ProjectBackground getProjectBackgroundById(final String projectId, final Integer backgroundId)
      throws ObjectNotFoundException {
    ProjectBackground result = getProjectBackgroundById(backgroundId);
    if (result != null && result.getProject().getProjectId().equals(projectId)) {
      return result;
    }
    throw new ObjectNotFoundException("Overlay with given id does not exist: " + backgroundId);
  }

  @Override
  public ProjectBackground getProjectBackgroundById(final Integer backgroundId) {
    return projectBackgroundDao.getById(backgroundId);
  }

  /**
   * @return the configurationService
   * @see #configurationService
   */
  public IConfigurationService getConfigurationService() {
    return configurationService;
  }

  /**
   * @param configurationService the configurationService to set
   * @see #configurationService
   */
  public void setConfigurationService(final IConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

  /**
   * @return the userService
   * @see #userService
   */
  public IUserService getUserService() {
    return userService;
  }

  /**
   * @param userService the userService to set
   * @see #userService
   */
  public void setUserService(final IUserService userService) {
    this.userService = userService;
  }

  /**
   * Sends notification email that background was removed.
   *
   * @param projectId      identifier of the project
   * @param backgroundName name of the background
   * @param email          notification email
   * @throws MessagingException thrown when there is a problem with sending email
   */
  protected void sendSuccesfullRemoveEmail(final String projectId, final String backgroundName, final String email)
      throws EmailException {
    getEmailSender().sendEmail("Minerva notification", "Project background " + backgroundName
        + " in map " + projectId + " was successfully removed.<br/>", email);
  }

  @Override
  public List<ProjectBackground> getProjectBackgroundsByUser(final User user) {
    return projectBackgroundDao.getProjectBackgroundsByUser(user);
  }

  @Override
  public void delete(final ProjectBackground background) {
    projectBackgroundDao.delete(background);
  }
}
