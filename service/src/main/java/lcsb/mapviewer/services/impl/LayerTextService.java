package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.graphics.LayerTextDao;
import lcsb.mapviewer.persist.dao.graphics.LayerTextProperty;
import lcsb.mapviewer.services.interfaces.ILayerTextService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.websockets.IWebSocketMessenger;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageLayerChildCreated;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageLayerChildDeleted;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageLayerChildUpdated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Transactional
@Service
public class LayerTextService implements ILayerTextService {

  private final IWebSocketMessenger webSocketMessenger;

  private final IModelService modelService;

  private final LayerTextDao layerTextDao;

  @Autowired
  public LayerTextService(final LayerTextDao layerTextDao, final IWebSocketMessenger webSocketMessenger, final IModelService modelService) {
    this.layerTextDao = layerTextDao;
    this.webSocketMessenger = webSocketMessenger;
    this.modelService = modelService;
  }

  @Override
  public Page<LayerText> getAll(final Map<LayerTextProperty, Object> filter, final Pageable pageable) {
    return layerTextDao.getAll(filter, pageable);
  }

  @Override
  public LayerText getById(final int id) {
    return layerTextDao.getById(id);
  }

  @Override
  public void update(final LayerText layerText) {
    layerTextDao.update(layerText);
    String projectId = layerText.getLayer().getModel().getProject().getProjectId();
    webSocketMessenger.broadcast(projectId, new WebSocketMessageLayerChildUpdated(layerText, projectId, layerText.getLayer()));

  }

  @Override
  public void remove(final LayerText layerText) {
    layerTextDao.delete(layerText);
    String projectId = layerText.getLayer().getModel().getProject().getProjectId();
    webSocketMessenger.broadcast(projectId, new WebSocketMessageLayerChildDeleted(layerText, projectId, layerText.getLayer()));
  }

  @Override
  public long getCount(final Map<LayerTextProperty, Object> filterOptions) {
    return layerTextDao.getCount(filterOptions);
  }

  @Override
  public void add(final LayerText layerText) {
    layerTextDao.add(layerText);
    ModelData model = modelService.getById(layerText.getLayer().getModel().getId());
    String projectId = model.getProject().getProjectId();
    webSocketMessenger.broadcast(projectId, new WebSocketMessageLayerChildCreated(layerText, projectId, layerText.getLayer()));
  }

}
