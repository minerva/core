package lcsb.mapviewer.services.impl;

import java.io.FileNotFoundException;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.cache.BigFileCache;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.cache.UploadedFileEntryDao;
import lcsb.mapviewer.services.interfaces.IFileService;

@Transactional
@Service
public class FileService implements IFileService {

  private UploadedFileEntryDao uploadedFileEntryDao;

  private BigFileCache bigFileCache;

  @Autowired
  public FileService(final UploadedFileEntryDao uploadedFileEntryDao, final BigFileCache bigFileCache) {
    this.uploadedFileEntryDao = uploadedFileEntryDao;
    this.bigFileCache = bigFileCache;
  }

  @Override
  public UploadedFileEntry getById(final Integer id) {
    UploadedFileEntry entry = uploadedFileEntryDao.getById(id);
    if (entry != null) {
      Hibernate.initialize(entry.getOwner());
    }
    return entry;
  }

  @Override
  public User getOwnerByFileId(final Integer id) {
    UploadedFileEntry entry = getById(id);
    if (entry != null) {
      return uploadedFileEntryDao.getById(id).getOwner();
    }
    return null;

  }

  @Override
  public void add(final UploadedFileEntry entry) {
    uploadedFileEntryDao.add(entry);
  }

  @Override
  public void update(final UploadedFileEntry fileEntry) {
    uploadedFileEntryDao.update(fileEntry);
  }

  @Override
  public String getLocalPathForFile(final String sourceUrl) throws FileNotFoundException {
    return bigFileCache.getLocalPathForFile(sourceUrl);
  }

  @Override
  public void delete(final UploadedFileEntry fileEntry) {
    uploadedFileEntryDao.delete(fileEntry);
  }

}
