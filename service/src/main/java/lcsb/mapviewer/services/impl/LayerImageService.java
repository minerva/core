package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.model.map.layout.graphics.LayerImage;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.graphics.LayerImageDao;
import lcsb.mapviewer.persist.dao.graphics.LayerImageProperty;
import lcsb.mapviewer.services.interfaces.ILayerImageService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.websockets.IWebSocketMessenger;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageLayerChildCreated;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageLayerChildDeleted;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageLayerChildUpdated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Transactional
@Service
public class LayerImageService implements ILayerImageService {

  private final IWebSocketMessenger webSocketMessenger;

  private final IModelService modelService;

  private final LayerImageDao layerImageDao;

  @Autowired
  public LayerImageService(final LayerImageDao layerImageDao,
                           final IWebSocketMessenger webSocketMessenger,
                           final IModelService modelService
  ) {
    this.layerImageDao = layerImageDao;
    this.webSocketMessenger = webSocketMessenger;
    this.modelService = modelService;
  }

  @Override
  public Page<LayerImage> getAll(final Map<LayerImageProperty, Object> filter, final Pageable pageable) {
    return layerImageDao.getAll(filter, pageable);
  }

  @Override
  public LayerImage getById(final int id) {
    return layerImageDao.getById(id);
  }

  @Override
  public void update(final LayerImage layerImage) {
    layerImageDao.update(layerImage);
    String projectId = layerImage.getLayer().getModel().getProject().getProjectId();
    webSocketMessenger.broadcast(projectId, new WebSocketMessageLayerChildUpdated(layerImage, projectId, layerImage.getLayer()));
  }

  @Override
  public void remove(final LayerImage layerImage) {
    layerImageDao.delete(layerImage);
    String projectId = layerImage.getLayer().getModel().getProject().getProjectId();
    webSocketMessenger.broadcast(projectId, new WebSocketMessageLayerChildDeleted(layerImage, projectId, layerImage.getLayer()));
  }

  @Override
  public long getCount(final Map<LayerImageProperty, Object> filterOptions) {
    return layerImageDao.getCount(filterOptions);
  }

  @Override
  public void add(final LayerImage layerImage) {
    layerImageDao.add(layerImage);
    ModelData model = modelService.getById(layerImage.getLayer().getModel().getId());
    String projectId = model.getProject().getProjectId();
    webSocketMessenger.broadcast(projectId, new WebSocketMessageLayerChildCreated(layerImage, projectId, layerImage.getLayer()));
  }

}
