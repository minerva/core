package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.graphics.LayerOvalDao;
import lcsb.mapviewer.persist.dao.graphics.LayerOvalProperty;
import lcsb.mapviewer.services.interfaces.ILayerOvalService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.websockets.IWebSocketMessenger;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageLayerChildCreated;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageLayerChildDeleted;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageLayerChildUpdated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Transactional
@Service
public class LayerOvalService implements ILayerOvalService {

  private final IWebSocketMessenger webSocketMessenger;

  private final IModelService modelService;

  private final LayerOvalDao layerOvalDao;

  @Autowired
  public LayerOvalService(final LayerOvalDao layerOvalDao, final IWebSocketMessenger webSocketMessenger, final IModelService modelService) {
    this.layerOvalDao = layerOvalDao;
    this.webSocketMessenger = webSocketMessenger;
    this.modelService = modelService;
  }

  @Override
  public Page<LayerOval> getAll(final Map<LayerOvalProperty, Object> filter, final Pageable pageable) {
    return layerOvalDao.getAll(filter, pageable);
  }

  @Override
  public LayerOval getById(final int id) {
    return layerOvalDao.getById(id);
  }

  @Override
  public void update(final LayerOval layerOval) {
    layerOvalDao.update(layerOval);
    String projectId = layerOval.getLayer().getModel().getProject().getProjectId();
    webSocketMessenger.broadcast(projectId, new WebSocketMessageLayerChildUpdated(layerOval, projectId, layerOval.getLayer()));
  }

  @Override
  public void remove(final LayerOval layerOval) {
    layerOvalDao.delete(layerOval);
    String projectId = layerOval.getLayer().getModel().getProject().getProjectId();
    webSocketMessenger.broadcast(projectId, new WebSocketMessageLayerChildDeleted(layerOval, projectId, layerOval.getLayer()));
  }

  @Override
  public long getCount(final Map<LayerOvalProperty, Object> filterOptions) {
    return layerOvalDao.getCount(filterOptions);
  }

  @Override
  public void add(final LayerOval layerOval) {
    layerOvalDao.add(layerOval);
    ModelData model = modelService.getById(layerOval.getLayer().getModel().getId());
    String projectId = model.getProject().getProjectId();
    webSocketMessenger.broadcast(projectId, new WebSocketMessageLayerChildCreated(layerOval, projectId, layerOval.getLayer()));
  }

}
