package lcsb.mapviewer.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.persist.dao.map.ReactionDao;
import lcsb.mapviewer.persist.dao.map.ReactionProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IReactionService;

@Transactional
@Service
public class ReactionService implements IReactionService {

  @Autowired
  private IModelService modelService;

  @Autowired
  private ReactionDao reactionDao;

  @Override
  public Reaction getById(final int id) {
    return reactionDao.getById(id);
  }

  @Override
  public Reaction getReactionById(final String projectId, final int mapId, final int elementId) throws ObjectNotFoundException {
    ModelData map = modelService.getModelByMapId(projectId, mapId);
    Map<ReactionProperty, Object> filter = new HashMap<>();
    filter.put(ReactionProperty.MAP, Arrays.asList(map));
    filter.put(ReactionProperty.ID, Arrays.asList(elementId));
    Page<Reaction> page = reactionDao.getAll(filter, Pageable.unpaged());
    if (page.getNumberOfElements() > 0) {
      return page.getContent().get(0);
    }
    return null;
  }

  @Override
  public List<Reaction> getReactionById(final String projectId, final String mapId, final Set<Integer> ids,
      final Set<Integer> participantIds) throws ObjectNotFoundException {
    List<ModelData> maps = modelService.getModelsByMapId(projectId, mapId);
    Map<ReactionProperty, Object> filter = new HashMap<>();
    filter.put(ReactionProperty.MAP, maps);
    if (ids.size() > 0) {
      filter.put(ReactionProperty.ID, new ArrayList<>(ids));
    }
    if (participantIds.size() > 0) {
      filter.put(ReactionProperty.PARTICIPANT_ID, new ArrayList<>(participantIds));
    }
    return reactionDao.getAll(filter, Pageable.unpaged()).getContent();
  }

  @Override
  public Map<MiriamType, Integer> getAnnotationStatistics(final String projectId) {
    Map<MiriamType, Integer> elementAnnotations = new TreeMap<>();
    Map<MiriamType, Integer> data = reactionDao.getAnnotationStatistics(projectId);
    for (final MiriamType mt : MiriamType.values()) {
      if (data.get(mt) != null) {
        elementAnnotations.put(mt, data.get(mt));
      } else {
        elementAnnotations.put(mt, 0);
      }
    }
    return elementAnnotations;
  }

  @Override
  public Page<Reaction> getAll(final Map<ReactionProperty, Object> filter, final Pageable pageable, final boolean initializeLazy) {
    Page<Reaction> result = getAll(filter, pageable);
    if (initializeLazy) {
      for (final Reaction reaction : result) {
        Hibernate.initialize(reaction.getModelData());
      }
    }
    return result;
  }

  @Override
  public Page<Reaction> getAll(final Map<ReactionProperty, Object> filter, final Pageable pageable) {
    return reactionDao.getAll(filter, pageable);
  }

  @Override
  public void update(final Reaction object) {
    reactionDao.update(object);
  }

  @Override
  public void remove(final Reaction object) {
    reactionDao.delete(object);
  }

  @Override
  public long getCount(final Map<ReactionProperty, Object> filterOptions) {
    return reactionDao.getCount(filterOptions);
  }

  @Override
  public void add(final Reaction object) {
    reactionDao.add(object);
  }

}
