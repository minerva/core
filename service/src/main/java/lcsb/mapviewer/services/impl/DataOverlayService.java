package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.TextFileUtils;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.ColorSchemaColumn;
import lcsb.mapviewer.converter.ColorSchemaReader;
import lcsb.mapviewer.converter.graphics.LegendGenerator;
import lcsb.mapviewer.converter.zip.ZipEntryFileFactory;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.FileEntry;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.model.map.model.ElementSubmodelConnection;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.overlay.DataOverlayType;
import lcsb.mapviewer.model.overlay.GeneVariant;
import lcsb.mapviewer.model.overlay.GeneVariantDataOverlayEntry;
import lcsb.mapviewer.model.overlay.GenericDataOverlayEntry;
import lcsb.mapviewer.model.overlay.InvalidDataOverlayException;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.map.DataOverlayDao;
import lcsb.mapviewer.persist.dao.map.DataOverlayProperty;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IDataOverlayService;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IModelService;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

@Transactional
@Service
public class DataOverlayService implements IDataOverlayService {

  private final Logger logger = LogManager.getLogger();

  @Autowired
  private DataOverlayDao dataOverlayDao;

  @Autowired
  private IModelService modelService;

  @Autowired
  private IElementService elementService;

  public DataOverlayService() {
  }

  @Override
  public DataOverlay createDataOverlay(final CreateDataOverlayParams params)
      throws IOException, InvalidDataOverlayException, ObjectNotFoundException {

    final ColorSchemaReader reader = new ColorSchemaReader();
    final Map<String, String> parameters = extractHeaderParameters(params.getColorInputStream(), params.getColorSchemaType());
    final DataOverlayType colorSchemaType = DataOverlayType
        .valueOf(parameters.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE));
    final Collection<DataOverlayEntry> schemas = reader.readColorSchema(params.getColorInputStream(), parameters);

    dataOverlayDao.flush();

    final DataOverlay overlay = new DataOverlay();
    overlay.setName(params.getName());
    overlay.setColorSchemaType(colorSchemaType);
    overlay.setPublic(false);
    UploadedFileEntry fileEntry = new UploadedFileEntry();
    fileEntry.setFileContent(IOUtils.toByteArray(params.getColorInputStream()));
    fileEntry.setOriginalFileName(params.getFileName());
    fileEntry.setLength(fileEntry.getFileContent().length);
    fileEntry.setOwner(params.getUser());
    overlay.setInputData(fileEntry);
    overlay.setDescription(params.getDescription());
    overlay.setProject(params.getProject());
    overlay.setCreator(params.getUser());
    overlay.addEntries(schemas);

    if (colorSchemaType == DataOverlayType.GENETIC_VARIANT) {
      final String genomeVersion = parameters.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_GENOME_VERSION);
      ReferenceGenomeType genomeType = ReferenceGenomeType.UCSC;
      if (parameters.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_GENOME_TYPE) != null) {
        genomeType = ReferenceGenomeType
            .valueOf(parameters.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_GENOME_TYPE));
      }
      overlay.setGenomeType(genomeType);
      overlay.setGenomeVersion(genomeVersion);
    }
    dataOverlayDao.add(overlay);

    logger.info("Data overlay " + overlay.getId() + " created successfully");
    Hibernate.initialize(overlay.getCreator());
    return overlay;
  }

  protected Map<String, String> extractHeaderParameters(final InputStream colorInputStream, final DataOverlayType suggestedOverlayType)
      throws IOException {
    final Map<String, String> parameters = TextFileUtils.getHeaderParametersFromFile(colorInputStream);
    DataOverlayType overlayType = suggestedOverlayType;
    if (overlayType == null) {
      final String type = parameters.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE);
      if (type != null) {
        overlayType = DataOverlayType.valueOf(type);
      } else {
        overlayType = DataOverlayType.GENERIC;
      }
    }

    if (parameters.get(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE) == null) {
      parameters.put(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE, overlayType.name());
    }
    return parameters;
  }

  /**
   * Prepares table with statistics about coloring.
   *
   * @param matches schemas that were used for coloring
   * @param scr     interface that returns list of columns that should be printed
   * @return table with statistics about coloring
   */
  protected String prepareTableResult(final Map<DataOverlayEntry, Integer> matches, final ColorSchemaReader scr) {
    final StringBuilder sb = new StringBuilder();

    final Collection<ColorSchemaColumn> columns = scr.getSetColorSchemaColumns(matches.keySet());

    for (final ColorSchemaColumn column : ColorSchemaColumn.values()) {
      if (columns.contains(column)) {
        sb.append(column.getColumnName())
            .append("\t");
      }
    }
    sb.append("matches<br/>\n");

    for (final DataOverlayEntry originalSchema : matches.keySet()) {
      for (final DataOverlayEntry schema : splitColorSchema(originalSchema)) {
        sb.append(prepareTableRow(columns, schema, matches.get(originalSchema)));
      }
    }

    return sb.toString();
  }

  /**
   * {@link DataOverlayEntry} sometimes contains merged value from few rows. This
   * method split single {@link DataOverlayEntry} object to simple flat objects
   * that can be serialized in a simple tab separated file.
   *
   * @param originalSchema original {@link DataOverlayEntry} object that we want to split into
   *                       flat objects
   * @return {@link List} of flat schema objects obtained from input
   */
  private List<DataOverlayEntry> splitColorSchema(final DataOverlayEntry originalSchema) {
    final List<DataOverlayEntry> result = new ArrayList<>();
    if (originalSchema instanceof GenericDataOverlayEntry) {
      result.add(originalSchema);
    } else if (originalSchema instanceof GeneVariantDataOverlayEntry) {
      for (final GeneVariant gv : ((GeneVariantDataOverlayEntry) originalSchema).getGeneVariants()) {
        final GeneVariantDataOverlayEntry copy = (GeneVariantDataOverlayEntry) originalSchema.copy();
        copy.getGeneVariants().clear();
        copy.addGeneVariant(gv.copy());
        result.add(copy);
      }
    } else {
      throw new InvalidArgumentException("Unknown class type: " + originalSchema.getClass());
    }
    return result;
  }

  /**
   * Prepares tab separated {@link String} representation of
   * {@link DataOverlayEntry}.
   *
   * @param columns columns that should be output in the result String
   * @param schema  input DataOverlayEntry that is going to be transformed into String
   *                representation
   */
  protected String prepareTableRow(final Collection<ColorSchemaColumn> columns, final DataOverlayEntry schema, final int match) {
    final StringBuilder sb = new StringBuilder();
    for (final ColorSchemaColumn column : ColorSchemaColumn.values()) {
      if (columns.contains(column)) {
        if (schema instanceof GenericDataOverlayEntry) {
          sb.append(prepareTableCellForGenericSchema((GenericDataOverlayEntry) schema, column));
        } else if (schema instanceof GeneVariantDataOverlayEntry) {
          sb.append(prepareTableCellForGeneVariationSchema((GeneVariantDataOverlayEntry) schema, column));
        } else {
          throw new InvalidArgumentException("Unknown schema type: " + schema.getClass());
        }
      }
    }
    sb.append(match + "").append("<br/>\n");
    return sb.toString();
  }

  /**
   * Returns String representing data of {@link GenericDataOverlayEntry} that
   * should appear in a given {@link ColorSchemaColumn}.
   *
   * @param schema object for which data will be returned
   * @param column column for which data should be returned
   */
  protected String prepareTableCellForGenericSchema(final GenericDataOverlayEntry schema, final ColorSchemaColumn column) {
    final StringBuilder sb = new StringBuilder();
    if (column.equals(ColorSchemaColumn.COLOR)) {
      if (schema.getColor() != null) {
        sb.append("#" + Integer.toHexString(schema.getColor().getRGB()).substring(2).toUpperCase());
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.NAME)) {
      sb.append(schema.getName() + "\t");
    } else if (column.equals(ColorSchemaColumn.MAP_NAME)) {
      sb.append(schema.getModelName() + "\t");
    } else if (column.equals(ColorSchemaColumn.VALUE)) {
      sb.append(schema.getValue() + "\t");
    } else if (column.equals(ColorSchemaColumn.COMPARTMENT)) {
      for (final String str : schema.getCompartments()) {
        sb.append(str + ", ");
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.TYPE)) {
      for (final Class<? extends BioEntity> str : schema.getTypes()) {
        sb.append(str.getSimpleName() + ", ");
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.IDENTIFIER)) {
      for (final MiriamData md : schema.getMiriamData()) {
        sb.append(md.getDataType().getCommonName() + ": " + md.getResource() + ", ");
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.ELEMENT_IDENTIFIER)) {
      sb.append(schema.getElementId() + "\t");
    } else if (column.equals(ColorSchemaColumn.LINE_WIDTH)) {
      sb.append(schema.getLineWidth() + "\t");
    } else if (column.equals(ColorSchemaColumn.REVERSE_REACTION)) {
      sb.append(schema.getReverseReaction() + "\t");
    } else if (column.equals(ColorSchemaColumn.POSITION)) {
      sb.append(schema.getReverseReaction() + "\t");
    } else if (column.equals(ColorSchemaColumn.DESCRIPTION)) {
      sb.append(schema.getDescription() + "\t");
    } else {
      throw new InvalidArgumentException("Unknown column type: " + column + " for schema type: " + schema.getClass());
    }
    return sb.toString();
  }

  /**
   * Returns String representing data of {@link GeneVariantDataOverlayEntry} that
   * should appear in a given {@link ColorSchemaColumn}.
   *
   * @param schema object for which data will be returned
   * @param column column for which data should be returned
   */
  protected String prepareTableCellForGeneVariationSchema(final GeneVariantDataOverlayEntry schema,
                                                          final ColorSchemaColumn column) {
    final GeneVariant gv = schema.getGeneVariants().iterator().next();
    final StringBuilder sb = new StringBuilder();
    if (column.equals(ColorSchemaColumn.COLOR)) {
      if (schema.getColor() != null) {
        sb.append("#" + Integer.toHexString(schema.getColor().getRGB()).substring(2).toUpperCase());
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.NAME)) {
      sb.append(schema.getName() + "\t");
    } else if (column.equals(ColorSchemaColumn.MAP_NAME)) {
      sb.append(schema.getModelName() + "\t");
    } else if (column.equals(ColorSchemaColumn.VALUE)) {
      sb.append(schema.getValue() + "\t");
    } else if (column.equals(ColorSchemaColumn.COMPARTMENT)) {
      for (final String str : schema.getCompartments()) {
        sb.append(str + ", ");
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.TYPE)) {
      for (final Class<? extends BioEntity> str : schema.getTypes()) {
        sb.append(str.getSimpleName() + ", ");
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.IDENTIFIER)) {
      for (final MiriamData md : schema.getMiriamData()) {
        sb.append(md.getDataType().getCommonName() + ": " + md.getResource() + ", ");
      }
      sb.append("\t");
    } else if (column.equals(ColorSchemaColumn.ELEMENT_IDENTIFIER)) {
      sb.append(schema.getElementId() + "\t");
    } else if (column.equals(ColorSchemaColumn.LINE_WIDTH)) {
      sb.append(schema.getLineWidth() + "\t");
    } else if (column.equals(ColorSchemaColumn.REVERSE_REACTION)) {
      sb.append(schema.getReverseReaction() + "\t");
    } else if (column.equals(ColorSchemaColumn.POSITION)) {
      sb.append(gv.getPosition() + "\t");
    } else if (column.equals(ColorSchemaColumn.DESCRIPTION)) {
      sb.append(schema.getDescription() + "\t");
    } else if (column.equals(ColorSchemaColumn.ORIGINAL_DNA)) {
      sb.append(gv.getOriginalDna() + "\t");
    } else if (column.equals(ColorSchemaColumn.ALTERNATIVE_DNA)) {
      sb.append(gv.getModifiedDna() + "\t");
    } else if (column.equals(ColorSchemaColumn.CONTIG)) {
      sb.append(gv.getContig() + "\t");
    } else {
      throw new InvalidArgumentException("Unknown column type: " + column + " for schema type: " + schema.getClass());
    }
    return sb.toString();
  }

  @Override
  public List<Pair<Element, DataOverlayEntry>> getElementReferencesForDataOverlay(final int overlayId, final List<ModelData> models) {
    return dataOverlayDao.getElementReferencesForModels(overlayId, models);
  }

  private Consumer<Pair<? extends BioEntity, DataOverlayEntry>> initializeLazy() {
    return new Consumer<Pair<? extends BioEntity, DataOverlayEntry>>() {
      @Override
      public void accept(final Pair<? extends BioEntity, DataOverlayEntry> arg0) {
        if (arg0.getRight() instanceof GeneVariantDataOverlayEntry) {
          Hibernate.initialize(((GeneVariantDataOverlayEntry) arg0.getRight()).getGeneVariants());
        }
        Hibernate.initialize(arg0.getLeft().getModelData());
        if (arg0.getLeft() instanceof Element) {
          final Element element = (Element) arg0.getLeft();
          if (element.getSubmodel() != null) {
            Hibernate.initialize(element.getSubmodel().getSubmodel());
          }
          Hibernate.initialize(element.getGlyph());
        }
      }
    };
  }

  @Override
  public List<Pair<Reaction, DataOverlayEntry>> getReactionReferencesForDataOverlay(final int overlayId, final List<ModelData> models) {
    return dataOverlayDao.getReactionReferencesForModels(overlayId, models);
  }

  private void getOverlayByIdNotNull(final String projectId, final int overlayId) throws ObjectNotFoundException {
    final DataOverlay overlay = dataOverlayDao.getById(overlayId);
    if (overlay == null || !overlay.getProject().getProjectId().equals(projectId)) {
      throw new ObjectNotFoundException("Overlay does not exist");
    }
  }

  @Override
  public List<Pair<? extends BioEntity, DataOverlayEntry>> getBioEntitiesForDataOverlay(
      final String projectId,
      final int mapId,
      final Integer overlayId,
      final boolean fetchLazy,
      final boolean includeIndirect) throws ObjectNotFoundException {
    final ModelData map = modelService.getModelByMapId(projectId, mapId);
    getOverlayByIdNotNull(projectId, overlayId);

    final List<Pair<? extends BioEntity, DataOverlayEntry>> result = new ArrayList<>();
    result.addAll(dataOverlayDao.getReactionsForModels(overlayId, Collections.singletonList(map)));
    result.addAll(dataOverlayDao.getElementsForModels(overlayId, Collections.singletonList(map)));
    if (fetchLazy) {
      for (int i = 0; i < result.size(); i++) {
        final Pair<? extends BioEntity, DataOverlayEntry> pair = result.get(i);
        if (pair.getLeft() instanceof HibernateProxy) {
          result.set(i, new Pair<BioEntity, DataOverlayEntry>((BioEntity) Hibernate.unproxy(pair.getLeft()), pair.getRight()));
        }
      }
    }
    if (includeIndirect) {
      final Map<ElementProperty, Object> properties = new HashMap<>();
      properties.put(ElementProperty.MAP, Collections.singletonList(map));
      properties.put(ElementProperty.SUBMAP_CONNECTION, Collections.singletonList(Boolean.TRUE));

      final List<Element> elements = elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent();

      for (final Element element : elements) {
        final ElementSubmodelConnection connection = element.getSubmodel();
        final List<Pair<? extends BioEntity, DataOverlayEntry>> listForSubmap = new ArrayList<>();
        listForSubmap.addAll(dataOverlayDao.getElementsForModels(overlayId, Collections.singletonList(connection.getSubmodel())));
        listForSubmap.addAll(dataOverlayDao.getReactionsForModels(overlayId, Collections.singletonList(connection.getSubmodel())));

        for (final Pair<? extends BioEntity, DataOverlayEntry> pair : listForSubmap) {
          result.add(new Pair<BioEntity, DataOverlayEntry>(element, pair.getRight()));
        }
      }
    }
    result.forEach(initializeLazy());
    return result;
  }

  @Override
  public List<Pair<? extends BioEntity, DataOverlayEntry>> getBioEntitiesForDataOverlay(
      final String projectId,
      final int mapId,
      final Integer overlayId,
      final boolean fetchLazy) throws ObjectNotFoundException {
    return getBioEntitiesForDataOverlay(projectId, mapId, overlayId, fetchLazy, false);
  }

  @Override
  public List<Pair<Element, DataOverlayEntry>> getFullElementForDataOverlay(
      final String projectId,
      final int mapId,
      final Integer id,
      final int overlayId,
      final boolean fetchLazy) throws ObjectNotFoundException {
    final ModelData map = modelService.getModelByMapId(projectId, mapId);
    getOverlayByIdNotNull(projectId, overlayId);

    final List<Pair<Element, DataOverlayEntry>> result =
        dataOverlayDao.getElementsForModels(overlayId, Collections.singletonList(map), Collections.singletonList(id));
    if (fetchLazy) {
      result.stream().forEach(initializeLazy());
    }
    return result;
  }

  @Override
  public List<Pair<Reaction, DataOverlayEntry>> getFullReactionForDataOverlay(
      final String projectId,
      final int mapId,
      final Integer id,
      final int overlayId,
      final boolean fetchLazy) throws ObjectNotFoundException {
    final ModelData map = modelService.getModelByMapId(projectId, mapId);
    getOverlayByIdNotNull(projectId, overlayId);

    final List<Pair<Reaction, DataOverlayEntry>> result =
        dataOverlayDao.getReactionsForModels(overlayId, Collections.singletonList(map), Collections.singletonList(id));
    if (fetchLazy) {
      result.stream().forEach(initializeLazy());
    }
    return result;
  }

  @Override
  public DataOverlay getDataOverlayById(final Integer overlayId) {
    return dataOverlayDao.getById(overlayId);
  }

  @Override
  public DataOverlay getDataOverlayById(final String projectId, final Integer overlayId) throws ObjectNotFoundException {
    final DataOverlay result = getDataOverlayById(overlayId);
    if (result != null && result.getProject().getProjectId().equals(projectId)) {
      Hibernate.initialize(result.getCreator());
      return result;
    }
    throw new ObjectNotFoundException("Overlay with given id does not exist: " + overlayId);
  }

  @Override
  public List<DataOverlay> getDataOverlaysByProject(final Project project) {
    return dataOverlayDao.getByProject(project);
  }

  @Override
  public List<DataOverlay> getDataOverlaysByProject(final Project project, final boolean loadLazy) {
    final List<DataOverlay> result = getDataOverlaysByProject(project);
    if (loadLazy) {
      for (final DataOverlay dataOverlay : result) {
        Hibernate.initialize(dataOverlay.getCreator());
      }
    }
    return result;
  }

  @Override
  public void removeDataOverlay(final DataOverlay dataOverlay) {
    dataOverlayDao.delete(dataOverlay);
  }

  @Override
  public void updateDataOverlay(final DataOverlay dataOverlay) {
    dataOverlayDao.update(dataOverlay);
  }

  @Override
  public User getOverlayCreator(final Integer overlayId) {
    final DataOverlay overlay = dataOverlayDao.getById(overlayId);
    if (overlay != null) {
      Hibernate.initialize(overlay.getCreator());
      return overlay.getCreator();
    }
    return null;
  }

  @Override
  public Page<DataOverlay> getAll(final Map<DataOverlayProperty, Object> filter, final Pageable pageable) {
    final Page<DataOverlay> result = dataOverlayDao.getAll(filter, pageable);
    for (final DataOverlay dataOverlay : result.getContent()) {
      Hibernate.initialize(dataOverlay.getCreator());
    }
    return result;
  }

  @Override
  public DataOverlay getById(final int id) {
    final DataOverlay overlay = dataOverlayDao.getById(id);
    if (overlay != null) {
      Hibernate.initialize(overlay.getCreator());
    }
    return overlay;
  }

  @Override
  public void update(final DataOverlay object) {
    dataOverlayDao.update(object);
  }

  @Override
  public void remove(final DataOverlay object) {
    dataOverlayDao.delete(object);
  }

  @Override
  public long getCount(final Map<DataOverlayProperty, Object> filterOptions) {
    return dataOverlayDao.getCount(filterOptions);
  }

  @Override
  public void add(final DataOverlay overlay) {
    dataOverlayDao.add(overlay);
  }

  @Override
  public FileEntry getDataOverlayFileById(final String projectId, final Integer overlayId) throws ObjectNotFoundException {
    final DataOverlay overlay = getDataOverlayById(projectId, overlayId);
    if (overlay != null && overlay.getInputData() != null) {
      Hibernate.initialize(overlay.getInputData().getFileContent());
      return overlay.getInputData();
    }
    return null;
  }

  @Override
  public byte[] generateLegend(final String projectId, final Integer overlayId, final ColorExtractor colorExtractor)
      throws ObjectNotFoundException, IOException {
    final DataOverlay overlay = getDataOverlayById(projectId, overlayId);
    return new LegendGenerator().generate(overlay, colorExtractor);
  }

  @Override
  public Set<DataOverlayEntry> getDataOverlayEntriesById(final String projectId, final Integer overlayId) throws ObjectNotFoundException {
    final Set<DataOverlayEntry> result = getDataOverlayById(projectId, overlayId).getEntries();
    Hibernate.initialize(result);
    for (final DataOverlayEntry dataOverlayEntry : result) {
      Hibernate.initialize(dataOverlayEntry.getTypes());
      Hibernate.initialize(dataOverlayEntry.getMiriamData());
      Hibernate.initialize(dataOverlayEntry.getCompartments());
    }
    return result;
  }
}
