package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.common.Md5;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.plugin.Plugin;
import lcsb.mapviewer.model.plugin.PluginDataEntry;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.plugin.PluginDao;
import lcsb.mapviewer.persist.dao.plugin.PluginDataEntryDao;
import lcsb.mapviewer.persist.dao.plugin.PluginProperty;
import lcsb.mapviewer.services.interfaces.IPluginService;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class PluginService implements IPluginService {

  private final PluginDao pluginDao;
  private final PluginDataEntryDao pluginDataEntryDao;

  @Autowired
  public PluginService(final PluginDao pluginDao, final PluginDataEntryDao pluginDataEntryDao) {
    this.pluginDao = pluginDao;
    this.pluginDataEntryDao = pluginDataEntryDao;
  }

  @Override
  public Plugin getByHash(final String hash) {
    Plugin result = pluginDao.getByHash(hash);
    if (result != null) {
      Hibernate.initialize(result.getUrls());
    }
    return result;
  }

  @Override
  public List<Plugin> getAll() {
    List<Plugin> result = pluginDao.getAll();
    for (final Plugin plugin : result) {
      Hibernate.initialize(plugin.getUrls());
    }
    return result;
  }

  @Override
  public Page<Plugin> getAll(final Map<PluginProperty, Object> filter, final Pageable pageable) {
    return pluginDao.getAll(filter, pageable);
  }


  @Override
  public PluginDataEntry getEntryByKey(final Plugin plugin, final String key, final User user) {
    PluginDataEntry result = pluginDataEntryDao.getByKey(plugin, key, user);
    if (result != null) {
      Hibernate.initialize(result.getUser());
    }
    return result;
  }

  @Override
  public void delete(final PluginDataEntry entry) {
    pluginDataEntryDao.delete(entry);
  }

  @Override
  public void delete(final Plugin plugin) {
    List<PluginDataEntry> entries = pluginDataEntryDao.getByPlugin(plugin);
    for (final PluginDataEntry pluginDataEntry : entries) {
      pluginDataEntryDao.delete(pluginDataEntry);
    }
    pluginDao.delete(plugin);
  }

  @Override
  public void add(final Plugin plugin) {
    pluginDao.add(plugin);
  }

  @Override
  public void add(final PluginDataEntry entry) {
    pluginDataEntryDao.add(entry);
  }

  @Override
  public void update(final PluginDataEntry entry) {
    pluginDataEntryDao.update(entry);
  }

  @Override
  public void update(final Plugin plugin) {
    Plugin oldPlugin = pluginDao.getByHash(plugin.getHash());
    if (oldPlugin != null && oldPlugin.getId() != plugin.getId()) {
      delete(oldPlugin);
    } else if (oldPlugin != null) {
      pluginDao.evict(oldPlugin);
    }

    pluginDao.update(plugin);
  }

  @Override
  public String getExpectedHash(final String url) {
    try {
      return Md5.compute(new WebPageDownloader().getFromNetwork(url));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }


  @Override
  public Plugin getById(final int id) {
    throw new NotImplementedException();
  }


  @Override
  public void remove(final Plugin object) {
    throw new NotImplementedException();
  }

  @Override
  public long getCount(final Map<PluginProperty, Object> filterOptions) {
    throw new NotImplementedException();
  }

}
