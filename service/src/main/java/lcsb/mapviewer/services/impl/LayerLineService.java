package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.graphics.PolylineDao;
import lcsb.mapviewer.persist.dao.graphics.PolylineDataProperty;
import lcsb.mapviewer.services.interfaces.ILayerLineService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.websockets.IWebSocketMessenger;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageLayerChildCreated;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageLayerChildDeleted;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageLayerChildUpdated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Transactional
@Service
public class LayerLineService implements ILayerLineService {

  private final IWebSocketMessenger webSocketMessenger;

  private final IModelService modelService;

  private final PolylineDao layerLineDao;

  @Autowired
  public LayerLineService(final PolylineDao layerLineDao, final IWebSocketMessenger webSocketMessenger, final IModelService modelService) {
    this.layerLineDao = layerLineDao;
    this.webSocketMessenger = webSocketMessenger;
    this.modelService = modelService;
  }

  @Override
  public Page<PolylineData> getAll(final Map<PolylineDataProperty, Object> filter, final Pageable pageable) {
    return layerLineDao.getAll(filter, pageable);
  }

  @Override
  public PolylineData getById(final int id) {
    return layerLineDao.getById(id);
  }

  @Override
  public void update(final PolylineData layerLine) {
    layerLineDao.update(layerLine);
    String projectId = layerLine.getLayer().getModel().getProject().getProjectId();
    webSocketMessenger.broadcast(projectId, new WebSocketMessageLayerChildUpdated(layerLine, projectId, layerLine.getLayer()));
  }

  @Override
  public void remove(final PolylineData layerLine) {
    layerLineDao.delete(layerLine);
    String projectId = layerLine.getLayer().getModel().getProject().getProjectId();
    webSocketMessenger.broadcast(projectId, new WebSocketMessageLayerChildDeleted(layerLine, projectId, layerLine.getLayer()));
  }

  @Override
  public long getCount(final Map<PolylineDataProperty, Object> filterOptions) {
    return layerLineDao.getCount(filterOptions);
  }

  @Override
  public void add(final PolylineData object) {
    layerLineDao.add(object);
    ModelData model = modelService.getById(object.getLayer().getModel().getId());
    String projectId = model.getProject().getProjectId();
    Integer modelId = model.getId();
    webSocketMessenger.broadcast(projectId, new WebSocketMessageLayerChildCreated(object, projectId, object.getLayer()));
  }

}
