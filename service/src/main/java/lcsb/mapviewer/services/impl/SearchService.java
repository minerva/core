package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.persist.dao.map.BioEntityProperty;
import lcsb.mapviewer.persist.dao.map.ModelProperty;
import lcsb.mapviewer.persist.dao.map.ReactionDao;
import lcsb.mapviewer.persist.dao.map.ReactionProperty;
import lcsb.mapviewer.persist.dao.map.species.ElementDao;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.ISearchService;
import lcsb.mapviewer.services.utils.BioEntitySearchResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Hibernate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is implementation of the service that returns information about species,
 * paths etc. in the model.
 *
 * @author Piotr Gawron
 */
@Transactional
@Service
public class SearchService implements ISearchService {

  private static final Logger logger = LogManager.getLogger();

  private final IModelService modelService;

  private final IProjectService projectService;

  private final ElementDao elementDao;

  private final ReactionDao reactionDao;

  /**
   * This object maps short string prefixes into class of elements on the map that
   * will be searched. For instance prefix "complex" might refer to
   * {@link Complex} class and an example query would look like: "complex:alpha
   * subcomplex".
   */
  private final Map<String, List<Class<? extends BioEntity>>> speciesSearchReversePrefix = new HashMap<>();

  /**
   * Default constructor that set list of prefixes used in the search queries.
   */
  public SearchService(final IModelService modelService, final ElementDao elementDao, final ReactionDao reactionDao,
                       final IProjectService projectService) {
    this.modelService = modelService;
    this.projectService = projectService;
    this.elementDao = elementDao;
    this.reactionDao = reactionDao;
    addSearchPrefix("complex", Collections.singletonList(Complex.class));
    addSearchPrefix("degraded", Collections.singletonList(Degraded.class));
    addSearchPrefix("drug", Collections.singletonList(Drug.class));
    addSearchPrefix("gene", Collections.singletonList(Gene.class));
    addSearchPrefix("ion", Collections.singletonList(Ion.class));
    addSearchPrefix("phenotype", Collections.singletonList(Phenotype.class));
    addSearchPrefix("protein", new ArrayList<>(new ElementUtils().getClassesByStringTypes("Protein")));
    addSearchPrefix("rna", Collections.singletonList(Rna.class));
    addSearchPrefix("molecule", Collections.singletonList(SimpleMolecule.class));
    addSearchPrefix("unknown", Collections.singletonList(Unknown.class));
    addSearchPrefix("element", new ArrayList<>(new ElementUtils().getAvailableElementSubclasses()));
    addSearchPrefix("reaction", new ArrayList<>(new ElementUtils().getAvailableReactionSubclasses()));
  }

  /**
   * Adds search prefix for an element class.
   *
   * @param prefix string prefix used in search queries
   * @param clazz  class for which this prefix stands
   */
  private void addSearchPrefix(final String prefix, final List<Class<? extends BioEntity>> clazz) {
    speciesSearchReversePrefix.put(prefix, clazz);
  }

  /**
   * Transform {@link Reaction} into set of result entries.
   *
   * @param reaction reaction to be transformed
   * @return list of result entries for all element of the {@link Reaction}
   */
  private List<BioEntity> reactionToResultList(final Reaction reaction) {
    final List<BioEntity> result = new ArrayList<>();
    result.add(reaction);
    for (final ReactionNode node : reaction.getReactionNodes()) {
      result.add(node.getElement());
      Hibernate.initialize(node.getElement());
    }
    return result;
  }

  @Override
  public List<BioEntity> searchByQuery(final String projectId, final String mapId, final String originalQuery, final int limit,
                                       final boolean perfectMatch, final String ipAddress) throws ObjectNotFoundException {
    final String query = originalQuery.toLowerCase();

    final List<ModelData> maps = modelService.getModelsByMapId(projectId, mapId);

    final MiriamData mt = getMiriamTypeForQuery(query);
    final List<BioEntity> result = new ArrayList<>();

    final Pageable pageable = PageRequest.of(0, limit);
    final Map<ReactionProperty, Object> reactionFilter = new HashMap<>();
    if (!mapId.equals("*")) {
      reactionFilter.put(ReactionProperty.MAP_ID, Collections.singletonList(Integer.valueOf(mapId)));
    }
    reactionFilter.put(ReactionProperty.PROJECT_ID, Collections.singletonList(projectId));

    final Map<ElementProperty, Object> elementFilter = new HashMap<>();
    elementFilter.put(ElementProperty.MAP, maps);

    if (mt != null) {
      elementFilter.put(ElementProperty.ANNOTATION, Collections.singletonList(mt));

      result.addAll(elementDao.getAll(elementFilter, pageable).getContent());

      reactionFilter.put(ReactionProperty.ANNOTATION, Collections.singletonList(mt));

      for (final Reaction reaction : reactionDao.getAll(reactionFilter, pageable)) {
        result.addAll(reactionToResultList(reaction));
      }
    } else {
      final String indexQuery = getQueryStringForIndex(query);
      final List<Class<? extends BioEntity>> type = getTypeForQuery(query);
      if (perfectMatch) {
        elementFilter.put(ElementProperty.PERFECT_TEXT, Collections.singletonList(indexQuery));
        reactionFilter.put(ReactionProperty.PERFECT_TEXT, Collections.singletonList(indexQuery));
      } else {
        elementFilter.put(ElementProperty.TEXT, Collections.singletonList(indexQuery));
        reactionFilter.put(ReactionProperty.TEXT, Collections.singletonList(indexQuery));
      }
      if (!type.isEmpty()) {
        elementFilter.put(ElementProperty.CLASS, type);
        reactionFilter.put(ReactionProperty.CLASS, type);
      }
      result.addAll(elementDao.getAll(elementFilter, pageable).getContent());

      for (final Reaction reaction : reactionDao.getAll(reactionFilter, pageable)) {
        result.addAll(reactionToResultList(reaction));
      }
    }
    return result;
  }

  @Override
  public List<BioEntity> searchByQuery(final String projectId, final String mapId, final String query, final int limit, final boolean perfectMatch)
      throws ObjectNotFoundException {
    return searchByQuery(projectId, mapId, query, limit, perfectMatch, null);
  }

  /**
   * This method transform a string into indexed query. AvailablePrefixes contains
   * a collection of all possible prefixes for query string that should be
   * omitted.
   *
   * @param originalQuery query to be transformed
   * @return indexed string of the original query
   */
  private String getQueryStringForIndex(final String originalQuery) {
    String result = originalQuery;
    for (final String string : speciesSearchReversePrefix.keySet()) {
      if (result.startsWith(string + ":")) {
        result = result.replaceFirst(string + ":", "");
        break;
      }
    }

    return getIndexStringForString(result);
  }

  private String getIndexStringForString(final String str) {
    return str.toLowerCase().replaceAll("[^a-z0-9]", "");
  }

  /**
   * Return type of elements that can be returned for the query.
   *
   * @param string query string.
   * @return class of the element that might exist in the result
   */
  private List<Class<? extends BioEntity>> getTypeForQuery(final String string) {
    final String type = string.split(":")[0];
    List<Class<? extends BioEntity>> result = speciesSearchReversePrefix.get(type);
    if (result == null) {
      result = new ArrayList<>();
      result.addAll(new ElementUtils().getAvailableElementSubclasses());
      result.addAll(new ElementUtils().getAvailableReactionSubclasses());
    }
    return result;
  }

  @Override
  public List<BioEntity> getClosestElements(final String projectId, final int mapId, final Point2D point, final int numberOfElements,
                                            final boolean perfectHit,
                                            final Collection<String> types) throws ObjectNotFoundException {
    final List<BioEntity> result = new ArrayList<>();

    final List<DistanceToObject> tmpList = new ArrayList<>();
    final ModelData map = modelService.getModelByMapId(projectId, mapId);
    final List<Class<? extends BioEntity>> classes = new ElementUtils().getClassesByStringTypes(types);

    for (final Pair<Reaction, Double> pair : reactionDao.getByCoordinatesWithDistance(map, point, numberOfElements, classes)) {
      tmpList.add(new DistanceToObject(pair));
    }
    for (final Element element : elementDao.getByCoordinates(map, point, numberOfElements, classes)) {
      tmpList.add(new DistanceToObject(element, point));
    }
    Collections.sort(tmpList);
    final int size = Math.min(tmpList.size(), numberOfElements);
    for (int i = 0; i < size; i++) {
      if (!perfectHit) {
        result.add(tmpList.get(i).getReference());
      } else if (tmpList.get(i).getDistance() < Configuration.EPSILON) {
        result.add(tmpList.get(i).getReference());
      }
    }
    for (final BioEntity bioEntity : result) {
      Hibernate.initialize(bioEntity);
    }
    return result;
  }

  @Override
  public List<String> getSuggestedQueryList(final String projectId) throws ObjectNotFoundException {
    return this.getSuggestedQueryList(projectId, "*");
  }

  @Override
  public List<String> getSuggestedQueryList(final String projectId, final String mapId) throws ObjectNotFoundException {
    return elementDao.getSuggestedQueries(modelService.getModelsByMapId(projectId, mapId));
  }

  /**
   * Tries to transform query into {@link MiriamData}.
   *
   * @param string query to transform
   * @return {@link MiriamData} that described query or null if query cannot be converted
   */
  protected MiriamData getMiriamTypeForQuery(final String string) {
    for (final MiriamType mt : MiriamType.values()) {
      if (string.startsWith(mt.toString().toLowerCase() + ":")) {
        return new MiriamData(mt, string.substring(mt.toString().length() + 1));
      }
    }
    return null;
  }

  /**
   * This class represents distance between object and some point. It's designed
   * to help sort objects by their distance to some point. It contains two
   * fields: object reference and distance.
   *
   * @author Piotr Gawron
   */
  private static class DistanceToObject implements Comparable<DistanceToObject> {
    /**
     * Reference to the object.
     */
    private final BioEntity reference;

    /**
     * Distance between the object and some point.
     */
    private final double distance;

    /**
     * Constructor for alias objects.
     *
     * @param alias alias reference to store
     * @param point point from which the distance will be computed
     */
    DistanceToObject(final Element alias, final Point2D point) {
      reference = alias;
      distance = alias.getDistanceFromPoint(point);
    }

    public DistanceToObject(final Pair<? extends BioEntity, Double> pair) {
      reference = pair.getLeft();
      distance = pair.getRight();
    }

    @Override
    public int compareTo(final DistanceToObject arg0) {
      if (arg0.getDistance() < getDistance()) {
        return 1;
      } else if (arg0.getDistance() > getDistance()) {
        return -1;
      } else {
        return arg0.getReference().getZ().compareTo(getReference().getZ());
      }
    }

    /**
     * @return the reference
     * @see #reference
     */
    public BioEntity getReference() {
      return reference;
    }

    /**
     * @return the distance
     * @see #distance
     */
    public double getDistance() {
      return distance;
    }

  }

  @Override
  public Page<BioEntitySearchResult> getBioEntitesByFilter(final Map<BioEntityProperty, Object> properties, final Pageable pageable,
                                                           final boolean perfectMatch) throws ObjectNotFoundException {
    final String originalQuery = (String) properties.get(BioEntityProperty.TEXT);
    if (originalQuery != null) {
      properties.remove(BioEntityProperty.TEXT);

      final String query = originalQuery.toLowerCase();

      final MiriamData mt = getMiriamTypeForQuery(query);

      if (mt != null) {
        properties.put(BioEntityProperty.ANNOTATION, mt);
      } else {
        final String indexQuery = getQueryStringForIndex(query);
        final List<Class<? extends BioEntity>> type = getTypeForQuery(query);
        if (perfectMatch) {
          properties.put(BioEntityProperty.PERFECT_TEXT, indexQuery);
        } else {
          properties.put(BioEntityProperty.TEXT, indexQuery);
        }
        properties.put(BioEntityProperty.CLASS, type);
      }
    }

    final Map<ElementProperty, Object> elementFilter = convertBioEntityFilterToElementFilter(properties);
    final Map<ReactionProperty, Object> reactionFilter = convertBioEntityFilterToReactionFilter(properties);

    final Page<Element> elementPage = elementDao.getAll(elementFilter, pageable);
    for (final Element element : elementPage.getContent()) {
      if (element.getSubmodel() != null) {
        Hibernate.initialize(element.getSubmodel().getSubmodel());
      }
      Hibernate.initialize(element.getGlyph());
      Hibernate.initialize(element.getModel());
    }

    final Page<Reaction> reactionPage;
    final List<BioEntity> bioEntities = new ArrayList<>(elementPage.getContent());
    if (elementPage.getNumberOfElements() < pageable.getPageSize()) {
      if (elementPage.getNumberOfElements() > 0) {
        final int count = pageable.getPageSize() - elementPage.getNumberOfElements();
        final Pageable reactionPageable = PageRequest.of(0, count);
        reactionPage = reactionDao.getAll(reactionFilter, reactionPageable);

        bioEntities.addAll(reactionPage.getContent());

      } else {
        final int offset = (int) (pageable.getOffset() - elementPage.getTotalElements());

        final int page = offset / (pageable.getPageSize() * 2);

        final Pageable reactionPageable = PageRequest.of(page, pageable.getPageSize() * 2);

        reactionPage = reactionDao.getAll(reactionFilter, reactionPageable);

        for (int i = 0, j = offset % (pageable.getPageSize() * 2); i < pageable.getPageSize() && j < reactionPage.getNumberOfElements(); i++, j++) {
          bioEntities.add(reactionPage.getContent().get(j));
        }
      }
    } else {
      reactionPage = reactionDao.getAll(reactionFilter, PageRequest.of(Integer.MAX_VALUE, 1));
    }

    final List<BioEntitySearchResult> result = new ArrayList<>();
    for (final BioEntity bioEntity : bioEntities) {
      if (bioEntity instanceof Element) {
        result.add(new BioEntitySearchResult(bioEntity, elementDao.isPerfectMatch((Element) bioEntity, originalQuery)));
      } else if (bioEntity instanceof Reaction) {
        result.add(new BioEntitySearchResult(bioEntity, reactionDao.isPerfectMatch((Reaction) bioEntity, originalQuery)));
      } else {
        throw new NotImplementedException("Don't know how to handle class: " + bioEntity);
      }
    }

    return new PageImpl<>(result, pageable, elementPage.getTotalElements() + reactionPage.getTotalElements());
  }

  private Map<ElementProperty, Object> convertBioEntityFilterToElementFilter(final Map<BioEntityProperty, Object> properties)
      throws ObjectNotFoundException {
    final Map<ElementProperty, Object> result = new HashMap<>();
    for (final BioEntityProperty type : properties.keySet()) {
      switch (type) {
        case ANNOTATION:
          result.put(ElementProperty.ANNOTATION, Collections.singletonList(properties.get(type)));
          break;
        case CLASS:
          result.put(ElementProperty.CLASS, properties.get(type));
          break;
        case MAP_ID:
          break;
        case PERFECT_TEXT:
          result.put(ElementProperty.PERFECT_TEXT, Collections.singletonList(properties.get(type)));
          break;
        case PROJECT_ID:
          result.put(ElementProperty.PROJECT, Collections.singletonList(projectService.getProjectByProjectId((String) properties.get(type))));
          if (properties.get(BioEntityProperty.MAP_ID) != null) {
            final List<ModelData> filteredModel = getModelList((Collection<Integer>) properties.get(BioEntityProperty.MAP_ID),
                (String) properties.get(type));
            result.put(ElementProperty.MAP, filteredModel);
          }
          break;
        case TEXT:
          result.put(ElementProperty.TEXT, Collections.singletonList(properties.get(type)));
          break;
        default:
          throw new NotImplementedException("Don't know how to handle BioEntityProperty: " + type);
      }
    }
    return result;
  }

  private List<ModelData> getModelList(final Collection<Integer> mapIds, final String projectId) throws ObjectNotFoundException {
    final List<ModelData> filteredModel = new ArrayList<>();
    final Map<ModelProperty, Object> properties = new HashMap<>();
    properties.put(ModelProperty.PROJECT_ID, Collections.singletonList(projectId));
    final List<ModelData> models = modelService.getAll(properties, Pageable.unpaged()).getContent();
    for (final ModelData modelData : models) {
      if (mapIds.contains(modelData.getId())) {
        filteredModel.add(modelData);
      }
    }
    if (filteredModel.isEmpty()) {
      throw new ObjectNotFoundException("Map with given id does not exist: " + mapIds);
    }
    return filteredModel;
  }

  private Map<ReactionProperty, Object> convertBioEntityFilterToReactionFilter(final Map<BioEntityProperty, Object> properties) {
    final Map<ReactionProperty, Object> result = new HashMap<>();
    for (final BioEntityProperty type : properties.keySet()) {
      switch (type) {
        case ANNOTATION:
          result.put(ReactionProperty.ANNOTATION, Collections.singletonList(properties.get(type)));
          break;
        case CLASS:
          result.put(ReactionProperty.CLASS, properties.get(type));
          break;
        case MAP_ID:
          // we handle this in project_id
          break;
        case PERFECT_TEXT:
          result.put(ReactionProperty.PERFECT_TEXT, Collections.singletonList(properties.get(type)));
          break;
        case PROJECT_ID:
          try {
            if (properties.get(BioEntityProperty.MAP_ID) != null) {
              final List<ModelData> filteredModel = getModelList((Collection<Integer>) properties.get(BioEntityProperty.MAP_ID),
                  (String) properties.get(type));
              result.put(ReactionProperty.MAP, filteredModel);
            } else {
              final Map<ModelProperty, Object> mapProperties = new HashMap<>();
              mapProperties.put(ModelProperty.PROJECT_ID, Collections.singletonList((String) properties.get(type)));
              final List<ModelData> models = modelService.getAll(mapProperties, Pageable.unpaged()).getContent();
              result.put(ReactionProperty.MAP, models);
            }

          } catch (final ObjectNotFoundException e) {
            logger.warn("Project not found: " + properties.get(type), e);
            result.put(ReactionProperty.MAP, new ArrayList<>());
          }
          break;
        case TEXT:
          result.put(ReactionProperty.TEXT, Collections.singletonList(properties.get(type)));
          break;
        default:
          throw new NotImplementedException();
      }
    }
    return result;
  }

}