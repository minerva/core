package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.graphics.LayerRectDao;
import lcsb.mapviewer.persist.dao.graphics.LayerRectProperty;
import lcsb.mapviewer.services.interfaces.ILayerRectService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.websockets.IWebSocketMessenger;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageLayerChildCreated;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageLayerChildDeleted;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageLayerChildUpdated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Transactional
@Service
public class LayerRectService implements ILayerRectService {

  private final IWebSocketMessenger webSocketMessenger;

  private final IModelService modelService;

  private final LayerRectDao layerRectDao;

  @Autowired
  public LayerRectService(final LayerRectDao layerRectDao, final IWebSocketMessenger webSocketMessenger, final IModelService modelService) {
    this.layerRectDao = layerRectDao;
    this.webSocketMessenger = webSocketMessenger;
    this.modelService = modelService;
  }

  @Override
  public Page<LayerRect> getAll(final Map<LayerRectProperty, Object> filter, final Pageable pageable) {
    return layerRectDao.getAll(filter, pageable);
  }

  @Override
  public LayerRect getById(final int id) {
    return layerRectDao.getById(id);
  }

  @Override
  public void update(final LayerRect layerRect) {
    layerRectDao.update(layerRect);
    String projectId = layerRect.getLayer().getModel().getProject().getProjectId();
    webSocketMessenger.broadcast(projectId, new WebSocketMessageLayerChildUpdated(layerRect, projectId, layerRect.getLayer()));
  }

  @Override
  public void remove(final LayerRect layerRect) {
    layerRectDao.delete(layerRect);
    String projectId = layerRect.getLayer().getModel().getProject().getProjectId();
    webSocketMessenger.broadcast(projectId, new WebSocketMessageLayerChildDeleted(layerRect, projectId, layerRect.getLayer()));
  }

  @Override
  public long getCount(final Map<LayerRectProperty, Object> filterOptions) {
    return layerRectDao.getCount(filterOptions);
  }

  @Override
  public void add(final LayerRect layerRect) {
    layerRectDao.add(layerRect);
    ModelData model = modelService.getById(layerRect.getLayer().getModel().getId());
    String projectId = model.getProject().getProjectId();
    webSocketMessenger.broadcast(projectId, new WebSocketMessageLayerChildCreated(layerRect, projectId, layerRect.getLayer()));
  }

}
