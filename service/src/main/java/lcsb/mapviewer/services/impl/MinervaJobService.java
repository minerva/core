package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.annotation.cache.CacheQueryMinervaJob;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.common.MinervaConfigurationHolder;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.cache.CacheQuery;
import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.model.job.MinervaJob;
import lcsb.mapviewer.model.job.MinervaJobExecutor;
import lcsb.mapviewer.model.job.MinervaJobPriority;
import lcsb.mapviewer.model.job.MinervaJobStatus;
import lcsb.mapviewer.model.job.MinervaJobType;
import lcsb.mapviewer.persist.CustomDatabasePopulator;
import lcsb.mapviewer.persist.dao.MinervaJobDao;
import lcsb.mapviewer.persist.dao.MinervaJobProperty;
import lcsb.mapviewer.persist.dao.cache.CacheTypeDao;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.datasource.init.ScriptException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;

@Service
public class MinervaJobService implements IMinervaJobService, ApplicationContextAware {

  private static final Set<Integer> jobsInExecutor = new HashSet<>();

  private static boolean queueEnabled = false;
  private static boolean shutdownOnFailure = true;

  private final Logger logger = LogManager.getLogger();
  private final MinervaJobDao minervaJobDao;
  private final CacheTypeDao cacheTypeDao;
  private final GeneralCacheInterface cache;
  private final ExecutorService jobScheduler = Executors.newSingleThreadExecutor();
  private final Map<MinervaJobType, MinervaJobExecutor> executors = new HashMap<>();
  private final CustomDatabasePopulator flyway;
  private final MinervaConfigurationHolder configurationHolder;
  private ApplicationContext context;
  private ExecutorService jobService;
  @Autowired
  private IMinervaJobService self;
  private Calendar lastRefreshTaskExceptionTimestamp = null;

  @Autowired
  public MinervaJobService(final MinervaJobDao minervaJobDao, final GeneralCacheInterface cache, final CacheTypeDao cacheTypeDao,
                           final CustomDatabasePopulator flyway, final MinervaConfigurationHolder configurationHolder) {
    this.minervaJobDao = minervaJobDao;
    this.cache = cache;
    this.cacheTypeDao = cacheTypeDao;
    this.flyway = flyway;
    this.configurationHolder = configurationHolder;
  }

  @PostConstruct
  public void init() {
    try {
      flyway.populate(flyway.getDataSource().getConnection());
    } catch (ScriptException | SQLException e) {
      logger.error(e, e);
    }

    jobService = Executors.newScheduledThreadPool(configurationHolder.getBatchThreads(), new ThreadFactory() {
      @Override
      public Thread newThread(final Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        return t;
      }
    });
    jobScheduler.execute(() -> {
      int errorIssues = 0;
      while (true) {
        try {
          Thread.sleep(50);
          synchronized (jobsInExecutor) {
            if (!queueEnabled) {
              logger.debug("QUEUE disabled");
              continue;
            }
            if (((ScheduledThreadPoolExecutor) jobService).getQueue().isEmpty()) {
              if (self.getPending(1).isEmpty()) {
                addMissingRefreshTasks(100);
              }
              for (MinervaJob job : self.getPending(10)) {
                if (!jobsInExecutor.contains(job.getId())) {
                  if (job.getPriority() < MinervaJobPriority.LOW.getValue()
                      && getNumberOfLowestJobsInExecutor() >= configurationHolder.getBatchThreads()
                      && configurationHolder.getBatchThreads() > 1) {
                    continue;
                  }
                  jobsInExecutor.add(job.getId());
                  self.evict(job);
                  jobService.submit(createJobExecutionWrapper(job));
                }
              }
            }
          }
          Thread.sleep(50);
          errorIssues = 0;
        } catch (InterruptedException e) {
          logger.error("QUEUE interrupted", e);
          break;
        } catch (Throwable e) {
          logger.error("Unexpected exception", e);
          errorIssues++;
          if (errorIssues >= 5) {
            break;
          }
          try {
            // wait for 30 seconds - we don't know what is happening probably something with
            // db
            Thread.sleep(30000);
          } catch (InterruptedException interruption) {
            logger.error("QUEUE interrupted", e);
            break;
          }
          logger.info("Recovering attempt: {}", errorIssues);
        }
      }
      if (MinervaJobService.shutdownOnFailure) {
        logger.fatal("QUEUE stopped. Stopping minerva");
        int exitCode = SpringApplication.exit(context, () -> 1);
        System.exit(exitCode);
      } else {
        logger.fatal("QUEUE stopped.");
      }
    });

    registerExecutor(MinervaJobType.REFRESH_CACHE, cache);
  }

  private int getNumberOfLowestJobsInExecutor() {
    int counter = 0;
    for (int id : jobsInExecutor) {
      MinervaJob job = self.getJobById(id);
      if (job != null && job.getPriority() == MinervaJobPriority.LOWEST.getValue()) {
        counter++;
      }
    }
    return counter;
  }

  @Override
  public void setApplicationContext(final ApplicationContext context) throws BeansException {
    this.context = context;
  }

  @Override
  @Transactional
  public boolean addJob(final MinervaJob job) {
    logger.debug("Adding job: {}, {}, priority: {}, {}", job.getId(), job.getJobType(), job.getPriority(), job.getJobParameters());
    if (job.getJobType().allowDuplicates() || minervaJobDao.getUnfinishedByJobParameters(job.getJobType(), job.getJobParameters()).isEmpty()) {
      minervaJobDao.add(job);
      logger.debug("Job added: {}, {}, priority: {}, {}", job.getId(), job.getJobType(), job.getPriority(), job.getJobParameters());
      return true;
    } else {
      logger.debug("Job skipped (duplicate found): {}, {}, priority: {}, {}",
          job.getId(), job.getJobType(), job.getPriority(), job.getJobParameters());
      return false;
    }
  }

  @Override
  @Transactional
  public MinervaJob updateJob(final MinervaJob job) {
    minervaJobDao.saveOrUpdate(job);
    return job;
  }

  @Override
  public void waitForTasksToFinish() {
    synchronized (jobsInExecutor) {
      if (((ScheduledThreadPoolExecutor) jobService).getQueue().isEmpty()) {
        if (self.getPending(1).isEmpty()) {
          addMissingRefreshTasks(Integer.MAX_VALUE);
        }
      }
    }

    logger.debug("Waiting for jobs to finish");
    while (self.getCount(MinervaJobStatus.PENDING) + self.getCount(MinervaJobStatus.RUNNING) > 0) {
      logger.debug("Waiting...");
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        logger.error("Problem with waiting", e);
        return;
      }
    }
    logger.debug("Waiting finished");
  }

  @Override
  @Transactional
  public List<MinervaJob> getPending(final int max) {
    return minervaJobDao.getPending(max);
  }

  @Override
  @Transactional
  public List<MinervaJob> getPending() {
    return minervaJobDao.getPending();
  }

  @Override
  @Transactional
  public MinervaJob getJobById(final int id) {
    return minervaJobDao.getById(id);
  }

  @Override
  @Transactional
  public CacheType getTypeById(final Integer typeId) {
    return cacheTypeDao.getById(typeId);
  }

  @Override
  @Transactional
  public void evict(final MinervaJob job) {
    minervaJobDao.evict(job);
  }

  @Override
  @Transactional
  public long getCount(final MinervaJobStatus status) {
    return minervaJobDao.getCount(status);
  }

  @Override
  public long getCount(final Map<MinervaJobProperty, Object> filterOptions) {
    return minervaJobDao.getCount(filterOptions);
  }


  @Override
  public void registerExecutor(final MinervaJobType jobType, final MinervaJobExecutor executor) {
    executors.put(jobType, executor);
  }

  @Override
  @Transactional
  public void setInterruptedOnRunningJobs() {
    for (MinervaJob job : minervaJobDao.getRunning()) {
      job.setJobStatus(MinervaJobStatus.INTERRUPTED);
      minervaJobDao.update(job);
    }

  }

  @Override
  @Transactional
  public List<MinervaJob> getAllJobs() {
    return minervaJobDao.getAll();
  }

  @Override
  public void removeOldJobs(final MinervaJobType type, final MinervaJobStatus status, final Calendar beforeDate) {
    minervaJobDao.deleteBefore(type, status, beforeDate);
  }

  @Transactional
  @Override
  public void removeOldJobs(final MinervaJobStatus status, final Calendar beforeDate) {
    minervaJobDao.deleteBefore(status, beforeDate);
  }

  @Override
  public void removeOldJobs(final Calendar beforeDate) {
    minervaJobDao.deleteBefore(beforeDate);
  }

  @Override
  public void enableQueue() {
    enableQueue(true);
  }

  @Override
  public void enableQueue(final boolean shutdownOnFailure) {
    logger.debug("Enable queue");
    MinervaJobService.queueEnabled = true;
    MinervaJobService.shutdownOnFailure = shutdownOnFailure;
  }

  @Override
  public void disableQueue() {
    logger.debug("Disable queue");
    MinervaJobService.queueEnabled = false;
  }

  @Override
  public void updateJobProgress(final MinervaJob job, final Double progress) {
    MinervaJob dbJob = self.getJobById(job.getId());
    if (dbJob != null) {
      dbJob.setProgress(progress);
      minervaJobDao.update(dbJob);
    } else {
      logger.warn("Job not found: {}", job.getId());
    }
  }

  private Callable<Object> createJobExecutionWrapper(final MinervaJob job) {
    return new Callable<Object>() {
      @Override
      public Object call() {
        try {
          startJob(job);

          MinervaJobExecutor executor = getExecutor(job.getJobType());
          if (executor == null) {
            logger.error("Cannot find executor for job: {} (job id: {})", job.getJobType(), job.getId());
            
            MinervaJob dbJob = self.getJobById(job.getId());
            dbJob.setJobStatus(MinervaJobStatus.FAILED);
            dbJob.setLogs("Cannot find executor for job: " + dbJob.getJobType() + " (job id: " + dbJob.getId() + ")");
            return self.updateJob(dbJob);
          }
          executor.execute(job);

          return finishJob(job);
        } catch (Throwable e) {
          return finishJobWithProblems(job, e);
        } finally {
          synchronized (jobsInExecutor) {
            jobsInExecutor.remove(job.getId());
          }
        }
      }

    };
  }

  protected MinervaJobExecutor getExecutor(final MinervaJobType jobType) {
    return executors.get(jobType);
  }

  private void addMissingRefreshTasks(final int limit) {
    try {
      List<CacheQuery> queries = cache.getExpired(limit);
      for (CacheQuery cacheQuery : queries) {
        CacheQueryMinervaJob params = new CacheQueryMinervaJob(cacheQuery.getQuery(), cacheQuery.getType());
        MinervaJob job = new MinervaJob(MinervaJobType.REFRESH_CACHE, MinervaJobPriority.LOWEST, params);
        self.addJob(job);
      }
    } catch (Exception e) {
      // log these errors not too often (once per 10 minutes)
      Calendar tenMinutesAgo = Calendar.getInstance();
      tenMinutesAgo.add(Calendar.MINUTE, -10);
      if (lastRefreshTaskExceptionTimestamp == null || lastRefreshTaskExceptionTimestamp.before(tenMinutesAgo)) {
        logger.error("Sever problem with adding tasks to refresh list", e);
        lastRefreshTaskExceptionTimestamp = Calendar.getInstance();
      }
    }
  }

  private void startJob(final MinervaJob job) {
    logger.debug("Starting job: {}, {}, priority: {}, {}", job.getId(), job.getJobType(), job.getPriority(), job.getJobParameters());
    MinervaJob dbJob = self.getJobById(job.getId());
    dbJob.setJobStatus(MinervaJobStatus.RUNNING);
    self.updateJob(dbJob);
  }

  private MinervaJob finishJob(final MinervaJob job) {
    logger.debug("Job finished: {}", job.getId());
    MinervaJob dbJob = self.getJobById(job.getId());
    dbJob.setJobStatus(MinervaJobStatus.DONE);
    return self.updateJob(dbJob);
  }

  private MinervaJob finishJobWithProblems(final MinervaJob job, final Throwable e) {
    logger.error("Problem with executing job: {} (job id: {})", job.getJobType(), job.getId(), e);
    MinervaJob dbJob = self.getJobById(job.getId());
    dbJob.setJobStatus(MinervaJobStatus.FAILED);
    dbJob.setLogs(ExceptionUtils.getStackTrace(e));
    return self.updateJob(dbJob);
  }

  @Override
  public Page<MinervaJob> getAll(final Map<MinervaJobProperty, Object> filter, final Pageable pageable) {
    return minervaJobDao.getAll(filter, pageable);
  }

  @Override
  public MinervaJob getById(final int id) {
    return minervaJobDao.getById(id);
  }

  @Override
  public void update(final MinervaJob object) {
    self.updateJob(object);
  }

  @Override
  public void remove(final MinervaJob object) {
    throw new NotImplementedException();
  }

  @Override
  public void add(final MinervaJob object) {
    self.addJob(object);
  }
}
