package lcsb.mapviewer.services.impl;

import java.util.Objects;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.services.MiriamConnector;
import lcsb.mapviewer.annotation.services.PubmedParser;
import lcsb.mapviewer.annotation.services.PubmedSearchException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.Article;
import lcsb.mapviewer.model.job.MinervaJob;
import lcsb.mapviewer.model.job.MinervaJobPriority;
import lcsb.mapviewer.model.job.MinervaJobType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.persist.dao.map.MiriamDataDao;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IMiriamService;
import lcsb.mapviewer.services.jobs.RefreshMiriamInfoMinervaJob;

@Transactional
@Service
public class MiriamService implements IMiriamService {

  private Logger logger = LogManager.getLogger();

  private MiriamConnector miriamConnector;

  private PubmedParser pubmedParser;

  private MiriamDataDao miriamDataDao;

  private IMinervaJobService minervaJobService;

  @Autowired
  private IMiriamService self;

  @Autowired
  public MiriamService(final MiriamConnector miriamConnector, final PubmedParser pubmedParser, final MiriamDataDao miriamDataDao,
      final IMinervaJobService minervaJobService) {
    this.miriamConnector = miriamConnector;
    this.pubmedParser = pubmedParser;
    this.miriamDataDao = miriamDataDao;
    this.minervaJobService = minervaJobService;
  }

  @PostConstruct
  public void init() {
    minervaJobService.registerExecutor(MinervaJobType.REFRESH_MIRIAM_INFO, self);
  }

  @Override
  public String getUrlString(final MiriamData miriamData) {
    return miriamConnector.getUrlString(miriamData);
  }

  @Override
  public Article getArticle(final MiriamData miriamData) throws PubmedSearchException {
    return pubmedParser.getPubmedArticleById(miriamData.getResource());
  }

  @Override
  public MiriamData getById(final int id) {
    return miriamDataDao.getById(id);
  }

  @Override
  public void add(final MiriamData md) {
    miriamDataDao.add(md);
    createRefreshJob(md);
  }

  private void createRefreshJob(final MiriamData md) {
    RefreshMiriamInfoMinervaJob params = new RefreshMiriamInfoMinervaJob(md.getId());
    minervaJobService.addJob(new MinervaJob(MinervaJobType.REFRESH_MIRIAM_INFO, MinervaJobPriority.MEDIUM, params));
  }

  @Override
  public void update(final MiriamData md) {
    miriamDataDao.saveOrUpdate(md);
    createRefreshJob(md);
  }

  @Override
  public void execute(final MinervaJob job) throws Exception {
    if (job.getJobType() == MinervaJobType.REFRESH_MIRIAM_INFO) {
      RefreshMiriamInfoMinervaJob parametres = new RefreshMiriamInfoMinervaJob(job.getJobParameters());
      Integer id = parametres.getId();
      MiriamData md = getById(id);

      if (md == null) {
        logger.debug("MiriamData does not exist (id:" + id + ")");
        return;
      }
      if (Objects.equals(md.getDataType(), MiriamType.PUBMED)) {
        md.setArticle(getArticle(md));
      } else {
        md.setArticle(null);
      }
      md.setLink(getUrlString(md));
      miriamDataDao.saveOrUpdate(md);
    } else {
      throw new NotImplementedException(job.getJobType() + " not imeplemented");
    }
  }

  @Override
  public void delete(final MiriamData md) {
    miriamDataDao.delete(md);
  }

}
