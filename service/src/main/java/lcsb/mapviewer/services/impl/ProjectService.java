package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.annotation.services.MeSHParser;
import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.annotation.services.TaxonomySearchException;
import lcsb.mapviewer.commands.ClearColorModelCommand;
import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.commands.CopyCommand;
import lcsb.mapviewer.commands.CreateHierarchyCommand;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.MinervaConfigurationHolder;
import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.ColorSchemaReader;
import lcsb.mapviewer.converter.ComplexZipConverter;
import lcsb.mapviewer.converter.ComplexZipConverterParams;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.ProjectFactory;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.converter.graphics.MapGenerator;
import lcsb.mapviewer.converter.graphics.MapGenerator.MapGeneratorParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.converter.zip.ZipEntryFile;
import lcsb.mapviewer.model.IgnoredLogMarker;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectLogEntry;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.ProjectStatus;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.job.MinervaJob;
import lcsb.mapviewer.model.job.MinervaJobPriority;
import lcsb.mapviewer.model.job.MinervaJobType;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.ProjectBackground;
import lcsb.mapviewer.model.map.layout.ProjectBackgroundImageLayer;
import lcsb.mapviewer.model.map.layout.ProjectBackgroundStatus;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.InvalidDataOverlayException;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.model.user.UserAnnotationSchema;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.ObjectValidator;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.ProjectLogEntryDao;
import lcsb.mapviewer.persist.dao.ProjectLogEntryProperty;
import lcsb.mapviewer.persist.dao.ProjectProperty;
import lcsb.mapviewer.persist.dao.graphics.LayerProperty;
import lcsb.mapviewer.persist.dao.map.ModelProperty;
import lcsb.mapviewer.persist.dao.user.UserAnnotationSchemaDao;
import lcsb.mapviewer.services.interfaces.ICommentService;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IFileService;
import lcsb.mapviewer.services.interfaces.ILayerService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectBackgroundService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.services.jobs.AnnotateProjectMinervaJob;
import lcsb.mapviewer.services.jobs.CreateProjectFailureMinervaJob;
import lcsb.mapviewer.services.jobs.CreateProjectMinervaJob;
import lcsb.mapviewer.services.jobs.DeleteBackgroundMinervaJob;
import lcsb.mapviewer.services.jobs.DeleteProjectMinervaJob;
import lcsb.mapviewer.services.jobs.RefreshChemicalInfoMinervaJob;
import lcsb.mapviewer.services.jobs.RefreshChemicalSuggestedQueryListMinervaJob;
import lcsb.mapviewer.services.jobs.RefreshDrugInfoMinervaJob;
import lcsb.mapviewer.services.jobs.RefreshMiriamInfoMinervaJob;
import lcsb.mapviewer.services.jobs.ReviveBackgroundsMinervaJob;
import lcsb.mapviewer.services.utils.CreateProjectParams;
import lcsb.mapviewer.services.utils.EmailException;
import lcsb.mapviewer.services.utils.EmailSender;
import lcsb.mapviewer.services.utils.data.BuildInBackgrounds;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LogEvent;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.PersistenceException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Implementation of the project service. It allows to manage and access project
 * data.
 *
 * @author Piotr Gawron
 */
@Service
public class ProjectService implements IProjectService {

  /**
   * Size of the artificial buffer that will be released when
   * {@link OutOfMemoryError} is thrown to gain some free memory and report
   * problem.
   */
  private static final int OUT_OF_MEMORY_BACKUP_BUFFER_SIZE = 10000;

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  /**
   * Data access object for projects.
   */
  private final ProjectDao projectDao;

  /**
   * Data access object for models.
   */
  private final ProjectLogEntryDao projectLogEntryDao;

  private final IModelService modelService;

  private final ILayerService layerService;

  /**
   * Service that allows to access and manage comments.
   */
  private final ICommentService commentService;

  /**
   * Service that manages and gives access to configuration parameters.
   */
  private final IConfigurationService configurationService;

  /**
   * Services that manages and gives access to user information.
   */
  private final IUserService userService;

  @Autowired
  private IFileService fileService;

  private final IProjectBackgroundService projectBackgroundService;

  private final IMinervaJobService minervaJobService;

  /**
   * Utils that help to manage the sessions in custom multi-threaded
   * implementation.
   */
  private final DbUtils dbUtils;

  /**
   * Access point and parser for the online ctd database.
   */
  private final MeSHParser meshParser;

  /**
   * Access point and parser for the online ctd database.
   */
  private final TaxonomyBackend taxonomyBackend;

  // self reference to obtain proxy object:
  // https://stackoverflow.com/questions/54800714/accessing-spring-bean-proxy-reference-itself
  @Autowired
  private IProjectService self;

  private final MinervaConfigurationHolder minervaConfigurationHolder;

  private final UserAnnotationSchemaDao userAnnotationSchemaDao;

  /**
   * Class that helps to generate images for frontend.
   */
  private final MapGenerator generator = new MapGenerator();

  @Autowired
  public ProjectService(final MinervaConfigurationHolder minervaConfigurationHolder,
                        final ProjectDao projectDao,
                        final ProjectLogEntryDao projectLogEntryDao,
                        final IModelService modelService,
                        final ILayerService layerService,
                        final ICommentService commentService,
                        final IConfigurationService configurationService,
                        final IUserService userService,
                        final DbUtils dbUtils,
                        final MeSHParser meshParser,
                        final TaxonomyBackend taxonomyBackend,
                        final IProjectBackgroundService projectBackgroundService,
                        final IMinervaJobService minervaJobService,
                        final UserAnnotationSchemaDao userAnnotationSchemaDao) {
    this.minervaConfigurationHolder = minervaConfigurationHolder;
    this.projectDao = projectDao;
    this.projectLogEntryDao = projectLogEntryDao;
    this.modelService = modelService;
    this.layerService = layerService;
    this.commentService = commentService;
    this.configurationService = configurationService;
    this.userService = userService;
    this.dbUtils = dbUtils;
    this.meshParser = meshParser;
    this.taxonomyBackend = taxonomyBackend;
    this.projectBackgroundService = projectBackgroundService;
    this.minervaJobService = minervaJobService;
    this.userAnnotationSchemaDao = userAnnotationSchemaDao;
  }

  @PostConstruct
  public void init() {
    minervaJobService.registerExecutor(MinervaJobType.CREATE_PROJECT, self);
    minervaJobService.registerExecutor(MinervaJobType.DELETE_PROJECT, self);
    minervaJobService.registerExecutor(MinervaJobType.CREATE_PROJECT_FAILURE, self);

    minervaJobService.registerExecutor(MinervaJobType.DELETE_BACKGROUND, self);
    minervaJobService.registerExecutor(MinervaJobType.REVIVE_BACKGROUNDS, self);
  }

  @Override
  public Project getProjectByProjectId(final String name) {
    final Project project = projectDao.getProjectByProjectId(name);
    if (project != null && project.getTopModelData() != null) {
      Hibernate.initialize(project.getTopModelData().getId());
    }
    return project;
  }

  @Override
  public Project getProjectByProjectId(final String projectId, final boolean initializeLazy) {
    final Project result = self.getProjectByProjectId(projectId);
    if (result != null && initializeLazy) {
      initLazy(result);
    }
    return result;
  }

  @Override
  public boolean projectExists(final String projectName) {
    if (projectName == null || projectName.isEmpty()) {
      return false;
    }
    return projectDao.isProjectExistsByName(projectName);
  }

  @Override
  public List<Project> getAllProjects() {
    final List<Project> projects = projectDao.getAll();
    for (final Project project : projects) {
      Hibernate.initialize(project.getLogEntries());
    }
    return projects;
  }

  @Override
  public List<Project> getAllProjects(final boolean initializeLazy) {
    final List<Project> result = self.getAllProjects();
    if (initializeLazy) {
      for (final Project project : result) {
        initLazy(project);
      }
    }
    return result;
  }

  @Override
  public MinervaJob submitRemoveProjectJob(final Project project) {
    final String homeDir = getProjectHomeDir(project);

    final DeleteProjectMinervaJob jobParameters = new DeleteProjectMinervaJob(project.getProjectId(), homeDir);
    final MinervaJob job = new MinervaJob(MinervaJobType.DELETE_PROJECT, MinervaJobPriority.MEDIUM, jobParameters);
    job.setExternalObject(project);
    minervaJobService.addJob(job);
    return job;
  }

  @Override
  public void removeProject(final String projectId, final String directory) {

    Project project = null;
    try {
      project = self.getProjectByProjectId(projectId);
      project.setStatus(ProjectStatus.REMOVING);
      project.setProgress(0);
      self.update(project);

      String email = null;
      final Map<ModelProperty, Object> filter = new HashMap<>();
      filter.put(ModelProperty.PROJECT_ID, projectId);

      for (final ModelData originalModel : modelService.getAll(filter, Pageable.unpaged()).getContent()) {
        final List<ModelData> models = new ArrayList<>();
        models.add(originalModel);
        for (final ModelSubmodelConnection connection : originalModel.getSubmodels()) {
          models.add(connection.getSubmodel());
        }
        for (final ModelData model : models) {
          logger.debug("Remove model: " + model.getId());
          commentService.removeCommentsForModel(model);

        }
        for (final ProjectBackground background : projectBackgroundService.getProjectBackgroundsByProject(project)) {
          self.removeBackground(background.getId(), directory);
        }
        email = project.getNotifyEmail();
      }
      if (directory != null) {
        final File homeDirFile = new File(directory);
        if (homeDirFile.exists()) {
          logger.debug("Removing project directory: " + homeDirFile.getAbsolutePath());
          try {
            FileUtils.deleteDirectory(homeDirFile);
          } catch (final IOException e) {
            logger.error("Problem with removing directory", e);
          }
        }
      }

      self.remove(project);

      logger.info("Project " + project.getProjectId() + " removed successfully.");

      if (email != null) {
        try {
          sendSuccesfullRemoveEmail(project.getProjectId(), email);
        } catch (final EmailException e) {
          logger.error("Problem with sending remove email.", e);
        }
      }

    } catch (final Exception e) {
      self.projectFailure(projectId, ProjectLogEntryType.OTHER,
          "Severe problem with removing object. Underlying error:\n" + e.getMessage()
              + "\nMore information can be found in log file.",
          e);
    }
  }

  public Project createProject(final CreateProjectParams params) {
    final Project project2 = self.getProjectByProjectId(params.getProjectId());
    if (project2 == null) {
      logger.error("Project with given id does not exist: {}", params.getProjectId());
      return null;
    }
    //noinspection MismatchedReadAndWriteOfArray
    double[] outOfMemoryBuffer;
    final MinervaLoggerAppender appender = MinervaLoggerAppender.createAppender();
    try {
      final UploadedFileEntry file = fileService.getById(params.getProjectFileId());
      logger.debug("Running: " + params.getProjectId() + "; fileId=" + params.getProjectFileId());
      outOfMemoryBuffer = new double[OUT_OF_MEMORY_BACKUP_BUFFER_SIZE];
      for (int i = 0; i < OUT_OF_MEMORY_BACKUP_BUFFER_SIZE; i++) {
        outOfMemoryBuffer[i] = Math.random() * OUT_OF_MEMORY_BACKUP_BUFFER_SIZE;
      }

      project2.setInputData(file);
      self.update(project2);

      self.updateProjectStatus(params.getProjectId(), ProjectStatus.PARSING_DATA, 0.0);
      self.createModel(params);

      self.createImages(params);

      MinervaLoggerAppender.unregisterLogEventStorage(appender);
      self.addLogEntries(params.getProjectId(), createLogEntries(appender));

      createRefreshDrugInfoJobs(params.getProjectId());
      submitRefreshChemicalInfoJobs(params.getProjectId());
      createRefreshMiriamInfoJobs(params.getProjectId());

      self.updateProjectStatus(params.getProjectId(), ProjectStatus.DONE, IProgressUpdater.MAX_PROGRESS);
      if (params.getNotifyEmail() != null && !params.getNotifyEmail().isEmpty()) {
        try {
          sendSuccesfullEmail(params);
        } catch (final EmailException e) {
          logger.error(e, e);
        }
      }

      logger.info("Project {} created successfully.", params.getProjectId());
    } catch (PersistenceException e) {
      outOfMemoryBuffer = null;
      logger.error("Problem with database", e);
      self.projectFailure(params.getProjectId(), ProjectLogEntryType.DATABASE_PROBLEM,
          "Problem with uploading to database. "
              + "You might violated some unhandled constraints or you run out of memory. Underlying error:\n"
              + e.getMessage() + "\nMore information can be found in log file.",
          e);
    } catch (final OutOfMemoryError outOfMemoryError) {
      // release some memory
      outOfMemoryBuffer = null;
      self.projectFailure(params.getProjectId(), ProjectLogEntryType.OUT_OF_MEMORY,
          "Out of memory: " + outOfMemoryError.getMessage(), outOfMemoryError);
    } catch (final Throwable e) {
      outOfMemoryBuffer = null;
      self.projectFailure(params.getProjectId(), ProjectLogEntryType.OTHER,
          "Problem with uploading map: " + e.getMessage() + ". More details can be found in log file.",
          e);
      final String email = params.getNotifyEmail();
      final String projectName = params.getProjectId();

      if (email != null) {
        self.sendUnsuccesfullEmail(projectName, email, e);
      }
    } finally {
      MinervaLoggerAppender.unregisterLogEventStorage(appender);
    }
    return self.getProjectByProjectId(params.getProjectId());
  }

  @Override
  public MinervaJob submitCreateProjectJob(final CreateProjectParams params) {
    final Project project = createProjectFromParams(params);
    fixProjectIssues(project);
    self.add(project);

    final CreateProjectMinervaJob jobParameters = new CreateProjectMinervaJob(params);

    final MinervaJob job = new MinervaJob(MinervaJobType.CREATE_PROJECT, MinervaJobPriority.HIGH, jobParameters);
    job.setExternalObject(project);
    minervaJobService.addJob(job);

    return job;

  }

  public void createRefreshMiriamInfoJobs(final String projectId) {
    for (MiriamData miriamData : self.getMiriamWithDuplicatesForProject(projectId)) {
      minervaJobService.addJob(createRefreshMiriamInfoJob(miriamData));
    }
  }

  public void createRefreshDrugInfoJobs(final String projectId) {
    for (MiriamData miriamData : self.getMiriamForProject(projectId)) {
      minervaJobService.addJob(createRefreshDrugInfoJob(miriamData));
    }
  }

  public void submitRefreshChemicalInfoJobs(final String projectId) {
    Project project = self.getProjectByProjectId(projectId, true);
    MiriamData disease = project.getDisease();
    if (disease != null) {
      for (MiriamData miriamData : self.getMiriamForProject(projectId)) {
        minervaJobService.addJob(createRefreshChemicalInfoJob(miriamData, disease));
      }
      RefreshChemicalSuggestedQueryListMinervaJob parameters = new RefreshChemicalSuggestedQueryListMinervaJob(disease, project.getId());
      MinervaJob job = new MinervaJob(MinervaJobType.REFRESH_CHEMICAL_SUGGESTED_QUERY_LIST, MinervaJobPriority.LOWEST, parameters);
      job.setExternalObject(project);
      minervaJobService.addJob(job);
    }
  }

  @Override
  public Set<MiriamData> getMiriamForProject(final String projectId) {
    return new HashSet<>(self.getMiriamWithDuplicatesForProject(projectId));
  }

  @Override
  public List<MiriamData> getMiriamWithDuplicatesForProject(final String projectId) {
    Project project = self.getProjectByProjectId(projectId, true);
    List<MiriamData> set = new ArrayList<>();

    for (ModelData model : project.getModels()) {
      set.addAll(model.getMiriamData());

      for (Element bioEntity : model.getElements()) {
        set.addAll(bioEntity.getMiriamData());
      }
      for (final Reaction bioEntity : model.getReactions()) {
        set.addAll(bioEntity.getMiriamData());
      }
    }
    return set;
  }

  private MinervaJob createRefreshDrugInfoJob(final MiriamData md) {
    final RefreshDrugInfoMinervaJob params = new RefreshDrugInfoMinervaJob(md.getDataType(), md.getResource());
    return new MinervaJob(MinervaJobType.REFRESH_DRUG_INFO, MinervaJobPriority.MEDIUM, params);
  }

  private MinervaJob createRefreshChemicalInfoJob(final MiriamData md, final MiriamData disease) {
    final RefreshChemicalInfoMinervaJob params = new RefreshChemicalInfoMinervaJob(md, disease);
    return new MinervaJob(MinervaJobType.REFRESH_CHEMICAL_INFO, MinervaJobPriority.MEDIUM, params);
  }

  private MinervaJob createRefreshMiriamInfoJob(final MiriamData md) {
    final RefreshMiriamInfoMinervaJob params = new RefreshMiriamInfoMinervaJob(md.getId());
    return new MinervaJob(MinervaJobType.REFRESH_MIRIAM_INFO, MinervaJobPriority.MEDIUM, params);
  }

  @Override
  public void update(final Project project) {
    projectDao.update(project);
    for (final ProjectBackground background : self.getBackgrounds(project.getProjectId(), false)) {
      background.setCreator(project.getOwner());
      self.updateBackground(background);
    }
    if (project.getOrganism() != null) {
      minervaJobService.addJob(createRefreshMiriamInfoJob(project.getOrganism()));
    }
    if (project.getDisease() != null) {
      minervaJobService.addJob(createRefreshMiriamInfoJob(project.getDisease()));
    }
  }

  /**
   * This method creates set of images for the model backgrounds.
   *
   * @param params configuration parameters including set of backgrounds to generate
   * @throws IOException               thrown when there are problems with generating files
   * @throws DrawingException          thrown when there was a problem with drawing a map
   * @throws CommandExecutionException thrown when one of the files describing backgrounds is invalid
   */
  @Override
  public void createImages(final CreateProjectParams params)
      throws IOException, DrawingException, CommandExecutionException {
    if (!params.isImages()) {
      return;
    }
    final Project project = self.getProjectByProjectId(params.getProjectId());
    project.getTopModel();
    self.updateProjectStatus(params.getProjectId(), ProjectStatus.GENERATING_IMAGES, 0);

    final int projectSize = project.getProjectBackgrounds().size();
    int counter = 0;
    for (int i = 0; i < project.getProjectBackgrounds().size(); i++) {
      final ProjectBackground background = project.getProjectBackgrounds().get(i);
      final double imgCounter = counter;
      final double finalSize = projectSize;
      final IProgressUpdater updater = progress -> self.updateProjectStatus(
          params.getProjectId(),
          ProjectStatus.GENERATING_IMAGES,
          IProgressUpdater.MAX_PROGRESS * imgCounter / finalSize + progress / finalSize
      );
      generateImagesForBuiltInBackground(background, updater);
      counter++;
    }
    for (final ProjectBackground background : project.getProjectBackgrounds()) {
      for (final ProjectBackgroundImageLayer imageLayer : background.getProjectBackgroundImageLayer()) {
        final String[] tmp = imageLayer.getDirectory().split("[\\\\/]");
        imageLayer.setDirectory(tmp[tmp.length - 1]);
      }
    }

    project.setProgress(100);
    self.update(project);

  }

  private void generateImagesForBuiltInBackground(final ProjectBackground background,
                                                  final IProgressUpdater updater) throws CommandExecutionException, IOException, DrawingException {
    for (final ProjectBackgroundImageLayer imageLayer : background.getProjectBackgroundImageLayer()) {
      final String directory = imageLayer.getDirectory();
      final Model model = imageLayer.getModel().getModel();
      Model output = model;
      if (background.isHierarchicalView()) {
        output = new CopyCommand(model).execute();
      }
      if (background.getName().equals(BuildInBackgrounds.CLEAN.getName())) {
        output = new CopyCommand(model).execute();
        new ClearColorModelCommand(output).execute();
      }
      final MapGeneratorParams imgParams = generator.new MapGeneratorParams().directory(directory).sbgn(background.getProject().isSbgnFormat())
          .nested(background.isHierarchicalView()).updater(updater);
      imgParams.model(output);
      generator.generateMapImages(imgParams);
    }
  }

  @Override
  public Project createModel(final CreateProjectParams params)
      throws InvalidInputDataExecption, ConverterException, IOException, InvalidDataOverlayException, CommandExecutionException {
    final User dbUser = userService.getUserByLogin(params.getUserLogin());
    UserAnnotationSchema userAnnotationSchema = dbUser.getAnnotationSchema();
    if (userAnnotationSchema == null) {
      userAnnotationSchema = new UserAnnotationSchema();
    }

    if (params.isComplex()) {
      self.createComplexProject(params);
    } else {
      self.createSimpleProject(params);
    }

    Map<LayerProperty, Object> layerFilter = new HashMap<>();
    layerFilter.put(LayerProperty.PROJECT_ID, params.getProjectId());
    List<Layer> layers = layerService.getAll(layerFilter, Pageable.unpaged()).getContent();
    for (final Layer layer : layers) {
      if (!layer.getName().equals(CreateHierarchyCommand.TEXT_LAYER_NAME)) {
        layer.setVisible(false);
        layerService.update(layer);
      }
    }

    self.updateProjectStatus(params.getProjectId(), ProjectStatus.UPLOADING_TO_DB, 0.0);

    self.addBuiltInBackgrounds(params);

    self.validateDataOverlays(params.getProjectId());


    self.createHierarchicalView(params.getProjectId());

    self.updateProjectStatus(params.getProjectId(), ProjectStatus.UPLOADING_TO_DB, 100.0);

    logger.trace("Model {} created", params.getProjectId());

    if (params.isUpdateAnnotations()) {
      self.submitAnnotateProjectJob(params.getProjectId(), userAnnotationSchema);
    }

    return self.getProjectByProjectId(params.getProjectId());
  }

  @Override
  public void addBuiltInBackgrounds(final CreateProjectParams params)
      throws InvalidInputDataExecption {
    User dbUser = userService.getUserByLogin(params.getUserLogin());
    Project project = self.getProjectByProjectId(params.getProjectId());
    final List<BuildInBackgrounds> buildInBackgrounds = new ArrayList<>();
    if (params.isNetworkBackgroundAsDefault()) {
      buildInBackgrounds.add(BuildInBackgrounds.NORMAL);
      buildInBackgrounds.add(BuildInBackgrounds.NESTED);
    } else {
      buildInBackgrounds.add(BuildInBackgrounds.NESTED);
      buildInBackgrounds.add(BuildInBackgrounds.NORMAL);
    }
    buildInBackgrounds.add(BuildInBackgrounds.CLEAN);

    // reverse the order of build in backgrounds, so we can insert them at the
    // beginning of list of backgrounds (the order will be the same)
    Collections.reverse(buildInBackgrounds);

    for (final BuildInBackgrounds buildInBackground : buildInBackgrounds) {
      int submodelId = 0;
      final ProjectBackground background = new ProjectBackground(buildInBackground.getName());
      background.setStatus(ProjectBackgroundStatus.NA);
      background.setProgress(0.0);
      background.setHierarchicalView(buildInBackground.isNested());
      background.setCreator(dbUser);
      project.addProjectBackground(0, background);

      final ProjectBackgroundImageLayer imageLayer = new ProjectBackgroundImageLayer(project.getTopModel(),
          params.getProjectDir() + "/" + buildInBackground.getDirectorySuffix() + submodelId + "/");
      submodelId++;
      background.addProjectBackgroundImageLayer(imageLayer);

      for (final ModelSubmodelConnection connection : project.getTopModel().getSubmodelConnections()) {
        final String directory = params.getProjectDir() + "/" + buildInBackground.getDirectorySuffix() + submodelId + "/";
        background.addProjectBackgroundImageLayer(new ProjectBackgroundImageLayer(connection.getSubmodel(), directory));

        assignZoomLevelDataToModel(connection.getSubmodel().getModel().getModelData());
        submodelId++;
      }
    }
    int order = 0;
    for (final ProjectBackground background : project.getProjectBackgrounds()) {
      background.setOrderIndex(order++);
    }
  }

  @Override
  public void validateDataOverlays(final String projectId) throws IOException, InvalidDataOverlayException {
    Project project = self.getProjectByProjectId(projectId);
    for (final DataOverlay l : project.getDataOverlays()) {
      if (l.getInputData() != null) {
        final ColorSchemaReader reader = new ColorSchemaReader();
        final Map<String, String> parameters = new DataOverlayService()
            .extractHeaderParameters(new ByteArrayInputStream(l.getInputData().getFileContent()), null);
        try {
          reader.readColorSchema(new ByteArrayInputStream(l.getInputData().getFileContent()), parameters);
        } catch (final InvalidDataOverlayException e) {
          throw new InvalidDataOverlayException("Problematic data overlay: " + l.getName() + ". " + e.getMessage(), e);
        }
      }
    }
  }

  @Override
  public void createHierarchicalView(final String projectId) throws CommandExecutionException {
    Project project = self.getProjectByProjectId(projectId);
    final Model originalModel = project.getTopModel();
    new CreateHierarchyCommand(originalModel, generator.computeZoomLevels(originalModel.getModelData()),
        generator.computeZoomFactor(originalModel.getModelData())).execute();
    for (final Model model : originalModel.getSubmodels()) {
      new CreateHierarchyCommand(model, generator.computeZoomLevels(model.getModelData()), generator.computeZoomFactor(model.getModelData()))
          .execute();
    }
    self.update(project);
    logger.debug("Hierarchy view added");
  }

  @Override
  public void createComplexProject(final CreateProjectParams params) throws InvalidInputDataExecption {
    try {
      final UploadedFileEntry file = fileService.getById(params.getProjectFileId());
      Project project = self.getProjectByProjectId(params.getProjectId());
      User dbUser = userService.getUserByLogin(params.getUserLogin());

      Class<? extends Converter> clazz = CellDesignerXmlParser.class;
      if (params.getParser() != null) {
        clazz = params.getParser();
      }
      final ComplexZipConverter parser = new ComplexZipConverter(clazz);
      final ComplexZipConverterParams complexParams;
      complexParams = new ComplexZipConverterParams().zipFile(file);
      complexParams.visualizationDir(params.getProjectDir());
      for (final ZipEntryFile entry : params.getZipEntries()) {
        complexParams.entry(entry);
      }
      final ProjectFactory projectFactory = new ProjectFactory(parser);
      projectFactory.create(complexParams, project);
      for (final DataOverlay overlay : project.getDataOverlays()) {
        overlay.setCreator(dbUser);
      }
      for (ModelData modelData : project.getModels()) {
        project.addLogEntries(fixModelIssues(modelData));
      }
      self.update(project);
    } catch (final IOException | ConverterException e) {
      throw new InvalidInputDataExecption("Problem with processing input file", e);
    }
  }

  @Override
  public void createSimpleProject(final CreateProjectParams params) throws ConverterException {
    try {
      final UploadedFileEntry file = fileService.getById(params.getProjectFileId());
      Project project = self.getProjectByProjectId(params.getProjectId());

      final Converter parser = params.getParser().newInstance();
      final InputStream input = new ByteArrayInputStream(file.getFileContent());
      final Model model = parser.createModel(new ConverterParams()
          .inputStream(input, file.getOriginalFileName())
          .sizeAutoAdjust(params.isAutoResize())
          .sbgnFormat(project.isSbgnFormat()));
      project.addLogEntries(fixModelIssues(model.getModelData()));
      assignZoomLevelDataToModel(model.getModelData());
      modelService.add(model.getModelData());
      project.setTopModel(model);
      self.update(project);
    } catch (InvalidInputDataExecption | InstantiationException | IllegalAccessException e) {
      throw new ConverterException(e);
    }
  }

  private void assignZoomLevelDataToModel(final ModelData topModel) throws InvalidInputDataExecption {
    final int maxZoomLevels = Integer
        .parseInt(configurationService.getValue(ConfigurationElementType.MAX_NUMBER_OF_MAP_LEVELS).getValue());

    final int zoomLevels = generator.computeZoomLevels(topModel);
    if (zoomLevels > maxZoomLevels) {
      throw new InvalidInputDataExecption(
          "Map " + topModel.getName() + " too big. You can change the max number of map zoom levels in configuration.");
    }
    topModel.setZoomLevels(zoomLevels);
    topModel.setTileSize(MapGenerator.TILE_SIZE);

  }

  /**
   * Updates status of the generating project.
   *
   * @param projectId project that is generated
   * @param status    what is the current status
   * @param progress  what is the progress
   */
  @Override
  public void updateProjectStatus(final String projectId, final ProjectStatus status, final double progress) {
    final Project project = self.getProjectByProjectId(projectId);
    if (project != null) {
      if (!status.equals(project.getStatus())
          || (Math.abs(progress - project.getProgress()) > IProgressUpdater.PROGRESS_BAR_UPDATE_RESOLUTION)) {
        project.setStatus(status);
        project.setProgress(progress);
        self.update(project);
      }
    } else {
      logger.trace("Update project status: {}, {}", status, progress);
    }
  }

  /**
   * Sends notification email that map was removed.
   *
   * @param projectId identifier of the project
   * @param email     notification email
   * @throws EmailException thrown when there is a problem with sending email
   */
  protected void sendSuccesfullRemoveEmail(final String projectId, final String email) throws EmailException {
    final EmailSender emailSender = new EmailSender(configurationService);
    emailSender.sendEmail("Minerva notification", "Map " + projectId + " was successfully removed.<br/>", email);
  }

  /**
   * Sends email about unsuccessful project creation.
   *
   * @param projectName name of the project
   * @param email       email where we want to send information
   * @param e           exception that caused problem
   */
  @Override
  public void sendUnsuccesfullEmail(final String projectName, final String email, final Throwable e) {
    final EmailSender emailSender = new EmailSender(configurationService);
    final StringBuilder content = new StringBuilder();
    content.append("There was a problem with generating ").append(projectName).append(" map.<br/>");
    content.append(e.getClass().getName()).append(": ").append(e.getMessage());
    try {
      emailSender.sendEmail("Minerva notification", content.toString(), email);
    } catch (final EmailException e1) {
      logger.error(e1);
    }
  }

  /**
   * Sends email about successful project creation.
   *
   * @param params model that was created
   * @throws EmailException exception thrown when there is a problem with sending email
   */
  protected void sendSuccesfullEmail(final CreateProjectParams params) throws EmailException {
    final EmailSender emailSender = new EmailSender(configurationService);
    emailSender.sendEmail("Minerva notification",
        "Your map " + params.getProjectId() + " was generated successfully.<br/>",
        params.getNotifyEmail());
  }

  private Project createProjectFromParams(final CreateProjectParams params) {
    final Project project = new Project(params.getProjectId());
    project.setOwner(userService.getUserByLogin(params.getUserLogin()));
    project.setName(params.getProjectName());
    if (params.getProjectDir() == null) {
      logger.warn("Project directory not set");
      project.setDirectory(null);
    } else {
      project.setDirectory(new File(params.getProjectDir()).getName());
    }
    project.setSbgnFormat(params.isSbgnFormat());
    project.setNotifyEmail(params.getNotifyEmail());

    MiriamData disease = null;
    if (params.getDisease() != null && !params.getDisease().isEmpty()) {
      disease = new MiriamData(MiriamType.MESH_2012, params.getDisease());
    }
    MiriamData organism = null;
    if (params.getOrganism() != null && !params.getOrganism().isEmpty()) {
      organism = new MiriamData(MiriamType.TAXONOMY, params.getOrganism());
    }
    if (meshParser.isValidMeshId(disease)) {
      project.setDisease(disease);
    } else {
      logger.warn("No valid disease is provided for project:" + project.getName());
    }
    try {
      if (taxonomyBackend.getNameForTaxonomy(organism) != null) {
        project.setOrganism(organism);
      } else {
        logger.warn(project.getProjectId() + "\tNo valid organism is provided for project. " + organism);
      }
    } catch (final TaxonomySearchException e) {
      logger.warn("Problem with accessing taxonomy db. More info in logs.", e);
    }

    project.setVersion(params.getVersion());
    project.setLicense(params.getLicense());
    project.setCustomLicenseName(params.getCustomLicenseName());
    project.setCustomLicenseUrl(params.getCustomLicenseUrl());
    return project;
  }

  Set<ProjectLogEntry> createLogEntries(final MinervaLoggerAppender appender) {
    final Set<ProjectLogEntry> result = new HashSet<>();
    for (final LogEvent event : appender.getWarnings()) {
      result.add(createLogEntry("WARNING", event));
    }
    for (final LogEvent event : appender.getErrors()) {
      result.add(createLogEntry("ERROR", event));
    }
    result.remove(null);
    return result;
  }

  private ProjectLogEntry createLogEntry(final Pair<Object, String> pair) {
    final ProjectLogEntry entry = new ProjectLogEntry();
    entry.setContent(pair.getRight());
    final Object left = pair.getLeft();
    if (left instanceof BioEntity) {
      final BioEntity bioEntity = (BioEntity) left;
      entry.setMapName(bioEntity.getModelData().getName());
      entry.setObjectClass(bioEntity.getClass().getSimpleName());
      entry.setObjectIdentifier(bioEntity.getElementId());
    } else if (left instanceof ModelData) {
      entry.setMapName(((ModelData) left).getName());
    }
    entry.setSeverity("WARNING");
    entry.setType(ProjectLogEntryType.DATABASE_PROBLEM);
    return entry;
  }

  private ProjectLogEntry createLogEntry(final String severity, final LogEvent event) {
    if (event.getMarker() instanceof IgnoredLogMarker) {
      return null;
    }
    ProjectLogEntry entry = null;
    if (event.getMarker() != null) {
      if (event.getMarker() instanceof LogMarker) {
        entry = ((LogMarker) event.getMarker()).getEntry();
      }
    }
    if (entry == null) {
      entry = new ProjectLogEntry();
      entry.setType(ProjectLogEntryType.OTHER);
    }
    entry.setSeverity(severity);
    entry.setContent(event.getMessage().getFormattedMessage());
    return entry;
  }

  List<ProjectLogEntry> fixModelIssues(final ModelData model) {
    List<ProjectLogEntry> result = new ArrayList<>();
    final ObjectValidator validator = new ObjectValidator();
    final List<Pair<Object, String>> issues = validator.getValidationIssues(model);
    if (!issues.isEmpty()) {
      for (final Pair<Object, String> pair : issues) {
        result.add(createLogEntry(pair));
      }
    }
    validator.fixValidationIssues(model);
    return result;
  }

  void fixProjectIssues(final Project project) {
    final ObjectValidator validator = new ObjectValidator();
    final List<Pair<Object, String>> issues = validator.getValidationIssues(project);
    if (!issues.isEmpty()) {
      for (final Pair<Object, String> pair : issues) {
        project.addLogEntry(createLogEntry(pair));
      }
    }
    validator.fixValidationIssues(project);
  }


  @Override
  public void removeBackground(final ProjectBackground projectBackground) {
    projectBackgroundService.delete(projectBackground);

  }

  @Override
  public void removeBackground(final int backgroundId, final String directory) {
    final MapGenerator mapGenerator = new MapGenerator();

    final ProjectBackground background = projectBackgroundService.getProjectBackgroundById(backgroundId);
    try {
      mapGenerator.removeProjectBackground(background, directory);
    } catch (final IOException e) {
      logger.error("Problem with removing directory for background: " + background.getId() + "; " + background.getName(), e);
    }
    background.getProject().getProjectBackgrounds().remove(background);
    projectBackgroundService.delete(background);
  }

  @Override
  public ProjectBackground getBackgroundById(final String projectId, final Integer backgroundId, final boolean initializeLazy) {
    for (final ProjectBackground projectBackground : projectBackgroundService.getProjectBackgroundsByProject(projectId)) {
      if (Objects.equals(projectBackground.getId(), backgroundId)) {
        if (initializeLazy) {
          Hibernate.initialize(projectBackground.getProjectBackgroundImageLayer());
          Hibernate.initialize(projectBackground.getProject());
        }
        return projectBackground;
      }
    }
    return null;
  }

  @Override
  public void updateBackground(final ProjectBackground background) {
    projectBackgroundService.updateProjectBackground(background);
  }

  @Override
  public long getNextId() {
    return projectDao.getNextId();
  }

  @Override
  public UploadedFileEntry getFileByProjectId(final String projectId) {
    final Project project = self.getProjectByProjectId(projectId);
    if (project == null) {
      return null;
    }
    Hibernate.initialize(project.getInputData());
    return project.getInputData();
  }

  @Override
  public List<ProjectBackground> getBackgrounds(final String projectId, final boolean initializeLazy) {
    final Project project = self.getProjectByProjectId(projectId);
    if (project != null) {
      for (final ProjectBackground projectBackground : project.getProjectBackgrounds()) {
        if (initializeLazy) {
          Hibernate.initialize(projectBackground.getProjectBackgroundImageLayer());
        }
      }
      return project.getProjectBackgrounds();
    } else {
      return new ArrayList<>();
    }
  }

  @Override
  public void add(final Project project) {
    projectDao.add(project);
  }

  @Override
  public void remove(final Project project) {
    projectDao.delete(project);
  }

  @Override
  public void addLogEntry(final Project p, final ProjectLogEntry entry) {
    entry.setProject(p);
    projectLogEntryDao.add(entry);
  }

  @Override
  public MinervaJob projectFailure(final String projectId, final ProjectLogEntryType errorType, final String message, final Throwable e) {
    logger.error(message, e);

    final CreateProjectFailureMinervaJob jobParameters = new CreateProjectFailureMinervaJob(message + ". Message: " + e.getMessage(), projectId,
        errorType.name());

    final MinervaJob job = new MinervaJob(MinervaJobType.CREATE_PROJECT_FAILURE, MinervaJobPriority.LOW, jobParameters);
    minervaJobService.addJob(job);

    return job;
  }

  @Override
  public void execute(final MinervaJob job) throws Exception {
    if (job.getJobType() == MinervaJobType.DELETE_PROJECT) {
      final DeleteProjectMinervaJob parameters = new DeleteProjectMinervaJob(job.getJobParameters());
      self.removeProject(parameters.getProjectId(), parameters.getDirectory());
    } else if (job.getJobType() == MinervaJobType.CREATE_PROJECT) {
      final CreateProjectMinervaJob parameters = new CreateProjectMinervaJob(job.getJobParameters());
      createProject(parameters.getParameters());

    } else if (job.getJobType() == MinervaJobType.CREATE_PROJECT_FAILURE) {
      final CreateProjectFailureMinervaJob parameters = new CreateProjectFailureMinervaJob(job.getJobParameters());

      final Project project = self.getProjectByProjectId(parameters.getProjectId());
      final ProjectLogEntry entry = new ProjectLogEntry();
      entry.setType(parameters.getType());
      entry.setSeverity("ERROR");
      entry.setContent(parameters.getMessage());
      project.setStatus(ProjectStatus.FAIL);
      project.setProgress(IProgressUpdater.MAX_PROGRESS);
      self.update(project);
      self.addLogEntry(project, entry);
    } else if (job.getJobType() == MinervaJobType.DELETE_BACKGROUND) {
      final DeleteBackgroundMinervaJob parameters = new DeleteBackgroundMinervaJob(job.getJobParameters());

      self.removeBackground(parameters.getBackgroundId(), parameters.getDirectory());

    } else if (job.getJobType() == MinervaJobType.REVIVE_BACKGROUNDS) {
      final ReviveBackgroundsMinervaJob parameters = new ReviveBackgroundsMinervaJob(job.getJobParameters());

      self.reviveBackgrounds(parameters.getProjectId());

    } else {
      throw new NotImplementedException(job.getJobType() + " not supported");
    }

  }

  @Override
  public void addLogEntries(final String projectId, final Set<ProjectLogEntry> createLogEntries) {
    final Project project = self.getProjectByProjectId(projectId);
    project.addLogEntries(createLogEntries);
    self.update(project);
  }

  @Override
  public void submitArchiveProjectJob(final String projectId) {
    final Project project = self.getProjectByProjectId(projectId);
    final String homeDir = getProjectHomeDir(project);

    project.setStatus(ProjectStatus.ARCHIVED);

    for (final ProjectBackground background : project.getProjectBackgrounds()) {
      final DeleteBackgroundMinervaJob jobParameters = new DeleteBackgroundMinervaJob(background.getId(), homeDir);
      final MinervaJob job = new MinervaJob(MinervaJobType.DELETE_BACKGROUND, MinervaJobPriority.LOW, jobParameters);
      job.setExternalObject(project);
      minervaJobService.addJob(job);
    }
  }

  @Override
  public void submitAnnotateProjectJob(final String projectId, final UserAnnotationSchema schema) {
    UserAnnotationSchema userAnnotationSchema = schema;
    if (schema != null && schema.getId() != 0) {
      userAnnotationSchema = userAnnotationSchemaDao.getById(schema.getId());
    }
    Project project = self.getProjectByProjectId(projectId);
    final AnnotateProjectMinervaJob jobParameters = new AnnotateProjectMinervaJob(projectId, userAnnotationSchema);
    final MinervaJob job = new MinervaJob(MinervaJobType.ANNOTATE_PROJECT, MinervaJobPriority.MEDIUM, jobParameters);
    job.setExternalObject(project);
    minervaJobService.addJob(job);
  }

  @Override
  public String getProjectHomeDir(final Project project) {
    final String homeDir;
    final String minervaRoot = minervaConfigurationHolder.getDataPath();
    if (project.getDirectory() != null) {
      homeDir = minervaRoot + "/map_images/" + project.getDirectory() + "/";
    } else {
      homeDir = minervaRoot + "/map_images/";
    }
    return homeDir;
  }

  @Override
  public void submitReviveProjectJob(final String projectId) {
    final Project project = self.getProjectByProjectId(projectId);

    project.setStatus(ProjectStatus.GENERATING_IMAGES);

    final ReviveBackgroundsMinervaJob jobParameters = new ReviveBackgroundsMinervaJob(projectId);
    final MinervaJob job = new MinervaJob(MinervaJobType.REVIVE_BACKGROUNDS, MinervaJobPriority.MEDIUM, jobParameters);
    job.setExternalObject(project);
    minervaJobService.addJob(job);
  }

  @Override
  public void reviveBackgrounds(final String projectId) {
    try {
      final Project project = self.getProjectByProjectId(projectId);
      final CreateProjectParams params = new CreateProjectParams()
          .projectId(projectId)
          .setUser(project.getOwner())
          .projectDir(getProjectHomeDir(project))
          .images(true);

      self.addBuiltInBackgrounds(params);
      self.createImages(params);

      project.setStatus(ProjectStatus.DONE);
    } catch (final Exception e) {
      dbUtils.getSessionFactory().getCurrentSession().clear();
      self.projectFailure(projectId, ProjectLogEntryType.OTHER,
          "Severe problem with reviving project.\n"
              + "Underlying error:\n" + e.getMessage()
              + "\nMore information can be found in log file.",
          e);
    }
  }

  @Override
  public Page<Project> getAll(final Map<ProjectProperty, Object> projectFilter, final Pageable pageable) {
    final Page<Project> result = projectDao.getAll(projectFilter, pageable);
    for (final Project project : result.getContent()) {
      Hibernate.initialize(project.getLogEntries());
    }
    return result;
  }

  @Override
  public Page<ProjectLogEntry> getLogEntries(final Map<ProjectLogEntryProperty, Object> searchFilter, final Pageable pageable) {
    return projectLogEntryDao.getAll(searchFilter, pageable);
  }

  @Override
  public Project getById(final int id) {
    throw new NotImplementedException();
  }

  @Override
  public long getCount(final Map<ProjectProperty, Object> filterOptions) {
    throw new NotImplementedException();
  }

  private void initLazy(final Project project) {
    for (final ModelData model : project.getModels()) {
      Hibernate.initialize(model);
    }
    Hibernate.initialize(project.getTopModelData());
    Hibernate.initialize(project.getLogEntries());
  }

}
