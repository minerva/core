package lcsb.mapviewer.services.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.model.Stacktrace;
import lcsb.mapviewer.persist.dao.StacktraceDao;
import lcsb.mapviewer.services.interfaces.IStacktraceService;

@Transactional
@Service
public class StacktraceService implements IStacktraceService {

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  private StacktraceDao stacktraceDao;

  public StacktraceService(final StacktraceDao stacktraceDao) {
    this.stacktraceDao = stacktraceDao;
  }

  @Override
  public Stacktrace createFromStackTrace(final Exception e) {
    stacktraceDao.removeOld();
    return stacktraceDao.add(new Stacktrace(e));
  }

  @Override
  public Stacktrace getById(final String id) {
    return stacktraceDao.getById(id);
  }

}