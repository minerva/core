package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.map.CommentDao;
import lcsb.mapviewer.persist.dao.map.CommentProperty;
import lcsb.mapviewer.persist.dao.map.ModelDao;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.ICommentService;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.services.utils.EmailException;
import lcsb.mapviewer.services.utils.EmailSender;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This class is responsible for all services connected with comments
 * functionality.
 *
 * @author Piotr Gawron
 */

@Transactional
@Service
public class CommentService implements ICommentService {

  /**
   * Max distance between two points on map for which two comments should be
   * considered as referring to the same point.
   */
  private static final double COMMENT_POINT_DISTANCE_EPSILON = 0.02;

  /**
   * Default class logger.
   */
  private static final Logger logger = LogManager.getLogger();

  /**
   * Data access object fir comments.
   */
  private CommentDao commentDao;

  private final ModelDao mapDao;

  /**
   * Service for operations on Model object.
   */
  private IModelService modelService;

  /**
   * Service for operations on User object.
   */
  private IUserService userService;

  /**
   * Service for operations on Configuration object.
   */
  private IConfigurationService configurationService;

  @Autowired
  public CommentService(final CommentDao commentDao,
                        final IModelService modelService,
                        final IUserService userService,
                        final IConfigurationService configurationService,
                        final ModelDao mapDao) {
    this.commentDao = commentDao;
    this.modelService = modelService;
    this.userService = userService;
    this.configurationService = configurationService;
    this.mapDao = mapDao;
  }

  @Override
  public Comment addComment(final String email, final String content, final Point2D coordinates, final BioEntity object,
                            final boolean pinned, final int mapId, final User owner) {
    ModelData map = mapDao.getById(mapId);
    Comment comment = new Comment();
    comment.setEmail(email);
    comment.setContent(content);
    comment.setModel(map);
    comment.setCoordinates(coordinates);
    if (object instanceof Reaction) {
      comment.setReaction((Reaction) object);
    } else if (object instanceof Element) {
      comment.setElement((Element) object);
    }

    comment.setUser(owner);
    comment.setPinned(pinned);
    commentDao.add(comment);

    try {
      EmailSender emailSender = new EmailSender(configurationService);
      emailSender.sendEmail("New comment appeard in the " + map.getProject().getProjectId(), content, map.getProject().getNotifyEmail());
    } catch (final EmailException e) {
      logger.error("Problem with sending email.", e);
    }

    return comment;
  }

  @Override
  public void deleteComment(final User loggedUser, final String commentId, final String reason) {
    int id = -1;
    try {
      id = Integer.parseInt(commentId);

    } catch (final Exception e) {
      logger.error(e.getMessage(), e);
    }
    Comment comment = commentDao.getById(id);
    if (comment == null) {
      logger.error("Invalid comment id: " + commentId);
    } else {
      comment.setDeleted(true);
      comment.setRemoveReason(reason);
      commentDao.update(comment);
    }
  }

  @Override
  public void deleteComment(final Comment comment, final String reason) {
    comment.setDeleted(true);
    comment.setRemoveReason(reason);
    commentDao.update(comment);
  }

  @Override
  public List<Comment> getCommentsByModel(final Project project, final String mapId) throws HibernateException, ObjectNotFoundException {
    List<Comment> comments = new ArrayList<>();
    for (final ModelData model : modelService.getModelsByMapId(project.getProjectId(), mapId)) {
      List<Comment> modelComments = commentDao.getCommentByModel(model, null, null);
      for (Comment comment : modelComments) {
        Hibernate.initialize(comment.getUser());
      }
      comments.addAll(modelComments);
    }

    return comments;
  }

  @Override
  public long getCommentCount() {
    return commentDao.getCount();
  }

  @Override
  public void removeCommentsForModel(final Model model) {
    List<Comment> comments = commentDao.getCommentByModel(model, null, null);
    for (final Comment comment : comments) {
      commentDao.delete(comment);
    }
  }

  @Override
  public void removeCommentsForModel(final ModelData model) {
    List<Comment> comments = commentDao.getCommentByModel(model, null, null);
    for (final Comment comment : comments) {
      commentDao.delete(comment);
    }
  }

  @Override
  public Comment getCommentById(final String projectId, final String commentId) {
    int id;
    try {
      id = Integer.parseInt(commentId);
    } catch (final NumberFormatException e) {
      id = -1;
    }
    return getCommentById(projectId, id);
  }

  @Override
  public Comment getCommentById(final String projectId, final Integer commentId) {
    return commentDao.getByIdAndProjectId(commentId, projectId);
  }

  /**
   * @return the commentDao
   * @see #commentDao
   */
  public CommentDao getCommentDao() {
    return commentDao;
  }

  /**
   * @param commentDao the commentDao to set
   * @see #commentDao
   */
  public void setCommentDao(final CommentDao commentDao) {
    this.commentDao = commentDao;
  }

  /**
   * @return the modelService
   * @see #modelService
   */
  public IModelService getModelService() {
    return modelService;
  }

  /**
   * @param modelService the modelService to set
   * @see #modelService
   */
  public void setModelService(final IModelService modelService) {
    this.modelService = modelService;
  }

  /**
   * @return the userService
   * @see #userService
   */
  public IUserService getUserService() {
    return userService;
  }

  /**
   * @param userService the userService to set
   * @see #userService
   */
  public void setUserService(final IUserService userService) {
    this.userService = userService;
  }

  /**
   * @return the configurationService
   * @see #configurationService
   */
  public IConfigurationService getConfigurationService() {
    return configurationService;
  }

  /**
   * @param configurationService the configurationService to set
   * @see #configurationService
   */
  public void setConfigurationService(final IConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

  /**
   * Checks if identifier of the points refer to the same point.
   *
   * @param uniqueId identifier assigned by the java code
   * @param objectId identifier assigned by the javascript code (browser)
   * @return <code>true</code> if both identifiers refer to the same point,  <code>false</code> otherwise
   */
  protected boolean equalPoints(final String uniqueId, final String objectId) {
    String tmp = uniqueId.substring(uniqueId.indexOf("[") + 1, uniqueId.indexOf("]"));
    String[] ids = tmp.split(",");
    Double x1 = Double.valueOf(ids[0].trim());
    Double y1 = Double.valueOf(ids[1].trim());

    tmp = objectId.substring(objectId.indexOf("(") + 1, objectId.indexOf(")"));
    ids = tmp.split(",");
    Double x2 = Double.valueOf(ids[0].trim());
    Double y2 = Double.valueOf(ids[1].trim());

    return !(Math.abs(x1 - x2) > COMMENT_POINT_DISTANCE_EPSILON)
        && !(Math.abs(y1 - y2) > COMMENT_POINT_DISTANCE_EPSILON);
  }

  @Override
  public User getOwnerByCommentId(final String projectId, final String commentId) {
    Comment comment = getCommentById(projectId, commentId);
    if (comment == null || comment.getUser() == null) {
      return null;
    }
    Hibernate.initialize(comment.getUser());
    return comment.getUser();
  }

  @Override
  public Page<Comment> getAll(final Map<CommentProperty, Object> filter, final Pageable pageable) {
    Page<Comment> page = commentDao.getAll(filter, pageable);
    for (Comment comment : page.getContent()) {
      Hibernate.initialize(comment.getUser());
    }
    return page;
  }

  @Override
  public Comment getById(final int id) {
    return commentDao.getById(id);
  }

  @Override
  public void update(final Comment object) {
    throw new NotImplementedException();
  }

  @Override
  public void remove(final Comment comment) {
    comment.setDeleted(true);
    commentDao.update(comment);
  }

  @Override
  public long getCount(final Map<CommentProperty, Object> filterOptions) {
    throw new NotImplementedException();
  }

  @Override
  public void add(final Comment comment) {
    commentDao.add(comment);
  }

}
