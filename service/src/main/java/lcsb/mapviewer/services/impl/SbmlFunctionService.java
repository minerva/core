package lcsb.mapviewer.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.persist.dao.map.kinetics.SbmlFunctionDao;
import lcsb.mapviewer.persist.dao.map.kinetics.SbmlFunctionProperty;
import lcsb.mapviewer.services.interfaces.ISbmlFunctionService;

@Transactional
@Service
public class SbmlFunctionService implements ISbmlFunctionService {

  @Autowired
  private SbmlFunctionDao functionDao;

  @Override
  public List<SbmlFunction> getFunctionsByProjectId(final String projectId, final String mapId) {
    Map<SbmlFunctionProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(SbmlFunctionProperty.PROJECT_ID, projectId);
    if (!Objects.equals("*", mapId)) {
      filterOptions.put(SbmlFunctionProperty.MAP_ID, Integer.valueOf(mapId));
    }
    return functionDao.getAll(filterOptions, Pageable.unpaged()).getContent();
  }

  @Override
  public SbmlFunction getFunctionById(final String projectId, final int mapId, final int functionId) {
    Map<SbmlFunctionProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(SbmlFunctionProperty.ID, functionId);
    filterOptions.put(SbmlFunctionProperty.MAP_ID, mapId);
    filterOptions.put(SbmlFunctionProperty.PROJECT_ID, projectId);
    Page<SbmlFunction> page = functionDao.getAll(filterOptions, Pageable.unpaged());
    if (page.getNumberOfElements() == 0) {
      return null;
    }
    return page.getContent().get(0);
  }

  @Override
  public void update(final SbmlFunction function) {
    functionDao.update(function);
  }

  @Override
  public SbmlFunction getById(final int functionId) {
    return functionDao.getById(functionId);
  }

  @Override
  public void remove(final SbmlFunction function) {
    functionDao.delete(function);
  }

  @Override
  public void add(final SbmlFunction function) {
    functionDao.add(function);
  }

  @Override
  public Page<SbmlFunction> getAll(final Map<SbmlFunctionProperty, Object> filter, final Pageable pageable) {
    return functionDao.getAll(filter, pageable);
  }

  @Override
  public long getCount(final Map<SbmlFunctionProperty, Object> filterOptions) {
    throw new NotImplementedException();
  }
}
