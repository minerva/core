package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.model.overlay.DataOverlayGroup;
import lcsb.mapviewer.persist.dao.map.DataOverlayGroupDao;
import lcsb.mapviewer.persist.dao.map.DataOverlayGroupProperty;
import lcsb.mapviewer.services.interfaces.IDataOverlayGroupService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Transactional
@Service
public class DataOverlayGroupService implements IDataOverlayGroupService {

  private final Logger logger = LogManager.getLogger();

  @Autowired
  private DataOverlayGroupDao dataOverlayGroupDao;

  public DataOverlayGroupService() {
  }

  @Override
  public Page<DataOverlayGroup> getAll(final Map<DataOverlayGroupProperty, Object> filter, final Pageable pageable) {
    return dataOverlayGroupDao.getAll(filter, pageable);
  }

  @Override
  public DataOverlayGroup getById(final int id) {
    return dataOverlayGroupDao.getById(id);
  }

  @Override
  public void update(final DataOverlayGroup object) {
    dataOverlayGroupDao.update(object);
  }

  @Override
  public void remove(final DataOverlayGroup object) {
    dataOverlayGroupDao.delete(object);
  }

  @Override
  public long getCount(final Map<DataOverlayGroupProperty, Object> filterOptions) {
    return dataOverlayGroupDao.getCount(filterOptions);
  }

  @Override
  public void add(final DataOverlayGroup object) {
    dataOverlayGroupDao.add(object);
  }
}
