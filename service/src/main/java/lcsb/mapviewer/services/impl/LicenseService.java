package lcsb.mapviewer.services.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.model.License;
import lcsb.mapviewer.persist.dao.LicenseDao;
import lcsb.mapviewer.services.interfaces.ILicenseService;

@Transactional
@Service
public class LicenseService implements ILicenseService {

  protected static Logger logger = LogManager.getLogger();

  @Autowired
  private LicenseDao licenseDao;

  @Override
  public Page<License> getByFilter(final Pageable pageable) {
    return licenseDao.getByFilter(pageable);
  }

  @Override
  public License getById(final int id) {
    return licenseDao.getById(id);
  }
}
