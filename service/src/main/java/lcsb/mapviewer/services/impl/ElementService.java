package lcsb.mapviewer.services.impl;

import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.map.species.ElementDao;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IModelService;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.TreeMap;

@Transactional
@Service
public class ElementService implements IElementService {

  @Autowired
  private IModelService modelService;

  @Autowired
  private ElementDao elementDao;

  /*
  @Override
  public Element getElementById(final String projectId, final int mapId, final int elementId) throws ObjectNotFoundException {
    final ModelData map = modelService.getModelByMapId(projectId, mapId);
    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.ID, Collections.singletonList(elementId));
    properties.put(ElementProperty.MAP, Collections.singletonList(map));
    final Page<Element> elements = elementDao.getAll(properties, Pageable.unpaged());
    if (elements.getNumberOfElements() > 0) {
      return elements.getContent().get(0);
    }
    throw new ObjectNotFoundException("Element does not exist");
  }
*/
  @Override
  public Page<Element> getAll(final Map<ElementProperty, Object> filter, final Pageable pageable) {
    return elementDao.getAll(filter, pageable);
  }

  @Override
  public Element getById(final int id) {
    return elementDao.getById(id);
  }

  @Override
  public Page<Element> getElementsByFilter(final Map<ElementProperty, Object> properties, final Pageable pageable,
                                           final boolean initializeLazy) {
    final Page<Element> result = elementDao.getAll(properties, pageable);
    if (initializeLazy) {
      for (final Element element : result) {
        initializeLazy(element);
      }
    }

    return result;
  }

  private void initializeLazy(final Element element) {
    if (element.getSubmodel() != null) {
      Hibernate.initialize(element.getSubmodel().getSubmodel());
    }
    Hibernate.initialize(element.getGlyph());
    Hibernate.initialize(element.getModel());
  }

  @Override
  public Map<MiriamType, Integer> getAnnotationStatistics(final String projectId) {
    final Map<MiriamType, Integer> elementAnnotations = new TreeMap<>();
    final Map<MiriamType, Integer> data = elementDao.getAnnotationStatistics(projectId);
    for (final MiriamType mt : MiriamType.values()) {
      if (data.get(mt) != null) {
        elementAnnotations.put(mt, data.get(mt));
      } else {
        elementAnnotations.put(mt, 0);
      }
    }
    return elementAnnotations;
  }

  @Override
  public void add(final Element element) {
    elementDao.add(element);
  }

  @Override
  public void update(final Element element) {
    elementDao.update(element);
  }

  @Override
  public void remove(final Element element) {
    elementDao.delete(element);
  }

  @Override
  public long getCount(final Map<ElementProperty, Object> filterOptions) {
    return elementDao.getCount(filterOptions);
  }

}
