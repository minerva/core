package lcsb.mapviewer.services.impl;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Custom password encoder using MD5 hash. Used for compatibility reasons.
 * 
 * @author Piotr Gawron
 *
 */
public class CustomMd5PasswordEncoder implements PasswordEncoder {

  /**
   * Transforms {@link CharSequence} into byte array.
   *
   * @param string
   *          input {@link CharSequence}
   * @return byte array representation of the input {@link CharSequence}
   */
  private static byte[] encodeUtf8(final CharSequence string) {
    ByteBuffer bb = Charset.forName("UTF-8").encode(CharBuffer.wrap(string));
    byte[] result = new byte[bb.remaining()];
    bb.get(result);
    return result;
  }

  @Override
  public String encode(final CharSequence password) {

    try {
      MessageDigest md = MessageDigest.getInstance("MD5");
      byte[] thedigest = md.digest(encodeUtf8(password));
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < thedigest.length; ++i) {
        // this line transforms single byte into hex representation
        sb.append(Integer.toHexString((thedigest[i] & 0xFF) | 0x100).substring(1, 3));
      }
      return sb.toString();
    } catch (final NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public boolean matches(final CharSequence arg0, final String arg1) {
    return encode(arg0).equals(arg1);
  }
}
