package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

public interface IElementService extends CrudService<Element, ElementProperty> {
  Page<Element> getElementsByFilter(Map<ElementProperty, Object> properties, Pageable pageable, final boolean initializeLazy);

  Map<MiriamType, Integer> getAnnotationStatistics(final String projectId);

}
