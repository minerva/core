package lcsb.mapviewer.services.interfaces;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import lcsb.mapviewer.model.License;

public interface ILicenseService {

  Page<License> getByFilter(final Pageable pageable);

  License getById(int id);
}
