package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.annotation.data.MeSH;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.model.map.MiriamData;

public interface IMeshService {

  MeSH getMesh(final MiriamData miriamData) throws AnnotatorException;

  boolean isValidMeshId(final MiriamData sourceData) throws AnnotatorException;
}
