package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.persist.dao.map.species.ModificationResidueProperty;

/**
 * Service that manages projects.
 *
 * @author Piotr Gawron
 */
public interface IModificationResidueService extends CrudService<ModificationResidue, ModificationResidueProperty> {

}
