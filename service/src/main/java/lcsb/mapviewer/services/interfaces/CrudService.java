package lcsb.mapviewer.services.interfaces;

import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.persist.dao.MinervaEntityProperty;

public interface CrudService<T extends MinervaEntity, S extends MinervaEntityProperty<T>> {

  @Transactional
  Page<T> getAll(Map<S, Object> filter, Pageable pageable);

  @Transactional
  T getById(int id);

  @Transactional
  void update(T object);

  @Transactional
  void remove(T object);

  @Transactional
  long getCount(final Map<S, Object> filterOptions);

  @Transactional
  void add(T object);

}
