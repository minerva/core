package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.persist.dao.graphics.PolylineDataProperty;

public interface ILayerLineService extends CrudService<PolylineData, PolylineDataProperty> {

}
