package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.persist.dao.map.AuthorProperty;

public interface IAuthorService extends CrudService<Author, AuthorProperty> {

}
