package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.annotation.services.TaxonomySearchException;
import lcsb.mapviewer.model.map.MiriamData;

public interface ITaxonomyService {

  String getNameForTaxonomy(final MiriamData miriamData) throws TaxonomySearchException;

}
