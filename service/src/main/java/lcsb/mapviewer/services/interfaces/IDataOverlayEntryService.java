package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.persist.dao.map.DataOverlayEntryProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

public interface IDataOverlayEntryService extends CrudService<DataOverlayEntry, DataOverlayEntryProperty> {

  Page<DataOverlayEntry> getAll(Map<DataOverlayEntryProperty, Object> filter, Pageable pageable, boolean initLazy);

  DataOverlayEntry getById(int id, boolean initLazy);
}
