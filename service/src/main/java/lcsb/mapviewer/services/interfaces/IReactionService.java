package lcsb.mapviewer.services.interfaces;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.persist.dao.map.ReactionProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;

public interface IReactionService extends CrudService<Reaction, ReactionProperty> {

  Reaction getReactionById(final String projectId, final int mapId, final int elementId) throws ObjectNotFoundException;

  List<Reaction> getReactionById(final String projectId, final String modelId, final Set<Integer> ids, final Set<Integer> participantIds)
      throws ObjectNotFoundException;

  Map<MiriamType, Integer> getAnnotationStatistics(final String projectId);

  Page<Reaction> getAll(Map<ReactionProperty, Object> filter, Pageable pageable, final boolean initializeLazy);

}
