package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.annotation.services.PubmedSearchException;
import lcsb.mapviewer.model.Article;
import lcsb.mapviewer.model.job.MinervaJobExecutor;
import lcsb.mapviewer.model.map.MiriamData;

public interface IMiriamService extends MinervaJobExecutor {

  String getUrlString(final MiriamData miriamData);

  Article getArticle(final MiriamData miriamData) throws PubmedSearchException;

  MiriamData getById(int id);

  void add(MiriamData md);

  void update(MiriamData md);

  void delete(MiriamData md);
}
