package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.EmailConfirmationToken;
import lcsb.mapviewer.model.user.ResetPasswordToken;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.user.UserProperty;
import lcsb.mapviewer.services.InvalidTokenException;
import lcsb.mapviewer.services.utils.EmailException;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface IUserService extends CrudService<User, UserProperty> {

  User getUserByLogin(final String login);

  User getUserByLogin(final String login, final boolean loadLazy);

  List<User> getUsers(final boolean loadLazy);

  void grantUserPrivilege(final User user, final PrivilegeType type);

  void grantUserPrivilege(final User user, final PrivilegeType type, final String objectId);

  void revokeUserPrivilege(final User user, final PrivilegeType type);

  void revokeUserPrivilege(final User user, final PrivilegeType type, final String objectId);

  void grantDefaultPrivileges(final User user);

  void grantPrivilegeToAllUsersWithDefaultAccess(final PrivilegeType type, final String objectId);

  /**
   * When an object is deleted we have to manually remove the access rights to it
   * for every user to avoid pollution. Hibernate delete cascading does not work
   * here, because security is completely decoupled from the business logic.
   *
   * @param privilegeType The concerned privilege domain.
   * @param objectId      The objectId for which to remove all access rights.
   */
  void revokeObjectDomainPrivilegesForAllUsers(final PrivilegeType privilegeType, final String objectId);

  /**
   * Returns {@link ColorExtractor} that transform overlay values into colors for
   * given user.
   *
   * @param user {@link User} for which {@link ColorExtractor} will be obtained
   * @return {@link ColorExtractor} that transform overlay values into colors for given user
   */
  ColorExtractor getColorExtractorForUser(final User user);

  Map<String, Boolean> ldapAccountExistsForLogin(final Collection<User> logins);

  void createResetPasswordToken(final User user) throws EmailException;

  void resetPassword(final String token, final String cryptedPassword) throws InvalidTokenException;

  long getPasswordTokenCount();

  void add(ResetPasswordToken token);

  void add(EmailConfirmationToken token);

  List<ResetPasswordToken> getPasswordTokens(String login);

  User getUserByOrcidId(String oidcId);

  User registerUser(User user) throws EmailException;

  void createRegisterUserToken(User user) throws EmailException;

  User confirmEmail(String login, String token) throws InvalidTokenException;

  void sendUserActivatedEmail(User user);

  void moveProjectPrivileges(String oldProjectId, String projectId);
}