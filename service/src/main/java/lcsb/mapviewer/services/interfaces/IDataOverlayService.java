package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.FileEntry;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.overlay.DataOverlayType;
import lcsb.mapviewer.model.overlay.InvalidDataOverlayException;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.map.DataOverlayProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Set;

public interface IDataOverlayService extends CrudService<DataOverlay, DataOverlayProperty> {

  class CreateDataOverlayParams {

    private static final int BUFFER_SIZE = 1024;

    private String name;

    private String description;

    private Project project;

    private User user;

    /**
     * Input stream with coloring information data in the stream should correspond
     * to a file that can be parsed by
     * {@link lcsb.mapviewer.converter.ColorSchemaReader ColorSchemaReader}
     * . This is a copy of original input stream, so we can read this input stream
     * multiple times.
     */
    private ByteArrayOutputStream colorInputStreamCopy;

    private String inputFileName;

    private DataOverlayType colorSchemaType;

    public String getName() {
      return name;
    }

    public CreateDataOverlayParams name(final String name) {
      this.name = name;
      return this;
    }

    public Project getProject() {
      return project;
    }

    public CreateDataOverlayParams project(final Project project) {
      this.project = project;
      return this;
    }

    public User getUser() {
      return user;
    }

    public CreateDataOverlayParams user(final User user) {
      this.user = user;
      return this;
    }

    public InputStream getColorInputStream() {
      return new ByteArrayInputStream(colorInputStreamCopy.toByteArray());
    }

    public CreateDataOverlayParams colorInputStream(final InputStream colorInputStream) throws IOException {
      if (colorInputStream == null) {
        this.colorInputStreamCopy = null;
      } else {
        this.colorInputStreamCopy = new ByteArrayOutputStream();

        final byte[] buffer = new byte[BUFFER_SIZE];
        int len;
        while ((len = colorInputStream.read(buffer)) > -1) {
          this.colorInputStreamCopy.write(buffer, 0, len);
        }
        this.colorInputStreamCopy.flush();
      }
      return this;
    }

    public String getDescription() {
      return description;
    }

    public CreateDataOverlayParams description(final String description) {
      this.description = description;
      return this;
    }

    public DataOverlayType getColorSchemaType() {
      return colorSchemaType;
    }

    public CreateDataOverlayParams colorSchemaType(final DataOverlayType colorSchemaType) {
      this.colorSchemaType = colorSchemaType;
      return this;
    }

    public String getFileName() {
      return inputFileName;
    }

    public CreateDataOverlayParams fileName(final String fileName) {
      this.inputFileName = fileName;
      return this;
    }
  }

  User getOverlayCreator(final Integer overlayId);

  DataOverlay createDataOverlay(final CreateDataOverlayParams params) throws IOException, InvalidDataOverlayException, ObjectNotFoundException;

  DataOverlay getDataOverlayById(final Integer overlayId);

  DataOverlay getDataOverlayById(final String projectId, final Integer overlayId) throws ObjectNotFoundException;

  List<DataOverlay> getDataOverlaysByProject(final Project project);

  List<DataOverlay> getDataOverlaysByProject(final Project project, final boolean loadLazy);

  void removeDataOverlay(final DataOverlay dataOverlay);

  void updateDataOverlay(final DataOverlay dataOverlay);

  List<Pair<Reaction, DataOverlayEntry>> getFullReactionForDataOverlay(final String projectId,
                                                                       final int mapId,
                                                                       final Integer id,
                                                                       final int overlayId, final boolean fetchLazy) throws ObjectNotFoundException;

  List<Pair<Element, DataOverlayEntry>> getFullElementForDataOverlay(final String projectId,
                                                                     final int mapId,
                                                                     final Integer id,
                                                                     final int overlayId, final boolean fetchLazy) throws ObjectNotFoundException;

  List<Pair<? extends BioEntity, DataOverlayEntry>> getBioEntitiesForDataOverlay(final String projectId,
                                                                                 final int mapId,
                                                                                 final Integer overlayId,
                                                                                 final boolean fetchLazy,
                                                                                 boolean includeIndirect) throws ObjectNotFoundException;

  List<Pair<? extends BioEntity, DataOverlayEntry>> getBioEntitiesForDataOverlay(final String projectId,
                                                                                 final int mapId,
                                                                                 final Integer overlayId,
                                                                                 final boolean fetchLazy) throws ObjectNotFoundException;

  List<Pair<Element, DataOverlayEntry>> getElementReferencesForDataOverlay(final int overlayId,
                                                                           final List<ModelData> models);

  List<Pair<Reaction, DataOverlayEntry>> getReactionReferencesForDataOverlay(final int overlayId,
                                                                             final List<ModelData> models);

  void add(final DataOverlay overlay);

  FileEntry getDataOverlayFileById(final String projectId, final Integer overlayId) throws ObjectNotFoundException;

  byte[] generateLegend(String projectId, Integer overlayId, ColorExtractor colorExtractor) throws ObjectNotFoundException, IOException;

  Set<DataOverlayEntry> getDataOverlayEntriesById(String projectId, Integer overlayId) throws ObjectNotFoundException;
}
