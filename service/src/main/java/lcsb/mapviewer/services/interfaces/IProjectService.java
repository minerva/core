package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectLogEntry;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.ProjectStatus;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.job.MinervaJob;
import lcsb.mapviewer.model.job.MinervaJobExecutor;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.layout.ProjectBackground;
import lcsb.mapviewer.model.overlay.InvalidDataOverlayException;
import lcsb.mapviewer.model.user.UserAnnotationSchema;
import lcsb.mapviewer.persist.dao.ProjectLogEntryProperty;
import lcsb.mapviewer.persist.dao.ProjectProperty;
import lcsb.mapviewer.services.utils.CreateProjectParams;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Service that manages projects.
 *
 * @author Piotr Gawron
 */
public interface IProjectService extends MinervaJobExecutor, CrudService<Project, ProjectProperty> {

  @Transactional
  Project getProjectByProjectId(final String projectId);

  @Transactional
  Project getProjectByProjectId(final String projectId, final boolean initializeLazy);

  /**
   * Checks if project with a given {@link Project#getProjectId()  identifier} exists.
   */
  @Transactional
  boolean projectExists(final String projectId);

  /**
   * Returns list of all projects.
   *
   * @return list of all projects.
   */
  @Transactional
  List<Project> getAllProjects();

  @Transactional
  List<Project> getAllProjects(boolean initializeLazy);

  /**
   * Removes project from the system.
   *
   * @param project object to remove
   */
  @Transactional
  MinervaJob submitRemoveProjectJob(final Project project);

  /**
   * Creates project using give parameters. See {@link CreateProjectParams}.
   *
   * @param params information about project to create
   */
  @Transactional
  MinervaJob submitCreateProjectJob(final CreateProjectParams params) throws SecurityException;

  Project createProject(final CreateProjectParams params) throws SecurityException;

  @Transactional
  void removeBackground(final ProjectBackground projectBackground);

  @Transactional
  void removeBackground(int backgroundId, String path);

  @Transactional
  ProjectBackground getBackgroundById(final String projectId, final Integer backgroundId, final boolean initializeLazy);

  @Transactional
  void updateBackground(final ProjectBackground background);

  @Transactional
  long getNextId();

  @Transactional
  UploadedFileEntry getFileByProjectId(final String projectId);

  @Transactional
  List<ProjectBackground> getBackgrounds(final String projectId, final boolean initializeLazy);

  @Transactional
  void addLogEntry(final Project p, final ProjectLogEntry entry);

  @Transactional
  MinervaJob projectFailure(final String projectId, final ProjectLogEntryType errorType, final String message, final Throwable e);

  @Transactional
  void removeProject(String projectId, String directory);

  Project createModel(CreateProjectParams params)
      throws InvalidInputDataExecption, ConverterException, IOException, InvalidDataOverlayException, InstantiationException, IllegalAccessException,
      CommandExecutionException;

  /**
   * This method creates set of images for the model backgrounds.
   *
   * @param params configuration parameters including set of backgrounds to generate
   * @throws IOException               thrown when there are problems with generating files
   * @throws DrawingException          thrown when there was a problem with drawing a map
   * @throws CommandExecutionException thrown when one of the files describing backgrounds is invalid
   */
  @Transactional
  void createImages(CreateProjectParams params) throws IOException, DrawingException, CommandExecutionException;

  /**
   * Sends email about unsuccessful project creation.
   *
   * @param projectName name of the project
   * @param email       email where we want to send information
   * @param e           exception that caused problem
   */
  @Transactional
  void sendUnsuccesfullEmail(String projectName, String email, Throwable e);

  /**
   * Updates status of the generating project.
   *
   * @param projectId project that is generated
   * @param status    what is the current status
   * @param progress  what is the progress
   */
  @Transactional
  void updateProjectStatus(String projectId, ProjectStatus status, double progress);

  @Transactional
  void addLogEntries(String projectId, Set<ProjectLogEntry> createLogEntries);

  @Transactional
  void submitArchiveProjectJob(String projectId);

  String getProjectHomeDir(Project project);

  @Transactional
  void submitReviveProjectJob(String projectId);

  @Transactional
  void reviveBackgrounds(String projectId);

  @Transactional
  Set<MiriamData> getMiriamForProject(String projectId);

  @Transactional
  List<MiriamData> getMiriamWithDuplicatesForProject(String projectId);

  @Transactional
  Page<ProjectLogEntry> getLogEntries(Map<ProjectLogEntryProperty, Object> searchFilter, Pageable pageable);

  @Transactional
  void addBuiltInBackgrounds(final CreateProjectParams params) throws InvalidInputDataExecption;

  @Transactional
  void validateDataOverlays(final String projectId) throws IOException, InvalidDataOverlayException;

  @Transactional
  void createHierarchicalView(final String projectId) throws CommandExecutionException;

  @Transactional
  void createComplexProject(CreateProjectParams params) throws InvalidInputDataExecption;

  @Transactional
  void createSimpleProject(CreateProjectParams params) throws ConverterException;

  @Transactional
  void submitAnnotateProjectJob(final String projectId, final UserAnnotationSchema schema);

  void submitRefreshChemicalInfoJobs(final String projectId);
}
