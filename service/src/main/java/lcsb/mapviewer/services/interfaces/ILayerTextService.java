package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.persist.dao.graphics.LayerTextProperty;

public interface ILayerTextService extends CrudService<LayerText, LayerTextProperty> {

}
