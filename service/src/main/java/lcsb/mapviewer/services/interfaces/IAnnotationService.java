package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.job.MinervaJobExecutor;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.model.user.UserAnnotationSchema;
import lcsb.mapviewer.services.jobs.AnnotateProjectMinervaJob;
import org.springframework.transaction.annotation.Transactional;

import java.util.function.Consumer;

public interface IAnnotationService extends MinervaJobExecutor {

  @Transactional
  UserAnnotationSchema prepareUserAnnotationSchema(final User user, final boolean initializeLazy);

  void handleAnnotateProjectJob(final AnnotateProjectMinervaJob parameters, Consumer<Double> updateProgress);

}
