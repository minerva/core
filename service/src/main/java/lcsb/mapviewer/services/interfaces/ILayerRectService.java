package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.persist.dao.graphics.LayerRectProperty;

public interface ILayerRectService extends CrudService<LayerRect, LayerRectProperty> {

}
