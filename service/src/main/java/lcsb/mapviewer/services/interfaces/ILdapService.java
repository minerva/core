package lcsb.mapviewer.services.interfaces;

import java.util.List;

import com.unboundid.ldap.sdk.LDAPException;

import lcsb.mapviewer.model.user.ConfigurationElementTypeGroup;
import lcsb.mapviewer.services.UserDTO;

/**
 * Connection service to LDAP server.
 * 
 * @author Piotr Gawron
 *
 */
public interface ILdapService {

  /**
   * Checks if login and password match
   * 
   * @param login
   *          user login
   * @param password
   *          password
   * @return true if user login/password match
   * @throws LDAPException
   *           thrown when there is problem with LDAP connection
   */
  boolean login(final String login, final String password) throws LDAPException;

  /**
   * Returns list of user names available in the LDAP server.
   * 
   * @return list of user names
   * @throws LDAPException
   *           thrown when there is problem with LDAP connection
   */
  List<String> getUsernames() throws LDAPException;

  /**
   * Returns user data information from LDAP for given login.
   * 
   * @param login
   *          user for which we obtain data
   * @return user data information from LDAP for given login or null if such user
   *         doesn't exist
   * @throws LDAPException
   *           thrown when there is problem with LDAP connection
   */
  UserDTO getUserByLogin(final String login) throws LDAPException;

  /**
   * Checks if LDAP configuration
   * ({@link ConfigurationElementTypeGroup#LDAP_CONFIGURATION}) is valid.
   * 
   * @return true if LDAP configuration
   *         ({@link ConfigurationElementTypeGroup#LDAP_CONFIGURATION}) is valid
   */
  boolean isValidConfiguration();

}
