package lcsb.mapviewer.services.interfaces;

import java.util.List;

import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.ConfigurationOption;

/**
 * Service used for accessing and modifying configuration parameters.
 * 
 * @author Piotr Gawron
 * 
 */
public interface IConfigurationService {

  /**
   * Returns value of the configuration parameter.
   * 
   * @param type
   *          configuration parameter
   * @return value of the configuration parameter given in type
   */
  String getConfigurationValue(final ConfigurationElementType type);

  /**
   * Sets configuration parameter.
   * 
   * @param type
   *          which configuration parameter
   * @param value
   *          what value
   */
  void setConfigurationValue(final ConfigurationElementType type, final String value);

  /**
   * Removes configuration parameter.
   * 
   * @param type
   *          which configuration parameter
   */
  void deleteConfigurationValue(final ConfigurationElementType type);

  /**
   * Returns information about all configuration parameters with current values.
   * 
   * @return list of all configuration parameters with current values
   */
  List<ConfigurationOption> getAllValues();

  /**
   * Updates configuration parameters.
   * 
   * @param values
   *          new values of the parameters
   */
  void updateConfiguration(final List<ConfigurationOption> values);

  /**
   * Returns {@link lcsb.mapviewer.common.Configuration#systemVersion git version}
   * from which the system was built.
   * 
   * @return git version
   */
  String getSystemSvnVersion();

  /**
   * Returns estimated current memory usage (in MB).
   * 
   * @return estimated current memory usage (in MB)
   */
  Long getMemoryUsage();

  /**
   * Returns max memory available to JVM (in MB).
   * 
   * @return max memory available to JVM (in MB)
   */
  Long getMaxMemory();

  /**
   * Returns system {@link lcsb.mapviewer.common.Configuration#systemBuild build
   * date}.
   * 
   * @return system build date
   */
  String getSystemBuild();

  /**
   * Returns {@link lcsb.mapviewer.common.Configuration#systemBuildVersion git
   * hash} of build process.
   * 
   * @return git hash
   */
  String getSystemGitVersion();

  ConfigurationOption getValue(final ConfigurationElementType type);

  ConfigurationOption getValue(final PrivilegeType type);
}
