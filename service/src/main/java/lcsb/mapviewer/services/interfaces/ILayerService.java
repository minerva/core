package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.persist.dao.graphics.LayerProperty;

public interface ILayerService extends CrudService<Layer, LayerProperty> {

}
