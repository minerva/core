package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.map.ModelProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;

import java.util.List;

/**
 * Service that manages models (maps).
 *
 * @author Piotr Gawron
 */
public interface IModelService extends CrudService<ModelData, ModelProperty> {

  List<ModelData> getModelsByMapId(final String projectId, final String mapId) throws ObjectNotFoundException;

  ModelData getModelByMapId(final String projectId, final int mapId) throws ObjectNotFoundException;

  Model getAndFetchModelByMapId(final String projectId, final int mapId) throws ObjectNotFoundException;

  int getPublicationCount(final String projectId) throws ObjectNotFoundException;
}
