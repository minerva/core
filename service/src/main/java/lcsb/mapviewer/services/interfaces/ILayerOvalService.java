package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.persist.dao.graphics.LayerOvalProperty;

public interface ILayerOvalService extends CrudService<LayerOval, LayerOvalProperty> {

}
