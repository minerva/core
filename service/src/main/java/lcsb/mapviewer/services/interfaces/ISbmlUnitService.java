package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.persist.dao.map.kinetics.SbmlUnitProperty;

public interface ISbmlUnitService extends CrudService<SbmlUnit, SbmlUnitProperty> {

}
