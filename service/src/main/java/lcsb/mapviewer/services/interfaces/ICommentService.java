package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.map.CommentProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import org.hibernate.HibernateException;

import java.awt.geom.Point2D;
import java.util.List;

/**
 * Service responsible for comments functionality.
 *
 * @author Piotr Gawron
 */
public interface ICommentService extends CrudService<Comment, CommentProperty> {
  /**
   * Adds comment to the map.
   *
   * @param pinned      parameter that defines if comment should be visible on the map.
   * @param bioEntity   determines which object is commented - it could be null; in this
   *                    case it means that a comment is general one.
   * @param email       email of the person that comments
   * @param content     content of the comment
   * @param mapId       on which submodel we comment on
   * @param coordinates where exactly the comment should be placed on
   * @return comment object created based on the data provided as parameters
   */
  Comment addComment(final String email, final String content, final Point2D coordinates, final BioEntity bioEntity,
                     final boolean pinned, final int mapId, final User owner);

  /**
   * This method remove comment. Comment is not removed from the system, only
   * 'deleted' flag is set.
   *
   * @param loggedUser user that wants to remove the comment
   * @param commentId  identifier of the comment that user wants to remove
   * @param reason     why user wants to remove the comment
   */
  void deleteComment(final User loggedUser, final String commentId, final String reason);

  void deleteComment(final Comment comment, final String reason);

  List<Comment> getCommentsByModel(final Project project, final String mapId) throws HibernateException, ObjectNotFoundException;

  /**
   * Returns number of comments in the system.
   *
   * @return number of comments in the system
   */
  long getCommentCount();

  /**
   * Removes comments from the model.
   *
   * @param model from which model we want to remove comments
   */
  void removeCommentsForModel(final Model model);

  /**
   * Removes comments from the model.
   *
   * @param model from which model we want to remove comments
   */
  void removeCommentsForModel(final ModelData model);

  Comment getCommentById(final String projectId, final String commentId);

  Comment getCommentById(final String projectId, final Integer commentId);

  User getOwnerByCommentId(final String projectId, final String commentId);
}
