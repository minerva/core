package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.overlay.DataOverlayGroup;
import lcsb.mapviewer.persist.dao.map.DataOverlayGroupProperty;

public interface IDataOverlayGroupService extends CrudService<DataOverlayGroup, DataOverlayGroupProperty> {
}
