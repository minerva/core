package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.persist.dao.graphics.GlyphProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

public interface IGlyphService extends CrudService<Glyph, GlyphProperty> {
  Page<Glyph> getAll(Map<GlyphProperty, Object> filterOptions, Pageable pageable, boolean initializeLazy);
}
