package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.persist.dao.map.kinetics.SbmlParameterProperty;

public interface ISbmlParameterService extends CrudService<SbmlParameter, SbmlParameterProperty> {

}
