package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.persist.dao.map.BioEntityProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.utils.BioEntitySearchResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Service that gives access to search functionalities.
 *
 * @author Piotr Gawron
 */
public interface ISearchService {

  /**
   * Search for elements on the map by query. Returns many possibilities from best
   * matching to less matching possibility.
   *
   * @param projectId    projectId on which we perform search
   * @param query        the query
   * @param limit        maximum number of results
   * @param perfectMatch should the match be perfect
   * @param ipAddress    ip address of a client who is searching (used for logging purpose)
   * @return list of objects that matches the query sorted by the match quality
   * @throws ObjectNotFoundException thrown when map does not exist
   */
  List<BioEntity> searchByQuery(final String projectId, final String mapId, final String query, final int limit, final boolean perfectMatch,
                                final String ipAddress) throws ObjectNotFoundException;

  List<BioEntity> searchByQuery(final String projectId, final String mapId, final String query, final int limit, final boolean perfectMatch)
      throws ObjectNotFoundException;

  /**
   * Returns the closest elements to the coordinates on the model.
   *
   * @param projectId        projectId on which the search is performed
   * @param point            coordinates where search is performed
   * @param numberOfElements how many closest elements should be returned
   * @return list of the closest elements
   * @throws ObjectNotFoundException thrown when map does not exist
   */
  List<BioEntity> getClosestElements(final String projectId, final int mapId, final Point2D point, final int numberOfElements,
                                     final boolean perfectHit,
                                     final Collection<String> types) throws ObjectNotFoundException;

  List<String> getSuggestedQueryList(final String projectId) throws ObjectNotFoundException;

  List<String> getSuggestedQueryList(final String projectId, String mapId) throws ObjectNotFoundException;

  Page<BioEntitySearchResult> getBioEntitesByFilter(Map<BioEntityProperty, Object> properties, Pageable pageable, boolean b)
      throws ObjectNotFoundException;

}
