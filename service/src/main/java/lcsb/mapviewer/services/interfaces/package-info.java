/**
 * This package contains interfaces to all services provided by the service
 * layer.
 */
package lcsb.mapviewer.services.interfaces;
