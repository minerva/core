package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.Stacktrace;

public interface IStacktraceService {

  Stacktrace createFromStackTrace(Exception e);

  Stacktrace getById(String id);
}
