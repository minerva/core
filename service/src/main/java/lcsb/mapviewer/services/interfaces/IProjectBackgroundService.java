package lcsb.mapviewer.services.interfaces;

import java.util.List;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.ProjectBackground;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.utils.EmailSender;

/**
 * Service that manages layouts of the map.
 *
 * @author Piotr Gawron
 *
 */
public interface IProjectBackgroundService {

  void updateProjectBackground(final ProjectBackground background);

  EmailSender getEmailSender();

  void setEmailSender(final EmailSender emailSender);

  List<ProjectBackground> getProjectBackgroundsByProject(final Project project);

  List<ProjectBackground> getProjectBackgroundsByProject(final String projectId);

  ProjectBackground getProjectBackgroundById(final Integer backgroundId);

  ProjectBackground getProjectBackgroundById(final String projectId, final Integer backgroundId) throws ObjectNotFoundException;

  List<ProjectBackground> getProjectBackgroundsByUser(final User user);

  void delete(ProjectBackground background);

}
