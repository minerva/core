package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;

public interface IPrivilegeService {

  Privilege getPrivilege(final PrivilegeType type);

  Privilege getPrivilege(final PrivilegeType type, final String objectId);

  void updatePrivilege(final Privilege privilege);

}
