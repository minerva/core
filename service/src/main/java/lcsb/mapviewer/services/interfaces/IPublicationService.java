package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.Article;
import lcsb.mapviewer.persist.dao.ArticleProperty;

public interface IPublicationService extends CrudService<Article, ArticleProperty> {

  void add(Article article);
}
