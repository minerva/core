package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.map.layout.graphics.LayerImage;
import lcsb.mapviewer.persist.dao.graphics.LayerImageProperty;

public interface ILayerImageService extends CrudService<LayerImage, LayerImageProperty> {

}
