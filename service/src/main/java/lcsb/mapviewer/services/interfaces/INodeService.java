package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.map.reaction.AbstractNode;

public interface INodeService {
  void remove(AbstractNode object);

  void update(AbstractNode input);

  AbstractNode getById(int id);
}
