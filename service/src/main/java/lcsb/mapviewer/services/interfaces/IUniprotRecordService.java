package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.map.species.field.UniprotRecord;
import lcsb.mapviewer.persist.dao.map.species.UniprotRecordProperty;

/**
 * Service that manages projects.
 *
 * @author Piotr Gawron
 */
public interface IUniprotRecordService extends CrudService<UniprotRecord, UniprotRecordProperty> {

}
