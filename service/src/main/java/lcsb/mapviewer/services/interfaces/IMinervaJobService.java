package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.model.job.MinervaJob;
import lcsb.mapviewer.model.job.MinervaJobExecutor;
import lcsb.mapviewer.model.job.MinervaJobStatus;
import lcsb.mapviewer.model.job.MinervaJobType;
import lcsb.mapviewer.persist.dao.MinervaJobProperty;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;

public interface IMinervaJobService extends CrudService<MinervaJob, MinervaJobProperty> {

  boolean addJob(MinervaJob job);

  MinervaJob updateJob(MinervaJob job);

  void waitForTasksToFinish();

  List<MinervaJob> getPending(int max);

  List<MinervaJob> getPending();

  @Transactional
  MinervaJob getJobById(int id);

  CacheType getTypeById(Integer typeId);

  void evict(MinervaJob job);

  long getCount(MinervaJobStatus pending);

  void registerExecutor(final MinervaJobType jobType, final MinervaJobExecutor executor);

  void setInterruptedOnRunningJobs();

  List<MinervaJob> getAllJobs();

  @Transactional
  void removeOldJobs(MinervaJobType type, MinervaJobStatus status, Calendar beforeDate);

  @Transactional
  void removeOldJobs(MinervaJobStatus status, Calendar beforeDate);

  @Transactional
  void removeOldJobs(Calendar beforeDate);

  void enableQueue();

  void enableQueue(boolean shutdownOnFailure);

  void disableQueue();

  @Transactional
  void updateJobProgress(MinervaJob job, Double progress);
}
