package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.persist.dao.map.OverviewImageProperty;

public interface IOverviewImageService extends CrudService<OverviewImage, OverviewImageProperty> {
}
