package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.map.species.field.Structure;
import lcsb.mapviewer.persist.dao.map.species.UniprotStructureProperty;

/**
 * Service that manages projects.
 *
 * @author Piotr Gawron
 */
public interface IUniprotStructureService extends CrudService<Structure, UniprotStructureProperty> {

}
