package lcsb.mapviewer.services.interfaces;

import lcsb.mapviewer.model.plugin.Plugin;
import lcsb.mapviewer.model.plugin.PluginDataEntry;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.plugin.PluginProperty;

import java.util.List;

public interface IPluginService extends CrudService<Plugin, PluginProperty> {

  Plugin getByHash(final String hash);

  List<Plugin> getAll();

  PluginDataEntry getEntryByKey(final Plugin plugin, final String key, final User user);

  void delete(final PluginDataEntry entry);

  void delete(final Plugin plugin);

  void add(final Plugin plugin);

  void add(final PluginDataEntry entry);

  void update(final PluginDataEntry entry);

  void update(final Plugin plugin);

  String getExpectedHash(String url);
}
