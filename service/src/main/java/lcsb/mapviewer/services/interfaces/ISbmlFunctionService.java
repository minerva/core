package lcsb.mapviewer.services.interfaces;

import java.util.List;

import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.persist.dao.map.kinetics.SbmlFunctionProperty;

public interface ISbmlFunctionService extends CrudService<SbmlFunction, SbmlFunctionProperty> {

  List<SbmlFunction> getFunctionsByProjectId(final String projectId, final String mapId);

  SbmlFunction getFunctionById(final String projectId, final int mapId, final int functionId);

}
