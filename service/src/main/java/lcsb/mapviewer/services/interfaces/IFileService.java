package lcsb.mapviewer.services.interfaces;

import java.io.FileNotFoundException;

import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.user.User;

public interface IFileService {

  UploadedFileEntry getById(final Integer id);

  User getOwnerByFileId(final Integer id);

  void add(final UploadedFileEntry entry);

  void update(final UploadedFileEntry fileEntry);

  String getLocalPathForFile(final String sourceUrl) throws FileNotFoundException;

  void delete(UploadedFileEntry fileEntry);

}
