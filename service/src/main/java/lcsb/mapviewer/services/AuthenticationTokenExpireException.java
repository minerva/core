package lcsb.mapviewer.services;

public class AuthenticationTokenExpireException extends SecurityException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public AuthenticationTokenExpireException(final String message) {
    super(message);
  }

}
