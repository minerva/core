package lcsb.mapviewer.services.http;

import org.apache.http.impl.client.CloseableHttpClient;

public interface HttpClientFactory {
  CloseableHttpClient create();
}
