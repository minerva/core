package lcsb.mapviewer.services.http;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

public class HttpClientFactoryImpl implements HttpClientFactory {

  public CloseableHttpClient create() {
    return HttpClientBuilder.create().build();
  }
}
