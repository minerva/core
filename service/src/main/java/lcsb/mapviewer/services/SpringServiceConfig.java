package lcsb.mapviewer.services;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;

import lcsb.mapviewer.annotation.SpringAnnotationConfig;
import lcsb.mapviewer.persist.SpringPersistConfig;

@Configuration
@Import({ SpringPersistConfig.class, SpringAnnotationConfig.class })
@ComponentScan(basePackages = { "lcsb.mapviewer.services" })
public class SpringServiceConfig {

  @Bean
  public SessionRegistry sessionRegistry() {
    return new SessionRegistryImpl();
  }

}
