package lcsb.mapviewer.services;

public class UserAccessException extends SecurityException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public UserAccessException(final String message) {
    super(message);
  }

}
