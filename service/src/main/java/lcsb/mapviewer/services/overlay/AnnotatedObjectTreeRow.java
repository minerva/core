package lcsb.mapviewer.services.overlay;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.annotation.services.ModelAnnotator;
import lcsb.mapviewer.annotation.services.annotators.IElementAnnotator;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.modelutils.map.ClassTreeNode;

/**
 * This object is used for representing row of class type for annotators on the
 * client side.
 * 
 * @author Piotr Gawron
 * 
 */
public class AnnotatedObjectTreeRow implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final transient Logger logger = LogManager.getLogger();

  /**
   * Name of the class.
   */
  private String name;
  /**
   * Class.
   */
  private Class<?> clazz;

  /**
   * List of annotators that could be used for annotating {@link #clazz}.
   */
  private List<String> validAnnotators = new ArrayList<>();

  /**
   * List of annotators that will be used for annotating {@link #clazz}.
   */
  private List<AnnotatorData> usedAnnotators = new ArrayList<>();

  /**
   * Should the type be checked to contain at least one of the
   * {@link #requiredAnnotations required miriam}.
   */
  private Boolean require = null;

  /**
   * List of valid {@link MiriamType} for the class. <br/>
   * SizeOf({@link #validAnnotations} + {@link #missingValidAnnotations}) ==
   * sizeOf( {@link MiriamType#values()}).
   */
  private List<MiriamType> validAnnotations = new ArrayList<MiriamType>();

  /**
   * List of {@link MiriamType} that may be moved to {@link #validAnnotations}.
   * 
   * <br/>
   * SizeOf({@link #validAnnotations} + {@link #missingValidAnnotations}) ==
   * sizeOf( {@link MiriamType#values()}).
   */
  private List<MiriamType> missingValidAnnotations = new ArrayList<MiriamType>();

  /**
   * List of required {@link MiriamType} for the class. <br/>
   * SizeOf({@link #requiredAnnotations} + {@link #missingRequiredAnnotations})
   * == sizeOf( {@link MiriamType#values()}).
   */
  private List<MiriamType> requiredAnnotations = new ArrayList<MiriamType>();

  /**
   * List of {@link MiriamType} that may be moved to
   * {@link #requiredAnnotations}.
   * 
   * <br/>
   * SizeOf({@link #requiredAnnotations} + {@link #missingRequiredAnnotations})
   * == sizeOf( {@link MiriamType#values()}).
   */
  private List<MiriamType> missingRequiredAnnotations = new ArrayList<MiriamType>();

  /**
   * Default constructor.
   * 
   * @param node
   *          information about {@link #clazz}
   */
  public AnnotatedObjectTreeRow(final ClassTreeNode node) {
    this(node, new ArrayList<>(), new ArrayList<>(), new HashSet<>(), new HashSet<>());
  }

  /**
   * Default constructor.
   * 
   * @param node
   *          information about {@link #clazz}
   * @param validAnnonators
   *          set of annotators that are available for {@link #clazz}
   * @param usedAnnonators
   *          set of annotators that should be used by default
   * @param validMiriam
   *          set of {@link MiriamType} that are valid by default
   * @param requiredMiriam
   *          set of {@link MiriamType} that are required by default
   */
  public AnnotatedObjectTreeRow(final ClassTreeNode node, final Collection<IElementAnnotator> validAnnonators,
      final Collection<IElementAnnotator> usedAnnonators,
      final Collection<MiriamType> validMiriam, final Collection<MiriamType> requiredMiriam) {
    this.name = node.getCommonName();
    this.clazz = node.getClazz();
    for (final IElementAnnotator elementAnnotator : validAnnonators) {
      if (usedAnnonators.contains(elementAnnotator)) {
        Class<?> clazz = null;
        for (Class<?> annotatorInterface : ModelAnnotator.unwrapProxy(elementAnnotator).getClass().getInterfaces()) {
          if (!IElementAnnotator.class.equals(annotatorInterface) && IElementAnnotator.class.isAssignableFrom(annotatorInterface)) {
            clazz = annotatorInterface;
          }
        }
        AnnotatorData annotatorData = new AnnotatorData(clazz);
        this.usedAnnotators.add(annotatorData);
      } else {
        this.validAnnotators.add(elementAnnotator.getCommonName());
      }
    }
    for (final MiriamType mt : MiriamType.values()) {
      if (validMiriam != null && validMiriam.contains(mt)) {
        validAnnotations.add(mt);
      } else {
        missingValidAnnotations.add(mt);
      }

      if (requiredMiriam != null && requiredMiriam.contains(mt)) {
        requiredAnnotations.add(mt);
      } else {
        missingRequiredAnnotations.add(mt);
      }
    }
    this.require = (Boolean) node.getData();
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * @return the clazz
   * @see #clazz
   */
  public Class<?> getClazz() {
    return clazz;
  }

  /**
   * @param clazz
   *          the clazz to set
   * @see #clazz
   */
  public void setClazz(final Class<?> clazz) {
    this.clazz = clazz;
  }

  /**
   * @return the validAnnotators
   * @see #validAnnotators
   */
  public List<String> getValidAnnotators() {
    return validAnnotators;
  }

  /**
   * @param validAnnotators
   *          the validAnnotators to set
   * @see #validAnnotators
   */
  public void setValidAnnotators(final List<String> validAnnotators) {
    this.validAnnotators = validAnnotators;
  }

  /**
   * @return the usedAnnotators
   * @see #usedAnnotators
   */
  public List<AnnotatorData> getUsedAnnotators() {
    return usedAnnotators;
  }

  /**
   * @param usedAnnotators
   *          the usedAnnotators to set
   * @see #usedAnnotators
   */
  public void setUsedAnnotators(final List<AnnotatorData> usedAnnotators) {
    this.usedAnnotators = usedAnnotators;
  }

  /**
   * @return the validAnnotations
   * @see #validAnnotations
   */
  public List<MiriamType> getValidAnnotations() {
    return validAnnotations;
  }

  /**
   * @param validAnnotations
   *          the validAnnotations to set
   * @see #validAnnotations
   */
  public void setValidAnnotations(final List<MiriamType> validAnnotations) {
    this.validAnnotations = validAnnotations;
  }

  /**
   * @return the require
   * @see #require
   */
  public Boolean getRequire() {
    return require;
  }

  /**
   * @param require
   *          the require to set
   * @see #require
   */
  public void setRequire(final Boolean require) {
    this.require = require;
  }

  /**
   * @return the missingRequiredAnnotations
   * @see #missingRequiredAnnotations
   */
  public List<MiriamType> getMissingRequiredAnnotations() {
    return missingRequiredAnnotations;
  }

  /**
   * @param missingRequiredAnnotations
   *          the missingRequiredAnnotations to set
   * @see #missingRequiredAnnotations
   */
  public void setMissingRequiredAnnotations(final List<MiriamType> missingRequiredAnnotations) {
    this.missingRequiredAnnotations = missingRequiredAnnotations;
  }

  /**
   * @return the requiredAnnotations
   * @see #requiredAnnotations
   */
  public List<MiriamType> getRequiredAnnotations() {
    return requiredAnnotations;
  }

  /**
   * @param requiredAnnotations
   *          the requiredAnnotations to set
   * @see #requiredAnnotations
   */
  public void setRequiredAnnotations(final List<MiriamType> requiredAnnotations) {
    this.requiredAnnotations = requiredAnnotations;
  }

  /**
   * @return the missingValidAnnotations
   * @see #missingValidAnnotations
   */
  public List<MiriamType> getMissingValidAnnotations() {
    return missingValidAnnotations;
  }

  /**
   * @param missingValidAnnotations
   *          the missingValidAnnotations to set
   * @see #missingValidAnnotations
   */
  public void setMissingValidAnnotations(final List<MiriamType> missingValidAnnotations) {
    this.missingValidAnnotations = missingValidAnnotations;
  }
}
