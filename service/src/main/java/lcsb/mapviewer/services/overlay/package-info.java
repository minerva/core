/**
 * This package contains data structure used by PrimeFaces framework or Google
 * Maps API.
 * 
 */
package lcsb.mapviewer.services.overlay;
