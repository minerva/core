package lcsb.mapviewer.services;

public class UserDTO {
  private String login;
  private String firstName;
  private String lastName;
  private String email;
  private String bindDn;

  public String getLogin() {
    return login;
  }

  public void setLogin(final String login) {
    this.login = login;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(final String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public String getBindDn() {
    return bindDn;
  }

  public void setBindDn(final String bindDn) {
    this.bindDn = bindDn;
  }
}
