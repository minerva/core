package lcsb.mapviewer.annotation;

import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.common.tests.TestUtils;
import lcsb.mapviewer.common.tests.UnitTestFailedWatcher;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.cache.CacheTypeDao;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

@ContextConfiguration(classes = SpringAnnotationTestConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class AnnotationNoTransactionTestFunctions extends TestUtils {

  private static final Map<String, Model> models = new HashMap<>();

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();

  @Autowired
  protected GeneralCacheInterface cache;

  @Autowired
  protected CacheTypeDao cacheTypeDao;

  private static int idCounter = 0;

  protected Model getModelForFile(final String fileName, final boolean fromCache) throws Exception {
    if (!fromCache) {
      logger.debug("File without cache: {}", fileName);
      return new CellDesignerXmlParser().createModel(new ConverterParams().filename(fileName));
    }
    Model result = AnnotationNoTransactionTestFunctions.models.get(fileName);
    if (result == null) {
      logger.debug("File to cache: {}", fileName);

      CellDesignerXmlParser parser = new CellDesignerXmlParser();
      result = parser.createModel(new ConverterParams().filename(fileName).sizeAutoAdjust(false));
      AnnotationNoTransactionTestFunctions.models.put(fileName, result);
    }
    return result;
  }

  public GenericProtein createProtein() {
    GenericProtein result = new GenericProtein("s" + (idCounter++));
    populateRandomData(result);

    return result;
  }

  private static void populateRandomData(final Species result) {
    result.setNameX(faker.number().numberBetween(10, 100));
    result.setNameY(faker.number().numberBetween(10, 100));
    result.setZ(faker.number().numberBetween(10, 100));
    result.setNameWidth(faker.number().numberBetween(10, 100));
    result.setNameHeight(faker.number().numberBetween(10, 100));
    result.setHomodimer(faker.number().numberBetween(1, 10));
    result.setNameHorizontalAlign(faker.options().option(HorizontalAlign.class));
    result.setNameVerticalAlign(faker.options().option(VerticalAlign.class));
    result.setHypothetical(faker.bool().bool());
    result.setName(faker.name().fullName());
  }

  public Ion createIon() {
    Ion result = new Ion("s" + (idCounter++));
    populateRandomData(result);

    return result;
  }

  public ModelData createModelData() {
    ModelData model = new ModelData();
    model.setWidth(faker.number().numberBetween(1, 1000));
    model.setHeight(faker.number().numberBetween(1, 1000));

    return model;
  }

  public Project createProject() {
    User admin = new User();
    admin.setId(BUILT_IN_TEST_ADMIN_ID);

    Project project = new Project();
    project.setProjectId(faker.numerify("T######"));
    project.setOwner(admin);

    return project;
  }

  protected Reaction createEmptyReaction() {
    Reaction reaction = new Reaction(faker.numerify("re######"));
    reaction.setLine(createLine());
    reaction.setZ(faker.number().numberBetween(1, 10));

    return reaction;
  }

  private PolylineData createLine() {
    PolylineData line = new PolylineData();
    line.setType(faker.options().option(LineType.class));
    return line;
  }

}
