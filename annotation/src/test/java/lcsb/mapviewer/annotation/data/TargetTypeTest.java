package lcsb.mapviewer.annotation.data;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class TargetTypeTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValues() {
    for (final TargetType type : TargetType.values()) {
      assertNotNull(type.getCommonName());
      assertNotNull(TargetType.valueOf(type.toString()));
    }
  }

}
