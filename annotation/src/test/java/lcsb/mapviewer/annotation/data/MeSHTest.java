package lcsb.mapviewer.annotation.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;

public class MeSHTest extends AnnotationTestFunctions {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new MeSH("is", "name", "desc", new ArrayList<>()));
  }

  @Test
  public void testGetters() {
    MeSH mesh = new MeSH();
    List<String> synonyms = new ArrayList<>();

    mesh.setSynonyms(synonyms);

    assertEquals(synonyms, mesh.getSynonyms());
  }

  @Test
  public void testToString() {
    MeSH mesh = new MeSH("is", "nam3e", "des3c", new ArrayList<>());

    String serial = mesh.toString();

    assertTrue(serial.contains("is"));
    assertTrue(serial.contains("nam3e"));
    assertTrue(serial.contains("des3c"));
  }

  @Test
  public void testSynonymsToString() {
    MeSH mesh = new MeSH();

    assertEquals("", mesh.getSynonymsString());

    mesh.getSynonyms().add("synon");
    assertEquals("synon", mesh.getSynonymsString());

    mesh.getSynonyms().add("bla");
    mesh.getSynonyms().add("qwe");
    assertEquals("synon,bla,qwe", mesh.getSynonymsString());
  }

}
