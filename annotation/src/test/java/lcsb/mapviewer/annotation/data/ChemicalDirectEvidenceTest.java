package lcsb.mapviewer.annotation.data;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class ChemicalDirectEvidenceTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValues() {
    for (final ChemicalDirectEvidence value : ChemicalDirectEvidence.values()) {
      assertNotNull(ChemicalDirectEvidence.valueOf(value.toString()));
    }
  }

}
