package lcsb.mapviewer.annotation.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.map.MiriamData;

public class EntrezDataTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    List<MiriamData> miriamData = new ArrayList<>();
    EntrezData entrez = new EntrezData();
    entrez.setMiriamData(miriamData);
    assertEquals(miriamData, entrez.getMiriamData());
  }

  @Test
  public void testAddMiriamData() {
    EntrezData entrez = new EntrezData();
    entrez.addMiriamData(new MiriamData());
    assertEquals(1, entrez.getMiriamData().size());
  }

  @Test
  public void testToString() {
    EntrezData entrez = new EntrezData();
    assertNotNull(entrez.toString());
  }

}
