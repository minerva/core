package lcsb.mapviewer.annotation.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.map.MiriamData;

public class ChemicalTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    List<Target> inferenceNetwork = new ArrayList<>();
    List<String> synonyms = new ArrayList<>();
    List<MiriamData> directEvidencePublication = new ArrayList<>();
    Chemical chemical = new Chemical();
    chemical.setInferenceNetwork(inferenceNetwork);
    chemical.setDirectEvidencePublication(directEvidencePublication);
    chemical.setSynonyms(synonyms);

    assertEquals(inferenceNetwork, chemical.getInferenceNetwork());
    assertEquals(directEvidencePublication, chemical.getDirectEvidencePublication());
    assertEquals(synonyms, chemical.getSynonyms());

  }

  @Test
  public void testGetSynonymsString() {
    Chemical chemical = new Chemical();
    assertEquals("", chemical.getSynonymsString());
    chemical.getSynonyms().add("12");
    assertEquals("12", chemical.getSynonymsString());
    chemical.getSynonyms().add("qwe");
    assertEquals("12,qwe", chemical.getSynonymsString());
    chemical.getSynonyms().add("zc");
    assertEquals("12,qwe,zc", chemical.getSynonymsString());
  }

  @Test
  public void testToString() {

    Chemical chemical = new Chemical();
    chemical.getInferenceNetwork().add(new Target());
    chemical.getDirectEvidencePublication().add(new MiriamData());
    assertNotNull(chemical.toString());
  }

}
