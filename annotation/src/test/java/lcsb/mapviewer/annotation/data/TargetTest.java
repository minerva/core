package lcsb.mapviewer.annotation.data;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;

public class TargetTest extends AnnotationTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Target());
  }

  @Test
  public void testGetters() {
    List<MiriamData> genes = new ArrayList<>();
    Target target = new Target();
    target.setGenes(genes);

    assertEquals(genes, target.getGenes());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testAddInvalidGene() {
    Target target = new Target();
    target.addGene(null);
  }
}
