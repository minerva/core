package lcsb.mapviewer.annotation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.fasterxml.jackson.databind.ObjectMapper;

import lcsb.mapviewer.persist.SpringPersistConfig;

@Configuration
@Import({ SpringAnnotationConfig.class, SpringPersistConfig.class })
public class SpringAnnotationTestConfig {

  @Bean
  public ObjectMapper objectMapper() {
    return new ObjectMapper();
  }

}
