package lcsb.mapviewer.annotation.cache;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.model.cache.CacheQuery;
import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.persist.dao.cache.CacheTypeDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;
import org.w3c.dom.Node;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class CacheableInterfaceTest extends AnnotationTestFunctions {

  @Autowired
  private List<Cacheable> interfaces;

  private final String query = "https://www.google.com/";

  private CacheType type;

  @Autowired
  private MockCacheInterface mockCacheImpl;

  @Before
  public void setUp() throws Exception {
    type = mockCacheImpl.getCacheType();
  }

  @After
  public void tearDown() throws Exception {
    cache.waitToFinishTasks();
    CacheQuery cacheQuery = cache.getByQuery(query, type);
    if (cacheQuery != null) {
      cache.delete(cacheQuery);
    }
  }

  @Test
  public void testImplementationCacheExists() {
    for (final Cacheable cacheableInterface : interfaces) {
      assertNotNull("Cache type does not exist for type: " + cacheableInterface, cacheableInterface.getCacheType());
    }
  }

  @Test
  public void testGetWebPage() throws Exception {
    CachableInterfaceMock ci = new CachableInterfaceMock(CachableInterface.class);
    String result = ci.getWebPageContent("http://www.drugbank.ca/biodb/polypeptides/P21728");
    assertNotNull(result);
  }

  @Test
  public void testGetWebPage2() throws Exception {
    CachableInterfaceMock ci = new CachableInterfaceMock(CachableInterface.class);
    String result = ci.getWebPageContent("http://www.drugbank.ca/drugs/DB00091");
    assertNotNull(result);
  }

  @Test
  public void testGetWebPageGetCache() throws Exception {
    CachableInterfaceMock ci = new CachableInterfaceMock(CachableInterface.class);

    CacheTypeDao mock = Mockito.mock(CacheTypeDao.class);
    when(mock.getByClassName(anyString())).thenReturn(type);
    ReflectionTestUtils.setField(ci, "cacheTypeDao", mock);
    ci.setCache(cache);

    String result = ci.getWebPageContent(query);
    assertNotNull(result);
    String cacheValue = ci.getCacheValue(query);
    assertEquals(result, cacheValue);
  }

  @Test
  public void testSetNode() throws Exception {
    CachableInterfaceMock ci = new CachableInterfaceMock(CachableInterface.class);

    CacheTypeDao mock = Mockito.mock(CacheTypeDao.class);
    when(mock.getByClassName(anyString())).thenReturn(type);
    ReflectionTestUtils.setField(ci, "cacheTypeDao", mock);

    String xml = "<xml><child/></xml>";
    ci.setCache(cache);
    ci.setCacheNode(query, super.getNodeFromXmlString(xml));

    Node node = ci.getCacheNode(query);
    assertNotNull(node);
  }

  @Test
  public void testNewLinesGetWebPage() throws Exception {
    CachableInterfaceMock ci = new CachableInterfaceMock(CachableInterface.class);
    ci.setCache(null);
    String result = ci.getWebPageContent("http://google.com/");
    assertNotNull(result);
    assertTrue(result.contains("\n"));
  }
}
