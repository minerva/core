package lcsb.mapviewer.annotation.cache;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.concurrent.ExecutionException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import lcsb.mapviewer.annotation.SpringAnnotationTestConfig;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.tests.TestUtils;
import lcsb.mapviewer.model.cache.CacheQuery;
import lcsb.mapviewer.model.cache.CacheType;

@ContextConfiguration(classes = SpringAnnotationTestConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class GeneralCacheWithExclusionTest extends TestUtils {

  @Autowired
  protected GeneralCacheInterface cache;

  private String query = "str";
  private CacheType type;

  @Autowired
  private MockCacheInterface mockCacheImpl;

  @Before
  public void setUp() throws Exception {
    type = mockCacheImpl.getCacheType();
  }

  @After
  public void tearDown() throws InterruptedException, ExecutionException {
    cache.waitToFinishTasks();
    CacheQuery cacheQuery = cache.getByQuery(query, type);
    if (cacheQuery != null) {
      cache.delete(cacheQuery);
    }
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructorWithInvalidParams() {
    new GeneralCacheWithExclusion(null, 1);
  }

  @Test
  public void testExclusion() throws Exception {
    GeneralCacheWithExclusion cacheWithExclusion = new GeneralCacheWithExclusion(cache, 2);

    cacheWithExclusion.setCachedQuery(query, type, super.getNodeFromXmlString("<val/>"));

    // after two tries we should get the data that was set
    assertNull(cacheWithExclusion.getXmlNodeByQuery(query, type));
    assertNull(cacheWithExclusion.getXmlNodeByQuery(query, type));
    assertNotNull(cacheWithExclusion.getXmlNodeByQuery(query, type));

  }

  @Test
  public void testClear() throws Exception {
    GeneralCacheWithExclusion cacheWithExclusion = new GeneralCacheWithExclusion(cache, 0);

    cacheWithExclusion.setCachedQuery(query, type, super.getNodeFromXmlString("<val/>"));

    assertNotNull(cacheWithExclusion.getXmlNodeByQuery(query, type));
    cacheWithExclusion.clearCache();

    assertNull(cacheWithExclusion.getXmlNodeByQuery(query, type));

  }

  @Test
  public void testRemove() throws Exception {
    GeneralCacheWithExclusion cacheWithExclusion = new GeneralCacheWithExclusion(cache, 0);

    cacheWithExclusion.setCachedQuery(query, type, super.getNodeFromXmlString("<val/>"));

    assertNotNull(cacheWithExclusion.getXmlNodeByQuery(query, type));
    // we need to wait because by default value will be refreshed
    cache.waitToFinishTasks();
    cacheWithExclusion.removeByQuery(query, type);
    cache.waitToFinishTasks();
    assertNull(cacheWithExclusion.getXmlNodeByQuery(query, type));
  }

}
