package lcsb.mapviewer.annotation.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.persist.dao.ConfigurationDao;

public class BigFileCacheTest extends AnnotationTestFunctions {

  private String httpUrl = "https://www.google.pl/humans.txt";

  @Autowired
  private BigFileCache bigFileCache;

  @Autowired
  private ConfigurationDao configurationDao;

  @Before
  public void setUp() {
  }

  @After
  public void tearDown() throws Exception {
    ReflectionTestUtils.setField(bigFileCache, "configurationDao", configurationDao);
  }

  @Test
  public void testDownloadFileWithUnknownProtocol() throws Exception {
    try {
      bigFileCache.downloadFile("wtf://file.com", null);

    } catch (final URISyntaxException e) {
      assertTrue(e.getMessage().contains("Unknown protocol for url"));
    }
  }

  @Test
  public void testDownloadAsyncFileFromHttp() throws Exception {
    bigFileCache.downloadFile(httpUrl, null);
    waitForDownload();
    String localFile = bigFileCache.getAbsolutePathForFile(httpUrl);
    assertTrue(bigFileCache.isLocalFileUpToDate(httpUrl));
    File f = new File(localFile);
    assertTrue(f.exists());
    String content = super.readFile(localFile);
    assertTrue(content.contains("Google"));
    bigFileCache.removeFile(httpUrl);

    assertNull(bigFileCache.getAbsolutePathForFile(httpUrl));

    assertFalse(f.exists());
  }

  private void waitForDownload() throws InterruptedException {
    while (bigFileCache.getDownloadThreadCount() > 0) {
      logger.debug("Waiting for download to finish");
      Thread.sleep(100);
    }
  }

  @Test(expected = FileNotFoundException.class)
  public void testIsLocalHttpFileUpToDateWhenMissing() throws Exception {
    bigFileCache.isLocalFileUpToDate(httpUrl + "?some_get_param");
  }

  @Test(expected = FileNotFoundException.class)
  public void testIsLocalHttpFileUpToDateWhenFileRemovedManually() throws Exception {
    bigFileCache.downloadFile(httpUrl, new IProgressUpdater() {
      @Override
      public void setProgress(final double progress) {
      }
    });
    new File(bigFileCache.getAbsolutePathForFile(httpUrl)).delete();
    try {
      bigFileCache.isLocalFileUpToDate(httpUrl);
    } finally {
      bigFileCache.removeFile(httpUrl);
    }
  }

  @Test(expected = InvalidStateException.class)
  public void testDownloadHttpWhenDownloadFails() throws Exception {
    ConfigurationDao mockDao = Mockito.mock(ConfigurationDao.class);
    when(mockDao.getValueByType(any())).thenThrow(new RuntimeException());
    ReflectionTestUtils.setField(bigFileCache, "configurationDao", mockDao);
    bigFileCache.downloadFile(httpUrl, null);
  }

  @Test(expected = IOException.class)
  public void testGetRemoteHttpFileSizeForInvalidUrl() throws Exception {
    Mockito.mock(BigFileCacheImpl.class, Mockito.CALLS_REAL_METHODS).getRemoteHttpFileSize("invalidurl");
  }

  @Test
  public void testRemoveFileThatDoesntExist() throws Exception {
    bigFileCache.removeFile("unexisting file");
  }

  @Test(expected = URISyntaxException.class)
  public void testIsLocalFileUpToDateForInvalidURI() throws Exception {
    bigFileCache.isLocalFileUpToDate("unexistingFileProtocol://bla");
  }

  @Test
  public void testGetDomainName() throws Exception {
    String domain = Mockito.mock(BigFileCacheImpl.class, Mockito.CALLS_REAL_METHODS).getDomainName("http://www.eskago.pl/radio/eska-rock");
    assertEquals("eskago.pl", domain);
  }

  @Test(expected = InvalidStateException.class)
  public void testExecuteProblematicTask() throws Exception {
    Future<?> task = Executors.newScheduledThreadPool(10, new ThreadFactory() {
      @Override
      public Thread newThread(final Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        return t;
      }
    }).submit(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        throw new Exception();
      }
    });
    Mockito.mock(BigFileCacheImpl.class, Mockito.CALLS_REAL_METHODS).executeTask(task);
  }

  @Test(expected = URISyntaxException.class)
  public void testExecuteProblematicTask2() throws Exception {
    Future<?> task = Executors.newScheduledThreadPool(10, new ThreadFactory() {
      @Override
      public Thread newThread(final Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        return t;
      }
    }).submit(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        throw new URISyntaxException("", "");
      }
    });
    Mockito.mock(BigFileCacheImpl.class, Mockito.CALLS_REAL_METHODS).executeTask(task);
  }

}
