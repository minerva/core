package lcsb.mapviewer.annotation.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.w3c.dom.Node;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.services.PubmedParser;
import lcsb.mapviewer.model.cache.CacheType;

@Rollback(true)
public class PermanentDatabaseLevelCacheWithoutTransactionTest extends AnnotationTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testByQuery() throws Exception {
    String xml = "<hello/>";
    String query = "blabla";

    String xml2 = "<hello23/>";

    CacheType type = cacheTypeDao.getByClassName(PubmedParser.class.getCanonicalName());

    Node sourceNode = getNodeFromXmlString(xml);
    Node sourceNode2 = getNodeFromXmlString(xml2);
    cache.removeByQuery(query, type);

    Node node = cache.getXmlNodeByQuery(query, type);
    assertNull(node);

    cache.setCachedQuery(query, type, sourceNode);
    node = cache.getXmlNodeByQuery(query, type);
    assertNotNull(node);
    assertEquals("hello", node.getNodeName());

    cache.setCachedQuery(query, type, sourceNode2);
    node = cache.getXmlNodeByQuery(query, type);
    assertNotNull(node);
    assertEquals("hello23", node.getNodeName());
  }

}
