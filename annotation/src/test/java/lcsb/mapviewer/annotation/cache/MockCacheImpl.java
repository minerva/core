package lcsb.mapviewer.annotation.cache;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Mock implementation of {@link CachableInterface}. It's used for invalid cache
 * elements: {@link lcsb.mapviewer.model.cache.CacheType#INVALID_CACHE}.
 * 
 * @author Piotr Gawron
 * 
 */
@Service
@Transactional
public class MockCacheImpl extends CachableInterface implements MockCacheInterface {

  protected static RuntimeException exceptionToThrow = null;
  protected static Error errorToThrow;

  public MockCacheImpl() {
    super(MockCacheInterface.class);
  }

  @Override
  public String refreshCacheQuery(final Object query) throws SourceNotAvailable {
    if (exceptionToThrow != null) {
      throw exceptionToThrow;
    }
    if (errorToThrow != null) {
      throw errorToThrow;
    }
    return "<node>[" + query + "] Random: " + Math.random() + "</node>";
  }

}
