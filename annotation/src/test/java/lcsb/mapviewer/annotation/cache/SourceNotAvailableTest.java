package lcsb.mapviewer.annotation.cache;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class SourceNotAvailableTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor() {
    Exception e = new SourceNotAvailable("meessage");
    assertTrue(e.getMessage().contains("meessage"));
  }

}
