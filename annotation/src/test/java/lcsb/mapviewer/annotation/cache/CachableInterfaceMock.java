package lcsb.mapviewer.annotation.cache;

public class CachableInterfaceMock extends CachableInterface {

  public CachableInterfaceMock(final Class<? extends CachableInterface> clazz) {
    super(clazz);
  }

  @Override
  public Object refreshCacheQuery(final Object query) throws SourceNotAvailable {
    return null;
  }

}
