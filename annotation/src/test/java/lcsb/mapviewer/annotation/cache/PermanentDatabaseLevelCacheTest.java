package lcsb.mapviewer.annotation.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.mutable.MutableBoolean;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.util.ReflectionTestUtils;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.services.PubmedParser;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.cache.CacheQuery;
import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.persist.dao.cache.CacheQueryDao;

@Rollback(true)
public class PermanentDatabaseLevelCacheTest extends AnnotationTestFunctions {

  private CacheType mockCacheType;

  @Autowired
  private CacheQueryDao cacheQueryDao;

  @Before
  public void setUp() throws Exception {
    mockCacheType = cacheTypeDao.getByClassName(MockCacheInterface.class.getCanonicalName());
  }

  @After
  public void tearDown() throws Exception {
    ReflectionTestUtils.setField(cache, "cacheQueryDao", cacheQueryDao);
  }

  @Test(expected = CacheException.class)
  public void testSetInvalidQuery() {
    String query = "blabla";
    Object object = new Object();

    CacheType type = cacheTypeDao.getByClassName(PubmedParser.class.getCanonicalName());

    cache.setCachedQuery(query, type, object);
  }

  @Test
  public void testGetInvalidXmlNode() {
    String query = "blabla";
    String object = "hello";

    CacheType type = cacheTypeDao.getByClassName(PubmedParser.class.getCanonicalName());

    cache.setCachedQuery(query, type, object);
    assertNull(cache.getXmlNodeByQuery(query, type));

    assertEquals(2, getWarnings().size());
    assertEquals(0, getErrors().size());
  }

  @Test
  public void testInvalidateWhenDbConnectionIsInterrupted() throws Exception {
    String query = "test query";
    CacheQueryDao mockDao = Mockito.mock(CacheQueryDao.class);
    // simulate db connection hanging
    when(mockDao.getByQuery(query, mockCacheType)).then(new Answer<CacheQuery>() {
      @Override
      public CacheQuery answer(final InvocationOnMock invocation) throws Throwable {
        Thread.sleep(100);
        return null;
      }
    });
    ReflectionTestUtils.setField(cache, "cacheQueryDao", mockDao);
    // run invalidation in separate thread
    Thread t = new Thread(new Runnable() {
      @Override
      public void run() {
        cache.invalidateByQuery(query, mockCacheType);
      }
    });

    t.start();
    // interrupt thread
    t.interrupt();
    t.join();

    // check if we have information in the logs about interruption (this is
    // not normal situaion)
    assertEquals(1, getErrors().size());
  }

  @Test
  public void testInvalidateWhenDbConnectionCrash() throws Exception {
    String query = "test query";
    CacheQueryDao mockDao = Mockito.mock(CacheQueryDao.class);
    // simulate db crash
    when(mockDao.getByQuery(query, mockCacheType)).thenThrow(new InvalidStateException());
    ReflectionTestUtils.setField(cache, "cacheQueryDao", mockDao);

    cache.invalidateByQuery(query, mockCacheType);

    // check if we have information in the logs about interruption (this is
    // not normal situation)
    assertEquals(1, getErrors().size());
  }

  @Test
  public void testClearCache() throws Exception {
    CacheQueryDao mockDao = Mockito.mock(CacheQueryDao.class);

    MutableBoolean cleared = new MutableBoolean(false);

    // simulate db crash
    Mockito.doAnswer(new Answer<Void>() {

      @Override
      public Void answer(final InvocationOnMock invocation) throws Throwable {
        cleared.setValue(true);
        return null;
      }
    }).when(mockDao).clearTable();
    ReflectionTestUtils.setField(cache, "cacheQueryDao", mockDao);

    cache.clearCache();

    assertTrue(cleared.getValue());
  }
}
