package lcsb.mapviewer.annotation.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.services.ModelAnnotator;
import lcsb.mapviewer.annotation.services.PubmedParser;
import lcsb.mapviewer.annotation.services.dapi.DapiConnector;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.model.job.MinervaJob;
import lcsb.mapviewer.model.job.MinervaJobPriority;
import lcsb.mapviewer.model.job.MinervaJobType;

public class GeneralCacheTest extends AnnotationTestFunctions {

  @Autowired
  private GeneralCacheInterface cache;

  @Autowired
  private GeneralCacheInterface databaseLevelCache;

  @Autowired
  private DapiConnector dapiConnector;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetXmlNodeByQueryForInvalidType() {
    cache.getXmlNodeByQuery("str", null);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetCachedQueryForInvalidType() {
    cache.setCachedQuery("str", null, null);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRemoveByQueryForInvalidType() {
    cache.removeByQuery("str", null);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidateByQueryForInvalidType() {
    cache.invalidateByQuery("str", null);
  }

  @Test
  public void testCachableInterfaceInvalidateUrl() throws Exception {
    CacheType type = cacheTypeDao.getByClassName(PubmedParser.class.getCanonicalName());

    String query = "https://www.google.com/";
    String newRes = "hello";
    cache.setCachedQuery(query, type, newRes);
    cache.invalidateByQuery(query, type);

    databaseLevelCache.waitToFinishTasks();

    String res = cache.getStringByQuery(query, type);

    assertNotNull(res);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetStringByQueryForInvalidType() {
    cache.getStringByQuery("str", null);
  }

  @Test
  public void testRemoveByQuery() {
    try {
      CacheType type = new CacheType();
      cache.setCachedQuery("str", type, "val");
      assertNotNull(cache.getStringByQuery("str", type));
      cache.removeByQuery("str", type);
      assertNull(cache.getStringByQuery("str", type));
    } catch (final InvalidArgumentException e) {
      assertTrue(e.getMessage().contains("Cache type cannot be null"));
    }
  }

  @Test
  public void testExecute() throws Exception {
    CacheQueryMinervaJob parameters = new CacheQueryMinervaJob("https://google.com/",
        ((CachableInterface) ModelAnnotator.unwrapProxy(dapiConnector)).getCacheType().getId());
    cache.execute(new MinervaJob(MinervaJobType.REFRESH_CACHE, MinervaJobPriority.MEDIUM, parameters));
    assertEquals(0, super.getErrors().size());
  }

}
