package lcsb.mapviewer.annotation.cache;

/**
 * Mock implementation of {@link CachableInterface}. It's used for invalid cache
 * elements: {@link lcsb.mapviewer.model.cache.CacheType#INVALID_CACHE}.
 * 
 * @author Piotr Gawron
 * 
 */
public interface MockCacheInterface extends Cacheable {
}
