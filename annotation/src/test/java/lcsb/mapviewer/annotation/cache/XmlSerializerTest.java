package lcsb.mapviewer.annotation.cache;

import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.mutable.MutableBoolean;
import org.junit.Test;
import org.w3c.dom.Node;

import lcsb.mapviewer.annotation.data.MeSH;
import lcsb.mapviewer.common.tests.TestUtils;

public class XmlSerializerTest extends TestUtils {

  @Test
  public void test() throws Exception {
    MeSH mesh = new MeSH("id", "name", null, Arrays.asList("x", "y", "z", null));

    XmlSerializer<MeSH> serializer = new XmlSerializer<>(MeSH.class);

    String string = serializer.objectToString(mesh);
    Node node = getNodeFromXmlString(string);
    MutableBoolean exceptionHappened = new MutableBoolean(false);

    List<Thread> threads = new ArrayList<>();
    for (int j = 0; j < 2; j++) {
      Thread thread = new Thread() {
        @Override
        public void run() {
          try {
            for (int i = 0; i < 10000; i++) {
              serializer.xmlToObject(node);
            }
          } catch (Throwable e) {
            exceptionHappened.setTrue();
            e.printStackTrace();
          }
        }
      };
      thread.start();
      threads.add(thread);
    }
    for (Thread thread : threads) {
      thread.join();
    }
    assertFalse(exceptionHappened.booleanValue());

  }

}
