package lcsb.mapviewer.annotation.services.dapi;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

import java.util.List;

import org.apache.commons.validator.routines.UrlValidator;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.services.dapi.dto.DapiDatabase;
import lcsb.mapviewer.annotation.services.dapi.dto.ReleaseDto;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.ConfigurationOption;
import lcsb.mapviewer.persist.dao.ConfigurationDao;

@Rollback(true)
public class DapiConnectorTest extends AnnotationTestFunctions {

  @Autowired
  private DapiConnector dapiConnector;

  @Autowired
  private ConfigurationDao configurationDao;

  @Before
  public void setUp() {
    assumeTrue("DAPI credentials are not provided", isDapiConfigurationAvailable());
    ConfigurationOption option = configurationDao.getByType(ConfigurationElementType.DAPI_LOGIN);
    if (option == null) {
      option = new ConfigurationOption();
      option.setType(ConfigurationElementType.DAPI_LOGIN);
      configurationDao.add(option);
    }
    option.setValue(getDapiLogin());
    configurationDao.update(option);
    option = configurationDao.getByType(ConfigurationElementType.DAPI_PASSWORD);
    if (option == null) {
      option = new ConfigurationOption();
      option.setType(ConfigurationElementType.DAPI_PASSWORD);
      configurationDao.add(option);
    }
    option.setValue(getDapiPassword());
    configurationDao.update(option);
  }

  @Test
  public void testLogin() throws DapiConnectionException {
    dapiConnector.login();
  }

  @Test
  public void testResetConnection() throws DapiConnectionException {
    dapiConnector.login();
    assertTrue(dapiConnector.isAuthenticated());
    dapiConnector.resetConnection();
    assertFalse(dapiConnector.isAuthenticated());
  }

  @Test
  public void testGetLatestReleaseAvailable() throws DapiConnectionException {
    String version = dapiConnector.getLatestReleaseAvailable(DrugBankParserImpl.DAPI_DATABASE_NAME);
    assertNotNull(version);
  }

  @Test
  public void testGetLatestReleaseUrl() throws DapiConnectionException {
    String url = dapiConnector.getLatestReleaseUrl(DrugBankParserImpl.DAPI_DATABASE_NAME);
    assertTrue(UrlValidator.getInstance().isValid(url));
  }

  @Test
  public void testGetDatabases() throws DapiConnectionException {
    List<DapiDatabase> databases = dapiConnector.getDatabases();
    assertTrue(databases.size() > 0);
  }

  @Test
  public void testIsReleaseAccepted() throws DapiConnectionException {
    assertFalse(dapiConnector.isReleaseAccepted("blae", new ReleaseDto()));
  }

}
