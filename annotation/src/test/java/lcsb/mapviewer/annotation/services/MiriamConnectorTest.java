package lcsb.mapviewer.annotation.services;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

public class MiriamConnectorTest extends AnnotationTestFunctions {

  @Autowired
  private MiriamConnector miriamConnector;

  @Autowired
  private GeneralCacheInterface cache;

  private final WebPageDownloader webPageDownloader = new WebPageDownloader();

  @Before
  public void setUp() {
  }

  @After
  public void tearDown() throws Exception {
    ReflectionTestUtils.setField(miriamConnector, "webPageDownloader", webPageDownloader);
    ReflectionTestUtils.setField(miriamConnector, "cache", cache);
  }

  @Test
  public void testGoUri() throws Exception {
    assertNotEquals(1, MiriamType.GO.getUris().size());
  }

  @Test(timeout = 15000)
  public void testMeshUrl() throws Exception {
    MiriamData md = new MiriamData(MiriamType.MESH_2012, "D004249");

    assertNotNull(getWebpage(miriamConnector.getUrlString(md)));
  }

  @Test
  public void testGetUrl() throws Exception {
    MiriamData md = TaxonomyBackend.HUMAN_TAXONOMY;

    String url = miriamConnector.getUrlString(md);
    assertNotNull(url);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetUrlForInvalidMiriam() throws Exception {
    miriamConnector.getUrlString(new MiriamData());
  }

  @SuppressWarnings("deprecation")
  @Test(expected = InvalidArgumentException.class)
  public void testGetUrlForInvalidMiriam2() throws Exception {
    miriamConnector.getUrlString(new MiriamData(MiriamType.UNKNOWN, ""));
  }

  @Test
  public void testGetUrlForInvalidMiriam3() throws Exception {
    String url = miriamConnector.getUrlString(new MiriamData(MiriamType.ENTREZ, "abc"));
    assertNull(url);
  }

  @Test
  public void testIsValidIdentifier() throws Exception {
    assertTrue(miriamConnector.isValidIdentifier(MiriamType.ENTREZ.getCommonName() + ":" + "1234"));
    assertFalse(miriamConnector.isValidIdentifier("blablabla"));
  }

  @Test
  public void testResolveUrl() throws Exception {
    for (final MiriamType mt : MiriamType.values()) {
      boolean deprecated = false;
      try {
        Field f = MiriamType.class.getField(mt.name());
        if (f.isAnnotationPresent(Deprecated.class)) {
          deprecated = true;
        }
      } catch (final NoSuchFieldException | SecurityException e) {
      }

      if (!deprecated) {
        assertTrue("Invalid MiriamType (" + mt + ") for MiriamType: " + mt, miriamConnector.isValidMiriamType(mt));
      }
    }
  }

  @Test
  public void testGetUrlForDoi() throws Exception {
    // exclude first cached value
    MiriamData md = new MiriamData(MiriamType.DOI, "10.1038/npjsba.2016.20");

    String url = miriamConnector.getUrlString(md);
    assertNotNull(url);
  }

  @Test
  public void testMiriamDataToUri() throws Exception {
    assertNotNull(miriamConnector.miriamDataToUri(TaxonomyBackend.HUMAN_TAXONOMY));
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(miriamConnector, "webPageDownloader", mockDownloader);
    miriamConnector.refreshCacheQuery("http://google.pl/");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery() throws Exception {
    miriamConnector.refreshCacheQuery("invalid_query");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery2() throws Exception {
    miriamConnector.refreshCacheQuery(new Object());
  }

  @Test
  public void testCheckValidyOfUnknownMiriamType() throws Exception {
    // user connector without cache
    @SuppressWarnings("deprecation")
    boolean value = miriamConnector.isValidMiriamType(MiriamType.UNKNOWN);
    assertFalse(value);
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, miriamConnector.getServiceStatus().getStatus());
  }

  @Test
  public void testGetUrlWithNewLine() throws Exception {
    MiriamData md = new MiriamData(MiriamType.HGNC_SYMBOL, "test\name");

    String url = miriamConnector.getUrlString(md);
    assertNotNull(url);
  }
}
