package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class ExternalServiceStatusTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    String name = "b";
    String page = "page";

    ExternalServiceStatus status = new ExternalServiceStatus(name, page);
    assertEquals(name, status.getName());
    assertEquals(page, status.getPage());
  }

}
