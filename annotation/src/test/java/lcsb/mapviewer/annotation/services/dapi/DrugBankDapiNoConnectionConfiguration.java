package lcsb.mapviewer.annotation.services.dapi;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import lcsb.mapviewer.persist.dao.ConfigurationDao;

@Profile("dapiNotConnectedTestProfile")
@Configuration
public class DrugBankDapiNoConnectionConfiguration {

  @Autowired
  private ConfigurationDao configurationDao;


  private String dapiLogin;
  private String dapiPassword;

  public DrugBankDapiNoConnectionConfiguration() {
    dapiLogin = System.getenv("DAPI_TEST_LOGIN");
    dapiPassword = System.getenv("DAPI_TEST_PASSWORD");
  }

  @Bean
  @Primary
  public DapiConnector dapiConnector() throws DapiConnectionException {
    DapiConnectorImpl mock = Mockito.spy(new DapiConnectorImpl(configurationDao));
    Mockito.doReturn(dapiLogin).when(mock).getDapiLogin();
    Mockito.doReturn(dapiPassword).when(mock).getDapiPassword();

    Mockito.doReturn(false).when(mock).isValidConnection();

    Mockito.doThrow(new DapiConnectionException("Dapi is not connected", -1)).when(mock).login();
    return mock;
  }

}
