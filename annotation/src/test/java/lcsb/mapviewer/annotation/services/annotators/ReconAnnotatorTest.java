package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;

public class ReconAnnotatorTest extends AnnotationTestFunctions {

  @Autowired
  private ReconAnnotator reconAnnotator;

  @Autowired
  private GeneralCacheInterface cache;

  private WebPageDownloader webPageDownloader = new WebPageDownloader();

  @Before
  public void setUp() {
  }

  @After
  public void tearDown() throws Exception {
    ReflectionTestUtils.setField(reconAnnotator, "webPageDownloader", webPageDownloader);
    ReflectionTestUtils.setField(reconAnnotator, "cache", cache);
  }

  @Test
  public void testAnnotateReaction() throws Exception {
    Reaction reaction = new Reaction("re");
    reaction.setAbbreviation("P5CRm");
    reconAnnotator.annotateElement(reaction);
    assertTrue("No new annotations from recon db imported", reaction.getMiriamData().size() > 0);
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testAnnotating_O16G2e_Reaction() throws Exception {
    Reaction reaction = new Reaction("re");
    reaction.setName("O16G2e");
    reconAnnotator.annotateElement(reaction);
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testSpeciesAnnotationWithInvalidResponse() throws Exception {
    Ion ion = new Ion("id");
    ion.setName("O16G2e");

    // exclude first cached value
    ReflectionTestUtils.setField(reconAnnotator, "cache", null);

    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("");
    ReflectionTestUtils.setField(reconAnnotator, "webPageDownloader", mockDownloader);

    reconAnnotator.annotateElement(ion);
    assertEquals(1, getWarnings().size());
    assertTrue(getWarnings().get(0).getMessage().getFormattedMessage().contains("No recon annotations"));
  }

  @Test
  public void testSpeciesAnnotationWithReactionResponse() throws Exception {
    Ion ion = new Ion("id");
    ion.setName("O16G2e");

    // exclude first cached value
    ReflectionTestUtils.setField(reconAnnotator, "cache", null);

    String response = super.readFile("testFiles/annotation/recon_reaction_response.json");

    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn(response);
    ReflectionTestUtils.setField(reconAnnotator, "webPageDownloader", mockDownloader);

    reconAnnotator.annotateElement(ion);
    assertEquals(2, getWarnings().size());
  }

  @Test
  public void testReactionAnnotationWithChemicalResponse() throws Exception {
    Reaction reaction = new Reaction("re");
    reaction.setName("nad[m]");

    // exclude first cached value
    ReflectionTestUtils.setField(reconAnnotator, "cache", null);

    String response = super.readFile("testFiles/annotation/recon_chemical_response.json");

    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn(response);
    ReflectionTestUtils.setField(reconAnnotator, "webPageDownloader", mockDownloader);

    reconAnnotator.annotateElement(reaction);
    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testAnnotatingWithParentehesis() throws Exception {
    Reaction reaction = new Reaction("re");
    reaction.setName("gm2_hs[g]");
    reconAnnotator.annotateElement(reaction);
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testAnnotatingForOxygen() throws Exception {
    SimpleMolecule molecule = new SimpleMolecule("id");
    molecule.setName("h2o");
    reconAnnotator.annotateElement(molecule);
    assertTrue(molecule.getMiriamData().size() > 0);
    assertEquals(0, getWarnings().size());
    assertEquals("H2O", molecule.getFormula());
  }

  @Test
  public void testAnnotatingWithWhitespace() throws Exception {
    Reaction reaction = new Reaction("re");
    reaction.setName("NDPK4m ");
    reconAnnotator.annotateElement(reaction);
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testAnnotateElement() throws Exception {
    SimpleMolecule smallMolecule = new SimpleMolecule("id");
    smallMolecule.setAbbreviation("h2o");
    reconAnnotator.annotateElement(smallMolecule);
    assertTrue("No new annotations from recon db imported", smallMolecule.getMiriamData().size() > 0);
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testAnnotateElement2() throws Exception {
    Species smallMolecule = new SimpleMolecule("id");
    smallMolecule.setAbbreviation("P5CRm");
    reconAnnotator.annotateElement(smallMolecule);
    assertEquals(0, smallMolecule.getMiriamData().size());
    assertFalse(getWarnings().size() == 0);
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, reconAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(reconAnnotator, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.DOWN, reconAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateChangeStatus() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenReturn("{\"results\":[{}]}");
    ReflectionTestUtils.setField(reconAnnotator, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.CHANGED, reconAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateChangeStatus2() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenReturn("{\"results\":[{\"keggId\":\"C00001\",\"xxx\":\"yyy\"}]}");
    ReflectionTestUtils.setField(reconAnnotator, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.CHANGED, reconAnnotator.getServiceStatus().getStatus());
  }

}
