package lcsb.mapviewer.annotation.services.dapi;

import static org.mockito.ArgumentMatchers.anyString;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import lcsb.mapviewer.persist.dao.ConfigurationDao;

@Profile("dapiDownTestProfile")
@Configuration
public class DrugBankDapiWithProblemConfiguration {

  @Autowired
  private ConfigurationDao configurationDao;

  private String dapiLogin;
  private String dapiPassword;

  public DrugBankDapiWithProblemConfiguration() {
    dapiLogin = System.getenv("DAPI_TEST_LOGIN");
    dapiPassword = System.getenv("DAPI_TEST_PASSWORD");
  }

  @Bean
  @Primary
  public DapiConnector dapiConnector() throws DapiConnectionException {
    DapiConnectorImpl mock = Mockito.spy(new DapiConnectorImpl(configurationDao));
    Mockito.doReturn(dapiLogin).when(mock).getDapiLogin();
    Mockito.doReturn(dapiPassword).when(mock).getDapiPassword();

    Mockito.doThrow(new DapiConnectionException("Simulated connection problem", -1)).when(mock).getAuthenticatedContent(anyString());
    Mockito.doThrow(new DapiConnectionException("Simulated connection problem", -1)).when(mock).getLatestReleaseUrl(anyString());
    Mockito.doThrow(new DapiConnectionException("Simulated connection problem", -1)).when(mock).login();

    Mockito.doReturn(true).when(mock).isValidConnection();

    return mock;
  }

}
