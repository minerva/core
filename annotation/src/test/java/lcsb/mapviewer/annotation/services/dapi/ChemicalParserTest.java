package lcsb.mapviewer.annotation.services.dapi;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.user.UserDao;
import org.apache.commons.lang3.math.NumberUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

@ActiveProfiles("ctdTestProfile")
@Rollback
public class ChemicalParserTest extends AnnotationTestFunctions {
  private final MiriamData parkinsonDiseaseId = new MiriamData(MiriamType.MESH_2012, "D010300");
  private final MiriamData bloodLossDisease = new MiriamData(MiriamType.MESH_2012, "D016063");

  @Autowired
  private ChemicalParser chemicalParser;

  @Autowired
  private ProjectDao projectDao;

  @Autowired
  private UserDao userDao;

  @Before
  public void setUp() {
    assumeTrue("DAPI credentials are not provided", isDapiConfigurationAvailable());
  }

  @Test
  public void testGetChemicalsForDb() throws Exception {
    List<Chemical> result = chemicalParser.getChemicalsForDisease(bloodLossDisease);
    assertNotNull(result);
    assertFalse(result.isEmpty());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetChemicalListForInvalidDiseaseId() throws Exception {
    chemicalParser.getChemicalsForDisease(null);
  }

  @Test
  public void testCreateChemicalListFromDBWithInvalidId() throws Exception {
    MiriamData diseaseID = new MiriamData(MiriamType.MESH_2012, "D01030012");
    List<Chemical> result = chemicalParser.getChemicalsForDisease(diseaseID);
    assertNotNull(result);
    assertTrue(result.isEmpty());
  }

  @Test
  public void testFindByName() throws Exception {
    List<Chemical> chemicals = chemicalParser.getChemicalsByName(parkinsonDiseaseId, "stilbene oxide");
    assertEquals(1, chemicals.size());
    for (final Chemical chem : chemicals) {
      assertNotNull(chem);
      for (final Target t : chem.getInferenceNetwork()) {
        for (final MiriamData md : t.getReferences()) {
          assertEquals(MiriamType.PUBMED, md.getDataType());
          assertTrue(NumberUtils.isDigits(md.getResource()));
        }
      }
    }
  }

  @Test
  public void testGetGlutathioneDisulfideData() throws Exception {
    List<Chemical> chemicals = chemicalParser.getChemicalsByName(parkinsonDiseaseId, "Glutathione Disulfide");

    assertEquals(1, chemicals.size());
    Chemical glutathioneDisulfide = chemicals.get(0);

    assertFalse(glutathioneDisulfide.getSynonyms().isEmpty());
  }

  @Test
  public void testGetChemicalBySynonym() throws Exception {
    String glutathioneDisulfideSynonym = "GSSG";
    List<Chemical> chemicals = chemicalParser.getChemicalsByName(parkinsonDiseaseId, glutathioneDisulfideSynonym);

    assertEquals(1, chemicals.size());
    Chemical mptp = chemicals.get(0);

    assertTrue(mptp.getSynonyms().contains(glutathioneDisulfideSynonym));
  }

  @Test
  public void testFindByInvalidName() throws Exception {
    List<Chemical> chemicals = chemicalParser.getChemicalsByName(parkinsonDiseaseId, "blah");
    assertTrue(chemicals.isEmpty());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testFindByInvalidDiseaseId() throws Exception {
    chemicalParser.getChemicalsByName(null, "Glutathione Disulfide");
  }

  @Test
  public void testGetByTarget() throws Exception {
    MiriamData target = new MiriamData(MiriamType.HGNC_SYMBOL, "PTI");
    List<MiriamData> targets = new ArrayList<>();
    targets.add(target);

    List<Chemical> list = chemicalParser.getChemicalListByTargets(targets, bloodLossDisease);
    assertNotNull(list);
    assertFalse(list.isEmpty());
  }

  @Test
  public void testGetByTargetWithAnnotator() throws Exception {
    MiriamData target = new MiriamData(MiriamType.HGNC_SYMBOL, "PTI", String.class);
    List<MiriamData> targets = new ArrayList<>();
    targets.add(target);

    List<Chemical> list = chemicalParser.getChemicalListByTargets(targets, bloodLossDisease);
    assertNotNull(list);
    assertFalse(list.isEmpty());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetByInvalidTarget() throws Exception {
    MiriamData target = new MiriamData(MiriamType.WIKIPEDIA, "TNF");
    List<MiriamData> targets = new ArrayList<>();
    targets.add(target);
    chemicalParser.getChemicalListByTargets(targets, parkinsonDiseaseId);
  }

  @Test
  public void testGetByUnknownHgnc() throws Exception {
    MiriamData target = new MiriamData(MiriamType.HGNC_SYMBOL, "UNK_HGNC");
    List<Chemical> chemicals = chemicalParser.getChemicalListByTarget(target, parkinsonDiseaseId);

    assertEquals(0, chemicals.size());
  }

  @Test
  public void testGetByTarget2() throws Exception {
    MiriamData target = new MiriamData(MiriamType.HGNC_SYMBOL, "GALM");
    List<MiriamData> targets = new ArrayList<>();
    targets.add(target);
    List<Chemical> list = chemicalParser.getChemicalListByTargets(targets, parkinsonDiseaseId);
    assertNotNull(list);
    assertTrue(list.isEmpty());
  }

  @Test
  public void testGetEmptySuggestedQueryList() throws Exception {
    Project project = new Project(TEST_PROJECT_ID);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    projectDao.add(project);

    chemicalParser.refreshChemicalSuggestedQueryList(project.getId(), parkinsonDiseaseId);
    List<String> result = chemicalParser.getSuggestedQueryList(project, parkinsonDiseaseId);

    assertEquals(0, result.size());
  }

  @Test
  @Ignore
  public void testGetSuggestedQueryList() throws Exception {
    Project project = new Project(TEST_PROJECT_ID);
    project.setOwner(userDao.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    Model model = getModelForFile("testFiles/target_chemical/target.xml", false);
    project.addModel(model);
    projectDao.add(project);

    chemicalParser.refreshChemicalSuggestedQueryList(project.getId(), bloodLossDisease);
    List<String> result = chemicalParser.getSuggestedQueryList(project, bloodLossDisease);

    assertFalse(result.isEmpty());
  }

  @Test
  public void testGetSuggestedQueryListForUnknownDisease() throws Exception {
    Project project = new Project();
    Model model = getModelForFile("testFiles/target_chemical/target.xml", false);
    project.addModel(model);

    List<String> result = chemicalParser.getSuggestedQueryList(project, null);

    assertEquals(0, result.size());
  }

}
