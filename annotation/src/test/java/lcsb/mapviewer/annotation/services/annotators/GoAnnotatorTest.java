package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.data.Go;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

public class GoAnnotatorTest extends AnnotationTestFunctions {

  @Autowired
  private GoAnnotator goAnnotator;

  @Autowired
  private GeneralCacheInterface cache;

  private final WebPageDownloader webPageDownloader = new WebPageDownloader();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
    ReflectionTestUtils.setField(goAnnotator, "webPageDownloader", webPageDownloader);
    ReflectionTestUtils.setField(goAnnotator, "cache", cache);
  }

  @Test
  public void testContent() throws Exception {
    Compartment compartmentAlias = new Compartment("id");
    compartmentAlias
        .addMiriamData(new MiriamData(MiriamType.GO, "GO:0046902"));
    goAnnotator.annotateElement(compartmentAlias);
    assertNotEquals("", compartmentAlias.getFullName());
    assertNotEquals("", compartmentAlias.getNotes());
  }

  @Test
  public void testGetGoData() throws Exception {
    MiriamData md = new MiriamData(MiriamType.GO, "GO:0042644");
    Go go = goAnnotator.getGoElement(md);
    assertEquals("GO:0042644", go.getGoTerm());
    assertNotNull(go.getCommonName());
    assertNotNull(go.getDescription());
    assertTrue(go.getCommonName().toLowerCase().contains("chloroplast nucleoid"));
  }

  @Test(expected = GoSearchException.class)
  public void testAnnotateWhenProblemWithNetworkResponse() throws Exception {
    ReflectionTestUtils.setField(goAnnotator, "cache", null);

    MiriamData md = new MiriamData(MiriamType.GO, "GO:0042644");
    WebPageDownloader downloader = Mockito.mock(WebPageDownloader.class);
    when(downloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("");
    ReflectionTestUtils.setField(goAnnotator, "webPageDownloader", downloader);
    goAnnotator.getGoElement(md);
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    // exclude first cached value
    ReflectionTestUtils.setField(goAnnotator, "cache", null);

    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(goAnnotator, "webPageDownloader", mockDownloader);
    goAnnotator.refreshCacheQuery("http://google.pl/");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery() throws Exception {
    goAnnotator.refreshCacheQuery("invalid_query");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery2() throws Exception {
    goAnnotator.refreshCacheQuery(new Object());
  }

  @Test
  public void testRefreshCacheQuery() throws Exception {
    String res = (String) goAnnotator.refreshCacheQuery("http://google.pl/");
    assertNotNull(res);
  }

  @Test(timeout = 15000)
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, goAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(goAnnotator, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.DOWN, goAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenReturn("{\"numberOfHits\": 1,\"results\": [{\"name\":\"x\"}]}");
    ReflectionTestUtils.setField(goAnnotator, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.CHANGED, goAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateChangedStatus2() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenReturn("{\"numberOfHits\": 1,\"results\": [{}]}");
    ReflectionTestUtils.setField(goAnnotator, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.CHANGED, goAnnotator.getServiceStatus().getStatus());
  }

}
