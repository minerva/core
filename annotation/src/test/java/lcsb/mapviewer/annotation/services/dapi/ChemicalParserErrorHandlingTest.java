package lcsb.mapviewer.annotation.services.dapi;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.Assume.assumeTrue;

@ActiveProfiles("dapiDownTestProfile")
@Rollback(true)
public class ChemicalParserErrorHandlingTest extends AnnotationTestFunctions {
  private final MiriamData parkinsonDiseaseId = new MiriamData(MiriamType.MESH_2012, "D010300");

  @Autowired
  private ChemicalParser chemicalParser;

  @Before
  public void setUp() {
    assumeTrue("DAPI credentials are not provided", isDapiConfigurationAvailable());
  }

  @Test(expected = ChemicalSearchException.class)
  public void testGetForDisease() throws Exception {
    // Parkinson disease
    chemicalParser.getChemicalsForDisease(parkinsonDiseaseId);
  }

  @Test(expected = ChemicalSearchException.class)
  public void testGetByName() throws Exception {
    chemicalParser.getChemicalsByName(parkinsonDiseaseId, "Glutathione Disulfide");

  }

}
