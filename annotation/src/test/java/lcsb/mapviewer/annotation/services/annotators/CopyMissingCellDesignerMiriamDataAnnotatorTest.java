package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.persist.dao.map.ModelDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;

public class CopyMissingCellDesignerMiriamDataAnnotatorTest extends AnnotationTestFunctions {

  @Autowired
  private CopyMissingCellDesignerMiriamDataAnnotator copyMissingCellDesignerMiriamDataAnnotator;

  @Autowired
  private ModelDao modelDao;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testCopyNoMapDefined() throws Exception {
    GenericProtein protein = createProtein();
    copyMissingCellDesignerMiriamDataAnnotator.annotateElement(protein);
    assertEquals(0, protein.getMiriamData().size());
  }

  @Test
  public void testCopyBasic() throws Exception {
    ModelData model = createModelData();
    GenericProtein toProtein = createProtein();
    GenericProtein fromProtein = new GenericProtein(toProtein);
    fromProtein.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    model.addElement(toProtein);
    model.addElement(fromProtein);

    modelDao.add(model);

    copyMissingCellDesignerMiriamDataAnnotator.annotateElement(toProtein);
    assertEquals(1, toProtein.getMiriamData().size());
  }

  @Test
  public void testCopyNoMatchName() throws Exception {
    ModelData model = createModelData();
    GenericProtein toProtein = createProtein();
    GenericProtein fromProtein = createProtein();
    fromProtein.setName(toProtein.getName() + "blah");
    fromProtein.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    model.addElement(toProtein);
    model.addElement(fromProtein);

    modelDao.add(model);

    copyMissingCellDesignerMiriamDataAnnotator.annotateElement(toProtein);
    assertEquals(0, toProtein.getMiriamData().size());
  }

  @Test
  public void testCopyNoMatchClass() throws Exception {
    ModelData model = createModelData();
    GenericProtein toProtein = createProtein();
    Ion fromProtein = createIon();
    fromProtein.setName(toProtein.getName());
    fromProtein.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    model.addElement(toProtein);
    model.addElement(fromProtein);

    modelDao.add(model);

    copyMissingCellDesignerMiriamDataAnnotator.annotateElement(toProtein);
    assertEquals(0, toProtein.getMiriamData().size());
  }

  @Test
  public void testCopyNoMatchModel() throws Exception {
    ModelData model = createModelData();
    GenericProtein toProtein = createProtein();
    GenericProtein fromProtein = new GenericProtein(toProtein);
    fromProtein.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"));
    model.addElement(toProtein);

    modelDao.add(model);

    ModelData modelFrom = createModelData();
    modelFrom.addElement(fromProtein);

    modelDao.add(modelFrom);

    copyMissingCellDesignerMiriamDataAnnotator.annotateElement(toProtein);
    assertEquals(0, toProtein.getMiriamData().size());
  }

}
