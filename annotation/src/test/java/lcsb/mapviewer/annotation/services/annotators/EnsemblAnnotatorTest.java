package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

public class EnsemblAnnotatorTest extends AnnotationTestFunctions {

  @Autowired
  private EnsemblAnnotator ensemblAnnotator;

  @Autowired
  private GeneralCacheInterface cache;

  private final WebPageDownloader webPageDownloader = new WebPageDownloader();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
    ReflectionTestUtils.setField(ensemblAnnotator, "webPageDownloader", webPageDownloader);
    ReflectionTestUtils.setField(ensemblAnnotator, "cache", cache);
  }

  @Test
  public void testGetAnnotationsForEnsemblId() throws Exception {
    MiriamData nsmf = new MiriamData(MiriamType.ENSEMBL, "ENSG00000157764");
    GenericProtein proteinAlias = new GenericProtein("id");
    proteinAlias.addMiriamData(nsmf);
    ensemblAnnotator.annotateElement(proteinAlias);
    assertNotNull(proteinAlias.getSymbol());
    assertNotNull(proteinAlias.getFullName());
    assertTrue(proteinAlias.getMiriamData().size() > 1);
    assertFalse(proteinAlias.getSynonyms().isEmpty());

    boolean ensemble = false;
    boolean hgncId = false;
    boolean hgncSymbol = false;
    boolean entrez = false;
    for (final MiriamData md : proteinAlias.getMiriamData()) {
      if (MiriamType.ENSEMBL.equals(md.getDataType())) {
        ensemble = true;
      } else if (MiriamType.HGNC.equals(md.getDataType())) {
        hgncId = true;
      } else if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
        hgncSymbol = true;
      } else if (MiriamType.ENTREZ.equals(md.getDataType())) {
        entrez = true;
      }
    }

    assertTrue("Ensemble symbol cannot be found", ensemble);
    assertTrue("Hgnc id cannot be found", hgncId);
    assertTrue("Hgnc symbol cannot be found", hgncSymbol);
    assertTrue("Entrez cannot be found", entrez);

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testGetAnnotationsWhenMoreThanOneId() throws Exception {
    MiriamData nsmf = new MiriamData(MiriamType.ENSEMBL, "ENSG00000157764");
    MiriamData nsmf1 = new MiriamData(MiriamType.ENSEMBL, "ENSG00000157765");
    GenericProtein proteinAlias = new GenericProtein("id");
    proteinAlias.addMiriamData(nsmf);
    proteinAlias.addMiriamData(nsmf1);
    ensemblAnnotator.annotateElement(proteinAlias);

    assertEquals(3, getWarnings().size());
  }

  @Test
  public void testGetAnnotationsWhenNoIdFound() throws Exception {
    GenericProtein proteinAlias = new GenericProtein("id");
    ensemblAnnotator.annotateElement(proteinAlias);

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testGetAnnotationsForInvalid() throws Exception {
    MiriamData nsmf = new MiriamData(MiriamType.ENSEMBL, "blabla");
    GenericProtein proteinAlias = new GenericProtein("id");
    proteinAlias.addMiriamData(nsmf);
    ensemblAnnotator.annotateElement(proteinAlias);

    assertEquals("There should be warning about invalid ensembl identifier", 1, getWarnings().size());
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, ensemblAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testStatusDown() throws Exception {
    // exclude first cached value
    ReflectionTestUtils.setField(ensemblAnnotator, "cache", null);

    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(ensemblAnnotator, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.DOWN, ensemblAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testStatusChanged() throws Exception {
    // exclude first cached value
    ReflectionTestUtils.setField(ensemblAnnotator, "cache", null);

    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    String dataXml = "<opt></opt>";
    String versionXml = "<opt><data release=\"" + EnsemblAnnotatorImpl.SUPPORTED_VERSION + "\"/></opt>";
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn(dataXml)
        .thenReturn(versionXml);
    ReflectionTestUtils.setField(ensemblAnnotator, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.CHANGED, ensemblAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testStatusChanged2() throws Exception {
    // exclude first cached value
    ReflectionTestUtils.setField(ensemblAnnotator, "cache", null);

    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    String dataXml = "<opt></opt>";
    String versionXml = "<opt><data release=\"blablabla\"/></opt>";
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn(dataXml)
        .thenReturn(versionXml);
    ReflectionTestUtils.setField(ensemblAnnotator, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.CHANGED, ensemblAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testAnnotateWhenEntrezReturnWrongResponse() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new WrongResponseCodeIOException(null, 0));
    ReflectionTestUtils.setField(ensemblAnnotator, "webPageDownloader", mockDownloader);
    Species proteinAlias = new GenericProtein("id");
    proteinAlias.addMiriamData(new MiriamData(MiriamType.ENSEMBL, "1234"));
    ensemblAnnotator.annotateElement(proteinAlias);
    assertEquals(1, getWarnings().size());
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    // exclude first cached value
    ReflectionTestUtils.setField(ensemblAnnotator, "cache", null);

    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(ensemblAnnotator, "webPageDownloader", mockDownloader);
    ensemblAnnotator.refreshCacheQuery("https://google.pl/");
  }

}
