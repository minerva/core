package lcsb.mapviewer.annotation.services.genome;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import lcsb.mapviewer.annotation.SpringAnnotationTestConfig;
import lcsb.mapviewer.annotation.cache.BigFileCache;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.tests.TestUtils;
import lcsb.mapviewer.common.tests.UnitTestFailedWatcher;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.ReferenceGenome;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeGeneMapping;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;

@ContextConfiguration(classes = SpringAnnotationTestConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class UcscReferenceGenomeConnectorNoTransactionTest extends TestUtils {

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();

  @Autowired
  private ReferenceGenomeConnector connector;

  @Autowired
  private TaxonomyBackend taxonomyBackend;

  @Autowired
  private GeneralCacheInterface cache;

  private WebPageDownloader webPageDownloader = new WebPageDownloader();

  @After
  public void tearDown() throws Exception {
    ReflectionTestUtils.setField(connector, "webPageDownloader", webPageDownloader);
    ReflectionTestUtils.setField(connector, "cache", cache);
    ReflectionTestUtils.setField(connector, "taxonomyBackend", taxonomyBackend);
  }

  @Test
  public void testDownloadGenomeVersion() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "1570291");
    String version = "eboVir3";
    List<String> list = connector.getDownloadedGenomeVersions(yeast);
    if (list.contains(version)) {
      connector.removeGenomeVersion(yeast, version);
    }
    list = connector.getDownloadedGenomeVersions(yeast);

    assertFalse(list.contains(version));

    connector.downloadGenomeVersion(yeast, version, new IProgressUpdater() {
      @Override
      public void setProgress(final double progress) {
      }
    }, false);

    list = connector.getDownloadedGenomeVersions(yeast);
    assertTrue(list.contains(version));

    List<ReferenceGenome> genomes = connector.getByType(ReferenceGenomeType.UCSC);
    ReferenceGenome genome = null;
    for (final ReferenceGenome referenceGenome : genomes) {
      if (referenceGenome.getVersion().equals(version)) {
        genome = referenceGenome;
      }
    }
    assertNotNull(genome);
    connector.downloadGeneMappingGenomeVersion(genome, "test", null, false,
        "http://www.biodalliance.org/datasets/flyThickets.bb");

    genomes = connector.getByType(ReferenceGenomeType.UCSC);
    genome = null;
    for (final ReferenceGenome referenceGenome : genomes) {
      if (referenceGenome.getVersion().equals(version)) {
        genome = referenceGenome;
      }
    }
    assertNotNull(genome);
    assertEquals(1, genome.getGeneMapping().size());

    try {
      // try to download the same thing for the second time
      connector.downloadGeneMappingGenomeVersion(genome, "test", new IProgressUpdater() {
        @Override
        public void setProgress(final double progress) {
        }
      }, false, "http://www.biodalliance.org/datasets/flyThickets.bb");
      fail("Exception expected");
    } catch (final ReferenceGenomeConnectorException e) {
      assertTrue(e.getMessage().contains("already exists"));
    }

    try {
      // try to download something that is not big bed
      connector.downloadGeneMappingGenomeVersion(genome, "test2", new IProgressUpdater() {
        @Override
        public void setProgress(final double progress) {
        }
      }, false, "http://www.biodalliance.org/datasets/flyThickets.txt");
      fail("Exception expected");
    } catch (final ReferenceGenomeConnectorException e) {
      assertTrue(e.getMessage().contains("Only big bed format files are supported"));
    }

    // connector.removeGeneMapping(genome.getGeneMapping().get(0));

    int warningCount = getWarnings().size();

    // try to remove invalid mapping
    connector.removeGeneMapping(new ReferenceGenomeGeneMapping());

    assertEquals(warningCount + 1, getWarnings().size());

    connector.removeGenomeVersion(yeast, version);
  }

  @Test
  public void testDownloadGenomeVersionAsync() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "1570291");
    String version = "eboVir3";

    BigFileCache bigFileCache = connector.getBigFileCache();
    try {
      List<String> list = connector.getDownloadedGenomeVersions(yeast);
      if (list.contains(version)) {
        connector.removeGenomeVersion(yeast, version);
      }
      list = connector.getDownloadedGenomeVersions(yeast);

      // create a mock for ftp connections (so we speed up tests), the real
      // connection is tested elsewhere
      BigFileCache mockCache = Mockito.mock(BigFileCache.class);
      Mockito.doAnswer(new Answer<Void>() {

        @Override
        public Void answer(final InvocationOnMock invocation) throws Throwable {
          IProgressUpdater updater = (IProgressUpdater) invocation.getArguments()[2];
          updater.setProgress(100);
          return null;
        }
      }).when(mockCache).downloadFile(anyString(), any());

      connector.setBigFileCache(mockCache);

      assertFalse(list.contains(version));

      connector.downloadGenomeVersion(yeast, version, null, true);

      waitForDownload();

      list = connector.getDownloadedGenomeVersions(yeast);
      assertTrue(list.contains(version));

      List<ReferenceGenome> genomes = connector.getByType(ReferenceGenomeType.UCSC);
      ReferenceGenome genome = null;
      for (final ReferenceGenome referenceGenome : genomes) {
        if (referenceGenome.getVersion().equals(version)) {
          genome = referenceGenome;
        }
      }
      assertNotNull(genome);
      connector.downloadGeneMappingGenomeVersion(genome, "test", new IProgressUpdater() {
        @Override
        public void setProgress(final double progress) {
        }
      }, true, "http://www.biodalliance.org/datasets/flyThickets.bb");

      waitForDownload();

      genomes = connector.getByType(ReferenceGenomeType.UCSC);
      genome = null;
      for (final ReferenceGenome referenceGenome : genomes) {
        if (referenceGenome.getVersion().equals(version)) {
          genome = referenceGenome;
        }
      }
      assertNotNull(genome);
      assertEquals(1, genome.getGeneMapping().size());

      try {
        // try to download the same thing for the second time
        connector.downloadGeneMappingGenomeVersion(genome, "test", new IProgressUpdater() {
          @Override
          public void setProgress(final double progress) {
          }
        }, false, "http://www.biodalliance.org/datasets/flyThickets.bb");
        fail("Exception expected");
      } catch (final ReferenceGenomeConnectorException e) {
        assertTrue(e.getMessage().contains("already exists"));
      }

      try {
        // try to download something that is not big bed
        connector.downloadGeneMappingGenomeVersion(genome, "test2", new IProgressUpdater() {
          @Override
          public void setProgress(final double progress) {
          }
        }, false, "http://www.biodalliance.org/datasets/flyThickets.txt");
        fail("Exception expected");
      } catch (final ReferenceGenomeConnectorException e) {
        assertTrue(e.getMessage().contains("Only big bed format files are supported"));
      }

      connector.removeGeneMapping(genome.getGeneMapping().get(0));

      int warningCount = getWarnings().size();

      // try to remove invalid mapping
      connector.removeGeneMapping(new ReferenceGenomeGeneMapping());

      assertEquals(warningCount + 1, getWarnings().size());

      connector.removeGenomeVersion(yeast, version);
    } finally {
      connector.setBigFileCache(bigFileCache);
    }
  }

  @Test
  public void testDownloadGenomeVersionAsyncWithException() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "1570291");
    String version = "eboVir3";

    BigFileCache bigFileCache = connector.getBigFileCache();
    try {
      List<String> list = connector.getDownloadedGenomeVersions(yeast);
      if (list.contains(version)) {
        connector.removeGenomeVersion(yeast, version);
      }
      list = connector.getDownloadedGenomeVersions(yeast);

      // create a mock for ftp connections (so we speed up tests), the real
      // connection is tested elsewhere
      BigFileCache mockCache = Mockito.mock(BigFileCache.class);

      connector.setBigFileCache(mockCache);

      assertFalse(list.contains(version));

      connector.downloadGenomeVersion(yeast, version, new IProgressUpdater() {
        @Override
        public void setProgress(final double progress) {
        }
      }, true);

      waitForDownload();

      list = connector.getDownloadedGenomeVersions(yeast);
      assertTrue(list.contains(version));

      List<ReferenceGenome> genomes = connector.getByType(ReferenceGenomeType.UCSC);
      ReferenceGenome genome = null;
      for (final ReferenceGenome referenceGenome : genomes) {
        if (referenceGenome.getVersion().equals(version)) {
          genome = referenceGenome;
        }
      }
      assertNotNull(genome);

      Mockito.doThrow(new IOException()).when(mockCache).downloadFile(anyString(), any());
      assertEquals(0, getErrors().size());
      connector.downloadGeneMappingGenomeVersion(genome, "test", new IProgressUpdater() {
        @Override
        public void setProgress(final double progress) {
        }
      }, true, "http://www.biodalliance.org/datasets/flyThickets.bb");

      waitForDownload();

      // during this downlad we expect exception thrown by downloader
      assertEquals(1, getErrors().size());

      Mockito.doThrow(new OutOfMemoryError()).when(mockCache).downloadFile(anyString(), any());
      connector.downloadGeneMappingGenomeVersion(genome, "test2", new IProgressUpdater() {
        @Override
        public void setProgress(final double progress) {
        }
      }, true, "http://www.biodalliance.org/datasets/flyThickets.bb");

      waitForDownload();
      // during this downlad we expect error, this shouldn't change list of
      // errors
      assertEquals(1, getErrors().size());

      Mockito.doThrow(new URISyntaxException("", "")).when(mockCache).downloadFile(anyString(), any());
      connector.downloadGeneMappingGenomeVersion(genome, "test3", new IProgressUpdater() {
        @Override
        public void setProgress(final double progress) {
        }
      }, true, "http://www.biodalliance.org/datasets/flyThickets.bb");

      waitForDownload();

      // during this downlad we expect error (exception thrown by downloader)
      assertEquals(2, getErrors().size());

      genomes = connector.getByType(ReferenceGenomeType.UCSC);
      genome = null;
      for (final ReferenceGenome referenceGenome : genomes) {
        if (referenceGenome.getVersion().equals(version)) {
          genome = referenceGenome;
        }
      }
      assertNotNull(genome);
      assertEquals(3, genome.getGeneMapping().size());

      connector.removeGeneMapping(genome.getGeneMapping().get(2));
      connector.removeGeneMapping(genome.getGeneMapping().get(1));
      connector.removeGeneMapping(genome.getGeneMapping().get(0));
      connector.removeGenomeVersion(yeast, version);
    } finally {
      connector.setBigFileCache(bigFileCache);
    }
  }

  private void waitForDownload() throws InterruptedException {
    connector.waitForDownloadsToFinish();
  }
}
