package lcsb.mapviewer.annotation.services.dapi;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

@ActiveProfiles("drugBankTestProfile")
@Rollback(true)
public class DrugbankParserTest extends AnnotationTestFunctions {

  @Autowired
  private DrugBankParser drugBankParser;

  @Before
  public void setUp() {
    assumeTrue("DAPI credentials are not provided", isDapiConfigurationAvailable());
  }

  @Test
  public void testFindUrokinase() throws Exception {
    Drug test = drugBankParser.findDrug("Urokinase");
    assertNotNull(test);
    assertEquals(MiriamType.DRUGBANK, test.getSources().get(0).getDataType());
    assertEquals("DB00013", test.getSources().get(0).getResource());
    assertEquals("Urokinase", test.getName());
    assertTrue(test.getBloodBrainBarrier().equalsIgnoreCase("N/A"));
    boolean res = test.getDescription().contains(
        "Low molecular weight form of human urokinase, that consists of an A chain of 2,000 daltons linked "
            + "by a sulfhydryl bond to a B chain of 30,400 daltons. Recombinant urokinase plasminogen activator");
    assertTrue(res);
    assertEquals(10, test.getTargets().size());
    assertNull(test.getApproved());
  }

  @Test
  public void testFindRapamycin() throws Exception {
    // finding synonym
    Drug rapamycinDrug = drugBankParser.findDrug("Rapamycin");
    assertNotNull(rapamycinDrug);
    assertEquals("DB00877", rapamycinDrug.getSources().get(0).getResource());
    assertEquals("Sirolimus", rapamycinDrug.getName());
    boolean res = rapamycinDrug.getDescription().contains("A macrolide compound obtained from Streptomyces");
    assertTrue(res);
    assertEquals(3, rapamycinDrug.getTargets().size());
  }

  @Test
  public void testFindAfegostat() throws Exception {
    // finding synonym
    Drug afegostatDrug = drugBankParser.findDrug("Afegostat");
    assertNotNull(afegostatDrug);
    assertFalse(afegostatDrug.getApproved());
  }

  @Test
  public void testFindMethylamine() throws Exception {
    Drug drug = drugBankParser.findDrug("Methylamine");
    assertNotNull(drug);
    assertEquals("Methylamine", drug.getName());
    assertEquals("DB01828", drug.getSources().get(0).getResource());
    assertEquals(null, drug.getDescription());
    assertEquals("Number of targets is wrong", 1, drug.getTargets().size());

    for (final Target target : drug.getTargets()) {
      assertEquals("Invalid source id", new MiriamData(MiriamType.DRUGBANK_TARGET_V4, "BE0001338"), target.getSource());
      assertEquals("Invalid organism", new MiriamData(MiriamType.TAXONOMY, "83333"), target.getOrganism());
      assertEquals("Ammonia channel", target.getName());
      assertEquals("Number of target references is wrong", 2, target.getReferences().size());
      assertEquals(1, target.getGenes().size());
      assertNotNull(target.getType());
    }
  }

  @Test
  public void testFindInvalidDrug() throws Exception {
    Drug test = drugBankParser.findDrug("qwertyuiop");
    assertNull(test);
  }

  @Test
  public void test7FindDrug() throws Exception {
    Drug test = drugBankParser.findDrug("Aluminum hydroxide");
    assertNotNull(test);
    assertEquals("Aluminum hydroxide", test.getName());
    assertEquals("DB06723", test.getSources().get(0).getResource());
    assertTrue(test.getDescription().contains(
        "Aluminum hydroxide is an inorganic salt used as an antacid. "
            + "It is a basic compound that acts by neutralizing hydrochloric acid in gastric secretions. "
            + "Subsequent increases in pH may inhibit the action of pepsin. "
            + "An increase in bicarbonate ions and prostaglandins may also confer cytoprotective effects."));
    assertEquals(0, test.getTargets().size());
  }

  @Test
  public void testFindDrugAmantadineWithBrandNames() throws Exception {
    Drug drug = drugBankParser.findDrug("amantadine");

    assertNotNull(drug);
    assertTrue(drug.getBrandNames().contains("PK-Merz"));
    assertTrue(drug.getBrandNames().contains("Symadine"));
  }

  @Test
  public void testFindDrugSelegiline() throws Exception {
    Drug test = drugBankParser.findDrug("Selegiline");
    assertNotNull(test);
    assertEquals(2, test.getTargets().size());
    for (final String string : new String[]{"MAOA", "MAOB"}) {
      boolean contain = false;
      for (final Target target : test.getTargets()) {
        for (final MiriamData md : target.getGenes()) {
          if (md.getResource().equals(string)) {
            contain = true;
          }
        }
      }
      assertTrue("Missing traget: " + string, contain);
    }
  }

  @Test
  public void testFindDrugDaidzinWithHtmlCharacters() throws Exception {
    Drug test = drugBankParser.findDrug("Daidzin");
    assertNotNull(test);
    for (final String str : test.getSynonyms()) {
      assertFalse(str.contains("&#39;"));
    }
  }

  @Test
  public void testFindDrugGliclazideWithHtmlTags() throws Exception {
    Drug test = drugBankParser.findDrug("Gliclazide");
    assertNotNull(test);
    assertFalse(test.getDescription().contains("<span"));
  }

  @Test
  public void testFindDrugSevofluraneWithHtmlTags() throws Exception {
    Drug test = drugBankParser.findDrug("sevoflurane");
    assertNotNull(test);
    for (final String str : test.getSynonyms()) {
      assertFalse(str.contains("<div"));
    }
  }

  @Test
  public void testFindDrugByHgncTarget() throws Exception {
    List<Drug> drugs = drugBankParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "SIRT3"));
    assertNotNull(drugs);
    assertTrue("At least 1 drug was expected", drugs.size() >= 1);
  }

  @Test
  public void testFindDrugByHgncTargetWithAnnotatorAssigned() throws Exception {
    List<Drug> drugs = drugBankParser
        .getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "SIRT3", String.class));
    assertNotNull(drugs);
    assertTrue("At least 1 drug was expected", drugs.size() >= 1);
  }

  @Test
  public void testFindAluminiumByName() throws Exception {
    Drug drug = drugBankParser.findDrug("Aluminium");
    assertTrue(drug.getName().equalsIgnoreCase("Aluminium"));
  }

  @Test
  public void testFindDrugByHgncTargetAndFilteredOutByOrganism() throws Exception {
    List<MiriamData> organisms = new ArrayList<>();
    organisms.add(new MiriamData(MiriamType.TAXONOMY, "-1"));
    List<Drug> drugs = drugBankParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "SIRT3"),
        organisms);
    assertNotNull(drugs);
    assertEquals("No drugs for this organisms should be found", 0, drugs.size());
  }

  @Test
  public void testFindDrugByHgncTargetAndFilteredByOrganism() throws Exception {
    List<MiriamData> organisms = new ArrayList<>();
    organisms.add(TaxonomyBackend.HUMAN_TAXONOMY);
    List<Drug> drugs = drugBankParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "SIRT3"),
        organisms);
    assertNotNull(drugs);
    assertTrue(drugs.size() > 0);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testFindDrugByInvalidTarget() throws Exception {
    drugBankParser.getDrugListByTarget(new MiriamData(MiriamType.WIKIPEDIA, "h2o"));
  }

  @Test
  public void testFindDrugByEmptyTarget() throws Exception {
    List<Drug> drugs = drugBankParser.getDrugListByTarget(null);
    assertEquals(0, drugs.size());
  }

  @Test
  public void testFindDrugByHgncTargets() throws Exception {
    MiriamData target1 = new MiriamData(MiriamType.HGNC_SYMBOL, "SIRT3");
    MiriamData target2 = new MiriamData(MiriamType.HGNC_SYMBOL, "SIRT5");
    List<Drug> drug1 = drugBankParser.getDrugListByTarget(target1);
    List<Drug> drug2 = drugBankParser.getDrugListByTarget(target2);
    int size = drug1.size() + drug2.size();

    List<MiriamData> list = new ArrayList<>();
    list.add(target1);
    list.add(target2);

    List<Drug> drugs = drugBankParser.getDrugListByTargets(list);
    assertNotNull(drugs);
    assertTrue("Merged list should be shorter then sum of the two lists", drugs.size() < size);
  }

  @Test
  public void testByTargetsForCCNB1() throws Exception {
    List<MiriamData> targets = new ArrayList<>();
    MiriamData miriamTarget = new MiriamData(MiriamType.HGNC_SYMBOL, "CCNB1");
    targets.add(miriamTarget);
    List<Drug> drugs = drugBankParser.getDrugListByTargets(targets);
    for (final Drug drug : drugs) {
      boolean found = false;
      for (final Target target : drug.getTargets()) {
        for (final MiriamData md : target.getGenes()) {
          if (miriamTarget.equals(md)) {
            found = true;
          }
        }
      }
      assertTrue("Drug " + drug.getName() + " doesn't contain expected target", found);
    }
  }

  @Test
  public void testFindIbuprofen() throws Exception {
    Drug drug = drugBankParser.findDrug("Ibuprofen");
    assertNotNull(drug);
    assertEquals("Ibuprofen", drug.getName());
    assertEquals("DB01050", drug.getSources().get(0).getResource());
    assertNotNull(drug.getDescription());
    assertTrue(drug.getTargets().size() >= 8);
  }

  @Test
  public void testFindIronSaccharate() throws Exception {
    Drug drug = drugBankParser.findDrug("Iron saccharate");
    assertNotNull(drug);
    MiriamData tfProtein = new MiriamData(MiriamType.HGNC_SYMBOL, "TF");
    for (final Target target : drug.getTargets()) {
      for (final MiriamData md : target.getGenes()) {
        assertFalse("TF is a carrier, not a target", md.equals(tfProtein));
      }
    }
  }

  @Test
  public void testFindDopamine() throws Exception {
    Drug test = drugBankParser.findDrug("Dopamine");
    assertNotNull(test);
    for (final Target target : test.getTargets()) {
      assertFalse("Invalid target found", target.getName().contains("Details"));
    }
    assertTrue("Synonym not found", test.getSynonyms().contains("Dopamina"));
    assertEquals("Approved information is not available", Boolean.TRUE, test.getApproved());
  }

  @Test
  public void testFindDrugWithSpecialCharacters() throws Exception {
    Drug test = drugBankParser.findDrug("Thymidine-5'- Diphosphate");
    assertNotNull(test);
    assertFalse(test.getName().contains("&"));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery() throws Exception {
    drugBankParser.refreshCacheQuery("invalid_query");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery2() throws Exception {
    drugBankParser.refreshCacheQuery(new Object());
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, drugBankParser.getServiceStatus().getStatus());
  }

  @Test
  public void testGetEmptySuggestedQueryList() throws Exception {
    Project project = new Project();
    project.setId(-2);

    List<String> result = drugBankParser.getSuggestedQueryListWithoutCache(project, TaxonomyBackend.HUMAN_TAXONOMY);

    assertEquals(0, result.size());
  }

  @Test
  public void testGetSuggestedQueryList() throws Exception {
    Project project = new Project();
    project.setId(-1);
    Model model = getModelForFile("testFiles/target_drugbank/target.xml", false);
    project.addModel(model);

    List<String> result = drugBankParser.getSuggestedQueryListWithoutCache(project, TaxonomyBackend.HUMAN_TAXONOMY);

    assertFalse(result.isEmpty());
  }

  @Test
  public void testGetSuggestedQueryListForUnknownOrganism() throws Exception {
    Project project = new Project();
    project.setId(-2);
    Model model = getModelForFile("testFiles/target_drugbank/target.xml", false);
    project.addModel(model);

    List<String> result = drugBankParser.getSuggestedQueryList(project, null);

    assertEquals(0, result.size());
  }

}
