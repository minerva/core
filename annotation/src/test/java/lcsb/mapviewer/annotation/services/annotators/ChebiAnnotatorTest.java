package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.data.Chebi;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;

public class ChebiAnnotatorTest extends AnnotationTestFunctions {

  @Autowired
  private ChebiAnnotator backend;

  @Autowired
  private GeneralCacheInterface cache;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
    ReflectionTestUtils.setField(backend, "webPageDownloader", new WebPageDownloader());
    ReflectionTestUtils.setField(backend, "cache", cache);
  }

  @Test
  public void testGetIdByName() throws Exception {
    MiriamData id = backend.getChebiForChebiName("adenine");
    assertEquals(new MiriamData(MiriamType.CHEBI, "CHEBI:16708"), id);
    id = backend.getChebiForChebiName("asdf bvclcx lcxj vxlcvkj");
    assertNull(id);
    assertNull(backend.getChebiForChebiName(null));
  }

  @Test
  public void testNameById() throws Exception {
    String name = backend.getChebiNameForChebiId(new MiriamData(MiriamType.CHEBI, "CHEBI:16708"));
    assertEquals("adenine", name);
    name = backend.getChebiNameForChebiId(new MiriamData(MiriamType.CHEBI, "16708"));
    assertEquals("adenine", name);
    name = backend.getChebiNameForChebiId(new MiriamData(MiriamType.CHEBI, "CHEBI:16708a"));
    assertNull(name);
    name = backend.getChebiNameForChebiId(null);
    assertNull(name);
  }

  @Test
  public void testChebiById() throws Exception {
    Chebi chebi = backend.getChebiElementForChebiId(new MiriamData(MiriamType.CHEBI, "CHEBI:16708"));
    assertNotNull(chebi);
    assertEquals("adenine", chebi.getName());
    assertEquals("CHEBI:16708", chebi.getChebiId());
    assertEquals("Nc1ncnc2[nH]cnc12", chebi.getSmiles());
    assertTrue(chebi.getSynonyms().contains("ADENINE"));
  }

  @Test(expected = ChebiSearchException.class)
  public void testChebiByIdWhenConnectionFails() throws Exception {
    // exclude first cached value
    ReflectionTestUtils.setField(backend, "cache", null);

    WebPageDownloader webPageDownloaderMock = Mockito.mock(WebPageDownloader.class);
    Mockito.doThrow(IOException.class).when(webPageDownloaderMock).getFromNetwork(any(String.class));
    Mockito.doThrow(IOException.class).when(webPageDownloaderMock).getFromNetwork(any(String.class), any(String.class), any(String.class));
    ReflectionTestUtils.setField(backend, "webPageDownloader", webPageDownloaderMock);

    backend.getChebiElementForChebiId(new MiriamData(MiriamType.CHEBI, "CHEBI:16708"));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testChebiByInvalidId() throws Exception {
    backend.getChebiElementForChebiId(new MiriamData(MiriamType.WIKIPEDIA, "water"));
  }

  @Test
  public void testChebi() throws Exception {
    Chebi chebi = backend.getChebiElementForChebiId(new MiriamData(MiriamType.CHEBI, "CHEBI:16708"));
    assertNotNull(chebi);
    assertEquals("adenine", chebi.getName());
    assertEquals("Nc1ncnc2[nH]cnc12", chebi.getSmiles());
  }

  @Test
  public void testChebiNameToId1() throws Exception {
    // easy tests with some molecule
    MiriamData result = backend.getChebiForChebiName("L-glutamate(2-)");
    assertNotNull(result);
    assertTrue(result.getResource().contains("CHEBI:29988"));
    // unknown molecule
    MiriamData string = backend.getChebiForChebiName("blablasblasblsabsal");
    assertNull(string);

    // and water
    result = backend.getChebiForChebiName("H2O");

    assertNotNull(result);
    assertTrue(result.getResource().contains("CHEBI:15377"));
  }

  @Test
  public void testChebiNameToId4() throws Exception {
    // easy tests with some molecule
    MiriamData result = backend.getChebiForChebiName("Ca2+");
    assertEquals(new MiriamData(MiriamType.CHEBI, "CHEBI:29108"), result);
  }

  @Test
  public void testChebiNameToId2() throws Exception {
    // and now tests more complicated (how to find hydron?)
    MiriamData md = backend.getChebiForChebiName("hydron");
    assertNotNull(md);
    assertTrue(md.getResource().contains("CHEBI:15378"));
    md = backend.getChebiForChebiName("H+");
    assertNotNull(md);
    assertTrue(md.getResource().contains("CHEBI:15378"));
  }

  @Test
  public void testChebiNameToId3() throws Exception {
    // and the tricky one - I couldn't find it via chebi online search tool...
    // But java API do the trick...
    // assertEquals("CHEBI:456214",converter.chebiNameToId("NADP+"));
    // however manual mapping done by chemist says different...:
    MiriamData md = backend.getChebiForChebiName("NADP+");
    assertNotNull(md);
    assertTrue(md.getResource().contains("CHEBI:18009"));
    md = backend.getChebiForChebiName("NADP(+)");
    assertNotNull(md);
    assertTrue(md.getResource().contains("CHEBI:18009"));
  }

  @Test
  public void testAnnotateChemical() throws Exception {
    SimpleMolecule simpleMolecule = new SimpleMolecule("id");
    simpleMolecule.setName("water");

    backend.annotateElement(simpleMolecule);

    MiriamData inchi = null;
    MiriamData inchiKey = null;

    for (MiriamData md : simpleMolecule.getMiriamData()) {
      if (md.getDataType() == MiriamType.INCHI) {
        inchi = md;
      }
      if (md.getDataType() == MiriamType.INCHIKEY) {
        inchiKey = md;
      }
    }

    assertNotEquals("", simpleMolecule.getFullName());
    assertNotNull(inchi);
    assertNotNull(inchiKey);
    assertNotEquals("", simpleMolecule.getSmiles());
    assertNotEquals(0, simpleMolecule.getSynonyms().size());
  }

  @Test(expected = AnnotatorException.class)
  public void testAnnotateWhenConnectionFails() throws Exception {
    SimpleMolecule simpleMolecule = new SimpleMolecule("id");
    simpleMolecule.setName("water");

    WebPageDownloader webPageDownloaderMock = Mockito.mock(WebPageDownloader.class);
    Mockito.doThrow(IOException.class).when(webPageDownloaderMock).getFromNetwork(any(String.class));
    Mockito.doThrow(IOException.class).when(webPageDownloaderMock).getFromNetwork(any(String.class), any(String.class), any(String.class));
    ReflectionTestUtils.setField(backend, "webPageDownloader", webPageDownloaderMock);

    ReflectionTestUtils.setField(backend, "cache", null);
    backend.annotateElement(simpleMolecule);
  }

  @Test
  public void testAnnotateChemicalWithInvalidName() throws Exception {
    SimpleMolecule simpleMolecule = new SimpleMolecule("id");
    simpleMolecule.setName("blasdh");

    backend.annotateElement(simpleMolecule);

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testUpdateChemicalFromDb() throws Exception {
    SimpleMolecule simpleMolecule = new SimpleMolecule("id");
    simpleMolecule
        .addMiriamData(new MiriamData(MiriamType.CHEBI, "CHEBI:15377"));

    backend.annotateElement(simpleMolecule);

    MiriamData inchi = null;
    MiriamData inchiKey = null;

    for (MiriamData md : simpleMolecule.getMiriamData()) {
      if (md.getDataType() == MiriamType.INCHI) {
        inchi = md;
      }
      if (md.getDataType() == MiriamType.INCHIKEY) {
        inchiKey = md;
      }
    }
    assertNotEquals("", simpleMolecule.getFullName());
    assertNotNull(inchi);
    assertNotNull(inchiKey);
    assertNotEquals("", simpleMolecule.getSmiles());
    assertNotEquals(0, simpleMolecule.getSynonyms().size());
  }

  @Test
  public void testUpdateChemicalFromDbWithDifferentData() throws Exception {
    SimpleMolecule simpleMolecule = new SimpleMolecule("id");
    simpleMolecule.setName("water");
    simpleMolecule.setFullName("xxx");
    simpleMolecule.setSmiles("x");
    simpleMolecule.addSynonym("x");
    simpleMolecule
        .addMiriamData(new MiriamData(MiriamType.CHEBI, "CHEBI:15377"));

    backend.annotateElement(simpleMolecule);

    assertEquals(3, getWarnings().size());
  }

  @Test
  public void testGetChebiForChebiNameInFormula() throws Exception {
    String chebiId = "CHEBI:16236";
    String formulae = "C2H6O";

    MiriamData result = backend.getChebiForChebiName(formulae);
    assertNotNull(result);
    assertEquals(chebiId, result.getResource());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidQuery() throws Exception {
    backend.refreshCacheQuery("invalid string");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidQuery2() throws Exception {
    backend.refreshCacheQuery(new Object());
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, backend.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    ReflectionTestUtils.setField(backend, "cache", null);
    WebPageDownloader webPageDownloaderMock = Mockito.mock(WebPageDownloader.class);
    Mockito.doThrow(IOException.class).when(webPageDownloaderMock).getFromNetwork(any(String.class));
    Mockito.doThrow(IOException.class).when(webPageDownloaderMock).getFromNetwork(any(String.class), any(String.class), any(String.class));
    ReflectionTestUtils.setField(backend, "webPageDownloader", webPageDownloaderMock);
    assertEquals(ExternalServiceStatusType.DOWN, backend.getServiceStatus().getStatus());
  }

  @Test
  public void testAnnotateWithStitch() throws Exception {
    Species bioEntity = new SimpleMolecule("id");
    bioEntity.addMiriamData(new MiriamData(MiriamType.CHEBI, "CHEBI:35697"));

    backend.annotateElement(bioEntity);

    MiriamData mdStitch = null;

    int counter = 0;
    for (final MiriamData md : bioEntity.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.STITCH)) {
        mdStitch = md; // there should be only one EC number for that TAIR<->UNIPROT record
        counter++;
      }
    }

    assertNotNull("No STITCH annotation extracted from STITCH annotator", mdStitch);
    assertEquals("Wrong number of Stitch annotations ", 1, counter);
    assertTrue("Invalid STITCH annotation extracted from STITCH annotator based on CHEBI ID",
        mdStitch.getResource().equalsIgnoreCase("WBYWAXJHAXSJNI"));
  }

}
