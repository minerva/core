package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Article;

public class PubmedParserTest extends AnnotationTestFunctions {

  @Autowired
  protected GeneralCacheInterface permanentDatabaseLevelCacheInterface;

  @Autowired
  private PubmedParser pubmedParser;

  private boolean status2;

  @Autowired
  private GeneralCacheInterface cache;

  private WebPageDownloader webPageDownloader = new WebPageDownloader();

  @Before
  public void setUp() throws Exception {
    status2 = Configuration.isApplicationCacheOn();
  }

  @After
  public void tearDown() throws Exception {
    Configuration.setApplicationCacheOn(status2);
    ReflectionTestUtils.setField(pubmedParser, "webPageDownloader", webPageDownloader);
    ReflectionTestUtils.setField(pubmedParser, "cache", cache);
  }

  @Test
  public void test() throws PubmedSearchException {
    Configuration.setApplicationCacheOn(false);
    Article art = pubmedParser.getPubmedArticleById(9481670);
    assertNotNull(art);
    assertEquals(
        "Adjacent asparagines in the NR2-subunit of the NMDA receptor channel control the voltage-dependent block by extracellular Mg2+.",
        art.getTitle());
    assertEquals((Integer) 1998, art.getYear());
    assertEquals("The Journal of physiology", art.getJournal());
    assertTrue(art.getStringAuthors().contains("Wollmuth"));
    assertTrue(art.getStringAuthors().contains("Kuner"));
    assertTrue(art.getStringAuthors().contains("Sakmann"));
  }

  @Test
  public void testCache() throws PubmedSearchException {
    Configuration.setApplicationCacheOn(true);

    Article art = pubmedParser.getPubmedArticleById(9481671);
    assertNotNull(art);
  }

  /**
   * This case was problematic with old API used to retrieve data from pubmed
   */
  @Test
  public void testProblematicCase() throws PubmedSearchException {
    Article art = pubmedParser.getPubmedArticleById(22363258);
    assertNotNull(art);
  }

  @Test
  public void testCitationCount() throws PubmedSearchException {
    Article art = pubmedParser.getPubmedArticleById(18400456);
    assertNotNull(art);
    assertTrue(art.getCitationCount() >= 53);
  }

  @Test
  public void testGetSummary() throws Exception {
    String summary = pubmedParser.getSummary(18400456);
    assertNotNull(summary);
    assertFalse(summary.isEmpty());
  }

  @Test
  public void testGetSummary2() throws Exception {
    String summary = pubmedParser.getSummary(23644949);
    assertNull(summary);
  }

  @Test
  public void testGetSummary3() throws Exception {
    String summary = pubmedParser.getSummary("18400456");
    assertNotNull(summary);
    assertFalse(summary.isEmpty());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery() throws Exception {
    pubmedParser.refreshCacheQuery("invalid_query");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery2() throws Exception {
    pubmedParser.refreshCacheQuery(new Object());
  }

  @Test
  public void testGetHtmlFullLinkForId() throws Exception {
    String htmlString = pubmedParser.getHtmlFullLinkForId(1234);
    assertTrue(htmlString.contains("Change in the kinetics of sulphacetamide tissue distribution"));
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, pubmedParser.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(pubmedParser, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.DOWN, pubmedParser.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus2() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenReturn("<responseWrapper><version>" + PubmedParserImpl.SUPPORTED_VERSION + "</version></responseWrapper>");
    ReflectionTestUtils.setField(pubmedParser, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.DOWN, pubmedParser.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateChangeStatus() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn(
        "<responseWrapper><version>" + PubmedParserImpl.SUPPORTED_VERSION + "blabla</version></responseWrapper>");
    ReflectionTestUtils.setField(pubmedParser, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.CHANGED, pubmedParser.getServiceStatus().getStatus());
  }

  @Test
  public void testGetApiVersion() throws Exception {
    assertNotNull(pubmedParser.getApiVersion());
  }

  @Test(expected = PubmedSearchException.class)
  public void testGetApiVersionWhenProblemWithExternalDb() throws Exception {
    ReflectionTestUtils.setField(pubmedParser, "cache", null);
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenReturn("unknown response");
    ReflectionTestUtils.setField(pubmedParser, "webPageDownloader", mockDownloader);
    pubmedParser.getApiVersion();
  }

  @Test
  public void testGetApiVersionWhenProblemWithExternalDb3() throws Exception {
    ReflectionTestUtils.setField(pubmedParser, "cache", null);
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("<xml/>");
    ReflectionTestUtils.setField(pubmedParser, "webPageDownloader", mockDownloader);
    assertNull(pubmedParser.getApiVersion());
  }

  @Test(expected = PubmedSearchException.class)
  public void testGetApiVersionWhenProblemWithExternalDb2() throws Exception {
    ReflectionTestUtils.setField(pubmedParser, "cache", null);
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(pubmedParser, "webPageDownloader", mockDownloader);
    pubmedParser.getApiVersion();
  }
}
