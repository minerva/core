package lcsb.mapviewer.annotation.services;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.cache.XmlSerializer;
import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.data.TargetType;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.annotation.services.annotators.UniprotAnnotator;
import lcsb.mapviewer.annotation.services.annotators.UniprotSearchException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import org.apache.commons.lang3.SerializationException;
import org.junit.After;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;
import org.w3c.dom.Node;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

public class ChEMBLParserTest extends AnnotationTestFunctions {

  @Autowired
  private GeneralCacheInterface cache;

  @Autowired
  private ChEMBLParser chemblParser;

  @Autowired
  private UniprotAnnotator uniprotAnnotator;

  @Autowired
  private HgncAnnotator hgncAnnotator;

  @Autowired
  private TaxonomyBackend taxonomyBackend;

  private final WebPageDownloader webPageDownloader = new WebPageDownloader();

  @After
  public void tearDown() throws Exception {
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", webPageDownloader);
    ReflectionTestUtils.setField(chemblParser, "cache", cache);
    ReflectionTestUtils.setField(chemblParser, "uniprotAnnotator", uniprotAnnotator);
    ReflectionTestUtils.setField(chemblParser, "taxonomyBackend", taxonomyBackend);
    ReflectionTestUtils.setField(chemblParser, "hgncAnnotator", hgncAnnotator);
  }

  @Test
  public void test1FindDrug() throws Exception {
    Drug drug = chemblParser.findDrug("CABOZANTINIB");

    assertEquals("CHEMBL2105717", drug.getSources().get(0).getResource());
    assertEquals("CABOZANTINIB", drug.getName());
    assertNull(drug.getDescription());
    assertEquals(2, drug.getTargets().size());
    List<String> lowerCaseSynonyms = new ArrayList<>();
    for (final String s : drug.getSynonyms()) {
      lowerCaseSynonyms.add(s.toLowerCase());
    }
    assertTrue(lowerCaseSynonyms.contains("cabozantinib"));
    assertTrue(drug.getApproved());
  }

  @Test
  public void test2FindDrug() throws Exception {
    String n = "DIMETHYL FUMARATE";
    Drug test = chemblParser.findDrug(n);

    assertEquals("CHEMBL2107333", test.getSources().get(0).getResource());
    assertEquals("DIMETHYL FUMARATE", test.getName());
    // assertNull(test.getDescription());
    assertEquals(1, test.getTargets().size());
  }

  @Test
  public void test3FindDrug() throws Exception {
    String n = "LOMITAPIDE";
    Drug test = chemblParser.findDrug(n);

    assertEquals("CHEMBL354541", test.getSources().get(0).getResource());
    assertEquals("LOMITAPIDE", test.getName());
    assertNull(test.getDescription());
    assertEquals(1, test.getTargets().size());

    for (final Target t : test.getTargets()) {
      assertEquals(0, t.getReferences().size());
    }
  }

  @Test
  public void test4FindDrug() throws Exception {
    String n = "COBICISTAT";
    Drug test = chemblParser.findDrug(n);

    assertEquals("CHEMBL2095208", test.getSources().get(0).getResource());
    assertEquals("COBICISTAT", test.getName());
    assertNull(test.getDescription());
    assertEquals(1, test.getTargets().size());

    for (final Target t : test.getTargets()) {
      assertEquals(0, t.getReferences().size());
    }
  }

  @Test
  public void test5FindDrug() throws Exception {
    String n = "TALIGLUCERASE ALFA";
    Drug test = chemblParser.findDrug(n);

    assertEquals("CHEMBL1964120", test.getSources().get(0).getResource());
    assertEquals("TALIGLUCERASE ALFA", test.getName());
    assertNull(test.getDescription());
    assertEquals(1, test.getTargets().size());

    for (final Target t : test.getTargets()) {
      assertEquals(0, t.getReferences().size());
    }
  }

  @Test
  public void test6FindDrug() throws Exception {
    String n = "ABIRATERONE ACETATE";
    Drug test = chemblParser.findDrug(n);

    assertEquals("CHEMBL271227", test.getSources().get(0).getResource());
    assertEquals("ABIRATERONE ACETATE", test.getName());
    assertNull(test.getDescription());
    assertEquals(1, test.getTargets().size());

    for (final Target t : test.getTargets()) {
      assertEquals(0, t.getReferences().size());
    }
  }

  @Test
  public void test7FindDrug() throws Exception {
    String n = "blah_blah_blah";
    Drug test = chemblParser.findDrug(n);
    assertNull(test);
  }

  @Test
  public void test8FindDrug() throws Exception {
    String n = "aa a32";
    Drug test = chemblParser.findDrug(n);
    assertNull(test);
  }

  @Test
  public void test10FindDrug() throws Exception {
    String n = "DEXAMETHASONE";
    Drug test = chemblParser.findDrug(n);

    assertEquals("CHEMBL384467", test.getSources().get(0).getResource());
    assertEquals("DEXAMETHASONE", test.getName());
    assertNull(test.getDescription());
    assertEquals(1, test.getTargets().size());

    for (final Target t : test.getTargets()) {
      assertTrue(t.getReferences()
          .contains(new MiriamData(MiriamType.PUBMED, "16891588")));
      assertTrue(t.getReferences()
          .contains(new MiriamData(MiriamType.PUBMED, "16956592")));
      assertTrue(t.getReferences()
          .contains(new MiriamData(MiriamType.PUBMED, "16971495")));
    }
  }

  /**
   * It is impossible to get references just from target, that's why after
   * getTargetFromId PubMedRef should be empty
   */
  @Test
  public void test1getTargetFromId() throws Exception {
    Target test = chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL3717"));

    assertEquals(test.getSource().getResource(), "CHEMBL3717");
    assertEquals(test.getName(), "Hepatocyte growth factor receptor");
    assertEquals(test.getOrganism().getResource(), "9606");
    assertEquals(TargetType.SINGLE_PROTEIN, test.getType());
    boolean contains = false;
    for (final MiriamData md : test.getGenes()) {
      if (md.getResource().equals("MET")) {
        contains = true;
        break;
      }
    }
    assertTrue(contains);
    assertEquals(0, test.getReferences().size());
  }

  @Test
  public void testGetTargetWithRnaComponent() throws Exception {
    Target test = chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL2363135"));

    assertEquals(test.getSource().getResource(), "CHEMBL2363135");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetTargetFromInvalidId() throws Exception {
    chemblParser.getTargetFromId(new MiriamData(MiriamType.WIKIPEDIA, "water"));
  }

  @Test(expected = DrugSearchException.class)
  public void testGetTargetFromIdWhenProblemWithDbConnection() throws Exception {
    ReflectionTestUtils.setField(chemblParser, "cache", null);
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("invalid xml");
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "water"));

  }

  @Test(expected = DrugSearchException.class)
  public void testFindDrugWhenProblemWithDbConnection() throws Exception {
    ReflectionTestUtils.setField(chemblParser, "cache", null);
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("invalid xml");
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    chemblParser.findDrug("test");
  }

  @Test(expected = DrugSearchException.class)
  public void testGetTargetFromIdWhenProblemWithDbConnection2() throws Exception {
    ReflectionTestUtils.setField(chemblParser, "cache", null);
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "water"));
  }

  @Test(expected = DrugSearchException.class)
  public void testGetTargetFromIdWhenProblemWithUniprot() throws Exception {
    UniprotAnnotator mockAnnotator = Mockito.mock(UniprotAnnotator.class);
    when(mockAnnotator.uniProtToHgnc(any())).thenThrow(new UniprotSearchException(null, null));
    ReflectionTestUtils.setField(chemblParser, "uniprotAnnotator", mockAnnotator);
    chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL3717"));

  }

  @Test(expected = DrugSearchException.class)
  public void testGetTargetFromIdWhenProblemWithTaxonomy() throws Exception {
    TaxonomyBackend mockAnnotator = Mockito.mock(TaxonomyBackend.class);
    when(mockAnnotator.getNameForTaxonomy(any())).thenThrow(new TaxonomySearchException(null, null));
    ReflectionTestUtils.setField(chemblParser, "taxonomyBackend", mockAnnotator);
    chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL3717"));
  }

  @Test
  public void testGetTargetFromIdWithProblematicResponse() throws Exception {
    ReflectionTestUtils.setField(chemblParser, "cache", null);
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenReturn("<target><unk/></target>");
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "water"));
    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testGetTargetFromIdWithProblematicResponse2() throws Exception {
    ReflectionTestUtils.setField(chemblParser, "cache", null);
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenReturn("<target><target_components><target_component/></target_components></target>");
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "water"));
    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testGetTargetFromIdWithProblematicResponse3() throws Exception {
    ReflectionTestUtils.setField(chemblParser, "cache", null);
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenReturn("<target><target_chembl_id>inv id</target_chembl_id></target>");
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "water"));
    assertEquals(1, getWarnings().size());
  }

  @Test
  public void test2getTargetFromId() throws Exception {
    Target test = chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL3522"));

    assertEquals(test.getSource().getResource(), "CHEMBL3522");
    assertEquals(test.getName(), "Cytochrome P450 17A1");
    assertEquals(test.getOrganism().getResource(), "9606");
    boolean contains = false;
    for (final MiriamData md : test.getGenes()) {
      if (md.getResource().equals("CYP17A1")) {
        contains = true;
        break;
      }
    }
    assertTrue(contains);
    assertEquals(0, test.getReferences().size());
  }

  @Test
  public void test4getTargetFromId() throws Exception {
    Target test = chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL2364681"));

    assertEquals(test.getSource().getResource(), "CHEMBL2364681");
    assertEquals(test.getName(), "Microsomal triglyceride transfer protein");
    assertEquals(test.getOrganism().getResource(), "9606");
    boolean contains = false;
    boolean contains2 = false;
    for (final MiriamData md : test.getGenes()) {
      if (md.getResource().equals("MTTP")) {
        contains = true;
      }
      if (md.getResource().equals("P4HB")) {
        contains2 = true;
      }
    }
    assertTrue(contains);
    assertTrue(contains2);
    assertEquals(0, test.getReferences().size());
  }

  @Test
  public void test5getTargetFromId() throws Exception {
    Target test = chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL2364176"));

    assertEquals(test.getSource().getResource(), "CHEMBL2364176");
    assertEquals(test.getName(), "Glucocerebroside");
    assertEquals(test.getOrganism().getResource(), "9606");
    assertEquals(0, test.getGenes().size());
    assertEquals(0, test.getReferences().size());
  }

  @Test
  public void test6getTargetFromId() throws Exception {
    Target test = chemblParser.getTargetFromId(new MiriamData(MiriamType.CHEMBL_TARGET, "CHEMBL2069156"));

    assertEquals(test.getSource().getResource(), "CHEMBL2069156");
    assertEquals(test.getName(), "Kelch-like ECH-associated protein 1");
    assertEquals(test.getOrganism().getResource(), "9606");
    boolean contains = false;
    for (final MiriamData md : test.getGenes()) {
      if (md.getResource().equals("KEAP1")) {
        contains = true;
        break;
      }
    }
    assertTrue(contains);
    assertEquals(0, test.getReferences().size());
  }

  @Test
  public void testGetTargetsForAmantadine() throws Exception {
    Drug drug = chemblParser.findDrug("AMANTADINE");
    for (final Target target : drug.getTargets()) {
      assertEquals(0, target.getReferences().size());
    }
    assertFalse(drug.getTargets().isEmpty());
  }

  @Test
  public void testGetTargetsWithReferences() throws Exception {
    Drug drug = chemblParser.findDrug("TRIMETHOPRIM");
    for (final Target target : drug.getTargets()) {
      assertEquals(0, target.getReferences().size());
    }
    assertFalse(drug.getTargets().isEmpty());
  }

  @Test
  public void testFindDrugByInvalidHgncTarget() throws Exception {
    List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "NR3B"));
    assertNotNull(drugs);
  }

  @Test(expected = DrugSearchException.class)
  public void testFindDrugByHgncWhenHgncCrash() throws Exception {
    HgncAnnotator mockAnnotator = Mockito.mock(HgncAnnotator.class);
    when(mockAnnotator.hgncToUniprot(any())).thenThrow(new AnnotatorException(""));
    ReflectionTestUtils.setField(chemblParser, "hgncAnnotator", mockAnnotator);
    chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "NR3B"));
  }

  @Test
  public void testFindDrugByHgncTargetWithManyUniprot() throws Exception {
    List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "AKAP7"));
    assertNotNull(drugs);
    // we should have a warning that akap7 has more than one uniprot id
    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testFindDrugByUniprotTarget() throws Exception {
    List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.UNIPROT, "O60391"));
    assertFalse(drugs.isEmpty());
  }

  @Test(expected = DrugSearchException.class)
  public void testFindDrugByInvalidTarget() throws Exception {
    chemblParser.getDrugListByTarget(new MiriamData(MiriamType.MESH_2012, "D010300"));
  }

  @Test
  public void testFindDrugByHgncTarget() throws Exception {
    List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
    assertNotNull(drugs);
    assertFalse(drugs.isEmpty());
  }

  @Test
  public void testFindDrugByPFKMTarget() throws Exception {
    // this gene raised some errors at some point
    List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "PFKM"));
    assertNotNull(drugs);
  }

  @Test
  public void testFindDrugsByHgncTargets() throws Exception {
    List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
    List<Drug> drugs2 = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3A"));
    List<MiriamData> hgnc = new ArrayList<>();
    hgnc.add(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
    hgnc.add(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3A"));
    List<Drug> drugs3 = chemblParser.getDrugListByTargets(hgnc);
    assertNotNull(drugs3);
    assertTrue(drugs.size() + drugs2.size() > drugs3.size());
  }

  @Test
  public void testFindDrugByHgncTargetAndFilteredOutByOrganism() throws Exception {
    List<MiriamData> organisms = new ArrayList<>();
    organisms.add(new MiriamData(MiriamType.TAXONOMY, "-1"));
    List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"), organisms);
    assertNotNull(drugs);
    assertEquals("No drugs for this organisms should be found", 0, drugs.size());
  }

  @Test
  public void testFindDrugByHgncTargetAndFilteredByOrganism() throws Exception {
    List<MiriamData> organisms = new ArrayList<>();
    organisms.add(TaxonomyBackend.HUMAN_TAXONOMY);
    List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"), organisms);
    assertNotNull(drugs);
    assertFalse(drugs.isEmpty());
  }

  @Test
  public void testFindDrugsByRepeatingHgncTargets() throws Exception {
    List<Drug> drugs = chemblParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
    List<MiriamData> hgnc = new ArrayList<>();
    hgnc.add(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
    hgnc.add(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
    hgnc.add(new MiriamData(MiriamType.HGNC_SYMBOL, "GRIN3B"));
    List<Drug> drugs3 = chemblParser.getDrugListByTargets(hgnc);
    assertNotNull(drugs3);
    assertEquals(drugs.size(), drugs3.size());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery() throws Exception {
    chemblParser.refreshCacheQuery("invalid_query");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery2() throws Exception {
    chemblParser.refreshCacheQuery(new Object());
  }

  @Test
  public void testRefreshCacheQuery() throws Exception {
    Object res = chemblParser.refreshCacheQuery("https://google.pl/");
    assertNotNull(res);
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    // disable cache
    ReflectionTestUtils.setField(chemblParser, "cache", null);

    // simulate problem with downloading
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    chemblParser.refreshCacheQuery("https://google.pl/");
  }

  @Test(expected = DrugSearchException.class)
  public void testGetDrugsByChemblTargetWhenChemblCrash() throws Exception {
    // disable cache
    ReflectionTestUtils.setField(chemblParser, "cache", null);

    // simulate problem with downloading
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    chemblParser.getDrugsByChemblTarget(new MiriamData());
  }

  @Test(expected = DrugSearchException.class)
  public void testGetDrugsByChemblTargetWhenChemblCrash2() throws Exception {
    // disable cache
    ReflectionTestUtils.setField(chemblParser, "cache", null);

    // simulate problem with downloading
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("");
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    chemblParser.getDrugsByChemblTarget(new MiriamData());
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryWhenChemblDbNotAvailable() throws Exception {
    String query = ChEMBLParserImpl.NAME_PREFIX + "TRIMETHOPRIM";

    // disable cache
    ReflectionTestUtils.setField(chemblParser, "cache", null);

    // simulate problem with downloading
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    chemblParser.refreshCacheQuery(query);
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, chemblParser.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.DOWN, chemblParser.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenReturn("<response><molecules/></response>");
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.CHANGED, chemblParser.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateChangedStatus2() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);

    // valid xml but with empty data
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenReturn("<response><molecules>"
            + "<molecule><pref_name/><max_phase/><molecule_chembl_id/><molecule_synonyms/></molecule>"
            + "</molecules><mechanisms/><molecule_forms/></response>");
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.CHANGED, chemblParser.getServiceStatus().getStatus());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetTargetsByInvalidDrugId() throws Exception {
    chemblParser.getTargetsByDrugId(new MiriamData());
  }

  @Test(expected = DrugSearchException.class)
  public void testGetTargetsByDrugIdWithExternalDbDown() throws Exception {
    ReflectionTestUtils.setField(chemblParser, "cache", null);
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("invalid xml");
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    chemblParser.getTargetsByDrugId(new MiriamData(MiriamType.CHEMBL_COMPOUND, "123"));
  }

  @Test(expected = DrugSearchException.class)
  public void testGetTargetsByDrugIdWithExternalDbDown2() throws Exception {
    ReflectionTestUtils.setField(chemblParser, "cache", null);
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    chemblParser.getTargetsByDrugId(new MiriamData(MiriamType.CHEMBL_COMPOUND, "123"));
  }

  @Test(expected = DrugSearchException.class)
  public void testFindDrugByUniprotTargetWhenChemblCrash() throws Exception {
    ReflectionTestUtils.setField(chemblParser, "cache", null);
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    chemblParser.getDrugListByTarget(new MiriamData(MiriamType.UNIPROT, "O60391"));
  }

  @Test(expected = DrugSearchException.class)
  public void testFindDrugByUniprotTargetWhenChemblCrash2() throws Exception {
    ReflectionTestUtils.setField(chemblParser, "cache", null);
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("invalid xml");
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    chemblParser.getDrugListByTarget(new MiriamData(MiriamType.UNIPROT, "O60391"));
  }

  @Test
  public void testParseReferences() throws Exception {
    Node node = super.getXmlDocumentFromFile("testFiles/chembl/references.xml");
    assertNotNull(node);
    Set<MiriamData> references = chemblParser.parseReferences(node.getFirstChild());
    // we might have more references (if we decide to parse wikipedia, isbn, etc.)
    assertTrue(references.size() >= 3);
    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testParseReferencesWithSpecInId() throws Exception {
    Node node = super.getXmlDocumentFromFile("testFiles/chembl/references_with_space.xml");
    assertNotNull(node);
    Set<MiriamData> references = Mockito.mock(ChEMBLParserImpl.class, Mockito.CALLS_REAL_METHODS).parseReferences(node.getFirstChild());
    assertEquals(1, references.size());
    assertFalse(references.iterator().next().getResource().contains(" "));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetDrugByInvalidDrugId() throws Exception {
    chemblParser.getDrugById(new MiriamData());
  }

  @Test(expected = DrugSearchException.class)
  public void testGetDrugByDrugIdWhenChemblCrash() throws Exception {
    ReflectionTestUtils.setField(chemblParser, "cache", null);
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("invalid xml");
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    chemblParser.getDrugById(new MiriamData(MiriamType.CHEMBL_COMPOUND, ""));
  }

  @Test(expected = DrugSearchException.class)
  public void testGetDrugByDrugIdWhenChemblCrash2() throws Exception {
    ReflectionTestUtils.setField(chemblParser, "cache", null);
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    chemblParser.getDrugById(new MiriamData(MiriamType.CHEMBL_COMPOUND, ""));
  }

  @Test
  public void testParseSynonymsNode() throws Exception {
    Node node = super.getNodeFromXmlString(
        "<molecule_synonyms><synonym><synonyms>some synonym</synonyms></synonym><unknown_node/></molecule_synonyms>");
    List<String> synonyms = chemblParser.parseSynonymsNode(node);
    assertEquals(1, synonyms.size());
    assertEquals(1, getWarnings().size());
  }

  @Test(expected = DrugSearchException.class)
  public void testGetTargetsForChildElementsWhenChemblCrash() throws Exception {
    ReflectionTestUtils.setField(chemblParser, "cache", null);
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    chemblParser.getTargetsForChildElements(new MiriamData());
  }

  @Test(expected = DrugSearchException.class)
  public void testGetTargetsForChildElementsWhenChemblCrash2() throws Exception {
    ReflectionTestUtils.setField(chemblParser, "cache", null);
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn("invalid xml");
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    chemblParser.getTargetsForChildElements(new MiriamData());
  }

  @Test
  public void testGetTargetsForChildElementsWhenChemblReturnPartlyInvalidResult() throws Exception {
    ReflectionTestUtils.setField(chemblParser, "cache", null);
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenReturn("<response><molecule_forms><node/></molecule_forms></response>");
    ReflectionTestUtils.setField(chemblParser, "webPageDownloader", mockDownloader);
    chemblParser.getTargetsForChildElements(new MiriamData());
    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testFindDrugBryBrandName() throws Exception {
    Drug drug = chemblParser.findDrug("picato");
    assertNotNull(drug);
    assertEquals("CHEMBL1863513", drug.getSources().get(0).getResource());
  }

  @Test
  public void testFindDrugWhenSerializerFails() throws Exception {
    @SuppressWarnings("unchecked")
    XmlSerializer<Drug> serializer = Mockito.mock(XmlSerializer.class);
    when(serializer.xmlToObject(any())).thenThrow(new SerializationException());

    ReflectionTestUtils.setField(chemblParser, "drugSerializer", serializer);

    assertNotNull(chemblParser.findDrug("CABOZANTINIB"));
  }

  @Test
  public void testTargetComponentToMiriamDataForNonHgncUniprotId() throws Exception {
    Node node = getNodeFromXmlString("<xml><accession>P0C2L1</accession></xml>");
    MiriamData result = chemblParser.targetComponentToMiriamData(node);
    assertNull(result);
  }

  @Test
  public void testGetByProblematicId() throws Exception {
    Drug drug = chemblParser.getDrugById(new MiriamData(MiriamType.CHEMBL_COMPOUND, "CHEMBL3545284"));
    assertNull(drug);
  }

}
