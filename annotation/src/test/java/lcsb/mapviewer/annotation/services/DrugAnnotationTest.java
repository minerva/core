package lcsb.mapviewer.annotation.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.XmlSerializer;
import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.services.dapi.DrugBankParser;

@ActiveProfiles("drugBankTestProfile")
public class DrugAnnotationTest extends AnnotationTestFunctions {

  @Autowired
  private DrugBankParser drugBankParser;

  private XmlSerializer<Drug> serializer = new XmlSerializer<Drug>(Drug.class);

  @Before
  public void setUp() throws Exception {
    assumeTrue("DAPI credentials are not provided", isDapiConfigurationAvailable());
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testXmlSerialization() throws Exception {

    Drug drug = drugBankParser.findDrug("Aspirin");

    String xml = serializer.objectToString(drug);

    Drug drug2 = serializer.xmlToObject(getNodeFromXmlString(xml));

    assertEquals(drug.getTargets().size(), drug2.getTargets().size());
    assertEquals(drug.getTargets().get(0).getGenes().size(), drug2.getTargets().get(0).getGenes().size());
    assertEquals(drug.getTargets().get(0).getReferences().size(), drug2.getTargets().get(0).getReferences().size());
  }

}
