package lcsb.mapviewer.annotation.services.dapi;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

public class DapiConnectorImplTest {

  @Test
  public void testGetLatestVersion() {
    DapiConnectorImpl connector = new DapiConnectorImpl(null);
    String version = connector.getLatestVersion(Arrays.asList("2.2", "3.3", "11.2", "10"));
    assertEquals("11.2", version);
  }

  @Test
  public void testGetLatestVersionWithNoVersions() {
    DapiConnectorImpl connector = new DapiConnectorImpl(null);
    String version = connector.getLatestVersion(new ArrayList<>());
    assertEquals(null, version);
  }
}
