package lcsb.mapviewer.annotation.services.genome;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.BigFileCache;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.annotation.services.TaxonomySearchException;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.ReferenceGenome;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.persist.dao.map.layout.ReferenceGenomeDao;

public class UcscReferenceGenomeConnectorTest extends AnnotationTestFunctions {

  @Autowired
  private ReferenceGenomeConnector connector;

  @Autowired
  private ReferenceGenomeDao referenceGenomeDao;

  @Autowired
  private TaxonomyBackend taxonomyBackend;

  @Autowired
  private GeneralCacheInterface cache;

  private WebPageDownloader webPageDownloader = new WebPageDownloader();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
    ReflectionTestUtils.setField(connector, "webPageDownloader", webPageDownloader);
    ReflectionTestUtils.setField(connector, "cache", cache);
    ReflectionTestUtils.setField(connector, "taxonomyBackend", taxonomyBackend);
  }

  @Test
  public void testGetAvailableGenomeVersion() throws Exception {
    MiriamData human = new MiriamData(MiriamType.TAXONOMY, "9606");
    List<String> list = connector.getAvailableGenomeVersion(human);
    assertTrue(list.size() >= 17);
  }

  @Test(expected = ReferenceGenomeConnectorException.class)
  public void testGetAvailableGenomeVersionWhenProblemWithTaxonomyServer() throws Exception {
    MiriamData human = new MiriamData(MiriamType.TAXONOMY, "9606");
    // simulate problems with taxonomy server
    TaxonomyBackend taxonomyMock = Mockito.mock(TaxonomyBackend.class);
    when(taxonomyMock.getByName(anyString())).thenThrow(new TaxonomySearchException(null, null));
    connector.setTaxonomyBackend(taxonomyMock);

    connector.getAvailableGenomeVersion(human);
  }

  @Test
  public void testGetAvailableGenomeVersionForInvalidTaxonomy() throws Exception {
    MiriamData unknown = new MiriamData(MiriamType.TAXONOMY, "10101010101");
    List<String> list = connector.getAvailableGenomeVersion(unknown);
    assertEquals(0, list.size());
  }

  @Test
  public void testGetAvailableOrganisms() throws Exception {
    MiriamData human = new MiriamData(MiriamType.TAXONOMY, "9606");
    MiriamData chicken = new MiriamData(MiriamType.TAXONOMY, "9031");
    List<MiriamData> list = connector.getAvailableOrganisms();
    assertTrue("At least 40 different organism were expected but found: " + list.size(), list.size() > 40);

    assertTrue(list.contains(human));
    assertTrue(list.contains(chicken));
  }

  @Test(expected = ReferenceGenomeConnectorException.class)
  public void testGetAvailableOrganismsWhenUcscWebpageDown() throws Exception {
    // exclude first cached value
    connector.setCache(null);

    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(connector, "webPageDownloader", mockDownloader);
    connector.getAvailableOrganisms();
  }

  @SuppressWarnings("unchecked")
  @Test(expected = InvalidStateException.class)
  public void testDownloadGenomeVersionWhenInternalUrlIsInvalid() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "1570291");
    String version = "eboVir3";

    ExecutorService originalExecutorService = connector.getSyncExecutorService();
    try {
      ExecutorService executorService = Mockito.mock(ExecutorService.class);
      connector.setSyncExecutorService(executorService);
      Future<?> task = Mockito.mock(Future.class);
      when(task.get()).thenThrow(new ExecutionException(new URISyntaxException("", "")));
      when(executorService.submit(any(Callable.class))).thenReturn(task);

      connector.downloadGenomeVersion(yeast, version, new IProgressUpdater() {
        @Override
        public void setProgress(final double progress) {
        }
      }, false);
    } finally {
      connector.setSyncExecutorService(originalExecutorService);
    }
  }

  @Test
  public void testDownloadGenomeVersionWithException() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "1570291");
    String version = "eboVir3";

    BigFileCache bigFileCache = connector.getBigFileCache();
    try {
      List<String> list = connector.getDownloadedGenomeVersions(yeast);
      if (list.contains(version)) {
        connector.removeGenomeVersion(yeast, version);
      }
      list = connector.getDownloadedGenomeVersions(yeast);

      // create a mock for ftp connections (so we speed up tests), the real
      // connection is tested elsewhere
      BigFileCache mockCache = Mockito.mock(BigFileCache.class);

      connector.setBigFileCache(mockCache);

      assertFalse(list.contains(version));

      connector.downloadGenomeVersion(yeast, version, new IProgressUpdater() {
        @Override
        public void setProgress(final double progress) {
        }
      }, false);

      list = connector.getDownloadedGenomeVersions(yeast);
      assertTrue(list.contains(version));

      List<ReferenceGenome> genomes = referenceGenomeDao.getByType(ReferenceGenomeType.UCSC);
      ReferenceGenome genome = null;
      for (final ReferenceGenome referenceGenome : genomes) {
        if (referenceGenome.getVersion().equals(version)) {
          genome = referenceGenome;
        }
      }
      assertNotNull(genome);

      Mockito.doThrow(new IOException()).when(mockCache).downloadFile(anyString(), any());
      assertEquals(0, getErrors().size());
      try {
        connector.downloadGeneMappingGenomeVersion(genome, "test", new IProgressUpdater() {
          @Override
          public void setProgress(final double progress) {
          }
        }, false, "http://www.biodalliance.org/datasets/flyThickets.bb");
        fail("Exception expected");
      } catch (final IOException e) {

      }

      Mockito.doThrow(new RuntimeException()).when(mockCache).downloadFile(anyString(), any());
      try {
        connector.downloadGeneMappingGenomeVersion(genome, "test2", new IProgressUpdater() {
          @Override
          public void setProgress(final double progress) {
          }
        }, false, "http://www.biodalliance.org/datasets/flyThickets.bb");

        fail("Exception expected");
      } catch (final InvalidStateException e) {

      }

      Mockito.doThrow(new URISyntaxException("", "")).when(mockCache).downloadFile(anyString(), any());

      try {
        connector.downloadGeneMappingGenomeVersion(genome, "test3", new IProgressUpdater() {
          @Override
          public void setProgress(final double progress) {
          }
        }, false, "http://www.biodalliance.org/datasets/flyThickets.bb");
        fail("Exception expected");
      } catch (final URISyntaxException e) {

      }

      genomes = referenceGenomeDao.getByType(ReferenceGenomeType.UCSC);
      genome = null;
      for (final ReferenceGenome referenceGenome : genomes) {
        if (referenceGenome.getVersion().equals(version)) {
          genome = referenceGenome;
        }
      }
      assertNotNull(genome);
      referenceGenomeDao.refresh(genome);
      assertEquals(3, genome.getGeneMapping().size());

      connector.removeGeneMapping(genome.getGeneMapping().get(2));
      connector.removeGeneMapping(genome.getGeneMapping().get(1));
      connector.removeGeneMapping(genome.getGeneMapping().get(0));
      connector.removeGenomeVersion(yeast, version);
    } finally {
      connector.setBigFileCache(bigFileCache);
    }
  }

  @Test
  public void testGetGenomeVersionFile() throws Exception {
    MiriamData yeast = new MiriamData(MiriamType.TAXONOMY, "4932");
    String version = "sacCer3";
    String url = connector.getGenomeVersionFile(yeast, version);
    assertNotNull(url);
    assertTrue(url.contains("sacCer3.2bit"));
  }

  @Test(expected = FileNotAvailableException.class)
  public void testGetUnknownGenomeVersionFile() throws FileNotAvailableException, IOException {
    MiriamData human = new MiriamData(MiriamType.TAXONOMY, "9606");
    String version = "hg8";
    connector.getGenomeVersionFile(human, version);
  }

  @Test
  public void testGenomeVersionExtractorForInvalidGenome() throws Exception {
    int version = connector.extractInt("");
    assertEquals(0, version);
  }

  @Test
  public void testGenomeVersionExtractorForInvalidGenome2() throws Exception {
    int version = connector.extractInt("x");
    assertEquals(0, version);
  }

  @Test
  public void testGenomeVersionExtractorForValid() throws Exception {
    int version = connector.extractInt("x1");
    assertEquals(1, version);
  }

  @Test
  public void testGenomeVersionExtractorForValid2() throws Exception {
    int version = connector.extractInt("xy12z");
    assertEquals(12, version);
  }

}
