package lcsb.mapviewer.annotation.services;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

public class TaxonomyBackendTest extends AnnotationTestFunctions {

  @Autowired
  private TaxonomyBackend taxonomyBackend;

  @Autowired
  private GeneralCacheInterface cache;

  private final WebPageDownloader webPageDownloader = new WebPageDownloader();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
    ReflectionTestUtils.setField(taxonomyBackend, "webPageDownloader", webPageDownloader);
    ReflectionTestUtils.setField(taxonomyBackend, "cache", cache);
  }

  @Test
  public void testByName() throws Exception {
    MiriamData md = taxonomyBackend.getByName("Human");
    assertEquals(TaxonomyBackend.HUMAN_TAXONOMY, md);
  }

  @Test
  public void testByHumansName() throws Exception {
    MiriamData md = taxonomyBackend.getByName("Humans");
    assertEquals(TaxonomyBackend.HUMAN_TAXONOMY, md);
  }

  @Test
  public void testByComplexName() throws Exception {
    assertNotNull(taxonomyBackend.getByName("Aplysia californica (final Sea Hare)"));
  }

  @Test
  public void testByCommonName() throws Exception {
    assertNotNull(taxonomyBackend.getByName("Rat"));
  }

  @Test
  public void testByEmptyName() throws Exception {
    assertNull(taxonomyBackend.getByName(null));
    assertNull(taxonomyBackend.getByName(""));
  }

  @Test
  public void testByAbreviationName() throws Exception {
    assertNotNull(taxonomyBackend.getByName("C. elegans"));
    assertNotNull(taxonomyBackend.getByName("D. sechellia"));
    assertNotNull(taxonomyBackend.getByName("P. pacificus"));
    assertNotNull(taxonomyBackend.getByName("T. castaneum"));
  }

  @Test
  public void testGetName() throws Exception {
    String name = taxonomyBackend.getNameForTaxonomy(TaxonomyBackend.HUMAN_TAXONOMY);
    assertTrue("Taxonomy id should refer to homo sapien, but this found: " + name, name.contains("Homo sapien"));
    assertNotNull(taxonomyBackend.getNameForTaxonomy(new MiriamData(MiriamType.TAXONOMY, "9605")));
  }

  @Test
  public void testGetNameForInvalidTax() throws Exception {
    String name = taxonomyBackend.getNameForTaxonomy(new MiriamData(MiriamType.TAXONOMY, "-a-"));
    assertNull(name);
  }

  @Test
  public void testGetNameForInvalidInputData() throws Exception {
    String name = taxonomyBackend.getNameForTaxonomy(null);
    assertNull(name);
  }

  @Test
  public void testByName2() throws Exception {
    MiriamData md = taxonomyBackend.getByName("homo sapiens");
    assertEquals(md, TaxonomyBackend.HUMAN_TAXONOMY);
  }

  @Test
  public void testByName3() throws Exception {
    MiriamData md = taxonomyBackend.getByName("bla bla kiwi");
    assertNull(md);
  }

  @Test
  public void testByMusMusculusName() throws Exception {
    MiriamData md = taxonomyBackend.getByName("Mus musculus");
    assertEquals(md, new MiriamData(MiriamType.TAXONOMY, "10090"));
  }

  @Test
  public void testByMouseName() throws Exception {
    MiriamData md = taxonomyBackend.getByName("Mouse");
    assertEquals(md, new MiriamData(MiriamType.TAXONOMY, "10090"));
  }

  @Test
  public void testRefreshCacheQuery() throws Exception {
    String result = (String) taxonomyBackend.refreshCacheQuery(TaxonomyBackendImpl.TAXONOMY_CACHE_PREFIX + "homo sapiens");
    assertNotNull(result);
  }

  @Test
  public void testRefreshCacheQuery2() throws Exception {
    String result = (String) taxonomyBackend
        .refreshCacheQuery(TaxonomyBackendImpl.TAXONOMY_NAME_CACHE_PREFIX + TaxonomyBackend.HUMAN_TAXONOMY.getResource());
    assertNotNull(result);
  }

  @Test
  public void testRefreshCacheQuery3() throws Exception {
    String result = (String) taxonomyBackend.refreshCacheQuery("https://google.pl/");
    assertNotNull(result);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery() throws Exception {
    taxonomyBackend.refreshCacheQuery("unk_query");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery2() throws Exception {
    taxonomyBackend.refreshCacheQuery(new Object());
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(taxonomyBackend, "webPageDownloader", mockDownloader);
    ReflectionTestUtils.setField(taxonomyBackend, "cache", null);

    taxonomyBackend.refreshCacheQuery("https://google.pl/");
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, taxonomyBackend.getServiceStatus().getStatus());
  }

}
