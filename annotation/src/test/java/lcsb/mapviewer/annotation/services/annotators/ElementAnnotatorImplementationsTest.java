package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.SpringAnnotationTestConfig;
import lcsb.mapviewer.annotation.services.ModelAnnotator;
import lcsb.mapviewer.annotation.services.annotators.ElementAnnotator.BioEntityProxy;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;
import lcsb.mapviewer.model.user.annotator.BioEntityField;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(Parameterized.class)
public class ElementAnnotatorImplementationsTest extends AnnotationTestFunctions {
  private static AnnotationConfigApplicationContext applicationContext;

  // CHECKSTYLE.OFF
  @Parameter
  public String testName;

  @Parameter(1)
  public Class<? extends ElementAnnotator> elementAnnotator;

  @Parameter(2)
  public MiriamData sourceIdentifier;

  @Parameter(3)
  public BioEntity bioEntity;
  // CHECKSTYLE.ON

  @AfterClass
  public static void shutDown() {
    applicationContext.close();
  }

  @Parameters(name = "{0}")
  public static Collection<Object[]> data() throws Exception {
    List<Object[]> result = new ArrayList<>();
    applicationContext = new AnnotationConfigApplicationContext(SpringAnnotationTestConfig.class);

    ModelAnnotator modelAnnotator = (ModelAnnotator) applicationContext.getBean("modelAnnotator");
    for (final IElementAnnotator annotator : modelAnnotator.getAvailableAnnotators()) {
      if (annotator.getExampleValidAnnotation() != null) {
        Object[] row = new Object[]{
            unwrapProxy(annotator).getClass().getSimpleName(),
            annotator.getClass(),
            annotator.getExampleValidAnnotation(),
            null};

        if (annotator.isAnnotatable(TransportReaction.class)) {
          row[3] = new TransportReaction("re");
        } else if (annotator.isAnnotatable(GenericProtein.class)) {
          row[3] = new GenericProtein("");
        } else if (annotator.isAnnotatable(SimpleMolecule.class)) {
          row[3] = new SimpleMolecule("");
        } else if (annotator.isAnnotatable(SquareCompartment.class)) {
          row[3] = new SquareCompartment("");
        } else {
          throw new NotImplementedException(annotator.getValidClasses() + "");
        }
        result.add(row);
      }
    }
    return result;
  }

  @Test
  public void testExpectedOutputAppeared() throws Exception {
    IElementAnnotator annotator = applicationContext.getBean(elementAnnotator);

    for (final AnnotatorOutputParameter output : annotator.getAvailableOuputProperties()) {
      AnnotatorData parameters = annotator.createAnnotatorData();
      parameters.removeAnnotatorParameters(new ArrayList<>(parameters.getAnnotatorParams()));
      parameters.addAnnotatorParameter(output);
      BioEntityProxy sourceProxy = createBioEntityProxy(annotator, parameters, bioEntity);

      BioEntity copy = bioEntity.copy();

      BioEntityProxy proxy = createBioEntityProxy(annotator, parameters, copy);

      assertEquals(sourceProxy.getFieldValue(output), proxy.getFieldValue(output));

      copy.addMiriamData(sourceIdentifier);
      parameters.addAnnotatorParameters(annotator.getExampleValidParameters());
      annotator.annotateElement(copy, parameters);

      assertNotEquals("Field " + output + " wasn't set by annotator", sourceProxy.getFieldValue(output),
          proxy.getFieldValue(output));

    }
  }

  @Test
  public void testUnexpectedOutputAppeared() throws Exception {
    IElementAnnotator annotator = applicationContext.getBean(elementAnnotator);
    AnnotatorData parameters = annotator.createAnnotatorData();
    parameters.removeAnnotatorParameters(new ArrayList<>(parameters.getAnnotatorParams()));
    for (final BioEntityField field : BioEntityField.values()) {
      AnnotatorOutputParameter parameter = new AnnotatorOutputParameter(field);
      if (!annotator.getAvailableOuputProperties().contains(parameter)) {
        parameters.addAnnotatorParameter(parameter);
      }
    }
    for (final MiriamType type : MiriamType.values()) {
      AnnotatorOutputParameter parameter = new AnnotatorOutputParameter(type);
      if (!annotator.getAvailableOuputProperties().contains(parameter)) {
        parameters.addAnnotatorParameter(parameter);
      }
    }
    BioEntity sourceCopy = bioEntity.copy();
    BioEntity annotatedCopy = bioEntity.copy();
    sourceCopy.addMiriamData(sourceIdentifier);
    annotatedCopy.addMiriamData(sourceIdentifier);

    BioEntityProxy sourceProxy = createBioEntityProxy(annotator, parameters, sourceCopy);
    BioEntityProxy proxy = createBioEntityProxy(annotator, parameters, annotatedCopy);

    annotator.annotateElement(annotatedCopy, parameters);

    for (final AnnotatorOutputParameter output : parameters.getOutputParameters()) {
      assertEquals("Field " + output + " was set by annotator but it's not defined as output parameter",
          sourceProxy.getFieldValue(output),
          proxy.getFieldValue(output));
    }
  }

  @Test
  public void testOutputNotModifiedWithEmptyParams() throws Exception {
    IElementAnnotator annotator = applicationContext.getBean(elementAnnotator);
    AnnotatorData parameters = annotator.createAnnotatorData();
    parameters.removeAnnotatorParameters(new ArrayList<>(parameters.getAnnotatorParams()));
    parameters.addAnnotatorParameter(new AnnotatorOutputParameter(MiriamType.WIKIPEDIA));
    BioEntity sourceCopy = bioEntity.copy();
    BioEntity annotatedCopy = bioEntity.copy();
    sourceCopy.addMiriamData(sourceIdentifier);
    annotatedCopy.addMiriamData(sourceIdentifier);

    BioEntityProxy sourceProxy = createBioEntityProxy(annotator, parameters, sourceCopy);
    BioEntityProxy proxy = createBioEntityProxy(annotator, parameters, annotatedCopy);

    annotator.annotateElement(annotatedCopy, parameters);

    for (final BioEntityField field : BioEntityField.values()) {
      AnnotatorOutputParameter output = new AnnotatorOutputParameter(field);
      assertEquals("Field " + output + " was set by annotator but it's not defined as output parameter",
          sourceProxy.getFieldValue(output),
          proxy.getFieldValue(output));
    }
  }

  private BioEntityProxy createBioEntityProxy(final IElementAnnotator annotator, final AnnotatorData parameters, final BioEntity sourceCopy)
      throws Exception {
    return ((ElementAnnotator) unwrapProxy(annotator)).new BioEntityProxy(sourceCopy, parameters);
  }

  @Test
  public void testCacheTypeExists() throws Exception {
    IElementAnnotator annotator = applicationContext.getBean(elementAnnotator);
    assertNotNull(annotator.getCacheType());
  }

  private static Object unwrapProxy(final Object bean) throws Exception {

    /*
     * If the given object is a proxy, set the return value as the object being
     * proxied, otherwise return the given object.
     */
    if (AopUtils.isAopProxy(bean) && bean instanceof Advised) {

      Advised advised = (Advised) bean;

      return advised.getTargetSource().getTarget();
    } else {
      return bean;
    }
  }

}
