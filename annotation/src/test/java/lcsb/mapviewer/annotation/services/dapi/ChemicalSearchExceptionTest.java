package lcsb.mapviewer.annotation.services.dapi;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class ChemicalSearchExceptionTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor() {
    ChemicalSearchException e = new ChemicalSearchException("msg");
    assertEquals("msg", e.getMessage());
  }

}
