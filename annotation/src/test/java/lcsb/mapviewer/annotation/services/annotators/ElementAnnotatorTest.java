package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;
import lcsb.mapviewer.model.user.annotator.BioEntityField;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

public class ElementAnnotatorTest extends AnnotationTestFunctions {

  private static final AnnotatorData allOutputFieldsAndAnnotations = new AnnotatorData(Object.class);
  private final ElementAnnotator annotator = Mockito.mock(ElementAnnotator.class, Mockito.CALLS_REAL_METHODS);
  @Autowired
  private HgncAnnotator autowiredAnnotator;

  @Autowired
  private GeneralCacheInterface cache;

  {
    for (final BioEntityField field : BioEntityField.values()) {
      allOutputFieldsAndAnnotations.addAnnotatorParameter(new AnnotatorOutputParameter(field));
    }
    for (final MiriamType type : MiriamType.values()) {
      allOutputFieldsAndAnnotations.addAnnotatorParameter(new AnnotatorOutputParameter(type));
    }
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSetNotMatchingSymbol() {
    final GenericProtein species = new GenericProtein("id");
    species.setSymbol("X");
    final ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setSymbol("Y");

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testSetNotMatchingFullname() {
    final GenericProtein species = new GenericProtein("id");
    species.setFullName("X");
    final ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setFullName("Y");

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testSetNotMatchingNotes() {
    final GenericProtein species = new GenericProtein("id");
    species.setNotes("X");
    final ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setDescription("Y");

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testSetEmptyDescription() {
    final GenericProtein species = new GenericProtein("id");
    species.setNotes("X");
    final ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setDescription(null);
    assertEquals("X", species.getNotes());
  }

  @Test
  public void testSetNotMatchingSmile() {
    final Ion species = new Ion("id");
    final ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setSmile("X");
    proxy.setSmile("Y");

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testSetNotMatchingCharge() {
    final Ion species = new Ion("id");
    species.setCharge(2);
    final ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setCharge("3");

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testSetNotMatchingSubsystem() {
    final Reaction species = new Reaction("re");
    species.setSubsystem("X");
    final ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setSubsystem("Y");

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testSetNotMatchingFormula() {
    final Reaction species = new Reaction("re");
    species.setFormula("X");
    final ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setFormula("Y");

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testSetNotMatchingAbbreviation() {
    final Reaction species = new Reaction("re");
    species.setAbbreviation("X");
    final ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setAbbreviation("Y");

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testSetNotMatchingMCS() {
    final Reaction species = new Reaction("re");
    species.setMechanicalConfidenceScore(1);
    final ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setMechanicalConfidenceScore("2");

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testSetTheSameMcs() throws Exception {
    final Reaction reaction = new Reaction("re");
    reaction.setMechanicalConfidenceScore(4);
    final ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(reaction, allOutputFieldsAndAnnotations);
    proxy.setMechanicalConfidenceScore("4");
    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testAddKegg() {
    final Reaction reaction = new Reaction("re");
    final ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(reaction, allOutputFieldsAndAnnotations);
    proxy.addMiriamData(new MiriamData(MiriamType.KEGG_COMPOUND, "R123"));
    assertEquals(1, reaction.getMiriamData().size());
  }

  @Test
  public void testSetSynonymsWarning() {
    final GenericProtein species = new GenericProtein("1");
    species.addSynonym("X");
    final ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setSynonyms(Collections.singletonList("Y"));

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testSetTheSameSynonyms() {
    final GenericProtein species = new GenericProtein("1");
    species.addSynonym("X");
    species.addSynonym("Y");
    final ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(species, allOutputFieldsAndAnnotations);
    proxy.setSynonyms(Arrays.asList("Y", "X"));

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testRefreshCacheQuery() throws Exception {
    final Object res = autowiredAnnotator.refreshCacheQuery("http://google.cz/");
    assertNotNull(res);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery() throws Exception {
    autowiredAnnotator.refreshCacheQuery("invalid_query");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery2() throws Exception {
    autowiredAnnotator.refreshCacheQuery(new Object());
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    try {
      // exclude first cached value
      ReflectionTestUtils.setField(autowiredAnnotator, "cache", null);

      final WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      ReflectionTestUtils.setField(autowiredAnnotator, "webPageDownloader", mockDownloader);
      autowiredAnnotator.refreshCacheQuery("http://google.pl/");
    } finally {
      ReflectionTestUtils.setField(autowiredAnnotator, "webPageDownloader", new WebPageDownloader());
      ReflectionTestUtils.setField(autowiredAnnotator, "cache", cache);
    }
  }

  @Test
  public void testMiriamWithNullResource() {
    final Reaction reaction = new Reaction("re");
    final ElementAnnotator.BioEntityProxy proxy = annotator.new BioEntityProxy(reaction, allOutputFieldsAndAnnotations);
    proxy.addMiriamData(new MiriamData(MiriamType.KEGG_COMPOUND, null));
    assertEquals(0, reaction.getMiriamData().size());
  }

}
