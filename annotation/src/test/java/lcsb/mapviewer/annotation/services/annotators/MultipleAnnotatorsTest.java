package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class MultipleAnnotatorsTest extends AnnotationTestFunctions {

  @Autowired
  private KeggAnnotator keggAnnotator;

  @Autowired
  private UniprotAnnotator uniprotAnnotator;

  @Autowired
  private HgncAnnotator hgncAnnotator;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAnnotateUniprotByUniprotAndKegg() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "P12345"));

    keggAnnotator.annotateElement(protein);
    uniprotAnnotator.annotateElement(protein);
    hgncAnnotator.annotateElement(protein);
    // biocompendiumAnnotator.annotateElement(protein);

    int cntNoAnnotator = 0;
    for (final MiriamData md : protein.getMiriamData()) {
      if (md.getAnnotator() == null) {
        cntNoAnnotator++;
      }
    }

    assertEquals("Wrong number of annotated elements with no information about annotator", 1, cntNoAnnotator);
  }

}
