package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

public class HgncAnnotatorTest extends AnnotationTestFunctions {

  @Autowired
  private HgncAnnotator hgncAnnotator;

  @Autowired
  private GeneralCacheInterface cache;

  private final WebPageDownloader webPageDownloader = new WebPageDownloader();

  @Before
  public void setUp() {
  }

  @After
  public void tearDown() throws Exception {
    ReflectionTestUtils.setField(hgncAnnotator, "webPageDownloader", webPageDownloader);
    ReflectionTestUtils.setField(hgncAnnotator, "cache", cache);
  }

  @Test
  public void testGetAnnotationsForSNCA() throws Exception {
    MiriamData snca = new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA");
    GenericProtein proteinAlias = new GenericProtein("id");
    proteinAlias.addMiriamData(snca);
    hgncAnnotator.annotateElement(proteinAlias);
    assertNotNull(proteinAlias.getSymbol());
    assertTrue(proteinAlias.getFormerSymbols().size() > 0);
    assertNotNull(proteinAlias.getFullName());
    assertTrue(proteinAlias.getMiriamData().size() > 1);
    assertTrue(proteinAlias.getSynonyms().size() > 0);

    boolean ensemble = false;
    boolean hgncId = false;
    boolean hgncSymbol = false;
    boolean refseq = false;
    boolean entrez = false;
    boolean uniprot = false;
    for (final MiriamData md : proteinAlias.getMiriamData()) {
      if (MiriamType.ENSEMBL.equals(md.getDataType())) {
        ensemble = true;
      } else if (MiriamType.HGNC.equals(md.getDataType())) {
        assertEquals("Invalid HGNC id", "11138", md.getResource());
        hgncId = true;
      } else if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
        hgncSymbol = true;
      } else if (MiriamType.REFSEQ.equals(md.getDataType())) {
        refseq = true;
      } else if (MiriamType.ENTREZ.equals(md.getDataType())) {
        entrez = true;
      } else if (MiriamType.UNIPROT.equals(md.getDataType())) {
        uniprot = true;
      }
    }

    assertTrue("Ensemble symbol cannot be found", ensemble);
    assertTrue("Hgnc id cannot be found", hgncId);
    assertTrue("Hgnc symbol cannot be found", hgncSymbol);
    assertTrue("RefSeq cannot be found", refseq);
    assertTrue("Entrez cannot be found", entrez);
    assertTrue("Uniprot cannot be found", uniprot);
  }

  @Test
  public void testGetAnnotationsForElementWithMultiHGNC() throws Exception {
    MiriamData snca = new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA");
    MiriamData park7 = new MiriamData(MiriamType.HGNC_SYMBOL, "PARK7");
    GenericProtein proteinAlias = new GenericProtein("id");
    proteinAlias.setName("SNCA");
    proteinAlias.addMiriamData(snca);
    proteinAlias.addMiriamData(park7);
    hgncAnnotator.annotateElement(proteinAlias);

    assertEquals(4, getWarnings().size());
  }

  @Test
  public void testGetAnnotationsForHGNC_ID() throws Exception {
    MiriamData nsmf = new MiriamData(MiriamType.HGNC, "11138");
    GenericProtein proteinAlias = new GenericProtein("id");
    proteinAlias.addMiriamData(nsmf);
    hgncAnnotator.annotateElement(proteinAlias);
    assertNotNull(proteinAlias.getSymbol());
    assertFalse(proteinAlias.getFormerSymbols().isEmpty());
    assertNotNull(proteinAlias.getFullName());
    assertTrue(proteinAlias.getMiriamData().size() > 1);
    assertFalse(proteinAlias.getSynonyms().isEmpty());

    boolean ensemble = false;
    boolean hgncId = false;
    boolean hgncSymbol = false;
    boolean refseq = false;
    boolean entrez = false;
    for (final MiriamData md : proteinAlias.getMiriamData()) {
      if (MiriamType.ENSEMBL.equals(md.getDataType())) {
        ensemble = true;
      } else if (MiriamType.HGNC.equals(md.getDataType())) {
        hgncId = true;
      } else if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
        hgncSymbol = true;
      } else if (MiriamType.REFSEQ.equals(md.getDataType())) {
        refseq = true;
      } else if (MiriamType.ENTREZ.equals(md.getDataType())) {
        entrez = true;
      }
    }

    assertTrue("Ensemble symbol cannot be found", ensemble);
    assertTrue("Hgnc id cannot be found", hgncId);
    assertTrue("Hgnc symbol cannot be found", hgncSymbol);
    assertTrue("RefSeq cannot be found", refseq);
    assertTrue("Entrez cannot be found", entrez);
  }

  @Test
  public void testGetAnnotationsForSNCA2() throws Exception {
    GenericProtein proteinAlias = new GenericProtein("id");
    proteinAlias.setName("SNCA");
    hgncAnnotator.annotateElement(proteinAlias);
    assertNotNull(proteinAlias.getSymbol());
    assertNotNull(proteinAlias.getName());
    assertTrue(proteinAlias.getFormerSymbols().size() > 0);
    assertNotNull(proteinAlias.getFullName());
    assertTrue(proteinAlias.getMiriamData().size() > 1);
    assertTrue(proteinAlias.getSynonyms().size() > 0);

    boolean ensemble = false;
    boolean hgncId = false;
    boolean hgncSymbol = false;
    boolean refseq = false;
    boolean entrez = false;
    for (final MiriamData md : proteinAlias.getMiriamData()) {
      if (MiriamType.ENSEMBL.equals(md.getDataType())) {
        ensemble = true;
      } else if (MiriamType.HGNC.equals(md.getDataType())) {
        hgncId = true;
      } else if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
        hgncSymbol = true;
      } else if (MiriamType.REFSEQ.equals(md.getDataType())) {
        refseq = true;
      } else if (MiriamType.ENTREZ.equals(md.getDataType())) {
        entrez = true;
      }
    }

    assertTrue("Ensemble symbol cannot be found", ensemble);
    assertTrue("Hgnc id cannot be found", hgncId);
    assertTrue("Hgnc symbol cannot be found", hgncSymbol);
    assertTrue("RefSeq cannot be found", refseq);
    assertTrue("Entrez cannot be found", entrez);
  }

  @Test
  public void testGetAnnotationsForBID() throws Exception {
    GenericProtein bidProtein = new GenericProtein("id");
    bidProtein.setName("BID");
    hgncAnnotator.annotateElement(bidProtein);

    GenericProtein bidMutationProtein = new GenericProtein("id2");
    bidMutationProtein.setName("BID (p15)");
    hgncAnnotator.annotateElement(bidMutationProtein);

    assertEquals(bidProtein.getSymbol(), bidMutationProtein.getSymbol());
    assertEquals(bidProtein.getFormerSymbols(), bidMutationProtein.getFormerSymbols());
    assertEquals(bidProtein.getFullName(), bidMutationProtein.getFullName());
    assertEquals(bidProtein.getMiriamData(), bidMutationProtein.getMiriamData());
    assertEquals(bidProtein.getSynonyms(), bidMutationProtein.getSynonyms());
  }

  @Test
  public void testGetAnnotationsForInvalid() throws Exception {
    Species proteinAlias = new GenericProtein("id");
    proteinAlias.setName("UNKNNOWNASD asd");
    hgncAnnotator.annotateElement(proteinAlias);

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testGetAnnotationsForInvalid2() throws Exception {
    Species proteinAlias = new GenericProtein("id");
    proteinAlias.setName("cAMP/cGMP-dependent protein kinase");
    hgncAnnotator.annotateElement(proteinAlias);

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, hgncAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testGetAnnotationsForInvalidMiriamSet() throws Exception {
    MiriamData md1 = new MiriamData(MiriamType.HGNC, "11138");
    MiriamData md2 = new MiriamData(MiriamType.HGNC, "111382");
    Species proteinAlias = new GenericProtein("");
    proteinAlias.addMiriamData(md1);
    proteinAlias.addMiriamData(md2);
    hgncAnnotator.annotateElement(proteinAlias);
    assertEquals(1, getWarnings().size());
    assertEquals("SNCA", proteinAlias.getName());
  }

  @Test
  public void testHgncIdToUniprot() throws Exception {
    List<MiriamData> result = hgncAnnotator.hgncToUniprot(new MiriamData(MiriamType.HGNC, "11138"));
    assertEquals(1, result.size());
    assertEquals(new MiriamData(MiriamType.UNIPROT, "P37840"), result.get(0));
    assertEquals(0, hgncAnnotator.hgncToUniprot(new MiriamData(MiriamType.HGNC, "1")).size());
  }

  @Test
  public void testHgncIdToName() throws Exception {
    assertEquals(new MiriamData(MiriamType.HGNC_SYMBOL, "FSD2"),
        hgncAnnotator.hgncIdToHgncName(new MiriamData(MiriamType.HGNC, "18024")));
    assertEquals(new MiriamData(MiriamType.HGNC_SYMBOL, "LMOD1"),
        hgncAnnotator.hgncIdToHgncName(new MiriamData(MiriamType.HGNC, "6647")));
  }

  @Test
  public void testUnknownHgncIdToName() throws Exception {
    assertNull(hgncAnnotator.hgncIdToHgncName(new MiriamData(MiriamType.HGNC, "asd")));
  }

  @Test
  public void testHgncToUniProt1() throws Exception {
    List<MiriamData> list = hgncAnnotator.hgncToUniprot(new MiriamData(MiriamType.HGNC_SYMBOL, "CASP8"));
    assertEquals(1, list.size());
    assertEquals("Q14790", list.get(0).getResource());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testHgncToUniProtWithInvalidArg() throws Exception {
    hgncAnnotator.hgncToUniprot(new MiriamData(MiriamType.CAS, "CASP8"));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testHgncToEntrezWithInvalidArg() throws Exception {
    hgncAnnotator.hgncToEntrez(new MiriamData(MiriamType.CAS, "CASP8"));
  }

  @Test
  public void testIsValidHgnc() throws Exception {
    assertFalse(hgncAnnotator.isValidHgncMiriam(new MiriamData(MiriamType.CAS, "CASP8")));
  }

  @Test
  public void testIsValidHgnc2() throws Exception {
    assertTrue(hgncAnnotator.isValidHgncMiriam(new MiriamData(MiriamType.HGNC_SYMBOL, "CASP8")));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testHgncIdToNameWithInvalidArg() throws Exception {
    hgncAnnotator.hgncIdToHgncName(new MiriamData(MiriamType.CAS, "CASP8"));
  }

  @Test
  public void testHgncToUniProt2() throws Exception {
    MiriamData data1 = new MiriamData(MiriamType.HGNC_SYMBOL, "CASP8");
    MiriamData data2 = new MiriamData(MiriamType.HGNC_SYMBOL, "CASP10");

    List<MiriamData> list = hgncAnnotator.hgncToUniprot(data1);
    assertEquals(1, list.size());
    assertEquals("Q14790", list.get(0).getResource());

    list = hgncAnnotator.hgncToUniprot(data2);
    assertEquals(1, list.size());
    assertEquals("Q92851", list.get(0).getResource());
  }

  @Test
  public void testHgncToUniProt3() throws Exception {
    MiriamData data1 = new MiriamData(MiriamType.HGNC_SYMBOL, "blablabla invalid name");
    List<MiriamData> list = hgncAnnotator.hgncToUniprot(data1);
    assertNotNull(list);
    assertEquals(0, list.size());
  }

  @Test
  public void testHgncToEntrez() throws Exception {
    // check by symbol
    MiriamData data1 = new MiriamData(MiriamType.HGNC_SYMBOL, "PTGS1");
    MiriamData entrez = hgncAnnotator.hgncToEntrez(data1);
    assertNotNull(entrez);
    assertEquals(new MiriamData(MiriamType.ENTREZ, "5742"), entrez);

    // check by id
    data1 = new MiriamData(MiriamType.HGNC, "11138");
    entrez = hgncAnnotator.hgncToEntrez(data1);
    assertNotNull(entrez);
    assertEquals(new MiriamData(MiriamType.ENTREZ, "6622"), entrez);
  }

  @Test
  public void testInvalidHgncToEntrez() throws Exception {
    MiriamData data1 = new MiriamData(MiriamType.HGNC_SYMBOL, "xxxxxsd");
    MiriamData entrez = hgncAnnotator.hgncToEntrez(data1);
    assertNull(entrez);

    data1 = new MiriamData(MiriamType.HGNC, "xxxxxsd");
    entrez = hgncAnnotator.hgncToEntrez(data1);
    assertNull(entrez);
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(hgncAnnotator, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.DOWN, hgncAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenReturn("<response><result/></response>");
    ReflectionTestUtils.setField(hgncAnnotator, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.CHANGED, hgncAnnotator.getServiceStatus().getStatus());
  }

}
