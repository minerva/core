package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class TairAnnotatorTest extends AnnotationTestFunctions {

  @Autowired
  private TairAnnotator tairAnnotator;

  @Autowired
  private GeneralCacheInterface cache;

  private final WebPageDownloader webPageDownloader = new WebPageDownloader();

  @Before
  public void setUp() {
  }

  @After
  public void tearDown() throws Exception {
    ReflectionTestUtils.setField(tairAnnotator, "webPageDownloader", webPageDownloader);
    ReflectionTestUtils.setField(tairAnnotator, "cache", cache);
  }

  @Test
  public void testAnnotate1() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "2200950")); // AT1G01030

    tairAnnotator.annotateElement(protein);

    assertTrue(protein.getMiriamData().contains(new MiriamData(MiriamType.UNIPROT, "Q9MAN1", TairAnnotator.class)));
  }

  @Test
  public void testLocusIdToLocusName() throws Exception {
    String name = tairAnnotator.tairLocusIdToTairName("2200950");
    assertEquals("AT1G01030", name);
  }

  @Test
  public void testInvalidLocusIdToLocusName() throws Exception {
    String name = tairAnnotator.tairLocusIdToTairName("--2200950");
    assertNull(name);
  }

  @Test
  public void testLocusIdToUniprot() throws Exception {
    Collection<MiriamData> collection = tairAnnotator.tairLocusIdToUniprot("2200950");
    assertTrue(collection.contains(new MiriamData(MiriamType.UNIPROT, "Q9MAN1")));
  }

  @Test
  public void testInvalidLocusIdToUniprot() throws Exception {
    Collection<MiriamData> collection = tairAnnotator.tairLocusIdToUniprot("--2200950");
    assertEquals(0, collection.size());
  }

  @Test
  // @Ignore("TAIR DB restricts queries by IP")
  public void testAnnotateExistingUniprot() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "2200427"));
    // Human version of the protein
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "P32246"));

    tairAnnotator.annotateElement(protein);

    int countUniprot = 0;

    for (final MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.UNIPROT)) {
        countUniprot++;
      }
    }

    assertTrue("No UNIPROT annotation extracted from TAIR annotator", countUniprot > 1);
  }

  @Test
  public void testAnnotateInvalidTair() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "bla"));
    tairAnnotator.annotateElement(protein);

    assertEquals(1, protein.getMiriamData().size());

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testInvalidTairToUniprot() throws Exception {
    assertNull(tairAnnotator.tairToUniprot(null));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidTairToUniprot2() throws Exception {
    tairAnnotator.tairToUniprot(new MiriamData(MiriamType.WIKIPEDIA, "bla"));
  }

  @Test
  public void testAnnotateInvalid() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    tairAnnotator.annotateElement(protein);

    assertEquals(0, protein.getMiriamData().size());
  }

  @Test
  public void testTairToUniprot() throws Exception {
    // TAIR locus AT1G01030
    logger.debug(tairAnnotator.tairToUniprot(new MiriamData(MiriamType.TAIR_LOCUS, "2200950")));
    assertTrue(tairAnnotator.tairToUniprot(new MiriamData(MiriamType.TAIR_LOCUS, "2200950"))
        .contains(new MiriamData(MiriamType.UNIPROT, "Q9MAN1")));
  }

  @Test
  public void testTairToUniprotFromKEGG() throws Exception {
    // TAIR Loci coming from annotators should be ignored by TAIR (only TAIR
    // LOCI provided by the human annotator should be considered)
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "2200427", KeggAnnotator.class));
    tairAnnotator.annotateElement(protein);
    assertEquals(1, protein.getMiriamData().size());
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, tairAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testUniprotTairLocusToId() throws Exception {
    assertEquals("2201786",
        tairAnnotator.tairLocusNameToId("AT1G08510"));
  }

  @Test
  public void testUniprotWrongTairLocusToId() throws Exception {
    assertNull(
        tairAnnotator.tairLocusNameToId("blah"));
  }

}
