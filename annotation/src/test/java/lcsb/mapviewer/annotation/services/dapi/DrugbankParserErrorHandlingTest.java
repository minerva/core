package lcsb.mapviewer.annotation.services.dapi;

import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.services.DrugSearchException;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

@ActiveProfiles("dapiDownTestProfile")
public class DrugbankParserErrorHandlingTest extends AnnotationTestFunctions {

  @Autowired
  private DrugBankParser drugBankParser;

  @Before
  public void setUp() {
    assumeTrue("DAPI credentials are not provided", isDapiConfigurationAvailable());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.DOWN, drugBankParser.getServiceStatus().getStatus());
  }

  @Test(expected = DrugSearchException.class)
  public void testFindDrug() throws Exception {
    drugBankParser.findDrug("aspirin");
  }

  @Test(expected = DrugSearchException.class)
  public void testFindByTarget() throws Exception {
    drugBankParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "SIRT3"));
  }
}
