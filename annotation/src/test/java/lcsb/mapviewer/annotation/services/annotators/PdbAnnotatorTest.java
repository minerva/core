package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.Structure;
import lcsb.mapviewer.model.map.species.field.UniprotRecord;

public class PdbAnnotatorTest extends AnnotationTestFunctions {

  @Autowired
  private PdbAnnotator pdbAnnotator;

  @Autowired
  private UniprotAnnotator uniprotAnnotator;

  @Autowired
  private GeneralCacheInterface cache;

  private WebPageDownloader webPageDownloader = new WebPageDownloader();

  @Before
  public void setUp() {
  }

  @After
  public void tearDown() throws Exception {
    ReflectionTestUtils.setField(pdbAnnotator, "webPageDownloader", webPageDownloader);
    ReflectionTestUtils.setField(pdbAnnotator, "cache", cache);
  }

  @Test
  public void testAnnotate1() throws Exception {
    String uniprotId = "P29373";
    Species protein = new GenericProtein("id");
    protein.setName(uniprotId);
    /*
     * One needs to have the UniProt ID first. This tests simulates situation
     * when the Uniprot annotator is called first.
     */
    uniprotAnnotator.annotateElement(protein);
    int cntAnnotations1 = protein.getMiriamData().size();
    pdbAnnotator.annotateElement(protein);
    int cntAnnotations2 = protein.getMiriamData().size();

    assertTrue(cntAnnotations2 > cntAnnotations1);

    boolean pdb = false;

    for (final MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.PDB)) {
        pdb = true;
        break;
      }
    }
    assertTrue("No PDB annotation extracted from pdb annotator", pdb);

    Set<UniprotRecord> urs = protein.getUniprots();
    // Test whether there is a uniprot record
    assertTrue(urs.size() > 0);
    UniprotRecord ur = null;
    for (final UniprotRecord ur1 : urs) {
      if (ur1.getUniprotId() == uniprotId) {
        ur = ur1;
      }
    }
    // Test whether there is a uniprot record with the uniprot ID which was
    // stored
    assertNotNull(ur);
    // Test whether structures are there
    Set<Structure> ss = ur.getStructures();
    assertNotNull(ss);
    assertTrue(ss.size() > 0);
    // Test whether uniprot is accessible from structure
    assertTrue(ss.iterator().next().getUniprot() == ur);
  }

  @Test
  public void testAnnotate2() throws Exception {
    Species protein = new GenericProtein("id");
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "P29373"));
    pdbAnnotator.annotateElement(protein);
    int cntAnnotations = protein.getMiriamData().size();

    assertTrue(cntAnnotations > 0);

    boolean pdb = false;

    for (final MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.PDB)) {
        pdb = true;
        break;
      }
    }
    assertTrue("No PDB annotation extracted from pdb annotator", pdb);
  }

  @Test
  public void testAnnotateNotUniprotAnnodated() throws Exception {
    Species protein = new GenericProtein("id");
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "P29373"));
    protein.setName("P29373");
    pdbAnnotator.annotateElement(protein);

    assertTrue("UniProt annotation in PDB annotator failed", protein.getMiriamData().size() > 0);

    boolean pdb = false;

    for (final MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.PDB)) {
        pdb = true;
        break;
      }
    }
    assertTrue("No PDB annotation extracted from PDB annotator", pdb);
  }

  @Test
  public void testAnnotateInvalidUniprot() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    pdbAnnotator.annotateElement(protein);

    assertEquals(0, protein.getMiriamData().size());
  }

  @Test
  public void testAnnotateValidUniprotNonexistingPdb() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("Q88VP8");
    pdbAnnotator.annotateElement(protein);

    boolean pdb = false;
    for (final MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.PDB)) {
        pdb = true;
        break;
      }
    }
    assertTrue("PDB mapping found for structure for which no should be available", !pdb);
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenThrow(new RuntimeException());
    ReflectionTestUtils.setField(pdbAnnotator, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.DOWN, pdbAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenReturn("{\"P29373\": [{\"xxx\": 140}]}");
    ReflectionTestUtils.setField(pdbAnnotator, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.CHANGED, pdbAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateInvalidJson() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("Q88VP8");
    uniprotAnnotator.annotateElement(protein);
    int cntAnnotations1 = protein.getMiriamData().size();

    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), anyString()))
        .thenReturn("\"P29373\": [{\"xxx\": 140}]}");
    ReflectionTestUtils.setField(pdbAnnotator, "webPageDownloader", mockDownloader);

    pdbAnnotator.annotateElement(protein);
    int cntAnnotations2 = protein.getMiriamData().size();
    assertTrue(cntAnnotations1 == cntAnnotations2);
  }

}
