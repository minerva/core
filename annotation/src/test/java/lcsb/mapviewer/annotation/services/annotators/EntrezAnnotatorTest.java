package lcsb.mapviewer.annotation.services.annotators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.GeneralCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.cache.XmlSerializer;
import lcsb.mapviewer.annotation.data.EntrezData;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class EntrezAnnotatorTest extends AnnotationTestFunctions {

  @Autowired
  private EntrezAnnotator entrezAnnotator;

  @Autowired
  private GeneralCacheInterface cache;

  private WebPageDownloader webPageDownloader = new WebPageDownloader();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
    ReflectionTestUtils.setField(entrezAnnotator, "webPageDownloader", webPageDownloader);
    ReflectionTestUtils.setField(entrezAnnotator, "cache", cache);
  }

  @Test
  public void testGetAnnotationsForEntrezId() throws Exception {
    MiriamData nsmf = new MiriamData(MiriamType.ENTREZ, "6647");
    GenericProtein proteinAlias = new GenericProtein("id");
    proteinAlias.addMiriamData(nsmf);
    entrezAnnotator.annotateElement(proteinAlias);
    assertNotNull(proteinAlias.getSymbol());
    assertNotNull(proteinAlias.getFullName());
    assertNotNull(proteinAlias.getNotes());
    assertFalse(proteinAlias.getNotes().isEmpty());
    assertTrue(proteinAlias.getMiriamData().size() > 1);
    assertTrue(proteinAlias.getSynonyms().size() > 0);

    boolean ensemble = false;
    boolean hgncId = false;
    boolean entrez = false;
    for (final MiriamData md : proteinAlias.getMiriamData()) {
      if (MiriamType.ENSEMBL.equals(md.getDataType())) {
        ensemble = true;
      } else if (MiriamType.HGNC.equals(md.getDataType())) {
        hgncId = true;
      } else if (MiriamType.ENTREZ.equals(md.getDataType())) {
        entrez = true;
      }
    }

    assertTrue("Ensemble symbol cannot be found", ensemble);
    assertTrue("Hgnc id cannot be found", hgncId);
    assertTrue("Entrez cannot be found", entrez);
  }

  @Test(timeout = 15000)
  public void testGetAnnotationsForInvalid() throws Exception {
    MiriamData nsmf = new MiriamData(MiriamType.ENTREZ, "blabla");
    GenericProtein proteinAlias = new GenericProtein("id");
    proteinAlias.addMiriamData(nsmf);
    entrezAnnotator.annotateElement(proteinAlias);

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, entrezAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(entrezAnnotator, "webPageDownloader", mockDownloader);
    assertEquals(ExternalServiceStatusType.DOWN, entrezAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    Object entrezSerializer = ReflectionTestUtils.getField(entrezAnnotator, "entrezSerializer");
    try {
      @SuppressWarnings("unchecked")
      XmlSerializer<EntrezData> mockSerializer = Mockito.mock(XmlSerializer.class);
      when(mockSerializer.xmlToObject(any())).thenReturn(new EntrezData());
      ReflectionTestUtils.setField(entrezAnnotator, "entrezSerializer", mockSerializer);
      assertEquals(ExternalServiceStatusType.CHANGED, entrezAnnotator.getServiceStatus().getStatus());
    } finally {
      ReflectionTestUtils.setField(entrezAnnotator, "entrezSerializer", entrezSerializer);
    }
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery() throws Exception {
    entrezAnnotator.refreshCacheQuery("invalid_query");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRefreshInvalidCacheQuery2() throws Exception {
    entrezAnnotator.refreshCacheQuery(new Object());
  }

  @Test(timeout = 25000)
  public void testRefreshEntrezData() throws Exception {
    assertNotNull(entrezAnnotator.refreshCacheQuery(EntrezAnnotatorImpl.ENTREZ_DATA_PREFIX + "6647"));
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryNotAvailable() throws Exception {
    // exclude first cached value
    ReflectionTestUtils.setField(entrezAnnotator, "cache", null);

    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(entrezAnnotator, "webPageDownloader", mockDownloader);
    entrezAnnotator.refreshCacheQuery("http://google.pl/");
  }

  @Test(expected = SourceNotAvailable.class)
  public void testRefreshCacheQueryWithInvalidEntrezServerResponse() throws Exception {
    // exclude first cached value
    ReflectionTestUtils.setField(entrezAnnotator, "cache", null);

    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new IOException());
    ReflectionTestUtils.setField(entrezAnnotator, "webPageDownloader", mockDownloader);
    entrezAnnotator.refreshCacheQuery(EntrezAnnotatorImpl.ENTREZ_DATA_PREFIX + "6647");
  }

  @Test
  public void testAnnotateElementWithTwoEntrez() throws Exception {
    Species proteinAlias = new GenericProtein("id");
    proteinAlias.addMiriamData(new MiriamData(MiriamType.ENTREZ, "6647"));
    proteinAlias.addMiriamData(new MiriamData(MiriamType.ENTREZ, "6648"));
    entrezAnnotator.annotateElement(proteinAlias);

    assertEquals(3, getWarnings().size());
  }

  @Test
  public void testAnnotateElementWithEncodedSynonyms() throws Exception {
    Species proteinAlias = new GenericProtein("id");
    proteinAlias.addMiriamData(new MiriamData(MiriamType.ENTREZ, "834106"));
    entrezAnnotator.annotateElement(proteinAlias);

    for (final String synonym : proteinAlias.getSynonyms()) {
      assertFalse("Invalid character found in synonym: " + synonym, synonym.contains("&"));
    }
  }

  @Test
  public void testParseEntrezResponse() throws Exception {
    ReflectionTestUtils.setField(entrezAnnotator, "cache", null);
    String response = super.readFile("testFiles/annotation/entrezResponse.xml");
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn(response);
    ReflectionTestUtils.setField(entrezAnnotator, "webPageDownloader", mockDownloader);
    EntrezData data = entrezAnnotator.getEntrezForMiriamData(new MiriamData(), null);
    boolean ensembl = false;
    boolean hgnc = false;
    for (final MiriamData md : data.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.HGNC)) {
        hgnc = true;
      }
      if (md.getDataType().equals(MiriamType.ENSEMBL)) {
        ensembl = true;
      }
    }
    assertTrue(ensembl);
    assertTrue(hgnc);
    assertNotNull(data.getDescription());
    assertFalse(data.getDescription().isEmpty());
  }

  @Test(expected = AnnotatorException.class)
  public void testParseInvalidEntrezResponse() throws Exception {
    ReflectionTestUtils.setField(entrezAnnotator, "cache", null);
    String response = "";
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class))).thenReturn(response);
    ReflectionTestUtils.setField(entrezAnnotator, "webPageDownloader", mockDownloader);
    entrezAnnotator.getEntrezForMiriamData(new MiriamData(), null);
  }

  @Test(expected = AnnotatorException.class)
  public void testParseInvalidEntrezResponse2() throws Exception {
    ReflectionTestUtils.setField(entrezAnnotator, "cache", null);
    WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
    when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
        .thenThrow(new WrongResponseCodeIOException(null, 404));
    ReflectionTestUtils.setField(entrezAnnotator, "webPageDownloader", mockDownloader);
    entrezAnnotator.getEntrezForMiriamData(new MiriamData(), null);
  }

  @Test
  public void testGetEntrezForMiriamDataWithoutLogMarker() throws Exception {
    EntrezData data = entrezAnnotator.getEntrezForMiriamData(new MiriamData(), null);
    assertNull(data);
  }

}
