package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

public class CazyAnnotatorTest extends AnnotationTestFunctions {

  @Autowired
  private CazyAnnotator cazyAnnotator;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testUniprotToCazy() throws Exception {
    assertEquals(new MiriamData(MiriamType.CAZY, "GH5_7"),
        cazyAnnotator.uniprotToCazy(new MiriamData(MiriamType.UNIPROT, "Q00012"), null));
  }

  @Test
  public void testAnnotateFromUniprot() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q00012"));

    cazyAnnotator.annotateElement(protein);

    MiriamData mdCazy = null;

    for (final MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.CAZY)) {
        mdCazy = md;
      }
    }

    assertNotNull("No UNIPROT annotation extracted from TAIR annotator", mdCazy);
    assertTrue("Invalid UNIPROT annotation extracted from TAIR annotator",
        mdCazy.getResource().equalsIgnoreCase("GH5_7"));
  }

  @Test
  @Ignore("TAIR DB restricts queries by IP")
  public void testAnnotateFromTair() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "AT3G53010"));

    cazyAnnotator.annotateElement(protein);

    MiriamData mdCazy = null;

    for (final MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.CAZY)) {
        mdCazy = md;
      }
    }

    assertNotNull("No CAZy annotation extracted from CAZy annotator", mdCazy);
    assertTrue("Invalid CAZy annotation extracted from CAZy annotator", mdCazy.getResource().equalsIgnoreCase("CE6"));
  }

  @Test
  public void testAnnotateMultipleUniprots() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "Q00012"));
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "A8MCI6"));

    cazyAnnotator.annotateElement(protein);

    int cntMDs = 0;

    for (final MiriamData md : protein.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.CAZY)) {
        cntMDs++;
      }
    }

    assertEquals("Wrong number of CAZy identifiers extracted from CAZy annotator", 2, cntMDs);
  }

  @Test
  public void testAnnotateInvalidEmpty() throws Exception {
    Species protein = new GenericProtein("id");
    protein.setName("bla");
    cazyAnnotator.annotateElement(protein);

    assertEquals(0, protein.getMiriamData().size());
  }

  @Test
  public void testAnnotateInvalidUniprot() throws Exception {
    Species protein = new GenericProtein("id");
    protein.addMiriamData(new MiriamData(MiriamType.UNIPROT, "bla"));
    cazyAnnotator.annotateElement(protein);

    assertEquals(1, protein.getMiriamData().size());

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testAnnotateInvalidTair() throws Exception {
    Species protein = new GenericProtein("id");
    protein.addMiriamData(new MiriamData(MiriamType.TAIR_LOCUS, "bla"));
    cazyAnnotator.annotateElement(protein);

    assertEquals(1, protein.getMiriamData().size());
  }

  @Test
  public void testInvalidUniprotToCazyNull() throws Exception {
    assertNull(cazyAnnotator.uniprotToCazy(null, null));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidUniprotToCazyWrongMd() throws Exception {
    cazyAnnotator.uniprotToCazy(new MiriamData(MiriamType.WIKIPEDIA, "bla"), null);
  }

  @Test
  public void testStatus() throws Exception {
    assertEquals(ExternalServiceStatusType.OK, cazyAnnotator.getServiceStatus().getStatus());
  }

  @Test
  public void testSimulateDownStatus() throws Exception {
    Object downloader = ReflectionTestUtils.getField(cazyAnnotator, "webPageDownloader");
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenThrow(new IOException());
      ReflectionTestUtils.setField(cazyAnnotator, "webPageDownloader", mockDownloader);

      assertEquals(ExternalServiceStatusType.DOWN, cazyAnnotator.getServiceStatus().getStatus());
    } finally {
      ReflectionTestUtils.setField(cazyAnnotator, "webPageDownloader", downloader);
    }
  }

  @Test
  public void testSimulateChangedStatus() throws Exception {
    Object downloader = ReflectionTestUtils.getField(cazyAnnotator, "webPageDownloader");
    try {
      WebPageDownloader mockDownloader = Mockito.mock(WebPageDownloader.class);
      when(mockDownloader.getFromNetwork(anyString(), anyString(), nullable(String.class)))
          .thenReturn("GN   Name=ACSS2; Synonyms=ACAS2;");
      ReflectionTestUtils.setField(cazyAnnotator, "webPageDownloader", mockDownloader);
      assertEquals(ExternalServiceStatusType.CHANGED, cazyAnnotator.getServiceStatus().getStatus());
    } finally {
      ReflectionTestUtils.setField(cazyAnnotator, "webPageDownloader", downloader);
    }
  }

}
