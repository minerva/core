package lcsb.mapviewer.annotation.services.dapi;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assume.assumeTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;

import lcsb.mapviewer.annotation.AnnotationTestFunctions;
import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

@ActiveProfiles("dapiNotConnectedTestProfile")
@Rollback(true)
public class DrugbankParserWithNoConnectionTest extends AnnotationTestFunctions {

  @Autowired
  private DrugBankParser drugBankParser;

  @Before
  public void setUp() {
    assumeTrue("DAPI credentials are not provided", isDapiConfigurationAvailable());
  }

  @Test
  public void testFindUrokinase() throws Exception {
    Drug test = drugBankParser.findDrug("Urokinase");
    assertNull(test);
  }

  @Test
  public void testFindDrugByHgncTarget() throws Exception {
    List<Drug> drugs = drugBankParser.getDrugListByTarget(new MiriamData(MiriamType.HGNC_SYMBOL, "SIRT3"));
    assertEquals(0, drugs.size());
  }
}
