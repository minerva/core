package lcsb.mapviewer.annotation.services;

import lcsb.mapviewer.annotation.AnnotationNoTransactionTestFunctions;
import lcsb.mapviewer.annotation.services.annotators.IElementAnnotator;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.user.UserAnnotationSchema;
import lcsb.mapviewer.model.user.UserClassAnnotators;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.dao.ProjectDao;
import lcsb.mapviewer.persist.dao.map.ModelDao;
import org.apache.commons.lang3.mutable.MutableDouble;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class ModelAnnotatorTest extends AnnotationNoTransactionTestFunctions {
  private final IProgressUpdater updater = progress -> {
  };

  @Autowired
  private ModelAnnotator modelAnnotator;

  @Autowired
  private ModelDao modelDao;

  @Autowired
  private ProjectDao projectDao;

  @Autowired
  private DbUtils dbUtils;

  private List<Project> projects = new ArrayList<>();

  @Before
  public void setUp() throws Exception {
    projects = new ArrayList<>();
  }

  @After
  public void tearDown() throws Exception {
    for (Project project : projects) {
      dbUtils.callInTransaction(() -> {
        projectDao.delete(projectDao.getById(project.getId()));
        return null;
      });
    }
  }

  @Test
  public void testAnnotateModel() {
    ModelData modelData = createModelData();
    modelData.addReaction(createEmptyReaction());

    Species protein1 = createProtein();
    protein1.setName("SNCA");

    Species protein2 = createProtein();
    protein2.setName("PDK1");
    protein2.addMiriamData(new MiriamData(MiriamType.CAS, "c"));

    modelData.addElement(protein1);
    modelData.addElement(protein2);
    Project project = createProject();
    project.addModel(modelData);
    this.persistProject(project);

    modelAnnotator.performAnnotations(project, updater, modelAnnotator.createDefaultAnnotatorSchema());

    dbUtils.callInTransaction(() -> {
      Model model = new ModelFullIndexed(modelDao.getById(modelData.getId()));
      assertFalse(model.getElementByElementId(protein1.getElementId()).getMiriamData().isEmpty());
      assertFalse(model.getElementByElementId(protein2.getElementId()).getMiriamData().isEmpty());

      return null;
    });
    modelAnnotator.performAnnotations(project, updater, modelAnnotator.createDefaultAnnotatorSchema());
  }


  @Test
  public void testAnnotateModelWithGivenAnnotators() {
    ModelData modelData = createModelData();
    GenericProtein species = createProtein();
    species.setName("SNCA");
    modelData.addElement(species);

    Ion species2 = createIon();
    species2.setName("h2o");
    species2.addMiriamData(new MiriamData(MiriamType.CHEBI, "12345"));
    modelData.addElement(species2);
    modelData.addReaction(createEmptyReaction());

    UserAnnotationSchema annotationSchema = new UserAnnotationSchema();

    UserClassAnnotators proteinAnnotators = new UserClassAnnotators(GenericProtein.class);
    proteinAnnotators.setAnnotators(new ArrayList<>());
    annotationSchema.addClassAnnotator(proteinAnnotators);

    UserClassAnnotators ionAnnotators = new UserClassAnnotators(Ion.class);
    ionAnnotators.setAnnotators(modelAnnotator.getDefaultAnnotators(Ion.class));
    annotationSchema.addClassAnnotator(ionAnnotators);

    Project project = createProject();
    project.addModel(modelData);
    this.persistProject(project);

    modelAnnotator.performAnnotations(project, updater, annotationSchema);

    dbUtils.callInTransaction(() -> {
      Model model = new ModelFullIndexed(modelDao.getById(modelData.getId()));

      Element protein = model.getElementByElementId(species.getElementId());
      Element ion = model.getElementByElementId(species2.getElementId());
      // we didn't annotate protein
      assertEquals(0, protein.getMiriamData().size());
      // but we should annotate ion
      assertFalse(ion.getFullName().isEmpty());

      return null;
    });
  }

  @Test
  public void testDuplicateAnnotations() throws Exception {
    ModelData modelData = getModelForFile("testFiles/annotation/duplicate.xml", false).getModelData();

    Project project = createProject();
    project.addModel(modelData);
    this.persistProject(project);

    modelAnnotator.performAnnotations(project, updater, modelAnnotator.createDefaultAnnotatorSchema());

    dbUtils.callInTransaction(() -> {
      Model model = new ModelFullIndexed(modelDao.getById(project.getTopModel().getId()));
      Set<String> knowAnnotations = new HashSet<>();
      for (final MiriamData md : model.getElementByElementId("sa1").getMiriamData()) {
        knowAnnotations.add(md.getDataType() + ":" + md.getRelationType() + ":" + md.getResource());
      }
      int counter = 0;
      for (final String string : knowAnnotations) {
        if (string.contains("29108")) {
          counter++;
        }
      }
      assertEquals(1, counter);
      return null;
    });

    modelAnnotator.performAnnotations(project, updater, modelAnnotator.createDefaultAnnotatorSchema());

    dbUtils.callInTransaction(() -> {
      Model model = new ModelFullIndexed(modelDao.getById(project.getTopModel().getId()));
      Set<String> knowAnnotations = new HashSet<>();
      for (final MiriamData md : model.getElementByElementId("sa1").getMiriamData()) {
        knowAnnotations.add(md.getDataType() + ":" + md.getRelationType() + ":" + md.getResource());
      }
      int counter = 0;
      for (final String string : knowAnnotations) {
        if (string.contains("29108")) {
          counter++;
        }
      }
      assertEquals(1, counter);
      return null;
    });
  }

  @Test
  public void testGetAnnotationsForSYN() throws Exception {
    ModelData modelData = getModelForFile("testFiles/annotation/emptyAnnotationsSyn1.xml", true).getModelData();

    Project project = createProject();
    project.addModel(modelData);
    this.persistProject(project);
    modelAnnotator.performAnnotations(project, updater, modelAnnotator.createDefaultAnnotatorSchema());

    dbUtils.callInTransaction(() -> {
      Project p = projectDao.getProjectByProjectId(project.getProjectId());
      Model model = new ModelFullIndexed(modelDao.getById(p.getTopModel().getId()));

      Element sa1 = model.getElementByElementId("sa1");
      Element sa2 = model.getElementByElementId("sa2");

      assertFalse(sa2.getNotes().contains("Symbol"));
      assertNotNull(sa1.getSymbol());
      assertNotEquals("", sa1.getSymbol());
      // modelAnnotator.removeIncorrectAnnotations(model, updater);
      assertNotNull(sa1.getSymbol());
      assertNotEquals("", sa1.getSymbol());
      assertFalse(sa1.getNotes().contains("Symbol"));
      assertFalse(sa2.getNotes().contains("Symbol"));
      assertNull(sa2.getSymbol());

      for (final Species el : model.getSpeciesList()) {
        if (el.getNotes() != null) {
          assertFalse("Invalid notes: " + el.getNotes(), el.getNotes().contains("Symbol"));
          assertFalse("Invalid notes: " + el.getNotes(), el.getNotes().contains("HGNC"));
        }
      }
      return null;
    });

  }

  @Test
  public void testAnnotateModelWithDrugMolecule() throws Exception {
    ModelData modelData = super.getModelForFile("testFiles/annotation/problematic.xml", false).getModelData();

    Project project = createProject();
    project.addModel(modelData);
    this.persistProject(project);

    modelAnnotator.performAnnotations(project, updater, modelAnnotator.createDefaultAnnotatorSchema());

  }


  @Test
  public void testGetDefaultRequired() {
    Map<Class<? extends BioEntity>, Set<MiriamType>> map1 = modelAnnotator.getDefaultRequiredClasses();
    assertNotNull(map1);
  }

  @Test
  public void testGetDefaultValid() {
    Map<Class<? extends BioEntity>, Set<MiriamType>> map2 = modelAnnotator.getDefaultValidClasses();
    assertNotNull(map2);
  }

  @Test
  public void testPerformAnnotationAndCheckProgress() {
    dbUtils.callInTransaction(() -> {
      Project project = createProject();
      Model model = new ModelFullIndexed(null);
      Model submodel = new ModelFullIndexed(null);
      Model submodel2 = new ModelFullIndexed(null);

      GenericProtein protein = createProtein();

      model.addSubmodelConnection(new ModelSubmodelConnection(submodel, SubmodelType.UNKNOWN));
      model.addSubmodelConnection(new ModelSubmodelConnection(submodel2, SubmodelType.UNKNOWN));

      model.addElement(protein);
      submodel.addElement(protein);
      submodel2.addElement(protein);

      final MutableDouble maxProgress = new MutableDouble(0.0);

      project.addModel(model);
      project.addModel(submodel);
      project.addModel(submodel2);
      projectDao.add(project);


      modelAnnotator.performAnnotations(project,
          progress -> maxProgress.setValue(Math.max(progress, maxProgress.getValue())), new UserAnnotationSchema());
      assertTrue(maxProgress.getValue() <= IProgressUpdater.MAX_PROGRESS);

      projectDao.delete(project);

      return null;
    });
  }

  @Test
  public void testGetAvailableAnnotators() {
    List<IElementAnnotator> list = modelAnnotator.getAvailableAnnotators(Protein.class);
    assertFalse(list.isEmpty());
  }

  @Test
  public void testGetAvailableAnnotators2() {
    List<IElementAnnotator> list = modelAnnotator.getAvailableAnnotators();
    assertFalse(list.isEmpty());
  }

  @Test
  public void testGetAvailableDefaultAnnotators() {
    List<IElementAnnotator> list = modelAnnotator.getAvailableDefaultAnnotators(Protein.class);
    assertFalse(list.isEmpty());
  }

  protected Project persistProject(final Project project) {
    Project projectData = dbUtils.callInTransactionWithResult(() -> {
      try {
        return projectDao.add(project);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    });
    projects.add(projectData);
    return projectData;
  }

}
