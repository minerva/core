package lcsb.mapviewer.annotation;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@ContextConfiguration(classes = SpringAnnotationTestConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class AnnotationTestFunctions extends AnnotationNoTransactionTestFunctions {

}
