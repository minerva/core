package lcsb.mapviewer.annotation.services.annotators;

/**
 * Exception thrown when there is a problem with annotator.
 * 
 * @author Piotr Gawron
 * 
 */
public class AnnotatorException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   * 
   * @param exception
   *          original exception
   */
  public AnnotatorException(final Exception exception) {
    super(exception);
  }

  /**
   * Default constructor.
   * 
   * @param message
   *          message associated with exception
   * @param exception
   *          original exception
   */
  public AnnotatorException(final String message, final Exception exception) {
    super(message, exception);
  }

  /**
   * Default constructor.
   * 
   * @param message
   *          message associated with exception
   */
  public AnnotatorException(final String message) {
    super(message);
  }

}
