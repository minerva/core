package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.cache.QueryCacheInterface;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.model.user.annotator.AnnotatorInputParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;
import lcsb.mapviewer.model.user.annotator.BioEntityField;
import org.springframework.stereotype.Service;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class is responsible for connecting to
 * <a href="https://rest.ensembl.org">Ensembl API</a> and annotate elements with
 * information taken from this service.
 *
 * @author Piotr Gawron
 */
@Service
public class EnsemblAnnotatorImpl extends ElementAnnotator implements EnsemblAnnotator {

  /**
   * Version of the rest API that is supported by this annotator.
   */
  static final String SUPPORTED_VERSION = "15.9";

  /**
   * Url address of ensembl restful service.
   */
  private static final String REST_SERVICE_URL = "https://rest.ensembl.org/xrefs/id/";

  /**
   * Suffix that is needed for getting proper result using
   * {@link #REST_SERVICE_URL}.
   */
  private static final String URL_SUFFIX = "?content-type=text/xml";

  /**
   * Url used for retrieving version of the restful API.
   */
  private static final String REST_SERVICE_VERSION_URL = "https://rest.ensembl.org/info/rest?content-type=text/xml";

  /**
   * Default constructor.
   */
  public EnsemblAnnotatorImpl() {
    super(EnsemblAnnotator.class, new Class[]{Protein.class, Rna.class, Gene.class}, false);
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    final ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    final QueryCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {

      final GenericProtein proteinAlias = new GenericProtein("mock_id");
      proteinAlias.addMiriamData(getExampleValidAnnotation());
      annotateElement(proteinAlias);

      if (proteinAlias.getFullName() == null || proteinAlias.getFullName().isEmpty()) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else {
        status.setStatus(ExternalServiceStatusType.OK);
      }

      final String version = getRestfulApiVersion();
      if (!SUPPORTED_VERSION.equals(version)) {
        logger.debug("Version of Ensembl API changed... (new version: {})", version);
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }

    } catch (final Exception e) {
      logger.error("{} is down", getCommonName(), e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  /**
   * Returns current version of restful API.
   *
   * @return version of Ensembl restful API
   * @throws IOException               thrown when there is a problem with accessing API
   * @throws InvalidXmlSchemaException thrown when the result returned by API is invalid
   */
  private String getRestfulApiVersion() throws IOException, InvalidXmlSchemaException {
    final String content = getWebPageContent(REST_SERVICE_VERSION_URL);
    final Node xml = XmlParser.getXmlDocumentFromString(content);
    final Node response = XmlParser.getNode("opt", xml.getChildNodes());
    final Node data = XmlParser.getNode("data", response.getChildNodes());

    return XmlParser.getNodeAttr("release", data);
  }

  @Override
  public boolean annotateElement(final BioEntityProxy element, final MiriamData identifier,
                                 final AnnotatorData parameters)
      throws AnnotatorException {
    if (identifier.getDataType().equals(MiriamType.ENSEMBL)) {
      final String query = REST_SERVICE_URL + identifier.getResource() + URL_SUFFIX;
      try {
        final String content = getWebPageContent(query);
        final Node xml = XmlParser.getXmlDocumentFromString(content);
        final Node response = XmlParser.getNode("opt", xml.getChildNodes());

        final NodeList list = response.getChildNodes();
        final Set<String> synonyms = new HashSet<>();
        for (int i = 0; i < list.getLength(); i++) {
          final Node node = list.item(i);
          if (node.getNodeType() == Node.ELEMENT_NODE) {
            if (node.getNodeName().equals("data")) {
              final String error = XmlParser.getNodeAttr("error", node);
              if (error != null && !error.isEmpty()) {
                logger.warn(element.getLogMarker(ProjectLogEntryType.OTHER), error);
              }
              final String dbname = XmlParser.getNodeAttr("dbname", node);

              if ("EntrezGene".equals(dbname)) {
                final String entrezId = XmlParser.getNodeAttr("primary_id", node);
                if (entrezId != null && !entrezId.isEmpty()) {
                  element.addMiriamData(MiriamType.ENTREZ, entrezId);
                }
                final String symbol = XmlParser.getNodeAttr("display_id", node);
                if (symbol != null) {
                  element.setSymbol(symbol);
                }
                final String fullName = XmlParser.getNodeAttr("description", node);
                if (fullName != null) {
                  element.setFullName(fullName);
                }
                final NodeList synonymNodeList = node.getChildNodes();

                for (int j = 0; j < synonymNodeList.getLength(); j++) {
                  final Node synonymNode = synonymNodeList.item(j);
                  if (synonymNode.getNodeType() == Node.ELEMENT_NODE
                      && "synonyms".equalsIgnoreCase(synonymNode.getNodeName())) {
                    synonyms.add(synonymNode.getTextContent());
                  }
                }
              } else if ("HGNC".equals(dbname)) {
                String hgncId = XmlParser.getNodeAttr("primary_id", node);
                if (hgncId != null && !hgncId.isEmpty()) {
                  hgncId = hgncId.replaceAll("HGNC:", "");
                  element.addMiriamData(MiriamType.HGNC, hgncId);
                }
                final String hgncSymbol = XmlParser.getNodeAttr("display_id", node);
                if (hgncSymbol != null && !hgncSymbol.isEmpty()) {
                  element.addMiriamData(MiriamType.HGNC_SYMBOL, hgncId);
                }
                final NodeList synonymNodeList = node.getChildNodes();

                for (int j = 0; j < synonymNodeList.getLength(); j++) {
                  final Node synonymNode = synonymNodeList.item(j);
                  if (synonymNode.getNodeType() == Node.ELEMENT_NODE
                      && "synonyms".equalsIgnoreCase(synonymNode.getNodeName())) {
                    synonyms.add(synonymNode.getTextContent());
                  }
                }
              }
            }
          }
        }
        if (!synonyms.isEmpty()) {
          element.setSynonyms(synonyms);
        }
        return true;
      } catch (final WrongResponseCodeIOException e) {
        logger.warn(element.getLogMarker(ProjectLogEntryType.CANNOT_FIND_INFORMATION),
            "Cannot find information for ensembl: " + identifier.getResource());
        return false;
      } catch (final Exception e) {
        throw new AnnotatorException(e);
      }
    } else {
      throw new NotImplementedException();
    }
  }

  @Override
  public String getCommonName() {
    return MiriamType.ENSEMBL.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.ENSEMBL.getDbHomepage();
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Collections.singletonList(new AnnotatorInputParameter(MiriamType.ENSEMBL));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Arrays.asList(new AnnotatorOutputParameter(MiriamType.ENTREZ),
        new AnnotatorOutputParameter(MiriamType.HGNC),
        new AnnotatorOutputParameter(MiriamType.HGNC_SYMBOL),
        new AnnotatorOutputParameter(BioEntityField.FULL_NAME),
        new AnnotatorOutputParameter(BioEntityField.SYMBOL),
        new AnnotatorOutputParameter(BioEntityField.SYNONYMS));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.ENSEMBL, "ENSG00000157764");
  }

}
