package lcsb.mapviewer.annotation.services.dapi.dto;

import org.springframework.stereotype.Service;

import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.model.map.MiriamType;

@Service
public class ChemicalEntityDtoConverter {

  private ChemicalTargetDtoConverter chemicalTargetDtoConverter;

  public ChemicalEntityDtoConverter(final ChemicalTargetDtoConverter chemicalTargetDtoConverter) {
    this.chemicalTargetDtoConverter = chemicalTargetDtoConverter;
  }

  public Drug dtoToDrug(final ChemicalEntityDto dto) {
    Drug result = new Drug();
    result.addSource(MiriamType.getMiriamDataFromPrefixIdentifier(dto.getSourceIdentifier()));
    result.setName(dto.getName());
    if (dto.getDescription() != null && !dto.getDescription().isEmpty()) {
      result.setDescription(dto.getDescription());
    }
    result.addBrandNames(dto.getBrandNames());
    result.addSynonyms(dto.getSynonyms());
    result.setApproved(dto.getApproved());
    for (final ChemicalTargetDto target : dto.getTargets()) {
      result.addTarget(chemicalTargetDtoConverter.dtoToTarget(target));
    }
    return result;
  }

  public Chemical dtoToChemical(final ChemicalEntityDto dto) {
    Chemical result = new Chemical();
    result.setChemicalId(MiriamType.getMiriamDataFromPrefixIdentifier(dto.getSourceIdentifier()));
    result.setChemicalName(dto.getName());
    result.addSynonyms(dto.getSynonyms());
    for (final ChemicalTargetDto target : dto.getTargets()) {
      result.addTarget(chemicalTargetDtoConverter.dtoToTarget(target));
    }
    return result;
  }

}
