package lcsb.mapviewer.annotation.services.dapi;

public class DapiConnectionException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  private Integer statusCode;

  public DapiConnectionException(final String string, final int statusCode) {
    super(string);
    this.statusCode = statusCode;
  }

  public DapiConnectionException(final String string, final Exception e) {
    super(string, e);
  }

  public Integer getStatusCode() {
    return statusCode;
  }

}
