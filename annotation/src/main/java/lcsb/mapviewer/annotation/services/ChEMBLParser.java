package lcsb.mapviewer.annotation.services;

import java.util.List;
import java.util.Set;

import org.w3c.dom.Node;

import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.model.map.MiriamData;

/**
 * Class used for accessing and parsing data from chembl database using
 * <a href="https://www.ebi.ac.uk/chembl/ws">ChEMBL Data Web Services</a>.
 * Parser was initially developed by Janek, but after new API released by ebi it
 * was refactored by Piotr.
 * 
 * @author Piotr Gawron
 * 
 */
public interface ChEMBLParser extends IExternalService, DrugAnnotation {

  List<Drug> getDrugsByChemblTarget(final MiriamData targetMiriam) throws DrugSearchException;

  Drug getDrugById(final MiriamData drugId) throws DrugSearchException;

  Target getTargetFromId(final MiriamData md) throws DrugSearchException;

  List<Target> getTargetsByDrugId(final MiriamData drugId) throws DrugSearchException;

  List<Target> getTargetsForChildElements(final MiriamData drugId) throws DrugSearchException;

  MiriamData targetComponentToMiriamData(final Node targetComponent) throws DrugSearchException;

  List<String> parseSynonymsNode(Node node);

  Set<MiriamData> parseReferences(Node firstChild);
}
