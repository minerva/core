package lcsb.mapviewer.annotation.services;

/**
 * Exception thrown when there was a problem when searching for a drug.
 * 
 * @author Piotr Gawron
 * 
 */
public class DrugSearchException extends Exception {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   *
   * @param string
   *          exception message
   */
  public DrugSearchException(final String string) {
    super(string);
  }

  /**
   * Default constructor.
   *
   * @param e
   *          parent exception
   */
  public DrugSearchException(final Exception e) {
    super(e);
  }

  /**
   * Default constructor.
   *
   * @param message
   *          exception message
   * @param e
   *          source exception
   */
  public DrugSearchException(final String message, final Exception e) {
    super(message, e);
  }

}
