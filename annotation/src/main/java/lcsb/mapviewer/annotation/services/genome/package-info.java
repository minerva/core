/**
 * This package contains classes that access reference genome information.
 */
package lcsb.mapviewer.annotation.services.genome;
