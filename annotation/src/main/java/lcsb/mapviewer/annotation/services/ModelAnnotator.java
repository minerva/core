package lcsb.mapviewer.annotation.services;

import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.ElementAnnotator;
import lcsb.mapviewer.annotation.services.annotators.IElementAnnotator;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.user.UserAnnotationSchema;
import lcsb.mapviewer.model.user.UserClassAnnotators;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.modelutils.map.ClassTreeNode;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.dao.map.ReactionDao;
import lcsb.mapviewer.persist.dao.map.ReactionProperty;
import lcsb.mapviewer.persist.dao.map.species.ElementDao;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.persist.dao.user.UserAnnotationSchemaDao;
import org.apache.commons.collections4.ListUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ModelAnnotator {

  private static final Logger logger = LogManager.getLogger();

  /**
   * List of all available {@link ElementAnnotator} objects.
   */
  private final List<IElementAnnotator> availableAnnotators;

  private final UserAnnotationSchemaDao userAnnotationSchemaDao;
  private final ElementDao elementDao;
  private final ReactionDao reactionDao;
  private final DbUtils dbUtils;

  @Autowired
  public ModelAnnotator(final List<IElementAnnotator> availableAnnotators,
                        final UserAnnotationSchemaDao userAnnotationSchemaDao,
                        final ElementDao elementDao,
                        final ReactionDao reactionDao,
                        final DbUtils dbUtils) {
    this.availableAnnotators = availableAnnotators;
    this.userAnnotationSchemaDao = userAnnotationSchemaDao;
    this.dbUtils = dbUtils;
    this.elementDao = elementDao;
    this.reactionDao = reactionDao;
  }

  /**
   * Performs all possible and automatic annotations on the model.
   *
   * @param inputAnnotationSchema information about {@link ElementAnnotator} objects that should be
   *                              used for a given classes
   * @param project               project to update
   * @param progressUpdater       callback function used for updating progress of the function
   */
  public void performAnnotations(final Project project, final IProgressUpdater progressUpdater, final UserAnnotationSchema inputAnnotationSchema) {
    UserAnnotationSchema annotationSchema = inputAnnotationSchema;
    if (annotationSchema != null && annotationSchema.getId() > 0) {
      annotationSchema = userAnnotationSchemaDao.getById(annotationSchema.getId());
    }
    if (annotationSchema == null) {
      logger.error("Annotation Schema does not exist");
      annotationSchema = new UserAnnotationSchema();
    }

    ElementUtils elementUtils = new ElementUtils();
    progressUpdater.setProgress(0);

    double counter = 0;
    Map<ElementProperty, Object> elementFilter = new HashMap<>();
    elementFilter.put(ElementProperty.PROJECT_ID, Collections.singletonList(project.getProjectId()));

    List<Integer> elementIds = dbUtils.callInTransactionWithResult(() -> elementDao.getAll(elementFilter))
        .stream()
        .map(Element::getId)
        .collect(Collectors.toList());

    Map<ReactionProperty, Object> reactionFilter = new HashMap<>();
    reactionFilter.put(ReactionProperty.PROJECT_ID, Collections.singletonList(project.getProjectId()));

    List<Integer> reactionIds = dbUtils.callInTransactionWithResult(() -> reactionDao.getAll(reactionFilter))
        .stream()
        .map(Reaction::getId)
        .collect(Collectors.toList());

    double amount = elementIds.size() + reactionIds.size();

    // annotate all elements
    for (final Integer reactionId : reactionIds) {
      Reaction reaction = dbUtils.callInTransactionWithResult(() -> reactionDao.getById(reactionId));

      List<AnnotatorData> list = annotationSchema.getAnnotatorsForClass(reaction.getClass());
      if (list == null) {
        list = getDefaultAnnotators(reaction.getClass());
      }
      for (final AnnotatorData annotatorData : list) {
        IElementAnnotator elementAnnotator = null;
        try {
          elementAnnotator = getAnnotator(annotatorData);
          elementAnnotator.annotateElement(reaction, annotatorData);
          dbUtils.callInTransaction(() -> {
            reactionDao.update(reaction);
            return null;
          });
        } catch (final AnnotatorException e) {
          logger.warn("{} annotation problem.", elementUtils.getElementTag(reaction, elementAnnotator), e);
        }
      }
      counter++;
      progressUpdater.setProgress(IProgressUpdater.MAX_PROGRESS * counter / amount);
    }

    for (final Integer elementId : elementIds) {
      Element element = dbUtils.callInTransactionWithResult(() -> elementDao.getById(elementId));

      List<AnnotatorData> list = annotationSchema.getAnnotatorsForClass(element.getClass());
      if (list == null) {
        list = getDefaultAnnotators(element.getClass());
      }
      for (final AnnotatorData annotatorData : list) {
        IElementAnnotator elementAnnotator = null;
        try {
          elementAnnotator = getAnnotator(annotatorData);
          elementAnnotator.annotateElement(element, annotatorData);
          dbUtils.callInTransaction(() -> {
            elementDao.update(element);
            return null;
          });
        } catch (final AnnotatorException e) {
          logger.warn("{} annotation problem.", elementUtils.getElementTag(element, elementAnnotator), e);
        }
      }
      counter++;
      progressUpdater.setProgress(IProgressUpdater.MAX_PROGRESS * counter / amount);
    }

  }

  private IElementAnnotator getAnnotator(final AnnotatorData annotatorData) throws AnnotatorException {
    for (final IElementAnnotator proxiedElementAnnotator : availableAnnotators) {
      IElementAnnotator elementAnnotator = (IElementAnnotator) unwrapProxy(proxiedElementAnnotator);
      if (annotatorData.getAnnotatorClassName().isAssignableFrom(elementAnnotator.getClass())) {
        return proxiedElementAnnotator;
      }
    }
    throw new AnnotatorException("Annotator not supported: " + annotatorData.getAnnotatorClassName());
  }

  /**
   * Returns list of default {@link ElementAnnotator} names that are available
   * for class given in the parameter.
   *
   * @param clazz class for which list of default annotators will be returned
   * @return annotators names for a given class
   */
  public List<AnnotatorData> getDefaultAnnotators(final Class<?> clazz) {
    List<AnnotatorData> result = new ArrayList<>();
    for (final IElementAnnotator annotator : getAvailableDefaultAnnotators(clazz)) {
      result.add(annotator.createAnnotatorData());
    }
    return result;
  }

  /**
   * @return {@link #availableAnnotators}
   */
  public List<IElementAnnotator> getAvailableAnnotators() {
    return availableAnnotators;
  }

  /**
   * Returns list of available annotators for a given class type.
   *
   * @param clazz class type
   * @return list of available annotators for a given class type
   */
  public List<IElementAnnotator> getAvailableAnnotators(final Class<?> clazz) {
    List<IElementAnnotator> result = new ArrayList<>();
    for (final IElementAnnotator annotator : availableAnnotators) {
      if (annotator.isAnnotatable(clazz)) {
        result.add(annotator);
      }
    }
    return result;
  }

  /**
   * Returns list of default {@link ElementAnnotator annotators} that are
   * available for class given in the parameter.
   *
   * @param clazz class for which list of default annotators will be returned
   * @return annotators for a given class
   */
  List<IElementAnnotator> getAvailableDefaultAnnotators(final Class<?> clazz) {
    List<IElementAnnotator> result = new ArrayList<>();
    for (final IElementAnnotator annotator : availableAnnotators) {
      if (annotator.isAnnotatable(clazz) && annotator.isDefault()) {
        result.add(annotator);
      }
    }
    return result;
  }

  /**
   * Returns map with information about default valid {@link MiriamType miriam
   * types } for {@link BioEntity} class type.
   *
   * @return map with information about valid {@link MiriamType miriam types } for {@link BioEntity} class type
   */
  @SuppressWarnings("unchecked")
  public Map<Class<? extends BioEntity>, Set<MiriamType>> getDefaultValidClasses() {
    Map<Class<? extends BioEntity>, Set<MiriamType>> result = new HashMap<>();
    ElementUtils eu = new ElementUtils();
    ClassTreeNode tree = eu.getAnnotatedElementClassTree();

    Queue<ClassTreeNode> nodes = new LinkedList<>();
    nodes.add(tree);
    while (!nodes.isEmpty()) {
      ClassTreeNode node = nodes.poll();
      Set<MiriamType> set = new HashSet<>();
      Class<? extends BioEntity> clazz = (Class<? extends BioEntity>) node.getClazz();
      for (final MiriamType mt : MiriamType.values()) {
        for (final Class<?> clazz2 : mt.getValidClass()) {
          if (clazz2.isAssignableFrom(clazz)) {
            set.add(mt);
          }
        }
      }
      result.put(clazz, set);
      nodes.addAll(node.getChildren());
    }
    return result;
  }

  /**
   * Returns map with information about default required {@link MiriamType
   * miriam types } for {@link BioEntity} class type.
   *
   * @return map with information about required {@link MiriamType miriam types
   * } for {@link BioEntity} class type
   */
  @SuppressWarnings("unchecked")
  public Map<Class<? extends BioEntity>, Set<MiriamType>> getDefaultRequiredClasses() {
    Map<Class<? extends BioEntity>, Set<MiriamType>> result = new HashMap<>();
    ElementUtils eu = new ElementUtils();
    ClassTreeNode tree = eu.getAnnotatedElementClassTree();

    Queue<ClassTreeNode> nodes = new LinkedList<>();
    nodes.add(tree);
    while (!nodes.isEmpty()) {
      ClassTreeNode node = nodes.poll();
      Set<MiriamType> set = null;
      Class<? extends BioEntity> clazz = (Class<? extends BioEntity>) node.getClazz();
      if ((Boolean) (node.getData())) {
        set = new HashSet<>();
        for (final MiriamType mt : MiriamType.values()) {
          for (final Class<?> clazz2 : mt.getRequiredClass()) {
            if (clazz2.isAssignableFrom(clazz)) {
              set.add(mt);
            }
          }
        }
      }
      result.put(clazz, set);
      nodes.addAll(node.getChildren());
    }
    return result;
  }

  public UserAnnotationSchema createDefaultAnnotatorSchema() {
    UserAnnotationSchema result = new UserAnnotationSchema();
    ElementUtils elementUtils = new ElementUtils();

    for (final Class<?> clazz : ListUtils.union(elementUtils.getAvailableElementSubclasses(),
        elementUtils.getAvailableReactionSubclasses())) {
      UserClassAnnotators uca = new UserClassAnnotators(clazz);
      uca.setAnnotators(getDefaultAnnotators(clazz));
      result.addClassAnnotator(uca);
    }
    return result;
  }

  public static Object unwrapProxy(final Object bean) {

    /*
     * If the given object is a proxy, set the return value as the object being
     * proxied, otherwise return the given object.
     */
    if (AopUtils.isAopProxy(bean) && bean instanceof Advised) {

      Advised advised = (Advised) bean;

      try {
        return advised.getTargetSource().getTarget();
      } catch (Exception e) {
        logger.error("Problem with unwrapping spring proxy", e);
        return bean;
      }
    } else {
      return bean;
    }
  }

}
