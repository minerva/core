package lcsb.mapviewer.annotation.services.dapi.dto;

import java.util.ArrayList;
import java.util.List;

public class ChemicalEntityDto {

  private String sourceIdentifier;
  private String name;
  private String description;
  private Boolean approved;

  private List<ChemicalTargetDto> targets = new ArrayList<>();
  private List<String> brandNames = new ArrayList<>();
  private List<String> synonyms = new ArrayList<>();

  public String getSourceIdentifier() {
    return sourceIdentifier;
  }

  public void setSourceIdentifier(final String sourceIdentifier) {
    this.sourceIdentifier = sourceIdentifier;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public List<ChemicalTargetDto> getTargets() {
    return targets;
  }

  public void setTargets(final List<ChemicalTargetDto> targets) {
    this.targets = targets;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public List<String> getBrandNames() {
    return brandNames;
  }

  public void setBrandNames(final List<String> brandNames) {
    this.brandNames = brandNames;
  }

  public List<String> getSynonyms() {
    return synonyms;
  }

  public void setSynonyms(final List<String> synonyms) {
    this.synonyms = synonyms;
  }

  public Boolean getApproved() {
    return approved;
  }

  public void setApproved(final Boolean approved) {
    this.approved = approved;
  }

  @Override
  public String toString() {
    return "ChemicalEntityDto [sourceIdentifier=" + sourceIdentifier + "]";
  }
}
