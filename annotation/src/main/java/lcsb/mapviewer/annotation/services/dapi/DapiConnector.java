package lcsb.mapviewer.annotation.services.dapi;

import java.util.List;

import lcsb.mapviewer.annotation.services.dapi.dto.DapiDatabase;
import lcsb.mapviewer.annotation.services.dapi.dto.ReleaseDto;
import lcsb.mapviewer.annotation.services.dapi.dto.UserDto;

public interface DapiConnector {
  void login() throws DapiConnectionException;

  String getLatestReleaseAvailable(final String databaseName) throws DapiConnectionException;

  String getLatestReleaseUrl(final String databaseName) throws DapiConnectionException;

  String getAuthenticatedContent(final String string) throws DapiConnectionException;

  boolean isValidConnection();

  boolean isAuthenticated();

  void resetConnection();

  List<DapiDatabase> getDatabases() throws DapiConnectionException;

  List<ReleaseDto> getReleases(final String database) throws DapiConnectionException;

  boolean isReleaseAccepted(final String database, final ReleaseDto release) throws DapiConnectionException;

  void acceptRelease(final String database, final String release) throws DapiConnectionException;

  void registerUser(final UserDto user) throws DapiConnectionException;
}
