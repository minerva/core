package lcsb.mapviewer.annotation.services.annotators;

/**
 * This is a class that implements a backend to Brenda enzyme database.
 * 
 * @author David Hoksza
 * 
 */
public interface BrendaAnnotator extends IElementAnnotator {
}
