package lcsb.mapviewer.annotation.services.dapi;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import lcsb.mapviewer.annotation.cache.CachableInterface;
import lcsb.mapviewer.annotation.cache.WebPageDownloader;
import lcsb.mapviewer.annotation.services.dapi.dto.AcceptedReleaseDto;
import lcsb.mapviewer.annotation.services.dapi.dto.DapiDatabase;
import lcsb.mapviewer.annotation.services.dapi.dto.ListDapiDatabase;
import lcsb.mapviewer.annotation.services.dapi.dto.ListReleaseDto;
import lcsb.mapviewer.annotation.services.dapi.dto.ReleaseDto;
import lcsb.mapviewer.annotation.services.dapi.dto.UserDto;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.persist.dao.ConfigurationDao;

@Service
@Transactional
public class DapiConnectorImpl extends CachableInterface implements DapiConnector {

  private static Logger logger = LogManager.getLogger();

  private static final String DAPI_DOMAIN = "dapi.lcsb.uni.lu";
  private static final String DAPI_BASE_URL = "https://" + DAPI_DOMAIN + "/api/";

  private ConfigurationDao configurationDao;

  private ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
      false);

  private Calendar lastAuthentication;

  private WebPageDownloader webPageDownloader = new WebPageDownloader();

  static {
    CookieManager cookieManager = new CookieManager();
    CookieHandler.setDefault(cookieManager);
  }

  public DapiConnectorImpl(final ConfigurationDao configurationDao) {
    super(DapiConnector.class);
    this.configurationDao = configurationDao;
  }

  @Override
  public String getLatestReleaseAvailable(final String databaseName) throws DapiConnectionException {
    try {
      String content = getAuthenticatedContent(DAPI_BASE_URL + "database/" + databaseName + "/releases/?size=1000",
          false);
      Set<String> releases = new HashSet<>();
      ListReleaseDto result = objectMapper.readValue(content, ListReleaseDto.class);
      for (final ReleaseDto release : result.getContent()) {
        releases.add(release.getName());
      }

      String userDataContent = getAuthenticatedContent(DAPI_BASE_URL + "users/" + getDapiLogin(), false);
      UserDto userInfo = objectMapper.readValue(userDataContent, UserDto.class);

      Set<String> acceptedReleases = new HashSet<>();
      for (AcceptedReleaseDto releaseDto : userInfo.getAcceptedReleaseLicenses()) {
        if (releases.contains(releaseDto.getRelease().getName())) {
          acceptedReleases.add(releaseDto.getRelease().getName());
        }
      }
      return getLatestVersion(acceptedReleases);
    } catch (final IOException e) {
      throw new DapiConnectionException("Problem with accessing dapi data", e);
    }
  }

  public String getDapiLogin() {
    return configurationDao.getValueByType(ConfigurationElementType.DAPI_LOGIN);
  }

  String getLatestVersion(final Collection<String> acceptedReleases) {
    List<DefaultArtifactVersion> versions = new ArrayList<>();
    for (final String string : acceptedReleases) {
      versions.add(new DefaultArtifactVersion(string));
    }
    Collections.sort(versions);
    if (versions.size() == 0) {
      return null;
    }
    return versions.get(versions.size() - 1).toString();
  }

  private String getAuthenticatedContent(final String string, final boolean fromCache) throws DapiConnectionException {
    if (!isAuthenticated()) {
      login();
    }
    try {
      if (!fromCache) {
        super.removeCacheValue(string);
      }
      if (getCacheValue(string) == null) {
        lastAuthentication = Calendar.getInstance();
      }
      return getWebPageContent(string);
    } catch (final IOException e) {
      throw new DapiConnectionException("Problem with accessing dapi data", e);
    }
  }

  @Override
  public String getAuthenticatedContent(final String string) throws DapiConnectionException {
    return getAuthenticatedContent(string, true);
  }

  @Override
  public String getLatestReleaseUrl(final String databaseName) throws DapiConnectionException {
    String release = getLatestReleaseAvailable(databaseName);
    if (release == null) {
      return null;
    }
    return DAPI_BASE_URL + "database/" + databaseName + "/releases/" + getLatestReleaseAvailable(databaseName) + "/";
  }

  @Override
  public boolean isAuthenticated() {
    Calendar nowMinus10Minutes = Calendar.getInstance();
    nowMinus10Minutes.add(Calendar.MINUTE, -10);
    if (lastAuthentication != null && lastAuthentication.after(nowMinus10Minutes)) {
      return true;
    } else {
      try {
        webPageDownloader.getFromNetwork(DAPI_BASE_URL + "users/isAuthenticated");
        return true;
      } catch (final IOException e) {
        return false;
      }
    }
  }

  @Override
  public void login() throws DapiConnectionException {
    try {
      login(getDapiLogin(), getDapiPassword());
      lastAuthentication = Calendar.getInstance();
    } catch (final IOException e) {
      CookieManager cookieManager = (CookieManager) CookieHandler.getDefault();
      cookieManager.getCookieStore().removeAll();
      throw new DapiConnectionException("Problem with connection to DAPI server", e);
    }
  }

  void login(final String login, final String password)
      throws MalformedURLException, IOException, ProtocolException, DapiConnectionException {
    if (login == null || password == null) {
      throw new DapiConnectionException("Dapi credentials are not provided.", -1);
    }
    String data = "login=" + URLEncoder.encode(login, "UTF-8") + "&password=" + URLEncoder.encode(password, "UTF-8");

    URL url = new URL(DAPI_BASE_URL + "doLogin");
    HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();

    urlConn.addRequestProperty("User-Agent", "minerva-framework");
    urlConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

    urlConn.setRequestMethod("POST");
    urlConn.setDoOutput(true);
    DataOutputStream wr = new DataOutputStream(urlConn.getOutputStream());
    wr.writeBytes(data);
    wr.close();

    urlConn.connect();

    int code = urlConn.getResponseCode();

    if (code != HttpURLConnection.HTTP_OK) {
      throw new DapiConnectionException("Problem with connection to dapi.", code);
    }
  }

  public String getDapiPassword() {
    return configurationDao.getValueByType(ConfigurationElementType.DAPI_PASSWORD);
  }

  @Override
  public boolean isValidConnection() {
    if (getDapiLogin() == null || getDapiLogin().isEmpty() || getDapiPassword() == null
        || getDapiPassword().isEmpty()) {
      return false;
    }
    if (isAuthenticated()) {
      return true;
    }
    try {
      login();
    } catch (final DapiConnectionException e) {
      logger.warn("Invalid credentials");
      return false;
    }
    return isAuthenticated();
  }

  @Override
  public void resetConnection() {
    CookieManager cookieManager = (CookieManager) CookieHandler.getDefault();
    List<HttpCookie> cookies = new ArrayList<>();
    for (final HttpCookie cookie : cookieManager.getCookieStore().getCookies()) {
      if (Objects.equals(cookie.getDomain(), DAPI_DOMAIN)) {
        cookies.add(cookie);
      }
    }
    for (final HttpCookie cookie : cookies) {
      cookieManager.getCookieStore().remove(null, cookie);
    }
    lastAuthentication = null;

  }

  @Override
  public List<DapiDatabase> getDatabases() throws DapiConnectionException {
    try {
      String url = DAPI_BASE_URL + "database/";
      String content = webPageDownloader.getFromNetwork(url);
      ListDapiDatabase result = objectMapper.readValue(content, ListDapiDatabase.class);
      return result.getContent();
    } catch (final Exception e) {
      throw new DapiConnectionException("Problem with accessing dapi", e);
    }
  }

  @Override
  public List<ReleaseDto> getReleases(final String database) throws DapiConnectionException {
    try {
      String url = DAPI_BASE_URL + "database/" + database + "/releases/";
      String content = webPageDownloader.getFromNetwork(url);
      ListReleaseDto result = objectMapper.readValue(content, ListReleaseDto.class);
      return result.getContent();
    } catch (final IOException e) {
      throw new DapiConnectionException("Problem with accessing dapi", e);
    }
  }

  @Override
  public boolean isReleaseAccepted(final String database, final ReleaseDto release) throws DapiConnectionException {
    try {
      String userDataContent = getAuthenticatedContent(DAPI_BASE_URL + "users/" + getDapiLogin(), false);
      UserDto userData = objectMapper.readValue(userDataContent, UserDto.class);
      for (AcceptedReleaseDto acceptedReleaseDto : userData.getAcceptedReleaseLicenses()) {
        if (acceptedReleaseDto.getRelease().getName().equals(release.getName())) {
          return true;
        }
      }
      return false;
    } catch (IOException e) {
      throw new DapiConnectionException("Problem with parsiong DAPI response", e);
    }
  }

  @Override
  public void acceptRelease(final String database, final String release) throws DapiConnectionException {
    if (!isAuthenticated()) {
      login();
    }

    try {
      webPageDownloader.getFromNetwork(DAPI_BASE_URL + "database/" + database + "/releases/" + release + ":acceptLicense", "POST", "");
    } catch (final IOException e) {
      throw new DapiConnectionException("Problem with accessing dapi", e);
    }
  }

  @Override
  public void registerUser(final UserDto user) throws DapiConnectionException {

    try {
      URL url = new URL(DAPI_BASE_URL + "users/" + user.getLogin());
      HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();

      urlConn.addRequestProperty("User-Agent", "minerva-framework");
      urlConn.setRequestProperty("Content-Type", "application/json; utf-8");

      urlConn.setRequestMethod("POST");
      urlConn.setDoOutput(true);
      DataOutputStream wr = new DataOutputStream(urlConn.getOutputStream());
      wr.writeBytes(objectMapper.writeValueAsString(user));
      wr.close();

      urlConn.connect();

      int code = urlConn.getResponseCode();

      if (code != HttpURLConnection.HTTP_OK) {
        throw new DapiConnectionException(
            "Problem with connection to dapi. ", code);
      }
    } catch (final IOException e) {
      throw new DapiConnectionException("Problem with connection to dapi.", e);
    }
  }

}
