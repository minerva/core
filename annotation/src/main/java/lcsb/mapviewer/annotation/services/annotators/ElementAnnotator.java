package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.cache.CachableInterface;
import lcsb.mapviewer.annotation.cache.Cacheable;
import lcsb.mapviewer.common.comparator.SetComparator;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Chemical;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.UniprotRecord;
import lcsb.mapviewer.model.user.AnnotatorParamDefinition;
import lcsb.mapviewer.model.user.annotator.AnnotatorConfigParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.model.user.annotator.AnnotatorInputParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;
import lcsb.mapviewer.model.user.annotator.BioEntityField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Interface that allows to annotate {@link BioEntity elements} in the system.
 * Different implementation use different resources to perform annotation. They
 * can annotate different types of elements.
 *
 * @author Piotr Gawron
 */
@Transactional
public abstract class ElementAnnotator extends CachableInterface implements IElementAnnotator {

  /**
   * Default class logger.
   */
  protected static Logger logger = LogManager.getLogger();

  /**
   * List of classes that can be annotated by this {@link IElementAnnotator
   * annotator}.
   */
  private final List<Class<? extends BioEntity>> validClasses = new ArrayList<>();
  /**
   * Parameters which this annotator can be provided. Should be set in
   * constructor.
   */
  protected List<AnnotatorParamDefinition> paramsDefs = new ArrayList<>();
  /**
   * Should be this annotator used as a default annotator.
   */
  private boolean isDefault = false;

  /**
   * Default constructor.
   *
   * @param validClasses list of classes for which this annotator is valid
   * @param isDefault    {@link #isDefault}
   * @param clazz        type that defines this interface
   */
  @SuppressWarnings("unchecked")
  public ElementAnnotator(final Class<? extends Cacheable> clazz, final Class<?>[] validClasses, final boolean isDefault) {
    super(clazz);
    for (final Class<?> validClass : validClasses) {
      if (BioEntity.class.isAssignableFrom(validClass)) {
        addValidClass((Class<? extends BioEntity>) validClass);
      } else {
        throw new InvalidArgumentException("Cannot pass class of type: " + validClass + ". Only classes extending "
            + BioEntity.class + " are accepted.");
      }
    }
    this.isDefault = isDefault;
  }

  /**
   * Annotate element.
   *
   * @param element object to be annotated
   * @throws AnnotatorException thrown when there is a problem with annotating not related to data
   */
  @Override
  public void annotateElement(final BioEntity element) throws AnnotatorException {
    annotateElement(element, createAnnotatorData());
  }

  /**
   * Annotate element using parameters.
   *
   * @param bioEntity  object to be annotated
   * @param parameters list of parameters passed to the annotator which is expected to be
   *                   in the same order as its {@link this#paramsDefs}
   * @throws AnnotatorException thrown when there is a problem with annotating not related to data
   */
  @Override
  public final void annotateElement(final BioEntity bioEntity, final AnnotatorData parameters) throws AnnotatorException {
    if (isAnnotatable(bioEntity)) {
      final BioEntityProxy proxy = new BioEntityProxy(bioEntity, parameters);
      List<AnnotatorInputParameter> inputParameters = parameters.getInputParameters();
      if (inputParameters.isEmpty()) {
        inputParameters = getAvailableInputParameters();
      }
      if (parameters.getOutputParameters().isEmpty()) {
        parameters.addAnnotatorParameters(this.getAvailableOuputProperties());
      }
      final List<Set<Object>> inputs = getInputsParameters(bioEntity, inputParameters);
      for (final Set<Object> inputSet : inputs) {
        boolean annotated = false;
        for (final Object object : inputSet) {
          if (object instanceof MiriamData) {
            if (annotateElement(proxy, (MiriamData) object, parameters)) {
              annotated = true;
            }
          } else if (object instanceof String) {
            if (annotateElement(proxy, (String) object, parameters)) {
              annotated = true;
            }
          } else {
            throw new NotImplementedException();
          }
        }
        if (annotated) {
          break;
        }
      }
    }
  }

  @Override
  public abstract boolean annotateElement(final BioEntityProxy element, final MiriamData identifier,
                                          final AnnotatorData parameters)
      throws AnnotatorException;

  @Override
  public boolean annotateElement(final BioEntityProxy element, final String name, final AnnotatorData parameters)
      throws AnnotatorException {
    throw new NotImplementedException();
  }

  /**
   * Returns a list of all classes that can be annotated using this annotator.
   *
   * @return a list of all classes that can be annotated using this annotator
   */
  @Override
  public List<Class<? extends BioEntity>> getValidClasses() {
    return validClasses;
  }

  /**
   * Returns <code>true</code> if this annotator can annotate the object given in
   * the parameter.
   *
   * @param object object to be tested if can be annotated
   * @return <code>true</code> if object can be annotated by this annotator, <code>false</code> otherwise
   */
  @Override
  public boolean isAnnotatable(final BioEntity object) {
    final Class<?> clazz = object.getClass();
    for (final Class<?> validClazz : getValidClasses()) {
      if (validClazz.isAssignableFrom(clazz)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Returns <code>true</code> if this annotator can annotate the object of given
   * class type.
   *
   * @param clazz class to be tested if can be annotated
   * @return <code>true</code> if class can be annotated by this annotator, <code>false</code> otherwise
   */
  @Override
  public boolean isAnnotatable(final Class<?> clazz) {
    for (final Class<?> clazz2 : validClasses) {
      if (clazz2.isAssignableFrom(clazz)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Adds a class to list of classes that can be annotated by the annotator.
   *
   * @param clazz class to add
   */
  private void addValidClass(final Class<? extends BioEntity> clazz) {
    validClasses.add(clazz);
  }

  /**
   * Returns the common name that should be presented to user.
   *
   * @return the common name
   */
  @Override
  public abstract String getCommonName();

  /**
   * Returns url to the external resource used for annotation.
   *
   * @return url
   */
  @Override
  public abstract String getUrl();

  /**
   * Provides description of the extraction process for {@link ElementAnnotator}
   * to be used in the front end.
   *
   * @return the description
   */
  @Override
  public String getDescription() {
    return "";
  }

  /**
   * Returns list with definitions of the parameters available for this annotator.
   *
   * @return the parameters {@link AnnotatorParamDefinition} list
   */
  @Override
  public Collection<AnnotatorParamDefinition> getParametersDefinitions() {
    return paramsDefs;
  }

  /**
   * Sets definitions of parameters for given annotator.
   *
   * @param paramDefs definitions to be set
   */
  @Override
  public void setParametersDefinitions(final List<AnnotatorParamDefinition> paramDefs) {
    this.paramsDefs = paramDefs;
  }

  /**
   * @return {@link #isDefault}
   */
  @Override
  public boolean isDefault() {
    return isDefault;
  }

  /**
   * Adds parameter definition to the definitions of parameters for given
   * annotator
   *
   * @param paramDef parameter definition to be added
   */
  @Override
  public void addParameterDefinition(final AnnotatorParamDefinition paramDef) {
    this.paramsDefs.add(paramDef);
  }

  /**
   * Returns list of available {@link AnnotatorInputParameter}. Order indicates
   * the default order that should be considered when extracting identifier.
   *
   * @return list of available {@link AnnotatorInputParameter} for class
   */
  @Override
  public abstract List<AnnotatorInputParameter> getAvailableInputParameters();

  /**
   * Returns list of available {@link AnnotatorOutputParameter}. Order indicates
   * the default order that should be considered when extracting identifier.
   *
   * @return list of available {@link AnnotatorOutputParameter} for class
   */
  @Override
  public abstract List<AnnotatorOutputParameter> getAvailableOuputProperties();

  @Override
  public List<Set<Object>> getInputsParameters(final BioEntity bioEntity, final List<AnnotatorInputParameter> inputParameters) {
    final List<Set<Object>> result = new ArrayList<>();
    for (final AnnotatorInputParameter parameter : inputParameters) {
      final Set<Object> inputs = new HashSet<>();
      if (parameter.getField() != null && parameter.getIdentifierType() == null) {
        inputs.add(BioEntityField.getFieldValueForBioEntity(bioEntity, parameter.getField()));
      } else if (parameter.getField() != null && parameter.getIdentifierType() != null) {
        final String value = BioEntityField.getFieldValueForBioEntity(bioEntity, parameter.getField());
        if (value != null && !value.isEmpty()) {
          inputs.add(new MiriamData(parameter.getIdentifierType(),
              BioEntityField.getFieldValueForBioEntity(bioEntity, parameter.getField())));
        }
      } else if (parameter.getField() == null && parameter.getIdentifierType() != null) {
        for (final MiriamData md : bioEntity.getMiriamData()) {
          if (md.getDataType().equals(parameter.getIdentifierType())) {
            inputs.add(md);
          }
        }
      } else {
        throw new InvalidArgumentException("Input parameter must have either field or identifierType defined");
      }
      result.add(inputs);
    }

    return result;
  }

  @Override
  public abstract MiriamData getExampleValidAnnotation();

  @Override
  public List<AnnotatorConfigParameter> getExampleValidParameters() {
    return new ArrayList<>();
  }

  /**
   * Creates default {@link AnnotatorData} for this {@link ElementAnnotator}.
   */
  @Override
  public AnnotatorData createAnnotatorData() {
    final Class<?> clazz = getElementAnnotatorInterface();
    final AnnotatorData result = new AnnotatorData(clazz);
    // by default use everything as input
    result.addAnnotatorParameters(getAvailableInputParameters());
    // and provide all available output
    result.addAnnotatorParameters(getAvailableOuputProperties());
    // and provide all available output
    for (final AnnotatorParamDefinition type : getParametersDefinitions()) {
      result.addAnnotatorParameter(type, "");
    }
    return result;
  }

  @Override
  public Class<?> getElementAnnotatorInterface() {
    Class<?> clazz = null;
    for (final Class<?> interfaceClazz : this.getClass().getInterfaces()) {
      if (interfaceClazz != IElementAnnotator.class && IElementAnnotator.class.isAssignableFrom(interfaceClazz)) {
        clazz = interfaceClazz;
      }
    }
    return clazz;
  }

  class BioEntityProxy {
    private final BioEntity originalBioEntity;
    private final AnnotatorData parameters;

    public BioEntityProxy(final BioEntity bioEntity, final AnnotatorData parameters) {
      originalBioEntity = bioEntity;
      this.parameters = parameters;

    }

    public void addMiriamData(final Collection<MiriamData> annotations) {
      for (final MiriamData miriamData : annotations) {
        addMiriamData(miriamData);
      }
    }

    public void addMiriamData(final MiriamData miriamData) {
      if (miriamData.getResource() != null) {
        if (!contains(miriamData)) {
          miriamData.setAnnotator(ElementAnnotator.this.getElementAnnotatorInterface());
          originalBioEntity.addMiriamData(miriamData);
        }
      }
    }

    public void addMiriamData(final MiriamType miriamType, final String resource) {
      addMiriamData(new MiriamData(miriamType, resource));
    }

    public void addMiriamData(final String generalIdentifier) {
      MiriamData md = null;
      try {
        md = MiriamType.getMiriamByUri(generalIdentifier);
      } catch (final InvalidArgumentException e) {
        try {
          md = MiriamType.getMiriamByUri("urn:miriam:" + generalIdentifier);
        } catch (final InvalidArgumentException e1) {
          logger.warn(getLogMarker(ProjectLogEntryType.INVALID_IDENTIFIER), "Unknown miriam uri: " + generalIdentifier);
        }
      }
      if (md != null) {
        addMiriamData(md);
      }
    }

    public boolean contains(final MiriamData identifier) {
      final MiriamData copy = new MiriamData(identifier);
      copy.setAnnotator(null);
      final MiriamData copy2 = new MiriamData(identifier);
      copy.setAnnotator(ElementAnnotator.this.getElementAnnotatorInterface());
      return originalBioEntity.getMiriamData().contains(copy) || originalBioEntity.getMiriamData().contains(copy2);
    }

    public LogMarker getLogMarker(final ProjectLogEntryType type) {
      final LogMarker result = new LogMarker(type, originalBioEntity);
      Class<?> clazz = ElementAnnotator.this.getElementAnnotatorInterface();
      if (clazz == null) {
        clazz = this.getClass();
      }
      result.getEntry().setSource(clazz.getSimpleName());
      return result;
    }

    public boolean isElement() {
      return originalBioEntity instanceof Element;
    }

    public void addUniprot(final UniprotRecord ur) {
      if (originalBioEntity instanceof Species) {
        final Species species = ((Species) originalBioEntity);
        species.getUniprots().add(ur);
        ur.setSpecies(species);
      } else {
        logger.warn("Cannot add uniprot object to: " + originalBioEntity.getClass().getSimpleName());
      }
    }

    public boolean isReaction() {
      return originalBioEntity instanceof Reaction;
    }

    /**
     * Sets synonyms to the element.
     *
     * @param synonyms new synonyms list
     */
    public void setSynonyms(final Collection<String> synonyms) {
      if (canAssignStringSet(synonyms, originalBioEntity.getSynonyms(), BioEntityField.SYNONYMS)) {
        final List<String> sortedSynonyms = new ArrayList<>(synonyms);
        Collections.sort(sortedSynonyms);

        originalBioEntity.setSynonyms(sortedSynonyms);
      }
    }

    private boolean canAssignStringSet(final Collection<String> newCollection, final Collection<String> oldCollection,
                                       final BioEntityField field) {
      if (!parameters.hasOutputField(field)) {
        return false;
      }
      if (oldCollection == null || oldCollection.isEmpty()) {
        return true;
      } else if (newCollection == null || newCollection.isEmpty()) {
        return false;
      } else {
        final SetComparator<String> stringSetComparator = new SetComparator<>(new StringComparator());
        final Set<String> set1 = new HashSet<>();
        final Set<String> set2 = new HashSet<>();

        set1.addAll(newCollection);
        set2.addAll(oldCollection);

        if (stringSetComparator.compare(set1, set2) != 0) {
          logger.warn(getLogMarker(ProjectLogEntryType.ANNOTATION_CONFLICT),
              field.getCommonName() + " don't match: \"" + set1 + "\", \"" + set2 + "\"");
          return false;
        }
        return true;
      }
    }

    /**
     * Sets symbol value to the element.
     *
     * @param symbol new symbol
     */
    public void setSymbol(final String symbol) {
      if (canAssign(symbol, originalBioEntity.getSymbol(), BioEntityField.SYMBOL)) {
        originalBioEntity.setSymbol(symbol);
      }
    }

    public void setName(final String nam) {
      if (canAssign(nam, originalBioEntity.getName(), BioEntityField.NAME)) {
        originalBioEntity.setName(nam);
      }
    }

    private boolean canAssign(final String newValue, final String oldValue, final BioEntityField field) {
      if (!parameters.hasOutputField(field)) {
        return false;
      }
      if (oldValue == null || oldValue.trim().equals("") || oldValue.equals(newValue)) {
        return true;
      } else {
        logger.warn(getLogMarker(ProjectLogEntryType.ANNOTATION_CONFLICT), field.getCommonName()
            + " doesn't match: \"" + newValue + "\", \"" + oldValue + "\"");
        return false;
      }
    }

    private boolean canAssign(final String newValue, final Integer oldValue, final BioEntityField field) {
      if (!parameters.hasOutputField(field)) {
        return false;
      }
      if (newValue == null || newValue.isEmpty()) {
        return false;
      }
      if (oldValue == null || oldValue == 0 || oldValue.toString().equals(newValue)) {
        return true;
      } else {
        logger.warn(getLogMarker(ProjectLogEntryType.ANNOTATION_CONFLICT),
            field.getCommonName() + " doesn't match: \"" + newValue + "\", \"" + oldValue + "\"");
        return false;
      }
    }

    public void setFormerSymbols(final Collection<String> formerSymbols) {
      if (originalBioEntity instanceof Element) {
        final Element element = (Element) originalBioEntity;
        if (canAssignStringSet(formerSymbols, element.getFormerSymbols(), BioEntityField.PREVIOUS_SYMBOLS)) {
          final List<String> sorted = new ArrayList<>(formerSymbols);
          Collections.sort(sorted);
          element.setFormerSymbols(sorted);
        }
      } else {
        logger.warn("Cannot assign " + BioEntityField.PREVIOUS_SYMBOLS.getCommonName() + " to "
            + originalBioEntity.getClass().getSimpleName());
      }
    }

    /**
     * Sets name to the element.
     *
     * @param name new name
     */
    public void setFullName(final String name) {
      if (originalBioEntity instanceof Element) {
        final Element element = (Element) originalBioEntity;
        if (canAssign(name, element.getFullName(), BioEntityField.FULL_NAME)) {
          element.setFullName(name);
        }
      } else {
        logger.warn("Cannot assign " + BioEntityField.FULL_NAME.getCommonName() + " to "
            + originalBioEntity.getClass().getSimpleName());
      }
    }

    /**
     * Adds description to {@link BioEntity#getNotes()}.
     *
     * @param description value to set
     */
    public void setDescription(final String description) {
      if (canAssign(description, "", BioEntityField.DESCRIPTION) && description != null) {
        if (originalBioEntity.getNotes() == null
            || description.toLowerCase().contains(originalBioEntity.getNotes().toLowerCase())) {
          originalBioEntity.setNotes(description);
        } else if (!originalBioEntity.getNotes().toLowerCase().contains(description.toLowerCase())) {
          originalBioEntity.setNotes(originalBioEntity.getNotes() + "\n" + description);
        }
      }
    }

    /**
     * Sets {@link Chemical#getSmiles()}.
     *
     * @param smile value to set
     */
    public void setSmile(final String smile) {
      if (originalBioEntity instanceof Chemical) {
        final Chemical element = (Chemical) originalBioEntity;
        if (canAssign(smile, element.getSmiles(), BioEntityField.SMILE)) {
          element.setSmiles(smile);
        }
      } else {
        logger.warn("Cannot assign " + BioEntityField.SMILE.getCommonName() + " to "
            + originalBioEntity.getClass().getSimpleName());
      }
    }

    /**
     * Sets {@link Species#getCharge()}.
     *
     * @param charge value to set
     */
    public void setCharge(final String charge) {
      if (originalBioEntity instanceof Species) {
        final Species element = (Species) originalBioEntity;
        if (canAssign(charge, element.getCharge(), BioEntityField.CHARGE)) {
          element.setCharge(Integer.valueOf(charge));
        }
      } else {
        logger.warn("Cannot assign " + BioEntityField.CHARGE.getCommonName() + " to "
            + originalBioEntity.getClass().getSimpleName());
      }
    }

    /**
     * Sets {@link Reaction#getSubsystem()}.
     *
     * @param subsystem value to set
     */
    public void setSubsystem(final String subsystem) {
      if (originalBioEntity instanceof Reaction) {
        final Reaction element = (Reaction) originalBioEntity;
        if (canAssign(subsystem, element.getSubsystem(), BioEntityField.SUBSYSTEM)) {
          element.setSubsystem(subsystem);
        }
      } else {
        logger.warn("Cannot assign " + BioEntityField.SUBSYSTEM.getCommonName() + " to "
            + originalBioEntity.getClass().getSimpleName());
      }
    }

    /**
     * Sets {@link BioEntity#getFormula()}.
     *
     * @param formula value to set
     */
    public void setFormula(final String formula) {
      if (canAssign(formula, originalBioEntity.getFormula(), BioEntityField.FORMULA)) {
        originalBioEntity.setFormula(formula);
      }
    }

    /**
     * Sets {@link BioEntity#getAbbreviation()}.
     *
     * @param value value to set
     */
    public void setAbbreviation(final String value) {
      if (canAssign(value, originalBioEntity.getAbbreviation(), BioEntityField.ABBREVIATION)) {
        originalBioEntity.setAbbreviation(value);
      }
    }

    /**
     * Sets {@link Reaction#getMechanicalConfidenceScore()}.
     *
     * @param value value to set
     */
    public void setMechanicalConfidenceScore(final String value) {
      if (originalBioEntity instanceof Reaction) {
        final Reaction element = (Reaction) originalBioEntity;
        if (canAssign(value, element.getMechanicalConfidenceScore(), BioEntityField.MCS)) {
          element.setMechanicalConfidenceScore(Integer.valueOf(value));
        }
      } else {
        logger.warn("Cannot assign " + BioEntityField.MCS.getCommonName() + " to "
            + originalBioEntity.getClass().getSimpleName());
      }
    }

    public Object getFieldValue(final AnnotatorOutputParameter output) {
      if (output.getIdentifierType() != null) {
        final Set<MiriamData> collection = new HashSet<>();
        for (final MiriamData md : originalBioEntity.getMiriamData()) {
          if (md.getDataType().equals(output.getIdentifierType())) {
            collection.add(md);
          }
        }
        return collection;
      } else {
        switch (output.getField()) {
          case ABBREVIATION:
            return originalBioEntity.getAbbreviation();
          case CHARGE:
            if (originalBioEntity instanceof Species) {
              return ((Species) originalBioEntity).getCharge();
            } else {
              return null;
            }
          case DESCRIPTION:
            return originalBioEntity.getNotes();
          case FORMULA:
            return originalBioEntity.getFormula();
          case FULL_NAME:
            if (originalBioEntity instanceof Element) {
              return ((Element) originalBioEntity).getFullName();
            } else {
              return null;
            }
          case NAME:
            return originalBioEntity.getName();
          case MCS:
            if (originalBioEntity instanceof Reaction) {
              return ((Reaction) originalBioEntity).getMechanicalConfidenceScore();
            } else {
              return null;
            }
          case PREVIOUS_SYMBOLS:
            if (originalBioEntity instanceof Element) {
              return ((Element) originalBioEntity).getFormerSymbols();
            } else {
              return null;
            }
          case SMILE:
            if (originalBioEntity instanceof Chemical) {
              return ((Chemical) originalBioEntity).getSmiles();
            } else {
              return null;
            }
          case SUBSYSTEM:
            if (originalBioEntity instanceof Reaction) {
              return ((Reaction) originalBioEntity).getSubsystem();
            } else {
              return null;
            }
          case SYMBOL:
            return originalBioEntity.getSymbol();
          case SYNONYMS:
            return originalBioEntity.getSynonyms();
          default:
            throw new NotImplementedException(output.getField() + "");
        }
      }
    }

    public int getMiriamDataCount() {
      return originalBioEntity.getMiriamData().size();
    }

    public Class<?> getBioEntityClass() {
      return originalBioEntity.getClass();
    }

    public Integer getModelId() {
      if (originalBioEntity.getModelData() != null) {
        return originalBioEntity.getModelData().getId();
      } else {
        return null;
      }
    }
  }


}
