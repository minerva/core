package lcsb.mapviewer.annotation.services;

import lcsb.mapviewer.annotation.cache.Cacheable;
import lcsb.mapviewer.model.Article;

/**
 * This class is a backend to publicly available pubmed API in
 * <a href="https://europepmc.org/RestfulWebService">Europe PubMed Central </a>.
 * 
 * @author Piotr Gawron
 * 
 */
public interface PubmedParser extends IExternalService, Cacheable {

  Article getPubmedArticleById(final Integer id) throws PubmedSearchException;

  Article getPubmedArticleById(final String id) throws PubmedSearchException;

  String getSummary(final Integer id) throws PubmedSearchException;

  String getSummary(String id) throws PubmedSearchException;

  String getApiVersion() throws PubmedSearchException;

  String getHtmlFullLinkForId(final Integer id, final boolean withTextPrefix) throws PubmedSearchException;

  String getHtmlFullLinkForId(final Integer id) throws PubmedSearchException;
}
