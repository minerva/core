package lcsb.mapviewer.annotation.services.annotators;

/**
 * Due to some limitation of CellDesigner, some annotations are missing in xml
 * file. Therefore, there are two other places where we look for annotations:
 * <ul>
 * <li>notes,</li>
 * <li>other elements with the same name and type.</li>
 * </ul>
 * <p>
 * This annotator copies data to elements without annotations from other elements
 * with the same name and type.
 * </p>
 */
public interface CopyMissingCellDesignerMiriamDataAnnotator extends IElementAnnotator {
}
