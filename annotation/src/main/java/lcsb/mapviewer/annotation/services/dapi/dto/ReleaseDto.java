package lcsb.mapviewer.annotation.services.dapi.dto;

public class ReleaseDto {
  private int id;

  private String name;

  private LicenseDto license;

  public int getId() {
    return id;
  }

  public void setId(final int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public LicenseDto getLicense() {
    return license;
  }

  public void setLicense(final LicenseDto license) {
    this.license = license;
  }
}
