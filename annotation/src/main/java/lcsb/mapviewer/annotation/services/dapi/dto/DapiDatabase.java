package lcsb.mapviewer.annotation.services.dapi.dto;

public class DapiDatabase {
  private String name;

  protected DapiDatabase() {

  }

  public DapiDatabase(final String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
