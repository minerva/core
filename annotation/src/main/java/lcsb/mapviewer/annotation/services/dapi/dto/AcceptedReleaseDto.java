package lcsb.mapviewer.annotation.services.dapi.dto;

public class AcceptedReleaseDto {
  private ReleaseDto release;

  public ReleaseDto getRelease() {
    return release;
  }

  public void setRelease(final ReleaseDto release) {
    this.release = release;
  }
}
