package lcsb.mapviewer.annotation.services;

/**
 * Interface that should be implemented by all classes that access external
 * resources.
 * 
 * @author Piotr Gawron
 * 
 */
public interface IExternalService {
  /**
   * Returns status of the external resource.
   * 
   * @return status of the external resource. Three different values are possible:
   *         <ul>
   *         <li>
   *         {@link lcsb.mapviewer.annotation.services.ExternalServiceStatus.Status#OK
   *         OK}</li>
   *         <li>
   *         {@link lcsb.mapviewer.annotation.services.ExternalServiceStatus.Status#DOWN
   *         DOWN}</li>
   *         <li>
   *         {@link lcsb.mapviewer.annotation.services.ExternalServiceStatus.Status#CHANGED
   *         CHANGED}</li>
   *         </ul>
   */
  ExternalServiceStatus getServiceStatus();
}
