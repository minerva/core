package lcsb.mapviewer.annotation.services;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SpqarQLResult {

  @JsonProperty
  private Map<String, List<String>> head;

  private Map<String, List<Map<String, Map<String, String>>>> results;

  public List<Map<String, Map<String, String>>> getResults() {
    return results.get("bindings");
  }
}
