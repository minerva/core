package lcsb.mapviewer.annotation.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang3.SerializationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.annotation.cache.CachableInterface;
import lcsb.mapviewer.annotation.cache.QueryCacheInterface;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.Article;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.persist.dao.ArticleDao;
import lcsb.mapviewer.persist.dao.ArticleProperty;

/**
 * This class is a backend to publicly available pubmed API in
 * <a href="https://europepmc.org/RestfulWebService">Europe PubMed Central </a>.
 * 
 * @author Piotr Gawron
 * 
 */
@Service
@Transactional
public class PubmedParserImpl extends CachableInterface implements PubmedParser {

  /**
   * Pubmed identifier used to check the status of service (functionality from
   * {@link IExternalService}).
   */
  static final int SERVICE_STATUS_PUBMED_ID = 12345;

  /**
   * Version of the remote API that is supported by this connecting class.
   */
  static final String SUPPORTED_VERSION = "6.9";
  static final String API_URL = "https://www.ebi.ac.uk/europepmc/webservices/rest/";

  /**
   * Connector used for accessing data from miriam registry.
   */
  private MiriamConnector miriamConnector;

  private ArticleDao articleDao;

  private Logger logger = LogManager.getLogger();

  @Autowired
  public PubmedParserImpl(final MiriamConnector miriamConnector, final ArticleDao articleDao) {
    super(PubmedParser.class);
    this.miriamConnector = miriamConnector;
    this.articleDao = articleDao;
  }

  /**
   * Returns article data for given pubmed identifier.
   *
   * @param id
   *          pubmed identifier
   * @return article data
   * @throws PubmedSearchException
   *           thrown when there is a problem with accessing information about
   *           pubmed
   */
  @Override
  public Article getPubmedArticleById(final Integer id) throws PubmedSearchException {
    String queryString = "pubmed: " + id;
    Map<ArticleProperty, Object> filter = new HashMap<>();
    filter.put(ArticleProperty.PUBMED_ID, id);
    Article result = null;
    Page<Article> articles = articleDao.getAll(filter, Pageable.unpaged());
    if (articles.getContent().size() > 0) {
      result = articles.getContent().get(0);
    }

    if (result != null && result.getTitle() != null) {
      return result;
    } else {
      logger.debug("Pubmed article (id=" + id + ") not found in cache. Accessing WebService...");
    }

    result = new Article(id + "");
    try {
      String url = API_URL + "search/resulttype=core&query="
          + java.net.URLEncoder.encode("src:med ext_id:" + id, "UTF-8");

      String content = getWebPageContent(url);

      Document mainDoc = XmlParser.getXmlDocumentFromString(content);
      // Document citationDoc = dBuilder.parse(citationStream);
      mainDoc.getDocumentElement().normalize();
      // citationDoc.getDocumentElement().normalize();
      XPathFactory factory = XPathFactory.newInstance();
      XPath xpath = factory.newXPath();
      NodeList nodeList = null;
      // Hit Count
      nodeList = (NodeList) xpath.compile("/responseWrapper/resultList/result/citedByCount").evaluate(mainDoc,
          XPathConstants.NODESET);
      if (nodeList != null && nodeList.getLength() > 0 && nodeList.item(0).getFirstChild() != null) {
        try {
          result.setCitationCount(Integer.valueOf(nodeList.item(0).getFirstChild().getNodeValue()));
        } catch (final Exception e) {
        }
      }
      // Title
      nodeList = (NodeList) xpath.compile("/responseWrapper/resultList/result/title").evaluate(mainDoc,
          XPathConstants.NODESET);
      if (nodeList != null && nodeList.getLength() > 0 && nodeList.item(0).getFirstChild() != null) {
        result.setTitle(nodeList.item(0).getFirstChild().getNodeValue());
      }
      // Year
      nodeList = (NodeList) xpath.compile("/responseWrapper/resultList/result/journalInfo/yearOfPublication")
          .evaluate(mainDoc, XPathConstants.NODESET);
      if (nodeList != null && nodeList.getLength() > 0 && nodeList.item(0).getFirstChild() != null) {
        try {
          result.setYear(Integer.valueOf(nodeList.item(0).getFirstChild().getNodeValue()));
        } catch (final Exception e) {
        }
      }
      // Journal
      nodeList = (NodeList) xpath.compile("/responseWrapper/resultList/result/journalInfo/journal/title")
          .evaluate(mainDoc, XPathConstants.NODESET);
      if (nodeList != null && nodeList.getLength() > 0 && nodeList.item(0).getFirstChild() != null) {
        result.setJournal(nodeList.item(0).getFirstChild().getNodeValue());
      }
      // Authors
      nodeList = (NodeList) xpath.compile("/responseWrapper/resultList/result/authorString").evaluate(mainDoc,
          XPathConstants.NODESET);
      if (nodeList != null && nodeList.getLength() > 0 && nodeList.item(0).getFirstChild() != null) {
        List<String> authors = new ArrayList<String>();
        String value = nodeList.item(0).getFirstChild().getNodeValue();
        if (value != null && !value.isEmpty()) {
          authors = Arrays.asList(value.split(","));
        }
        result.setAuthors(authors);
      }
      result.setLink(miriamConnector.getUrlString(new MiriamData(MiriamType.PUBMED, id + "")));
      result.setPubmedId(id + "");

      if (result.getTitle() != null && !result.getTitle().trim().isEmpty()) {
        try {
          articleDao.add(result);
        } catch (final SerializationException e) {
          logger.warn("Problem with serialization of the string: " + queryString);
        }
      } else {
        result = null;
      }

    } catch (final Exception e) {
      throw new PubmedSearchException(e);
    }
    return result;
  }

  @Override
  public Article getPubmedArticleById(final String id) throws PubmedSearchException {
    if (id == null) {
      return null;
    }
    return getPubmedArticleById(Integer.valueOf(id.replaceAll("[^0-9.]", "").trim()));
  }

  /**
   * This method return html \< a\ > tag with link for pubmed id (with some
   * additional information).
   *
   * @param id
   *          pubmed identifier
   * @param withTextPrefix
   *          should prefix be added to the tag
   * @return link to webpage with pubmed article
   * @throws PubmedSearchException
   *           thrown when there is a problem with accessing information about
   *           pubmed
   */
  @Override
  public String getHtmlFullLinkForId(final Integer id, final boolean withTextPrefix) throws PubmedSearchException {
    String result = "";
    Article article = getPubmedArticleById(id);
    result += "<div style=\"float:left;\" title=\"" + article.getTitle() + ", ";
    result += article.getStringAuthors() + ", ";
    result += article.getYear() + ", ";
    result += article.getJournal() + "\">";
    if (withTextPrefix) {
      result += "pubmed: ";
    }
    result += "<a target=\"_blank\" href = \"" + article.getLink() + "\">" + id + "</a>&nbsp;</div>";
    return result;
  }

  /**
   * This method return html \< a\ > tag with link for pubmed id (with some
   * additional information).
   *
   * @param id
   *          pubmed identifier
   * @return link to webpage with pubmed article
   * @throws PubmedSearchException
   *           thrown when there is a problem with accessing information about
   *           pubmed
   */
  @Override
  public String getHtmlFullLinkForId(final Integer id) throws PubmedSearchException {
    return getHtmlFullLinkForId(id, true);
  }

  /**
   * Get the summary of the article.
   *
   * @param id
   *          pubmed identifier
   * @return summary of the article.
   * @throws PubmedSearchException
   *           thrown when there is a problem with accessing information about
   *           pubmed
   */
  @Override
  public String getSummary(final Integer id) throws PubmedSearchException {
    Article article = getPubmedArticleById(id);
    if (article == null) {
      return null;
    }
    String result = article.getTitle() + ", " + article.getStringAuthors() + ", " + article.getYear() + ", "
        + article.getJournal();
    return result;
  }

  /**
   * Get the summary of the article.
   *
   * @param id
   *          pubmed identifier
   * @return summary of the article.
   * @throws PubmedSearchException
   *           thrown when there is a problem with accessing information about
   *           pubmed
   */
  @Override
  public String getSummary(final String id) throws PubmedSearchException {
    return getSummary(Integer.valueOf(id));
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus("Europe PubMed Central",
        "https://europepmc.org/RestfulWebService");
    QueryCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      Article art = getPubmedArticleById(SERVICE_STATUS_PUBMED_ID);
      if (!getApiVersion().equals(SUPPORTED_VERSION)) {
        logger.debug("New europepmc API version: " + getApiVersion());
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else if (art == null) {
        status.setStatus(ExternalServiceStatusType.DOWN);
      } else {
        status.setStatus(ExternalServiceStatusType.OK);
      }
    } catch (final Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  /**
   * Returns current version of the pmc API.
   *
   * @return version of the API to which this class is connected
   * @throws PubmedSearchException
   *           thrown when there is a problem with accessing external database
   */
  @Override
  public String getApiVersion() throws PubmedSearchException {
    try {
      String url = API_URL + "search/resulttype=core&query=src%3Amed+ext_id%3A23644949";

      String content = getWebPageContent(url);

      Document mainDoc = XmlParser.getXmlDocumentFromString(content);
      mainDoc.getDocumentElement().normalize();
      XPathFactory factory = XPathFactory.newInstance();
      XPath xpath = factory.newXPath();
      NodeList nodeList = (NodeList) xpath.compile("/responseWrapper/version").evaluate(mainDoc,
          XPathConstants.NODESET);
      if (nodeList.getLength() > 0) {
        return nodeList.item(0).getTextContent();
      }
      return null;
    } catch (final IOException e) {
      throw new PubmedSearchException("Problem with accessing pubmed db", e);
    } catch (final InvalidXmlSchemaException | XPathExpressionException e) {
      throw new PubmedSearchException("Invalid response from pubmed db", e);
    }
  }

  /**
   * @return the miriamConnector
   * @see #miriamConnector
   */
  public MiriamConnector getMiriamConnector() {
    return miriamConnector;
  }

  /**
   * @param miriamConnector
   *          the miriamConnector to set
   * @see #miriamConnector
   */
  public void setMiriamConnector(final MiriamConnector miriamConnector) {
    this.miriamConnector = miriamConnector;
  }

}
