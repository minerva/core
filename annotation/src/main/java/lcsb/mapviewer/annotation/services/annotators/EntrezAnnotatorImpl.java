package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.cache.QueryCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.XmlSerializer;
import lcsb.mapviewer.annotation.data.EntrezData;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.model.user.annotator.AnnotatorInputParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;
import lcsb.mapviewer.model.user.annotator.BioEntityField;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.stereotype.Service;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class is responsible for connecting to
 * <a href="http://eutils.ncbi.nlm.nih.gov/entrez/">Entrez API</a> and annotate
 * elements with information taken from this service.
 *
 * @author Piotr Gawron
 */
@Service
public class EntrezAnnotatorImpl extends ElementAnnotator implements EntrezAnnotator {

  /**
   * Prefix used in cache key to indicate that cached value contains
   * {@link EntrezData} object for a given entrez id.
   */
  static final String ENTREZ_DATA_PREFIX = "ENTREZ_DATA:";
  /**
   * Address of Entrez API that should be used for retrieving data.
   */
  private static final String REST_API_URL = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=gene&rettype=xml&id=";

  /**
   * Object that allows to serialize {@link EntrezData} elements into xml string
   * and deserialize xml into {@link EntrezData} objects.
   */
  private final XmlSerializer<EntrezData> entrezSerializer;

  /**
   * Default constructor.
   */
  public EntrezAnnotatorImpl() {
    super(EntrezAnnotator.class, new Class[]{Protein.class, Rna.class, Gene.class}, false);
    entrezSerializer = new XmlSerializer<>(EntrezData.class);
  }

  @Override
  public String refreshCacheQuery(final Object query) throws SourceNotAvailable {
    String result = null;
    try {
      if (query instanceof String) {
        String name = (String) query;
        if (name.startsWith("http")) {
          result = getWebPageContent(name);
        } else if (name.startsWith(ENTREZ_DATA_PREFIX)) {
          String id = name.replace(ENTREZ_DATA_PREFIX, "");
          result = entrezSerializer
              .objectToString(getEntrezForMiriamData(new MiriamData(MiriamType.ENTREZ, id), null));
        } else {
          throw new InvalidArgumentException("Don't know what to do with input string: " + name);
        }
      } else {
        throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
      }
    } catch (final AnnotatorException e) {
      throw new SourceNotAvailable(e);
    } catch (final IOException e) {
      throw new SourceNotAvailable(e);
    }
    return result;
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    QueryCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      GenericProtein proteinAlias = new GenericProtein("id");
      proteinAlias.addMiriamData(getExampleValidAnnotation());
      annotateElement(proteinAlias);

      if (proteinAlias.getFullName() == null || proteinAlias.getFullName().equals("")) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else {
        status.setStatus(ExternalServiceStatusType.OK);
      }
    } catch (final Exception e) {
      logger.error(getCommonName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  @Override
  public boolean annotateElement(final BioEntityProxy element, final MiriamData identifier, final AnnotatorData parameters)
      throws AnnotatorException {
    if (identifier.getDataType().equals(MiriamType.ENTREZ)) {
      EntrezData data = getEntrezForMiriamData(identifier, element.getLogMarker(ProjectLogEntryType.OTHER));
      if (data != null) {
        element.setSymbol(data.getSymbol());

        element.setFullName(data.getFullName());

        element.addMiriamData(data.getMiriamData());
        element.setSynonyms(data.getSynonyms());
        element.setDescription(data.getDescription());
        return true;
      }
      return false;
    } else {
      throw new NotImplementedException();
    }
  }

  @Override
  public String getCommonName() {
    return MiriamType.ENTREZ.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.ENTREZ.getDbHomepage();
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Collections.singletonList(new AnnotatorInputParameter(MiriamType.ENTREZ));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Arrays.asList(new AnnotatorOutputParameter(MiriamType.ENSEMBL),
        new AnnotatorOutputParameter(MiriamType.HGNC),
        new AnnotatorOutputParameter(BioEntityField.FULL_NAME),
        new AnnotatorOutputParameter(BioEntityField.DESCRIPTION),
        new AnnotatorOutputParameter(BioEntityField.SYMBOL),
        new AnnotatorOutputParameter(BioEntityField.SYNONYMS));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.ENTREZ, "6621");
  }

  @Override
  public EntrezData getEntrezForMiriamData(final MiriamData entrezMiriamData, final LogMarker marker) throws AnnotatorException {
    EntrezData result = entrezSerializer
        .xmlToObject(super.getCacheNode(ENTREZ_DATA_PREFIX + entrezMiriamData.getResource()));
    if (result == null) {
      String query = REST_API_URL + entrezMiriamData.getResource();
      try {
        String content = getWebPageContent(query);
        result = new EntrezData();
        Node xml = XmlParser.getXmlDocumentFromString(content, false);

        Node response = XmlParser.getNode("Entrezgene-Set", xml.getChildNodes());
        if (response != null) {
          Node errorNode = XmlParser.getNode("Error", response.getChildNodes());
          if (errorNode != null) {
            LogMarker m = new LogMarker(marker);
            m.getEntry().setType(ProjectLogEntryType.INVALID_IDENTIFIER);
            logger.warn(m, " Invalid " + MiriamType.ENTREZ + " identifier: \"" + entrezMiriamData.getResource() + "\"");
            return null;
          }
        } else {
          LogMarker m = new LogMarker(marker);
          m.getEntry().setType(ProjectLogEntryType.OTHER);
          logger.warn(m, "Problematic entrez response for identifier: \"" + entrezMiriamData.getResource() + "\"");
          return null;
        }

        Node rootNode = XmlParser.getNode("Entrezgene", response.getChildNodes());

        Node annotationNode = XmlParser.getNode("Entrezgene_gene", rootNode.getChildNodes());
        Node annotationGenRefNode = XmlParser.getNode("Gene-ref", annotationNode.getChildNodes());

        Node symbolNode = XmlParser.getNode("Gene-ref_locus", annotationGenRefNode.getChildNodes());
        if (symbolNode != null) {
          String symbol = symbolNode.getTextContent();
          result.setSymbol(symbol);
        }

        Node fullNameNode = XmlParser.getNode("Gene-ref_desc", annotationGenRefNode.getChildNodes());
        if (fullNameNode != null) {
          String fullname = fullNameNode.getTextContent();
          result.setFullName(fullname);
        }

        Node listNode = XmlParser.getNode("Gene-ref_db", annotationGenRefNode.getChildNodes());
        Set<String> synonyms = new HashSet<String>();
        if (listNode != null) {
          NodeList list = XmlParser.getNode("Gene-ref_db", annotationGenRefNode.getChildNodes()).getChildNodes();

          for (int i = 0; i < list.getLength(); i++) {
            Node node = list.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
              if (node.getNodeName().equals("Dbtag")) {
                Node dbNameNode = XmlParser.getNode("Dbtag_db", node.getChildNodes());
                String dbName = dbNameNode.getTextContent();

                Node dbIdNode1 = XmlParser.getNode("Dbtag_tag", node.getChildNodes());
                Node dbIdNode2 = XmlParser.getNode("Object-id", dbIdNode1.getChildNodes());
                Node dbIdNode3 = XmlParser.getNode("Object-id_str", dbIdNode2.getChildNodes());
                if (dbIdNode3 == null) {
                  dbIdNode3 = XmlParser.getNode("Object-id_id", dbIdNode2.getChildNodes());
                }
                String dbId = null;
                if (dbIdNode3 != null) {
                  dbId = dbIdNode3.getTextContent();
                }
                if ("HGNC".equals(dbName)) {
                  if (dbId != null && !dbId.isEmpty()) {
                    dbId = dbId.replaceAll("HGNC:", "");
                    result.addMiriamData(new MiriamData(MiriamType.HGNC, dbId));
                  }
                } else if ("Ensembl".equals(dbName)) {
                  if (dbId != null && !dbId.isEmpty()) {
                    result.addMiriamData(new MiriamData(MiriamType.ENSEMBL, dbId));
                  }
                }

              }
            }
          }

        }

        Node synListNode = XmlParser.getNode("Gene-ref_syn", annotationGenRefNode.getChildNodes());
        if (synListNode != null) {
          NodeList list = XmlParser.getNode("Gene-ref_syn", annotationGenRefNode.getChildNodes()).getChildNodes();

          for (int i = 0; i < list.getLength(); i++) {
            Node node = list.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
              if (node.getNodeName().equals("Gene-ref_syn_E")) {
                synonyms.add(StringEscapeUtils.unescapeHtml4(node.getTextContent()));
              }
            }
          }
        }
        if (synonyms.size() > 0) {
          List<String> sortedSynonyms = new ArrayList<String>();
          sortedSynonyms.addAll(synonyms);
          Collections.sort(sortedSynonyms);
          result.setSynonyms(sortedSynonyms);
        }
        Node notesNode = XmlParser.getNode("Entrezgene_summary", rootNode.getChildNodes());
        if (notesNode != null) {
          String notes = notesNode.getTextContent();
          if (notes != null) {
            result.setDescription(notes);
          }
        }
        setCacheValue(ENTREZ_DATA_PREFIX + entrezMiriamData.getResource(), entrezSerializer.objectToString(result));
      } catch (final WrongResponseCodeIOException e) {
        if (e.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST) {
          LogMarker m = new LogMarker(marker);
          m.getEntry().setType(ProjectLogEntryType.INVALID_IDENTIFIER);
          logger.warn(m, " Invalid " + MiriamType.ENTREZ.getCommonName() + " identifier found");
        } else {
          throw new AnnotatorException(e);
        }
      } catch (final IOException e) {
        throw new AnnotatorException(e);
      } catch (final InvalidXmlSchemaException e) {
        throw new AnnotatorException(e);
      }
    }
    return result;
  }
}
