package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.model.user.annotator.AnnotatorInputParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;
import lcsb.mapviewer.model.user.annotator.BioEntityField;
import lcsb.mapviewer.persist.dao.map.species.ElementDao;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class CopyMissingCellDesignerMiriamDataAnnotatorImpl extends ElementAnnotator implements CopyMissingCellDesignerMiriamDataAnnotator {

  private final ElementDao elementDao;

  @Autowired
  public CopyMissingCellDesignerMiriamDataAnnotatorImpl(final ElementDao elementDao) {
    super(CopyMissingCellDesignerMiriamDataAnnotator.class, new Class[]{Element.class}, true);
    this.elementDao = elementDao;
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), "");
    status.setStatus(ExternalServiceStatusType.OK);
    return status;
  }

  @Override
  public boolean annotateElement(final BioEntityProxy element, final MiriamData identifier, final AnnotatorData parameters)
      throws AnnotatorException {
    return false;
  }

  @Override
  public boolean annotateElement(final BioEntityProxy object, final String name, final AnnotatorData parameters)
      throws AnnotatorException {
    if (object.isElement() && object.getMiriamDataCount() == 0) {
      Map<ElementProperty, Object> filter = new HashMap<>();
      filter.put(ElementProperty.PERFECT_TEXT, Collections.singletonList(name));
      filter.put(ElementProperty.MAP_ID, Collections.singletonList(object.getModelId()));
      filter.put(ElementProperty.CLASS, Collections.singletonList(object.getBioEntityClass()));
      List<Element> elements = elementDao.getAll(filter, Pageable.unpaged()).getContent();
      for (Element element : elements) {
        if (!element.getMiriamData().isEmpty()) {
          for (MiriamData miriamData : element.getMiriamData()) {
            object.addMiriamData(new MiriamData(miriamData));
          }
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public String getCommonName() {
    return "CellDesigner copy issue";
  }

  @Override
  public String getUrl() {
    return "https://minerva.pages.uni.lu/doc/";
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Collections.singletonList(new AnnotatorInputParameter(BioEntityField.NAME));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return new ArrayList<>();
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.UNIPROT, "P12345");
  }

}
