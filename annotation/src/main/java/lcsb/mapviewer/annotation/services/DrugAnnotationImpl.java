package lcsb.mapviewer.annotation.services;

import lcsb.mapviewer.annotation.cache.CachableInterface;
import lcsb.mapviewer.annotation.cache.Cacheable;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.XmlSerializer;
import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.ProjectDao;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Abstract class with some functionalities used by class accessing drug
 * databases.
 *
 * @author Piotr Gawron
 */
@Transactional
public abstract class DrugAnnotationImpl extends CachableInterface implements DrugAnnotation {

  /**
   * Prefix used for caching list of suggested queries for project.
   */
  protected static final String PROJECT_SUGGESTED_QUERY_PREFIX = "PROJECT_DRUG_QUERIES:";

  /**
   * Default class logger.
   */
  protected final Logger logger = LogManager.getLogger();

  /**
   * Object that allows to serialize {@link Drug} elements into xml string and
   * deserialize xml into {@link Drug} objects.
   */
  private XmlSerializer<Drug> drugSerializer;

  /**
   * Object used to access information about organism taxonomy.
   */
  @Autowired
  private TaxonomyBackend taxonomyBackend;

  @Autowired
  private HgncAnnotator hgncAnnotator;

  @Autowired
  private ProjectDao projectDao;

  /**
   * Default constructor. Initializes structures used for transforming
   * {@link Drug} from/to xml.
   *
   * @param clazz class that identifies this {@link Cacheable}
   */
  public DrugAnnotationImpl(final Class<? extends Cacheable> clazz) {
    super(clazz);
    drugSerializer = new XmlSerializer<>(Drug.class);
  }

  /**
   * Returns a drug for given name.
   *
   * @param name name of the drug
   * @return drug for a given name
   * @throws DrugSearchException thrown when there are problems with finding drug
   */
  @Override
  public abstract Drug findDrug(final String name) throws DrugSearchException;

  /**
   * Returns list of drugs that target protein.
   *
   * @param target    {@link MiriamData} describing target
   * @param organisms list of organisms to which results should be limited (when no
   *                  organisms defined filtering will be turned off)
   * @return list of drugs
   * @throws DrugSearchException thrown when there are problems with finding drug
   */

  @Override
  public abstract List<Drug> getDrugListByTarget(final MiriamData target, final Collection<MiriamData> organisms)
      throws DrugSearchException;

  /**
   * Returns list of drugs that target protein.
   *
   * @param target {@link MiriamData} describing target
   * @return list of drugs
   * @throws DrugSearchException thrown when there are problems with finding drug
   */
  @Override
  public List<Drug> getDrugListByTarget(final MiriamData target) throws DrugSearchException {
    return getDrugListByTarget(target, new ArrayList<>());
  }

  @Override
  public String refreshCacheQuery(final Object query) throws SourceNotAvailable {
    String result = null;
    try {
      if (query instanceof String) {
        final String name = (String) query;
        if (name.startsWith(PROJECT_SUGGESTED_QUERY_PREFIX)) {
          final String[] tmp = name.split("\n");
          final Integer id = Integer.valueOf(tmp[1]);
          final MiriamData diseaseId = new MiriamData(MiriamType.MESH_2012, tmp[2]);
          final Project project = projectDao.getById(id);
          if (project == null) {
            throw new SourceNotAvailable("Project with given id doesn't exist: " + id);
          }
          final List<String> list = getSuggestedQueryListWithoutCache(project, diseaseId);
          result = StringUtils.join(list, "\n");

        } else if (name.startsWith("http")) {
          result = getWebPageContent(name);
        } else {
          throw new InvalidArgumentException("Don't know what to do with string \"" + query + "\"");
        }
      } else {
        throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
      }
    } catch (final DrugSearchException e) {
      throw new SourceNotAvailable(e);
    } catch (final IOException e) {
      throw new SourceNotAvailable(e);
    }
    return result;
  }

  /**
   * Returns list of drugs that target at least one protein from the parameter
   * list.
   *
   * @param targets   list of {@link MiriamData} describing targets
   * @param organisms list of organisms to which results should be limited (when no
   *                  organisms defined filtering will be turned off)
   * @return list of drugs
   * @throws DrugSearchException thrown when there are problems with connection to drug database
   */
  @Override
  public List<Drug> getDrugListByTargets(final Collection<MiriamData> targets, final Collection<MiriamData> organisms)
      throws DrugSearchException {
    final List<Drug> result = new ArrayList<Drug>();
    final Set<String> set = new HashSet<String>();
    final Set<MiriamData> searchedResult = new HashSet<MiriamData>();
    for (final MiriamData md : targets) {
      if (searchedResult.contains(md)) {
        continue;
      }
      searchedResult.add(md);
      final List<Drug> drugs = getDrugListByTarget(md, organisms);
      for (final Drug drug : drugs) {
        if (!set.contains(drug.getSources().get(0).getResource())) {
          result.add(drug);
          set.add(drug.getSources().get(0).getResource());
        } else {
          continue;
        }
      }
    }
    return result;
  }

  /**
   * Returns list of drugs that target at least one protein from the parameter
   * list.
   *
   * @param targets list of {@link MiriamData} describing targets
   * @return list of drugs
   * @throws DrugSearchException thrown when there are problems with connection to drug database
   */
  @Override
  public List<Drug> getDrugListByTargets(final Collection<MiriamData> targets) throws DrugSearchException {
    return getDrugListByTargets(targets, new ArrayList<>());
  }

  /**
   * @return the drugSerializer
   * @see #drugSerializer
   */
  protected XmlSerializer<Drug> getDrugSerializer() {
    return drugSerializer;
  }

  /**
   * @param drugSerializer the drugSerializer to set
   * @see #drugSerializer
   */
  protected void setDrugSerializer(final XmlSerializer<Drug> drugSerializer) {
    this.drugSerializer = drugSerializer;
  }

  /**
   * Checks if organism name matches list of organisms.
   */
  protected boolean organismMatch(final String organismName, final Collection<MiriamData> organisms) {
    if (organismName == null || organismName.isEmpty() || organisms.size() == 0) {
      return true;
    }
    try {
      final MiriamData organism = taxonomyBackend.getByName(organismName);
      return organisms.contains(organism);
    } catch (final TaxonomySearchException e) {
      logger.error("Problem with taxonomy search for: " + organismName, e);
      return true;
    }
  }

  /**
   * @return the taxonomyBackend
   * @see #taxonomyBackend
   */
  public TaxonomyBackend getTaxonomyBackend() {
    return taxonomyBackend;
  }

  /**
   * @param taxonomyBackend the taxonomyBackend to set
   * @see #taxonomyBackend
   */
  public void setTaxonomyBackend(final TaxonomyBackend taxonomyBackend) {
    this.taxonomyBackend = taxonomyBackend;
  }

  @Override
  public List<String> getSuggestedQueryList(final Project project, final MiriamData organism) throws DrugSearchException {
    if (organism == null) {
      return new ArrayList<>();
    }
    final String cacheQuery = PROJECT_SUGGESTED_QUERY_PREFIX + "\n" + project.getId() + "\n" + organism.getResource();
    String cachedData = getCacheValue(cacheQuery);
    final List<String> result;
    if (cachedData == null) {
      return null;
    } else {
      result = new ArrayList<>();
      for (final String string : cachedData.split("\n")) {
        if (!string.isEmpty()) {
          result.add(string);
        }
      }
    }
    return result;
  }

  @Override
  public void setDrugSuggestedQueryList(final int projectId, final MiriamData organism, final List<String> list) {
    String cachedData = StringUtils.join(list, "\n");
    final String cacheQuery = PROJECT_SUGGESTED_QUERY_PREFIX + "\n" + projectId + "\n" + organism.getResource();
    setCacheValue(cacheQuery, cachedData);
  }


  @Override
  public List<String> getSuggestedQueryListWithoutCache(final Project sourceProject, final MiriamData organism)
      throws DrugSearchException {
    final Project project;
    if (Hibernate.isInitialized(sourceProject.getModels())) {
      project = sourceProject;
    } else {
      project = projectDao.getById(sourceProject.getId());
    }
    final Set<MiriamData> targets = new HashSet<>();
    for (final ModelData model : project.getModels()) {
      for (final Element element : model.getElements()) {
        MiriamData uniprot = null;
        MiriamData hgncSymbol = null;
        for (final MiriamData miriam : element.getMiriamData()) {
          if (miriam.getDataType().equals(MiriamType.UNIPROT)) {
            uniprot = miriam;
          } else if (miriam.getDataType().equals(MiriamType.HGNC_SYMBOL)) {
            hgncSymbol = miriam;
          }
        }
        if (hgncSymbol != null) {
          targets.add(hgncSymbol);
        } else if (uniprot != null) {
          targets.add(uniprot);
        } else {
          boolean validClass = false;
          for (final Class<?> clazz : MiriamType.HGNC_SYMBOL.getValidClass()) {
            if (clazz.isAssignableFrom(element.getClass())) {
              validClass = true;
            }
          }
          if (validClass) {
            final MiriamData md = new MiriamData(MiriamType.HGNC_SYMBOL, element.getName());
            try {
              if (hgncAnnotator.isValidHgncMiriam(md)) {
                targets.add(md);
              }
            } catch (final AnnotatorException e) {
              logger.error("Problem with accessing HGNC database", e);
            }
          }
        }
      }
    }
    final List<MiriamData> organisms = new ArrayList<>();
    if (organism != null) {
      organisms.add(organism);
    }
    final List<String> result = new ArrayList<>();
    final Set<String> resultSet = new HashSet<>();
    for (final MiriamData target : targets) {
      final List<Drug> drugs = getDrugListByTarget(target, organisms);
      for (final Drug chemical : drugs) {
        resultSet.add(chemical.getName());
      }

    }
    result.addAll(resultSet);
    Collections.sort(result);
    return result;
  }

  @Override
  public void refreshDrugSuggestedQueryList(final Integer projectId, final MiriamData organism) {
    Project project = projectDao.getById(projectId);

    try {
      final List<String> result = getSuggestedQueryListWithoutCache(project, organism);
      logger.debug(result);
      setDrugSuggestedQueryList(projectId, organism, result);
    } catch (DrugSearchException e) {
      logger.error("Problem with refreshing chemical query list", e);
    }
  }

}
