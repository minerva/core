package lcsb.mapviewer.annotation.services;

/**
 * Status of the external service.
 * 
 * @author Piotr Gawron
 * 
 */
public enum ExternalServiceStatusType {

  /**
   * Service is ok and access method is ok.
   */
  OK("Ok", 0),

  /**
   * Service is down (we don't know anything about access method).
   */
  DOWN("Not available", 1),

  /**
   * Service is up, but access method changed.
   */
  CHANGED("Specification changed", 2);

  /**
   * Common name of the status.
   */
  private String commonName;

  /**
   * Severity level of the status (the higher the worse).
   */
  private int level;

  /**
   * Default constructor.
   * 
   * @param commonName
   *          name of the status
   * @param level
   *          severity level of the status
   */
  ExternalServiceStatusType(final String commonName, final int level) {
    this.commonName = commonName;
    this.level = level;
  }

  /**
   * @return the commonName
   * @see #commonName
   */
  public String getCommonName() {
    return commonName;
  }

  /**
   * @return the level
   * @see #level
   */
  public int getLevel() {
    return level;
  }

}
