package lcsb.mapviewer.annotation.services.annotators;

import java.util.Collection;

import lcsb.mapviewer.model.map.MiriamData;

/**
 * This is a class that implements a backend to TAIR. Note that TAIR annotation
 * process will annotate only records which have a TAIR ID assigned by a human
 * annotator. Otherwise, it would generate UniProt miriam records also for TAIR
 * IDs generated from, e.g., final KEGG annotator, i.e. for homologues and these
 * UniProt IDs would be indistinguishable from the UniProt IDs describing the
 * molecule.
 *
 * @author David Hoksza
 * 
 */
public interface TairAnnotator extends IElementAnnotator {

  Collection<MiriamData> tairToUniprot(final MiriamData tair) throws AnnotatorException;

  String tairLocusIdToTairName(String string) throws AnnotatorException;

  Collection<MiriamData> tairLocusIdToUniprot(String string) throws AnnotatorException;

  String tairLocusNameToId(String tairLocusId) throws AnnotatorException;
}
