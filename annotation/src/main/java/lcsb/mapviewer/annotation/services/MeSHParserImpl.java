package lcsb.mapviewer.annotation.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.annotation.cache.CachableInterface;
import lcsb.mapviewer.annotation.cache.QueryCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.XmlSerializer;
import lcsb.mapviewer.annotation.data.MeSH;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import org.apache.commons.lang3.SerializationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Node;

import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Service
@Transactional
public class MeSHParserImpl extends CachableInterface implements MeSHParser {

  /**
   * Prefix used in the DB to identify the cache entry.
   */
  static final String MESH_PREFIX = "mesh:";

  private static final String SPARQL_QUERY_PREFIX = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
      + "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
      + "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n"
      + "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n"
      + "PREFIX meshv: <http://id.nlm.nih.gov/mesh/vocab#>\n"
      + "PREFIX mesh: <http://id.nlm.nih.gov/mesh/>\n"
      + "PREFIX mesh2022: <http://id.nlm.nih.gov/mesh/2022/>\n"
      + "PREFIX mesh2021: <http://id.nlm.nih.gov/mesh/2021/>\n"
      + "PREFIX mesh2020: <http://id.nlm.nih.gov/mesh/2020/>\n"
      + "\n";

  private static final String SPARQL_URL = "https://id.nlm.nih.gov/mesh/sparql?format=JSON&inference=true&offset=0&limit=1000&query=";

  private final Logger logger = LogManager.getLogger();

  private final XmlSerializer<MeSH> meshSerializer;

  private final ObjectMapper objectMapper = new ObjectMapper();

  @Autowired
  public MeSHParserImpl() {
    super(MeSHParser.class);
    meshSerializer = new XmlSerializer<>(MeSH.class);
  }

  @Override
  public Object refreshCacheQuery(final Object query) throws SourceNotAvailable {
    Object result = null;
    try {
      if (query instanceof String) {
        String identifier = (String) query;
        if (identifier.startsWith(MESH_PREFIX)) {
          String[] ids = identifier.split("\\:");
          if (ids.length == 2) {
            MiriamData meshID = new MiriamData(MiriamType.MESH_2012, ids[1]);
            MeSH mesh = getMeSHByIdFromDB(meshID);
            result = meshSerializer.objectToString(mesh);
          } else {
            throw new InvalidArgumentException("Problematic query: \"" + query + "\"");
          }
        } else {
          result = super.refreshCacheQuery(query);
        }
      } else {
        result = super.refreshCacheQuery(query);
      }
    } catch (SourceNotAvailable e) {
      throw new SourceNotAvailable("Problem with accessing Mesh database", e);
    }
    return result;
  }

  @Override
  public MeSH getMeSH(final MiriamData meshID) {
    if (meshID == null || meshID.getResource() == null) {
      throw new InvalidArgumentException("mesh cannot be null ");
    }
    if (meshID.getResource().contains(" ")) {
      return null;
    }

    MeSH mesh = null;
    // look for Mesh in the cache
    String id = getIdentifier(meshID);
    Node meshNode = super.getCacheNode(id);
    if (meshNode != null && meshNode.hasChildNodes()) {
      try {
        mesh = meshSerializer.xmlToObject(meshNode);
      } catch (final SerializationException e) {
        logger.warn("Problem with processing cached info about mesh: " + meshID);
        mesh = null;
      }
    }
    if (mesh == null) {
      try {
        mesh = getMeSHByIdFromDB(meshID);
      } catch (SourceNotAvailable e) {
        logger.error("Problem with accessing MeSH database", e);
        return null;
      }
    }
    if (mesh != null) {
      super.setCacheValue(id, this.meshSerializer.objectToString(mesh));
    }

    return mesh;
  }

  /**
   * @param meshID id as miriam data.
   * @return return the object.
   */
  @Override
  public String getIdentifier(final MiriamData meshID) {
    return MESH_PREFIX + meshID.getResource();
  }

  /**
   * @param meshID mesh id as Miriam data.
   * @return return as mesh object.
   * @throws SourceNotAvailable thrown when there is a problem with accessing mesh db
   */
  private MeSH getMeSHByIdFromDB(final MiriamData meshID) throws SourceNotAvailable {
    MeSH result = new MeSH();

    result.setName(getName(meshID));
    if (result.getName() == null) {
      return null;
    }
    result.addSynonyms(getSynonyms(meshID));
    result.setDescription(getDescription(meshID));
    result.setMeSHId(meshID.getResource());
    result.removeSynonym(result.getName());

    return result;
  }

  private String getName(final MiriamData meshID) throws SourceNotAvailable {
    String query = (SPARQL_QUERY_PREFIX
        + "SELECT * \n"
        + "FROM <http://id.nlm.nih.gov/mesh>\n"
        + "WHERE {\n"
        + "  mesh:MESH_ID rdfs:label ?name .\n"
        + "}\n").replace("MESH_ID", meshID.getResource());

    try {
      String page = getWebPageContent(SPARQL_URL + URLEncoder.encode(query, "UTF-8"));
      SpqarQLResult result = objectMapper.readValue(page, SpqarQLResult.class);

      String name = null;
      for (Map<String, Map<String, String>> row : result.getResults()) {
        name = (String) ((Map<?, ?>) row.get("name")).get("value");
      }
      return name;
    } catch (Exception e) {
      throw new SourceNotAvailable(e);
    }
  }

  private String getDescription(final MiriamData meshID) throws SourceNotAvailable {
    String query = (SPARQL_QUERY_PREFIX
        + "SELECT * \n"
        + "FROM <http://id.nlm.nih.gov/mesh>\n"
        + "WHERE {\n"
        + "  mesh:MESH_ID meshv:concept ?concept .\n"
        + " ?concept meshv:scopeNote ?description .\n"
        + "}\n").replace("MESH_ID", meshID.getResource());

    try {
      String page = getWebPageContent(SPARQL_URL + URLEncoder.encode(query, "UTF-8"));
      SpqarQLResult result = objectMapper.readValue(page, SpqarQLResult.class);

      String description = null;
      for (Map<String, Map<String, String>> row : result.getResults()) {
        description = (String) ((Map<?, ?>) row.get("description")).get("value");
      }
      return description;
    } catch (Exception e) {
      throw new SourceNotAvailable(e);
    }
  }

  private Set<String> getSynonyms(final MiriamData meshID) throws SourceNotAvailable {
    Set<String> synonyms = new HashSet<>();
    String query = (SPARQL_QUERY_PREFIX
        + "SELECT * \n"
        + "FROM <http://id.nlm.nih.gov/mesh>\n"
        + "WHERE {\n"
        + "  mesh:MESH_ID meshv:concept ?concept .\n"
        + " ?concept meshv:term ?term.\n"
        + " ?term rdfs:label ?synonym.\n"
        + "}\n").replace("MESH_ID", meshID.getResource());

    try {
      String page = getWebPageContent(SPARQL_URL + URLEncoder.encode(query, "UTF-8"));
      SpqarQLResult result = objectMapper.readValue(page, SpqarQLResult.class);

      for (Map<String, Map<String, String>> row : result.getResults()) {
        synonyms.add((String) ((Map<?, ?>) row.get("synonym")).get("value"));
      }
    } catch (Exception e) {
      throw new SourceNotAvailable(e);
    }
    query = (SPARQL_QUERY_PREFIX
        + "SELECT * \n"
        + "FROM <http://id.nlm.nih.gov/mesh>\n"
        + "WHERE {\n"
        + "  mesh:MESH_ID meshv:concept ?concept .\n"
        + " ?concept rdfs:label ?conceptName.\n"
        + "}\n").replace("MESH_ID", meshID.getResource());

    try {
      String page = getWebPageContent(SPARQL_URL + URLEncoder.encode(query, "UTF-8"));
      SpqarQLResult result = objectMapper.readValue(page, SpqarQLResult.class);

      for (Map<String, Map<String, String>> row : result.getResults()) {
        synonyms.add((String) ((Map<?, ?>) row.get("conceptName")).get("value"));
      }
    } catch (Exception e) {
      throw new SourceNotAvailable(e);
    }
    return synonyms;
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    QueryCacheInterface cacheCopy = getCache();
    this.setCache(null);
    ExternalServiceStatus status = new ExternalServiceStatus(MiriamType.MESH_2012.getCommonName(),
        MiriamType.MESH_2012.getDbHomepage());
    try {
      status.setStatus(ExternalServiceStatusType.OK);
      MiriamData meshId = new MiriamData(MiriamType.MESH_2012, "D010300");
      MeSH mesh = getMeSH(meshId);
      if (mesh == null || !meshId.getResource().equals(mesh.getMeSHId())) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (final Exception e) {
      logger.error(e, e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  /**
   * Checks if the mesh identifier is valid.
   *
   * @param meshId mesh id
   * @return <code>true</code> if it's valid
   * @throws AnnotatorException thrown when there is problem with accessing mesh db
   */
  @Override
  public boolean isValidMeshId(final MiriamData meshId) {
    if (meshId == null) {
      return false;
    }
    return getMeSH(meshId) != null;
  }
}
