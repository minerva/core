package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.cache.QueryCacheInterface;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.model.user.annotator.AnnotatorInputParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;
import lcsb.mapviewer.model.user.annotator.BioEntityField;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This is a class that implements a backend to uniprot restfull API.
 *
 * @author Piotr Gawron
 */
@Service
public class UniprotAnnotatorImpl extends ElementAnnotator implements UniprotAnnotator {

  /**
   * Pattern used for finding hgnc symbol from uniprot info page .
   */
  private final Pattern uniprotToHgnc = Pattern.compile("GN[\\ ]+Name=([^;\\ ]+)");

  /**
   * Pattern used for finding entrez identifier from uniprot info page .
   */
  private final Pattern uniprotToEntrez = Pattern.compile("DR[\\ ]+GeneID;\\ ([^;\\ ]+)");

  /**
   * Pattern used for finding EC symbol from UniProt info page .
   */
  private final Pattern uniprotToEC = Pattern
      .compile("EC=((\\d+\\.-\\.-\\.-)|(\\d+\\.\\d+\\.-\\.-)|(\\d+\\.\\d+\\.\\d+\\.-)|(\\d+\\.\\d+\\.\\d+\\.\\d+))");

  public UniprotAnnotatorImpl() {
    super(UniprotAnnotator.class, new Class[]{Protein.class, Gene.class, Rna.class}, false);
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    QueryCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      MiriamData md = uniProtToHgnc(getExampleValidAnnotation());

      status.setStatus(ExternalServiceStatusType.OK);
      if (md == null) {
        status.setStatus(ExternalServiceStatusType.DOWN);
      } else if (!md.getResource().equalsIgnoreCase("LRRK2")) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (final Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  @Override
  public boolean annotateElement(final BioEntityProxy object, final MiriamData identifier, final AnnotatorData parameters)
      throws AnnotatorException {
    if (identifier.getDataType().equals(MiriamType.UNIPROT)) {

      String accessUrl = getUniprotUrl(identifier.getResource());
      try {
        String pageContent = getWebPageContent(accessUrl);
        // check if we have data over there
        if (!pageContent.contains("<title>Error</title>")) {
          Set<MiriamData> annotations = new HashSet<>();
          annotations.addAll(parseHgnc(pageContent));
          annotations.addAll(parseEntrez(pageContent));
          annotations.addAll(parseEC(pageContent));
          annotations.add(uniprotToString(identifier));
          object.addMiriamData(MiriamType.UNIPROT, identifier.getResource());
          object.addMiriamData(annotations);
          return true;
        } else {
          // this means that entry with a given uniprot id doesn't exist
          if (object.contains(identifier)) {
            logger.warn(object.getLogMarker(ProjectLogEntryType.INVALID_IDENTIFIER),
                " Invalid uniprot id: " + identifier);
          }
          return false;
        }
      } catch (final WrongResponseCodeIOException exception) {
        logger.warn("Cannot find uniprot data for id: " + identifier);
        return false;
      } catch (final IOException exception) {
        throw new AnnotatorException(exception);
      }
    } else {
      throw new NotImplementedException();
    }
  }

  /**
   * Transform UniProt {@link MiriamData} data to STRING {@link MiriamData}.
   *
   * @param uniprot {@link MiriamData} with UniProt identifier
   * @return {@link MiriamData} with STRING identifier
   * @throws AnnotatorException thrown when there is a problem with accessing external database
   */
  private MiriamData uniprotToString(final MiriamData uniprot) throws AnnotatorException {
    if (uniprot == null) {
      return null;
    }

    if (!MiriamType.UNIPROT.equals(uniprot.getDataType())) {
      throw new InvalidArgumentException(MiriamType.UNIPROT + " expected.");
    }

    return new MiriamData(MiriamType.STRING, uniprot.getResource());
  }

  @Override
  public String getCommonName() {
    return MiriamType.UNIPROT.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.UNIPROT.getDbHomepage();
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Arrays.asList(new AnnotatorInputParameter(MiriamType.UNIPROT),
        new AnnotatorInputParameter(BioEntityField.NAME, MiriamType.UNIPROT));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Arrays.asList(new AnnotatorOutputParameter(MiriamType.HGNC_SYMBOL),
        new AnnotatorOutputParameter(MiriamType.UNIPROT),
        new AnnotatorOutputParameter(MiriamType.EC),
        new AnnotatorOutputParameter(MiriamType.STRING),
        new AnnotatorOutputParameter(MiriamType.ENTREZ));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.UNIPROT, "Q5S007");
  }

  /**
   * Returns url to uniprot restful API about uniprot entry.
   *
   * @param uniprotId uniprot identifier
   * @return url to uniprot restful API about uniprot entry
   */
  private String getUniprotUrl(final String uniprotId) {
    return "https://rest.uniprot.org/uniprotkb/" + uniprotId + ".txt";
  }

  /**
   * Parse uniprot webpage to find information about {@link MiriamType#ENTREZ} and
   * returns them.
   *
   * @param pageContent uniprot info page
   * @return set of entrez identifiers found on the webpage
   */
  private Collection<MiriamData> parseEntrez(final String pageContent) {
    Collection<MiriamData> result = new HashSet<MiriamData>();
    Matcher m = uniprotToEntrez.matcher(pageContent);
    if (m.find()) {
      result.add(new MiriamData(MiriamType.ENTREZ, m.group(1)));
    }
    return result;
  }

  /**
   * Parse uniprot webpage to find information about
   * {@link MiriamType#HGNC_SYMBOL} and returns them.
   *
   * @param pageContent uniprot info page
   * @return set of entrez identifiers found on the webpage
   */
  private Collection<MiriamData> parseHgnc(final String pageContent) {
    Collection<MiriamData> result = new HashSet<>();
    Matcher m = uniprotToHgnc.matcher(pageContent);
    if (m.find()) {
      result.add(new MiriamData(MiriamType.HGNC_SYMBOL, m.group(1)));
    }
    return result;
  }

  /**
   * Parse UniProt webpage to find information about {@link MiriamType#EC}s and
   * returns them.
   *
   * @param pageContent UniProt info page
   * @return EC found on the page
   */
  private Collection<MiriamData> parseEC(final String pageContent) {
    Collection<MiriamData> result = new HashSet<>();
    Matcher m = uniprotToEC.matcher(pageContent);
    while (m.find()) {
      result.add(new MiriamData(MiriamType.EC, m.group(1)));
    }
    return result;
  }

  /**
   * Transform uniprot identifier into hgnc name.
   *
   * @param uniprot {@link MiriamData} with uniprot identifier
   * @return {@link MiriamData} with hgnc name
   * @throws UniprotSearchException thrown when there is a problem with accessing external database
   */
  @Override
  public MiriamData uniProtToHgnc(final MiriamData uniprot) throws UniprotSearchException {
    if (uniprot == null) {
      return null;
    }

    if (!MiriamType.UNIPROT.equals(uniprot.getDataType())) {
      throw new InvalidArgumentException(MiriamType.UNIPROT + " expected.");
    }

    String accessUrl = getUniprotUrl(uniprot.getResource());
    try {
      String pageContent = getWebPageContent(accessUrl);
      Collection<MiriamData> collection = parseHgnc(pageContent);
      if (collection.size() > 0) {
        return collection.iterator().next();
      } else {
        return null;
      }
    } catch (WrongResponseCodeIOException e) {
      if (e.getResponseCode() == HttpStatus.NOT_FOUND.value() || e.getResponseCode() == HttpStatus.BAD_REQUEST.value()) {
        logger.warn("Unknown uniprot id: " + uniprot.getResource());
        return null;
      } else {
        throw new UniprotSearchException("Problem with accessing uniprot webpage", e);
      }
    } catch (IOException e) {
      throw new UniprotSearchException("Problem with accessing uniprot webpage", e);
    }

  }

  /**
   * Transform uniprot identifier into EC identifiers.
   *
   * @param uniprot {@link MiriamData} with uniprot identifier
   * @return ArrayList of {@link MiriamData} with EC codes
   * @throws UniprotSearchException thrown when there is a problem with accessing external database
   */
  @Override
  public Collection<MiriamData> uniProtToEC(final MiriamData uniprot) throws UniprotSearchException {
    if (uniprot == null) {
      return new HashSet<>();
    }

    if (!MiriamType.UNIPROT.equals(uniprot.getDataType())) {
      throw new InvalidArgumentException(MiriamType.UNIPROT + " expected.");
    }

    String accessUrl = getUniprotUrl(uniprot.getResource());
    try {
      String pageContent = getWebPageContent(accessUrl);
      return parseEC(pageContent);
    } catch (final WrongResponseCodeIOException e) {
      return new HashSet<>();
    } catch (final IOException e) {
      throw new UniprotSearchException("Problem with accessing uniprot webpage", e);
    }

  }

}
