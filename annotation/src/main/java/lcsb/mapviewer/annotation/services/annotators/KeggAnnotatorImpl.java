package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.cache.QueryCacheInterface;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.user.AnnotatorParamDefinition;
import lcsb.mapviewer.model.user.annotator.AnnotatorConfigParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.model.user.annotator.AnnotatorInputParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This is a class that implements KEGG annotator which extract from KEGG PUBMED
 * records and homologous information about homologous genes in different
 * organisms based on parameterization of the annotator.
 *
 * @author David Hoksza
 */
@Service
public class KeggAnnotatorImpl extends ElementAnnotator implements KeggAnnotator {

  /**
   * Pattern used for finding PUBMED IDs in KEGG page.
   */
  private final Pattern pubmedMatcher = Pattern.compile("\\[PMID:(\\d+)\\]");

  /**
   * Pattern used for finding ATH orghologs in KEGG GENE sectionpage.
   */
  private final Pattern athOrthologMatcher = Pattern.compile(" *ATH: (.*)");

  /**
   * Service used for annotation of entities using {@link MiriamType#TAIR_LOCUS
   * TAIR}.
   */
  private final TairAnnotator tairAnnotator;

  /**
   * Service used for retrieving EC numbers based on {@link MiriamType#UNIPROT}
   */
  private final UniprotAnnotator uniprotAnnotator;

  /**
   * Constructor.
   */
  public KeggAnnotatorImpl(final TairAnnotator tairAnnotator, final UniprotAnnotator uniprotAnnotator) {
    super(KeggAnnotator.class, new Class[]{Protein.class, Gene.class, Rna.class}, false);
    this.addParameterDefinition(AnnotatorParamDefinition.KEGG_ORGANISM_IDENTIFIER);
    this.tairAnnotator = tairAnnotator;
    this.uniprotAnnotator = uniprotAnnotator;
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    QueryCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      Species protein = new GenericProtein("id");
      protein.addMiriamData(getExampleValidAnnotation());
      annotateElement(protein);

      status.setStatus(ExternalServiceStatusType.OK);

      Set<String> pmids = new HashSet<String>();
      pmids.add("30409");
      pmids.add("3134");
      int cntMatches = 0;
      for (final MiriamData md : protein.getMiriamData()) {
        if (pmids.contains(md.getResource())) {
          cntMatches++;
        }
      }

      if (cntMatches != 2) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (final Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  @Override
  public boolean annotateElement(final BioEntityProxy object, final MiriamData identifier, final AnnotatorData parameters)
      throws AnnotatorException {
    Set<MiriamData> ecIdentifiers = new HashSet<>();
    try {
      if (identifier.getDataType().equals(MiriamType.TAIR_LOCUS)) {
        for (final MiriamData uniprotId : tairAnnotator.tairToUniprot(identifier)) {
          ecIdentifiers.addAll(uniprotAnnotator.uniProtToEC(uniprotId));
        }
      } else if (identifier.getDataType().equals(MiriamType.UNIPROT)) {
        ecIdentifiers.addAll(uniprotAnnotator.uniProtToEC(identifier));
      } else if (identifier.getDataType().equals(MiriamType.EC)) {
        ecIdentifiers.add(identifier);
      } else {
        throw new NotImplementedException();
      }
    } catch (final UniprotSearchException e) {
      logger.warn(e, e);
      return false;
    }
    if (ecIdentifiers.size() == 0) {
      logger.warn("Cannot find kegg data for id: " + identifier);
      return false;
    }

    // annotate from KEGG
    Set<MiriamData> annotations = new HashSet<>();
    for (final MiriamData ecIdentifier : ecIdentifiers) {
      String accessUrl = getKeggUrl(ecIdentifier.getResource());

      try {
        String pageContent = getWebPageContent(accessUrl);
        annotations.addAll(parseKegg(pageContent, parameters));

      } catch (final WrongResponseCodeIOException exception) {
        logger.warn("Cannot find kegg data for id: " + identifier);
      } catch (final IOException exception) {
        throw new AnnotatorException(exception);
      } catch (final UniprotSearchException e) {
        logger.warn(e, e);
        return false;
      }
    }
    object.addMiriamData(annotations);
    return annotations.size() > 0;
  }

  @Override
  public String getCommonName() {
    return "KEGG";
  }

  @Override
  public String getUrl() {
    return "https://www.genome.jp/kegg/";
  }

  @Override
  public String getDescription() {
    return "Annotations extracted from KEGG ENZYME Database based on species EC numbers. "
        + "Annotation include relevant publications and homologous genes for given EC numbers.";
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Arrays.asList(new AnnotatorInputParameter(MiriamType.TAIR_LOCUS),
        new AnnotatorInputParameter(MiriamType.UNIPROT),
        new AnnotatorInputParameter(MiriamType.EC));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Arrays.asList(new AnnotatorOutputParameter(MiriamType.PUBMED),
        new AnnotatorOutputParameter(MiriamType.TAIR_LOCUS));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.EC, "3.1.2.14");
  }

  @Override
  public List<AnnotatorConfigParameter> getExampleValidParameters() {
    return Collections.singletonList(new AnnotatorConfigParameter(AnnotatorParamDefinition.KEGG_ORGANISM_IDENTIFIER, "ATH"));
  }

  /**
   * Returns url to KEGG restful API about enzyme classification.
   *
   * @param ecId enzyme classification
   * @return url to KEGG restful API about given EC
   */
  private String getKeggUrl(final String ecId) {
    return "https://rest.kegg.jp/get/" + ecId;
  }

  /**
   * Parse KEGG webpage to find information about {@link MiriamType#PUBMED}s and
   * returns them.
   *
   * @param pageContent Kegg page
   * @param params      List of {@link UserAnnotatorsParam} to be used for parameterization.
   *                    Should contain only at most one record with space separated KEGG
   *                    organisms names. If the value has not been set by the user, null
   *                    will be passed.
   * @return {@link MiriamType#PUBMED}s found on the page
   */
  private Collection<MiriamData> parseKegg(final String pageContent, final AnnotatorData params) throws UniprotSearchException {

    // Retrieve Pubmeds
    Collection<MiriamData> result = new HashSet<>();
    Matcher m = pubmedMatcher.matcher(pageContent);
    while (m.find()) {
      result.add(new MiriamData(MiriamType.PUBMED, m.group(1)));
    }

    String organismParameter = params.getValue(AnnotatorParamDefinition.KEGG_ORGANISM_IDENTIFIER);
    // Retrieve homologous organisms based on parameterization
    if (organismParameter != null) {
      String[] keggOrgnismCodes = organismParameter.trim().split(" +");
      for (final String code : keggOrgnismCodes) {
        if (!code.equalsIgnoreCase("ATH")) {
          logger.warn("KEGG annotator currently supports only ATH (" + code + " passed)");
        } else {
          m = athOrthologMatcher.matcher(pageContent);
          if (m.find()) {
            String[] tairLocusNames = m.group(1).trim().split(" ");
            for (String tairLocusName : tairLocusNames) {
              // some codes are in the form AT1G08510(FATB)
              tairLocusName = tairLocusName.split("\\(")[0];
              try {
                String name = tairAnnotator.tairLocusNameToId(tairLocusName);
                if (name != null) {
                  result.add(new MiriamData(MiriamType.TAIR_LOCUS, name));
                }
              } catch (AnnotatorException e) {
                throw new UniprotSearchException(e);
              }
            }
          }
        }
      }
    }

    return result;
  }
}
