package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.cache.QueryCacheInterface;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.model.user.annotator.AnnotatorInputParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
public class BrendaAnnotatorImpl extends ElementAnnotator implements BrendaAnnotator {

  private final TairAnnotator tairAnnotator;

  private final UniprotAnnotator uniprotAnnotator;

  @Autowired
  public BrendaAnnotatorImpl(final TairAnnotator tairAnnotator, final UniprotAnnotator uniprotAnnotator) {
    super(BrendaAnnotator.class, new Class[]{Protein.class, Gene.class, Rna.class}, false);
    this.tairAnnotator = tairAnnotator;
    this.uniprotAnnotator = uniprotAnnotator;
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    QueryCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      Collection<MiriamData> mds = uniprotAnnotator.uniProtToEC(getExampleValidAnnotation());

      status.setStatus(ExternalServiceStatusType.OK);
      List<String> ecs = new ArrayList<>();
      if (mds != null) {
        for (final MiriamData md : mds) {
          ecs.add(md.getResource());
        }
      }
      if (mds == null || mds.size() != 2 || !ecs.contains("2.6.1.1") || !ecs.contains("2.6.1.7")) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (final Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  @Override
  public boolean annotateElement(final BioEntityProxy object, final MiriamData identifier, final AnnotatorData parameters)
      throws AnnotatorException {
    List<MiriamData> mdUniprots = new ArrayList<>();
    if (identifier.getDataType().equals(MiriamType.TAIR_LOCUS)) {
      mdUniprots.addAll(tairAnnotator.tairToUniprot(identifier));
    } else if (identifier.getDataType().equals(MiriamType.UNIPROT)) {
      mdUniprots.add(identifier);
    } else {
      throw new NotImplementedException();
    }

    List<String> ecIds = new ArrayList<>();
    for (final MiriamData mdUniprot : mdUniprots) {
      try {
        Collection<MiriamData> mdECs = uniprotAnnotator.uniProtToEC(mdUniprot);
        for (final MiriamData mdEC : mdECs) {
          if (!ecIds.contains(mdEC.getResource())) {
            ecIds.add(mdEC.getResource());
            object.addMiriamData(MiriamType.BRENDA, mdEC.getResource());
          }
        }
      } catch (final UniprotSearchException e) {
        logger.warn(object.getLogMarker(ProjectLogEntryType.CANNOT_FIND_INFORMATION),
            "Cannot find EC data for UniProt id: " + mdUniprot.getResource());
      }

    }
    return ecIds.size() > 0;
  }

  @Override
  public String getCommonName() {
    return MiriamType.BRENDA.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.BRENDA.getDbHomepage();
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Arrays.asList(new AnnotatorInputParameter(MiriamType.TAIR_LOCUS),
        new AnnotatorInputParameter(MiriamType.UNIPROT));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Collections.singletonList(new AnnotatorOutputParameter(MiriamType.BRENDA));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.UNIPROT, "P12345");
  }

}
