package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.cache.BigFileCache;
import lcsb.mapviewer.annotation.cache.QueryCacheInterface;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.model.user.annotator.AnnotatorInputParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This is a class that implements a backend to TAIR. Note that TAIR annotation
 * process will annotate only records which have a TAIR ID assigned by a human
 * annotator. Otherwise, it would generate UniProt miriam records also for TAIR
 * IDs generated from, e.g., final KEGG annotator, i.e. for homologues and these
 * UniProt IDs would be indistinguishable from the UniProt IDs describing the
 * molecule.
 *
 * @author David Hoksza
 */
@Service
public class TairAnnotatorImpl extends ElementAnnotator implements TairAnnotator {

  private static final String URL_SOURCE_DATABASE = "https://minerva-dev.lcsb.uni.lu/TAIR2UniprotMapping-Jul2023.txt";

  private static final int BUFFER_SIZE = 1024;

  private ByteArrayOutputStream sourceInputStream;

  private final BigFileCache bigFileCache;

  @Autowired
  public TairAnnotatorImpl(final BigFileCache bigFileCache) {
    super(TairAnnotator.class, new Class[]{Protein.class, Gene.class, Rna.class}, false);
    this.bigFileCache = bigFileCache;
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    final ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    final QueryCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      final Collection<MiriamData> collection = tairToUniprot(getExampleValidAnnotation());

      status.setStatus(ExternalServiceStatusType.OK);
      if (collection.size() == 0) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else {
        boolean contains = false;
        for (final MiriamData miriamData : collection) {
          if (miriamData.getResource().equalsIgnoreCase("Q9MAN1")) {
            contains = true;
            break;
          }
        }
        if (!contains) {
          status.setStatus(ExternalServiceStatusType.CHANGED);
        }
      }
    } catch (final Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  @Override
  public boolean annotateElement(final BioEntityProxy object, final MiriamData identifier, final AnnotatorData parameters)
      throws AnnotatorException {

    if (identifier.getAnnotator() != null) {
      return false;
    }
    if (identifier.getDataType().equals(MiriamType.TAIR_LOCUS)) {
      // UniProt are only obained from TAIR's which were provided by the
      // annotator
      // (otherwise we would get
      // also UniProt IDs for, e.g., homologous genes' TAIR IDs obtained from
      // KEGG
      final Collection<MiriamData> collection = tairToUniprot(identifier);
      if (collection.size() > 0) {
        object.addMiriamData(collection);
        return true;
      } else {
        logger.warn("Cannot find uniprot data for id: " + identifier.getResource() + " in the tair page");
        return false;
      }
    } else {
      throw new NotImplementedException();
    }
  }

  @Override
  public String getCommonName() {
    return "TAIR";
  }

  @Override
  public String getUrl() {
    return MiriamType.TAIR_LOCUS.getDbHomepage();
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Collections.singletonList(new AnnotatorInputParameter(MiriamType.TAIR_LOCUS));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Collections.singletonList(new AnnotatorOutputParameter(MiriamType.UNIPROT));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.TAIR_LOCUS, "2200950");
  }

  /**
   * Transform TAIR identifier into uniprot identifier.
   * <p>
   * Used to use the TAIR record page, but that tends to change and moreover TAIR
   * limits number of accesses from an address. So now the transformation queries
   * directly UniProt from which the mapping can be obtained as well.
   * </p>
   *
   * @param tair {@link MiriamData} with TAIR identifier
   * @return {@link MiriamData} with UniProt identifier
   * @throws AnnotatorException thrown when there is a problem with accessing external database
   */
  @Override
  public Collection<MiriamData> tairToUniprot(final MiriamData tair) throws AnnotatorException {
    if (tair == null) {
      return null;
    }

    if (!MiriamType.TAIR_LOCUS.equals(tair.getDataType())) {
      throw new InvalidArgumentException(MiriamType.TAIR_LOCUS + " expected.");
    }

    return tairLocusIdToUniprot(tair.getResource());
  }

  @Override
  public String tairLocusIdToTairName(final String string) throws AnnotatorException {
    try {
      final BufferedReader reader = new BufferedReader(new InputStreamReader(getSourceInputStream()));
      while (reader.ready()) {
        final String line = reader.readLine();
        final String[] tmp = line.split("\t");
        if (tmp.length > 2) {
          if (tmp[1].equalsIgnoreCase("locus:" + string)) {
            return tmp[2];
          }
        }
      }
      return null;
    } catch (final IOException e) {
      throw new AnnotatorException(e);
    }
  }

  private InputStream getSourceInputStream() throws IOException {
    if (this.sourceInputStream == null) {

      if (!bigFileCache.isCached(URL_SOURCE_DATABASE)) {
        downloadSourceFile();
      }
      final String filename = bigFileCache.getAbsolutePathForFile(URL_SOURCE_DATABASE);
      this.sourceInputStream = downloadedFileToInputStream(filename);

    }
    return new ByteArrayInputStream(sourceInputStream.toByteArray());
  }

  private ByteArrayOutputStream downloadedFileToInputStream(final String filename) throws IOException {
    final ByteArrayOutputStream result = new ByteArrayOutputStream();
    final byte[] buffer = new byte[BUFFER_SIZE];
    int len;
    final InputStream stream = new FileInputStream(new File(filename));
    try {
      while ((len = stream.read(buffer)) > -1) {
        result.write(buffer, 0, len);
      }
    } finally {
      stream.close();
    }
    result.flush();
    return result;
  }

  private void downloadSourceFile() throws IOException {
    try {
      bigFileCache.downloadFile(URL_SOURCE_DATABASE, new IProgressUpdater() {
        @Override
        public void setProgress(final double progress) {
        }
      });

    } catch (final URISyntaxException e) {
      throw new IOException("Problem with downloading file: " + URL_SOURCE_DATABASE, e);
    }
  }

  @Override
  public Collection<MiriamData> tairLocusIdToUniprot(final String string) throws AnnotatorException {
    final Set<MiriamData> result = new HashSet<>();
    try {
      final BufferedReader reader = new BufferedReader(new InputStreamReader(getSourceInputStream()));
      while (reader.ready()) {
        final String line = reader.readLine();
        final String[] tmp = line.split("\t");
        if (tmp.length > 2) {
          if (tmp[1].equalsIgnoreCase("locus:" + string)) {
            result.add(new MiriamData(MiriamType.UNIPROT, tmp[0]));
          }
        }
      }
      return result;
    } catch (final IOException e) {
      throw new AnnotatorException(e);
    }
  }

  @Override
  public String tairLocusNameToId(final String tairLocusId) throws AnnotatorException {
    try {
      final BufferedReader reader = new BufferedReader(new InputStreamReader(getSourceInputStream()));
      while (reader.ready()) {
        final String line = reader.readLine();
        final String[] tmp = line.split("\t");
        if (tmp.length > 2) {
          if (tmp[2].equalsIgnoreCase(tairLocusId)) {
            return tmp[1].replace("locus:", "");
          }
        }
      }
      return null;
    } catch (final IOException e) {
      throw new AnnotatorException(e);
    }
  }

}
