package lcsb.mapviewer.annotation.services.annotators;

import org.springframework.stereotype.Service;

/**
 * This is a class that implements KEGG annotator which extract from KEGG PUBMED
 * records and homologous information about homologous genes in different
 * organisms based on parameterization of the annotator.
 * 
 * @author David Hoksza
 * 
 */
@Service
public interface KeggAnnotator extends IElementAnnotator {

}
