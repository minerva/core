package lcsb.mapviewer.annotation.services;

import lcsb.mapviewer.annotation.cache.Cacheable;
import lcsb.mapviewer.annotation.data.MeSH;
import lcsb.mapviewer.model.map.MiriamData;

public interface MeSHParser extends IExternalService, Cacheable {

  MeSH getMeSH(MiriamData meshID);

  boolean isValidMeshId(MiriamData meshID);

  Object getIdentifier(MiriamData meshID);
}
