package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.map.MiriamData;

/**
 * This is a class that implements a backend to CAZy.
 * 
 * @author David Hoksza
 * 
 */
public interface CazyAnnotator extends IElementAnnotator {

  MiriamData uniprotToCazy(final MiriamData uniprot, final LogMarker marker) throws AnnotatorException;

}
