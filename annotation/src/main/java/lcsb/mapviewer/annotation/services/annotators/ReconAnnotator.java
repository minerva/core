package lcsb.mapviewer.annotation.services.annotators;

/**
 * This is a class that implements a backend to RECON database.
 * 
 * @author Piotr Gawron
 * 
 */
public interface ReconAnnotator extends IElementAnnotator {

}
