package lcsb.mapviewer.annotation.services.dapi.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ListChemicalEntityDto {

  private List<ChemicalEntityDto> content = new ArrayList<>();

  public List<ChemicalEntityDto> getContent() {
    return content;
  }

  public void setContent(final List<ChemicalEntityDto> content) {
    this.content = content;
  }

  @Override
  public String toString() {
    return "ListChemicalEntityDto [content=" + content + "]";
  }

  public void exclude(final List<String> idsToExclude) {
    content =
        content.stream()
            .filter(chemicalEntityDto -> !idsToExclude.contains(chemicalEntityDto.getSourceIdentifier()))
            .collect(Collectors.toList());
  }
}
