package lcsb.mapviewer.annotation.services.annotators;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.annotation.cache.QueryCacheInterface;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.field.Structure;
import lcsb.mapviewer.model.map.species.field.UniprotRecord;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.model.user.annotator.AnnotatorInputParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This is a class that implements a backend to the EBI's PDB SIFTS REST API
 * mapping.
 *
 * @author David Hoksza
 */
@Service
public class PdbAnnotatorImpl extends ElementAnnotator implements PdbAnnotator {

  @Autowired
  public PdbAnnotatorImpl() {
    super(PdbAnnotator.class, new Class[]{Protein.class, Rna.class, Gene.class}, false);
  }

  /**
   * Tests if given input string is a valid JSON document.
   *
   * @param json Input document as a string.
   * @return True or false dependent on whether the input string is a valid JSON document
   */
  private boolean isJson(final String json) {
    if (json == null) {
      return false;
    }
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      objectMapper.enable(DeserializationFeature.FAIL_ON_READING_DUP_TREE_KEY);
      objectMapper.readTree(json);

      return true;
    } catch (IOException ex) {
      return false;
    }
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    QueryCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      Collection<Structure> structures = uniProtToPdb(getExampleValidAnnotation());

      if (structures.size() > 0) {
        status.setStatus(ExternalServiceStatusType.OK);
      } else {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (final Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  @Override
  public boolean annotateElement(final BioEntityProxy bioEntity, final MiriamData identifier, final AnnotatorData parameters)
      throws AnnotatorException {
    if (identifier.getDataType().equals(MiriamType.UNIPROT)) {
      try {
        Collection<Structure> structures = uniProtToPdb(identifier);
        if (structures.size() == 0) {
          logger.warn(bioEntity.getLogMarker(ProjectLogEntryType.OTHER),
              " No PDB mapping for UniProt ID: " + identifier.getResource());
          return false;
        } else {
          // add the annotations to the set of annotation irrespective on
          // which uniprot record the structures belong to (since one molecule
          // can have multiple uniprot records associated, but annotations do
          // do not have the concept of hierarchy or complex data types)
          Set<MiriamData> annotations = new HashSet<MiriamData>();
          for (final Structure s : structures) {
            annotations.add(new MiriamData(MiriamType.PDB, s.getPdbId()));
          }
          bioEntity.addMiriamData(annotations);

          // insert the full information directly into species, .i.e.
          // create new uniprot record which includes the mapped structures
          // and add it to the species (bioentity)
          UniprotRecord ur = new UniprotRecord();
          ur.setUniprotId(identifier.getResource());
          for (final Structure s : structures) {
            s.setUniprot(ur);
          }
          ur.addStructures(structures);
          bioEntity.addUniprot(ur);
          return true;
        }
      } catch (final WrongResponseCodeIOException exception) {
        logger.warn("Not found PDB mapping (wrong response code) for UniProt ID: " + identifier.getResource());
        return false;
      } catch (final IOException exception) {
        throw new AnnotatorException(exception);
      }
    } else {
      throw new NotImplementedException();
    }
  }

  @Override
  public String getCommonName() {
    return MiriamType.PDB.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.PDB.getDbHomepage();
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Collections.singletonList(new AnnotatorInputParameter(MiriamType.UNIPROT));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Collections.singletonList(new AnnotatorOutputParameter(MiriamType.PDB));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.UNIPROT, "P29373");
  }

  /**
   * Returns url to JSON with best mapping PDB entries given the UniProt entry.
   *
   * @param uniprotId uniprot identifier
   * @return url with best mapping PDB entries to the UniProt entry
   */
  private String getPdbMappingUrl(final String uniprotId) {
    return "https://www.ebi.ac.uk/pdbe/api/mappings/best_structures/" + uniprotId;
  }

  /**
   * Parse UniProt-to-PDB mapping JSON file. {@link MiriamType#PDB} and returns
   * them.
   *
   * @param pageContentJson JSON file with the UniProt to PDB mapping
   * @return set of PDB identifiers found on the webpage
   */
  private Collection<Structure> processMappingData(final String pageContentJson) {
    Collection<Structure> result = new HashSet<>();
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      Map<String, List<PdbBestMappingEntry>> m = objectMapper.readValue(pageContentJson, new TypeReference<Map<String, List<PdbBestMappingEntry>>>() {
      });
      if (m != null) {
        for (final String key : m.keySet()) {
          for (final PdbBestMappingEntry e : m.get(key)) {
            if (e != null) {
              result.add(e.convertToStructure());
              // result.add(new MiriamData(MiriamType.PDB, e.pdb_id));
            }
          }
        }
      }
    } catch (Exception e1) {
      logger.error(e1, e1);
    }

    return result;
  }

  /**
   * Transform UniProt identifier into PDB IDs.
   *
   * @param uniprot {@link MiriamData} with UniProt identifier
   * @return JSON String with mapping. thrown when there is a problem with accessing external database
   */
  private Collection<Structure> uniProtToPdb(final MiriamData uniprot) throws IOException {
    if (uniprot == null) {
      return new ArrayList<>();
    }

    if (!MiriamType.UNIPROT.equals(uniprot.getDataType())) {
      throw new InvalidArgumentException(MiriamType.UNIPROT + " expected.");
    }

    String accessUrl = getPdbMappingUrl(uniprot.getResource());
    String json = getWebPageContent(accessUrl);

    return isJson(json) ? processMappingData(json) : new ArrayList<>();
  }
}
