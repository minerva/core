package lcsb.mapviewer.annotation.services.annotators;

/**
 * This class is responsible for connecting to
 * <a href="https://rest.ensembl.org">Ensembl API</a> and annotate elements with
 * information taken from this service.
 * 
 * 
 * @author Piotr Gawron
 * 
 */
public interface EnsemblAnnotator extends IElementAnnotator {
}
