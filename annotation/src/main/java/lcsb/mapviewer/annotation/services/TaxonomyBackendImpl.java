package lcsb.mapviewer.annotation.services;

import lcsb.mapviewer.annotation.cache.BigFileCache;
import lcsb.mapviewer.annotation.cache.CachableInterface;
import lcsb.mapviewer.annotation.cache.QueryCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * This class is a backend to Taxonomy.
 *
 * @author Piotr Gawron
 */
@Service
public class TaxonomyBackendImpl extends CachableInterface implements TaxonomyBackend {

  /**
   * Prefix string used for marking the query in database as query by taxonomy
   * term.
   */
  static final String TAXONOMY_CACHE_PREFIX = "TAXONOMY_TERM: ";

  /**
   * Prefix string used for marking queries in database as queries returning
   * name by taxonomy id.
   */
  static final String TAXONOMY_NAME_CACHE_PREFIX = "TAXONOMY_NAME_BY_ID: ";

  private static final String URL_SOURCE_DATABASE = "https://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdmp.zip";

  private static final int BUFFER_SIZE = 1024;

  private final Logger logger = LogManager.getLogger();

  private ByteArrayOutputStream sourceInputStream;

  private final BigFileCache bigFileCache;


  public TaxonomyBackendImpl(final BigFileCache bigFileCache) {
    super(TaxonomyBackend.class);
    this.bigFileCache = bigFileCache;
  }

  @Override
  public String refreshCacheQuery(final Object query) throws SourceNotAvailable {
    String result = null;
    try {
      if (query instanceof String) {
        String name = (String) query;
        if (name.startsWith(TAXONOMY_CACHE_PREFIX)) {
          String term = name.substring(TAXONOMY_CACHE_PREFIX.length());
          MiriamData md = getByName(term);
          if (md != null) {
            result = md.getResource();
          }
        } else if (name.startsWith(TAXONOMY_NAME_CACHE_PREFIX)) {
          String id = name.substring(TAXONOMY_NAME_CACHE_PREFIX.length());
          result = getNameForTaxonomy(new MiriamData(MiriamType.TAXONOMY, id));
        } else if (name.startsWith("http")) {
          result = getWebPageContent(name);
        } else {
          throw new InvalidArgumentException("Don't know what to do with query: " + query);
        }
      } else {
        throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
      }
    } catch (final TaxonomySearchException | IOException e) {
      throw new SourceNotAvailable(e);
    }
    return result;
  }

  @Override
  public MiriamData getByName(final String term) throws TaxonomySearchException {
    String queryTerm = term;
    if (queryTerm == null || queryTerm.trim().isEmpty()) {
      return null;
    }
    queryTerm = prepareTerm(queryTerm);
    String res = getCacheValue(TAXONOMY_CACHE_PREFIX + queryTerm);
    if (res != null) {
      if (res.equals("null")) {
        return null;
      } else {
        return new MiriamData(MiriamType.TAXONOMY, res);
      }
    }
    try {
      ZipInputStream zipStream = new ZipInputStream(getSourceInputStream());
      ZipEntry zipEntry = zipStream.getNextEntry();
      while (zipEntry != null) {
        String filename = zipEntry.getName();
        if (filename.equalsIgnoreCase("names.dmp")) {
          BufferedReader reader = new BufferedReader(new InputStreamReader(zipStream));
          while (reader.ready()) {
            String line = reader.readLine();
            String[] tags = line.split("\\|");
            if (tags.length < 4) {
              continue;
            }
            if (tags[1].trim().equalsIgnoreCase(queryTerm)) {
              res = tags[0].trim();
            }
          }
        }
        zipEntry = zipStream.getNextEntry();
      }
      zipStream.closeEntry();
      zipStream.close();

      if (res == null) {
        logger.warn("Unknown organism: {}", queryTerm);
        setCacheValue(TAXONOMY_CACHE_PREFIX + queryTerm, "null");
        return null;
      } else {
        setCacheValue(TAXONOMY_CACHE_PREFIX + queryTerm, res);
        return new MiriamData(MiriamType.TAXONOMY, res);
      }
    } catch (final IOException e) {
      throw new TaxonomySearchException("Problem with accessing taxonomy database", e);
    }

  }

  private static String prepareTerm(final String term) {
    String queryTerm = term.trim();
    if (queryTerm.contains("(")) {
      queryTerm = queryTerm.substring(0, queryTerm.indexOf("(")).trim();
    }
    if (queryTerm.toUpperCase().startsWith("C.")) {
      queryTerm = "Caenorhabditis" + queryTerm.substring(2);
    } else if (queryTerm.toUpperCase().startsWith("D.")) {
      queryTerm = "Drosophila" + queryTerm.substring(2);
    } else if (queryTerm.toUpperCase().startsWith("P.")) {
      queryTerm = "Pristionchus" + queryTerm.substring(2);
    } else if (queryTerm.toUpperCase().startsWith("T.")) {
      queryTerm = "Tribolium" + queryTerm.substring(2);
    } else if (queryTerm.equalsIgnoreCase("Mouse")) {
      queryTerm = "Mus musculus";
    } else if (queryTerm.equalsIgnoreCase("Rat")) {
      queryTerm = "Rattus norvegicus";
    } else if (queryTerm.equalsIgnoreCase("Humans")) {
      queryTerm = "Human";
    }
    return queryTerm;
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(MiriamType.TAXONOMY.getCommonName(),
        MiriamType.TAXONOMY.getDbHomepage());

    QueryCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      MiriamData md = getByName("human");
      if (md == null) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else if (md.equals(TaxonomyBackend.HUMAN_TAXONOMY)) {
        status.setStatus(ExternalServiceStatusType.OK);
      } else {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (final Exception e) {
      logger.error("{} is down", MiriamType.TAXONOMY.getCommonName(), e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  @Override
  public String getNameForTaxonomy(final MiriamData miriamData) throws TaxonomySearchException {
    if (miriamData == null) {
      return null;
    }
    String res = getCacheValue(TAXONOMY_NAME_CACHE_PREFIX + miriamData.getResource());
    if (res != null) {
      return res;
    }

    try {
      ZipInputStream zipStream = new ZipInputStream(getSourceInputStream());
      ZipEntry zipEntry = zipStream.getNextEntry();
      while (zipEntry != null) {
        String filename = zipEntry.getName();
        if (filename.equalsIgnoreCase("names.dmp")) {
          BufferedReader reader = new BufferedReader(new InputStreamReader(zipStream));
          while (reader.ready()) {
            String line = reader.readLine();
            String[] tags = line.split("\\|");
            if (tags.length < 4) {
              continue;
            }
            if (tags[0].trim().equals(miriamData.getResource())) {
              if (res == null) {
                res = tags[1].trim();
              }
              if (tags[3].trim().equalsIgnoreCase("scientific name")) {
                res = tags[1].trim();
              }
            }
          }
        }
        zipEntry = zipStream.getNextEntry();
      }
      zipStream.closeEntry();
      zipStream.close();

      if (res == null) {
        logger.warn("Unknown organism: {}", miriamData);
        return null;
      } else {
        setCacheValue(TAXONOMY_NAME_CACHE_PREFIX + miriamData.getResource(), res);
        return res;
      }
    } catch (final IOException e) {
      throw new TaxonomySearchException("Problem with accessing taxonomy database", e);
    }
  }

  private InputStream getSourceInputStream() throws IOException {
    if (this.sourceInputStream == null) {

      if (!bigFileCache.isCached(URL_SOURCE_DATABASE)) {
        downloadSourceFile();
      }
      final String filename = bigFileCache.getAbsolutePathForFile(URL_SOURCE_DATABASE);
      this.sourceInputStream = downloadedFileToInputStream(filename);

    }
    return new ByteArrayInputStream(sourceInputStream.toByteArray());
  }

  private ByteArrayOutputStream downloadedFileToInputStream(final String filename) throws IOException {
    final ByteArrayOutputStream result = new ByteArrayOutputStream();
    final byte[] buffer = new byte[BUFFER_SIZE];
    int len;
    try (InputStream stream = Files.newInputStream(new File(filename).toPath())) {
      while ((len = stream.read(buffer)) > -1) {
        result.write(buffer, 0, len);
      }
    }
    result.flush();
    return result;
  }

  private void downloadSourceFile() throws IOException {
    logger.debug("Downloading source file...");
    try {
      bigFileCache.downloadFile(URL_SOURCE_DATABASE, progress -> {
      });

    } catch (final URISyntaxException e) {
      throw new IOException("Problem with downloading file: " + URL_SOURCE_DATABASE, e);
    }
  }

}
