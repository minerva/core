package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.cache.QueryCacheInterface;
import lcsb.mapviewer.annotation.data.Chebi;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Chemical;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.model.user.annotator.AnnotatorInputParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;
import lcsb.mapviewer.model.user.annotator.BioEntityField;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Node;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Transactional
public class ChebiAnnotatorImpl extends ElementAnnotator implements ChebiAnnotator {

  public ChebiAnnotatorImpl() {
    super(ChebiAnnotator.class, new Class[]{Chemical.class}, true);
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    QueryCacheInterface cacheCopy = getCache();
    this.setCache(null);
    try {
      MiriamData data = getChebiForChebiName("water");
      status.setStatus(ExternalServiceStatusType.OK);
      logger.debug(data);
      if (data == null) {
        logger.error(status.getName() + " is down");
        status.setStatus(ExternalServiceStatusType.DOWN);
      } else if (!data.getResource().equals("CHEBI:15377")) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (final Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    } finally {
      this.setCache(cacheCopy);
    }
    return status;
  }

  @Override
  public MiriamData getChebiForChebiName(final String inputName) throws ChebiSearchException {
    if (inputName == null) {
      return null;
    }
    // Japanese people use strange dash symbol
    String name = inputName.replace("−", "-").toLowerCase().trim();

    try {

      String urlSearchName = "https://www.ebi.ac.uk/webservices/chebi/2.0/test/getLiteEntity?searchCategory=CHEBI+NAME&maximumResults=200&starsCategory=ALL&search="
          + URLEncoder.encode(name, "UTF-8");
      String nameSearchContent = getWebPageContent(urlSearchName);

      List<MiriamData> ids = getIdsFromSearchContent(nameSearchContent);
      for (MiriamData miriamData : ids) {
        Chebi chebi = getChebiElementForChebiId(miriamData);
        if (chebi.getName().trim().equalsIgnoreCase(name)) {
          return miriamData;
        }
        for (String synonym : chebi.getSynonyms()) {
          if (synonym.trim().equalsIgnoreCase(name)) {
            return miriamData;
          }
        }
        for (String formulae : chebi.getFormulaes()) {
          if (formulae.trim().equalsIgnoreCase(name)) {
            return miriamData;
          }
        }
      }

      String urlSearchAll = "https://www.ebi.ac.uk/webservices/chebi/2.0/test/getLiteEntity?searchCategory=ALL&maximumResults=200&starsCategory=ALL&search="
          + URLEncoder.encode(name, "UTF-8");
      String allSearchContent = getWebPageContent(urlSearchAll);

      ids = getIdsFromSearchContent(allSearchContent);
      for (MiriamData miriamData : ids) {
        Chebi chebi = getChebiElementForChebiId(miriamData);
        if (chebi.getName().equalsIgnoreCase(name)) {
          return miriamData;
        }
        for (String synonym : chebi.getSynonyms()) {
          if (synonym.equalsIgnoreCase(name)) {
            return miriamData;
          }
        }
        for (String formulae : chebi.getFormulaes()) {
          if (formulae.trim().equalsIgnoreCase(name)) {
            return miriamData;
          }
        }
      }

    } catch (final Exception e) {
      throw new ChebiSearchException("Problem with chebi connection", e);
    }
    return null;
  }

  private List<MiriamData> getIdsFromSearchContent(final String content) throws InvalidXmlSchemaException {
    List<MiriamData> result = new ArrayList<>();
    Node root = XmlParser.getXmlDocumentFromString(content);
    Node envelope = XmlParser.getNode("S:Envelope", root);
    Node body = XmlParser.getNode("S:Body", envelope);
    Node getLiteEntityResponse = XmlParser.getNode("getLiteEntityResponse", body);
    Node data = XmlParser.getNode("return", getLiteEntityResponse);
    List<Node> nodes = XmlParser.getNodes("ListElement", data.getChildNodes());
    for (Node node : nodes) {
      Node idNode = XmlParser.getNode("chebiId", node);
      if (idNode != null) {
        result.add(new MiriamData(MiriamType.CHEBI, idNode.getTextContent()));
      }
    }
    return result;
  }

  @Override
  public Chebi getChebiElementForChebiId(final MiriamData md) throws ChebiSearchException {
    if (!MiriamType.CHEBI.equals(md.getDataType())) {
      throw new InvalidArgumentException(MiriamType.CHEBI + " expected");
    }
    String id = md.getResource().toLowerCase().trim();
    if (!id.contains("chebi")) {
      id = "chebi:" + id;
    }
    id = id.toUpperCase();
    try {
      Chebi result = null;

      String url = "https://www.ebi.ac.uk/webservices/chebi/2.0/test/getCompleteEntity?chebiId=" + URLEncoder.encode(id, "UTF-8");
      String content = getWebPageContent(url);
      Node root = XmlParser.getXmlDocumentFromString(content);
      Node envelope = XmlParser.getNode("S:Envelope", root);
      if (envelope == null) {
        return null;
      }
      Node body = XmlParser.getNode("S:Body", envelope);
      if (body == null) {
        return null;
      }
      Node getLiteEntityResponse = XmlParser.getNode("getCompleteEntityResponse", body);
      if (getLiteEntityResponse == null) {
        return null;
      }
      Node data = XmlParser.getNode("return", getLiteEntityResponse);

      result = new Chebi();

      String name = getNodeContent("chebiAsciiName", data);
      String chebiId = getNodeContent("chebiId", data);
      String smiles = getNodeContent("smiles", data);
      String inchi = getNodeContent("inchi", data);
      String inchiKey = getNodeContent("inchiKey", data);

      List<Node> synonymNodes = XmlParser.getNodes("Synonyms", data.getChildNodes());
      for (Node node : synonymNodes) {
        result.addSynonym(getNodeContent("data", node));
      }

      List<Node> formulaeNodes = XmlParser.getNodes("Formulae", data.getChildNodes());
      for (Node node : formulaeNodes) {
        result.addFormulae(getNodeContent("data", node));
      }

      result.setName(name);
      result.setChebiId(chebiId);
      result.setSmiles(smiles);
      result.setInchi(inchi);
      result.setInchiKey(inchiKey);

      return result;
    } catch (final Exception e) {
      throw new ChebiSearchException("Problem with chebi", e);
    }
  }

  private String getNodeContent(final String nodeName, final Node data) {
    Node node = XmlParser.getNode(nodeName, data);
    if (node == null) {
      return null;
    }
    String result = node.getTextContent();
    if (result != null) {
      result = result.trim();
    }
    return result;
  }

  @Override
  public String getChebiNameForChebiId(final MiriamData id) throws ChebiSearchException {
    if (id == null) {
      return null;
    }
    Chebi chebi = getChebiElementForChebiId(id);
    if (chebi == null) {
      return null;
    }
    return chebi.getName();
  }

  @Override
  public boolean annotateElement(final BioEntityProxy element, final MiriamData identifier, final AnnotatorData parameters)
      throws AnnotatorException {
    try {
      Chebi chebi = getChebiElementForChebiId(identifier);
      if (chebi != null) {
        element.setFullName(chebi.getName());

        element.addMiriamData(MiriamType.INCHI, chebi.getInchi());
        element.addMiriamData(MiriamType.INCHIKEY, chebi.getInchiKey());
        element.setSmile(chebi.getSmiles());
        element.setSynonyms(chebi.getSynonyms());

        String inchiKey = chebi.getInchiKey();
        if (inchiKey != null) {
          element.addMiriamData(MiriamType.STITCH, inchiKeyExtractMainLayer(inchiKey));
        }

        return true;
      }
      return false;
    } catch (final ChebiSearchException e) {
      throw new AnnotatorException("Problem with getting information about chebi", e);
    }
  }

  @Override
  public boolean annotateElement(final BioEntityProxy element, final String name, final AnnotatorData parameters)
      throws AnnotatorException {
    try {
      MiriamData chebi = getChebiForChebiName(name);
      if (chebi != null) {
        element.addMiriamData(chebi);
        return annotateElement(element, chebi, parameters);
      } else {
        logger.warn(element.getLogMarker(ProjectLogEntryType.CANNOT_FIND_INFORMATION),
            "Chemical name cannot be found in chebi: " + name);
        return false;
      }
    } catch (final Exception e) {
      throw new AnnotatorException("Problem with getting information about chebi", e);
    }
  }

  /**
   * Returns main layer of the InchKey, * i.e. everything before first hyphen:
   * WBYWAXJHAXSJNI-VOTSOKGWSA-N -> WBYWAXJHAXSJNI
   *
   * @param inchiKey Full inchiKey
   * @return Main layer of the InchiKey
   */
  private String inchiKeyExtractMainLayer(final String inchiKey) {
    return inchiKey.replaceFirst("-.*", "");
  }

  @Override
  public String getCommonName() {
    return MiriamType.CHEBI.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.CHEBI.getDbHomepage();
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Arrays.asList(new AnnotatorInputParameter(MiriamType.CHEBI),
        new AnnotatorInputParameter(BioEntityField.NAME));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Arrays.asList(new AnnotatorOutputParameter(BioEntityField.FULL_NAME),
        new AnnotatorOutputParameter(MiriamType.INCHI),
        new AnnotatorOutputParameter(MiriamType.INCHIKEY),
        new AnnotatorOutputParameter(MiriamType.STITCH),
        new AnnotatorOutputParameter(BioEntityField.SMILE),
        new AnnotatorOutputParameter(BioEntityField.SYNONYMS));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.CHEBI, "CHEBI:15377");
  }

}
