package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.data.EntrezData;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.map.MiriamData;

/**
 * This class is responsible for connecting to
 * <a href="http://eutils.ncbi.nlm.nih.gov/entrez/">Entrez API</a> and annotate
 * elements with information taken from this service.
 * 
 * 
 * @author Piotr Gawron
 * 
 */
public interface EntrezAnnotator extends IElementAnnotator {

  EntrezData getEntrezForMiriamData(final MiriamData entrezMiriamData, final LogMarker marker) throws AnnotatorException;
}
