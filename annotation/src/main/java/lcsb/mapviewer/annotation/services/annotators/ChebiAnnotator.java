package lcsb.mapviewer.annotation.services.annotators;

import org.springframework.stereotype.Service;

import lcsb.mapviewer.annotation.data.Chebi;
import lcsb.mapviewer.model.map.MiriamData;

/**
 * This is a class that implements a backend to publicly available chebi API.
 * 
 * @author Piotr Gawron
 * 
 */
@Service
public interface ChebiAnnotator extends IElementAnnotator {

  Chebi getChebiElementForChebiId(final MiriamData md) throws ChebiSearchException;

  MiriamData getChebiForChebiName(final String inputName) throws ChebiSearchException;

  String getChebiNameForChebiId(final MiriamData id) throws ChebiSearchException;
}
