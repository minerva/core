package lcsb.mapviewer.annotation.services.annotators;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.annotation.cache.QueryCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.cache.XmlSerializer;
import lcsb.mapviewer.annotation.data.Go;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.model.user.annotator.AnnotatorInputParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;
import lcsb.mapviewer.model.user.annotator.BioEntityField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is a backend to Gene Ontology API.
 *
 * @author Piotr Gawron
 */
@Service
public class GoAnnotatorImpl extends ElementAnnotator implements GoAnnotator {

  /**
   * Prefix string used for marking the query in database as query by go term.
   */
  static final String GO_TERM_CACHE_PREFIX = "GO_TERM: ";

  /**
   * Object that allows to serialize {@link Go} elements into xml string and
   * deserialize xml into {@link Go} objects.
   */
  private final XmlSerializer<Go> goSerializer;

  private final ObjectMapper objectMapper = new ObjectMapper();

  /**
   * Constructor. Initializes structures used for transforming {@link Go}
   * from/to xml.
   */
  @Autowired
  public GoAnnotatorImpl() {
    super(GoAnnotator.class, new Class[]{Phenotype.class, Compartment.class, Complex.class}, true);
    goSerializer = new XmlSerializer<>(Go.class);
  }

  @Override
  public String refreshCacheQuery(final Object query) throws SourceNotAvailable {
    String result = null;
    try {
      if (query instanceof String) {
        String name = (String) query;
        if (name.startsWith(GO_TERM_CACHE_PREFIX)) {
          String term = name.substring(GO_TERM_CACHE_PREFIX.length());
          MiriamData md = new MiriamData(MiriamType.GO, term);
          result = goSerializer.objectToString(getGoElement(md));
        } else if (name.startsWith("http")) {
          result = getWebPageContent(name);
        } else {
          throw new InvalidArgumentException("Don't know what to do with query: " + query);
        }
      } else {
        throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
      }
    } catch (final IOException | GoSearchException e) {
      throw new SourceNotAvailable(e);
    }
    return result;
  }

  @Override
  public boolean annotateElement(final BioEntityProxy object, final MiriamData identifier, final AnnotatorData parameters)
      throws AnnotatorException {
    if (identifier.getDataType().equals(MiriamType.GO)) {
      try {
        Go go = getGoElement(identifier);
        if (go != null) {
          object.setFullName(go.getCommonName());
          object.setDescription(go.getDescription());
          return true;
        }
        return false;
      } catch (final GoSearchException e) {
        throw new AnnotatorException(e);
      }
    } else {
      throw new NotImplementedException();
    }

  }

  @Override
  public String getCommonName() {
    return MiriamType.GO.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.GO.getDbHomepage();
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Collections.singletonList(new AnnotatorInputParameter(MiriamType.GO));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Arrays.asList(
        new AnnotatorOutputParameter(BioEntityField.FULL_NAME),
        new AnnotatorOutputParameter(BioEntityField.DESCRIPTION));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.GO, "GO:0046902");
  }

  /**
   * Returns go entry from the Gene Ontology database for the goTerm
   * (identifier).
   *
   * @param md {@link MiriamData} object referencing to entry in GO database
   * @return entry in Gene Ontology database
   * @throws GoSearchException thrown when there is a problem with accessing data in go database
   */
  @Override
  public Go getGoElement(final MiriamData md) throws GoSearchException {
    Go result = goSerializer.xmlToObject(getCacheNode(GO_TERM_CACHE_PREFIX + md.getResource()));

    if (result != null) {
      return result;
    } else {
      result = new Go();
    }

    String accessUrl = "https://www.ebi.ac.uk/QuickGO/services/ontology/go/terms/" + md.getResource();
    try {
      String page = getWebPageContent(accessUrl);

      Map<String, Object> resultMap = objectMapper.readValue(page, new TypeReference<HashMap<String, Object>>() {
      });

      Double hits = Double.valueOf(resultMap.get("numberOfHits") + "");
      if (hits < 1) {
        return null;
      }
      List<?> objects = (List<?>) resultMap.get("results");

      Map<?, ?> object = (Map<?, ?>) objects.get(0);
      result.setGoTerm((String) object.get("id"));
      result.setCommonName((String) object.get("name"));

      Map<?, ?> descr = (Map<?, ?>) object.get("definition");
      if (descr != null) {
        result.setDescription((String) descr.get("text"));
      }

      return result;
    } catch (final Exception e) {
      throw new GoSearchException("Problem with accesing go database", e);
    }
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    QueryCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      Compartment compartment = new Compartment("some_id");
      compartment.addMiriamData(getExampleValidAnnotation());
      annotateElement(compartment);

      if (compartment.getFullName() == null || compartment.getFullName().equals("")) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else if (compartment.getNotes() == null || compartment.getNotes().equals("")) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else {
        status.setStatus(ExternalServiceStatusType.OK);
      }
    } catch (final Exception e) {
      logger.error("GeneOntology is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

}
