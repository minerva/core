package lcsb.mapviewer.annotation.services.dapi.dto;

import java.util.ArrayList;
import java.util.List;

public class ChemicalTargetDto {

  private String name;
  private String sourceIdentifier;
  private String associatedDisease;
  private String organism;
  private List<String> identifiers = new ArrayList<>();
  private List<String> references = new ArrayList<>();

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public List<String> getIdentifiers() {
    return identifiers;
  }

  public void setIdentifiers(final List<String> identifiers) {
    this.identifiers = identifiers;
  }

  public String getSourceIdentifier() {
    return sourceIdentifier;
  }

  public void setSourceIdentifier(final String sourceIdentifier) {
    this.sourceIdentifier = sourceIdentifier;
  }

  public String getOrganism() {
    return organism;
  }

  public void setOrganism(final String organism) {
    this.organism = organism;
  }

  public List<String> getReferences() {
    return references;
  }

  public void setReferences(final List<String> references) {
    this.references = references;
  }

  public String getAssociatedDisease() {
    return associatedDisease;
  }

  public void setAssociatedDisease(final String associatedDisease) {
    this.associatedDisease = associatedDisease;
  }
}
