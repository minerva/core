package lcsb.mapviewer.annotation.services.annotators;

import java.util.List;

import lcsb.mapviewer.model.map.MiriamData;

/**
 * This class is responsible for connecting to
 * <a href="https://rest.genenames.org/">HGNC restfull API</a> and annotate
 * elements with information taken from this service.
 * 
 * 
 * @author Piotr Gawron
 * 
 */
public interface HgncAnnotator extends IElementAnnotator {

  MiriamData hgncIdToHgncName(final MiriamData miriamData) throws AnnotatorException;

  MiriamData hgncToEntrez(final MiriamData md) throws AnnotatorException;

  List<MiriamData> hgncToUniprot(final MiriamData miriamData) throws AnnotatorException;

  boolean isValidHgncMiriam(final MiriamData md) throws AnnotatorException;
}
