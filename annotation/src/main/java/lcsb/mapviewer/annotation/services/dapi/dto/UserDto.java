package lcsb.mapviewer.annotation.services.dapi.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserDto {

  @NotNull
  private String login;

  @NotEmpty
  private String password;

  @Email
  private String email;

  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  private List<AcceptedReleaseDto> acceptedReleaseLicenses = new ArrayList<>();

  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(final String password) {
    this.password = password;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(final String login) {
    this.login = login;
  }

  public List<AcceptedReleaseDto> getAcceptedReleaseLicenses() {
    return acceptedReleaseLicenses;
  }

  public void setAcceptedReleaseLicenses(final List<AcceptedReleaseDto> acceptedReleaseLicenses) {
    this.acceptedReleaseLicenses = acceptedReleaseLicenses;
  }
}
