package lcsb.mapviewer.annotation.services;

import lcsb.mapviewer.annotation.cache.Cacheable;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

/**
 * Class responsible for connection to Miriam DB. It allows to update URI of
 * database being used and retrieve urls for miriam uri.
 * 
 * @author Piotr Gawron
 * 
 */
public interface MiriamConnector extends IExternalService, Cacheable {

  String getUrlString(MiriamData miriamData);

  boolean isValidIdentifier(String string);

  boolean isValidMiriamType(MiriamType mt);

  String miriamDataToUri(MiriamData humanTaxonomy);
}
