package lcsb.mapviewer.annotation.services;

import java.io.IOException;

/**
 * Exception that should be thrown when accessing http url ends up with
 * IOException caused by invalid response code. This exception is for internal
 * use only.
 * 
 * @author Piotr Gawron
 * 
 */
public class WrongResponseCodeIOException extends IOException {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  /**
   * Response code returned by the super {@link IOException}.
   */
  private int responseCode;

  /**
   * Default constructor.
   * 
   * @param exception
   *          super exception
   * @param code
   *          {@link #responseCode}
   */
  public WrongResponseCodeIOException(final IOException exception, final int code) {
    super(exception);
    this.responseCode = code;
  }

  /**
   * @return the responseCode
   * @see #responseCode
   */
  public int getResponseCode() {
    return responseCode;
  }
}
