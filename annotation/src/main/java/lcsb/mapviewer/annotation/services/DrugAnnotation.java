package lcsb.mapviewer.annotation.services;

import lcsb.mapviewer.annotation.cache.Cacheable;
import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;

import java.util.Collection;
import java.util.List;

/**
 * Abstract class with some functionalities used by class accessing drug
 * databases.
 *
 * @author Piotr Gawron
 */
public interface DrugAnnotation extends Cacheable, IExternalService {

  Drug findDrug(final String inputName) throws DrugSearchException;

  List<Drug> getDrugListByTarget(final MiriamData target, final Collection<MiriamData> organisms) throws DrugSearchException;

  List<Drug> getDrugListByTarget(final MiriamData target) throws DrugSearchException;

  List<Drug> getDrugListByTargets(final Collection<MiriamData> targets) throws DrugSearchException;

  List<Drug> getDrugListByTargets(final Collection<MiriamData> targets, final Collection<MiriamData> organisms) throws DrugSearchException;

  List<String> getSuggestedQueryList(final Project project, final MiriamData organism) throws DrugSearchException;

  void setDrugSuggestedQueryList(int projectId, MiriamData organism, List<String> strings);

  List<String> getSuggestedQueryListWithoutCache(Project sourceProject, MiriamData organism)
      throws DrugSearchException;

  void refreshDrugSuggestedQueryList(Integer projectId, MiriamData organism);
}
