package lcsb.mapviewer.annotation.services.dapi;

import lcsb.mapviewer.annotation.cache.Cacheable;
import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;

import java.util.Collection;
import java.util.List;

public interface ChemicalParser extends Cacheable {

  List<Chemical> getChemicalsForDisease(final MiriamData disease) throws ChemicalSearchException;

  List<Chemical> getChemicalListByTargets(final Collection<MiriamData> targets, final MiriamData disease) throws ChemicalSearchException;

  List<Chemical> getChemicalListByTarget(final MiriamData target, final MiriamData disease) throws ChemicalSearchException;

  List<String> getSuggestedQueryList(final Project sourceProject, final MiriamData diseaseMiriam) throws ChemicalSearchException;

  List<Chemical> getChemicalsByName(final MiriamData disease, final String chemicalName) throws ChemicalSearchException;

  void refreshChemicalSuggestedQueryList(Integer projectId, MiriamData organism);

  void setChemicalSuggestedQueryList(Integer projectId, MiriamData organism, List<String> queryList);
}
