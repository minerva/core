package lcsb.mapviewer.annotation.services.annotators;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import lcsb.mapviewer.annotation.cache.Cacheable;
import lcsb.mapviewer.annotation.services.IExternalService;
import lcsb.mapviewer.annotation.services.annotators.ElementAnnotator.BioEntityProxy;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.user.AnnotatorParamDefinition;
import lcsb.mapviewer.model.user.annotator.AnnotatorConfigParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.model.user.annotator.AnnotatorInputParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;

public interface IElementAnnotator extends IExternalService, Cacheable {

  void annotateElement(final BioEntity element) throws AnnotatorException;

  void annotateElement(final BioEntity bioEntity, final AnnotatorData parameters) throws AnnotatorException;

  boolean annotateElement(BioEntityProxy element, MiriamData identifier, AnnotatorData parameters) throws AnnotatorException;

  boolean annotateElement(BioEntityProxy element, String name, AnnotatorData parameters) throws AnnotatorException;

  AnnotatorData createAnnotatorData();

  boolean isAnnotatable(final BioEntity object);

  boolean isAnnotatable(final Class<?> clazz);

  boolean isDefault();

  String getCommonName();

  List<Class<? extends BioEntity>> getValidClasses();

  String getUrl();

  String getDescription();

  Collection<AnnotatorParamDefinition> getParametersDefinitions();

  void setParametersDefinitions(List<AnnotatorParamDefinition> paramDefs);

  void addParameterDefinition(AnnotatorParamDefinition paramDef);

  List<AnnotatorInputParameter> getAvailableInputParameters();

  List<AnnotatorOutputParameter> getAvailableOuputProperties();

  List<Set<Object>> getInputsParameters(BioEntity bioEntity, List<AnnotatorInputParameter> inputParameters);

  MiriamData getExampleValidAnnotation();

  List<AnnotatorConfigParameter> getExampleValidParameters();

  Class<?> getElementAnnotatorInterface();

}
