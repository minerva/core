package lcsb.mapviewer.annotation.services.dapi.dto;

import java.util.ArrayList;
import java.util.List;

public class ListReleaseDto {

  private List<ReleaseDto> content = new ArrayList<>();

  public List<ReleaseDto> getContent() {
    return content;
  }

  public void setContent(final List<ReleaseDto> content) {
    this.content = content;
  }

}
