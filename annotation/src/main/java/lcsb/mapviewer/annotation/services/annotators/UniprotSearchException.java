package lcsb.mapviewer.annotation.services.annotators;

/**
 * Exception that should be thrown when there is a problem with finding
 * information about Uniprot.
 * 
 * @author Piotr Gawron
 *
 */
public class UniprotSearchException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Constructs a new exception with the specified detail message and cause.
   *
   * @param message
   *          the detail message
   * @param cause
   *          the cause (A <tt>null</tt> value is permitted, and indicates that
   *          the cause is nonexistent or unknown.)
   */
  public UniprotSearchException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public UniprotSearchException(final Throwable cause) {
    super(cause);
  }

}
