package lcsb.mapviewer.annotation.services.genome;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.annotation.cache.BigFileCache;
import lcsb.mapviewer.annotation.cache.CachableInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.annotation.services.TaxonomySearchException;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.cache.BigFileEntry;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.ReferenceGenome;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeGeneMapping;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.persist.dao.map.layout.ReferenceGenomeDao;
import lcsb.mapviewer.persist.dao.map.layout.ReferenceGenomeGeneMappingDao;

/**
 * Implementation of reference genome connector accessing
 * {@link ReferenceGenomeType#UCSC} database.
 * 
 * @author Piotr Gawron
 *
 */
@Service
@Transactional
public class UcscReferenceGenomeConnector extends CachableInterface implements ReferenceGenomeConnector {

  /**
   * Prefix string used for marking queries in cache database that identifies list
   * of reference genome versions by organism id.
   */
  static final String FILENAME_BY_ORGANISM_VERSION_PREFIX = "ORGANISM_VERSION_FILE:";

  private Logger logger = LogManager.getLogger();

  @Autowired
  private ReferenceGenomeDao referenceGenomeDao;

  /**
   * Data access object for reference genome mappings that we have in a database.
   */
  @Autowired
  private ReferenceGenomeGeneMappingDao referenceGenomeGeneMappingDao;

  /**
   * Interface for storing/accessing big files on teh server (we have local copy
   * if big files on the server).
   */
  @Autowired
  private BigFileCache bigFileCache;

  /**
   * Service used for executing tasks in separate thread.
   */
  private ExecutorService asyncExecutorService;

  /**
   * Service used for executing tasks immediately.
   */
  private ExecutorService syncExecutorService;

  /**
   * Regex pattern that helps to find out organism names in source file.
   */
  private Pattern organismNamePattern = Pattern.compile("<!--([A-Za-z\\-\\.\\ ]+)Downloads [=]*[\\ ]*-->");

  /**
   * Regex pattern that helps to find out reference genome versions.
   */
  private Pattern organismDataUrlPattern = Pattern.compile("\\/goldenPath\\/([A-Za-z0-9\\-\\.]+)\\/bigZips\\/");

  /**
   * Access point to taxonomy information.
   */
  private TaxonomyBackend taxonomyBackend;

  @Autowired
  private ReferenceGenomeConnector self;

  /**
   * Constructor.
   */
  @Autowired
  public UcscReferenceGenomeConnector(final TaxonomyBackend taxonomyBackend) {
    super(UcscReferenceGenomeConnector.class);
    this.taxonomyBackend = taxonomyBackend;
    asyncExecutorService = Executors.newScheduledThreadPool(10, new ThreadFactory() {
      @Override
      public Thread newThread(final Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        return t;
      }
    });
    syncExecutorService = Executors.newScheduledThreadPool(1, new ThreadFactory() {
      @Override
      public Thread newThread(final Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        return t;
      }
    });

    // put in the queue empty task to make sure that everything was initialized
    // (additional managing thread was createed)
    asyncExecutorService.submit(new Callable<Object>() {
      @Override
      public Object call() throws Exception {
        return null;
      }
    });
    syncExecutorService.submit(new Callable<Object>() {
      @Override
      public Object call() throws Exception {
        return null;
      }
    });
  }

  @Override
  public List<String> getDownloadedGenomeVersions(final MiriamData organism) {
    List<String> results = new ArrayList<>();

    List<ReferenceGenome> genomes = self.getByType(ReferenceGenomeType.UCSC);
    for (final ReferenceGenome referenceGenome : genomes) {
      if (referenceGenome.getOrganism().equals(organism)) {
        results.add(referenceGenome.getVersion());
      }
    }
    return results;
  }

  @Override
  public List<String> getAvailableGenomeVersion(final MiriamData organism) throws ReferenceGenomeConnectorException {
    Set<String> ids = new HashSet<>();
    try {
      String content = getWebPageContent("https://hgdownload.soe.ucsc.edu/downloads.html");
      Integer start = null;
      Integer end = content.length();
      Matcher matcher = organismNamePattern.matcher(content);
      while (matcher.find()) {
        String name = matcher.group(1).trim();
        if (start != null) {
          end = matcher.start();
          break;
        }

        if (name.equalsIgnoreCase("Shared Data")) {
          continue;
        }
        if (name.equalsIgnoreCase("liftOver File")) {
          continue;
        }
        MiriamData taxonomy = taxonomyBackend.getByName(name);
        if (organism.equals(taxonomy)) {
          start = matcher.end();
        }
      }
      // we haven't found a start point for our organism (organism couldn't be
      // found in the list of available organisms)
      if (start != null) {
        String organismContent = content.substring(start, end);

        matcher = organismDataUrlPattern.matcher(organismContent);
        while (matcher.find()) {
          String name = matcher.group(1).trim();
          ids.add(name);
        }
      }

    } catch (final IOException | TaxonomySearchException e) {
      throw new ReferenceGenomeConnectorException("Problem with accessing UCSC database", e);
    }
    List<String> result = new ArrayList<>();
    result.addAll(ids);
    Collections.sort(result, new Comparator<String>() {
      @Override
      public int compare(final String o1, final String o2) {
        return extractInt(o2) - extractInt(o1);
      }

    });
    return result;
  }

  @Override
  public List<MiriamData> getAvailableOrganisms() throws ReferenceGenomeConnectorException {
    try {
      List<MiriamData> result = new ArrayList<>();
      String content = getWebPageContent("https://hgdownload.soe.ucsc.edu/downloads.html");

      Matcher matcher = organismNamePattern.matcher(content);
      while (matcher.find()) {
        String name = matcher.group(1).trim();
        if (name.equalsIgnoreCase("Shared Data")) {
          continue;
        }
        if (name.equalsIgnoreCase("liftOver File")) {
          continue;
        }
        MiriamData taxonomy = taxonomyBackend.getByName(name);
        if (taxonomy != null) {
          result.add(taxonomy);
        }
      }

      return result;
    } catch (final IOException | TaxonomySearchException e) {
      throw new ReferenceGenomeConnectorException("Problem with accessing UCSC database", e);
    }
  }

  @Override
  public void downloadGenomeVersion(final MiriamData organism, final String version, final IProgressUpdater updater, final boolean async)
      throws FileNotAvailableException, IOException, ReferenceGenomeConnectorException {
    try {
      downloadGenomeVersion(organism, version, updater, async, getGenomeVersionFile(organism, version));
    } catch (final URISyntaxException e) {
      throw new InvalidStateException(e);
    }
  }

  @Override
  public void downloadGenomeVersion(final MiriamData organism, final String version, final IProgressUpdater updater, final boolean async,
      final String customUrl) throws IOException, URISyntaxException, ReferenceGenomeConnectorException {
    Callable<Void> computations = new DownloadGenomeVersionTask(organism, version, customUrl, updater);
    if (async) {
      getAsyncExecutorService().submit(computations);
    } else {
      Future<Void> task = getSyncExecutorService().submit(computations);
      executeTask(task);
    }
  }

  @Override
  public void removeGenomeVersion(final MiriamData organism, final String version) throws IOException {

    List<ReferenceGenome> genomes = self.getByType(ReferenceGenomeType.UCSC);
    for (final ReferenceGenome referenceGenome : genomes) {
      if (referenceGenome.getOrganism().equals(organism) && referenceGenome.getVersion().equals(version)) {
        // removing file from big file cache might not be the best idea here
        if (getBigFileCache().isCached(referenceGenome.getSourceUrl())) {
          getBigFileCache().removeFile(referenceGenome.getSourceUrl());
        }
        for (final ReferenceGenomeGeneMapping mapping : referenceGenome.getGeneMapping()) {
          if (getBigFileCache().isCached(mapping.getSourceUrl())) {
            getBigFileCache().removeFile(mapping.getSourceUrl());
          }
        }
        self.delete(referenceGenome);
      }
    }
  }

  @Override
  public String getGenomeVersionFile(final MiriamData organism, final String version) throws FileNotAvailableException, IOException {
    String filename = super.getCacheValue(
        FILENAME_BY_ORGANISM_VERSION_PREFIX + organism.getResource() + "\n" + version);
    if (filename != null) {
      return filename;
    }

    String remotePath = "https://hgdownload.soe.ucsc.edu/" + getGenomePath(organism, version);
    String content = getWebPageContent(remotePath);
    List<String> files = get2BitFiles(content);
    if (files.size() > 0) {
      filename = files.get(0);
    }
    if (filename == null) {
      throw new FileNotAvailableException("Cannot find file with genome for: " + organism + "; " + version);
    }
    String result = remotePath + filename;
    super.setCacheValue(FILENAME_BY_ORGANISM_VERSION_PREFIX + organism.getResource() + "\n" + version, result);
    return result;
  }

  private List<String> get2BitFiles(final String pageContent) {
    Pattern matcher = Pattern.compile("(\"[a-zA-Z\\.0-9]*2bit\")");

    List<String> result = new ArrayList<>();
    Matcher m = matcher.matcher(pageContent);
    while (m.find()) {
      result.add(m.group(1).replace("\"", ""));
    }

    return result;
  }

  @Override
  public Object refreshCacheQuery(final Object query) throws SourceNotAvailable {
    String result = null;
    try {
      if (query instanceof String) {
        String name = (String) query;
        if (name.startsWith("http")) {
          result = getWebPageContent(name);
        } else if (name.startsWith(FILENAME_BY_ORGANISM_VERSION_PREFIX)) {
          String[] tmp = name.substring(FILENAME_BY_ORGANISM_VERSION_PREFIX.length()).split("\n");
          result = getGenomeVersionFile(new MiriamData(MiriamType.TAXONOMY, tmp[0]), tmp[1]);
        } else {
          throw new InvalidArgumentException("Don't know what to do with string \"" + query + "\"");
        }
      } else {
        throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
      }
    } catch (final FileNotAvailableException e) {
      throw new SourceNotAvailable("Cannot find file for the query: " + query, e);
    } catch (final IOException e) {
      throw new SourceNotAvailable(e);
    }
    return result;
  }

  /**
   * @return the taxonomyBackend
   * @see #taxonomyBackend
   */
  @Override
  public TaxonomyBackend getTaxonomyBackend() {
    return taxonomyBackend;
  }

  /**
   * @param taxonomyBackend
   *          the taxonomyBackend to set
   * @see #taxonomyBackend
   */
  @Override
  public void setTaxonomyBackend(final TaxonomyBackend taxonomyBackend) {
    this.taxonomyBackend = taxonomyBackend;
  }

  /**
   * Returns local path on ftp server to folder with data about given organism and
   * version.
   *
   * @param organism
   *          organism of reference genome
   * @param version
   *          of reference genome
   * @return local path on ftp server to folder with data about reference genome
   */
  private String getGenomePath(final MiriamData organism, final String version) {
    return "/goldenPath/" + version + "/bigZips/";
  }

  /**
   * Extracts int from the version of the genome. The genome version look like
   * follow: xxxx011.
   *
   * @param s
   *          genome version where suffix part is integer number that informs
   *          about version
   * @return {@link Integer} representing version of the genome from string that
   *         describes genome version (it contains also some letters characters}
   */
  @Override
  public int extractInt(final String s) {
    int startIndex = 0;
    int endIndex = s.length() - 2;
    for (int i = 0; i < s.length(); i++) {
      startIndex = i;
      if (s.charAt(i) >= '0' && s.charAt(i) <= '9') {
        break;
      }
    }
    for (int i = startIndex; i < s.length(); i++) {
      if (s.charAt(i) < '0' || s.charAt(i) > '9') {
        break;
      }
      endIndex = i;
    }
    endIndex++;
    if (startIndex >= endIndex) {
      return 0;
    } else {
      return Integer.parseInt(s.substring(startIndex, endIndex));
    }
  }

  /**
   * Task that will be able to fetch genome file from ftp server.
   *
   * @author Piotr Gawron
   *
   */
  private final class DownloadGenomeVersionTask implements Callable<Void> {

    /**
     * Url to the file that we want to download.
     *
     */
    private String url;

    /**
     * Callback listener that will receive information about upload progress.
     *
     */
    private IProgressUpdater updater;

    /**
     * Organism for which we want to fetch genome.
     */
    private MiriamData organism;

    /**
     * Version of the genome.
     */
    private String version;

    /**
     * Default constructor.
     *
     * @param url
     *          {@link #url}
     * @param updater
     *          {@link #updater}
     * @param organism
     *          {@link #organism}
     * @param version
     *          {@link #version}
     */
    private DownloadGenomeVersionTask(final MiriamData organism, final String version, final String url, final IProgressUpdater updater) {
      this.url = url;
      this.organism = organism;
      this.version = version;
      if (updater != null) {
        this.updater = updater;
      } else {
        this.updater = new IProgressUpdater() {
          @Override
          public void setProgress(final double progress) {
          }
        };
      }
    }

    @Override
    public Void call() throws Exception {
      ReferenceGenome referenceGenome = new ReferenceGenome();
      referenceGenome.setOrganism(organism);
      referenceGenome.setType(ReferenceGenomeType.UCSC);
      referenceGenome.setVersion(version);
      referenceGenome.setSourceUrl(url);
      self.add(referenceGenome);

      final int referenceGenomeId = referenceGenome.getId();

      BigFileEntry entry = getBigFileCache().downloadFile(url, new IProgressUpdater() {
        @Override
        public void setProgress(final double progress) {
          if (updater != null) {
            updater.setProgress(progress);
          }
          // we have to get the object because it's in separate thread
          ReferenceGenome temp = self.getReferenceGenomeById(referenceGenomeId);
          temp.setDownloadProgress(progress);
          self.update(temp);
        }
      });
      referenceGenome = self.getReferenceGenomeById(referenceGenomeId);
      referenceGenome.setFile(entry);
      self.update(referenceGenome);
      return null;
    }

  }

  @Override
  public void downloadGeneMappingGenomeVersion(final ReferenceGenome referenceGenome, final String name, final IProgressUpdater updater,
      final boolean async, final String url)
      throws IOException, URISyntaxException, ReferenceGenomeConnectorException {
    Callable<Void> computations = new DownloadGeneMappingGenomeVersionTask(referenceGenome, name, url, updater);
    if (async) {
      getAsyncExecutorService().submit(computations);
    } else {
      Future<Void> task = getSyncExecutorService().submit(computations);
      executeTask(task);
    }
  }

  @Override
  public void removeGeneMapping(final ReferenceGenomeGeneMapping mapping2) throws IOException {
    ReferenceGenomeGeneMapping dbMapping = getReferenceGenomeGeneMappingDao().getById(mapping2.getId());
    if (dbMapping == null) {
      logger.warn("Mapping doesn't exist in the DB");
    } else {
      if (getBigFileCache().isCached(dbMapping.getSourceUrl())) {
        getBigFileCache().removeFile(dbMapping.getSourceUrl());
      }
      ReferenceGenome genome = dbMapping.getReferenceGenome();
      genome.getGeneMapping().remove(dbMapping);
      getReferenceGenomeGeneMappingDao().delete(dbMapping);
    }
  }

  /**
   * @return the referenceGenomeGeneMappingDao
   * @see #referenceGenomeGeneMappingDao
   */
  public ReferenceGenomeGeneMappingDao getReferenceGenomeGeneMappingDao() {
    return referenceGenomeGeneMappingDao;
  }

  /**
   * @param referenceGenomeGeneMappingDao
   *          the referenceGenomeGeneMappingDao to set
   * @see #referenceGenomeGeneMappingDao
   */
  public void setReferenceGenomeGeneMappingDao(final ReferenceGenomeGeneMappingDao referenceGenomeGeneMappingDao) {
    this.referenceGenomeGeneMappingDao = referenceGenomeGeneMappingDao;
  }

  /**
   * @return the bigFileCache
   * @see #bigFileCache
   */
  @Override
  public BigFileCache getBigFileCache() {
    return bigFileCache;
  }

  /**
   * @param bigFileCache
   *          the bigFileCache to set
   * @see #bigFileCache
   */
  @Override
  public void setBigFileCache(final BigFileCache bigFileCache) {
    this.bigFileCache = bigFileCache;
  }

  /**
   * @return the asyncExecutorService
   * @see #asyncExecutorService
   */
  protected ExecutorService getAsyncExecutorService() {
    return asyncExecutorService;
  }

  /**
   * @param asyncExecutorService
   *          the asyncExecutorService to set
   * @see #asyncExecutorService
   */
  protected void setAsyncExecutorService(final ExecutorService asyncExecutorService) {
    this.asyncExecutorService = asyncExecutorService;
  }

  /**
   * @return the syncExecutorService
   * @see #syncExecutorService
   */
  @Override
  public ExecutorService getSyncExecutorService() {
    return syncExecutorService;
  }

  /**
   * @param syncExecutorService
   *          the syncExecutorService to set
   * @see #syncExecutorService
   */
  @Override
  public void setSyncExecutorService(final ExecutorService syncExecutorService) {
    this.syncExecutorService = syncExecutorService;
  }

  /**
   * Return number of tasks that are executed or are waiting for execution.
   *
   * @return number of tasks that are executed or are waiting for execution
   */
  public int getDownloadThreadCount() {
    return ((ScheduledThreadPoolExecutor) asyncExecutorService).getQueue().size()
        + ((ScheduledThreadPoolExecutor) asyncExecutorService).getActiveCount()
        + ((ScheduledThreadPoolExecutor) syncExecutorService).getQueue().size()
        + ((ScheduledThreadPoolExecutor) syncExecutorService).getActiveCount();
  }

  /**
   * Executes download/update task.
   *
   * @param task
   *          task to be executed
   * @throws URISyntaxException
   *           thrown when task finsihed with {@link URISyntaxException}
   * @throws IOException
   *           thrown when task finsihed with {@link IOException}
   * @throws ReferenceGenomeConnectorException
   *           thrown when there is a problem with genome connector
   */
  void executeTask(final Future<?> task) throws URISyntaxException, IOException, ReferenceGenomeConnectorException {
    try {
      task.get();
    } catch (final InterruptedException e) {
      logger.error(e, e);
    } catch (final ExecutionException e) {
      if (e.getCause() instanceof URISyntaxException) {
        throw (URISyntaxException) e.getCause();
      } else if (e.getCause() instanceof IOException) {
        throw new IOException(e.getCause());
      } else if (e.getCause() instanceof ReferenceGenomeConnectorException) {
        throw new ReferenceGenomeConnectorException((ReferenceGenomeConnectorException) e.getCause());
      } else {
        throw new InvalidStateException(e);
      }
    }
  }

  /**
   * Task that will be able to fetch mapping genome file with given version from
   * externalserver.
   *
   * @author Piotr Gawron
   *
   */
  private final class DownloadGeneMappingGenomeVersionTask implements Callable<Void> {

    /**
     * Url to the file that we want to download.
     *
     */
    private String url;

    /**
     * Callback listener that will receive information about download progress.
     *
     */
    private IProgressUpdater updater;

    /**
     * Name of the gen genome mapping.
     */
    private String name;

    /**
     * Genome where mapping should be added.
     */
    private ReferenceGenome referenceGenome;

    /**
     * Default constructor.
     *
     * @param url
     *          {@link #url}
     * @param updater
     *          {@link #updater}
     * @param referenceGenome
     *          {@link #referenceGenome}
     * @param name
     *          {@link #name}
     */
    private DownloadGeneMappingGenomeVersionTask(final ReferenceGenome referenceGenome, final String name, final String url,
        final IProgressUpdater updater) throws ReferenceGenomeConnectorException {
      this.url = url;
      this.referenceGenome = referenceGenome;
      this.name = name;
      if (updater != null) {
        this.updater = updater;
      } else {
        this.updater = new IProgressUpdater() {
          @Override
          public void setProgress(final double progress) {
          }
        };
      }

    }

    @Override
    public Void call() throws Exception {
      try {
        ReferenceGenome referenceGenome = self.getReferenceGenomeById(this.referenceGenome.getId());
        for (final ReferenceGenomeGeneMapping mapping : referenceGenome.getGeneMapping()) {
          if (mapping.getName().equals(name)) {
            throw new ReferenceGenomeConnectorException("Gene mapping with name: \"" + name + "\" already exists.");
          }
        }
        if (!url.toLowerCase().endsWith("bb")) {
          throw new ReferenceGenomeConnectorException(
              "Only big bed format files are supported but found: \"" + url + "\".");
        }
        ReferenceGenomeGeneMapping mapping = new ReferenceGenomeGeneMapping();
        mapping.setName(name);
        mapping.setSourceUrl(url);
        referenceGenome.addReferenceGenomeGeneMapping(mapping);
        self.update(referenceGenome);
        BigFileEntry entry = getBigFileCache().downloadFile(url, new IProgressUpdater() {
          @Override
          public void setProgress(final double progress) {
            if (updater != null) {
              updater.setProgress(progress);
            }
            // we have to get the object because it's in separate thread
            ReferenceGenomeGeneMapping temp = self.getReferenceGenomeGeneMappingById(mapping.getId());
            temp.setDownloadProgress(progress);
            self.update(temp);
          }
        });
        ReferenceGenomeGeneMapping temp = self.getReferenceGenomeGeneMappingById(mapping.getId());
        temp.setDownloadProgress(100.0);
        temp.setFile(entry);
        self.update(temp);
        return null;
      } catch (final Exception e) {
        logger.error(e, e);
        throw e;
      }
    }

  }

  @Override
  public ReferenceGenome getReferenceGenomeById(final int id) {
    return referenceGenomeDao.getById(id);
  }

  @Override
  public void update(final ReferenceGenome referenceGenome) {
    referenceGenomeDao.update(referenceGenome);
  }

  @Override
  public void update(final ReferenceGenomeGeneMapping mapping) {
    referenceGenomeGeneMappingDao.update(mapping);
  }

  @Override
  public List<ReferenceGenome> getByType(final ReferenceGenomeType ucsc) {
    return referenceGenomeDao.getByType(ucsc);
  }

  @Override
  public void delete(final ReferenceGenome referenceGenome) {
    referenceGenomeDao.delete(referenceGenome);
  }

  @Override
  public void add(final ReferenceGenome referenceGenome) {
    referenceGenomeDao.add(referenceGenome);
  }

  @Override
  public ReferenceGenomeGeneMapping getReferenceGenomeGeneMappingById(final int id) {
    return referenceGenomeGeneMappingDao.getById(id);
  }

  @Override
  public void waitForDownloadsToFinish() throws InterruptedException {
    while (getDownloadThreadCount() > 0) {
      logger.debug("Waiting for download to finish");
      Thread.sleep(100);
    }
  }
}
