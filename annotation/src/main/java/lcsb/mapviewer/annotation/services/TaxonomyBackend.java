package lcsb.mapviewer.annotation.services;

import lcsb.mapviewer.annotation.cache.Cacheable;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

/**
 * This class is a backend to Taxonomy.
 *
 * @author Piotr Gawron
 */
public interface TaxonomyBackend extends IExternalService, Cacheable {

  /**
   * Constant defining human taxonomy.
   */
  MiriamData HUMAN_TAXONOMY = new MiriamData(MiriamType.TAXONOMY, "9606");

  MiriamData getByName(final String term) throws TaxonomySearchException;

  String getNameForTaxonomy(final MiriamData miriamData) throws TaxonomySearchException;

}
