package lcsb.mapviewer.annotation.services.annotators;

import com.fasterxml.jackson.annotation.JsonProperty;

import lcsb.mapviewer.model.map.species.field.Structure;

/**
 * Structure of the PDB entries returned by the PDBe REST API "Best Structures"
 * 
 * @author David Hoksza
 *
 */
public class PdbBestMappingEntry {
  @JsonProperty(value = "chain_id")
  private String chainId;

  @JsonProperty(value = "experimental_method")
  private String experimentalMethod;
  
  @JsonProperty(value = "pdb_id")
  private String pdbId;
  
  @JsonProperty(value = "start")
  private int start;
  
  @JsonProperty(value = "end")
  private int end;

  @JsonProperty(value = "unp_end")
  private int unpEnd;
  
  @JsonProperty(value = "coverage")
  private double coverage;
  
  @JsonProperty(value = "unp_start")
  private int unpStart;

  @JsonProperty(value = "resolution")
  private double resolution;
  
  @JsonProperty(value = "tax_id")
  private int taxId;

  public Structure convertToStructure() {

    Structure s = new Structure();

    s.setPdbId(this.pdbId);
    s.setChainId(this.chainId);
    s.setCoverage(this.coverage);
    s.setResolution(this.resolution);
    s.setStructStart(this.start);
    s.setStructEnd(this.end);
    s.setUnpStart(this.unpStart);
    s.setUnpEnd(this.unpEnd);
    s.setExperimentalMethod(this.experimentalMethod);
    s.setTaxId(this.taxId);

    return s;
  }
}
