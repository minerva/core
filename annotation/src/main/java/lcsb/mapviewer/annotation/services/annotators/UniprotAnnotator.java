package lcsb.mapviewer.annotation.services.annotators;

import java.util.Collection;

import lcsb.mapviewer.model.map.MiriamData;

/**
 * This is a class that implements a backend to uniprot restfull API.
 * 
 * @author Piotr Gawron
 * 
 */
public interface UniprotAnnotator extends IElementAnnotator {

  Collection<MiriamData> uniProtToEC(final MiriamData uniprot) throws UniprotSearchException;

  MiriamData uniProtToHgnc(final MiriamData uniprot) throws UniprotSearchException;

}
