package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.cache.QueryCacheInterface;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.MiriamConnector;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.model.user.annotator.AnnotatorInputParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;
import lcsb.mapviewer.model.user.annotator.BioEntityField;
import org.springframework.stereotype.Service;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class is responsible for connecting to
 * <a href="https://rest.genenames.org/">HGNC restfull API</a> and annotate
 * elements with information taken from this service.
 *
 * @author Piotr Gawron
 */
@Service
public class HgncAnnotatorImpl extends ElementAnnotator implements HgncAnnotator {

  /**
   * Address of HGNC API that should be used for retrieving data.
   */
  private static final String REST_API_URL = "https://rest.genenames.org/fetch/";

  private final MiriamConnector miriamConnector;

  /**
   * Constructor.
   */
  public HgncAnnotatorImpl(final MiriamConnector miriamConnector) {
    super(HgncAnnotator.class, new Class[]{Protein.class, Rna.class, Gene.class}, true);
    this.miriamConnector = miriamConnector;
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    QueryCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      GenericProtein proteinAlias = new GenericProtein("id");
      proteinAlias.addMiriamData(getExampleValidAnnotation());
      annotateElement(proteinAlias);

      if (proteinAlias.getFullName() == null || proteinAlias.getFullName().isEmpty()) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else {
        status.setStatus(ExternalServiceStatusType.OK);
      }
    } catch (final Exception e) {
      logger.error(getCommonName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  @Override
  public boolean annotateElement(final BioEntityProxy element, final MiriamData identifier, final AnnotatorData parameters)
      throws AnnotatorException {
    String query;
    if (identifier.getDataType().equals(MiriamType.HGNC_SYMBOL)) {

      query = getHgncNameUrl(identifier.getResource().replaceAll("/", "_"));
    } else if (identifier.getDataType().equals(MiriamType.HGNC)) {

      query = getHgncIdUrl(identifier.getResource());
    } else {
      throw new NotImplementedException();
    }
    try {
      String content = getWebPageContent(query);
      Node xml = XmlParser.getXmlDocumentFromString(content);
      Node response = XmlParser.getNode("response", xml.getChildNodes());
      Node result = XmlParser.getNode("result", response.getChildNodes());
      String count = XmlParser.getNodeAttr("numFound", result);
      if (count == null || count.isEmpty() || count.equals("0")) {
        // print warn info when we have only hgnc identifiers (or no
        // identifiers at all)
        logger.warn(element.getLogMarker(ProjectLogEntryType.CANNOT_FIND_INFORMATION),
            "Cannot find information for element.");
        return false;
      } else {
        Node entry = XmlParser.getNode("doc", result.getChildNodes());

        NodeList list = entry.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
          Node node = list.item(i);
          if (node.getNodeType() == Node.ELEMENT_NODE) {
            if (node.getNodeName().equals("str")) {
              String type = XmlParser.getNodeAttr("name", node);
              if (type.equals("hgnc_id") && !identifier.getDataType().equals(MiriamType.HGNC)) {
                // add hgnc id only when there was no hgnc_id in the element
                String id = XmlParser.getNodeValue(node);
                id = id.replaceAll("HGNC:", "");
                element.addMiriamData(MiriamType.HGNC, id);
              } else if (type.equals("ensembl_gene_id")) {
                element.addMiriamData(MiriamType.ENSEMBL, XmlParser.getNodeValue(node));
              } else if (type.equals("entrez_id")) {
                element.addMiriamData(MiriamType.ENTREZ, XmlParser.getNodeValue(node));
              } else if (type.equals("symbol")) {
                if (!identifier.getDataType().equals(MiriamType.HGNC_SYMBOL) || !element.contains(identifier)) {
                  // add hgnc symbol annotation only when there was no
                  // hgnc_symbol in the element
                  element.addMiriamData(MiriamType.HGNC_SYMBOL, XmlParser.getNodeValue(node));
                }
                element.setSymbol(XmlParser.getNodeValue(node));
                element.setName(XmlParser.getNodeValue(node));
              } else if (type.equals("name")) {
                element.setFullName(XmlParser.getNodeValue(node));
              }
            } else if (node.getNodeName().equals("arr")) {
              String type = XmlParser.getNodeAttr("name", node);
              if (type.equals("refseq_accession")) {
                NodeList sublist = node.getChildNodes();
                for (int j = 0; j < sublist.getLength(); j++) {
                  Node subnode = sublist.item(j);
                  if (subnode.getNodeType() == Node.ELEMENT_NODE) {
                    element.addMiriamData(MiriamType.REFSEQ, XmlParser.getNodeValue(subnode));
                  }
                }
              } else if (type.equals("prev_symbol")) {
                List<String> strings = new ArrayList<String>();
                NodeList sublist = node.getChildNodes();
                for (int j = 0; j < sublist.getLength(); j++) {
                  Node subnode = sublist.item(j);
                  if (subnode.getNodeType() == Node.ELEMENT_NODE) {
                    strings.add(XmlParser.getNodeValue(subnode));
                  }
                }
                element.setFormerSymbols(strings);
              } else if (type.equals("alias_symbol")) {
                List<String> strings = new ArrayList<>();
                NodeList sublist = node.getChildNodes();
                for (int j = 0; j < sublist.getLength(); j++) {
                  Node subnode = sublist.item(j);
                  if (subnode.getNodeType() == Node.ELEMENT_NODE) {
                    strings.add(XmlParser.getNodeValue(subnode));
                  }
                }
                element.setSynonyms(strings);
              } else if (type.equals("uniprot_ids")) {
                NodeList sublist = node.getChildNodes();
                for (int j = 0; j < sublist.getLength(); j++) {
                  Node subnode = sublist.item(j);
                  if (subnode.getNodeType() == Node.ELEMENT_NODE) {
                    element.addMiriamData(MiriamType.UNIPROT, XmlParser.getNodeValue(subnode));
                  }
                }
              }

            }
          }
        }
        return true;
      }
    } catch (final WrongResponseCodeIOException e) {
      logger.warn(element.getLogMarker(ProjectLogEntryType.CANNOT_FIND_INFORMATION),
          "Cannot find information for element.");
      return false;
    } catch (final Exception e) {
      throw new AnnotatorException(e);
    }
  }

  @Override
  public String getCommonName() {
    return MiriamType.HGNC.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.HGNC.getDbHomepage();
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Arrays.asList(new AnnotatorInputParameter(MiriamType.HGNC_SYMBOL),
        new AnnotatorInputParameter(MiriamType.HGNC),
        new AnnotatorInputParameter(BioEntityField.NAME, MiriamType.HGNC_SYMBOL));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Arrays.asList(
        new AnnotatorOutputParameter(MiriamType.ENSEMBL),
        new AnnotatorOutputParameter(MiriamType.ENTREZ),
        new AnnotatorOutputParameter(MiriamType.HGNC),
        new AnnotatorOutputParameter(MiriamType.HGNC_SYMBOL),
        new AnnotatorOutputParameter(MiriamType.REFSEQ),
        new AnnotatorOutputParameter(MiriamType.UNIPROT),
        new AnnotatorOutputParameter(BioEntityField.SYMBOL),
        new AnnotatorOutputParameter(BioEntityField.PREVIOUS_SYMBOLS),
        new AnnotatorOutputParameter(BioEntityField.SYNONYMS),
        new AnnotatorOutputParameter(BioEntityField.NAME),
        new AnnotatorOutputParameter(BioEntityField.FULL_NAME));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA");
  }

  /**
   * Creates query url for given {@link MiriamType#HGNC} identifier.
   *
   * @param id {@link MiriamType#HGNC} identifier
   * @return url to restful api webpage for given identifier
   */
  private String getHgncIdUrl(final String id) {
    return REST_API_URL + "hgnc_id/" + id;
  }

  /**
   * Creates query url for given {@link MiriamType#HGNC_SYMBOL}.
   *
   * @param name {@link MiriamType#HGNC_SYMBOL}
   * @return url to restful API web page for given HGNC symbol
   */
  private String getHgncNameUrl(final String name) {
    String hgncSymbol = name;
    hgncSymbol = hgncSymbol.split("\\s+")[0];
    return REST_API_URL + "symbol/" + hgncSymbol;
  }

  /**
   * Converts {@link MiriamType#HGNC}/{@link MiriamType#HGNC_SYMBOL} identifier
   * into list of {@link MiriamType#UNIPROT} identifiers.
   *
   * @param miriamData {@link MiriamData} with {@link MiriamType#HGNC} identifier
   * @return list of {@link MiriamData} with {@link MiriamType#UNIPROT}
   * @throws AnnotatorException thrown when there was problem with accessing external database or
   *                            any other unknown problem
   */
  @Override
  public List<MiriamData> hgncToUniprot(final MiriamData miriamData) throws AnnotatorException {
    String query = null;
    if (MiriamType.HGNC.equals(miriamData.getDataType())) {
      query = getHgncIdUrl(miriamData.getResource());
    } else if (MiriamType.HGNC_SYMBOL.equals(miriamData.getDataType())) {
      query = getHgncNameUrl(miriamData.getResource());
    } else {
      throw new InvalidArgumentException("Only " + MiriamType.HGNC + " and " + MiriamType.HGNC_SYMBOL
          + " are accepted but " + miriamData.getDataType() + " found.");
    }
    try {
      List<MiriamData> result = new ArrayList<>();
      String content = getWebPageContent(query);
      Node xml = XmlParser.getXmlDocumentFromString(content);
      Node response = XmlParser.getNode("response", xml.getChildNodes());
      Node resultNode = XmlParser.getNode("result", response.getChildNodes());
      String count = XmlParser.getNodeAttr("numFound", resultNode);
      if (count == null || count.isEmpty() || count.equals("0")) {
        return result;
      } else {
        Node entry = XmlParser.getNode("doc", resultNode.getChildNodes());

        NodeList list = entry.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
          Node node = list.item(i);
          if (node.getNodeType() == Node.ELEMENT_NODE) {
            if (node.getNodeName().equals("arr")) {
              String type = XmlParser.getNodeAttr("name", node);
              if (type.equals("uniprot_ids")) {
                NodeList uniprotList = node.getChildNodes();
                for (int j = 0; j < uniprotList.getLength(); j++) {
                  Node uniprotNode = uniprotList.item(j);
                  if (uniprotNode.getNodeType() == Node.ELEMENT_NODE) {
                    if (uniprotNode.getNodeName().equals("str")) {
                      result.add(new MiriamData(MiriamType.UNIPROT, uniprotNode.getTextContent()));
                    }
                  }
                }
              }
            }
          }
        }
      }
      return result;
    } catch (final WrongResponseCodeIOException e) {
      logger.warn("No HGNC data found for id: " + miriamData);
      return new ArrayList<>();
    } catch (final Exception e) {
      throw new AnnotatorException(e);
    }
  }

  /**
   * Converts {@link MiriamType#HGNC} identifier into
   * {@link MiriamType#HGNC_SYMBOL}.
   *
   * @param miriamData {@link MiriamData} with {@link MiriamType#HGNC} identifier
   * @return {@link MiriamData} with {@link MiriamType#HGNC_SYMBOL}
   * @throws AnnotatorException thrown when there was problem with accessing external database or
   *                            any other unknown problem
   */
  @Override
  public MiriamData hgncIdToHgncName(final MiriamData miriamData) throws AnnotatorException {
    String query = null;
    if (MiriamType.HGNC.equals(miriamData.getDataType())) {
      query = getHgncIdUrl(miriamData.getResource());
    } else {
      throw new InvalidArgumentException(
          "Only " + MiriamType.HGNC + " is accepted but " + miriamData.getDataType() + " found.");
    }
    try {
      String content = getWebPageContent(query);
      Node xml = XmlParser.getXmlDocumentFromString(content);
      Node response = XmlParser.getNode("response", xml.getChildNodes());
      Node resultNode = XmlParser.getNode("result", response.getChildNodes());
      String count = XmlParser.getNodeAttr("numFound", resultNode);
      if (count == null || count.isEmpty() || count.equals("0")) {
        return null;
      } else {
        Node entry = XmlParser.getNode("doc", resultNode.getChildNodes());

        NodeList list = entry.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
          Node node = list.item(i);
          if (node.getNodeType() == Node.ELEMENT_NODE) {
            if (node.getNodeName().equals("str")) {
              String type = XmlParser.getNodeAttr("name", node);
              if (type.equals("symbol")) {
                return new MiriamData(MiriamType.HGNC_SYMBOL, node.getTextContent());
              }
            }
          }
        }
      }
    } catch (final Exception e) {
      throw new AnnotatorException(e);
    }
    return null;
  }

  /**
   * Returns <code>true</code> if the {@link MiriamData} given in the parameter
   * is a valid {@link MiriamType#HGNC} or {@link MiriamType#HGNC_SYMBOL}.
   *
   * @param md {@link MiriamData} to validate
   * @return <code>true</code> if the {@link MiriamData} given in the parameter is a valid {@link MiriamType#HGNC} or {@link MiriamType#HGNC_SYMBOL}
   * @throws AnnotatorException thrown when there is a problem accessing HGNC restful AI
   */
  @Override
  public boolean isValidHgncMiriam(final MiriamData md) throws AnnotatorException {
    if (MiriamType.HGNC.equals(md.getDataType()) || MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
      String url = miriamConnector.getUrlString(md);
      if (url == null) {
        return false;
      }
      return !hgncToUniprot(md).isEmpty();
    } else {
      return false;
    }
  }

  /**
   * Transforms HGNC symbol into {@link MiriamType#ENTREZ} identifier.
   *
   * @param md hgnc symbol
   * @return {@link MiriamType#ENTREZ} identifier for a given hgnc symbol
   * @throws AnnotatorException thrown when there is a problem with accessing hgnc server
   */
  @Override
  public MiriamData hgncToEntrez(final MiriamData md) throws AnnotatorException {
    String query = null;
    if (MiriamType.HGNC_SYMBOL.equals(md.getDataType())) {
      query = getHgncNameUrl(md.getResource());
    } else if (MiriamType.HGNC.equals(md.getDataType())) {
      query = getHgncIdUrl(md.getResource());
    } else {
      throw new InvalidArgumentException("Invalid miriam data: " + md);
    }
    try {
      String content = getWebPageContent(query);
      Node xml = XmlParser.getXmlDocumentFromString(content);
      Node response = XmlParser.getNode("response", xml.getChildNodes());
      Node result = XmlParser.getNode("result", response.getChildNodes());
      String count = XmlParser.getNodeAttr("numFound", result);
      if (count == null || count.isEmpty() || count.equals("0")) {
        return null;
      } else {
        Node entry = XmlParser.getNode("doc", result.getChildNodes());

        NodeList list = entry.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
          Node node = list.item(i);
          if (node.getNodeType() == Node.ELEMENT_NODE) {
            if (node.getNodeName().equals("str")) {
              String type = XmlParser.getNodeAttr("name", node);
              if (type.equals("entrez_id")) {
                String id = XmlParser.getNodeValue(node);
                return new MiriamData(MiriamType.ENTREZ, id);
              }
            }
          }
        }
      }
      return null;
    } catch (final IOException | InvalidXmlSchemaException e) {
      throw new AnnotatorException(e);
    }
  }
}
