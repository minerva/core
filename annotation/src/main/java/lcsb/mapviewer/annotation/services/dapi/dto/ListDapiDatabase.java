package lcsb.mapviewer.annotation.services.dapi.dto;

import java.util.ArrayList;
import java.util.List;

public class ListDapiDatabase {

  private List<DapiDatabase> content = new ArrayList<>();

  public List<DapiDatabase> getContent() {
    return content;
  }

  public void setContent(final List<DapiDatabase> content) {
    this.content = content;
  }

}
