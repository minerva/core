package lcsb.mapviewer.annotation.services.annotators;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.annotation.cache.QueryCacheInterface;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Chemical;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.model.user.annotator.AnnotatorInputParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;
import lcsb.mapviewer.model.user.annotator.BioEntityField;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * This is a class that implements a backend to RECON database.
 *
 * @author Piotr Gawron
 */
@Service
public class ReconAnnotatorImpl extends ElementAnnotator implements ReconAnnotator {

  /**
   * Address of annotation rest service for elements.
   */
  private static final String ELEMENT_ANNOTATION_URL_PREFIX = "https://www.vmh.life/_api/metabolites/?page_size=10000&format=json&search=";

  /**
   * Address of annotation rest service for reactions.
   */
  private static final String REACTION_ANNOTATION_URL_PREFIX = "https://www.vmh.life/_api/reactions/?page_size=10000&format=json&search=";

  private final ObjectMapper objectMapper = new ObjectMapper();

  public ReconAnnotatorImpl() {
    super(ReconAnnotator.class, new Class[]{Chemical.class, Reaction.class}, false);
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    QueryCacheInterface cacheCopy = getCache();
    this.setCache(null);
    MinervaLoggerAppender appender = MinervaLoggerAppender.createAppender();
    try {
      SimpleMolecule smallMoleculeAlias = new SimpleMolecule("some_id");
      smallMoleculeAlias.setName("h2o");
      annotateElement(smallMoleculeAlias);
      status.setStatus(ExternalServiceStatusType.OK);
      if (smallMoleculeAlias.getMiriamData().size() == 0) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else if (appender.getWarnings().size() > 0) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (final Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
    this.setCache(cacheCopy);
    return status;
  }

  @SuppressWarnings("deprecation")
  @Override
  public boolean annotateElement(final BioEntityProxy element, final MiriamData identifier, final AnnotatorData parameters)
      throws AnnotatorException {
    if (identifier.getDataType().equals(MiriamType.VMH_METABOLITE)) {
      String url = null;
      if (element.isElement()) {
        url = ELEMENT_ANNOTATION_URL_PREFIX + identifier.getResource();
      } else if (element.isReaction()) {
        url = REACTION_ANNOTATION_URL_PREFIX + identifier.getResource();
      } else {
        logger.warn("Unknown class type: " + element.getClass());
        return false;
      }
      try {
        String content = getWebPageContent(url);

        if (!content.isEmpty()) {

          Map<String, Object> contentObject = objectMapper.readValue(content, new TypeReference<HashMap<String, Object>>() {
          });

          List<?> results = (List<?>) contentObject.get("results");
          if (results.size() > 0) {
            Map<String, Object> jobject = getBestMatchForAbbreviation(identifier.getResource(), results);

            for (Entry<String, Object> entry : jobject.entrySet()) {
              String key = entry.getKey();
              String value = null;
              if (entry.getValue() != null) {
                value = (entry.getValue() + "").trim();
              }
              if (value == null || value.isEmpty()) {
                continue;
              }

              boolean unknown = false;
              if (key.equals("url")) {
                continue;
              } else if (key.equals("rxn_id")) {
                continue;
              } else if (key.equals("abbreviation")) {
                element.setAbbreviation(value);
              } else if (key.equals("description")) {
                element.setDescription(value);
              } else if (key.equals("formula")) {
                element.setFormula(value);
              } else if (key.equals("reversible")) {
                continue;
              } else if (key.equals("mcs")) {
                element.setMechanicalConfidenceScore(value);
              } else if (key.equals("notes")) {
                element.setDescription(value);
              } else if (key.equals("ecnumber")) {
                element.addMiriamData(MiriamType.EC, value);
              } else if (key.equals("chargedFormula")) {
                element.setFormula(value);
              } else if (key.equals("charge")) {
                element.setCharge(value);
              } else if (key.equals("inchiString")) {
                element.addMiriamData(MiriamType.INCHI, value);
              } else if (key.equals("cheBlId")) {
                if (!value.startsWith("CHEBI:")) {
                  value = "CHEBI:" + value;
                }
                element.addMiriamData(MiriamType.CHEBI, value);
              } else if (key.equals("subsystem")) {
                element.setSubsystem(value);
              } else if (key.equals("keggId")) {
                MiriamData md = createKeggMiriam(element, value);
                if (md != null) {
                  element.addMiriamData(md);
                }
              } else if (key.equals("fullName")) {
                element.setFullName(value);
              } else if (key.equals("keggorthology")) {
                MiriamData md = createKeggMiriam(element, value);
                if (md != null) {
                  element.addMiriamData(md);
                }
              } else if (key.equals("pubChemId")) {
                element.addMiriamData(MiriamType.PUBCHEM, value);
              } else if (key.equals("hmdb")) {
                element.addMiriamData(MiriamType.HMDB, value);
              } else if (key.equals("cog")) {
                element.addMiriamData(MiriamType.COG, value);
              } else if (key.equals("synonyms")) {
                List<String> synonyms = new ArrayList<>();
                Collections.addAll(synonyms, value.split(","));
                element.setSynonyms(synonyms);
              } else if (key.equals("miriam")) {
                element.addMiriamData(value);
              } else if (key.equals("inchiKey")) {
                element.addMiriamData(MiriamType.INCHIKEY, value);
              } else if (key.equals("smile")) {
                element.setSmile(value);
              } else if (key.equals("chemspider")) {
                element.addMiriamData(MiriamType.CHEMSPIDER, value);
              } else if (key.equals("wikipedia")) {
                element.addMiriamData(MiriamType.WIKIPEDIA, value);
              } else if (key.equals("casRegistry")) {
                element.addMiriamData(MiriamType.CAS, value);
              } else if (key.equals("chembl")) {
                element.addMiriamData(MiriamType.CHEMBL_COMPOUND, value);
              } else if (key.equals("mesh_id")) {
                element.addMiriamData(MiriamType.MESH_2012, value);
              } else if (key.equals("rhea")) {
                element.addMiriamData(MiriamType.RHEA, value);
              } else if (key.equals("ref")) {
                continue;
              } else if (key.equals("lastModified")) {
                continue;
              } else if (key.equals("met_id")) {
                continue;
              } else if (key.equals("csecoida")) {
                continue;
              } else if (key.equals("pdmapName")) {
                // for now we skip it...
                continue;
              } else if (key.equals("reconMap")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("reconMap3")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("massbalance")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("isHuman")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("isMicrobe")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("iupac")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("avgmolweight")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("monoisotopicweight")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("biggId")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("lmId")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("hepatonetId")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("metanetx")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("seed")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("food_db")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("metlin")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("epa_id")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("createdDate")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("updatedDate")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("biocyc")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("organellemap")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("fda_id")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("chodb_id")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("mitochondrionmap")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("nucleusmap")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("peroxisomemap")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("reticulummap")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("golgimap")) {
                // for now we don't handle it
                continue;
              } else if (key.equals("lysosomemap")) {
                // for now we don't handle it
                continue;
              } else {
                unknown = true;
              }
              if (unknown) {
                logger.warn(element.getLogMarker(ProjectLogEntryType.OTHER),
                    "Unknown field in recon annotation: \"" + key + "\" (value: " + value + ")");
              }
            }
            return true;
          } else {
            logger.warn(element.getLogMarker(ProjectLogEntryType.CANNOT_FIND_INFORMATION),
                "No recon annotations \"" + identifier.getResource() + "\"");
            return false;
          }
        } else {
          logger.warn(element.getLogMarker(ProjectLogEntryType.CANNOT_FIND_INFORMATION),
              "No recon annotations \"" + identifier.getResource() + "\"");
          return false;
        }
      } catch (final IOException e) {
        throw new AnnotatorException(e);
      }
    } else {
      throw new NotImplementedException();
    }
  }

  @Override
  public String getCommonName() {
    return "Recon annotator";
  }

  @Override
  public String getUrl() {
    return "https://www.vmh.life/";
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Arrays.asList(new AnnotatorInputParameter(BioEntityField.ABBREVIATION, MiriamType.VMH_METABOLITE),
        new AnnotatorInputParameter(BioEntityField.NAME, MiriamType.VMH_METABOLITE),
        new AnnotatorInputParameter(MiriamType.VMH_METABOLITE));
  }

  @SuppressWarnings("deprecation")
  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Arrays.asList(new AnnotatorOutputParameter(MiriamType.EC),
        new AnnotatorOutputParameter(MiriamType.CHEBI),
        new AnnotatorOutputParameter(MiriamType.KEGG_COMPOUND),
        new AnnotatorOutputParameter(MiriamType.KEGG_REACTION),
        new AnnotatorOutputParameter(MiriamType.KEGG_ORTHOLOGY),
        new AnnotatorOutputParameter(MiriamType.PUBCHEM),
        new AnnotatorOutputParameter(MiriamType.HMDB),
        new AnnotatorOutputParameter(MiriamType.INCHI),
        new AnnotatorOutputParameter(MiriamType.INCHIKEY),
        new AnnotatorOutputParameter(MiriamType.COG),
        new AnnotatorOutputParameter(MiriamType.VMH_METABOLITE),
        new AnnotatorOutputParameter(MiriamType.VMH_REACTION),
        new AnnotatorOutputParameter(MiriamType.CHEMSPIDER),
        new AnnotatorOutputParameter(MiriamType.WIKIPEDIA),
        new AnnotatorOutputParameter(MiriamType.CAS),
        new AnnotatorOutputParameter(MiriamType.CHEMBL_COMPOUND),
        new AnnotatorOutputParameter(MiriamType.MESH_2012),
        new AnnotatorOutputParameter(MiriamType.RHEA),
        new AnnotatorOutputParameter(BioEntityField.ABBREVIATION),
        new AnnotatorOutputParameter(BioEntityField.DESCRIPTION),
        new AnnotatorOutputParameter(BioEntityField.FORMULA),
        new AnnotatorOutputParameter(BioEntityField.MCS),
        new AnnotatorOutputParameter(BioEntityField.CHARGE),
        new AnnotatorOutputParameter(BioEntityField.SUBSYSTEM),
        new AnnotatorOutputParameter(BioEntityField.SYNONYMS),
        new AnnotatorOutputParameter(BioEntityField.SMILE));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    // we don't provide example because there should be separate example for
    // reaction and element
    return null;
  }

  private MiriamData createKeggMiriam(final BioEntityProxy element, final String value) {
    MiriamData md = null;
    if (value.startsWith("C")) {
      md = new MiriamData(MiriamType.KEGG_COMPOUND, value);
    } else if (value.startsWith("R")) {
      md = new MiriamData(MiriamType.KEGG_REACTION, value);
    } else if (value.startsWith("K")) {
      md = new MiriamData(MiriamType.KEGG_ORTHOLOGY, value);
    } else {
      logger.warn(element.getLogMarker(ProjectLogEntryType.INVALID_IDENTIFIER),
          "Unknown Kegg identifier type (only Kegg compounds and reactions are supported): \"" + value + "\"");
    }
    return md;
  }

  @SuppressWarnings("unchecked")
  private Map<String, Object> getBestMatchForAbbreviation(final String id, final List<?> asJsonArray) {
    String match = null;
    Map<String, Object> result = null;
    for (final Object element : asJsonArray) {
      if (((Map<?, ?>) element).get("abbreviation") != null) {
        String objAbreviation = (String) ((Map<?, ?>) element).get("abbreviation");
        if (match == null) {
          match = objAbreviation;
          result = (Map<String, Object>) element;
        } else if (match.length() > objAbreviation.length()) {
          match = objAbreviation;
          result = (Map<String, Object>) element;
        } else if (id.equalsIgnoreCase(objAbreviation)) {
          match = objAbreviation;
          result = (Map<String, Object>) element;
        }
      } else if (match == null) {
        logger.warn("No abbreviation found for element: " + element);
        result = (Map<String, Object>) element;
      }
    }
    return result;
  }
}
