package lcsb.mapviewer.annotation.services.dapi;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.annotation.cache.CachableInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.services.MeSHParser;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.annotation.services.dapi.dto.ChemicalEntityDto;
import lcsb.mapviewer.annotation.services.dapi.dto.ChemicalEntityDtoConverter;
import lcsb.mapviewer.annotation.services.dapi.dto.ListChemicalEntityDto;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.persist.dao.ProjectDao;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@Service
public class ChemicalParserImpl extends CachableInterface implements ChemicalParser {

  /**
   * Prefix used for caching list of suggested queries for project.
   */
  protected static final String PROJECT_SUGGESTED_QUERY_PREFIX = "PROJECT_CHEMICAL_QUERIES:";

  private static final String DAPI_DATABASE_NAME = "CTD";

  private final Logger logger = LogManager.getLogger();

  private final DapiConnector dapiConnector;

  private final MeSHParser meSHParser;

  private final ChemicalEntityDtoConverter chemicalEntityDtoConverter;

  private final ProjectDao projectDao;

  private final HgncAnnotator hgncAnnotator;

  private final ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
      false);

  @Autowired
  public ChemicalParserImpl(final DapiConnector dapiConnector,
                            final ChemicalEntityDtoConverter chemicalEntityDtoConverter,
                            final MeSHParser meSHParser,
                            final ProjectDao projectDao,
                            final HgncAnnotator hgncAnnotator
  ) {
    super(ChemicalParserImpl.class);
    this.dapiConnector = dapiConnector;
    this.chemicalEntityDtoConverter = chemicalEntityDtoConverter;
    this.meSHParser = meSHParser;
    this.projectDao = projectDao;
    this.hgncAnnotator = hgncAnnotator;
  }

  /**
   * @param disease the MESH id for the disease
   * @return list of chemicals id, name pairs
   * @throws ChemicalSearchException thrown when there is problem with accessing ctd database
   */
  public List<Chemical> getChemicalsForDisease(final MiriamData disease) throws ChemicalSearchException {
    if (disease == null || disease.getResource() == null) {
      throw new InvalidArgumentException("disease cannot be null");
    }
    if (!dapiConnector.isValidConnection()) {
      return new ArrayList<>();
    }
    try {
      final String url = dapiConnector.getLatestReleaseUrl(DAPI_DATABASE_NAME);
      if (url == null) {
        return null;
      }

      final String content = dapiConnector
          .getAuthenticatedContent(
              url + "/drugs/?size=1000&columns=sourceIdentifier&target_disease_identifier="
                  + URLEncoder.encode(disease.getPrefixedIdentifier(), "UTF-8"));

      final ListChemicalEntityDto dto = objectMapper.readValue(content, ListChemicalEntityDto.class);

      return fetchChemicals(dto, disease);
    } catch (final DapiConnectionException | IOException e) {
      throw new ChemicalSearchException("Problem with accessing DAPI", e);
    }
  }

  /**
   * Returns list of chemicals for given set of target genes related to the
   * disease.
   *
   * @param targets set of target genes
   * @param disease identifier of the disease
   * @return list of chemicals for given set of target genes related to the disease
   * @throws ChemicalSearchException thrown when there is a problem with accessing ctd database
   */
  public List<Chemical> getChemicalListByTargets(final Collection<MiriamData> targets, final MiriamData disease)
      throws ChemicalSearchException {

    final Map<MiriamData, Chemical> chemicals = new HashMap<>();
    for (final MiriamData miriamData : targets) {
      for (final Chemical chemical : getChemicalListByTarget(miriamData, disease)) {
        chemicals.put(chemical.getChemicalId(), chemical);
      }
    }
    return new ArrayList<>(chemicals.values());
  }

  public List<Chemical> getChemicalListByTarget(final MiriamData target, final MiriamData disease) throws ChemicalSearchException {
    return getChemicalListByTarget(target, disease, new ArrayList<>());
  }

  /**
   * Returns list of chemicals that interact with a target.
   *
   * @param target  target for which we are looking for chemicals
   * @param disease we want to get chemicals in context of the disease
   * @return list of chemicals that interact with a target
   * @throws ChemicalSearchException thrown when there is a problem with accessing chemical database
   */
  public List<Chemical> getChemicalListByTarget(final MiriamData target, final MiriamData disease, final List<String> idsToExclude)
      throws ChemicalSearchException {
    if (target == null) {
      return new ArrayList<>();
    }
    if (!dapiConnector.isValidConnection()) {
      return new ArrayList<>();
    }
    final List<MiriamData> targets = extractHgncTargets(target);

    try {
      final String url = dapiConnector.getLatestReleaseUrl(DAPI_DATABASE_NAME);
      if (url == null) {
        return null;
      }

      final Map<MiriamData, Chemical> result = new HashMap<>();

      for (final MiriamData t : targets) {
        final String content = dapiConnector
            .getAuthenticatedContent(
                url + "/drugs/?size=1000&columns=sourceIdentifier&target_identifier="
                    + URLEncoder.encode(t.getPrefixedIdentifier(), "UTF-8")
                    + "&target_disease_identifier=" + URLEncoder.encode(disease.getPrefixedIdentifier(), "UTF-8"));

        final ListChemicalEntityDto dto = objectMapper.readValue(content, ListChemicalEntityDto.class);

        dto.exclude(idsToExclude);
        final List<Chemical> chemicals = fetchChemicals(dto, disease);
        for (final Chemical chemical : chemicals) {
          if (chemical.containsTarget(target)) {
            result.put(chemical.getChemicalId(), chemical);
          }
        }
        for (ChemicalEntityDto entityDto : dto.getContent()) {
          idsToExclude.add(entityDto.getSourceIdentifier());
        }
      }
      return new ArrayList<>(result.values());
    } catch (final DapiConnectionException | IOException e) {
      throw new ChemicalSearchException("Problem with accessing DAPI", e);
    }
  }

  private List<MiriamData> extractHgncTargets(final MiriamData target) {
    final List<MiriamData> targets = new ArrayList<>();
    if (target.getDataType().equals(MiriamType.HGNC_SYMBOL)) {
      targets.add(target);
    } else {
      throw new InvalidArgumentException(
          "Only those identifiers are accepted: " + MiriamType.HGNC_SYMBOL + ". Found: " + target);
    }
    return targets;
  }

  public List<String> getSuggestedQueryList(final Project sourceProject, final MiriamData diseaseMiriam) throws ChemicalSearchException {
    if (diseaseMiriam == null) {
      return new ArrayList<>();
    }

    final String cacheQuery = PROJECT_SUGGESTED_QUERY_PREFIX + "\n" + sourceProject.getId() + "\n" + diseaseMiriam.getResource();
    String cachedData = getCacheValue(cacheQuery);
    final List<String> result;
    if (cachedData == null) {
      return null;
    } else {
      result = new ArrayList<>();
      for (final String string : cachedData.split("\n")) {
        if (!string.isEmpty()) {
          result.add(string);
        }
      }
    }
    return result;
  }

  private List<String> getSuggestedQueryListWithoutCache(final Project project, final MiriamData diseaseMiriam) throws ChemicalSearchException {
    final Set<ModelData> maps = new HashSet<>(project.getModels());
    for (final ModelData model : project.getModels()) {
      for (final ModelSubmodelConnection submodel : model.getSubmodels()) {
        maps.add(submodel.getSubmodel());
      }
    }

    final Set<MiriamData> hgncSymbols = new HashSet<>();
    for (final ModelData model : maps) {
      for (final BioEntity bioEntity : model.getElements()) {
        for (final MiriamData md : bioEntity.getMiriamData()) {
          if (md.getDataType().equals(MiriamType.HGNC_SYMBOL)) {
            try {
              if (hgncAnnotator.isValidHgncMiriam(md)) {
                hgncSymbols.add(new MiriamData(md.getDataType(), md.getResource()));
              }
            } catch (Exception e) {
              logger.warn("Problem with checking validity of hgnc symbol for " + md, e);
            }
          }
        }
        MiriamData md = new MiriamData(MiriamType.HGNC_SYMBOL, bioEntity.getName().toUpperCase());
        try {
          if (hgncAnnotator.isValidHgncMiriam(md)) {
            hgncSymbols.add(md);
          }
        } catch (Exception e) {
          logger.warn("Problem with checking validity of hgnc symbol for " + md, e);
        }
      }
    }
    final Set<String> names = new HashSet<>();
    List<String> idsToExclude = new ArrayList<>();

    for (final MiriamData hgncSymbol : hgncSymbols) {
      // fetching all chemicals for big map is very memory exhaustive, therefore
      // do it step by step to prevent storing everything in the memory at single
      // point
      try {
        final List<Chemical> chemicals = getChemicalListByTarget(hgncSymbol, diseaseMiriam, idsToExclude);
        for (final Chemical chemical : chemicals) {
          names.add(chemical.getChemicalName());
          names.addAll(chemical.getSynonyms());
        }
      } catch (Exception e) {
        logger.error("Problem with getting chemicals for " + hgncSymbol, e);
      }
    }
    final List<String> result = new ArrayList<>(names);
    Collections.sort(result);
    return result;
  }

  public List<Chemical> getChemicalsByName(final MiriamData disease, final String chemicalName) throws ChemicalSearchException {
    try {
      if (disease == null) {
        throw new InvalidArgumentException("disease cannot be null");
      }

      if (!dapiConnector.isValidConnection()) {
        return null;
      }
      final String url = dapiConnector.getLatestReleaseUrl(DAPI_DATABASE_NAME);
      if (url == null) {
        return null;
      }

      String content = dapiConnector
          .getAuthenticatedContent(
              url + "/drugs/?columns=sourceIdentifier&name=" + URLEncoder.encode(chemicalName, "UTF-8")
                  + "&target_disease_identifier="
                  + URLEncoder.encode(disease.getPrefixedIdentifier(), "UTF-8"));

      ListChemicalEntityDto result = objectMapper.readValue(content, ListChemicalEntityDto.class);
      List<Chemical> chemicals = fetchChemicals(result, disease);

      if (chemicals.isEmpty()) {
        content = dapiConnector
            .getAuthenticatedContent(
                url + "/drugs/?columns=sourceIdentifier&synonym=" + URLEncoder.encode(chemicalName, "UTF-8"));
        result = objectMapper.readValue(content, ListChemicalEntityDto.class);
        chemicals = fetchChemicals(result, disease);
      }
      return chemicals;
    } catch (final DapiConnectionException | IOException e) {
      throw new ChemicalSearchException("Problem with fetching chemical data", e);
    }
  }

  @Override
  public void refreshChemicalSuggestedQueryList(final Integer projectId, final MiriamData organism) {
    Project project = projectDao.getById(projectId);

    try {
      final List<String> result = getSuggestedQueryListWithoutCache(project, organism);
      setChemicalSuggestedQueryList(projectId, organism, result);
    } catch (ChemicalSearchException e) {
      logger.error("Problem with refreshing chemical query list", e);
    }
  }

  @Override
  public void setChemicalSuggestedQueryList(final Integer projectId, final MiriamData organism, final List<String> queryList) {
    final String cacheQuery = PROJECT_SUGGESTED_QUERY_PREFIX + "\n" + projectId + "\n" + organism.getResource();
    String cachedData = StringUtils.join(queryList, "\n");
    setCacheValue(cacheQuery, cachedData);
  }

  private List<Chemical> fetchChemicals(final ListChemicalEntityDto list, final MiriamData disease) throws DapiConnectionException {
    final List<Chemical> result = new ArrayList<>();
    for (final ChemicalEntityDto entity : list.getContent()) {
      final Chemical chemical = getByIdentifier(MiriamType.getMiriamDataFromPrefixIdentifier(entity.getSourceIdentifier()),
          disease);
      if (chemical == null) {
        logger.warn("Invalid chemical identifier: {}", entity.getSourceIdentifier());
      } else {
        result.add(chemical);
      }
    }
    return result;
  }

  private Chemical getByIdentifier(final MiriamData identifier, final MiriamData disease) throws DapiConnectionException {
    final String url = dapiConnector.getLatestReleaseUrl(DAPI_DATABASE_NAME);
    if (url == null) {
      return null;
    }
    final String content = dapiConnector.getAuthenticatedContent(url + "/drugs/" + identifier.getResource());

    try {
      final ChemicalEntityDto result = objectMapper.readValue(content, ChemicalEntityDto.class);

      final Chemical chemical = chemicalEntityDtoConverter.dtoToChemical(result);
      chemical.setMesh(meSHParser.getMeSH(chemical.getChemicalId()));
      final Set<Target> targetsToRemove = new HashSet<>();
      for (final Target t : chemical.getTargets()) {
        if (disease != null && !Objects.equals(t.getAssociatedDisease(), disease)) {
          targetsToRemove.add(t);
        }
      }
      chemical.removeTargets(targetsToRemove);
      return chemical;
    } catch (final IOException e) {
      throw new DapiConnectionException("Problem with accessing dapi", e);
    }
  }

  @Override
  public String refreshCacheQuery(final Object query) throws SourceNotAvailable {
    String result;
    try {
      if (query instanceof String) {
        final String name = (String) query;
        if (name.startsWith(PROJECT_SUGGESTED_QUERY_PREFIX)) {
          final String[] tmp = name.split("\n");
          final Integer id = Integer.valueOf(tmp[1]);
          final MiriamData diseaseId = new MiriamData(MiriamType.MESH_2012, tmp[2]);
          final Project project = projectDao.getById(id);
          if (project == null) {
            throw new SourceNotAvailable("Project with given id doesn't exist: " + id);
          }
          final List<String> list = getSuggestedQueryListWithoutCache(project, diseaseId);
          result = StringUtils.join(list, "\n");
        } else if (name.startsWith("http")) {
          result = getWebPageContent(name);
        } else {
          throw new InvalidArgumentException("Don't know what to do with string \"" + query + "\"");
        }
      } else {
        throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
      }
    } catch (IOException | ChemicalSearchException e) {
      throw new SourceNotAvailable(e);
    }
    return result;
  }

}
