package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.data.Go;
import lcsb.mapviewer.model.map.MiriamData;

/**
 * This class is a backend to Gene Ontology API.
 * 
 * @author Piotr Gawron
 * 
 */
public interface GoAnnotator extends IElementAnnotator {

  Go getGoElement(final MiriamData md) throws GoSearchException;

}
