package lcsb.mapviewer.annotation.services.dapi;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import lcsb.mapviewer.annotation.cache.QueryCacheInterface;
import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.services.DrugAnnotationImpl;
import lcsb.mapviewer.annotation.services.DrugSearchException;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.annotation.services.dapi.dto.ChemicalEntityDto;
import lcsb.mapviewer.annotation.services.dapi.dto.ChemicalEntityDtoConverter;
import lcsb.mapviewer.annotation.services.dapi.dto.ListChemicalEntityDto;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

@Service
public class DrugBankParserImpl extends DrugAnnotationImpl implements DrugBankParser {

  public static final String DAPI_DATABASE_NAME = "DrugBank";

  private static Logger logger = LogManager.getLogger();

  private DapiConnector dapiConnector;

  private HgncAnnotator hgncAnnotator;

  private ChemicalEntityDtoConverter chemicalEntityDtoConverter;

  private ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
      false);

  @Autowired
  public DrugBankParserImpl(final DapiConnector dapiConnector, final ChemicalEntityDtoConverter chemicalEntityDtoConverter,
      final HgncAnnotator hgncAnnotator) {
    super(DrugBankParser.class);
    this.dapiConnector = dapiConnector;
    this.chemicalEntityDtoConverter = chemicalEntityDtoConverter;
    this.hgncAnnotator = hgncAnnotator;
  }

  @Override
  public Drug findDrug(final String drugName) throws DrugSearchException {
    try {
      if (!dapiConnector.isValidConnection()) {
        return null;
      }
      String url = dapiConnector.getLatestReleaseUrl(DAPI_DATABASE_NAME);
      if (url == null) {
        return null;
      }

      String content = dapiConnector
          .getAuthenticatedContent(
              url + "/drugs/?columns=sourceIdentifier&name=" + URLEncoder.encode(drugName, "UTF-8"));

      ListChemicalEntityDto result = objectMapper.readValue(content, ListChemicalEntityDto.class);
      List<Drug> drugs = fetchDrugs(result);

      if (drugs.size() == 0) {
        content = dapiConnector
            .getAuthenticatedContent(
                url + "/drugs/?columns=sourceIdentifier&synonym=" + URLEncoder.encode(drugName, "UTF-8"));
        result = objectMapper.readValue(content, ListChemicalEntityDto.class);
        drugs = fetchDrugs(result);
      } else if (drugs.size() > 1) {
        logger.warn("Found more than one drug by name: " + drugName);
      }
      if (drugs.size() > 0) {
        return drugs.get(0);
      }
      return null;
    } catch (final DapiConnectionException | IOException e) {
      throw new DrugSearchException("Problem with fetching drugbank data", e);
    }
  }

  private List<Drug> fetchDrugs(final ListChemicalEntityDto list) throws DapiConnectionException {
    List<Drug> result = new ArrayList<>();
    for (final ChemicalEntityDto entity : list.getContent()) {
      Drug drug = getByIdentifier(MiriamType.getMiriamDataFromPrefixIdentifier(entity.getSourceIdentifier()));
      if (drug == null) {
        logger.warn("Invalid drug identifier: " + entity.getSourceIdentifier());
      } else {
        result.add(drug);
      }
    }
    return result;
  }

  private Drug getByIdentifier(final MiriamData identifier) throws DapiConnectionException {
    String url = dapiConnector.getLatestReleaseUrl(DAPI_DATABASE_NAME);
    if (url == null) {
      return null;
    }
    String content = dapiConnector.getAuthenticatedContent(url + "/drugs/" + identifier.getResource());

    try {
      ChemicalEntityDto result = objectMapper.readValue(content, ChemicalEntityDto.class);

      return chemicalEntityDtoConverter.dtoToDrug(result);
    } catch (final IOException e) {
      throw new DapiConnectionException("Problem with accessing dapi", e);
    }
  }

  @Override
  public List<Drug> getDrugListByTarget(final MiriamData target, final Collection<MiriamData> organisms)
      throws DrugSearchException {
    if (target == null) {
      return new ArrayList<>();
    }
    if (!dapiConnector.isValidConnection()) {
      return new ArrayList<>();
    }
    List<MiriamData> targets = new ArrayList<>();
    if (target.getDataType().equals(MiriamType.HGNC_SYMBOL)) {
      try {
        targets.addAll(hgncAnnotator.hgncToUniprot(target));
      } catch (final AnnotatorException e) {
        logger.warn("Problem with converting to uniprot identifier", e);
        targets.add(target);
      }
    } else if (target.getDataType().equals(MiriamType.UNIPROT)) {
      targets.add(target);
    } else {
      throw new InvalidArgumentException(
          "Only those identifiers are accepted: " + MiriamType.HGNC_SYMBOL + "," + MiriamType.UNIPROT);
    }
    try {
      String url = dapiConnector.getLatestReleaseUrl(DAPI_DATABASE_NAME);
      if (url == null) {
        return null;
      }

      Map<MiriamData, Drug> result = new HashMap<>();

      for (final MiriamData t : targets) {
        String content = dapiConnector
            .getAuthenticatedContent(
                url + "/drugs/?size=1000&columns=sourceIdentifier&target_identifier="
                    + URLEncoder.encode(t.getPrefixedIdentifier(), "UTF-8"));

        ListChemicalEntityDto dto = objectMapper.readValue(content, ListChemicalEntityDto.class);

        List<Drug> drugs = fetchDrugs(dto);
        for (final Drug drug : drugs) {
          for (final Target drugTarget : drug.getTargets()) {
            if (organisms.size() == 0 || organisms.contains(drugTarget.getOrganism())) {
              result.put(drug.getSources().get(0), drug);
            }
          }
        }

      }
      return new ArrayList<>(result.values());
    } catch (final DapiConnectionException | IOException e) {
      throw new DrugSearchException("Problem with accessing DAPI", e);
    }
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus("DrugBank", MiriamType.DRUGBANK.getDbHomepage());

    QueryCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      Drug drug = findDrug("Amantadine");
      status.setStatus(ExternalServiceStatusType.OK);
      if (drug == null) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (final Exception e) {
      logger.error("DrugBank (DAPI) is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

}
