package lcsb.mapviewer.annotation.services.genome;

/**
 * Exception taht should be thrown when file for specific genome version id not
 * available.
 * 
 * @author Piotr Gawron
 *
 */
public class FileNotAvailableException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   * 
   * @param message
   *          error message
   */
  public FileNotAvailableException(final String message) {
    super(message);
  }

  /**
   * Default constructor.
   * 
   * @param cause
   *          error cause
   */
  public FileNotAvailableException(final Throwable cause) {
    super(cause);
  }

  /**
   * Default constructor.
   * 
   * @param message
   *          error message
   * @param cause
   *          error cause
   */
  public FileNotAvailableException(final String message, final Throwable cause) {
    super(message, cause);
  }
}
