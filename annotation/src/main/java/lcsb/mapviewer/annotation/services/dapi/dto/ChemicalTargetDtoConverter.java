package lcsb.mapviewer.annotation.services.dapi.dto;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.data.TargetType;
import lcsb.mapviewer.annotation.services.annotators.UniprotAnnotator;
import lcsb.mapviewer.annotation.services.annotators.UniprotSearchException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

@Service
public class ChemicalTargetDtoConverter {

  private static Logger logger = LogManager.getLogger();

  private UniprotAnnotator uniprotAnnotator;

  public ChemicalTargetDtoConverter(final UniprotAnnotator uniprotAnnotator) {
    this.uniprotAnnotator = uniprotAnnotator;
  }

  public Target dtoToTarget(final ChemicalTargetDto dto) {
    Target result = new Target();
    result.setName(dto.getName());
    result.setSource(MiriamType.getMiriamDataFromPrefixIdentifier(dto.getSourceIdentifier()));
    result.setAssociatedDisease(MiriamType.getMiriamDataFromPrefixIdentifier(dto.getAssociatedDisease()));
    result.setOrganism(MiriamType.getMiriamDataFromPrefixIdentifier(dto.getOrganism()));
    for (final String identifier : dto.getIdentifiers()) {
      MiriamData md = MiriamType.getMiriamDataFromPrefixIdentifier(identifier);
      if (md.getDataType().equals(MiriamType.UNIPROT)) {
        try {
          MiriamData hgnc = uniprotAnnotator.uniProtToHgnc(md);
          if (hgnc == null) {
            logger.warn("Cannot find hgnc symbol for uniprot: " + md);
          } else {
            md = hgnc;
          }
        } catch (final UniprotSearchException e) {
          logger.warn("Problem with translating uniprot id: " + md, e);
        }
      }
      result.addGene(md);
    }
    for (final String identifier : dto.getReferences()) {
      MiriamData md = MiriamType.getMiriamDataFromPrefixIdentifier(identifier);
      result.addReference(md);
    }
    if (result.getGenes().size() > 1) {
      result.setType(TargetType.PROTEIN_FAMILY);
    } else if (result.getGenes().size() == 1) {
      result.setType(TargetType.SINGLE_PROTEIN);
    } else {
      result.setType(TargetType.OTHER);
    }
    return result;
  }
}
