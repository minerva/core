package lcsb.mapviewer.annotation.services.genome;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.ExecutorService;

import lcsb.mapviewer.annotation.cache.BigFileCache;
import lcsb.mapviewer.annotation.cache.Cacheable;
import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.layout.ReferenceGenome;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeGeneMapping;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;

/**
 * Interface defining operation on single reference genome type database.
 * 
 * @author Piotr Gawron
 *
 */
public interface ReferenceGenomeConnector extends Cacheable {

  /**
   * Returns list of downloaded reference genomes. This list should contain list
   * of differente versions of reference genome from this database for given
   * organism.
   * 
   * @param organism
   *          organism for which list should be obtained
   * @return list of downloaded reference genomes
   */
  List<String> getDownloadedGenomeVersions(final MiriamData organism);

  /**
   * Returns list of available reference genomes for given organism.
   * 
   * @param organism
   *          organism for which list should be obtained
   * @return list of available reference genomes
   * @throws ReferenceGenomeConnectorException
   *           thrown when there is a problem with accessing external database
   */
  List<String> getAvailableGenomeVersion(final MiriamData organism) throws ReferenceGenomeConnectorException;

  /**
   * Returns list of reference genome organism available in this database.
   * 
   * @return list of reference genome organisms
   * @throws ReferenceGenomeConnectorException
   *           thrown when there is a problem with accessing external database
   */
  List<MiriamData> getAvailableOrganisms() throws ReferenceGenomeConnectorException;

  /**
   * Downloads reference genome with specified version.
   * 
   * @param organism
   *          organism for which reference genome should be downloaded
   * @param version
   *          version of reference genome
   * @param updater
   *          callback funtion that will be called to update information about
   *          progress
   * @param async
   *          should the download be performed asynchronously
   * @throws FileNotAvailableException
   *           thrown when file for given input reference genome is not available
   * @throws IOException
   *           thrown when there is a problem with downloading file
   * @throws ReferenceGenomeConnectorException
   *           thrown when there is a problem with genome connector
   */
  void downloadGenomeVersion(final MiriamData organism, final String version, final IProgressUpdater updater, final boolean async)
      throws FileNotAvailableException, IOException, ReferenceGenomeConnectorException;

  /**
   * Downloads reference genome with specified version.
   * 
   * @param organism
   *          organism for which reference genome should be downloaded
   * @param version
   *          version of reference genome
   * @param updater
   *          callback function that will be called to update information about
   *          progress
   * @param async
   *          should the download be performed asynchronously
   * @param url
   *          url that points to the reference genome
   * @throws IOException
   *           thrown when there is a problem with downloading file
   * @throws URISyntaxException
   *           thrown when url is invalid
   * @throws ReferenceGenomeConnectorException
   *           thrown when there is a problem with genome connector
   */
  void downloadGenomeVersion(final MiriamData organism, final String version, final IProgressUpdater updater, final boolean async, final String url)
      throws IOException, URISyntaxException, ReferenceGenomeConnectorException;

  /**
   * Downloads gene mapping for reference genome.
   * 
   * @param referenceGenome
   *          reference genome for which gene mapping is downloaded
   * @param updater
   *          callback function that will be called to update information about
   *          progress
   * @param async
   *          should the download be performed asynchronously
   * @param name
   *          name of the mapping
   * @param url
   *          url that points to the reference genome
   * @throws IOException
   *           thrown when there is a problem with downloading file
   * @throws URISyntaxException
   *           thrown when url is invalid
   * @throws ReferenceGenomeConnectorException
   *           thrown when there is problem with accessing external database
   */
  void downloadGeneMappingGenomeVersion(final ReferenceGenome referenceGenome, final String name, final IProgressUpdater updater,
      final boolean async, final String url)
      throws IOException, URISyntaxException, ReferenceGenomeConnectorException;

  /**
   * Removes reference genome.
   * 
   * @param organism
   *          organism that defines reference genome
   * @param version
   *          version of the reference genome
   * @throws IOException
   *           thrown when there is a problem with removing file
   */
  void removeGenomeVersion(final MiriamData organism, final String version) throws IOException;

  /**
   * Returns url to the file that describes reference genome.
   * 
   * @param organism
   *          organism of reference genome
   * @param version
   *          version of the reference genome
   * @return url to the file that describes reference genome
   * @throws FileNotAvailableException
   *           thrown when file describing reference genome cannot be found
   */
  String getGenomeVersionFile(final MiriamData organism, final String version) throws FileNotAvailableException, IOException;

  /**
   * Removes gene mapping from reference genome.
   * 
   * @param mapping
   *          mapping to be removed
   * @throws IOException
   *           thrown when removing of a file encountered some problems
   */

  void removeGeneMapping(final ReferenceGenomeGeneMapping mapping) throws IOException;

  List<ReferenceGenome> getByType(ReferenceGenomeType ucsc);

  void delete(ReferenceGenome referenceGenome);

  void add(ReferenceGenome referenceGenome);

  ReferenceGenome getReferenceGenomeById(int id);

  void update(ReferenceGenome referenceGenome);

  void update(ReferenceGenomeGeneMapping temp);

  // ------------------------

  TaxonomyBackend getTaxonomyBackend();

  void setTaxonomyBackend(TaxonomyBackend taxonomyMock);

  void setSyncExecutorService(ExecutorService executorService);

  ExecutorService getSyncExecutorService();

  BigFileCache getBigFileCache();

  void setBigFileCache(BigFileCache mockCache);

  int extractInt(String string);

  ReferenceGenomeGeneMapping getReferenceGenomeGeneMappingById(int id);

  void waitForDownloadsToFinish() throws InterruptedException;

}
