package lcsb.mapviewer.annotation.services;

/**
 * This class represents status of the service that access data from external
 * resources (database, webpage, etc.).
 * 
 * @author Piotr Gawron
 * 
 */
public class ExternalServiceStatus {

  /**
   * Name of the service.
   */
  private String name;

  /**
   * Status of the external resource.
   */
  private ExternalServiceStatusType status;

  /**
   * Webpage associated with external resource.
   */
  private String page;

  /**
   * Default constructor.
   * 
   * @param name
   *          name of the service
   * @param page
   *          webpage associated with the external resource
   */
  public ExternalServiceStatus(final String name, final String page) {
    this.setName(name);
    this.setPage(page);
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * @return the status
   * @see #status
   */
  public ExternalServiceStatusType getStatus() {
    return status;
  }

  /**
   * @param status
   *          the status to set
   * @see #status
   */
  public void setStatus(final ExternalServiceStatusType status) {
    this.status = status;
  }

  /**
   * @return the page
   * @see #page
   */
  public String getPage() {
    return page;
  }

  /**
   * @param page
   *          the page to set
   * @see #page
   */
  public void setPage(final String page) {
    this.page = page;
  }

}
