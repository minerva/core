package lcsb.mapviewer.annotation.services.dapi;

/**
 * Exception thrown when there was a problem when searching for a chemical.
 * 
 * @author Piotr Gawron
 * 
 */
public class ChemicalSearchException extends Exception {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   *
   * @param string
   *          exception message
   */
  public ChemicalSearchException(final String string) {
    super(string);
  }

  /**
   * Default constructor.
   *
   * @param e
   *          parent exception
   */
  public ChemicalSearchException(final Exception e) {
    super(e);
  }

  /**
   * Default constructor.
   *
   * @param message
   *          exception message
   * @param e
   *          source exception
   */
  public ChemicalSearchException(final String message, final Exception e) {
    super(message, e);
  }

}
