package lcsb.mapviewer.annotation.services.annotators;

/**
 * This is a class that implements a backend to the EBI's PDB SIFTS REST API
 * mapping.
 * 
 * @author David Hoksza
 * 
 */
public interface PdbAnnotator extends IElementAnnotator {
}
