package lcsb.mapviewer.annotation.services;

import lcsb.mapviewer.annotation.cache.QueryCacheInterface;
import lcsb.mapviewer.annotation.cache.SourceNotAvailable;
import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.data.Target;
import lcsb.mapviewer.annotation.data.TargetType;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.annotators.HgncAnnotator;
import lcsb.mapviewer.annotation.services.annotators.UniprotAnnotator;
import lcsb.mapviewer.annotation.services.annotators.UniprotSearchException;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import org.apache.commons.lang3.SerializationException;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class used for accessing and parsing data from chembl database using
 * <a href="https://www.ebi.ac.uk/chembl/ws">ChEMBL Data Web Services</a>.
 * Parser was initially developed by Janek, but after new API released by ebi it
 * was refactored by Piotr.
 *
 * @author Piotr Gawron
 */
@Service
@Transactional
public class ChEMBLParserImpl extends DrugAnnotationImpl implements ChEMBLParser {

  /**
   * Prefix used in key for storing data by name.
   */
  static final String NAME_PREFIX = "drug:";
  /**
   * Url that access data of drug identified by drug chembl identifier.
   */
  private static final String DRUG_BY_DRUG_ID_API_URL = "https://www.ebi.ac.uk/chembl/api/data/molecule?molecule_chembl_id=";
  /**
   * Url that access data about targetting mechanisms for a give drug chembl
   * identifier.
   */
  private static final String TARGET_MECHANISM_BY_DRUG_ID_API_URL = "https://www.ebi.ac.uk/chembl/api/data/mechanism?molecule_chembl_id=";
  /**
   * Url that access data about drugs identified by drug target identifier.
   */
  private static final String DRUG_BY_TARGET_ID_API_URL = "https://www.ebi.ac.uk/chembl/api/data/mechanism?target_chembl_id=";
  /**
   * Url that access data of drug identified by name.
   */
  private static final String DRUG_NAME_API_URL = "https://www.ebi.ac.uk/chembl/api/data/molecule?pref_name__exact=";

  /**
   * Url that access data of drug identified by name.
   */
  private static final String DRUG_SYNONYM_API_URL = "https://www.ebi.ac.uk/chembl/api/data/molecule?molecule_synonyms__molecule_synonym__iexact=";

  /**
   * Url that access target by identifier.
   */
  private static final String TARGET_API_URL = "https://www.ebi.ac.uk/chembl/api/data/target/";
  /**
   * Url that list of all child nodes for chembl ontology term.
   */
  private static final String PARENT_CHILD_API_URL = "https://www.ebi.ac.uk/chembl/api/data/molecule_form/";
  /**
   * Suffix that should be added to {@link #PARENT_CHILD_API_URL}.
   */
  private static final String PARENT_CHILD_API_URL_SUFFIX = ".xml";
  /**
   * Url used for finding targets containing uniprot identifiers. Important - for
   * now the limit of targets is 1000 so if more targets will be found this link
   * will not provide them.
   */
  private static final String URL_TARGET_FROM_UNIPROT = "https://www.ebi.ac.uk/chembl/api/data/target?limit=1000&target_components__accession=";
  /**
   * Length of the prefix used in key for storing data by name.
   */
  private static final int NAME_PREFIX_LENGTH = NAME_PREFIX.length();

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  /**
   * Service used for annotation of proteins using {@link MiriamType#UNIPROT
   * uniprot}.
   */
  private UniprotAnnotator uniprotAnnotator;

  /**
   * Service used for annotation of proteins (and finding annotation of proteins).
   */
  private HgncAnnotator hgncAnnotator;

  private final DoubleComparator doubleComparator = new DoubleComparator();

  /**
   * Constructor.
   */
  @Autowired
  public ChEMBLParserImpl(final UniprotAnnotator uniprotAnnotator, final HgncAnnotator hgncAnnotator) {
    super(ChEMBLParser.class);
    this.uniprotAnnotator = uniprotAnnotator;
    this.hgncAnnotator = hgncAnnotator;
  }

  /**
   * Returns target for given chembl identifier.
   *
   * @param md {@link MiriamType#CHEMBL_TARGET chembl identifier} of the target
   * @return target
   * @throws DrugSearchException thrown when there are problems with connection to ChEMBL database
   */
  @Override
  public Target getTargetFromId(final MiriamData md) throws DrugSearchException {
    if (!md.getDataType().equals(MiriamType.CHEMBL_TARGET)) {
      throw new InvalidArgumentException(
          "Target must be of a type: " + MiriamType.CHEMBL_TARGET + ". But found: " + md.getDataType());
    }
    if (md.getResource().isEmpty()) {
      throw new InvalidArgumentException("Invalid target id" + md);
    }
    try {
      Target target = new Target();
      target.setSource(md);

      String accessUrl = TARGET_API_URL + md.getResource();
      String inputLine = getWebPageContent(accessUrl);

      Document document = XmlParser.getXmlDocumentFromString(inputLine);

      Node response = XmlParser.getNode("target", document.getChildNodes());

      NodeList list = response.getChildNodes();

      for (int i = 0; i < list.getLength(); i++) {
        Node node = list.item(i);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          if (node.getNodeName().equalsIgnoreCase("organism")) {
            // nothing to do here, we use taxonomy id
          } else if (node.getNodeName().equalsIgnoreCase("tax_id")) {
            String value = node.getTextContent();
            MiriamData taxonomy = new MiriamData(MiriamType.TAXONOMY, value);
            String organismName = getTaxonomyBackend().getNameForTaxonomy(taxonomy);
            if (organismName != null) {
              target.setOrganism(taxonomy);
            }
          } else if (node.getNodeName().equalsIgnoreCase("pref_name")) {
            // node with information about name
            target.setName(node.getTextContent());
          } else if (node.getNodeName().equalsIgnoreCase("target_chembl_id")) {
            // identifier of the element
            if (!node.getTextContent().equalsIgnoreCase(md.getResource())) {
              logger.warn("Problem with data returned by API. We asked for target: " + md.getResource()
                  + ", but got results for: " + node.getNodeName());
            }
          } else if (node.getNodeName().equalsIgnoreCase("target_type")) {
            // type of target
            String tmp = node.getTextContent();
            if (tmp.equals("SINGLE PROTEIN")) {
              target.setType(TargetType.SINGLE_PROTEIN);
            } else if (tmp.equals("PROTEIN FAMILY")) {
              target.setType(TargetType.PROTEIN_FAMILY);
            } else if (tmp.equals("PROTEIN COMPLEX")) {
              target.setType(TargetType.COMPLEX_PROTEIN);
            } else {
              target.setType(TargetType.OTHER);
            }
          } else if (node.getNodeName().equalsIgnoreCase("target_components")) {
            for (int j = 0; j < node.getChildNodes().getLength(); j++) {
              Node tmpNode = node.getChildNodes().item(j);
              if (tmpNode.getNodeType() == Node.ELEMENT_NODE) {
                MiriamData targetElement = targetComponentToMiriamData(tmpNode);
                if (targetElement != null) {
                  target.addGene(targetElement);
                } else {
                  logger.warn("Problem with processing target component: " + XmlParser.nodeToString(tmpNode));
                }
              }
            }

          } else if (node.getNodeName().equalsIgnoreCase("species_group_flag")) {
            // nothing to do here
            continue;
          } else if (node.getNodeName().equalsIgnoreCase("cross_references")) {
            // nothing to do here
            continue;
          } else {
            logger.warn("Unknown node: " + node.getNodeName() + ";" + node.getTextContent());
          }
        }
      }

      return target;
    } catch (final InvalidXmlSchemaException e) {
      throw new DrugSearchException("Problem with accessing information about target: " + md, e);
    } catch (final IOException e) {
      throw new DrugSearchException("Problem with accessing information about target: " + md, e);
    } catch (final TaxonomySearchException e) {
      throw new DrugSearchException("Problem with accessing information about target: " + md, e);
    }
  }

  /**
   * Parse node of target component to get information about
   * {@link MiriamType#HGNC_SYMBOL} identifier.
   *
   * @param targetComponent xml node to parse
   * @return {@link MiriamData} with {@link MiriamType#HGNC_SYMBOL} identifier that could be extracted from input data
   * @throws DrugSearchException thrown when there is a problem with annotations
   */
  @Override
  public MiriamData targetComponentToMiriamData(final Node targetComponent) throws DrugSearchException {
    MiriamData result = null;
    Node uniprotAccessionId = XmlParser.getNode("accession", targetComponent.getChildNodes());
    if (uniprotAccessionId != null) {
      String uniprotId = uniprotAccessionId.getTextContent();
      if (uniprotId != null && !uniprotId.isEmpty()) {
        try {
          result = uniprotAnnotator.uniProtToHgnc(new MiriamData(MiriamType.UNIPROT, uniprotId));
          if (result != null) {
            result.setAnnotator(null);
          }
        } catch (final UniprotSearchException e) {
          throw new DrugSearchException(e);
        }
      }
    }
    return result;
  }

  /**
   * Returns list of targets for given drug.
   *
   * @param drugId identifier of a drug for which targets are looked
   * @return list of targets for given drug
   * @throws DrugSearchException thrown when there are problems with connection to ChEMBL database
   */
  @Override
  public List<Target> getTargetsByDrugId(final MiriamData drugId) throws DrugSearchException {
    if (!MiriamType.CHEMBL_COMPOUND.equals(drugId.getDataType())) {
      throw new InvalidArgumentException("Only " + MiriamType.CHEMBL_COMPOUND + " are accepted");
    }
    List<Target> targets = new ArrayList<>();
    try {
      String url = TARGET_MECHANISM_BY_DRUG_ID_API_URL + drugId.getResource();
      String page = getWebPageContent(url);
      Document document = XmlParser.getXmlDocumentFromString(page);

      Node response = XmlParser.getNode("response", document);
      Node mechanisms = XmlParser.getNode("mechanisms", response);
      NodeList nodeList = mechanisms.getChildNodes();
      for (int i = 0; i < nodeList.getLength(); i++) {
        Node node = nodeList.item(i);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          if (node.getNodeName().equalsIgnoreCase("mechanism")) {
            Node chemblTargetIdNode = XmlParser.getNode("target_chembl_id", node);
            String chemblTargetId = chemblTargetIdNode.getTextContent();
            if (chemblTargetId != null && !chemblTargetId.isEmpty()) {
              MiriamData targetMiriam = new MiriamData(MiriamType.CHEMBL_TARGET, chemblTargetId);

              Target target = getTargetFromId(targetMiriam);
              Node referenceNode = XmlParser.getNode("mechanism_refs", node);

              Set<MiriamData> references = parseReferences(referenceNode);
              target.addReferences(references);
              targets.add(target);
            }
          }
        }
      }
      return targets;
    } catch (final IOException e) {
      throw new DrugSearchException("Problem with accessing DrugBank database", e);
    } catch (final InvalidXmlSchemaException e) {
      throw new DrugSearchException("Problem with parsing DrugBank database response", e);
    }
  }

  /**
   * Parse xml node with references.
   *
   * @param referenceNode node with references
   * @return list of references obtained from xml node
   */
  @Override
  public Set<MiriamData> parseReferences(final Node referenceNode) {
    Set<MiriamData> result = new HashSet<>();
    if (referenceNode != null) {
      NodeList nodeList = referenceNode.getChildNodes();
      for (int i = 0; i < nodeList.getLength(); i++) {
        Node node = nodeList.item(i);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          if (node.getNodeName().equalsIgnoreCase("mechanism")) {
            Node typeNode = XmlParser.getNode("ref_type", node);
            if ("PubMed".equalsIgnoreCase(typeNode.getTextContent())) {
              Node idNode = XmlParser.getNode("ref_id", node);
              String id = idNode.getTextContent().trim();
              result.add(new MiriamData(MiriamType.PUBMED, id));
            }
          } else {
            logger.warn("Unknown node: {}", node.getNodeName());
          }
        }
      }
    }
    return result;
  }

  @Override
  public Drug findDrug(final String inputName) throws DrugSearchException {
    String name = inputName.toUpperCase();

    String query = NAME_PREFIX + name.toLowerCase().trim();

    try {
      Drug drug = getDrugSerializer().xmlToObject(getCacheNode(query));
      if (drug != null) {
        return drug;
      }
    } catch (final SerializationException e) {
      logger.error(e, e);
    }

    try {
      name = URLEncoder.encode(name, "UTF-8");
      String accessUrl = DRUG_NAME_API_URL + name;
      String page = getWebPageContent(accessUrl);

      Document document = XmlParser.getXmlDocumentFromString(page);
      List<Drug> drugs = extractDrugsFromDocument(document);
      if (drugs.size() == 0) {
        accessUrl = DRUG_SYNONYM_API_URL + name;
        page = getWebPageContent(accessUrl);
        document = XmlParser.getXmlDocumentFromString(page);
        drugs = extractDrugsFromDocument(document);
      }
      if (drugs.size() > 1) {
        logger.warn("More drugs than one found for query: " + query);
      }

      if (drugs.size() == 0) {
        return null;
      }
      setCacheValue(query, getDrugSerializer().objectToString(drugs.get(0)));
      return drugs.get(0);
    } catch (final IOException e) {
      throw new DrugSearchException("Problem with accessing CHEMBL database", e);
    } catch (final InvalidXmlSchemaException e) {
      throw new DrugSearchException("Problem with parsing data from CHEMBL database", e);
    }
  }

  private List<Drug> extractDrugsFromDocument(final Document document) throws DrugSearchException {
    List<Drug> result = new ArrayList<>();
    Node response = XmlParser.getNode("response", document.getChildNodes());
    Node molecules = XmlParser.getNode("molecules", response.getChildNodes());

    NodeList list = molecules.getChildNodes();

    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("molecule")) {
          result.add(parseDrug(node));
        }
      }
    }
    return result;
  }

  @Override
  public List<Drug> getDrugListByTarget(final MiriamData target, final Collection<MiriamData> organisms)
      throws DrugSearchException {
    List<Drug> result = new ArrayList<>();
    List<MiriamData> uniprotData = new ArrayList<>();
    if (MiriamType.HGNC_SYMBOL.equals(target.getDataType())) {
      try {
        uniprotData = hgncAnnotator.hgncToUniprot(target);
      } catch (final AnnotatorException e) {
        throw new DrugSearchException("Cannot transform target into uniprot identifier", e);
      }
    } else if (MiriamType.UNIPROT.equals(target.getDataType())) {
      uniprotData.add(target);
    } else {
      throw new DrugSearchException("Don't know how to process target of " + target.getDataType() + " type.");
    }
    if (uniprotData.size() == 0) {
      return result;
    } else if (uniprotData.size() > 1) {
      logger.warn("Too many uniprot ids. Only first will be taken");
    }
    try {
      String url = URL_TARGET_FROM_UNIPROT + uniprotData.get(0).getResource();

      String page = getWebPageContent(url);
      Set<String> drugNames = new HashSet<>();

      Document document = XmlParser.getXmlDocumentFromString(page);

      Node response = XmlParser.getNode("response", document);
      Node targets = XmlParser.getNode("targets", response);
      NodeList nodeList = targets.getChildNodes();
      for (int i = 0; i < nodeList.getLength(); i++) {
        Node node = nodeList.item(i);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          if (node.getNodeName().equalsIgnoreCase("target")) {
            String organismName = XmlParser.getNode("organism", node).getTextContent();
            if (organismMatch(organismName, organisms)) {
              Node chemblTargetIdNode = XmlParser.getNode("target_chembl_id", node);
              MiriamData targetMiriam = new MiriamData(MiriamType.CHEMBL_TARGET, chemblTargetIdNode.getTextContent());
              List<Drug> drugs = getDrugsByChemblTarget(targetMiriam);
              for (final Drug drug : drugs) {
                // don't add duplicates
                if (!drugNames.contains(drug.getName())) {
                  drugNames.add(drug.getName());
                  result.add(drug);
                }
              }
            }
          }
        }
      }

    } catch (final IOException e) {
      throw new DrugSearchException("Problem with accessing Chembl database", e);
    } catch (final InvalidXmlSchemaException e) {
      throw new DrugSearchException("Problem with parsing Chembl response", e);
    }

    return result;

  }

  @Override
  public String refreshCacheQuery(final Object query) throws SourceNotAvailable {
    String result = null;
    try {
      if (query instanceof String) {
        String name = (String) query;
        if (name.startsWith(NAME_PREFIX)) {
          name = name.substring(NAME_PREFIX_LENGTH);
          result = getDrugSerializer().objectToString(findDrug(name));
        } else {
          result = super.refreshCacheQuery(query);
        }
      } else {
        result = super.refreshCacheQuery(query);
      }
    } catch (final DrugSearchException e) {
      throw new SourceNotAvailable(e);
    }
    return result;
  }

  /**
   * Parse xml node representing drug and returns drug with information obtained
   * from there and other places in chembl API.
   *
   * @param node xml node representing drug, sample can be found <a href=
   *             "https://www.ebi.ac.uk/chembl/api/data/molecule?molecule_chembl_id=CHEMBL2068724">
   *             here</a> (molecule subnode):
   * @return {@link Drug} obtained from xml node
   * @throws DrugSearchException thrown when there is a problem with accessing chembl database
   */
  private Drug parseDrug(final Node node) throws DrugSearchException {
    Drug drug = new Drug();
    Node nameNode = XmlParser.getNode("pref_name", node);
    Node phaseNode = XmlParser.getNode("max_phase", node);
    Node idNode = XmlParser.getNode("molecule_chembl_id", node);

    drug.setName(nameNode.getTextContent());
    MiriamData drugId = new MiriamData(MiriamType.CHEMBL_COMPOUND, idNode.getTextContent());
    drug.addSource(drugId);

    drug.setDescription(null);
    Node synonymsNode = XmlParser.getNode("molecule_synonyms", node);

    drug.setSynonyms(parseSynonymsNode(synonymsNode));
    drug.addTargets(getTargetsByDrugId(drugId));
    if (NumberUtils.isCreatable(phaseNode.getTextContent())) {
      drug.setApproved(doubleComparator.compare(4.0, Double.parseDouble(phaseNode.getTextContent())) == 0);
    } else {
      drug.setApproved(false);
    }
    drug.addTargets(getTargetsForChildElements(drug.getSources().get(0)));

    return drug;
  }

  /**
   * Get targets for chembl children nodes.
   *
   * @param drugId id of the parent drug
   * @return list of drug targets for children molecules in chembl ontology
   * @throws DrugSearchException thrown when there is a problemw ith processing request
   */
  @Override
  public List<Target> getTargetsForChildElements(final MiriamData drugId) throws DrugSearchException {
    List<Target> targets = new ArrayList<>();
    try {
      String id = drugId.getResource();
      String query = PARENT_CHILD_API_URL + id + PARENT_CHILD_API_URL_SUFFIX;
      String page = getWebPageContent(query);

      Document document = XmlParser.getXmlDocumentFromString(page);

      Node response = XmlParser.getNode("response", document);
      Node forms = XmlParser.getNode("molecule_forms", response);
      NodeList list = forms.getChildNodes();
      for (int i = 0; i < list.getLength(); i++) {
        Node node = list.item(i);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          if (node.getNodeName().equalsIgnoreCase("molecule_form")) {
            Node idNode = XmlParser.getNode("molecule_chembl_id", node);
            if (!idNode.getTextContent().equals(id)) {
              targets.addAll(getTargetsByDrugId(new MiriamData(MiriamType.CHEMBL_COMPOUND, idNode.getTextContent())));
            }
          } else {
            logger.warn("Unknown node type: " + node.getNodeName());
          }
        }
      }
      return targets;
    } catch (final IOException e) {
      throw new DrugSearchException("Problem with accessing CHEMBL database", e);
    } catch (final InvalidXmlSchemaException e) {
      throw new DrugSearchException("Problem with parsing data from CHEMBL database", e);
    }
  }

  /**
   * Parse xml node with synonyms.
   *
   * @param synonymsNode xml node with synonyms
   * @return list of synonyms obtained from the input xml
   */
  @Override
  public List<String> parseSynonymsNode(final Node synonymsNode) {
    List<String> result = new ArrayList<>();
    NodeList list = synonymsNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("synonym")) {
          Node synonymNode = XmlParser.getNode("synonyms", node);
          result.add(synonymNode.getTextContent());
        } else {
          logger.warn("Unknown node: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Returns information about drug by chembl drug identifier.
   *
   * @param drugId {@link MiriamType#CHEMBL_COMPOUND chembl drug identifier}
   * @return drug by chembl drug identifier
   * @throws DrugSearchException thrown when there is a problem with accessing data about drug
   */
  @Override
  public Drug getDrugById(final MiriamData drugId) throws DrugSearchException {
    if (!MiriamType.CHEMBL_COMPOUND.equals(drugId.getDataType())) {
      throw new InvalidArgumentException("Only " + MiriamType.CHEMBL_COMPOUND + " input data is accepted.");
    }
    try {
      String content = getWebPageContent(DRUG_BY_DRUG_ID_API_URL + drugId.getResource());
      Document document = XmlParser.getXmlDocumentFromString(content);

      Node response = XmlParser.getNode("response", document);
      if (response == null) {
        throw new DrugSearchException("Invalid source XML for drug: " + drugId);
      }

      Node pageMetaNode = XmlParser.getNode("page_meta", response);
      if (pageMetaNode == null) {
        throw new DrugSearchException("Invalid source XML for drug: " + drugId);
      }
      Node totalCount = XmlParser.getNode("total_count", pageMetaNode);
      if (totalCount == null) {
        throw new DrugSearchException("Invalid source XML for drug: " + drugId);
      }
      if (totalCount.getTextContent().trim().isEmpty()) {
        return null;
      }


      Node moleculesNode = XmlParser.getNode("molecules", response);
      if (moleculesNode == null) {
        throw new DrugSearchException("Invalid source XML for drug: " + drugId);
      }
      Node moleculeNode = XmlParser.getNode("molecule", moleculesNode);
      if (moleculeNode == null) {
        throw new DrugSearchException("Invalid source XML for drug: " + drugId);
      }
      return parseDrug(moleculeNode);
    } catch (final IOException e) {
      throw new DrugSearchException("Problem with accessing external database for id: " + drugId, e);
    } catch (final InvalidXmlSchemaException e) {
      throw new DrugSearchException("Problem with parsing database result for id: " + drugId, e);
    }
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus("ChEMBL", MiriamType.CHEMBL_COMPOUND.getDbHomepage());

    QueryCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      Drug drug = findDrug("Amantadine");
      status.setStatus(ExternalServiceStatusType.OK);
      if (drug == null) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      } else if (drug.getName().isEmpty()) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (final Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  /**
   * Returns list of drugs found by target identified in the parameter.
   *
   * @param targetMiriam {@link MiriamType#CHEMBL_TARGET chembl target identifier}
   * @return list of drugs found by target identified in the parameter
   * @throws DrugSearchException thrown when there is a problem with accessing chembl database
   */
  @Override
  public List<Drug> getDrugsByChemblTarget(final MiriamData targetMiriam) throws DrugSearchException {
    try {
      List<Drug> result = new ArrayList<>();
      String url = DRUG_BY_TARGET_ID_API_URL + targetMiriam.getResource();
      String page = getWebPageContent(url);
      Document document = XmlParser.getXmlDocumentFromString(page);

      Node response = XmlParser.getNode("response", document);
      Node mechanisms = XmlParser.getNode("mechanisms", response);
      NodeList nodeList = mechanisms.getChildNodes();
      for (int i = 0; i < nodeList.getLength(); i++) {
        Node node = nodeList.item(i);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          if (node.getNodeName().equalsIgnoreCase("mechanism")) {
            Node chemblMoleculeIdNode = XmlParser.getNode("molecule_chembl_id", node);
            MiriamData drugId = new MiriamData(MiriamType.CHEMBL_COMPOUND, chemblMoleculeIdNode.getTextContent());
            Drug drug = getDrugById(drugId);
            if (drug != null) {
              result.add(drug);
            }
          }
        }
      }
      return result;
    } catch (final IOException e) {
      throw new DrugSearchException("Problem with accessing Chembl database", e);
    } catch (final InvalidXmlSchemaException e) {
      throw new DrugSearchException("Problem with parsing Chembl response", e);
    }

  }

  /**
   * @return the uniprotAnnotator
   * @see #uniprotAnnotator
   */
  public UniprotAnnotator getUniprotAnnotator() {
    return uniprotAnnotator;
  }

  /**
   * @param uniprotAnnotator the uniprotAnnotator to set
   * @see #uniprotAnnotator
   */
  public void setUniprotAnnotator(final UniprotAnnotator uniprotAnnotator) {
    this.uniprotAnnotator = uniprotAnnotator;
  }

  /**
   * @return the hgncAnnotator
   * @see #hgncAnnotator
   */
  public HgncAnnotator getHgncAnnotator() {
    return hgncAnnotator;
  }

  /**
   * @param hgncAnnotator the hgncAnnotator to set
   * @see #hgncAnnotator
   */
  public void setHgncAnnotator(final HgncAnnotator hgncAnnotator) {
    this.hgncAnnotator = hgncAnnotator;
  }

}
