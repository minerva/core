package lcsb.mapviewer.annotation.services;

/**
 * Exception thrown when there was a problem when searching for a pubmed data.
 * 
 * @author Piotr Gawron
 * 
 */
public class PubmedSearchException extends Exception {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   *
   * @param e
   *          parent exception
   */
  public PubmedSearchException(final Exception e) {
    super(e);
  }

  /**
   * Default constructor.
   *
   * @param message
   *          exception message
   * @param e
   *          source exception
   */
  public PubmedSearchException(final String message, final Exception e) {
    super(message, e);
  }

}
