package lcsb.mapviewer.annotation.services.annotators;

import lcsb.mapviewer.annotation.cache.QueryCacheInterface;
import lcsb.mapviewer.annotation.services.ExternalServiceStatus;
import lcsb.mapviewer.annotation.services.ExternalServiceStatusType;
import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.model.user.annotator.AnnotatorInputParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CazyAnnotatorImpl extends ElementAnnotator implements CazyAnnotator {

  private final TairAnnotator tairAnnotator;

  /**
   * Pattern used for finding UniProt symbol from TAIR info page .
   */
  private final Pattern cazyIdMatcher = Pattern.compile("\\/((GT|GH|PL|CE|CBM)\\d+(\\_\\d+)?)\\.html");

  @Autowired
  public CazyAnnotatorImpl(final TairAnnotator tairAnnotator) {
    super(CazyAnnotator.class, new Class[]{Protein.class, Gene.class, Rna.class}, false);
    this.tairAnnotator = tairAnnotator;
  }

  @Override
  public ExternalServiceStatus getServiceStatus() {
    ExternalServiceStatus status = new ExternalServiceStatus(getCommonName(), getUrl());

    QueryCacheInterface cacheCopy = getCache();
    this.setCache(null);

    try {
      MiriamData md = uniprotToCazy(getExampleValidAnnotation(), new LogMarker(ProjectLogEntryType.OTHER, new GenericProtein("")));

      status.setStatus(ExternalServiceStatusType.OK);
      if (md == null || !md.getResource().equalsIgnoreCase("GH5_7")) {
        status.setStatus(ExternalServiceStatusType.CHANGED);
      }
    } catch (final Exception e) {
      logger.error(status.getName() + " is down", e);
      status.setStatus(ExternalServiceStatusType.DOWN);
    }
    this.setCache(cacheCopy);
    return status;
  }

  @Override
  public boolean annotateElement(final BioEntityProxy object, final MiriamData identifier, final AnnotatorData parameters)
      throws AnnotatorException {
    List<MiriamData> mdUniprots = new ArrayList<>();
    if (identifier.getDataType().equals(MiriamType.TAIR_LOCUS)) {
      mdUniprots.addAll(tairAnnotator.tairToUniprot(identifier));
    } else if (identifier.getDataType().equals(MiriamType.UNIPROT)) {
      mdUniprots.add(identifier);
    } else {
      throw new NotImplementedException();
    }
    List<String> cazyIds = new ArrayList<String>();
    for (final MiriamData mdUniprot : mdUniprots) {
      MiriamData mdCazy = uniprotToCazy(mdUniprot, object.getLogMarker(ProjectLogEntryType.OTHER));
      if (mdCazy != null && !cazyIds.contains(mdCazy.getResource())) {
        cazyIds.add(mdCazy.getResource());
        object.addMiriamData(mdCazy);
      }
    }
    return !cazyIds.isEmpty();
  }

  @Override
  public String getCommonName() {
    return MiriamType.CAZY.getCommonName();
  }

  @Override
  public String getUrl() {
    return MiriamType.CAZY.getDbHomepage();
  }

  @Override
  public List<AnnotatorInputParameter> getAvailableInputParameters() {
    return Arrays.asList(new AnnotatorInputParameter(MiriamType.TAIR_LOCUS),
        new AnnotatorInputParameter(MiriamType.UNIPROT));
  }

  @Override
  public List<AnnotatorOutputParameter> getAvailableOuputProperties() {
    return Collections.singletonList(new AnnotatorOutputParameter(MiriamType.CAZY));
  }

  @Override
  public MiriamData getExampleValidAnnotation() {
    return new MiriamData(MiriamType.UNIPROT, "Q00012");
  }

  /**
   * Returns URL to TAIR page about TAIR entry.
   *
   * @param uniProtId UniProt identifier
   * @return URL to CAZY UniProt accession search result page
   */
  private String getCazyUrl(final String uniProtId) {
    return "https://www.cazy.org/search?page=recherche&recherche=" + uniProtId + "&tag=10";
  }

  /**
   * Parse CAZy webpage to find information about {@link MiriamType#CAZY} and
   * returns them.
   *
   * @param pageContent CAZy info page
   * @return CAZy family identifier found on the page
   */
  private Collection<MiriamData> parseCazy(final String pageContent) {
    Collection<MiriamData> result = new HashSet<>();
    Matcher m = cazyIdMatcher.matcher(pageContent);
    if (m.find()) {
      result.add(new MiriamData(MiriamType.CAZY, m.group(1)));
    }
    return result;
  }

  /**
   * Transform UniProt identifier to CAZy identifier.
   *
   * @param uniprot {@link MiriamData} with UniProt identifier
   * @return {@link MiriamData} with CAZy identifier
   * @throws AnnotatorException thrown when there is a problem with accessing external database
   */
  @Override
  public MiriamData uniprotToCazy(final MiriamData uniprot, final LogMarker marker) throws AnnotatorException {
    if (uniprot == null) {
      return null;
    }

    if (!MiriamType.UNIPROT.equals(uniprot.getDataType())) {
      throw new InvalidArgumentException(MiriamType.UNIPROT + " expected.");
    }

    String accessUrl = getCazyUrl(uniprot.getResource());
    try {
      String pageContent = getWebPageContent(accessUrl);
      Collection<MiriamData> collection = parseCazy(pageContent);
      if (!collection.isEmpty()) {
        return collection.iterator().next();
      } else {
        if (marker != null) {
          marker.getEntry().setType(ProjectLogEntryType.INVALID_IDENTIFIER);
        }
        logger.warn(marker, "Cannot find CAZy data for UniProt id: " + uniprot.getResource());
        return null;
      }
    } catch (final WrongResponseCodeIOException exception) {
      if (marker != null) {
        marker.getEntry().setType(ProjectLogEntryType.OTHER);
      }
      logger.warn(marker, "Wrong response code when retrieving CAZy data for UniProt id: " + uniprot.getResource());
      return null;
    } catch (final IOException exception) {
      throw new AnnotatorException(exception);
    }
  }
}
