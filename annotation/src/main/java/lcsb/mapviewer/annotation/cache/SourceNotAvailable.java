package lcsb.mapviewer.annotation.cache;

/**
 * Exception thrown when source of the data that is stored in database is no
 * longer available.
 * 
 * @author Piotr Gawron
 * 
 */
public class SourceNotAvailable extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Public constructor with parent exception that was catched.
   * 
   * @param e
   *          parent exception
   */
  public SourceNotAvailable(final Exception e) {
    super(e);
  }

  /**
   * Constructor receives some kind of message.
   * 
   * @param message
   *          message associated with exception
   */
  public SourceNotAvailable(final String message) {
    super(message);
  }

  /**
   * Public constructor with parent exception that was catched.
   * 
   * @param message
   *          message associated with exception
   * @param e
   *          parent exception
   */
  public SourceNotAvailable(final String message, final Exception e) {
    super(message, e);
  }

}
