/**
 * This package contains implementation of cache utils. General interface of
 * cache is defined in
 * {@link lcsb.mapviewer.annotation.cache.QueryCacheInterface
 * QueryCacheInterface}. There are three known implementations of this
 * interface:
 * <ul>
 * <li>{@link lcsb.mapviewer.annotation.cache.ApplicationLevelCache
 * ApplicationLevelCache} - allows to cache data within application scope. After
 * application restart everything is lost,</li>
 * <li>{@link lcsb.mapviewer.annotation.cache.GeneralCache
 * PermanentDatabaseLevelCache} - allows to cache data in the database. It's
 * slower than {@link lcsb.mapviewer.annotation.cache.ApplicationLevelCache
 * ApplicationLevelCache} cache, but the data is available until they are
 * removed from the database,</li>
 * <li>{@link lcsb.mapviewer.annotation.cache.GeneralCache GeneralCache} - it's
 * a mix of above methods. When query to cache is send, first local application
 * cache is looked up and if it's not found there the database cache is
 * checked.</li>
 * </ul>
 * The data stored in cache has
 * {@link lcsb.mapviewer.db.model.reactome.CacheQuery#expires validity date}
 * after which data from cache shouldn't be used. For every data type stored in
 * cache there is {@link lcsb.mapviewer.annotation.cache.CachableInterface
 * CachableInterface} defined. This class is responsible for updating
 * information from original source when expire date is reached.
 */
package lcsb.mapviewer.annotation.cache;
