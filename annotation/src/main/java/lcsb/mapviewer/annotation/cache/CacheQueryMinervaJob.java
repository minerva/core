package lcsb.mapviewer.annotation.cache;

import java.util.HashMap;
import java.util.Map;

import lcsb.mapviewer.model.job.InvalidMinervaJobParameters;
import lcsb.mapviewer.model.job.MinervaJobParameters;

public class CacheQueryMinervaJob extends MinervaJobParameters {

  private String query;
  private Integer type;

  public CacheQueryMinervaJob(final String query, final Integer type) {
    this.query = query;
    this.type = type;
  }

  public CacheQueryMinervaJob(final Map<String, Object> jobParameters) throws InvalidMinervaJobParameters {
    super(jobParameters);
  }

  @Override
  protected void assignData(final Map<String, Object> jobParameters) {
    this.query = (String) jobParameters.get("query");
    this.type = (Integer) jobParameters.get("type");
  }

  @Override
  protected Map<String, Object> getJobParameters() {
    Map<String, Object> params = new HashMap<>();
    params.put("query", query);
    params.put("type", type);
    return params;
  }

  public Integer getType() {
    return type;
  }

  public String getQuery() {
    return query;
  }

}
