package lcsb.mapviewer.annotation.cache;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.output.CountingOutputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.MinervaConfigurationHolder;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.cache.BigFileEntry;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.persist.dao.ConfigurationDao;
import lcsb.mapviewer.persist.dao.cache.BigFileEntryDao;

/**
 * Interface for accessing and storing big files. They are stored in local file
 * system.
 * 
 * @author Piotr Gawron
 *
 */
@Service("BigFileCache")
@Transactional
public class BigFileCacheImpl implements BigFileCache {

  /**
   * Buffer size used for downloading single chunk of a file.
   */
  private static final int DOWNLOAD_BUFFER_SIZE = 1024 * 1024;

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger();

  /**
   * Access class for database objects storing statuses of the files.
   */
  private BigFileEntryDao bigFileEntryDao;

  /**
   * Access class for database objects storing some configuration information.
   */
  private ConfigurationDao configurationDao;

  /**
   * Service used for executing tasks in separate thread.
   */
  private ExecutorService asyncExecutorService;

  /**
   * Service used for executing tasks immediately.
   */
  private ExecutorService syncExecutorService;

  @Autowired
  private BigFileCache self;

  @Autowired
  private MinervaConfigurationHolder configurationHolder;

  /**
   * Constructor.
   */
  @Autowired
  public BigFileCacheImpl(final BigFileEntryDao bigFileEntryDao, final ConfigurationDao configurationDao) {
    this.bigFileEntryDao = bigFileEntryDao;
    this.configurationDao = configurationDao;
    // the executor is a daemon thread so that it will get killed automatically
    // when the main program exits
    asyncExecutorService = Executors.newScheduledThreadPool(10, new ThreadFactory() {
      @Override
      public Thread newThread(final Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        return t;
      }
    });
    syncExecutorService = Executors.newScheduledThreadPool(1, new ThreadFactory() {
      @Override
      public Thread newThread(final Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        return t;
      }
    });

    // put in the queue empty task to make sure that everything was initialized
    // (additional managing thread was createed)
    asyncExecutorService.submit(new Callable<Object>() {
      @Override
      public Object call() throws Exception {
        return null;
      }
    });
    syncExecutorService.submit(new Callable<Object>() {
      @Override
      public Object call() throws Exception {
        return null;
      }
    });
  }

  /**
   * Returns a path for a file in local file system.
   * 
   * @param url
   *          url to the file
   * @return a path for a file in local file system
   * @throws FileNotFoundException
   *           thrown when file should be in file system, but couldn't be found
   *           there (somebody manually removed it)
   */
  @Override
  public String getAbsolutePathForFile(final String url) throws FileNotFoundException {
    BigFileEntry entry = bigFileEntryDao.getByUrl(url);
    if (entry == null) {
      return null;
    }
    if (entry.getDownloadProgress() == null || entry.getDownloadProgress() < 100.0) {
      throw new FileNotFoundException("File is not complete: " + entry.getLocalPath() + ". Downloaded from: " + url);
    }
    File f = new File(configurationHolder.getDataPath() + entry.getLocalPath());
    if (!f.exists()) {
      throw new FileNotFoundException("Missing big file: " + f.getAbsolutePath() + ". Downloaded from: " + url);
    }

    return f.getAbsolutePath();
  }

  /**
   * Returns a path for a file in local file system for an ftp url.
   * 
   * @param url
   *          ftp url to the file
   * @return a path for a file in local file system for an ftp url
   * @throws FileNotFoundException
   *           thrown when file should be in file system, but couldn't be found
   *           there (somebody manually removed it)
   */
  @Override
  public String getLocalPathForFile(final String url) throws FileNotFoundException {
    BigFileEntry entry = bigFileEntryDao.getByUrl(url);
    if (entry == null) {
      return null;
    }
    if (entry.getDownloadProgress() == null || entry.getDownloadProgress() < 100.0) {
      throw new FileNotFoundException("File is not complete: " + entry.getLocalPath() + ". Downloaded from: " + url);
    }
    File f = new File(configurationHolder.getDataPath() + entry.getLocalPath());
    if (!f.exists()) {
      throw new FileNotFoundException("Missing big file: " + f.getAbsolutePath() + ". Downloaded from: " + url);
    }

    return f.getAbsolutePath();
  }

  /**
   * Download big file from ftp server.
   * 
   * @param url
   *          url for file to download
   * @param updater
   *          callback method that will be called with info updates about
   *          downloading process
   * @throws IOException
   *           thrown when there is a problem with downloading file
   * @throws URISyntaxException
   *           thrown when url is invalid
   */
  @Override
  public BigFileEntry downloadFile(final String url, final IProgressUpdater updater) throws IOException, URISyntaxException {
    Callable<BigFileEntry> computations = null;
    if (url.toLowerCase().startsWith("http:") || url.toLowerCase().startsWith("https:")) {
      computations = new GetHttpFileTask(url, updater);
    } else {
      throw new URISyntaxException(url, "Unknown protocol for url");
    }

    try {
      return asyncExecutorService.submit(computations).get();
    } catch (final InterruptedException e) {
      logger.error(e, e);
      return null;
    } catch (final ExecutionException e) {
      if (e.getCause() instanceof URISyntaxException) {
        throw (URISyntaxException) e.getCause();
      } else if (e.getCause() instanceof IOException) {
        throw new IOException(e.getCause());
      } else {
        throw new InvalidStateException(e);
      }
    }
  }

  /**
   * Creates new {@link BigFileEntry} for given url.
   * 
   * @param url
   *          url for which entry will be created
   * @return new {@link BigFileEntry} for given url
   * @throws URISyntaxException
   *           thrown when url is invalid
   */
  @Override
  public BigFileEntry createEntryForBigFile(final String url) throws URISyntaxException, IOException {
    String localPath = configurationDao.getValueByType(ConfigurationElementType.BIG_FILE_STORAGE_DIR);
    BigFileEntry entry = new BigFileEntry();
    entry.setDownloadDate(Calendar.getInstance());
    entry.setLocalPath(localPath);
    entry.setUrl(url);
    bigFileEntryDao.add(entry);

    localPath = entry.getLocalPath() + "/" + entry.getId() + "/";
    File dirFile = new File(configurationHolder.getDataPath() + localPath);

    if (dirFile.exists() && dirFile.isDirectory()) {
      logger.warn("Directory for the file already exists: " + dirFile.getAbsolutePath());
    } else if (!dirFile.mkdirs()) {
      throw new IOException("Cannot create directory: " + dirFile.getAbsolutePath());
    }

    localPath += getFileName(url);

    entry.setLocalPath(localPath);
    bigFileEntryDao.update(entry);
    bigFileEntryDao.flush();
    return entry;
  }

  /**
   * Checks if local file for given url is up to date. The check is based on file
   * size check (ftp doesn't provide checksums for files, so we cannot compare it
   * differently without downloading file).
   *
   * @param url
   *          url to ftp file
   * @return <code>true</code> if file is up to date, <code>false</code> in other
   *         case
   * @throws URISyntaxException
   *           thrown when url is invalid
   * @throws IOException
   *           thrown when there is a problem with accessing local or remote file
   */
  @Override
  public boolean isLocalFileUpToDate(final String url) throws URISyntaxException, IOException {
    if (url.toLowerCase().startsWith("http")) {
      return isLocalHttpFileUpToDate(url);
    } else {
      throw new URISyntaxException(url, "Unknown protocol");
    }
  }

  /**
   * Checks if local file fetched from http is up to date.
   *
   * @param url
   *          url to remote file
   * @return <code>true</code> if file is up to date, <code>false</code> otherwise
   * @throws IOException
   *           thrown when local file cannot be found or there is a problem with
   *           accessing remote file
   */
  public boolean isLocalHttpFileUpToDate(final String url) throws IOException {
    boolean result = false;
    BigFileEntry entry = bigFileEntryDao.getByUrl(url);
    if (entry == null) {
      throw new FileNotFoundException("File wasn't downloaded: " + url);
    }
    String path = configurationHolder.getDataPath() + entry.getLocalPath();
    File f = new File(path);
    if (!f.exists()) {
      throw new FileNotFoundException("Missing file: " + path + ". Downloaded from: " + url);
    }
    long localSize = f.length();

    long remoteSize = getRemoteHttpFileSize(url);

    result = (localSize == remoteSize);
    return result;
  }

  /**
   * Returns size of the remote file access via http url.
   *
   * @param url
   *          url address to the file
   * @return size of the remote file access via http url
   * @throws IOException
   *           thrown when there is a problem with accessing remote file
   */
  long getRemoteHttpFileSize(final String url) throws IOException {
    long remoteSize = -1;
    HttpURLConnection conn = null;
    try {
      URL website = new URL(url);
      conn = (HttpURLConnection) website.openConnection();
      conn.setRequestMethod("HEAD");
      conn.getInputStream();
      remoteSize = conn.getContentLength();
    } finally {
      if (conn != null) {
        conn.disconnect();
      }
    }
    return remoteSize;
  }

  /**
   * Removes local file copy of a file given in a parameter.
   *
   * @param url
   *          ftp url of a file
   * @throws IOException
   *           thrown when there is a problem with deleting file
   */
  @Override
  public void removeFile(final String url) throws IOException {
    List<BigFileEntry> entries = bigFileEntryDao.getAllByUrl(url);
    for (final BigFileEntry entry : entries) {
      String path = configurationHolder.getDataPath() + entry.getLocalPath();
      File f = new File(path);
      if (!f.exists()) {
        logger.warn("Missing file: " + path + ". Downloaded from: " + url);
      }
      String dirPath = FilenameUtils.getFullPath(path);

      FileUtils.deleteDirectory(new File(dirPath));

      bigFileEntryDao.delete(entry);
    }
  }

  /**
   * Executes download/update task.
   *
   * @param task
   *          task to be executed
   * @throws URISyntaxException
   *           thrown when task finished with {@link URISyntaxException}
   * @throws IOException
   *           thrown when task finished with {@link IOException}
   */
  void executeTask(final Future<?> task) throws URISyntaxException, IOException {
    try {
      task.get();
    } catch (final InterruptedException e) {
      logger.error(e, e);
    } catch (final ExecutionException e) {
      if (e.getCause() instanceof URISyntaxException) {
        throw (URISyntaxException) e.getCause();
      } else if (e.getCause() instanceof IOException) {
        throw new IOException(e.getCause());
      } else {
        throw new InvalidStateException(e);
      }
    }
  }

  /**
   * Returns server domain name from url.
   *
   * @param url
   *          url to be processed
   * @return server domain name from url
   * @throws URISyntaxException
   *           thrown when url is invalid
   */
  String getDomainName(final String url) throws URISyntaxException {
    URI uri = new URI(url);
    String domain = uri.getHost();
    if (domain.startsWith("www.")) {
      domain = domain.substring("www.".length());
    }

    return domain;
  }

  /**
   * Returns path to file on server without server domain name from url.
   *
   * @param url
   *          url to be processed
   * @return path to file on server without server domain name from url
   * @throws URISyntaxException
   *           thrown when url is invalid
   */
  public String getFilePath(final String url) throws URISyntaxException {
    URI uri = new URI(url);
    return uri.getPath();
  }

  /**
   * Returns simple file name from url.
   *
   * @param url
   *          url to be processed
   * @return simple file name from url
   * @throws URISyntaxException
   *           thrown when url is invalid
   */
  public String getFileName(final String url) throws URISyntaxException {
    URI uri = new URI(url);
    return FilenameUtils.getName(uri.getPath());
  }

  /**
   * Checks if the file identified by url is cached.
   *
   * @param sourceUrl
   *          url that identifies file
   * @return <code>true</code> if the file is cached, <code>false</code> otherwise
   */
  @Override
  public boolean isCached(final String sourceUrl) {
    BigFileEntry entry = bigFileEntryDao.getByUrl(sourceUrl);
    if (entry != null) {
      File f = new File(configurationHolder.getDataPath() + entry.getLocalPath());
      if (!f.exists()) {
        logger.warn("File is supposed to be cached but it's not there... " + sourceUrl);
        return false;
      }
      if (entry.getDownloadProgress() == null || entry.getDownloadProgress() < 100.0) {
        logger.warn("File is not complete: " + entry.getLocalPath() + ".");
        return false;
      }
    }
    return entry != null;
  }

  /**
   * @param bigFileEntryDao
   *          the bigFileEntryDao to set
   * @see #bigFileEntryDao
   */
  public void setBigFileEntryDao(final BigFileEntryDao bigFileEntryDao) {
    this.bigFileEntryDao = bigFileEntryDao;
  }

  /**
   * Return number of tasks that are executed or are waiting for execution.
   *
   * @return number of tasks that are executed or are waiting for execution
   */
  @Override
  public int getDownloadThreadCount() {
    return ((ScheduledThreadPoolExecutor) asyncExecutorService).getQueue().size()
        + ((ScheduledThreadPoolExecutor) asyncExecutorService).getActiveCount()
        + ((ScheduledThreadPoolExecutor) syncExecutorService).getQueue().size()
        + ((ScheduledThreadPoolExecutor) syncExecutorService).getActiveCount();
  }

  /**
   * Class that describes task of downloading http file.
   *
   * @author Piotr Gawron
   *
   */
  private final class GetHttpFileTask implements Callable<BigFileEntry> {

    /**
     * Url to the file that we want to download.
     *
     */
    private String url;

    /**
     * Callback listener that will receive information about upload progress.
     *
     */
    private IProgressUpdater updater;

    /**
     * Default constructor.
     *
     * @param url
     *          {@link #url}
     * @param updater
     *          {@link #updater}
     */
    private GetHttpFileTask(final String url, final IProgressUpdater updater) {
      this.url = url;
      if (updater != null) {
        this.updater = updater;
      } else {
        this.updater = new IProgressUpdater() {
          @Override
          public void setProgress(final double progress) {
          }
        };
      }
    }

    @Override
    public BigFileEntry call() throws Exception {
      BufferedInputStream in = null;
      CountingOutputStream cos = null;
      try {
        try {
          if (self.getAbsolutePathForFile(url) != null) {
            logger.warn("File already downloaded. Skipping...");
            return self.getByUrl(url);
          }
        } catch (final FileNotFoundException e) {
          self.removeFile(url);
        }
        BigFileEntry entry = self.createEntryForBigFile(url);
        entry.setDownloadThreadId(Thread.currentThread().getId());
        self.update(entry);

        final long size = getRemoteHttpFileSize(url);

        OutputStream output = new FileOutputStream(configurationHolder.getDataPath() + entry.getLocalPath());
        cos = new CountingOutputStream(output) {
          private double lastProgress = -1;

          @Override
          protected void beforeWrite(final int n) {
            super.beforeWrite(n);
            double newProgress = ((double) getCount()) / ((double) size) * IProgressUpdater.MAX_PROGRESS;
            if (newProgress - lastProgress >= IProgressUpdater.PROGRESS_BAR_UPDATE_RESOLUTION) {
              lastProgress = newProgress;
              entry.setDownloadProgress(lastProgress);
              self.update(entry);
              updater.setProgress(lastProgress);
            }
          }
        };
        URL website = new URL(url);
        in = new BufferedInputStream(website.openStream());

        final byte[] data = new byte[DOWNLOAD_BUFFER_SIZE];
        int count;
        while ((count = in.read(data, 0, DOWNLOAD_BUFFER_SIZE)) != -1) {
          cos.write(data, 0, count);
        }

        entry.setDownloadProgress(IProgressUpdater.MAX_PROGRESS);
        self.update(entry);
        updater.setProgress(IProgressUpdater.MAX_PROGRESS);
        output.close();

        return entry;
      } catch (final Exception e) {
        logger.error(e, e);
        throw e;
      } finally {
        // close opened streams
        if (in != null) {
          in.close();
        }
        if (cos != null) {
          cos.close();
        }
      }
    }
  }

  @Override
  public void update(final BigFileEntry entry) {
    bigFileEntryDao.update(entry);
  }

  @Override
  public BigFileEntry getByUrl(final String url) {
    return bigFileEntryDao.getByUrl(url);
  }

  @Override
  public void add(final BigFileEntry entry) {
    bigFileEntryDao.add(entry);
  }

}