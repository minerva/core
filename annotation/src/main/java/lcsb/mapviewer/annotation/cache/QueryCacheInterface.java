package lcsb.mapviewer.annotation.cache;

import org.w3c.dom.Node;

import lcsb.mapviewer.model.cache.CacheType;

/**
 * This interface describes general functionality of the cache interface.
 * 
 * @author Piotr Gawron
 * 
 */
public interface QueryCacheInterface {

  /**
   * Returns xml node from the cache. The entry (xml node in this case) is
   * identified by string and type associated with the entry.
   * 
   * @param identifier
   *          string identifying node in the database
   * @param type
   *          type of the entry
   * @return node from cache for given identifier and type
   */
  Node getXmlNodeByQuery(final String identifier, final CacheType type);

  /**
   * Returns string from the cache. The entry (string in this case) is
   * identified by string and type associated with the entry.
   * 
   * @param identifier
   *          string identifying node in the database
   * @param type
   *          type of the entry
   * @return node from cache for given string identifier and type
   */
  String getStringByQuery(final String identifier, final CacheType type);

  /**
   * Puts new value into cache. The entry is identified with type and
   * identifier.
   * 
   * @param identifier
   *          string identifying entry in the database
   * @param type
   *          type of the entry
   * @param value
   *          value to be stored in the cache
   */
  void setCachedQuery(final String identifier, final CacheType type, final Object value);

  void setCachedQuery(final String identifier, final CacheType type, final Object value, final int validDays);

  /**
   * Clears permanently whole cache.
   */
  void clearCache();

  /**
   * Removes element from the database.
   * 
   * @param identifier
   *          string identifying entry in the database
   * @param type
   *          type of the entry
   */
  void removeByQuery(final String identifier, final CacheType type);

  /**
   * This method invalidate result, but doesn't remove it (data should be
   * available until resource will be updated from original source). Moreover,
   * value reload method is called.
   * 
   * @param identifier
   *          string identifying entry in the database
   * @param type
   *          type of the entry
   */
  void invalidateByQuery(final String identifier, final CacheType type);

}
