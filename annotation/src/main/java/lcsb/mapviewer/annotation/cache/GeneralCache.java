package lcsb.mapviewer.annotation.cache;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.cache.CacheQuery;
import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.model.job.MinervaJob;
import lcsb.mapviewer.model.job.MinervaJobType;
import lcsb.mapviewer.persist.dao.cache.CacheQueryDao;
import lcsb.mapviewer.persist.dao.cache.CacheTypeDao;

/**
 * Implementation of database level cache ( {@link GeneralCacheInterface}). The
 * bean implements {@link ApplicationContextAware} to be able to inject
 * dependencies into {@link CachableInterface} classes that are responsible for
 * specific parts of the cache. Every single task in this implementation is
 * performed in new transaction (and separate thread).
 * 
 * @author Piotr Gawron
 * 
 */
@Transactional
@Service
public class GeneralCache implements GeneralCacheInterface, ApplicationContextAware {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Data access object for query entries accessed by string key.
   */
  private CacheQueryDao cacheQueryDao;

  private CacheTypeDao cacheTypeDao;

  /**
   * Service used for executing database tasks in separate thread.
   */
  private ExecutorService service;

  @Autowired
  private GeneralCacheInterface self;

  private ApplicationContext applicationContext;

  @Autowired
  public GeneralCache(final CacheQueryDao cacheQueryDao, final CacheTypeDao cacheTypeDao) {
    this.cacheQueryDao = cacheQueryDao;
    this.cacheTypeDao = cacheTypeDao;
  }

  /**
   * Post init spring method used for initialization of {@link #service} used for
   * execution of db tasks.
   */
  @PostConstruct
  public void init() {
    // the executor is a daemon thread so that it will get killed automatically
    // when the main program exits
    service = Executors.newScheduledThreadPool(1, new ThreadFactory() {
      @Override
      public Thread newThread(final Runnable r) {
        Thread t = new Thread(r);
        t.setDaemon(true);
        return t;
      }
    });

    // put in the queue empty task to make sure that everything was initialized
    // (additional managing thread was created)
    service.submit(new Callable<Object>() {
      @Override
      public Object call() throws Exception {
        return null;
      }
    });
  }

  @Override
  public Node getXmlNodeByQuery(final String query, final CacheType type) {
    if (type == null) {
      throw new InvalidArgumentException("Cache type cannot be null");
    }

    final Future<CacheQuery> task = service.submit(new QueryTask(query, type));
    CacheQuery entry = executeTask(task);
    if (entry == null) {
      return null;
    }
    try {
      Document document = XmlParser.getXmlDocumentFromString(entry.getValue());
      Node result = null;
      if (document != null) {
        result = document.getFirstChild();
      }
      return result;
    } catch (final InvalidXmlSchemaException e) {
      logger.warn("Invalid xml for query: " + query);
      logger.warn("xml: " + entry.getValue());
      removeByQuery(query, type);
      return null;
    }
  }

  @Override
  public String getStringByQuery(final String query, final CacheType type) {
    if (type == null) {
      throw new InvalidArgumentException("Cache type cannot be null");
    }

    final Future<CacheQuery> task = service.submit(new QueryTask(query, type));
    CacheQuery entry = executeTask(task);
    if (entry == null) {
      return null;
    }
    return entry.getValue();
  }

  @Override
  public void setCachedQuery(final String query, final CacheType type, final Object object) {
    if (type == null) {
      throw new InvalidArgumentException("Cache type cannot be null");
    }

    setCachedQuery(query, type, object, type.getValidity());
  }

  @Override
  public void setCachedQuery(final String query, final CacheType type, final Object object, final int validDays) {
    if (type == null) {
      throw new InvalidArgumentException("Cache type cannot be null");
    }

    String value = null;
    if (object instanceof String) {
      value = (String) object;
    } else if (object instanceof Node) {
      value = XmlParser.nodeToString((Node) object, true);
    } else if (object == null) {
      value = null;
    } else {
      throw new CacheException("Unknown object type: " + object.getClass());
    }
    if (value == null) {
      removeByQuery(query, type);
    } else {
      final Future<CacheQuery> task = service.submit(new AddTask(query, type, value, validDays));
      executeTask(task);
    }
  }

  @Override
  public void clearCache() {
    cacheQueryDao.clearTable();
  }

  @Override
  public void removeByQuery(final String query, final CacheType type) {
    if (type == null) {
      throw new InvalidArgumentException("Cache type cannot be null");
    }

    final Future<CacheQuery> task = service.submit(new RemoveTask(query, type));
    executeTask(task);
  }

  @Override
  public void invalidateByQuery(final String query, final CacheType type) {
    if (type == null) {
      throw new InvalidArgumentException("Cache type cannot be null");
    }

    final Future<CacheQuery> task = service.submit(new InvalidateTask(query, type));
    executeTask(task);
  }

  /**
   * Executes and returns result of the task provided in the parameter. This
   * method is blocking (it's waiting for the results).
   *
   * @param task
   *          task to be executed
   * @return value returned by the task
   */
  private CacheQuery executeTask(final Future<CacheQuery> task) {
    try {
      return task.get();
    } catch (final InterruptedException e1) {
      logger.error(e1, e1);
    } catch (final ExecutionException e1) {
      logger.error(e1, e1);
    }
    return null;
  }

  @Override
  public void waitToFinishTasks() throws InterruptedException, ExecutionException {
    service.submit(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        return null;
      }
    }).get();
  }

  /**
   * This class represents new thread task for querying database.
   *
   * @author Piotr Gawron
   *
   */
  private final class QueryTask implements Callable<CacheQuery> {

    /**
     * Identifier of cached entry.
     *
     * @see CacheQuery#getQuery()
     */
    private String query;

    /**
     * Type of cached entry.
     *
     * @see CacheQuery#getType()
     */
    private CacheType type;

    /**
     * Default constructor.
     *
     * @param query
     *          {@link #query}
     * @param type
     *          {@link #type}
     */
    private QueryTask(final String query, final CacheType type) {
      logger.debug("Query task start");
      this.query = query;
      this.type = type;
    }

    @Override
    public CacheQuery call() throws Exception {
      logger.debug("Query task call");
      CacheQuery entry = self.getByQuery(query, type);
      logger.debug("Query task data retrieved");
      return entry;
    }

  }

  /**
   * This class represents new thread task for adding entry to database.
   *
   * @author Piotr Gawron
   *
   */
  private final class AddTask implements Callable<CacheQuery> {

    /**
     * Identifier of cached entry.
     *
     * @see CacheQuery#getQuery()
     */
    private String query;

    /**
     * Type of cached entry.
     *
     * @see CacheQuery#getType()
     */
    private CacheType type;

    /**
     * Value to cache.
     *
     * @see CacheQuery#getValue()
     */
    private String value;

    /**
     * How long should the entry be valid in days.
     *
     */
    private int validDays;

    /**
     * Default constructor.
     *
     * @param query
     *          {@link #query}
     * @param type
     *          {@link #type}
     * @param value
     *          {@link #value}
     */
    private AddTask(final String query, final CacheType type, final String value, final int validDays) {
      logger.debug("Add task start");
      this.query = query;
      this.type = type;
      this.value = value;
      this.validDays = validDays;
    }

    @Override
    public CacheQuery call() throws Exception {
      logger.debug("Add task call");
      CacheQuery entry = self.getByQuery(query, type);

      if (entry == null) {
        entry = new CacheQuery();
        entry.setQuery(query);
        entry.setAccessed(Calendar.getInstance());
      } else {
        entry.setAccessed(Calendar.getInstance());
      }
      entry.setValue(value);
      entry.setType(type);
      Calendar expires = Calendar.getInstance();
      expires.add(Calendar.DAY_OF_MONTH, validDays);
      entry.setExpires(expires);

      self.addOrUpdate(entry);
      return entry;
    }

  }

  /**
   * This class represents new thread task for removing entry from database.
   *
   * @author Piotr Gawron
   *
   */
  private final class RemoveTask implements Callable<CacheQuery> {

    /**
     * Identifier of cached entry.
     *
     * @see CacheQuery#getQuery()
     */
    private String query;

    /**
     * Type of cached entry.
     *
     * @see CacheQuery#getType()
     */
    private CacheType type;

    /**
     * Default constructor.
     *
     * @param query
     *          {@link #query}
     * @param type
     *          {@link #type}
     */
    private RemoveTask(final String query, final CacheType type) {
      logger.debug("Remove task start");
      this.query = query;
      this.type = type;
    }

    @Override
    public CacheQuery call() throws Exception {
      logger.debug("Remove task call");
      CacheQuery entry = self.getByQuery(query, type);
      if (entry != null) {
        self.delete(entry);
      }
      return entry;
    }

  }

  /**
   * This class represents new thread task for invalidating entry in database.
   *
   * @author Piotr Gawron
   *
   */
  private final class InvalidateTask implements Callable<CacheQuery> {

    /**
     * Identifier of cached entry.
     */
    private String query;

    /**
     * Type of cached entry.
     */
    private CacheType type;

    /**
     * Default constructor.
     *
     * @param query
     *          {@link #query}
     * @param type
     *          {@link #type}
     */
    private InvalidateTask(final String query, final CacheType type) {
      logger.debug("Invalidate task start");
      this.query = query;
      this.type = type;
    }

    @Override
    public CacheQuery call() throws Exception {
      logger.debug("Invalidate task call");
      Calendar date = Calendar.getInstance();

      date.add(Calendar.DATE, -1);

      CacheQuery entry = self.getByQuery(query, type);
      if (entry != null) {
        entry.setExpires(date);
        self.addOrUpdate(entry);
      }
      return entry;
    }

  }

  @Override
  public CacheQuery getByQuery(final String query, final CacheType type) {
    if (type == null) {
      throw new InvalidArgumentException("Cache type cannot be null");
    }

    return cacheQueryDao.getByQuery(query, type);
  }

  @Override
  public void addOrUpdate(final CacheQuery entry) {
    cacheQueryDao.saveOrUpdate(entry);
  }

  @Override
  public void delete(final CacheQuery entry) {
    cacheQueryDao.delete(entry);
  }

  @Override
  public List<CacheQuery> getExpired(final int limit) {
    return cacheQueryDao.getExpired(limit);
  }

  @Override
  public void execute(final MinervaJob job) throws Exception {
    if (job.getJobType() == MinervaJobType.REFRESH_CACHE) {
      CacheQueryMinervaJob parameters = new CacheQueryMinervaJob(job.getJobParameters());
      Integer typeId = parameters.getType();
      String query = parameters.getQuery();
      CacheType type = cacheTypeDao.getById(typeId);

      logger.debug("Refresh task call");
      Cacheable cachableInterface = null;
      try {
        Class<?> clazz = Class.forName(type.getClassName());
        Object object = applicationContext.getBean(clazz);
        if (object instanceof Cacheable) {
          cachableInterface = (Cacheable) object;
        } else {
          logger.fatal("Invalid class type: " + object.getClass() + " [" + object.toString()
              + "]. Class cannot be cast into " + Cacheable.class);
        }
      } catch (final Exception e) {
        logger.fatal("Problem with creating cache query updater", e);
      }
      if (cachableInterface != null) {
        try {
          cachableInterface.setCache(new GeneralCacheWithExclusion(self, 1));
          logger.debug("Refresh task session started");
          Object result = cachableInterface.refreshCacheQuery(query);
          if (result == null) {
            removeByQuery(query, type);
          } else {
            setCachedQuery(query, type, result);
          }
        } catch (final SourceNotAvailable e) {
          removeByQuery(query, type);
          logger.error("Cannot refresh cache", e);
        } catch (final InvalidArgumentException e) {
          removeByQuery(query, type);
          logger.error("Problem with refreshing. Invalid data to refresh", e);
        } catch (final Exception e) {
          removeByQuery(query, type);
          logger.error("Severe problem in cache", e);
        } finally {
          logger.debug("Refresh task session closed");
        }
      } else {
        removeByQuery(query, type);
      }
      logger.debug("Refresh task finish");
    } else {
      throw new NotImplementedException(job.getJobType() + " not implemented");
    }
  }

  @Override
  public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }

}