package lcsb.mapviewer.annotation.cache;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.annotation.services.WrongResponseCodeIOException;

/**
 * This class helps to download web pages. The pages are downloaded in separate
 * thread, due to the fact that sometimes java socket can hang (related JVM bug:
 * https://bugs.openjdk.java.net/browse/JDK-8075484 ).
 * 
 * @author Piotr Gawron
 *
 */
public class WebPageDownloader {

  /**
   * Hard timeout (threads are terminated) of http connection that access data
   * across Internet.
   */
  private static final int HTTP_CONNECTION_TIMEOUT_SEC = 120;
  /**
   * How much time should the thread sleep before another try to access the web
   * page that thrown INTERNAL SERVER ERROR http response.
   */
  private static final int HTTP_INTERNAL_ERROR_RECONNECT_SLEEP_TIME = 2000;
  /**
   * How many times should the thread retry to connect to the server after
   * receiving INTERNAL SERVER ERROR http response.
   */
  private static final int HTTP_INTERNAL_ERROR_RECONNECT_TIMES = 2;
  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  /**
   * Returns content of the web page identified by url.
   * 
   * @param accessUrl
   *          url of a web page
   * @return content of the web page identified by url
   * @throws IOException
   *           thrown when there is any problem with accessing webpage
   */
  public String getFromNetwork(final String accessUrl) throws IOException {
    return getFromNetwork(accessUrl, "GET", null);
  }

  /**
   * Returns content of the webpage identified by url. The HTTP query is done
   * using the provided POST if postData is not null.
   *
   *
   * @param accessUrl
   *          url of a webpage
   * @param httpRequestMethod
   *          type of HTTP request (GET, POST, PUT, PATCH, DELETE, ...)
   * @param data
   *          string to be sent in the body of the
   * @return content of the webpage identified by url
   * @throws IOException
   *           thrown when there is any problem with accessing webpage
   */
  public String getFromNetwork(final String accessUrl, final String httpRequestMethod, final String data) throws IOException {

    /**
     * Tasks that retrieves content from web page
     * 
     * @author Piotr Gawron
     *
     */
    class Task implements Callable<String> {
      private volatile String result = null;

      @Override
      public String call() throws Exception {
        int tries = 0;
        int code = HttpURLConnection.HTTP_INTERNAL_ERROR;
        HttpURLConnection urlConn = null;

        // connect to the server, if HTTP_INTERNAL_ERROR occurred then try to
        // reconnect HTTP_INTERNAL_ERROR_RECONNECT_TIMES times
        while (code == HttpURLConnection.HTTP_INTERNAL_ERROR || code == HttpURLConnection.HTTP_NOT_FOUND) {
          urlConn = openConnection(accessUrl);
          urlConn.addRequestProperty("User-Agent", "minerva-framework");
          urlConn.setRequestProperty("Accept", "*/*");

          urlConn.setRequestMethod(httpRequestMethod);
          if (data != null) {
            urlConn.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(urlConn.getOutputStream());
            wr.writeBytes(data);
            wr.close();
          }

          try {
            urlConn.connect();
            code = urlConn.getResponseCode();
          } catch (final FileNotFoundException e) {
            code = HttpURLConnection.HTTP_NOT_FOUND;
          } catch (final IOException e) {
            code = HttpURLConnection.HTTP_INTERNAL_ERROR;
          }
          tries++;
          if (tries > HTTP_INTERNAL_ERROR_RECONNECT_TIMES) {
            break;
          }
          if (code == HttpURLConnection.HTTP_INTERNAL_ERROR || code == HttpURLConnection.HTTP_NOT_FOUND) {
            logger.debug("Problem with webpage: " + accessUrl);
            logger.debug("Retrying: " + accessUrl);
            try {
              Thread.sleep(HTTP_INTERNAL_ERROR_RECONNECT_SLEEP_TIME);
            } catch (final InterruptedException e) {
            }
          }
        }
        try {
          BufferedReader in = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
          result = IOUtils.toString(in);
        } catch (final IOException e) {
          logger.error(
              "Problem with fetching url (" + (HTTP_INTERNAL_ERROR_RECONNECT_TIMES + 1) + " tries): " + accessUrl);
          if (e.getClass().equals(IOException.class) || e.getClass().equals(FileNotFoundException.class)) {
            throw new WrongResponseCodeIOException(e, code);
          } else {
            throw e;
          }
        }
        return result;
      }

    }

    ExecutorService executor = Executors.newSingleThreadExecutor();
    Future<String> future = executor.submit(new Task());

    try {
      int timeout = HTTP_CONNECTION_TIMEOUT_SEC;

      // dapi is slow for now
      if (accessUrl.indexOf("dapi.lcsb.uni.lu") >= 0) {
        timeout *= 10;
      }
      String result = future.get(timeout, TimeUnit.SECONDS);
      executor.shutdownNow();
      return result;
    } catch (final Exception e) {
      if (e.getCause() instanceof IOException) {
        throw (IOException) e.getCause();
      }
      throw new IOException("Problem with accessing webpage: " + accessUrl, e);
    }
  }

  /**
   * Opens {@link HttpURLConnection connection} to the url given in the argument.
   * 
   * @param accessUrl
   *          url to the web page
   * @return {@link HttpURLConnection connection} to the url given in the argument
   * @throws MalformedURLException
   *           thrown when url is invalid
   * @throws IOException
   *           thrown when there is problem with opening connection
   */
  HttpURLConnection openConnection(final String accessUrl) throws MalformedURLException, IOException {
    URL url = new URL(accessUrl);
    HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
    return urlConn;
  }

}
