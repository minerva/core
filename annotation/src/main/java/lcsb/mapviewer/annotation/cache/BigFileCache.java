package lcsb.mapviewer.annotation.cache;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;

import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.model.cache.BigFileEntry;

/**
 * Interface for accessing and storing big files. They are stored in local file
 * system.
 * 
 * @author Piotr Gawron
 *
 */
public interface BigFileCache {

  BigFileEntry downloadFile(final String url, final IProgressUpdater updater) throws IOException, URISyntaxException;

  String getAbsolutePathForFile(final String url) throws FileNotFoundException;

  boolean isCached(final String sourceUrl);

  boolean isLocalFileUpToDate(final String url) throws URISyntaxException, IOException;

  void removeFile(final String url) throws IOException;

  String getLocalPathForFile(final String url) throws FileNotFoundException;

  int getDownloadThreadCount();

  void update(final BigFileEntry entry);

  BigFileEntry getByUrl(final String url);

  BigFileEntry createEntryForBigFile(final String url) throws URISyntaxException, IOException;

  void add(BigFileEntry entry);

}