package lcsb.mapviewer.annotation.cache;

import java.util.List;
import java.util.concurrent.ExecutionException;

import lcsb.mapviewer.model.cache.CacheQuery;
import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.model.job.MinervaJobExecutor;

/**
 * Interface describing database level cache. It has the same functionality as
 * {@link QueryCacheInterface}. It allows to cache data within application
 * scope. After application restarts everything is lost.
 * 
 * @author Piotr Gawron
 * 
 */
public interface GeneralCacheInterface extends QueryCacheInterface, MinervaJobExecutor {

  /**
   * Waits for all tasks in the cache to finish (refresh/get/etc).
   * 
   */
  void waitToFinishTasks() throws InterruptedException, ExecutionException;

  CacheQuery getByQuery(String query, CacheType type);

  void addOrUpdate(CacheQuery entry);

  void delete(CacheQuery entry);

  List<CacheQuery> getExpired(int limit);

}
