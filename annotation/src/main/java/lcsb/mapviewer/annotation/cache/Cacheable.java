package lcsb.mapviewer.annotation.cache;

import lcsb.mapviewer.model.cache.CacheType;

public interface Cacheable {

  Object refreshCacheQuery(final Object query) throws SourceNotAvailable;

  CacheType getCacheType();

  void setCache(final QueryCacheInterface cache);

  QueryCacheInterface getCache();

  String cleanHtml(String string);

}
