package lcsb.mapviewer.annotation.cache;

import java.io.IOException;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.cache.CacheType;
import lcsb.mapviewer.persist.dao.cache.CacheTypeDao;

/**
 * Interface used for retrieving data that are no longer valid in the cache
 * (their expire date was reached).
 * 
 * @author Piotr Gawron
 * 
 */
@Transactional
public abstract class CachableInterface implements Cacheable {

  private static Logger logger = LogManager.getLogger();

  /**
   * String containing class name of the class that extends
   * {@link CachableInterface}. String name is used because cache layer doesn't
   * have access to every possible cacheable interface, therefore it cannot
   * enlist it.
   */
  private String cacheIdentifier;
  /**
   * Type of the cache elements associated with this interface.
   */
  private CacheType cacheType;
  /**
   * Cache mechanism used to speed up computation.
   */
  @Autowired
  private QueryCacheInterface cache;
  /**
   * Cache mechanism used to speed up computation.
   */
  @Autowired
  private CacheTypeDao cacheTypeDao;
  /**
   * This object downloads web pages.
   */
  private WebPageDownloader webPageDownloader = new WebPageDownloader();

  /**
   * Default constructor.
   *
   * @param clazz
   *          type that defines this interface
   */
  public CachableInterface(final Class<?> clazz) {
    this.cacheIdentifier = clazz.getCanonicalName();
  }

  /**
   * Refresh cache data from original source.
   *
   * @param query
   *          text used to identify the resource
   * @return newly obtained data from the source
   * @throws SourceNotAvailable
   *           thrown when original source is unavailable
   */
  @Override
  public Object refreshCacheQuery(final Object query) throws SourceNotAvailable {
    String result = null;
    try {
      if (query instanceof String) {
        String name = (String) query;
        if (name.startsWith("http")) {
          result = getWebPageContent(name);
        } else {
          throw new InvalidArgumentException("Don't know what to do with string \"" + query + "\"");
        }
      } else {
        throw new InvalidArgumentException("Don't know what to do with class: " + query.getClass());
      }
    } catch (final IOException e) {
      throw new SourceNotAvailable(e);
    }
    return result;
  }

  /**
   * Puts some string value into cache.
   * 
   * @param key
   *          string that identifies value
   * @param result
   *          value to put into cache
   */
  protected final void setCacheValue(final String key, final String result) {
    if (cache != null) {
      cache.setCachedQuery(key, getCacheType(), result);
    }
  }

  protected final void setCacheValue(final String key, final String result, final int validDays) {
    if (cache != null) {
      cache.setCachedQuery(key, getCacheType(), result, validDays);
    }
  }

  /**
   * Retrieves string from cache.
   * 
   * @param key
   *          string that identifies xml node
   * @return string from cache
   */
  protected final String getCacheValue(final String key) {
    if (cache != null) {
      return cache.getStringByQuery(key, getCacheType());
    } else {
      return null;
    }
  }

  protected final void removeCacheValue(final String key) {
    if (cache != null) {
      cache.removeByQuery(key, getCacheType());
    }
  }

  /**
   * @return the cacheType
   * @see #cacheType
   */
  @Override
  public final CacheType getCacheType() {
    if (cacheType == null) {
      cacheType = cacheTypeDao.getByClassName(cacheIdentifier);
    }
    return cacheType;
  }

  /**
   * Retrieves xml node from cache.
   * 
   * @param key
   *          string that identifies xml node
   * @return xml node from cache
   */
  protected final Node getCacheNode(final String key) {
    if (cache != null) {
      return cache.getXmlNodeByQuery(key, getCacheType());
    } else {
      return null;
    }
  }

  /**
   * Saves xml node in the cache.
   * 
   * @param id
   *          identifier of the node
   * @param item
   *          node to save
   */
  protected void setCacheNode(final String id, final Node item) {
    if (cache != null) {
      cache.setCachedQuery(id, getCacheType(), item);
    }
  }

  /**
   * @return the cache
   * @see #cache
   */
  @Override
  public final QueryCacheInterface getCache() {
    return cache;
  }

  /**
   * @param cache
   *          the cache to set
   * @see #cache
   */
  @Override
  public final void setCache(final QueryCacheInterface cache) {
    this.cache = cache;
  }

  /**
   * Returns a content of the web page for a given url using GET request.
   * 
   * @param accessUrl
   *          web page url address
   * @return content of the web page
   * @throws IOException
   *           thrown when there are problems with connection to ChEMBL database
   */
  protected final String getWebPageContent(final String accessUrl) throws IOException {
    return getWebPageContent(accessUrl, "GET", null);
  }

  /**
   * Returns a content of the web page for a given url. If postData is not null,
   * the page will be accessed using POST request. Otherwise GET will be used.
   * 
   * @param accessUrl
   *          web page url address
   * @param httpRequestMethod
   *          type of HTTP request (GET, POST, PUT, PATCH, DELETE, ...)
   * @param data
   *          string to be sent in the body of the request
   * @return content of the web page
   * @throws IOException
   *           thrown when there are problems with connection to ChEMBL database
   */
  protected final String getWebPageContent(final String accessUrl, final String httpRequestMethod, final String data) throws IOException {
    // check if we have the page in our internal cache

    String cacheKey = accessUrl;
    if (data != null) {
      cacheKey += data;
    }

    String result = getCacheValue(cacheKey);
    if (result == null) {
      result = webPageDownloader.getFromNetwork(accessUrl, httpRequestMethod, data);
      if (result != null) {
        setCacheValue(cacheKey, result);
      }
    }
    return result;
  }

  /**
   * Removes html tags from text.
   * 
   * @param text
   *          text text to clean
   * @return text without html tags
   */
  @Override
  public String cleanHtml(final String text) {
    int startIndex;
    int endIndex;
    // and now clean the description from html tags (should be somehow
    // improved...)

    StringBuilder result = new StringBuilder();

    int oldEndIndex = 0;
    startIndex = text.indexOf("<");
    while (startIndex >= 0) {
      endIndex = text.indexOf(">", startIndex);
      result.append(text.substring(oldEndIndex, startIndex));
      if (endIndex < 0) {
        logger.warn("Problem with html code: " + text);
        oldEndIndex = text.length();
        break;
      } else {
        oldEndIndex = endIndex + 1;
      }

      startIndex = text.indexOf("<", oldEndIndex);
    }

    result.append(text.substring(oldEndIndex));
    return StringEscapeUtils.unescapeHtml4(result.toString().replaceAll("[\n\r]+", " "));
  }
}
