package lcsb.mapviewer.annotation.data.serializer;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.PropertyFilter;

import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.modelutils.serializer.CustomExceptFilter;

public class DrugSerializer extends JsonSerializer<Drug> {

  @Override
  public void serialize(final Drug drug, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    PropertyFilter filter = null;
    FilterProvider provider = serializers.getConfig().getFilterProvider();
    if (provider != null) {
      filter = provider.findPropertyFilter("drugFilter", null);
    }

    gen.writeStartObject();

    for (final String string : availableColumns()) {
      String column = string.toLowerCase();
      Object value = null;
      if (column.equals("id") || column.equals("idobject")) {
        value = drug.getName();
      } else if (column.equals("name")) {
        value = drug.getName();
      } else if (column.equals("references")) {
        value = drug.getSources();
      } else if (column.equals("description")) {
        value = drug.getDescription();
      } else if (column.equals("bloodbrainbarrier")) {
        value = drug.getBloodBrainBarrier();
      } else if (column.equals("brandnames")) {
        value = drug.getBrandNames();
      } else if (column.equals("synonyms")) {
        value = drug.getSynonyms();
      } else if (column.equals("targets")) {
        value = drug.getTargets();
      } else {
        value = "Unknown column";
      }
      writeField(gen, string, value, filter);
    }

    gen.writeEndObject();
  }

  private void writeField(final JsonGenerator gen, final String field, final Object value, final PropertyFilter filter) throws IOException {
    if (filter == null
        || (filter instanceof CustomExceptFilter && ((CustomExceptFilter) filter).includeField(field))) {
      gen.writeObjectField(field, value);
    }
  }

  public static List<String> availableColumns() {
    return Arrays.asList("name", "references", "description", "bloodBrainBarrier", "brandNames", "synonyms", "id", "targets");

  }
}