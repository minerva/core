package lcsb.mapviewer.annotation.data.serializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.PropertyFilter;

import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.annotation.data.MeSH;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.modelutils.serializer.CustomExceptFilter;

public class ChemicalSerializer extends JsonSerializer<Chemical> {

  @Override
  public void serialize(final Chemical chemical, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    PropertyFilter filter = null;
    FilterProvider provider = serializers.getConfig().getFilterProvider();
    if (provider != null) {
      filter = provider.findPropertyFilter("chemicalFilter", null);
    }

    String description = "Mesh term not available";
    List<String> synonyms = new ArrayList<>();

    MeSH mesh = chemical.getMesh();
    if (mesh != null) {
      description = mesh.getDescription();
      synonyms = mesh.getSynonyms();
    }

    gen.writeStartObject();

    for (final String string : availableColumns()) {
      String column = string.toLowerCase();
      Object value = null;
      switch (column) {
        case "id":
        case "idobject":
          value = chemical.getChemicalId();
          break;
        case "name":
          value = chemical.getChemicalName();
          break;
        case "references":
          List<MiriamData> references = new ArrayList<>();
          references.add(chemical.getChemicalId());
          if (chemical.getCasID() != null) {
            references.add(chemical.getCasID());
          }
          value = references;
          break;
        case "directevidencereferences":
          value = chemical.getDirectEvidencePublication();
          break;
        case "description":
          value = description;
          break;
        case "directevidence":
          if (chemical.getDirectEvidence() != null) {
            value = chemical.getDirectEvidence().getValue();
          }
          break;
        case "synonyms":
          value = synonyms;
          break;
        case "targets":
          value = chemical.getTargets();
          break;
        default:
          value = "Unknown column";
          break;
      }
      writeField(gen, string, value, filter);
    }

    gen.writeEndObject();
  }

  private void writeField(final JsonGenerator gen, final String field, final Object value, final PropertyFilter filter) throws IOException {
    if (filter == null
        || (filter instanceof CustomExceptFilter && ((CustomExceptFilter) filter).includeField(field))) {
      gen.writeObjectField(field, value);
    }
  }

  public static List<String> availableColumns() {
    return Arrays.asList("name", "references", "description", "synonyms", "id", "directEvidenceReferences", "directEvidence", "targets");
  }
}