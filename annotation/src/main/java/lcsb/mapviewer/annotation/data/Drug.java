package lcsb.mapviewer.annotation.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lcsb.mapviewer.annotation.data.serializer.DrugSerializer;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.model.map.MiriamData;

/**
 * Desribes information about drug retrieved from external database.
 * 
 * @author Piotr Gawron
 * 
 */
@XmlRootElement
@JsonSerialize(using = DrugSerializer.class)
public class Drug implements Serializable, TargettingStructure {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Source of the data.
   */
  private List<MiriamData> sources = new ArrayList<>();

  /**
   * Name of the drug.
   */
  private String name;

  /**
   * Description of the drug.
   */
  private String description;

  /**
   * Status of blood brain barriers for the drug.
   */
  private String bloodBrainBarrier = "N/A";

  /**
   * Is the drug approved.
   */
  private Boolean approved;

  /**
   * Known brand names.
   */
  private List<String> brandNames = new ArrayList<>();

  /**
   * Known targets.
   */
  private List<Target> targets = new ArrayList<>();

  /**
   * Known synonyms.
   */
  private List<String> synonyms = new ArrayList<>();

  /**
   * Default constructor.
   */
  public Drug() {

  }

  /**
   * Constructor that initializes object with the same information as parameter.
   * 
   * @param drug
   *          original drug
   */
  public Drug(final Drug drug) {
    if (drug == null) {
      return;
    }
    this.name = drug.name;
    this.sources.addAll(drug.getSources());
    this.description = drug.description;
    this.brandNames.addAll(drug.getBrandNames());
    this.synonyms.addAll(drug.getSynonyms());
    this.targets.addAll(drug.getTargets());
    this.bloodBrainBarrier = drug.getBloodBrainBarrier();
    this.setApproved(drug.getApproved());
  }

  @Override
  public String toString() {
    StringBuilder result = new StringBuilder("");
    for (final MiriamData md : sources) {
      result.append("source: " + md.getDataType() + ", drugId: " + md.getResource() + "\n");
    }
    result.append(", name: " + name + "\nDescription:\n" + description + "\nSynonyms:");
    for (final String s : synonyms) {
      result.append(s + "|");
    }
    result.append("\nTargets:");
    for (final Target t : targets) {
      if (t.getSource() != null) {
        result.append(t.getSource().getResource() + "|" + t.getName() + "|" + t.getGenes());
      } else {
        result.append("N/A |" + t.getName() + "|" + t.getGenes());
      }
      result.append(" (References:");
      for (final MiriamData md : t.getReferences()) {
        result.append(md.getResource() + ",");
      }
      result.append(")+\n");

    }
    return result.toString();
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * @return the description
   * @see #description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description
   *          the description to set
   * @see #description
   */
  public void setDescription(final String description) {
    this.description = description;
  }

  /**
   * @return the bloodBrainBarrier
   * @see #bloodBrainBarrier
   */
  public String getBloodBrainBarrier() {
    return bloodBrainBarrier;
  }

  /**
   * @param bloodBrainBarrier
   *          the bloodBrainBarrier to set
   * @see #bloodBrainBarrier
   */
  public void setBloodBrainBarrier(final String bloodBrainBarrier) {
    this.bloodBrainBarrier = bloodBrainBarrier;
  }

  /**
   * @return the brandNames
   * @see #brandNames
   */
  public List<String> getBrandNames() {
    return brandNames;
  }

  /**
   * @param brandNames
   *          the brandNames to set
   * @see #brandNames
   */
  public void setBrandNames(final List<String> brandNames) {
    this.brandNames = brandNames;
  }

  /**
   * @return the synonyms
   * @see #synonyms
   */
  public List<String> getSynonyms() {
    return synonyms;
  }

  /**
   * @param synonyms
   *          the synonyms to set
   * @see #synonyms
   */
  public void setSynonyms(final List<String> synonyms) {
    this.synonyms = synonyms;
  }

  /**
   * Adds brand name into {@link #brandNames}.
   *
   * @param brandName
   *          object to add
   */
  public void addBrandName(final String brandName) {
    this.brandNames.add(brandName);
  }

  /**
   * Adds synonym into {@link #synonyms}.
   *
   * @param synonym
   *          object to add
   */
  public void addSynonym(final String synonym) {
    if (synonyms.contains(synonym)) {
      logger.warn("Synonym already exists in the drug: " + synonym);
    } else {
      this.synonyms.add(synonym);
    }
  }

  /**
   * Adds target into {@link #targets} list.
   *
   * @param target
   *          object to add
   */
  public void addTarget(final Target target) {
    this.targets.add(target);
  }

  /**
   * @param source
   *          the source to add
   * @see #sources
   */
  public void addSource(final MiriamData source) {
    this.sources.add(source);
  }

  /**
   * @return the sources
   * @see #sources
   */
  public List<MiriamData> getSources() {
    return sources;
  }

  /**
   * @return the targets
   * @see #targets
   */
  @XmlElement(name = "target")
  public List<Target> getTargets() {
    return targets;
  }

  /**
   * @param sources
   *          the sources to set
   * @see #sources
   */
  public void setSources(final List<MiriamData> sources) {
    this.sources = sources;
  }

  /**
   * Adds targets to the drug.
   * 
   * @param targets2
   *          list of targets to add
   */
  @Override
  public void addTargets(final List<? extends Target> targets2) {
    this.targets.addAll(targets2);
  }

  /**
   * @return the approved
   * @see #approved
   */
  public Boolean getApproved() {
    return approved;
  }

  /**
   * @param approved
   *          the approved to set
   * @see #approved
   */
  public void setApproved(final Boolean approved) {
    this.approved = approved;
  }

  /**
   * Comparator of the objects by their name.
   * 
   * @author Piotr Gawron
   * 
   */
  public static class NameComparator implements Comparator<Drug> {
    /**
     * Default string comparator.
     */
    private StringComparator stringComparator = new StringComparator();

    @Override
    public int compare(final Drug arg0, final Drug arg1) {
      return stringComparator.compare(arg0.getName(), arg1.getName());
    }

  }

  public void addBrandNames(final List<String> brandNames) {
    this.brandNames.addAll(brandNames);
  }

  public void addSynonyms(final List<String> synonyms) {
    this.synonyms.addAll(synonyms);
  }

  public void removeTargets(final Collection<Target> targetsToRemove) {
    this.getTargets().removeAll(targetsToRemove);
  }
}