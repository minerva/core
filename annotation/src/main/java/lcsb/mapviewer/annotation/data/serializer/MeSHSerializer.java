package lcsb.mapviewer.annotation.data.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.annotation.data.MeSH;

public class MeSHSerializer extends JsonSerializer<MeSH> {

  @Override
  public void serialize(final MeSH mesh, final JsonGenerator gen, final SerializerProvider serializers)
      throws IOException {

    gen.writeStartObject();

    gen.writeStringField("name", mesh.getName());
    gen.writeStringField("id", mesh.getMeSHId());
    gen.writeStringField("description", mesh.getDescription());
    gen.writeObjectField("synonyms", mesh.getSynonyms());

    gen.writeEndObject();
  }

}