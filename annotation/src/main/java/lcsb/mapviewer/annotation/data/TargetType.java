package lcsb.mapviewer.annotation.data;

/**
 * Type of the {@link Target}.
 * 
 * @author Piotr Gawron
 * 
 */
public enum TargetType {

  /**
   * Target is a single protein.
   */
  SINGLE_PROTEIN("Single protein"),

  /**
   * Target is a complex.
   */
  COMPLEX_PROTEIN("Complex protein"),

  /**
   * Target is a protein family.
   */
  PROTEIN_FAMILY("Protein family"),

  /**
   * Target is of other type.
   */
  OTHER("Other");

  /**
   * Human readable name.
   */
  private String commonName;

  /**
   * Default constructor that creates enum position.
   * 
   * @param commonName
   *          {@link #commonName}
   */
  TargetType(final String commonName) {
    this.commonName = commonName;
  }

  /**
   * 
   * @return {@link #commonName}
   */
  public String getCommonName() {
    return commonName;
  }
}
