package lcsb.mapviewer.annotation.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Describes element targeted by an element from external database. It could be
 * a {@link Drug} or {@link Chemical}.
 *
 * @author Piotr Gawron
 */
public class Target implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  private static final transient Logger logger = LogManager.getLogger();

  /**
   * Database from which target was received.
   */
  @JsonIgnore
  private MiriamData source;

  /**
   * Name of the target.
   */
  @JsonProperty("name")
  private String name;

  /**
   * Organism in which target is located.
   */
  @JsonIgnore
  private MiriamData organism;

  @JsonIgnore
  private MiriamData associatedDisease;

  /**
   * List of genes located in a target.
   */
  @JsonProperty("targetParticipants")
  private List<MiriamData> genes = new ArrayList<>();

  /**
   * List of references describing target.
   */
  @JsonProperty("references")
  private List<MiriamData> references = new ArrayList<>();

  /**
   * Type of target.
   */
  @JsonIgnore
  private TargetType type;

  /**
   * Default constructor.
   */
  public Target() {

  }

  /**
   * Constructor that initializes target with some data.
   *
   * @param source     from where the information about target was retrieved
   * @param gene       {@link MiriamData} that identifies gene
   * @param references list of references that support this target
   */
  public Target(final MiriamData source, final MiriamData gene, final Collection<MiriamData> references) {
    this.source = source;
    if (gene != null) {
      addGene(gene);
    }
    addReferences(references);
    if (getGenes().size() == 1) {
      setType(TargetType.SINGLE_PROTEIN);
    }
  }

  /**
   * @return the type
   * @see #type
   */
  public TargetType getType() {
    return type;
  }

  /**
   * @param type the type to set
   * @see #type
   */
  public void setType(final TargetType type) {
    this.type = type;
  }

  /**
   * @return {@link #references} list
   */
  @XmlElement(name = "pubmed")
  public List<MiriamData> getReferences() {
    return references;
  }

  /**
   * @param md new {@link #references} list
   */
  public void addReference(final MiriamData md) {
    if (this.references.contains(md)) {
      logger.warn("Target {} already has MiriamData: {}", getName(), md);
    } else {
      this.references.add(md);
    }
  }

  /**
   * Adds gene to the gene list.
   *
   * @param gene object to add
   */
  public void addGene(final MiriamData gene) {
    if (gene == null) {
      throw new InvalidArgumentException("Cannot add null element");
    }
    this.genes.add(gene);
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   * @see #name
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * @return the organism
   * @see #organism
   */
  public MiriamData getOrganism() {
    return organism;
  }

  /**
   * @param organism the organism to set
   * @see #organism
   */
  public void setOrganism(final MiriamData organism) {
    this.organism = organism;
  }

  /**
   * @return the geneCardId
   * @see #genes
   */
  public List<MiriamData> getGenes() {
    return genes;
  }

  /**
   * @param genes the genes to set
   * @see #genes
   */
  public void setGenes(final List<MiriamData> genes) {
    this.genes = genes;
  }

  /**
   * @return the source
   * @see #source
   */
  public MiriamData getSource() {
    return source;
  }

  /**
   * @param source the source to set
   * @see #source
   */
  public void setSource(final MiriamData source) {
    this.source = source;
  }

  /**
   * Adds set of references to the target.
   *
   * @param references objects to add
   * @see #references
   */
  public void addReferences(final Collection<MiriamData> references) {
    this.references.addAll(references);
  }

  @Override
  public String toString() {
    return "[" + this.getClass().getSimpleName() + "]: " + name + ", source: " + source + ", organism: " + organism
        + "; disease: " + associatedDisease;
  }

  public MiriamData getAssociatedDisease() {
    return associatedDisease;
  }

  public void setAssociatedDisease(final MiriamData associatedDisease) {
    this.associatedDisease = associatedDisease;
  }

}
