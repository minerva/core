package lcsb.mapviewer.annotation.data;

import java.util.Collection;
import java.util.List;

import lcsb.mapviewer.model.map.MiriamData;

/**
 * Interface for all externall objects that interact with elements on the map.
 * 
 * @author Piotr Gawron
 *
 */
public interface TargettingStructure {

  /**
   * 
   * @return list of {@link MiriamData annotations} that describe source of this
   *         object
   */
  Collection<MiriamData> getSources();

  /**
   * 
   * @return list of {@link Target targets} with which object is interacting
   */
  Collection<Target> getTargets();

  void removeTargets(final Collection<Target> targets);

  void addTargets(final List<? extends Target> projectTargets);

}
