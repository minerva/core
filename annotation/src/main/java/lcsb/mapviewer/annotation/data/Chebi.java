package lcsb.mapviewer.annotation.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class represents <a href="http://www.ebi.ac.uk/chebi/">chebi</a> object.
 * 
 * @author Piotr Gawron
 * 
 */
@XmlRootElement
public class Chebi implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  private String name;

  private String chebiId;

  private String smiles;

  private String inchi;

  private String inchiKey;

  private List<String> synonyms = new ArrayList<>();

  private List<String> formulaes = new ArrayList<>();

  public Chebi() {

  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * @return the chebiId
   * @see #chebiId
   */
  public String getChebiId() {
    return chebiId;
  }

  /**
   * @param chebiId
   *          the chebiId to set
   * @see #chebiId
   */
  public void setChebiId(final String chebiId) {
    this.chebiId = chebiId;
  }

  /**
   * @return the smiles
   * @see #smiles
   */
  public String getSmiles() {
    return smiles;
  }

  /**
   * @param smiles
   *          the smiles to set
   * @see #smiles
   */
  public void setSmiles(final String smiles) {
    this.smiles = smiles;
  }

  /**
   * @return the inchi
   * @see #inchi
   */
  public String getInchi() {
    return inchi;
  }

  /**
   * @param inchi
   *          the inchi to set
   * @see #inchi
   */
  public void setInchi(final String inchi) {
    this.inchi = inchi;
  }

  /**
   * @return the inchiKey
   * @see #inchiKey
   */
  public String getInchiKey() {
    return inchiKey;
  }

  /**
   * @param inchiKey
   *          the inchiKey to set
   * @see #inchiKey
   */
  public void setInchiKey(final String inchiKey) {
    this.inchiKey = inchiKey;
  }

  /**
   * @return the synonyms
   * @see #synonyms
   */
  public List<String> getSynonyms() {
    return synonyms;
  }

  public void addSynonyms(final List<String> synonyms2) {
    this.synonyms.addAll(synonyms2);

  }

  public List<String> getFormulaes() {
    return formulaes;
  }

  public void setFormulaes(final List<String> formulaes) {
    this.formulaes = formulaes;
  }

  public void addFormulae(final String formulae) {
    formulaes.add(formulae);
  }

  public void addSynonym(final String synonym) {
    synonyms.add(synonym);

  }

}
