package lcsb.mapviewer.annotation.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import lcsb.mapviewer.model.map.MiriamData;

/**
 * Object that stores important information about entrez id used in annotation
 * process.
 * 
 * @author Piotr Gawron
 *
 */
@XmlRootElement
public class EntrezData implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Symbol of a gene.
   */
  private String symbol = null;

  /**
   * Full name of a gene.
   */
  private String fullName = null;

  /**
   * Synonyms of a gene.
   */
  private List<String> synonyms = new ArrayList<>();

  /**
   * Description of a gene.
   */
  private String description = null;

  /**
   * Miriam annotations of a gene.
   */
  private List<MiriamData> miriamData = new ArrayList<>();

  /**
   * @return the symbol
   * @see #symbol
   */
  public String getSymbol() {
    return symbol;
  }

  /**
   * @param symbol
   *          the symbol to set
   * @see #symbol
   */
  public void setSymbol(final String symbol) {
    this.symbol = symbol;
  }

  /**
   * @return the fullName
   * @see #fullName
   */
  public String getFullName() {
    return fullName;
  }

  /**
   * @param fullName
   *          the fullName to set
   * @see #fullName
   */
  public void setFullName(final String fullName) {
    this.fullName = fullName;
  }

  /**
   * @return the synonyms
   * @see #synonyms
   */
  public List<String> getSynonyms() {
    return synonyms;
  }

  /**
   * @param synonyms
   *          the synonyms to set
   * @see #synonyms
   */
  public void setSynonyms(final List<String> synonyms) {
    this.synonyms = synonyms;
  }

  /**
   * @return the description
   * @see #description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description
   *          the description to set
   * @see #description
   */
  public void setDescription(final String description) {
    this.description = description;
  }

  /**
   * @return the miriamData
   * @see #miriamData
   */
  public List<MiriamData> getMiriamData() {
    return miriamData;
  }

  /**
   * @param miriamData
   *          the miriamData to set
   * @see #miriamData
   */
  public void setMiriamData(final List<MiriamData> miriamData) {
    this.miriamData = miriamData;
  }

  /**
   * Adds {@link MiriamData} to the list of annotations.
   *
   * @param miriamData2
   *          object to add
   */
  public void addMiriamData(final MiriamData miriamData2) {
    this.miriamData.add(miriamData2);
  }

  @Override
  public String toString() {
    String result = "Symbol: " + symbol + "\tFull name: " + fullName + "\tSynonyms: " + synonyms + "\tDescription: "
        + description + "\tMiriam: " + miriamData;
    return result;
  }

}
