package lcsb.mapviewer.annotation.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lcsb.mapviewer.annotation.data.serializer.ChemicalSerializer;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.model.map.MiriamData;

/**
 * Object representing element obtained from Toxigenomic database.
 *
 * @author Ayan Rota
 *
 */
@XmlRootElement
@JsonSerialize(using = ChemicalSerializer.class)
public class Chemical implements Serializable, TargettingStructure {

  /**
   *
   */
  private static final long serialVersionUID = 3892326511802845188L;

  /**
   * Name.
   */
  @JsonProperty(value = "name")
  private String chemicalName;

  /**
   * Toxigenomic database chemcial ID (MeSH).
   */

  @JsonProperty(value = "id")
  private MiriamData chemicalId;

  /**
   * Toxigenomic database ID (CAS).
   */
  @JsonIgnore
  private MiriamData casID;

  private MeSH mesh;

  /**
   * Evidence evidence either marker/mechanism and/or T therapeutic.
   */
  private ChemicalDirectEvidence directEvidence;

  /**
   * direct Evidence Publication.
   */
  @JsonProperty(value = "directEvidenceReferences")
  private List<MiriamData> directEvidencePublication = new ArrayList<>();

  /**
   * Genes interacting with the chemical plus publications.
   */
  @JsonIgnore
  private List<Target> inferenceNetwork = new ArrayList<>();

  /**
   * Inference score.
   */
  @JsonIgnore
  private Float inferenceScore;

  /**
   * Reference count.
   */
  @JsonIgnore
  private Integer refScore;

  /**
   * Known synonyms.
   */
  private List<String> synonyms = new ArrayList<>();

  /**
   * default constructor.
   */
  public Chemical() {
    super();
  }

  /**
   * @return the chemicalName
   */
  public String getChemicalName() {
    return chemicalName;
  }

  /**
   * @param chemicalName
   *          the chemicalName to set
   */
  public void setChemicalName(final String chemicalName) {
    this.chemicalName = chemicalName;
  }

  /**
   * @return the chemicalId
   */
  public MiriamData getChemicalId() {
    return chemicalId;
  }

  /**
   * @param chemicalId
   *          the chemicalId to set
   */
  public void setChemicalId(final MiriamData chemicalId) {
    this.chemicalId = chemicalId;
  }

  /**
   * @return the casType
   */
  public MiriamData getCasID() {
    return casID;
  }

  /**
   * @param casID
   *          the casType to set
   */
  public void setCasID(final MiriamData casID) {
    this.casID = casID;
  }

  /**
   * @return the directEvidence
   */
  public ChemicalDirectEvidence getDirectEvidence() {
    return directEvidence;
  }

  /**
   * @param directEvidence
   *          the directEvidence to set
   */
  public void setDirectEvidence(final ChemicalDirectEvidence directEvidence) {
    this.directEvidence = directEvidence;
  }

  /**
   * @return the inferenceNetwork
   */
  public List<Target> getInferenceNetwork() {
    return inferenceNetwork;
  }

  /**
   * @param inferenceNetwork
   *          the inferenceNetwork to set
   */
  public void setInferenceNetwork(final List<Target> inferenceNetwork) {
    this.inferenceNetwork = inferenceNetwork;
  }

  /**
   * @return the inferenceScore
   */
  public Float getInferenceScore() {
    return inferenceScore;
  }

  /**
   * @param inferenceScore
   *          the inferenceScore to set
   */
  public void setInferenceScore(final Float inferenceScore) {
    this.inferenceScore = inferenceScore;
  }

  /**
   * @return the refScore
   */
  public Integer getRefScore() {
    return refScore;
  }

  /**
   * @param refScore
   *          the refScore to set
   */
  public void setRefScore(final Integer refScore) {
    this.refScore = refScore;
  }

  /**
   * @return the directEvidencePublication
   */
  public List<MiriamData> getDirectEvidencePublication() {
    return directEvidencePublication;
  }

  /**
   * @param directEvidencePublication
   *          the directEvidencePublication to set
   */
  public void setDirectEvidencePublication(final List<MiriamData> directEvidencePublication) {
    this.directEvidencePublication = directEvidencePublication;
  }

  /**
   * @return list of synonyms.
   */
  public List<String> getSynonyms() {
    return synonyms;
  }

  /**
   * @param synonyms
   *          list of all names that are synonyms.
   */
  public void setSynonyms(final List<String> synonyms) {
    this.synonyms = synonyms;
  }

  /**
   * @return list of synonyms as string.
   */
  @JsonIgnore
  public String getSynonymsString() {
    return StringUtils.join(synonyms, ",");
  }

  @Override
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("\nname: " + chemicalName + "\nchemicalId:" + chemicalId + "\ncasID:" + casID);
    result.append("\nsynonyms:" + getSynonymsString());
    result.append("\ninferenceScore:" + inferenceScore + "\nrefScore:" + refScore);
    result.append("\ninferenceNetwork: ");
    for (final Target item : getInferenceNetwork()) {
      if (item != null) {
        result.append(item.toString());
      }
    }
    result.append("\ndirectEvidence:" + directEvidence + "\ndirectEvidencePublication:\n");
    for (final MiriamData publication : directEvidencePublication) {
      result.append(
          "publication DB: " + publication.getDataType() + ", publication Id: " + publication.getResource() + "\n");
    }

    return result.toString();
  }

  @Override
  @JsonIgnore
  public Collection<MiriamData> getSources() {
    List<MiriamData> sources = new ArrayList<>();
    if (getCasID() != null) {
      sources.add(getCasID());
    }
    if (getChemicalId() != null) {
      sources.add(getChemicalId());
    }
    return sources;
  }

  @Override
  public Collection<Target> getTargets() {
    return getInferenceNetwork();
  }

  public void addSynonyms(final List<String> synonyms) {
    this.synonyms.addAll(synonyms);
  }

  public void addTarget(final Target target) {
    this.getTargets().add(target);
  }

  /**
   * Comparator of the objects by their name.
   *
   * @author Piotr Gawron
   *
   */
  public static class NameComparator implements Comparator<Chemical> {
    /**
     * Default string comparator.
     */
    private StringComparator stringComparator = new StringComparator();

    @Override
    public int compare(final Chemical arg0, final Chemical arg1) {
      return stringComparator.compare(arg0.getChemicalName(), arg1.getChemicalName());
    }

  }

  @Override
  public void removeTargets(final Collection<Target> targetsToRemove) {
    this.getTargets().removeAll(targetsToRemove);
  }

  @Override
  public void addTargets(final List<? extends Target> projectTargets) {
    for (final Target target : projectTargets) {
      addTarget(target);
    }
  }

  public boolean containsTarget(final MiriamData miriamTarget) {
    boolean result = false;
    for (final Target target : getTargets()) {
      result |= target.getGenes().contains(new MiriamData(miriamTarget.getDataType(), miriamTarget.getResource()));
    }
    return result;
  }

  public void setMesh(final MeSH meSH) {
    this.mesh = meSH;
  }

  public MeSH getMesh() {
    return mesh;
  }

}
