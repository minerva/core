package lcsb.mapviewer.annotation.data;

import java.util.Comparator;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.model.map.BioEntity;

public class ProjectTarget extends Target {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  public static final Comparator<? super ProjectTarget> COMPARATOR = new Comparator<ProjectTarget>() {

    @Override
    public int compare(final ProjectTarget o1, final ProjectTarget o2) {
      Integer size1 = o1.bioEntities.size();
      Integer size2 = o2.bioEntities.size();
      if (size1 == size2) {
        return new StringComparator().compare(o1.getName(), o2.getName());
      }
      return -size1.compareTo(size2);
    }
  };

  @JsonProperty("targetElements")
  private List<BioEntity> bioEntities;

  public ProjectTarget(final Target target, final List<BioEntity> bioEntities) {
    setSource(target.getSource());
    setName(target.getName());
    setOrganism(target.getOrganism());
    setAssociatedDisease(target.getAssociatedDisease());
    setGenes(target.getGenes());
    addReferences(target.getReferences());
    setType(target.getType());
    this.bioEntities = bioEntities;
  }

  public List<BioEntity> getBioEntities() {
    return bioEntities;
  }

  public void setBioEntites(final List<BioEntity> bioEntities) {
    this.bioEntities = bioEntities;
  }

}
