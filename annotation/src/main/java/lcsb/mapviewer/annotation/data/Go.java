package lcsb.mapviewer.annotation.data;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Object representing element obtained from Gene Ontoly database.
 * 
 * @author Piotr Gawron
 * 
 */
@XmlRootElement
public class Go implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Human readable name.
   */
  private String commonName;

  /**
   * Description of the object.
   */
  private String description;

  /**
   * Go identifier.
   */
  private String goTerm;

  /**
   * @return the commonName
   * @see #commonName
   */
  public String getCommonName() {
    return commonName;
  }

  /**
   * @param commonName
   *          the commonName to set
   * @see #commonName
   */
  public void setCommonName(final String commonName) {
    this.commonName = commonName;
  }

  /**
   * @return the description
   * @see #description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description
   *          the description to set
   * @see #description
   */
  public void setDescription(final String description) {
    this.description = description;
  }

  /**
   * @return the goTerm
   * @see #goTerm
   */
  public String getGoTerm() {
    return goTerm;
  }

  /**
   * @param goTerm
   *          the goTerm to set
   * @see #goTerm
   */
  public void setGoTerm(final String goTerm) {
    this.goTerm = goTerm;
  }

}
