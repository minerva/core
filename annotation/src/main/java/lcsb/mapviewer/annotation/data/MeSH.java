package lcsb.mapviewer.annotation.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lcsb.mapviewer.annotation.data.serializer.MeSHSerializer;

/**
 * This class represents <a href="http://www.nlm.nih.gov/cgi/mesh//">MeSH</a>
 * object.
 * 
 * @author Ayan Rota
 * 
 */
@XmlRootElement
@JsonSerialize(using = MeSHSerializer.class)
public class MeSH implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  /**
   * short name.
   */
  private String name;

  /**
   * MeSH identifier.
   */
  private String meSHId;

  /**
   * Detailed description of the MeSH object.
   */
  private String description;

  /**
   * List of synonyms.
   */
  private List<String> synonyms = new ArrayList<>();

  /**
   * Default constructor.
   */
  public MeSH() {

  }

  /**
   * @param name
   *          short name.
   * @param meSHId
   *          database identifier.
   * @param description
   *          long description.
   * @param synonyms
   *          list of terms used as names for this object.
   */
  public MeSH(final String meSHId, final String name, final String description, final List<String> synonyms) {
    super();
    this.name = name;
    this.meSHId = meSHId;
    this.description = description;
    this.synonyms = synonyms;
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * @return the synonyms
   * @see #synonyms
   */
  public List<String> getSynonyms() {
    return synonyms;
  }

  /**
   * @param synonyms
   *          the synonyms to set
   * @see #synonyms
   */
  public void setSynonyms(final List<String> synonyms) {
    this.synonyms = synonyms;
  }

  /**
   * @return database identifier.
   */
  public String getMeSHId() {
    return meSHId;
  }

  /**
   * @param meSHId
   *          database identifier
   */
  public void setMeSHId(final String meSHId) {
    this.meSHId = meSHId;
  }

  /**
   * @return description of the object.
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description
   *          long description.
   */
  public void setDescription(final String description) {
    this.description = description;
  }

  @Override
  public String toString() {
    StringBuffer result = new StringBuffer();
    result.append("\nid: " + meSHId);
    result.append("\nname: " + name);
    result.append("\ndescription:" + description);
    result.append("\nsynonyms:" + getSynonymsString());
    return result.toString();
  }

  /**
   * @return list of synonyms as string.
   */
  public String getSynonymsString() {
    return StringUtils.join(synonyms, ",");
  }

  /**
   * Adds synonym to the synonym list.
   * 
   * @param synonym
   *          synonym to add
   */
  public void addSynonym(final String synonym) {
    synonyms.add(synonym);
  }

  /**
   * Adds synonyms to the synonym list.
   * 
   * @param synonymsToAdd
   *          synonyms to add
   */
  public void addSynonyms(final Set<String> synonymsToAdd) {
    for (String synonym : synonymsToAdd) {
      this.synonyms.add(synonym);
    }

  }

  public void removeSynonym(final String synonym) {
    this.synonyms.remove(synonym);
  }

}
