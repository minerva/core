package lcsb.mapviewer.annotation.data.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.annotation.data.ProjectTarget;

public class ProjectTargetSerializer extends JsonSerializer<ProjectTarget> {

  @Override
  public void serialize(final ProjectTarget target, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeStringField("name", target.getName());
    gen.writeObjectField("references", target.getReferences());
    gen.writeObjectField("targetParticipants", target.getGenes());

    gen.writeObjectField("targetElements", target.getBioEntities());

    gen.writeEndObject();
  }

}