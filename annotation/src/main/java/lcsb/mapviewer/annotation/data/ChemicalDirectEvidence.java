package lcsb.mapviewer.annotation.data;

/**
 * Object representing element obtained from Toxigenomic database.
 * 
 * @author Ayan Rota
 * 
 */
public enum ChemicalDirectEvidence {

  /**
   * Values.
   */
  MARKER("marker/mechanism"),
  THERAPEUTIC("therapeutic");

  /**
   * Value.
   */
  private String value;

  /**
   * @param value
   *          string to set the value.
   */
  ChemicalDirectEvidence(final String value) {

    this.value = value;
  }

  /**
   * @return the value
   */
  public String getValue() {
    return value;
  }

}
