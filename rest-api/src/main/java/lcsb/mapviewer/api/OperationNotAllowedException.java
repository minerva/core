package lcsb.mapviewer.api;

/**
 * Thrown when API operation is not allowed, but operation is valid. This can
 * happen when user tries to remove default map.
 * 
 * @author Piotr Gawron
 *
 */
public class OperationNotAllowedException extends QueryException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   * 
   * @param message
   *          error message
   */
  public OperationNotAllowedException(final String message) {
    super(message);
  }

  /**
   * Constructor with error message and parent exception.
   * 
   * @param message
   *          error message
   * @param reason
   *          parent exception that caused this one
   */
  public OperationNotAllowedException(final String message, final Exception reason) {
    super(message, reason);
  }

}
