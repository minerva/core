package lcsb.mapviewer.api.projects.overlays;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.FileEntry;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.overlay.DataOverlayType;
import lcsb.mapviewer.model.overlay.InvalidDataOverlayException;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.modelutils.serializer.CustomExceptFilter;
import lcsb.mapviewer.modelutils.serializer.model.map.ElementIdentifierType;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IDataOverlayService;
import lcsb.mapviewer.services.interfaces.IDataOverlayService.CreateDataOverlayParams;
import lcsb.mapviewer.services.interfaces.IFileService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/api/projects/{projectId}/overlays"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class OverlayController extends BaseController {

  private final IUserService userService;
  private final IProjectService projectService;
  private final IModelService modelService;
  private final IDataOverlayService dataOverlayService;
  private final IFileService fileService;

  public OverlayController(final IUserService userService,
                           final IProjectService projectService,
                           final IModelService modelService,
                           final IDataOverlayService dataOverlayService,
                           final IFileService fileService) {
    this.userService = userService;
    this.projectService = projectService;
    this.dataOverlayService = dataOverlayService;
    this.fileService = fileService;
    this.modelService = modelService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId) ")
  @PostFilter("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)"
      + " or hasAuthority('READ_PROJECT:' + #projectId) and (filterObject.creator.login == authentication.name or filterObject.isPublic())")
  @GetMapping(value = "/")
  public List<DataOverlay> getOverlayList(
      final @PathVariable(value = "projectId") String projectId,
      final @RequestParam(value = "creator", defaultValue = "") String creator,
      final @RequestParam(value = "publicOverlay", defaultValue = "false") boolean publicOverlay)
      throws lcsb.mapviewer.services.ObjectNotFoundException {
    final Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    final List<DataOverlay> result = dataOverlayService.getDataOverlaysByProject(project, true);

    return result.stream()
        .filter(overlay -> !publicOverlay || overlay.isPublic())
        .filter(
            overlay -> creator.isEmpty() || overlay.getCreator() != null && overlay.getCreator().getLogin().equals(creator))
        .collect(Collectors.toList());
  }

  @PostAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)"
      + " or hasAuthority('READ_PROJECT:' + #projectId) and (returnObject.creator.login == authentication.name or returnObject.isPublic())")
  @GetMapping(value = "/{overlayId}/")
  public DataOverlay getOverlayById(
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "overlayId") Integer overlayId) throws QueryException, ObjectNotFoundException {
    return dataOverlayService.getDataOverlayById(projectId, overlayId);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)"
      + " or hasAuthority('READ_PROJECT:' + #projectId) and "
      + "   (@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name "
      + "    or @dataOverlayService.getDataOverlayById(#overlayId)?.public == true)")
  @GetMapping(value = "/{overlayId}/models/{modelId}/bioEntities/")
  public MappingJacksonValue getOverlayElements(
      final @PathVariable(value = "projectId") String projectId,
      final @Valid @Pattern(regexp = "^[-+]?\\d+$|^\\*$") @PathVariable(value = "modelId") String modelId,
      final @PathVariable(value = "overlayId") Integer overlayId,
      final @RequestParam(value = "columns", defaultValue = "") String columns) throws QueryException, ObjectNotFoundException {
    if (overlayId == null) {
      throw new QueryException("Invalid overlayId");
    }
    dataOverlayService.getDataOverlayById(projectId, overlayId);

    final List<ModelData> models = modelService.getModelsByMapId(projectId, modelId);

    final List<OverlayEntryDTO> data = new ArrayList<>();

    for (final ModelData model : models) {
      final List<Pair<Element, DataOverlayEntry>> speciesList =
          dataOverlayService.getElementReferencesForDataOverlay(overlayId, Collections.singletonList(model));
      for (final Pair<Element, DataOverlayEntry> elementDataOverlay : speciesList) {
        data.add(new OverlayEntryDTO(elementDataOverlay, ElementIdentifierType.ALIAS, model));
      }

      final List<Pair<Reaction, DataOverlayEntry>> reactions =
          dataOverlayService.getReactionReferencesForDataOverlay(overlayId, Collections.singletonList(model));
      for (final Pair<Reaction, DataOverlayEntry> reactionDataOverlay : reactions) {
        data.add(new OverlayEntryDTO(reactionDataOverlay, ElementIdentifierType.REACTION, model));
      }
    }

    final MappingJacksonValue result = new MappingJacksonValue(data);
    String[] columnList = columns.split(",");
    if (columns.trim().isEmpty()) {
      final List<String> list = new ArrayList<>(new OverlayEntryDTOSerializer().availableColumns());
      list.remove("geneVariations");
      columnList = list.toArray(new String[]{});
    }
    final SimpleFilterProvider provider = new SimpleFilterProvider();
    provider.addFilter("overlayEntryFilter",
        new CustomExceptFilter(columnList));
    result.setFilters(provider);
    return result;
  }

  @JsonSerialize(using = OverlayEntryDTOSerializer.class)
  static class OverlayEntryDTO {
    private BioEntity bioEntity;
    private DataOverlayEntry entry;
    private ElementIdentifierType type;
    private ModelData model;

    public OverlayEntryDTO(final Pair<? extends BioEntity, DataOverlayEntry> pair, final ElementIdentifierType type, final ModelData model) {
      this.bioEntity = pair.getLeft();
      this.entry = pair.getRight();
      this.type = type;
      this.model = model;
    }

    public OverlayEntryDTO(final Pair<? extends BioEntity, DataOverlayEntry> pair) {
      this(pair,
          pair.getLeft() instanceof Element ? ElementIdentifierType.ALIAS : ElementIdentifierType.REACTION,
          pair.getLeft().getModelData());
    }

    public BioEntity getBioEntity() {
      return bioEntity;
    }

    public void setBioEntity(final BioEntity bioEntity) {
      this.bioEntity = bioEntity;
    }

    public DataOverlayEntry getEntry() {
      return entry;
    }

    public void setEntry(final DataOverlayEntry entry) {
      this.entry = entry;
    }

    public ElementIdentifierType getType() {
      return type;
    }

    public void setType(final ElementIdentifierType type) {
      this.type = type;
    }

    public ModelData getModel() {
      return model;
    }

    public void setModel(final ModelData model) {
      this.model = model;
    }
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)"
      + " or hasAuthority('READ_PROJECT:' + #projectId) and "
      + "   (@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name "
      + "  or @dataOverlayService.getDataOverlayById(#overlayId)?.public == true)")
  @GetMapping(value = "/{overlayId}/models/{modelId}/bioEntities/reactions/{reactionId}/")
  public MappingJacksonValue getFullReaction(
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "modelId") Integer modelId,
      final @PathVariable(value = "overlayId") Integer overlayId,
      final @PathVariable(value = "reactionId") Integer reactionId,
      final @RequestParam(value = "columns", defaultValue = "") String columns)
      throws QueryException, ObjectNotFoundException {
    final List<OverlayEntryDTO> data = new ArrayList<>();
    final List<Pair<Reaction, DataOverlayEntry>> reactionDataOverlay = dataOverlayService
        .getFullReactionForDataOverlay(projectId,
            modelId,
            reactionId, overlayId, true);
    if (reactionDataOverlay.size() == 0) {
      throw new ObjectNotFoundException("Reaction data cannot be found");
    }
    for (final Pair<Reaction, DataOverlayEntry> pair : reactionDataOverlay) {
      data.add(new OverlayEntryDTO(pair));
    }
    final MappingJacksonValue result = new MappingJacksonValue(data);
    if (!columns.trim().isEmpty()) {
      final String[] columnList = columns.split(",");
      final SimpleFilterProvider provider = new SimpleFilterProvider();
      provider.addFilter("overlayEntryFilter",
          new CustomExceptFilter(columnList));
      result.setFilters(provider);
    }
    return result;
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)"
      + " or hasAuthority('READ_PROJECT:' + #projectId) and "
      + "   (@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name "
      + "  or @dataOverlayService.getDataOverlayById(#overlayId)?.public == true)")
  @GetMapping(value = "/{overlayId}/models/{modelId}/bioEntities/elements/{elementId}/")
  public MappingJacksonValue getFullSpecies(
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "modelId") Integer modelId,
      final @PathVariable(value = "overlayId") Integer overlayId,
      final @PathVariable(value = "elementId") Integer elementId,
      final @RequestParam(value = "columns", defaultValue = "") String columns)
      throws QueryException, ObjectNotFoundException {
    final List<OverlayEntryDTO> data = new ArrayList<>();
    final List<Pair<Element, DataOverlayEntry>> elementDataOverlay = dataOverlayService
        .getFullElementForDataOverlay(
            projectId,
            modelId,
            elementId,
            overlayId, true);
    if (elementDataOverlay.size() == 0) {
      throw new ObjectNotFoundException("Element data cannot be found");
    }
    for (final Pair<Element, DataOverlayEntry> pair : elementDataOverlay) {
      data.add(new OverlayEntryDTO(pair));
    }
    final MappingJacksonValue result = new MappingJacksonValue(data);
    if (!columns.trim().isEmpty()) {
      final String[] columnList = columns.split(",");
      final SimpleFilterProvider provider = new SimpleFilterProvider();
      provider.addFilter("overlayEntryFilter",
          new CustomExceptFilter(columnList));
      result.setFilters(provider);
    }
    return result;
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or (hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId))"
      + " or (hasAuthority('READ_PROJECT:' + #projectId) and authentication.name != '" + Configuration.ANONYMOUS_LOGIN + "')")
  @PostMapping(value = "/")
  public DataOverlay addOverlay(
      final Authentication authentication,
      final @PathVariable(value = "projectId") String projectId,
      final @NotBlank @RequestParam(value = "name") String name,
      final @RequestParam(value = "description") String description,
      final @RequestParam(value = "content", defaultValue = "") String content,
      final @RequestParam(value = "fileId", required = false) Integer fileId,
      final @RequestParam(value = "filename") String filename,
      final @RequestParam(value = "type", required = false) DataOverlayType requestedType)
      throws QueryException, IOException, SecurityException, ObjectNotFoundException {
    DataOverlayType type = requestedType;
    if (type == null) {
      type = DataOverlayType.GENERIC;
    }
    final User user = userService.getUserByLogin(authentication.getName());
    final Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    if (content.isEmpty() && fileId == null) {
      throw new QueryException("Either content or fileId must be provided");
    }
    try {
      final InputStream stream;
      if (!content.isEmpty()) {
        stream = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
      } else {
        try {
          final UploadedFileEntry file = fileService.getById(fileId);
          if (file == null) {
            throw new QueryException("Invalid file id: " + fileId);
          }
          if (file.getOwner() == null || !file.getOwner().getLogin().equals(user.getLogin())) {
            throw new SecurityException("Access denied to source file");
          }
          stream = new ByteArrayInputStream(file.getFileContent());
        } catch (final NumberFormatException e) {
          throw new QueryException("Invalid fileId: " + fileId);
        }
      }

      final DataOverlay layout = dataOverlayService
          .createDataOverlay(new CreateDataOverlayParams()
              .colorInputStream(stream)
              .description(description)
              .fileName(filename)
              .name(name).user(user)
              .colorSchemaType(type)
              .project(project));

      final int count = (int) dataOverlayService.getDataOverlaysByProject(project, true).stream()
          .filter(overlay -> (overlay.getCreator().getLogin().equals(user.getLogin()) && !overlay.isPublic()))
          .count();
      layout.setOrderIndex(count);
      dataOverlayService.updateDataOverlay(layout);

      return getOverlayById(projectId, layout.getId());
    } catch (final InvalidDataOverlayException e) {
      throw new QueryException(e.getMessage(), e);
    }
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + "or hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId)"
      + "or hasAuthority('READ_PROJECT:' + #projectId) and @dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name")
  @DeleteMapping(value = "/{overlayId}")
  public Object removeOverlay(
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "overlayId") Integer overlayId) throws QueryException, IOException, ObjectNotFoundException {
    final DataOverlay layout = dataOverlayService.getDataOverlayById(projectId, overlayId);
    final User owner = layout.getCreator();
    final Project project = layout.getProject();
    dataOverlayService.removeDataOverlay(layout);

    reorderOverlays(owner, project);
    return new HashMap<>();
  }

  private void reorderOverlays(final User owner, final Project project) {
    final List<DataOverlay> overlays = dataOverlayService.getDataOverlaysByProject(project, true).stream()
        .filter(lay -> {
          return Objects.equals(owner.getId(), lay.getCreator().getId());
        }).collect(Collectors.toList());

    final List<DataOverlay> publicOverlays = overlays.stream().filter(lay -> {
      return lay.isPublic();
    }).collect(Collectors.toList());

    final List<DataOverlay> privateOverlays = overlays.stream().filter(lay -> {
      return !lay.isPublic();
    }).collect(Collectors.toList());

    enforceOrder(publicOverlays);
    enforceOrder(privateOverlays);
  }

  private void enforceOrder(final List<DataOverlay> overlays) {
    overlays.sort(DataOverlay.ORDER_COMPARATOR);
    for (int i = 0; i < overlays.size(); i++) {
      final DataOverlay overlay = overlays.get(i);
      if (overlay.getOrderIndex() != i + 1) {
        overlay.setOrderIndex(i + 1);
        dataOverlayService.updateDataOverlay(overlay);
      }
    }
  }

  static class UpdateDataOverlayDTO {
    private String creator;
    private String description;
    private String name;
    private Integer order;
    private DataOverlayType type;
    private Boolean publicOverlay;

    public String getCreator() {
      return creator;
    }

    public void setCreator(final String creator) {
      this.creator = creator;
    }

    public String getDescription() {
      return description;
    }

    public void setDescription(final String description) {
      this.description = description;
    }

    public String getName() {
      return name;
    }

    public Integer getOrder() {
      return order;
    }

    public void setOrder(final Integer order) {
      this.order = order;
    }

    public DataOverlayType getType() {
      return type;
    }

    public void setType(final DataOverlayType type) {
      this.type = type;
    }

    public Boolean getPublicOverlay() {
      return publicOverlay;
    }

    public void setPublicOverlay(final Boolean publicOverlay) {
      this.publicOverlay = publicOverlay;
    }

  }

  static class UpdateDataOverlay {
    @NotNull
    private UpdateDataOverlayDTO overlay;

    public UpdateDataOverlayDTO getOverlay() {
      return overlay;
    }

    public void setOverlay(final UpdateDataOverlayDTO overlay) {
      this.overlay = overlay;
    }
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId)"
      + " or hasAuthority('READ_PROJECT:' + #projectId) and @dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name")
  @PatchMapping(value = "/{overlayId}")
  public DataOverlay updateOverlay(
      final @Valid @RequestBody UpdateDataOverlay body,
      final @PathVariable(value = "overlayId") Integer overlayId,
      final @PathVariable(value = "projectId") String projectId)
      throws QueryException, IOException, ObjectNotFoundException {
    final UpdateDataOverlayDTO overlayData = body.getOverlay();
    final Set<User> reorderUsers = new HashSet<>();
    final DataOverlay layout = dataOverlayService.getDataOverlayById(projectId, overlayId);
    if (overlayData.getDescription() != null) {
      layout.setDescription(overlayData.getDescription());
    }
    if (overlayData.getName() != null) {
      if (overlayData.getName().trim().isEmpty()) {
        throw new QueryException("name cannot be empty");
      }
      if (overlayData.getName().length() > 255) {
        throw new QueryException("name too long");
      }
      layout.setName(overlayData.getName());
    }
    if (overlayData.getOrder() != null) {
      layout.setOrderIndex(overlayData.getOrder());
    }
    if (overlayData.getType() != null) {
      layout.setColorSchemaType(overlayData.getType());
    }
    if (overlayData.getPublicOverlay() != null) {
      if (!Objects.equals(layout.isPublic(), overlayData.getPublicOverlay())) {
        reorderUsers.add(layout.getCreator());
      }
      layout.setPublic(overlayData.getPublicOverlay());
    }
    if (overlayData.getCreator() != null) {
      if ("".equals(overlayData.getCreator())) {
        throw new QueryException("overlay creator must be defined");
      } else {
        if (layout.getCreator() == null) {
          reorderUsers.add(userService.getUserByLogin(overlayData.getCreator()));
        } else if (!layout.getCreator().getLogin().equals(overlayData.getCreator())) {
          reorderUsers.add(userService.getUserByLogin(overlayData.getCreator()));
          reorderUsers.add(layout.getCreator());
        }
        layout.setCreator(userService.getUserByLogin(overlayData.getCreator()));
      }
    }
    dataOverlayService.updateDataOverlay(layout);
    for (final User user : reorderUsers) {
      reorderOverlays(user, layout.getProject());
    }
    return getOverlayById(layout.getProject().getProjectId(), overlayId);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)"
      + " or hasAuthority('READ_PROJECT:' + #projectId) and "
      + "   (@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name "
      + "  or @dataOverlayService.getDataOverlayById(#overlayId)?.public == true)")
  @GetMapping(value = "/{overlayId}:downloadSource")
  public ResponseEntity<byte[]> getOverlaySource(
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "overlayId") Integer overlayId)
      throws QueryException, ObjectNotFoundException {

    final FileEntry file = dataOverlayService.getDataOverlayFileById(projectId, overlayId);
    MediaType type = MediaType.TEXT_PLAIN;
    String filename = file.getOriginalFileName();
    if (filename != null) {
      if (file.getOriginalFileName().endsWith("xml")) {
        type = MediaType.APPLICATION_XML;
      }
    } else {
      filename = overlayId + ".txt";
    }
    return ResponseEntity.ok().contentLength(file.getFileContent().length).contentType(type)
        .header("Content-Disposition", "attachment; filename=" + filename).body(file.getFileContent());
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)"
      + " or hasAuthority('READ_PROJECT:' + #projectId) and "
      + "   (@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name "
      + "  or @dataOverlayService.getDataOverlayById(#overlayId)?.public == true)")
  @GetMapping(value = "/{overlayId}:downloadLegend", produces = MediaType.IMAGE_PNG_VALUE)
  public ResponseEntity<byte[]> getOverlayLegend(
      final Authentication authentication,
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "overlayId") Integer overlayId)
      throws QueryException, IOException, ObjectNotFoundException {
    final User user = userService.getUserByLogin(authentication.getName());

    final ColorExtractor colorExtractor = userService.getColorExtractorForUser(user);

    final byte[] pngImage = dataOverlayService.generateLegend(projectId, overlayId, colorExtractor);
    final MediaType type = MediaType.IMAGE_PNG;
    final String filename = "legend-" + overlayId + ".png";
    return ResponseEntity.ok().contentLength(pngImage.length).contentType(type)
        .header("Content-Disposition", "attachment; filename=" + filename).body(pngImage);
  }
}