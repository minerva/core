package lcsb.mapviewer.api.projects.models.bioentities.reactions;

import java.awt.geom.Line2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.modelutils.serializer.MathMLSerializer;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IReactionService;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/api/projects/{projectId}/models/{modelId}/bioEntities/reactions"
    })
public class ReactionsController extends BaseController {

  private Logger logger = LogManager.getLogger();

  private PointTransformation pt = new PointTransformation();
  private IReactionService reactionService;

  @Autowired
  public ReactionsController(final IReactionService reactionService) {
    this.reactionService = reactionService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @RequestMapping(value = "/", method = { RequestMethod.GET, RequestMethod.POST }, produces = {
      MediaType.APPLICATION_JSON_VALUE })
  public List<Map<String, Object>> getReactions(
      final @PathVariable(value = "projectId") String projectId,
      final @Valid @Pattern(regexp = "^[-+]?\\d+$|^\\*$") @PathVariable(value = "modelId") String modelId,
      final @RequestParam(value = "id", defaultValue = "") String id,
      final @RequestParam(value = "columns", defaultValue = "") String columns,
      final @RequestParam(value = "participantId", defaultValue = "") String participantId) throws QueryException, ObjectNotFoundException {

    Set<Integer> ids = new LinkedHashSet<>();
    if (!id.equals("")) {
      for (final String str : id.split(",")) {
        ids.add(Integer.valueOf(str));
      }
    }
    Set<Integer> participantIds = new LinkedHashSet<>();
    if (!participantId.equals("")) {
      for (final String str : participantId.split(",")) {
        participantIds.add(Integer.valueOf(str));
      }
    }

    Set<String> columnsSet = createReactionColumnSet(columns);

    List<Reaction> reactions = reactionService.getReactionById(projectId, modelId, ids, participantIds);

    List<Map<String, Object>> result = new ArrayList<>();
    for (final Reaction reaction : reactions) {
      result.add(preparedReaction(reaction, columnsSet));
    }
    return result;
  }

  private Map<String, Object> preparedReaction(final Reaction reaction, final Set<String> columnsSet) {
    Map<String, Object> result = new TreeMap<>();
    for (final String string : columnsSet) {
      String column = string.toLowerCase();
      Object value;
      switch (column) {
        case "id":
        case "idobject":
          value = reaction.getId();
          break;
        case "modelid":
          value = reaction.getModelData().getId();
          break;
        case "reactionid":
          value = reaction.getIdReaction();
          break;
        case "name":
          value = reaction.getName();
          break;
        case "centerpoint":
          Line2D centerLine = reaction.getLine().getLines().get(reaction.getLine().getLines().size() / 2);
          value = pt.getPointOnLine(centerLine.getP1(), centerLine.getP2(), 0.5);
          break;
        case "products": {
          value = reaction.getProducts();
          break;
        }
        case "reactants": {
          value = reaction.getReactants();
          break;
        }
        case "modifiers": {
          value = reaction.getModifiers();
          break;
        }
        case "type":
          value = reaction.getStringType();
          break;
        case "hierarchyvisibilitylevel":
          value = reaction.getVisibilityLevel();
          break;
        case "lines":
          value = getLines(reaction);
          break;
        case "notes":
          value = reaction.getNotes();
          break;
        case "kineticlaw":
          value = kineticsToMap(reaction.getKinetics());
          break;
        case "references":
          value = reaction.getMiriamData();
          break;
        default:
          value = "Unknown column";
          break;
      }
      result.put(string, value);
    }
    return result;
  }

  private List<Map<String, Object>> getLines(final Reaction reaction) {
    List<Map<String, Object>> result = new ArrayList<>();
    for (final Reactant reactant : reaction.getReactants()) {
      result.addAll(getLines(reactant));
    }
    for (final Product product : reaction.getProducts()) {
      result.addAll(getLines(product));
    }
    for (final Modifier modifier : reaction.getModifiers()) {
      result.addAll(getLines(modifier));
    }
    for (final NodeOperator operator : reaction.getOperators()) {
      result.addAll(getLines(operator));
    }
    for (final Line2D line2d : reaction.getLine().getLines()) {
      result.add(lineToMap(line2d, "MIDDLE"));
    }

    return result;
  }

  protected List<Map<String, Object>> getLines(final AbstractNode node) {
    // order in this method is to keep the order from previous version, it can
    // be
    // changed in the future, but I wanted to make that it's giving the same
    // results
    // when refactoring
    List<Map<String, Object>> result = new ArrayList<>();
    List<Line2D> startLines = new ArrayList<>();
    List<Line2D> middleLines = new ArrayList<>();
    List<Line2D> endLines = new ArrayList<>();

    List<Line2D> lines = node.getLine().getLines();

    if (node instanceof Reactant) {
      startLines.add(lines.get(0));
      for (int i = 1; i < lines.size(); i++) {
        middleLines.add(lines.get(i));
      }
    } else if (node instanceof Product) {
      endLines.add(lines.get(lines.size() - 1));
      for (int i = 0; i < lines.size() - 1; i++) {
        middleLines.add(lines.get(i));
      }
    } else if (node instanceof Modifier) {
      middleLines.add(lines.get(lines.size() - 1));
      for (int i = 0; i < lines.size() - 1; i++) {
        middleLines.add(lines.get(i));
      }
    } else {
      middleLines.addAll(lines);
    }

    for (final Line2D line2d : startLines) {
      result.add(lineToMap(line2d, "START"));
    }
    for (final Line2D line2d : endLines) {
      result.add(lineToMap(line2d, "END"));
    }
    for (final Line2D line2d : middleLines) {
      result.add(lineToMap(line2d, "MIDDLE"));
    }

    return result;
  }

  private Map<String, Object> lineToMap(final Line2D line2d, final String type) {
    Map<String, Object> result = new LinkedHashMap<>();
    result.put("start", line2d.getP1());
    result.put("end", line2d.getP2());
    result.put("type", type);
    return result;
  }

  private Map<String, Object> kineticsToMap(final SbmlKinetics kinetics) {
    if (kinetics == null) {
      return null;
    }
    Map<String, Object> result = new TreeMap<>();
    result.put("definition", kinetics.getDefinition());
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      SimpleModule s = new SimpleModule();
      s.addSerializer(String.class, new MathMLSerializer());
      objectMapper.registerModule(s);

      result.put("mathMlPresentation", objectMapper.readValue(objectMapper.writeValueAsString(kinetics.getDefinition()), String.class));
    } catch (final IOException e) {
      logger.error("Problems with transforming kinetics", e);
    }

    List<Integer> functionIds = new ArrayList<>();
    for (final SbmlFunction function : kinetics.getFunctions()) {
      functionIds.add(function.getId());
    }
    result.put("functionIds", functionIds);
    List<Integer> parameterIds = new ArrayList<>();
    for (final SbmlParameter parameter : kinetics.getParameters()) {
      parameterIds.add(parameter.getId());
    }
    result.put("parameterIds", parameterIds);
    return result;
  }

  public List<String> getAvailableReactionColumns() {
    List<String> result = new ArrayList<>();
    result.add("id");
    result.add("reactionId");
    result.add("modelId");
    result.add("type");
    result.add("name");
    result.add("lines");
    result.add("kineticLaw");
    result.add("centerPoint");
    result.add("products");
    result.add("reactants");
    result.add("modifiers");
    result.add("hierarchyVisibilityLevel");
    result.add("references");
    result.add("notes");
    return result;
  }

  private Set<String> createReactionColumnSet(final String columns) {
    Set<String> columnsSet = new LinkedHashSet<>();
    if (columns.equals("")) {
      columnsSet.addAll(getAvailableReactionColumns());
    } else {
      Collections.addAll(columnsSet, columns.split(","));
    }
    return columnsSet;
  }

}