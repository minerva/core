package lcsb.mapviewer.api.projects.models;

import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.core.LogEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.OptionalDoubleDeserializer;
import lcsb.mapviewer.api.OptionalIntegerDeserializer;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.commands.ClearColorModelCommand;
import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.commands.ColorModelCommand;
import lcsb.mapviewer.commands.CommandExecutionException;
import lcsb.mapviewer.commands.CopyCommand;
import lcsb.mapviewer.commands.SubModelCommand;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.converter.graphics.ImageGenerators;
import lcsb.mapviewer.model.IgnoredLogMarker;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectLogEntry;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.cache.FileEntry;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.layout.ProjectBackground;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.overlay.InvalidDataOverlayException;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.map.ModelProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IDataOverlayService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectBackgroundService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.services.utils.data.BuildInBackgrounds;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/api/projects/{projectId:.+}/models"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class ModelController extends BaseController {

  private IUserService userService;
  private IModelService modelService;
  private IProjectService projectService;
  private IProjectBackgroundService projectBackgroundService;
  private IDataOverlayService dataOverlayService;
  private List<Converter> converters;

  @Autowired
  public ModelController(final IUserService userService,
      final IModelService modelService,
      final IProjectService projectService,
      final IProjectBackgroundService projectBackgroundService,
      final IDataOverlayService dataOverlayService,
      final List<Converter> converters) {
    this.userService = userService;
    this.modelService = modelService;
    this.projectService = projectService;
    this.projectBackgroundService = projectBackgroundService;
    this.dataOverlayService = dataOverlayService;
    this.converters = converters;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public List<ModelData> getModels(final @PathVariable(value = "projectId") String projectId)
      throws QueryException, ObjectNotFoundException {
    Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    List<ModelData> result = new ArrayList<>();
    Map<ModelProperty, Object> filter = new HashMap<>();
    filter.put(ModelProperty.PROJECT_ID, project.getProjectId());

    Collection<ModelData> originalModels = modelService.getAll(filter, Pageable.unpaged()).getContent();

    if (project.getTopModelData() != null) {
      int topModelId = project.getTopModelData().getId();
      ModelData topModel = null;
      for (ModelData model : originalModels) {
        if (!Objects.equals(model.getId(), topModelId)) {
          result.add(model);
        } else {
          topModel = model;
        }
      }
      result.sort(ModelData.ID_COMPARATOR);
      if (topModel != null) {
        result.add(0, topModel);
      }
    }
    return result;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{modelId:.+}")
  public Object getModel(
      final @Valid @Pattern(regexp = "^[-+]?\\d+$|^\\*$") @PathVariable(value = "modelId") String modelId,
      final @PathVariable(value = "projectId") String projectId) throws QueryException, ObjectNotFoundException {
    if (modelId.equals("*")) {
      return getModels(projectId);
    } else {
      try {
        ModelData result = modelService.getModelByMapId(projectId, Integer.valueOf(modelId));
        if (result == null) {
          throw new ObjectNotFoundException("Map with given id doesn't exist");
        }

        return result;
      } catch (final NumberFormatException e) {
        throw new QueryException("Invalid modelId", e);
      }
    }
  }

  static class UpdateModelDTO {

    @JsonProperty("defaultCenterX")
    @JsonDeserialize(using = OptionalDoubleDeserializer.class)
    private Optional<Number> defaultCenterX = Optional.empty();

    @JsonProperty("defaultCenterY")
    @JsonDeserialize(using = OptionalDoubleDeserializer.class)
    private Optional<Number> defaultCenterY = Optional.empty();

    @JsonProperty("defaultZoomLevel")
    @JsonDeserialize(using = OptionalIntegerDeserializer.class)
    private Optional<Number> defaultZoomLevel = Optional.empty();

    @JsonProperty("id")
    private Optional<Integer> id = Optional.empty();

    public Optional<Number> getDefaultCenterX() {
      return defaultCenterX;
    }

    public void setDefaultCenterX(final Optional<Number> defaultCenterX) {
      this.defaultCenterX = defaultCenterX;
    }

    public Optional<Number> getDefaultCenterY() {
      return defaultCenterY;
    }

    public void setDefaultCenterY(final Optional<Number> defaultCenterY) {
      this.defaultCenterY = defaultCenterY;
    }

    public Optional<Number> getDefaultZoomLevel() {
      return defaultZoomLevel;
    }

    public void setDefaultZoomLevel(final Optional<Number> defaultZoomLevel) {
      this.defaultZoomLevel = defaultZoomLevel;
    }

    public Optional<Integer> getId() {
      return id;
    }

    public void setId(final Optional<Integer> id) {
      this.id = id;
    }
  }

  static class UpdateModelBody {
    @NotNull
    private UpdateModelDTO model;

    public UpdateModelDTO getModel() {
      return model;
    }

    public void setModel(final UpdateModelDTO model) {
      this.model = model;
    }
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PatchMapping(value = "/{modelId:.+}")
  public ModelData updateModel(
      final @PathVariable(value = "modelId") Integer mapId,
      final @PathVariable(value = "projectId") String projectId,
      final @Valid @RequestBody UpdateModelBody body) throws IOException, QueryException, ObjectNotFoundException {

    ModelData model = modelService.getModelByMapId(projectId, mapId);
    if (body.getModel().getDefaultCenterX() == null) {
      model.setDefaultCenterX(null);
    } else if (body.getModel().getDefaultCenterX().isPresent()) {
      model.setDefaultCenterX(body.getModel().getDefaultCenterX().get().doubleValue());
    }
    if (body.getModel().getDefaultCenterY() == null) {
      model.setDefaultCenterY(null);
    } else if (body.getModel().getDefaultCenterY().isPresent()) {
      model.setDefaultCenterY(body.getModel().getDefaultCenterY().get().doubleValue());
    }
    if (body.getModel().getDefaultZoomLevel() == null) {
      model.setDefaultZoomLevel(null);
    } else if (body.getModel().getDefaultZoomLevel().isPresent()) {
      model.setDefaultZoomLevel(body.getModel().getDefaultZoomLevel().get().intValue());
    }
    if (body.getModel().getId() != null && body.getModel().getId().isPresent()) {
      if (!Objects.equals(body.getModel().getId().get(), model.getId())) {
        throw new QueryException("Id doesn't match: " + body.getModel().getId().get() + ", " + model.getId());
      }
    }
    modelService.update(model);
    return model;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{modelId:.+}:downloadImage")
  public ResponseEntity<byte[]> getModelAsImage(
      final Authentication authentication,
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "modelId") Integer mapId,
      final @RequestParam(value = "handlerClass") String handlerClass,
      final @RequestParam(value = "backgroundOverlayId", required = false) Integer backgroundOverlayId,
      final @RequestParam(value = "overlayIds", defaultValue = "") String overlayIds,
      final @RequestParam(value = "zoomLevel", required = false) Double zoomLevel,
      final @RequestParam(value = "polygonString", defaultValue = "") String polygonString)
      throws QueryException, IOException, InvalidDataOverlayException, CommandExecutionException, DrawingException, ObjectNotFoundException {
    User user = userService.getUserByLogin(authentication.getName());

    ColorExtractor colorExtractor = userService.getColorExtractorForUser(user);

    ProjectBackground background = null;
    if (backgroundOverlayId != null) {
      background = projectBackgroundService.getProjectBackgroundById(projectId, backgroundOverlayId);
    } else {
      List<ProjectBackground> backgrounds = projectBackgroundService.getProjectBackgroundsByProject(projectId);
      if (backgrounds.size() > 0) {
        background = backgrounds.get(0);
      }
    }

    Model originalModel = modelService.getAndFetchModelByMapId(projectId, mapId);
    boolean sbgn = originalModel.getProject().isSbgnFormat();
    originalModel.setProject(null);

    Model colorModel = new CopyCommand(originalModel).execute();

    if (background != null) {
      if (Objects.equals(background.getName(), BuildInBackgrounds.CLEAN.getName())) {
        // this might not return true if we change CLEAN.title in future...

        // if it's clean then remove coloring
        new ClearColorModelCommand(colorModel).execute();
      }
    }

    double level = Configuration.MIN_ZOOM_LEVEL;
    if (zoomLevel != null) {
      level = zoomLevel;
    }

    Path2D polygon = stringToPolygon(polygonString, colorModel);

    SubModelCommand subModelCommand = new SubModelCommand(colorModel, polygon);

    Model part = subModelCommand.execute();

    Double minX = polygon.getBounds2D().getMinX();
    Double minY = polygon.getBounds2D().getMinY();
    Double maxX = polygon.getBounds2D().getMaxX();
    Double maxY = polygon.getBounds2D().getMaxY();

    maxX = Math.min(originalModel.getWidth(), maxX);
    maxY = Math.min(originalModel.getHeight(), maxY);
    minX = Math.max(0.0, minX);
    minY = Math.max(0.0, minY);

    maxX = Math.max(minX + 1, maxX);
    maxY = Math.max(minY + 1, maxY);

    double scale = Math.max(originalModel.getHeight(), originalModel.getWidth())
        / (originalModel.getTileSize());

    scale /= Math.pow(2, level - Configuration.MIN_ZOOM_LEVEL);

    Params params = new Params().x(minX).y(minY).height((maxY - minY) / scale).width((maxX - minX) / scale)
        .level((int) level - Configuration.MIN_ZOOM_LEVEL)
        // automatically set nested view as disabled
        .nested(false).scale(scale).colorExtractor(colorExtractor).sbgn(sbgn).model(part);
    if (background != null) {
      params.nested(background.isHierarchicalView());
    }
    List<Integer> visibleDataOverlaysIds = stringListToIntegerList(overlayIds);
    for (final Integer integer : visibleDataOverlaysIds) {
      List<Pair<? extends BioEntity, DataOverlayEntry>> map = dataOverlayService.getBioEntitiesForDataOverlay(projectId,
          mapId,
          integer,
          false);
      params.addVisibleDataOverlay(map);
    }

    ImageGenerators imageGenerator = new ImageGenerators();
    if (!imageGenerator.isValidClassName(handlerClass)) {
      throw new QueryException("Invalid handlerClass");
    }
    String extension = imageGenerator.getExtension(handlerClass);
    File file = File.createTempFile("map", "." + extension);

    imageGenerator.generate(handlerClass, params, file.getAbsolutePath());

    byte[] content = IOUtils.toByteArray(new FileInputStream(file));
    file.delete();

    return ResponseEntity.ok().contentLength(content.length)
        .contentType(MediaType.APPLICATION_OCTET_STREAM)
        .header("Content-Disposition", "attachment; filename=" + "map." + extension)
        .body(content);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @RequestMapping(value = "/{modelId:.+}:downloadModel", method = { RequestMethod.GET,
      RequestMethod.POST }, produces = { MediaType.APPLICATION_OCTET_STREAM_VALUE, "application/zip" })
  public ResponseEntity<byte[]> getModelAsModelFile(
      final Authentication authentication,
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "modelId") Integer modelId,
      final @RequestParam(value = "handlerClass") String handlerClass,
      final @RequestParam(value = "overlayIds", defaultValue = "") String overlayIds,
      final @RequestParam(value = "backgroundOverlayId", required = false) Integer backgroundOverlayId,
      final @RequestParam(value = "polygonString", defaultValue = "") String polygonString,
      final @RequestParam(value = "strictCutoff", defaultValue = "true") boolean strictCutoff,
      final @RequestParam(value = "elementIds", defaultValue = "") String elementIds,
      final @RequestParam(value = "reactionIds", defaultValue = "") String reactionIds,
      final @RequestHeader(value = "Accept", required = false) String accept)
      throws Exception {

    User user = userService.getUserByLogin(authentication.getName());
    FileEntry file = getModelAsModelFile(
        projectId, modelId, handlerClass, overlayIds, polygonString, elementIds, reactionIds, user,
        backgroundOverlayId, strictCutoff);
    if (isMimeTypeIncluded(accept, MimeType.ZIP.getTextRepresentation())) {
      return sendAsZip(file);
    }
    if (file.getFileContent().length >= 1024 * 1024) {
      return sendAsZip(file);
    }
    return ResponseEntity.ok().contentLength(file.getFileContent().length)
        .contentType(MediaType.APPLICATION_OCTET_STREAM)
        .header("Content-Disposition", "attachment; filename=" + file.getOriginalFileName())
        .body(file.getFileContent());
  }

  private FileEntry getModelAsModelFile(final String projectId, final Integer mapId, final String handlerClass,
      final String overlayIds, final String polygonString, final String elementIds,
      final String reactionIds, final User user, final Integer backgroundOverlayId, final boolean strictCutoff)
      throws QueryException, IOException, InvalidDataOverlayException, CommandExecutionException,
      ConverterException, InconsistentModelException, ObjectNotFoundException {

    Model originalModel = modelService.getAndFetchModelByMapId(projectId, mapId);
    originalModel.setProject(null);

    Path2D polygon = stringToPolygon(polygonString, originalModel);

    Set<Integer> elementIdsList = stringListToIntegerSet(elementIds);
    Set<Integer> reactionIdsList = stringListToIntegerSet(reactionIds);

    // create model bounded by the polygon
    SubModelCommand subModelCommand = new SubModelCommand(originalModel, polygon, elementIdsList, reactionIdsList, strictCutoff);
    Model part = subModelCommand.execute();

    // Get list of overlay ids
    List<Integer> overlayIdsList = stringListToIntegerList(overlayIds);
    // Remove all colors
    if (overlayIdsList.size() > 0) {
      new ClearColorModelCommand(part).execute();
    }

    if (backgroundOverlayId != null) {
      ProjectBackground background = projectBackgroundService.getProjectBackgroundById(projectId, backgroundOverlayId);

      if (background == null) {
        throw new ObjectNotFoundException("Unknown background in model. Background id=" + backgroundOverlayId);
      }
      if (Objects.equals(background.getName(), BuildInBackgrounds.CLEAN.getName())) {
        // this might not return true if we change CLEAN.name in future...

        // if it's clean then remove coloring
        new ClearColorModelCommand(part).execute();
      }
    }

    // Color with overlays
    for (final Integer overlayId : overlayIdsList) {
      Set<DataOverlayEntry> schemas = dataOverlayService.getDataOverlayEntriesById(projectId, overlayId);

      new ColorModelCommand(part, schemas, userService.getColorExtractorForUser(user)).execute();
    }

    Converter parser = getModelParser(handlerClass);
    InputStream is = parser.model2InputStream(part);

    String fileExtension = parser.getFileExtensions().get(0);

    UploadedFileEntry entry = new UploadedFileEntry();
    entry.setOriginalFileName("model." + fileExtension);
    entry.setFileContent(IOUtils.toByteArray(is));
    entry.setLength(entry.getFileContent().length);
    return entry;

  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @RequestMapping(value = "/{modelId:.+}:downloadModelWarnings", method = { RequestMethod.GET,
      RequestMethod.POST })
  public MappingJacksonValue getModelWarningsAsModelFile(
      final Authentication authentication,
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "modelId") Integer modelId,
      final @RequestParam(value = "handlerClass") String handlerClass,
      final @RequestParam(value = "overlayIds", defaultValue = "") String overlayIds,
      final @RequestParam(value = "strictCutoff", defaultValue = "true") boolean strictCutoff,
      final @RequestParam(value = "backgroundOverlayId", required = false) Integer backgroundOverlayId,
      final @RequestParam(value = "polygonString", defaultValue = "") String polygonString,
      final @RequestParam(value = "elementIds", defaultValue = "") String elementIds,
      final @RequestParam(value = "reactionIds", defaultValue = "") String reactionIds)
      throws Exception {
    User user = userService.getUserByLogin(authentication.getName());

    MinervaLoggerAppender appender = MinervaLoggerAppender.createAppender(true);
    try {
      Map<String, Object> result = new HashedMap<>();
      getModelAsModelFile(projectId, modelId, handlerClass, overlayIds, polygonString, elementIds,
          reactionIds, user,
          backgroundOverlayId, strictCutoff);
      List<ProjectLogEntry> warnings = new ArrayList<>();
      for (final LogEvent event : appender.getWarnings()) {
        ProjectLogEntry entry = createLogEntry("WARNING", event);
        if (entry != null) {
          warnings.add(entry);
        }
      }

      result.put("data", warnings);

      MappingJacksonValue wrapper = new MappingJacksonValue(result);

      wrapper.setFilters(new SimpleFilterProvider()
          .addFilter("projectLogEntryFilter",
              SimpleBeanPropertyFilter.filterOutAllExcept("type", "severity", "objectIdentifier", "objectClass",
                  "content")));

      return wrapper;
    } finally {
      MinervaLoggerAppender.unregisterLogEventStorage(appender);
    }
  }

  protected Converter getModelParser(final String handlerClass) throws QueryException {
    for (final Converter converter : converters) {
      if (converter.getClass().getCanonicalName().equals(handlerClass)) {
        try {
          return (Converter) Class.forName(handlerClass).getConstructor().newInstance();
        } catch (final InstantiationException | IllegalAccessException | ClassNotFoundException | IllegalArgumentException | InvocationTargetException
            | NoSuchMethodException | SecurityException e) {
          throw new QueryException("Problem with handler:" + handlerClass);
        }
      }
    }
    throw new QueryException("Unknown handlerClass: " + handlerClass);
  }

  private Path2D stringToPolygon(final String polygonString, final Model colorModel) {
    String[] stringPointArray = polygonString.split(";");

    List<Point2D> points = new ArrayList<>();
    for (final String string : stringPointArray) {
      if (!string.trim().equals("")) {
        double x = Double.valueOf(string.split(",")[0]);
        double y = Double.valueOf(string.split(",")[1]);
        points.add(new Point2D.Double(x, y));
      }
    }

    if (points.size() <= 2) {
      points.clear();
      points.add(new Point2D.Double(0, 0));
      points.add(new Point2D.Double(colorModel.getWidth(), 0));
      points.add(new Point2D.Double(colorModel.getWidth(), colorModel.getHeight()));
      points.add(new Point2D.Double(0, colorModel.getHeight()));
    }

    Path2D polygon = new Path2D.Double();
    polygon.moveTo(points.get(0).getX(), points.get(0).getY());
    for (int i = 1; i < points.size(); i++) {
      Point2D point = points.get(i);
      polygon.lineTo(point.getX(), point.getY());
    }
    polygon.closePath();
    return polygon;
  }

  protected Set<Integer> stringListToIntegerSet(final String elementIds) {
    Set<Integer> result = new LinkedHashSet<>();
    if (elementIds != null) {
      String[] stringIds = elementIds.split(",");
      for (final String string : stringIds) {
        if (!string.trim().isEmpty()) {
          result.add(Integer.valueOf(string));
        }
      }
    }
    return result;
  }

  private List<Integer> stringListToIntegerList(final String overlayIds) {
    List<Integer> result = new ArrayList<>();
    if (overlayIds != null && !overlayIds.trim().isEmpty()) {
      for (final String string : overlayIds.split(",")) {
        result.add(Integer.parseInt(string));
      }
    }
    return result;
  }

  private ProjectLogEntry createLogEntry(final String severity, final LogEvent event) {
    if (event.getMarker() instanceof IgnoredLogMarker) {
      return null;
    }
    StackTraceElement stackTraceElement = event.getSource();
    if (stackTraceElement == null) {
      return null;
    }
    if (stackTraceElement.getClassName() == null || !stackTraceElement.getClassName().startsWith("lcsb.mapviewer")) {
      return null;
    }
    ProjectLogEntry entry = null;
    if (event.getMarker() != null) {
      if (event.getMarker() instanceof LogMarker) {
        entry = ((LogMarker) event.getMarker()).getEntry();
      }
    }
    if (entry == null) {
      entry = new ProjectLogEntry();
      entry.setType(ProjectLogEntryType.OTHER);
    }
    entry.setSeverity(severity);
    entry.setContent(event.getMessage().getFormattedMessage());
    return entry;
  }

}