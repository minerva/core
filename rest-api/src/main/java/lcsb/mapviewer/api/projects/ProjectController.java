package lcsb.mapviewer.api.projects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.OperationNotAllowedException;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.api.UpdateConflictException;
import lcsb.mapviewer.api.minervanet.MinervaNetController;
import lcsb.mapviewer.common.MinervaConfigurationHolder;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.zip.ZipEntryFile;
import lcsb.mapviewer.model.License;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectLogEntry;
import lcsb.mapviewer.model.ProjectStatus;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.layout.ProjectBackground;
import lcsb.mapviewer.model.map.layout.ProjectBackgroundImageLayer;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.modelutils.map.ValidationUtil;
import lcsb.mapviewer.modelutils.serializer.id.ModelDataAsModelIdSerializer;
import lcsb.mapviewer.modelutils.serializer.model.map.MinifiedBioEntitySerializer;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.FailedDependencyException;
import lcsb.mapviewer.services.ObjectExistsException;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IFileService;
import lcsb.mapviewer.services.interfaces.ILicenseService;
import lcsb.mapviewer.services.interfaces.IMeshService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectBackgroundService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.services.utils.CreateProjectParams;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.beans.PropertyEditorSupport;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/api/projects"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class ProjectController extends BaseController {

  private final Logger logger = LogManager.getLogger();

  private final MinervaNetController minervaNetController;
  private final IProjectService projectService;
  private final IDirectoryNameGenerator directoryNameGenerator;
  private final IConfigurationService configurationService;
  private final IModelService modelService;
  private final IElementService elementService;
  private final IReactionService reactionService;
  private final IUserService userService;
  private final IFileService fileService;
  private final IMeshService meshService;
  private final ObjectMapper objectMapper = new ObjectMapper();
  private final IProjectBackgroundService projectBackgroundService;
  private final MinervaConfigurationHolder minervaConfigurationHolder;
  private final ILicenseService licenseService;

  private final List<Converter> modelConverters;

  @Autowired
  public ProjectController(
      final MinervaConfigurationHolder minervaConfigurationHolder,
      final IUserService userService,
      final IProjectService projectService,
      final IMeshService meshService, final List<Converter> modelConverters, final IFileService fileService,
      final IElementService elementService,
      final IReactionService reactionService, final IModelService modelService,
      final IConfigurationService configurationService,
      final IProjectBackgroundService projectBackgroundService, final MinervaNetController minervaNetController,
      final ILicenseService licenseService,
      final IDirectoryNameGenerator directoryNameGenerator) {
    this.minervaConfigurationHolder = minervaConfigurationHolder;
    this.userService = userService;
    this.projectService = projectService;
    this.meshService = meshService;
    this.modelConverters = modelConverters;
    this.fileService = fileService;
    this.elementService = elementService;
    this.modelService = modelService;
    this.reactionService = reactionService;
    this.configurationService = configurationService;
    this.projectBackgroundService = projectBackgroundService;
    this.minervaNetController = minervaNetController;
    this.directoryNameGenerator = directoryNameGenerator;
    this.licenseService = licenseService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{projectId:.+}")
  public ProjectDTO getProject(final @PathVariable(value = "projectId") String projectId)
      throws ObjectNotFoundException {
    final Project project = projectService.getProjectByProjectId(projectId, true);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    final String defaultMapId = getDefaultProjectId();
    try {
      return new ProjectDTO(project, minervaNetController.getSharedProjects().contains(projectId), Objects.equals(projectId, defaultMapId));
    } catch (final FailedDependencyException | IOException e) {
      logger.error("Problem with minervanet", e);
      return new ProjectDTO(project, false, Objects.equals(projectId, defaultMapId));
    }
  }

  private String getDefaultProjectId() {
    return configurationService.getConfigurationValue(ConfigurationElementType.DEFAULT_MAP);
  }

  static class UpdateProjectData {
    private ProjectData project;

    public ProjectData getProject() {
      return project;
    }

    public void setProject(final ProjectData project) {
      this.project = project;
    }
  }

  static class ProjectData {
    private String version;
    private String owner;
    private String projectId;
    private String name;
    private String notifyEmail;
    private Optional<MiriamData> organism;
    private Optional<MiriamData> disease;
    private Optional<License> license;

    private String customLicenseName;
    private String customLicenseUrl;

    public String getVersion() {
      return version;
    }

    public void setVersion(final String version) {
      this.version = version;
    }

    public String getProjectId() {
      return projectId;
    }

    public void setProjectId(final String projectId) {
      this.projectId = projectId;
    }

    public String getName() {
      return name;
    }

    public void setName(final String name) {
      this.name = name;
    }

    public String getNotifyEmail() {
      return notifyEmail;
    }

    public void setNotifyEmail(final String notifyEmail) {
      this.notifyEmail = notifyEmail;
    }

    public Optional<MiriamData> getOrganism() {
      return organism;
    }

    public void setOrganism(final Optional<MiriamData> organism) {
      this.organism = organism;
    }

    public Optional<MiriamData> getDisease() {
      return disease;
    }

    public void setDisease(final Optional<MiriamData> disease) {
      this.disease = disease;
    }

    public String getOwner() {
      return owner;
    }

    public void setOwner(final String owner) {
      this.owner = owner;
    }

    public Optional<License> getLicense() {
      return license;
    }

    public void setLicense(final Optional<License> license) {
      this.license = license;
    }

    public String getCustomLicenseName() {
      return customLicenseName;
    }

    public void setCustomLicenseName(final String customLicenseName) {
      this.customLicenseName = customLicenseName;
    }

    public String getCustomLicenseUrl() {
      return customLicenseUrl;
    }

    public void setCustomLicenseUrl(final String customLicenseUrl) {
      this.customLicenseUrl = customLicenseUrl;
    }
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PatchMapping(value = "/{projectId:.+}", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ProjectDTO updateProject(
      final @RequestBody UpdateProjectData body,
      final Authentication authentication,
      final @PathVariable(value = "projectId") String projectId) throws IOException, QueryException, ObjectNotFoundException {
    final ProjectData data = body.getProject();
    final Project project = projectService.getProjectByProjectId(projectId, true);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    if (data.getVersion() != null) {
      if (data.getVersion().length() > 20) {
        throw new QueryException("version is too long (>20 characters)");
      }
      project.setVersion(data.getVersion());
    }

    if (data.getProjectId() != null) {
      if (!project.getProjectId().equalsIgnoreCase(data.getProjectId())) {
        throw new QueryException("You cannot modify projectId");
      }
    }
    if (data.getName() != null) {
      if (data.getName().length() > 255) {
        throw new QueryException("name is too long (>255 characters)");
      }
      project.setName(data.getName());
    }
    if (data.getNotifyEmail() != null) {
      project.setNotifyEmail(data.getNotifyEmail());
    }
    if (data.getOwner() != null && !data.getOwner().trim().isEmpty()) {
      if (!Objects.equals(project.getOwner().getLogin(), data.getOwner())) {
        final User user = userService.getUserByLogin(authentication.getName(), true);
        if (user.getPrivileges().contains(new Privilege(PrivilegeType.IS_ADMIN))) {
          final User newOwner = userService.getUserByLogin(data.getOwner());
          if (newOwner == null) {
            throw new ObjectNotFoundException("User " + data.getOwner() + " does not exist");
          }
          project.setOwner(newOwner);
        } else {
          throw new AccessDeniedException("Access denied");
        }
      }
    }
    if (data.getOrganism().isPresent()) {
      final MiriamData organism = updateMiriamData(project.getOrganism(), data.getOrganism().get());
      project.setOrganism(organism);
    }
    if (data.getDisease().isPresent()) {
      try {
        final MiriamData sourceData = updateMiriamData(null, data.getDisease().get());
        if (meshService.isValidMeshId(sourceData)) {
          final MiriamData disease = updateMiriamData(project.getDisease(), data.getDisease().get());
          project.setDisease(disease);
        } else if (sourceData == null || sourceData.getResource().isEmpty()) {
          project.setDisease(null);
        } else {
          throw new QueryException("invalid mesh identifier: " + data.getDisease().get());
        }
      } catch (final AnnotatorException e) {
        throw new QueryException("invalid miriamData: " + data.getDisease().get(), e);
      }
    }
    if (data.getCustomLicenseName() != null) {
      project.setCustomLicenseName(data.getCustomLicenseName());
    }

    if (data.getCustomLicenseUrl() != null) {
      project.setCustomLicenseUrl(data.getCustomLicenseUrl());
    }

    if (data.getLicense() != null && data.getLicense().isPresent()) {
      if (data.getLicense().get() != null) {
        Integer id = data.getLicense().get().getId();
        License license = getLicenseById(id);
        project.setLicense(license);
      } else {
        project.setLicense(null);
      }
    }

    projectService.update(project);
    return getProject(projectId);
  }

  private License getLicenseById(final Integer id) throws QueryException {
    if (id != null && id != 0) {
      License license = licenseService.getById(id);
      if (license == null) {
        throw new QueryException("invalid license id: " + id);
      }
      return license;
    } else {
      return null;
    }
  }

  private MiriamData updateMiriamData(final MiriamData original, final MiriamData newData) {
    if (newData == null) {
      return null;
    }
    if (newData.getResource() == null || newData.getResource().isEmpty()) {
      return null;
    }
    MiriamData result = original;
    if (result == null) {
      result = new MiriamData();
    }

    result.setDataType(newData.getDataType());
    result.setResource(newData.getResource());
    return result;
  }

  static class PrivilegeDTO {
    private String login;
    private PrivilegeType privilegeType;

    public String getLogin() {
      return login;
    }

    public void setLogin(final String login) {
      this.login = login;
    }

    public PrivilegeType getPrivilegeType() {
      return privilegeType;
    }

    public void setPrivilegeType(final PrivilegeType privilegeType) {
      this.privilegeType = privilegeType;
    }
  }

  @PreAuthorize("hasAuthority('IS_ADMIN') "
      + "or (hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId))")
  @PatchMapping(value = "/{projectId:.+}:grantPrivileges")
  public void grantPrivileges(
      final @RequestBody List<PrivilegeDTO> privileges,
      final @PathVariable(value = "projectId") String projectId) throws IOException, QueryException, ObjectNotFoundException {
    getProject(projectId);
    for (final PrivilegeDTO m : privileges) {
      final User user = userService.getUserByLogin(m.getLogin());
      if (user == null) {
        throw new ObjectNotFoundException("User does not exist");
      }
      userService.grantUserPrivilege(user, m.getPrivilegeType(), projectId);
    }
  }

  @PreAuthorize("hasAuthority('IS_ADMIN') "
      + "or (hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId))")
  @PatchMapping(value = "/{projectId:.+}:revokePrivileges")
  public void revokePrivileges(
      final @RequestBody List<PrivilegeDTO> privileges,
      final @PathVariable(value = "projectId") String projectId) throws IOException, QueryException, ObjectNotFoundException {
    getProject(projectId);
    for (final PrivilegeDTO m : privileges) {
      final User user = userService.getUserByLogin(m.getLogin());
      if (user == null) {
        throw new ObjectNotFoundException("User does not exist");
      }
      userService.revokeUserPrivilege(user, m.getPrivilegeType(), projectId);
    }
  }

  static class CreateProjectDTO {
    private String projectId;

    @NotNull
    @JsonProperty("file-id")
    private Integer fileId;

    @JsonProperty("license-id")
    private Integer licenseId;

    @JsonProperty("custom-license-name")
    private String customLicenseName;

    @JsonProperty("custom-license-url")
    private String customLicenseUrl;

    @NotEmpty
    private String parser;

    @JsonProperty("auto-resize")
    private String autoResize;
    @JsonProperty("notify-email")
    private String notifyEmail;
    private String description;
    private String disease;

    @Length(max = 255)
    private String name;
    private String organism;
    private String sbgn;

    @Length(max = 20)
    private String version;
    private String annotate;
    @JsonProperty("zip-entries")
    private Map<String, ZipEntryFile> zipEntries;

    public String getProjectId() {
      return projectId;
    }

    public void setProjectId(final String projectId) {
      this.projectId = projectId;
    }

    public Integer getFileId() {
      return fileId;
    }

    public void setFileId(final Integer fileId) {
      this.fileId = fileId;
    }

    public String getParser() {
      return parser;
    }

    public void setParser(final String parser) {
      this.parser = parser;
    }

    public String getAutoResize() {
      return autoResize;
    }

    public void setAutoResize(final String autoResize) {
      this.autoResize = autoResize;
    }

    public String getNotifyEmail() {
      return notifyEmail;
    }

    public void setNotifyEmail(final String notifyEmail) {
      this.notifyEmail = notifyEmail;
    }

    public String getDescription() {
      return description;
    }

    public void setDescription(final String description) {
      this.description = description;
    }

    public String getDisease() {
      return disease;
    }

    public void setDisease(final String disease) {
      this.disease = disease;
    }

    public String getName() {
      return name;
    }

    public void setName(final String name) {
      this.name = name;
    }

    public String getOrganism() {
      return organism;
    }

    public void setOrganism(final String organism) {
      this.organism = organism;
    }

    public String getSbgn() {
      return sbgn;
    }

    public void setSbgn(final String sbgn) {
      this.sbgn = sbgn;
    }

    public String getVersion() {
      return version;
    }

    public void setVersion(final String version) {
      this.version = version;
    }

    public String getAnnotate() {
      return annotate;
    }

    public void setAnnotate(final String annotate) {
      this.annotate = annotate;
    }

    public Map<String, ZipEntryFile> getZipEntries() {
      return zipEntries;
    }

    public void setZipEntries(final Map<String, ZipEntryFile> zipEntries) {
      this.zipEntries = zipEntries;
    }

    public Integer getLicenseId() {
      return licenseId;
    }

    public String getCustomLicenseName() {
      return customLicenseName;
    }

    public void setCustomLicenseName(final String customLicenseName) {
      this.customLicenseName = customLicenseName;
    }

    public String getCustomLicenseUrl() {
      return customLicenseUrl;
    }

    public void setCustomLicenseUrl(final String customLicenseUrl) {
      this.customLicenseUrl = customLicenseUrl;
    }
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'IS_CURATOR')")
  @PostMapping(value = "/{projectId:.+}")
  public ProjectDTO addProject(
      final Authentication authentication,
      final @RequestParam Map<String, Object> body,
      final @Size(max = 255) @Pattern(regexp = "^[a-z0-9A-Z\\-_]+$",
          message = "projectId can contain only alphanumeric characters and -_")
      @PathVariable(value = "projectId") String projectId)
      throws IOException, QueryException, SecurityException, ObjectExistsException, ObjectNotFoundException {
    fixFormUrlEncodedBody(body);
    if (Objects.equals(body.get("zip-entries"), "")) {
      body.remove("zip-entries");
    }
    final CreateProjectDTO data = objectMapper.readValue(objectMapper.writeValueAsString(body), CreateProjectDTO.class);
    try {
      ValidationUtil.validate(data);
    } catch (final InvalidArgumentException e) {
      throw new QueryException(e.getMessage(), e);
    }

    final User user = userService.getUserByLogin(authentication.getName());
    if (projectService.projectExists(projectId)) {
      throw new ObjectExistsException("Project with given id already exists");
    }
    final CreateProjectParams params = new CreateProjectParams();
    final String directory = computePathForProject(projectId);

    final UploadedFileEntry file = fileService.getById(data.getFileId());
    if (file == null) {
      throw new QueryException("Invalid file id: " + data.getFileId());
    }

    if (file.getOwner() == null || !file.getOwner().getLogin().equals(user.getLogin())) {
      throw new AccessDeniedException("Access denied to source file");
    }
    final Converter parser = getModelParser(data.getParser());

    if (data.getZipEntries() != null) {
      params.complex(data.getZipEntries().size() > 0);
      for (final ZipEntryFile entry : data.getZipEntries().values()) {
        params.addZipEntry(entry);
      }
    }

    params.parser(parser);
    params.autoResize(data.getAutoResize());
    params.description(data.getDescription());
    params.images(true);
    params.notifyEmail(data.getNotifyEmail());
    params.projectDir(directory);
    params.projectDisease(data.getDisease());
    params.projectFile(file.getId());
    params.projectId(projectId);
    params.projectName(data.getName());
    params.projectOrganism(data.getOrganism());
    params.sbgnFormat(data.getSbgn());
    params.version(data.getVersion());
    params.annotations(data.getAnnotate());
    params.setUser(user);
    params.license(getLicenseById(data.getLicenseId()));
    params.customLicenseName(data.getCustomLicenseName());
    params.customLicenseUrl(data.getCustomLicenseUrl());

    projectService.submitCreateProjectJob(params);

    userService.grantUserPrivilege(user, PrivilegeType.WRITE_PROJECT, projectId);
    userService.grantUserPrivilege(user, PrivilegeType.READ_PROJECT, projectId);
    userService.grantPrivilegeToAllUsersWithDefaultAccess(PrivilegeType.READ_PROJECT, projectId);
    userService.grantPrivilegeToAllUsersWithDefaultAccess(PrivilegeType.WRITE_PROJECT, projectId);

    return getProject(projectId);
  }

  @SuppressWarnings("unchecked")
  void fixFormUrlEncodedBody(final Map<String, Object> body) {
    int fixedKeys;
    do {
      fixedKeys = 0;
      final Set<String> keys = new HashSet<>(body.keySet());
      for (final String key : keys) {
        if (key.endsWith("]")) {
          final int from = key.lastIndexOf("[") + 1;
          final int to = key.length() - 1;
          final String subkey = key.substring(from, to);
          final String rootKey = key.substring(0, from - 1);
          final Object value = body.get(key);
          Map<String, Object> elementMap = (Map<String, Object>) body.get(rootKey);
          if (elementMap == null) {
            elementMap = new HashMap<>();
            body.put(rootKey, elementMap);
          }
          if (elementMap.get(subkey) != null) {
            if (elementMap.get(subkey) instanceof Map && value instanceof Map) {
              ((Map<String, Object>) value).putAll((Map<String, Object>) elementMap.get(subkey));
            }
          }
          elementMap.put(subkey, value);

          body.remove(key);
          fixedKeys++;
        }
      }
    } while (fixedKeys > 0);
  }

  protected String computePathForProject(final String projectId) {
    return minervaConfigurationHolder.getDataPath() + "/map_images/" + directoryNameGenerator.projectIdToDirectoryName(projectId) + "/";
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{projectId:.+}")
  public ProjectDTO removeProject(final @PathVariable(value = "projectId") String projectId)
      throws QueryException, FailedDependencyException, ObjectNotFoundException {
    if (configurationService.getConfigurationValue(ConfigurationElementType.DEFAULT_MAP).equals(projectId)) {
      throw new OperationNotAllowedException("You cannot remove default map");
    }

    final ProjectDTO project = getProject(projectId);
    projectService.submitRemoveProjectJob(project);

    userService.revokeObjectDomainPrivilegesForAllUsers(PrivilegeType.WRITE_PROJECT, projectId);
    userService.revokeObjectDomainPrivilegesForAllUsers(PrivilegeType.READ_PROJECT, projectId);

    final String token = configurationService.getConfigurationValue(ConfigurationElementType.MINERVANET_AUTH_TOKEN);
    if (token != null && !token.isEmpty()) {
      try {
        if (minervaNetController.getSharedProjects().contains(projectId)) {
          minervaNetController.unregisterProject(projectId);
        }
      } catch (final IOException | QueryException e) {
        logger.error("Problem with unregistering from minerva net", e);
      }
    }

    return project;
  }

  @PostFilter("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + filterObject['projectId'])")
  @GetMapping(value = "/")
  public List<ProjectDTO> getProjects() {
    final List<ProjectDTO> result = new ArrayList<>();
    Set<String> sharedProjects = new HashSet<>();
    try {
      sharedProjects = minervaNetController.getSharedProjects();
    } catch (final FailedDependencyException | IOException e) {
      logger.error("Problem with minerva net", e);
    }
    final String defaultMapId = getDefaultProjectId();
    for (final Project project : projectService.getAllProjects()) {
      result.add(new ProjectDTO(project, sharedProjects.contains(project.getProjectId()), Objects.equals(project.getProjectId(), defaultMapId)));
    }
    return result;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{projectId}/statistics")
  public Object getStatistics(final @PathVariable(value = "projectId") String projectId) throws QueryException, ObjectNotFoundException {
    final Map<String, Object> result = new TreeMap<>();

    result.put("elementAnnotations", elementService.getAnnotationStatistics(projectId));

    result.put("reactionAnnotations", reactionService.getAnnotationStatistics(projectId));

    result.put("publications", modelService.getPublicationCount(projectId));

    return result;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{projectId}:downloadSource")
  public ResponseEntity<byte[]> getProjectSource(final @PathVariable(value = "projectId") String projectId)
      throws QueryException, ObjectNotFoundException {

    final UploadedFileEntry file = projectService.getFileByProjectId(projectId);
    if (file == null) {
      throw new ObjectNotFoundException("File doesn't exist");
    }
    MediaType type = MediaType.TEXT_PLAIN;
    if (file.getOriginalFileName().endsWith("xml")) {
      type = MediaType.APPLICATION_XML;
    } else if (file.getOriginalFileName().endsWith("zip")) {
      type = MediaType.APPLICATION_OCTET_STREAM;
    }
    return ResponseEntity.ok().contentLength(file.getFileContent().length).contentType(type)
        .header("Content-Disposition", "attachment; filename=" + file.getOriginalFileName())
        .body(file.getFileContent());
  }

  static class LogPage {
    private List<LogEntry> data;
    private int totalSize;
    private int filteredSize;
    private Integer start;
    private int length;

    public int getLength() {
      return length;
    }

    public void setLength(final int length) {
      this.length = length;
    }

    public Integer getStart() {
      return start;
    }

    public void setStart(final Integer start) {
      this.start = start;
    }

    public int getFilteredSize() {
      return filteredSize;
    }

    public void setFilteredSize(final int filteredSize) {
      this.filteredSize = filteredSize;
    }

    public int getTotalSize() {
      return totalSize;
    }

    public void setTotalSize(final int totalSize) {
      this.totalSize = totalSize;
    }

    public List<LogEntry> getData() {
      return data;
    }

    public void setData(final List<LogEntry> data) {
      this.data = data;
    }

  }

  static class LogEntry implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Integer id;
    private String content;
    private String level;
    private String type;
    private String objectIdentifier;
    private String objectClass;
    private String mapName;
    private String source;

    public LogEntry(final ProjectLogEntry s) {
      this.id = s.getId();
      this.content = s.getContent();
      this.level = s.getSeverity();
      this.type = s.getType().toString();
      this.objectIdentifier = s.getObjectIdentifier();
      this.objectClass = s.getObjectClass();
      this.mapName = s.getMapName();
      this.source = s.getSource();
    }

    public Integer getId() {
      return id;
    }

    public void setId(final Integer id) {
      this.id = id;
    }

    public String getContent() {
      return content;
    }

    public void setContent(final String content) {
      this.content = content;
    }

    public String getLevel() {
      return level;
    }

    public void setLevel(final String level) {
      this.level = level;
    }

    public String getType() {
      return type;
    }

    public void setType(final String type) {
      this.type = type;
    }

    public String getObjectIdentifier() {
      return objectIdentifier;
    }

    public void setObjectIdentifier(final String objectIdentifier) {
      this.objectIdentifier = objectIdentifier;
    }

    public String getObjectClass() {
      return objectClass;
    }

    public void setObjectClass(final String objectClass) {
      this.objectClass = objectClass;
    }

    public String getMapName() {
      return mapName;
    }

    public void setMapName(final String mapName) {
      this.mapName = mapName;
    }

    public String getSource() {
      return source;
    }

    public void setSource(final String source) {
      this.source = source;
    }
  }

  public enum LogSortColumn {
    ID,
    LEVEL,
    TYPE,
    OBJECT_IDENTIFIER,
    OBJECT_CLASS,
    MAP_NAME,
    SOURCE,
    CONTENT;

    @JsonCreator
    public static LogSortColumn fromValue(final String value) {
      switch (value) {
        case ("id"):
          return LogSortColumn.ID;
        case ("level"):
          return LogSortColumn.LEVEL;
        case ("objectIdentifier"):
          return LogSortColumn.OBJECT_IDENTIFIER;
        case ("objectClass"):
          return LogSortColumn.OBJECT_CLASS;
        case ("mapName"):
          return LogSortColumn.MAP_NAME;
        case ("source"):
          return LogSortColumn.SOURCE;
        case ("content"):
          return LogSortColumn.CONTENT;
        default:
          return null;
      }
    }
  }

  public class LogSortColumnConverter extends PropertyEditorSupport {

    @Override
    public void setAsText(final String text) throws IllegalArgumentException {
      setValue(LogSortColumn.fromValue(text));
    }

  }

  @InitBinder
  public void initBinder(final WebDataBinder webdataBinder) {
    webdataBinder.registerCustomEditor(LogSortColumn.class, new LogSortColumnConverter());
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{projectId}/logs/")
  public LogPage getLogs(
      final @PathVariable(value = "projectId") String projectId,
      final @RequestParam(value = "level", defaultValue = "*") String level,
      final @RequestParam(value = "start", defaultValue = "0") Integer start,
      final @RequestParam(value = "length", defaultValue = "10") Integer length,
      final @RequestParam(value = "sortColumn", defaultValue = "id") LogSortColumn sortColumn,
      final @RequestParam(value = "sortOrder", defaultValue = "asc") String sortOrder,
      final @RequestParam(value = "search", defaultValue = "") String searchQuery) throws QueryException, ObjectNotFoundException {
    final Project project = getProject(projectId);

    final Comparator<LogEntry> comparator = getComparatorForColumn(sortColumn, sortOrder);

    final List<LogEntry> resultList = new ArrayList<>();

    final List<LogEntry> logEntries = getEntries(project, level);

    final List<LogEntry> filteredList = new ArrayList<>();

    final String search = searchQuery.toLowerCase();
    for (final LogEntry entry : logEntries) {
      if (isSearchResult(entry, search)) {
        filteredList.add(entry);
      }
    }
    if (comparator != null) {
      filteredList.sort(comparator);
    }

    int index = 0;
    for (final LogEntry entry : filteredList) {
      if (index >= start && index < start + length) {
        resultList.add(entry);
      }
      index++;
    }

    final LogPage result = new LogPage();
    result.setData(resultList);
    result.setTotalSize(logEntries.size());
    result.setFilteredSize(filteredList.size());
    result.setStart(start);
    result.setLength(resultList.size());
    return result;

  }

  private boolean isSearchResult(final LogEntry entry, final String search) {
    if (search == null || search.isEmpty()) {
      return true;
    }
    return entry.getContent().toLowerCase().contains(search) || entry.getId().toString().contains(search);

  }

  private List<LogEntry> getEntries(final Project project, final String level) {
    final List<LogEntry> result = new ArrayList<>();
    for (final ProjectLogEntry s : project.getLogEntries()) {
      if (level == null || level.equals("*") || level.equalsIgnoreCase(s.getSeverity())) {
        result.add(new LogEntry(s));
      }
    }
    return result;
  }

  private Comparator<LogEntry> getComparatorForColumn(final LogSortColumn sortColumnEnum, final String sortOrder)
      throws QueryException {
    final int orderFactor;
    if (sortOrder.equalsIgnoreCase("desc")) {
      orderFactor = -1;
    } else {
      orderFactor = 1;
    }
    final StringComparator stringComparator = new StringComparator();
    if (sortColumnEnum == null) {
      return null;
    } else if (sortColumnEnum.equals(LogSortColumn.ID)) {
      return (o1, o2) -> o1.getId().compareTo(o2.getId()) * orderFactor;
    } else if (sortColumnEnum.equals(LogSortColumn.CONTENT)) {
      return (o1, o2) -> o1.getContent().compareTo(o2.getContent()) * orderFactor;
    } else if (sortColumnEnum.equals(LogSortColumn.LEVEL)) {
      return (o1, o2) -> o1.getLevel().compareTo(o2.getLevel()) * orderFactor;
    } else if (sortColumnEnum.equals(LogSortColumn.MAP_NAME)) {
      return (o1, o2) -> stringComparator.compare(o1.getMapName(), o2.getMapName()) * orderFactor;
    } else if (sortColumnEnum.equals(LogSortColumn.OBJECT_CLASS)) {
      return (o1, o2) -> stringComparator.compare(o1.getObjectClass(), o2.getObjectClass()) * orderFactor;
    } else if (sortColumnEnum.equals(LogSortColumn.OBJECT_IDENTIFIER)) {
      return (o1, o2) -> stringComparator.compare(o1.getObjectIdentifier(), o2.getObjectIdentifier()) * orderFactor;
    } else if (sortColumnEnum.equals(LogSortColumn.SOURCE)) {
      return (o1, o2) -> stringComparator.compare(o1.getSource(), o2.getSource()) * orderFactor;
    } else if (sortColumnEnum.equals(LogSortColumn.TYPE)) {
      return (o1, o2) -> stringComparator.compare(o1.getType(), o2.getType()) * orderFactor;
    } else {
      throw new QueryException("Sort order not implemented for: " + sortColumnEnum);
    }
  }

  static class SubmapConnection {

    @JsonSerialize(using = MinifiedBioEntitySerializer.class)
    private BioEntity from;

    @JsonSerialize(using = ModelDataAsModelIdSerializer.class)
    private ModelData to;

    public SubmapConnection(final Element element) {
      from = element;
      to = element.getSubmodel().getSubmodel();
    }

    public BioEntity getFrom() {
      return from;
    }

    public void setFrom(final BioEntity from) {
      this.from = from;
    }

    public ModelData getTo() {
      return to;
    }

    public void setTo(final ModelData to) {
      this.to = to;
    }
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{projectId}/submapConnections")
  public List<SubmapConnection> getSubmapConnections(final @PathVariable(value = "projectId") String projectId)
      throws QueryException, ObjectNotFoundException {
    final Project project = getProject(projectId);
    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.PROJECT, Collections.singletonList(project));
    properties.put(ElementProperty.SUBMAP_CONNECTION, Collections.singletonList(Boolean.TRUE));

    final List<Element> elements = elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent();

    final List<SubmapConnection> result = new ArrayList<>();
    for (final Element element : elements) {
      if (element.getSubmodel() != null) {
        result.add(new SubmapConnection(element));
      }
    }
    return result;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId) ")
  @GetMapping(value = "/{projectId}/backgrounds/")
  public List<ProjectBackground> getBackgrounds(
      final @PathVariable(value = "projectId") String projectId)
      throws lcsb.mapviewer.services.ObjectNotFoundException {
    final List<ProjectBackground> result = projectBackgroundService.getProjectBackgroundsByProject(getProject(projectId));
    result.sort(ProjectBackground.ORDER_COMPARATOR);
    return result;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId) "
      + "or not @projectService.projectExists(#projectId)")
  @GetMapping(value = "/{projectId}/backgrounds/{backgroundId}")
  public ProjectBackground getBackgroundById(
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "backgroundId") Integer backgroundId) throws QueryException, ObjectNotFoundException {
    final ProjectBackground background = projectService.getBackgroundById(projectId, backgroundId, true);
    if (background == null) {
      throw new ObjectNotFoundException("Background does not exist");
    }
    return background;
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + "or hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{projectId}/backgrounds/{backgroundId}")
  public Object removeBackground(
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "backgroundId") Integer backgroundId) throws QueryException, IOException, ObjectNotFoundException {
    final Project project = getProject(projectId);
    final ProjectBackground projectBackground = getBackgroundById(projectId, backgroundId);
    if (projectBackground == null) {
      throw new ObjectNotFoundException("Background does not exist");
    }
    projectService.removeBackground(projectBackground);
    for (final ProjectBackgroundImageLayer layer : projectBackground.getProjectBackgroundImageLayer()) {
      final String path = minervaConfigurationHolder.getDataPath() + "/map_images/" + project.getDirectory() + "/" + layer.getDirectory();
      try {
        FileUtils.deleteDirectory(new File(path));
      } catch (final IOException e) {
        logger.error("Problem with removing background directory", e);
      }
    }
    return new TreeMap<>();
  }

  static class BackgroundDTO {
    private String description;
    private Integer id;
    private String name;
    private Integer order;
    private Boolean defaultOverlay;
    private String creator;

    public String getDescription() {
      return description;
    }

    public void setDescription(final String description) {
      this.description = description;
    }

    public Integer getId() {
      return id;
    }

    public void setId(final Integer id) {
      this.id = id;
    }

    public String getName() {
      return name;
    }

    public void setName(final String name) {
      this.name = name;
    }

    public Integer getOrder() {
      return order;
    }

    public void setOrder(final Integer order) {
      this.order = order;
    }

    public Boolean getDefaultOverlay() {
      return defaultOverlay;
    }

    public void setDefaultOverlay(final Boolean defaultOverlay) {
      this.defaultOverlay = defaultOverlay;
    }

    public String getCreator() {
      return creator;
    }

    public void setCreator(final String creator) {
      this.creator = creator;
    }
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId)")
  @PatchMapping(value = "/{projectId}/backgrounds/{backgroundId}")
  public ProjectBackground updateBackground(
      final @RequestBody BackgroundDTO data,
      final @PathVariable(value = "backgroundId") Integer backgroundId,
      final @PathVariable(value = "projectId") String projectId)
      throws QueryException, IOException, ObjectNotFoundException {
    final ProjectBackground background = getBackgroundById(projectId, backgroundId);
    if (data.getDescription() != null) {
      background.setDescription(data.getDescription());
    }
    if (data.getId() != null) {
      if (!Objects.equals(data.getId(), backgroundId)) {
        throw new QueryException("cannot change id");
      }
    }
    if (data.getName() != null) {
      if (data.getName().trim().isEmpty()) {
        throw new QueryException("name cannot be empty");
      }
      background.setName(data.getName());
    }
    if (data.getOrder() != null) {
      background.setOrderIndex(data.getOrder());
    }
    if (data.getDefaultOverlay() != null) {
      background.setDefaultOverlay(data.getDefaultOverlay());
    }
    if (data.getCreator() != null) {
      final User user = userService.getUserByLogin(data.getCreator());
      if (user == null) {
        throw new QueryException("overlay creator must be defined");
      }
      background.setCreator(user);
    }
    projectService.updateBackground(background);

    return getBackgroundById(projectId, backgroundId);
  }

  protected Converter getModelParser(final String handlerClass) throws QueryException {
    for (final Converter converter : modelConverters) {
      if (converter.getClass().getCanonicalName().equals(handlerClass)) {
        try {
          return (Converter) Class.forName(handlerClass).getConstructor().newInstance();
        } catch (final InstantiationException | IllegalAccessException | ClassNotFoundException
                       | IllegalArgumentException | InvocationTargetException
                       | NoSuchMethodException | SecurityException e) {
          throw new QueryException("Problem with handler:" + handlerClass);
        }
      }
    }
    throw new QueryException("Unknown handlerClass: " + handlerClass);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN') "
      + "or (hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId))")
  @PostMapping(value = "/{projectId:.+}:archive")
  public ProjectDTO archive(
      final @PathVariable(value = "projectId") String projectId) throws IOException, QueryException, ObjectNotFoundException {
    final Project project = getProject(projectId);
    if (project.getStatus() != ProjectStatus.DONE) {
      throw new UpdateConflictException("Project cannot be archived");
    }
    projectService.submitArchiveProjectJob(projectId);
    return getProject(projectId);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN') "
      + "or (hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId))")
  @PostMapping(value = "/{projectId:.+}:revive")
  public ProjectDTO revive(
      final @PathVariable(value = "projectId") String projectId) throws IOException, QueryException, ObjectNotFoundException {
    final Project project = getProject(projectId);
    if (project.getStatus() != ProjectStatus.ARCHIVED) {
      throw new UpdateConflictException("Project is not archived");
    }
    projectService.submitReviveProjectJob(projectId);
    return getProject(projectId);
  }

}