package lcsb.mapviewer.api.projects.overlays;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.PropertyFilter;

import lcsb.mapviewer.api.projects.overlays.OverlayController.OverlayEntryDTO;
import lcsb.mapviewer.model.overlay.DataOverlayType;
import lcsb.mapviewer.model.overlay.GeneVariantDataOverlayEntry;
import lcsb.mapviewer.modelutils.serializer.CustomExceptFilter;
import lcsb.mapviewer.modelutils.serializer.model.map.ElementIdentifierType;

public class OverlayEntryDTOSerializer extends JsonSerializer<OverlayEntryDTO> {

  @Override
  public void serialize(final OverlayEntryDTO entry, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {

    PropertyFilter filter = null;
    FilterProvider provider = serializers.getConfig().getFilterProvider();
    if (provider != null) {
      filter = provider.findPropertyFilter("overlayEntryFilter", null);
    }

    gen.writeStartObject();

    gen.writeObjectField("type", entry.getType());

    gen.writeObjectFieldStart("overlayContent");
    for (final String string : availableColumns()) {
      String column = string.toLowerCase();
      Object value = null;
      switch (column) {
        case "id":
        case "idobject":
          // casting to string is only to provide the same results as before
          // refactoring
          value = entry.getBioEntity().getId() + "";
          break;
        case "uniqueid":
          value = entry.getBioEntity().getId();
          break;
        case "modelid":
          value = entry.getModel().getId();
          break;
        case "value":
          value = entry.getEntry().getValue();
          break;
        case "color":
          value = entry.getEntry().getColor();
          break;
        case "description":
          value = entry.getEntry().getDescription();
          break;
        case "width":
          if (Objects.equals(entry.getType(), ElementIdentifierType.REACTION)) {
            value = entry.getEntry().getLineWidth();
          } else {
            continue;
          }
          break;
        case "type":
          if (entry.getEntry() instanceof GeneVariantDataOverlayEntry) {
            value = DataOverlayType.GENETIC_VARIANT;
          } else {
            value = DataOverlayType.GENERIC;
          }
          break;
        case "genevariations":
          if (entry.getEntry() instanceof GeneVariantDataOverlayEntry) {
            value = ((GeneVariantDataOverlayEntry) entry.getEntry()).getGeneVariants();
          } else {
            value = new Object[] {};
          }
          break;
        default:
          value = "Unknown column";
          break;
      }
      writeField(gen, string, value, filter);
    }
    gen.writeEndObject();

    gen.writeEndObject();
  }

  List<String> availableColumns() {
    return Arrays.asList(
        "modelId",
        "idObject",
        "value",
        "color",
        "description",
        "type",
        "geneVariations",
        "uniqueId",
        "width");
  }

  private void writeField(final JsonGenerator gen, final String field, final Object value, final PropertyFilter filter) throws IOException {
    if (filter == null
        || (filter instanceof CustomExceptFilter && ((CustomExceptFilter) filter).includeField(field))) {
      gen.writeObjectField(field, value);
    }
  }

}