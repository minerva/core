package lcsb.mapviewer.api.projects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lcsb.mapviewer.common.Md5;
import lcsb.mapviewer.services.interfaces.IProjectService;

@Service
public class DirectoryNameGenerator implements IDirectoryNameGenerator {

  private IProjectService projectService;

  @Autowired
  public DirectoryNameGenerator(final IProjectService projectService) {
    this.projectService = projectService;
  }

  @Override
  public String projectIdToDirectoryName(final String projectId) {
    long id = projectService.getNextId();
    return Md5.compute(projectId + "-" + id);
  }

}
