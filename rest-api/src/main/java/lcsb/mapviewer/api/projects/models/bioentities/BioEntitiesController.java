package lcsb.mapviewer.api.projects.models.bioentities;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.ISearchService;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/api/projects/{projectId}/models/{modelId}/"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class BioEntitiesController extends BaseController {

  private ISearchService searchService;

  @Autowired
  public BioEntitiesController(final ISearchService searchService) {
    this.searchService = searchService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "bioEntities:search")
  public List<BioEntity> searchBioEntities(
      final @PathVariable(value = "projectId") String projectId,
      final @Valid @Pattern(regexp = "^[-+]?\\d+$|^\\*$") @PathVariable(value = "modelId") String modelId,
      final @RequestParam(value = "coordinates", defaultValue = "") String coordinates,
      final @RequestParam(value = "query", required = false) String query,
      final @RequestParam(value = "count", required = false) Integer requestedCount,
      final @RequestParam(value = "type", defaultValue = "") String type,
      final @RequestParam(value = "perfectMatch", defaultValue = "false") boolean perfectMatch) throws QueryException, ObjectNotFoundException {
    if (!coordinates.trim().isEmpty()) {
      if (modelId.equals("*")) {
        throw new QueryException("modelId must be defined when searching by coordinates");
      }
      Integer count = requestedCount;
      if (count == null) {
        count = 5;
      }
      String[] tmp = coordinates.split(",");
      if (tmp.length != 2) {
        throw new QueryException("Coordinates must be in the format: 'xxx.xx,yyy.yy'");
      }
      Double x = null;
      Double y = null;
      try {
        x = Double.valueOf(tmp[0]);
        y = Double.valueOf(tmp[1]);
      } catch (final NumberFormatException e) {
        throw new QueryException("Coordinates must be in the format: 'xxx.xx,yyy.yy'", e);
      }
      Point2D pointCoordinates = new Point2D.Double(x, y);
      Set<String> types = new LinkedHashSet<>();
      if (!type.isEmpty()) {
        for (final String str : type.split(",")) {
          types.add(str.toLowerCase());
        }
      }
      int mapId = Integer.valueOf(modelId);

      return searchService.getClosestElements(projectId, mapId, pointCoordinates, count, perfectMatch, types);
    } else if (query != null) {
      Integer count = requestedCount;
      if (count == null) {
        count = 100;
      }
      return searchService.searchByQuery(projectId, modelId, query, count, perfectMatch);
    } else {
      return new ArrayList<>();
    }
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @RequestMapping(value = "bioEntities/suggestedQueryList", method = { RequestMethod.GET, RequestMethod.POST })
  public List<String> getSuggestedQueryList(final @PathVariable(value = "projectId") String projectId)
      throws QueryException, ObjectNotFoundException {
    return searchService.getSuggestedQueryList(projectId);
  }

}