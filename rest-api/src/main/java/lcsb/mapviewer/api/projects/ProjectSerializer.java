package lcsb.mapviewer.api.projects;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.model.map.OverviewImageLink;
import lcsb.mapviewer.model.map.OverviewLink;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

public class ProjectSerializer extends JsonSerializer<ProjectDTO> {

  private static transient Logger logger = LogManager.getLogger();

  @Override
  public void serialize(final ProjectDTO entry, final JsonGenerator gen,
                        final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeStringField("version", entry.getVersion());
    gen.writeObjectField("disease", entry.getDisease());
    gen.writeObjectField("organism", entry.getOrganism());
    gen.writeNumberField("idObject", entry.getId());

    if (entry.getStatus() != null) {
      gen.writeStringField("status", entry.getStatus().toString());
    }

    gen.writeStringField("directory", entry.getDirectory());
    gen.writeNumberField("progress", entry.getProgress());
    gen.writeStringField("notifyEmail", entry.getNotifyEmail());
    gen.writeBooleanField("logEntries", !entry.getLogEntries().isEmpty());

    gen.writeStringField("directory", entry.getDirectory());
    gen.writeStringField("name", entry.getName());
    gen.writeBooleanField("sharedInMinervaNet", entry.isSharedInMinervaNet());
    gen.writeBooleanField("defaultProject", entry.isDefaultProject());
    if (entry.getOwner() != null) {
      gen.writeStringField("owner", entry.getOwner().getLogin());
    } else {
      gen.writeStringField("owner", null);
    }
    gen.writeStringField("projectId", entry.getProjectId());
    gen.writeObjectField("creationDate", entry.getCreationDate());
    gen.writeObjectField("overviewImageViews", entry.getOverviewImages());

    Set<OverviewImage> set = new LinkedHashSet<>(entry.getOverviewImages());
    for (final OverviewImage image : entry.getOverviewImages()) {
      for (final OverviewLink ol : image.getLinks()) {
        if (ol instanceof OverviewImageLink) {
          set.remove(((OverviewImageLink) ol).getLinkedOverviewImage());
        }
      }
    }
    if (!set.isEmpty()) {
      gen.writeObjectField("topOverviewImage", set.iterator().next());
    } else if (!entry.getOverviewImages().isEmpty()) {
      logger.warn("Cannot determine top level image. Taking first one. {}", entry.getOverviewImages().get(0).getFilename());
      gen.writeObjectField("topOverviewImage", entry.getOverviewImages().get(0));
    } else {
      gen.writeObjectField("topOverviewImage", null);
    }

    gen.writeObjectField("license", entry.getLicense());
    gen.writeStringField("customLicenseName", entry.getCustomLicenseName());
    gen.writeStringField("customLicenseUrl", entry.getCustomLicenseUrl());

    gen.writeEndObject();
  }
}