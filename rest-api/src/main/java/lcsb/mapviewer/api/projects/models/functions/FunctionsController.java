package lcsb.mapviewer.api.projects.models.functions;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.ISbmlFunctionService;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/api/projects/{projectId}/models/{modelId}/functions"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class FunctionsController extends BaseController {

  private ISbmlFunctionService sbmlFunctionService;

  @Autowired
  public FunctionsController(final ISbmlFunctionService sbmlFunctionService) {
    this.sbmlFunctionService = sbmlFunctionService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{functionId}")
  public SbmlFunction getFunction(
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "modelId") int modelId,
      final @PathVariable(value = "functionId") int functionId) throws QueryException, ObjectNotFoundException {
    SbmlFunction result = sbmlFunctionService.getFunctionById(projectId, modelId, functionId);
    if (result == null) {
      throw new ObjectNotFoundException();
    }
    return result;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public List<SbmlFunction> getFunctions(
      final @PathVariable(value = "projectId") String projectId,
      final @Valid @Pattern(regexp = "^[-+]?\\d+$|^\\*$") @PathVariable(value = "modelId") String modelId)
      throws QueryException {
    return sbmlFunctionService.getFunctionsByProjectId(projectId, modelId);
  }

}