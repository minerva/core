package lcsb.mapviewer.api.projects.models.parameters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.persist.dao.map.kinetics.SbmlParameterProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.ISbmlParameterService;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/api/projects/{projectId}/models/{modelId}/parameters"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class ParametersController extends BaseController {

  private ISbmlParameterService parameterService;

  @Autowired
  public ParametersController(final ISbmlParameterService parameterService) {
    this.parameterService = parameterService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{parameterId}")
  public Map<String, Object> getParameter(
      final @PathVariable(value = "projectId") String projectId,
      final @Valid @Pattern(regexp = "^[-+]?\\d+$|^\\*$") @PathVariable(value = "modelId") String modelId,
      final @PathVariable(value = "parameterId") Integer id) throws QueryException, ObjectNotFoundException {
    for (final SbmlParameter parameter : getGlobalParametersFromProject(projectId, modelId)) {
      if (parameter.getId() == id) {
        return parameterToMap(parameter, true);
      }
    }
    for (final SbmlParameter parameter : getReactionParametersFromProject(projectId, modelId)) {
      if (parameter.getId() == id) {
        return parameterToMap(parameter, false);
      }
    }
    throw new ObjectNotFoundException("Parameter with given id doesn't exist");
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public List<Map<String, Object>> getParameters(
      final @PathVariable(value = "projectId") String projectId,
      final @Valid @Pattern(regexp = "^[-+]?\\d+$|^\\*$") @PathVariable(value = "modelId") String modelId) throws QueryException {
    List<Map<String, Object>> result = new ArrayList<>();
    for (final SbmlParameter parameter : getGlobalParametersFromProject(projectId, modelId)) {
      result.add(parameterToMap(parameter, true));
    }
    for (final SbmlParameter parameter : getReactionParametersFromProject(projectId, modelId)) {
      result.add(parameterToMap(parameter, false));
    }
    return result;
  }

  private List<SbmlParameter> getReactionParametersFromProject(final String projectId,
      @Valid @Pattern(regexp = "^[-+]?\\d+$|^\\*$") final String modelId)
      throws QueryException {
    Map<SbmlParameterProperty, Object> filter = new HashMap<>();
    filter.put(SbmlParameterProperty.PROJECT_ID, projectId);
    if (!Objects.equals(modelId, "*")) {
      filter.put(SbmlParameterProperty.MAP_ID, Integer.valueOf(modelId));
    }
    return parameterService.getAll(filter, Pageable.unpaged()).getContent();
  }

  private Collection<SbmlParameter> getGlobalParametersFromProject(final String projectId, final String modelId)
      throws QueryException {
    Map<SbmlParameterProperty, Object> filter = new HashMap<>();
    filter.put(SbmlParameterProperty.PROJECT_ID, projectId);
    if (!modelId.equals("*")) {
      filter.put(SbmlParameterProperty.MAP_ID, Integer.valueOf(modelId));
    }
    return parameterService.getAll(filter, Pageable.unpaged()).getContent();
  }

  private Map<String, Object> parameterToMap(final SbmlParameter parameter, final boolean global) {
    Map<String, Object> result = new TreeMap<>();
    result.put("id", parameter.getId());
    result.put("parameterId", parameter.getParameterId());
    result.put("name", parameter.getName());
    result.put("value", parameter.getValue());
    result.put("global", global);
    if (parameter.getUnits() != null) {
      result.put("unitsId", parameter.getUnits().getId());
    } else {
      result.put("unitsId", null);
    }
    return result;
  }

}