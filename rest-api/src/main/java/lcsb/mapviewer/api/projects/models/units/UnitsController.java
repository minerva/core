package lcsb.mapviewer.api.projects.models.units;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.persist.dao.map.kinetics.SbmlUnitProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.ISbmlUnitService;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/api/projects/{projectId}/models/{modelId}/units",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class UnitsController extends BaseController {

  private ISbmlUnitService unitService;

  @Autowired
  public UnitsController(final ISbmlUnitService unitService) {
    this.unitService = unitService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{unitId}")
  public SbmlUnit getUnit(
      final @PathVariable(value = "projectId") String projectId,
      final @Valid @Pattern(regexp = "^[-+]?\\d+$|^\\*$") @PathVariable(value = "modelId") String mapId,
      final @PathVariable(value = "unitId") Integer unitId) throws QueryException, ObjectNotFoundException {
    Map<SbmlUnitProperty, Object> filter = new HashMap<>();
    filter.put(SbmlUnitProperty.PROJECT_ID, projectId);
    if (!Objects.equals("*", mapId)) {
      filter.put(SbmlUnitProperty.MAP_ID, Integer.valueOf(mapId));
    }
    filter.put(SbmlUnitProperty.ID, unitId);
    Page<SbmlUnit> page = unitService.getAll(filter, Pageable.unpaged());
    if (page.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Unit does not exist");
    }
    return page.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public List<SbmlUnit> getUnits(
      final @PathVariable(value = "projectId") String projectId,
      final @Valid @Pattern(regexp = "^[-+]?\\d+$|^\\*$") @PathVariable(value = "modelId") String mapId)
      throws QueryException, ObjectNotFoundException {
    Map<SbmlUnitProperty, Object> filter = new HashMap<>();
    filter.put(SbmlUnitProperty.PROJECT_ID, projectId);
    if (!Objects.equals("*", mapId)) {
      filter.put(SbmlUnitProperty.MAP_ID, Integer.valueOf(mapId));
    }
    Page<SbmlUnit> page = unitService.getAll(filter, Pageable.unpaged());
    if (page.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Unit does not exist");
    }
    return page.getContent();
  }

}