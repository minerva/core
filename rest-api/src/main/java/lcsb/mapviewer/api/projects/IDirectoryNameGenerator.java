package lcsb.mapviewer.api.projects;

public interface IDirectoryNameGenerator {
  String projectIdToDirectoryName(final String projectId);

}
