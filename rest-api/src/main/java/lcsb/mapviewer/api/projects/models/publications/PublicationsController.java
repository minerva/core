package lcsb.mapviewer.api.projects.models.publications;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.CustomPage;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.Article;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.ArticleProperty;
import lcsb.mapviewer.persist.dao.map.ReactionProperty;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IPublicationService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/api/projects/{projectId}/models/{modelId}/publications"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class PublicationsController extends BaseController {

  private final IModelService modelService;

  private final IPublicationService publicationService;

  private final IElementService elementService;
  private final IReactionService reactionService;

  static class PublicationWrapper {
    private Article article;

    public PublicationWrapper(final Article article2) {
      this.article = article2;
    }

    public Article getArticle() {
      return article;
    }

    public void setArticle(final Article article) {
      this.article = article;
    }
  }

  public static class ArticleDTO {
    private List<BioEntity> elements;
    private PublicationWrapper publication;

    public List<BioEntity> getElements() {
      return elements;
    }

    public void setElements(final List<BioEntity> elements) {
      this.elements = elements;
    }

    public PublicationWrapper getPublication() {
      return publication;
    }

    public void setPublication(final PublicationWrapper publication) {
      this.publication = publication;
    }
  }

  @Autowired
  public PublicationsController(final IModelService modelService, final IPublicationService publicationService,
                                final IElementService elementService,
                                final IReactionService reactionService) {
    this.modelService = modelService;
    this.publicationService = publicationService;
    this.elementService = elementService;
    this.reactionService = reactionService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public Page<ArticleDTO> getPublications(
      final @PathVariable(value = "projectId") String projectId,
      final @Valid @Pattern(regexp = "^[-+]?\\d+$|^\\*$") @PathVariable(value = "modelId") String modelId,
      final @RequestParam(value = "page", defaultValue = "0") Integer page,
      final @RequestParam(value = "length", defaultValue = "10") Integer length,
      final @RequestParam(value = "sortColumn", defaultValue = "pubmedId") String sortColumn,
      final @RequestParam(value = "sortOrder", defaultValue = "asc") String sortOrder,
      final @RequestParam(value = "search", defaultValue = "") String search) throws QueryException, ObjectNotFoundException {

    if (length <= 0) {
      throw new QueryException("Page length must not be less than one");
    }
    final ArticleProperty sortProperty = extractSortColumn(sortColumn);
    final Pageable pageable;
    if (sortProperty != null) {
      pageable = PageRequest.of(page, length, Sort.by(Direction.fromString(sortOrder), sortProperty.name()));
    } else {
      pageable = PageRequest.of(page, length);

    }

    final List<ModelData> models = modelService.getModelsByMapId(projectId, modelId);
    if (models.size() == 0) {
      return new CustomPage<>(new ArrayList<>(), pageable, 0, 0);
    }

    final Map<ArticleProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(ArticleProperty.MODEL, models);
    final long count = publicationService.getCount(filterOptions);

    if (search != null && !search.trim().equals("")) {
      filterOptions.put(ArticleProperty.TEXT, search);
    }
    final Page<Article> articles = publicationService.getAll(filterOptions, pageable);

    final List<MiriamData> mds = new ArrayList<>();
    for (final Article article : articles) {
      mds.add(new MiriamData(MiriamType.PUBMED, article.getPubmedId()));
    }

    final Map<ElementProperty, Object> elementSearch = new HashMap<>();
    elementSearch.put(ElementProperty.ANNOTATION, mds);
    elementSearch.put(ElementProperty.MAP, models);

    final List<BioEntity> bioEntities = new ArrayList<>();
    bioEntities.addAll(elementService.getElementsByFilter(elementSearch, Pageable.unpaged(), true).getContent());

    final Map<ReactionProperty, Object> reactionSearch = new HashMap<>();
    reactionSearch.put(ReactionProperty.ANNOTATION, mds);
    reactionSearch.put(ReactionProperty.MAP, models);

    bioEntities.addAll(reactionService.getAll(reactionSearch, Pageable.unpaged(), true).getContent());

    final List<ArticleDTO> resultList = new ArrayList<>();

    final Map<MiriamData, List<BioEntity>> publications = new HashMap<>();

    for (final BioEntity bioEntity : bioEntities) {
      for (final MiriamData md : bioEntity.getMiriamData()) {
        if (md.getDataType().equals(MiriamType.PUBMED)) {
          final List<BioEntity> list = publications.computeIfAbsent(md, k -> new ArrayList<>());
          list.add(bioEntity);
        }
      }

    }

    for (final Article article : articles) {
      final List<BioEntity> elements = publications.computeIfAbsent(new MiriamData(MiriamType.PUBMED, article.getPubmedId()),
          k -> new ArrayList<>());
      final ArticleDTO entry = new ArticleDTO();
      entry.setElements(elements);
      entry.setPublication(new PublicationWrapper(article));
      resultList.add(entry);
    }

    return new CustomPage<>(resultList, pageable, count, articles.getTotalElements());
  }

  private ArticleProperty extractSortColumn(final String sortColumn) throws QueryException {
    if (sortColumn == null) {
      return null;
    }
    switch (sortColumn.toLowerCase().trim()) {
      case "":
        return null;
      case "pubmedid":
        return ArticleProperty.PUBMED_ID;
      case "year":
        return ArticleProperty.YEAR;
      case "journal":
        return ArticleProperty.JOURNAL;
      case "title":
        return ArticleProperty.TITLE;
      default:
        throw new QueryException("Unknown sort order: " + sortColumn);
    }
  }

}