package lcsb.mapviewer.api.projects.models.bioentities.elements;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.ModificationType;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.AbstractSiteModification;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;
import lcsb.mapviewer.model.map.species.field.RegulatoryRegion;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithStructuralState;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import lcsb.mapviewer.model.map.species.field.Structure;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;
import lcsb.mapviewer.model.map.species.field.UniprotRecord;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
 
@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/api/projects/{projectId}/models/{modelId}/bioEntities/elements",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class ElementsController extends BaseController {

  private final IElementService elementService;

  @Autowired
  public ElementsController(final IElementService elementService) {
    this.elementService = elementService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @RequestMapping(value = "/", method = {RequestMethod.GET, RequestMethod.POST})
  public List<Map<String, Object>> getElements(
      final @PathVariable(value = "projectId") String projectId,
      final @Valid @Pattern(regexp = "^[-+]?\\d+$|^\\*$") @PathVariable(value = "modelId") String modelId,
      final @RequestParam(value = "id", defaultValue = "") String id,
      final @RequestParam(value = "type", defaultValue = "") String type,
      final @RequestParam(value = "columns", defaultValue = "") String columns,
      final @RequestParam(value = "includedCompartmentIds", defaultValue = "") String includedCompartmentIds,
      final @RequestParam(value = "excludedCompartmentIds", defaultValue = "") String excludedCompartmentIds)
      throws QueryException, ObjectNotFoundException {
    final List<Integer> ids = stringToIntegerList(id);

    final Set<String> types = new LinkedHashSet<>();
    if (!type.isEmpty()) {
      for (final String str : type.split(",")) {
        types.add(str.toLowerCase());
      }
    }

    final List<Class<? extends BioEntity>> typeList = new ElementUtils().getClassesByStringTypes(types);
    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.PROJECT_ID, Collections.singletonList(projectId));
    if (!Objects.equals(modelId, "*")) {
      properties.put(ElementProperty.MAP_ID, Collections.singletonList(Integer.valueOf(modelId)));
    }
    if (ids.size() > 0) {
      properties.put(ElementProperty.ID, new ArrayList<>(ids));
    }
    if (typeList.size() > 0) {
      properties.put(ElementProperty.CLASS, typeList);
    }
    final List<Integer> includedCompartments = stringToIntegerList(includedCompartmentIds);
    if (includedCompartments.size() > 0) {
      properties.put(ElementProperty.INCLUDED_IN_COMPARTMENT, includedCompartments);
    }
    final List<Integer> excludedCompartments = stringToIntegerList(excludedCompartmentIds);
    if (excludedCompartments.size() > 0) {
      properties.put(ElementProperty.EXCLUDED_FROM_COMPARTMENT, excludedCompartments);
    }

    final List<Element> allElements = elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent();

    final Set<String> columnsSet = createElementColumnSet(columns);

    final List<Map<String, Object>> result = new ArrayList<>();
    for (final Element element : allElements) {
      result.add(preparedElement(element, columnsSet));

    }
    return result;
  }

  private List<Integer> stringToIntegerList(final String id) {
    final Set<Integer> ids = new LinkedHashSet<>();
    if (!id.equals("")) {
      for (final String str : id.split(",")) {
        ids.add(Integer.valueOf(str));
      }
    }
    return new ArrayList<>(ids);
  }

  private Map<String, Object> preparedElement(final Element element, final Set<String> columnsSet) {
    final Map<String, Object> result = new TreeMap<>();
    for (final String string : columnsSet) {
      final String column = string.toLowerCase();
      Object value = null;
      switch (column) {
        case "id":
        case "idobject":
          value = element.getId();
          break;
        case "modelid":
          value = element.getModelData().getId();
          break;
        case "elementid":
          value = element.getElementId();
          break;
        case "name":
          value = element.getName();
          break;
        case "type":
          value = element.getStringType();
          break;
        case "symbol":
          value = element.getSymbol();
          break;
        case "fullname":
          value = element.getFullName();
          break;
        case "abbreviation":
          value = element.getAbbreviation();
          break;
        case "compartmentid":
          if (element.getCompartment() != null) {
            value = element.getCompartment().getId();
          }
          break;
        case "complexid":
          if (element instanceof Species) {
            if (((Species) element).getComplex() != null) {
              value = ((Species) element).getComplex().getId();
            }
          }
          break;
        case "initialconcentration":
          if (element instanceof Species) {
            value = ((Species) element).getInitialConcentration();
          }
          break;
        case "initialamount":
          if (element instanceof Species) {
            value = ((Species) element).getInitialAmount();
          }
          break;
        case "boundarycondition":
          if (element instanceof Species) {
            value = ((Species) element).isBoundaryCondition();
          }
          break;
        case "immediatelink":
          value = element.getImmediateLink();
          break;
        case "constant":
          if (element instanceof Species) {
            value = ((Species) element).isConstant();
          }
          break;
        case "hypothetical":
          if (element instanceof Species) {
            value = ((Species) element).isHypothetical();
          }
          break;
        case "activity":
          if (element instanceof Species) {
            value = ((Species) element).getActivity();
          }
          break;
        case "references":
          value = element.getMiriamData();
          break;
        case "synonyms":
          value = element.getSynonyms();
          break;
        case "homomultimer":
          if (element instanceof Species) {
            value = ((Species) element).getHomodimer();
          } else {
            value = null;
          }
          break;
        case "formula":
          value = element.getFormula();
          break;
        case "notes":
          value = element.getNotes();
          break;
        case "other":
          value = getOthersForElement(element);
          break;
        case "formersymbols":
          value = element.getFormerSymbols();
          break;
        case "hierarchyvisibilitylevel":
          value = element.getVisibilityLevel();
          break;
        case "transparencylevel":
          value = element.getTransparencyLevel();
          break;
        case "linkedsubmodel":
          if (element.getSubmodel() != null) {
            value = element.getSubmodel().getSubmodel().getId();
          }
          break;
        case "bounds":
          value = createBounds(element.getX(), element.getY(), element.getZ(), element.getWidth(), element.getHeight());
          break;
        case "glyph":
          value = createGlyph(element.getGlyph());
          break;
        default:
          value = "Unknown column";
          break;
      }
      result.put(string, value);
    }
    return result;
  }

  private Map<String, Object> createGlyph(final Glyph glyph) {
    if (glyph == null) {
      return null;
    } else {
      final Map<String, Object> result = new HashMap<>();
      result.put("fileId", glyph.getFile().getId());
      return result;
    }
  }

  protected Map<String, Object> getOthersForElement(final Element element) {
    final Map<String, Object> result = new TreeMap<>();
    List<Map<String, Object>> modifications = new ArrayList<>();
    StructuralState structuralState = null;
    Map<String, Object> structures = new TreeMap<>();
    if (element instanceof Protein) {
      final Protein protein = ((Protein) element);
      modifications = getModifications(protein.getModificationResidues());
    } else if (element instanceof Rna) {
      final Rna rna = ((Rna) element);
      modifications = getModifications(rna.getModificationResidues());
    } else if (element instanceof AntisenseRna) {
      final AntisenseRna antisenseRna = ((AntisenseRna) element);
      modifications = getModifications(antisenseRna.getModificationResidues());
    } else if (element instanceof Gene) {
      final Gene gene = ((Gene) element);
      modifications = getModifications(gene.getModificationResidues());
    }
    if (element instanceof Species) {
      final Species species = (Species) element;
      structures = getStructures(species.getUniprots());
    }
    if (element instanceof SpeciesWithStructuralState) {
      final SpeciesWithStructuralState species = (SpeciesWithStructuralState) element;
      for (final ModificationResidue mr : species.getModificationResidues()) {
        if (mr instanceof StructuralState) {
          structuralState = (StructuralState) mr;
        }
      }
    }
    result.put("modifications", modifications);
    if (structuralState != null) {
      result.put("structuralState", structuralState.getName());
    } else {
      result.put("structuralState", null);
    }
    result.put("structures", structures);

    return result;
  }

  private List<Map<String, Object>> getModifications(final List<? extends ModificationResidue> elements) {
    final List<Map<String, Object>> result = new ArrayList<>();
    for (final ModificationResidue region : elements) {
      if (region instanceof StructuralState) {
        continue;
      }
      final Map<String, Object> row = new TreeMap<>();
      row.put("name", region.getName());
      row.put("modificationId", region.getIdModificationResidue());
      if (region instanceof AbstractSiteModification) {
        final AbstractSiteModification siteModification = ((AbstractSiteModification) region);
        if (siteModification.getState() != null) {
          row.put("state", siteModification.getState().name());
        }
      }
      final String type;
      if (region instanceof Residue) {
        type = ModificationType.RESIDUE.name();
      } else if (region instanceof BindingRegion) {
        type = ModificationType.BINDING_REGION.name();
      } else if (region instanceof CodingRegion) {
        type = ModificationType.CODING_REGION.name();
      } else if (region instanceof ProteinBindingDomain) {
        type = ModificationType.PROTEIN_BINDING_DOMAIN.name();
      } else if (region instanceof RegulatoryRegion) {
        type = ModificationType.REGULATORY_REGION.name();
      } else if (region instanceof TranscriptionSite) {
        if (((TranscriptionSite) region).getDirection().equals("LEFT")) {
          type = ModificationType.TRANSCRIPTION_SITE_LEFT.name();
        } else {
          type = ModificationType.TRANSCRIPTION_SITE_RIGHT.name();
        }
      } else if (region instanceof ModificationSite) {
        type = ModificationType.MODIFICATION_SITE.name();
      } else {
        throw new InvalidArgumentException("Unknown class: " + region.getClass());
      }
      row.put("type", type);
      result.add(row);
    }
    return result;
  }

  private Map<String, Object> getStructures(final Set<UniprotRecord> uniprots) {
    final Map<String, Object> result = new TreeMap<>();
    for (final UniprotRecord uniprotRec : uniprots) {
      final Set<Object> structs = new LinkedHashSet<>();
      for (final Structure struct : uniprotRec.getStructures()) {
        structs.add(struct.toMap());
      }
      result.put(uniprotRec.getUniprotId(), structs);
    }
    return result;
  }

  private Map<String, Object> createBounds(final Double x, final Double y, final Integer z, final Double width, final Double height) {
    final Map<String, Object> result = new TreeMap<>();
    result.put("x", x);
    result.put("y", y);
    result.put("z", z);
    result.put("width", width);
    result.put("height", height);
    return result;
  }

  public List<String> getAvailableElementColumns() {
    final List<String> result = new ArrayList<>();
    result.add("id");
    result.add("elementId");
    result.add("modelId");
    result.add("name");
    result.add("type");
    result.add("notes");
    result.add("symbol");
    result.add("complexId");
    result.add("compartmentId");
    result.add("fullName");
    result.add("abbreviation");
    result.add("formula");
    result.add("synonyms");
    result.add("formerSymbols");
    result.add("references");
    result.add("bounds");
    result.add("hierarchyVisibilityLevel");
    result.add("transparencyLevel");
    result.add("linkedSubmodel");
    result.add("other");
    result.add("initialConcentration");
    result.add("boundaryCondition");
    result.add("immediateLink");
    result.add("constant");
    result.add("hypothetical");
    result.add("activity");
    result.add("initialAmount");
    result.add("glyph");
    result.add("homomultimer");
    return result;
  }

  private Set<String> createElementColumnSet(final String columns) {
    final Set<String> columnsSet = new LinkedHashSet<>();
    if (columns.equals("")) {
      columnsSet.addAll(getAvailableElementColumns());
    } else {
      columnsSet.addAll(Arrays.asList(columns.split(",")));
    }
    return columnsSet;
  }

}