package lcsb.mapviewer.api.projects.chemicals;

import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.annotation.services.dapi.ChemicalSearchException;
import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.modelutils.serializer.CustomExceptFilter;
import lcsb.mapviewer.modelutils.serializer.model.map.ElementIdentifierType;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.search.DbSearchCriteria;
import lcsb.mapviewer.services.search.chemical.IChemicalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(
    value = {
        "/minerva/api/projects"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class ChemicalController extends BaseController {

  private final IProjectService projectService;
  private final IChemicalService chemicalService;
  private final IElementService elementService;

  @Autowired
  public ChemicalController(final IProjectService projectService, final IChemicalService chemicalService, final IElementService elementService) {
    this.projectService = projectService;
    this.chemicalService = chemicalService;
    this.elementService = elementService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{projectId}/chemicals:search")
  public MappingJacksonValue getChemicalsByQuery(
      final @PathVariable(value = "projectId") String projectId,
      final @RequestParam(value = "columns", defaultValue = "") String columns,
      final @RequestParam(value = "query", defaultValue = "") String query,
      final @RequestParam(value = "target", defaultValue = "") String target) throws QueryException, ObjectNotFoundException {
    final Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    if (project.getDisease() == null) {
      throw new QueryException("Project doesn't have disease associated to it");
    }

    List<Chemical> data = new ArrayList<>();
    if (!query.equals("")) {

      MiriamData organism = project.getOrganism();
      if (organism == null) {
        organism = TaxonomyBackend.HUMAN_TAXONOMY;
      }

      final Chemical chemical = chemicalService.getByName(query,
          new DbSearchCriteria().project(project).organisms(organism).colorSet(0).disease(project.getDisease()));
      if (chemical != null) {
        data.add(chemical);
      }
    } else if (target.contains(":")) {
      final String targetType = target.split(":", -1)[0];
      final String targetId = target.split(":", -1)[1];

      final Integer dbId = Integer.valueOf(targetId);
      final List<Element> targets = new ArrayList<>();
      if (targetType.equals(ElementIdentifierType.ALIAS.getJsName())) {
        final Map<ElementProperty, Object> properties = new HashMap<>();
        properties.put(ElementProperty.ID, Collections.singletonList(dbId));
        properties.put(ElementProperty.PROJECT_ID, Collections.singletonList(projectId));
        final Page<Element> elements = elementService.getAll(properties, Pageable.unpaged());
        if (elements.getNumberOfElements() == 0) {
          throw new ObjectNotFoundException("Element does not exist");
        }
        targets.addAll(elements.getContent());
      } else {
        throw new QueryException("Targeting for the type not implemented");
      }
      MiriamData organism = project.getOrganism();
      if (organism == null) {
        organism = TaxonomyBackend.HUMAN_TAXONOMY;
      }

      data = chemicalService.getForTargets(targets,
          new DbSearchCriteria().project(project).organisms(organism).disease(project.getDisease()));
    }

    final MappingJacksonValue result = new MappingJacksonValue(data);
    if (!columns.trim().isEmpty()) {
      final String[] columnList = columns.split(",");
      final SimpleFilterProvider provider = new SimpleFilterProvider();
      provider.addFilter("chemicalFilter",
          new CustomExceptFilter(columnList));
      result.setFilters(provider);
    }
    return result;

  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{projectId}/chemicals/suggestedQueryList")
  public List<String> getSuggestedQueryList(final @PathVariable(value = "projectId") String projectId)
      throws ChemicalSearchException, ObjectNotFoundException {
    final Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist: " + projectId);
    }
    return chemicalService.getSuggestedQueryList(project, project.getDisease());
  }
}