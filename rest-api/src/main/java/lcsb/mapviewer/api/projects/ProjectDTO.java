package lcsb.mapviewer.api.projects;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;

@JsonSerialize(using = ProjectSerializer.class)
public class ProjectDTO extends Project {

  private static final long serialVersionUID = 1L;

  @JsonProperty
  private boolean sharedInMinervaNet;

  @JsonProperty
  private boolean defaultProject;

  public ProjectDTO(final Project project, final boolean sharedInMinervaNet, final boolean defaultProject) {
    this(project);
    this.sharedInMinervaNet = sharedInMinervaNet;
    this.defaultProject = defaultProject;
  }

  private ProjectDTO(final Project project) {
    this.setId(project.getId());
    this.setProjectId(project.getProjectId());
    this.setDisease(project.getDisease());
    this.setName(project.getName());
    this.setVersion(project.getVersion());
    this.setOwner(project.getOwner());
    this.setNotifyEmail(project.getNotifyEmail());
    this.setDirectory(project.getDirectory());
    this.setStatus(project.getStatus());
    this.setProgress(project.getProgress());
    this.setLogEntries(project.getLogEntries());
    this.setCreationDate(project.getCreationDate());
    this.setModels(project.getModels());
    this.setProjectBackgrounds(project.getProjectBackgrounds());
    this.setDataOverlays(project.getDataOverlays());
    this.setInputData(project.getInputData());
    this.setOrganism(project.getOrganism());
    this.setSbgnFormat(project.isSbgnFormat());
    this.setOverviewImages(project.getOverviewImages());
    this.setGlyphs(project.getGlyphs());
    this.setLicense(project.getLicense());
    this.setCustomLicenseName(project.getCustomLicenseName());
    this.setCustomLicenseUrl(project.getCustomLicenseUrl());
  }

  public boolean isSharedInMinervaNet() {
    return sharedInMinervaNet;
  }

  public void setSharedInMinervaNet(final boolean sharedInMinervaNet) {
    this.sharedInMinervaNet = sharedInMinervaNet;
  }

  @Override
  public Model getTopModel() {
    throw new NotImplementedException();
  }

  @Override
  public ModelData getTopModelData() {
    throw new NotImplementedException();
  }

  public boolean isDefaultProject() {
    return defaultProject;
  }

  public void setDefaultProject(final boolean defaultProject) {
    this.defaultProject = defaultProject;
  }

}
