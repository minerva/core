package lcsb.mapviewer.api.projects.comments;

import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.modelutils.serializer.CustomExceptFilter;
import lcsb.mapviewer.modelutils.serializer.model.map.CommentSerializer;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.ICommentService;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/api/projects/{projectId}/comments"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class CommentController extends BaseController {

  private final IUserService userService;
  private final IProjectService projectService;
  private final ICommentService commentService;
  private final IElementService elementService;
  private final IReactionService reactionService;

  @Autowired
  public CommentController(final IUserService userService, final IProjectService projectService, final ICommentService commentService,
                           final IElementService elementService, final IReactionService reactionService) {
    this.userService = userService;
    this.projectService = projectService;
    this.elementService = elementService;
    this.reactionService = reactionService;
    this.commentService = commentService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/models/{modelId}/")
  public MappingJacksonValue getComments(
      final Authentication authentication,
      final @PathVariable(value = "projectId") String projectId,
      final @RequestParam(value = "columns", defaultValue = "") String columns,
      final @Valid @Pattern(regexp = "^[-+]?\\d+$|^\\*$") @PathVariable(value = "modelId") String modelId,
      final @RequestParam(value = "removed", required = false) Boolean removed) throws QueryException, ObjectNotFoundException {
    final boolean isAdmin = authentication.getAuthorities()
        .contains(new SimpleGrantedAuthority(PrivilegeType.IS_ADMIN.name()));
    final boolean isProjectCurator = authentication.getAuthorities()
        .contains(new SimpleGrantedAuthority(PrivilegeType.IS_CURATOR.name()))
        && authentication.getAuthorities()
        .contains(new SimpleGrantedAuthority(PrivilegeType.READ_PROJECT.name() + ":" + projectId));

    final List<Comment> comments = getComments(authentication, projectId, modelId, removed);

    final List<String> columnList = getColumns(columns);

    if (!isAdmin && !isProjectCurator) {
      columnList.removeIf(string -> {
        return string.equalsIgnoreCase("author");
      });
      columnList.removeIf(string -> {
        return string.equalsIgnoreCase("email");
      });
      columnList.removeIf(string -> {
        return string.equalsIgnoreCase("removeReason");
      });
    }
    final MappingJacksonValue result = new MappingJacksonValue(comments);
    final SimpleFilterProvider provider = new SimpleFilterProvider();
    provider.addFilter("commentFilter", new CustomExceptFilter(columnList));
    result.setFilters(provider);
    return result;
  }

  private List<Comment> getComments(final Authentication authentication, final String projectId, final String modelId, final Boolean removed)
      throws ObjectNotFoundException, QueryException {
    final Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    final List<Comment> comments = commentService.getCommentsByModel(project, modelId);

    if (removed != null) {
      comments.removeIf(comment -> {
        return comment.isDeleted() != removed;
      });
    }

    comments.removeIf(comment -> {
      if (authentication.getAuthorities().contains(new SimpleGrantedAuthority(PrivilegeType.IS_ADMIN.toString()))) {
        return false;
      }
      if (authentication.getAuthorities().contains(new SimpleGrantedAuthority(PrivilegeType.IS_CURATOR.toString()))
          && authentication.getAuthorities().contains(new SimpleGrantedAuthority(PrivilegeType.READ_PROJECT + ":" + projectId))) {
        return false;
      }

      if (comment.getUser() != null && comment.getUser().getLogin().equals(authentication.getName())) {
        return false;
      }
      return !comment.isPinned();
    });
    return comments;
  }

  private List<String> getColumns(final String columns) {
    List<String> columnList = CommentSerializer.availableColumns();
    if (!columns.trim().isEmpty()) {
      columnList = Arrays.asList(columns.split(","));
    }
    columnList = new ArrayList<>(columnList);
    return columnList;
  }

  static class RemoveCommentDTO {
    private String reason;

    public String getReason() {
      return reason;
    }

    public void setReason(final String reason) {
      this.reason = reason;
    }
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId)"
      + " or @commentService.getOwnerByCommentId(#projectId, #commentId)?.login == authentication.name")
  @DeleteMapping(value = "/{commentId}/")
  public Object removeComment(
      final @RequestBody(required = false) RemoveCommentDTO body,
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "commentId") Integer commentId) throws QueryException, IOException, ObjectNotFoundException {
    String reason = null;
    if (body != null) {
      reason = body.getReason();
    }
    final Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    final Comment comment = commentService.getCommentById(projectId, commentId);

    if (comment == null) {
      throw new ObjectNotFoundException("Comment with given id doesn't exist");
    }
    commentService.deleteComment(comment, reason);
    return new TreeMap<>();
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/models/{modelId}/bioEntities/reactions/{reactionId}")
  public MappingJacksonValue getCommentsByReaction(
      final Authentication authentication,
      final @PathVariable(value = "projectId") String projectId,
      final @RequestParam(value = "columns", defaultValue = "") String columns,
      final @Valid @Pattern(regexp = "^[-+]?\\d+$|^\\*$") @PathVariable(value = "modelId") String modelId,
      final @PathVariable(value = "reactionId") Integer reactionId,
      final @RequestParam(value = "removed", required = false) Boolean removed) throws QueryException, ObjectNotFoundException {

    final List<Comment> comments = getComments(authentication, projectId, modelId, removed);
    comments.removeIf(comment -> {
      return comment.getReaction() == null || comment.getReaction().getId() != reactionId;

    });
    final List<String> columnList = getColumns(columns);

    final MappingJacksonValue result = new MappingJacksonValue(comments);
    final SimpleFilterProvider provider = new SimpleFilterProvider();
    provider.addFilter("commentFilter", new CustomExceptFilter(columnList));
    result.setFilters(provider);
    return result;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/models/{modelId}/bioEntities/elements/{elementId}")
  public MappingJacksonValue getCommentsByElement(
      final Authentication authentication,
      final @PathVariable(value = "projectId") String projectId,
      final @RequestParam(value = "columns", defaultValue = "") String columns,
      final @PathVariable(value = "elementId") Integer elementId,
      final @Valid @Pattern(regexp = "^[-+]?\\d+$|^\\*$") @PathVariable(value = "modelId") String modelId,
      final @RequestParam(value = "removed", required = false) Boolean removed) throws QueryException, ObjectNotFoundException {
    final List<Comment> comments = getComments(authentication, projectId, modelId, removed);
    comments.removeIf(comment -> {
      return comment.getElement() == null || comment.getElement().getId() != elementId;

    });
    final List<String> columnList = getColumns(columns);

    final MappingJacksonValue result = new MappingJacksonValue(comments);
    final SimpleFilterProvider provider = new SimpleFilterProvider();
    provider.addFilter("commentFilter", new CustomExceptFilter(columnList));
    result.setFilters(provider);
    return result;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/models/{modelId}/points/{coordinates:.+}")
  public MappingJacksonValue getCommentsByPoint(
      final Authentication authentication,
      final @PathVariable(value = "projectId") String projectId,
      final @RequestParam(value = "columns", defaultValue = "") String columns,
      final @PathVariable(value = "coordinates") String coordinates,
      final @Valid @Pattern(regexp = "^[-+]?\\d+$|^\\*$") @PathVariable(value = "modelId") String modelId,
      final @RequestParam(value = "removed", required = false) Boolean removed) throws QueryException, ObjectNotFoundException {
    final List<Comment> comments = getComments(authentication, projectId, modelId, removed);
    comments.removeIf(comment -> {
      return comment.getReaction() != null || comment.getElement() != null;

    });
    comments.removeIf(comment -> {
      return (!Objects.equals(coordinates, String.format("%.2f,%.2f", comment.getCoordinates().getX(), comment.getCoordinates().getY())));
    });
    final List<String> columnList = getColumns(columns);

    final MappingJacksonValue result = new MappingJacksonValue(comments);
    final SimpleFilterProvider provider = new SimpleFilterProvider();
    provider.addFilter("commentFilter", new CustomExceptFilter(columnList));
    result.setFilters(provider);
    return result;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @PostMapping(value = "/models/{modelId}/bioEntities/elements/{elementId}")
  public Comment addCommentForElement(
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "elementId") Integer elementId,
      final @NotNull @Size(max = 255) @RequestParam(value = "email") String email,
      final @RequestParam(value = "content") String content,
      final @RequestParam(value = "pinned", defaultValue = "true") Boolean pinned,
      final @RequestParam(value = "coordinates") String coordinates,
      final @PathVariable(value = "modelId") Integer modelId, final Authentication authentication) throws QueryException, ObjectNotFoundException {
    final Point2D pointCoordinates = parseCoordinates(coordinates);

    User user = userService.getUserByLogin(authentication.getName());
    if (user.getLogin().equals(Configuration.ANONYMOUS_LOGIN)) {
      user = null;
    }
    final Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }


    final Comment comment = commentService.addComment(email, content, pointCoordinates,
        getElementById(projectId, modelId, elementId), pinned,
        modelId, user);

    return comment;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @PostMapping(value = "/models/{modelId}/bioEntities/reactions/{reactionId}")
  public Comment addCommentForReaction(
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "reactionId") Integer reactionId,
      final @NotNull @Size(max = 255) @RequestParam(value = "email") String email,
      final @RequestParam(value = "content") String content,
      final @RequestParam(value = "pinned", defaultValue = "true") Boolean pinned,
      final @RequestParam(value = "coordinates") String coordinates,
      final @PathVariable(value = "modelId") Integer modelId, final Authentication authentication) throws QueryException, ObjectNotFoundException {
    final Point2D pointCoordinates = parseCoordinates(coordinates);
    User user = userService.getUserByLogin(authentication.getName());
    if (user.getLogin().equals(Configuration.ANONYMOUS_LOGIN)) {
      user = null;
    }
    final Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }

    final Comment comment = commentService.addComment(email, content, pointCoordinates,
        reactionService.getReactionById(projectId, modelId, reactionId), pinned,
        modelId, user);

    return comment;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @PostMapping(value = "/models/{modelId}/points/{coordinates}")
  public Comment addCommentForPoint(
      final @PathVariable(value = "projectId") String projectId,
      final @NotNull @Size(max = 255) @RequestParam(value = "email") String email,
      final @RequestParam(value = "content") String content,
      final @RequestParam(value = "pinned", defaultValue = "true") Boolean pinned,
      final @PathVariable(value = "coordinates") String coordinates,
      final @PathVariable(value = "modelId") Integer modelId, final Authentication authentication) throws QueryException, ObjectNotFoundException {
    final Point2D pointCoordinates = parseCoordinates(coordinates);
    User user = userService.getUserByLogin(authentication.getName());
    if (user.getLogin().equals(Configuration.ANONYMOUS_LOGIN)) {
      user = null;
    }
    final Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }

    final Comment comment = commentService.addComment(email, content, pointCoordinates, null, pinned,
        modelId, user);

    return comment;
  }

  private Point2D parseCoordinates(final String coordinates) throws QueryException {
    final String[] tmp = coordinates.split(",");
    if (tmp.length != 2) {
      throw new QueryException("Coordinates must be in the format: 'xxx.xx,yyy.yy'");
    }
    Double x = null;
    Double y = null;
    try {
      x = Double.valueOf(tmp[0]);
      y = Double.valueOf(tmp[1]);
    } catch (final NumberFormatException e) {
      throw new QueryException("Coordinates must be in the format: 'xxx.xx,yyy.yy'", e);
    }
    final Point2D pointCoordinates = new Point2D.Double(x, y);
    return pointCoordinates;
  }

  public Element getElementById(final String projectId, final int mapId, final int elementId) throws ObjectNotFoundException {
    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.ID, Collections.singletonList(elementId));
    properties.put(ElementProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(ElementProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<Element> elements = elementService.getAll(properties, Pageable.unpaged());
    if (elements.getNumberOfElements() > 0) {
      return elements.getContent().get(0);
    }
    throw new ObjectNotFoundException("Element does not exist");
  }
}