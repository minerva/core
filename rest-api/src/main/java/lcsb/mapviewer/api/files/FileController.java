package lcsb.mapviewer.api.files;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IFileService;
import lcsb.mapviewer.services.interfaces.IUserService;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/api/files"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class FileController extends BaseController {

  private IUserService userService;
  private IFileService fileService;

  @Autowired
  public FileController(final IUserService userService, final IFileService fileService) {
    this.userService = userService;
    this.fileService = fileService;
  }

  @PreAuthorize("isAuthenticated() and authentication.name != '" + Configuration.ANONYMOUS_LOGIN + "'")
  @PostMapping(value = "/")
  public UploadedFileEntry createFile(
      final Authentication authentication,
      final @RequestParam(value = "filename") String filename,
      final @Min(1) @RequestParam(value = "length") Long length) {
    User user = userService.getUserByLogin(authentication.getName());
    UploadedFileEntry entry = new UploadedFileEntry();
    entry.setOriginalFileName(filename);
    entry.setFileContent(new byte[] {});
    entry.setLength(length);
    entry.setOwner(user);
    fileService.add(entry);
    try {
      return getFile(entry.getId());
    } catch (final ObjectNotFoundException e) {
      throw new InvalidStateException(e);
    }
  }

  @PostAuthorize("hasAuthority('IS_ADMIN') or returnObject.owner == authentication.name")
  @GetMapping(value = "/{id}")
  public UploadedFileEntry getFile(final @Valid @PathVariable(value = "id") Integer id) throws ObjectNotFoundException {
    UploadedFileEntry fileEntry = fileService.getById(id);
    if (fileEntry == null) {
      throw new ObjectNotFoundException("File not found");
    }
    return fileEntry;
  }

  @PreAuthorize("@fileService.getOwnerByFileId(#id)?.login == authentication.name")
  @PostMapping(value = "/{id}:uploadContent", consumes = { MediaType.APPLICATION_OCTET_STREAM_VALUE })
  public UploadedFileEntry uploadContent(final @PathVariable(value = "id") Integer id, final @RequestBody byte[] data)
      throws QueryException, ObjectNotFoundException {
    UploadedFileEntry fileEntry = fileService.getById(id);
    if (fileEntry == null) {
      throw new ObjectNotFoundException("File not found");
    }
    long missingByteLength = fileEntry.getLength() - fileEntry.getFileContent().length;
    if (data.length > missingByteLength) {
      throw new QueryException(
          "Too many bytes sent. There are " + missingByteLength + " missing bytes, but " + data.length + " sent.");
    }
    byte[] newConent = ArrayUtils.addAll(fileEntry.getFileContent(), data);
    fileEntry.setFileContent(newConent);
    fileService.update(fileEntry);
    return fileEntry;
  }

}