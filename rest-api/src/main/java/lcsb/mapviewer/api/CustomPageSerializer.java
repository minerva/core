package lcsb.mapviewer.api;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class CustomPageSerializer<T> extends JsonSerializer<CustomPage<T>> {

  @Override
  public void serialize(final CustomPage<T> page, final JsonGenerator jsonGenerator, final SerializerProvider serializers) throws IOException {

    jsonGenerator.writeStartObject();
    jsonGenerator.writeObjectField("data", page.getContent());
    jsonGenerator.writeNumberField("totalSize", page.getTotalPages());
    jsonGenerator.writeNumberField("filteredSize", page.getFiltered());
    jsonGenerator.writeNumberField("length", page.getSize());
    jsonGenerator.writeNumberField("page", page.getNumber());
    jsonGenerator.writeEndObject();
  }

}
