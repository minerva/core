package lcsb.mapviewer.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

public class CustomPage<T> extends PageImpl<T> {

  @JsonProperty("filteredSize")
  private long filtered;

  public CustomPage(final List<T> content) {
    super(content);
  }

  public CustomPage(final List<T> content, final Pageable pageable, final long total) {
    super(content, pageable, total);
    this.filtered = total;
  }

  public CustomPage(final List<T> content, final Pageable pageable, final long total, final long filtered) {
    super(content, pageable, total);
    this.filtered = filtered;
  }

  public CustomPage(final Page<T> page) {
    super(page.getContent(), page.getPageable(), page.getTotalElements());
  }

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  public long getFiltered() {
    return filtered;
  }

}
