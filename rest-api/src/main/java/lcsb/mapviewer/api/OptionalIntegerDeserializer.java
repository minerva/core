package lcsb.mapviewer.api;

import java.io.IOException;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class OptionalIntegerDeserializer extends StdDeserializer<Optional<Integer>> {

  protected OptionalIntegerDeserializer() {
    super(Optional.class);
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Override
  public Optional<Integer> deserialize(final JsonParser parser, final DeserializationContext ctxt) throws IOException, JsonProcessingException {
    String source = parser.getValueAsString();
    if (source == null) {
      return Optional.empty();
    } else {
      return Optional.of(Integer.valueOf(source));
    }
  }
}
