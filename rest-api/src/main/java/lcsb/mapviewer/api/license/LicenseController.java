package lcsb.mapviewer.api.license;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.CustomPage;
import lcsb.mapviewer.model.License;
import lcsb.mapviewer.services.interfaces.ILicenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/api/licenses"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class LicenseController extends BaseController {

  private final ILicenseService licenseService;

  @Autowired
  public LicenseController(final ILicenseService licenseService) {
    this.licenseService = licenseService;
  }

  @GetMapping(value = "/")
  public Page<License> getFile(final Pageable pageable) {
    return new CustomPage(licenseService.getByFilter(pageable));

  }

}