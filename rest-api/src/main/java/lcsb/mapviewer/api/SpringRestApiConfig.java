package lcsb.mapviewer.api;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import lcsb.mapviewer.services.SpringServiceConfig;

@Configuration
@EnableWebMvc
@Import({ SpringServiceConfig.class })
@ComponentScan(basePackages = { "lcsb.mapviewer" })
public class SpringRestApiConfig {

}
