package lcsb.mapviewer.api;

/**
 * Thrown when object cannot be updated over API due to conflicting queries.
 * 
 * @author Piotr Gawron
 *
 */
public class UpdateConflictException extends QueryException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   * 
   * @param message
   *          error message
   */
  public UpdateConflictException(final String message) {
    super(message);
  }

  /**
   * Constructor with error message and parent exception.
   * 
   * @param message
   *          error message
   * @param reason
   *          parent exception that caused this one
   */
  public UpdateConflictException(final String message, final Exception reason) {
    super(message, reason);
  }

}
