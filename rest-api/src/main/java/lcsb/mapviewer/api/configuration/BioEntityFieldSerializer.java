package lcsb.mapviewer.api.configuration;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.user.annotator.BioEntityField;

public class BioEntityFieldSerializer extends JsonSerializer<BioEntityField> {

  @Override
  public void serialize(final BioEntityField type, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeStringField("commonName", type.getCommonName());
    gen.writeStringField("name", type.name());

    gen.writeEndObject();
  }
}