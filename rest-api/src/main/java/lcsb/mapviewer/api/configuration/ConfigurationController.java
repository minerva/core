package lcsb.mapviewer.api.configuration;

import lcsb.mapviewer.annotation.services.ModelAnnotator;
import lcsb.mapviewer.annotation.services.dapi.DapiConnectionException;
import lcsb.mapviewer.annotation.services.dapi.DapiConnector;
import lcsb.mapviewer.annotation.services.dapi.dto.DapiDatabase;
import lcsb.mapviewer.annotation.services.dapi.dto.ReleaseDto;
import lcsb.mapviewer.annotation.services.dapi.dto.UserDto;
import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator;
import lcsb.mapviewer.converter.graphics.ImageGenerators;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.overlay.DataOverlayType;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.ConfigurationOption;
import lcsb.mapviewer.model.user.annotator.BioEntityField;
import lcsb.mapviewer.modelutils.map.ClassTreeNode;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.services.ObjectExistsException;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;
import java.util.stream.Collectors;

@RestController
@RequestMapping(
    value = {
        "/minerva/api/configuration"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class ConfigurationController extends BaseController {

  private DapiConnector dapiConnector;
  private IConfigurationService configurationService;
  private ModelAnnotator modelAnnotator;
  private List<Converter> modelConverters;

  @Autowired
  public ConfigurationController(final DapiConnector dapiConnector,
                                 final IConfigurationService configurationService,
                                 final List<Converter> modelConverters,
                                 final ModelAnnotator modelAnnotator) {
    this.dapiConnector = dapiConnector;
    this.configurationService = configurationService;
    this.modelConverters = modelConverters;
    this.modelAnnotator = modelAnnotator;
  }

  @GetMapping(value = "/")
  public ConfigurationDto getConfiguration(final Authentication authentication) {

    ConfigurationDto result = new ConfigurationDto();
    result.setOptions(getOptions(authentication));
    result.setImageFormats(getImageFormats());
    result.setModelFormats(modelConverters);
    result.setOverlayTypes(DataOverlayType.values());
    result.setElementTypes(getElementTypes());
    result.setReactionTypes(getReactionTypes());
    result.setMiriamTypes(getMiriamTypes());
    result.setMapTypes(SubmodelType.values());
    result.setUnitTypes(SbmlUnitType.values());
    result.setModificationStateTypes(getModificationStateTypes());
    result.setPrivilegeTypes(getPrivilegeTypes());
    result.setVersion(configurationService.getSystemSvnVersion());
    result.setBuildDate(configurationService.getSystemBuild());
    result.setGitHash(configurationService.getSystemGitVersion());
    result.setAnnotators(modelAnnotator.getAvailableAnnotators());
    result.setBioEntityFields(BioEntityField.values());
    return result;
  }

  @GetMapping(value = "/options/")
  public List<ConfigurationOption> getOptions(final Authentication authentication) {
    boolean isAdmin = authentication.getAuthorities()
        .contains(new SimpleGrantedAuthority(PrivilegeType.IS_ADMIN.toString()));
    return configurationService.getAllValues().stream()
        .filter(option -> !option.getType().isServerSide() || isAdmin)
        .collect(Collectors.toList());
  }

  static class DapiStatus {
    private boolean validConnection;

    public DapiStatus(final boolean validConnection) {
      this.validConnection = validConnection;
    }

    public boolean isValidConnection() {
      return validConnection;
    }

    public void setValidConnection(final boolean validConnection) {
      this.validConnection = validConnection;
    }
  }

  @GetMapping(value = "/dapi/")
  public DapiStatus getDapi() {
    return new DapiStatus(dapiConnector.isValidConnection());
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @PostMapping(value = "/dapi:registerUser")
  public void registerDapiUser(final @Valid @RequestBody UserDto user) throws DapiConnectionException, QueryException, ObjectExistsException {
    try {
      dapiConnector.registerUser(user);
    } catch (final DapiConnectionException e) {
      if (e.getStatusCode() == HttpStatus.SC_CONFLICT) {
        throw new ObjectExistsException("User with given login/email exists", e);
      } else if (e.getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
        throw new QueryException("Invalid input data", e);
      }
    }
    configurationService.setConfigurationValue(ConfigurationElementType.DAPI_LOGIN, user.getLogin());
    configurationService.setConfigurationValue(ConfigurationElementType.DAPI_PASSWORD, user.getPassword());
  }

  @GetMapping(value = "/dapi/database/")
  public List<DapiDatabase> getDapiDatabases() {
    try {
      return dapiConnector.getDatabases();
    } catch (final DapiConnectionException e) {
      logger.error("Problem with dapi", e);
    }
    return new ArrayList<>();
  }

  @GetMapping(value = "/dapi/database/{database}/release/")
  public List<Map<String, Object>> getDapiReleases(final @PathVariable(value = "database") String database) {
    List<Map<String, Object>> result = new ArrayList<>();
    try {
      for (final ReleaseDto release : dapiConnector.getReleases(database)) {
        Map<String, Object> entry = new HashMap<>();
        entry.put("name", release.getName());
        entry.put("licenseUrl", release.getLicense().getUrl());
        entry.put("licenseContent", release.getLicense().getContent());
        entry.put("accepted", dapiConnector.isReleaseAccepted(database, release));
        result.add(entry);
      }
    } catch (final DapiConnectionException e) {
      logger.error("Problem with dapi", e);
    }
    return result;
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @PostMapping(value = "/dapi/database/{database}/release/{release:.+}:acceptLicense")
  public void acceptReleasesLicense(
      final @PathVariable(value = "database") String database,
      final @PathVariable(value = "release") String release) throws DapiConnectionException {
    dapiConnector.acceptRelease(database, release);
  }

  static class OptionUpdateData {
    @NotNull
    private ConfigurationOption option;

    public ConfigurationOption getOption() {
      return option;
    }

    public void setOption(final ConfigurationOption option) {
      this.option = option;
    }
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @PatchMapping(value = "/options/{option}")
  public ConfigurationOption updateOption(
      final @Valid @RequestBody OptionUpdateData data,
      final @PathVariable(value = "option") ConfigurationElementType type) throws QueryException {
    String value = data.getOption().getValue();
    try {
      configurationService.setConfigurationValue(type, value);
    } catch (final InvalidArgumentException e) {
      throw new QueryException(e.getMessage(), e);
    }
    return configurationService.getValue(type);
  }

  private List<Class<? extends AbstractImageGenerator>> getImageFormats() {
    List<Class<? extends AbstractImageGenerator>> result = new ArrayList<>();
    ImageGenerators imageGenerators = new ImageGenerators();

    for (Pair<String, Class<? extends AbstractImageGenerator>> element : imageGenerators.getAvailableImageGenerators()) {
      result.add(element.getRight());
    }
    return result;
  }

  private List<ClassTreeNode> getElementTypes() {
    return getClassStringTypesList(Element.class);
  }

  private List<ClassTreeNode> getClassStringTypesList(final Class<?> elementClass) {
    List<ClassTreeNode> result = new ArrayList<>();
    ElementUtils elementUtils = new ElementUtils();
    ClassTreeNode top = elementUtils.getAnnotatedElementClassTree();
    Queue<ClassTreeNode> queue = new LinkedList<>();
    queue.add(top);
    while (!queue.isEmpty()) {
      ClassTreeNode clazz = queue.poll();
      queue.addAll(clazz.getChildren());
      if (elementClass.isAssignableFrom(clazz.getClazz())) {
        result.add(clazz);
      }
    }
    return result;
  }

  private List<ClassTreeNode> getReactionTypes() {
    return getClassStringTypesList(Reaction.class);
  }

  private Map<String, MiriamType> getMiriamTypes() {
    Map<String, MiriamType> result = new TreeMap<>();
    for (final MiriamType type : MiriamType.values()) {
      result.put(type.name(), type);
    }
    return result;
  }

  private Map<String, ModificationState> getModificationStateTypes() {
    Map<String, ModificationState> result = new TreeMap<>();
    for (final ModificationState type : ModificationState.values()) {
      result.put(type.name(), type);
    }
    return result;
  }

  private Map<String, PrivilegeType> getPrivilegeTypes() {
    Map<String, PrivilegeType> result = new TreeMap<>();
    for (final PrivilegeType type : PrivilegeType.values()) {
      result.put(type.name(), type);
    }
    return result;
  }

}