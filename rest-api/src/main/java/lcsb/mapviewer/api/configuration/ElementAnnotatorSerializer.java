package lcsb.mapviewer.api.configuration;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.annotation.services.annotators.IElementAnnotator;

public class ElementAnnotatorSerializer extends JsonSerializer<IElementAnnotator> {

  @Override
  public void serialize(final IElementAnnotator annotator, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeStringField("className", annotator.createAnnotatorData().getAnnotatorClassName().getName());
    gen.writeStringField("name", annotator.getCommonName());
    gen.writeStringField("description", annotator.getDescription());
    gen.writeStringField("url", annotator.getUrl());
    gen.writeObjectField("elementClassNames", annotator.getValidClasses());
    gen.writeObjectField("parameters", annotator.createAnnotatorData().getAnnotatorParams());

    gen.writeEndObject();
  }
}