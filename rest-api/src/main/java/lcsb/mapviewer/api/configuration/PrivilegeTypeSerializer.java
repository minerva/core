package lcsb.mapviewer.api.configuration;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.security.PrivilegeType;

public class PrivilegeTypeSerializer extends JsonSerializer<PrivilegeType> {

  @Override
  public void serialize(final PrivilegeType type, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeStringField("commonName", type.getDescription());
    if (type.getPrivilegeObjectType() != null) {
      gen.writeStringField("objectType", type.getPrivilegeObjectType().getSimpleName());
    } else {
      gen.writeStringField("objectType", null);
    }
    gen.writeStringField("valueType", "boolean");

    gen.writeEndObject();
  }
}