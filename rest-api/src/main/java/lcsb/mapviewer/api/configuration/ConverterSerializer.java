package lcsb.mapviewer.api.configuration;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.converter.Converter;

public class ConverterSerializer extends JsonSerializer<Converter> {

  @Override
  public void serialize(final Converter converter, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeStringField("name", converter.getCommonName());
    gen.writeStringField("handler", converter.getClass().getCanonicalName());
    gen.writeStringField("extension", converter.getFileExtensions().get(0));
    gen.writeArrayFieldStart("extensions");
    for (String extension : converter.getFileExtensions()) {
      gen.writeString(extension);
    }
    gen.writeEndArray();

    gen.writeEndObject();
  }
}