package lcsb.mapviewer.api.configuration;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.annotation.services.annotators.IElementAnnotator;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.overlay.DataOverlayType;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.ConfigurationOption;
import lcsb.mapviewer.model.user.annotator.BioEntityField;
import lcsb.mapviewer.modelutils.map.ClassTreeNode;
import lcsb.mapviewer.modelutils.serializer.ClassTreeNodeSerializer;

import java.util.List;
import java.util.Map;

public class ConfigurationDto {
  private List<ConfigurationOption> options;

  @JsonSerialize(contentUsing = AbstractImageGeneratorSerializer.class)
  private List<Class<? extends AbstractImageGenerator>> imageFormats;

  @JsonSerialize(contentUsing = ConverterSerializer.class)
  private List<Converter> modelFormats;

  @JsonSerialize(contentUsing = DataOverlayTypeSerializer.class)
  private DataOverlayType[] overlayTypes;

  @JsonSerialize(contentUsing = ClassTreeNodeSerializer.class)
  private List<ClassTreeNode> elementTypes;

  @JsonSerialize(contentUsing = ClassTreeNodeSerializer.class)
  private List<ClassTreeNode> reactionTypes;

  @JsonSerialize(contentUsing = MiriamTypeSerializer.class)
  private Map<String, MiriamType> miriamTypes;

  @JsonSerialize(contentUsing = SubmodelTypeSerializer.class)
  private SubmodelType[] mapTypes;

  @JsonSerialize(contentUsing = SbmlUnitTypeSerializer.class)
  private SbmlUnitType[] unitTypes;

  @JsonSerialize(contentUsing = ModificationStateSerializer.class)
  private Map<String, ModificationState> modificationStateTypes;

  @JsonSerialize(contentUsing = PrivilegeTypeSerializer.class)
  private Map<String, PrivilegeType> privilegeTypes;

  private String version;

  private String buildDate;

  private String gitHash;

  @JsonSerialize(contentUsing = ElementAnnotatorSerializer.class)
  private List<IElementAnnotator> annotators;

  @JsonSerialize(contentUsing = BioEntityFieldSerializer.class)
  private BioEntityField[] bioEntityFields;

  public List<ConfigurationOption> getOptions() {
    return options;
  }

  public void setOptions(final List<ConfigurationOption> options) {
    this.options = options;
  }

  public List<Class<? extends AbstractImageGenerator>> getImageFormats() {
    return imageFormats;
  }

  public void setImageFormats(final List<Class<? extends AbstractImageGenerator>> imageFormats) {
    this.imageFormats = imageFormats;
  }

  public List<Converter> getModelFormats() {
    return modelFormats;
  }

  public void setModelFormats(final List<Converter> modelFormats) {
    this.modelFormats = modelFormats;
  }

  public DataOverlayType[] getOverlayTypes() {
    return overlayTypes;
  }

  public void setOverlayTypes(final DataOverlayType[] overlayTypes) {
    this.overlayTypes = overlayTypes;
  }

  public List<ClassTreeNode> getElementTypes() {
    return elementTypes;
  }

  public void setElementTypes(final List<ClassTreeNode> elementTypes) {
    this.elementTypes = elementTypes;
  }

  public List<ClassTreeNode> getReactionTypes() {
    return reactionTypes;
  }

  public void setReactionTypes(final List<ClassTreeNode> reactionTypes) {
    this.reactionTypes = reactionTypes;
  }

  public Map<String, MiriamType> getMiriamTypes() {
    return miriamTypes;
  }

  public void setMiriamTypes(final Map<String, MiriamType> miriamTypes) {
    this.miriamTypes = miriamTypes;
  }

  public SubmodelType[] getMapTypes() {
    return mapTypes;
  }

  public void setMapTypes(final SubmodelType[] mapTypes) {
    this.mapTypes = mapTypes;
  }

  public SbmlUnitType[] getUnitTypes() {
    return unitTypes;
  }

  public void setUnitTypes(final SbmlUnitType[] unitTypes) {
    this.unitTypes = unitTypes;
  }

  public Map<String, ModificationState> getModificationStateTypes() {
    return modificationStateTypes;
  }

  public void setModificationStateTypes(final Map<String, ModificationState> modificationStateTypes) {
    this.modificationStateTypes = modificationStateTypes;
  }

  public Map<String, PrivilegeType> getPrivilegeTypes() {
    return privilegeTypes;
  }

  public void setPrivilegeTypes(final Map<String, PrivilegeType> privilegeTypes) {
    this.privilegeTypes = privilegeTypes;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(final String version) {
    this.version = version;
  }

  public String getBuildDate() {
    return buildDate;
  }

  public void setBuildDate(final String buildDate) {
    this.buildDate = buildDate;
  }

  public String getGitHash() {
    return gitHash;
  }

  public void setGitHash(final String gitHash) {
    this.gitHash = gitHash;
  }

  public List<IElementAnnotator> getAnnotators() {
    return annotators;
  }

  public void setAnnotators(final List<IElementAnnotator> annotators) {
    this.annotators = annotators;
  }

  public BioEntityField[] getBioEntityFields() {
    return bioEntityFields;
  }

  public void setBioEntityFields(final BioEntityField[] bioEntityFields) {
    this.bioEntityFields = bioEntityFields;
  }

}
