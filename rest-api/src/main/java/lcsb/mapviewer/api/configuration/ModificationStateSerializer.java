package lcsb.mapviewer.api.configuration;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.map.species.field.ModificationState;

public class ModificationStateSerializer extends JsonSerializer<ModificationState> {

  @Override
  public void serialize(final ModificationState state, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeStringField("commonName", state.getFullName());
    gen.writeStringField("abbreviation", state.getAbbreviation());

    gen.writeEndObject();
  }
}