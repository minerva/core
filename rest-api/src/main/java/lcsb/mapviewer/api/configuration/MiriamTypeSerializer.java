package lcsb.mapviewer.api.configuration;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.map.MiriamType;

public class MiriamTypeSerializer extends JsonSerializer<MiriamType> {

  @Override
  public void serialize(final MiriamType type, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeStringField("commonName", type.getCommonName());
    gen.writeStringField("homepage", type.getDbHomepage());
    gen.writeStringField("registryIdentifier", type.getRegistryIdentifier());
    gen.writeObjectField("uris", type.getUris());

    gen.writeEndObject();
  }
}