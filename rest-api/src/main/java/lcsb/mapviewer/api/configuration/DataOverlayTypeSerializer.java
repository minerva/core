package lcsb.mapviewer.api.configuration;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.overlay.DataOverlayType;

public class DataOverlayTypeSerializer extends JsonSerializer<DataOverlayType> {

  @Override
  public void serialize(final DataOverlayType type, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeStringField("name", type.name());

    gen.writeEndObject();
  }
}