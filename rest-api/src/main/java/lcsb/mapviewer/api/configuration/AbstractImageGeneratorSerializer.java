package lcsb.mapviewer.api.configuration;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator;
import lcsb.mapviewer.converter.graphics.ImageGenerators;

public class AbstractImageGeneratorSerializer extends JsonSerializer<Class<? extends AbstractImageGenerator>> {

  private static final ImageGenerators imageGenerators = new ImageGenerators();

  @Override
  public void serialize(final Class<? extends AbstractImageGenerator> clazz, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    for (Pair<String, Class<? extends AbstractImageGenerator>> pair : imageGenerators.getAvailableImageGenerators()) {
      if (pair.getRight() == clazz) {
        gen.writeStringField("name", pair.getLeft());
        gen.writeStringField("handler", clazz.getCanonicalName());
        gen.writeStringField("extension", imageGenerators.getExtension(clazz.getCanonicalName()));
      }
    }

    gen.writeEndObject();
  }
}