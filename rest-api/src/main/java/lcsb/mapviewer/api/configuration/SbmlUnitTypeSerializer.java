package lcsb.mapviewer.api.configuration;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;

public class SbmlUnitTypeSerializer extends JsonSerializer<SbmlUnitType> {

  @Override
  public void serialize(final SbmlUnitType type, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeStringField("name", type.getCommonName());
    gen.writeStringField("id", type.name());

    gen.writeEndObject();
  }
}