package lcsb.mapviewer.api.taxonomy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.annotation.services.TaxonomySearchException;
import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.ITaxonomyService;

@RestController
@RequestMapping(
    value = {
        "/minerva/api/taxonomy"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class TaxonomyController extends BaseController {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger();

  private ITaxonomyService taxonomyService;

  @Autowired
  public TaxonomyController(final ITaxonomyService taxonomyService) {
    this.taxonomyService = taxonomyService;
  }

  static class Taxonomy {
    private String name;
    private String id;

    public Taxonomy(final String name, final String id) {
      this.name = name;
      this.id = id;
    }

    public String getName() {
      return name;
    }

    public void setName(final String name) {
      this.name = name;
    }

    public String getId() {
      return id;
    }

    public void setId(final String id) {
      this.id = id;
    }
  }

  @GetMapping(value = "/{id:.+}")
  public Taxonomy getTaxonomy(final @PathVariable(value = "id") String id)
      throws TaxonomySearchException, ObjectNotFoundException {
    String name = taxonomyService.getNameForTaxonomy(new MiriamData(MiriamType.TAXONOMY, id));
    if (name == null) {
      throw new ObjectNotFoundException("Object not found: " + id);
    }
    return new Taxonomy(name, id);
  }
}