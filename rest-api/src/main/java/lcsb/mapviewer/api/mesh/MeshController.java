package lcsb.mapviewer.api.mesh;

import lcsb.mapviewer.annotation.data.MeSH;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IMeshService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(
    value = {
        "/minerva/api/mesh"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class MeshController extends BaseController {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger();

  private final IMeshService meshService;

  @Autowired
  public MeshController(final IMeshService meshService) {
    this.meshService = meshService;
  }

  @GetMapping(value = "/{id:.+}")
  public MeSH getMesh(final @PathVariable(value = "id") String id)
      throws AnnotatorException, ObjectNotFoundException {

    MeSH mesh = meshService.getMesh(new MiriamData(MiriamType.MESH_2012, id));
    if (mesh == null) {
      throw new ObjectNotFoundException("Object not found: " + id);
    }
    return mesh;
  }
}