package lcsb.mapviewer.api.minervanet;

public class ReportSubmissionException extends RuntimeException {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public ReportSubmissionException() {
    super();
  }

  public ReportSubmissionException(final String message) {
    super(message);
  }

  public ReportSubmissionException(final String message, final Throwable cause) {
    super(message, cause);
  }

  public ReportSubmissionException(final Throwable cause) {
    super(cause);
  }

  protected ReportSubmissionException(final String message, final Throwable cause, final boolean enableSuppression,
      final boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
