package lcsb.mapviewer.api.minervanet;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.validator.routines.UrlValidator;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.FailedDependencyException;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IUserService;

@RestController
@RequestMapping(
    value = {
        "/minerva/api/minervanet"
    })
public class MinervaNetController extends BaseController {

  private Logger logger = LogManager.getLogger();

  private IConfigurationService configurationService;
  private IUserService userService;

  @Autowired
  public MinervaNetController(final IConfigurationService configurationService, final IUserService userService) {
    this.configurationService = configurationService;
    this.userService = userService;
  }

  @PostMapping(value = "/submitError")
  public void submitError(final @RequestBody ErrorReport report) throws IOException {
    String version = configurationService.getSystemSvnVersion();
    report.setVersion(version);

    ObjectMapper mapper = new ObjectMapper();
    String jsonReport = mapper.writeValueAsString(report);

    String server = configurationService.getValue(ConfigurationElementType.MINERVANET_URL).getValue();

    String url = server + "/api/reports";

    try (final CloseableHttpClient client = HttpClientBuilder.create().build()) {
      StringEntity requestEntity = new StringEntity(jsonReport, ContentType.APPLICATION_JSON);
      HttpPost post = new HttpPost(url);
      post.setEntity(requestEntity);
      try (final CloseableHttpResponse response = client.execute(post)) {
        HttpEntity responseEntity = response.getEntity();
        String responseBody = EntityUtils.toString(responseEntity, "UTF-8");
        if (response.getStatusLine().getStatusCode() != 200 || !responseBodyValid(responseBody)) {
          String error = "Could not submit report to MinervaNet. Reason: " + responseBody;
          logger.error(error);
          throw new ReportSubmissionException(error);
        }
      }
    }
  }

  private boolean responseBodyValid(final String body) {
    ObjectMapper mapper = new ObjectMapper();
    mapper.enable(DeserializationFeature.FAIL_ON_READING_DUP_TREE_KEY);
    try {
      mapper.readTree(body);
    } catch (final IOException e) {
      return false;
    }
    return true;
  }

  @PostMapping(value = "/registerMachine")
  @PreAuthorize("hasAnyAuthority('IS_ADMIN')")
  public void registerMachine(final Authentication authentication) throws Exception {
    User user = userService.getUserByLogin(authentication.getName());
    String minervaRootUrl = configurationService.getConfigurationValue(ConfigurationElementType.MINERVA_ROOT);
    if (!new UrlValidator().isValid(minervaRootUrl)) {
      throw new QueryException("Minerva root configuration option is invalid: " + minervaRootUrl);
    }

    String server = configurationService.getValue(ConfigurationElementType.MINERVANET_URL).getValue();

    String url = server + "/api/machines/";

    Map<String, Object> data = new HashMap<>();
    data.put("email", user.getEmail());
    if (user.getEmail() == null || user.getEmail().isEmpty()) {
      throw new QueryException("User registering map must have valid email address");
    }
    data.put("name", user.getName() + " (" + user.getLogin() + ")");
    data.put("rootUrl", minervaRootUrl);

    ObjectMapper mapper = new ObjectMapper();
    String jsonReport = mapper.writeValueAsString(data);

    Map<String, Object> responseObject;
    try (final CloseableHttpClient client = HttpClientBuilder.create().build()) {
      StringEntity requestEntity = new StringEntity(jsonReport, ContentType.APPLICATION_JSON);
      HttpPost post = new HttpPost(url);
      post.setEntity(requestEntity);
      try (final CloseableHttpResponse response = client.execute(post)) {
        HttpEntity responseEntity = response.getEntity();
        String responseBody = EntityUtils.toString(responseEntity, "UTF-8");
        if (response.getStatusLine().getStatusCode() != 200) {
          String error = "Problem with registering minerva instance. Reason: " + responseBody;
          logger.error(error);
          throw new FailedDependencyException(error);
        }
        responseObject = mapper.readValue(responseBody, new TypeReference<Map<String, Object>>() {
        });
      }
    }
    String token = (String) responseObject.get("authenticationToken");
    configurationService.setConfigurationValue(ConfigurationElementType.MINERVANET_AUTH_TOKEN, token);
  }

  @PostMapping(value = "/unregisterMachine")
  @PreAuthorize("hasAnyAuthority('IS_ADMIN')")
  public void unregisterMachine() throws IOException, QueryException, FailedDependencyException {
    String authToken = configurationService.getConfigurationValue(ConfigurationElementType.MINERVANET_AUTH_TOKEN);

    if (authToken == null || authToken.isEmpty()) {
      throw new QueryException("MINERVANET_AUTH_TOKEN is not provided");
    }

    String server = configurationService.getValue(ConfigurationElementType.MINERVANET_URL).getValue();
    String machineId = getMachineIdInMinervaNet();
    String url = server + "/api/machines/" + machineId;

    try (final CloseableHttpClient client = HttpClientBuilder.create().build()) {
      HttpDelete post = new HttpDelete(url);
      post.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + authToken);
      try (final CloseableHttpResponse response = client.execute(post)) {
        HttpEntity responseEntity = response.getEntity();
        String responseBody = EntityUtils.toString(responseEntity, "UTF-8");
        if (response.getStatusLine().getStatusCode() != 200) {
          String error = "Problem with unregistering minerva instance. Reason: " + responseBody + " (status code: "
              + response.getStatusLine().getStatusCode() + ")";
          logger.error(error);
          throw new FailedDependencyException(error);
        }
      }
    }
    configurationService.setConfigurationValue(ConfigurationElementType.MINERVANET_AUTH_TOKEN, "");
  }

  private String getMachineIdInMinervaNet() throws IOException, FailedDependencyException {
    String minervaRoot = configurationService.getValue(ConfigurationElementType.MINERVA_ROOT).getValue();

    String server = configurationService.getValue(ConfigurationElementType.MINERVANET_URL).getValue();
    String url = server + "/api/machines/?size=1000";

    ObjectMapper mapper = new ObjectMapper();

    try (final CloseableHttpClient client = HttpClientBuilder.create().build()) {
      HttpGet get = new HttpGet(url);
      try (final CloseableHttpResponse response = client.execute(get)) {
        HttpEntity responseEntity = response.getEntity();
        String responseBody = EntityUtils.toString(responseEntity, "UTF-8");
        if (response.getStatusLine().getStatusCode() != 200) {
          String error = "Problem with unregistering minerva instance. Reason: " + responseBody + " (status code: "
              + response.getStatusLine().getStatusCode() + ")";
          logger.error(error);
          throw new FailedDependencyException(error);
        }
        Map<String, Object> responseObject = mapper.readValue(responseBody, new TypeReference<Map<String, Object>>() {
        });
        @SuppressWarnings("unchecked")
        List<Map<String, Object>> list = (List<Map<String, Object>>) responseObject.get("pageContent");
        for (Map<String, Object> machine : list) {
          if (machine.get("rootUrl").equals(minervaRoot)) {
            return machine.get("id") + "";
          }
        }
      }
    }
    throw new FailedDependencyException("Minerva-net claims that this minerva instance is not registered");
  }

  @PostMapping(value = "/projects/{projectId}:registerProject")
  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId)")
  public void registerProject(final Authentication authentication, final @PathVariable(value = "projectId") String projectId)
      throws IOException, QueryException, FailedDependencyException {
    User user = userService.getUserByLogin(authentication.getName());

    String authToken = configurationService.getConfigurationValue(ConfigurationElementType.MINERVANET_AUTH_TOKEN);

    if (authToken == null || authToken.isEmpty()) {
      throw new QueryException("MINERVANET_AUTH_TOKEN is not provided");
    }

    String server = configurationService.getValue(ConfigurationElementType.MINERVANET_URL).getValue();
    String machineId = getMachineIdInMinervaNet();
    String url = server + "/api/machines/" + machineId + "/projects/";

    Map<String, String> data = new HashMap<>();
    data.put("email", user.getEmail());
    if (user.getEmail() == null || user.getEmail().isEmpty()) {
      throw new QueryException("User registering map must have valid email address");
    }
    data.put("projectId", projectId);

    ObjectMapper mapper = new ObjectMapper();
    String jsonReport = mapper.writeValueAsString(data);

    try (final CloseableHttpClient client = HttpClientBuilder.create().build()) {
      StringEntity requestEntity = new StringEntity(jsonReport, ContentType.APPLICATION_JSON);
      HttpPost post = new HttpPost(url);
      post.setEntity(requestEntity);
      post.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + authToken);
      try (final CloseableHttpResponse response = client.execute(post)) {
        HttpEntity responseEntity = response.getEntity();
        String responseBody = EntityUtils.toString(responseEntity, "UTF-8");
        if (response.getStatusLine().getStatusCode() != 200) {
          String error = "Problem with registering minerva project. Reason: " + responseBody + " (status code: "
              + response.getStatusLine().getStatusCode() + ")";
          logger.error(error);
          throw new FailedDependencyException(error);
        }
      }
    }
  }

  @PostMapping(value = "/projects/{projectId}:unregisterProject")
  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId)")
  public void unregisterProject(final @PathVariable(value = "projectId") String projectId)
      throws IOException, QueryException, FailedDependencyException {
    String authToken = configurationService.getConfigurationValue(ConfigurationElementType.MINERVANET_AUTH_TOKEN);

    if (authToken == null || authToken.isEmpty()) {
      throw new QueryException("MINERVANET_AUTH_TOKEN is not provided");
    }

    String server = configurationService.getValue(ConfigurationElementType.MINERVANET_URL).getValue();
    String machineId = getMachineIdInMinervaNet();
    String url = server + "/api/machines/" + machineId + "/projects/" + projectId;

    try (final CloseableHttpClient client = HttpClientBuilder.create().build()) {
      HttpDelete post = new HttpDelete(url);
      post.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + authToken);
      try (final CloseableHttpResponse response = client.execute(post)) {
        HttpEntity responseEntity = response.getEntity();
        String responseBody = EntityUtils.toString(responseEntity, "UTF-8");
        if (response.getStatusLine().getStatusCode() != 200) {
          String error = "Problem with unregistering minerva project. Reason: " + responseBody + " (status code: "
              + response.getStatusLine().getStatusCode() + ")";
          logger.error(error);
          throw new FailedDependencyException(error);
        }
      }
    }
  }

  public Set<String> getSharedProjects() throws FailedDependencyException, IOException {
    if (Objects.equals("", configurationService.getValue(ConfigurationElementType.MINERVANET_AUTH_TOKEN).getValue())) {
      return new HashSet<>();
    }
    Set<String> result = new HashSet<>();
    String server = configurationService.getValue(ConfigurationElementType.MINERVANET_URL).getValue();
    String machineId = getMachineIdInMinervaNet();
    String url = server + "/api/machines/" + machineId + "/projects/";
    try (final CloseableHttpClient client = HttpClientBuilder.create().build()) {
      HttpGet post = new HttpGet(url);
      try (final CloseableHttpResponse response = client.execute(post)) {
        HttpEntity responseEntity = response.getEntity();
        String responseBody = EntityUtils.toString(responseEntity, "UTF-8");
        if (response.getStatusLine().getStatusCode() != 200) {
          String error = "Problem with listing projects. Reason: " + responseBody + " (status code: "
              + response.getStatusLine().getStatusCode() + ")";
          logger.error(error);
          throw new FailedDependencyException(error);
        }
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> responseObject = mapper.readValue(responseBody, new TypeReference<Map<String, Object>>() {
        });
        @SuppressWarnings("unchecked")
        List<Map<String, Object>> listOfProjects = (List<Map<String, Object>>) responseObject.get("pageContent");
        for (Map<String, Object> map : listOfProjects) {
          result.add((String) map.get("projectId"));
        }
      }
    }

    return result;
  }

  @GetMapping(value = "/isMachineRegistered")
  public Map<String, Object> isMachineRegistered() {
    Map<String, Object> result = new HashMap<>();
    String authToken = configurationService.getConfigurationValue(ConfigurationElementType.MINERVANET_AUTH_TOKEN);
    result.put("status", authToken != null && !authToken.isEmpty());
    return result;
  }

}
