package lcsb.mapviewer.api.users;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.services.ObjectNotFoundException;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/api/oauth"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class OauthController extends BaseController {

  private ClientRegistrationRepository clientRegistrationRepository;

  @Autowired
  @Lazy
  public OauthController(final ClientRegistrationRepository clientRegistrationRepository) {
    this.clientRegistrationRepository = clientRegistrationRepository;
  }

  @GetMapping(value = "/providers")
  public Map<String, String> oauthProviders()
      throws IOException, QueryException, ObjectNotFoundException {

    String authorizationRequestBaseUri = "/oauth2/authorize-client";

    Map<String, String> result = new HashMap<>();
    for (String client : lcsb.mapviewer.common.Configuration.availableOAuthClients) {
      ClientRegistration clientRegistration = clientRegistrationRepository.findByRegistrationId(client);
      if (clientRegistration != null) {
        result.put(clientRegistration.getClientName(), authorizationRequestBaseUri + "/" + clientRegistration.getRegistrationId());
      }
    }
    return result;
  }

}