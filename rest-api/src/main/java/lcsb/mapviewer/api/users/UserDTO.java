package lcsb.mapviewer.api.users;

import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonFilter;

import lcsb.mapviewer.model.user.User;

@JsonFilter("userFilter")
public class UserDTO extends User {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private boolean ldapAccountAvailable;

  private Calendar lastActive;

  public UserDTO(final User user, final boolean ldapAccountAvailable, final Calendar lastActive) {
    this.ldapAccountAvailable = ldapAccountAvailable;
    setId(user.getId());
    setLogin(user.getLogin());
    setCryptedPassword(user.getCryptedPassword());
    setName(user.getName());
    setSurname(user.getSurname());
    setEmail(user.getEmail());
    setMinColor(user.getMinColor());
    setMaxColor(user.getMaxColor());
    setNeutralColor(user.getNeutralColor());
    setSimpleColor(user.getSimpleColor());
    setRemoved(user.isRemoved());
    setActive(user.isActive());
    setConfirmed(user.isConfirmed());
    setConnectedToLdap(user.isConnectedToLdap());
    setTermsOfUseConsent(user.isTermsOfUseConsent());
    setTermsOfUseConsentDates(user.getTermsOfUseConsentDates());
    setPrivileges(user.getPrivileges());
    setAnnotationSchema(user.getAnnotationSchema());
    setLastActive(lastActive);
    setOrcidId(user.getOrcidId());
  }

  public boolean isLdapAccountAvailable() {
    return ldapAccountAvailable;
  }

  public void setLdapAccountAvailable(final boolean ldapAccountAvailable) {
    this.ldapAccountAvailable = ldapAccountAvailable;
  }

  public Calendar getLastActive() {
    return lastActive;
  }

  public void setLastActive(final Calendar lastActive) {
    this.lastActive = lastActive;
  }
}
