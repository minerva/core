package lcsb.mapviewer.api.users;

import javax.validation.constraints.NotNull;

import lcsb.mapviewer.model.user.UserAnnotationSchema;

public class UserPreferencesDTO {

  @NotNull
  private UserAnnotationSchema preferences;

  protected UserPreferencesDTO() {

  }

  public UserPreferencesDTO(final UserAnnotationSchema annotationSchema) {
    this.preferences = annotationSchema;
  }

  public UserAnnotationSchema getPreferences() {
    return preferences;
  }

  public void setPreferences(final UserAnnotationSchema preferences) {
    this.preferences = preferences;
  }

}
