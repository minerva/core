package lcsb.mapviewer.api.users;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.OperationNotAllowedException;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.api.UpdateConflictException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.model.user.UserAnnotationSchema;
import lcsb.mapviewer.model.user.UserClassAnnotators;
import lcsb.mapviewer.model.user.UserGuiPreference;
import lcsb.mapviewer.modelutils.serializer.CustomExceptFilter;
import lcsb.mapviewer.modelutils.serializer.model.security.PrivilegeKeyDeserializer;
import lcsb.mapviewer.services.InvalidTokenException;
import lcsb.mapviewer.services.ObjectExistsException;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IAnnotationService;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.services.utils.EmailException;
import lcsb.mapviewer.services.utils.EmailSender;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.validator.routines.UrlValidator;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.orm.hibernate5.HibernateOptimisticLockingFailureException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

@RestController
@Validated
@RequestMapping(
    value = {
        "/api",
        "/minerva/api",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController extends BaseController {

  private final IUserService userService;
  private final IAnnotationService annotationService;
  private final IConfigurationService configurationService;
  private final PasswordEncoder passwordEncoder;
  private final EmailSender emailSender;

  private final SessionRegistry sessionRegistry;

  @Autowired
  public UserController(final IUserService userService,
                        final PasswordEncoder passwordEncoder,
                        final EmailSender emailSender,
                        final IConfigurationService configurationService,
                        final IAnnotationService annotationService,
                        final SessionRegistry sessionRegistry) {
    this.userService = userService;
    this.passwordEncoder = passwordEncoder;
    this.emailSender = emailSender;
    this.configurationService = configurationService;
    this.sessionRegistry = sessionRegistry;
    this.annotationService = annotationService;
  }

  /**
   * This API action is protected by spring security and will fail if the user is
   * not authenticated. Therefore, simply calling this action can be used to detect
   * whether the current session is still valid.
   */
  @GetMapping(value = "/users/isSessionValid")
  @PreAuthorize("isAuthenticated() and authentication.name != '" + Configuration.ANONYMOUS_LOGIN + "'")
  public Object isSessionValid(final Authentication authentication, final HttpServletRequest request) {
    final Map<String, Object> result = new TreeMap<>();
    result.put("login", authentication.getName());
    result.put("token", request.getSession().getId());
    return result;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'IS_CURATOR') or #login == authentication.name")
  @GetMapping(value = "/users/{login:.+}")
  public MappingJacksonValue getUser(
      final @PathVariable(value = "login") String login,
      final @RequestParam(value = "columns", defaultValue = "") String columns) throws ObjectNotFoundException {
    final Set<String> columnSet = createUserColumnSet(columns);
    final User user = userService.getUserByLogin(login, true);
    if (user == null) {
      throw new ObjectNotFoundException("User doesn't exist");
    }
    if (user.getAnnotationSchema() == null) {
      user.setAnnotationSchema(annotationService.prepareUserAnnotationSchema(user, true));
    }
    Boolean ldapAvailable = false;
    if (columnSet.contains("ldapAccountAvailable")) {
      final List<User> userList = new ArrayList<>();
      userList.add(user);
      ldapAvailable = userService.ldapAccountExistsForLogin(userList).get(login);
    }
    return createResponseWithColumns(columns, new UserDTO(user, ldapAvailable, getLastActive(user)));
  }

  private Calendar getLastActive(final User user) {
    Calendar result = null;
    for (final Object principal : sessionRegistry.getAllPrincipals()) {
      String login = principal.toString();
      if (principal instanceof org.springframework.security.core.userdetails.User) {
        login = ((org.springframework.security.core.userdetails.User) principal).getUsername();
      }
      if (Objects.equals(user.getLogin(), login)) {
        for (final SessionInformation sessionInformation : sessionRegistry.getAllSessions(principal, false)) {
          final Calendar date = Calendar.getInstance();
          date.setTime(sessionInformation.getLastRequest());
          if (result == null) {
            result = date;
          } else if (result.before(date)) {
            result = date;
          }
        }
      }
    }
    return result;
  }

  public static class UserPrivilegesDTO {
    @JsonDeserialize(keyUsing = PrivilegeKeyDeserializer.class)
    private Map<Privilege, Boolean> privileges = new HashMap<>();

    public Map<Privilege, Boolean> getPrivileges() {
      return privileges;
    }

    public void setPrivileges(final Map<Privilege, Boolean> privileges) {
      this.privileges = privileges;
    }
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @PatchMapping(value = "/users/{login}:updatePrivileges", consumes = {MediaType.APPLICATION_JSON_VALUE})
  public MappingJacksonValue updatePrivileges(
      final @NotNull @RequestBody UserPrivilegesDTO data,
      final @PathVariable(value = "login") String login) throws IOException, QueryException, ObjectNotFoundException {
    final User user = userService.getUserByLogin(login);
    if (user == null) {
      throw new QueryException("User does not exist.");
    }

    for (final Privilege privilege : data.getPrivileges().keySet()) {

      final boolean grant;
      try {
        grant = data.getPrivileges().get(privilege);
      } catch (final ClassCastException e) {
        throw new QueryException("Privilege can only be set to true (grant) or false (revoke).", e);
      }
      if (privilege.isObjectPrivilege()) {
        if (grant) {
          userService.grantUserPrivilege(user, privilege.getType(), privilege.getObjectId());
        } else {
          userService.revokeUserPrivilege(user, privilege.getType(), privilege.getObjectId());
        }
      } else {
        if (grant) {
          userService.grantUserPrivilege(user, privilege.getType());
        } else {
          userService.revokeUserPrivilege(user, privilege.getType());
        }
      }
    }
    return getUser(login, "");
  }

  @PreAuthorize("hasAuthority('IS_ADMIN') or #login == authentication.name")
  @PatchMapping(value = "/users/{login}:updatePreferences")
  public UserPreferencesDTO updatePreferences(
      final @Valid @RequestBody UserPreferencesDTO body,
      final @PathVariable(value = "login") String login) throws Exception {
    try {
      User modifiedUser = userService.getUserByLogin(login);
      if (modifiedUser == null) {
        throw new ObjectNotFoundException("User doesn't exist");
      }

      final UserAnnotationSchema schema = annotationService.prepareUserAnnotationSchema(modifiedUser, true);

      if (body.getPreferences().getValidateMiriamTypes() != null) {
        schema.setValidateMiriamTypes(body.getPreferences().getValidateMiriamTypes());
      }
      if (body.getPreferences().getAnnotateModel() != null) {
        schema.setAnnotateModel(body.getPreferences().getAnnotateModel());
      }
      if (body.getPreferences().getAutoResizeMap() != null) {
        schema.setAutoResizeMap(body.getPreferences().getAutoResizeMap());
      }
      if (body.getPreferences().getSemanticZoomContainsMultipleOverlays() != null) {
        schema.setSemanticZoomContainsMultipleOverlays(body.getPreferences().getSemanticZoomContainsMultipleOverlays());
      }
      if (body.getPreferences().getSbgnFormat() != null) {
        schema.setSbgnFormat(body.getPreferences().getSbgnFormat());
      }

      updateElementAnnotators(schema, body.getPreferences());
      updateGuiPreferences(schema, body.getPreferences());

      modifiedUser = userService.getUserByLogin(login);
      modifiedUser.setAnnotationSchema(schema);
      userService.update(modifiedUser);
      return new UserPreferencesDTO(userService.getUserByLogin(login, true).getAnnotationSchema());
    } catch (final IllegalArgumentException e) {
      throw new QueryException("Invalid input", e);
    } catch (final HibernateOptimisticLockingFailureException e) {
      throw new UpdateConflictException("Conflict when updating preferences.", e);
    } catch (final ConstraintViolationException e) {
      if (e.getCause().getMessage().contains("duplicate key value violates unique constraint")) {
        throw new UpdateConflictException("Conflict when updating preferences.", e);
        // hsqldb
      } else if (e.getCause().getMessage().contains("unique constraint or index violation")) {
        throw new UpdateConflictException("Conflict when updating preferences.", e);
      }
      throw e;
    } catch (final Exception e) {
      if (e.getCause() instanceof ConstraintViolationException) {
        // postgres
        if (e.getCause().getCause().getMessage().contains("duplicate key value violates unique constraint")) {
          throw new UpdateConflictException("Conflict when updating preferences.", e);
          // hsqldb
        } else if (e.getCause().getCause().getMessage().contains("unique constraint or index violation")) {
          throw new UpdateConflictException("Conflict when updating preferences.", e);
        }
      }
      throw e;
    }
  }

  private void updateGuiPreferences(final UserAnnotationSchema schema, final UserAnnotationSchema newData) {
    for (final UserGuiPreference preference : newData.getGuiPreferences()) {
      schema.setGuiPreference(preference.getKey(), preference.getValue());
    }
  }

  private void updateElementAnnotators(final UserAnnotationSchema schema, final UserAnnotationSchema newData) throws QueryException {
    for (final UserClassAnnotators newClassAnnotator : newData.getClassAnnotators()) {
      UserClassAnnotators annotator = null;
      for (final UserClassAnnotators userClassAnnotators : schema.getClassAnnotators()) {
        if (userClassAnnotators.getClassName().equals(newClassAnnotator.getClassName())) {
          annotator = userClassAnnotators;
        }
      }
      if (annotator == null) {
        annotator = new UserClassAnnotators(newClassAnnotator.getClass(), newClassAnnotator.getAnnotators());
        schema.addClassAnnotator(annotator);
      } else {
        annotator.setAnnotators(newClassAnnotator.getAnnotators());
      }
    }
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'IS_CURATOR')")
  @GetMapping(value = "/users/")
  public MappingJacksonValue getUsers(final @RequestParam(value = "columns", defaultValue = "") String columns) {
    List<UserDTO> data = new ArrayList<>();
    final List<User> users = userService.getUsers(true);
    final Map<String, Boolean> ldapAvailability = userService.ldapAccountExistsForLogin(users);
    for (final User user : users) {
      data.add(new UserDTO(user, ldapAvailability.get(user.getLogin()), getLastActive(user)));
    }
    data = data.stream()
        .sorted(Comparator.comparing(User::getLogin, Comparator.reverseOrder()))
        .collect(Collectors.toList());

    return createResponseWithColumns(columns, data);
  }

  private MappingJacksonValue createResponseWithColumns(final String columns, final Object data) {
    final MappingJacksonValue result = new MappingJacksonValue(data);
    final SimpleFilterProvider provider = new SimpleFilterProvider();
    if (!columns.trim().isEmpty()) {
      provider.addFilter("userFilter", new CustomExceptFilter(columns.split(",")));
    } else {
      provider.addFilter("userFilter", new CustomExceptFilter(createUserColumnSet(columns)));
    }
    result.setFilters(provider);
    return result;
  }

  static class UpdateUserDTO {
    private String name;
    private String surname;
    private String password;

    @Pattern(regexp = "^(([0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{3}[0-9X])|())$")
    private String orcidId;
    private Boolean connectedToLdap;
    private Boolean termsOfUseConsent;
    private String email;
    private Boolean active;

    public String getName() {
      return name;
    }

    public void setName(final String name) {
      this.name = name;
    }

    public String getSurname() {
      return surname;
    }

    public void setSurname(final String surname) {
      this.surname = surname;
    }

    public String getPassword() {
      return password;
    }

    public void setPassword(final String password) {
      this.password = password;
    }

    public Boolean getConnectedToLdap() {
      return connectedToLdap;
    }

    public void setConnectedToLdap(final Boolean connectedToLdap) {
      this.connectedToLdap = connectedToLdap;
    }

    public Boolean getTermsOfUseConsent() {
      return termsOfUseConsent;
    }

    public void setTermsOfUseConsent(final Boolean termsOfUseConsent) {
      this.termsOfUseConsent = termsOfUseConsent;
    }

    public String getEmail() {
      return email;
    }

    public void setEmail(final String email) {
      this.email = email;
    }

    public String getOrcidId() {
      return orcidId;
    }

    public void setOrcidId(final String orcidId) {
      this.orcidId = orcidId;
    }

    public Boolean getActive() {
      return active;
    }

    public void setActive(final Boolean active) {
      this.active = active;
    }
  }

  static class UpdateUserData {
    @Valid
    private UpdateUserDTO user;

    public UpdateUserDTO getUser() {
      return user;
    }

    public void setUser(final UpdateUserDTO user) {
      this.user = user;
    }
  }

  @PreAuthorize("hasAuthority('IS_ADMIN') or #login == authentication.name")
  @PatchMapping(value = "/users/{login:.+}")
  public MappingJacksonValue updateUser(
      final @Valid @RequestBody UpdateUserData body,
      final @PathVariable(value = "login") String login,
      final Authentication authentication) throws QueryException, IOException, ObjectNotFoundException {
    final UpdateUserDTO userData = body.getUser();

    final boolean isAdmin = authentication.getAuthorities().contains(new SimpleGrantedAuthority(PrivilegeType.IS_ADMIN.toString()));

    if (userData == null) {
      throw new QueryException("user field cannot be undefined");
    }
    final User user = userService.getUserByLogin(login);
    if (user == null) {
      throw new ObjectNotFoundException("user doesn't exist");
    }
    if (userData.getName() != null) {
      user.setName(userData.getName());
    }
    if (userData.getSurname() != null) {
      user.setSurname(userData.getSurname());
    }
    if (userData.getOrcidId() != null) {
      final User oldUser = userService.getUserByOrcidId(userData.getOrcidId());
      if (oldUser != null && !Objects.equals(oldUser.getLogin(), user.getLogin()) && !userData.getOrcidId().trim().isEmpty()) {
        throw new UpdateConflictException("Orcid id is already assigned to another user");
      }
      user.setOrcidId(userData.getOrcidId());
    }
    if (userData.getEmail() != null) {
      user.setEmail(userData.getEmail());
    }
    if (userData.getTermsOfUseConsent() != null) {
      user.setTermsOfUseConsent(userData.getTermsOfUseConsent());
    }
    if (userData.getConnectedToLdap() != null) {
      if (isAdmin) {
        user.setConnectedToLdap(userData.getConnectedToLdap());
      } else if (!Objects.equals(user.isConnectedToLdap(), userData.getConnectedToLdap())) {
        throw new AccessDeniedException("connectedtoldap can be updated by admin");
      }
    }
    boolean sendActivation = false;
    if (userData.getActive() != null) {
      if (isAdmin) {
        if (!user.isActive() && userData.getActive()) {
          sendActivation = true;
        }
        user.setActive(userData.getActive());
      } else if (!Objects.equals(user.isActive(), userData.getActive())) {
        throw new AccessDeniedException("active can be updated by admin");
      }
    }
    if (userData.getPassword() != null) {
      if (userData.getPassword() != null && !userData.getPassword().trim().isEmpty()) {
        user.setCryptedPassword(passwordEncoder.encode(userData.getPassword()));
      }
    }
    userService.update(user);
    if (sendActivation) {
      userService.sendUserActivatedEmail(user);
    }

    return getUser(login, "");
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @PostMapping(value = "/users/{login:.+}")
  public MappingJacksonValue addUser(
      final @RequestParam(name = "name", required = false) String name,
      final @RequestParam(name = "surname", required = false) String surname,
      final @RequestParam(name = "email", required = false) String email,
      final @NotBlank @RequestParam(name = "password") String password,
      final @RequestParam(name = "defaultPrivileges", defaultValue = "false") boolean defaultPrivileges,
      final @PathVariable(value = "login") String login) throws QueryException, ObjectExistsException, ObjectNotFoundException {
    User user = userService.getUserByLogin(login);
    if (user != null) {
      throw new ObjectExistsException("user exists");
    }
    user = new User();
    user.setLogin(login);
    user.setName(name);
    user.setSurname(surname);
    user.setEmail(email);
    user.setCryptedPassword(passwordEncoder.encode(password));
    userService.add(user);
    if (defaultPrivileges) {
      userService.grantDefaultPrivileges(user);
    }
    return getUser(login, "");
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @DeleteMapping(value = "/users/{login:.+}")
  public Object removeUser(final @PathVariable(value = "login") String login) throws Exception {
    final User user = userService.getUserByLogin(login);
    if (user == null) {
      throw new ObjectNotFoundException("user doesn't exists");
    } else if (user.getLogin().equals(Configuration.ANONYMOUS_LOGIN)) {
      throw new OperationNotAllowedException("guest account cannot be removed");
    }
    userService.remove(user);
    return okStatus();
  }

  @PostMapping(value = "/users/{login}:requestResetPassword")
  public Object requestResetPasswordToken(
      final @PathVariable(value = "login") String login) throws QueryException, ObjectNotFoundException {
    final User user = userService.getUserByLogin(login);
    if (user == null) {
      throw new ObjectNotFoundException("User does not exist");
    }
    if (user.getEmail() == null || user.getEmail().equals("")) {
      throw new QueryException("User does not have email address defined");
    }
    if (user.isConnectedToLdap()) {
      throw new QueryException("User authentication is provided over LDAP");
    }

    if (configurationService.getConfigurationValue(ConfigurationElementType.MINERVA_ROOT).trim().isEmpty()) {
      throw new InvalidStateException("Cannot create token - minerva root url is not defined");
    }

    if (!emailSender.canSendEmails()) {
      throw new InvalidStateException("Cannot create token - minerva cannot send emails");
    }

    try {
      userService.createResetPasswordToken(user);
      return okStatus();
    } catch (EmailException e) {
      throw new InvalidStateException("Cannot create token - minerva could not send an email", e);
    }
  }

  @PostMapping(value = "/users:resetPassword")
  public Object resetPassword(
      final @RequestParam(value = "token") String token,
      final @RequestParam(value = "password") String password)
      throws IOException, QueryException, ObjectNotFoundException {
    try {
      userService.resetPassword(token, passwordEncoder.encode(password));
      return okStatus();
    } catch (final InvalidTokenException e) {
      throw new ObjectNotFoundException("Invalid token", e);
    }

  }

  @PostMapping(value = "/users/{login}:confirmEmail")
  public Object confirmEmail(
      final @PathVariable(value = "login") String login,
      final @RequestParam(value = "token", required = true) String token) throws QueryException, EmailException, ObjectNotFoundException {
    try {
      final User user = userService.confirmEmail(login, token);
      final String message;
      if (user.isActive()) {
        message = "Your email is confirmed. You can login";
      } else {
        message = "Your email is confirmed. You need to wait for admin approval before you can login";
      }
      final Map<String, Object> result = okStatus();
      result.put("message", message);
      return result;
    } catch (final InvalidTokenException e) {
      throw new ObjectNotFoundException("Invalid token", e);
    }

  }

  public Set<String> createUserColumnSet(final String columns) {
    final Set<String> columnsSet = new LinkedHashSet<>();
    if (columns.equals("")) {
      columnsSet.add("id");
      columnsSet.add("login");
      columnsSet.add("name");
      columnsSet.add("surname");
      columnsSet.add("email");
      columnsSet.add("minColor");
      columnsSet.add("maxColor");
      columnsSet.add("neutralColor");
      columnsSet.add("simpleColor");
      columnsSet.add("removed");
      columnsSet.add("privileges");
      columnsSet.add("termsOfUseConsent");
      columnsSet.add("connectedToLdap");
      columnsSet.add("ldapAccountAvailable");
      columnsSet.add("lastActive");
      columnsSet.add("orcidId");
      columnsSet.add("active");
      columnsSet.add("confirmed");
    } else {
      columnsSet.addAll(Arrays.asList(columns.split(",")));
    }
    return columnsSet;
  }

  @PostMapping(value = "/users:registerUser")
  public Object registerUser(
      final @RequestBody UpdateUserDTO data) throws QueryException, MessagingException, ObjectNotFoundException {
    if (!configurationService.getConfigurationValue(ConfigurationElementType.ALLOW_AUTO_REGISTER).equalsIgnoreCase("true")) {
      throw new QueryException("Registration of new users is not enabled.");
    }
    if (!emailSender.canSendEmails()) {
      throw new QueryException("Cannot register new user. Cannot send emails from minerva");
    }
    final String minervaRootUrl = configurationService.getConfigurationValue(ConfigurationElementType.MINERVA_ROOT);
    if (!new UrlValidator().isValid(minervaRootUrl)) {
      throw new QueryException("Cannot send confirmation email.");
    }

    final User user = new User();
    user.setLogin(data.getEmail());
    if (!EmailValidator.getInstance().isValid(data.getEmail())) {
      throw new QueryException("Invalid email address: " + data.getEmail());
    }
    user.setEmail(data.getEmail());
    user.setName(data.getName());
    user.setSurname(data.getSurname());
    user.setCryptedPassword(passwordEncoder.encode(data.getPassword()));

    try {
      userService.registerUser(user);
      userService.grantDefaultPrivileges(user);
      return getUser(user.getLogin(), "");
    } catch (EmailException e) {
      throw new InvalidStateException("Cannot register user. Minerva could not send an email", e);
    } catch (InvalidArgumentException e) {
      throw new QueryException("Cannot register user. " + e.getMessage(), e);
    }
  }

  @PostMapping(value = "/users/{login}:resendConfirmEmail")
  public Object resendConfirmationEmail(
      final @PathVariable(value = "login") String login) throws QueryException, MessagingException, ObjectNotFoundException {
    final User user = userService.getUserByLogin(login);
    if (user == null) {
      throw new QueryException("Cannot send confirmation email. User does not exist");
    }
    if (user.isConfirmed()) {
      throw new QueryException("Cannot send confirmation email. Account already confirmed");
    }
    if (!emailSender.canSendEmails()) {
      throw new QueryException("Cannot send confirmation email.");
    }
    final String minervaRootUrl = configurationService.getConfigurationValue(ConfigurationElementType.MINERVA_ROOT);
    if (!new UrlValidator().isValid(minervaRootUrl)) {
      throw new QueryException("Cannot send confirmation email.");
    }

    try {
      userService.createRegisterUserToken(user);
      return okStatus();
    } catch (EmailException e) {
      throw new InvalidStateException("Cannot create token - minerva could not send an email", e);
    }
  }

}