package lcsb.mapviewer.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.annotation.services.dapi.DapiConnectionException;
import lcsb.mapviewer.api.minervanet.ReportSubmissionException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.Stacktrace;
import lcsb.mapviewer.services.FailedDependencyException;
import lcsb.mapviewer.services.ObjectExistsException;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IStacktraceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.Nullable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {
  private static final Logger logger = LogManager.getLogger();

  private final ObjectMapper objectMapper = new ObjectMapper();

  @Autowired
  private IStacktraceService stacktraceService;

  @Override
  protected ResponseEntity<Object> handleExceptionInternal(
      final Exception ex, @Nullable final Object body, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
    return handleException(ex);
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<Object> handleException(final Exception e) {
    if (e instanceof AccessDeniedException) {
      return createErrorResponse("Access denied.", e.getMessage(), new HttpHeaders(), HttpStatus.FORBIDDEN);
    } else if (e instanceof ObjectNotFoundException) {
      return createErrorResponse("Object not found.", e.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND);
    } else if (e instanceof ObjectExistsException) {
      return createErrorResponse("Object already exists.", e.getMessage(), new HttpHeaders(), HttpStatus.CONFLICT);
    } else if (e instanceof PreconditionFailedException) {
      return createErrorResponse("Precondition failed.", e.getMessage(), new HttpHeaders(), HttpStatus.PRECONDITION_FAILED);
    } else if (e instanceof FailedDependencyException) {
      return createErrorResponse("Problem with external service.", e.getMessage(), new HttpHeaders(), HttpStatus.FAILED_DEPENDENCY);
    } else if (e instanceof ReportSubmissionException) {
      return createErrorResponse("Problem with external service.", e.getMessage(), new HttpHeaders(), HttpStatus.FAILED_DEPENDENCY);
    } else if (e instanceof UpdateConflictException) {
      return createErrorResponse("There is a conflict with another query. Try again later.", e.getMessage(),
          new HttpHeaders(), HttpStatus.CONFLICT);
    } else if (e instanceof OperationNotAllowedException) {
      return createErrorResponse("Operation not allowed.", e.getMessage(), new HttpHeaders(), HttpStatus.FORBIDDEN);
    } else if (e instanceof DapiConnectionException) {
      logger.debug(e, e);
      return createErrorResponse("There is problem with DAPI connection.", e.getMessage(), new HttpHeaders(),
          HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
    } else if (e instanceof ConstraintViolationException) {
      final StringBuilder sb = new StringBuilder();
      for (final ConstraintViolation<?> violation : ((ConstraintViolationException) e).getConstraintViolations()) {
        sb.append("[").append(violation.getMessage()).append("] ");
      }
      return createErrorResponse("Query server error.", sb.toString(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    } else if (e instanceof MethodArgumentNotValidException) {
      final StringBuilder sb = new StringBuilder();
      for (final ObjectError error : ((MethodArgumentNotValidException) e).getBindingResult().getAllErrors()) {
        if (error instanceof FieldError) {
          sb.append("[").append(((FieldError) error).getField()).append(" - ").append(error.getDefaultMessage()).append("] ");
        } else {
          sb.append("[").append(error).append("] ");
        }
      }
      logger.debug(e, e);
      return createErrorResponse("Query server error.", sb.toString(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    } else if (e instanceof HttpMediaTypeNotSupportedException) {
      logger.debug(e, e);
      return createErrorResponse("Query server error.", e.getMessage(), new HttpHeaders(), HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    } else if (e instanceof QueryException
        || e instanceof HttpMessageNotReadableException
        || e instanceof MissingServletRequestParameterException
        || e instanceof JsonMappingException
        || e instanceof MissingPathVariableException
        || e instanceof MethodArgumentTypeMismatchException) {
      logger.debug(e, e);
      return createErrorResponse("Query server error.", e.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    } else if (e instanceof ServletRequestBindingException && e.getMessage().contains(Configuration.AUTH_TOKEN)) {
      return createErrorResponse("Access denied.", e.getMessage(), new HttpHeaders(), HttpStatus.FORBIDDEN);
    } else {
      logger.error(e, e);
      return createErrorResponse(e, new HttpHeaders());
    }
  }

  private ResponseEntity<Object> createErrorResponse(final Exception e,
                                                     final HttpHeaders httpHeaders) {

    String content = "Internal server error.";
    try {
      Stacktrace st = stacktraceService.createFromStackTrace(e);

      final Map<String, String> response = new HashMap<>();
      response.put("error", "Internal server error.");
      response.put("reason", e.getMessage());
      response.put("error-id", st.getId());
      content = objectMapper.writeValueAsString(response);
    } catch (final JsonProcessingException exception) {
      logger.error(exception, exception);
    }
    return new ResponseEntity<>(content, httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  private ResponseEntity<Object> createErrorResponse(final String errorMessage,
                                                     final String error,
                                                     final HttpHeaders httpHeaders,
                                                     final HttpStatus status) {

    String content = errorMessage;
    try {
      final Map<String, String> response = new HashMap<>();
      response.put("error", errorMessage);
      response.put("reason", error);
      content = objectMapper.writeValueAsString(response);
    } catch (final JsonProcessingException exception) {
      logger.error(exception, exception);
    }
    return new ResponseEntity<>(content, httpHeaders, status);
  }

}
