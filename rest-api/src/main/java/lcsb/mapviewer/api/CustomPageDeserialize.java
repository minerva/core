package lcsb.mapviewer.api;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.data.domain.PageRequest;

import java.io.IOException;
import java.util.List;

public class CustomPageDeserialize<T> extends JsonDeserializer<CustomPage<T>> {

  @Override
  public CustomPage<T> deserialize(final JsonParser parser, final DeserializationContext context) throws IOException {
    ObjectCodec codec = parser.getCodec();
    JsonNode node = codec.readTree(parser);
    List<T> elements = node.get("data").traverse(codec).readValueAs(new TypeReference<List<T>>() {
    });
    int total = node.get("totalSize").asInt();
    int filtered = node.get("filteredSize").asInt();
    int length = node.get("length").asInt();
    int page = node.get("page").asInt();

    return new CustomPage<>(
        elements,
        PageRequest.of(page, length),
        total,
        filtered);
  }

}
