package lcsb.mapviewer.api;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * This class is a configuration for spring that disables content type
 * recognition based on the "extension" (last part after dot in url). It allows
 * API to return json for urls like: http://localhost:8080/minerva/api/users/t.t
 * <p>
 * More info can be found here:
 * <ul>
 * <li>https://spring.io/blog/2013/05/11/content-negotiation-using-spring-mvc
 * </li>
 * <li>https://stackoverflow.com/questions/30793717/spring-throwing-
 * httpmediatypenotacceptableexception-could-not-find-acceptable-r</li>
 * </ul>
 * </p>
 * 
 * @author Piotr Gawron
 *
 */
@Configuration
@EnableWebMvc
public class ContentNegotiationConfig implements WebMvcConfigurer {
  @Override
  public void configureContentNegotiation(final ContentNegotiationConfigurer configurer) {
    // Turn off suffix-based content negotiation
    configurer.favorPathExtension(false);
  }
}