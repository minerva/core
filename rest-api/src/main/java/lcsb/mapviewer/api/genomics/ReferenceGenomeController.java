package lcsb.mapviewer.api.genomics;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.annotation.services.genome.ReferenceGenomeConnectorException;
import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.ReferenceGenome;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeGeneMapping;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.services.ObjectExistsException;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IReferenceGenomeService;
import lcsb.mapviewer.services.utils.ReferenceGenomeExistsException;

@RestController
@RequestMapping(
    value = {
        "/minerva/api/genomics"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class ReferenceGenomeController extends BaseController {

  private IReferenceGenomeService referenceGenomeService;

  @Autowired
  public ReferenceGenomeController(final IReferenceGenomeService referenceGenomeService) {
    this.referenceGenomeService = referenceGenomeService;
  }

  @GetMapping(value = "/taxonomies/{organismId}/genomeTypes/{type}/versions/{version}/")
  public ReferenceGenome getGenomesByQuery(
      final @NotEmpty @PathVariable(value = "organismId") String organismId,
      final @NotNull @PathVariable(value = "type") ReferenceGenomeType type,
      final @PathVariable(value = "version") String version) throws QueryException, ObjectNotFoundException {
    MiriamData organism = new MiriamData(MiriamType.TAXONOMY, organismId);

    ReferenceGenome genome = referenceGenomeService.getReferenceGenomeViewByParams(organism, type, version.replaceAll("\\*", ""));
    if (genome == null) {
      throw new ObjectNotFoundException("Cannot find requested reference genome");
    }
    return genome;
  }

  static class RemoteUrl {
    private String url;

    public String getUrl() {
      return url;
    }
  }

  @GetMapping(value = "/taxonomies/{organismId}/genomeTypes/{type}/versions/{version}:getAvailableRemoteUrls")
  public List<RemoteUrl> getRemoteUrls(
      final @NotEmpty @PathVariable(value = "organismId") String organismId,
      final @NotNull @PathVariable(value = "type") ReferenceGenomeType genomeType,
      final @PathVariable(value = "version") String version) throws QueryException, IOException {
    MiriamData organism = new MiriamData(MiriamType.TAXONOMY, organismId);
    String url = referenceGenomeService.getUrlForGenomeVersion(genomeType, organism, version);

    List<RemoteUrl> result = new ArrayList<>();
    if (url != null) {
      RemoteUrl row = new RemoteUrl();
      row.url = url;
      result.add(row);
    }
    return result;
  }

  @GetMapping(value = "/taxonomies/")
  public Set<MiriamData> getGenomeTaxonomies() throws QueryException {
    try {
      Set<MiriamData> organisms = new LinkedHashSet<>();
      for (final ReferenceGenomeType type : ReferenceGenomeType.values()) {
        organisms.addAll(referenceGenomeService.getOrganismsByReferenceGenomeType(type));
      }
      return organisms;
    } catch (final ReferenceGenomeConnectorException e) {
      throw new QueryException("Problem with obtaining organism list", e);
    }
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @PostMapping(value = "/")
  public Object addGenome(
      final @NotEmpty(message = "organismId cannot be empty") @RequestParam(value = "organismId") String organismId,
      final @NotNull @RequestParam(value = "type") ReferenceGenomeType type,
      final @NotEmpty @RequestParam(value = "version") String version,
      final @NotEmpty @RequestParam(value = "url") String url)
      throws QueryException, IOException, ReferenceGenomeConnectorException, ObjectExistsException {
    MiriamData organism = new MiriamData(MiriamType.TAXONOMY, organismId);
    try {
      referenceGenomeService.addReferenceGenome(type, organism, version, url);
      return new HashMap<>();
    } catch (final ReferenceGenomeExistsException e) {
      throw new ObjectExistsException(e);
    } catch (final URISyntaxException e) {
      throw new QueryException("Problem wih given uri", e);
    }
  }

  @GetMapping(value = "/")
  public List<ReferenceGenome> getDownloaded() {
    return referenceGenomeService.getDownloadedGenomes();
  }

  static class OrganismType {
    private ReferenceGenomeType type;

    public ReferenceGenomeType getType() {
      return type;
    }
  }

  @GetMapping(value = "/taxonomies/{organismId}/genomeTypes/")
  public List<OrganismType> getGenomeTaxonomyTypes(final @PathVariable(value = "organismId") String organism) throws QueryException {
    try {
      List<OrganismType> result = new ArrayList<>();
      for (final ReferenceGenomeType type : ReferenceGenomeType.values()) {
        List<MiriamData> organisms = referenceGenomeService.getOrganismsByReferenceGenomeType(type);
        for (final MiriamData miriamData : organisms) {
          if (miriamData.getResource().equals(organism)) {
            OrganismType organismType = new OrganismType();
            organismType.type = type;
            result.add(organismType);
          }
        }
      }
      return result;
    } catch (final ReferenceGenomeConnectorException e) {
      throw new QueryException("Problem with obtaining organism list", e);
    }
  }

  static class OrganismTypeVersion {
    private String version;

    public String getVersion() {
      return version;
    }

    public void setVersion(final String version) {
      this.version = version;
    }
  }

  @GetMapping(value = "/taxonomies/{organismId}/genomeTypes/{type}/versions/")
  public List<OrganismTypeVersion> getGenomeVersion(
      final @NotEmpty @PathVariable(value = "organismId") String organismId,
      final @NotNull @PathVariable(value = "type") ReferenceGenomeType genomeType) throws QueryException {
    MiriamData organism = new MiriamData(MiriamType.TAXONOMY, organismId);
    try {
      List<OrganismTypeVersion> result = new ArrayList<>();
      List<String> versions = referenceGenomeService.getAvailableGenomeVersions(genomeType, organism);
      for (final String string : versions) {
        OrganismTypeVersion entry = new OrganismTypeVersion();
        entry.setVersion(string);
        result.add(entry);
      }
      return result;
    } catch (final ReferenceGenomeConnectorException e) {
      throw new QueryException("Problem with obtaining version list", e);
    }
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @DeleteMapping(value = "/{genomeId}/")
  public Object removeGenome(final @NotNull @PathVariable(value = "genomeId") Integer genomeId)
      throws IOException, QueryException, ObjectNotFoundException {
    ReferenceGenome genome = referenceGenomeService.getReferenceGenomeById(genomeId);
    if (genome == null) {
      throw new ObjectNotFoundException("Object does not exist");
    }
    referenceGenomeService.removeGenome(genome);
    return new HashMap<>();
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @DeleteMapping(value = "/{genomeId}/geneMapping/{mappingId}/")
  public Map<String, Object> removeGeneMapping(
      final @NotNull @PathVariable(value = "genomeId") Integer genomeId,
      final @NotNull @PathVariable(value = "mappingId") Integer mappingId)
      throws IOException, QueryException, ObjectNotFoundException {
    ReferenceGenome genome = referenceGenomeService.getReferenceGenomeById(genomeId);
    if (genome == null) {
      throw new ObjectNotFoundException("Genome doesn't exist");
    }
    ReferenceGenomeGeneMapping geneMapping = null;
    for (final ReferenceGenomeGeneMapping mapping : genome.getGeneMapping()) {
      if (mapping.getId() == mappingId) {
        geneMapping = mapping;
      }
    }
    if (geneMapping == null) {
      throw new ObjectNotFoundException("Gene mapping doesn't exist");
    }
    referenceGenomeService.removeReferenceGenomeGeneMapping(geneMapping);
    return new HashMap<>();
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @PostMapping(value = "/{genomeId}/geneMapping/", consumes = { MediaType.APPLICATION_FORM_URLENCODED_VALUE })
  public Map<String, Object> addGeneMapping(final @PathVariable(value = "genomeId") Integer id,
      final @RequestParam String name,
      final @RequestParam String url)
      throws QueryException, IOException, ReferenceGenomeConnectorException {

    try {
      ReferenceGenome genome = referenceGenomeService.getReferenceGenomeById(id);
      if (!url.toLowerCase().endsWith("bb")) {
        throw new QueryException("Only big bed format files are supported but found: \"" + url + "\".");
      }
      if (url.startsWith("http")) {
        try {
          HttpURLConnection huc = (HttpURLConnection) new URL(url).openConnection();
          huc.setRequestMethod("HEAD");
          HttpStatus status = HttpStatus.valueOf(huc.getResponseCode());
          if (status != HttpStatus.OK) {
            throw new QueryException("Invalid url. Server returned: " + status.name() + " (" + status.value() + ").");
          }
        } catch (IOException e) {
          throw new QueryException("Problem with processing url.", e);
        }

      }

      referenceGenomeService.addReferenceGenomeGeneMapping(genome, name, url);
      return new HashMap<>();
    } catch (final URISyntaxException e) {
      throw new QueryException("Problem wih given uri", e);
    }
  }

}