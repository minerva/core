package lcsb.mapviewer.api;

public class LayoutChangeNotSupportedException extends QueryException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public LayoutChangeNotSupportedException(final String fieldName, final Object newValue, final Object oldValue) {
    super("Cannot change value for field: " + fieldName + ". Layout changes are not supported");
  }

}
