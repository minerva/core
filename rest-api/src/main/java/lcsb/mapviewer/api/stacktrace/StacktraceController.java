package lcsb.mapviewer.api.stacktrace;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.model.Stacktrace;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IStacktraceService;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/api/stacktrace",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class StacktraceController extends BaseController {

  private IStacktraceService stacktraceService;

  @Autowired
  public StacktraceController(final IStacktraceService stacktraceService) {
    this.stacktraceService = stacktraceService;
  }

  @GetMapping(value = "/{stacktraceId}")
  public Stacktrace getProject(final @PathVariable(value = "stacktraceId") String stacktraceId)
      throws ObjectNotFoundException {
    Stacktrace st = stacktraceService.getById(stacktraceId);
    if (st == null) {
      throw new ObjectNotFoundException("Object does not exist");
    }
    return st;
  }

}