package lcsb.mapviewer.api.convert;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.SBMLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.commands.CopyCommand;
import lcsb.mapviewer.commands.CreateHierarchyCommand;
import lcsb.mapviewer.commands.MergeCommand;
import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.converter.graphics.ImageGenerators;
import lcsb.mapviewer.model.cache.FileEntry;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.modelutils.map.LogFormatter;

@RestController
@RequestMapping(
    value = {
        "/minerva/api/convert"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class ConvertController extends BaseController {

  private Logger logger = LogManager.getLogger();

  private List<Converter> modelConverters;

  @Autowired
  public ConvertController(final List<Converter> modelConverters) {
    this.modelConverters = modelConverters;
  }

  @PostMapping(value = "/{fromFormat}:{toFormat:.+}",
      consumes = {
          MediaType.APPLICATION_XML_VALUE,
          MediaType.APPLICATION_OCTET_STREAM_VALUE,
          MediaType.TEXT_PLAIN_VALUE
      },
      produces = MediaType.APPLICATION_XML_VALUE)
  public byte[] convertInput(final @PathVariable(value = "fromFormat") String fromFormat,
      final @PathVariable(value = "toFormat") String toFormat,
      final @RequestBody byte[] body) throws IOException, QueryException, SBMLException,
      InvalidInputDataExecption, InconsistentModelException, ConverterException {

    ConverterParams params = new ConverterParams().inputStream(new ByteArrayInputStream(body));
    MinervaLoggerAppender appender = MinervaLoggerAppender.createAppender();
    Model original;
    try {
      original = getModelParserByNameOrClass(fromFormat).createModel(params);
      StringBuilder notes = new StringBuilder(original.getNotes());
      if (!appender.getWarnings().isEmpty()) {
        for (final String entry : new LogFormatter().createFormattedWarnings(appender.getWarnings())) {
          notes.append("\n" + entry);
        }
      }
      original.setNotes(notes.toString());
    } catch (final InvalidInputDataExecption | ConverterException e) {
      throw new QueryException("Problem with input file", e);
    } finally {
      MinervaLoggerAppender.unregisterLogEventStorage(appender);
    }
    Model model = getModelWithPathwayAndCompartment(original);

    Converter exporter = getModelParserByNameOrClass(toFormat);
    return IOUtils.toByteArray(exporter.model2InputStream(model));
  }

  private Model getModelWithPathwayAndCompartment(final Model original) {
    Model model = new CopyCommand(original).execute();
    try {
      new CreateHierarchyCommand(original, 10, 1024).execute();
      for (Layer layer : original.getLayers()) {
        layer.setVisible(false);
      }
    } catch (Exception e) {
      logger.error("Problem with creating pathways", e);
      model = original;
    }
    return model;
  }

  @PostMapping(value = "/image/{fromFormat}:{toFormat:.+}",
      consumes = {
          MediaType.APPLICATION_XML_VALUE,
          MediaType.APPLICATION_OCTET_STREAM_VALUE,
          MediaType.TEXT_PLAIN_VALUE
      })
  public @ResponseBody ResponseEntity<byte[]> convertInputToImage(
      final @PathVariable(value = "fromFormat") String fromFormat,
      final @PathVariable(value = "toFormat") String toFormat,
      final @RequestBody byte[] body) throws QueryException, IOException, DrawingException {
    Model original = null;
    try {
      original = getModelParserByNameOrClass(fromFormat).createModel(new ConverterParams().inputStream(new ByteArrayInputStream(body)));
    } catch (final InvalidInputDataExecption | ConverterException e) {
      throw new QueryException("Problem with input file", e);
    }

    Model model = getModelWithPathwayAndCompartment(original);

    AbstractImageGenerator generator = getImageGenerator(toFormat, createImageParams(model));
    ByteArrayOutputStream os = new ByteArrayOutputStream();
    generator.saveToOutputStream(os);
    return ResponseEntity.ok().contentLength(os.size())
        .contentType(MediaType.APPLICATION_OCTET_STREAM)
        .header("Content-Disposition", "attachment; filename=model" + toFormat)
        .body(os.toByteArray());
  }

  private AbstractImageGenerator getImageGenerator(final String extOrClass, final AbstractImageGenerator.Params params)
      throws QueryException {
    for (Pair<String, Class<? extends AbstractImageGenerator>> element : new ImageGenerators()
        .getAvailableImageGenerators()) {

      try {
        Class<? extends AbstractImageGenerator> clazz = element.getRight();
        Constructor<?> ctor = clazz.getConstructor(AbstractImageGenerator.Params.class);
        AbstractImageGenerator generator = (AbstractImageGenerator) ctor.newInstance(params);
        if (extOrClass.equals(clazz.getCanonicalName()) || extOrClass.equalsIgnoreCase(generator.getFileExtension())) {
          return generator;
        }

      } catch (final NoSuchMethodException | java.lang.SecurityException | InstantiationException
          | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
        logger.error("Creation of image generator class for '" + element.getLeft() + "' failed.");
        throw new QueryException("Issue with obtaining image generator for extension " + extOrClass + ".");
      }

    }

    throw new QueryException("Image generator for extension " + extOrClass + " not available.");
  }

  private AbstractImageGenerator.Params createImageParams(final Model model) {
    double padding = 5.0;
    double maxDim = 10000.0;

    double w = model.getWidth();
    double h = model.getHeight();

    double targetHeight = Math.min(h, maxDim);
    double targetWidth = Math.min(w, maxDim);

    double scale = targetWidth / w;
    if (h * scale > targetHeight) {
      scale = targetHeight / h;
    }

    double widthScaled = w * scale;
    double heightScaled = h * scale;

    return new AbstractImageGenerator.Params().model(model).width(widthScaled + padding).height(heightScaled + padding)
        .scale(1 / scale)
        .x(0);

  }

  @PostMapping(value = "/merge/{fromFormat}:{toFormat}",
      consumes = {
          MediaType.APPLICATION_OCTET_STREAM_VALUE,
          MediaType.TEXT_PLAIN_VALUE
      },
      produces = {
          MediaType.APPLICATION_OCTET_STREAM_VALUE,
          MediaType.APPLICATION_XML_VALUE,
          "application/zip" })
  public @ResponseBody ResponseEntity<byte[]> mergeFilesToFile(
      final @PathVariable(value = "fromFormat") String fromFormat,
      final @PathVariable(value = "toFormat") String toFormat,
      final @RequestHeader(value = "Accept", required = false) String accept,
      @RequestHeader(value = "X-MINERVA-KEEP-NOTES", defaultValue = "false") final String keepNotesString,
      final @RequestBody byte[] body)
      throws Exception {

    boolean keepNotes = keepNotesString.equalsIgnoreCase("true");

    try {
      ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(body));
      ZipEntry zipEntry = zis.getNextEntry();
      List<Pair<String, Model>> modelsWithFilename = new ArrayList<>();
      while (zipEntry != null) {
        String filename = zipEntry.getName();
        if (!zipEntry.isDirectory() && filename.toLowerCase().startsWith("maps/")) {

          Model model = getModelParserByNameOrClass(fromFormat)
              .createModel(new ConverterParams().inputStream(convertZipInputStreamToInputStream(zis, zipEntry, "UTF-8"),
                  filename));
          if (!keepNotes) {
            for (BioEntity bioEntity : model.getBioEntities()) {
              bioEntity.setNotes("");
            }
          }
          modelsWithFilename.add(new Pair<>(filename, model));
        }
        zipEntry = zis.getNextEntry();
      }
      zis.closeEntry();
      zis.close();

      modelsWithFilename.sort(new Comparator<Pair<String, Model>>() {

        @Override
        public int compare(final Pair<String, Model> o1, final Pair<String, Model> o2) {
          return o1.getLeft().compareTo(o2.getLeft());
        }
      });
      List<Model> models = new ArrayList<>();
      for (Pair<String, Model> pair : modelsWithFilename) {
        models.add(pair.getRight());
      }
      Model result = new MergeCommand(models).execute();

      Converter exporter = getModelParserByNameOrClass(toFormat);
      byte[] bytea = IOUtils.toByteArray(exporter.model2InputStream(result));

      if (isMimeTypeIncluded(accept, MimeType.ZIP.getTextRepresentation())) {
        FileEntry file = new UploadedFileEntry();
        file.setOriginalFileName("merged." + exporter.getFileExtensions().get(0));
        file.setFileContent(bytea);
        return sendAsZip(file);
      } else {
        return ResponseEntity.ok().contentLength(bytea.length)
            .contentType(MediaType.APPLICATION_OCTET_STREAM)
            .header("Content-Disposition", "attachment; filename=model" + toFormat)
            .body(bytea);
      }
    } catch (final InvalidInputDataExecption | ConverterException | InconsistentModelException e) {
      throw new QueryException("Input file is invalid", e);
    }
  }

  private InputStream convertZipInputStreamToInputStream(final ZipInputStream in, final ZipEntry entry, final String encoding)
      throws IOException {
    final int BUFFER = 2048;
    int count = 0;
    byte[] data = new byte[BUFFER];
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    while ((count = in.read(data)) != -1) {
      out.write(data, 0, count);
    }
    return new ByteArrayInputStream(out.toByteArray());
  }

  @RequestMapping(value = "/", method = { RequestMethod.GET, RequestMethod.POST })
  public ConverterInformation getInformation() {
    ConverterInformation result = new ConverterInformation();
    result.setInputs(modelConverters);
    result.setOutputs(modelConverters);
    return result;
  }

  @RequestMapping(value = "/image/", method = { RequestMethod.GET, RequestMethod.POST })
  public GraphicsConverterInformation getInformationImage() {

    GraphicsConverterInformation result = new GraphicsConverterInformation();
    result.setInputs(modelConverters);
    result.setOutputs(new ArrayList<>());

    ImageGenerators igs = new ImageGenerators();
    for (Pair<String, Class<? extends AbstractImageGenerator>> generator : igs.getAvailableImageGenerators()) {
      result.getOutputs().add(generator.getRight());
    }

    return result;
  }

  private Converter getModelParserByNameOrClass(final String id) throws QueryException {
    try {
      return getModelParserByName(id);
    } catch (final QueryException e) {
      return getModelParser(id);
    }
  }

  private Converter getModelParserByName(final String name) throws QueryException {
    for (final Converter converter : modelConverters) {
      if (removeWhiteSpaces(converter.getCommonName()).equalsIgnoreCase(name)) {
        try {
          return converter.getClass().getConstructor().newInstance();
        } catch (final InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
            | SecurityException e) {
          throw new InvalidStateException(e);
        }
      }
    }
    throw new QueryException("Unknown parser name: " + name);
  }

  private String removeWhiteSpaces(final String str) {
    return str.replace(' ', '_');
  }

  protected Converter getModelParser(final String handlerClass) throws QueryException {
    for (final Converter converter : modelConverters) {
      if (converter.getClass().getCanonicalName().equalsIgnoreCase(handlerClass)) {
        try {
          return (Converter) Class.forName(handlerClass).getConstructor().newInstance();
        } catch (final InstantiationException | IllegalAccessException | ClassNotFoundException | IllegalArgumentException | InvocationTargetException
            | NoSuchMethodException | SecurityException e) {
          throw new QueryException("Problem with handler:" + handlerClass);
        }
      }
    }
    throw new QueryException("Unknown handlerClass: " + handlerClass);
  }

}