package lcsb.mapviewer.api.convert;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator;

public class GraphicsConverterInformation {
  @JsonSerialize(contentUsing = ConverterSerializer.class)
  private List<Converter> inputs;

  @JsonSerialize(contentUsing = AbstractImageGeneratorSerializer.class)
  private List<Class<? extends AbstractImageGenerator>> outputs;

  public List<Converter> getInputs() {
    return inputs;
  }

  public void setInputs(final List<Converter> inputs) {
    this.inputs = inputs;
  }

  public List<Class<? extends AbstractImageGenerator>> getOutputs() {
    return outputs;
  }

  public void setOutputs(final List<Class<? extends AbstractImageGenerator>> outputs) {
    this.outputs = outputs;
  }
}
