package lcsb.mapviewer.api.convert;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lcsb.mapviewer.converter.Converter;

public class ConverterInformation {
  @JsonSerialize(contentUsing = ConverterSerializer.class)
  private List<Converter> inputs;

  @JsonSerialize(contentUsing = ConverterSerializer.class)
  private List<Converter> outputs;

  public List<Converter> getInputs() {
    return inputs;
  }

  public void setInputs(final List<Converter> inputs) {
    this.inputs = inputs;
  }

  public List<Converter> getOutputs() {
    return outputs;
  }

  public void setOutputs(final List<Converter> outputs) {
    this.outputs = outputs;
  }
}
