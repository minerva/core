package lcsb.mapviewer.api.convert;

import java.io.IOException;
import java.util.Arrays;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.converter.graphics.AbstractImageGenerator;
import lcsb.mapviewer.converter.graphics.ImageGenerators;

public class AbstractImageGeneratorSerializer extends JsonSerializer<Class<? extends AbstractImageGenerator>> {

  private static final ImageGenerators imageGenerators = new ImageGenerators();

  @Override
  public void serialize(final Class<? extends AbstractImageGenerator> clazz, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeObjectField("available_names", Arrays.asList(imageGenerators.getExtension(clazz), clazz.getCanonicalName()));

    gen.writeEndObject();
  }
}