package lcsb.mapviewer.api.convert;

import java.io.IOException;
import java.util.Arrays;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.converter.Converter;

public class ConverterSerializer extends JsonSerializer<Converter> {

  @Override
  public void serialize(final Converter converter, final JsonGenerator gen, final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeObjectField("available_names",
        Arrays.asList(converter.getCommonName().replace(' ', '_'), converter.getClass().getCanonicalName()));

    gen.writeEndObject();
  }
}