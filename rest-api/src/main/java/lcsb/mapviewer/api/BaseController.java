package lcsb.mapviewer.api;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.model.cache.FileEntry;

public abstract class BaseController {
  protected static Logger logger = LogManager.getLogger();

  protected ResponseEntity<byte[]> sendAsZip(final FileEntry originalFile) throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ZipOutputStream zos = new ZipOutputStream(baos);

    ZipEntry entry = new ZipEntry(originalFile.getOriginalFileName());

    zos.putNextEntry(entry);
    zos.write(originalFile.getFileContent());
    zos.closeEntry();
    zos.close();

    return ResponseEntity.ok().contentLength(baos.size())
        .contentType(MediaType.parseMediaType(MimeType.ZIP.getTextRepresentation()))
        .header("Content-Disposition", "attachment; filename=" + originalFile.getOriginalFileName() + ".zip")
        .body(baos.toByteArray());
  }

  protected Map<String, Object> okStatus() {
    Map<String, Object> result = new HashMap<>();
    result.put("status", "OK");
    return result;
  }

  protected boolean isMimeTypeIncluded(final String accept, final String mimeType) {
    List<MediaType> mediaTypes = MediaType.parseMediaTypes(accept);
    for (final MediaType mediaType : mediaTypes) {
      if (Objects.equals(mediaType.getType() + "/" + mediaType.getSubtype(), mimeType)) {
        return true;
      }
    }
    return false;
  }

}
