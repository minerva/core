package lcsb.mapviewer.api.plugins;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.plugin.Plugin;
import lcsb.mapviewer.model.plugin.PluginDataEntry;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IPluginService;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/api/plugins"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class PluginController extends BaseController {

  private final IUserService userService;
  private final IPluginService pluginService;

  @Autowired
  public PluginController(final IUserService userService, final IPluginService pluginService) {
    this.userService = userService;
    this.pluginService = pluginService;
  }

  @PreAuthorize("(not #isPublic and #isDefault!=true) or hasAuthority('IS_ADMIN')")
  @PostMapping(value = "/")
  public Plugin createPlugin(
      final @NotEmpty @RequestParam(value = "hash") String hash,
      final @RequestParam(value = "name") String name,
      final @RequestParam(value = "version") String version,
      final @RequestParam(value = "isPublic", defaultValue = "false") boolean isPublic,
      final @RequestParam(value = "isDefault", required = false) Boolean isDefault,
      final @URL @RequestParam(value = "url", defaultValue = "") String url) throws QueryException {
    Plugin plugin = pluginService.getByHash(hash);
    if (plugin != null) {
      plugin.addUrl(url);
      plugin.setPublic(plugin.isPublic() || isPublic);
      if (isDefault != null) {
        plugin.setDefault(isDefault);
      }
      pluginService.update(plugin);
    } else {
      plugin = new Plugin();
      plugin.setHash(hash);
      plugin.setName(name);
      plugin.setVersion(version);
      plugin.setPublic(isPublic);
      if (isDefault != null) {
        plugin.setDefault(isDefault);
      }
      if (!url.isEmpty()) {
        plugin.addUrl(url);
        if (!url.startsWith("http://localhost")) {
          String md5;
          try {
            md5 = pluginService.getExpectedHash(url);
          } catch (Exception e) {
            throw new QueryException("Problem with fetching url", e);
          }
          if (!Objects.equals(md5, hash)) {
            throw new QueryException(String.format("Invalid hash. Expected %s, but %s found", md5, hash));
          }
        }
      }
      pluginService.add(plugin);
    }
    return plugin;
  }

  static class UpdatePluginData {
    private Boolean isPublic;
    private Boolean isDefault;
    private String version;
    private String name;
    private String hash;
    private Integer id;
    private List<String> urls = new ArrayList<>();

    public Boolean getIsPublic() {
      return isPublic;
    }

    public void setIsPublic(final Boolean isPublic) {
      this.isPublic = isPublic;
    }

    public Boolean getIsDefault() {
      return isDefault;
    }

    public void setIsDefault(final Boolean isDefault) {
      this.isDefault = isDefault;
    }

    public String getVersion() {
      return version;
    }

    public void setVersion(final String version) {
      this.version = version;
    }

    public String getName() {
      return name;
    }

    public void setName(final String name) {
      this.name = name;
    }

    public String getHash() {
      return hash;
    }

    public void setHash(final String hash) {
      this.hash = hash;
    }

    public Integer getId() {
      return id;
    }

    public void setId(final Integer id) {
      this.id = id;
    }

    public List<String> getUrls() {
      return urls;
    }

    public void setUrls(final List<String> urls) {
      this.urls = urls;
    }
  }

  static class UpdateData {
    @NotNull
    private UpdatePluginData plugin;

    public UpdatePluginData getPlugin() {
      return plugin;
    }

    public void setPlugin(final UpdatePluginData plugin) {
      this.plugin = plugin;
    }
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @PatchMapping(value = "/{hash}")
  public Plugin updatePlugin(
      final @PathVariable(value = "hash") String hash,
      final @Valid @RequestBody UpdateData body) throws QueryException, ObjectNotFoundException {
    UpdatePluginData data = body.getPlugin();
    Plugin plugin = pluginService.getByHash(hash);
    if (plugin == null) {
      throw new ObjectNotFoundException("Plugin doesn't exist");
    }

    if (data.getIsPublic() != null) {
      plugin.setPublic(data.getIsPublic());
    }
    if (data.getIsDefault() != null) {
      plugin.setDefault(data.getIsDefault());
    }
    if (data.getName() != null) {
      plugin.setName(data.getName());
    }
    if (data.getVersion() != null) {
      plugin.setVersion(data.getVersion());
    }
    if (data.getHash() != null) {
      plugin.setHash(data.getHash());
    }
    if (data.getId() != null) {
      if (data.getId() != plugin.getId() && data.getId() != 0) {
        throw new QueryException("plugin id cannot be changed");
      }
    }
    if (!data.getUrls().isEmpty()) {
      plugin.getUrls().clear();
      for (final String string : data.getUrls()) {
        plugin.getUrls().add(string);
      }
    }
    pluginService.update(plugin);
    return pluginService.getByHash(plugin.getHash());
  }

  @GetMapping(value = "/")
  public List<Plugin> getPlugins(
      final @RequestParam(value = "onlyPublic", defaultValue = "false") String onlyPublic) {
    boolean onlyPublicBool = onlyPublic.equalsIgnoreCase("true");
    List<Plugin> plugins = pluginService.getAll();
    plugins.sort(Plugin.ID_COMPARATOR);

    List<Plugin> result = new ArrayList<>();
    for (final Plugin plugin : plugins) {
      if (!onlyPublicBool || plugin.isPublic()) {
        result.add(plugin);
      }
    }
    return result;

  }

  @GetMapping(value = "/{hash}")
  public Plugin getPlugin(final @PathVariable(value = "hash") String hash) throws ObjectNotFoundException {
    Plugin plugin = pluginService.getByHash(hash);
    if (plugin == null) {
      throw new ObjectNotFoundException("Plugin doesn't exist");
    }
    return plugin;
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @DeleteMapping(value = "/{hash}")
  public void removePlugin(final @PathVariable(value = "hash") String hash) throws ObjectNotFoundException {
    Plugin plugin = pluginService.getByHash(hash);
    if (plugin != null) {
      pluginService.delete(plugin);
    } else {
      throw new ObjectNotFoundException("Plugin doesn't exist");
    }
  }

  @PostMapping(value = "/{hash}/data/users/{key}")
  public PluginDataEntry createPluginDataEntry(
      final Authentication authentication,
      final @PathVariable(value = "hash") String hash,
      final @PathVariable(value = "key") String key,
      final @RequestParam(value = "value", defaultValue = "") String value) throws QueryException, ObjectNotFoundException {
    User user = userService.getUserByLogin(authentication.getName());
    return createPluginDataEntry(hash, user, key, value);
  }

  @PostMapping(value = "/{hash}/data/global/{key}")
  public PluginDataEntry createPluginDataEntry(
      final @PathVariable(value = "hash") String hash,
      final @PathVariable(value = "key") String key,
      final @RequestParam(value = "value", defaultValue = "") String value) throws QueryException, ObjectNotFoundException {
    return createPluginDataEntry(hash, null, key, value);
  }

  private PluginDataEntry createPluginDataEntry(final String hash, final User user, final String key, final String value)
      throws QueryException, ObjectNotFoundException {
    Plugin plugin = pluginService.getByHash(hash);
    if (plugin == null) {
      throw new ObjectNotFoundException("Plugin doesn't exist");
    }
    int length = 0;
    if (value != null) {
      length = value.getBytes(StandardCharsets.UTF_8).length;
    }
    if (length >= 1024 * 1024) {
      throw new QueryException("Data entry value too big (" + length + "; max length = " + 1024 * 1024 + ")");
    }
    PluginDataEntry entry = pluginService.getEntryByKey(plugin, key, user);
    if (entry == null) {
      entry = new PluginDataEntry();
      entry.setKey(key);
      entry.setPlugin(plugin);
      entry.setUser(user);
      entry.setValue(value);
      pluginService.add(entry);
    } else {
      entry.setValue(value);
      pluginService.update(entry);
    }

    return entry;
  }

  @DeleteMapping(value = "/{hash}/data/users/{key}")
  public Object deletePluginDataEntry(
      final Authentication authentication,
      final @PathVariable(value = "hash") String hash,
      final @PathVariable(value = "key") String key) throws QueryException, ObjectNotFoundException {
    User user = userService.getUserByLogin(authentication.getName());
    return deletePluginDataEntry(hash, user, key);
  }

  @DeleteMapping(value = "/{hash}/data/global/{key}")
  public Object deletePluginDataEntry(
      final @PathVariable(value = "hash") String hash,
      final @PathVariable(value = "key") String key) throws QueryException, ObjectNotFoundException {
    return deletePluginDataEntry(hash, null, key);
  }

  private Object deletePluginDataEntry(final String hash, final User user, final String key) throws ObjectNotFoundException {
    Plugin plugin = pluginService.getByHash(hash);
    if (plugin == null) {
      throw new ObjectNotFoundException("Plugin doesn't exist");
    }

    PluginDataEntry entry = pluginService.getEntryByKey(plugin, key, user);
    if (entry == null) {
      throw new ObjectNotFoundException("Data entry not found");
    } else {
      pluginService.delete(entry);
    }
    return okStatus();
  }

  @GetMapping(value = "/{hash}/data/users/{key}")
  public PluginDataEntry getPluginDataEntry(
      final Authentication authentication,
      final @PathVariable(value = "hash") String hash,
      final @PathVariable(value = "key") String key) throws ObjectNotFoundException {
    User user = userService.getUserByLogin(authentication.getName());
    return getPluginDataEntry(hash, user, key);
  }

  @GetMapping(value = "/{hash}/data/global/{key}")
  public PluginDataEntry getPluginDataEntry(
      final @PathVariable(value = "hash") String hash,
      final @PathVariable(value = "key") String key) throws ObjectNotFoundException {
    return getPluginDataEntry(hash, null, key);
  }

  private PluginDataEntry getPluginDataEntry(final String hash, final User user, final String key) throws ObjectNotFoundException {
    Plugin plugin = pluginService.getByHash(hash);
    if (plugin == null) {
      throw new ObjectNotFoundException("Plugin doesn't exist");
    }
    PluginDataEntry entry = pluginService.getEntryByKey(plugin, key, user);
    if (entry == null) {
      throw new ObjectNotFoundException("Entry doesn't exist");
    }

    return entry;
  }
}