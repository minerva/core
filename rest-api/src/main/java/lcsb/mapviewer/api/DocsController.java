package lcsb.mapviewer.api;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.servlet.ServletContext;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IConfigurationService;

@RestController
@RequestMapping(value = "/docs", produces = MediaType.TEXT_HTML_VALUE)
public class DocsController extends BaseController {

  private Logger logger = LogManager.getLogger();

  private ServletContext context;

  private IConfigurationService configurationService;

  @Autowired
  public DocsController(final ServletContext context, final IConfigurationService configurationService) {
    this.context = context;
    this.configurationService = configurationService;
  }

  @GetMapping(value = "/{requestedPage:.+}")
  public byte[] getTaxonomy(final @PathVariable String requestedPage) throws ObjectNotFoundException, IOException {
    String page = requestedPage;
    if (page == null || page.isEmpty()) {
      page = "index.html";
    }
    File file = new File(context.getRealPath("/docs/" + page));
    if (file.exists()) {
      String content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
      String root = configurationService.getConfigurationValue(ConfigurationElementType.MINERVA_ROOT);
      if (root != null && !root.trim().isEmpty()) {
        content = content.replace("https://minerva-service.lcsb.uni.lu/minerva/", root);
      }
      logger.debug(page + " - " + root);
      return content.getBytes();
    } else {
      throw new ObjectNotFoundException("File does not exist");
    }
  }
}