package lcsb.mapviewer.api;

public class PreconditionFailedException extends QueryException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public PreconditionFailedException(final String message) {
    super(message);
  }

}
