package lcsb.mapviewer.api;

import lcsb.mapviewer.annotation.SpringAnnotationConfig;
import lcsb.mapviewer.persist.SpringPersistConfig;
import lcsb.mapviewer.services.SpringServiceConfig;
import lcsb.mapviewer.services.websockets.IWebSocketMessenger;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;

@Configuration
@Import({
    SpringRestApiConfig.class,
    SpringPersistConfig.class,
    SpringServiceConfig.class,
    SpringAnnotationConfig.class
})
public class SpringRestApiTestConfig {

  @Bean
  public SessionRegistryImpl sessionRegistry() {
    return new SessionRegistryImpl();
  }

  @Bean
  public ClientRegistrationRepository clientRegistrationRepository() {
    return Mockito.mock(ClientRegistrationRepository.class);
  }

  @Bean
  public IWebSocketMessenger webSocketMessenger() {
    return Mockito.mock(IWebSocketMessenger.class);
  }

}
