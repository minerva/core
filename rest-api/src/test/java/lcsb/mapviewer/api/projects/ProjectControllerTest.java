package lcsb.mapviewer.api.projects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lcsb.mapviewer.api.RestTestFunctions;

public class ProjectControllerTest extends RestTestFunctions {

  @Autowired
  private ProjectController projectController;

  @Test
  public void test() {
    Map<String, Object> body = new HashMap<>();
    body.put("zip-entries[0][_type]", "MAP");
    body.put("zip-entries[0][_filename]", "main.xml");
    body.put("zip-entries[0][_data][root]", "true");
    body.put("zip-entries[0][_data][name]", "main");
    body.put("zip-entries[0][_data][type][id]", "UNKNOWN");
    body.put("zip-entries[1][_type]", "MAP1");
    projectController.fixFormUrlEncodedBody(body);

    assertTrue(body.get("zip-entries") instanceof Map);

    @SuppressWarnings("unchecked")
    Map<String, Map<String, Object>> entries = (Map<String, Map<String, Object>>) body.get("zip-entries");
    assertEquals(2, entries.size());
    assertEquals(entries.get("0").get("_type"), "MAP");

    @SuppressWarnings("unchecked")
    Map<String, Object> data = (Map<String, Object>) entries.get("0").get("_data");
    assertEquals(data.get("root"), "true");
    assertEquals(data.get("name"), "main");
    assertTrue(data.get("type") instanceof Map);

    assertEquals(entries.get("1").get("_type"), "MAP1");

  }

}
