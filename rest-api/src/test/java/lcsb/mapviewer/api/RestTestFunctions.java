package lcsb.mapviewer.api;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import lcsb.mapviewer.common.tests.TestUtils;
import lcsb.mapviewer.common.tests.UnitTestFailedWatcher;
import lcsb.mapviewer.persist.DbUtils;

@Transactional
@Rollback(true)
@ContextConfiguration(classes = SpringRestApiTestConfig.class)
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class RestTestFunctions extends TestUtils {

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();

  @Autowired
  protected DbUtils dbUtils;

  private Logger logger = LogManager.getLogger();

  @Before
  public void generalSetUp() {
    dbUtils.setAutoFlush(false);
  }

  @After
  public void generatTearDown() throws IOException {
    File f = new File("map_images");
    if (f.exists()) {
      logger.info("Removing output test directory: " + f.getAbsolutePath());
      FileUtils.deleteDirectory(f);
    }

  }

}
