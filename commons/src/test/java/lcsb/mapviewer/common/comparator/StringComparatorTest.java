package lcsb.mapviewer.common.comparator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StringComparatorTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testNotNullComparison() {
    StringComparator comp = new StringComparator();
    assertTrue(comp.compare("test1", "test1") == 0);
    assertTrue(comp.compare("xxx3", "xxx3") == 0);
    assertFalse(comp.compare("xxx3", "test1") == 0);
    assertFalse(comp.compare("test1", "xxx3") == 0);
  }

  @Test
  public void testComparisonWithoutWhitespace() {
    StringComparator comp = new StringComparator();
    assertTrue(comp.compare("test1 32", "test1    32", true) == 0);
    assertTrue(comp.compare("test1\t32", "test1   \n32", true) == 0);
    assertFalse(comp.compare("test132", "test1 32", true) == 0);
  }

  @Test
  public void testNullComparison() {
    StringComparator comp = new StringComparator();
    assertTrue(comp.compare(null, null) == 0);
    assertFalse(comp.compare("xxx3", null) == 0);
    assertFalse(comp.compare(null, "xxx3") == 0);
  }

}
