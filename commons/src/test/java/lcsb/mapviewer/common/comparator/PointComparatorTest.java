package lcsb.mapviewer.common.comparator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PointComparatorTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testNotNullComparison() {
    PointComparator comp = new PointComparator();
    assertTrue(comp.compare(new Point2D.Double(106, 5.01), new Point2D.Double(106, 5.01)) == 0);
    assertTrue(comp.compare(new Point2D.Double(106, 2.73), new Point2D.Double(106, 2.73)) == 0);
    assertFalse(comp.compare(new Point2D.Double(106, 2.73), new Point2D.Double(106, 5.01)) == 0);
    assertFalse(comp.compare(new Point2D.Double(106, 5.01), new Point2D.Double(106, 2.73)) == 0);
  }

  @Test
  public void testEpsilonComp() {
    PointComparator comp = new PointComparator(10);
    assertTrue(comp.compare(new Point2D.Double(106, 5.01), new Point2D.Double(106, 1.01)) == 0);
    assertFalse(comp.compare(new Point2D.Double(106, 2.73), new Point2D.Double(106, -105.01)) == 0);
    assertTrue(comp.compare(new Point2D.Double(106, 5.01), new Point2D.Double(106, -2.73)) == 0);
  }

  @Test
  public void testNullComparison() {
    PointComparator comp = new PointComparator();
    assertTrue(comp.compare(null, null) == 0);
    assertFalse(comp.compare(new Point2D.Double(106, 2.73), null) == 0);
    assertFalse(comp.compare(null, new Point2D.Double(106, 2.73)) == 0);
  }

}
