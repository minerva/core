package lcsb.mapviewer.common.comparator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DoubleComparatorTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testNotNullComparison() {
    DoubleComparator comp = new DoubleComparator();
    assertTrue(comp.compare(new Double(5.01), new Double(5.01)) == 0);
    assertTrue(comp.compare(new Double(2.73), new Double(2.73)) == 0);
    assertFalse(comp.compare(new Double(2.73), new Double(5.01)) == 0);
    assertFalse(comp.compare(new Double(5.01), new Double(2.73)) == 0);
  }

  @Test
  public void testEpsilonComp() {
    DoubleComparator comp = new DoubleComparator(10);
    assertTrue(comp.compare(new Double(5.01), new Double(1.01)) == 0);
    assertFalse(comp.compare(new Double(2.73), new Double(-105.01)) == 0);
    assertTrue(comp.compare(new Double(5.01), new Double(-2.73)) == 0);
  }

  @Test
  public void testNullComparison() {
    DoubleComparator comp = new DoubleComparator();
    assertTrue(comp.compare(null, null) == 0);
    assertFalse(comp.compare(new Double(2.73), null) == 0);
    assertFalse(comp.compare(null, new Double(2.73)) == 0);
  }

}
