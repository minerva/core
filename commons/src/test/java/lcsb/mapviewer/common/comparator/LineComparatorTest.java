package lcsb.mapviewer.common.comparator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LineComparatorTest {

  private Point2D p1 = new Point2D.Double(106, 5.01);
  private Point2D p1close = new Point2D.Double(106, 5.11);
  private Point2D p2 = new Point2D.Double(106, 2.73);
  private Point2D p2far = new Point2D.Double(106, 202.73);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testNotNullComparison() {
    LineComparator comp = new LineComparator();
    assertTrue(comp.compare(new Line2D.Double(p1, p2), new Line2D.Double(p1, p2)) == 0);
    assertTrue(comp.compare(new Line2D.Double(p2, p1), new Line2D.Double(p2, p1)) == 0);
    assertFalse(comp.compare(new Line2D.Double(p1, p2), new Line2D.Double(p2, p1)) == 0);
    assertFalse(comp.compare(new Line2D.Double(p2, p1), new Line2D.Double(p1, p2)) == 0);
  }

  @Test
  public void testEpsilonComp() {
    LineComparator comp = new LineComparator(10);
    assertFalse(comp.compare(new Line2D.Double(p1, p2), new Line2D.Double(p1, p2far)) == 0);
    assertTrue(comp.compare(new Line2D.Double(p1, p2), new Line2D.Double(p1close, p2)) == 0);
  }

  @Test
  public void testNullComparison() {
    LineComparator comp = new LineComparator();
    assertTrue(comp.compare(null, null) == 0);
    assertFalse(comp.compare(new Line2D.Double(p1, p2), null) == 0);
    assertFalse(comp.compare(null, new Line2D.Double(p1, p2)) == 0);
  }

}
