package lcsb.mapviewer.common.comparator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BooleanComparatorTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testNotNullComparison() {
    BooleanComparator comp = new BooleanComparator();
    assertTrue(comp.compare(new Boolean(true), new Boolean(true)) == 0);
    assertTrue(comp.compare(new Boolean(false), new Boolean(false)) == 0);
    assertFalse(comp.compare(new Boolean(false), new Boolean(true)) == 0);
    assertFalse(comp.compare(new Boolean(true), new Boolean(false)) == 0);
  }

  @Test
  public void testNullComparison() {
    BooleanComparator comp = new BooleanComparator();
    assertTrue(comp.compare(null, null) == 0);
    assertFalse(comp.compare(new Boolean(false), null) == 0);
    assertFalse(comp.compare(null, new Boolean(false)) == 0);
  }

}
