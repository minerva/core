package lcsb.mapviewer.common.comparator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class IntegerComparatorTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testNotNullComparison() {
    IntegerComparator comp = new IntegerComparator();
    assertTrue(comp.compare(new Integer(13), new Integer(13)) == 0);
    assertTrue(comp.compare(new Integer(-59), new Integer(-59)) == 0);
    assertFalse(comp.compare(new Integer(-59), new Integer(13)) == 0);
    assertFalse(comp.compare(new Integer(13), new Integer(-59)) == 0);
  }

  @Test
  public void testNullComparison() {
    IntegerComparator comp = new IntegerComparator();
    assertTrue(comp.compare(null, null) == 0);
    assertFalse(comp.compare(new Integer(-59), null) == 0);
    assertFalse(comp.compare(null, new Integer(-59)) == 0);
  }
}
