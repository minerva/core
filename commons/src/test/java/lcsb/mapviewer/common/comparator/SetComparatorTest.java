package lcsb.mapviewer.common.comparator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class SetComparatorTest {

  @Test
  public void testEqual() {
    Set<Color> set1 = new HashSet<>();
    Set<Color> set2 = new HashSet<>();
    set1.add(Color.BLACK);
    set2.add(Color.BLACK);
    SetComparator<Color> comparator = new SetComparator<>(new ColorComparator());
    assertEquals(0, comparator.compare(set1, set2));
  }

  @Test
  public void testEqualWithCustomClass() {
    class Data {
      protected int x;

      Data(final int x) {
        this.x = x;
      }
    }
    
    class DataComparator implements Comparator<Data> {
      @Override
      public int compare(final Data o1, final Data o2) {
        return o1.x - o2.x;
      }
    }

    Set<Data> set1 = new HashSet<>();
    Set<Data> set2 = new HashSet<>();
    set1.add(new Data(1));
    set2.add(new Data(1));

    SetComparator<Data> comparator = new SetComparator<>(new DataComparator());
    assertEquals(0, comparator.compare(set1, set2));

    set2.add(new Data(2));
    assertTrue(0 != comparator.compare(set1, set2));
    assertTrue(0 != comparator.compare(set2, set1));
  }

}
