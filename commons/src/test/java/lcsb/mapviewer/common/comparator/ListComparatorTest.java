package lcsb.mapviewer.common.comparator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.junit.Test;

public class ListComparatorTest {

  @Test
  public void testEqual() {
    List<Color> set1 = new ArrayList<>();
    List<Color> set2 = new ArrayList<>();
    set1.add(Color.BLACK);
    set2.add(Color.BLACK);
    ListComparator<Color> comparator = new ListComparator<>(new ColorComparator());
    assertEquals(0, comparator.compare(set1, set2));
  }

  @Test
  public void testDifferent() {
    List<Color> set1 = new ArrayList<>();
    List<Color> set2 = new ArrayList<>();
    set1.add(Color.BLACK);
    set2.add(Color.BLUE);
    ListComparator<Color> comparator = new ListComparator<>(new ColorComparator());
    assertTrue(comparator.compare(set1, set2) != 0);
  }

  @Test
  public void testEqualWithCustomClass() {
    class Data {
      protected int x;

      Data(final int x) {
        this.x = x;
      }
    }
    
    class DataComparator implements Comparator<Data> {
      @Override
      public int compare(final Data o1, final Data o2) {
        return o1.x - o2.x;
      }
    }

    List<Data> set1 = new ArrayList<>();
    List<Data> set2 = new ArrayList<>();
    set1.add(new Data(1));
    set2.add(new Data(1));

    ListComparator<Data> comparator = new ListComparator<>(new DataComparator());
    assertEquals(0, comparator.compare(set1, set2));

    set2.add(new Data(2));
    assertTrue(0 != comparator.compare(set1, set2));
    assertTrue(0 != comparator.compare(set2, set1));
  }

}
