package lcsb.mapviewer.common.comparator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Color;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ColorComparatorTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testNotNullComparison() {
    ColorComparator comp = new ColorComparator();
    assertTrue(comp.compare(Color.BLACK, Color.BLACK) == 0);
    assertTrue(comp.compare(Color.RED, Color.RED) == 0);
    assertFalse(comp.compare(Color.RED, Color.BLACK) == 0);
    assertFalse(comp.compare(Color.BLACK, Color.RED) == 0);
  }

  @Test
  public void testNullComparison() {
    ColorComparator comp = new ColorComparator();
    assertTrue(comp.compare(null, null) == 0);
    assertFalse(comp.compare(Color.RED, null) == 0);
    assertFalse(comp.compare(null, Color.RED) == 0);
  }

}
