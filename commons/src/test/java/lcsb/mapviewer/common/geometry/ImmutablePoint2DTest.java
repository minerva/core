package lcsb.mapviewer.common.geometry;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Point2D;

import org.junit.Test;

public class ImmutablePoint2DTest {

  @Test
  public void testComparison() {
    Point2D p1 = new ImmutablePoint2D(10, 0);
    Point2D p2 = new ImmutablePoint2D(10, 0);
    assertEquals(p1, p2);
  }

  @Test(expected = UnsupportedOperationException.class)
  public void testImmutability() {
    Point2D p1 = new ImmutablePoint2D(10, 0);
    p1.setLocation(0, 10);
  }

}
