package lcsb.mapviewer.common.geometry;

import static org.junit.Assert.assertEquals;

import java.awt.Color;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.CommonTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;

public class ColorParserTest extends CommonTestFunctions {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidColor() throws Exception {
    ColorParser parser = new ColorParser();
    parser.parse("qwe");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidColor2() throws Exception {
    ColorParser parser = new ColorParser();
    parser.parse("fffffff");
  }

  @Test
  public void testParseColor() throws Exception {
    ColorParser parser = new ColorParser();
    Color color = parser.parse("#ffffff");
    assertEquals(Color.WHITE, color);
    assertEquals(255, color.getAlpha());
  }

  @Test
  public void testParseColorWithAlpha() throws Exception {
    ColorParser parser = new ColorParser();
    Color color = parser.parse("#ffffff80");
    assertEquals(128, color.getAlpha());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testParseNull() throws Exception {
    ColorParser parser = new ColorParser();
    parser.parse(null);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testParseEmpty() throws Exception {
    ColorParser parser = new ColorParser();
    parser.parse("");
  }

  @Test
  public void testSetColorToHtmlString() throws Exception {
    ColorParser parser = new ColorParser();

    String colorString = "#ffffff";
    Color color = parser.parse(colorString);
    assertEquals(colorString, parser.colorToHtml(color));

    colorString = "#fc00ff";
    color = parser.parse(colorString);
    assertEquals(colorString, parser.colorToHtml(color));
  }

  @Test
  public void testSerialization() throws Exception {
    ColorParser parser = new ColorParser();

    Color color = new Color(1, 2, 3, 4);
    Color color2 = parser.deserializeColor(parser.serializeColor(color));
    assertEquals(color, color2);
  }

}
