package lcsb.mapviewer.common.geometry;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Line2D;

import org.junit.Test;

import lcsb.mapviewer.common.Configuration;

public class ImmutableLine2DTest {

  @Test
  public void testGetX1() {
    Line2D line = new ImmutableLine2D(10, 20, 30, 40);
    assertEquals(line.getX1(), 10, Configuration.EPSILON);
  }

  @Test
  public void testGetY1() {
    Line2D line = new ImmutableLine2D(10, 20, 30, 40);
    assertEquals(line.getY1(), 20, Configuration.EPSILON);
  }

  @Test
  public void testGetX2() {
    Line2D line = new ImmutableLine2D(10, 20, 30, 40);
    assertEquals(line.getX2(), 30, Configuration.EPSILON);
  }

  @Test
  public void testGetY2() {
    Line2D line = new ImmutableLine2D(10, 20, 30, 40);
    assertEquals(line.getY2(), 40, Configuration.EPSILON);
  }

  @Test(expected = UnsupportedOperationException.class)
  public void testSetLine() {
    Line2D line = new ImmutableLine2D(10, 20, 30, 40);
    line.setLine(2, 3, 4, 5);
  }

  @Test
  public void testGetBounds2D() {
    Line2D line = new ImmutableLine2D(10, 20, 40, 30);
    assertEquals(line.getBounds2D().getMinX(), 10, Configuration.EPSILON);
    assertEquals(line.getBounds2D().getMinY(), 20, Configuration.EPSILON);
    assertEquals(line.getBounds2D().getMaxX(), 40, Configuration.EPSILON);
    assertEquals(line.getBounds2D().getMaxY(), 30, Configuration.EPSILON);
  }

  @Test
  public void testGetP1() {
    Line2D line = new ImmutableLine2D(10, 20, 40, 30);
    assertEquals(line.getP1().getX(), 10, Configuration.EPSILON);
    assertEquals(line.getP1().getY(), 20, Configuration.EPSILON);
  }

  @Test
  public void testGetP2() {
    Line2D line = new ImmutableLine2D(10, 20, 40, 30);
    assertEquals(line.getP2().getX(), 40, Configuration.EPSILON);
    assertEquals(line.getP2().getY(), 30, Configuration.EPSILON);
  }

}
