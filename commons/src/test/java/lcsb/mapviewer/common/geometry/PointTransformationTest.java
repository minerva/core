package lcsb.mapviewer.common.geometry;

import lcsb.mapviewer.common.Configuration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PointTransformationTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testRotate() {
    PointTransformation pt = new PointTransformation();
    Point2D p1 = new Point2D.Double(1, 0);
    Point2D p2 = new Point2D.Double(2, 0);
    Point2D p3 = new Point2D.Double(3, 0);
    Point2D res = pt.rotatePoint(p1, Math.PI, p2);
    assertEquals(0.0, p3.distance(res), Configuration.EPSILON);
  }

  @Test
  public void testIsValid() {
    PointTransformation pt = new PointTransformation();
    Point2D p1 = new Point2D.Double(1, 0);
    Point2D p2 = new Point2D.Double(1, Double.NEGATIVE_INFINITY);
    Point2D p3 = new Point2D.Double(Double.NEGATIVE_INFINITY, 2);
    assertTrue(pt.isValidPoint(p1));
    assertFalse(pt.isValidPoint(p2));
    assertFalse(pt.isValidPoint(p3));
  }

  @Test
  public void testCopy() {
    PointTransformation pt = new PointTransformation();
    Point2D p1 = new Point2D.Double(1, 0);
    Point2D p2 = pt.copyPoint(p1);
    assertFalse(p2 == p1);
    assertEquals(0, p1.distance(p2), Configuration.EPSILON);
  }

  @Test
  public void testPointOnLineOnStart() {
    PointTransformation pt = new PointTransformation();
    Point2D p1 = new Point2D.Double(1, 0);
    Point2D p2 = new Point2D.Double(12, 38);
    Point2D result = pt.getPointOnLine(p1, p2, 0);

    assertEquals(p1, result);
  }

  @Test
  public void testPointOnLineOnEnd() {
    PointTransformation pt = new PointTransformation();
    Point2D p1 = new Point2D.Double(1, 0);
    Point2D p2 = new Point2D.Double(12, 38);
    Point2D result = pt.getPointOnLine(p1, p2, 1);

    assertEquals(p2, result);
  }

  @Test
  public void testPointOnLineOnCenter() {
    PointTransformation pt = new PointTransformation();
    Point2D p1 = new Point2D.Double(1, 0);
    Point2D p2 = new Point2D.Double(12, 38);
    Point2D result = pt.getPointOnLine(p1, p2, 0.5);

    assertEquals(new Point2D.Double(6.5, 19), result);
  }

  @Test
  public void testPointOnLineInTheMiddle() {
    PointTransformation pt = new PointTransformation();
    Point2D p1 = new Point2D.Double(2, 0);
    Point2D p2 = new Point2D.Double(12, 20);
    Point2D result = pt.getPointOnLine(p1, p2, 0.4);

    assertEquals(new Point2D.Double(6, 8), result);
  }

  @Test
  public void testClosestPointOnPathIterator() {
    PointTransformation pt = new PointTransformation();
    Polygon triangle = new Polygon();
    triangle.addPoint(0, 0);
    triangle.addPoint(10, 0);
    triangle.addPoint(0, 10);

    Point2D result = pt.getClosestPointOnPathIterator(new Point2D.Double(10, 10),
        triangle.getPathIterator(new AffineTransform()));
    assertEquals(new Point2D.Double(5, 5), result);

    result = pt.getClosestPointOnPathIterator(new Point2D.Double(5, -10),
        triangle.getPathIterator(new AffineTransform()));
    assertEquals(new Point2D.Double(5, 0), result);

    result = pt.getClosestPointOnPathIterator(new Point2D.Double(-10, 5),
        triangle.getPathIterator(new AffineTransform()));
    assertEquals(new Point2D.Double(0, 5), result);

  }

  @Test
  public void testCollinearPoints() {
    PointTransformation pt = new PointTransformation();

    Point2D p1 = new ImmutablePoint2D(10, 20);
    Point2D p2 = new ImmutablePoint2D(100, 200);
    Point2D p3 = new ImmutablePoint2D(1000, 2000);
    assertTrue(pt.arePointsCollinear(p1, p2, p3));
    assertTrue(pt.arePointsCollinear(p1, p3, p2));
    assertTrue(pt.arePointsCollinear(p2, p1, p3));
    assertTrue(pt.arePointsCollinear(p2, p3, p1));
    assertTrue(pt.arePointsCollinear(p3, p1, p2));
    assertTrue(pt.arePointsCollinear(p3, p2, p1));
  }

}
