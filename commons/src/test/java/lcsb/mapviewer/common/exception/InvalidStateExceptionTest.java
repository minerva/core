package lcsb.mapviewer.common.exception;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InvalidStateExceptionTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor1() {
    assertNotNull(new InvalidStateException());
  }

  @Test
  public void testConstructor2() {
    assertNotNull(new InvalidStateException("str"));
  }

  @Test
  public void testConstructor3() {
    assertNotNull(new InvalidStateException(new Exception()));
  }

  @Test
  public void testConstructor4() {
    assertNotNull(new InvalidStateException("dsr", new Exception()));
  }

}
