package lcsb.mapviewer.common.exception;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NotImplementedExceptionTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor1() {
    assertNotNull(new NotImplementedException());
  }

  @Test
  public void testConstructor2() {
    assertNotNull(new NotImplementedException("str"));
  }

}
