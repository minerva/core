package lcsb.mapviewer.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PairTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor() {
    Pair<String, Integer> pair = new Pair<String, Integer>("test str", 12);
    assertEquals("test str", pair.getLeft());
    assertEquals((Integer) 12, pair.getRight());
  }

  @Test
  public void testEquals() {
    Pair<String, Integer> pair = new Pair<String, Integer>("test str", 12);
    Pair<String, Integer> pair2 = new Pair<String, Integer>("test str", 12);
    Pair<String, String> pair3 = new Pair<String, String>("test str", "str");
    Pair<String, Integer> pair4 = new Pair<String, Integer>("test str", 124);
    Pair<String, Integer> pair5 = new Pair<String, Integer>("test str1", 12);
    assertTrue(pair.equals(pair2));
    assertFalse(pair.equals(pair3));
    assertFalse(pair.equals(pair4));
    assertFalse(pair.equals(pair5));

    assertTrue(pair2.equals(pair));
    assertFalse(pair3.equals(pair));
    assertFalse(pair4.equals(pair));
    assertFalse(pair5.equals(pair));
    assertFalse(pair4.equals(new Object()));
  }

  @Test
  public void testHashCode() {
    Pair<String, Integer> pair = new Pair<String, Integer>("test str", 12);
    Pair<String, Integer> pair2 = new Pair<String, Integer>("test str", 12);
    Pair<String, Integer> pair3 = new Pair<String, Integer>("test str", 124);
    assertEquals(pair.hashCode(), pair2.hashCode());
    assertTrue(pair.hashCode() != pair3.hashCode());
  }

  @Test
  public void testNullEquals() {
    Pair<String, Integer> pair = new Pair<String, Integer>("test str", 12);
    Pair<String, Integer> pair2 = null;
    assertFalse(pair.equals(pair2));
  }

  @Test
  public void testToString() {
    Pair<String, Integer> pair = new Pair<String, Integer>("test str", 12);
    assertTrue(pair.toString().contains(pair.getLeft().toString()));
    assertTrue(pair.toString().contains(pair.getRight().toString()));
  }

}
