package lcsb.mapviewer.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TextFileUtilsTest extends CommonTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetHeaderParametersFromFile() throws Exception {
    String fileContent = "#header\n"
        + "#param1=value1\n"
        + "#\n"
        + "no header\n"
        + "#param2=value\n";

    Map<String, String> params = TextFileUtils
        .getHeaderParametersFromFile(new ByteArrayInputStream(fileContent.getBytes(StandardCharsets.UTF_8)));
    assertTrue(params.keySet().size() >= 1);
    assertNull(params.get("param2"));
    assertEquals("value1", params.get("param1"));
    assertNull(params.get("header"));
  }

  @Test
  public void testGetHeaderParametersFromEmptyFile() throws Exception {
    String fileContent = "";

    Map<String, String> params = TextFileUtils
        .getHeaderParametersFromFile(new ByteArrayInputStream(fileContent.getBytes(StandardCharsets.UTF_8)));
    assertNotNull(params.get(TextFileUtils.COLUMN_COUNT_PARAM));
  }

  @Test
  public void testParseHeader() throws Exception {
    InputStream is = new FileInputStream("testFiles/fileHeader.txt");
    Map<String, String> map = TextFileUtils.getHeaderParametersFromFile(is);

    assertEquals("1.0", map.get("VERSION"));
    assertEquals("example name", map.get("NAME"));
    assertEquals("layout description", map.get("DESCRIPTION"));
    assertEquals("", map.get("MISSING"));
    assertEquals("A=B", map.get("TRICKY"));
  }

  @Test
  public void testPrivateConstructor() throws Exception {
    Constructor<?> constr = TextFileUtils.class.getDeclaredConstructor(new Class<?>[] {});
    constr.setAccessible(true);
    assertNotNull(constr.newInstance(new Object[] {}));
  }

}
