package lcsb.mapviewer.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ConfigurationTest extends CommonTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    Configuration.setApplicationCacheOn(false);
    assertFalse(Configuration.isApplicationCacheOn());
    Configuration.setAutocompleteSize(1);
    assertEquals(1, Configuration.getAutocompleteSize());
  }

  @Test
  public void testLoadSystemBuildVersion() {
    Configuration.loadSystemVersion((String) null);
    assertEquals("Unknown", Configuration.getSystemBuildVersion(null));
    assertEquals("Unknown", Configuration.getSystemBuild(null));
    assertEquals("Unknown", Configuration.getSystemVersion(null));
  }

  @Test
  public void testLoadSystemBuildVersion2() {
    Configuration.loadSystemVersion((String) null);
    Configuration.loadSystemVersion("/tmp");
    assertEquals("Unknown", Configuration.getSystemBuildVersion(null));
    assertEquals("Unknown", Configuration.getSystemBuild(null));
    assertEquals("Unknown", Configuration.getSystemVersion(null));
  }

  @Test
  public void testGetSystemBuildVersion() {
    assertEquals("78606521500fd349b603a77e9f64cfe57517c1a6", Configuration.getSystemBuildVersion("testFiles/version/", true));
    assertEquals("78606521500fd349b603a77e9f64cfe57517c1a6", Configuration.getSystemBuildVersion(null, false));
    assertEquals("16.0.0~alpha.2", Configuration.getSystemVersion("testFiles/version/", true));
  }

  @Test
  public void testGetSystemBuild() {
    assertEquals("2021-03-29T08:44:16+0200", Configuration.getSystemBuild("testFiles/version/", true));
    assertEquals("2021-03-29T08:44:16+0200", Configuration.getSystemBuild(null, false));
  }

  @Test
  public void testXGetSystemVersion() {
    List<String> frame = new ArrayList<>();
    frame.add("test");
    Configuration.setxFrameDomain(frame);
    assertEquals(frame, Configuration.getxFrameDomain());
  }

  @Test
  public void testLoadMissingSystemVersion() {
    FrameworkVersion version = Configuration.getFrameworkVersion("testFiles/invalid_version/");
    assertEquals("Unknown", version.getGitVersion());
    assertEquals("Unknown", version.getTime());
    assertEquals("Unknown", version.getVersion());
    assertEquals(1, super.getWarnings().size());
  }

  @Test
  public void testGetFrameworkVersion() {
    FrameworkVersion version = Configuration.getFrameworkVersion(null);
    assertEquals("Unknown", version.getGitVersion());
    assertEquals("Unknown", version.getTime());
    assertEquals("Unknown", version.getVersion());
  }

  @Test
  public void testGetMemorySaturationRatioTriggerClean() {
    double newRatio = 33;
    double oldRatio = Configuration.getMemorySaturationRatioTriggerClean();
    try {
      Configuration.setMemorySaturationRatioTriggerClean(newRatio);
      assertEquals(newRatio, Configuration.getMemorySaturationRatioTriggerClean(), Configuration.EPSILON);
    } finally {
      Configuration.setMemorySaturationRatioTriggerClean(oldRatio);
    }
  }

  @Test
  public void testPrivateConstructor() throws Exception {
    Constructor<?> constr = Configuration.class.getDeclaredConstructor(new Class<?>[] {});
    constr.setAccessible(true);
    assertNotNull(constr.newInstance(new Object[] {}));
  }
}
