package lcsb.mapviewer.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Constructor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;

public class ObjectUtilsTest extends CommonTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetIdOfObject() {
    Object obj = new Object() {
      @SuppressWarnings("unused")
      public int getId() {
        return 107;
      }
    };
    assertEquals((Integer) 107, ObjectUtils.getIdOfObject(obj));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetIdOfObjectWithoutId() {
    Object obj = new Object();
    ObjectUtils.getIdOfObject(obj);
  }

  @Test
  public void testPrivateConstructor() throws Exception {
    Constructor<?> constr = ObjectUtils.class.getDeclaredConstructor(new Class<?>[] {});
    constr.setAccessible(true);
    assertNotNull(constr.newInstance(new Object[] {}));
  }

}
