package lcsb.mapviewer.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class GlobalLoggerAppenderTest extends CommonTestFunctions {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testLogCatching() {
    MinervaLoggerAppender appender = MinervaLoggerAppender.createAppender();
    logger.warn("test");
    logger.debug("1");
    logger.error("2");
    logger.info("3");
    logger.fatal("4");
    logger.trace("5");
    assertEquals(1, appender.getDebugs().size());
    assertEquals(1, appender.getWarnings().size());
    assertEquals(1, appender.getFatals().size());
    assertEquals(1, appender.getErrors().size());
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
    logger.warn("test");
    assertEquals(1, appender.getWarnings().size());
  }

  @Test
  public void testLogContent() {
    MinervaLoggerAppender appender = MinervaLoggerAppender.createAppender();
    logger.warn("test");
    logger.warn("xyz");
    assertEquals(2, appender.getWarnings().size());
    assertNotEquals(appender.getWarnings().get(0), appender.getWarnings().get(1));
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
  }

}
