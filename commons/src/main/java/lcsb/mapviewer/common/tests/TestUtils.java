package lcsb.mapviewer.common.tests;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import net.datafaker.Faker;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LogEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.springframework.test.context.TestPropertySource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.Desktop;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

@TestPropertySource(properties = "spring.flyway.enabled=false")
public class TestUtils {

  protected static Faker faker = new Faker();

  protected static final String BUILT_IN_TEST_ADMIN_PASSWORD = "admin";
  protected static final String BUILT_IN_TEST_ADMIN_LOGIN = "admin";

  protected static final int BUILT_IN_TEST_ADMIN_ID = 1;

  protected static final String BUILT_IN_PROJECT = "empty";
  protected static final int BUILT_IN_MAP_ID = 3;

  protected static final String TEST_PROJECT = "test_project";
  protected static final String TEST_PROJECT_2 = "test_project_2";

  protected static final String TEST_USER_PASSWORD = "test_pass";
  protected static final String TEST_USER_LOGIN = "test_user";


  protected static String TEST_PROJECT_ID = "Some_id";

  protected static double EPSILON = Configuration.EPSILON;

  protected static final Logger logger = LogManager.getLogger();

  private static final int BLOCK_SIZE = 65536;

  protected String dapiLogin;
  protected String dapiPassword;

  private final DocumentBuilder db;

  private MinervaLoggerAppender appender;

  @Rule
  public TestName testName = new TestName();

  public TestUtils() {
    try {
      dapiLogin = System.getenv("DAPI_TEST_LOGIN");
      dapiPassword = System.getenv("DAPI_TEST_PASSWORD");
      db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    } catch (ParserConfigurationException e) {
      throw new InvalidStateException();
    }
  }

  @Before
  public final void _setUp() throws Exception {
    logger.debug("Starting test: " + this.getClass().getSimpleName() + "; " + testName.getMethodName());
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
    appender = MinervaLoggerAppender.createAppender(false);
  }

  @After
  public final void _tearDown() throws Exception {
    MinervaLoggerAppender.unregisterLogEventStorage(appender);
  }

  protected final List<LogEvent> getWarnings() {
    return appender.getWarnings();
  }

  protected final List<LogEvent> getErrors() {
    return appender.getErrors();
  }

  protected final List<LogEvent> getFatals() {
    return appender.getFatals();
  }

  protected final List<LogEvent> getInfos() {
    return appender.getInfos();
  }

  protected final List<LogEvent> getDebugs() {
    return appender.getDebugs();
  }

  public final boolean isDapiConfigurationAvailable() {
    return dapiLogin != null && dapiPassword != null;
  }

  public final String getDapiLogin() {
    return dapiLogin;
  }

  public final String getDapiPassword() {
    return dapiPassword;
  }

  public final boolean equalFiles(final String fileA, final String fileB) throws IOException {
    FileInputStream inputStreamA = new FileInputStream(fileA);
    FileInputStream inputStreamB = new FileInputStream(fileB);
    // vary BLOCK_SIZE to suit yourself.
    // it should probably a factor or multiple of the size of a disk
    // sector/cluster.
    // Note that your max heap size may need to be adjused
    // if you have a very big block size or lots of these comparators.

    // assume inputStreamA and inputStreamB are streams from your two files.
    byte[] streamABlock = new byte[BLOCK_SIZE];
    byte[] streamBBlock = new byte[BLOCK_SIZE];
    boolean match = true;
    int bytesReadA = 0;
    int bytesReadB = 0;
    do {
      bytesReadA = inputStreamA.read(streamABlock);
      bytesReadB = inputStreamB.read(streamBBlock);
      match = ((bytesReadA == bytesReadB) && Arrays.equals(streamABlock, streamBBlock));
    } while (match && (bytesReadA > -1));
    inputStreamA.close();
    inputStreamB.close();
    return match;
  }

  public final String readFile(final String file) throws IOException {
    StringBuilder stringBuilder = new StringBuilder();
    BufferedReader reader = new BufferedReader(new FileReader(file));
    try {
      String line = null;
      String ls = System.getProperty("line.separator");

      while ((line = reader.readLine()) != null) {
        stringBuilder.append(line);
        stringBuilder.append(ls);
      }
    } finally {
      reader.close();
    }

    return stringBuilder.toString();
  }

  public final Node getNodeFromXmlString(final String text) throws InvalidXmlSchemaException, IOException {
    InputSource is = new InputSource();
    is.setCharacterStream(new StringReader(text));
    return getXmlDocumentFromInputSource(is).getChildNodes().item(0);
  }

  public final Document getXmlDocumentFromFile(final String fileName) throws InvalidXmlSchemaException, IOException {
    File file = new File(fileName);
    InputStream inputStream = new FileInputStream(file);
    Reader reader = null;
    try {
      reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
      InputSource is = new InputSource(reader);

      Document result = getXmlDocumentFromInputSource(is);
      inputStream.close();
      return result;
    } catch (final UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    return null;
  }

  public final Document getXmlDocumentFromInputSource(final InputSource stream) throws InvalidXmlSchemaException, IOException {
    DocumentBuilder db;
    try {
      db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    } catch (final ParserConfigurationException e) {
      throw new InvalidXmlSchemaException("Problem with xml parser");
    }
    Document doc = null;
    try {
      doc = db.parse(stream);
    } catch (final SAXException e) {
      throw new InvalidXmlSchemaException(e);
    }
    return doc;
  }

  public final String nodeToString(final Node node) {
    return nodeToString(node, false);
  }

  public final String nodeToString(final Node node, final boolean includeHeadNode) {
    if (node == null) {
      return null;
    }
    StringWriter sw = new StringWriter();
    try {
      Transformer t = TransformerFactory.newInstance().newTransformer();
      t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
      t.setOutputProperty(OutputKeys.INDENT, "yes");
      t.setOutputProperty(OutputKeys.METHOD, "xml");

      NodeList list = node.getChildNodes();
      for (int i = 0; i < list.getLength(); i++) {
        Node element = list.item(i);
        t.transform(new DOMSource(element), new StreamResult(sw));
      }
    } catch (final TransformerException te) {
      logger.debug("nodeToString Transformer Exception");
    }
    if (includeHeadNode) {
      return "<" + node.getNodeName() + ">" + sw + "</" + node.getNodeName() + ">";
    }
    return sw.toString();
  }

  public final String getWebpage(final String accessUrl) throws IOException {
    String inputLine;
    IOException exception = null;
    for (int i = 0; i < 3; i++) {
      try {
        StringBuilder tmp = new StringBuilder();
        URL url = new URL(accessUrl);
        HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
        urlConn.setRequestMethod("GET");
        urlConn.connect();
        BufferedReader in = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

        while ((inputLine = in.readLine()) != null) {
          tmp.append(inputLine);
        }
        in.close();
        return tmp.toString();
      } catch (final IOException e) {
        exception = e;
      }
    }
    throw exception;
  }

  public void showImage(final BufferedImage bi) throws IOException {
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    ImageIO.write(bi, "PNG", outputStream);
    byte[] output2 = outputStream.toByteArray();

    FileUtils.writeByteArrayToFile(new File("tmp.png"), output2);

    Desktop.getDesktop().open(new File("tmp.png"));
  }

  public Element fileToNode(final String filename) throws SAXException, IOException {
    InputStream is = new FileInputStream(new File(filename));
    Document doc = null;
    doc = db.parse(is);
    return doc.getDocumentElement();
  }

  public boolean isAnnotatedBy(final Enum<?> enumObject, final Class<? extends Annotation> clazz) {
    boolean deprecated = false;
    try {
      Field f = enumObject.getClass().getField(enumObject.name());
      if (f.isAnnotationPresent(clazz)) {
        deprecated = true;
      }
    } catch (final NoSuchFieldException | SecurityException e) {
    }
    return deprecated;
  }

}
