package lcsb.mapviewer.common.tests;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class UnitTestFailedWatcher extends TestWatcher {
  @Override
  protected void failed(final Throwable e, final Description description) {
    if (!(e instanceof AssertionError)) {
      e.printStackTrace();
    }
  }
}
