/**
 * Provides classes (with functionality not implemented in jre) that allows to
 * manipulate on the common geometry objects, like:
 * <ul>
 * <li>lines,</li>
 * <li>ellipses,</li>
 * <li>points,</li>
 * </ul>
 * .
 */
package lcsb.mapviewer.common.geometry;
