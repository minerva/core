package lcsb.mapviewer.common.geometry;

import java.awt.Shape;
import java.awt.Stroke;

/**
 * This class represent {@link Stroke} that consist of two nested {@link Stroke
 * strokes}. In this way we can create a double line. More information can be
 * found <a href="http://www.jhlabs.com/java/java2d/strokes/">here</a>.
 * 
 * @author Piotr Gawron
 *
 */
public class CompositeStroke implements Stroke {

  /**
   * Outside stroke.
   */
  private Stroke stroke1;

  /**
   * Inside stroke used as a border of outside stroke.
   */
  private Stroke stroke2;

  /**
   * DEfault constructor.
   * 
   * @param stroke1
   *          {@link #stroke1}
   * @param stroke2
   *          {@link #stroke2}
   */
  public CompositeStroke(final Stroke stroke1, final Stroke stroke2) {
    this.stroke1 = stroke1;
    this.stroke2 = stroke2;
  }

  @Override
  public Shape createStrokedShape(final Shape shape) {
    return stroke2.createStrokedShape(stroke1.createStrokedShape(shape));
  }
}
