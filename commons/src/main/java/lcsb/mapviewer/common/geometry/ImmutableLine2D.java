package lcsb.mapviewer.common.geometry;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 * Implementation of the Point2D that does not allow to modify data. Useful for
 * getters of data that should not be modified by chance.
 * 
 * @author Piotr Gawron
 *
 */
public final class ImmutableLine2D extends Line2D {
  private final double x1;
  private final double y1;
  private final double x2;
  private final double y2;

  public ImmutableLine2D(final double x1, final double y1, final double x2, final double y2) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
  }

  public ImmutableLine2D(final Line2D line) {
    this(line.getX1(), line.getY1(), line.getX2(), line.getY2());
  }

  @Override
  public Rectangle2D getBounds2D() {
    return new Rectangle2D.Double(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x1 - x2), Math.abs(y1 - y2));
  }

  @Override
  public double getX1() {
    return x1;
  }

  @Override
  public double getY1() {
    return y1;
  }

  @Override
  public Point2D getP1() {
    return new ImmutablePoint2D(x1, y1);
  }

  @Override
  public double getX2() {
    return x2;
  }

  @Override
  public double getY2() {
    return y2;
  }

  @Override
  public Point2D getP2() {
    return new ImmutablePoint2D(x2, y2);
  }

  @Override
  public void setLine(final double x1, final double y1, final double x2, final double y2) {
    throw new UnsupportedOperationException();
  }
}
