package lcsb.mapviewer.common.geometry;

import lcsb.mapviewer.common.exception.InvalidArgumentException;

import java.awt.Color;

/**
 * Parser class to extract {@link Color} objects from {@link String}.
 *
 * @author Piotr Gawron
 */
public class ColorParser {

  /**
   * Base of the hex representation.
   */
  private static final int HEX_BASE = 16;

  /**
   * Length of the string describing color in RGB: "#RRGGBB".
   */
  private static final int COLOR_STRING_LENGTH_WITHOUT_ALPHA = 7;

  /**
   * Length of the string describing color in RGB: "#RRGGBBAA".
   */
  private static final int COLOR_STRING_LENGTH_WITH_ALPHA = 9;

  /**
   * Where starts description of red color in string representing color.
   */
  private static final int COLOR_SUBSTRING_START_RED = 1;

  /**
   * Where starts description of green color in string representing color.
   */
  private static final int COLOR_SUBSTRING_START_GREEN = 3;

  /**
   * Where starts description of blue color in string representing color.
   */
  private static final int COLOR_SUBSTRING_START_BLUE = 5;

  /**
   * Extracts {@link Color} from input {@link String}.
   *
   * @param string text to process
   * @return {@link Color} obtained from input text
   */
  public Color parse(final String string) {
    if (string == null || string.isEmpty()) {
      throw new InvalidArgumentException(
          "Invalid color value: " + string + ". Correct format: #xxxxxx (where x is a hex value)");
    }

    String colorString = string;
    if (string.charAt(0) != '#') {
      colorString = "#" + string;
    }
    if (colorString.length() == COLOR_STRING_LENGTH_WITHOUT_ALPHA) {
      return parseColorWithoutAlpha(colorString);
    } else {
      return parseColorWithAlpha(colorString);
    }
  }

  /**
   * Extracts {@link Color} from input {@link String}.
   *
   * @param inputString text to process
   * @return {@link Color} obtained from input text
   */
  public Color parseColorWithoutAlpha(final String inputString) {
    if (inputString == null || inputString.isEmpty()) {
      throw new InvalidArgumentException(
          "Invalid color value: " + inputString + ". Correct format: #xxxxxx (where x is a hex value)");
    }
    String string = inputString;
    if (string.charAt(0) != '#') {
      string = "#" + string;
    }
    if (string.length() != COLOR_STRING_LENGTH_WITHOUT_ALPHA) {
      throw new InvalidArgumentException(
          "Invalid color value: " + string + ". Correct format: #xxxxxx (where x is a hex value)");
    } else {
      try {
        return new Color(
            Integer.valueOf(string.substring(COLOR_SUBSTRING_START_RED, COLOR_SUBSTRING_START_GREEN), HEX_BASE),
            Integer.valueOf(string.substring(COLOR_SUBSTRING_START_GREEN, COLOR_SUBSTRING_START_BLUE), HEX_BASE),
            Integer.valueOf(string.substring(COLOR_SUBSTRING_START_BLUE, COLOR_STRING_LENGTH_WITHOUT_ALPHA), HEX_BASE));
      } catch (final NumberFormatException e) {
        throw new InvalidArgumentException(
            "Invalid color value: " + string + ". Correct format: #xxxxxx (where x is a hex value)");
      }
    }
  }

  /**
   * Extracts {@link Color} from input {@link String}.
   *
   * @param inputString text to process
   * @return {@link Color} obtained from input text
   */
  public Color parseColorWithAlpha(final String inputString) {
    if (inputString == null || inputString.isEmpty()) {
      throw new InvalidArgumentException(
          "Invalid color value: " + inputString + ". Correct format: #xxxxxxxx (where x is a hex value)");
    }
    String string = inputString;
    if (string.charAt(0) != '#') {
      string = "#" + string;
    }
    if (string.length() != COLOR_STRING_LENGTH_WITH_ALPHA) {
      throw new InvalidArgumentException(
          "Invalid color value: " + string + ". Correct format: #xxxxxxxx (where x is a hex value)");
    } else {
      try {
        return new Color(
            Integer.valueOf(string.substring(COLOR_SUBSTRING_START_RED, COLOR_SUBSTRING_START_GREEN), HEX_BASE),
            Integer.valueOf(string.substring(COLOR_SUBSTRING_START_GREEN, COLOR_SUBSTRING_START_BLUE), HEX_BASE),
            Integer.valueOf(string.substring(COLOR_SUBSTRING_START_BLUE, COLOR_STRING_LENGTH_WITHOUT_ALPHA), HEX_BASE),
            Integer.valueOf(string.substring(COLOR_STRING_LENGTH_WITHOUT_ALPHA, COLOR_STRING_LENGTH_WITH_ALPHA),
                HEX_BASE));
      } catch (final NumberFormatException e) {
        throw new InvalidArgumentException(
            "Invalid color value: " + string + ". Correct format: #xxxxxxxx (where x is a hex value)");
      }
    }
  }

  /**
   * Transforms {@link Color} into html RGB representation: #RRGGBB
   */
  public String colorToHtml(final Color color) {

    return "#" + String.format("%02x", color.getRed()) + String.format("%02x", color.getGreen())
        + String.format("%02x", color.getBlue());
  }

  public String serializeColor(final Color color) {
    return colorToHtml(color) + String.format("%02x", color.getAlpha());
  }

  public Color deserializeColor(final String text) {
    if (text.length() == COLOR_STRING_LENGTH_WITHOUT_ALPHA) {
      return parseColorWithoutAlpha(text);
    } else {
      return parseColorWithAlpha(text);
    }
  }
}
