package lcsb.mapviewer.common.geometry;

import lcsb.mapviewer.common.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Class for basic point transformations.
 *
 * @author Piotr Gawron
 */
public class PointTransformation {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger();

  /**
   * Rotates point around center using the angle.
   *
   * @param point  object that we want to rotate
   * @param angle  angle by which we want to rotate
   * @param center central point around which we rotate the object
   * @return the same object rotated by the appropriate angle
   */
  public Point2D rotatePoint(final Point2D point, final double angle, final Point2D center) {
    double s = Math.sin(angle);
    double c = Math.cos(angle);

    // translate point back to origin:

    point.setLocation(point.getX() - center.getX(), point.getY() - center.getY());

    // rotate point
    double xnew = point.getX() * c - point.getY() * s;
    double ynew = point.getX() * s + point.getY() * c;

    // translate point back:
    point.setLocation(xnew + center.getX(), ynew + center.getY());
    return point;
  }

  /**
   * Checks if a point given in the parameter is valid (can be used for
   * drawing). The point is considered as valid if coordinates are finite (final
   * NaN and Infinity are invalid - they cannot be drawn).
   *
   * @param point point to check
   * @return <code>true</code> if coordinates are normal real numbers,
   * <code>false</code> otherwise (NaN, infinity)
   */
  public boolean isValidPoint(final Point2D point) {
    return Double.isFinite(point.getX()) && Double.isFinite(point.getY());
  }

  /**
   * Creates a copy of the point.
   *
   * @param point object to be copied
   * @return copy of the object
   */
  public Point2D copyPoint(final Point2D point) {
    return new Point2D.Double(point.getX(), point.getY());
  }

  /**
   * Returns a point on line.
   *
   * @return {@link Point2D} on line defined by input points
   */
  public Point2D getPointOnLine(final Point2D start, final Point2D end, final double coef) {
    double x = start.getX() + (end.getX() - start.getX()) * coef;
    double y = start.getY() + (end.getY() - start.getY()) * coef;
    return new Point2D.Double(x, y);
  }

  public Point2D getClosestPointOnPathIterator(final Point2D point, final PathIterator pi) {
    if (pi == null) {
      return null;
    }
    LineTransformation lt = new LineTransformation();
    double[] coordinates = new double[LineTransformation.PATH_ITERATOR_COORDINATES_STRUCT_SIZE];
    Point2D first = null;
    Point2D last = null;
    Point2D actual = null;

    Point2D result = null;
    double distance = Double.MAX_VALUE;
    while (!pi.isDone()) {
      int type = pi.currentSegment(coordinates);
      last = actual;
      actual = new Point2D.Double(coordinates[0], coordinates[1]);
      switch (type) {
        case PathIterator.SEG_MOVETO:
          break;
        case PathIterator.SEG_LINETO:
          break;
        case PathIterator.SEG_QUADTO:
          break;
        case PathIterator.SEG_CUBICTO:

          // in case when there is an arc we define only end points of the arc
          // as a border
          actual = new Point2D.Double(coordinates[LineTransformation.SEG_CUBICTO_END_X_COORDINATE_INDEX],
              coordinates[LineTransformation.SEG_CUBICTO_END_Y_COORDINATE_INDEX]);
          break;
        case PathIterator.SEG_CLOSE:
          actual = first;
          break;
        default:
          break;
      }
      if (first == null) {
        first = actual;

        // check if the two following points are not the same (this could cause
        // NaN values)
      } else if (last.distance(actual) > Configuration.EPSILON) {
        Point2D closest = lt.closestPointOnSegmentLineToPoint(last, actual, point);
        if (result == null || distance > closest.distance(point)) {
          result = closest;
          distance = closest.distance(point);
        }
      }

      pi.next();
    }
    return result;
  }

  public boolean arePointsCollinear(final Point2D point1,
                                    final Point2D point2,
                                    final Point2D point3) {
    List<Point2D> points = Arrays.asList(point1, point2, point3);
    points.sort(new Comparator<Point2D>() {
      @Override
      public int compare(final Point2D o1, final Point2D o2) {
        if (o1.getX() == o2.getX()) {
          if (o1.getY() == o2.getY()) {
            return 0;
          } else if (o1.getY() < o2.getY()) {
            return 1;
          } else {
            return -1;
          }
        } else if (o1.getX() < o2.getX()) {
          return 1;
        } else {
          return -1;
        }
      }
    });
    Point2D p1 = points.get(0);
    Point2D p2 = points.get(1);
    Point2D p3 = points.get(2);
    Point2D closestPointOnLine = new LineTransformation().closestPointOnSegmentLineToPoint(p1, p3, p2);
    return closestPointOnLine.distance(p2) <= Configuration.EPSILON;
  }

}
