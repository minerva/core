package lcsb.mapviewer.common.geometry;

import java.awt.geom.Dimension2D;

public class DoubleDimension extends Dimension2D {

  private double width;
  private double height;

  public DoubleDimension(final double width, final double height) {
    this.width = width;
    this.height = height;
  }

  @Override
  public double getWidth() {
    return width;
  }

  @Override
  public double getHeight() {
    return height;
  }

  @Override
  public void setSize(final double width, final double height) {
    this.width = width;
    this.height = height;
  }

  @Override
  public String toString() {
    return "DoubleDimension[" + width + "," + height + "]";
  }

}
