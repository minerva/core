package lcsb.mapviewer.common.geometry;

import java.awt.geom.Point2D;

/**
 * Implementation of the Point2D that does not allow to modify data. Useful for
 * getters of data that should not be modified by chance.
 *
 * @author Piotr Gawron
 */
public final class ImmutablePoint2D extends Point2D {
  private final double x;
  private final double y;

  public ImmutablePoint2D(final double x, final double y) {
    this.x = x;
    this.y = y;
  }

  public ImmutablePoint2D(final Point2D point) {
    this(point.getX(), point.getY());
  }

  @Override
  public double getX() {
    return x;
  }

  @Override
  public double getY() {
    return y;
  }

  @Override
  public void setLocation(final double x, final double y) {
    throw new UnsupportedOperationException();
  }

  @Override
  public String toString() {
    return "ImmutablePoint2D[" + x + ", " + y + "]";

  }
}
