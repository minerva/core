package lcsb.mapviewer.common.geometry;

import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Configuration;

/**
 * This class contains basic operators on ellipse used by converters.
 * 
 * @author Piotr Gawron
 * 
 */
public class EllipseTransformation {
  /**
   * PI value.
   */
  private static final double PI = Math.PI;
  /**
   * PI/2 value.
   */
  private static final double PI_1_2 = Math.PI / 2;
  /**
   * 3/2 PI value.
   */
  private static final double PI_3_2 = PI_1_2 * 3;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  /**
   * Method returns a cross point between ellipse and a line (from center point
   * with angle between the line and X axis).
   * 
   * @param ellipse
   *          ellipse object on which we want to find point
   * @param angle
   *          angle in degrees
   * @return point on the ellipse that connect center of the ellipse with the line
   *         that cross X axis by angle
   */
  public Point2D getPointOnEllipseByDegree(final Ellipse2D ellipse, final double angle) {
    return getPointOnEllipseByRadian(ellipse, Math.toRadians(angle));
  }

  /**
   * Method returns a cross point between ellipse and a line (from center point
   * with angle between the line and X axis).
   * 
   * @param x
   *          x coordinate of the ellipse
   * @param y
   *          y coordinate of the ellipse
   * @param width
   *          width of the ellipse
   * @param height
   *          height of the ellipse
   * @param angle
   *          angle in degrees
   * @return point on the ellipse that connect center of the ellipse with the line
   *         that cross X axis by angle
   */
  public Point2D getPointOnEllipseByDegree(final double x, final double y, final double width, final double height,
      final double angle) {
    return getPointOnEllipseByRadian(x, y, width, height, Math.toRadians(angle));
  }

  /**
   * Method returns a cross point between ellipse and a line (from center point
   * with angle between the line and X axis).
   * 
   * @param x
   *          x coordinate of the ellipse
   * @param y
   *          y coordinate of the ellipse
   * @param width
   *          width of the ellipse
   * @param height
   *          height of the ellipse
   * @param angle
   *          angle in radians
   * @return point on the ellipse that connect center of the ellipse with the line
   *         that cross X axis by angle
   */
  public Point2D getPointOnEllipseByRadian(final double x, final double y, final double width, final double height,
      final double angle) {
    double boundedAngle = angle;

    while (boundedAngle < 0) {
      boundedAngle += 2 * Math.PI;
    }
    while (boundedAngle > (2 * Math.PI - Configuration.EPSILON)) {
      boundedAngle -= 2 * Math.PI;
    }
    Point2D result = new Point2D.Double();

    double a = width / 2;
    double b = height / 2;

    double resX = 0;
    double resY = 0;
    // special cases (atan is infinity)
    if (Math.abs(boundedAngle - 0) < Configuration.EPSILON) {
      resX = -a;
      resY = 0;
    } else if (Math.abs(boundedAngle - PI) < Configuration.EPSILON) {
      resX = a;
      resY = 0;
    } else if (Math.abs(boundedAngle - PI_1_2) < Configuration.EPSILON) {
      resX = 0;
      resY = -b;
    } else if (Math.abs(boundedAngle - PI_3_2) < Configuration.EPSILON) {
      resX = 0;
      resY = b;
    } else {
      double tan = Math.tan(boundedAngle);

      resX = a * b / Math.sqrt(b * b + a * a * tan * tan);
      resY = resX * tan;
      if (boundedAngle <= PI_1_2 || boundedAngle > PI_3_2) {
        resX = -resX;
        resY = -resY;
      }
    }

    resX += x + a;
    resY += y + b;

    result.setLocation(resX, resY);
    return result;
  }

  /**
   * Method returns a cross point between ellipse and a line (from center point
   * with angle between the line and X axis).
   * 
   * @param ellipse
   *          ellipse object on which we want to find point
   * @param angle
   *          angle in radians
   * @return point on the ellipse that connect center of the ellipse with the line
   *         that cross X axis by angle
   */
  public Point2D getPointOnEllipseByRadian(final Ellipse2D ellipse, final double angle) {
    double width = ellipse.getWidth();
    double height = ellipse.getHeight();

    return getPointOnEllipseByRadian(ellipse.getX(), ellipse.getY(), width, height, angle);
  }

  public Point2D getClosestPoint(final Point2D point, final Ellipse2D ellipse) {
    return getPointOnEllipseByRadian(ellipse,
        Math.atan2(ellipse.getCenterY() - point.getY(), ellipse.getCenterX() - point.getX()));
  }
}
