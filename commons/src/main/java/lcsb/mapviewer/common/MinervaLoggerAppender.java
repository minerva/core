package lcsb.mapviewer.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.Property;
import org.apache.logging.log4j.core.impl.MutableLogEvent;
import org.apache.logging.log4j.core.layout.PatternLayout;

/**
 * Custom log4j {@link AbstractAppender}. This class is used to store logs in
 * the memory. Such logs should be stored for short period of time. Only logs
 * for the {@link Thread} that created the object will be stored.
 * 
 * @author Piotr Gawron
 *
 */
public class MinervaLoggerAppender extends AbstractAppender {

  /**
   * Every logger must have different name. We use this counter to create new
   * names.
   */
  private static int loggerCounter = 0;
  /**
   * List of {@link LogLevel#DEBUG} logs.
   */
  private List<LogEvent> debugEvents = new ArrayList<>();
  /**
   * List of {@link LogLevel#INFO} logs.
   */
  private List<LogEvent> infoEvents = new ArrayList<>();
  /**
   * List of {@link LogLevel#WARN} logs.
   */
  private List<LogEvent> warnEvents = new ArrayList<>();
  /**
   * List of {@link LogLevel#ERROR} logs.
   */
  private List<LogEvent> errorEvents = new ArrayList<>();
  /**
   * List of {@link LogLevel#FATAL} logs.
   */
  private List<LogEvent> fatalEvents = new ArrayList<>();
  /**
   * List of logs with unknown log level.
   */
  private List<LogEvent> otherEvents = new ArrayList<>();
  /**
   * Identifier of {@link Thread} that created this object.
   */
  private long threadId;
  /**
   * Flag that describe if we log only entries for current thread (
   * <code>true</code>) or for all threads (<code>false</code>).
   */
  private boolean currentThreadLogOnly = true;

  /**
   * Private constructor preventing instantiation. Appender should be created
   * using factory method: {@link MinervaLoggerAppender#createAppender()}.
   * 
   */
  private MinervaLoggerAppender(final String name, final Filter filter, final Layout<? extends Serializable> layout,
      final boolean ignoreExceptions, final boolean currentThreadLogOnly) {
    super(name, filter, layout, ignoreExceptions, new Property[0]);
    this.threadId = Thread.currentThread().getId();
    this.currentThreadLogOnly = currentThreadLogOnly;
  }

  /**
   * Creates appender that will store logs in memory.
   * 
   * @return appender
   */
  public static MinervaLoggerAppender createAppender() {
    return createAppender(true);
  }

  /**
   * Creates appender that will store logs in memory.
   * 
   * @param currentThreadLogOnly
   *          if <code>true</code> logs should be taken only from thread that
   *          created object, if <code>false</code> all logs will be stored
   * @return appender
   */
  public static MinervaLoggerAppender createAppender(final boolean currentThreadLogOnly) {
    LoggerContext context = LoggerContext.getContext(false);
    org.apache.logging.log4j.core.config.Configuration config = context.getConfiguration();
    PatternLayout layout = PatternLayout.createDefaultLayout(config);
    if (layout == null) {
      layout = PatternLayout.createDefaultLayout();
    }

    final Filter filter = null;

    final MinervaLoggerAppender appender = new MinervaLoggerAppender("MinervaAppender" + loggerCounter++, filter,
        layout, true, currentThreadLogOnly);

    appender.start();
    config.addAppender(appender);
    final Level level = Level.DEBUG;
    config.getRootLogger().addAppender(appender, level, filter);

    // Configurator.setAllLevels(LogManager.getRootLogger().getName(), level);

    return appender;

  }

  public static void unregisterLogEventStorage(final MinervaLoggerAppender appender) {
    if (appender != null) {
      final LoggerContext context = LoggerContext.getContext(false);
      final org.apache.logging.log4j.core.config.Configuration config = context.getConfiguration();
      config.getRootLogger().removeAppender(appender.getName());
    }
  }

  @Override
  public void append(final LogEvent logEvent) {
    // store information for all thread only if it is flagged by
    // currentThreadLogOnly, if not only logs from current thread should be
    // stored
    if (!currentThreadLogOnly || threadId == Thread.currentThread().getId()) {
      LogEvent eventToBePersisted = logEvent;
      if (logEvent instanceof MutableLogEvent) {
        eventToBePersisted = ((MutableLogEvent) logEvent).createMemento();
      }
      if (eventToBePersisted.getLevel().equals(Level.DEBUG)) {
        debugEvents.add(eventToBePersisted);
      } else if (eventToBePersisted.getLevel().equals(Level.INFO)) {
        infoEvents.add(eventToBePersisted);
      } else if (eventToBePersisted.getLevel().equals(Level.WARN)) {
        warnEvents.add(eventToBePersisted);
      } else if (eventToBePersisted.getLevel().equals(Level.ERROR)) {
        errorEvents.add(eventToBePersisted);
      } else if (eventToBePersisted.getLevel().equals(Level.FATAL)) {
        fatalEvents.add(eventToBePersisted);
      } else {
        otherEvents.add(eventToBePersisted);
      }
    }
  }

  /**
   * Returns list of warning logs.
   * 
   * @return list of warning logs
   */
  public List<LogEvent> getWarnings() {
    return warnEvents;
  }

  /**
   * Returns list of error logs.
   * 
   * @return list of error logs
   */
  public List<LogEvent> getErrors() {
    return errorEvents;
  }

  /**
   * Returns list of fatal logs.
   * 
   * @return list of fatal logs
   */
  public List<LogEvent> getFatals() {
    return fatalEvents;
  }

  /**
   * Returns list of warning logs.
   * 
   * @return list of warning logs
   */
  public List<LogEvent> getInfos() {
    return infoEvents;
  }

  public List<LogEvent> getDebugs() {
    return debugEvents;
  }

}
