package lcsb.mapviewer.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
@PropertySources({
    @PropertySource(value = "file:/etc/minerva/minerva.properties", ignoreResourceNotFound = true)
})
public class MinervaConfigurationHolder {

  private static final Logger logger = LogManager.getLogger();

  @Value("${batch.threads:4}")
  private Integer batchThreads;

  @Value("${data.path:/usr/share/minerva/data/}")
  private String dataPath = null;

  public Integer getBatchThreads() {
    return batchThreads;
  }

  public String getDataPath() {
    return dataPath;
  }

  public String getDataRootPath() {
    String path = getDataPath();
    if (path == null) {
      path = new File(lcsb.mapviewer.common.Configuration.getJarPath()).getParent();
    }
    logger.debug("Data path: {}", path);
    return path;
  }

}
