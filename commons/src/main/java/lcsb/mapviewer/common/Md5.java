package lcsb.mapviewer.common;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Md5 {
  private static Logger logger = LogManager.getLogger();

  public static String compute(final String data) {
    try {
      MessageDigest md = MessageDigest.getInstance("MD5");
      byte[] mdbytes = md.digest(data.getBytes());
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < mdbytes.length; i++) {
        // this magic formula transforms integer into hex value
        sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
      }
      return sb.toString();
    } catch (final NoSuchAlgorithmException e) {
      logger.fatal("Problem with instance of MD5 encoder", e);
    }

    return null;
  }
}
