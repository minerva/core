package lcsb.mapviewer.common;

/**
 * Mime content types of data used in the system. If there is publicly available
 * class with all possible types then we should use it, but I couldn't quickly
 * find it. For the full list see
 * <a href="http://www.iana.org/assignments/media-types/media-types.xhtml">IANA
 * MIME media types list</a>
 * 
 * @author Piotr Gawron
 * 
 */
public enum MimeType {
  /**
   * SBML type of file.
   */
  SBML("application/sbml+xml"),

  /**
   * Standard text file.
   */
  TEXT("text/plain"),

  /**
   * Standard XML file.
   */
  XML("application/xml"),

  /**
   * <a href="http://www.w3.org/TR/SVG11/mimereg.html">SVG</a> file type.
   */
  SVG("image/svg+xml"),

  /**
   * JPG image file type.
   */
  JPG("image/jpeg"),

  /**
   * PNG image file type.
   */
  PNG("image/png"),

  /**
   * PDF files (see <a href="http://www.rfc-editor.org/rfc/rfc3778.txt"> RFC 3778,
   * The application/pdf Media Type</a>).
   */
  PDF("application/pdf"),

  /**
   * CSS files.
   */
  CSS("text/css"),

  /**
   * JavaScript files.
   */
  JS("text/javascript"),

  /**
   * Zip files.
   */
  ZIP("application/zip");

  /**
   * String representation of the MIME content.
   */
  private String textRepresentation;

  /**
   * Default constructor with string definition.
   * 
   * @param textRepresentation
   *          text representation for MIME type
   */
  MimeType(final String textRepresentation) {
    this.textRepresentation = textRepresentation;
  }

  /**
   * @return the textRepresentation
   */
  public String getTextRepresentation() {
    return textRepresentation;
  }
}
