package lcsb.mapviewer.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.jar.JarInputStream;
import java.util.jar.Manifest;

/**
 * Basic configuration parameters of the system (these values cannot be modified
 * by the user, user modified values are kept in the db:
 * {@link lcsb.mapviewer.db.model.user.Configuration Configuration} ).
 *
 * @author Piotr Gawron
 */
public final class Configuration {

  /**
   * What is the minimal zoom level in the Google Maps API. It cannot be set to 0
   * because Google Maps API was designed to visualize map of Earth which is based
   * on torus. Therefore, if we use too small zoom level we will see next to the
   * right edge elements from the left part. When we increase minimal zoom level
   * there will be a gap (huge enough) between the overlapping parts and user will
   * be unaware of that fact.
   */
  public static final int MIN_ZOOM_LEVEL = 2;
  /**
   * This constant describes minimum size (in square pixels) of object visible
   * during nesting (it is a soft limit, can be override by depth of the depending
   * tree).
   */
  public static final double MIN_VISIBLE_OBJECT_SIZE = 55000;
  /**
   * This constant describes maximum size (in square pixels) of object visible
   * during nesting.
   */
  public static final double MAX_VISIBLE_OBJECT_SIZE = 80000;

  /**
   * Name of the cookie for authentication token.
   */
  public static final String AUTH_TOKEN = "MINERVA_AUTH_TOKEN";
  /**
   * Guest account.
   */
  public static final String ANONYMOUS_LOGIN = "anonymous";
  /**
   * Epsilon used for different types of comparisons.
   */
  public static final double EPSILON = 1e-6;

  public static final int HOMODIMER_OFFSET = 6;
  /**
   * Default value for {@link #memorySaturationRatioTriggerClean}. It defines at
   * what memory usage level application should release cached objects (to prevent
   * unnecessary out of memory exceptions).
   */
  private static final double DEFAULT_MEMORY_SATURATION_TRIGGER_CLEAN = 0.9;
  /**
   * How many elements should be visible in auto-complete lists.
   */
  private static final int DEFAULT_AUTOCOMPLETE_SIZE = 5;

  /**
   * Free space required to properly generate images.
   */
  private static long requiredFreeSpaceInBytes = 10L * 1024L * 1024L;

  /**
   * Default class logger.
   */
  private static final Logger logger = LogManager.getLogger();
  /**
   * Max session length in seconds.
   */
  private static int sessionLength = 60 * 120;
  /**
   * Should the application cache be turned on.
   */
  private static boolean applicationCacheOn = true;
  /**
   * This constant defines at what memory usage level application should release
   * cached objects (to prevent unnecessary out of memory exceptions).
   */
  private static Double memorySaturationRatioTriggerClean = DEFAULT_MEMORY_SATURATION_TRIGGER_CLEAN;
  /**
   * What is the size of auto-complete elements.
   */
  private static int autocompleteSize = DEFAULT_AUTOCOMPLETE_SIZE;
  /**
   * Git version from which framework was built.
   */
  private static String systemBuildVersion = null;

  /**
   * Date when the framework was built.
   */
  private static String systemBuildTime = null;

  /**
   * Version of the system (used by debian package).
   */
  private static String systemVersion = null;

  /**
   * Address that should be allowed to use x-frame.
   */
  private static List<String> xFrameDomain = new ArrayList<>();

  /**
   * Should CORS be disabled.
   */
  private static boolean disableCors = false;

  private static String JAR_PATH;

  public static final List<String> availableOAuthClients = Collections.singletonList("orcid");

  /**
   * Default constructor which prevents instantiation.
   */
  private Configuration() {

  }

  /**
   * @return the applicationCacheOn
   */
  public static boolean isApplicationCacheOn() {
    return applicationCacheOn;
  }

  /**
   * @param applicationCacheOn the applicationCacheOn to set
   */
  public static void setApplicationCacheOn(final boolean applicationCacheOn) {
    Configuration.applicationCacheOn = applicationCacheOn;
  }

  /**
   * @return the autocompleteSize
   */
  public static int getAutocompleteSize() {
    return autocompleteSize;
  }

  /**
   * @param autocompleteSize the autocompleteSize to set
   */
  public static void setAutocompleteSize(final int autocompleteSize) {
    Configuration.autocompleteSize = autocompleteSize;
  }

  /**
   * @param baseDir directory where the system is placed
   * @return {@link #systemBuildVersion}
   */
  public static String getSystemBuildVersion(final String baseDir) {
    if (systemBuildVersion == null) {
      loadSystemVersion(baseDir);
    }
    return systemBuildVersion;
  }

  /**
   * @param baseDir     directory where the system is placed
   * @param forceReload if true then forces to reload data from text file
   * @return {@link #systemBuildVersion}
   */
  public static String getSystemBuildVersion(final String baseDir, final boolean forceReload) {
    if (forceReload) {
      systemBuildVersion = null;
    }
    return getSystemBuildVersion(baseDir);
  }

  /**
   * @param baseDir directory where the system is placed
   * @return {@link #systemVersion}
   */
  public static String getSystemVersion(final String baseDir) {
    if (systemVersion == null) {
      loadSystemVersion(baseDir);
    }
    return systemVersion;
  }

  /**
   * @param baseDir     directory where the system is placed
   * @param forceReload if true then forces to reload data from text file
   * @return {@link #systemBuildVersion}
   */
  public static String getSystemVersion(final String baseDir, final boolean forceReload) {
    if (forceReload) {
      systemVersion = null;
    }
    return getSystemVersion(baseDir);
  }

  /**
   * Loads system version (git version, build date) from the file.
   *
   * @param baseDir directory where the system is placed
   */
  static void loadSystemVersion(final String baseDir) {
    systemBuildVersion = "Unknown";
    systemBuildTime = "Unknown";
    systemVersion = "Unknown";

    if (baseDir != null) {
      loadSystemBuildVersion(baseDir);
    } else if (JAR_PATH != null) {
      loadSystemBuildVersion(JAR_PATH);
    } else {

      logger.error("Cannot find MANIFEST.MF file.", new Exception());
    }
  }

  /**
   * Loads system version (git version, build date) from the file.
   *
   * @param filename file from which data is loaded
   */
  private static void loadSystemBuildVersion(final String filename) {
    try {
      boolean problem = false;
      File file = new File(filename);
      Manifest manifest;
      if (file.isDirectory()) {
        manifest = new Manifest(new FileInputStream(new File(filename + "/META-INF/MANIFEST.MF")));
      } else {
        JarInputStream jarStream = new JarInputStream(new FileInputStream(new File(filename)));
        manifest = new Manifest(jarStream.getManifest());
      }

      systemBuildTime = manifest.getMainAttributes().getValue("git-timestamp");
      if (systemBuildTime == null) {
        systemBuildTime = "Unknown";
        problem = true;
      }
      systemBuildVersion = manifest.getMainAttributes().getValue("git-version");
      if (systemBuildVersion == null) {
        systemBuildVersion = "Unknown";
        problem = true;
      }
      systemVersion = manifest.getMainAttributes().getValue("version");
      if (systemVersion == null) {
        systemVersion = "Unknown";
        problem = true;
      }

      if (problem) {
        logger.warn("MANIFEST.MF file does not contain required information.");
      }
    } catch (final IOException e) {
      logger.error(e);
    }
  }

  /**
   * @param baseDir directory where the system is placed
   * @return the systemBuild
   * @see #systemBuildTime
   */
  public static String getSystemBuild(final String baseDir) {
    if (systemBuildTime == null) {
      loadSystemVersion(baseDir);
    }
    return systemBuildTime;
  }

  /**
   * @param baseDir     directory where the system is placed
   * @param forceReload if true then forces to reload data from text file
   * @return the systemBuild
   * @see #systemBuildTime
   */
  public static String getSystemBuild(final String baseDir, final boolean forceReload) {
    if (forceReload) {
      systemBuildTime = null;
    }
    return getSystemBuild(baseDir);
  }

  /**
   * @return the xFrametDomain
   * @see #xFrameDomain
   */
  public static List<String> getxFrameDomain() {
    return xFrameDomain;
  }

  /**
   * @param domains the xFrametDomain to set
   * @see #xFrameDomain
   */
  public static void setxFrameDomain(final List<String> domains) {
    Configuration.xFrameDomain = domains;
  }

  /**
   * @return the memorySaturationRatioTriggerClean
   * @see #memorySaturationRatioTriggerClean
   */
  public static Double getMemorySaturationRatioTriggerClean() {
    return memorySaturationRatioTriggerClean;
  }

  /**
   * @param memorySaturationRatioTriggerClean the memorySaturationRatioTriggerClean to set
   * @see #memorySaturationRatioTriggerClean
   */
  public static void setMemorySaturationRatioTriggerClean(final Double memorySaturationRatioTriggerClean) {
    Configuration.memorySaturationRatioTriggerClean = memorySaturationRatioTriggerClean;
  }

  /**
   * Returns information about version of the framework.
   *
   * @param baseDir directory where project was deployed (or information about version
   *                is stored)
   * @return information about version of the framework
   */
  public static FrameworkVersion getFrameworkVersion(final String baseDir) {
    FrameworkVersion result = new FrameworkVersion();
    loadSystemVersion(baseDir);
    result.setGitVersion(getSystemBuildVersion(baseDir));
    result.setTime(getSystemBuild(baseDir));
    result.setVersion(getSystemVersion(baseDir));
    return result;
  }

  public static int getSessionLength() {
    return sessionLength;
  }

  public static void setSessionLength(final int sessionLength) {
    Configuration.sessionLength = sessionLength;
  }

  public static boolean isDisableCors() {
    return disableCors;
  }

  public static void setDisableCors(final boolean disableCors) {
    Configuration.disableCors = disableCors;
  }

  public static long getRequiredFreeSpaceInBytes() {
    return requiredFreeSpaceInBytes;
  }

  public static void setRequiredFreeSpaceInBytes(final long requiredFreeSpaceInBytes) {
    Configuration.requiredFreeSpaceInBytes = requiredFreeSpaceInBytes;
  }

  public static void setJarPath(final String jarPath) {
    JAR_PATH = jarPath;
  }

  public static String getJarPath() {
    return JAR_PATH;
  }

}
