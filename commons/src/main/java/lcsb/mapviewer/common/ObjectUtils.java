package lcsb.mapviewer.common;

import java.lang.reflect.Method;

import lcsb.mapviewer.common.exception.InvalidArgumentException;

/**
 * Util class that performs primitive operations on the object.
 * 
 * @author Piotr Gawron
 * 
 */
public final class ObjectUtils {

  /**
   * Default constructor. Prevents instatiation.
   */
  private ObjectUtils() {
  }

  /**
   * Returns object identifier. It assumes that object contains getId method that
   * returns Integer.
   * 
   * @param object
   *          object for which identifier is looked for
   * @return object identifier
   */
  public static Integer getIdOfObject(final Object object) {
    Integer id = (Integer) getParamByGetter(object, "getId");
    return id;
  }

  /**
   * Returns object param using string getter function.
   * 
   * @param object
   *          object from which parameter will be taken
   * @param getterName
   *          string with name of the getter function
   * @return object field
   */
  public static Object getParamByGetter(final Object object, final String getterName) {
    try {
      Method method = object.getClass().getMethod(getterName);
      Object result = method.invoke(object);
      return result;
    } catch (final Exception e) {
      throw new InvalidArgumentException(e.getMessage());
    }
  }
}
