package lcsb.mapviewer.common;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;

/**
 * Abstract class with methods which help in parsing xml using DOM.
 * 
 * @author Piotr Gawron
 * 
 */
public final class XmlParser {

  /**
   * Base of the hex representation.
   */
  private static final int HEX_BASE = 16;
  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();
  /**
   * {@link DocumentBuilder} objects that will manipulate xml nodes.
   */
  private static DocumentBuilder validatedDocumentBuilder;
  private static DocumentBuilder nonValidatedDocumentBuilder;

  static {
    DocumentBuilderFactory nonValidateDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
    nonValidateDocumentBuilderFactory.setNamespaceAware(false);
    nonValidateDocumentBuilderFactory.setValidating(false);
    try {
      nonValidateDocumentBuilderFactory.setFeature(
          "http://xml.org/sax/features/namespaces", false);
      nonValidateDocumentBuilderFactory.setFeature(
          "http://xml.org/sax/features/validation", false);
      nonValidateDocumentBuilderFactory.setFeature(
          "http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
      nonValidateDocumentBuilderFactory.setFeature(
          "http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
      nonValidatedDocumentBuilder = nonValidateDocumentBuilderFactory.newDocumentBuilder();
      validatedDocumentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    } catch (final ParserConfigurationException e) {
      logger.error(e, e);
    }
  }

  /**
   * private constructor. This class has no state and contains only static utility
   * methods.
   */
  private XmlParser() {
  }

  /**
   * Method returns the node of xml nodelist 'nodes' with 'tagName' name. If node
   * could not be found then null is returned.
   * 
   * @param tagName
   *          name of node to look for
   * @param nodes
   *          list of nodes
   * @return node from nodes list with the tagName name, <b>null</b> if such node
   *         doesn't exist
   */
  public static Node getNode(final String tagName, final NodeList nodes, final boolean ignoreNamespace) {
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase(tagName)) {
          return node;
        }
        if (ignoreNamespace) {
          if (node.getNodeName().toLowerCase().endsWith(":" + tagName.toLowerCase())) {
            return node;
          }
        }
      }
    }
    return null;
  }

  public static Node getNode(final String tagName, final NodeList nodes) {
    return getNode(tagName, nodes, false);
  }

  /**
   * Method returns the child node of xml 'parentNode' with 'tagName' name. If
   * node could not be found then null is returned.
   * 
   * @param tagName
   *          name of node to look for
   * @param parentNode
   *          parent node
   * @return node from nodes list with the tagName name, <b>null</b> if such node
   *         doesn't exist
   */
  public static Node getNode(final String tagName, final Node parentNode) {
    return getNode(tagName, parentNode.getChildNodes());
  }

  /**
   * Method returns list of nodes with 'tagName' name. If node could not be found
   * then empty list is returned.
   * 
   * @param tagName
   *          name of node to look for
   * @param nodes
   *          list of input nodes
   * @return list of nodes with 'tagName' name
   */
  public static List<Node> getNodes(final String tagName, final NodeList nodes) {
    List<Node> result = new ArrayList<Node>();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeName().equalsIgnoreCase(tagName)) {
        result.add(node);
      }
    }
    return result;
  }

  /**
   * Method returns the value of node attribute. If attribute could not be found
   * then "" is returned.
   * 
   * @param attrName
   *          name of the attribute to look for
   * @param node
   *          a node
   * @return the value of node attribute, empty string("") if attribute doesn't
   *         exist
   */
  public static String getNodeAttr(final String attrName, final Node node) {
    NamedNodeMap attrs = node.getAttributes();
    for (int y = 0; y < attrs.getLength(); y++) {
      Node attr = attrs.item(y);
      if (attr.getNodeName().equalsIgnoreCase(attrName)) {
        return attr.getNodeValue();
      }
    }
    return "";
  }

  /**
   * Method returns the text value of node. If text could not be found then "" is
   * returned.
   * 
   * @param node
   *          a node
   * @return the text value of node or empty string ("") if the text could be
   *         found.
   */
  public static String getNodeValue(final Node node) {
    if (node == null) {
      return "";
    }
    NodeList childNodes = node.getChildNodes();
    for (int x = 0; x < childNodes.getLength(); x++) {
      Node data = childNodes.item(x);
      if (data.getNodeType() == Node.TEXT_NODE) {
        return data.getNodeValue();
      }
    }
    return "";
  }

  /**
   * Method returns the xml Document from input source given as a parameter.
   * 
   * @param stream
   *          input stream with xml document
   * @return Document node for the input stream
   * @throws InvalidXmlSchemaException
   *           thrown when there is a problem with xml
   */
  static synchronized Document getXmlDocumentFromInputSource(final InputSource stream, final boolean validate)
      throws InvalidXmlSchemaException {
    try {
      if (validate) {
        return validatedDocumentBuilder.parse(stream);
      } else {
        return nonValidatedDocumentBuilder.parse(stream);
      }
    } catch (final SAXException | IOException e) {
      throw new InvalidXmlSchemaException("Problem with xml parser", e);
    }
  }

  /**
   * Method returns the xml Document from text given as a source.
   * 
   * @param text
   *          string representing xml document
   * @param validate
   *          should the parser validate the input (if validation is off it's much
   *          faster in some cases, especially when dtd files don't have to be
   *          downloaded from web)
   * @return Document for the xml document given in the input
   * @throws InvalidXmlSchemaException
   *           thrown when there is a problem with xml
   */
  public static Document getXmlDocumentFromString(final String text, final boolean validate)
      throws InvalidXmlSchemaException {
    InputSource is = new InputSource();
    is.setCharacterStream(new StringReader(text));
    try {
      return getXmlDocumentFromInputSource(is, validate);
    } catch (final NullPointerException e) {
      logger.error("Problem with input xml: " + text);
      throw new InvalidXmlSchemaException(e);
    }
  }

  /**
   * Method returns the xml Document from text given as a source.
   * 
   * @param text
   *          string representing xml document
   * @return Document for the xml document given in the input
   * @throws InvalidXmlSchemaException
   *           thrown when there is a problem with xml
   */
  public static Document getXmlDocumentFromString(final String text) throws InvalidXmlSchemaException {
    return getXmlDocumentFromString(text, true);
  }

  /**
   * Transforms node into string xml format.
   * 
   * @param node
   *          node that should be transformed into xml string
   * @return string representation of the xml node
   */
  public static String nodeToString(final Node node) {
    return nodeToString(node, false);
  }

  /**
   * Transforms node into string xml format.
   * 
   * @param node
   *          node that should be transformed into xml string
   * @param includeHeadNode
   *          should the top level node exist in the output
   * @return string representation of the xml node
   */
  public static String nodeToString(final Node node, final boolean includeHeadNode) {
    if (node == null) {
      return null;
    }
    StringWriter sw = new StringWriter();
    try {
      Transformer t = TransformerFactory.newInstance().newTransformer();
      t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
      t.setOutputProperty(OutputKeys.INDENT, "yes");
      t.setOutputProperty(OutputKeys.METHOD, "xml");

      if (includeHeadNode) {
        t.transform(new DOMSource(node), new StreamResult(sw));
      } else {
        NodeList list = node.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
          Node element = list.item(i);
          t.transform(new DOMSource(element), new StreamResult(sw));
        }
      }
    } catch (final TransformerException te) {
      logger.debug("nodeToString Transformer Exception");
    }
    return sw.toString();
  }

  /**
   * This method transform color encoded in string (final CellDesigner format)
   * into Color.
   * 
   * @param color
   *          string representing color
   * @return Color object for the given string
   */
  public static Color stringToColor(final String color) {
    try {
      String alpha = color.substring(0, 2);
      Color tmp = new Color(hexToInteger(color.substring(2)));
      return new Color(tmp.getRed(), tmp.getGreen(), tmp.getBlue(), hexToInteger(alpha));
    } catch (final Exception e) {
      throw new InvalidArgumentException("Invalid color string: " + color);
    }
  }

  /**
   * Transforms hex string into Integer.
   * 
   * @param hexString
   *          string representation in hex base
   * @return Integer value of the hex string
   */
  static Integer hexToInteger(final String hexString) {
    return Integer.valueOf(hexString, HEX_BASE);
  }

  /**
   * Transforms Color object into string representing this color in RGB.
   * 
   * @param color
   *          color that should be converted into string
   * @return hex string representation of the color
   */
  public static String colorToString(final Color color) {
    return String.format("%08X", color.getRGB());
  }

  /**
   * Method that reads file and transforms it into a string.
   * 
   * @param fileName
   *          path to a file
   * @return string containing data from the file (default coding is used)
   * @throws IOException
   *           thrown when there are some problems with a file
   */
  static String fileToString(final String fileName) throws IOException {
    BufferedReader reader = new BufferedReader(new FileReader(fileName));
    String line = null;
    StringBuilder stringBuilder = new StringBuilder();
    String ls = System.getProperty("line.separator");

    while ((line = reader.readLine()) != null) {
      stringBuilder.append(line);
      stringBuilder.append(ls);
    }
    reader.close();

    return stringBuilder.toString();
  }

  /**
   * Method that reads all data from inputstream and transform it into a string.
   * UTF-8 coding is used.
   * 
   * @param inputStream
   *          stream from which we read data
   * @return string representing all data from input stream
   * @throws IOException
   *           thrown if there are some problems with input stream
   */
  static String inputStreamToString(final InputStream inputStream) throws IOException {
    StringWriter writer = new StringWriter();
    IOUtils.copy(inputStream, writer, "UTF-8");
    return writer.toString();
  }

  /**
   * Method that encode string into a string that can be used in xml file.
   * 
   * @param string
   *          string to be escaped
   * @return escaped string, ready to be used in xml
   */
  public static String escapeXml(final String string) {
    if (string == null) {
      return null;
    }
    // quite expensive
    return StringEscapeUtils.escapeXml10(string).replaceAll("\n", "&#10;").replace("\r", "&#13;");
  }

  public static String unescapeXml(final String string) {
    if (string == null) {
      return null;
    }
    // quite expensive
    return StringEscapeUtils.unescapeXml(string.replaceAll("&#10;", "\n").replace("&#13;", "\r"));
  }

  public static List<Node> getAllNotNecessirellyDirectChild(final String tagName, final Node root) {
    List<Node> result = new ArrayList<>();
    for (int x = 0; x < root.getChildNodes().getLength(); x++) {
      Node node = root.getChildNodes().item(x);
      if (node.getNodeName().equalsIgnoreCase(tagName)) {
        result.add(node);
      } else {
        result.addAll(getAllNotNecessirellyDirectChild(tagName, node));
      }
    }
    return result;
  }

  public static String lowercaseXmlNames(final String inputXml) {
    try {
      TransformerFactory factory = TransformerFactory.newInstance();

      // Use the factory to create a template containing the xsl file
      Templates template = factory
          .newTemplates(new StreamSource(ClassLoader.getSystemResourceAsStream("to_lowercase.xsl")));

      // Use the template to create a transformer
      Transformer xformer = template.newTransformer();

      ByteArrayOutputStream output = new ByteArrayOutputStream();
      // Prepare the input and output files
      Source source = new StreamSource(new ByteArrayInputStream(inputXml.getBytes()));
      Result result = new StreamResult(output);

      // Apply the xsl file to the source file and write the result
      // to the output file
      xformer.transform(source, result);
      return output.toString("UTF-8");
    } catch (final Exception e) {
      throw new InvalidStateException("Problem with translating input xml", e);
    }
  }

}
