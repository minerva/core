package lcsb.mapviewer.common;

import java.io.Serializable;

/**
 * View representation of the {@link FrameworkVersion}. Contains information
 * about single framework version.
 * 
 * @author Piotr Gawron
 * 
 */
public class FrameworkVersion implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Version of the framework.
   */
  private String version;

  /**
   * When this version was committed to git.
   */
  private String gitVersion;

  /**
   * When the framework was built.
   */
  private String time;

  /**
   * Default constructor.
   * 
   */
  public FrameworkVersion() {
  }

  /**
   * @return the version
   * @see #version
   */
  public String getVersion() {
    return version;
  }

  /**
   * @param version
   *          the version to set
   * @see #version
   */
  public void setVersion(final String version) {
    this.version = version;
  }

  /**
   * @return the gitVersion
   * @see #gitVersion
   */
  public String getGitVersion() {
    return gitVersion;
  }

  /**
   * @param gitVersion
   *          the gitVersion to set
   * @see #gitVersion
   */
  public void setGitVersion(final String gitVersion) {
    this.gitVersion = gitVersion;
  }

  /**
   * @return the time
   * @see #time
   */
  public String getTime() {
    return time;
  }

  /**
   * @param time
   *          the time to set
   * @see #time
   */
  public void setTime(final String time) {
    this.time = time;
  }

}
