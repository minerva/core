package lcsb.mapviewer.common.exception;

/**
 * Exception thrown when the class of the object is invalid.
 * 
 * @author Piotr Gawron
 * 
 */
public class InvalidClassException extends RuntimeException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor with message passed in the argument.
   * 
   * @param string
   *          message of this exception
   */
  public InvalidClassException(final String string) {
    super(string);
  }

  /**
   * Default constructor with message passed in the argument and super exception.
   * 
   * @param string
   *          message of this exception
   * @param e
   *          exception caught and passed to this object
   */
  public InvalidClassException(final String string, final Exception e) {
    super(string, e);
  }
}
