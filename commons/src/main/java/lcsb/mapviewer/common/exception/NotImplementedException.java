package lcsb.mapviewer.common.exception;

/**
 * Exception that should be thrown when application/class/method entered to the
 * part of code that wasn't implemented (for instance the class is not abstract,
 * but method should be considered abstract...).
 * 
 * @author Piotr Gawron
 * 
 */
public class NotImplementedException extends RuntimeException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public NotImplementedException() {
    super();
  }

  /**
   * Default constructor with message passed in the argument.
   * 
   * @param string
   *          message of this exception
   */
  public NotImplementedException(final String string) {
    super(string);
  }

}
