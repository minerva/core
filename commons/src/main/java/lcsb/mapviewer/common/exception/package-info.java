/**
 * Common exceptions thrown by the system.
 */
package lcsb.mapviewer.common.exception;
