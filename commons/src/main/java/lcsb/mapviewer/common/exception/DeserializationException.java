package lcsb.mapviewer.common.exception;

import com.fasterxml.jackson.core.JsonProcessingException;

public class DeserializationException extends JsonProcessingException {

  public DeserializationException(final String msg) {
    super(msg);
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

}
