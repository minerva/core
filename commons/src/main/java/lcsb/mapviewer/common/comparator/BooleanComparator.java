package lcsb.mapviewer.common.comparator;

import java.util.Comparator;

/**
 * Comparator implemented for {@link Boolean} class.
 * 
 * @author Piotr Gawron
 * 
 */
public class BooleanComparator implements Comparator<Boolean> {

  @Override
  public int compare(final Boolean arg0, final Boolean arg1) {
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }
    } else if (arg1 == null) {
      return -1;
    }
    return arg0.compareTo(arg1);
  }

}
