package lcsb.mapviewer.common.comparator;

import java.util.Comparator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class AbstractComparator<T> implements Comparator<T> {
  private Logger logger = LogManager.getLogger();

  public int compare(final T o1, final T o2, final String objectName) {
    int result = compare(o1, o2);
    if (result != 0) {
      logger.debug(objectName + " different: " + o1 + "\t" + o2);
    }
    return result;
  }
}
