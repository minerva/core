package lcsb.mapviewer.common.comparator;

import lcsb.mapviewer.common.Configuration;

/**
 * Comparator used for {@link Double} class.
 * 
 * @author Piotr Gawron
 * 
 */
public class DoubleComparator extends AbstractComparator<Double> {

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Default constructor.
   */
  public DoubleComparator() {
    this(Configuration.EPSILON);
  }

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public DoubleComparator(final double epsilon) {
    this.epsilon = epsilon;
  }

  @Override
  public int compare(final Double arg0, final Double arg1) {
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }

    } else if (arg1 == null) {
      return -1;
    }
    if (Math.abs(arg0 - arg1) < epsilon) {
      return 0;
    } else {
      return arg0.compareTo(arg1);
    }
  }

}
