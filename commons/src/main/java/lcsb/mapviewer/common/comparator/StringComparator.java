package lcsb.mapviewer.common.comparator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Comparator used for {@link String} class. It's null safe (it allows to
 * compare strings that are null).
 *
 * @author Piotr Gawron
 */
public class StringComparator extends AbstractComparator<String> {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger();

  @Override
  public int compare(final String arg0, final String arg1) {
    return compare(arg0, arg1, false);
  }

  /**
   * Allows to compare two strings ignoring whitespace difference.
   *
   * @param arg0                       first string to compare
   * @param arg1                       second string to compare
   * @param ignoreWhiteSpaceDifference should the difference in whitespace be ignored
   * @return 0 when strings are identical, -1/1 when they are different
   */
  public int compare(final String arg0, final String arg1, final boolean ignoreWhiteSpaceDifference) {
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }
    } else if (arg1 == null) {
      return -1;
    }

    if (ignoreWhiteSpaceDifference) {
      final String str1 = arg0.replaceAll("[\n\r\t\\ ]+", "\n");
      final String str2 = arg1.replaceAll("[\n\r\t\\ ]+", "\n");
      return str1.trim().compareTo(str2.trim());
    }
    return arg0.trim().compareTo(arg1.trim());
  }

  public int compareIgnoreCase(final String arg0, final String arg1) {
    String str0 = arg0;
    if (arg0 != null) {
      str0 = arg0.toUpperCase();
    }
    String str1 = arg1;
    if (arg1 != null) {
      str1 = arg1.toUpperCase();
    }
    return compare(str0, str1, false);
  }
}
