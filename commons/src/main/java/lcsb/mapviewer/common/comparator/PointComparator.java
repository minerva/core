package lcsb.mapviewer.common.comparator;

import java.awt.geom.Point2D;
import java.util.Comparator;

import lcsb.mapviewer.common.Configuration;

/**
 * Comparator used for {@link Point2D} class.
 * 
 * @author Piotr Gawron
 * 
 */
public class PointComparator implements Comparator<Point2D> {

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public PointComparator(final double epsilon) {
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public PointComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  public int compare(final Point2D arg0, final Point2D arg1) {
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }

    } else if (arg1 == null) {
      return -1;
    }
    if (Math.abs(arg0.distance(arg1)) < epsilon) {
      return 0;
    } else {
      // this could be modified to assure monotonousness of comparison
      return -1;
    }
  }

}
