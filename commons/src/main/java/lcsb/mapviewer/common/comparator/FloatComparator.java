package lcsb.mapviewer.common.comparator;

import java.util.Comparator;

import lcsb.mapviewer.common.Configuration;

/**
 * Comparator used for {@link Float} class.
 * 
 * @author Piotr Gawron
 * 
 */
public class FloatComparator implements Comparator<Float> {

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Default constructor.
   */
  public FloatComparator() {
    this(Configuration.EPSILON);
  }

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public FloatComparator(final double epsilon) {
    this.epsilon = epsilon;
  }

  @Override
  public int compare(final Float arg0, final Float arg1) {
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }

    } else if (arg1 == null) {
      return -1;
    }
    if (Math.abs(arg0 - arg1) < epsilon) {
      return 0;
    } else {
      return arg0.compareTo(arg1);
    }
  }

}
