package lcsb.mapviewer.common.comparator;

import java.util.Calendar;
import java.util.Comparator;

/**
 * Comparator implementation for {@link Calendar} class.
 * 
 * @author Piotr Gawron
 * 
 */
public class CalendarComparator implements Comparator<Calendar> {

  @Override
  public int compare(final Calendar arg0, final Calendar arg1) {
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }

    } else if (arg1 == null) {
      return -1;
    }
    return ((Long) arg0.getTimeInMillis()).compareTo(arg1.getTimeInMillis());
  }

}
