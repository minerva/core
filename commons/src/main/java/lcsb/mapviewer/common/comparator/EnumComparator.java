package lcsb.mapviewer.common.comparator;

import java.awt.Color;
import java.util.Comparator;

/**
 * Comparator implementation for {@link Color} class.
 * 
 * @author Piotr Gawron
 * 
 */
public class EnumComparator<E extends Enum<E>> implements Comparator<E> {

  @Override
  public int compare(final E arg0, final E arg1) {
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }

    } else if (arg1 == null) {
      return -1;
    }
    return arg0.compareTo(arg1);
  }

}
