package lcsb.mapviewer.common.comparator;

import java.util.Comparator;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Comparator used for comparing sets of strings.
 * 
 * @author Piotr Gawron
 * 
 */
public class SetComparator<T> implements Comparator<Set<T>> {
  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger();

  private Comparator<T> objectComparator;

  public SetComparator(final Comparator<T> objectComparator) {
    this.objectComparator = objectComparator;
  }

  @Override
  public int compare(final Set<T> arg0, final Set<T> arg1) {
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }
    } else if (arg1 == null) {
      return -1;
    }

    for (final T objectInList1 : arg1) {
      boolean found = false;
      for (final T objectInList0 : arg0) {
        if (objectComparator.compare(objectInList0, objectInList1) == 0) {
          found = true;
        }
      }
      if (!found) {
        logger.debug("Cannot find object " + objectInList1 + " in set: " + arg0);
        return 1;
      }
    }

    for (final T objectInList0 : arg0) {
      boolean found = false;
      for (final T objectInList1 : arg1) {
        if (objectComparator.compare(objectInList0, objectInList1) == 0) {
          found = true;
        }
      }
      if (!found) {
        new Exception().printStackTrace();
        logger.debug("Cannot find object " + objectInList0 + " in set: " + arg1);
        return 1;
      }
    }
    return 0;
  }

  public Comparator<T> getObjectComparator() {
    return objectComparator;
  }

  public void setObjectComparator(final Comparator<T> objectComparator) {
    this.objectComparator = objectComparator;
  }

}
