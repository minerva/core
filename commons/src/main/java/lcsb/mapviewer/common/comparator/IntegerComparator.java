package lcsb.mapviewer.common.comparator;

/**
 * Comparator implemented for {@link Integer} class.
 * 
 * @author Piotr Gawron
 * 
 */
public class IntegerComparator extends AbstractComparator<Integer> {

  @Override
  public int compare(final Integer arg0, final Integer arg1) {
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }
    } else if (arg1 == null) {
      return -1;
    }
    return arg0.compareTo(arg1);
  }

}
