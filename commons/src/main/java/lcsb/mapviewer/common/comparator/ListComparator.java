package lcsb.mapviewer.common.comparator;

import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Comparator used for comparing lists of objects.
 * 
 * @author Piotr Gawron
 * 
 */
public class ListComparator<T> implements Comparator<List<T>> {
  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger();

  private Comparator<T> objectComparator;

  public ListComparator(final Comparator<T> objectComparator) {
    this.objectComparator = objectComparator;
  }

  @Override
  public int compare(final List<T> arg0, final List<T> arg1) {
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }
    } else if (arg1 == null) {
      return -1;
    }

    if (arg0.size() != arg1.size()) {
      new Exception().printStackTrace();
      logger.debug("Number of elements in list different: " + arg0.size() + ", " + arg1.size());
      return new IntegerComparator().compare(arg0.size(), arg1.size());
    }
    for (int i = 0; i < arg0.size(); i++) {
      if (objectComparator.compare(arg0.get(i), arg1.get(i)) != 0) {
        return objectComparator.compare(arg0.get(i), arg1.get(i));
      }
    }
    return 0;
  }

}
