package lcsb.mapviewer.converter.graphics;

/**
 * Exception thrown when there are some problems with drawing.
 * 
 * @author Piotr Gawron
 * 
 */
public class DrawingException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   * 
   * @param string
   *          exception message
   * @param e
   *          super exception
   */
  public DrawingException(final String string, final Exception e) {
    super(string, e);
  }

  /**
   * Default constructor.
   * 
   * @param string
   *          exception message
   */
  public DrawingException(final String string) {
    super(string);
  }

}
