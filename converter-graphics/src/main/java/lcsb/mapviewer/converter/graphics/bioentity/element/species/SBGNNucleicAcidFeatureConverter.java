package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.util.ArrayList;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This class defines methods used for drawing {@link Species} of nucleic acid
 * feature in the SBGN way on the {@link Graphics2D} object.
 * 
 * @author Michał Kuźma
 *
 */

public class SBGNNucleicAcidFeatureConverter extends SpeciesConverter<Species> {

  /**
   * How big should be the arc in rectangle for nucleic acid feature
   * representation.
   */
  private static final int RECTANGLE_CORNER_ARC_SIZE = 5;

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into colors
   *          when drawing {@link Species}
   */
  public SBGNNucleicAcidFeatureConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  /**
   * Returns shape of SBGN Nucleic acid feature.
   * 
   * @param species
   *          {@link Species} for which the shape should be returned
   * @return shape of the SBGN Nucleic acid feature for given alias
   */
  @Override
  protected Shape getShape(final Species species) {
    GeneralPath bottomRoundedRectangle = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
    double x = species.getX();
    double y = species.getY();
    double width = species.getWidth();
    double height = species.getHeight();

    bottomRoundedRectangle.moveTo(x, y);
    bottomRoundedRectangle.lineTo(x, y + height - RECTANGLE_CORNER_ARC_SIZE);
    bottomRoundedRectangle.curveTo(x, y + height, x + RECTANGLE_CORNER_ARC_SIZE, y + height,
        x + RECTANGLE_CORNER_ARC_SIZE, y + height);
    bottomRoundedRectangle.lineTo(x + width - RECTANGLE_CORNER_ARC_SIZE, y + height);
    bottomRoundedRectangle
        .curveTo(x + width, y + height, x + width, y + height - RECTANGLE_CORNER_ARC_SIZE, x + width,
            y + height - RECTANGLE_CORNER_ARC_SIZE);
    bottomRoundedRectangle.lineTo(x + width, y);
    bottomRoundedRectangle.closePath();
    return bottomRoundedRectangle;
  }

  @Override
  protected void drawImpl(final Species species, final Graphics2D graphics, final ConverterParams params) {
    // Unit of information text - multimer cardinality
    java.util.List<String> unitOfInformationText = new ArrayList<>();
    if (species.getStatePrefix() != null && species.getStateLabel() != null) {
      if (species.getStatePrefix().equals("free input")) {
        unitOfInformationText.add(species.getStateLabel());
      } else {
        unitOfInformationText.add(species.getStatePrefix() + ":" + species.getStateLabel());
      }
    }

    int homodir = species.getHomodimer();

    species.setWidth(species.getWidth() - SpeciesConverter.HOMODIMER_OFFSET * (species.getHomodimer() - 1));
    species.setHeight(species.getHeight() - SpeciesConverter.HOMODIMER_OFFSET * (species.getHomodimer() - 1));

    species.setX(species.getX() + SpeciesConverter.HOMODIMER_OFFSET * homodir);
    species.setY(species.getY() + SpeciesConverter.HOMODIMER_OFFSET * homodir);

    int glyphCount;
    if (homodir > 1) {
      glyphCount = 2;
    } else {
      glyphCount = 1;
    }
    for (int i = 0; i < glyphCount; i++) {
      species.setX(species.getX() - SpeciesConverter.HOMODIMER_OFFSET);
      species.setY(species.getY() - SpeciesConverter.HOMODIMER_OFFSET);

      Shape shape = getShape(species);

      Color c = graphics.getColor();
      graphics.setColor(species.getFillColor());
      graphics.fill(shape);
      graphics.setColor(c);

      Stroke stroke = graphics.getStroke();
      graphics.setStroke(getBorderLine(species));
      graphics.draw(shape);
      graphics.setStroke(stroke);

    }

    if (homodir > 1) {
      unitOfInformationText.add("N:" + Integer.toString(homodir));
    }

    drawUnitOfInformation(unitOfInformationText, species, graphics);
    drawText(species, graphics, params);

    species.setWidth(species.getWidth() + SpeciesConverter.HOMODIMER_OFFSET * (homodir - 1));
    species.setHeight(species.getHeight() + SpeciesConverter.HOMODIMER_OFFSET * (homodir - 1));
  }

  @Override
  protected PathIterator getBoundPathIterator(final Species species) {
    return getShape(species).getPathIterator(new AffineTransform());
  }

}
