package lcsb.mapviewer.converter.graphics.layer;

import java.awt.Color;
import java.awt.Graphics2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.converter.graphics.geometry.ArrowTransformation;
import lcsb.mapviewer.model.graphics.PolylineData;

/**
 * This class allows to draw {@link PolylineData} on Graphics2D.
 * 
 * @author Piotr Gawron
 * 
 */
public class LayerLineConverter {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  /**
   * This objects helps drawing arrows.
   */
  private ArrowTransformation arrowTransformation = new ArrowTransformation();

  /**
   * Draws line on the Graphics2D.
   * 
   * @param line
   *          object to be drawn
   * @param graphics
   *          where we want to draw the object
   */
  public void draw(final PolylineData line, final Graphics2D graphics) {
    Color tmpColor = graphics.getColor();
    graphics.setColor(line.getColor());
    arrowTransformation.drawLine(line, graphics);

    graphics.setColor(tmpColor);

  }
}