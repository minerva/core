package lcsb.mapviewer.converter.graphics;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;

import lcsb.mapviewer.common.MimeType;

/**
 * This implementation of {@link AbstractImageGenerator} allows to generate JPG
 * images.
 * 
 * @author Piotr Gawron
 * 
 */
public class JpgImageGenerator extends NormalImageGenerator {

  /**
   * Default constructor. Create an image that is described by params. For more
   * information see
   * {@link lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params
   * params}.
   * 
   * @param params
   *          parameters used for the image creation.
   * @throws DrawingException
   *           thrown when there was a problem with drawing a map
   */
  public JpgImageGenerator(final Params params) throws DrawingException {
    super(params);
  }

  @Override
  protected void closeImageObject() {
  }

  @Override
  public void saveToFileImplementation(final String fileName) throws IOException {
    FileOutputStream fos = new FileOutputStream(new File(fileName));
    saveToOutputStreamImplementation(fos);
    fos.close();
  }

  @Override
  public void saveToOutputStreamImplementation(final OutputStream os) throws IOException {
    ImageIO.write(getBi(), "JPG", os);
  }

  @Override
  public void savePartToFileImplementation(final int x, final int y, final int width, final int height, final String fileName) throws IOException {
    FileOutputStream fos = new FileOutputStream(new File(fileName));
    savePartToOutputStreamImplementation(x, y, width, height, fos);
    fos.close();
  }

  @Override
  public void savePartToOutputStreamImplementation(final int x, final int y, final int width, final int height, final OutputStream os)
      throws IOException {
    BufferedImage tmpBI = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    Graphics2D tmpGraphics = tmpBI.createGraphics();
    tmpGraphics.drawImage(getBi(), 0, 0, width, height, x, y, x + width, y + width, null);
    ImageIO.write(tmpBI, "JPG", os);
  }

  @Override
  public String getFormatName() {
    return "JPG image";
  }

  @Override
  public MimeType getMimeType() {
    return MimeType.JPG;
  }

  @Override
  public String getFileExtension() {
    return "jpg";
  }

}
