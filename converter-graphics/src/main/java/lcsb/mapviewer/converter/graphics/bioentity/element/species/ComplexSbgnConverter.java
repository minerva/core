package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;

/**
 * This class defines methods used for drawing ComplexAlias on the graphics2d
 * object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ComplexSbgnConverter extends ComplexConverter {

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link DataOverlayEntry} values into
   *          colors when drawing {@link Species}
   */
  public ComplexSbgnConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected void drawImpl(final Complex complex, final Graphics2D graphics, final ConverterParams params) {
    if (complex.getState().equalsIgnoreCase("complexnoborder")) {
      return;
    }

    int oldHomodimer = complex.getHomodimer();
    int homodir;
    if (complex.getHomodimer() > 1) {
      homodir = 2;
    } else {
      homodir = 1;
    }

    complex.setHomodimer(homodir);
    super.drawImpl(complex, graphics, params);
    complex.setHomodimer(oldHomodimer);

    java.util.List<String> unitOfInformationText = new ArrayList<>();
    if (complex.getStatePrefix() != null && complex.getStateLabel() != null) {
      unitOfInformationText.add(complex.getStatePrefix() + ":" + complex.getStateLabel());
    }
    if (homodir > 1) {
      unitOfInformationText.add("N:" + complex.getHomodimer());
    }

    Color oldColor = graphics.getColor();
    graphics.setColor(complex.getBorderColor());
    drawUnitOfInformation(unitOfInformationText, complex, graphics);
    graphics.setColor(oldColor);
  }

}
