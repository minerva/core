package lcsb.mapviewer.converter.graphics;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileStore;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.commands.CreateHierarchyCommand;
import lcsb.mapviewer.commands.SemanticZoomLevelMatcher;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.converter.graphics.bioentity.BioEntityConverterImpl;
import lcsb.mapviewer.converter.graphics.bioentity.element.species.ModificationResidueConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.species.ModificationResidueSbgnConverter;
import lcsb.mapviewer.converter.graphics.layer.LayerLineConverter;
import lcsb.mapviewer.converter.graphics.layer.LayerOvalConverter;
import lcsb.mapviewer.converter.graphics.layer.LayerRectConverter;
import lcsb.mapviewer.converter.graphics.layer.LayerTextConverter;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;

/**
 * This class is responsible for creation of the image from the model. It's an
 * abstract class that implements the conversion of a model into drawing on
 * Graphics2D object with specified zoom level. However, it does't implement
 * transformation of a graphics2D to an output file. Among many parameters there
 * are two main modes:
 * <ul>
 * <li><i>classical</i> - in this mode everything is drawn as it's visible</li>
 * <li><i>nested</i> - in this mode, on some levels, compartments, complexes and
 * pathways cover big part of the map hiding underlying complexity. In this mode
 * very important thing is to understand difference between
 * {@link lcsb.mapviewer.converter.graphics.AbstractImageGenerator.level level}
 * and {@link lcsb.mapviewer.converter.graphics.AbstractImageGenerator.scale
 * scale} parameters. Scale defines magnification of the image (it's a double
 * value), whereas level defines how far from the top we are in hierarchical
 * view (it's an integer usually in the range <0..7>. Usually there is
 * correlation between scale and level, but it's not obligatory. Level=0 means
 * that we want to see as little elements as possible, whereas level=infinity
 * means that we want to see everything</li>
 * </ul>
 * <p>
 * All draw operations are performed in default constructor. Therefore if we
 * want to create image of a different part of the map we have to create new
 * object. There are few reasons of that:
 * <ul>
 * <li>drawing is quite expensive, so lets do it once</li>
 * <li>we can draw only with the defined zoom level</li>
 * <li>when we have everything drawn we can extract different parts of the image
 * from graphics2D and save it into separate files</li>
 * </ul>
 * </p>
 * 
 * @author Piotr Gawron
 * 
 */
public abstract class AbstractImageGenerator {

  /**
   * Model is drawn as a partial image (x,y,width,height parameters of
   * {@link lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params
   * Params} class. This partial image sometimes contains some object that
   * doesn't intersect with this border (usually text descriptions). This margin
   * defines how far extend drawing.
   * <p>
   * TODO maybe more efficient (and safe) solution would be to include text
   * Description as parts of the object border
   * </p>
   */
  private static final int SINGLE_FRAME_MARGIN = 100;
  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();
  /**
   * Class that allows to check if element is visible (or transparent) when
   * drawing. It's used to filter out invisible elements when drawing
   * semantic/hierarchy view.
   */
  private SemanticZoomLevelMatcher zoomLevelMatcher = new SemanticZoomLevelMatcher();
  /**
   * On which level in hierarchical view we should visualize map.
   */
  private int level = Integer.MAX_VALUE;
  /**
   * Zoom factor of the drawing.
   */
  private double scale = 1;
  /**
   * This is a frame border from which we take all elements to draw.
   */
  private Rectangle2D.Double border = null;
  /**
   * Object that helps to convert {@link DataOverlayEntry} values into colors.
   */
  private ColorExtractor colorExtractor = null;
  /**
   * Graphics object which allows to draw objects.
   */
  private Graphics2D graphics;
  /**
   * SBGN display format.
   */
  private boolean sbgnFormat = false;
  /**
   * List of params used for drawing.
   */
  private Params params;
  /**
   * Flag indicating {@link #draw()} method was executed.
   */
  private boolean drawn = false;

  /**
   * Default constructor. Create an image that is described by params. For more
   * information see
   * {@link lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params
   * params}.
   *
   * @param params
   *          list of all params to create appropriate image
   * @throws DrawingException
   *           thrown when there was a problem with drawing a map
   *
   */
  protected AbstractImageGenerator(final Params params) {
    this.params = params;
  }

  /**
   * Don't allow to create default public constructors with empty list of
   * parameters.
   */
  protected AbstractImageGenerator() {
    super();
  }

  /**
   * This method creates a graphics object for different implementations of
   * canvas with fixed width and height.
   *
   * @param width
   *          width of the canvas (graphics2d) to be created
   * @param height
   *          height of the canvas (graphics2d) to be created
   */
  protected abstract void createImageObject(final double width, final double height);

  /**
   * Draw a model into {@link #getGraphics()} object.
   * 
   * @throws DrawingException
   *           thrown when there is a problem with drawing
   */
  protected void draw() throws DrawingException {
    if (isDrawn()) {
      logger.warn("Model was already drawn. Skipping");
      return;
    }
    this.level = params.getLevel();
    this.scale = params.getScale();

    colorExtractor = new ColorExtractor(params.getMinColor(), params.getMaxColor(), params.getSimpleColor(), params.getNeutralColor());

    // set border frame extended by a margin
    border = new Rectangle2D.Double(params.getX() - SINGLE_FRAME_MARGIN, params.getY() - SINGLE_FRAME_MARGIN,
        params.getWidth() * scale + 2 * SINGLE_FRAME_MARGIN, params.getHeight() * scale + 2 * SINGLE_FRAME_MARGIN);
    createImageObject(params.getWidth(), params.getHeight());

    // turn on anti-aliasing
    graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

    graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

    graphics.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

    // set scale
    graphics.scale(1 / scale, 1 / scale);

    // move the upper left corner to coordinates
    graphics.translate(-params.getX(), -params.getY());

    if (params.isBackground()) {
      // create a background
      graphics.setColor(Color.WHITE);
      graphics.fillRect((int) params.getX() - 1, (int) params.getY() - 1,
          ((int) Math.ceil(params.getWidth() * scale)) + 2, ((int) Math.ceil(params.getHeight() * scale)) + 2);
      graphics.setColor(Color.BLACK);
    }

    // Get the SBGN display format option from the model
    this.sbgnFormat = params.isSbgn();

    List<Drawable> drawables = getDrawables();

    // draw all elements
    for (final Drawable element : drawables) {
      if (element instanceof Species) {
        drawSpecies((Species) element);
      } else if (element instanceof Reaction) {
        drawReaction((Reaction) element);
      } else if (element instanceof Compartment) {
        drawCompartment((Compartment) element);
      } else if (element instanceof LayerText) {
        drawText((LayerText) element);
      } else if (element instanceof LayerOval) {
        drawOval((LayerOval) element);
      } else if (element instanceof LayerRect) {
        drawRect((LayerRect) element);
      } else if (element instanceof PolylineData) {
        drawLine((PolylineData) element);
      } else if (element instanceof ModificationResidue) {
        drawModificationResidue((ModificationResidue) element);
      } else {
        throw new DrawingException("Unknown class type: " + element);
      }
    }
    closeImageObject();
    setDrawn(true);
  }

  private List<Drawable> getDrawables() {
    boolean isOriginallyVisible = false;
    for (Layer layer : params.getModel().getLayers()) {
      if (Objects.deepEquals(layer.getName(), CreateHierarchyCommand.TEXT_LAYER_NAME)) {
        isOriginallyVisible = layer.isVisible();
        layer.setVisible(true);
      }
    }
    List<Drawable> drawables = new ArrayList<>(params.getModel().getDrawables(true));
    drawables.sort(BioEntity.Z_INDEX_COMPARATOR);
    for (Layer layer : params.getModel().getLayers()) {
      if (Objects.deepEquals(layer.getName(), CreateHierarchyCommand.TEXT_LAYER_NAME)) {
        // revert change that was required to obtain drawables with text layer
        layer.setVisible(isOriginallyVisible);
      }
    }
    return drawables;
  }

  private void drawText(final LayerText element) {
    new LayerTextConverter().draw(element, graphics);
  }

  private void drawRect(final LayerRect element) {
    new LayerRectConverter().draw(element, graphics);
  }

  private void drawOval(final LayerOval element) {
    new LayerOvalConverter().draw(element, graphics);
  }

  private void drawLine(final PolylineData element) {
    new LayerLineConverter().draw(element, graphics);
  }

  protected void drawModificationResidue(final ModificationResidue modificationResidue) {
    ConverterParams stateParams = new ConverterParams().level(level).nested(params.nested);
    if (!this.sbgnFormat) {
      new ModificationResidueConverter().drawModification(modificationResidue, graphics, stateParams);
    } else {
      new ModificationResidueSbgnConverter().drawModification(modificationResidue, graphics, stateParams);
    }

  }

  /**
   * Method called after drawing. It should close drawing canvas properly.
   */
  protected abstract void closeImageObject();

  /**
   * This method draw a {@link Compartment} on a graphics.
   *
   * @param compartment
   *          object that we want to draw
   * @throws DrawingException
   *           thrown when there was a problem with drawing {@link Compartment}
   */
  protected void drawCompartment(final Compartment compartment) throws DrawingException {
    // get a converter for this compartment
    BioEntityConverterImpl converter = new BioEntityConverterImpl(compartment, colorExtractor);
    ConverterParams compartmentParams = new ConverterParams().level(level).nested(params.nested);

    if (params.nested) {
      compartmentParams.scale(Math.max(scale, 1));
    }

    converter.draw(compartment, graphics, compartmentParams, params.getVisibleDataOverlaysForElement(compartment));

    if (zoomLevelMatcher.isTransparent(level, compartment.getTransparencyLevel()) || !params.nested) {
      if (!compartment.containsIdenticalSpecies()) {
        if (!(compartment instanceof PathwayCompartment)) {
          converter.drawText(compartment, graphics, compartmentParams);
        }
      }
    }
  }

  /**
   * This method draw a {@link Species} on a graphics.
   *
   * @param species
   *          object to be drawn
   * @throws DrawingException
   *           thrown when there was a problem with drawing a {@link Species}
   */
  protected void drawSpecies(final Species species) throws DrawingException {
    if (!cross(species.getBorder())) {
      return;
    }

    boolean rescale = false;
    if (species instanceof Complex) {
      Complex complex = (Complex) species;
      if (complex.getElements().size() == 0) {
        rescale = true;
      } else {
        rescale = true;
        for (final Element child : complex.getElements()) {
          if ((zoomLevelMatcher.isVisible(level, child.getVisibilityLevel())) || !params.nested) {
            rescale = false;
          }
        }
      }
    }

    // at the beginning try to find an appropriate converter
    BioEntityConverterImpl converter = new BioEntityConverterImpl(species, sbgnFormat, colorExtractor);
    double customScale = 1;
    if (rescale) {
      customScale = scale;
    }
    converter.draw(species, graphics,
        new ConverterParams().scale(customScale).level(level).sbgnFormat(sbgnFormat).nested(params.nested),
        params.getVisibleDataOverlaysForElement(species));

  }

  /**
   * This method draw a reaction on a graphics.
   *
   * @param reaction
   *          object to be drawn
   * @throws DrawingException
   *           thrown when there was a problem with drawing a {@link Reaction}
   */
  protected void drawReaction(final Reaction reaction) throws DrawingException {
    if (!cross(reaction.getLines())) {
      return;
    }
    BioEntityConverterImpl converter = new BioEntityConverterImpl(reaction, sbgnFormat, colorExtractor);
    converter.draw(reaction, graphics, new ConverterParams().sbgnFormat(sbgnFormat).nested(params.nested).level(level),
        params.getVisibleDataOverlaysForElement(reaction));
  }

  /**
   * Checks if one of the lines in parameter cross the frame.
   *
   * @param lines
   *          list of lines to check
   * @return true if the cross point exist, false otherwise
   */
  private boolean cross(final List<Line2D> lines) {
    for (final Line2D line2d : lines) {
      if (border.intersectsLine(line2d)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Checks if the rectangle in the parameters cross the fram in which we are
   * currently draw image.
   *
   * @param rect
   *          rectangle to check
   * @return true if rectangle check the frame, false otherwise
   */
  protected boolean cross(final Rectangle2D rect) {
    return border.intersects(rect);
  }

  /**
   * @return the graphics
   */
  protected Graphics2D getGraphics() {
    return graphics;
  }

  /**
   * @param graphics
   *          the graphics to set
   */
  protected void setGraphics(final Graphics2D graphics) {
    this.graphics = graphics;
  }

  /**
   * Saves generated image into file.
   *
   * @param fileName
   *          file where the images should be saved
   * @throws IOException
   *           thrown when there is problem with output file
   * @throws DrawingException
   *           thrown when there was a problem with drawing map
   */
  public final void saveToFile(final String fileName) throws IOException, DrawingException {
    if (!isDrawn()) {
      draw();
    }
    checkFreeSpace(fileName);
    saveToFileImplementation(fileName);
  }

  /**
   * Saves generated image from {@link #getGraphics()} into file.
   *
   * @param fileName
   *          file where the images should be saved
   * @throws IOException
   *           thrown when there is problem with output file
   * @throws DrawingException
   *           thrown when there was a problem with drawing map
   */
  protected abstract void saveToFileImplementation(final String fileName) throws IOException;

  /**
   * Saves generated image into {@link OutputStream}.
   *
   * @param os
   *          stream where the images should be saved
   * @throws IOException
   *           thrown when there is problem with output stream
   * @throws DrawingException
   *           thrown when there was a problem with drawing map
   */
  public final void saveToOutputStream(final OutputStream os) throws IOException, DrawingException {
    if (!isDrawn()) {
      draw();
    }
    saveToOutputStreamImplementation(os);
  }

  /**
   * Saves generated image from {@link #getGraphics()} into {@link OutputStream}
   * .
   *
   * @param os
   *          stream where the images should be saved
   * @throws IOException
   *           thrown when there is problem with output stream
   * @throws DrawingException
   *           thrown when there was a problem with drawing map
   */
  protected abstract void saveToOutputStreamImplementation(final OutputStream os) throws IOException;

  /**
   * Saves part of the generated image file.
   *
   * @param fileName
   *          file where the images should be saved
   * @param x
   *          x left margin of the image part
   * @param y
   *          y top margin of the image part
   * @param width
   *          width of the image part
   * @param height
   *          height of the image part
   * @throws IOException
   *           thrown when there is problem with output file
   * @throws DrawingException
   *           thrown when there was a problem with drawing map
   */
  public final void savePartToFile(final int x, final int y, final int width, final int height, final String fileName)
      throws IOException, DrawingException {
    if (!isDrawn()) {
      draw();
    }
    checkFreeSpace(fileName);
    savePartToFileImplementation(x, y, width, height, fileName);
  }

  private void checkFreeSpace(final String fileName) throws IOException {
    File file = new File(fileName).getAbsoluteFile();
    FileStore store = Files.getFileStore(file.getParentFile().toPath());
    long freeSpace = store.getUsableSpace();
    if (freeSpace < Configuration.getRequiredFreeSpaceInBytes()) {
      throw new IOException(
          "Not enough space left on device: " + freeSpace + " bytes. Required: " + Configuration.getRequiredFreeSpaceInBytes() + " bytes");
    }
  }

  /**
   * Saves part of the generated image from {@link #getGraphics()} into file.
   *
   * @param fileName
   *          file where the images should be saved
   * @param x
   *          x left margin of the image part
   * @param y
   *          y top margin of the image part
   * @param width
   *          width of the image part
   * @param height
   *          height of the image part
   * @throws IOException
   *           thrown when there is problem with output file
   */
  protected abstract void savePartToFileImplementation(final int x, final int y, final int width, final int height,
      final String fileName) throws IOException;

  /**
   * Saves part of the generated image into {@link OutputStream}.
   *
   * @param os
   *          stream where the images should be saved
   * @param x
   *          x left margin of the image part
   * @param y
   *          y top margin of the image part
   * @param width
   *          width of the image part
   * @param height
   *          height of the image part
   * @throws IOException
   *           thrown when there is problem with output file
   * @throws DrawingException
   *           thrown when there was a problem with drawing map
   */
  public final void savePartToOutputStream(final int x, final int y, final int width, final int height,
      final OutputStream os) throws IOException, DrawingException {
    if (!isDrawn()) {
      draw();
    }
    savePartToOutputStreamImplementation(x, y, width, height, os);
  }

  /**
   * Saves part of the generated image from {@link #getGraphics()} into
   * {@link OutputStream}.
   *
   * @param os
   *          stream where the images should be saved
   * @param x
   *          x left margin of the image part
   * @param y
   *          y top margin of the image part
   * @param width
   *          width of the image part
   * @param height
   *          height of the image part
   * @throws IOException
   *           thrown when there is problem with output file
   * @throws DrawingException
   *           thrown when there was a problem with drawing map
   */
  protected abstract void savePartToOutputStreamImplementation(final int x, final int y, final int width,
      final int height, final OutputStream os) throws IOException;

  /**
   * Returns name of the format to which this graphic converter will transform.
   *
   * @return name of the format to which this graphic converter will transform
   */
  public abstract String getFormatName();

  /**
   * Returns {@link MimeType} that should be used for files generated by this
   * image generator.
   *
   * @return {@link MimeType} that should be used for files generated by this
   *         image generator
   */
  public abstract MimeType getMimeType();

  /**
   * Returns default file extension used by this image generator.
   *
   * @return default file extension used by this image generator
   */
  public abstract String getFileExtension();

  /**
   * @return the sbgnFormat
   */
  protected boolean isSbgnFormat() {
    return sbgnFormat;
  }

  /**
   * @param sbgnFormat
   *          the sbgnFormat to set
   */
  protected void sbgnFormat(final boolean sbgnFormat) {
    this.sbgnFormat = sbgnFormat;
  }

  /**
   * @return the drawn
   * @see #drawn
   */
  private boolean isDrawn() {
    return drawn;
  }

  /**
   * @param drawn
   *          the drawn to set
   * @see #drawn
   */
  private void setDrawn(final boolean drawn) {
    this.drawn = drawn;
  }

  /**
   * @return the params
   * @see #params
   */
  protected Params getParams() {
    return params;
  }

  /**
   * @param params
   *          the params to set
   * @see #params
   */
  protected void setParams(final Params params) {
    this.params = params;
  }

  /**
   * This class contains a list of params that are used for drawing.
   *
   * @author Piotr Gawron
   *
   */
  public static class Params {

    /**
     * Default class logger.
     */
    private final Logger logger = LogManager.getLogger();

    /**
     * Scale at which we want to visualize the map. The default value is 1.0 (no
     * magnification).
     */
    private double scale = 1;

    /**
     * x coordinate from which we start to draw a map. This is absolute value on
     * the map (scale doesn't influence it).
     */

    private double x = 0;

    /**
     * y coordinate from which we start to draw a map. This is absolute value on
     * the map (scale doesn't influence it).
     */
    private double y = 0;

    /**
     * width of the generated image. It's an absolute value in pixels, therefore
     * when scale is set to value different than 1.0 the width on the map will
     * also be modified to fit into width pixels.
     */
    private Double width = null;

    /**
     * height of the generated image. It's an absolute value in pixels,
     * therefore when scale is set to value different than 1.0 the height on the
     * map will also be modified to fit into height pixels.
     */
    private Double height = null;

    /**
     * Model to be visualized.
     */
    private Model model = null;

    /**
     * Should we draw white background.
     */
    private boolean background = true;

    /**
     * At which hierarchy level we visualize map. Level=0 means that we want to
     * see as little elements as possible, whereas level=infinity means that we
     * want to see everything.
     */
    private int level = Integer.MAX_VALUE;

    /**
     * Should the visualization include hierarchical view or not.
     */
    private boolean nested = false;

    /**
     * Should sbgn standard be used.
     */
    private boolean sbgn = false;

    /**
     * List of objects containing information about layouts visualized in the
     * JavaScript (on client side). Every object (map) represents data for
     * single layout. In this map we have pairs between {@link Element}/
     * {@link Reaction} and {@link DataOverlayEntry} used to visualize the
     * element.
     *
     */
    private List<List<Pair<? extends BioEntity, DataOverlayEntry>>> visibleDataOverlays = new ArrayList<>();

    /**
     * Color that should be used for drawing overlays with minimum value.
     */
    private Color minColor = Color.BLACK;

    /**
     * Color that should be used for drawing overlays with maximum value.
     */
    private Color maxColor = Color.BLACK;

    private Color simpleColor = Color.BLACK;

    private Color neutralColor = Color.WHITE;

    /**
     * @param scale
     *          scale to set
     * @return object with all parameters
     * @see #scale
     */
    public Params scale(final double scale) {
      this.scale = scale;
      return this;
    }

    /**
     * @param scale
     *          scale to set
     * @return object with all parameters
     * @see #scale
     */
    public Params scale(final int scale) {
      this.scale = scale;
      return this;
    }

    /**
     *
     * @param x
     *          x coordinate to be set
     * @return object with all parameters
     * @see #x
     */
    public Params x(final double x) {
      this.x = x;
      return this;
    }

    /**
     * @param x
     *          x coordinate to be set
     * @return object with all parameters
     * @see #x
     */
    public Params x(final int x) {
      this.x = x;
      return this;
    }

    /**
     * @param y
     *          y coordinate to be set
     * @return object with all parameters
     * @see #y
     */
    public Params y(final double y) {
      this.y = y;
      return this;
    }

    /**
     * @param y
     *          y coordinate to be set
     * @return object with all parameters
     * @see #y
     */
    public Params y(final int y) {
      this.y = y;
      return this;
    }

    /**
     * @param width
     *          width to set
     * @return object with all parameters
     * @see #width
     */
    public Params width(final double width) {
      this.width = width;
      return this;
    }

    /**
     * @param width
     *          width to set
     * @return object with all parameters
     * @see #width
     */
    public Params width(final int width) {
      this.width = (double) width;
      return this;
    }

    /**
     *
     * @param height
     *          height to set
     * @return object with all parameters
     * @see #height
     */
    public Params height(final double height) {
      this.height = height;
      return this;
    }

    /**
     * @param height
     *          height to set
     * @return object with all parameters
     * @see #height
     */
    public Params height(final int height) {
      this.height = (double) height;
      return this;
    }

    /**
     *
     * @param background
     *          background flag to set
     * @return object with all parameters
     * @see #background
     */
    public Params background(final boolean background) {
      this.background = background;
      return this;
    }

    /**
     *
     * @param nested
     *          nested flag to set
     * @return object with all parameters
     * @see #nested
     */
    public Params nested(final boolean nested) {
      this.nested = nested;
      return this;
    }

    /**
     *
     * @param model
     *          model to set
     * @return object with all parameters
     * @see #model
     */
    public Params model(final Model model) {
      this.model = model;
      if (this.width == null) {
        this.width = model.getWidth();
      }
      if (this.height == null) {
        this.height = model.getHeight();
      }
      return this;
    }

    /**
     *
     * @param level
     *          level to set
     * @return object with all parameters
     * @see #level
     */
    public Params level(final int level) {
      this.level = level;
      return this;
    }

    /**
     * Sets {@link #level} parameter from {@link String}.
     *
     * @param zoomLevel
     *          new {@link #level} value
     * @return object with all parameters
     */
    public Params level(final String zoomLevel) {
      if (zoomLevel == null) {
        logger.warn("Parameter equals to null...");
      } else {
        this.level = Integer.valueOf(zoomLevel);
      }
      return this;
    }

    /**
     *
     * @return scale value
     * @see #scale
     */
    public double getScale() {
      return scale;
    }

    /**
     *
     * @return x coordinate value
     * @see #x
     */
    public double getX() {
      return x;
    }

    /**
     *
     * @return y coordinate value
     * @see #y
     */
    public double getY() {
      return y;
    }

    /**
     *
     * @return width value
     * @see #width
     */
    public double getWidth() {
      return width;
    }

    /**
     *
     * @return height value
     * @see #height
     */
    public double getHeight() {
      return height;
    }

    /**
     *
     * @return model object
     * @see #model
     */
    public Model getModel() {
      return model;
    }

    /**
     *
     * @return background value
     * @see #background
     */
    public boolean isBackground() {
      return background;
    }

    /**
     *
     * @return nested value
     * @see #nested
     */
    public boolean isNested() {
      return nested;
    }

    /**
     * @return the level
     * @see #level
     */
    public int getLevel() {
      return level;
    }

    @Override
    public String toString() {
      String result = "[" + this.getClass().getSimpleName() + "] " + "Model:" + model + "," + "scale:" + scale + ","
          + "x:" + x + "," + "y:" + y + "," + "width:" + width + "," + "height:" + height + "," + "background:"
          + background + "," + "level:" + level + "," + "nested:" + nested + ",";
      return result;
    }

    /**
     * Adds layout data that is visualized on the client side. Data overlay
     * contains mapping between {@link Element}/{@link Reaction} and
     * {@link DataOverlayEntry} used for coloring specific element.
     *
     * @param map
     *          layout data containing mapping between {@link Element}/
     *          {@link Reaction} and {@link DataOverlayEntry} used for coloring
     *          specific element
     */
    public void addVisibleDataOverlay(final List<Pair<? extends BioEntity, DataOverlayEntry>> map) {
      visibleDataOverlays.add(map);
    }

    public List<List<Pair<? extends BioEntity, DataOverlayEntry>>> getVisibleDataOverlays() {
      return visibleDataOverlays;
    }

    /**
     * Returns list with {@link DataOverlayEntry} used to visualize the specific
     * object in data overlays visualized in the JavaScript.
     *
     * @param object
     *          object ({@link Element} or {@link Reaction}) for which we return
     *          list of {@link DataOverlayEntry} in different layouts
     * @return list with {@link DataOverlayEntry} used to visualize the specific
     *         object in data overlays visualized in the JavaScript
     * @see #visibleDataOverlays
     */
    public List<List<DataOverlayEntry>> getVisibleDataOverlaysForElement(final BioEntity object) {
      List<List<DataOverlayEntry>> result = new ArrayList<>();
      for (List<Pair<? extends BioEntity, DataOverlayEntry>> map : visibleDataOverlays) {
        List<DataOverlayEntry> dataEntries = new ArrayList<>();
        for (Pair<? extends BioEntity, DataOverlayEntry> pair : map) {
          if (pair.getLeft().getId() == object.getId() && pair.getLeft().getClass() == object.getClass()) {
            dataEntries.add(pair.getRight());
          }
        }
        result.add(dataEntries);
      }
      return result;
    }

    /**
     * Returns {@link Color} that should be used for drawing overlays with
     * maximum value.
     *
     * @return {@link Color} that should be used for drawing overlays with
     *         maximum value
     */
    public Color getMaxColor() {
      return maxColor;
    }

    public Color getSimpleColor() {
      return simpleColor;
    }

    /**
     * Returns {@link Color} that should be used for drawing overlays with
     * minimum value.
     *
     * @return {@link Color} that should be used for drawing overlays with
     *         minimum value
     */
    public Color getMinColor() {
      return minColor;
    }

    /**
     * @param minColor
     *          minColor to set
     * @return object with all parameters
     * @see #minColor
     */
    public Params minColor(final Color minColor) {
      this.minColor = minColor;
      return this;
    }

    /**
     * @param maxColor
     *          maxColor to set
     * @return object with all parameters
     * @see #maxColor
     */
    public Params maxColor(final Color maxColor) {
      this.maxColor = maxColor;
      return this;
    }

    /**
     * @param simpleColor
     *          simpleColor to set
     * @return object with all parameters
     * @see #simpleColor
     */
    public Params simpleColor(final Color simpleColor) {
      this.simpleColor = simpleColor;
      return this;
    }

    /**
     * @return the sbgn
     * @see #sbgn
     */
    public boolean isSbgn() {
      return sbgn;
    }

    /**
     * @param sbgn
     *          the sbgn to set
     * @see #sbgn
     * @return object with all parameters
     */
    public Params sbgn(final boolean sbgn) {
      this.sbgn = sbgn;
      return this;
    }

    public Params colorExtractor(final ColorExtractor colorExtractor) {
      return minColor(colorExtractor.getMinColor())
          .maxColor(colorExtractor.getMaxColor())
          .simpleColor(colorExtractor.getSimpleColor())
          .neutralColor(colorExtractor.getNeutralColor());
    }

    public Params neutralColor(final Color neutralColor) {
      this.neutralColor = neutralColor;
      return this;
    }

    public Color getNeutralColor() {
      return this.neutralColor;
    }

  }
}