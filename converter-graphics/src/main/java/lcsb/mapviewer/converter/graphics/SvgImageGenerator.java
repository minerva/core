package lcsb.mapviewer.converter.graphics;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * This class implements SVG generator for the image object.This class uses
 * batik library to create all converters.
 * 
 * @author Piotr Gawron
 * 
 */
public class SvgImageGenerator extends AbstractImageGenerator {

  /**
   * Root dom element. We need it to be able to provide viewBox.
   */
  private Element root;

  /**
   * Default constructor. Create an image that is described by params. For more
   * information see
   * {@link lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params
   * params}.
   *
   * @param params
   *          parameters used for the image creation.
   * @throws DrawingException
   *           thrown when there was a problem with drawing a map
   */
  public SvgImageGenerator(final Params params) throws DrawingException {
    super(params);
  }

  @Override
  protected void createImageObject(final double width, final double height) {

    // Get a DOMImplementation.
    DOMImplementation domImpl = GenericDOMImplementation.getDOMImplementation();

    // Create an instance of org.w3c.dom.Document.
    String svgNS = "http://www.w3.org/2000/svg";
    Document document = domImpl.createDocument(svgNS, "svg", null);

    // Create an instance of the SVG Generator.
    SVGGraphics2D graphics = new SVGGraphics2D(document);
    root = graphics.getRoot();
    root.setAttributeNS(null, "viewBox", "0 0 " + (int) width + " " + (int) height);
    setGraphics(graphics);

  }

  @Override
  protected void closeImageObject() {
  }

  @Override
  public void saveToFileImplementation(final String fileName) throws IOException {
    saveToOutputStreamImplementation(new FileOutputStream(fileName));
  }

  @Override
  public void saveToOutputStreamImplementation(final OutputStream os) throws IOException {
    // for some reason if you remove this line either viewbox is not define or
    // content is not there
    ((SVGGraphics2D) getGraphics()).getRoot(root);
    ((SVGGraphics2D) getGraphics()).stream(root, new OutputStreamWriter(os));
  }

  @Override
  public void savePartToFileImplementation(final int x, final int y, final int width, final int height, final String fileName) throws IOException {
    throw new NotImplementedException("Partial save is not implemented in SVG image generator");
  }

  @Override
  public void savePartToOutputStreamImplementation(final int x, final int y, final int width, final int height, final OutputStream os)
      throws IOException {
    throw new NotImplementedException("Partial save is not implemented in SVG image generator");
  }

  @Override
  public String getFormatName() {
    return "SVG image";
  }

  @Override
  public MimeType getMimeType() {
    return MimeType.SVG;
  }

  @Override
  public String getFileExtension() {
    return "svg";
  }

}
