package lcsb.mapviewer.converter.graphics.bioentity;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.DrawableConverter;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;

/**
 * This interface defines what operations should be possible to convert
 * {@link BioEntity} into a graphics on Graphics2D object.
 * 
 * @author Piotr Gawron
 * 
 * @param <T>
 *          class of alias to convert
 */
public abstract class BioEntityConverter<T extends BioEntity> extends DrawableConverter<T> {

  /**
   * Alpha value (0..255) used for visualizing overlay data that are normally
   * visualized in JavaScript.
   */
  public static final int LAYOUT_ALPHA = 200;

  /**
   * This function draw {@link BioEntity} on the {@link Graphics2D} object.
   * 
   * @param bioEntity
   *          {@link BioEntity} that should be drawn
   * @param graphics
   *          where we want to draw bioEntity
   * @param params
   *          visualization params (like, should the object be filled with solid
   *          color, etc.), for more information see {@link ConverterParams}
   * @throws DrawingException
   *           thrown when there is a problem with drawing {@link BioEntity}
   * 
   */
  public void draw(final T bioEntity, final Graphics2D graphics, final ConverterParams params) throws DrawingException {
    draw(bioEntity, graphics, params, new ArrayList<>());
  }

  /**
   * This function draw representation of the alias on the graphics object.
   * 
   * @param bioEntity
   *          {@link BioEntity} that should be drawn
   * @param graphics
   *          where we want to draw bioEntity
   * @param params
   *          visualization params (like, should the object be filled with solid
   *          color, etc.), for more information see {@link ConverterParams}
   * @param visualizedOverlaysColorSchemas
   *          list of {@link DataOverlayEntry} that were used for visualizing this
   *          BioEntity in different data overlays that should be overlaid on the
   *          alias
   * @throws DrawingException
   *           thrown when there is a problem with drawing {@link BioEntity}
   * 
   */
  protected abstract void draw(final T bioEntity, final Graphics2D graphics, final ConverterParams params,
      List<List<DataOverlayEntry>> visualizedOverlaysColorSchemas)
      throws DrawingException;

  /**
   * This function draw {@link BioEntity} on the {@link Graphics2D} object.
   * 
   * @param bioEntity
   *          {@link BioEntity} that should be drawn
   * @param graphics
   *          where we want to draw bioEntity
   * @param params
   *          visualization params (like, should the object be filled with solid
   *          color, etc.), for more information see {@link ConverterParams}
   * @throws DrawingException
   *           thrown when there is a problem with drawing {@link BioEntity}
   * 
   */
  protected abstract void drawImpl(final T bioEntity, final Graphics2D graphics, final ConverterParams params) throws DrawingException;

  /**
   * This function will find proper font size to display text within it. Then it
   * will print this text.
   * 
   * @param bioEntity
   *          {@link BioEntity} with description to be drawn
   * @param graphics
   *          where the description should be drawn
   * @param params
   *          parameters of visualization (centering, scale)
   * @throws DrawingException
   *           thrown when there is a problem with drawing {@link BioEntity}
   */
  public abstract void drawText(final T bioEntity, final Graphics2D graphics, final ConverterParams params) throws DrawingException;

}
