package lcsb.mapviewer.converter.graphics.geometry;

/**
 * Exception thrown when the rectangle in which we want to draw description is
 * too small.
 * 
 * @author Piotr Gawron
 * 
 */
public class RectangleTooSmallException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor with text message.
   * 
   * @param message
   *          string message for this exception
   */
  public RectangleTooSmallException(final String message) {
    super(message);
  }
}
