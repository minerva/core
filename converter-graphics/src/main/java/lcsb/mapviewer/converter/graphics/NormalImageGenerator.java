package lcsb.mapviewer.converter.graphics;

import java.awt.image.BufferedImage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Extension of the image generator class that use standard awt implementation
 * taht allows basic export.
 * 
 * @author Piotr Gawron
 * 
 */
public abstract class NormalImageGenerator extends AbstractImageGenerator {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger();

  /**
   * Buffered image structure used for image generating.
   */
  private BufferedImage bi;

  /**
   * Default constructor. Create an image that is described by params. For more
   * information see
   * {@link lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params
   * params}.
   * 
   * @param params
   *          parameters used for the image creation.
   * @throws DrawingException
   *           thrown when there was a problem with drawing a map
   */
  protected NormalImageGenerator(final Params params) throws DrawingException {
    super(params);
  }

  @Override
  protected void createImageObject(final double width, final double height) {
    bi = new BufferedImage((int) width, (int) height, BufferedImage.TYPE_INT_ARGB);
    setGraphics(bi.createGraphics());
  }

  protected BufferedImage getBi() {
    return bi;
  }

  /**
   * @param bi
   *          the bi to set
   * @see #bi
   */
  protected void setBi(final BufferedImage bi) {
    this.bi = bi;
  }

}
