/**
 * Provides classes that draws
 * {@link lcsb.mapviewer.db.model.map.reaction.Reaction reactions} on the
 * {@link java.awt.Graphics2D Graphics2D} object.
 */
package lcsb.mapviewer.converter.graphics.bioentity.reaction;
