package lcsb.mapviewer.converter.graphics;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params;
import lcsb.mapviewer.model.map.layout.ProjectBackground;
import lcsb.mapviewer.model.map.layout.ProjectBackgroundImageLayer;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

/**
 * This class allows to generate set of images that could be later on used by
 * Google Maps API.
 *
 * @author Piotr Gawron
 */
public class MapGenerator {

  /**
   * Default tile size. It must be 256px*256px, if different value is chosen then
   * there are problems with positioning all elements by GoogleMaps API (like
   * putting markers, line, getting position etc).
   */
  public static final int TILE_SIZE = 256;
  /**
   * Because generating of PNG file require to transform the whole Model class
   * into canvas, the bigger image is created and after the image is created small
   * chunks are sliced and saved to files. For instance if TILES_CACHE_NUM=4 and
   * TILE_SIZE=256, then ImageGenerator will create PNG images 1024px*1024px and
   * this images are sliced into 256px*256px images. Of course TILES_CACHE_NUM
   * cannot be too big due to Out of Memory problem.
   */

  static final int TILES_CACHE_NUM = 4;
  /**
   * Default class logger.
   */
  private static final Logger logger = LogManager.getLogger();

  /**
   * This method generates PNG images used by GoogleMaps API.
   *
   * @param params {@link MapGeneratorParams params} used for generating images
   * @throws IOException      exception thrown when there are some problems with creating images
   * @throws DrawingException thrown when there was a problem with drawing a map
   */
  public void generateMapImages(final MapGeneratorParams params) throws IOException, DrawingException {

    if (params.nested) {
      logger
          .debug("Creating nested model with " + params.levels + " levels and maximum zoom rate: " + params.zoomFactor);
    } else {
      logger.debug(
          "Creating not nested model with " + params.levels + " levels and maximum zoom rate: " + params.zoomFactor);
    }

    // compute number of tiles (for progress bar)
    double num = 0;
    for (int i = 0; i <= params.levels; i++) {
      double m = Math.ceil((int) Math.pow(2, i)) / TILES_CACHE_NUM;
      m = Math.ceil(m);
      m *= TILES_CACHE_NUM;
      num += m * m;
    }

    String zoomDirStr = params.directory;
    File zoomDir = new File(zoomDirStr);
    if (zoomDir.isDirectory()) {
      logger.warn("Removing directory: " + zoomDir.getAbsolutePath());
      FileUtils.deleteDirectory(zoomDir);
    } else if (zoomDir.isFile()) {
      logger.warn("Removing file: " + zoomDir.getAbsolutePath());
      zoomDir.delete();
    }

    int progress = 0;
    for (int i = 0; i <= params.levels; i++) {
      zoomDirStr = params.directory + System.getProperty("file.separator") + (i + Configuration.MIN_ZOOM_LEVEL)
          + System.getProperty("file.separator");
      int tiles = (int) (Math.pow(2, i));
      for (int j = 0; j < tiles + TILES_CACHE_NUM; j++) {
        zoomDir = new File(zoomDirStr + System.getProperty("file.separator") + j);
        if (!zoomDir.exists()) {

          if (!zoomDir.mkdirs()) {
            logger.warn("Problem with creating dir: " + zoomDir.getAbsolutePath());
          }
        }
      }
      for (int j = 0; j < tiles; j += TILES_CACHE_NUM) {
        for (int k = 0; k < tiles; k += TILES_CACHE_NUM) {
          params.updater.setProgress((IProgressUpdater.MAX_PROGRESS * progress / num));
          progress += TILES_CACHE_NUM * TILES_CACHE_NUM;
          if (j * TILE_SIZE * params.zoomFactor > params.model.getWidth()
              || k * TILE_SIZE * params.zoomFactor > params.model.getHeight()) {
            continue;
          } else {
            Params imgParams = new Params();
            imgParams.scale(params.zoomFactor);
            imgParams.x(j * TILE_SIZE * params.zoomFactor);
            imgParams.y(k * TILE_SIZE * params.zoomFactor);
            imgParams.width(TILE_SIZE * TILES_CACHE_NUM);
            imgParams.height(TILE_SIZE * TILES_CACHE_NUM);
            imgParams.model(params.model);
            imgParams.sbgn(params.sbgn);
            imgParams.level(i);
            imgParams.nested(params.nested);
            PngImageGenerator generator = new PngImageGenerator(imgParams);
            for (int cx = 0; cx < TILES_CACHE_NUM; cx++) {
              for (int cy = 0; cy < TILES_CACHE_NUM; cy++) {
                if ((j + cx) * TILE_SIZE * params.zoomFactor >= params.model.getWidth()
                    || (k + cy) * TILE_SIZE * params.zoomFactor >= params.model.getHeight()) {
                  continue;
                } else {
                  String fileName = zoomDirStr + (j + cx) + System.getProperty("file.separator") + (k + cy) + ".PNG";
                  generator.savePartToFile(cx * TILE_SIZE, cy * TILE_SIZE, TILE_SIZE, TILE_SIZE, fileName);
                }
              }
            }
          }
        }
      }
      params.zoomFactor /= 2;
    }
  }

  /**
   * Computes how many zoom levels should exists for the map model.
   *
   * @param model map model
   * @return zoom levels for the model
   */
  public int computeZoomLevels(final ModelData model) {
    return (int) Math.ceil(Math.log(computeZoomFactor(model)) / Math.log(2));
  }

  /**
   * Computes the scale that should be used on the top zoom level.
   *
   * @param model model for which computation is done
   * @return scale on the top zoom level
   */
  public double computeZoomFactor(final ModelData model) {
    return Math.max(model.getHeight(), model.getWidth()) / (TILE_SIZE);
  }

  /**
   * Removes files that were generated for the layout.
   *
   * @param background layout object for which images will be removed
   * @throws IOException thrown when there are some problems with removing files
   */
  public void removeProjectBackground(final ProjectBackground background) throws IOException {
    removeProjectBackground(background, null);
  }

  /**
   * Removes files that were generated for the layout.
   *
   * @param background background for which images will be removed
   * @param homeDir    determines the directory where images are stored (it's optional)
   * @throws IOException thrown when there are some problems with removing files
   */
  public void removeProjectBackground(final ProjectBackground background, final String homeDir) throws IOException {
    for (final ProjectBackgroundImageLayer imageLayer : background.getProjectBackgroundImageLayer()) {
      String directory = imageLayer.getDirectory();
      File dir = new File(directory);
      if (!dir.isAbsolute() && homeDir != null) {
        dir = new File(homeDir + "/" + directory);
      }
      if (dir.exists()) {
        FileUtils.deleteDirectory(dir);
      } else {
        logger.warn("Trying to remove dir that doesn't exist: " + dir.getAbsolutePath() + " (" + directory + ")");
      }
    }
  }

  /**
   * Parameter class with information for generating set of images:
   * <ul>
   * <li>{@link #model}</li>
   * <li>{@link #directory}</li>
   * <li>{@link #nested}</li>
   * <li>{@link #removeEmpty}</li>
   * <li>{@link #updater}</li>
   * <li>{@link #zoomFactor}</li>
   * <li>{@link #levels}</li>.
   * </ul>
   *
   * @author Piotr Gawron
   */
  public class MapGeneratorParams {
    /**
     * Information about map to be generated.
     */
    private Model model;
    /**
     * Where we want to put the files. The structure of the directory will be as
     * follows: directory/subDirA/subDirB/file.PNG. subDirA is a level of zooming.
     * subDirB is x coordinate of the image. file is y coordinate of the image.
     */
    private String directory;
    /**
     * Do we want to generate images in hierarchical view or normal.
     */
    private boolean nested = false;
    /**
     * Should we remove empty elements in nested view.
     */
    private boolean removeEmpty = false;
    /**
     * Callback function that update progress information.
     */
    private IProgressUpdater updater = new IProgressUpdater() {
      @Override
      public void setProgress(final double progress) {
      }
    };

    /**
     * Zoom factor on the lowest level.
     */
    private Double zoomFactor;

    /**
     * How many levels.
     */
    private Integer levels;

    /**
     * Should sbgn standard be used.
     */
    private boolean sbgn;

    /**
     * @param model the model to set
     * @return full {@link MapGeneratorParams} object
     * @see #model
     */
    public MapGeneratorParams model(final Model model) {
      this.model = model;
      if (zoomFactor == null) {
        zoomFactor = computeZoomFactor(model.getModelData());
      }
      if (levels == null) {
        levels = computeZoomLevels(model.getModelData());
      }
      return this;
    }

    /**
     * @param directory the directory to set
     * @return full {@link MapGeneratorParams} object
     * @see #directory
     */
    public MapGeneratorParams directory(final String directory) {
      this.directory = directory;
      return this;
    }

    /**
     * @return the nested
     * @see #nested
     */
    public boolean isNested() {
      return nested;
    }

    /**
     * @param nested the nested to set
     * @return full {@link MapGeneratorParams} object
     * @see #nested
     */
    public MapGeneratorParams nested(final boolean nested) {
      this.nested = nested;
      return this;
    }

    /**
     * @return the removeEmpty
     * @see #removeEmpty
     */
    public boolean isRemoveEmpty() {
      return removeEmpty;
    }

    /**
     * @param removeEmpty the removeEmpty to set
     * @return full {@link MapGeneratorParams} object
     * @see #removeEmpty
     */
    public MapGeneratorParams removeEmpty(final boolean removeEmpty) {
      this.removeEmpty = removeEmpty;
      return this;
    }

    /**
     * @return the updater
     * @see #updater
     */
    public IProgressUpdater getUpdater() {
      return updater;
    }

    /**
     * @param updater the updater to set
     * @return full {@link MapGeneratorParams} object
     * @see #updater
     */
    public MapGeneratorParams updater(final IProgressUpdater updater) {
      this.updater = updater;
      return this;
    }

    /**
     * @return the zoomFactor
     * @see #zoomFactor
     */
    public Double getZoomFactor() {
      return zoomFactor;
    }

    /**
     * @param zoomFactor the zoomFactor to set
     * @return full {@link MapGeneratorParams} object
     * @see #zoomFactor
     */
    public MapGeneratorParams zoomFactor(final Double zoomFactor) {
      this.zoomFactor = zoomFactor;
      return this;
    }

    /**
     * @return the levels
     * @see #levels
     */
    public Integer getLevels() {
      return levels;
    }

    /**
     * @param levels the levels to set
     * @return full {@link MapGeneratorParams} object
     * @see #levels
     */
    public MapGeneratorParams levels(final Integer levels) {
      this.levels = levels;
      return this;
    }

    /**
     * @return the sbgn
     * @see #sbgn
     */
    public boolean isSbgn() {
      return sbgn;
    }

    /**
     * @param sbgn the sbgn to set
     * @return object with all parameters
     * @see #sbgn
     */
    public MapGeneratorParams sbgn(final boolean sbgn) {
      this.sbgn = sbgn;
      return this;
    }

  }

}
