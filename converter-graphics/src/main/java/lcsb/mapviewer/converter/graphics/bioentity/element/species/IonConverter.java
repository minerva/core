package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Ellipse2D;
import java.awt.geom.PathIterator;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This class defines methods used for drawing SpeciesAlias of {@link Ion} on
 * the {@link Graphics2D} object.
 * 
 * @author Piotr Gawron
 * 
 */
public class IonConverter extends SpeciesConverter<Ion> {

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into
   *          colors when drawing {@link Species}
   */
  public IonConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected void drawImpl(final Ion ion, final Graphics2D graphics, final ConverterParams params) {
    Shape shape = getShape(ion);
    Color oldColor = graphics.getColor();
    graphics.setColor(ion.getFillColor());
    graphics.fill(shape);
    graphics.setColor(ion.getBorderColor());
    Stroke stroke = graphics.getStroke();
    graphics.setStroke(getBorderLine(ion));
    graphics.draw(shape);
    graphics.setStroke(stroke);
    drawText(ion, graphics, params);
    graphics.setColor(oldColor);
  }

  @Override
  protected Shape getShape(final Ion ion) {
    double diameter = getDiameter(ion);
    double x = getXCoord(ion, diameter);
    double y = getYCoord(ion);
    Shape shape = new Ellipse2D.Double(x, y, diameter, diameter);
    return shape;
  }

  /**
   * Returns transformed y coordinate for the {@link Ion}.
   * 
   * @param ion
   *          {@link Ion} to to which we are looking for y coordinate
   * @return y coordinate of the {@link Ion}
   */
  private double getYCoord(final Ion ion) {
    double y = ion.getY();
    return y;
  }

  /**
   * Returns transformed x coordinate for the {@link Ion}.
   * 
   * @param ion
   *          {@link Ion} to which we are looking for x coordinate
   * @param diameter
   *          diameter of circle representation of ion
   * @return x coordinate of the {@link Ion}
   */
  private double getXCoord(final Ion ion, final double diameter) {
    double x = ion.getX() + (ion.getWidth() - diameter) / 2;
    return x;
  }

  /**
   * Returns diameter of circle representation of an {@link Ion}.
   * 
   * @param ion
   *          {@link Ion} to to which we are looking for diameter.
   * @return diameter of {@link Ion} circle representation
   */
  private double getDiameter(final Ion ion) {
    double diameter = Math.min(ion.getWidth(), ion.getHeight());
    if (diameter < 0) {
      logger.warn("Something is wrong. Size cannot be negative");
      diameter = 0;
    }

    return diameter;
  }

  @Override
  public PathIterator getBoundPathIterator(final Ion ion) {
    throw new InvalidStateException("This class doesn't have bound");
  }

}
