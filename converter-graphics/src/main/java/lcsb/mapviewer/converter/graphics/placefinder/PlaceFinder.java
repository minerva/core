package lcsb.mapviewer.converter.graphics.placefinder;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.commands.SemanticZoomLevelMatcher;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;

/**
 * This class allows to find free space where description should appear for the
 * alias in hierarchical view.
 * 
 * @author Piotr Gawron
 * 
 */
public class PlaceFinder {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();
  /**
   * Class that allows to check if element is visible (or transparent) when
   * drawing. It's used to filter out invisible elements when drawing
   * semantic/hierarchy view.
   */
  private SemanticZoomLevelMatcher zoomLevelMatcher = new SemanticZoomLevelMatcher();
  /**
   * Whole surface is split into rectangular regions. The lines that cut the
   * surface correspond to borders of aliases that overlap current alias. This
   * matrix defines which of these regions can be used for description.
   */
  private Field[][] matrix;
  /**
   * List of {@link Compartment compartment aliases} that split surface into
   * regions.
   */
  private List<Compartment> aliases;
  /**
   * Alias for which computation is done.
   */
  private Element mainAlias;
  /**
   * Left boundary of the rectangle in which we want to fit our description.
   */
  private double leftBound;
  /**
   * Right boundary of the rectangle in which we want to fit our description.
   */
  private double rightBound;
  /**
   * Top boundary of the rectangle in which we want to fit our description.
   */
  private double upBound;
  /**
   * Bottom boundary of the rectangle in which we want to fit our description.
   */
  private double bottomBound;
  /**
   * Model for which this {@link PlaceFinder} was created.
   */
  private Model model;
  /**
   * Map with aliases that should be considered when looking for a place to put
   * text.
   */
  private Map<Element, Map<Integer, List<Compartment>>> otherAliases = new HashMap<>();

  /**
   * Default constructor for place finder in a model.
   *
   * @param model
   *          model
   *
   */
  public PlaceFinder(final Model model) {
    this.model = model;
  }

  /**
   * Finds the biggest rectangle.
   *
   * @return the biggest rectangle
   */
  private Rectangle2D findRectangle() {
    Rectangle2D biggestRectangle = new Rectangle2D.Double();
    Rectangle2D temporaryRectangle = null;
    for (int y = 0; y < getSizeY(); y++) {
      for (int x = 0; x < getSizeX(); x++) {
        temporaryRectangle = computeRectangleMaxArea(x, y);
        double newArea = temporaryRectangle.getWidth() * temporaryRectangle.getHeight();
        double oldArea = biggestRectangle.getWidth() * biggestRectangle.getHeight();
        if (newArea > oldArea) {
          biggestRectangle = temporaryRectangle;
        }
      }
    }

    return biggestRectangle;
  }

  /**
   * Computes the biggest rectangle that starts in the region on the position
   * [x,y].
   *
   * @param x
   *          x index of the region
   * @param y
   *          y index of the region
   * @return the biggest rectangle that starts in the region on the given position
   */
  private Rectangle2D computeRectangleMaxArea(final int x, final int y) {
    Rectangle2D result = new Rectangle2D.Double();
    int possibleMaximumOfHeight = matrix[x][y].getRange();
    for (int k = x; k >= 0 && matrix[k][y].isAvailable(); k--) {
      if (matrix[k][y].getRange() < possibleMaximumOfHeight) {
        possibleMaximumOfHeight = matrix[k][y].getRange();
      }
      Rectangle2D temporary = computeRectangle(x, y, k, possibleMaximumOfHeight);
      double newArea = temporary.getWidth() * temporary.getHeight();
      double oldArea = result.getWidth() * result.getHeight();
      if (newArea > oldArea) {
        result = temporary;
      }
    }
    return result;
  }

  /**
   * Computes rectangle in region range [k..x][y..y+height].
   *
   * @param x
   *          end x coordinate
   * @param y
   *          start y coordinate
   * @param k
   *          start x coordinate
   * @param height
   *          height in y coordinate
   * @return rectangle in region range
   */
  private Rectangle2D computeRectangle(final int x, final int y, final int k, final int height) {
    double resX = matrix[k][y].getBorder().getX();
    double resY = matrix[k][y].getBorder().getY();
    double resWidth = matrix[x][y + height - 1].getBorder().getX() - resX
        + matrix[x][y + height - 1].getBorder().getWidth();
    double resHeight = matrix[x][y + height - 1].getBorder().getY() - resY
        + matrix[x][y + height - 1].getBorder().getHeight();

    return new Rectangle2D.Double(resX, resY, resWidth, resHeight);
  }

  /**
   * Computes how deep in y axis can regions be extended from starting x,y region.
   */
  private void findRanges() {
    for (int i = 0; i < getSizeX(); i++) {
      for (int j = 0; j < getSizeY(); j++) {
        if (!matrix[i][j].isAvailable()) {
          int k = j;
          int temporaryRange = 0;
          while (k >= 0 && matrix[i][k].getRange() == -1) {
            matrix[i][k--].setRange(temporaryRange++);
          }
        }
      }
      if (matrix[i][getSizeY() - 1].isAvailable()) {
        int k = getSizeY() - 1;
        int temporaryRange = 1;
        while (k >= 0 && matrix[i][k].getRange() == -1) {
          matrix[i][k--].setRange(temporaryRange++);
        }
      }

    }
  }

  /**
   * Disables regions for all aliases.
   */
  private void fillMatrixWithAliases() {
    for (final Element alias : aliases) {
      if (alias != mainAlias) {
        fillMatrixWithAlias(alias);
      }
    }
  }

  /**
   * Disable regions for given alias.
   *
   * @param alias
   *          alias that should disable regions
   */
  private void fillMatrixWithAlias(final Element alias) {
    Double a = alias.getX();
    Double b = alias.getX() + alias.getWidth();
    Double c = alias.getY() + alias.getHeight();
    Double d = alias.getY();

    fillWithPoints(a, b, c, d);
  }

  /**
   * Disable regions between given coordinates.
   *
   * @param x
   *          starting x coordinate
   * @param xw
   *          ending x coordinate
   * @param yh
   *          ending y coordinate
   * @param y
   *          starting y coordinate
   */
  private void fillWithPoints(final Double x, final Double xw, final Double yh, final Double y) {
    Integer widthStart = 0;
    Integer widthEnd = matrix.length;
    Integer heightStart = 0;
    Integer heightEnd = matrix[0].length;

    for (int i = 0; i < matrix.length; i++) {
      if (matrix[i][0].getBorder().getX() <= x) {
        widthStart = i;
      }
      if (matrix[i][0].getBorder().getX() <= xw) {
        widthEnd = i;
      }
    }
    if (matrix[matrix.length - 1][0].getBorder().getX() + matrix[matrix.length - 1][0].getBorder().getWidth() <= xw) {
      widthEnd = matrix.length;
    }
    for (int i = 0; i < matrix[0].length; i++) {
      if (matrix[0][i].getBorder().getY() <= y) {
        heightStart = i;
      }
      if (matrix[0][i].getBorder().getY() <= yh) {
        heightEnd = i;
      }
    }
    if (matrix[0][matrix[0].length - 1].getBorder().getY()
        + matrix[0][matrix[0].length - 1].getBorder().getHeight() <= yh) {
      heightEnd = matrix[0].length;
    }

    for (int i = widthStart; i < widthEnd; i++) {
      for (int j = heightStart; j < heightEnd; j++) {
        matrix[i][j].setAvailable(false);
      }
    }
  }

  /**
   * This method looks for a place to put description of the {@link Compartment}
   * on the map.
   *
   * @param compartment
   *          {@link Compartment} for which we try to determine text position
   * @param level
   *          level at which we will visualize element
   * @return bounds where text could be put
   */
  public Rectangle2D getRetangle(final Compartment compartment, final int level) {
    Map<Integer, List<Compartment>> lists = otherAliases.get(compartment);
    if (lists == null) {
      lists = new HashMap<>();
      otherAliases.put(compartment, lists);
    }
    List<Compartment> list = lists.get(level);
    if (list == null) {
      list = new ArrayList<Compartment>();
      for (final Compartment compAlias : model.getCompartments()) {
        if (zoomLevelMatcher.isVisible(level, compAlias.getVisibilityLevel())
            && !zoomLevelMatcher.isTransparent(level, compAlias.getTransparencyLevel())) {
          if (compAlias.cross(compartment) && compAlias.getSize() <= compartment.getSize()) {
            list.add(compAlias);
          }
        }
      }
      lists.put(level, list);
    }

    this.mainAlias = compartment;
    this.aliases = list;

    leftBound = compartment.getX();
    rightBound = (compartment.getX() + compartment.getWidth());
    upBound = compartment.getY();
    bottomBound = (compartment.getY() + compartment.getHeight());

    createMatrix();

    fillMatrixWithAliases();

    findRanges(); // 2*O(n^2)

    return findRectangle();
  }

  /**
   * Create empty matrix with fields.
   *
   * @see #matrix
   */
  protected void createMatrix() {
    Set<Double> xs = new TreeSet<Double>();
    Set<Double> ys = new TreeSet<Double>();
    List<Double> widths = new ArrayList<Double>();
    // CHECKSTYLE.OFF
    List<Double> xCoordsOfBegginingOfRectangle = new ArrayList<Double>();
    List<Double> yCoordsOfBegginingOfRectangle = new ArrayList<Double>();
    // CHECKSTYLE.ON
    List<Double> heights = new ArrayList<Double>();

    for (final Element alias : aliases) {
      double x1 = alias.getX();
      double x2 = alias.getX() + alias.getWidth();
      double y1 = alias.getY();
      double y2 = alias.getY() + alias.getHeight();
      if (x1 >= leftBound && x1 <= rightBound) {
        xs.add(x1);
      }
      if (x2 >= leftBound && x2 <= rightBound) {
        xs.add(x2);
      }

      if (y1 >= upBound && y1 <= bottomBound) {
        ys.add(y1);
      }
      if (y2 >= upBound && y2 <= bottomBound) {
        ys.add(y2);
      }
    }
    Double previous = null;
    for (final Double width : xs) {
      if (previous != null) {
        widths.add(width - previous);
      }
      xCoordsOfBegginingOfRectangle.add(width);
      previous = width;
    }
    previous = null;
    for (final Double height : ys) {
      if (previous != null) {
        heights.add(height - previous);
      }
      yCoordsOfBegginingOfRectangle.add(height);
      previous = height;
    }

    matrix = new Field[xs.size() - 1][ys.size() - 1];

    for (int i = 0; i < getSizeX(); i++) {
      for (int j = 0; j < getSizeY(); j++) {
        Field field = new Field();
        xCoordsOfBegginingOfRectangle.get(i);
        yCoordsOfBegginingOfRectangle.get(j);
        widths.get(i);
        heights.get(j);
        field.setBorder(new Rectangle2D.Double(xCoordsOfBegginingOfRectangle.get(i),
            yCoordsOfBegginingOfRectangle.get(j), widths.get(i), heights.get(j)));
        matrix[i][j] = field;
      }
    }
  }

  /**
   *
   * @return {@link #model}
   */
  public Model getModel() {
    return model;
  }

  /**
   * Returns width of the {@link #matrix}.
   *
   * @return width of the {@link #matrix}
   */
  private int getSizeX() {
    return matrix.length;
  }

  /**
   * Returns height of the {@link #matrix}.
   *
   * @return height of the {@link #matrix}
   */
  private int getSizeY() {
    return matrix[0].length;
  }

  /**
   * Represents small part of the map (enclosed by cuting lines) where something
   * can be put. Contains borders (height, weight, location etc). Informatin if it
   * can be used or no and range (magic field that describes how much to the
   * bottom rectangle can be extended).
   *
   * @author Piotr Gawron
   *
   */
  private class Field {
    /**
     * Border of the field.
     */
    private Rectangle2D border;
    /**
     * Can the field be used for something or is occupied?
     */
    private boolean available = true;
    /**
     * How many fields to the bottom are free (how much could we extend empty
     * rectangle.
     */
    private int range = -1;

    /**
     * @return the border
     * @see #border
     */
    Rectangle2D getBorder() {
      return border;
    }

    /**
     * @param border
     *          the border to set
     * @see #border
     */
    void setBorder(final Rectangle2D border) {
      this.border = border;
    }

    /**
     * @return the available
     * @see #available
     */
    public boolean isAvailable() {
      return available;
    }

    /**
     * @param available
     *          the available to set
     * @see #available
     */
    public void setAvailable(final boolean available) {
      this.available = available;
    }

    /**
     * @return the range
     * @see #range
     */
    public int getRange() {
      return range;
    }

    /**
     * @param range
     *          the range to set
     * @see #range
     */
    public void setRange(final int range) {
      this.range = range;
    }
  }

}
