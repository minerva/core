package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Ellipse2D;
import java.awt.geom.PathIterator;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This class defines methods used for drawing {@link SimpleMolecule} on the
 * {@link Graphics2D} object.
 * 
 * @author Piotr Gawron
 * 
 */
public class SimpleMoleculeConverter extends SpeciesConverter<SimpleMolecule> {

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into
   *          colors when drawing {@link Species}
   */
  public SimpleMoleculeConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected void drawImpl(final SimpleMolecule simpleMolecule, final Graphics2D graphics,
      final ConverterParams params) {

    int homodimer = simpleMolecule.getHomodimer();
    simpleMolecule.setWidth(simpleMolecule.getWidth() - SpeciesConverter.HOMODIMER_OFFSET * (homodimer - 1));
    simpleMolecule.setHeight(simpleMolecule.getHeight() - SpeciesConverter.HOMODIMER_OFFSET * (homodimer - 1));

    simpleMolecule.setX(simpleMolecule.getX() + SpeciesConverter.HOMODIMER_OFFSET * (homodimer));
    simpleMolecule.setY(simpleMolecule.getY() + SpeciesConverter.HOMODIMER_OFFSET * (homodimer));

    for (int i = 0; i < homodimer; i++) {
      simpleMolecule.setX(simpleMolecule.getX() - SpeciesConverter.HOMODIMER_OFFSET);
      simpleMolecule.setY(simpleMolecule.getY() - SpeciesConverter.HOMODIMER_OFFSET);
      Shape shape = getShape(simpleMolecule);
      Color oldColor = graphics.getColor();
      graphics.setColor(simpleMolecule.getFillColor());
      graphics.fill(shape);
      graphics.setColor(simpleMolecule.getBorderColor());
      Stroke stroke = graphics.getStroke();
      graphics.setStroke(getBorderLine(simpleMolecule));
      graphics.draw(shape);
      graphics.setStroke(stroke);
      graphics.setColor(oldColor);
    }
    simpleMolecule.setWidth(simpleMolecule.getWidth() + SpeciesConverter.HOMODIMER_OFFSET * (homodimer - 1));
    simpleMolecule.setHeight(simpleMolecule.getHeight() + SpeciesConverter.HOMODIMER_OFFSET * (homodimer - 1));
    drawText(simpleMolecule, graphics, params);
  }

  @Override
  protected Shape getShape(final SimpleMolecule simpleMolecule) {
    return new Ellipse2D.Double(simpleMolecule.getX(), simpleMolecule.getY(), simpleMolecule.getWidth(),
        simpleMolecule.getHeight());
  }

  @Override
  public PathIterator getBoundPathIterator(final SimpleMolecule simpleMolecule) {
    throw new InvalidStateException("This class doesn't provide boundPath");
  }

}
