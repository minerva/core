package lcsb.mapviewer.converter.graphics;

/**
 * Set of parameters used for drawing elements (Aliases) on the Graphics2D.
 * 
 * @author Piotr Gawron
 * 
 */
public class ConverterParams {

  /**
   * At which level the object is visualized. It helps to determine font size.
   * However it's possible that this value is not required.
   */
  private int level = 0;

  /**
   * What is the scale. It allows to adjust font size to be readable.
   */
  private double scale = 1;

  /**
   * Should the map be displayed in SBGN format.
   */
  private boolean sbgnFormat = false;

  /**
   * Does the visualization include nesting/hierarchy.
   */
  private boolean nested = false;

  /**
   * @param scale
   *          the scale to set
   * @return object with all parameters
   * @see #scale
   */
  public ConverterParams scale(final double scale) {
    this.scale = scale;
    return this;
  }

  /**
   * @param nested
   *          the nested to set
   * @return object with all parameters
   * @see #nested
   */
  public ConverterParams nested(final boolean nested) {
    this.nested = nested;
    return this;
  }

  /**
   * @return the level
   * @see #level
   */
  public int getLevel() {
    return level;
  }

  /**
   * @param level
   *          the level to set
   * @return ConverterParams object with all parameters
   * @see #level
   */
  public ConverterParams level(final int level) {
    this.level = level;
    return this;
  }

  /**
   * @param sbgnFormat
   *          the sbgnFormat to set
   * @return ConverterParams object with all parameters
   * @see #sbgnFormat
   */
  public ConverterParams sbgnFormat(final boolean sbgnFormat) {
    this.sbgnFormat = sbgnFormat;
    return this;
  }

  /**
   * @return the scale
   * @see #scale
   */
  public double getScale() {
    return scale;
  }

  /**
   * @return the sbgnFormat
   * @see #sbgnFormat
   */
  public boolean isSbgnFormat() {
    return sbgnFormat;
  }

  /**
   * @return the nested
   * @see #nested
   */
  public boolean isNested() {
    return nested;
  }

  @Override
  public String toString() {
    String result = "[" + this.getClass().getSimpleName() + "] "
        + "level:" + level + ","
        + "scale:" + scale + ","
        + "nested:" + nested + ","
        + "sbgnFormat:" + sbgnFormat;
    return result;
  }

}
