package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;

/**
 * This class defines methods used for drawing {@link SimpleMolecule} on the
 * {@link Graphics2D} object.
 * 
 * @author Piotr Gawron
 * 
 */
public class SimpleMoleculeSbgnConverter extends SimpleMoleculeConverter {

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link DataOverlayEntry} values into
   *          colors when drawing {@link Species}
   */
  public SimpleMoleculeSbgnConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected void drawImpl(final SimpleMolecule simpleMolecule, final Graphics2D graphics,
      final ConverterParams params) {
    int oldHomodimer = simpleMolecule.getHomodimer();

    int homodir;
    if (simpleMolecule.getHomodimer() > 1) {
      homodir = 2;
    } else {
      homodir = 1;
    }
    simpleMolecule.setHomodimer(homodir);
    super.drawImpl(simpleMolecule, graphics, params);
    simpleMolecule.setHomodimer(oldHomodimer);

    List<String> unitOfInformationText = new ArrayList<>();
    if (simpleMolecule.getStatePrefix() != null && simpleMolecule.getStateLabel() != null) {
      unitOfInformationText.add(simpleMolecule.getStatePrefix() + ":" + simpleMolecule.getStateLabel());
    }
    if (homodir > 1) {
      unitOfInformationText.add("N:" + simpleMolecule.getHomodimer());
    }

    Color oldColor = graphics.getColor();
    drawUnitOfInformation(unitOfInformationText, simpleMolecule, graphics);
    graphics.setColor(oldColor);
  }

  @Override
  protected Shape getShape(final SimpleMolecule simpleMolecule) {
    int arc = (int) Math.min(simpleMolecule.getWidth(), simpleMolecule.getHeight());
    return new RoundRectangle2D.Double(simpleMolecule.getX(), simpleMolecule.getY(), simpleMolecule.getWidth(),
        simpleMolecule.getHeight(), arc, arc);
  }

}
