package lcsb.mapviewer.converter.graphics.bioentity.element.compartment;

import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.model.map.compartment.BottomSquareCompartment;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;

/**
 * Class responsible for drawing BottomSquareCompartment on the Graphics2D.
 * 
 * @author Piotr Gawron
 * 
 */
public class BottomSquareCompartmentConverter extends CompartmentConverter<BottomSquareCompartment> {

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link DataOverlayEntry} values into
   *          colors when drawing {@link Species}
   */
  public BottomSquareCompartmentConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected Shape getOuterShape(final BottomSquareCompartment compartment) {
    return new Line2D.Double(0, compartment.getY(), compartment.getWidth(), compartment.getY());
  }

  @Override
  protected Shape getInnerShape(final BottomSquareCompartment compartment) {
    return new Line2D.Double(0, compartment.getY() + compartment.getThickness(), compartment.getWidth(),
        compartment.getY() + compartment.getThickness());
  }

  @Override
  protected Shape getBorderShape(final BottomSquareCompartment compartment) {
    return new Area(new Rectangle2D.Double(0.0, compartment.getY(), compartment.getWidth(), compartment.getHeight()));
  }

}
