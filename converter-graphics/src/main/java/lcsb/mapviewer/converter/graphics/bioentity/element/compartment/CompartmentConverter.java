package lcsb.mapviewer.converter.graphics.bioentity.element.compartment;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.common.geometry.LineTransformation;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.converter.graphics.bioentity.element.ElementConverter;
import lcsb.mapviewer.converter.graphics.geometry.FontFinder;
import lcsb.mapviewer.converter.graphics.geometry.RectangleTooSmallException;
import lcsb.mapviewer.converter.graphics.placefinder.PlaceFinder;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * Abstract class responsible for common methods to draw compartmentAliases on
 * Graphics2D.
 * 
 * @author Piotr Gawron
 * 
 * @param <T>
 *          class for which the comparator is created
 * 
 */
public abstract class CompartmentConverter<T extends Compartment> extends ElementConverter<T> {

  /**
   * Alpha level for inside of the transparent compartments.
   */
  public static final int DEFAULT_ALPHA_LEVEL = 8;
  /**
   * Default alpha level for semi-transparent borders.
   */
  protected static final int HIGH_ALPHA_LEVEL = 127;

  /**
   * Default alpha level for transparent compartments.
   */
  private static int alphaLevel = DEFAULT_ALPHA_LEVEL;
  /**
   * Class used for transformation of lines.
   */
  private LineTransformation lineTransformation = new LineTransformation();
  /**
   * Class used for finding place to draw description of the compartment.
   */
  private PlaceFinder placeFinder;

  /**
   * Object used for synchronization when accessing {@link #placeFinder}.
   */
  private String placeFinderSynchronization = "";

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link DataOverlayEntry} values into
   *          colors when drawing elements
   * 
   */
  protected CompartmentConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  /**
   * @return the alphaLevel
   */
  public static int getAlphaLevel() {
    return alphaLevel;
  }

  /**
   * @param alphaLevel
   *          the alphaLevel to set
   */
  public static void setAlphaLevel(final int alphaLevel) {
    CompartmentConverter.alphaLevel = alphaLevel;
  }

  /**
   * @return the lineTransformation
   */
  protected LineTransformation getLineTransformation() {
    return lineTransformation;
  }

  /**
   * @param lineTransformation
   *          the lineTransformation to set
   */
  protected void setLineTransformation(final LineTransformation lineTransformation) {
    this.lineTransformation = lineTransformation;
  }

  @Override
  protected void drawImpl(final T compartment, final Graphics2D graphics, final ConverterParams params) throws DrawingException {
    // keep the old values of color and line type
    Color oldColor = graphics.getColor();
    Stroke oldStroke = graphics.getStroke();

    Shape s1 = getOuterShape(compartment);
    Shape s3 = getInnerShape(compartment);
    Shape a1 = getBorderShape(compartment);

    Color borderColor = compartment.getBorderColor();

    Color fillColor = compartment.getFillColor();

    // fill the background
    boolean fill = !isTransparent(compartment, params);
    if (fill) {
      graphics.setColor(fillColor);
    } else {
      Color bgAlphaColor = new Color(fillColor.getRed(), fillColor.getGreen(), fillColor.getBlue(), getAlphaLevel());
      graphics.setColor(bgAlphaColor);
    }
    graphics.fill(s1);

    // create borders
    graphics.setColor(borderColor);
    graphics.setStroke(LineType.SOLID.getStroke(compartment.getOuterWidth()));
    graphics.draw(s1);
    graphics.setStroke(LineType.SOLID.getStroke(compartment.getInnerWidth()));
    graphics.draw(s3);

    fillColor = new Color(fillColor.getRed(), fillColor.getGreen(), fillColor.getBlue(), HIGH_ALPHA_LEVEL);
    graphics.setColor(fillColor);
    graphics.fill(a1);

    // restore color and line type
    graphics.setColor(oldColor);
    graphics.setStroke(oldStroke);

    // draw description
    if (fill) {
      Rectangle2D tmpBorder = compartment.getNameBorder();
      compartment.setNameBorder(compartment.getBorder());
      drawText(compartment, graphics, params);
      compartment.setNameBorder(tmpBorder);
    }
  }

  protected Shape getOuterShape(final T compartment) {
    throw new NotImplementedException();
  }

  protected Shape getInnerShape(final T compartment) {
    throw new NotImplementedException();
  }

  protected Shape getBorderShape(final T compartment) {
    Area result = new Area(getOuterShape(compartment));
    result.subtract(new Area(getInnerShape(compartment)));
    return result;
  }

  @Override
  public void drawText(final T compartment, final Graphics2D graphics, final ConverterParams params)
      throws DrawingException {
    if (compartment.getWidth() < Configuration.EPSILON || compartment.getHeight() < Configuration.EPSILON) {
      throw new DrawingException(
          new ElementUtils().getElementTag(compartment) + "Dimension of the alias must be bigger than 0.");
    }
    boolean textCentered = !isTransparent(compartment, params);
    Rectangle2D border;
    if (textCentered) {
      synchronized (placeFinderSynchronization) {
        if (placeFinder == null || placeFinder.getModel() != compartment.getModelData()) {
          placeFinder = new PlaceFinder(compartment.getModelData().getModel());
        }
        border = placeFinder.getRetangle(compartment, params.getLevel());
      }
    } else {
      border = new Rectangle2D.Double(
          compartment.getNameX(), compartment.getNameY(),
          compartment.getNameWidth(), compartment.getNameHeight());

    }
    double fontSize = compartment.getFontSize() * params.getScale();
    String fontName = Font.SANS_SERIF;
    Color tmpColor = graphics.getColor();
    try {
      graphics.setColor(compartment.getFontColor());
      fontSize = FontFinder.findMaxFontSize((int) Math.round(fontSize), fontName, graphics, border,
          compartment.getName());
      if (textCentered) {
        FontFinder.drawText((int) fontSize, fontName, graphics, border, compartment.getName(),
            HorizontalAlign.CENTER, VerticalAlign.MIDDLE);
      } else {
        FontFinder.drawText((int) fontSize, fontName, graphics, border, compartment.getName(),
            compartment.getNameHorizontalAlign(), compartment.getNameVerticalAlign());
      }
    } catch (final RectangleTooSmallException e) {
      // if it's too small then don't draw
      return;
    } finally {
      graphics.setColor(tmpColor);
    }
  }

}
