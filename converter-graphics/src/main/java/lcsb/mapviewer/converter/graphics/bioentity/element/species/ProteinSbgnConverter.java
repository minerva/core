package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.util.List;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.map.species.IonChannelProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This class defines methods used for drawing {@link Protein} on the
 * {@link Graphics2D} object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ProteinSbgnConverter extends ProteinConverter {

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into colors
   *          when drawing {@link Species}
   */
  public ProteinSbgnConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected void drawImpl(final Protein protein, final Graphics2D graphics, final ConverterParams params) {
    List<String> unitOfInformationText = getUnitOfInformation(protein);

    int originalHomodimer = protein.getHomodimer();
    int homodir;
    if (protein.getHomodimer() > 1) {
      homodir = 2;
    } else {
      homodir = 1;
    }

    protein.setHomodimer(homodir);
    super.drawImpl(protein, graphics, params);
    protein.setHomodimer(originalHomodimer);

    drawUnitOfInformation(unitOfInformationText, protein, graphics);
  }

  @Override
  protected List<String> getUnitOfInformation(final Protein protein) {
    List<String> unitOfInformationText = super.getUnitOfInformation(protein);
    if (protein instanceof IonChannelProtein) {
      if (protein.getActivity()) {
        unitOfInformationText.add("open");
      } else {
        unitOfInformationText.add("closed");
      }
    }

    if (protein.getHomodimer() > 1) {
      String unit = "N:" + Integer.toString(protein.getHomodimer());
      if (!unitOfInformationText.contains(unit)) {
        unitOfInformationText.add(unit);
      }
    }
    return unitOfInformationText;
  }

  @Override
  protected Shape getShape(final Protein protein) {
    return getGenericShape(protein);
  }

  @Override
  void drawActivityShape(final Protein protein, final Graphics2D graphics) {
  }
}
