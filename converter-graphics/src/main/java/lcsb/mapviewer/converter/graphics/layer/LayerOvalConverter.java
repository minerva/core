package lcsb.mapviewer.converter.graphics.layer;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.model.map.layout.graphics.LayerOval;

/**
 * This class allows to draw {@link LayerOval} on Graphics2D.
 * 
 * @author Piotr Gawron
 * 
 */
public class LayerOvalConverter {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  /**
   * Draws oval on the Graphics2D.
   * 
   * @param oval
   *          object to be drawn
   * @param graphics
   *          where we want to draw the object
   */
  public void draw(final LayerOval oval, final Graphics2D graphics) {
    Color tmpColor = graphics.getColor();
    graphics.setColor(oval.getColor());
    Ellipse2D o = new Ellipse2D.Double(oval.getX(), oval.getY(), oval.getWidth(), oval.getHeight());
    graphics.draw(o);
    graphics.setColor(tmpColor);
  }
}