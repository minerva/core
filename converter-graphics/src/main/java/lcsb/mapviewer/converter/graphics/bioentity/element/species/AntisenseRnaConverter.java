package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;

/**
 * This class defines methods used for drawing {@link AntisenseRna} on the
 * {@link Graphics2D} object.
 * 
 * @author Piotr Gawron
 * 
 */

public class AntisenseRnaConverter extends SpeciesConverter<AntisenseRna> {

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link DataOverlayEntry} values into
   *          colors when drawing {@link Species}
   */
  public AntisenseRnaConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected void drawImpl(final AntisenseRna antisenseRna, final Graphics2D graphics, final ConverterParams params) {
    Color oldColor = graphics.getColor();
    GeneralPath path = getShape(antisenseRna);
    graphics.setColor(antisenseRna.getFillColor());
    graphics.fill(path);
    Stroke stroke = graphics.getStroke();
    graphics.setColor(antisenseRna.getBorderColor());
    graphics.setStroke(getBorderLine(antisenseRna));
    graphics.draw(path);
    graphics.setStroke(stroke);

    drawText(antisenseRna, graphics, params);
    graphics.setColor(oldColor);
  }

  /**
   * Returns {@link AntisenseRna} border as a {@link GeneralPath} object.
   * 
   * @param antisenseRna
   *          {@link AntisenseRna} for which we want to get border
   * @return border of the {@link AntisenseRna}
   */
  @Override
  protected GeneralPath getShape(final AntisenseRna antisenseRna) {
    GeneralPath path = new GeneralPath(Path2D.WIND_EVEN_ODD, 4);
    path.moveTo(antisenseRna.getX(), antisenseRna.getY());
    path.lineTo(antisenseRna.getX() + antisenseRna.getWidth() * 3 / 4, antisenseRna.getY());
    path.lineTo(antisenseRna.getX() + antisenseRna.getWidth(), antisenseRna.getY() + antisenseRna.getHeight());
    path.lineTo(antisenseRna.getX() + antisenseRna.getWidth() / 4, antisenseRna.getY() + antisenseRna.getHeight());
    path.closePath();
    return path;
  }

  @Override
  public PathIterator getBoundPathIterator(final AntisenseRna alias) {
    return getShape(alias).getPathIterator(new AffineTransform());
  }

}
