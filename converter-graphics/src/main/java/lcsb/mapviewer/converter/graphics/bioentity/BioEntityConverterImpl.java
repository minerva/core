package lcsb.mapviewer.converter.graphics.bioentity;

import java.awt.Graphics2D;
import java.util.List;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.converter.graphics.bioentity.element.compartment.BottomSquareCompartmentConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.compartment.LeftSquareCompartmentConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.compartment.OvalCompartmentConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.compartment.PathwayCompartmentConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.compartment.RightSquareCompartmentConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.compartment.SquareCompartmentConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.compartment.TopSquareCompartmentConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.species.AntisenseRnaConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.species.ComplexConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.species.ComplexSbgnConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.species.DegradedConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.species.DrugConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.species.GeneConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.species.IonConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.species.PhenotypeConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.species.ProteinConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.species.ProteinSbgnConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.species.RnaConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.species.SBGNNucleicAcidFeatureConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.species.SimpleMoleculeConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.species.SimpleMoleculeSbgnConverter;
import lcsb.mapviewer.converter.graphics.bioentity.element.species.UnknownConverter;
import lcsb.mapviewer.converter.graphics.bioentity.reaction.ReactionConverter;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.compartment.BottomSquareCompartment;
import lcsb.mapviewer.model.map.compartment.LeftSquareCompartment;
import lcsb.mapviewer.model.map.compartment.OvalCompartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.compartment.RightSquareCompartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.compartment.TopSquareCompartment;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.IonChannelProtein;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.ReceptorProtein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.TruncatedProtein;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * This class is designed to convert any type of {@link Element} into a graphic
 * glyph that will be visualized on the {@link Graphics2D} object.
 * 
 * @author Piotr Gawron
 * 
 */
public class BioEntityConverterImpl extends BioEntityConverter<BioEntity> {

  /**
   * Converter used for conversion of the {@link Element} given in constructor.
   */
  @SuppressWarnings("rawtypes")
  private BioEntityConverter elementConverter = null;

  /**
   * Support constructor. Used in case of SBGN format display
   *
   * @param element
   *          {@link Element} for which this converter will be used
   * @param colorExtractor
   *          object that helps to convert overlay values into colors
   * @param sbgnFormat
   *          boolean value indicating if SBGN display format should be used
   */
  public BioEntityConverterImpl(final BioEntity element, final boolean sbgnFormat, final ColorExtractor colorExtractor) {

    if (sbgnFormat) {
      elementConverter = getSbgnConverterForElement(element, colorExtractor);
    } else {
      elementConverter = getConverterForElement(element, colorExtractor);
    }

    // if we don't know which converter to use then throw an exception
    if (elementConverter == null) {
      throw new InvalidArgumentException(
          new ElementUtils().getElementTag(element) + "Unknown converter for class: " + element.getClass());
    }
  }

  private BioEntityConverter<? extends BioEntity> getSbgnConverterForElement(final BioEntity element,
      final ColorExtractor colorExtractor) {
    if (element instanceof AntisenseRna || element instanceof Rna || element instanceof Gene) {
      return new SBGNNucleicAcidFeatureConverter(colorExtractor);
    } else if (element instanceof Protein) {
      return new ProteinSbgnConverter(colorExtractor);
    } else if (element instanceof SimpleMolecule) {
      return new SimpleMoleculeSbgnConverter(colorExtractor);
    } else if (element instanceof Complex) {
      return new ComplexSbgnConverter(colorExtractor);
    } else {
      return getConverterForElement(element, colorExtractor);
    }
  }

  /**
   * Default constructor.
   *
   * @param colorExtractor
   *          object that helps to convert overlay values into colors
   * @param element
   *          {@link Element} for which this converter will be used
   */
  public BioEntityConverterImpl(final Element element, final ColorExtractor colorExtractor) {
    this(element, false, colorExtractor);
  }

  /**
   * Returns a converter for given element. If converter doesn't exist then
   * exception is thrown.
   *
   * @param element
   *          {@link Element} for which we are looking for a converter
   * @param colorExtractor
   *          object that helps to convert overlay values into colors
   * @return converter that can be applied for the given element
   */
  private BioEntityConverter<? extends BioEntity> getConverterForElement(final BioEntity element,
      final ColorExtractor colorExtractor) {
    if (element == null) {
      throw new InvalidArgumentException("element cannot be null");
    }
    if (element instanceof GenericProtein) {
      return new ProteinConverter(colorExtractor);
    } else if (element instanceof IonChannelProtein) {
      return new ProteinConverter(colorExtractor);
    } else if (element instanceof ReceptorProtein) {
      return new ProteinConverter(colorExtractor);
    } else if (element instanceof TruncatedProtein) {
      return new ProteinConverter(colorExtractor);
    } else if (element instanceof Degraded) {
      return new DegradedConverter(colorExtractor);
    } else if (element instanceof Complex) {
      return new ComplexConverter(colorExtractor);
    } else if (element instanceof SimpleMolecule) {
      return new SimpleMoleculeConverter(colorExtractor);
    } else if (element instanceof Drug) {
      return new DrugConverter(colorExtractor);
    } else if (element instanceof Ion) {
      return new IonConverter(colorExtractor);
    } else if (element instanceof Phenotype) {
      return new PhenotypeConverter(colorExtractor);
    } else if (element instanceof Rna) {
      return new RnaConverter(colorExtractor);
    } else if (element instanceof AntisenseRna) {
      return new AntisenseRnaConverter(colorExtractor);
    } else if (element instanceof Gene) {
      return new GeneConverter(colorExtractor);
    } else if (element instanceof Unknown) {
      return new UnknownConverter(colorExtractor);
    } else if (element instanceof SquareCompartment) {
      return new SquareCompartmentConverter(colorExtractor);
    } else if (element instanceof OvalCompartment) {
      return new OvalCompartmentConverter(colorExtractor);
    } else if (element instanceof PathwayCompartment) {
      return new PathwayCompartmentConverter(colorExtractor);
    } else if (element instanceof BottomSquareCompartment) {
      return new BottomSquareCompartmentConverter(colorExtractor);
    } else if (element instanceof TopSquareCompartment) {
      return new TopSquareCompartmentConverter(colorExtractor);
    } else if (element instanceof LeftSquareCompartment) {
      return new LeftSquareCompartmentConverter(colorExtractor);
    } else if (element instanceof RightSquareCompartment) {
      return new RightSquareCompartmentConverter(colorExtractor);
    } else if (element instanceof Reaction) {
      return new ReactionConverter(colorExtractor);
    } else {
      throw new NotImplementedException(new ElementUtils().getElementTag(element) + "Unknown element class");
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  protected void drawImpl(final BioEntity bioEntity, final Graphics2D graphics, final ConverterParams params) throws DrawingException {
    elementConverter.draw(bioEntity, graphics, params);
  }

  @SuppressWarnings("unchecked")
  @Override
  public void draw(final BioEntity element, final Graphics2D graphics, final ConverterParams params,
      final List<List<DataOverlayEntry>> visualizedDataOverlayEntries) throws DrawingException {
    try {
      if (isVisible(element, params)) {
        elementConverter.draw(element, graphics, params, visualizedDataOverlayEntries);
      }
    } catch (final Exception e) {
      throw new DrawingException(new ElementUtils().getElementTag(element) + "Problem with drawing element.", e);
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public void drawText(final BioEntity element, final Graphics2D graphics, final ConverterParams params)
      throws DrawingException {
    if (isVisible(element, params)) {
      elementConverter.drawText(element, graphics, params);
    }
  }

}
