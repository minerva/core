package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.map.species.Phenotype;

/**
 * This class defines methods used for drawing SpeciesAlias of {@link Phenotype}
 * on the {@link Graphics2D} object.
 * 
 * @author Piotr Gawron
 * 
 */
public class PhenotypeConverter extends SpeciesConverter<Phenotype> {

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into colors
   *          when drawing elements
   * 
   */
  public PhenotypeConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected void drawImpl(final Phenotype phenotype, final Graphics2D graphics, final ConverterParams params) {
    GeneralPath path = getShape(phenotype);

    Color oldColor = graphics.getColor();
    graphics.setColor(phenotype.getFillColor());
    graphics.fill(path);
    graphics.setColor(phenotype.getBorderColor());
    Stroke stroke = graphics.getStroke();
    graphics.setStroke(getBorderLine(phenotype));
    graphics.draw(path);
    graphics.setStroke(stroke);
    drawText(phenotype, graphics, params);
    graphics.setColor(oldColor);
  }

  /**
   * Returns shape of the {@link Phenotype} as a {@link GeneralPath} object.
   * 
   * @param phenotype
   *          {@link Phenotype} for which we are looking for a border
   * @return {@link GeneralPath} object defining border of the given
   *         {@link Phenotype}
   */
  @Override
  protected GeneralPath getShape(final Phenotype phenotype) {
    GeneralPath path = new GeneralPath(Path2D.WIND_EVEN_ODD, 6);

    double x = phenotype.getX();
    double y = phenotype.getY();
    double width = phenotype.getWidth();
    double height = phenotype.getHeight();

    double cutSize = Math.min(width, height) / 2;

    path.moveTo(x, y + height / 2);
    path.lineTo(x + cutSize, y);
    path.lineTo(x + width - cutSize, y);
    path.lineTo(x + width, y + height / 2);
    path.lineTo(x + width - cutSize, y + height);
    path.lineTo(x + cutSize, y + height);

    path.closePath();
    return path;
  }

  @Override
  public PathIterator getBoundPathIterator(final Phenotype phenotype) {
    return getShape(phenotype).getPathIterator(new AffineTransform());
  }

}
