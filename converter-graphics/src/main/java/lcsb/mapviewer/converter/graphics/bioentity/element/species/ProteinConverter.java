package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.IonChannelProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.ReceptorProtein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.TruncatedProtein;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;

/**
 * This class defines methods used for drawing {@link Protein} on the
 * {@link Graphics2D} object.
 * 
 * @author Piotr Gawron
 * 
 */
public class ProteinConverter extends SpeciesConverter<Protein> {

  /**
   * Width of the ion part in the open channel representation.
   */
  private static final int ION_CHANNEL_WIDTH = 20;

  /**
   * Width of the gap in the open channel representation.
   */
  private static final int OPEN_ION_CHANNEL_WIDTH = 20;

  /**
   * How big should be the arc in rectangle for protein representation.
   */
  private static final int RECTANGLE_CORNER_ARC_SIZE = 10;

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link DataOverlayEntry} values into
   *          colors when drawing {@link Species}
   */
  public ProteinConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  /**
   * Returns shape of {@link Protein}.
   * 
   * @param protein
   *          {@link Protein} for which we are looking for a border
   * @return Shape object defining given alias
   */
  Shape getGenericShape(final Protein protein) {
    return new RoundRectangle2D.Double(protein.getX(), protein.getY(), protein.getWidth(), protein.getHeight(),
        RECTANGLE_CORNER_ARC_SIZE, RECTANGLE_CORNER_ARC_SIZE);
  }

  @Override
  protected void drawImpl(final Protein protein, final Graphics2D graphics, final ConverterParams params) {

    java.util.List<String> unitOfInformationText = getUnitOfInformation(protein);

    int homodir = protein.getHomodimer();

    protein.setWidth(protein.getWidth() - SpeciesConverter.HOMODIMER_OFFSET * (protein.getHomodimer() - 1));
    protein.setHeight(protein.getHeight() - SpeciesConverter.HOMODIMER_OFFSET * (protein.getHomodimer() - 1));

    protein.setX(protein.getX() + SpeciesConverter.HOMODIMER_OFFSET * (homodir));
    protein.setY(protein.getY() + SpeciesConverter.HOMODIMER_OFFSET * (homodir));

    for (int homodimerId = 0; homodimerId < homodir; homodimerId++) {
      protein.setX(protein.getX() - SpeciesConverter.HOMODIMER_OFFSET);
      protein.setY(protein.getY() - SpeciesConverter.HOMODIMER_OFFSET);

      drawActivityShape(protein, graphics);
      Shape shape = getShape(protein);
      Color oldColor = graphics.getColor();
      graphics.setColor(protein.getFillColor());
      graphics.fill(shape);
      graphics.setColor(protein.getBorderColor());
      Stroke stroke = graphics.getStroke();
      graphics.setStroke(getBorderLine(protein));
      graphics.draw(shape);
      graphics.setStroke(stroke);
      graphics.setColor(oldColor);
    }

    if (!params.isSbgnFormat()) {
      drawUnitOfInformation(unitOfInformationText, protein, graphics);
    }
    drawText(protein, graphics, params);
    protein.setWidth(protein.getWidth() + SpeciesConverter.HOMODIMER_OFFSET * (protein.getHomodimer() - 1));
    protein.setHeight(protein.getHeight() + SpeciesConverter.HOMODIMER_OFFSET * (protein.getHomodimer() - 1));
  }

  protected java.util.List<String> getUnitOfInformation(final Protein protein) {
    java.util.List<String> unitOfInformationText = new ArrayList<>();
    if (protein.getStatePrefix() != null && protein.getStateLabel() != null) {
      if (protein.getStatePrefix().equals("free input")) {
        unitOfInformationText.add(protein.getStateLabel());
      } else {
        unitOfInformationText.add(protein.getStatePrefix() + ":" + protein.getStateLabel());
      }
    }
    return unitOfInformationText;
  }

  void drawActivityShape(final Protein protein, final Graphics2D graphics) {
    if (protein instanceof GenericProtein && protein.getActivity()) {
      drawActivityGenericProtein(protein, graphics);
    } else if (protein instanceof TruncatedProtein && protein.getActivity()) {
      drawActivityTruncatedShape(protein, graphics);
    } else if (protein instanceof ReceptorProtein && protein.getActivity()) {
      drawActivityReceptorProtein(protein, graphics);
    }
  }

  @Override
  protected Shape getShape(final Protein protein) {
    Shape shape = null;
    if (protein instanceof GenericProtein) {
      shape = getGenericShape(protein);
    } else if (protein instanceof IonChannelProtein) {
      shape = getIonChannelShape(protein);
    } else if (protein instanceof TruncatedProtein) {
      shape = getTruncatedShape(protein);
    } else if (protein instanceof ReceptorProtein) {
      shape = getReceptorShape(protein);
    } else {
      logger.warn(new LogMarker(ProjectLogEntryType.DRAWING_ISSUE, protein) + "Unknown shape for protein");
      shape = getDefaultAliasShape(protein);
    }
    return shape;
  }

  /**
   * Draws activity border of {@link GenericProtein}.
   * 
   * @param protein
   *          {@link Protein} that will be drawn
   * @param graphics
   *          where we are drawing
   */
  void drawActivityGenericProtein(final Protein protein, final Graphics2D graphics) {
    int border = ACTIVITY_BORDER_DISTANCE;
    protein.increaseBorder(border);
    Shape shape2 = getGenericShape(protein);
    Stroke stroke = graphics.getStroke();
    graphics.setStroke(LineType.DOTTED.getStroke(protein.getLineWidth()));
    graphics.draw(shape2);
    graphics.setStroke(stroke);
    protein.increaseBorder(-border);
  }

  /**
   * Draws activity border of {@link ReceptorProtein}.
   * 
   * @param protein
   *          {@link Protein} that will be drawn
   * @param graphics
   *          where we are drawing
   */
  public void drawActivityReceptorProtein(final Protein protein, final Graphics2D graphics) {
    int border = ACTIVITY_BORDER_DISTANCE;
    protein.setX(protein.getX() - border);
    protein.setY(protein.getY() - border);
    protein.setWidth(protein.getWidth() + border * 2);
    protein.setHeight(protein.getHeight() + border * 2);
    Shape shape2 = getReceptorShape(protein);
    Stroke stroke = graphics.getStroke();
    graphics.setStroke(LineType.DOTTED.getStroke(protein.getLineWidth()));
    graphics.draw(shape2);
    graphics.setStroke(stroke);
    protein.setX(protein.getX() + border);
    protein.setY(protein.getY() + border);
    protein.setWidth(protein.getWidth() - border * 2);
    protein.setHeight(protein.getHeight() - border * 2);
  }

  /**
   * Draws activity border of {@link TruncatedProtein}.
   * 
   * @param protein
   *          {@link Protein} that will be drawn
   * @param graphics
   *          where we are drawing
   */
  public void drawActivityTruncatedShape(final Protein protein, final Graphics2D graphics) {
    int border = ACTIVITY_BORDER_DISTANCE;
    protein.setX(protein.getX() - border);
    protein.setY(protein.getY() - border);
    protein.setWidth(protein.getWidth() + border * 2);
    protein.setHeight(protein.getHeight() + border * 2);
    Shape shape2 = getTruncatedShape(protein);
    Stroke stroke = graphics.getStroke();
    graphics.setStroke(LineType.DOTTED.getStroke(protein.getLineWidth()));
    graphics.draw(shape2);
    graphics.setStroke(stroke);
    protein.setX(protein.getX() + border);
    protein.setY(protein.getY() + border);
    protein.setWidth(protein.getWidth() - border * 2);
    protein.setHeight(protein.getHeight() - border * 2);
  }

  /**
   * Returns shape of receptor protein.
   * 
   * @param protein
   *          alias for which we are looking for a border
   * @return Shape object defining given alias
   */
  protected Shape getReceptorShape(final Protein protein) {
    Shape shape;
    GeneralPath path = new GeneralPath(Path2D.WIND_EVEN_ODD);
    ArrayList<Point2D> points = getReceptorPoints(protein);
    path.moveTo(points.get(0).getX(), points.get(0).getY());
    for (int i = 1; i < points.size(); i++) {
      path.lineTo(points.get(i).getX(), points.get(i).getY());
    }
    path.closePath();
    shape = path;
    return shape;
  }

  /**
   * Returns shape of truncated protein.
   * 
   * @param protein
   *          alias for which we are looking for a border
   * @return Shape object defining given alias
   */
  protected Shape getTruncatedShape(final Protein protein) {
    Shape shape;
    GeneralPath path = new GeneralPath();
    path.moveTo(protein.getX() + 10, protein.getY());
    path.lineTo(protein.getX() + protein.getWidth(), protein.getY());
    path.lineTo(protein.getX() + protein.getWidth(), protein.getY() + protein.getHeight() * 3 / 5);
    path.lineTo(protein.getX() + protein.getWidth() * 4 / 5, protein.getY() + protein.getHeight() * 2 / 5);
    path.lineTo(protein.getX() + protein.getWidth() * 4 / 5, protein.getY() + protein.getHeight());
    path.lineTo(protein.getX() + 10, protein.getY() + protein.getHeight());
    path.curveTo(protein.getX() + 5, protein.getY() + protein.getHeight() - 2, protein.getX() + 2,
        protein.getY() + protein.getHeight() - 5, protein.getX(), protein.getY() + protein.getHeight() - 10);
    path.lineTo(protein.getX(), protein.getY() + 10);
    path.curveTo(protein.getX() + 2, protein.getY() + 5, protein.getX() + 5, protein.getY() + 2, protein.getX() + 10,
        protein.getY());

    path.closePath();
    shape = path;
    return shape;
  }

  /**
   * Returns shape of receptor protein as a list of points.
   * 
   * @param protein
   *          alias for which we are looking for a border
   * @return list of points defining border of the given alias
   */
  private ArrayList<Point2D> getReceptorPoints(final Protein protein) {
    double x = protein.getX();
    double y = protein.getY();
    double width = protein.getWidth();
    double height = protein.getHeight();
    ArrayList<Point2D> points = new ArrayList<>();

    points.add(new Point2D.Double(x, y + height * 2 / 5));
    points.add(new Point2D.Double(x, y));
    points.add(new Point2D.Double(x + width / 2, y + height / 5));
    points.add(new Point2D.Double(x + width, y));
    points.add(new Point2D.Double(x + width, y + height * 2 / 5));
    points.add(new Point2D.Double(x + width, y + height * 4 / 5));
    points.add(new Point2D.Double(x + width / 2, y + height));
    points.add(new Point2D.Double(x, y + height * 4 / 5));

    return points;
  }

  @Override
  public PathIterator getBoundPathIterator(final Protein protein) {
    if (protein instanceof GenericProtein) {
      return getGenericShape(protein).getPathIterator(new AffineTransform());
    } else if (protein instanceof ReceptorProtein) {
      return getReceptorShape(protein).getPathIterator(new AffineTransform());
    } else if (protein instanceof IonChannelProtein) {
      return getGenericShape(protein).getPathIterator(new AffineTransform());
    } else if (protein instanceof TruncatedProtein) {
      return getTruncatedShape(protein).getPathIterator(new AffineTransform());
    } else {
      throw new InvalidArgumentException("Not implemented protein converter for type: " + protein.getClass());
    }
  }

  Shape getIonChannelShape(final Protein protein) {
    Area a1;
    if (!protein.getActivity()) {
      a1 = new Area(
          new RoundRectangle2D.Double(protein.getX(), protein.getY(), protein.getWidth() - ION_CHANNEL_WIDTH - 1,
              protein.getHeight(), RECTANGLE_CORNER_ARC_SIZE, RECTANGLE_CORNER_ARC_SIZE));
    } else {
      a1 = new Area(new RoundRectangle2D.Double(protein.getX(), protein.getY(),
          protein.getWidth() - ION_CHANNEL_WIDTH - OPEN_ION_CHANNEL_WIDTH - 1, protein.getHeight(),
          RECTANGLE_CORNER_ARC_SIZE, RECTANGLE_CORNER_ARC_SIZE));
    }
    Area a2 = new Area(
        new RoundRectangle2D.Double(protein.getX() + protein.getWidth() - ION_CHANNEL_WIDTH, protein.getY(),
            ION_CHANNEL_WIDTH, protein.getHeight(), RECTANGLE_CORNER_ARC_SIZE, RECTANGLE_CORNER_ARC_SIZE));
    a1.add(a2);
    return a1;
  }

}
