package lcsb.mapviewer.converter.graphics.bioentity.element.compartment;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This class allows to draw {@link PathwayCompartment} on the
 * {@link Graphics2D} class.
 * 
 * @author Piotr Gawron
 * 
 */
public class PathwayCompartmentConverter extends CompartmentConverter<PathwayCompartment> {

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into colors
   *          when drawing {@link Species}
   */
  public PathwayCompartmentConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected void drawImpl(final PathwayCompartment compartment, final Graphics2D graphics, final ConverterParams params)
      throws DrawingException {
    // keep the old values of colors and line
    Color oldColor = graphics.getColor();
    Stroke oldStroke = graphics.getStroke();

    Shape shape = new Rectangle2D.Double(compartment.getX(), compartment.getY(), compartment.getWidth(),
        compartment.getHeight());

    Color borderColor = compartment.getBorderColor();
    Color backgroundColor = compartment.getFillColor();

    // fill the background
    boolean fill = !isTransparent(compartment, params);
    if (!fill) {
      backgroundColor = new Color(backgroundColor.getRed(), backgroundColor.getGreen(), backgroundColor.getBlue(),
          getAlphaLevel());
    }
    graphics.setColor(backgroundColor);
    graphics.fill(shape);

    // draw the border
    graphics.setColor(borderColor);
    graphics.setStroke(LineType.SOLID.getStroke(compartment.getOuterWidth()));
    graphics.draw(shape);

    // draw description of the compartment
    if (fill) {
      Rectangle2D tmpBorder = compartment.getNameBorder();
      compartment.setNameBorder(compartment.getBorder());
      drawText(compartment, graphics, params);
      compartment.setNameBorder(tmpBorder);
    } else {
      drawText(compartment, graphics, params);
    }

    // restore old color and line type
    graphics.setColor(oldColor);
    graphics.setStroke(oldStroke);

  }

}
