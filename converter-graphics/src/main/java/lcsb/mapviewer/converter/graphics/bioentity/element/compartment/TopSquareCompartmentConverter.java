package lcsb.mapviewer.converter.graphics.bioentity.element.compartment;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.model.map.compartment.TopSquareCompartment;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Class responsible for drawing TopSquareCompartment on the {@link Graphics2D}.
 * 
 * @author Piotr Gawron
 * 
 */
public class TopSquareCompartmentConverter extends CompartmentConverter<TopSquareCompartment> {

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into
   *          colors when drawing {@link Species}
   */
  public TopSquareCompartmentConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected Shape getOuterShape(final TopSquareCompartment compartment) {
    return new Line2D.Double(0, compartment.getHeight(), compartment.getWidth(), compartment.getHeight());
  }

  @Override
  protected Shape getInnerShape(final TopSquareCompartment compartment) {
    return new Line2D.Double(
        0, compartment.getHeight() - compartment.getThickness(), compartment.getWidth(),
        compartment.getHeight() - compartment.getThickness());
  }

  @Override
  protected Shape getBorderShape(final TopSquareCompartment compartment) {
    return new Area(new Rectangle2D.Double(0.0, 0.0, compartment.getWidth(), compartment.getHeight()));
  }

}
