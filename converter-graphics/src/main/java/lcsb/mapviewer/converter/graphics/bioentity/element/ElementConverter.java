package lcsb.mapviewer.converter.graphics.bioentity.element;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.converter.graphics.bioentity.BioEntityConverter;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;

/**
 * This interface defines what operations should be possible to convert
 * {@link Element} into a graphics on Graphics2D object.
 * 
 * @author Piotr Gawron
 * 
 * @param <T>
 *          class of alias to convert
 */
public abstract class ElementConverter<T extends Element> extends BioEntityConverter<T> {

  /**
   * Object that helps to convert {@link DataOverlayEntry} values into colors.
   */
  private ColorExtractor colorExtractor;

  protected ElementConverter(final ColorExtractor colorExtractor) {
    this.colorExtractor = colorExtractor;
  }

  @Override
  public final void draw(final T bioEntity, final Graphics2D graphics, final ConverterParams params) throws DrawingException {
    if (bioEntity.getGlyph() != null) {
      drawGlyph(bioEntity, graphics);
    } else {
      super.draw(bioEntity, graphics, params);
    }
  }

  @Override
  public final void draw(final T element, final Graphics2D graphics, final ConverterParams params,
      final List<List<DataOverlayEntry>> visualizedDataOverlayssColorSchemas) throws DrawingException {
    if (element.getGlyph() != null) {
      drawGlyph(element, graphics);
    } else {
      drawImpl(element, graphics, params);
    }

    Color oldColor = graphics.getColor();
    int count = 0;
    double width = element.getWidth() / visualizedDataOverlayssColorSchemas.size();
    for (final List<DataOverlayEntry> schemas : visualizedDataOverlayssColorSchemas) {
      if (schemas != null) {
        double startX = (double) count / (double) visualizedDataOverlayssColorSchemas.size();

        int x = (int) (startX * element.getWidth() + element.getX());

        int schemaCount = schemas.size();
        boolean drawBorder = false;
        for (int i = 0; i < schemaCount; i++) {
          DataOverlayEntry schema = schemas.get(i);
          if (schema != null) {
            Color color = getNormalizedColor(schema);
            Color bgAlphaColor = new Color(color.getRed(), color.getGreen(), color.getBlue(), LAYOUT_ALPHA);
            graphics.setColor(bgAlphaColor);
            int y = (int) (element.getY() + i * element.getHeight() / schemaCount);
            int height = (int) (element.getY() + (i + 1) * element.getHeight() / schemaCount) - y;
            graphics.fillRect(x, y, (int) width, height);
            drawBorder = true;
          }
        }
        if (drawBorder) {
          graphics.setColor(Color.BLACK);
          graphics.drawRect(x, element.getY().intValue(), (int) width, element.getHeight().intValue());
        }
      }
      count++;
    }
    graphics.setColor(oldColor);
  }

  /**
   * Draws a {@link Glyph} for given bioEntity.
   * 
   * @param bioEntity
   *          element that should be visualized as a {@link Glyph}
   * @param graphics
   *          {@link Graphics2D} where we are drawing
   * @throws DrawingException
   *           thrown when there is a problem with drawing
   */
  protected void drawGlyph(final T bioEntity, final Graphics2D graphics) throws DrawingException {
    try {
      Image img = ImageIO.read(new ByteArrayInputStream(bioEntity.getGlyph().getFile().getFileContent()));
      
      Image scaledImage = img.getScaledInstance(bioEntity.getWidth().intValue(), bioEntity.getHeight().intValue(), Image.SCALE_SMOOTH);

      graphics.drawImage(scaledImage,
          bioEntity.getX().intValue(), bioEntity.getY().intValue(),
          null);
    } catch (final IOException e) {
      throw new DrawingException(
          "Problem with processing glyph file: " + bioEntity.getGlyph().getFile().getOriginalFileName(), e);
    }
  }

  protected Color getNormalizedColor(final DataOverlayEntry schema) {
    return colorExtractor.getNormalizedColor(schema);
  }

}
