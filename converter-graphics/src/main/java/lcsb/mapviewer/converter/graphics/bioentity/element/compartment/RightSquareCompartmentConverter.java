package lcsb.mapviewer.converter.graphics.bioentity.element.compartment;

import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.model.map.compartment.RightSquareCompartment;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Class responsible for drawing RightSquareCompartment on the Graphics2D.
 * 
 * @author Piotr Gawron
 * 
 */
public class RightSquareCompartmentConverter extends CompartmentConverter<RightSquareCompartment> {

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into
   *          colors when drawing {@link Species}
   */
  public RightSquareCompartmentConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected Shape getOuterShape(final RightSquareCompartment compartment) {
    return new Line2D.Double(compartment.getX(), compartment.getHeight(), compartment.getX(), 0);
  }

  @Override
  protected Shape getInnerShape(final RightSquareCompartment compartment) {
    return new Line2D.Double(compartment.getX() + compartment.getThickness(), compartment.getHeight(),
        compartment.getX(), 0);
  }

  @Override
  protected Shape getBorderShape(final RightSquareCompartment compartment) {
    return new Area(
        new Rectangle2D.Double(compartment.getX(), 0.0, compartment.getWidth(), compartment.getHeight()));
  }
}
