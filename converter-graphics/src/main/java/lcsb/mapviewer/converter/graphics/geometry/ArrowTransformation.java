package lcsb.mapviewer.converter.graphics.geometry;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.ArrowTypeData;
import lcsb.mapviewer.model.graphics.PolylineData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Class containing basic function for drawing arrows.
 *
 * @author Piotr Gawron
 */
public class ArrowTransformation {
  /**
   * Distance of the crossbar from main arrow in the crossbar arrow end.
   */
  private static final int CROSSBAR_ARROW_END = 5;

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Object used to perform transformations on point objects.
   */
  private PointTransformation pointTransformation = new PointTransformation();

  /**
   * Draw normal arrow head (with white filling).
   *
   * @param line     line on which we draw arrow end
   * @param graphics graphics object on which we draw
   * @param len      length of the arrow part
   * @param angle    angle of the arrow head
   */
  private void drawBlankEnd(final Line2D line, final Graphics2D graphics, final double len, final double angle) {

    Point2D startPoint = line.getP1();
    Point2D endPoint = line.getP2();

    double dx = startPoint.getX() - endPoint.getX();
    double dy = startPoint.getY() - endPoint.getY();
    double dist = Math.sqrt(dx * dx + dy * dy);
    double coef = len / dist;
    double arrowX = endPoint.getX() - coef * dx;
    double arrowY = endPoint.getY() - coef * dy;
    Point2D p1 = new Point2D.Double(arrowX, arrowY);
    p1 = pointTransformation.rotatePoint(p1, angle, endPoint);
    Point2D p2 = new Point2D.Double(arrowX, arrowY);
    p2 = pointTransformation.rotatePoint(p2, -angle, endPoint);

    GeneralPath triangle = new GeneralPath();
    triangle.moveTo(endPoint.getX(), endPoint.getY());
    triangle.lineTo(p1.getX(), p1.getY());
    triangle.lineTo(p2.getX(), p2.getY());
    triangle.closePath();

    Color color = graphics.getColor();
    graphics.setColor(Color.WHITE);
    graphics.fill(triangle);
    graphics.setColor(color);
    graphics.draw(triangle);

  }

  /**
   * Draw normal arrow head (with white filling) with a crossbar (short
   * perpendicular line to the one given as input) behind it.
   *
   * @param line     line on which we draw arrow end
   * @param graphics graphics object on which we draw
   * @param len      length of the arrow part
   * @param angle    angle of the arrow head
   */
  private void drawBlankCrossbarEnd(final Line2D line, final Graphics2D graphics, final double len,
                                    final double angle) {

    double len1 = len;
    double len2 = len + CROSSBAR_ARROW_END;

    Point2D startPoint = line.getP1();
    Point2D endPoint = line.getP2();

    double dx = startPoint.getX() - endPoint.getX();
    double dy = startPoint.getY() - endPoint.getY();
    double dist = Math.sqrt(dx * dx + dy * dy);

    GeneralPath triangle = getArrowTriangle(line, len1, angle, false);
    Color color = graphics.getColor();
    graphics.setColor(Color.WHITE);
    graphics.fill(triangle);
    graphics.setColor(color);
    graphics.draw(triangle);

    double coef = (len2 - len1) / dist;
    List<Point2D> points = getArrowTrianglePoints(line, len1, angle);
    Point2D p1 = points.get(0);
    Point2D p2 = points.get(1);

    p1.setLocation(p1.getX() + coef * dx, p1.getY() + coef * dy);
    p2.setLocation(p2.getX() + coef * dx, p2.getY() + coef * dy);
    Line2D l = new Line2D.Double(p1, p2);
    graphics.draw(l);

  }

  /**
   * Draw arrow head (without filling and perpedicular closing) as an arrow end.
   *
   * @param line     line on which we draw arrow end
   * @param graphics graphics object on which we draw
   * @param len      length of the arrow part
   * @param angle    angle of the arrow head
   */
  private void drawOpenEnd(final Line2D line, final Graphics2D graphics, final double len, final double angle) {
    GeneralPath triangle = getArrowTriangle(line, len, angle, true);
    graphics.draw(triangle);
  }

  /**
   * Draw normal arrow head with a crossbar (short perpendicular line to the one
   * given as input) behind it .
   *
   * @param line     line on which we draw arrow end
   * @param graphics graphics object on which we draw
   * @param len      length of the arrow part
   * @param angle    angle of the arrow head
   */
  private void drawFullCrossbarEnd(final Line2D line, final Graphics2D graphics, final double len, final double angle) {
    double len1 = len;
    double len2 = len1 + CROSSBAR_ARROW_END;

    Point2D startPoint = line.getP1();
    Point2D endPoint = line.getP2();

    double dx = startPoint.getX() - endPoint.getX();
    double dy = startPoint.getY() - endPoint.getY();
    double dist = Math.sqrt(dx * dx + dy * dy);

    GeneralPath triangle = getArrowTriangle(line, len1, angle, false);
    graphics.fill(triangle);

    double coef = (len2 - len1) / dist;
    List<Point2D> points = getArrowTrianglePoints(line, len1, angle);
    Point2D p1 = points.get(0);
    Point2D p2 = points.get(1);

    p1.setLocation(p1.getX() + coef * dx, p1.getY() + coef * dy);
    p2.setLocation(p2.getX() + coef * dx, p2.getY() + coef * dy);
    Line2D l = new Line2D.Double(p1, p2);
    graphics.draw(l);
  }

  /**
   * Draw crossbar as arrow head (short perpendicular line to the one given as
   * input).
   *
   * @param line     line on which we draw arrow end
   * @param graphics graphics object on which we draw
   */
  private void drawCrossbarEnd(final Line2D line, final Graphics2D graphics) {
    double len = CROSSBAR_ARROW_END;
    double angle = Math.PI / 2;

    List<Point2D> points = getArrowTrianglePoints(line, len, angle);

    Line2D l = new Line2D.Double(points.get(0), points.get(1));
    graphics.draw(l);

  }

  /**
   * Draw diamond as an arrow head.
   *
   * @param line     line on which we draw arrow end
   * @param graphics graphics object on which we draw
   * @param length   length of the diamond part
   * @param angle    angle of the diamond head
   */
  private void drawDiamondEnd(final Line2D line, final Graphics2D graphics, final double length, final double angle) {
    double len = length / 2;

    Point2D startPoint = line.getP1();
    Point2D endPoint = line.getP2();

    double dx = startPoint.getX() - endPoint.getX();
    double dy = startPoint.getY() - endPoint.getY();
    double dist = Math.sqrt(dx * dx + dy * dy);
    double coef = len / dist;
    double arrowX = endPoint.getX() - coef * dx;
    double arrowY = endPoint.getY() - coef * dy;
    Point2D p1 = new Point2D.Double(arrowX, arrowY);
    p1 = pointTransformation.rotatePoint(p1, angle, endPoint);
    Point2D p2 = new Point2D.Double(arrowX, arrowY);
    p2 = pointTransformation.rotatePoint(p2, -angle, endPoint);
    arrowX = endPoint.getX() + 2 * coef * dx;
    arrowY = endPoint.getY() + 2 * coef * dy;
    Point2D p3 = new Point2D.Double(arrowX, arrowY);

    GeneralPath diamond = new GeneralPath();
    diamond.moveTo(endPoint.getX(), endPoint.getY());
    diamond.lineTo(p1.getX(), p1.getY());
    diamond.lineTo(p3.getX(), p3.getY());
    diamond.lineTo(p2.getX(), p2.getY());
    diamond.closePath();

    Color color = graphics.getColor();
    graphics.setColor(Color.WHITE);
    graphics.fill(diamond);
    graphics.setColor(color);
    graphics.draw(diamond);

  }

  /**
   * Draw standard filled arrow as an arrow head.
   *
   * @param line     line on which we draw arrow end
   * @param graphics graphics object on which we draw
   * @param len      length of the arrow part
   * @param angle    angle of the arrow head
   */
  private void drawFullArrowEnd(final Line2D line, final Graphics2D graphics, final double len, final double angle) {
    GeneralPath triangle = getArrowTriangle(line, len, angle, false);
    graphics.fill(triangle);
  }

  /**
   * Draw circle as an arrow head.
   *
   * @param line     line on which we draw circle end
   * @param graphics graphics object on which we draw
   * @param len      size (diameter) of the circle
   */
  private void drawCircleEnd(final Line2D line, final Graphics2D graphics, final double len) {
    double diameter = len;

    Point2D startPoint = line.getP1();
    Point2D endPoint = line.getP2();

    double dx = startPoint.getX() - endPoint.getX();
    double dy = startPoint.getY() - endPoint.getY();
    double dist = Math.sqrt(dx * dx + dy * dy);
    double coef = diameter / dist / 2;
    double x = endPoint.getX() + coef * dx;
    double y = endPoint.getY() + coef * dy;

    Ellipse2D circle = new Ellipse2D.Double(x - diameter / 2, y - diameter / 2, diameter, diameter);

    Color color = graphics.getColor();
    graphics.setColor(Color.WHITE);
    graphics.fill(circle);
    graphics.setColor(color);
    graphics.draw(circle);
  }

  /**
   * This method creates a General Path that describes triangle end of an arrow.
   * The parameters determine position of segment line (line), the length of arrow
   * end (len) and angle of an arrow. The open field determines if triangle should
   * be closed or not.
   *
   * @param line  line to which we want to add arrow end
   * @param len   length of the arrow end
   * @param angle angle of the arrow head
   * @param open  should the returned triangle be closed or not
   * @return GeneralPath object that describes arrow head of the arrow
   */
  private GeneralPath getArrowTriangle(final Line2D line, final double len, final double angle, final boolean open) {
    GeneralPath result = new GeneralPath();
    List<Point2D> points = getArrowTrianglePoints(line, len, angle);
    result.moveTo(points.get(0).getX(), points.get(0).getY());
    result.lineTo(line.getX2(), line.getY2());
    result.lineTo(points.get(1).getX(), points.get(1).getY());
    if (!open) {
      result.closePath();
    }
    return result;
  }

  /**
   * This method computes two points to be used as the end of an arrow. The
   * parameters determine position of segment line (line), the length of arrow end
   * (len) and angle of an arrow.
   *
   * @param line  line to which we want to find arrow head points
   * @param len   length of the arrow end
   * @param angle angle of the arrow head
   * @return list of two points that determines the points of an arrow
   */
  List<Point2D> getArrowTrianglePoints(final Line2D line, final double len, final double angle) {
    Point2D startPoint = line.getP1();
    Point2D endPoint = line.getP2();

    double dx = startPoint.getX() - endPoint.getX();
    double dy = startPoint.getY() - endPoint.getY();
    // compute the length of line segment
    double dist = Math.sqrt(dx * dx + dy * dy);
    double coef = len / dist;
    if (dist < Configuration.EPSILON) {
      logger.error("Cannot draw arrow segment with length equal to 0.0");
      coef = 0;
    }
    // compute the coordinates of a point on the line that will have to be
    // rotated
    double arrowX = endPoint.getX() - coef * dx;
    double arrowY = endPoint.getY() - coef * dy;
    // rotate first point
    Point2D p1 = new Point2D.Double(arrowX, arrowY);
    p1 = pointTransformation.rotatePoint(p1, angle, endPoint);
    // rotate and the second one
    Point2D p2 = new Point2D.Double(arrowX, arrowY);
    p2 = pointTransformation.rotatePoint(p2, -angle, endPoint);
    ArrayList<Point2D> result = new ArrayList<>();
    result.add(p1);
    result.add(p2);
    return result;

  }

  /**
   * This method draws a line ld on the graphics.
   *
   * @param ld       line to be drawn (containing information about arrow ends)
   * @param graphics where we want to draw the object
   */
  public void drawLine(final PolylineData ld, final Graphics2D graphics) {
    // get current color
    Color color = graphics.getColor();
    graphics.setColor(ld.getColor());

    // transform ld to a line
    GeneralPath path = ld.toGeneralPath();
    // get current stroke
    Stroke stroke = graphics.getStroke();
    // set stroke and draw line

    Stroke newStroke = ld.getType().getStroke(ld.getWidth());
    if (ld.getWidth() != 1.0 && newStroke instanceof BasicStroke) {
      BasicStroke basicStroke = (BasicStroke) newStroke;
      newStroke = new BasicStroke((float) ld.getWidth(), basicStroke.getEndCap(), basicStroke.getLineJoin(),
          basicStroke.getMiterLimit(), basicStroke.getDashArray(), basicStroke.getDashPhase());
    }
    graphics.setStroke(newStroke);
    graphics.draw(path);

    // and now try to draw an arrow end

    List<Line2D> lines = ld.getLines();

    ArrowTypeData arrowType = ld.getEndAtd();
    Point2D p1 = lines.get(lines.size() - 1).getP1();
    Point2D p2 = lines.get(lines.size() - 1).getP2();
    drawArrowEnd(graphics, arrowType, p1, p2);

    arrowType = ld.getBeginAtd();
    p1 = lines.get(0).getP2();
    p2 = lines.get(0).getP1();
    drawArrowEnd(graphics, arrowType, p1, p2);

    // restore default stroke type
    graphics.setStroke(stroke);
    graphics.setColor(color);

  }

  /**
   * Draws head of the arrow on the graphics2d element. The line is defined by two
   * points: startPoint, endPoint. Where endPoint defines the end with the arrow.
   *
   * @param graphics   where we ant to draw an arrow head
   * @param arrowType  what kind of arrow head we want to draw
   * @param startPoint first point of the line
   * @param endPoint   second point of the line (with the arrow head)
   */
  protected void drawArrowEnd(final Graphics2D graphics, final ArrowTypeData arrowType, final Point2D startPoint,
                              final Point2D endPoint) {
    graphics.setStroke(arrowType.getArrowLineType().getStroke(1.0));

    // get the last segment of polyline - this segment will determine the
    // position of arrow end
    Line2D line = new Line2D.Double(startPoint, endPoint);

    // basing on a type call different drawing method
    if (arrowType.getArrowType() == ArrowType.FULL) {
      drawFullArrowEnd(line, graphics, arrowType.getLen(), arrowType.getAngle());
    } else if (arrowType.getArrowType() == ArrowType.CIRCLE) {
      drawCircleEnd(line, graphics, arrowType.getLen());
    } else if (arrowType.getArrowType() == ArrowType.NONE) {
      return;
    } else if (arrowType.getArrowType() == ArrowType.BLANK) {
      drawBlankEnd(line, graphics, arrowType.getLen(), arrowType.getAngle());
    } else if (arrowType.getArrowType() == ArrowType.CROSSBAR) {
      drawCrossbarEnd(line, graphics);
    } else if (arrowType.getArrowType() == ArrowType.DIAMOND) {
      drawDiamondEnd(line, graphics, arrowType.getLen(), arrowType.getAngle());
    } else if (arrowType.getArrowType() == ArrowType.FULL_CROSSBAR) {
      drawFullCrossbarEnd(line, graphics, arrowType.getLen(), arrowType.getAngle());
    } else if (arrowType.getArrowType() == ArrowType.BLANK_CROSSBAR) {
      drawBlankCrossbarEnd(line, graphics, arrowType.getLen(), arrowType.getAngle());
    } else if (arrowType.getArrowType() == ArrowType.OPEN) {
      drawOpenEnd(line, graphics, arrowType.getLen(), arrowType.getAngle());
    } else {
      logger.debug("Unknown arrow type: {}", arrowType.getArrowType());
    }
  }

}
