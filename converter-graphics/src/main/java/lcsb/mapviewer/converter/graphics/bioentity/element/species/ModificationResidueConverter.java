package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.DrawableConverter;
import lcsb.mapviewer.converter.graphics.geometry.ArrowTransformation;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.AbstractRegionModification;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;
import lcsb.mapviewer.model.map.species.field.RegulatoryRegion;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Arrays;

public class ModificationResidueConverter extends DrawableConverter<ModificationResidue> {

  /**
   * Length of the arrow for transcription site visualization.
   */
  public static final int TRANSCRIPTION_SITE_ARROW_LENGTH = 8;

  /**
   * Angle of the arrow for transcription site visualization.
   */
  public static final double TRANSCRIPTION_SITE_ARROW_ANGLE = 3 * Math.PI / 4;

  private ArrowTransformation arrowTransformation = new ArrowTransformation();

  /**
   * This method draws modification. If params.sbgn is set to false then
   * modification is not drawn if empty.
   *
   * @param mr       modification to be drawn
   * @param graphics where the modification should be drawn
   * @param params   parameters with information if we should use sbgn notation
   */
  public void drawModification(final ModificationResidue mr, final Graphics2D graphics,
                               final ConverterParams params) {
    if (!isVisible(mr.getSpecies(), params)) {
      return;
    }

    if (mr instanceof Residue) {
      drawResidue((Residue) mr, graphics, params.isSbgnFormat());
    } else if (mr instanceof ModificationSite) {
      drawModificationSite((ModificationSite) mr, graphics, params.isSbgnFormat());
    } else if (mr instanceof BindingRegion) {
      drawBindingRegion((BindingRegion) mr, graphics);
    } else if (mr instanceof ProteinBindingDomain) {
      drawProteinBindingDomain((ProteinBindingDomain) mr, graphics);
    } else if (mr instanceof CodingRegion) {
      drawCodingRegion((CodingRegion) mr, graphics);
    } else if (mr instanceof RegulatoryRegion) {
      drawRegulatoryRegion((RegulatoryRegion) mr, graphics);
    } else if (mr instanceof TranscriptionSite) {
      drawTranscriptionSite((TranscriptionSite) mr, graphics);
    } else if (mr instanceof StructuralState) {
      drawStructuralState((StructuralState) mr, graphics);
    } else {
      throw new InvalidArgumentException("Unknown class type: " + mr.getClass());
    }
  }

  protected void drawResidue(final Residue residue, final Graphics2D graphics, final boolean drawEmptyModification) {
    if (!drawEmptyModification && residue.getState() == null) {
      return;
    }
    Ellipse2D ellipse = new Ellipse2D.Double(residue.getX(), residue.getY(), residue.getWidth(), residue.getHeight());
    Color c = graphics.getColor();
    graphics.setColor(Color.WHITE);
    graphics.fill(ellipse);
    graphics.setColor(residue.getBorderColor());
    graphics.draw(ellipse);

    ModificationState state = residue.getState();
    if (state != null) {
      String str = state.getAbbreviation();
      drawModificationResidueText(residue, graphics, str);
    }
    graphics.setColor(c);
  }

  private void drawModificationResidueText(final ModificationResidue residue, final Graphics2D graphics, final String str) {
    Color tmpColor = graphics.getColor();
    graphics.setColor(residue.getBorderColor());
    Rectangle2D border = new Rectangle2D.Double(residue.getNameX(), residue.getNameY(), residue.getNameWidth(), residue.getNameHeight());
    drawText(border, str, graphics, residue.getNameHorizontalAlign(), residue.getNameVerticalAlign());
    graphics.setColor(tmpColor);
  }

  /**
   * This method draws {@link ModificationSite} of the {@link Species}. If
   * drawEmptyModification is set to false then modification is not drawn if
   * empty.
   *
   * @param modificationSite      modification to be drawn
   * @param graphics              - where the modification should be drawn
   * @param drawEmptyModification flag that indicates if we should draw empty modification
   */
  protected void drawModificationSite(final ModificationSite modificationSite, final Graphics2D graphics,
                                      final boolean drawEmptyModification) {
    if (!drawEmptyModification && modificationSite.getState() == null) {
      return;
    }
    Ellipse2D ellipse = new Ellipse2D.Double(modificationSite.getX(), modificationSite.getY(),
        modificationSite.getWidth(), modificationSite.getWidth());
    Color c = graphics.getColor();
    graphics.setColor(Color.WHITE);
    graphics.fill(ellipse);
    graphics.setColor(modificationSite.getBorderColor());
    graphics.draw(ellipse);
    graphics.drawLine(
        (int) (modificationSite.getX() + modificationSite.getWidth() / 2),
        (int) (modificationSite.getY() + modificationSite.getWidth()),
        (int) (modificationSite.getX() + modificationSite.getWidth() / 2),
        (int) (modificationSite.getY() + modificationSite.getHeight()));

    ModificationState state = modificationSite.getState();
    if (state != null) {
      String str = state.getAbbreviation();
      drawModificationResidueText(modificationSite, graphics, str);
    }
    graphics.setColor(c);
  }

  protected void drawBindingRegion(final BindingRegion bindingRegion, final Graphics2D graphics) {
    drawRegionModification(bindingRegion, graphics);
  }

  protected void drawProteinBindingDomain(final ProteinBindingDomain proteinBindingDomain, final Graphics2D graphics) {
    drawRegionModification(proteinBindingDomain, graphics);
  }

  protected void drawCodingRegion(final CodingRegion codingRegion, final Graphics2D graphics) {
    drawRegionModification(codingRegion, graphics);
  }

  private void drawRegionModification(final AbstractRegionModification modification, final Graphics2D graphics) {
    double width = modification.getWidth();
    double height = modification.getHeight();
    Rectangle2D rectangle = new Rectangle2D.Double(modification.getX(), modification.getY(), width, height);
    Color c = graphics.getColor();
    graphics.setColor(Color.WHITE);
    graphics.fill(rectangle);
    graphics.setColor(modification.getBorderColor());
    graphics.draw(rectangle);

    String str = modification.getName();
    if (str != null && !str.trim().isEmpty()) {
      drawModificationResidueText(modification, graphics, str);
    }
    graphics.setColor(c);

  }

  protected void drawRegulatoryRegion(final RegulatoryRegion regulatoryRegion, final Graphics2D graphics) {
    drawRegionModification(regulatoryRegion, graphics);
  }

  protected void drawTranscriptionSite(final TranscriptionSite transcriptionSite, final Graphics2D graphics) {
    double y = transcriptionSite.getY() + transcriptionSite.getHeight();
    double x = transcriptionSite.getX();
    if (transcriptionSite.getDirection().equals("RIGHT")) {
      x += transcriptionSite.getWidth();
    }
    Point2D p1 = new Point2D.Double(x, y);

    y -= transcriptionSite.getHeight();
    Point2D p2 = new Point2D.Double(x, y);

    if (transcriptionSite.getDirection().equals("RIGHT")) {
      x -= transcriptionSite.getWidth();
    } else {
      x += transcriptionSite.getWidth();
    }
    Point2D p3 = new Point2D.Double(x, y);

    PolylineData line = new PolylineData(Arrays.asList(p1, p2, p3));
    line.setColor(transcriptionSite.getBorderColor());
    line.getEndAtd().setArrowType(ArrowType.FULL);
    line.getEndAtd().setLen(TRANSCRIPTION_SITE_ARROW_LENGTH);
    line.getEndAtd().setAngle(TRANSCRIPTION_SITE_ARROW_ANGLE);

    arrowTransformation.drawLine(line, graphics);

    if (transcriptionSite.getName() != null && !transcriptionSite.getName().isEmpty()) {
      drawModificationResidueText(transcriptionSite, graphics, transcriptionSite.getName());
    }
  }

  protected void setArrowTransformation(final ArrowTransformation arrowTransformation) {
    this.arrowTransformation = arrowTransformation;
  }

  protected void drawStructuralState(final StructuralState state, final Graphics2D graphics) {
    Shape ellipse = getStructuralStateShape(state);
    Color c = graphics.getColor();
    graphics.setColor(Color.WHITE);
    graphics.fill(ellipse);
    graphics.setColor(c);
    graphics.draw(ellipse);
    if (!state.getName().isEmpty()) {
      Font tmpFont = graphics.getFont();
      Font font = new Font(Font.SANS_SERIF, Font.PLAIN, state.getFontSize().intValue());
      graphics.setFont(font);

      drawModificationResidueText(state, graphics, state.getName());
      graphics.setFont(tmpFont);
    }

  }

  Shape getStructuralStateShape(final StructuralState state) {
    return new Ellipse2D.Double(
        state.getX(),
        state.getY(),
        state.getWidth(),
        state.getHeight());
  }

}
