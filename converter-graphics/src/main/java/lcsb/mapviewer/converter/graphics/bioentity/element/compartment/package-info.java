/**
 * Provides classes that draws different implemention of {@link Compartment} on
 * the {@link Graphics2D}.
 */
package lcsb.mapviewer.converter.graphics.bioentity.element.compartment;
