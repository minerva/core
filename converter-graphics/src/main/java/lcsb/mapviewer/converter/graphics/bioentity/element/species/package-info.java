/**
 * Provides classes that draws
 * {@link lcsb.mapviewer.db.model.map.species.Species Species} on the Graphics2D
 * object.
 */
package lcsb.mapviewer.converter.graphics.bioentity.element.species;
