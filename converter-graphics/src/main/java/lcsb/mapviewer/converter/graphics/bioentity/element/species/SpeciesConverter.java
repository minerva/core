package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.geometry.LineTransformation;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.bioentity.element.ElementConverter;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

/**
 * This class defines basics used for drawing {@link Species} (node in the graph
 * representation) on the {@link Graphics2D} object.
 *
 * @param <T> type of {@link Species} class that can be drawn with this converter
 * @author Piotr Gawron
 */
public abstract class SpeciesConverter<T extends Species> extends ElementConverter<T> {

  /**
   * What is the distance between homodimer aliases when homodimer>1.
   */
  public static final int HOMODIMER_OFFSET = 6;

  /**
   * How far from the original shape should the activity border be drawn.
   */
  protected static final int ACTIVITY_BORDER_DISTANCE = 5;
  /**
   * Default species font size.
   */
  protected static final int DEFAULT_SPECIES_FONT_SIZE = 12;

  /**
   * Height of the rectangle that contains unit of information.
   */
  private static final int UNIT_OF_INFORMATION_HEIGHT = 20;

  /**
   * Size of the margin in the unit of information description.
   */
  private static final int TEXT_MARGIN_FOR_UNIT_OF_INFORMATION_DESC = 20;

  /**
   * Minimum width of the unit of information rectangle.
   */
  private static final int MIN_UNIT_OF_INFORMATION_WIDTH = 40;

  /**
   * Graphical helper object with line transformation functions.
   */
  private LineTransformation lineTransformation = new LineTransformation();

  /**
   * Default font used to draw unit of information of the species.
   */
  private Font unitOfInformationFont = null;

  /**
   * Default constructor.
   *
   * @param colorExtractor Object that helps to convert {@link DataOverlayEntry} values into
   *                       colors when drawing {@link Species}
   */
  protected SpeciesConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  /**
   * Returns default shape of the {@link Species}.
   *
   * @param species {@link Species} for which we are looking for a border
   * @return {@link Shape} object defining given {@link Species}
   */
  protected Shape getDefaultAliasShape(final Species species) {
    Shape shape;
    shape = new Rectangle(species.getX().intValue(), species.getY().intValue(), species.getWidth().intValue(),
        species.getHeight().intValue());
    return shape;
  }

  /**
   * Returns font that should be used for drawing description of the
   * {@link Species}.
   *
   * @param species {@link Species} for which we are looking for a font
   * @param params  specific drawing parameters (like scale)
   * @return {@link Font} that should be used for drawing {@link Species} description
   */
  protected Font getFont(final Species species, final ConverterParams params) {
    double fontSize = species.getFontSize();
    return new Font(Font.SANS_SERIF, Font.PLAIN, (int) (fontSize * params.getScale()));
  }

  /**
   * Returns text describing {@link Species}.
   *
   * @param species object under investigation
   * @return description of the {@link Species}
   */
  protected String getText(final T species) {
    String name = species.getName();
    if (name.isEmpty()) {
      name = " ";
    }

    return name;
  }

  @Override
  public void drawText(final T species, final Graphics2D graphics, final ConverterParams params) {
    String text = getText(species);
    Font oldFont = graphics.getFont();
    Color oldColor = graphics.getColor();
    Font font = getFont(species, params);
    graphics.setColor(species.getFontColor());
    graphics.setFont(font);

    Rectangle2D point = species.getNameBorder();
    drawText(point, text, graphics, species.getNameHorizontalAlign(), species.getNameVerticalAlign());
    graphics.setFont(oldFont);
    graphics.setColor(oldColor);
  }

  /**
   * Returns line style used for drawing alias border.
   *
   * @param species {@link Species} to be drawn
   * @return style of the line used to draw {@link Species}
   */
  protected Stroke getBorderLine(final Species species) {
    return species.getBorderLineType().getStroke(species.getLineWidth().floatValue());
  }

  /**
   * Returns border of the {@link Species} as {@link PathIterator}.
   *
   * @param species {@link Species} for which we are looking for a border
   * @return {@link PathIterator} object defining given {@link Species}
   */
  protected abstract PathIterator getBoundPathIterator(final T species);

  /**
   * @return the lineTransformation
   */
  protected LineTransformation getLineTransformation() {
    return lineTransformation;
  }

  /**
   * @param lineTransformation the lineTransformation to set
   */
  protected void setLineTransformation(final LineTransformation lineTransformation) {
    this.lineTransformation = lineTransformation;
  }

  /**
   * @return the unitOfInformationFont
   */
  protected Font getUnitOfInformationFont() {
    return unitOfInformationFont;
  }

  /**
   * @param unitOfInformationFont the unitOfInformationFont to set
   */
  protected void setUnitOfInformationFont(final Font unitOfInformationFont) {
    this.unitOfInformationFont = unitOfInformationFont;
  }

  /**
   * Draws unit of information for the {@link Species} (rectangle in the top part
   * of the alias).
   *
   * @param units    unit of information text
   * @param species  unit of information should be drawn on this {@link Species}
   * @param graphics where the drawing should be performed
   */
  protected void drawUnitOfInformation(final List<String> units, final T species, final Graphics2D graphics) {
    Color oldColor = graphics.getColor();
    List<String> unitsToDraw = new ArrayList<>();
    for (final String text : units) {
      if (text != null && !text.trim().isEmpty()) {
        unitsToDraw.add(text);
      }
    }
    double distancePerUnit = species.getWidth() / (unitsToDraw.size() + 1);
    int count = 1;
    for (final String text : unitsToDraw) {
      Point2D p = new Point2D.Double(species.getX() + distancePerUnit * count, species.getY());

      double width = MIN_UNIT_OF_INFORMATION_WIDTH;
      width = Math.max(MIN_UNIT_OF_INFORMATION_WIDTH,
          graphics.getFontMetrics().stringWidth(text) + TEXT_MARGIN_FOR_UNIT_OF_INFORMATION_DESC);
      width = Math.min(width, species.getWidth());
      double height = UNIT_OF_INFORMATION_HEIGHT;

      Rectangle2D rectangle = new Rectangle2D.Double(p.getX() - width / 2, p.getY() - height / 2, width, height);
      graphics.setColor(Color.WHITE);
      graphics.fill(rectangle);
      graphics.setColor(species.getBorderColor());
      graphics.draw(rectangle);
      graphics.setColor(species.getFontColor());
      Font font = graphics.getFont();
      graphics.setFont(getUnitOfInformationFont());

      width = graphics.getFontMetrics().stringWidth(text);
      height = graphics.getFontMetrics().getAscent() - graphics.getFontMetrics().getDescent();

      graphics.drawString(text, (int) (p.getX() - width / 2), (int) (p.getY() + height / 2));
      graphics.setFont(font);

      count++;
    }
    graphics.setColor(oldColor);
  }

  protected abstract Shape getShape(final T species);

}
