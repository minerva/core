package lcsb.mapviewer.converter.graphics.bioentity.reaction;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.common.geometry.LineTransformation;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.DrawingException;
import lcsb.mapviewer.converter.graphics.bioentity.BioEntityConverter;
import lcsb.mapviewer.converter.graphics.geometry.ArrowTransformation;
import lcsb.mapviewer.model.graphics.ArrowTypeData;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.OrOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.ReactionRect;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.overlay.GenericDataOverlayEntry;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class allows to draw reaction on the graphics2D.
 *
 * @author Piotr Gawron
 */
public class ReactionConverter extends BioEntityConverter<Reaction> {

  /**
   * Size of the rectangle drawn on the central line of the reaction.
   */
  public static final double RECT_SIZE = 10;
  /**
   * Default font size of reaction description.
   */
  public static final int DESCRIPTION_FONT_SIZE = 10;

  /**
   * {@link DataOverlayEntry} used for coloring reactions where we have more than
   * one layout.
   */
  public static final List<DataOverlayEntry> DEFAULT_COLOR_SCHEMA = Collections.singletonList(new GenericDataOverlayEntry());
  /**
   * When drawing operator this value defines radius of the joining operator
   * circle.
   */
  private static final int DEFAULT_OPERATOR_RADIUS = 6;

  private static final double SBGN_OPERATOR_RADIUS = 10.0;
  private static final double SBGN_OPERATOR_FONT_SIZE = 8.0;

  /**
   * Line width of #DEFAULT_COLOR_SCHEMA used for coloring reactions where we have
   * more than one layout.
   */
  private static final double DEFAULT_COLOR_SCHEMA_LINE_WIDTH = 3.0;

  static {
    DEFAULT_COLOR_SCHEMA.get(0).setColor(Color.BLACK);
    DEFAULT_COLOR_SCHEMA.get(0).setLineWidth(DEFAULT_COLOR_SCHEMA_LINE_WIDTH);
  }

  /**
   * Font used for drawing reaction description on the map.
   */
  private Font descFont = null;

  /**
   * Graphical helper object with line transformation functions.
   */
  private LineTransformation lineTransformation = new LineTransformation();

  /**
   * This objects helps drawing arrows.
   */
  private ArrowTransformation arrowTransformation = new ArrowTransformation();

  /**
   * Object used to perform transformations on point objects.
   */
  private PointTransformation pointTransformation = new PointTransformation();

  /**
   * Object that helps to convert {@link DataOverlayEntry} values into colors.
   */
  private final ColorExtractor colorExtractor;

  /**
   * Default constructor.
   *
   * @param colorExtractor Object that helps to convert {@link DataOverlayEntry} values into
   *                       colors when drawing {@link Reaction}
   */
  public ReactionConverter(final ColorExtractor colorExtractor) {
    super();
    descFont = new Font(Font.SANS_SERIF, Font.BOLD, DESCRIPTION_FONT_SIZE);
    this.colorExtractor = colorExtractor;
  }

  /**
   * This method draws a rectangle in the center of reaction (defined by line).
   *
   * @param line       line on which the rectangle is drawn
   * @param insideType type of the rectangle to be drawn
   * @param graphics   where the rectangle should be drawn
   */
  protected void drawRectangleData(final PolylineData line, final Point2D processCoordinates, final ReactionRect insideType,
                                   final Graphics2D graphics) {
    Color oldColor = graphics.getColor();
    Font oldFont = graphics.getFont();

    graphics.setColor(line.getColor());

    Point2D startPoint = line.getLines().get((line.getLines().size() - 1) / 2).getP1();
    Point2D endPoint = line.getLines().get((line.getLines().size() - 1) / 2).getP2();
    if (startPoint.distance(endPoint) > RECT_SIZE) {
      Point2D rectStartPoint = pointTransformation.getPointOnLine(startPoint, endPoint,
          0.5 - RECT_SIZE / (2 * startPoint.distance(endPoint)));
      Point2D rectEndPoint = pointTransformation.getPointOnLine(startPoint, endPoint,
          0.5 + RECT_SIZE / (2 * startPoint.distance(endPoint)));
      PolylineData preRectangleLine = new PolylineData(line);
      preRectangleLine.removeLines();
      preRectangleLine.addLine(startPoint, rectStartPoint);

      PolylineData postRectangleLine = new PolylineData(line);
      postRectangleLine.removeLines();
      postRectangleLine.addLine(rectEndPoint, endPoint);

      arrowTransformation.drawLine(preRectangleLine, graphics);
      arrowTransformation.drawLine(postRectangleLine, graphics);

      startPoint = rectStartPoint;
      endPoint = rectEndPoint;
    }
    if (line.getLines().size() > 1) {
      PolylineData postRectangleLine = new PolylineData(line);
      for (int i = 0; i < (line.getLines().size() + 1) / 2; i++) {
        line.removeLine(0);
      }
      arrowTransformation.drawLine(postRectangleLine, graphics);
    }
    if (line.getLines().size() > 2) {
      PolylineData preRectangleLine = new PolylineData(line);
      preRectangleLine.removeLines();
      for (int i = 0; i < (line.getLines().size() + 1) / 2; i++) {
        preRectangleLine.addLine(line.getLines().get(i));
      }
      arrowTransformation.drawLine(preRectangleLine, graphics);
    }

    double pointX = (startPoint.getX() + endPoint.getX()) / 2;
    double pointY = (startPoint.getY() + endPoint.getY()) / 2;
    double dx = endPoint.getX() - startPoint.getX();
    double dy = endPoint.getY() - startPoint.getY();
    double angle = Math.atan2(dy, dx);

    if (processCoordinates != null) {
      angle = 0;
      pointX = processCoordinates.getX();
      pointY = processCoordinates.getY();
      graphics.draw(new Line2D.Double(startPoint, endPoint));
    }
    Point2D centerPoint = new Point2D.Double(pointX, pointY);

    if (insideType == null) {
      Stroke oldStroke = graphics.getStroke();

      Stroke newStroke = line.getType().getStroke(line.getWidth());
      graphics.setStroke(newStroke);

      Point2D topPoint = new Point2D.Double(pointX + RECT_SIZE / 2, pointY);
      topPoint = new PointTransformation().rotatePoint(topPoint, angle, centerPoint);

      Point2D bottomPoint = new Point2D.Double(pointX - RECT_SIZE / 2, pointY);
      bottomPoint = new PointTransformation().rotatePoint(bottomPoint, angle, centerPoint);
      graphics.draw(new Line2D.Double(topPoint, bottomPoint));

      graphics.setStroke(oldStroke);
    } else {

      // find rectangle
      Rectangle2D rect = new Rectangle2D.Double();
      rect.setRect(pointX - RECT_SIZE / 2, pointY - RECT_SIZE / 2, RECT_SIZE, RECT_SIZE);

      // rotate graphics by the angle defined by line (instead of rotating
      // rectangle)
      graphics.rotate(angle, pointX, pointY);
      // fill rectangle
      graphics.setColor(Color.white);
      graphics.fill(rect);
      graphics.setColor(line.getColor());
      // draw rectangle border
      graphics.draw(rect);

      // un-rotate the graphics
      graphics.rotate(-angle, pointX, pointY);

      // draw text inside rectangle
      Font tmpFont = graphics.getFont();
      graphics.setFont(descFont);
      String insideDesc = insideType.getText();
      double textWidth = graphics.getFontMetrics().stringWidth(insideDesc);
      double textHeight = graphics.getFontMetrics().getAscent() - 2;
      graphics.drawString(insideDesc, (int) (pointX - textWidth / 2), (int) (pointY + textHeight / 2));
      graphics.setFont(tmpFont);

      // if we should put bolt character inside then do it
      if (insideType == ReactionRect.RECT_BOLT) {
        GeneralPath path = new GeneralPath();
        path.moveTo(pointX + 2, pointY - RECT_SIZE / 2 + 1);
        path.lineTo(pointX + 2, pointY + RECT_SIZE / 2 - 3);
        path.lineTo(pointX - 2, pointY - RECT_SIZE / 2 + 3);
        path.lineTo(pointX - 2, pointY + RECT_SIZE / 2 - 1);
        graphics.draw(path);
      }

    }
    graphics.setColor(oldColor);
    graphics.setFont(oldFont);
  }

  @Override
  protected void drawImpl(final Reaction reaction, final Graphics2D graphics, final ConverterParams params) {
    Color color = graphics.getColor();
    // first reactants
    for (final Reactant reactant : reaction.getReactants()) {
      if (isVisible(reactant, params)) {
        drawReactant(graphics, reactant);
      }
    }
    // now products
    for (final Product product : reaction.getProducts()) {
      if (isVisible(product, params)) {
        drawProduct(graphics, product);
      }
    }
    // draw modifiers
    for (final Modifier modifier : reaction.getModifiers()) {
      if (isVisible(modifier, params)) {
        drawModifier(graphics, modifier);
      }
    }

    // and operators
    for (final NodeOperator operator : reaction.getOperators()) {
      if (isVisible(operator, params)) {
        drawOperator(graphics, operator, params.isSbgnFormat());
      }
    }

    // in the end we draw rectangle in the middle
    drawRectangleData(reaction.getLine(), reaction.getProcessCoordinates(), reaction.getReactionRect(), graphics);
    graphics.setColor(color);
  }

  @Override
  public void draw(final Reaction reaction, final Graphics2D graphics, final ConverterParams params,
                   final List<List<DataOverlayEntry>> visualizedDataOverlaysColorSchemas) throws DrawingException {
    if (visualizedDataOverlaysColorSchemas.isEmpty()) {
      drawImpl(reaction, graphics, params);
    } else if (visualizedDataOverlaysColorSchemas.size() == 1) {
      if (visualizedDataOverlaysColorSchemas.get(0) == null) {
        drawImpl(reaction, graphics, params);
      } else {
        List<Pair<AbstractNode, PolylineData>> oldData = new ArrayList<>();
        for (final AbstractNode node : reaction.getNodes()) {
          oldData.add(new Pair<>(node, node.getLine()));
        }
        PolylineData oldReactionData = reaction.getLine();
        applyColorSchema(reaction, visualizedDataOverlaysColorSchemas.get(0));
        drawImpl(reaction, graphics, params);
        for (Pair<AbstractNode, PolylineData> pair : oldData) {
          pair.getLeft().setLine(pair.getRight());
        }
        reaction.setLine(oldReactionData);
      }
    } else {
      int count = 0;
      for (final List<DataOverlayEntry> schema : visualizedDataOverlaysColorSchemas) {
        if (!schema.isEmpty()) {
          count++;
        }
      }
      if (count == 0) {
        drawImpl(reaction, graphics, params);
      } else {
        List<Pair<AbstractNode, PolylineData>> oldData = new ArrayList<>();
        for (final AbstractNode node : reaction.getNodes()) {
          oldData.add(new Pair<>(node, node.getLine()));
        }
        PolylineData oldReactionData = reaction.getLine();
        applyColorSchema(reaction, DEFAULT_COLOR_SCHEMA);
        drawImpl(reaction, graphics, params);
        for (Pair<AbstractNode, PolylineData> pair : oldData) {
          pair.getLeft().setLine(pair.getRight());
        }
        reaction.setLine(oldReactionData);
      }
    }
  }

  @Override
  public void drawText(final Reaction bioEntity, final Graphics2D graphics, final ConverterParams params) throws DrawingException {
  }

  /**
   * Modify reaction with data from {@link DataOverlayEntry}.
   *
   * @param reaction     reaction to modify
   * @param colorSchemas {@link DataOverlayEntry} to modify reaction
   */
  private void applyColorSchema(final Reaction reaction, final List<DataOverlayEntry> colorSchemas) {
    PolylineData pd = new PolylineData(reaction.getLine());
    if (colorSchemas.size() == 1) {
      DataOverlayEntry colorSchema = colorSchemas.get(0);
      pd.setColor(colorExtractor.getNormalizedColor(colorSchema));
      if (colorSchema.getLineWidth() != null) {
        pd.setWidth(colorSchema.getLineWidth());
      }
      reaction.setLine(pd);

      for (final AbstractNode node : reaction.getNodes()) {
        pd = new PolylineData(node.getLine());
        pd.setColor(colorExtractor.getNormalizedColor(colorSchema));
        if (colorSchema.getLineWidth() != null) {
          pd.setWidth(colorSchema.getLineWidth());
        }
        if (colorSchema.getReverseReaction() != null && colorSchema.getReverseReaction()) {
          ArrowTypeData atd = pd.getBeginAtd();
          pd.setBeginAtd(pd.getEndAtd());
          pd.setEndAtd(atd);
        }
        node.setLine(pd);
      }
    } else if (colorSchemas.size() > 1) {
      throw new NotImplementedException("Drawing reaction with more hits than one is not implemented");
    }

  }

  /**
   * Draw modifier on the graphics2d.
   *
   * @param graphics where we want to draw the object
   * @param modifier object to be drawn
   */
  private void drawModifier(final Graphics2D graphics, final Modifier modifier) {
    // modifier consists only from the arrow
    arrowTransformation.drawLine(modifier.getLine(), graphics);
  }

  /**
   * Draw operator on the graphics2d.
   *
   * @param graphics   where we want to draw the object
   * @param operator   object to be drawn
   * @param sbgnFormat true if operator is to be drawn in SBGN format
   */
  void drawOperator(final Graphics2D graphics, final NodeOperator operator, final boolean sbgnFormat) {
    Font oldFont = graphics.getFont();
    Color oldColor = graphics.getColor();

    // draw line
    arrowTransformation.drawLine(operator.getLine(), graphics);

    Point2D centerPoint = operator.getLine().getStartPoint();
    Color fillColor = Color.WHITE;
    double radius = DEFAULT_OPERATOR_RADIUS;
    String text = operator.getOperatorText();

    if (operator instanceof AndOperator) {
      radius = 1.5;
      fillColor = operator.getLine().getColor();
    }
    // bigger connecting point circles in SBGN view
    if (sbgnFormat && (operator instanceof AndOperator || operator instanceof OrOperator)) {
      fillColor = Color.WHITE;
      radius = SBGN_OPERATOR_RADIUS;
      graphics.setFont(getSbgnOperatorFontSize());
      if (operator instanceof AndOperator) {
        text = "AND";
      }
      if (operator instanceof OrOperator) {
        text = "OR";
      }
    }
    // it's a circle
    Ellipse2D cir = new Ellipse2D.Double(centerPoint.getX() - radius, centerPoint.getY() - radius, 2 * radius,
        2 * radius);
    graphics.setColor(fillColor);
    graphics.fill(cir);
    graphics.setColor(operator.getLine().getColor());
    graphics.draw(cir);
    // and text defined by operator
    Rectangle2D rect = graphics.getFontMetrics().getStringBounds(text, graphics);
    graphics.drawString(text, (int) (centerPoint.getX() - rect.getWidth() / 2 + 1),
        (int) (centerPoint.getY() + rect.getHeight() / 2) - 2);
    graphics.setColor(oldColor);
    graphics.setFont(oldFont);
  }

  /**
   * Draw product on the graphics2D.
   *
   * @param graphics where we want to draw the object
   * @param product  object to be drawn
   */
  private void drawProduct(final Graphics2D graphics, final Product product) {
    arrowTransformation.drawLine(product.getLine(), graphics);
  }

  /**
   * Draw reactant on the graphics2D.
   *
   * @param graphics where the reactant should be drawn
   * @param reactant object to be drawn on the graphics
   */
  private void drawReactant(final Graphics2D graphics, final Reactant reactant) {
    arrowTransformation.drawLine(reactant.getLine(), graphics);
  }

  /**
   * @return {@link #descFont}
   */
  protected Font getDescFont() {
    return descFont;
  }

  /**
   * @param descFont new {@link #descFont} value
   */
  protected void setDescFont(final Font descFont) {
    this.descFont = descFont;
  }

  /**
   * @return {@link #lineTransformation}
   */
  protected LineTransformation getLineTransformation() {
    return lineTransformation;
  }

  /**
   * @param lineTransformation new {@link #lineTransformation}
   */
  protected void setLineTransformation(final LineTransformation lineTransformation) {
    this.lineTransformation = lineTransformation;
  }

  /**
   * @return {@link #arrowTransformation}
   */
  protected ArrowTransformation getArrowTransformation() {
    return arrowTransformation;
  }

  /**
   * @param arrowTransformation new {@link #arrowTransformation}
   */
  protected void setArrowTransformation(final ArrowTransformation arrowTransformation) {
    this.arrowTransformation = arrowTransformation;
  }

  /**
   * @return {@link #pointTransformation}
   */
  protected PointTransformation getPointTransformation() {
    return pointTransformation;
  }

  /**
   * @param pointTransformation new {@link #pointTransformation}
   */
  protected void setPointTransformation(final PointTransformation pointTransformation) {
    this.pointTransformation = pointTransformation;
  }

  private Font getSbgnOperatorFontSize() {
    return new Font(Font.SANS_SERIF, Font.PLAIN, (int) SBGN_OPERATOR_FONT_SIZE);
  }

}