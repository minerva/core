package lcsb.mapviewer.converter.graphics.bioentity.element.compartment;

import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.model.map.compartment.LeftSquareCompartment;

/**
 * Class responsible for drawing LeftSquareCompartment on the Graphics2D.
 * 
 * @author Piotr Gawron
 * 
 */
public class LeftSquareCompartmentConverter extends CompartmentConverter<LeftSquareCompartment> {

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into
   *          colors when drawing elements
   * 
   */
  public LeftSquareCompartmentConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected Shape getOuterShape(final LeftSquareCompartment compartment) {
    return new Line2D.Double(compartment.getWidth(), compartment.getHeight(), compartment.getWidth(), 0);
  }

  @Override
  protected Shape getInnerShape(final LeftSquareCompartment compartment) {
    return new Line2D.Double(
        compartment.getWidth() - compartment.getThickness(), 0, compartment.getWidth() - compartment.getThickness(),
        compartment.getHeight());
  }

  @Override
  protected Shape getBorderShape(final LeftSquareCompartment compartment) {
    return new Area(new Rectangle2D.Double(0.0, 0.0, compartment.getWidth(), compartment.getHeight()));
  }

}
