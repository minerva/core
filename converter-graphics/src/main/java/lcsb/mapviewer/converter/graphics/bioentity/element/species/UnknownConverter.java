package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Ellipse2D;
import java.awt.geom.PathIterator;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.Unknown;

/**
 * This class defines methods used for drawing {@link Unknown} on the
 * {@link Graphics2D} object.
 * 
 * @author Piotr Gawron
 * 
 */
public class UnknownConverter extends SpeciesConverter<Unknown> {

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into
   *          colors when drawing {@link Species}
   */
  public UnknownConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected void drawImpl(final Unknown unknown, final Graphics2D graphics, final ConverterParams params) {
    if (unknown.getActivity()) {
      int border = ACTIVITY_BORDER_DISTANCE;
      unknown.increaseBorder(border);
      Shape shape2 = getShape(unknown);
      Stroke stroke = graphics.getStroke();
      graphics.setStroke(LineType.DOTTED.getStroke(unknown.getLineWidth()));
      graphics.draw(shape2);
      graphics.setStroke(stroke);
      unknown.increaseBorder(-border);
    }

    Shape shape = getShape(unknown);
    Color c = graphics.getColor();
    graphics.setColor(unknown.getFillColor());
    graphics.fill(shape);
    graphics.setColor(c);
    drawText(unknown, graphics, params);
  }

  @Override
  protected Shape getShape(final Unknown unknown) {
    Shape shape = new Ellipse2D.Double(unknown.getX(), unknown.getY(), unknown.getWidth(), unknown.getHeight());
    return shape;
  }

  @Override
  public PathIterator getBoundPathIterator(final Unknown unknown) {
    throw new InvalidStateException("This class doesn't provide boundPath");
  }

}
