package lcsb.mapviewer.converter.graphics.bioentity.element.compartment;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Class responsible for drawing SquareCompartment on the {@link Graphics2D}.
 * 
 * @author Piotr Gawron
 * 
 */
public class SquareCompartmentConverter extends CompartmentConverter<SquareCompartment> {
  /**
   * How big is the arc in the corner of rectangle that represents square
   * compartment.
   */
  private static final int RECTANGLE_CORNER_ARC_SIZE = 20;

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into
   *          colors when drawing {@link Species}
   */
  public SquareCompartmentConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  /**
   * Returns shape representing compartment.
   * 
   * @param compartment
   *          compartment for which we are looking for a {@link Shape}
   * @return {@link Shape} object that represents compartment
   */
  private Shape getShape(final Compartment compartment) {
    return new RoundRectangle2D.Double(
        compartment.getX(), compartment.getY(), compartment.getWidth(), compartment.getHeight(),
        RECTANGLE_CORNER_ARC_SIZE, RECTANGLE_CORNER_ARC_SIZE);
  }

  @Override
  protected Shape getOuterShape(final SquareCompartment compartment) {
    return getShape(compartment);
  }

  @Override
  protected Shape getInnerShape(final SquareCompartment compartment) {
    compartment.increaseBorder(-compartment.getThickness());
    Shape result = getShape(compartment);
    compartment.increaseBorder(compartment.getThickness());
    return result;
  }
}
