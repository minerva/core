package lcsb.mapviewer.converter.graphics.geometry;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

/**
 * This is utility class that helps to find optimal font to fit text into
 * rectangle.
 *
 * @author Piotr Gawron
 */
public final class FontFinder {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static final Logger logger = LogManager.getLogger();

  /**
   * Constructor that prevents class from instantiate.
   */
  private FontFinder() {

  }

  /**
   * Methods looks for the biggest font size that allows to fit text into
   * rectangle, but is not bigger than initialFontSze. IMPORTANT: it's not
   * optimal, so don't set initialFontSize into infinity :).
   *
   * @param initialFontSize maximum font size that we consider
   * @param fontName        name of the font that we use
   * @param graphics        what is the graphics implementation that we use
   * @param border          border of the rectangle where we want to place our text
   * @param text            text to draw
   * @return max font size that allows to draw the text into borders (but not bigger than initialFontSize)
   * @throws RectangleTooSmallException if the border is too small to draw text
   */
  public static double findMaxFontSize(final double initialFontSize, final String fontName, final Graphics2D graphics, final Rectangle2D border,
                                       final String text)
      throws RectangleTooSmallException {
    if (border.getWidth() <= 0 || border.getHeight() <= 0) {
      throw new RectangleTooSmallException("Size of the rectangle must be grater then 0.0");
    }
    int result = (int) initialFontSize;
    if (result <= 0) {
      throw new InvalidArgumentException("initialFontSize must be at least 1.0");
    }
    Font oldFont = graphics.getFont();
    graphics.setFont(new Font(fontName, Font.PLAIN, result));
    while (!textFits(graphics, border, text)) {
      result--;
      if (result <= 0) {
        throw new RectangleTooSmallException("Cannot fit text into the border.");
      }
      graphics.setFont(new Font(fontName, Font.PLAIN, result));
    }

    graphics.setFont(oldFont);
    return result;
  }

  /**
   * Check if the font in graphics allows to draw text in the rectangle.
   *
   * @param graphics implementation of the graphics (with font property)
   * @param border   border of the text in which we want to put text
   * @param text     text that we want to draw
   * @return true if the text can be drawn in the border, false otherwise
   */
  private static boolean textFits(final Graphics2D graphics, final Rectangle2D border, final String text) {
    double width = border.getWidth();
    double height = border.getHeight();

    int lines = 1;
    int index = 0;
    StringBuilder line = new StringBuilder();
    while (index < text.length()) {
      StringBuilder word = new StringBuilder();
      while (index < text.length()) {
        if (text.charAt(index) == ' ' || text.charAt(index) == '\t' || text.charAt(index) == '\n') {
          break;
        } else {
          word.append(text.charAt(index));
          index++;
        }
      }
      if (!lineFits(graphics, width, line.toString() + word)) {
        if (line.length() == 0) {
          return false;
        }
        lines++;
        if (index == text.length() || text.charAt(index) == '\n') {
          line = new StringBuilder();
          lines++;
        } else {
          line = word;
          line.append(text.charAt(index));
        }
      } else if (index < text.length()) {
        line.append(word);
        if (text.charAt(index) == '\n') {
          line = new StringBuilder();
          lines++;
        } else {
          line.append(text.charAt(index));
        }
      }
      index++;
    }

    return lines * (graphics.getFontMetrics().getHeight()) <= height;
  }

  /**
   * Check if width of the text line is not bigger than required.
   *
   * @param graphics font property
   * @param width    max width of the text
   * @param text     text that we want to check
   * @return true if width of the text line is not bigger than required, false otherwise
   */
  private static boolean lineFits(final Graphics2D graphics, final double width, final String text) {
    return width >= graphics.getFontMetrics().stringWidth(text);
  }

  /**
   * Writes centered text in rectangle using fontSize given as a parameter.
   *
   * @param fontSize font size
   * @param fontName font name
   * @param graphics where we want to write a text
   * @param border   what are the borders
   * @param text     what text we want to write
   */
  public static void drawText(final int fontSize, final String fontName, final Graphics2D graphics, final Rectangle2D border, final String text,
                              final HorizontalAlign horizontalAlign, final VerticalAlign verticalAlign) {
    Font oldFont = graphics.getFont();
    graphics.setFont(new Font(fontName, Font.PLAIN, fontSize));

    double width = border.getWidth();

    List<String> lines = new ArrayList<>();
    int index = 0;
    StringBuilder line = new StringBuilder();
    while (index < text.length()) {
      StringBuilder word = new StringBuilder();
      while (index < text.length()) {
        if (text.charAt(index) == ' ' || text.charAt(index) == '\t' || text.charAt(index) == '\n') {
          break;
        } else {
          word.append(text.charAt(index));
          index++;
        }
      }
      if (!lineFits(graphics, width, line.toString() + word)) {
        if (line.length() == 0) {
          throw new InvalidArgumentException("Font too big.");
        }
        lines.add(line.toString());
        if (index == text.length() || text.charAt(index) == '\n') {
          line = new StringBuilder();
          lines.add(word.toString());
        } else {
          line = word;
          line.append(text.charAt(index));
        }
      } else {
        line.append(word);
        if (index < text.length()) {
          if (text.charAt(index) == '\n') {
            lines.add(line.toString());
            line = new StringBuilder();
          } else {
            line.append(text.charAt(index));
          }
        }
      }
      index++;
    }
    lines.add(line.toString());

    double lineHeight = graphics.getFontMetrics().getHeight();
    double y;
    switch (verticalAlign) {
      case TOP:
        y = border.getY() + lineHeight / 2;
        break;
      case MIDDLE:
        y = (int) (border.getCenterY() - lineHeight * lines.size() / 2.0 + graphics.getFontMetrics().getAscent());
        break;
      case BOTTOM:
        y = border.getMaxY() - lineHeight / 2 - lineHeight * lines.size();
        break;
      default:
        throw new InvalidArgumentException("Don't know how to align text with: " + verticalAlign);
    }

    for (final String string : lines) {
      double textWidth = graphics.getFontMetrics().stringWidth(string);
      double currX;
      switch (horizontalAlign) {
        case LEFT:
          currX = border.getMinX();
          break;
        case CENTER:
          currX = (border.getCenterX() - textWidth / 2);
          break;
        case RIGHT:
          currX = (border.getMaxX() - textWidth);
          break;
        default:
          throw new InvalidArgumentException("Don't know how to align text with: " + horizontalAlign);
      }

      graphics.drawString(string, (int) currX, (int) y);
      y += lineHeight;
    }

    graphics.setFont(oldFont);
  }

  /**
   * Writes centered text in rectangle using fontSize given as a parameter.
   *
   * @param fontSize font size
   * @param fontName font name
   * @param graphics where we want to write a text
   * @param border   what are the borders
   * @param text     what text we want to write
   */
  public static void drawText(final int fontSize, final String fontName, final Graphics2D graphics, final Rectangle2D border, final String text) {
    drawText(fontSize, fontName, graphics, border, text, HorizontalAlign.CENTER, VerticalAlign.MIDDLE);
  }
}
