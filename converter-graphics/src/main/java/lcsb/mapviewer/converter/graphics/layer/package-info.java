/**
 * Provides classes that draws layouts with additional graphics elements on the
 * Graphics2D.
 */
package lcsb.mapviewer.converter.graphics.layer;
