package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;

/**
 * This class defines methods used for drawing {@link Degraded} on the
 * {@link Graphics2D} object.
 * 
 * @author Piotr Gawron
 * 
 */
public class DegradedConverter extends SpeciesConverter<Degraded> {
  /**
   * Part of height of the line used to cross degraded circle that goes behind
   * this circle.
   */
  private static final int CROSS_LINE_EXTENDED_LENGTH = 7;

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link DataOverlayEntry} values into
   *          colors when drawing {@link Species}
   */
  public DegradedConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected void drawImpl(final Degraded degraded, final Graphics2D graphics, final ConverterParams params) {
    Area a1 = getShape(degraded);
    Color oldColor = graphics.getColor();
    graphics.setColor(degraded.getFillColor());
    graphics.fill(a1);
    graphics.setColor(degraded.getBorderColor());
    Stroke stroke = graphics.getStroke();
    graphics.setStroke(getBorderLine(degraded));
    graphics.draw(a1);
    graphics.setStroke(stroke);
    drawText(degraded, graphics, params);
    graphics.setColor(oldColor);
  }

  @Override
  protected Area getShape(final Degraded degraded) {
    double diameter = getDiameter(degraded);
    double x = getXCoord(degraded, diameter);
    double y = getYCoord(degraded);
    Area a1 = new Area(new Ellipse2D.Double(x, y, diameter, diameter));

    double lineX1 = degraded.getX() + degraded.getWidth() / 2 + CROSS_LINE_EXTENDED_LENGTH;
    double lineY1 = degraded.getY();

    double lineX2 = degraded.getX() + degraded.getWidth() / 2 - CROSS_LINE_EXTENDED_LENGTH;
    double lineY2 = degraded.getY() + diameter + 2 * CROSS_LINE_EXTENDED_LENGTH;

    GeneralPath path = new GeneralPath(Path2D.WIND_EVEN_ODD, 2);
    path.moveTo(lineX1, lineY1);
    path.lineTo(lineX1 + 1, lineY1);
    path.lineTo(lineX2 + 1, lineY2);
    path.lineTo(lineX2, lineY2);
    path.closePath();

    a1.exclusiveOr(new Area(path));
    return a1;
  }

  /**
   * Returns transformed y coordinate for the {@link Degraded} bioentity.
   * 
   * @param degraded
   *          {@link Degraded} to to which we are looking for y coordinate
   * @return y coordinate of the alias
   */
  private double getYCoord(final Degraded degraded) {
    double y = degraded.getY() + CROSS_LINE_EXTENDED_LENGTH;
    return y;
  }

  /**
   * Returns transformed x coordinate for the degraded alias.
   * 
   * @param degraded
   *          object alias to to which we are looking for x coordinate
   * @param diameter
   *          diameter of cross line used in this alias
   * @return x coordinate of the {@link Degraded} bioentity.
   */
  private double getXCoord(final Degraded degraded, final double diameter) {
    double x = degraded.getX() + (degraded.getWidth() - diameter) / 2;
    return x;
  }

  /**
   * Computes diameter of cross line for the degraded alias.
   * 
   * @param degraded
   *          object alias to to which we are looking for diameter.
   * @return diameter of the cross line
   */
  private double getDiameter(final Degraded degraded) {
    double diameter = Math.min(degraded.getWidth(), degraded.getHeight()) - 2 * CROSS_LINE_EXTENDED_LENGTH;
    if (diameter < 0) {
      logger.warn("Diameter cannot be negative...");
      diameter = 0;
    }
    return diameter;
  }

  @Override
  public String getText(final Degraded degraded) {
    return "";
  }

  @Override
  public PathIterator getBoundPathIterator(final Degraded degraded) {
    throw new InvalidStateException("This class doesn't have bound");
  }

}
