package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This class defines methods used for drawing Drug SpeciesAlias on the
 * graphics2d object.
 * 
 * @author Piotr Gawron
 * 
 */
public class DrugConverter extends SpeciesConverter<Drug> {

  /**
   * Distance between internal and external border of drug graphical
   * representation.
   */
  private static final int OFFSET_BETWEEN_BORDERS = 4;

  /**
   * How big should be the arc in rectangle for drug representation.
   */
  private static final int RECTANGLE_CORNER_ARC_SIZE = 40;

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link ColorSchema} values into
   *          colors when drawing {@link Species}
   */
  public DrugConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  /**
   * Returns shape of the Drug .
   * 
   * @param drug
   *          {@link Drug} for which we are looking for a border
   * @return Shape object defining given alias
   */
  @Override
  protected Shape getShape(final Drug drug) {
    return new RoundRectangle2D.Double(drug.getX(), drug.getY(), drug.getWidth(), drug.getHeight(),
        RECTANGLE_CORNER_ARC_SIZE, RECTANGLE_CORNER_ARC_SIZE);
  }

  @Override
  protected void drawImpl(final Drug drug, final Graphics2D graphics, final ConverterParams params) {
    Shape a1 = getShape(drug);
    double offset = OFFSET_BETWEEN_BORDERS;
    Shape a2 = new RoundRectangle2D.Double(
        drug.getX() + offset, drug.getY() + offset, drug.getWidth() - 2 * offset, drug.getHeight() - 2 * offset,
        RECTANGLE_CORNER_ARC_SIZE,
        RECTANGLE_CORNER_ARC_SIZE);
    Color oldColor = graphics.getColor();
    graphics.setColor(drug.getFillColor());
    graphics.fill(a1);
    graphics.setColor(drug.getBorderColor());
    Stroke stroke = graphics.getStroke();
    graphics.setStroke(getBorderLine(drug));
    graphics.draw(a1);
    graphics.draw(a2);
    graphics.setStroke(stroke);
    drawText(drug, graphics, params);
    graphics.setColor(oldColor);
  }

  /**
   * Returns shape of the {@link Drug} as a list of points.
   * 
   * @param drug
   *          {@link Drug} for which we are looking for a border
   * @return list of points defining border of the given {@link Drug}
   */
  protected ArrayList<Point2D> getDrugPoints(final Drug drug) {
    ArrayList<Point2D> list = new ArrayList<>();

    double x = drug.getX();
    double y = drug.getY();
    double width = drug.getWidth();
    double height = drug.getHeight();

    list.add(new Point2D.Double(x, y + height / 2));
    list.add(new Point2D.Double(x + width / 12, y));
    list.add(new Point2D.Double(x + width / 2, y));
    list.add(new Point2D.Double(x + width * 11 / 12, y));
    list.add(new Point2D.Double(x + width, y + height / 2));
    list.add(new Point2D.Double(x + width * 11 / 12, y + height));
    list.add(new Point2D.Double(x + width / 2, y + height));
    list.add(new Point2D.Double(x + width / 12, y + height));

    return list;
  }

  @Override
  public PathIterator getBoundPathIterator(final Drug drug) {
    return getShape(drug).getPathIterator(new AffineTransform());
  }

}
