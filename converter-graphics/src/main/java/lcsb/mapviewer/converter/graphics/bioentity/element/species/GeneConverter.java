package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;

/**
 * This class defines methods used for drawing Gene SpeciesAlias on the
 * graphics2d object.
 * 
 * @author Piotr Gawron
 * 
 */
public class GeneConverter extends SpeciesConverter<Gene> {

  /**
   * Default constructor.
   * 
   * @param colorExtractor
   *          Object that helps to convert {@link DataOverlayEntry} values into
   *          colors when drawing {@link Species}
   */
  public GeneConverter(final ColorExtractor colorExtractor) {
    super(colorExtractor);
  }

  @Override
  protected void drawImpl(final Gene gene, final Graphics2D graphics, final ConverterParams params) {
    Shape shape = getShape(gene);
    Color c = graphics.getColor();
    graphics.setColor(gene.getFillColor());
    graphics.fill(shape);
    graphics.setColor(gene.getBorderColor());
    Stroke stroke = graphics.getStroke();
    graphics.setStroke(getBorderLine(gene));
    graphics.draw(shape);
    graphics.setStroke(stroke);

    drawText(gene, graphics, params);
    graphics.setColor(c);
  }

  /**
   * Shape representation of the {@link Gene}.
   * 
   * @param gene
   *          {@link Gene} for which we are looking for a {@link Shape}
   * @return {@link Shape} that represents {@link Gene}
   */
  @Override
  protected Shape getShape(final Gene gene) {
    Shape shape;
    shape = new Rectangle(gene.getX().intValue(), gene.getY().intValue(), gene.getWidth().intValue(),
        gene.getHeight().intValue());
    return shape;
  }

  @Override
  public PathIterator getBoundPathIterator(final Gene gene) {
    return getShape(gene).getPathIterator(new AffineTransform());
  }

}
