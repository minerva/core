package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;

import lcsb.mapviewer.model.map.species.field.StructuralState;

public class ModificationResidueSbgnConverter extends ModificationResidueConverter {

  @Override
  Shape getStructuralStateShape(final StructuralState state) {
    double arcSize = Math.min(state.getWidth(), state.getHeight());

    return new RoundRectangle2D.Double(
        state.getX(),
        state.getY(),
        state.getWidth(),
        state.getHeight(),
        arcSize,
        arcSize);
  }

}
