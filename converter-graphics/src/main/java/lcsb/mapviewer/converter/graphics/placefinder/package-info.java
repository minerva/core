/**
 * This package contains utility to find place where description can be put
 * among many overlapping rectangles.
 */
package lcsb.mapviewer.converter.graphics.placefinder;
