package lcsb.mapviewer.converter.graphics.bioentity;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.nio.file.Files;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.GraphicsTestFunctions;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class BioEntityConverterImplTest extends GraphicsTestFunctions {

  private ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE, Color.WHITE);

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDrawReactionWithSemanticZoomingAndReactantOff() throws Exception {
    Graphics2D graphics = Mockito.mock(Graphics2D.class);
    Reaction reaction = createReaction(1.0);
    reaction.getReactants().get(0).getElement().setVisibilityLevel("11");

    BioEntityConverterImpl rc = new BioEntityConverterImpl(reaction, false, colorExtractor);
    rc.draw(reaction, graphics, new ConverterParams().nested(true).level(10));

    verify(graphics, times(0)).draw(any(GeneralPath.class));
  }

  @Test
  public void testDrawReactionWithSemanticZoomingAndProductOff() throws Exception {
    Graphics2D graphics = Mockito.mock(Graphics2D.class);

    Reaction reaction = createReaction(1.0);
    reaction.getProducts().get(0).getElement().setVisibilityLevel("11");

    BioEntityConverterImpl rc = new BioEntityConverterImpl(reaction, false, colorExtractor);
    rc.draw(reaction, graphics, new ConverterParams().nested(true).level(10));

    verify(graphics, times(0)).draw(any(GeneralPath.class));
  }

  @Test
  public void testDrawAliasWithGlyph() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = Mockito.spy(bi.createGraphics());

    GenericProtein alias = createProtein();
    Glyph glyph = new Glyph();
    UploadedFileEntry file = new UploadedFileEntry();
    file.setOriginalFileName("test");
    byte[] fileContent = Files.readAllBytes(new File("testFiles/glyph.png").toPath());

    file.setFileContent(fileContent);
    glyph.setFile(file);
    alias.setGlyph(glyph);
    BioEntityConverterImpl converter = new BioEntityConverterImpl(alias, false, colorExtractor);
    converter.draw(alias, graphics, new ConverterParams());

    verify(graphics, times(1)).drawImage(any(Image.class), anyInt(), anyInt(), nullable(ImageObserver.class));
  }

  private Reaction createReaction(final double lineWidth) {
    Reaction result = new Reaction("re");

    Modifier modifier = new Catalysis(new GenericProtein("s1"));
    modifier.setLine(new PolylineData(new Point2D.Double(100, 20), new Point2D.Double(100, 80)));
    modifier.getLine().setWidth(lineWidth);

    Reactant reactant = new Reactant(new GenericProtein("s2"));
    reactant.setLine(new PolylineData(new Point2D.Double(90, 90), new Point2D.Double(10, 90)));
    reactant.getLine().setWidth(lineWidth);
    Product product = new Product(new GenericProtein("s3"));
    product.setLine(new PolylineData(new Point2D.Double(200, 90), new Point2D.Double(110, 90)));
    product.getLine().setWidth(lineWidth);
    result.addModifier(modifier);
    result.addProduct(product);
    result.addReactant(reactant);
    return result;
  }

}
