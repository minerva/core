package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.GraphicsTestFunctions;
import lcsb.mapviewer.converter.graphics.geometry.ArrowTransformation;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;
import lcsb.mapviewer.model.map.species.field.RegulatoryRegion;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ModificationResidueConverterTest extends GraphicsTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDrawResidue() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();

    ModificationResidue residue = new Residue();
    residue.setPosition(new Point2D.Double(10, 10));

    ModificationResidueConverter converter = Mockito.spy(new ModificationResidueConverter());
    converter.drawModification(residue, graphics, new ConverterParams());
    verify(converter, times(1)).drawResidue(any(), any(), anyBoolean());
  }

  @Test
  public void testDrawResidueWithState() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();

    Residue residue = createResidue();

    ModificationResidueConverter converter = Mockito.spy(new ModificationResidueConverter());
    converter.drawModification(residue, graphics, new ConverterParams());
    verify(converter, times(1)).drawResidue(any(), any(), anyBoolean());
  }

  @Test
  public void testDrawBindingRegion() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();

    Protein protein = createProtein();
    BindingRegion bindingRegion = new BindingRegion();
    bindingRegion.setPosition(new Point2D.Double(10, 10));
    bindingRegion.setWidth(100.0);
    bindingRegion.setHeight(10.0);
    protein.addBindingRegion(bindingRegion);

    ModificationResidueConverter converter = Mockito.spy(new ModificationResidueConverter());
    converter.drawModification(bindingRegion, graphics, new ConverterParams());
    verify(converter, times(1)).drawBindingRegion(any(), any());
  }

  @Test
  public void testDrawModificationSite() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();

    ModificationSite modificationSite = createModificationSite();

    ModificationResidueConverter converter = Mockito.spy(new ModificationResidueConverter());
    converter.drawModification(modificationSite, graphics, new ConverterParams());
    verify(converter, times(1)).drawModificationSite(any(), any(), anyBoolean());
  }

  @Test
  public void testDrawProteinBindingRegion() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();

    ProteinBindingDomain proteinBindingRegion = new ProteinBindingDomain();
    proteinBindingRegion.setPosition(new Point2D.Double(10, 10));
    proteinBindingRegion.setWidth(100);
    proteinBindingRegion.setHeight(20);

    ModificationResidueConverter converter = Mockito.spy(new ModificationResidueConverter());
    converter.drawModification(proteinBindingRegion, graphics, new ConverterParams());
    verify(converter, times(1)).drawProteinBindingDomain(any(), any());
  }

  @Test
  public void testDrawCodingRegion() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();

    CodingRegion codingRegion = createCodingRegion();

    ModificationResidueConverter converter = Mockito.spy(new ModificationResidueConverter());
    converter.drawModification(codingRegion, graphics, new ConverterParams());
    verify(converter, times(1)).drawCodingRegion(any(), any());
  }

  @Test
  public void testDrawRegulatoryRegion() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();

    RegulatoryRegion regulatoryRegion = new RegulatoryRegion();
    regulatoryRegion.setPosition(new Point2D.Double(10, 10));
    regulatoryRegion.setWidth(100);
    regulatoryRegion.setHeight(20);

    ModificationResidueConverter converter = Mockito.spy(new ModificationResidueConverter());
    converter.drawModification(regulatoryRegion, graphics, new ConverterParams());
    verify(converter, times(1)).drawRegulatoryRegion(any(), any());
  }

  @Test
  public void testDrawTranscriptionSite() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();

    TranscriptionSite transcriptionSite = new TranscriptionSite();
    transcriptionSite.setPosition(new Point2D.Double(10, 10));
    transcriptionSite.setWidth(100);
    transcriptionSite.setHeight(20);
    transcriptionSite.setActive(true);
    transcriptionSite.setBorderColor(Color.BLACK);
    transcriptionSite.setDirection("LEFT");

    ModificationResidueConverter converter = Mockito.spy(new ModificationResidueConverter());

    converter.drawModification(transcriptionSite, graphics, new ConverterParams());
    verify(converter, times(1)).drawTranscriptionSite(any(), any());
  }

  @Test
  public void testDrawTranscriptionSiteSize() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();

    TranscriptionSite transcriptionSite = new TranscriptionSite();
    transcriptionSite.setPosition(new Point2D.Double(10, 10));
    transcriptionSite.setWidth(100);
    transcriptionSite.setHeight(20);
    transcriptionSite.setDirection("LEFT");

    ArrowTransformation arrowTransformation = Mockito.spy(new ArrowTransformation());

    ModificationResidueConverter converter = new ModificationResidueConverter();
    converter.setArrowTransformation(arrowTransformation);

    converter.drawModification(transcriptionSite, graphics, new ConverterParams());
    verify(arrowTransformation, times(1)).drawLine(
        argThat(polylineData -> polylineData.getEndAtd().getLen() == ModificationResidueConverter.TRANSCRIPTION_SITE_ARROW_LENGTH),
        any());
  }

  @Test
  public void testDrawTranscriptionSiteDescription() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = Mockito.spy(bi.createGraphics());

    TranscriptionSite transcriptionSite = createTranscriptionSite();

    ModificationResidueConverter converter = new ModificationResidueConverter();

    converter.drawModification(transcriptionSite, graphics, new ConverterParams());
    verify(graphics, times(1)).drawString(anyString(), anyInt(), anyInt());
    verify(graphics, times(2)).setColor(Color.GREEN);
  }
}
