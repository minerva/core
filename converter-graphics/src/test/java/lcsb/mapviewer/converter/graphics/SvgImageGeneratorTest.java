package lcsb.mapviewer.converter.graphics;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Complex;

public class SvgImageGeneratorTest extends GraphicsTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSaveToFile() throws Exception {
    Model model = createCompartmentModel();

    SvgImageGenerator sig = new SvgImageGenerator(new AbstractImageGenerator.Params().model(model));

    ByteArrayOutputStream output = new ByteArrayOutputStream();
    sig.saveToOutputStream(output);

    assertTrue(output.toString().contains("viewBox"));
  }

  private Model createCompartmentModel() {
    Model model = new ModelFullIndexed(null);
    model.setWidth(526);
    model.setHeight(346);
    Complex alias = new Complex("1");
    alias.setName("a");
    alias.setX(300);
    alias.setY(90);
    alias.setWidth(100);
    alias.setHeight(50);
    model.addElement(alias);

    return model;
  }

}
