package lcsb.mapviewer.converter.graphics;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Pair;

public class ImageGeneratorsTest extends GraphicsTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAvailableGenerators() {
    ImageGenerators imageGenerators = new ImageGenerators();

    boolean pngGenFound = false;
    boolean pdfGenFound = false;
    for (Pair<String, Class<? extends AbstractImageGenerator>> element : imageGenerators
        .getAvailableImageGenerators()) {
      if (element.getRight().equals(PngImageGenerator.class)) {
        pngGenFound = true;
      }
      if (element.getRight().equals(PdfImageGenerator.class)) {
        pdfGenFound = true;
      }
    }
    assertTrue(pngGenFound);
    assertTrue(pdfGenFound);
  }

}
