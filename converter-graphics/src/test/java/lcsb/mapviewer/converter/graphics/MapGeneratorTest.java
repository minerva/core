package lcsb.mapviewer.converter.graphics;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.converter.graphics.MapGenerator.MapGeneratorParams;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.ProjectBackground;
import lcsb.mapviewer.model.map.layout.ProjectBackgroundImageLayer;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class MapGeneratorTest extends GraphicsTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGenerateMapImagesModelStringBoolean() throws Exception {
    String dir = "test_images";
    File f = new File(dir);
    if (f.exists()) {
      FileUtils.deleteDirectory(f);
    }

    Model model = createModel();
    MapGenerator generator = new MapGenerator();
    MapGeneratorParams params = generator.new MapGeneratorParams().model(model).directory(dir);
    generator.generateMapImages(params);

    File f1 = new File(dir + "/2/0/0.PNG");
    assertTrue(f1.exists());
    File f2 = new File(dir + "/4/1/1.PNG");
    assertTrue(f2.exists());

    f = new File(dir);
    FileUtils.deleteDirectory(f);
  }

  private Model createModel() {
    Model model = new ModelFullIndexed(null);
    model.setWidth(1305.7682370820667);
    model.setHeight(473.0);
    return model;
  }

  @Test
  public void testRemoveBackground() throws Exception {
    String dir = "test_images";
    File f = new File(dir);

    Model model = createModel();
    Project project = new Project();
    project.addModel(model);
    ProjectBackground background = new ProjectBackground("a");
    background.addProjectBackgroundImageLayer(new ProjectBackgroundImageLayer(model, dir));
    project.addProjectBackground(background);

    MapGenerator generator = new MapGenerator();
    MapGeneratorParams params = generator.new MapGeneratorParams().model(model).directory(dir);
    generator.generateMapImages(params);
    assertTrue(f.exists());
    generator.removeProjectBackground(project.getProjectBackgrounds().get(0));

    assertFalse(f.exists());
  }
}
