package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.image.BufferedImage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.GraphicsTestFunctions;
import lcsb.mapviewer.model.map.species.Complex;

public class ComplexConverterTest extends GraphicsTestFunctions {
  

  private ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE, Color.WHITE);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDrawText() throws Exception {
    int size = 200;
    BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();
    ComplexConverter rc = new ComplexConverter(colorExtractor);

    Complex alias = createComplex();
    rc.drawText(alias, graphics, new ConverterParams());

    boolean onlyWhite = true;
    for (int x = 0; x < size; x++) {
      for (int y = 0; y < size; y++) {
        if (bi.getRGB(x, y) != 0) {
          onlyWhite = false;
        }
      }
    }
    assertFalse(onlyWhite);
  }

  @Test
  public void testDrawTooBigText() throws Exception {
    int size = 200;
    double scale = 100;
    BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();
    Color color = graphics.getColor();
    graphics.scale(1.0 / scale, 1.0 / scale);
    ComplexConverter rc = new ComplexConverter(colorExtractor);

    Complex alias = createComplex();
    rc.drawText(alias, graphics, new ConverterParams());

    assertEquals(color, graphics.getColor());
  }

  @Override
  protected Complex createComplex() {
    Complex protein = new Complex("id");
    protein.setName("NAME_OF_THE_ELEMENT");
    protein.setX(10);
    protein.setY(20);
    protein.setWidth(100);
    protein.setHeight(80);
    protein.setFillColor(Color.WHITE);

    return protein;
  }

  @Test
  public void testDrawHomodimer() throws Exception {
    int size = 200;
    BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = Mockito.spy(bi.createGraphics());
    ComplexConverter rc = new ComplexConverter(colorExtractor);

    Complex protein = createComplex();
    protein.setHomodimer(4);
    rc.drawImpl(protein, graphics, new ConverterParams().sbgnFormat(false));

    Mockito.verify(graphics, times(4)).draw(any(Shape.class));
  }

}
