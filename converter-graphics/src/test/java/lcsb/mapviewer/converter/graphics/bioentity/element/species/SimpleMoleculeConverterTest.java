package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.GraphicsTestFunctions;
import lcsb.mapviewer.model.map.species.SimpleMolecule;

public class SimpleMoleculeConverterTest extends GraphicsTestFunctions {

  private ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE, Color.WHITE);

  private BufferedImage bi;
  private Graphics2D graphics;

  private SimpleMoleculeConverter rc;

  @Before
  public void setUp() throws Exception {
    int size = 200;
    bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
    graphics = Mockito.spy(bi.createGraphics());
    rc = new SimpleMoleculeConverter(colorExtractor);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDrawText() throws Exception {

    SimpleMolecule simpleMolecule = createSimpleMolecule();
    double x = simpleMolecule.getX();
    double width = simpleMolecule.getWidth();
    rc.drawImpl(simpleMolecule, graphics, new ConverterParams().sbgnFormat(true));
    assertEquals("Coordinates shouldn't be changed when drawing SBGN-ML", x, simpleMolecule.getX(),
        Configuration.EPSILON);
    assertEquals("Width shouldn't be changed when drawing SBGN-ML", width, simpleMolecule.getWidth(),
        Configuration.EPSILON);
  }

  @Test
  public void testDrawHomodimer() throws Exception {
    SimpleMolecule molecule = createSimpleMolecule();
    molecule.setHomodimer(4);
    rc.drawImpl(molecule, graphics, new ConverterParams().sbgnFormat(false));

    Mockito.verify(graphics, times(4)).draw(any(Ellipse2D.class));
  }

}
