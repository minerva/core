package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;

import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.GraphicsTestFunctions;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.field.StructuralState;

public class ComplexSbgnConverterTest extends GraphicsTestFunctions {

  private ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE, Color.WHITE);

  @Test
  public void testStructuralStateIsNotDrawnInComplex() throws Exception {
    int size = 200;
    BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = Mockito.spy(bi.createGraphics());
    ComplexConverter rc = new ComplexSbgnConverter(colorExtractor);

    Complex protein = createComplex();
    StructuralState state = createStructuralState(protein);
    protein.addStructuralState(state);
    rc.drawImpl(protein, graphics, new ConverterParams().sbgnFormat(false));

    Mockito.verify(graphics, times(0)).draw(any(RoundRectangle2D.class));
    Mockito.verify(graphics, times(0)).draw(any(Ellipse2D.class));
  }

}
