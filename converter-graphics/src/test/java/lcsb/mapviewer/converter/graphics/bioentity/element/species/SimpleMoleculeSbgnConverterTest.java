package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.GraphicsTestFunctions;
import lcsb.mapviewer.model.map.species.SimpleMolecule;

public class SimpleMoleculeSbgnConverterTest extends GraphicsTestFunctions {

  private ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE, Color.WHITE);

  private BufferedImage bi;
  private Graphics2D graphics;

  private SimpleMoleculeSbgnConverter rc;

  @Before
  public void setUp() throws Exception {
    int size = 200;
    bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
    graphics = Mockito.spy(bi.createGraphics());
    rc = new SimpleMoleculeSbgnConverter(colorExtractor);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDrawHomodimer() throws Exception {
    SimpleMolecule molecule = createSimpleMolecule();
    molecule.setHomodimer(4);
    rc.drawImpl(molecule, graphics, new ConverterParams().sbgnFormat(false));

    Mockito.verify(graphics, times(2)).draw(any(RoundRectangle2D.class));
  }

}
