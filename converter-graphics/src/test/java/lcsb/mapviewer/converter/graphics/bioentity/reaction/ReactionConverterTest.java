package lcsb.mapviewer.converter.graphics.bioentity.reaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.GraphicsTestFunctions;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.OrOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.ReactionRect;
import lcsb.mapviewer.model.map.reaction.type.UnknownTransitionReaction;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.overlay.GenericDataOverlayEntry;

public class ReactionConverterTest extends GraphicsTestFunctions {

  private ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE, Color.WHITE);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDrawReactionWithDataOverlays() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();
    ReactionConverter rc = new ReactionConverter(colorExtractor);

    Reaction reaction = createReaction(5.0);
    rc.draw(reaction, graphics, new ConverterParams().sbgnFormat(false));

    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    ImageIO.write(bi, "PNG", outputStream);
    byte[] output1 = outputStream.toByteArray();

    Reaction reaction2 = createReaction(1.0);

    bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    graphics = bi.createGraphics();

    DataOverlayEntry schema = new GenericDataOverlayEntry();
    schema.setColor(Color.BLACK);
    schema.setLineWidth(5.0);
    List<DataOverlayEntry> schemas = new ArrayList<>();
    schemas.add(schema);

    rc.draw(reaction2, graphics, new ConverterParams().sbgnFormat(false), Arrays.asList(schemas));

    outputStream = new ByteArrayOutputStream();
    ImageIO.write(bi, "PNG", outputStream);
    byte[] output2 = outputStream.toByteArray();

    // FileUtils.writeByteArrayToFile(new File("tmp.png"), output1);
    // FileUtils.writeByteArrayToFile(new File("tmp2.png"), output2);

    // Desktop.getDesktop().open(new File("tmp.png"));
    // Desktop.getDesktop().open(new File("tmp2.png"));

    assertTrue(Arrays.equals(output1, output2));

  }

  @Test
  public void testDrawAfterDrawingReactionWithDataOverlays() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();
    ReactionConverter rc = new ReactionConverter(colorExtractor);

    Reaction reaction = createReaction(1.0);
    rc.draw(reaction, graphics, new ConverterParams().sbgnFormat(false));

    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    ImageIO.write(bi, "PNG", outputStream);
    byte[] output1 = outputStream.toByteArray();

    Reaction reaction2 = createReaction(1.0);

    bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    graphics = bi.createGraphics();

    DataOverlayEntry schema = new GenericDataOverlayEntry();
    schema.setColor(Color.BLACK);
    schema.setLineWidth(5.0);
    List<DataOverlayEntry> schemas = new ArrayList<>();
    schemas.add(schema);

    rc.draw(reaction2, graphics, new ConverterParams().sbgnFormat(false), Arrays.asList(schemas));
    bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    graphics = bi.createGraphics();
    rc.draw(reaction2, graphics, new ConverterParams().sbgnFormat(false), new ArrayList<>());

    outputStream = new ByteArrayOutputStream();
    ImageIO.write(bi, "PNG", outputStream);
    byte[] output2 = outputStream.toByteArray();

    // FileUtils.writeByteArrayToFile(new File("tmp.png"), output1);
    // FileUtils.writeByteArrayToFile(new File("tmp2.png"), output2);

    // Desktop.getDesktop().open(new File("tmp.png"));
    // Desktop.getDesktop().open(new File("tmp2.png"));

    assertTrue(Arrays.equals(output1, output2));

  }

  @Test
  public void testDrawReactionWithFewDataOverlays() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();
    ReactionConverter rc = new ReactionConverter(colorExtractor);

    Reaction reaction = createReaction(3.0);
    rc.draw(reaction, graphics, new ConverterParams().sbgnFormat(false));

    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    ImageIO.write(bi, "PNG", outputStream);
    byte[] output1 = outputStream.toByteArray();

    Reaction reaction2 = createReaction(1.0);

    bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    graphics = bi.createGraphics();

    DataOverlayEntry schema = new GenericDataOverlayEntry();
    schema.setColor(Color.BLACK);
    schema.setLineWidth(5.0);
    List<List<DataOverlayEntry>> schemas = new ArrayList<>();
    schemas.add(Arrays.asList(schema));
    schemas.add(Arrays.asList(schema));

    rc.draw(reaction2, graphics, new ConverterParams().sbgnFormat(false), schemas);

    outputStream = new ByteArrayOutputStream();
    ImageIO.write(bi, "PNG", outputStream);
    byte[] output2 = outputStream.toByteArray();

    // FileUtils.writeByteArrayToFile(new File("tmp.png"), output1);
    // FileUtils.writeByteArrayToFile(new File("tmp2.png"), output2);

    // Desktop.getDesktop().open(new File("tmp.png"));
    // Desktop.getDesktop().open(new File("tmp2.png"));

    assertTrue(Arrays.equals(output1, output2));

  }

  private Reaction createReaction(final double lineWidth) {
    Reaction result = new Reaction("re");

    Modifier modifier = new Catalysis(new GenericProtein("s1"));
    modifier.setLine(new PolylineData(new Point2D.Double(100, 20), new Point2D.Double(100, 80)));
    modifier.getLine().setWidth(lineWidth);

    Reactant reactant = new Reactant(new GenericProtein("s2"));
    reactant.setLine(new PolylineData(new Point2D.Double(90, 90), new Point2D.Double(10, 90)));
    reactant.getLine().setWidth(lineWidth);
    Product product = new Product(new GenericProtein("s3"));
    product.setLine(new PolylineData(new Point2D.Double(200, 90), new Point2D.Double(110, 90)));
    product.getLine().setWidth(lineWidth);
    result.addModifier(modifier);
    result.addProduct(product);
    result.addReactant(reactant);
    result.setLine(new PolylineData(new Point2D.Double(105, 90), new Point2D.Double(95, 90)));
    result.getLine().setWidth(lineWidth);
    return result;
  }

  private Reaction createComplexReaction(final double lineWidth) {
    Reaction result = new Reaction("re");

    Modifier modifier1 = new Catalysis(new GenericProtein("s1-1"));
    modifier1.setLine(new PolylineData(new Point2D.Double(80, 20), new Point2D.Double(100, 40)));
    modifier1.getLine().setWidth(lineWidth);

    Modifier modifier2 = new Catalysis(new GenericProtein("s1-2"));
    modifier2.setLine(new PolylineData(new Point2D.Double(120, 20), new Point2D.Double(100, 40)));
    modifier2.getLine().setWidth(lineWidth);

    AndOperator modifierOperator = new AndOperator();
    modifierOperator.addInput(modifier1);
    modifierOperator.addInput(modifier2);
    modifierOperator.setLine(new PolylineData(new Point2D.Double(100, 40), new Point2D.Double(100, 80)));

    Reactant reactant1 = new Reactant(new GenericProtein("s2-1"));
    reactant1.setLine(new PolylineData(new Point2D.Double(60, 90), new Point2D.Double(10, 70)));
    reactant1.getLine().setWidth(lineWidth);
    Reactant reactant2 = new Reactant(new GenericProtein("s2-1"));
    reactant2.setLine(new PolylineData(new Point2D.Double(60, 90), new Point2D.Double(10, 110)));
    reactant2.getLine().setWidth(lineWidth);
    OrOperator reactantOperator = new OrOperator();
    reactantOperator.addInput(reactant1);
    reactantOperator.addInput(reactant2);
    reactantOperator.setLine(new PolylineData(new Point2D.Double(60, 90), new Point2D.Double(90, 90)));

    Product product1 = new Product(new GenericProtein("s3-1"));
    product1.setLine(new PolylineData(new Point2D.Double(200, 70), new Point2D.Double(130, 90)));
    product1.getLine().setWidth(lineWidth);
    Product product2 = new Product(new GenericProtein("s3-2"));
    product2.setLine(new PolylineData(new Point2D.Double(200, 110), new Point2D.Double(130, 90)));
    product2.getLine().setWidth(lineWidth);
    AndOperator productOperator = new AndOperator();
    productOperator.addOutput(product1);
    productOperator.addOutput(product2);
    productOperator.setLine(new PolylineData(new Point2D.Double(130, 90), new Point2D.Double(110, 90)));

    result.setLine(new PolylineData(new Point2D.Double(105, 90), new Point2D.Double(95, 90)));

    result.addModifier(modifier1);
    result.addModifier(modifier2);
    result.addNode(modifierOperator);
    result.addProduct(product1);
    result.addProduct(product2);
    result.addNode(productOperator);
    result.addReactant(reactant1);
    result.addReactant(reactant2);
    result.addNode(reactantOperator);
    return result;
  }

  @Test
  public void testDrawReactionWithSemanticZooming() throws Exception {
    Graphics2D graphics = Mockito.mock(Graphics2D.class);
    ReactionConverter rc = new ReactionConverter(colorExtractor);

    Reaction reaction = createReaction(1.0);
    rc.draw(reaction, graphics, new ConverterParams().nested(true).level(10));

    verify(graphics, times(4)).draw(nullable(Shape.class));
  }

  @Test
  public void testDrawReactionWithSemanticZoomingAndModifierOff() throws Exception {
    Graphics2D graphics = Mockito.mock(Graphics2D.class);
    ReactionConverter rc = new ReactionConverter(colorExtractor);

    Reaction reaction = createReaction(1.0);
    reaction.getModifiers().get(0).getElement().setVisibilityLevel("11");
    rc.draw(reaction, graphics, new ConverterParams().nested(true).level(10));

    verify(graphics, times(3)).draw(nullable(Shape.class));
  }

  @Test
  public void testDrawReactionWithSemanticZoomingAndReactantOff() throws Exception {
    Graphics2D graphics = Mockito.mock(Graphics2D.class);
    ReactionConverter rc = new ReactionConverter(colorExtractor);

    Reaction reaction = createReaction(1.0);
    reaction.getReactants().get(0).getElement().setVisibilityLevel("11");
    rc.draw(reaction, graphics, new ConverterParams().nested(true).level(10));

    verify(graphics, times(3)).draw(nullable(Shape.class));
  }

  @Test
  public void testDrawReactionWithSemanticZoomingAndProductOff() throws Exception {
    Graphics2D graphics = Mockito.mock(Graphics2D.class);
    ReactionConverter rc = new ReactionConverter(colorExtractor);

    Reaction reaction = createReaction(1.0);
    reaction.getProducts().get(0).getElement().setVisibilityLevel("11");
    rc.draw(reaction, graphics, new ConverterParams().nested(true).level(10));

    verify(graphics, times(3)).draw(nullable(Shape.class));
  }

  @Test
  public void testDrawComplexReactionWithSemanticZoomingAndModiferOff() throws Exception {
    Graphics2D graphics = createGraphicsMock();
    ReactionConverter rc = new ReactionConverter(colorExtractor);

    Reaction reaction = createComplexReaction(1.0);
    reaction.getModifiers().get(0).getElement().setVisibilityLevel("11");
    rc.draw(reaction, graphics, new ConverterParams().nested(true).level(10));

    verify(graphics, times(12)).draw(nullable(Shape.class));
  }

  @Test
  public void testDrawOperatorWithSbgn() throws Exception {
    Graphics2D graphics = createGraphicsMock();
    ReactionConverter rc = new ReactionConverter(colorExtractor);

    NodeOperator operator = new AndOperator();
    operator.setLine(new PolylineData(new Point2D.Double(1, 2), new Point2D.Double(3, 4)));
    rc.drawOperator(graphics, operator, true);

    verify(graphics, times(1)).drawString(eq("AND"), anyInt(), anyInt());
  }

  @Test
  public void testDrawOperatorWithoutSbgn() throws Exception {
    Graphics2D graphics = createGraphicsMock();
    ReactionConverter rc = new ReactionConverter(colorExtractor);

    NodeOperator operator = new AndOperator();
    operator.setLine(new PolylineData(new Point2D.Double(1, 2), new Point2D.Double(3, 4)));
    rc.drawOperator(graphics, operator, false);

    verify(graphics, times(1)).drawString(eq(""), anyInt(), anyInt());
  }

  @Test
  public void testDrawComplexReactionWithSemanticZoomingAndAllModifersOff() throws Exception {
    Graphics2D graphics = createGraphicsMock();
    ReactionConverter rc = new ReactionConverter(colorExtractor);

    Reaction reaction = createComplexReaction(1.0);
    reaction.getModifiers().get(0).getElement().setVisibilityLevel("11");
    reaction.getModifiers().get(1).getElement().setVisibilityLevel("11");
    rc.draw(reaction, graphics, new ConverterParams().nested(true).level(10));

    verify(graphics, times(9)).draw(nullable(Shape.class));
  }

  @Test
  public void testDrawComplexReactionWithSemanticZoomingAndReactantOff() throws Exception {
    Graphics2D graphics = createGraphicsMock();
    ReactionConverter rc = new ReactionConverter(colorExtractor);

    Reaction reaction = createComplexReaction(1.0);
    reaction.getReactants().get(0).getElement().setVisibilityLevel("11");
    rc.draw(reaction, graphics, new ConverterParams().nested(true).level(10));

    verify(graphics, times(12)).draw(nullable(Shape.class));
  }

  @Test
  public void testDrawComplexReactionWithSemanticZoomingAndProductOff() throws Exception {
    Graphics2D graphics = createGraphicsMock();
    ReactionConverter rc = new ReactionConverter(colorExtractor);

    Reaction reaction = createComplexReaction(1.0);
    reaction.getProducts().get(0).getElement().setVisibilityLevel("11");
    rc.draw(reaction, graphics, new ConverterParams().nested(true).level(10));

    verify(graphics, times(12)).draw(nullable(Shape.class));
  }

  private Reaction createUnknownTransitionReaction(final double angle) {
    PointTransformation pt = new PointTransformation();

    Reaction result = new UnknownTransitionReaction("re");

    Point2D center = new Point2D.Double(100, 90);
    Point2D reactantCenter = new Point2D.Double(10, 90);
    Point2D productCenter = new Point2D.Double(200, 90);

    Point2D reactantEnd = new Point2D.Double(90, 90);
    Point2D productEnd = new Point2D.Double(110, 90);

    result.setLine(new PolylineData(new Point2D.Double(105, 90), new Point2D.Double(95, 90)));

    Reactant reactant = new Reactant(new GenericProtein("s2"));

    reactant.setLine(
        new PolylineData(pt.rotatePoint(reactantEnd, angle, center), pt.rotatePoint(reactantCenter, angle, center)));
    reactant.getLine().setWidth(1.0);
    Product product = new Product(new GenericProtein("s3"));
    product.setLine(
        new PolylineData(pt.rotatePoint(productCenter, angle, center), pt.rotatePoint(productEnd, angle, center)));
    product.getLine().setWidth(1.0);
    result.addProduct(product);
    result.addReactant(reactant);
    return result;
  }

  @Test
  public void testDrawReactionWithRectangle() throws Exception {
    for (double angle = 0.0; angle <= Math.PI * 2; angle += 0.1) {
      Graphics2D graphics = createGraphicsMock();
      ReactionConverter rc = new ReactionConverter(colorExtractor);

      Reaction reaction = createUnknownTransitionReaction(angle);
      rc.draw(reaction, graphics, new ConverterParams().sbgnFormat(false));

      InOrder inOrder = Mockito.inOrder(graphics);

      inOrder.verify(graphics).rotate(anyDouble(), anyDouble(), anyDouble());
      inOrder.verify(graphics).rotate(anyDouble(), anyDouble(), anyDouble());
      inOrder.verify(graphics).drawString(any(String.class), anyInt(), anyInt());

    }
  }

  @Test
  public void testDrawRectangleDataSimple() throws Exception {
    Graphics2D graphics = createGraphicsMock();
    ReactionConverter rc = new ReactionConverter(colorExtractor);

    PolylineData line = new PolylineData(new Point2D.Double(10, 20),
        new Point2D.Double(10 + ReactionConverter.RECT_SIZE, 20));

    rc.drawRectangleData(line, null, ReactionRect.RECT_EMPTY, graphics);

    Mockito.verify(graphics, times(1)).fill(any(Shape.class));
    Mockito.verify(graphics, times(1)).draw(any(Shape.class));
  }

  @Test
  public void testDrawRectangleDataWithLineAround() throws Exception {
    Graphics2D graphics = createGraphicsMock();
    ReactionConverter rc = new ReactionConverter(colorExtractor);

    PolylineData line = new PolylineData(new Point2D.Double(10, 20),
        new Point2D.Double(10 + ReactionConverter.RECT_SIZE + 100, 20));

    rc.drawRectangleData(line, null, ReactionRect.RECT_EMPTY, graphics);

    Mockito.verify(graphics, times(1)).fill(any(Shape.class));
    Mockito.verify(graphics, times(3)).draw(any(Shape.class));
  }

  @Test
  public void testDrawRectangleDataWithLineDifferentWidth() throws Exception {
    Graphics2D graphics = createGraphicsMock();
    ReactionConverter rc = new ReactionConverter(colorExtractor);

    PolylineData line = new PolylineData(new Point2D.Double(10, 20),
        new Point2D.Double(10 + ReactionConverter.RECT_SIZE + 100, 20));

    rc.drawRectangleData(line, null, null, graphics);

    Mockito.verify(graphics, times(7)).setStroke(any(Stroke.class));
  }

  @Test
  public void testDrawRectangleDataWithOneSidePolyLineAround() throws Exception {
    Graphics2D graphics = createGraphicsMock();
    ReactionConverter rc = new ReactionConverter(colorExtractor);

    PolylineData line = new PolylineData(
        Arrays.asList(new Point2D.Double(10, 20), new Point2D.Double(100, 20), new Point2D.Double(200, 20)));

    rc.drawRectangleData(line, null, ReactionRect.RECT_EMPTY, graphics);

    Mockito.verify(graphics, times(1)).fill(any(Shape.class));
    Mockito.verify(graphics, times(4)).draw(any(Shape.class));
  }

  @Test
  public void testDrawRectangleDataWithPolyLineAround() throws Exception {
    Graphics2D graphics = createGraphicsMock();
    ReactionConverter rc = new ReactionConverter(colorExtractor);

    PolylineData line = new PolylineData(Arrays.asList(
        new Point2D.Double(10, 20),
        new Point2D.Double(100, 20),
        new Point2D.Double(100, 200),
        new Point2D.Double(200, 200),
        new Point2D.Double(200, 300),
        new Point2D.Double(300, 300),
        new Point2D.Double(300, 400)));

    rc.drawRectangleData(line, null, ReactionRect.RECT_EMPTY, graphics);

    Mockito.verify(graphics, times(1)).fill(any(Shape.class));
    Mockito.verify(graphics, times(5)).draw(any(Shape.class));
  }

  @Test
  public void testDrawSbgnReaction() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();

    Font f = graphics.getFont();
    ReactionConverter rc = new ReactionConverter(colorExtractor);

    Reaction reaction = createComplexReaction(5.0);
    rc.draw(reaction, graphics, new ConverterParams().sbgnFormat(true));

    assertEquals(f, graphics.getFont());

    // ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    // ImageIO.write(bi, "PNG", outputStream);
    // byte[] output1 = outputStream.toByteArray();

    // FileUtils.writeByteArrayToFile(new File("tmp.png"), output1);
    //
    // Desktop.getDesktop().open(new File("tmp.png"));

  }

}
