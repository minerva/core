package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.image.BufferedImage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.GraphicsTestFunctions;
import lcsb.mapviewer.model.map.species.Protein;

public class ProteinConverterTest extends GraphicsTestFunctions {

  private ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE, Color.WHITE);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDrawHomodimer() throws Exception {
    int size = 200;
    BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = Mockito.spy(bi.createGraphics());
    ProteinConverter rc = new ProteinConverter(colorExtractor);

    Protein protein = createProtein();
    protein.setHomodimer(4);
    rc.drawImpl(protein, graphics, new ConverterParams().sbgnFormat(false));
    
    Mockito.verify(graphics, times(4)).draw(any(Shape.class));
  }
}
