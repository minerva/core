package lcsb.mapviewer.converter.graphics;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.bioentity.BioEntityConverterImpl;
import lcsb.mapviewer.model.map.compartment.BottomSquareCompartment;
import lcsb.mapviewer.model.map.compartment.LeftSquareCompartment;
import lcsb.mapviewer.model.map.compartment.RightSquareCompartment;
import lcsb.mapviewer.model.map.compartment.TopSquareCompartment;

public class ConverterTest extends GraphicsTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void test() {
    ColorExtractor colorExtractor = new ColorExtractor(Color.BLUE, Color.RED, Color.BLUE, Color.WHITE);
    new BioEntityConverterImpl(new BottomSquareCompartment("id1"), colorExtractor);
    new BioEntityConverterImpl(new TopSquareCompartment("id2"), colorExtractor);
    new BioEntityConverterImpl(new LeftSquareCompartment("id3"), colorExtractor);
    new BioEntityConverterImpl(new RightSquareCompartment("id4"), colorExtractor);
  }

  @Test
  @Ignore("it's just code for generating scale")
  public void testX() throws IOException {
    BufferedImage tmpBI = new BufferedImage(900, 100, BufferedImage.TYPE_INT_ARGB);
    Graphics2D tmpGraphics = tmpBI.createGraphics();
    int startX = 10;
    int startY = 15;
    int stepSize = 40;
    int height = 25;
    tmpGraphics.setColor(Color.BLACK);
    for (int i = 0; i < 21; i++) {
      tmpGraphics.drawLine(startX + i * stepSize, height + startY, startX + i * stepSize, height * 2);
      String str = "" + ((double) (i - 10)) / ((double) (10));
      tmpGraphics.drawString(str, startX + i * stepSize, height * 2 + startY);
    }
    for (int i = 0; i < 10 * stepSize; i++) {
      double ratio = ((double) i) / ((double) (10 * stepSize));
      tmpGraphics.setBackground(getColor(ratio, Color.BLUE, Color.WHITE));
      tmpGraphics.setColor(getColor(ratio, Color.BLUE, Color.WHITE));
      tmpGraphics.drawRect(startX + i, startY, 1, height);
    }
    for (int i = 0; i < 10 * stepSize; i++) {
      double ratio = ((double) i) / ((double) (10 * stepSize));
      tmpGraphics.setBackground(getColor(ratio, Color.WHITE, Color.RED));
      tmpGraphics.setColor(getColor(ratio, Color.WHITE, Color.RED));
      tmpGraphics.drawRect(10 * stepSize + startX + i, startY, 1, height);
    }
    ImageIO.write(tmpBI, "PNG", new File("tmp.png"));
    Desktop.getDesktop().open(new File("tmp.png"));
  }

  private Color getColor(final double d, final Color startColor, final Color endColor) {

    return new Color((int) (startColor.getRed() + d * (endColor.getRed() - startColor.getRed())),
        (int) (startColor.getGreen() + d * (endColor.getGreen() - startColor.getGreen())),
        (int) (startColor.getBlue() + d * (endColor.getBlue() - startColor.getBlue())));
  }

}
