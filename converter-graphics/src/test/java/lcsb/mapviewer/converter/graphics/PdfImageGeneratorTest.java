package lcsb.mapviewer.converter.graphics;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.commands.CreateHierarchyCommand;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Complex;

public class PdfImageGeneratorTest extends GraphicsTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSaveToFile() throws Exception {
    String filename = "tmp.pdf";

    File f = new File(filename);
    if (f.exists()) {
      f.delete();
    }

    assertFalse(f.exists());

    Model model = createCompartmentModel();
    Layer layer = model.getLayers().iterator().next();

    layer.addLayerText(new LayerText(new Rectangle2D.Double(10, 10, 500, 500), "X asd"));
    layer.addLayerText(new LayerText(new Rectangle2D.Double(20, 20, 200, 200), "Y qwe ret"));
    layer.addLayerText(new LayerText(new Rectangle2D.Double(100, 100, 199, 220), "Z dgf fsd aaewq ret"));

    for (final LayerText text : layer.getTexts()) {
      text.setZ(0);
    }

    new CreateHierarchyCommand(model, 1, 8).execute();
    model.getCompartments().get(1).setTransparencyLevel("2");
    model.getCompartments().get(0).setTransparencyLevel("2");
    model.getCompartments().get(3).setTransparencyLevel("2");

    PdfImageGenerator pig = createAbstractImageGeneratorMock(model, null);

    pig.saveToFile("tmp.pdf");

    assertTrue(f.exists());
    f.delete();
    assertFalse(f.exists());
  }

  @Test(expected = IOException.class)
  public void testSaveWithoutEnoughSpace() throws Exception {
    String filename = "tmp.pdf";

    File f = new File(filename);
    if (f.exists()) {
      f.delete();
    }

    assertFalse(f.exists());

    Model model = createCompartmentModel();
    PdfImageGenerator pig = createAbstractImageGeneratorMock(model, null);

    long originalValue = Configuration.getRequiredFreeSpaceInBytes();
    try {
      Configuration.setRequiredFreeSpaceInBytes(Long.MAX_VALUE);

      pig.saveToFile("tmp.pdf");
    } finally {
      Configuration.setRequiredFreeSpaceInBytes(originalValue);

    }
  }

  private PdfImageGenerator createAbstractImageGeneratorMock(final Model model, final Graphics2D graphics) throws DrawingException {
    PdfImageGenerator pig = new PdfImageGenerator(
        new Params().level(2).scale(4).width(600).height(600).model(model).nested(true));
    if (graphics != null) {
      pig.setGraphics(graphics);
    }
    return Mockito.spy(pig);
  }

  private Model createCompartmentModel() {
    Model model = new ModelFullIndexed(null);
    model.setWidth(526);
    model.setHeight(346);

    LayerText text = new LayerText();
    text.setX(256.0);
    text.setY(79.0);
    text.setZ(0);
    text.setWidth(233.0);
    text.setHeight(188.0);
    text.setNotes("asd as");
    text.setColor(Color.BLACK);
    Layer layer = new Layer();
    layer.addLayerText(text);
    layer.setVisible(true);
    model.addLayer(layer);

    layer.addLayerLine(new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(100, 100)));

    Complex alias = createComplex();
    model.addElement(alias);

    return model;
  }

  @Test
  public void testDrawPathwayWithoutNesting() throws Exception {
    Graphics2D graphics = createGraphicsMock();

    Model model = createCompartmentModel();

    PathwayCompartment pathway = createPathway();
    pathway.setWidth(100);
    pathway.setHeight(100);
    model.addElement(pathway);
    AbstractImageGenerator gen = createAbstractImageGeneratorMock(model, graphics);
    gen.getParams().nested(false);
    gen.drawCompartment(pathway);

    // 3 times for proteins and 4 times for reaction
    verify(graphics, times(1)).draw(any());
  }

}
