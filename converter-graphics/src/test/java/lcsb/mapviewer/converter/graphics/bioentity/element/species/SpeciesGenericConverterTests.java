package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.atLeastOnce;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.GraphicsTestFunctions;
import lcsb.mapviewer.converter.graphics.bioentity.BioEntityConverter;
import lcsb.mapviewer.converter.graphics.bioentity.BioEntityConverterImpl;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.IonChannelProtein;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.ReceptorProtein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.TruncatedProtein;

@RunWith(Parameterized.class)
public class SpeciesGenericConverterTests extends GraphicsTestFunctions {

  private ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE, Color.WHITE);

  private Element species;

  public SpeciesGenericConverterTests(final Species species) {
    this.species = species;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {

    Collection<Object[]> data = new ArrayList<Object[]>();
    data.add(new Object[] { assignData(new AntisenseRna("id")) });
    data.add(new Object[] { assignData(new Complex("id")) });
    data.add(new Object[] { assignData(new Degraded("id")) });
    data.add(new Object[] { assignData(new Drug("id")) });
    data.add(new Object[] { assignData(new Gene("id")) });
    data.add(new Object[] { assignData(new Ion("id")) });
    data.add(new Object[] { assignData(new Phenotype("id")) });
    data.add(new Object[] { assignData(new GenericProtein("id")) });
    data.add(new Object[] { assignData(new IonChannelProtein("id")) });
    data.add(new Object[] { assignData(new ReceptorProtein("id")) });
    data.add(new Object[] { assignData(new TruncatedProtein("id")) });
    data.add(new Object[] { assignData(new Rna("id")) });
    data.add(new Object[] { assignData(new SimpleMolecule("id")) });
    return data;
  }

  private static Element assignData(final Element element) {
    assignCoordinates(10, 10, 100, 200, element);
    element.setFillColor(Color.BLUE);
    element.setBorderColor(Color.YELLOW);
    element.setFontColor(Color.GREEN);
    element.setName("xyz");
    element.setTransparencyLevel("100");

    return element;
  }

  @Test
  public void testDrawAndColorUsage() throws Exception {
    BioEntityConverter<BioEntity> converter = new BioEntityConverterImpl(species, colorExtractor);
    int size = 600;
    Model model = new ModelFullIndexed(null);

    BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = Mockito.spy(bi.createGraphics());

    model.addElement(species);

    graphics.setColor(Color.PINK);
    converter.draw(species, graphics, new ConverterParams().nested(true));

    assertEquals("Color was changed by the converter", Color.PINK, graphics.getColor());

    ArgumentCaptor<Color> argument = ArgumentCaptor.forClass(Color.class);
    Mockito.verify(graphics, atLeastOnce()).setColor(argument.capture());
    List<Color> values = removeAlpha(argument.getAllValues());

    assertTrue("Fill colour wasn't used", values.contains(Color.BLUE));
    assertTrue("Border colour wasn't used", values.contains(Color.YELLOW));
    assertTrue("Font colour wasn't used", values.contains(Color.GREEN));

  }
}
