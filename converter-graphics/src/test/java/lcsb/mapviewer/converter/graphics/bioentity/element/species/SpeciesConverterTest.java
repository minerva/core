package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.GraphicsTestFunctions;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.overlay.GenericDataOverlayEntry;

public class SpeciesConverterTest extends GraphicsTestFunctions {

  private ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE, Color.WHITE);

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDrawAliasWithDataOverlays() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();
    ProteinConverter rc = new ProteinConverter(colorExtractor);

    GenericProtein alias = createProtein();
    rc.draw(alias, graphics, new ConverterParams());

    int val = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());

    GenericProtein alias2 = createProtein();

    bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    graphics = bi.createGraphics();

    DataOverlayEntry schema = new GenericDataOverlayEntry();
    schema.setColor(Color.RED);
    List<DataOverlayEntry> schemas = new ArrayList<>();
    schemas.add(schema);

    rc.draw(alias2, graphics, new ConverterParams(), Arrays.asList(schemas));

    int val2 = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());

    assertTrue(val != val2);
  }

  @Test
  public void testDrawAfterDrawingReactionWithDataOverlays() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();
    ProteinConverter rc = new ProteinConverter(colorExtractor);

    GenericProtein alias = createProtein();
    rc.draw(alias, graphics, new ConverterParams());

    int val = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());

    GenericProtein alias2 = createProtein();

    bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    graphics = bi.createGraphics();

    DataOverlayEntry schema = new GenericDataOverlayEntry();
    schema.setColor(Color.RED);
    List<DataOverlayEntry> schemas = new ArrayList<>();
    schemas.add(schema);

    rc.draw(alias2, graphics, new ConverterParams(), Arrays.asList(schemas));

    int val2 = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());

    bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    graphics = bi.createGraphics();

    rc.draw(alias2, graphics, new ConverterParams(), new ArrayList<>());

    int val3 = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());

    assertTrue(val != val2);
    assertEquals(val, val3);
  }

  @Test
  public void testDrawReactionWithFewDataOverlays() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();
    ProteinConverter rc = new ProteinConverter(colorExtractor);

    GenericProtein alias = createProtein();
    rc.draw(alias, graphics, new ConverterParams());

    int val = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());

    GenericProtein alias2 = createProtein();

    bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    graphics = bi.createGraphics();

    DataOverlayEntry entry1 = new GenericDataOverlayEntry();
    entry1.setColor(Color.RED);

    DataOverlayEntry entry2 = new GenericDataOverlayEntry();
    entry2.setColor(Color.BLUE);

    rc.draw(alias2, graphics, new ConverterParams(),
        Arrays.asList(Collections.singletonList(entry1),
            Collections.singletonList(null),
            Collections.singletonList(entry2)));

    int val2 = bi.getRGB((int) (alias.getX() + alias.getWidth() / 4), (int) alias.getCenterY());
    int val3 = bi.getRGB((int) (alias.getCenterX()), (int) alias.getCenterY());
    int val4 = bi.getRGB((int) (alias.getX() + 3 * alias.getWidth() / 4), (int) alias.getCenterY());

    assertTrue(val != val2);
    assertEquals(val, val3);
    assertTrue(val != val4);
  }

  @Test
  public void testDrawReactionWithFewDataOverlayEntries() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();
    ProteinConverter rc = new ProteinConverter(colorExtractor);

    GenericProtein alias = createProtein();
    rc.draw(alias, graphics, new ConverterParams());

    int val = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());

    GenericProtein alias2 = createProtein();

    bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    graphics = bi.createGraphics();

    DataOverlayEntry schema = new GenericDataOverlayEntry();
    schema.setColor(Color.RED);
    List<DataOverlayEntry> schemas = new ArrayList<>();
    schemas.add(schema);
    schemas.add(null);
    schema = new GenericDataOverlayEntry();
    schema.setColor(Color.BLUE);
    schemas.add(schema);

    rc.draw(alias2, graphics, new ConverterParams(), Arrays.asList(schemas));

    int val2 = bi.getRGB((int) alias.getCenterX(), (int) (alias.getCenterY() - alias.getHeight() / 4));
    int val3 = bi.getRGB((int) alias.getCenterX(), (int) alias.getCenterY());
    int val4 = bi.getRGB((int) alias.getCenterX(), (int) (alias.getCenterY() + alias.getHeight() / 4));

    assertTrue(val != val2);
    assertEquals(val, val3);
    assertTrue(val != val4);
  }

  @Test
  public void testDrawText() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = Mockito.spy(bi.createGraphics());
    graphics.setColor(Color.YELLOW);

    GenericProtein protein = createProtein();
    protein.setFontColor(Color.PINK);

    ProteinConverter converter = Mockito.spy(new ProteinConverter(colorExtractor));
    converter.drawText(protein, graphics, new ConverterParams());

    assertEquals(Color.YELLOW, graphics.getColor());

    ArgumentCaptor<Color> argument = ArgumentCaptor.forClass(Color.class);
    Mockito.verify(graphics, atLeastOnce()).setColor(argument.capture());
    List<Color> values = removeAlpha(argument.getAllValues());

    assertTrue("Font colour wasn't used", values.contains(Color.PINK));
  }

  @Test
  public void testDrawAliasWithGlyph() throws Exception {
    BufferedImage bi = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = Mockito.spy(bi.createGraphics());
    ProteinConverter rc = new ProteinConverter(colorExtractor);

    GenericProtein alias = createProtein();
    Glyph glyph = new Glyph();
    UploadedFileEntry file = new UploadedFileEntry();
    file.setOriginalFileName("test");
    byte[] fileContent = Files.readAllBytes(new File("testFiles/glyph.png").toPath());

    file.setFileContent(fileContent);
    glyph.setFile(file);
    alias.setGlyph(glyph);
    rc.draw(alias, graphics, new ConverterParams());

    verify(graphics, times(1)).drawImage(any(Image.class), anyInt(), anyInt(), nullable(ImageObserver.class));
  }

  protected GenericProtein createProtein() {
    GenericProtein protein = super.createProtein();
    protein.setName("");
    return protein;
  }

}
