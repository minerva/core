package lcsb.mapviewer.converter.graphics.bioentity.element.species;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.util.Arrays;

import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.converter.graphics.ConverterParams;
import lcsb.mapviewer.converter.graphics.GraphicsTestFunctions;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.field.StructuralState;

public class ProteinSbgnConverterTest extends GraphicsTestFunctions {

  private ColorExtractor colorExtractor = new ColorExtractor(Color.RED, Color.GREEN, Color.BLUE, Color.WHITE);

  @Test
  public void testDrawStructuralState() throws Exception {
    int size = 200;
    BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = Mockito.spy(bi.createGraphics());
    ProteinConverter rc = new ProteinSbgnConverter(colorExtractor);

    Protein protein = createProtein();
    StructuralState state = createStructuralState(protein);
    protein.addStructuralState(state);
    rc.drawImpl(protein, graphics, new ConverterParams().sbgnFormat(false));

    Mockito.verify(graphics, atLeastOnce()).draw(any(RoundRectangle2D.class));
    Mockito.verify(graphics, times(0)).draw(any(Ellipse2D.class));
  }

  @Test
  public void testDrawHomodimerInformation() throws Exception {
    int size = 200;
    BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = Mockito.spy(bi.createGraphics());
    ProteinConverter rc = new ProteinSbgnConverter(colorExtractor);

    Protein protein = createProtein();
    protein.setHomodimer(22);
    protein.setStatePrefix("bla");
    protein.setStateLabel("xxx");
    rc.drawImpl(protein, graphics, new ConverterParams().sbgnFormat(true));

    Mockito.verify(graphics, times(2)).draw(any(Rectangle2D.class));
  }

  @Test
  public void testDrawUnitOfInformation() throws Exception {
    int size = 200;
    BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = Mockito.spy(bi.createGraphics());
    ProteinConverter rc = new ProteinSbgnConverter(colorExtractor);

    Protein protein = createProtein();
    protein.setHomodimer(22);
    protein.setStatePrefix("bla");
    protein.setStateLabel("xxx");
    graphics.setColor(Color.CYAN);
    rc.drawUnitOfInformation(Arrays.asList("test"), protein, graphics);

    Mockito.verify(graphics, times(1)).drawString(anyString(), anyInt(), anyInt());
    Mockito.verify(graphics, times(1)).setColor(protein.getBorderColor());
    Mockito.verify(graphics, times(1)).setColor(protein.getFontColor());
    assertEquals(Color.CYAN, graphics.getColor());
  }

  @Test
  public void testDrawHomodimerInformationTogetherWithCardinality() throws Exception {
    int size = 200;
    BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = Mockito.spy(bi.createGraphics());
    ProteinConverter rc = new ProteinSbgnConverter(colorExtractor);

    Protein protein = createProtein();
    protein.setHomodimer(4);
    protein.setStatePrefix("N");
    protein.setStateLabel("4");
    rc.drawImpl(protein, graphics, new ConverterParams().sbgnFormat(true));

    Mockito.verify(graphics, times(2)).drawString(anyString(), anyInt(), anyInt());
    Mockito.verify(graphics, times(1)).draw(any(Rectangle2D.class));
  }

  @Test
  public void testDrawHomodimerInformationTogetherWithDifferentCardinality() throws Exception {
    int size = 200;
    BufferedImage bi = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = Mockito.spy(bi.createGraphics());
    ProteinConverter rc = new ProteinSbgnConverter(colorExtractor);

    Protein protein = createProtein();
    protein.setHomodimer(4);
    protein.setStatePrefix("N");
    protein.setStateLabel("14");
    rc.drawImpl(protein, graphics, new ConverterParams().sbgnFormat(true));

    Mockito.verify(graphics, times(3)).drawString(anyString(), anyInt(), anyInt());
    Mockito.verify(graphics, times(2)).draw(any(Rectangle2D.class));
  }

}
