package lcsb.mapviewer.converter.graphics;

import lcsb.mapviewer.common.tests.TestUtils;
import lcsb.mapviewer.common.tests.UnitTestFailedWatcher;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;
import org.mockito.Mockito;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

public abstract class GraphicsTestFunctions extends TestUtils {

  protected static Logger logger = LogManager.getLogger();

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();

  private static int elementCounter = 1;

  protected Graphics2D createGraphicsMock() {
    Graphics2D graphics = Mockito.mock(Graphics2D.class);
    FontMetrics fontMetrics = Mockito.mock(FontMetrics.class);
    when(fontMetrics.getStringBounds(nullable(String.class), nullable(Graphics.class)))
        .thenReturn(new Rectangle2D.Double());
    when(graphics.getFontMetrics()).thenReturn(fontMetrics);
    return graphics;
  }

  protected Reaction createReaction(final GenericProtein modifierElement, final GenericProtein reactantElement,
                                    final GenericProtein productElement) {
    Reaction reaction = new Reaction("r" + elementCounter++);

    Point2D center = new Point2D.Double((reactantElement.getCenterX() + productElement.getCenterX()) / 2,
        (reactantElement.getCenterY() + productElement.getCenterY()) / 2);

    Modifier modifier = new Catalysis(modifierElement);
    modifier.setLine(new PolylineData(modifierElement.getCenter(), center));
    modifier.getLine().setWidth(1.0);

    Reactant reactant = new Reactant(reactantElement);
    reactant.setLine(new PolylineData(reactantElement.getCenter(), center));
    reactant.getLine().setWidth(1.0);
    Product product = new Product(productElement);
    product.setLine(new PolylineData(productElement.getCenter(), center));
    product.getLine().setWidth(1.0);
    reaction.addModifier(modifier);
    reaction.addProduct(product);
    reaction.addReactant(reactant);
    reaction.setLine(new PolylineData(center, center));
    reaction.setZ(elementCounter);
    return reaction;
  }

  protected GenericProtein createProtein() {
    GenericProtein result = new GenericProtein("s" + elementCounter++);
    result.setFillColor(Color.YELLOW);
    result.setBorderColor(Color.BLUE);
    result.setFontColor(Color.DARK_GRAY);
    assignCoordinates(10, 10, 40, 40, result);
    return result;
  }

  protected Complex createComplex() {
    Complex complex = new Complex("c" + elementCounter++);
    complex.setName("a");
    complex.setX(300);
    complex.setY(90);
    complex.setWidth(100);
    complex.setHeight(50);
    complex.setZ(elementCounter);
    return complex;
  }

  protected LayerText createText() {
    LayerText layerText = new LayerText();
    layerText.setX(256.0);
    layerText.setY(79.0);
    layerText.setWidth(233.0);
    layerText.setHeight(188.0);
    layerText.setZ(0);
    layerText.setNotes("asd as");
    layerText.setColor(Color.BLACK);
    return layerText;
  }

  protected List<Color> removeAlpha(final List<Color> allValues) {
    List<Color> result = new ArrayList<>();
    for (final Color color : allValues) {
      result.add(new Color(color.getRGB()));
    }
    return result;
  }

  protected SimpleMolecule createSimpleMolecule() {
    SimpleMolecule protein = new SimpleMolecule("sm" + elementCounter++);
    assignCoordinates(10, 10, 100, 80, protein);
    return protein;
  }

  protected StructuralState createStructuralState(final Species species) {
    StructuralState state = new StructuralState();
    state.setSpecies(species);
    assignCoordinates(state);
    state.setName("state of: " + species.getName());
    return state;
  }

  private static void assignCoordinates(final ModificationResidue state) {
    if (state.getWidth() == 0) {
      state.setWidth(100);
    }
    if (state.getHeight() == 0) {
      state.setHeight(20);
    }
    if (state.getX() == null) {
      state.setX(state.getSpecies().getX());
    }
    if (state.getY() == null) {
      state.setY(state.getSpecies().getY());
    }
    state.setZ(elementCounter++);
    state.setFontSize(10);
    state.setNameX(state.getX());
    state.setNameY(state.getY());
    state.setNameWidth(state.getWidth());
    state.setNameHeight(state.getHeight());
  }

  protected static void assignCoordinates(final double x, final double y, final double width, final double height, final Element element) {
    element.setX(x);
    element.setY(y);
    element.setZ(elementCounter);
    element.setWidth(width);
    element.setHeight(height);
    element.setNameX(x);
    element.setNameY(y);
    element.setNameWidth(width);
    element.setNameHeight(height);
    element.setNameVerticalAlign(VerticalAlign.MIDDLE);
    element.setNameHorizontalAlign(HorizontalAlign.CENTER);
  }


  protected CodingRegion createCodingRegion() {
    CodingRegion codingRegion = new CodingRegion();
    codingRegion.setBorderColor(Color.black);
    codingRegion.setName("xyz");
    codingRegion.setPosition(new Point2D.Double(10, 10));
    codingRegion.setWidth(100);
    codingRegion.setHeight(20);

    assignCoordinates(codingRegion);

    return codingRegion;
  }

  protected SquareCompartment createSquareCompartment() {
    SquareCompartment compartment = new SquareCompartment("id");
    assignCoordinates(10, 10, 100, 200, compartment);

    return compartment;
  }

  protected PathwayCompartment createPathway() {
    PathwayCompartment pathway = new PathwayCompartment("id");
    assignCoordinates(10, 10, 100, 200, pathway);
    return pathway;
  }

  protected Residue createResidue() {
    Residue residue = new Residue();
    residue.setState(ModificationState.ACETYLATED);
    residue.setBorderColor(Color.BLACK);
    residue.setPosition(new Point2D.Double(10, 10));
    assignCoordinates(residue);
    return residue;
  }

  protected Residue createResidue(final Species species) {
    Residue state = new Residue();
    state.setZ(elementCounter++);
    state.setPosition(species.getCenter());
    state.setNameX(state.getX());
    state.setNameY(state.getY());
    state.setNameWidth(state.getWidth());
    state.setNameHeight(state.getHeight());
    state.setState(ModificationState.PHOSPHORYLATED);
    return state;
  }


  protected ModificationSite createModificationSite() {
    ModificationSite modificationSite = new ModificationSite();
    modificationSite.setBorderColor(Color.BLACK);
    modificationSite.setPosition(new Point2D.Double(10, 10));
    modificationSite.setState(ModificationState.PHOSPHORYLATED);
    assignCoordinates(modificationSite);
    return modificationSite;
  }

  protected static TranscriptionSite createTranscriptionSite() {
    TranscriptionSite transcriptionSite = new TranscriptionSite();
    transcriptionSite.setPosition(new Point2D.Double(10, 10));
    transcriptionSite.setWidth(100);
    transcriptionSite.setHeight(20);
    transcriptionSite.setActive(true);
    transcriptionSite.setDirection("LEFT");
    transcriptionSite.setBorderColor(Color.GREEN);
    transcriptionSite.setName("x");
    assignCoordinates(transcriptionSite);
    return transcriptionSite;
  }

}
