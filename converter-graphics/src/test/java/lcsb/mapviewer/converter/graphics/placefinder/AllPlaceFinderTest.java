package lcsb.mapviewer.converter.graphics.placefinder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.Arrays;

import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class AllPlaceFinderTest {
  private int id = 10;

  @Test
  public void oneEmptyCompartments() throws InvalidXmlSchemaException, IOException {
    String actual = null;
    Model model = new ModelFullIndexed(null);
    model.addElement(createCompartmentAlias(94.0, 70.0, 355.0, 173.0));

    for (final Compartment alias : model.getCompartments()) {
      PlaceFinder pf = new PlaceFinder(model);
      actual = pf.getRetangle(alias, Integer.valueOf(alias.getVisibilityLevel())).toString();
    }
    String expected = "";
    Rectangle2D result = model.getCompartments().get(0).getBorder();
    expected = result.toString();
    assertEquals(expected, actual);
  }

  @Test
  public void twoSeparatedCompartments() throws InvalidXmlSchemaException, IOException {
    String actual = null;
    Model model = new ModelFullIndexed(null);
    model.addElement(createCompartmentAlias(70.0, 81.0, 229.0, 213.0));
    model.addElement(createCompartmentAlias(332.0, 66.0, 188.0, 255.0));

    String expected = "";
    for (final Compartment alias : model.getCompartments()) {
      PlaceFinder pf = new PlaceFinder(model);
      actual = pf.getRetangle(alias, Integer.valueOf(alias.getVisibilityLevel())).toString();
      Rectangle2D result = alias.getBorder();
      expected = result.toString();
      assertEquals(expected, actual);
    }
  }

  @Test
  public void twoIntersectingCompartments() throws InvalidXmlSchemaException, IOException {
    Model model = new ModelFullIndexed(null);
    Compartment alias = createCompartmentAlias(87.0, 56.0, 235.0, 219.0);
    model.addElement(alias);
    Compartment alias2 = createCompartmentAlias(227.0, 17.0, 290.0, 317.0);
    model.addElement(alias2);

    PlaceFinder pf = new PlaceFinder(model);
    Rectangle2D rect = pf.getRetangle(alias2, Integer.valueOf(alias.getVisibilityLevel()));
    double actual = rect.getWidth() * rect.getHeight();
    double expected = alias2.getWidth() * alias2.getHeight();
    assertTrue(actual < expected);
  }

  @Test
  public void oneCompartmentCompletlyOverlapSecondOne() throws InvalidXmlSchemaException, IOException {
    Model model = new ModelFullIndexed(null);
    model.addElement(createCompartmentAlias(45.0, 38.0, 174.0, 169.0));
    model.addElement(createCompartmentAlias(118.0, 85.0, 60.0, 68.0));
    double actual = 0.0;
    for (final Compartment alias : model.getCompartments()) {
      PlaceFinder pf = new PlaceFinder(model);
      Rectangle2D rect = pf.getRetangle(alias, Integer.valueOf(alias.getVisibilityLevel()));
      actual = rect.getWidth() * rect.getHeight();
      assertTrue(actual > 0);
    }
  }

  @Test
  public void perfectOverlapingOfThreeCompartments() throws InvalidXmlSchemaException, IOException {
    Model model = new ModelFullIndexed(null);
    model.addElement(createCompartmentAlias(226.0, 70.0, 138.0, 112.0));
    model.addElement(createCompartmentAlias(163.0, 70.0, 138.0, 112.0));
    model.addElement(createCompartmentAlias(88.0, 70.0, 138.0, 112.0));
    double actual = 0.0;
    Rectangle2D result = null;
    for (final Compartment alias : model.getCompartments()) {
      PlaceFinder pf = new PlaceFinder(model);
      Rectangle2D rect = pf.getRetangle(alias, Integer.valueOf(alias.getVisibilityLevel()));
      actual += rect.getWidth() * rect.getHeight();
    }
    result = model.getCompartments().get(0).getBorder();
    assertEquals(result.getWidth() * result.getHeight(), actual, 0.01);
  }

  boolean equals(final boolean[][] a, final boolean[][] b) {
    int ax = a.length;
    int bx = b.length;
    // logger.debug("Length of second table is " + bx);
    boolean tru = false;
    if (ax == bx) {
      tru = true;
      for (int i = 0; i < ax; i++) {
        if (!Arrays.equals(a[i], b[i])) {
          tru = false;
        }
      }
    }
    return tru;
  }

  private Compartment createCompartmentAlias(final double x, final double y, final double width, final double height) {
    Compartment result = new SquareCompartment("" + id++);
    result.setX(x);
    result.setY(y);
    result.setWidth(width);
    result.setHeight(height);
    result.setTransparencyLevel("10");
    result.setVisibilityLevel("9");

    return result;
  }

  @Test
  public void problematic() throws InvalidXmlSchemaException, IOException {
    Model model = new ModelFullIndexed(null);
    model.addElement(createCompartmentAlias(4521.0, 0.0, 10000.0, 6000.0));
    model.addElement(createCompartmentAlias(4828.0, 0.0, 5172.0, 6000.0));
    double actual = 0.0;
    for (final Compartment alias : model.getCompartments()) {
      PlaceFinder pf = new PlaceFinder(model);
      Rectangle2D rect = pf.getRetangle(alias, Integer.valueOf(alias.getVisibilityLevel()));
      actual = rect.getWidth() * rect.getHeight();
      assertTrue(actual > 0);
    }
  }

}
