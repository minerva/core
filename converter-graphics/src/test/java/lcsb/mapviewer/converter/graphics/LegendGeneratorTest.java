package lcsb.mapviewer.converter.graphics;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

import java.awt.Color;
import java.io.File;
import java.io.IOException;

import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.commands.ColorExtractor;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.overlay.GeneVariant;
import lcsb.mapviewer.model.overlay.GeneVariantDataOverlayEntry;
import lcsb.mapviewer.model.overlay.GenericDataOverlayEntry;

public class LegendGeneratorTest extends GraphicsTestFunctions {

  private LegendGenerator generator = Mockito.spy(new LegendGenerator());
  private ColorExtractor extractor = new ColorExtractor(Color.RED, Color.BLUE, Color.GREEN, Color.WHITE);

  @Test
  public void testGenericWithValue() throws IOException {
    DataOverlay overlay = new DataOverlay();
    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.setValue(0.5);
    overlay.addEntry(entry);
    generator.savetToPng(overlay, extractor, "tmp.png");
    File file = new File("tmp.png");
    assertTrue(file.exists());
    Mockito.verify(generator, times(1)).generateGenericLegendByValue(any(), any(), any());
    // Desktop.getDesktop().open(file);
    file.delete();
  }

  @Test
  public void testGenericWithSimple() throws Exception {
    DataOverlay overlay = new DataOverlay();
    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.setName("X");
    overlay.addEntry(entry);
    generator.savetToPng(overlay, extractor, "tmp.png");
    File file = new File("tmp.png");
    assertTrue(file.exists());
    Mockito.verify(generator, times(1)).generateGenericLegendBySimple(any(), any(), any());
    // Desktop.getDesktop().open(file);
    file.delete();
  }

  @Test
  public void testGenericWithColor() throws IOException {
    DataOverlay overlay = new DataOverlay();
    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.setColor(Color.YELLOW);
    overlay.addEntry(entry);
    entry = new GenericDataOverlayEntry();
    entry.setColor(Color.PINK);
    overlay.addEntry(entry);
    generator.savetToPng(overlay, extractor, "tmp.png");
    File file = new File("tmp.png");
    assertTrue(file.exists());
    Mockito.verify(generator, times(1)).generateGenericLegendByColor(any(), any(), any());
    // Desktop.getDesktop().open(file);
    file.delete();
  }

  @Test
  public void testGenericWithNullColor() throws IOException {
    DataOverlay overlay = new DataOverlay();
    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.setColor(Color.YELLOW);
    overlay.addEntry(entry);
    entry = new GenericDataOverlayEntry();
    entry.setColor(null);
    entry.setElementId("re1");
    overlay.addEntry(entry);
    generator.savetToPng(overlay, extractor, "tmp.png");
    File file = new File("tmp.png");
    assertTrue(file.exists());
    Mockito.verify(generator, times(1)).generateGenericLegendByColor(any(), any(), any());
    // Desktop.getDesktop().open(file);
    file.delete();
  }

  @Test
  public void testGenetic() throws IOException {
    DataOverlay overlay = new DataOverlay();
    GeneVariantDataOverlayEntry entry = new GeneVariantDataOverlayEntry();
    entry.setColor(Color.BLUE);
    entry.addGeneVariant(new GeneVariant());
    entry.addGeneVariant(new GeneVariant());
    entry.addGeneVariant(new GeneVariant());
    entry.addGeneVariant(new GeneVariant());
    overlay.addEntry(entry);
    for (int i = 4; i <= 5; i++) {
      entry = new GeneVariantDataOverlayEntry();
      for (int j = 0; j < i; j++) {
        entry.addGeneVariant(new GeneVariant());
      }
      entry.setColor(new Color(i, 255, 0));
      overlay.addEntry(entry);
    }
    generator.savetToPng(overlay, extractor, "tmp.png");
    File file = new File("tmp.png");
    assertTrue(file.exists());
    // Desktop.getDesktop().open(file);
    file.delete();
    Mockito.verify(generator, times(1)).generateGeneticLegend(any(), any(), any());

  }

  @Test
  public void testMix() throws IOException {
    DataOverlay overlay = new DataOverlay();
    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.setColor(Color.YELLOW);
    overlay.addEntry(entry);
    GeneVariantDataOverlayEntry entry2 = new GeneVariantDataOverlayEntry();
    entry2.addGeneVariant(new GeneVariant());
    overlay.addEntry(entry2);
    generator.savetToPng(overlay, extractor, "tmp.png");
    File file = new File("tmp.png");
    assertTrue(file.exists());
    Mockito.verify(generator, times(1)).generateMessageLegend(any(), any());
    file.delete();
  }

  @Test
  public void testUnknownEntryType() throws IOException {
    DataOverlay overlay = new DataOverlay();
    overlay.addEntry(Mockito.mock(DataOverlayEntry.class));
    generator.savetToPng(overlay, extractor, "tmp.png");
    File file = new File("tmp.png");
    assertTrue(file.exists());
    Mockito.verify(generator, times(1)).generateMessageLegend(any(), any());
    file.delete();
  }

  @Test
  public void testEmptyOverlay() throws IOException {
    DataOverlay overlay = new DataOverlay();
    generator.savetToPng(overlay, extractor, "tmp.png");
    File file = new File("tmp.png");
    assertTrue(file.exists());
    Mockito.verify(generator, times(1)).generateMessageLegend(any(), any());
    // Desktop.getDesktop().open(file);
    file.delete();
  }

  @Test
  public void testGenerate() throws IOException {
    DataOverlay overlay = new DataOverlay();
    GenericDataOverlayEntry entry = new GenericDataOverlayEntry();
    entry.setValue(0.5);
    overlay.addEntry(entry);
    byte[] result = generator.generate(overlay, extractor);
    assertNotNull(result);
  }

}
