package lcsb.mapviewer.converter.graphics.geometry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.converter.graphics.GraphicsTestFunctions;

public class FontFinderTest extends GraphicsTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testFindFontSize1() throws Exception {
    String text = "Some text to write";
    int coordX = 20;
    int coordY = 40;
    int height = 20;

    BufferedImage bi = new BufferedImage((int) 400, (int) 400, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();
    graphics.fill(new Rectangle2D.Double(0, 0, 400, 400));

    Rectangle2D border = new Rectangle2D.Double(coordX, coordY, 100, height);

    double size = FontFinder.findMaxFontSize(13, Font.SANS_SERIF, graphics, border, text);

    assertTrue(size < 13);
  }

  @Test
  public void testFindFontSize2() throws Exception {
    String text = "Some text to write";
    int coordX = 20;
    int coordY = 40;
    int height = 60;

    BufferedImage bi = new BufferedImage((int) 400, (int) 400, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();
    graphics.fill(new Rectangle2D.Double(0, 0, 400, 400));

    Rectangle2D border = new Rectangle2D.Double(coordX, coordY, 100, height);

    double size = FontFinder.findMaxFontSize(20, Font.SANS_SERIF, graphics, border, text);

    graphics.setColor(Color.BLACK);
    graphics.draw(border);
    FontFinder.drawText((int) size, Font.SANS_SERIF, graphics, border, text);

    // different O compute it differently ...
    assertTrue(size > 18);
    assertTrue(size <= 20);
  }

  @Test
  public void testFindFontSize3() throws Exception {
    String text = "Some\ntext\n to write";
    int coordX = 20;
    int coordY = 40;
    int height = 60;

    BufferedImage bi = new BufferedImage((int) 400, (int) 400, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();
    graphics.fill(new Rectangle2D.Double(0, 0, 400, 400));

    Rectangle2D border = new Rectangle2D.Double(coordX, coordY, 100, height);

    double size = FontFinder.findMaxFontSize(20, Font.SANS_SERIF, graphics, border, text);

    graphics.setColor(Color.BLACK);
    graphics.draw(border);
    FontFinder.drawText((int) size, Font.SANS_SERIF, graphics, border, text);

    assertTrue(size < 20);
  }

  @Test
  public void testFindFontSize4() throws Exception {
    String text = "";
    int coordX = 20;
    int coordY = 40;
    int height = 60;

    BufferedImage bi = new BufferedImage((int) 400, (int) 400, BufferedImage.TYPE_INT_ARGB);
    Graphics2D graphics = bi.createGraphics();
    graphics.fill(new Rectangle2D.Double(0, 0, 400, 400));

    Rectangle2D border = new Rectangle2D.Double(coordX, coordY, 100, height);

    double size = FontFinder.findMaxFontSize(20, Font.SANS_SERIF, graphics, border, text);

    assertEquals(20, (int) size);
    size = FontFinder.findMaxFontSize(20, Font.SANS_SERIF, graphics, border, " ");

    assertEquals(20, (int) size);
  }

}
