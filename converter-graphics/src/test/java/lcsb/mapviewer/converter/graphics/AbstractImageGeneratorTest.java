package lcsb.mapviewer.converter.graphics;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;

public class AbstractImageGeneratorTest extends GraphicsTestFunctions {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDrawSimpleMap() throws Exception {
    Graphics2D graphics = createGraphicsMock();

    Model model = createSimpleModel();

    AbstractImageGenerator gen = createAbstractImageGeneratorMock(graphics, model);
    gen.draw();

    // 3 times for proteins and 4 times for reaction
    verify(graphics, times(7)).draw(any());
  }

  @Test
  public void testDrawSimpleMapWithNesting() throws Exception {
    Graphics2D graphics = createGraphicsMock();

    Model model = createSimpleModel();

    AbstractImageGenerator gen = createAbstractImageGeneratorMock(graphics, model);
    gen.setParams(new AbstractImageGenerator.Params().model(model).nested(true));
    gen.draw();

    // 3 times for proteins and 4 times for reaction
    verify(graphics, times(7)).draw(any());
  }

  @Test
  public void testDrawMapWithInvisibleLayerNesting() throws Exception {
    Graphics2D graphics = createGraphicsMock();

    Model model = createEmptyModel();
    Layer layer = new Layer();
    layer.setVisible(false);
    LayerRect rect = new LayerRect();
    rect.setX(10.0);
    rect.setX(10.0);
    rect.setWidth(20.);
    rect.setHeight(20.);
    layer.addLayerRect(rect);
    model.addLayer(layer);

    AbstractImageGenerator gen = createAbstractImageGeneratorMock(graphics, model);
    gen.setParams(new AbstractImageGenerator.Params().model(model).nested(true));
    gen.draw();

    // 3 times for proteins and 4 times for reaction
    verify(graphics, times(0)).draw(any());
  }

  @Test
  public void testDrawSimpleMapWithWhenNestingHidesElement() throws Exception {
    Graphics2D graphics = createGraphicsMock();

    Model model = createSimpleModel();
    model.getReactions().iterator().next().getModifiers().get(0).getElement().setVisibilityLevel(2);

    AbstractImageGenerator gen = createAbstractImageGeneratorMock(graphics, model);
    gen.setParams(new AbstractImageGenerator.Params().model(model).nested(true).level(0));
    gen.draw();

    // 2 times for proteins and 3 times for reaction
    verify(graphics, times(5)).draw(any());
  }

  private Model createSimpleModel() {
    Model model = createEmptyModel();

    GenericProtein protein1 = createProtein();
    model.addElement(protein1);

    GenericProtein protein2 = createProtein();
    protein2.setX(30);
    model.addElement(protein2);

    GenericProtein protein3 = createProtein();
    protein3.setX(40);
    model.addElement(protein3);

    Reaction reaction = createReaction(protein1, protein2, protein3);

    model.addReaction(reaction);

    return model;
  }

  private Model createEmptyModel() {
    Model model = new ModelFullIndexed(null);
    model.setWidth(100);
    model.setHeight(100);
    return model;
  }

  private AbstractImageGenerator createAbstractImageGeneratorMock(final Graphics2D graphics, final Model model,
      final boolean nested) throws Exception {
    AbstractImageGenerator result = Mockito.mock(AbstractImageGenerator.class, Mockito.CALLS_REAL_METHODS);
    result.setGraphics(graphics);
    result.setParams(new AbstractImageGenerator.Params().model(model).level(0).nested(nested));
    return result;

  }

  private AbstractImageGenerator createAbstractImageGeneratorMock(final Graphics2D graphics, final Model model) throws Exception {
    return createAbstractImageGeneratorMock(graphics, model, false);
  }

  @Test
  public void testDrawMapWithStructuralState() throws Exception {
    Graphics2D graphics = createGraphicsMock();

    Model model = createEmptyModel();
    Protein protein = createProtein();
    protein.addStructuralState(createStructuralState(protein));
    model.addElement(protein);

    AbstractImageGenerator gen = createAbstractImageGeneratorMock(graphics, model);
    gen.draw();

    Mockito.verify(graphics, atLeastOnce()).draw(any(Ellipse2D.class));
  }

  @Test
  public void testDrawMapWithStructuralStateOnHiddenSpecies() throws Exception {
    Graphics2D graphics = createGraphicsMock();

    Model model = createEmptyModel();
    Protein protein = createProtein();
    protein.addStructuralState(createStructuralState(protein));
    protein.setVisibilityLevel(Integer.MAX_VALUE);
    model.addElement(protein);

    AbstractImageGenerator gen = createAbstractImageGeneratorMock(graphics, model, true);
    gen.draw();

    Mockito.verify(graphics, times(0)).draw(any(Ellipse2D.class));
  }

  @Test
  public void testDrawMapWithModificationResidue() throws Exception {
    Graphics2D graphics = createGraphicsMock();

    Model model = createEmptyModel();
    Protein protein = createProtein();
    protein.addResidue(createResidue(protein));
    model.addElement(protein);

    AbstractImageGenerator gen = createAbstractImageGeneratorMock(graphics, model);
    gen.draw();

    Mockito.verify(graphics, atLeastOnce()).draw(any(Ellipse2D.class));
  }

  @Test
  public void testDrawMapWithModificationResidueOnHiddenSpecies() throws Exception {
    Graphics2D graphics = createGraphicsMock();

    Model model = createEmptyModel();
    Protein protein = createProtein();
    protein.addResidue(createResidue(protein));
    protein.setVisibilityLevel(Integer.MAX_VALUE);
    model.addElement(protein);

    AbstractImageGenerator gen = createAbstractImageGeneratorMock(graphics, model, true);
    gen.draw();

    Mockito.verify(graphics, times(0)).draw(any(Ellipse2D.class));
  }

}
