package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.species.Degraded;

public class DegradedTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CellDesignerDegraded());
  }

  @Test
  public void testConstructor1() {
    CellDesignerDegraded degraded = new CellDesignerDegraded(new CellDesignerSpecies<Degraded>());
    assertNotNull(degraded);
  }

  @Test
  public void testCopy() {
    CellDesignerDegraded degraded = new CellDesignerDegraded().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    CellDesignerDegraded degraded = Mockito.spy(CellDesignerDegraded.class);
    degraded.copy();
  }

}
