package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.geom.Point2D;
import java.lang.reflect.Constructor;
import java.util.Arrays;

import org.junit.Test;

import lcsb.mapviewer.model.graphics.PolylineData;

public class PolylineDataFactoryTest {

  @Test
  public void testPrivateCotnstructor() throws Exception {
    Constructor<?> constr = PolylineDataFactory.class.getDeclaredConstructor(new Class<?>[] {});
    constr.setAccessible(true);
    assertNotNull(constr.newInstance(new Object[] {}));
  }

  @Test
  public void testRemoveCollinearPoints2() throws Exception {
    PolylineData line = new PolylineData(Arrays.asList(new Point2D.Double(10, 20),
        new Point2D.Double(11, 30), new Point2D.Double(12, 40)));
    PolylineData modifiedLine = PolylineDataFactory.removeCollinearPoints(line);
    assertEquals(1, modifiedLine.getLines().size());
    assertEquals(modifiedLine.getStartPoint(), new Point2D.Double(10, 20));
    assertEquals(modifiedLine.getEndPoint(), new Point2D.Double(12, 40));
  }

  @Test
  public void testRemoveCollinearPoints() throws Exception {
    PolylineData line = new PolylineData(Arrays.asList(
        new Point2D.Double(10, 20),
        new Point2D.Double(10, 30),
        new Point2D.Double(10, 35),
        new Point2D.Double(10, 40)));
    PolylineData modifiedLine = PolylineDataFactory.removeCollinearPoints(line);
    assertEquals(1, modifiedLine.getLines().size());
    assertEquals(modifiedLine.getStartPoint(), new Point2D.Double(10, 20));
    assertEquals(modifiedLine.getEndPoint(), new Point2D.Double(10, 40));
  }

  @Test
  public void testRemoveCollinearPointsWithoutCollinearPoints() throws Exception {
    PolylineData line = new PolylineData(Arrays.asList(
        new Point2D.Double(10, 20),
        new Point2D.Double(10, 30),
        new Point2D.Double(20, 40),
        new Point2D.Double(10, 40)));
    PolylineData modifiedLine = PolylineDataFactory.removeCollinearPoints(line);
    assertEquals(3, modifiedLine.getLines().size());
  }

}
