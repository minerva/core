package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.species.Unknown;

public class UnknownTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CellDesignerUnknown());
  }

  @Test
  public void testConstructor1() {
    CellDesignerUnknown degraded = new CellDesignerUnknown(new CellDesignerSpecies<Unknown>());
    assertNotNull(degraded);
  }

  @Test
  public void testCopy() {
    CellDesignerUnknown degraded = new CellDesignerUnknown().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    CellDesignerUnknown unknown = Mockito.spy(CellDesignerUnknown.class);
    unknown.copy();
  }

}
