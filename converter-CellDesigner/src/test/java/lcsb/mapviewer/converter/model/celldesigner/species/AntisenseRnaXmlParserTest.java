package lcsb.mapviewer.converter.model.celldesigner.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerAntisenseRna;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.ModificationType;

public class AntisenseRnaXmlParserTest extends CellDesignerTestFunctions {
  protected Logger logger = LogManager.getLogger();

  private AntisenseRnaXmlParser antisenseRnaParser;
  private String testAntisenseRnaFile = "testFiles/xmlNodeTestExamples/antisense_rna.xml";
  private CellDesignerElementCollection elements;

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();
    antisenseRnaParser = new AntisenseRnaXmlParser(elements);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseXmlSpecies() throws Exception {
    String xmlString = readFile(testAntisenseRnaFile);
    Pair<String, CellDesignerAntisenseRna> result = antisenseRnaParser.parseXmlElement(xmlString);
    CellDesignerAntisenseRna antisenseRna = result.getRight();
    assertEquals("arn1", result.getLeft());
    assertEquals("s1", antisenseRna.getName());
    assertTrue(antisenseRna.getNotes().contains("some notes"));
    assertEquals(1, antisenseRna.getRegions().size());
    CellDesignerModificationResidue region = antisenseRna.getRegions().get(0);
    assertEquals("tr1", region.getIdModificationResidue());
    assertEquals("zzz", region.getName());
    assertEquals(0.3, region.getSize(), 1e-6);
    assertEquals(0.29999999999999993, region.getPos(), 1e-6);
    assertEquals(ModificationType.CODING_REGION, region.getModificationType());
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlSpecies() throws Exception {
    String xmlString = readFile("testFiles/invalid/antisense_rna.xml");
    antisenseRnaParser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlSpecies2() throws Exception {
    String xmlString = readFile("testFiles/invalid/antisense_rna2.xml");
    antisenseRnaParser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlSpecies3() throws Exception {
    String xmlString = readFile("testFiles/invalid/antisense_rna3.xml");
    antisenseRnaParser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlSpecies4() throws Exception {
    String xmlString = readFile("testFiles/invalid/antisense_rna4.xml");
    antisenseRnaParser.parseXmlElement(xmlString);
  }

  @Test
  public void testToXml() throws Exception {
    String xmlString = readFile(testAntisenseRnaFile);
    Pair<String, CellDesignerAntisenseRna> result = antisenseRnaParser.parseXmlElement(xmlString);
    CellDesignerAntisenseRna antisenseRna = result.getRight();

    String transformedXml = antisenseRnaParser.toXml(antisenseRna.createModelElement());
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    InputSource is = new InputSource(new StringReader(transformedXml));
    Document doc = builder.parse(is);
    NodeList root = doc.getChildNodes();
    assertEquals("celldesigner:AntisenseRNA", root.item(0).getNodeName());

    Pair<String, CellDesignerAntisenseRna> result2 = antisenseRnaParser.parseXmlElement(xmlString);
    CellDesignerAntisenseRna antisenseRna2 = result2.getRight();
    assertEquals(result.getLeft(), result2.getLeft());
    assertEquals(antisenseRna.getName(), antisenseRna2.getName());
    assertEquals(antisenseRna.getNotes().trim(), antisenseRna2.getNotes().trim());

    assertEquals(1, antisenseRna2.getRegions().size());
    CellDesignerModificationResidue region = antisenseRna.getRegions().get(0);
    CellDesignerModificationResidue region2 = antisenseRna2.getRegions().get(0);
    assertEquals(region.getIdModificationResidue(), region2.getIdModificationResidue());
    assertEquals(region.getName(), region2.getName());
    assertEquals(region.getSize(), region2.getSize(), 1e-6);
    assertEquals(region.getPos(), region2.getPos(), 1e-6);
    assertEquals(region.getModificationType(), region2.getModificationType());
  }

  @Test
  public void testToXmlShouldNotContainNotes() throws Exception {
    String xmlString = readFile(testAntisenseRnaFile);
    Pair<String, CellDesignerAntisenseRna> result = antisenseRnaParser.parseXmlElement(xmlString);
    CellDesignerAntisenseRna antisenseRna = result.getRight();

    String transformedXml = antisenseRnaParser.toXml(antisenseRna.createModelElement());
    assertFalse(transformedXml.contains("celldesigner:notes"));
  }
}
