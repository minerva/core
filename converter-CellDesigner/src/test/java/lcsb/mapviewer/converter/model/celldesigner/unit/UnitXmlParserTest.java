package lcsb.mapviewer.converter.model.celldesigner.unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;

public class UnitXmlParserTest extends CellDesignerTestFunctions {

  private UnitXmlParser parser = new UnitXmlParser();

  @Test
  public void testParseBasicData() throws InvalidXmlSchemaException, IOException {
    SbmlUnit unit = parser.parseFunction(super.getXmlDocumentFromFile("testFiles/unit/simple.xml").getFirstChild());
    assertNotNull(unit);
    assertEquals(unit.getUnitId(), "unit_id");
    assertEquals(unit.getName(), "unit name");
  }

  @Test
  public void testParseUnitTypes() throws InvalidXmlSchemaException, IOException {
    SbmlUnit unit = parser.parseFunction(super.getXmlDocumentFromFile("testFiles/unit/simple.xml").getFirstChild());
    assertNotNull(unit);
    assertEquals(unit.getUnitTypeFactors().size(), 1);
  }

}
