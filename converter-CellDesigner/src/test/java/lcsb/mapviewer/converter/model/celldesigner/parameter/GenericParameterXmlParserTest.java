package lcsb.mapviewer.converter.model.celldesigner.parameter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.kinetics.SbmlParameterComparator;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

@RunWith(Parameterized.class)
public class GenericParameterXmlParserTest extends CellDesignerTestFunctions {

  private String filename;

  public GenericParameterXmlParserTest(final String filename) {
    this.filename = filename;
  }

  @Parameters(name = "{0}")
  public static Collection<Object[]> data() throws IOException {
    List<Object[]> result = new ArrayList<>();
    try (final Stream<Path> walk = Files.walk(Paths.get("testFiles/parameter"))) {

      List<String> filenames = walk.filter(Files::isRegularFile)
          .map(x -> x.toString()).collect(Collectors.toList());
      for (final String string : filenames) {
        result.add(new Object[] { string });
      }
      return result;
    }
  }

  @Test
  public void testSerialization() throws InvalidXmlSchemaException, IOException {
    SbmlUnit volume = new SbmlUnit("volume");
    Model model = new ModelFullIndexed(null);
    model.addUnit(volume);
    ParameterXmlParser parser = new ParameterXmlParser(model);
    SbmlParameter parameter = parser
        .parseParameter(super.getXmlDocumentFromFile(filename).getFirstChild());

    assertNotNull(parameter);

    String xml = parser.toXml(parameter);
    SbmlParameter parameter2 = parser.parseParameter(super.getNodeFromXmlString(xml));
    assertEquals(0, new SbmlParameterComparator().compare(parameter, parameter2));
  }
}
