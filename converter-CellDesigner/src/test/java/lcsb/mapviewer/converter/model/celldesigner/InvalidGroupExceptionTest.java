package lcsb.mapviewer.converter.model.celldesigner;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InvalidGroupExceptionTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor() {
    assertNotNull(new InvalidGroupException());
    assertNotNull(new InvalidGroupException("str"));
    assertNotNull(new InvalidGroupException("str2", new Exception()));
    assertNotNull(new InvalidGroupException(new Exception()));
  }

}
