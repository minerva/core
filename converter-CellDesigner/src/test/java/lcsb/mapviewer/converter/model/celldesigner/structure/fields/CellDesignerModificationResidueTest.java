package lcsb.mapviewer.converter.model.celldesigner.structure.fields;

import static org.junit.Assert.assertNotNull;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;
import lcsb.mapviewer.model.map.species.field.RegulatoryRegion;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;

@RunWith(Parameterized.class)
public class CellDesignerModificationResidueTest extends CellDesignerTestFunctions {

  private CellDesignerModificationResidue region;
  private Element species;

  public CellDesignerModificationResidueTest(final ModificationResidue residue) {
    residue.setPosition(new Point2D.Double(100, 200));
    this.region = new CellDesignerModificationResidue(residue);
    this.species = residue.getSpecies();
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<>();
    ModificationResidue residue;

    residue = new BindingRegion();
    residue.setSpecies(createProtein());
    data.add(new Object[] { residue });
    
    residue = new CodingRegion();
    residue.setSpecies(createRna());
    data.add(new Object[] { residue });
    
    residue = new ProteinBindingDomain();
    residue.setSpecies(createRna());
    data.add(new Object[] { residue });
    
    residue = new RegulatoryRegion();
    residue.setSpecies(createGene());
    data.add(new Object[] { residue });
    
    residue = new TranscriptionSite();
    ((TranscriptionSite)residue).setDirection("LEFT");
    residue.setSpecies(createGene());
    data.add(new Object[] { residue });
    
    residue = new ModificationSite();
    residue.setSpecies(createGene());
    data.add(new Object[] { residue });
    
    residue = new Residue();
    residue.setSpecies(createProtein());
    data.add(new Object[] { residue });

    return data;
  }

  @Test
  public void testToString() {
    assertNotNull(region.toString());
  }

  @Test
  public void testToModificationResidueContainsColor() {
    ModificationResidue residue = region.createModificationResidue(species);
    assertNotNull(residue.getBorderColor());
    
  }

}
