package lcsb.mapviewer.converter.model.celldesigner.parameter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class ParameterXmlParserTest extends CellDesignerTestFunctions {

  @Test
  public void testParseBasicData() throws InvalidXmlSchemaException, IOException {
    SbmlUnit volume = new SbmlUnit("volume");
    Model model = new ModelFullIndexed(null);
    model.addUnit(volume);
    ParameterXmlParser parser = new ParameterXmlParser(model);
    SbmlParameter parameter = parser
        .parseParameter(super.getXmlDocumentFromFile("testFiles/parameter/simple.xml").getFirstChild());

    assertNotNull(parameter);
    assertEquals(parameter.getParameterId(), "param_id");
    assertEquals(parameter.getName(), "Parameter name");
    assertEquals(parameter.getValue(), 12.0, Configuration.EPSILON);
    assertEquals(parameter.getUnits(), volume);
  }

  @Test
  public void testParseInfinity() throws InvalidXmlSchemaException, IOException {
    SbmlUnit volume = new SbmlUnit("volume");
    Model model = new ModelFullIndexed(null);
    model.addUnit(volume);
    ParameterXmlParser parser = new ParameterXmlParser(model);
    SbmlParameter parameter = parser
        .parseParameter(super.getXmlDocumentFromFile("testFiles/parameter/infinity_parameter.xml").getFirstChild());

    assertNotNull(parameter);
    assertTrue(Double.isInfinite(parameter.getValue()));
    assertTrue(parameter.getValue() > 0);
  }

  @Test
  public void testParseNegativeInfinity() throws InvalidXmlSchemaException, IOException {
    SbmlUnit volume = new SbmlUnit("volume");
    Model model = new ModelFullIndexed(null);
    model.addUnit(volume);
    ParameterXmlParser parser = new ParameterXmlParser(model);
    SbmlParameter parameter = parser
        .parseParameter(
            super.getXmlDocumentFromFile("testFiles/parameter/negative_infinity_parameter.xml").getFirstChild());

    assertNotNull(parameter);
    assertTrue(Double.isInfinite(parameter.getValue()));
    assertTrue(parameter.getValue() < 0);
  }

}
