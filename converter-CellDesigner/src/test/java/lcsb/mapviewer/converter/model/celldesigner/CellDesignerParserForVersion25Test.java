package lcsb.mapviewer.converter.model.celldesigner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.model.map.model.Model;

@RunWith(Parameterized.class)
public class CellDesignerParserForVersion25Test extends CellDesignerTestFunctions {

  private Path filePath;

  public CellDesignerParserForVersion25Test(final Path filePath) {
    this.filePath = filePath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<Object[]>();
    Files.walk(Paths.get("testFiles/cd_2.5")).forEach(filePath -> {
      if (Files.isRegularFile(filePath)) {
        data.add(new Object[] { filePath });
      }
    });
    return data;
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().filename(filePath.toString()).sizeAutoAdjust(false));

    testXmlSerialization(model);
  }
}
