package lcsb.mapviewer.converter.model.celldesigner.function;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlFunctionComparator;

public class FunctionXmlParserTest extends CellDesignerTestFunctions {

  private FunctionXmlParser parser = new FunctionXmlParser();

  @Test
  public void testParseBasicData() throws InvalidXmlSchemaException, IOException {
    SbmlFunction fun = parser
        .parseFunction(super.getXmlDocumentFromFile("testFiles/function/simple.xml").getFirstChild());
    assertNotNull(fun);
    assertEquals(fun.getFunctionId(), "function_id");
    assertEquals(fun.getName(), "Function name");
    assertTrue("math node is not present", fun.getDefinition().indexOf("math") >= 0);
    assertTrue(fun.getDefinition().indexOf("lambda") >= 0);
    assertTrue(fun.getDefinition().indexOf("apply") >= 0);
  }

  @Test
  public void testParseArgumentsData() throws InvalidXmlSchemaException, IOException {
    SbmlFunction fun = parser
        .parseFunction(super.getXmlDocumentFromFile("testFiles/function/simple.xml").getFirstChild());
    assertNotNull(fun);
    assertEquals(fun.getArguments().size(), 2);
    assertEquals(fun.getArguments().get(0), "x");
  }

  @Test
  public void testToXml() throws InvalidXmlSchemaException, IOException {
    SbmlFunction fun = parser
        .parseFunction(super.getXmlDocumentFromFile("testFiles/function/simple.xml").getFirstChild());
    String xml = parser.toXml(fun);

    SbmlFunction fun2 = parser.parseFunction(super.getNodeFromXmlString(xml));

    SbmlFunctionComparator comparator = new SbmlFunctionComparator();
    assertEquals(0, comparator.compare(fun, fun2));

  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidDefinition() throws InvalidXmlSchemaException, IOException {
    parser.parseFunction(super.getXmlDocumentFromFile("testFiles/function/invalid_definition.xml").getFirstChild());
  }

}
