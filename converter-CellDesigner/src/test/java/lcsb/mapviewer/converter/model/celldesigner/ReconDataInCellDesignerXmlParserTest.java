package lcsb.mapviewer.converter.model.celldesigner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Species;

public class ReconDataInCellDesignerXmlParserTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseElement() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/annotation/recon_annotation_example.xml"));
    assertNotNull(model);
    Species species = (Species) model.getElementByElementId("sa1");
    assertNotNull(species);
    assertEquals("S1_SYMB", species.getSymbol());
    assertEquals("ABREVIATION", species.getAbbreviation());
    assertEquals("FORMULA", species.getFormula());
    assertEquals((Integer) 324, species.getCharge());
    assertEquals(1, species.getSynonyms().size());
    assertTrue(species.getSynonyms().contains("syn44"));
    assertEquals("DESCRIPTION", species.getNotes());
  }

  @Test
  public void testParseReaction() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser
        .createModel(new ConverterParams().filename("testFiles/annotation/recon_annotation_example.xml"));
    assertNotNull(model);
    Reaction reaction = model.getReactionByReactionId("re1");
    assertNotNull(reaction);
    assertEquals("SAW", reaction.getSymbol());
    assertEquals("ABREVIATION2", reaction.getAbbreviation());
    assertEquals("FORMULA2", reaction.getFormula());
    assertEquals((Integer) 100, reaction.getMechanicalConfidenceScore());
    assertEquals((Double) 200.201, reaction.getLowerBound());
    assertEquals((Double) 300.301, reaction.getUpperBound());
    assertEquals("SUBSYSTEM2", reaction.getSubsystem());
    assertEquals(
        "(urn:miriam:ccds:CCDS8639.1) or (urn:miriam:ccds:CCDS26.1) or (urn:miriam:ccds:CCDS314.2) or (urn:miriam:ccds:CCDS314.1)",
        reaction.getGeneProteinReaction());
    assertEquals(1, reaction.getSynonyms().size());
    assertTrue(reaction.getSynonyms().contains("SYN1"));
    assertEquals("DESCRIPTION2", reaction.getNotes());
  }

}
