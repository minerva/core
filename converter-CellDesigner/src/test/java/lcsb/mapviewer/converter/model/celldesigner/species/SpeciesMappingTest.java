package lcsb.mapviewer.converter.model.celldesigner.species;

import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Field;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerSpecies;
import lcsb.mapviewer.model.map.species.Protein;

public class SpeciesMappingTest extends CellDesignerTestFunctions {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAllValues() {
    for (final SpeciesMapping value : SpeciesMapping.values()) {
      assertNotNull(SpeciesMapping.valueOf(value.toString()));
    }
  }

  @Test
  public void testGetMappingByStringCaseInsensitive() throws Exception {
    SpeciesMapping mapping = SpeciesMapping.getMappingByString("PHENOTYPE");
    assertNotNull(mapping);
    mapping = SpeciesMapping.getMappingByString("phenotype");
    assertNotNull(mapping);
  }

  @Test(expected = InvalidStateException.class)
  public void testCreateInvalidSpeciesImpl() throws Exception {
    // artificial implementation of Species that is invalid
    class InvalidSpecies extends CellDesignerSpecies<Protein> {
      private static final long serialVersionUID = 1L;

      @SuppressWarnings("unused")
      public InvalidSpecies() {
        throw new NotImplementedException();
      }
    }

    // modify one of the elements of SpeciesMapping so it will have invalid
    // implementation
    SpeciesMapping typeToModify = SpeciesMapping.ANTISENSE_RNA;
    Class<? extends CellDesignerSpecies<?>> clazz = typeToModify.getCellDesignerClazz();

    try {
      Field field = typeToModify.getClass().getDeclaredField("cellDesignerClazz");
      field.setAccessible(true);
      field.set(typeToModify, InvalidSpecies.class);

      // and check if we catch properly information about problematic
      // implementation
      typeToModify.createSpecies(new CellDesignerSpecies<Protein>());
    } finally {
      // restore correct values for the modified type (if not then other test
      // might fail...)
      Field field = typeToModify.getClass().getDeclaredField("cellDesignerClazz");
      field.setAccessible(true);
      field.set(typeToModify, clazz);
    }

  }

}
