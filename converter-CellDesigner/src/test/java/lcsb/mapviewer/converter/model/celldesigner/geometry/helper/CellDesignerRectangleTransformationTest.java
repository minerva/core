package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;

public class CellDesignerRectangleTransformationTest {

  private CellDesignerRectangleTransformation transformation = new CellDesignerRectangleTransformation();

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetPointOnRectangleByAnchor() {
    Point2D poitn = transformation.getPointOnRectangleByAnchor(0, 0, 10, 12, null);
    assertEquals(0.0, poitn.distance(new Point2D.Double(5, 6)), Configuration.EPSILON);
  }

}
