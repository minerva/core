package lcsb.mapviewer.converter.model.celldesigner.species;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.alias.ComplexAliasXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerComplexSpecies;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class ComplexParserTest extends CellDesignerTestFunctions {

  private final String testAliasId = "sa3";
  private final String testCompartmentAliasId = "ca3";
  private final String testCompartmentAliasId2 = "ca4";

  private CellDesignerElementCollection elements;

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testComplexState() throws Exception {
    final Model model = getModelForFile("testFiles/problematic/complex_with_state.xml");
    final Complex complex = model.getElementByElementId("csa1");
    final StructuralState state = (StructuralState) complex.getModificationResidues().get(0);
    assertNotNull(state);
    assertEquals("test state", state.getName());

    assertEquals(0, getWarnings().size());
  }

  @Test
  public void testMovedState() throws Exception {
    final Model model = getModelForFile("testFiles/protein_with_moved_state.xml");
    testXmlSerialization(model);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidInputNode() throws Exception {
    final Model model = new ModelFullIndexed(null);
    final ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

    parser.parseXmlAlias(readFile("testFiles/invalid/invalid_complex_alias.xml"));
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidInputNode2() throws Exception {
    final Model model = new ModelFullIndexed(null);
    final CellDesignerComplexSpecies element = new CellDesignerComplexSpecies();
    element.setElementId("s3");
    elements.addElement(element);
    final ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

    parser.parseXmlAlias(readFile("testFiles/invalid/invalid_complex_alias2.xml"));
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidInputNode3() throws Exception {
    final Model model = new ModelFullIndexed(null);
    final CellDesignerComplexSpecies element = new CellDesignerComplexSpecies();
    element.setElementId("s3");
    elements.addElement(element);
    final ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

    parser.parseXmlAlias(readFile("testFiles/invalid/invalid_complex_alias3.xml"));
  }

  @Test(expected = NotImplementedException.class)
  public void testParseInvalidInputNode4() throws Exception {
    final Model model = new ModelFullIndexed(null);
    final CellDesignerComplexSpecies element = new CellDesignerComplexSpecies();
    element.setElementId("s3");
    elements.addElement(element);
    final ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

    parser.parseXmlAlias(readFile("testFiles/invalid/invalid_complex_alias4.xml"));
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidInputNode5() throws Exception {
    final Model model = new ModelFullIndexed(null);
    final CellDesignerComplexSpecies element = new CellDesignerComplexSpecies();
    element.setElementId("s3");
    elements.addElement(element);
    final ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

    parser.parseXmlAlias(readFile("testFiles/invalid/invalid_complex_alias5.xml"));
  }

  @Test
  public void testToXmlWithParent() throws Exception {
    final Model model = new ModelFullIndexed(null);

    final Compartment compartment = new Compartment(testCompartmentAliasId);

    final Complex alias = new Complex(testAliasId);

    alias.setCompartment(compartment);

    model.addElement(alias);
    final ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

    final String xml = parser.toXml(alias);

    assertTrue(xml.contains(testAliasId));
    assertTrue(xml.contains(testCompartmentAliasId));
  }

  @Test
  public void testToXmlWithCompartment() throws Exception {
    final Model model = new ModelFullIndexed(null);

    final Compartment ca = new Compartment(testCompartmentAliasId);

    final Complex alias = new Complex(testAliasId);

    alias.setCompartment(ca);

    model.addElement(alias);
    final ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

    final String xml = parser.toXml(alias);

    assertTrue(xml.contains(testAliasId));
    assertTrue(xml.contains(testCompartmentAliasId));
  }

  @Test
  public void testToXmlWithUnknownCompartment() throws Exception {
    final Model model = Mockito.mock(ModelFullIndexed.class);
    final ModelData md = new ModelData();
    md.setModel(model);

    final Compartment ca2 = new Compartment(testCompartmentAliasId2);
    ca2.setX(6);
    ca2.setY(6);
    ca2.setWidth(190);
    ca2.setHeight(190);

    final Compartment ca = new Compartment(testCompartmentAliasId);
    ca.setX(5);
    ca.setY(5);
    ca.setWidth(200);
    ca.setHeight(200);

    final List<Compartment> list = new ArrayList<>();
    list.add(ca);
    list.add(ca2);

    // ensure that we return list (firts bigger compartment, then smaller)
    when(model.getCompartments()).thenReturn(list);
    when(model.getModelData()).thenReturn(md);

    final Complex alias = new Complex(testAliasId);
    alias.setX(10);
    alias.setY(10);
    alias.setWidth(100);
    alias.setHeight(100);
    alias.setModel(model);

    model.addElement(alias);
    final ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

    final String xml = parser.toXml(alias);

    assertTrue(xml.contains(testAliasId));
    assertTrue(xml.contains(testCompartmentAliasId2));
  }

  @Test
  public void testToXmlWithComplexParent() throws Exception {
    final Model model = new ModelFullIndexed(null);

    final Complex complex = new Complex(testCompartmentAliasId);

    final Complex alias = new Complex(testAliasId);

    alias.setComplex(complex);

    model.addElement(alias);
    final ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

    final String xml = parser.toXml(alias);

    assertTrue(xml.contains(testAliasId));
    assertTrue(xml.contains(testCompartmentAliasId));
  }

  @Test
  public void testToXmlWithActiveComplex() throws Exception {
    final Model model = new ModelFullIndexed(null);

    final Complex alias = new Complex(testAliasId);

    alias.setActivity(true);

    model.addElement(alias);
    final ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

    final String xml = parser.toXml(alias);

    assertTrue(xml.contains(testAliasId));
    assertTrue(xml.contains("<celldesigner:activity>active</celldesigner:activity>"));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testAddInvalidReference() throws Exception {
    final Model model = new ModelFullIndexed(null);
    final CellDesignerComplexSpecies element = new CellDesignerComplexSpecies();
    element.setElementId("s2");
    elements.addElement(element);

    final ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

    final Complex child = parser.parseXmlAlias(readFile("testFiles/xmlNodeTestExamples/cd_complex_alias_with_parent.xml"));

    parser.addReference(child);
  }

  @Test
  public void testParseWithEmptyState() throws Exception {
    final Model model = new ModelFullIndexed(null);
    final CellDesignerComplexSpecies element = new CellDesignerComplexSpecies("s2597");
    elements.addElement(element);

    final ComplexAliasXmlParser parser = new ComplexAliasXmlParser(elements, model);

    final Complex child = parser
        .parseXmlAlias(readFile("testFiles/xmlNodeTestExamples/cd_complex_alias_with_empty_state.xml"));

    assertNotNull(child);
    assertNull(child.getStateLabel());
    assertNull(child.getStatePrefix());
  }

}
