package lcsb.mapviewer.converter.model.celldesigner.geometry;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.map.species.Complex;

public class ComplexConverterTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetAnchorForAlias() throws Exception {
    Complex alias = new Complex("id");
    alias.setWidth(200);
    alias.setHeight(300);
    alias.setX(100.0);
    alias.setY(50.0);

    CellDesignerAliasConverter converter = new CellDesignerAliasConverter(alias, false);

    for (final CellDesignerAnchor anchor : CellDesignerAnchor.values()) {
      Point2D point = converter.getPointCoordinates(alias, anchor);
      CellDesignerAnchor newAnchor = converter.getAnchorForCoordinates(alias, point);
      assertEquals(anchor, newAnchor);
    }
  }

  @Test
  public void testAngleConversion() {
    Complex protein = new Complex("id");
    protein.setWidth(10);
    protein.setHeight(60);
    protein.setX(200);
    protein.setY(500);

    CellDesignerAliasConverter converter = new CellDesignerAliasConverter(protein, false);

    for (double angle = 0.0; angle < Math.PI * 2; angle += 0.1) {
      Point2D point = converter.getResidueCoordinates(protein, angle);
      double angle2 = converter.getAngleForPoint(protein, point);
      assertEquals(angle, angle2, Configuration.EPSILON);
    }
  }

}
