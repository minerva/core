package lcsb.mapviewer.converter.model.celldesigner.geometry;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Ion;

public class IonCellDesignerAliasConverterTest {

  private IonCellDesignerAliasConverter converter = new IonCellDesignerAliasConverter(false);

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetAnchorPointCoordinatesForInvalidAlias() {
    Ion alias = new Ion("id");
    alias.setWidth(-1);
    Point2D point = converter.getAnchorPointCoordinates(alias, 0);
    assertNotNull(point);
  }

  @Test
  public void testGetAnchorPointCoordinatesForInvalidAlias2() {
    Ion alias = new Ion("id");
    Point2D point = converter.getAnchorPointCoordinates(alias, 0);
    assertNotNull(point);
  }

  @Test
  public void testNotImplementedMethod() {
    try {
      converter.getBoundPathIterator(null);
    } catch (final NotImplementedException e) {
      assertTrue(e.getMessage().contains("This class doesn't have bound"));
    }
  }

}
