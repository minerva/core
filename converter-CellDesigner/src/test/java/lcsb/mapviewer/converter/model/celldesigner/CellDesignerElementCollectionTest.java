package lcsb.mapviewer.converter.model.celldesigner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerComplexSpecies;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.field.Residue;

public class CellDesignerElementCollectionTest extends CellDesignerTestFunctions {


  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = InvalidArgumentException.class)
  public void testAddTheSameElementTwice() {
    CellDesignerElementCollection collection = new CellDesignerElementCollection();
    CellDesignerComplexSpecies complex = new CellDesignerComplexSpecies();
    complex.setElementId("id");
    collection.addElement(complex);
    collection.addElement(complex);
  }

  @Test
  public void testIdOfElementsWithDifferentLocation() {
    CellDesignerElementCollection collection = new CellDesignerElementCollection();
    Protein p1 = createProtein();
    p1.setName("SNCA");
    Residue r1 = new Residue();
    r1.setName("xyz");
    p1.addResidue(r1);

    Protein p2 = createProtein();
    p2.setName("SNCA");
    Residue r2 = new Residue();
    r2.setName("abc");

    p2.addResidue(r2);
    assertNotEquals(collection.getElementId(p1), collection.getElementId(p2));
  }

  @Test
  public void testNormalizeToCd() {
    CellDesignerElementCollection collection = new CellDesignerElementCollection();
    assertFalse(collection.normalizeIdToCellDesigner("a.b").contains("."));
    assertFalse(collection.normalizeIdToCellDesigner("a_b").contains("."));
  }

}
