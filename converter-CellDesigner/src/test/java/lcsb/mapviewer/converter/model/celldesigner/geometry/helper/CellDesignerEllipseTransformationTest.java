package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CellDesignerEllipseTransformationTest {
  private CellDesignerEllipseTransformation et = new CellDesignerEllipseTransformation();
  private double x = 0;
  private double y = 10;
  private double width = 20;
  private double height = 30;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetPointOnEllipseByAnchorNull() {
    Point2D p = et.getPointOnEllipseByAnchor(x, y, width, height, null);
    assertEquals(x + width / 2, p.getX(), 1e-6);
    assertEquals(y + height / 2, p.getY(), 1e-6);
  }

  @Test
  public void testGetPointOnEllipseByAnchorE() {
    Point2D p = et.getPointOnEllipseByAnchor(x, y, width, height, CellDesignerAnchor.E);
    assertEquals(x + width, p.getX(), 1e-6);
    assertEquals(y + height / 2, p.getY(), 1e-6);
  }

  @Test
  public void testGetPointOnEllipseByAnchorW() {
    Point2D p = et.getPointOnEllipseByAnchor(x, y, width, height, CellDesignerAnchor.W);
    assertEquals(x, p.getX(), 1e-6);
    assertEquals(y + height / 2, p.getY(), 1e-6);
  }

  @Test
  public void testGetPointOnEllipseByAnchorN() {
    Point2D p = et.getPointOnEllipseByAnchor(x, y, width, height, CellDesignerAnchor.N);
    assertEquals(x + width / 2, p.getX(), 1e-6);
    assertEquals(y, p.getY(), 1e-6);
  }

  @Test
  public void testGetPointOnEllipseByAnchorS() {
    Point2D p = et.getPointOnEllipseByAnchor(x, y, width, height, CellDesignerAnchor.S);
    assertEquals(x + width / 2, p.getX(), 1e-6);
    assertEquals(y + height, p.getY(), 1e-6);
  }

  @Test
  public void testGetPointOnEllipseByAnchorESE() {
    Point2D p = et.getPointOnEllipseByAnchor(x, y, width, height, CellDesignerAnchor.ESE);
    assertEquals(x + width / 2 + Math.cos(Math.PI / 8) * width / 2, p.getX(), 1e-6);
    assertEquals(y + height / 2 + Math.sin(Math.PI / 8) * height / 2, p.getY(), 1e-6);
  }

  @Test
  public void testGetPointOnEllipseByAnchorSE() {
    Point2D p = et.getPointOnEllipseByAnchor(x, y, width, height, CellDesignerAnchor.SE);
    assertEquals(x + width / 2 + Math.cos(Math.PI / 4) * width / 2, p.getX(), 1e-6);
    assertEquals(y + height / 2 + Math.sin(Math.PI / 4) * height / 2, p.getY(), 1e-6);
  }

  @Test
  public void testGetPointOnEllipseByAnchorSSE() {
    Point2D p = et.getPointOnEllipseByAnchor(x, y, width, height, CellDesignerAnchor.SSE);
    assertEquals(x + width / 2 + Math.cos(Math.PI * 3 / 8) * width / 2, p.getX(), 1e-6);
    assertEquals(y + height / 2 + Math.sin(Math.PI * 3 / 8) * height / 2, p.getY(), 1e-6);

  }

  @Test
  public void testGetPointOnEllipseByAnchorSSW() {
    Point2D p = et.getPointOnEllipseByAnchor(x, y, width, height, CellDesignerAnchor.SSW);
    assertEquals(x + width / 2 - Math.sin(Math.PI / 8) * width / 2, p.getX(), 1e-6);
    assertEquals(y + height / 2 + Math.cos(Math.PI / 8) * height / 2, p.getY(), 1e-6);
  }

  @Test
  public void testGetPointOnEllipseByAnchorSW() {
    Point2D p = et.getPointOnEllipseByAnchor(x, y, width, height, CellDesignerAnchor.SW);
    assertEquals(x + width / 2 - Math.sin(Math.PI / 4) * width / 2, p.getX(), 1e-6);
    assertEquals(y + height / 2 + Math.cos(Math.PI / 4) * height / 2, p.getY(), 1e-6);
  }

  @Test
  public void testGetPointOnEllipseByAnchorWSW() {
    Point2D p = et.getPointOnEllipseByAnchor(x, y, width, height, CellDesignerAnchor.WSW);
    assertEquals(x + width / 2 - Math.sin(Math.PI * 3 / 8) * width / 2, p.getX(), 1e-6);
    assertEquals(y + height / 2 + Math.cos(Math.PI * 3 / 8) * height / 2, p.getY(), 1e-6);
  }

  @Test
  public void testGetPointOnEllipseByAnchorWNW() {
    Point2D p = et.getPointOnEllipseByAnchor(x, y, width, height, CellDesignerAnchor.WNW);
    assertEquals(x + width / 2 - Math.sin(Math.PI * 3 / 8) * width / 2, p.getX(), 1e-6);
    assertEquals(y + height / 2 - Math.cos(Math.PI * 3 / 8) * height / 2, p.getY(), 1e-6);
  }

  @Test
  public void testGetPointOnEllipseByAnchorNW() {
    Point2D p = et.getPointOnEllipseByAnchor(x, y, width, height, CellDesignerAnchor.NW);
    assertEquals(x + width / 2 - Math.sin(Math.PI / 4) * width / 2, p.getX(), 1e-6);
    assertEquals(y + height / 2 - Math.cos(Math.PI / 4) * height / 2, p.getY(), 1e-6);
  }

  @Test
  public void testGetPointOnEllipseByAnchorNNW() {
    Point2D p = et.getPointOnEllipseByAnchor(x, y, width, height, CellDesignerAnchor.NNW);
    assertEquals(x + width / 2 - Math.sin(Math.PI / 8) * width / 2, p.getX(), 1e-6);
    assertEquals(y + height / 2 - Math.cos(Math.PI / 8) * height / 2, p.getY(), 1e-6);
  }

  @Test
  public void testGetPointOnEllipseByAnchorNNE() {
    Point2D p = et.getPointOnEllipseByAnchor(x, y, width, height, CellDesignerAnchor.NNE);
    assertEquals(x + width / 2 + Math.sin(Math.PI / 8) * width / 2, p.getX(), 1e-6);
    assertEquals(y + height / 2 - Math.cos(Math.PI / 8) * height / 2, p.getY(), 1e-6);
  }

  @Test
  public void testGetPointOnEllipseByAnchorNE() {
    Point2D p = et.getPointOnEllipseByAnchor(x, y, width, height, CellDesignerAnchor.NE);
    assertEquals(x + width / 2 + Math.sin(Math.PI / 4) * width / 2, p.getX(), 1e-6);
    assertEquals(y + height / 2 - Math.cos(Math.PI / 4) * height / 2, p.getY(), 1e-6);
  }

  @Test
  public void testGetPointOnEllipseByAnchorENE() {
    Point2D p = et.getPointOnEllipseByAnchor(x, y, width, height, CellDesignerAnchor.ENE);
    assertEquals(x + width / 2 + Math.sin(Math.PI * 3 / 8) * width / 2, p.getX(), 1e-6);
    assertEquals(y + height / 2 - Math.cos(Math.PI * 3 / 8) * height / 2, p.getY(), 1e-6);
  }
}
