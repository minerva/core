package lcsb.mapviewer.converter.model.celldesigner.compartment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerCompartment;
import lcsb.mapviewer.model.map.compartment.Compartment;

public class CompartmentCollectionXmlParserTest extends CellDesignerTestFunctions {

  private int aliasIdCounter = 0;

  private CompartmentCollectionXmlParser parser;
  private CellDesignerElementCollection elements;

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();
    parser = new CompartmentCollectionXmlParser(elements);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseXmlReactionCollection() throws Exception {
    String xmlString = readFile("testFiles/compartment/listOfCompartments.xml");
    Node node = getNodeFromXmlString(xmlString);
    List<CellDesignerCompartment> compartments = parser.parseXmlCompartmentCollection(node);

    assertEquals(4, compartments.size());
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlReactionCollection() throws Exception {
    String xmlString = readFile("testFiles/invalid/listOfCompartments.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseXmlCompartmentCollection(node);
  }

  @Test
  public void testCompartmentCollectionToXmlString() throws Exception {
    String xmlString = readFile("testFiles/compartment/listOfCompartments.xml");
    Node node = getNodeFromXmlString(xmlString);
    List<CellDesignerCompartment> cellDesignerCompartments = parser.parseXmlCompartmentCollection(node);

    List<Compartment> compartments = new ArrayList<>();
    for (final CellDesignerCompartment cellDesignerCompartment : cellDesignerCompartments) {
      Compartment comp = cellDesignerCompartment.createModelElement("ca" + aliasIdCounter);
      compartments.add(comp);
      aliasIdCounter++;
    }

    String xmlString2 = parser.toXml(compartments);
    assertNotNull(xmlString2);

    node = getNodeFromXmlString(xmlString2);

    elements = new CellDesignerElementCollection();
    parser = new CompartmentCollectionXmlParser(elements);

    List<CellDesignerCompartment> compartments2 = parser.parseXmlCompartmentCollection(node);
    for (int i = 0; i < cellDesignerCompartments.size(); i++) {
      CellDesignerCompartment compartmentA = cellDesignerCompartments.get(i);
      CellDesignerCompartment compartmentB = compartments2.get(i);
      assertEquals(compartmentA.getName(), compartmentB.getName());
    }
  }

}
