package lcsb.mapviewer.converter.model.celldesigner.reaction;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class UnknownModifierClassExceptionTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    String modificationType = "str";
    UnknownModifierClassException exception = new UnknownModifierClassException(null, null, null);
    exception.setModificationType(modificationType);
    assertEquals(modificationType, exception.getModificationType());
  }

}
