package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class ProteinTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CellDesignerGenericProtein());
  }

  @Test
  public void testConstructor1() {
    CellDesignerProtein<?> protein = new CellDesignerGenericProtein();
    protein.setStructuralState("srt");
    List<CellDesignerModificationResidue> residues = new ArrayList<>();
    residues.add(new CellDesignerModificationResidue());

    protein.setModificationResidues(residues);
    CellDesignerProtein<?> protein2 = new CellDesignerGenericProtein(protein);
    assertNotNull(protein2);
  }

  @Test
  public void testUpdate() {
    CellDesignerProtein<?> protein = new CellDesignerGenericProtein();
    protein.setStructuralState("");
    CellDesignerProtein<?> protein2 = new CellDesignerGenericProtein();
    protein2.setStructuralState("srt");
    List<CellDesignerModificationResidue> residues = new ArrayList<>();
    residues.add(new CellDesignerModificationResidue());

    protein2.setModificationResidues(residues);

    protein.update(protein2);
    assertEquals(protein2.getStructuralState(), protein.getStructuralState());
  }

  @Test
  public void testAddModificationResidue() {
    CellDesignerProtein<?> protein = new CellDesignerGenericProtein();
    CellDesignerModificationResidue mr = new CellDesignerModificationResidue();
    mr.setIdModificationResidue("id1");

    CellDesignerModificationResidue mr2 = new CellDesignerModificationResidue();
    mr2.setIdModificationResidue("id1");

    protein.addModificationResidue(mr);
    assertEquals(1, protein.getModificationResidues().size());
    protein.addModificationResidue(mr2);
    assertEquals(1, protein.getModificationResidues().size());
  }

  @Test
  public void testSetStructuralState() {
    CellDesignerProtein<?> protein = new CellDesignerGenericProtein();
    protein.setStructuralState("str");
    protein.setStructuralState("str1");

    assertEquals("str1", protein.getStructuralState());
  }

  @Test
  public void testCopy() {
    CellDesignerProtein<?> protein = new CellDesignerProtein<GenericProtein>().copy();
    assertNotNull(protein);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    CellDesignerProtein<?> mock = Mockito.spy(CellDesignerProtein.class);
    mock.copy();
  }

}
