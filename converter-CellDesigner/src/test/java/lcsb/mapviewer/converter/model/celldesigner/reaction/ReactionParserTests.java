package lcsb.mapviewer.converter.model.celldesigner.reaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGenericProtein;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NandOperator;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.OrOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.UnknownOperator;
import lcsb.mapviewer.model.map.reaction.type.DissociationReaction;
import lcsb.mapviewer.model.map.reaction.type.HeterodimerAssociationReaction;
import lcsb.mapviewer.model.map.reaction.type.KnownTransitionOmittedReaction;
import lcsb.mapviewer.model.map.reaction.type.NegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.ReactionRect;
import lcsb.mapviewer.model.map.reaction.type.ReducedTriggerReaction;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.reaction.type.TranscriptionReaction;
import lcsb.mapviewer.model.map.reaction.type.TranslationReaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.reaction.type.TruncationReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownReducedPhysicalStimulationReaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;

public class ReactionParserTests extends CellDesignerTestFunctions {

  private ReactionXmlParser parser;
  private CellDesignerElementCollection elements;

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();

    parser = new ReactionXmlParser(elements, false, false);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testColorReaction() throws Exception {
    Model model = getModelForFile("testFiles/colorfull_reaction.xml");
    Reaction reaction = model.getReactionByReactionId("re1");
    PolylineData line = reaction.getReactants().get(0).getLine();
    assertFalse(
        "000".equals(line.getColor().getRed() + "" + line.getColor().getGreen() + "" + line.getColor().getBlue()));
    line = reaction.getProducts().get(0).getLine();
    assertFalse(
        "000".equals(line.getColor().getRed() + "" + line.getColor().getGreen() + "" + line.getColor().getBlue()));
    line = reaction.getModifiers().get(0).getLine();
    assertFalse(
        "000".equals(line.getColor().getRed() + "" + line.getColor().getGreen() + "" + line.getColor().getBlue()));
    line = reaction.getOperators().get(0).getLine();
    assertFalse(
        "000".equals(line.getColor().getRed() + "" + line.getColor().getGreen() + "" + line.getColor().getBlue()));
  }

  @Test
  public void testColorReactionWithModifierOperator() throws Exception {
    Model model = getModelForFile("testFiles/colorfull_reaction_2.xml");
    Reaction reaction = model.getReactionByReactionId("re1");
    Modifier m1 = reaction.getModifiers().get(0);
    Modifier m2 = reaction.getModifiers().get(1);
    NodeOperator o = reaction.getOperators().get(0);

    Set<Color> colors = new HashSet<>();
    colors.add(m1.getLine().getColor());
    colors.add(m2.getLine().getColor());
    colors.add(o.getLine().getColor());
    assertEquals("Each part has different color", 3, colors.size());

    assertFalse("000".equals(getStringColor(m1.getLine())));
    assertFalse("000".equals(getStringColor(m2.getLine())));
    assertFalse("Operator line not parsed properly", "000".equals(getStringColor(o.getLine())));
  }

  private String getStringColor(final PolylineData line) {
    return line.getColor().getRed() + "" + line.getColor().getGreen() + "" + line.getColor().getBlue();
  }

  @Test
  public void testMissingLines() throws Exception {
    Model model = getModelForFile("testFiles/problematic/pd_map_with_problematic_reaction_line.xml");
    assertTrue(model.getElementByElementId("sa5003") instanceof GenericProtein);
    Set<Reaction> list = model.getReactions();
    for (final Reaction reaction : list) {
      // reaction re1607 in this model was problematic, but in fact the
      // problem
      // can be in any reaction line
      // if (reaction.getId().equals("re1607")) {
      List<Line2D> lines = reaction.getLines();
      for (final Line2D line2d : lines) {
        assertFalse(Double.isNaN(line2d.getX1()));
        assertFalse(Double.isNaN(line2d.getX2()));
        assertFalse(Double.isNaN(line2d.getY1()));
        assertFalse(Double.isNaN(line2d.getY2()));
      }
      // }
    }
  }

  @Test
  public void testTransitionReaction() throws Exception {
    Model model = getModelForFile("testFiles/reactions/transition.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof StateTransitionReaction);
    assertEquals(1, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);

    assertTrue(reaction.isReversible());
    assertEquals(ArrowType.FULL, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(3, reactant.getLine().getLines().size());

    Product product = reaction.getProducts().get(0);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(2, product.getLine().getLines().size());

    assertTrue(reactant.getLine().getEndPoint().distance(product.getLine().getStartPoint()) < 20);
    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testTransition2() throws Exception {
    Model model = getModelForFile("testFiles/reactions/transitionWithAdditionalNodes.xml");
    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(2, reaction.getOperators().size());
    for (final Reactant reactant : reaction.getReactants()) {
      assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
      assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
      logger.debug(reactant.getLine().getEndPoint());
      logger.debug(reaction.getReactants().get(0).getLine().getEndPoint());
      assertTrue(
          reactant.getLine().getEndPoint().distance(reaction.getReactants().get(0).getLine().getEndPoint()) < 1e-6);
    }

    for (final Product product : reaction.getProducts()) {
      assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
      assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());

      assertTrue(
          product.getLine().getStartPoint().distance(reaction.getProducts().get(0).getLine().getStartPoint()) < 1e-6);
    }
    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testTransiotionOmitted() throws Exception {
    Model model = getModelForFile("testFiles/reactions/transition_omitted.xml");
    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(0, reaction.getOperators().size());
    assertEquals(KnownTransitionOmittedReaction.class, reaction.getClass());
    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testUnknownTransition() throws Exception {
    Model model = getModelForFile("testFiles/reactions/unknown_transition.xml");
    Reaction reaction = model.getReactions().iterator().next();
    assertNotNull(reaction.getReactionRect());

  }

  @Test
  public void testTranscription() throws Exception {
    Model model = getModelForFile("testFiles/reactions/transcription.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof TranscriptionReaction);
    assertEquals(1, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(LineType.DASH_DOT_DOT, reactant.getLine().getType());
    assertEquals(1, reactant.getLine().getLines().size());

    Product product = reaction.getProducts().get(0);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(1, product.getLine().getLines().size());

    assertTrue(reactant.getLine().getEndPoint().distance(product.getLine().getStartPoint()) < 20);
    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testTranscription2() throws Exception {
    Model model = getModelForFile("testFiles/reactions/transcription_with_additions.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof TranscriptionReaction);
    assertEquals(2, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(LineType.DASH_DOT_DOT, reactant.getLine().getType());
    assertEquals(1, reactant.getLine().getLines().size());
    NodeOperator operator = null;
    for (final NodeOperator operator2 : reaction.getOperators()) {
      if (operator2.isProductOperator()) {
        operator = operator2;
      }
    }
    assertEquals(LineType.DASH_DOT_DOT, operator.getLine().getType());

    Product product = reaction.getProducts().get(0);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(1, product.getLine().getLines().size());

    assertNotNull(reaction.getReactionRect());

    super.testXmlSerialization(model);
  }

  @Test
  public void testTranslation() throws Exception {
    Model model = getModelForFile("testFiles/reactions/translation.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof TranslationReaction);
    assertEquals(2, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(LineType.DASH_DOT, reactant.getLine().getType());
    assertEquals(1, reactant.getLine().getLines().size());
    NodeOperator operator = reaction.getOperators().iterator().next();
    assertEquals(LineType.DASH_DOT, operator.getLine().getType());

    Product product = reaction.getProducts().get(0);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(1, product.getLine().getLines().size());

    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testTransport() throws Exception {
    Model model = getModelForFile("testFiles/reactions/transport.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof TransportReaction);
    assertEquals(1, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(1, reactant.getLine().getLines().size());

    Product product = reaction.getProducts().get(0);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL_CROSSBAR, product.getLine().getEndAtd().getArrowType());
    assertEquals(1, product.getLine().getLines().size());

    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testHeterodimer() throws Exception {
    Model model = getModelForFile("testFiles/reactions/heterodimer.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof HeterodimerAssociationReaction);
    assertEquals(2, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(2, reactant.getLine().getLines().size());
    reactant = reaction.getReactants().get(1);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(3, reactant.getLine().getLines().size());

    Product product = reaction.getProducts().get(0);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(3, product.getLine().getLines().size());

    NodeOperator operator = reaction.getOperators().iterator().next();
    assertEquals(LineType.SOLID, operator.getLine().getType());
    assertEquals(7, operator.getLine().getLines().size());

    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testHeterodimerWithAdditions() throws Exception {
    Model model = getModelForFile("testFiles/reactions/heterodimerWithAdditions.xml");

    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof HeterodimerAssociationReaction);
    assertEquals(3, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(2, reactant.getLine().getLines().size());
    reactant = reaction.getReactants().get(1);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(3, reactant.getLine().getLines().size());

    for (final Product product : reaction.getProducts()) {
      assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
      assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    }

    assertEquals(3, reaction.getOperators().size());
    NodeOperator operator = reaction.getOperators().iterator().next();
    assertEquals(LineType.SOLID, operator.getLine().getType());
    assertEquals(7, operator.getLine().getLines().size());

    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testDissociation() throws Exception {
    Model model = getModelForFile("testFiles/reactions/dissociation.xml");
    assertEquals(2, model.getReactions().size());
    Reaction reaction = null;
    for (final Reaction reaction2 : model.getReactions()) {
      if (reaction2.getIdReaction().equals("re1")) {
        reaction = reaction2;
      }
    }
    assertTrue(reaction instanceof DissociationReaction);
    assertEquals(1, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    logger.debug(reactant.getLine());
    assertEquals(2, reactant.getLine().getLines().size());

    assertEquals(2, reaction.getProducts().size());
    Product product = reaction.getProducts().get(0);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(5, product.getLine().getLines().size());
    product = reaction.getProducts().get(1);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(4, product.getLine().getLines().size());

    assertEquals(1, reaction.getOperators().size());
    NodeOperator operator = reaction.getOperators().iterator().next();
    assertEquals(LineType.SOLID, operator.getLine().getType());
    assertEquals(5, operator.getLine().getLines().size());

    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testDissociationWithAddition() throws Exception {
    Model model = getModelForFile("testFiles/reactions/dissociationWithAddition.xml");
    assertEquals(2, model.getReactions().size());
    Reaction reaction = null;
    for (final Reaction reaction2 : model.getReactions()) {
      if (reaction2.getIdReaction().equals("re1")) {
        reaction = reaction2;
      }
    }
    assertTrue(reaction instanceof DissociationReaction);
    assertEquals(2, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(2, reactant.getLine().getLines().size());

    assertEquals(3, reaction.getProducts().size());
    Product product = reaction.getProducts().get(0);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(5, product.getLine().getLines().size());
    product = reaction.getProducts().get(1);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(4, product.getLine().getLines().size());

    assertEquals(3, reaction.getOperators().size());
    NodeOperator operator = reaction.getOperators().iterator().next();
    assertEquals(LineType.SOLID, operator.getLine().getType());
    assertEquals(5, operator.getLine().getLines().size());
    assertEquals(AndOperator.class, operator.getClass());

    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testTruncation() throws Exception {
    Model model = getModelForFile("testFiles/reactions/truncation.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof TruncationReaction);
    assertEquals(1, reaction.getReactants().size());
    Reactant reactant = reaction.getReactants().get(0);
    assertEquals(ArrowType.NONE, reactant.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.NONE, reactant.getLine().getEndAtd().getArrowType());
    assertEquals(2, reactant.getLine().getLines().size());

    assertEquals(2, reaction.getProducts().size());
    Product product = reaction.getProducts().get(0);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(3, product.getLine().getLines().size());
    product = reaction.getProducts().get(1);
    assertEquals(ArrowType.NONE, product.getLine().getBeginAtd().getArrowType());
    assertEquals(ArrowType.FULL, product.getLine().getEndAtd().getArrowType());
    assertEquals(4, product.getLine().getLines().size());

    assertEquals(1, reaction.getOperators().size());
    NodeOperator operator = reaction.getOperators().iterator().next();
    assertEquals(LineType.SOLID, operator.getLine().getType());
    assertEquals(1, operator.getLine().getLines().size());
    assertEquals(AndOperator.class, operator.getClass());
    assertEquals(reaction.getReactionRect(), ReactionRect.RECT_BOLT);

    assertNotNull(reaction.getReactionRect());
  }

  @Test
  public void testTruncationWithModifier() throws Exception {
    Model model = getModelForFile("testFiles/reactions/truncationWithModifier.xml");
    Reaction reaction = model.getReactions().iterator().next();
    Modifier m = reaction.getModifiers().get(0);
    assertEquals(ArrowType.CIRCLE, m.getLine().getEndAtd().getArrowType());

    m = reaction.getModifiers().get(1);
    assertEquals(ArrowType.CROSSBAR, m.getLine().getEndAtd().getArrowType());
  }

  @Test
  public void testComplexModifier1() throws Exception {
    Model model = getModelForFile("testFiles/reactions/complexModifier1.xml");

    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(1, reaction.getOperators().size());
    assertEquals(2, reaction.getModifiers().size());

    NodeOperator operator = reaction.getOperators().iterator().next();
    assertTrue(operator.isModifierOperator());
    assertEquals(AndOperator.class, operator.getClass());

  }

  @Test
  public void testComplexModifier2() throws Exception {
    Model model = getModelForFile("testFiles/reactions/complexModifier2.xml");
    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(1, reaction.getOperators().size());
    assertEquals(2, reaction.getModifiers().size());

    NodeOperator operator = reaction.getOperators().iterator().next();
    assertTrue(operator.isModifierOperator());
    assertEquals(OrOperator.class, operator.getClass());
  }

  @Test
  public void testComplexModifier3() throws Exception {
    Model model = getModelForFile("testFiles/reactions/complexModifier3.xml");
    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(1, reaction.getOperators().size());
    assertEquals(2, reaction.getModifiers().size());

    NodeOperator operator = reaction.getOperators().iterator().next();
    assertTrue(operator.isModifierOperator());
    assertEquals(NandOperator.class, operator.getClass());
  }

  @Test
  public void testComplexModifier4() throws Exception {
    Model model = getModelForFile("testFiles/reactions/complexModifier4.xml");
    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(1, reaction.getOperators().size());
    assertEquals(2, reaction.getModifiers().size());

    NodeOperator operator = reaction.getOperators().iterator().next();
    assertTrue(operator.isModifierOperator());
    assertEquals(UnknownOperator.class, operator.getClass());
  }

  @Test
  public void testComplexReaction() throws Exception {
    Model model = getModelForFile("testFiles/reactions/complexReaction.xml");
    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(0, reaction.getModifiers().size());
    assertEquals(2, reaction.getOperators().size());
    assertEquals(2, reaction.getProducts().size());
    assertEquals(2, reaction.getReactants().size());

    NodeOperator operator = reaction.getOperators().iterator().next();
    assertEquals(AndOperator.class, operator.getClass());
  }

  @Test
  public void testComplexModifiers5() throws Exception {
    Model model = getModelForFile("testFiles/reactions/complexModifier5.xml");

    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(4, reaction.getModifiers().size());
    logger.debug(reaction.getModifiers());
    assertEquals(1, reaction.getOperators().size());
    assertEquals(2, reaction.getOperators().iterator().next().getInputs().size());
  }

  @Test
  public void testProblematicAnchors() throws Exception {
    Model model = getModelForFile("testFiles/reactions/problemWithAnchors2.xml");

    Reaction reaction1 = null;
    Reaction reaction2 = null;
    for (final Reaction reaction : model.getReactions()) {
      if (reaction.getIdReaction().equals("re2")) {
        reaction1 = reaction;
      }
      if (reaction.getIdReaction().equals("re3")) {
        reaction2 = reaction;
      }
    }
    Reactant reactant = reaction1.getReactants().get(0);
    Element alias1 = reaction1.getReactants().get(0).getElement();
    Element alias2 = reaction1.getProducts().get(0).getElement();
    Product product = reaction1.getProducts().get(0);
    Point2D point = new Point2D.Double(alias1.getX() + alias1.getWidth() / 2, alias1.getY() + alias1.getHeight());
    Point2D point2 = new Point2D.Double(alias2.getX(), alias2.getY() + alias2.getHeight() / 2);
    assertTrue(point.distance(reactant.getLine().getStartPoint()) < 1);
    assertTrue(point2.distance(product.getLine().getEndPoint()) < 1);

    reactant = reaction2.getReactants().get(0);
    alias1 = reaction2.getReactants().get(0).getElement();
    alias2 = reaction2.getProducts().get(0).getElement();
    product = reaction2.getProducts().get(0);
    point = new Point2D.Double(alias1.getX() + alias1.getWidth(), alias1.getY() + alias1.getHeight() / 2);
    point2 = new Point2D.Double(alias2.getX(), alias2.getY() + alias2.getHeight() / 2);
    assertTrue(point.distance(reactant.getLine().getStartPoint()) < 1);
    assertTrue(point2.distance(product.getLine().getEndPoint()) < 1);
  }

  @Test
  public void testProblematicAnchors3() throws Exception {
    Model model = getModelForFile("testFiles/reactions/problemWithAnchors3.xml");
    Reaction reaction = null;
    for (final Reaction reaction2 : model.getReactions()) {
      if (reaction2.getIdReaction().equals("re3")) {
        reaction = reaction2;
      }
    }
    Point2D point = new Point2D.Double(164.85583789974368, 86.060142902597);
    Point2D point2 = new Point2D.Double(397.06477630152193, 284.99999999999994);

    assertTrue(point.distance(reaction.getModifiers().get(0).getLine().getStartPoint()) < 1);
    assertTrue(point2.distance(reaction.getModifiers().get(1).getLine().getStartPoint()) < 1);
  }

  @Test
  public void testPositiveInfluence() throws Exception {
    Model model = getModelForFile("testFiles/reactions/positive_influence.xml");
    Reaction reaction = model.getReactions().iterator().next();
    assertNull(reaction.getReactionRect());
  }

  @Test
  public void testProblematicAnchors2() throws Exception {
    Model model = getModelForFile("testFiles/reactions/complexReactionWithModifier.xml");
    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(reaction.getProducts().get(0).getLine().length(), reaction.getReactants().get(0).getLine().length(),
        1e-6);
  }

  @Test
  public void testProblematicAnchorsWithTwoReactantReaction() throws Exception {
    Model model = getModelForFile("testFiles/reactions/centeredAnchorInModifier.xml");
    Reaction reaction = model.getReactions().iterator().next();
    Reactant r = null;
    for (final Reactant reactant : reaction.getReactants()) {
      if (reactant.getElement().getName().equals("s3")) {
        r = reactant;
      }
    }
    assertNotNull(r);
    // I think there is still a bug because it should work with smaller delta
    // :)
    assertEquals(r.getLine().getLines().get(0).getX2(), r.getLine().getLines().get(1).getX2(), 1);
  }

  @Test
  public void testReactionWithModifiers() throws Exception {
    Model model = getModelForFile("testFiles/reactions/reactionWithModifiers.xml");
    Reaction reaction = model.getReactions().iterator().next();
    List<Modifier> modifiers = reaction.getModifiers();
    Modifier sa3 = null;
    Modifier sa4 = null;
    for (final Modifier modifier : modifiers) {
      if (modifier.getElement().getElementId().equals("sa3")) {
        sa3 = modifier;
      }
      if (modifier.getElement().getElementId().equals("sa4")) {
        sa4 = modifier;
      }
    }
    assertEquals(sa3.getLine().getLines().get(0).getP1(), new Point2D.Double(101.9591678008944, 67.99999999999999));
    assertEquals(sa3.getLine().getLines().get(0).getP2(), new Point2D.Double(101.86750788643532, 112.89589905362774));
    assertEquals(sa3.getLine().getLines().get(1).getP2(), new Point2D.Double(190.66666666666666, 117.66666666666667));

    assertEquals(sa4.getLine().getLines().get(0).getP1(), new Point2D.Double(267.7075561388218, 54.00000000000001));
    assertEquals(sa4.getLine().getLines().get(0).getP2(), new Point2D.Double(298.3735877669656, 71.67109819284718));
    assertEquals(sa4.getLine().getLines().get(1).getP2(), new Point2D.Double(190.66666666666666, 117.66666666666667));

  }

  @Test
  public void testReactionOperatorsWithOperators() throws Exception {
    Model model = getModelForFile("testFiles/reactions/reactionWithOperators.xml");
    Reaction reaction = model.getReactions().iterator().next();
    NodeOperator operator1 = reaction.getOperators().get(0);
    NodeOperator operator2 = reaction.getOperators().get(1);
    NodeOperator operator3 = reaction.getOperators().get(2);

    Product product1 = reaction.getProducts().get(0);
    Product product2 = reaction.getProducts().get(1);

    assertEquals(operator1.getLine().getLines().get(0).getP1(), new Point2D.Double(287.0, 242.00000000000009));
    assertEquals(operator1.getLine().getLines().get(0).getP2(), new Point2D.Double(287.97349260719136, 204.40292958328325));
    assertEquals(operator1.getLine().getEndPoint(), operator2.getLine().getStartPoint());

    assertEquals(operator2.getLine().getLines().get(0).getP1(), new Point2D.Double(359.1840955643148, 203.94471254019686));
    assertEquals(operator2.getLine().getLines().get(0).getP2(), new Point2D.Double(372.98682911109324, 203.85589644414273));

    assertEquals(operator3.getLine().getLines().get(0).getP1(), new Point2D.Double(394.78939704287654, 203.7156040186537));
    assertEquals(operator3.getLine().getLines().get(0).getP2(), new Point2D.Double(380.98666349609823, 203.80442011470785));

    assertEquals(product1.getLine().getLines().get(0).getP1(), new Point2D.Double(394.78939704287654, 203.7156040186537));
    assertEquals(product1.getLine().getLines().get(0).getP2(), new Point2D.Double(466.00000000000006, 203.2573869755673));

    assertEquals(product2.getLine().getLines().get(0).getP1(), new Point2D.Double(394.78939704287654, 203.7156040186537));
    assertEquals(product2.getLine().getLines().get(0).getP2(), new Point2D.Double(452.9689432192971, 107.0000000000000));
  }

  @Test
  public void testReactionProductsWithOperators() throws Exception {
    Model model = getModelForFile("testFiles/reactions/reactionWithOperators.xml");
    Reaction reaction = model.getReactions().iterator().next();

    Product product1 = reaction.getProducts().get(0);
    Product product2 = reaction.getProducts().get(1);

    assertEquals(product1.getLine().getLines().get(0).getP1(), new Point2D.Double(394.78939704287654, 203.7156040186537));
    assertEquals(product1.getLine().getLines().get(0).getP2(), new Point2D.Double(466.00000000000006, 203.2573869755673));

    assertEquals(product2.getLine().getLines().get(0).getP1(), new Point2D.Double(394.78939704287654, 203.7156040186537));
    assertEquals(product2.getLine().getLines().get(0).getP2(), new Point2D.Double(452.9689432192971, 107.0000000000000));
  }

  @Test
  public void testReactionReactantsWithOperators() throws Exception {
    Model model = getModelForFile("testFiles/reactions/reactionWithOperators.xml");
    Reaction reaction = model.getReactions().iterator().next();

    Reactant reactant1 = reaction.getReactants().get(0);
    Reactant reactant2 = reaction.getReactants().get(1);
    Reactant reactant3 = reaction.getReactants().get(2);

    assertEquals(reactant1.getLine().getLines().get(0).getP1(), new Point2D.Double(112.5361842105263, 167.4638157894737));
    assertEquals(reactant1.getLine().getLines().get(0).getP2(), new Point2D.Double(287.0, 242.00000000000009));

    assertEquals(reactant2.getLine().getLines().get(0).getP1(), new Point2D.Double(121.0, 242.0));
    assertEquals(reactant2.getLine().getLines().get(0).getP2(), new Point2D.Double(287.0, 242.00000000000009));

    assertEquals(reactant3.getLine().getLines().get(0).getP1(), new Point2D.Double(116.65392247520948, 72.34607752479050));
    assertEquals(reactant3.getLine().getLines().get(0).getP2(), new Point2D.Double(359.1840955643148, 203.94471254019686));
  }

  @Test
  public void testTransitionReactionToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_transition.xml");

    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    assertEquals(reaction1.getClass(), reaction2.getClass());
    assertEquals(reaction1.getReactants().size(), reaction2.getReactants().size());
    Reactant reactant1 = reaction1.getReactants().get(0);
    Reactant reactant2 = reaction2.getReactants().get(0);
    assertEquals(reactant1.getLine().getBeginAtd().getArrowType(), reactant2.getLine().getBeginAtd().getArrowType());
    assertEquals(reactant1.getLine().getEndAtd().getArrowType(), reactant2.getLine().getEndAtd().getArrowType());
    assertEquals(reactant1.getLine().getLines().size(), reactant2.getLine().getLines().size());

    Product product1 = reaction1.getProducts().get(0);
    Product product2 = reaction2.getProducts().get(0);
    assertEquals(product1.getLine().getBeginAtd().getArrowType(), product2.getLine().getBeginAtd().getArrowType());
    assertEquals(product1.getLine().getEndAtd().getArrowType(), product2.getLine().getEndAtd().getArrowType());
    assertEquals(product1.getLine().getLines().size(), product2.getLine().getLines().size());

    assertEquals(reactant1.getLine().getEndPoint().getX(), reactant2.getLine().getEndPoint().getX(), EPSILON);
    assertEquals(reactant1.getLine().getEndPoint().getY(), reactant2.getLine().getEndPoint().getY(), EPSILON);

    assertEquals(reaction1.getReactionRect(), reaction2.getReactionRect());

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }

    assertEquals(product1.getLine().getColor(), product2.getLine().getColor());
    assertEquals(product1.getLine().getWidth(), product2.getLine().getWidth(), 1e-6);
    assertEquals(reaction1.getLine().getColor(), product2.getLine().getColor());
  }

  @Test
  public void testTransitionWidthAdditionalNodelReactionToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_transitionWithAdditionalNodes.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    assertEquals(reaction1.getClass(), reaction2.getClass());
    assertEquals(reaction1.getReactants().size(), reaction2.getReactants().size());
    Reactant reactant1 = reaction1.getReactants().get(1);
    Reactant reactant2 = reaction2.getReactants().get(1);
    assertEquals(reactant1.getLine().getBeginAtd().getArrowType(), reactant2.getLine().getBeginAtd().getArrowType());
    assertEquals(reactant1.getLine().getEndAtd().getArrowType(), reactant2.getLine().getEndAtd().getArrowType());
    assertEquals(reactant1.getLine().getLines().size(), reactant2.getLine().getLines().size());

    Product product1 = reaction1.getProducts().get(1);
    Product product2 = reaction2.getProducts().get(1);
    assertEquals(product1.getLine().getBeginAtd().getArrowType(), product2.getLine().getBeginAtd().getArrowType());
    assertEquals(product1.getLine().getEndAtd().getArrowType(), product2.getLine().getEndAtd().getArrowType());
    assertEquals(product1.getLine().getLines().size(), product2.getLine().getLines().size());

    assertTrue(reactant1.getLine().getEndPoint().distance(reactant2.getLine().getEndPoint()) < 1e-6);
    assertEquals(reaction1.getReactionRect(), reaction2.getReactionRect());

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }

    assertEquals(product1.getLine().getColor(), product2.getLine().getColor());
    assertEquals(product1.getLine().getWidth(), product2.getLine().getWidth(), 1e-6);
  }

  @Test
  public void testTranslationToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_translation.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    assertEquals(reaction1.getClass(), reaction2.getClass());
    assertEquals(reaction1.getReactants().size(), reaction2.getReactants().size());
    Reactant reactant1 = reaction1.getReactants().get(1);
    Reactant reactant2 = reaction2.getReactants().get(1);
    assertEquals(reactant1.getLine().getBeginAtd().getArrowType(), reactant2.getLine().getBeginAtd().getArrowType());
    assertEquals(reactant1.getLine().getEndAtd().getArrowType(), reactant2.getLine().getEndAtd().getArrowType());
    assertEquals(reactant1.getLine().getLines().size(), reactant2.getLine().getLines().size());

    Product product1 = reaction1.getProducts().get(1);
    Product product2 = reaction2.getProducts().get(1);
    assertEquals(product1.getLine().getBeginAtd().getArrowType(), product2.getLine().getBeginAtd().getArrowType());
    assertEquals(product1.getLine().getEndAtd().getArrowType(), product2.getLine().getEndAtd().getArrowType());
    assertEquals(product1.getLine().getLines().size(), product2.getLine().getLines().size());

    assertTrue(reactant1.getLine().getEndPoint().distance(reactant2.getLine().getEndPoint()) < 1e-6);
    assertEquals(reaction1.getReactionRect(), reaction2.getReactionRect());

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }

    assertEquals(product1.getLine().getColor(), product2.getLine().getColor());
    assertEquals(product1.getLine().getWidth(), product2.getLine().getWidth(), 1e-6);
  }

  @Test
  public void testTransportToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_transport.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
    assertEquals(reaction1.getMiriamData().size(), reaction2.getMiriamData().size());
    assertEquals(reaction1.getNotes().trim(), reaction2.getNotes().trim());
  }

  @Test
  public void testTruncationToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_truncation.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testTruncationWithModifierToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_truncationWithModifier.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }

    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testUnknownTransitionToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_unknown_transition.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testTransitionOmittedToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_transition_omitted.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testTranscriptionToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_transcription.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testTranscriptionWithAdditionsToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_transcription_with_additions.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testReactionWithOperatorsToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_with_operators.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testReactionWithModifiersToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_with_modifiers.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);

    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testReactionPositiveInfluenceToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_positive_influence.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testReactionHeterodimerWithAdditionsToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_heterodimer_with_additions.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    assertEquals(reaction1.getNodes().size(), reaction2.getNodes().size());

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    logger.debug(reaction1.getReactants().get(0).getLine());
    logger.debug(reaction1.getReactants().get(1).getLine());
    logger.debug(reaction1.getProducts().get(0).getLine());
    logger.debug(reaction1.getOperators().get(0).getLine());

    logger.debug("---");

    logger.debug(reaction2.getReactants().get(0).getLine());
    logger.debug(reaction2.getReactants().get(1).getLine());
    logger.debug(reaction2.getProducts().get(0).getLine());
    logger.debug(reaction2.getOperators().get(0).getLine());

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testReactionHeterodimerToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_heterodimer.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    assertEquals(reaction1.getNodes().size(), reaction2.getNodes().size());

    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  @Test
  public void testOperatorInReactionHeterodimer() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_heterodimer.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);
    for (final NodeOperator operator : reaction1.getOperators()) {
      assertTrue(operator instanceof AndOperator);
    }
  }

  @Test
  public void testReactionDissociationWithAdditionToXml() throws Exception {
    Model model = getModelFilledWithSpecies();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/reaction_dissociation_with_addition.xml");
    Node node = getNodeFromXmlString(xmlString);
    Reaction reaction1 = parser.getReaction(node, model);

    String xmlString2 = parser.toXml(reaction1);
    assertNotNull(xmlString2);
    // logger.debug(xmlString2);
    Node node2 = getNodeFromXmlString(xmlString2);
    Reaction reaction2 = parser.getReaction(node2, model);

    assertEquals(reaction1.getName(), reaction2.getName());
    assertNotNull(reaction1.getName());
    assertFalse(reaction1.getName().trim().equals(""));
    List<Line2D> lines1 = reaction1.getLines();
    List<Line2D> lines2 = reaction2.getLines();

    for (int i = 0; i < lines1.size(); i++) {
      assertEquals(lines1.get(i).getX1(), lines2.get(i).getX1(), 1e-6);
      assertEquals(lines1.get(i).getX2(), lines2.get(i).getX2(), 1e-6);
      assertEquals(lines1.get(i).getY1(), lines2.get(i).getY1(), 1e-6);
      assertEquals(lines1.get(i).getY2(), lines2.get(i).getY2(), 1e-6);
    }
    assertEquals(reaction1.getClass(), reaction2.getClass());
  }

  private Model getModelFilledWithSpecies() {
    Model model = new ModelFullIndexed(null);

    Species sa1 = new GenericProtein("sa1");
    sa1.setX(100.0);
    sa1.setY(200.0);
    sa1.setWidth(300.0);
    sa1.setHeight(400.0);
    model.addElement(sa1);
    elements.addModelElement(sa1, new CellDesignerGenericProtein("s1"));

    Species sa2 = new GenericProtein("sa2");
    sa2.setX(1050.0);
    sa2.setY(2050.0);
    sa2.setWidth(300.0);
    sa2.setHeight(450.0);
    model.addElement(sa2);
    elements.addModelElement(sa2, new CellDesignerGenericProtein("s2"));

    Species sa3 = new GenericProtein("sa3");
    sa3.setX(600.0);
    sa3.setY(250.0);
    sa3.setWidth(300.0);
    sa3.setHeight(400.0);
    model.addElement(sa3);
    elements.addModelElement(sa3, new CellDesignerGenericProtein("s3"));

    Species sa4 = new GenericProtein("sa4");
    sa4.setX(550.0);
    sa4.setY(350.0);
    sa4.setWidth(300.0);
    sa4.setHeight(450.0);
    model.addElement(sa4);
    elements.addElement(new CellDesignerGenericProtein("s4"));

    Species sa5 = new GenericProtein("sa5");
    sa5.setX(10.0);
    sa5.setY(250.0);
    sa5.setWidth(300.0);
    sa5.setHeight(450.0);
    model.addElement(sa5);
    elements.addElement(new CellDesignerGenericProtein("s5"));

    Species sa6 = new GenericProtein("sa6");
    sa6.setX(10.0);
    sa6.setY(250.0);
    sa6.setWidth(300.0);
    sa6.setHeight(450.0);
    model.addElement(sa6);

    elements.addElement(new CellDesignerGenericProtein("s6"));

    Species sa10 = new GenericProtein("sa10");
    sa10.setX(210.0);
    sa10.setY(220.0);
    sa10.setWidth(320.0);
    sa10.setHeight(250.0);
    model.addElement(sa10);
    elements.addElement(new CellDesignerGenericProtein("s10"));

    Species sa11 = new GenericProtein("sa11");
    sa11.setX(11.0);
    sa11.setY(320.0);
    sa11.setWidth(321.0);
    sa11.setHeight(150.0);
    model.addElement(sa11);
    elements.addElement(new CellDesignerGenericProtein("s11"));

    Species sa12 = new GenericProtein("sa12");
    sa12.setX(12.0);
    sa12.setY(20.0);
    sa12.setWidth(321.0);
    sa12.setHeight(150.0);
    model.addElement(sa12);
    elements.addElement(new CellDesignerGenericProtein("s12"));

    Species sa13 = new GenericProtein("sa13");
    sa13.setX(513.0);
    sa13.setY(20.0);
    sa13.setWidth(321.0);
    sa13.setHeight(150.0);
    model.addElement(sa13);
    elements.addElement(new CellDesignerGenericProtein("s13"));

    Species sa14 = new GenericProtein("sa14");
    sa14.setX(14.0);
    sa14.setY(820.0);
    sa14.setWidth(321.0);
    sa14.setHeight(150.0);
    model.addElement(sa14);
    elements.addElement(new CellDesignerGenericProtein("s14"));

    Species sa15 = new GenericProtein("sa15");
    sa15.setX(815.0);
    sa15.setY(620.0);
    sa15.setWidth(321.0);
    sa15.setHeight(150.0);
    model.addElement(sa15);
    elements.addElement(new CellDesignerGenericProtein("s15"));

    return model;
  }

  @Test
  public void testLogiGateAndReaction() throws Exception {
    Model model = getModelForFile("testFiles/reactions/logicGateAnd.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof ReducedTriggerReaction);
    assertEquals(1, reaction.getOperators().size());
    assertTrue(reaction.getOperators().get(0) instanceof AndOperator);
    assertEquals(2, reaction.getReactants().size());
    assertEquals(1, reaction.getProducts().size());

    assertEquals(0.0, reaction.getReactants().get(0).getLine().getEndPoint().distance(200.0, 127.0), EPSILON);
    assertEquals(0.0, reaction.getReactants().get(1).getLine().getEndPoint().distance(200.0, 127.0), EPSILON);
    assertEquals(0.0, reaction.getOperators().get(0).getLine().getStartPoint().distance(200.0, 127.0), EPSILON);

    super.testXmlSerialization(model);
  }

  @Test
  public void testLogiGateAndWithThreeInputsReaction() throws Exception {
    Model model = getModelForFile("testFiles/reactions/logicGateAndWithThreeInputs.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof ReducedTriggerReaction);

    super.testXmlSerialization(model);
  }

  @Test
  public void testLogiGateOrReaction() throws Exception {
    Model model = getModelForFile("testFiles/reactions/logicGateOr.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof NegativeInfluenceReaction);
    assertEquals(1, reaction.getOperators().size());
    assertTrue(reaction.getOperators().get(0) instanceof OrOperator);
    assertEquals(2, reaction.getReactants().size());
    assertEquals(1, reaction.getProducts().size());

    assertEquals(0.0, reaction.getReactants().get(0).getLine().getEndPoint().distance(200.0, 127.0), EPSILON);
    assertEquals(0.0, reaction.getReactants().get(1).getLine().getEndPoint().distance(200.0, 127.0), EPSILON);
    assertEquals(0.0, reaction.getOperators().get(0).getLine().getStartPoint().distance(200.0, 127.0), EPSILON);

    super.testXmlSerialization(model);
  }

  @Test
  public void testLogiGateNotReaction() throws Exception {
    Model model = getModelForFile("testFiles/reactions/logicGateNot.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof UnknownReducedPhysicalStimulationReaction);
    assertEquals(1, reaction.getOperators().size());
    assertTrue(reaction.getOperators().get(0) instanceof NandOperator);
    assertEquals(2, reaction.getReactants().size());
    assertEquals(1, reaction.getProducts().size());

    assertEquals(0.0, reaction.getReactants().get(0).getLine().getEndPoint().distance(200.0, 127.0), EPSILON);
    assertEquals(0.0, reaction.getReactants().get(1).getLine().getEndPoint().distance(200.0, 127.0), EPSILON);
    assertEquals(0.0, reaction.getOperators().get(0).getLine().getStartPoint().distance(200.0, 127.0), EPSILON);

    assertNotNull(reaction.getLine());

    super.testXmlSerialization(model);
  }

  @Test
  public void testLogiGateUnknownReaction() throws Exception {
    Model model = getModelForFile("testFiles/reactions/logicGateUnknown.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertTrue(reaction instanceof ReducedTriggerReaction);
    assertEquals(1, reaction.getOperators().size());
    assertTrue(reaction.getOperators().get(0) instanceof UnknownOperator);
    assertEquals(2, reaction.getReactants().size());
    assertEquals(1, reaction.getProducts().size());

    assertEquals(0.0, reaction.getReactants().get(0).getLine().getEndPoint().distance(200.0, 127.0), EPSILON);
    assertEquals(0.0, reaction.getReactants().get(1).getLine().getEndPoint().distance(200.0, 127.0), EPSILON);
    assertEquals(0.0, reaction.getOperators().get(0).getLine().getStartPoint().distance(200.0, 127.0), EPSILON);

    super.testXmlSerialization(model);
  }

  @Test
  public void testHeterodimerWithAdditionalReactant() throws Exception {
    Model model = getModelForFile("testFiles/reactions/heterodimer_association_with_additional_reactant.xml");
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();

    NodeOperator o1 = reaction.getOperators().get(0);
    NodeOperator o2 = reaction.getOperators().get(1);

    assertEquals(0, o1.getLine().getEndPoint().distance(o2.getLine().getStartPoint()), Configuration.EPSILON);
  }

  @Test
  public void testProblematicDrawing() throws Exception {
    Model model = getModelForFile("testFiles/problematic/reaction_drawing_problem.xml");
    Reaction reaction = model.getReactionByReactionId("re1");
    assertNotNull(reaction.getLine());
  }

  @Test
  public void testProteinsInsideComplex() throws Exception {
    Model model = getModelForFile("testFiles/problematic/proteins_inside_complex.xml");
    for (final Element el : model.getElements()) {
      assertFalse(el.getClass().equals(Protein.class));
    }
  }

  @Test
  public void testKinetcs() throws Exception {
    Model model = getModelForFile("testFiles/reactions/kinetics.xml");
    Reaction reaction = model.getReactionByReactionId("re1");
    assertNotNull(reaction.getKinetics());
  }

  @Test
  public void testKinetcsWithCompartment() throws Exception {
    Model model = getModelForFile("testFiles/reactions/kinetics_with_compartment.xml");
    Reaction reaction = model.getReactionByReactionId("re1");
    assertNotNull(reaction.getKinetics());
  }

  @Test
  public void testModifierWithOperator() throws Exception {
    Model model = getModelForFile("testFiles/reactions/modifier_with_operator.xml");
    Reaction reaction = model.getReactionByReactionId("re1");
    assertEquals(2, reaction.getOperators().iterator().next().getLine().getLines().size());
    assertEquals(2, reaction.getModifiers().get(0).getLine().getLines().size());
    assertEquals(2, reaction.getModifiers().get(1).getLine().getLines().size());
    Model model2 = super.serializeModel(model);
    reaction = model2.getReactionByReactionId("re1");
    assertEquals(2, reaction.getOperators().iterator().next().getLine().getLines().size());
    assertEquals(2, reaction.getModifiers().get(0).getLine().getLines().size());
    assertEquals(2, reaction.getModifiers().get(1).getLine().getLines().size());
  }

  @Test
  public void testProblematicHeterodimerAssociation() throws Exception {
    Model model = getModelForFile("testFiles/problematic/problematic_heterodimer_association.xml");
    Reaction reaction = model.getReactions().iterator().next();
    for (final Line2D line : reaction.getLines()) {
      assertTrue("Expected coordinates must be not negative. Found: " + line.getP1(), line.getP1().getX() >= 0);
      assertTrue("Expected coordinates must be not negative. Found: " + line.getP1(), line.getP1().getY() >= 0);
      assertTrue("Expected coordinates must be not negative. Found: " + line.getP2(), line.getP2().getX() >= 0);
      assertTrue("Expected coordinates must be not negative. Found: " + line.getP2(), line.getP2().getY() >= 0);
    }
  }

  @Test
  public void testGateReactionWithMoreParticipants() throws Exception {
    Model model = getModelForFile("testFiles/reactions/logicGateWithMoreParticipants.xml");
    Reaction reaction = model.getReactionByReactionId("re1");
    assertEquals(3, reaction.getReactants().size());
    Reactant r1 = reaction.getReactants().get(0);
    Reactant r2 = reaction.getReactants().get(1);
    Reactant r3 = reaction.getReactants().get(2);
    Product p = reaction.getProducts().get(0);

    assertEquals(2, r1.getLine().getLines().size());
    assertEquals(1, r2.getLine().getLines().size());
    assertEquals(2, r3.getLine().getLines().size());

    assertEquals(1, p.getLine().getLines().size());

    super.testXmlSerialization(model);
  }

}
