package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class GenericProteinTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CellDesignerGenericProtein());
  }

  @Test
  public void testConstructor() {
    CellDesignerGenericProtein species = new CellDesignerGenericProtein(new CellDesignerSpecies<GenericProtein>());
    assertNotNull(species);
  }

  @Test
  public void testCopy1() {
    CellDesignerGenericProtein species = new CellDesignerGenericProtein(new CellDesignerSpecies<GenericProtein>())
        .copy();
    assertNotNull(species);
  }

  @Test(expected = NotImplementedException.class)
  public void testCopy2() {
    CellDesignerGenericProtein protein = Mockito.spy(CellDesignerGenericProtein.class);
    protein.copy();
  }
}
