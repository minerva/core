package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;

public class PhenotypeTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CellDesignerPhenotype());
  }

  @Test
  public void testConstructor1() {
    CellDesignerPhenotype original = new CellDesignerPhenotype();
    CellDesignerPhenotype copy = new CellDesignerPhenotype(original);
    assertNotNull(copy);
  }

  @Test
  public void testCopy() {
    CellDesignerPhenotype degraded = new CellDesignerPhenotype().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    CellDesignerPhenotype phenotype = Mockito.spy(CellDesignerPhenotype.class);
    phenotype.copy();
  }

}
