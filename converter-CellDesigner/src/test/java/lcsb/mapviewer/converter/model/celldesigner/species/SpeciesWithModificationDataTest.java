package lcsb.mapviewer.converter.model.celldesigner.species;

import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.Residue;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SpeciesWithModificationDataTest extends CellDesignerTestFunctions {

  @Test
  public void addProteinWithResidues() {
    final SpeciesWithModificationData data = new SpeciesWithModificationData(new GenericProtein("1"), "1");

    final Protein protein = createProtein();
    protein.addResidue(createResidue(protein));
    protein.addResidue(createResidue(protein));

    data.addInfoFromSpecies(protein);

    assertEquals(2, data.getResidues().size());
  }

  @Test
  public void addProteinWithResiduesTwice() {
    final SpeciesWithModificationData data = new SpeciesWithModificationData(new GenericProtein("1"), "1");

    final Protein protein = createProtein();
    protein.addResidue(createResidue(protein));
    protein.addResidue(createResidue(protein));

    data.addInfoFromSpecies(protein);
    data.addInfoFromSpecies(protein);

    assertEquals(2, data.getResidues().size());
  }

  @Test
  public void addDifferentProteinWithResidues() {
    final SpeciesWithModificationData data = new SpeciesWithModificationData(new GenericProtein("1"), "1");

    final Protein protein = createProtein();
    protein.addResidue(createResidue(protein));

    final Protein protein2 = createProtein();
    final Residue residue = createResidue(protein2);
    residue.setName("xx");
    protein2.addResidue(residue);

    final Residue residue2 = createResidue(protein2);
    residue2.setX(protein2.getCenterX());
    residue2.setY(protein2.getCenterY());
    protein2.addResidue(residue2);

    data.addInfoFromSpecies(protein);
    data.addInfoFromSpecies(protein2);

    assertEquals(3, data.getResidues().size());
    logger.debug(data.getResidues());
  }

  @Test
  public void addProteinWithResiduesDifferentState() {
    final SpeciesWithModificationData data = new SpeciesWithModificationData(new GenericProtein("1"), "1");

    final Protein protein = createProtein();
    final Residue residue = createResidue(protein);
    residue.setState(ModificationState.ACETYLATED);
    protein.addResidue(residue);

    data.addInfoFromSpecies(protein);

    final Protein protein2 = createProtein();
    final Residue residue2 = createResidue(protein2);
    residue2.setState(ModificationState.PHOSPHORYLATED);
    protein2.addResidue(residue2);

    data.addInfoFromSpecies(protein2);

    assertEquals(1, data.getResidues().size());
  }

  @Test
  public void addProteinWithBindingRegions() {
    final SpeciesWithModificationData data = new SpeciesWithModificationData(new GenericProtein("1"), "1");

    final Protein protein = createProtein();
    protein.addBindingRegion(createBindingRegion(protein));
    protein.addBindingRegion(createBindingRegion(protein));

    data.addInfoFromSpecies(protein);

    assertEquals(2, data.getBindingRegions().size());
  }

  @Test
  public void addProteinWithBindingRegionTwice() {
    final SpeciesWithModificationData data = new SpeciesWithModificationData(new GenericProtein("1"), "1");

    final Protein protein = createProtein();
    protein.addBindingRegion(createBindingRegion(protein));
    protein.addBindingRegion(createBindingRegion(protein));

    data.addInfoFromSpecies(protein);
    data.addInfoFromSpecies(protein);

    assertEquals(2, data.getBindingRegions().size());
  }

  @Test
  public void addDifferentProteinWithBindingRegions() {
    final SpeciesWithModificationData data = new SpeciesWithModificationData(new GenericProtein("1"), "1");

    final Protein protein = createProtein();
    protein.addBindingRegion(createBindingRegion(protein));
    final BindingRegion bindingRegion = createBindingRegion(protein);
    bindingRegion.setWidth(30);
    protein.addBindingRegion(bindingRegion);

    final Protein protein2 = createProtein();
    final BindingRegion bindingRegion1 = createBindingRegion(protein2);
    bindingRegion1.setName("xx");
    protein2.addBindingRegion(bindingRegion1);

    final BindingRegion bindingRegion2 = createBindingRegion(protein2);
    bindingRegion2.setX(protein2.getCenterX());
    bindingRegion2.setY(protein2.getCenterY());
    protein2.addBindingRegion(bindingRegion2);

    data.addInfoFromSpecies(protein);
    data.addInfoFromSpecies(protein2);

    assertEquals(4, data.getBindingRegions().size());
  }

  @Test
  public void getAngleForResidue() {
    final Protein protein = createProtein();
    protein.addResidue(createResidue(protein));

    final SpeciesWithModificationData data = new SpeciesWithModificationData(protein, "1");

    final Double angle = new CellDesignerModificationResidue(data.getResidues().get(0)).getAngle();
    assertNotNull(angle);
  }

  @Test
  public void getAngleForBindingRegion() {
    final Protein protein = createProtein();
    protein.addBindingRegion(createBindingRegion(protein));

    final Double originalAngle = new CellDesignerModificationResidue(protein.getModificationResidues().get(0)).getAngle();

    final SpeciesWithModificationData data = new SpeciesWithModificationData(protein, "1");

    final Double angle = new CellDesignerModificationResidue(data.getBindingRegions().get(0)).getAngle();
    assertEquals(originalAngle, angle);
  }

  @Test
  public void getSizeForBindingRegion() {
    final Protein protein = createProtein();
    protein.addBindingRegion(createBindingRegion(protein));

    final Double originalSize = new CellDesignerModificationResidue(protein.getModificationResidues().get(0)).getSize();

    final SpeciesWithModificationData data = new SpeciesWithModificationData(protein, "1");

    final Double size = new CellDesignerModificationResidue(data.getBindingRegions().get(0)).getSize();
    assertEquals(originalSize, size);
  }


  @Test
  public void getIdOfModificationResidue() {
    final SpeciesWithModificationData data = new SpeciesWithModificationData(new GenericProtein("1"), "1");

    final Protein protein = createProtein();
    final BindingRegion region1 = createBindingRegion(protein);
    final BindingRegion region2 = createBindingRegion(protein);
    final BindingRegion region3 = createBindingRegion(protein);
    region3.setX(protein.getCenterX());
    protein.addBindingRegion(region1);
    protein.addBindingRegion(region2);
    protein.addBindingRegion(region3);

    data.addInfoFromSpecies(protein);

    final String newId1 = data.getModificationResidueIdFromOriginal(region1);
    final String newId2 = data.getModificationResidueIdFromOriginal(region2);
    final String newId3 = data.getModificationResidueIdFromOriginal(region3);

    assertEquals(data.getBindingRegions().get(0).getIdModificationResidue(), newId1);
    assertEquals(data.getBindingRegions().get(1).getIdModificationResidue(), newId2);
    assertEquals(data.getBindingRegions().get(2).getIdModificationResidue(), newId3);
  }

}