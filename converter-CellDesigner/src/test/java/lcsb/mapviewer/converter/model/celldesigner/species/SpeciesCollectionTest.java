package lcsb.mapviewer.converter.model.celldesigner.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGene;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGenericProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerSpecies;
import lcsb.mapviewer.model.map.species.Species;

public class SpeciesCollectionTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAdd() {
    SpeciesCollection<CellDesignerGene> coll = new SpeciesCollection<CellDesignerGene>();
    CellDesignerGene g = new CellDesignerGene();
    g.setElementId("general Id");
    coll.add(g, "geneId");
    assertNotNull(coll.getSpeciesListByLocalId("geneId"));
    assertEquals(1, coll.getSpeciesListByLocalId("geneId").size());
  }

  @Test
  public void testUpdate() {
    SpeciesCollection<CellDesignerProtein<?>> coll = new SpeciesCollection<>();
    CellDesignerProtein<?> protein = new CellDesignerGenericProtein();
    protein.setElementId("");
    coll.add(protein, "prot id");

    CellDesignerProtein<?> p2 = new CellDesignerGenericProtein();
    p2.setElementId("id2");
    coll.updateSpeciesByLocalId(p2, "prot id");
    assertEquals(1, coll.getSpeciesListByLocalId("prot id").size());
    CellDesignerProtein<?> p3 = coll.getSpeciesListByLocalId("prot id").get(0);

    assertEquals(protein, p3);
  }

  @Test
  public void testUpdateWithTypeChange() {
    SpeciesCollection<CellDesignerProtein<?>> coll = new SpeciesCollection<>();
    CellDesignerProtein<?> p = new CellDesignerProtein<>();
    p.setElementId("");
    coll.add(p, "prot id");

    CellDesignerGenericProtein p2 = new CellDesignerGenericProtein();
    p2.setElementId("id2");
    coll.updateSpeciesByLocalId(p2, "prot id");
    assertEquals(1, coll.getSpeciesListByLocalId("prot id").size());
    CellDesignerProtein<?> p3 = coll.getSpeciesListByLocalId("prot id").get(0);

    // we don't have generic data anymore but new instance
    assertFalse(p3.equals(p));
    assertFalse(p3.equals(p2));
  }

  @Test(expected = InvalidStateException.class)
  public void testCreateNewInstance() {
    SpeciesCollection<CellDesignerSpecies<?>> coll = new SpeciesCollection<>();
    class InvalidSpecies extends CellDesignerSpecies<Species> {
      private static final long serialVersionUID = 1L;

      public InvalidSpecies() {
      }

      @SuppressWarnings("unused")
      public InvalidSpecies(final CellDesignerSpecies<?> sp) {
        throw new NotImplementedException();
      }
    }

    InvalidSpecies p = new InvalidSpecies();
    p.setElementId("");

    coll.createNewInstance(p.getClass(), p);
  }
}
