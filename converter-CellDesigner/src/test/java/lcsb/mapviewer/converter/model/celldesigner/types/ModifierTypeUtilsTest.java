package lcsb.mapviewer.converter.model.celldesigner.types;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;

public class ModifierTypeUtilsTest extends CellDesignerTestFunctions {

  private ModifierTypeUtils utils = new ModifierTypeUtils();

  private Modifier invalidModifier = Mockito.mock(Modifier.class, Mockito.CALLS_REAL_METHODS);

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetInvalidModifierTypeForClazz() {
    ModifierType type = utils.getModifierTypeForClazz(invalidModifier.getClass());
    assertNull(type);
  }

  @Test
  public void testGetInvalidStringTypeByModifier() {
    String type = utils.getStringTypeByModifier(invalidModifier);
    assertNull(type);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetTargetLineIndexByInvalidModifier() {
    utils.getTargetLineIndexByModifier(invalidModifier);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetTargetLineIndexByInvalidModifier2() {
    NodeOperator operator = new AndOperator();
    operator.addInput(new Reactant(createProtein()));
    utils.getTargetLineIndexByModifier(operator);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetTargetLineIndexByInvalidModifier3() {
    NodeOperator operator = new AndOperator();
    operator.addInput(invalidModifier);
    utils.getTargetLineIndexByModifier(operator);
  }

  @Test
  public void testGetAnchorPointOnReactionRectByInvalidType() {
    Reaction reaction = createDummyReaction();
    Point2D point = utils.getAnchorPointOnReactionRect(reaction, "0,0");
    assertNotNull(point);
  }

  @Test
  public void testGetAnchorPointOnReactionRectByInvalidType2() {
    Reaction reaction = createDummyReaction();
    utils.getAnchorPointOnReactionRect(reaction, "0");

    assertEquals(1, getWarnings().size());
  }

  private Reaction createDummyReaction() {
    Reaction reaction = new Reaction("re");
    Reactant reactant = new Reactant(createProtein());
    Product product = new Product(createProtein());
    reactant.setLine(new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(1, 0)));
    product.setLine(new PolylineData(new Point2D.Double(12, 0), new Point2D.Double(13, 0)));
    reaction.addReactant(reactant);
    reaction.addProduct(product);
    reaction.setLine(new PolylineData(new Point2D.Double(1, 0), new Point2D.Double(12, 0)));
    return reaction;
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetAnchorPointOnReactionRectByInvalidType3() {
    Reaction reaction = createDummyReaction();
    utils.getAnchorPointOnReactionRect(reaction, "10,10");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testUpdateLineEndPointForInvalidModifier() {
    utils.updateLineEndPoint(invalidModifier);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testUpdateLineEndPointForInvalidModifier2() {
    NodeOperator operator = new AndOperator();
    operator.addInput(new Product(createProtein()));
    operator.addInput(invalidModifier);
    utils.updateLineEndPoint(operator);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testUpdateLineEndPointForInvalidModifier3() {
    NodeOperator operator = new AndOperator();
    operator.addInput(new Product(createProtein()));
    operator.addInput(new Product(createProtein()));
    utils.updateLineEndPoint(operator);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testCreateModifierForStringType2() throws Exception {
    utils.createModifierForStringType("unjkType", null);
  }

}
