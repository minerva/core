package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.species.Drug;

public class DrugTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CellDesignerDrug());
  }

  @Test
  public void testConstructor1() {
    CellDesignerDrug degraded = new CellDesignerDrug(new CellDesignerSpecies<Drug>());
    assertNotNull(degraded);
  }

  @Test
  public void testCopy() {
    CellDesignerDrug degraded = new CellDesignerDrug().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    CellDesignerDrug drug = Mockito.spy(CellDesignerDrug.class);
    drug.copy();
  }

}
