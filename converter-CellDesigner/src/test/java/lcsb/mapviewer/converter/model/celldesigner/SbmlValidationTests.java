package lcsb.mapviewer.converter.model.celldesigner;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Protein;

@RunWith(Parameterized.class)
public class SbmlValidationTests extends CellDesignerTestFunctions {

  private Model model;
  private String filename;

  public SbmlValidationTests(final String filename, final Model model) {
    this.filename = filename;
    this.model = model;
  }

  @Parameters(name = "{0}")
  public static Collection<Object[]> data() throws Exception {
    List<Object[]> result = new ArrayList<>();
    result.add(getTestSetFromFile("testFiles/empty.xml"));
    result.add(getTestSetFromFile("testFiles/parameter.xml"));
    result.add(getTestSetFromFile("testFiles/model_with_annotations.xml"));
    result.add(getTestSetFromFile("testFiles/reactions/modifier_with_operator.xml"));
    result.add(getTestSetFromFile("testFiles/problematic/problematic_reaction_name.xml"));
    result.add(getTestSetFromFile("testFiles/reactions/kinetics.xml"));
    result.add(getTestSetFromFile("testFiles/problematic/invalid_sbml_id.xml"));
    result.add(emptyProteinNameTest());
    return result;
  }

  private static Object[] emptyProteinNameTest() {
    Model model = new ModelFullIndexed(null);
    model.setWidth(200);
    model.setHeight(200);
    Protein protein = createProtein();
    protein.setName("");
    model.addElement(protein);
    return new Object[] { "empty-protein-name", model };
  }

  private static Object[] getTestSetFromFile(final String string) throws Exception {
    return new Object[] { string, getModelForFile(string) };
  }

  @Test
  @Ignore("sbml validatior is down")
  public void testIsValidSbml() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    String xml = parser.model2String(model);

    CloseableHttpClient httpClient = HttpClients.createDefault();
    HttpPost uploadFile = new HttpPost("http://sbml.org/validator/");
    MultipartEntityBuilder builder = MultipartEntityBuilder.create();
    builder.addTextBody("file", xml, ContentType.TEXT_PLAIN);
    builder.addBinaryBody(
        "file",
        new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)),
        ContentType.APPLICATION_OCTET_STREAM,
        filename);
    builder.addTextBody("output", "xml", ContentType.TEXT_PLAIN);
    builder.addTextBody("offcheck", "u,r", ContentType.TEXT_PLAIN);

    HttpEntity multipart = builder.build();
    uploadFile.setEntity(multipart);
    CloseableHttpResponse response = httpClient.execute(uploadFile);
    String responseXml = EntityUtils.toString(response.getEntity());
    Document document = XmlParser.getXmlDocumentFromString(responseXml);
    List<Node> problems = XmlParser.getAllNotNecessirellyDirectChild("problem", document);
    if (problems.size() > 0) {
      logger.debug(xml);
      logger.debug(responseXml);
    }
    assertEquals("SBML is invalid", 0, problems.size());
  }

}
