package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.species.AntisenseRna;

public class ComplexSpeciesTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CellDesignerComplexSpecies());
  }

  @Test
  public void testConstructor1() {
    CellDesignerComplexSpecies degraded = new CellDesignerComplexSpecies(new CellDesignerSpecies<AntisenseRna>());
    assertNotNull(degraded);
  }

  @Test
  public void testGetters() {
    CellDesignerComplexSpecies species = new CellDesignerComplexSpecies();
    species.addElement(new CellDesignerSpecies<AntisenseRna>());
    CellDesignerComplexSpecies complex = new CellDesignerComplexSpecies(species);

    Set<CellDesignerElement<?>> elements = new HashSet<>();
    complex.setElements(elements);
    assertEquals(elements, complex.getElements());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testAddElement() {
    CellDesignerComplexSpecies species = new CellDesignerComplexSpecies();
    species.addElement(new CellDesignerSpecies<AntisenseRna>());
    species.addElement(new CellDesignerSpecies<AntisenseRna>());
  }

  @Test
  public void testSetStructuralState() {
    CellDesignerComplexSpecies species = new CellDesignerComplexSpecies();
    species.setStructuralState("a");
    species.setStructuralState("b");
    assertEquals("b", species.getStructuralState());
  }

  @Test
  public void testGetAllSimpleChildren() {
    CellDesignerComplexSpecies species = new CellDesignerComplexSpecies();
    species.addElement(new CellDesignerSpecies<AntisenseRna>());
    CellDesignerComplexSpecies complex = new CellDesignerComplexSpecies("a");
    complex.addElement(new CellDesignerSpecies<AntisenseRna>("s"));
    complex.addElement(new CellDesignerSpecies<AntisenseRna>("d"));
    species.addElement(complex);
    assertEquals(3, species.getAllSimpleChildren().size());
  }

  @Test
  public void testCopy() {
    CellDesignerComplexSpecies degraded = new CellDesignerComplexSpecies().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    CellDesignerComplexSpecies complex = Mockito.spy(CellDesignerComplexSpecies.class);
    complex.copy();
  }

  @Test(expected = NotImplementedException.class)
  public void testCreateInvalidElement() {
    CellDesignerComplexSpecies complex = new CellDesignerComplexSpecies();
    complex.addElement(new CellDesignerGene());
    complex.createModelElement();
  }

}
