package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.field.ModificationState;

public class AntisenseRnaTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CellDesignerAntisenseRna());
  }

  @Test
  public void testConstructor1() {
    CellDesignerAntisenseRna original = new CellDesignerAntisenseRna();
    original.addRegion(new CellDesignerModificationResidue());
    CellDesignerAntisenseRna antisenseRna = new CellDesignerAntisenseRna(original);
    assertNotNull(antisenseRna);
  }

  @Test
  public void testAddRnaRegion() {
    CellDesignerAntisenseRna original = new CellDesignerAntisenseRna();
    CellDesignerModificationResidue region = new CellDesignerModificationResidue();
    region.setIdModificationResidue("id1");
    original.addRegion(region);

    CellDesignerModificationResidue region2 = new CellDesignerModificationResidue();
    region2.setIdModificationResidue("id1");
    region2.setName("nam");
    original.addRegion(region2);

    assertEquals(1, original.getRegions().size());

    assertEquals("nam", original.getRegions().get(0).getName());

    CellDesignerModificationResidue region3 = new CellDesignerModificationResidue();
    region3.setIdModificationResidue("id2");
    region3.setName("nam");
    original.addRegion(region3);

    assertEquals(2, original.getRegions().size());
  }

  @Test
  public void testUpdate() {
    CellDesignerAntisenseRna original = new CellDesignerAntisenseRna();
    CellDesignerModificationResidue region2 = new CellDesignerModificationResidue();
    region2.setIdModificationResidue("id1");
    region2.setName("nam");
    original.addRegion(region2);
    CellDesignerModificationResidue region3 = new CellDesignerModificationResidue();
    region3.setIdModificationResidue("id2");
    region3.setName("nam");
    original.addRegion(region3);

    CellDesignerAntisenseRna copy = new CellDesignerAntisenseRna(original);
    copy.addRegion(new CellDesignerModificationResidue());
    copy.getRegions().get(0).setState(ModificationState.ACETYLATED);

    original.update(copy);

    boolean acetylatedFound = false;
    for (final CellDesignerModificationResidue region : copy.getRegions()) {
      if (ModificationState.ACETYLATED.equals(region.getState())) {
        acetylatedFound = true;
      }
    }
    assertTrue(acetylatedFound);
    assertEquals(3, copy.getRegions().size());

    original.update(new CellDesignerGenericProtein());
  }

  @Test
  public void testGetters() {
    CellDesignerAntisenseRna antisenseRna = new CellDesignerAntisenseRna(new CellDesignerSpecies<AntisenseRna>());

    List<CellDesignerModificationResidue> regions = new ArrayList<>();

    antisenseRna.setRegions(regions);

    assertEquals(regions, antisenseRna.getRegions());
  }

  @Test
  public void testCopy() {
    CellDesignerAntisenseRna antisenseRna = new CellDesignerAntisenseRna().copy();
    assertNotNull(antisenseRna);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    CellDesignerAntisenseRna antisenseRna = Mockito.spy(CellDesignerAntisenseRna.class);
    antisenseRna.copy();
  }

}
