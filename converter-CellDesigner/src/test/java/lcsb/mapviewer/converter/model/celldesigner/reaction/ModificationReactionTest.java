package lcsb.mapviewer.converter.model.celldesigner.reaction;

import static org.junit.Assert.assertEquals;

import java.io.FileInputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.type.CatalysisReaction;
import lcsb.mapviewer.model.map.reaction.type.InhibitionReaction;
import lcsb.mapviewer.model.map.reaction.type.ModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.PhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.TriggerReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownCatalysisReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownInhibitionReaction;

public class ModificationReactionTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testCatalysisReaction() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    FileInputStream fis = new FileInputStream("testFiles/reactions/modification_reaction/catalysis.xml");
    Model model = parser.createModel(new ConverterParams().inputStream(fis));
    assertEquals(CatalysisReaction.class, model.getReactionByReactionId("re1").getClass());
  }

  @Test
  public void testInhibitionReaction() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    FileInputStream fis = new FileInputStream("testFiles/reactions/modification_reaction/inhibition.xml");
    Model model = parser.createModel(new ConverterParams().inputStream(fis));
    assertEquals(InhibitionReaction.class, model.getReactionByReactionId("re2").getClass());
  }

  @Test
  public void testModulationReaction() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    FileInputStream fis = new FileInputStream("testFiles/reactions/modification_reaction/modulation.xml");
    Model model = parser.createModel(new ConverterParams().inputStream(fis));
    assertEquals(ModulationReaction.class, model.getReactionByReactionId("re1").getClass());
  }

  @Test
  public void testPhysicalStimulationReaction() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    FileInputStream fis = new FileInputStream("testFiles/reactions/modification_reaction/physical_stimulation.xml");
    Model model = parser.createModel(new ConverterParams().inputStream(fis));
    assertEquals(PhysicalStimulationReaction.class, model.getReactionByReactionId("re1").getClass());
  }

  @Test
  public void testTriggerReaction() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    FileInputStream fis = new FileInputStream("testFiles/reactions/modification_reaction/trigger.xml");
    Model model = parser.createModel(new ConverterParams().inputStream(fis));
    assertEquals(TriggerReaction.class, model.getReactionByReactionId("re1").getClass());
  }

  @Test
  public void testUnknownCatalysisReaction() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    FileInputStream fis = new FileInputStream("testFiles/reactions/modification_reaction/unknown_catalysis.xml");
    Model model = parser.createModel(new ConverterParams().inputStream(fis));
    assertEquals(UnknownCatalysisReaction.class, model.getReactionByReactionId("re1").getClass());
  }

  @Test
  public void testUnknownInhibitionReaction() throws Exception {
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    FileInputStream fis = new FileInputStream("testFiles/reactions/modification_reaction/unknown_inhibition.xml");
    Model model = parser.createModel(new ConverterParams().inputStream(fis));
    assertEquals(UnknownInhibitionReaction.class, model.getReactionByReactionId("re1").getClass());
  }
}
