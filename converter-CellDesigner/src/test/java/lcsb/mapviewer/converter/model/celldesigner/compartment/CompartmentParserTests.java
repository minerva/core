package lcsb.mapviewer.converter.model.celldesigner.compartment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class CompartmentParserTests extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseCompartmensBottom() throws Exception {
    Model model = getModelForFile("testFiles/compartment/bottom_compartment.xml");
    Compartment c = model.getElementByElementId("ca4");
    assertTrue(c.getY() > 0);
    assertTrue(c.getWidth() > 0);
    assertTrue(c.getHeight() > 0);
    assertEquals(0, c.getX(), Configuration.EPSILON);

    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testParseCompartmensTop() throws Exception {
    Model model = getModelForFile("testFiles/compartment/top_compartment.xml");

    Compartment c = model.getElementByElementId("ca3");
    assertEquals(0, c.getX(), Configuration.EPSILON);
    assertEquals(0, c.getY(), Configuration.EPSILON);
    assertTrue(c.getWidth() > 0);
    assertTrue(c.getHeight() < model.getHeight());
    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testParseCompartmensRight() throws Exception {
    Model model = getModelForFile("testFiles/compartment/right_compartment.xml");
    Compartment c = model.getElementByElementId("ca2");
    assertTrue(c.getX() > 0);
    assertTrue(c.getWidth() > 0);
    assertTrue(c.getHeight() > 0);
    assertEquals(0, c.getY(), Configuration.EPSILON);
    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testParseCompartmensLeft() throws Exception {
    Model model = getModelForFile("testFiles/compartment/left_compartment.xml");
    Compartment c = model.getElementByElementId("ca1");
    assertEquals(0, c.getX(), Configuration.EPSILON);
    assertEquals(0, c.getY(), Configuration.EPSILON);
    assertTrue(c.getWidth() < model.getWidth());
    assertTrue(c.getHeight() > 0);

    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testParseCompartmentBottomRight() throws Exception {
    Model model = getModelForFile("testFiles/compartment/bottom_right_compartment.xml");
    Compartment c = model.getElementByElementId("ca1");
    assertTrue(c.getX() > 0);
    assertTrue(c.getY() > 0);
    assertTrue(c.getWidth() > 0);
    assertTrue(c.getHeight() > 0);

    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testParseCompartmentBottomLeft() throws Exception {
    Model model = getModelForFile("testFiles/compartment/bottom_left_compartment.xml");
    Compartment c = model.getElementByElementId("ca1");
    assertEquals(c.getX(), 0, Configuration.EPSILON);
    assertTrue(c.getY() > 0);
    assertTrue(c.getWidth() > 0);
    assertTrue(c.getHeight() > 0);

    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testParseCompartmentTopLeft() throws Exception {
    Model model = getModelForFile("testFiles/compartment/top_left_compartment.xml");
    Compartment c = model.getElementByElementId("ca1");
    assertEquals(c.getX(), 0, Configuration.EPSILON);
    assertEquals(c.getY(), 0, Configuration.EPSILON);
    assertTrue(c.getWidth() > 0);
    assertTrue(c.getHeight() > 0);

    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testParseCompartmentTopRight() throws Exception {
    Model model = getModelForFile("testFiles/compartment/top_right_compartment.xml");
    Compartment c = model.getElementByElementId("ca1");
    assertTrue(c.getX() > 0);
    assertEquals(c.getY(), 0, Configuration.EPSILON);
    assertTrue(c.getWidth() > 0);
    assertTrue(c.getHeight() > 0);

    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testOldCellDesignerCompartment() throws Exception {
    Model model = getModelForFile("testFiles/compartment/old_compartment.xml");
    Compartment c = model.getElementByElementId("ca1");
    assertNotNull(c);

    assertEquals(1, model.getElements().size());
  }

  @Test
  public void testToXmlWithCompartmentIdNotRecognizedByCellDesigner() throws Exception {
    Model model = new ModelFullIndexed(null);
    Compartment compartment = new SquareCompartment("c");
    compartment.setName("xyz");
    assignCoordinates(compartment);
    model.addElement(compartment);

    Model model2 = super.serializeModel(model);
    assertTrue(model2.getCompartments().get(0).getElementId().length() >= 2);
  }

  @Test
  public void testToXmlWithCompartmentIdNotRecognizedByCellDesignerAndChildren() throws Exception {
    Model model = new ModelFullIndexed(null);
    Compartment compartment = new SquareCompartment("c");
    assignCoordinates(compartment);
    compartment.setName("xyz");
    model.addElement(compartment);

    Complex complex = new Complex("comp");
    GenericProtein protein = new GenericProtein("prot1");
    complex.addSpecies(protein);

    model.addElement(complex);
    model.addElement(protein);

    compartment.addElement(complex);

    GenericProtein protein2 = new GenericProtein("prot2");
    compartment.addElement(protein2);
    model.addElement(protein2);

    Model model2 = super.serializeModel(model);
    Compartment compartment2 = model2.getCompartments().get(0);

    assertEquals(compartment2, model2.getElementByElementId("prot2").getCompartment());
    assertEquals(compartment2, model2.getElementByElementId("comp").getCompartment());
  }

}
