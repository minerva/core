package lcsb.mapviewer.converter.model.celldesigner;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.graphics.PolylineDataComparator;
import lcsb.mapviewer.model.map.layout.ElementGroup;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerComparator;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.model.map.layout.graphics.LayerOvalComparator;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.layout.graphics.LayerRectComparator;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.layout.graphics.LayerTextComparator;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class LayerXmlParserTest extends CellDesignerTestFunctions {

  private final LayerXmlParser parser = new LayerXmlParser();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseLayers() throws Exception {
    LayerComparator comparator = new LayerComparator();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/layer_collection.xml");
    Node node = getNodeFromXmlString(xmlString);
    Collection<Layer> layers = parser.parseLayers(node);
    assertNotNull(layers);
    assertEquals(1, layers.size());

    xmlString = parser.layerCollectionToXml(layers);
    node = getNodeFromXmlString(xmlString);
    Collection<Layer> layers2 = parser.parseLayers(node);
    assertNotNull(layers2);
    assertEquals(1, layers2.size());
    assertEquals(0, comparator.compare(layers.iterator().next(), layers2.iterator().next()));
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidLayers() throws Exception {
    String xmlString = readFile("testFiles/invalid/layer_collection.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseLayers(node);
  }

  @Test
  public void testParseBlockDiagrams() throws Exception {
    Model model = new ModelFullIndexed(null);
    String xmlString = readFile("testFiles/xmlNodeTestExamples/block_diagrams.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseBlocks(model, node);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidBlockDiagrams() throws Exception {
    Model model = new ModelFullIndexed(null);
    String xmlString = readFile("testFiles/invalid/block_diagrams.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseBlocks(model, node);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidGroups() throws Exception {
    Model model = new ModelFullIndexed(null);
    String xmlString = readFile("testFiles/invalid/group_collection.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseGroups(model, node);
  }

  @Test(expected = InvalidGroupException.class)
  public void testParseInvalidGroups2() throws Exception {
    Model model = new ModelFullIndexed(null);
    String xmlString = readFile("testFiles/invalid/group_collection2.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseGroups(model, node);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidGroups3() throws Exception {
    Model model = new ModelFullIndexed(null);
    GenericProtein alias = new GenericProtein("sa1");
    model.addElement(alias);

    String xmlString = readFile("testFiles/invalid/group_collection3.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseGroups(model, node);
  }

  @Test
  public void testGetLayer() throws Exception {
    LayerComparator comparator = new LayerComparator();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/layer.xml");
    Node node = getNodeFromXmlString(xmlString);
    Layer layer = parser.getLayer(node);
    assertNotNull(layer);

    xmlString = parser.layerToXml(layer);
    node = getNodeFromXmlString(xmlString);
    Layer layers2 = parser.getLayer(node);
    assertNotNull(layers2);
    assertEquals(0, comparator.compare(layer, layers2));
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidLayer() throws Exception {
    String xmlString = readFile("testFiles/invalid/layer.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.getLayer(node);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidLayer2() throws Exception {
    String xmlString = readFile("testFiles/invalid/layer2.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.getLayer(node);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidLayer3() throws Exception {
    String xmlString = readFile("testFiles/invalid/layer3.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.getLayer(node);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidLayer4() throws Exception {
    String xmlString = readFile("testFiles/invalid/layer4.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.getLayer(node);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidLayer5() throws Exception {
    String xmlString = readFile("testFiles/invalid/layer5.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.getLayer(node);
  }

  @Test
  public void testGetLayerRect() throws Exception {
    LayerRectComparator comparator = new LayerRectComparator();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/layer_square.xml");
    Node node = getNodeFromXmlString(xmlString);
    LayerRect layer = parser.getLayerRect(node);
    assertNotNull(layer);

    xmlString = parser.layerRectToXml(layer);
    node = getNodeFromXmlString(xmlString);
    LayerRect layers2 = parser.getLayerRect(node);
    assertNotNull(layers2);
    assertEquals(0, comparator.compare(layer, layers2));
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidLayerRect() throws Exception {
    String xmlString = readFile("testFiles/invalid/layer_square.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.getLayerRect(node);
  }

  @Test
  public void testGetLayerLine() throws Exception {
    PolylineDataComparator comparator = new PolylineDataComparator();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/layer_line.xml");
    Node node = getNodeFromXmlString(xmlString);
    PolylineData layer = parser.getLayerLine(node);
    assertNotNull(layer);

    xmlString = parser.layerLineToXml(layer);
    node = getNodeFromXmlString(xmlString);
    PolylineData layers2 = parser.getLayerLine(node);
    assertNotNull(layers2);
    assertEquals(0, comparator.compare(layer, layers2));
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidLayerLine() throws Exception {
    String xmlString = readFile("testFiles/invalid/layer_line.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.getLayerLine(node);
  }

  @Test
  public void testGetLayerOval() throws Exception {
    LayerOvalComparator comparator = new LayerOvalComparator();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/layer_oval.xml");
    Node node = getNodeFromXmlString(xmlString);
    LayerOval layer = parser.getLayerOval(node);
    assertNotNull(layer);

    xmlString = parser.layerOvalToXml(layer);
    node = getNodeFromXmlString(xmlString);
    LayerOval layers2 = parser.getLayerOval(node);
    assertNotNull(layers2);
    assertEquals(0, comparator.compare(layer, layers2));
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidLayerOval() throws Exception {
    String xmlString = readFile("testFiles/invalid/layer_oval.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.getLayerOval(node);
  }

  @Test
  public void testGetLayerText() throws Exception {
    LayerTextComparator comparator = new LayerTextComparator();
    String xmlString = readFile("testFiles/xmlNodeTestExamples/layer_text.xml");
    Node node = getNodeFromXmlString(xmlString);
    LayerText layer = parser.getLayerText(node);
    assertNotNull(layer);

    xmlString = parser.layerTextToXml(layer);
    node = getNodeFromXmlString(xmlString);
    LayerText layers2 = parser.getLayerText(node);
    assertNotNull(layers2);
    assertEquals(0, comparator.compare(layer, layers2));
  }

  @Test
  public void testGetLayerTextWithZIndex() throws Exception {
    String xmlString = readFile("testFiles/xmlNodeTestExamples/layer_text_with_z_index.xml");
    Node node = getNodeFromXmlString(xmlString);
    LayerText layer = parser.getLayerText(node);
    assertEquals((Integer) 19, layer.getZ());
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidLayerText() throws Exception {
    String xmlString = readFile("testFiles/invalid/layer_text.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.getLayerText(node);
  }

  @Test
  public void testGetAliasGroup() throws Exception {
    Model model = new ModelFullIndexed(null);
    Species alias = new GenericProtein("sa1035");
    model.addElement(alias);

    alias = new GenericProtein("sa1036");
    model.addElement(alias);

    alias = new GenericProtein("sa1037");
    model.addElement(alias);

    alias = new GenericProtein("sa1038");
    model.addElement(alias);

    String string = "<celldesigner:group id=\"g74\" members=\"sa1035,sa1036,sa1037,sa1038\"/>";
    Node node = getNodeFromXmlString(string);
    ElementGroup group = parser.getAliasGroup(node, model);
    assertNotNull(group);

    assertEquals(4, group.getElements().size());

    assertEquals("g74", group.getIdGroup());
  }

  @Test
  public void testLayerTextToXml() throws Exception {
    LayerText text = new LayerText(new Rectangle2D.Double(1, 2, 3, 4), "DF");
    String xml = parser.layerTextToXml(text);
    assertNotNull(xml);
  }

  @Test
  public void testRemoveBackgroundColor() throws Exception {
    String notes = parser.removeBackgroundColor("dcxvxcvxcvx\nBackgroundColor=#ccffcc");
    assertFalse(notes.contains("BackgroundColor"));
  }

  @Test
  public void testRemoveBackgroundColorWithColon() throws Exception {
    String notes = parser.removeBackgroundColor("dcxvxcvxcvx\nBackgroundColor:#ccffcc");
    assertFalse(notes.contains("BackgroundColor"));
  }

  @Test
  public void testRemoveNonExistingBackgroundColor() throws Exception {
    String notes = parser.removeBackgroundColor("dcxvxcvxcvx\nBckgroundColor=#ccffcc");
    assertTrue(notes.contains("dcxvxcvxcvx\nBckgroundColor=#ccffcc"));
  }

  @Test
  public void testLayerTextToXmlWithDefaultBackgroundColor() throws Exception {
    LayerText text = new LayerText();
    String xml = parser.layerTextToXml(text);
    assertFalse(xml.contains("BackgroundColor"));
  }

  @Test
  public void testLayerTextToXmlWithCustomDefaultBackgroundColor() throws Exception {
    LayerText text = new LayerText();
    text.setBackgroundColor(Color.BLUE);
    text.setBorderColor(Color.YELLOW);
    String xml = parser.layerTextToXml(text);
    assertTrue(xml.contains("BackgroundColor"));
    assertTrue(xml.contains("BorderColor"));
  }

  @Test
  public void testInvalidId() throws Exception {
    Model model = createEmptyModel();

    Layer layer = createLayer();
    layer.setLayerId("bla");
    layer.setName("xyz");
    model.addLayer(layer);

    Model serializedModel = super.serializeModel(model);

    ModelComparator comparator = new ModelComparator();
    assertNotEquals(0, comparator.compare(model, serializedModel));
    assertNotEquals(layer.getLayerId(), serializedModel.getLayers().iterator().next().getLayerId());
  }

}
