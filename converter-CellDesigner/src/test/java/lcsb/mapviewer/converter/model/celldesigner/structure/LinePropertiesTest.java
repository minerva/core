package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.converter.model.celldesigner.structure.fields.LineProperties;

public class LinePropertiesTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    String type = "str_t";
    LineProperties lp = new LineProperties();
    lp.setType(type);
    assertEquals(type, lp.getType());
  }

}
