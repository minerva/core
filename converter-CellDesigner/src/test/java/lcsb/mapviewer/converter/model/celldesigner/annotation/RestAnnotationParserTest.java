package lcsb.mapviewer.converter.model.celldesigner.annotation;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import org.apache.xerces.dom.DocumentImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.awt.Color;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class RestAnnotationParserTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void test() throws Exception {
    RestAnnotationParser rap = new RestAnnotationParser();

    String response = readFile("testFiles/annotation/sampleDescription.txt");

    List<String> synonyms = rap.getSynonyms(response);
    assertEquals(3, synonyms.size());
    assertTrue(synonyms.contains("CAMK"));
    assertTrue(synonyms.contains("CAMKIV"));

    List<String> formerSymbols = rap.getFormerSymbols(response);
    assertEquals(0, formerSymbols.size());
  }

  @Test
  public void testParseNotes() throws Exception {
    RestAnnotationParser parser = new RestAnnotationParser();
    Document node = getXmlDocumentFromFile("testFiles/xmlNodeTestExamples/notes.xml");
    Species proteinAlias = new GenericProtein("id");
    parser.processNotes(node.getFirstChild(), proteinAlias);
    String notes = proteinAlias.getNotes();
    assertNotNull(notes);
    assertFalse(notes.contains("body"));
    assertTrue(notes.contains("some"));
    // after redoing it should be the same
    parser.processNotes(node.getFirstChild(), proteinAlias);
    notes = proteinAlias.getNotes();
    assertNotNull(notes);
    assertFalse(notes.contains("body"));
    assertTrue(notes.contains("some"));
  }

  @Test
  public void testParseNotes2() throws Exception {
    RestAnnotationParser parser = new RestAnnotationParser();
    Document node = getXmlDocumentFromFile("testFiles/xmlNodeTestExamples/notes3.xml");
    Species proteinAlias = new GenericProtein("id");
    parser.processNotes(node.getFirstChild(), proteinAlias);
    String notes = proteinAlias.getNotes();
    assertNotNull(notes);
    assertFalse(notes.contains("body"));
    assertTrue(notes.contains("some"));
    // after redoing it should be the same
    parser.processNotes(node.getFirstChild(), proteinAlias);
    notes = proteinAlias.getNotes();
    assertNotNull(notes);
    assertFalse(notes.contains("body"));
    assertTrue(notes.contains("some"));
  }

  @Test
  public void testParseNotes3() throws Exception {
    RestAnnotationParser parser = new RestAnnotationParser();
    Document node = getXmlDocumentFromFile("testFiles/xmlNodeTestExamples/notes4.xml");
    Species proteinAlias = new GenericProtein("id");
    proteinAlias.setNotes("text");
    parser.processNotes(node.getFirstChild(), proteinAlias);
    String notes = proteinAlias.getNotes();
    assertNotNull(notes);
    assertFalse(notes.contains("body"));
    assertTrue(notes.contains("some"));
    // after redoing it should be the same
    parser.processNotes(node.getFirstChild(), proteinAlias);
    notes = proteinAlias.getNotes();
    assertNotNull(notes);
    assertFalse(notes.contains("body"));
    assertTrue(notes.contains("some"));
  }

  @Test
  public void testParseCollisionNotes() throws Exception {
    RestAnnotationParser parser = new RestAnnotationParser();
    Document node = getXmlDocumentFromFile("testFiles/xmlNodeTestExamples/notes2.xml");

    GenericProtein protein = new GenericProtein("pr_id1");
    protein.addSynonym("a1");
    protein.addFormerSymbol("a1");
    protein.setFullName("yyy");
    protein.setAbbreviation("aaaa");
    protein.setCharge(9);
    protein.setSymbol("D");
    parser.processNotes(node.getFirstChild(), protein);
    assertEquals(1, protein.getSynonyms().size());
    assertEquals(1, protein.getFormerSymbols().size());

    assertEquals(6, getWarnings().size());
  }

  @Test
  public void testParseInvalidNotes1() throws Exception {
    RestAnnotationParser parser = new RestAnnotationParser();
    Document node = getXmlDocumentFromFile("testFiles/xmlNodeTestExamples/notes_invalid_charge.xml");

    Species proteinAlias = new GenericProtein("id");
    parser.processNotes(node.getFirstChild(), proteinAlias);

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testParseCollisionNotes2() throws Exception {
    RestAnnotationParser parser = new RestAnnotationParser();
    Document node = getXmlDocumentFromFile("testFiles/xmlNodeTestExamples/notes2.xml");
    Reaction reaction = new Reaction("re");
    reaction.addSynonym("a1");
    reaction.setAbbreviation("aaaa");
    reaction.setSubsystem("sss");
    reaction.setGeneProteinReaction("str");
    reaction.setFormula("xxx");
    reaction.setMechanicalConfidenceScore(-45);
    reaction.setUpperBound(4.5);
    reaction.setLowerBound(4.5);

    parser.processNotes(node.getFirstChild(), reaction);

    assertEquals(8, getWarnings().size());
  }

  @Test
  public void testMoveAnnotationsFromNotes() throws Exception {
    Model model = getModelForFile("testFiles/copyingAnnotationModel.xml");

    Set<Element> elements = model.getElements();
    for (final Element element : elements) {
      if (element.getName().equals("blabla")) {
        assertEquals(2, element.getMiriamData().size());
        element.getMiriamData()
            .add(new MiriamData(MiriamType.PUBMED, "12345"));
        element.getMiriamData()
            .add(new MiriamData(MiriamType.PUBMED, "333666"));
        assertEquals(2, element.getMiriamData().size());
      }
      assertFalse(element.getNotes().contains("rdf:RDF"));
    }
  }

  @Test
  public void testProcessRdfDescription() throws Exception {
    RestAnnotationParser rap = new RestAnnotationParser();
    Species speciesAlias = new GenericProtein("id");
    speciesAlias.setNotes("begining\n" + "<rdf:RDF>" + "<rdf:Description rdf:about=\"#s2157\">\n" + "<bqmodel:is>\n"
        + "<rdf:Bag>\n" + "<rdf:li rdf:resource=\"urn:miriam:obo.chebi:CHEBI%3A17515\"/>\n" + "</rdf:Bag>\n"
        + "</bqmodel:is>\n" + "</rdf:Description>\n" + "</rdf:RDF>" + "\nend ending");
    rap.processRdfDescription(speciesAlias);
    assertEquals("begining\n\nend ending", speciesAlias.getNotes());
  }

  @Test
  public void testGetMiriamDataFromAnnotation() throws Exception {
    RestAnnotationParser rap = new RestAnnotationParser();
    String response = readFile("testFiles/annotation/sampleDescription.txt");

    Set<MiriamData> set = rap.getMiriamData(response);
    for (final MiriamData miriamData : set) {
      if (miriamData.getDataType().equals(MiriamType.KEGG_GENES)) {
        assertTrue(miriamData.getResource().contains("hsa"));
      }
    }
  }

  @Test
  public void testGetMiriamDataFromEmpty() throws Exception {
    RestAnnotationParser rap = new RestAnnotationParser();

    Set<MiriamData> set = rap.getMiriamData(null);
    assertEquals(0, set.size());
  }

  @Test
  public void testCreateAnnotationString() throws Exception {
    RestAnnotationParser rap = new RestAnnotationParser();

    String former1 = "form1";
    String former2 = "form2";
    GenericProtein element = new GenericProtein("id");
    element.addFormerSymbol(former1);
    element.addFormerSymbol(former2);
    element.setCharge(1);
    String str = rap.createAnnotationString(element, true);
    assertTrue(str.contains(former1));
    assertTrue(str.contains(former2));

    for (final NoteField field : NoteField.values()) {
      boolean deprecated = rap.isFieldAnnotated(field, Deprecated.class);
      boolean importOnly = rap.isFieldAnnotated(field, ImportOnly.class);

      if (!deprecated && !importOnly) {
        if (field.getClazz().isAssignableFrom(element.getClass())
            || CellDesignerElement.class.isAssignableFrom(field.getClazz())) {
          assertTrue("Export string doesn't contain info about: " + field.getCommonName(),
              str.indexOf(field.getCommonName()) >= 0);
        }
      } else {
        assertEquals("Export string contains info about: " + field.getCommonName() + " but shouldn't",
            -1, str.indexOf(field.getCommonName()));
      }

    }
  }

  @Test
  public void testCreateEmptyAnnotationString() throws Exception {
    RestAnnotationParser rap = new RestAnnotationParser();
    GenericProtein element = new GenericProtein("id");
    String str = rap.createAnnotationString(element, false);

    for (final NoteField field : NoteField.values()) {
      if (field.getClazz().isAssignableFrom(element.getClass())
          || CellDesignerElement.class.isAssignableFrom(field.getClazz())) {
        assertFalse("Export string contain info about: " + field.getCommonName() + ". But shouldn't",
            str.indexOf(field.getCommonName()) >= 0);
      }

    }
  }

  @Test
  public void testGetFormerSymbolsFromInvalidStr() throws Exception {
    RestAnnotationParser rap = new RestAnnotationParser();

    assertEquals(0, rap.getFormerSymbols(null).size());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetNotesInvalid() throws Exception {
    RestAnnotationParser rap = new RestAnnotationParser();

    Document xmlDoc = new DocumentImpl();
    Node node = xmlDoc.createElement("bla");

    rap.getNotes(node);
  }

  @Test
  public void testGetNotesWithMissingBody() throws Exception {
    RestAnnotationParser rap = new RestAnnotationParser();

    Node node = super.getNodeFromXmlString("<celldesigner:notes>\n"
        + "              <html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
        + "                <head>\n"
        + "                  <title/>\n"
        + "                </head>\n"
        + "              </html>\n"
        + "            </celldesigner:notes>");
    String notes = rap.getNotes(node);
    assertNotNull(notes);
  }

  @Test
  public void testGetNotesWithLinkInside() throws Exception {
    RestAnnotationParser rap = new RestAnnotationParser();

    Node node = super.getNodeFromXmlString("<notes>\n"
        + "Name: L-cystine\n"
        + "Synonyms: (R,R)-3,3&apos;-dithiobis(2-aminopropanoic acid), "
        + "(R-(R*,R*))-3,3&apos;-Dithiobis(2-aminopropanoic acid), "
        + "3,3&apos;-Dithiobis-L-alanine, beta,beta&apos;-diamino-beta,beta&apos;-dicarboxydiethyl disulfide, "
        + "beta,beta&apos;-dithiodialanine, bis(beta-amino-beta-carboxyethyl) disulfide, E921, "
        + "L-alpha-Diamino-beta-dithiolactic acid, L-Cystine, L-Dicysteine, oxidized L-cysteine, final Recon metabolite: mma\n"
        + "Description: VMH database: <a xmlns=\"http://www.w3.org/1999/xhtml\" href=\"http://vmh.uni.lu/#metabolite/mma\">mma</a>\n"
        + "SemanticZoomLevelVisibility: 5\n"
        + "Charge: 0\n"
        + "</notes>\n");
    String notes = rap.getNotes(node);
    assertFalse(notes.contains("http://www.w3.org/1999/xhtml"));
  }

  @Test(expected = NotImplementedException.class)
  public void testProcessNotesForInvalidElement() throws Exception {
    RestAnnotationParser parser = new RestAnnotationParser();
    Document node = getXmlDocumentFromFile("testFiles/xmlNodeTestExamples/notes4.xml");
    BioEntity bioEntity = Mockito.mock(BioEntity.class);
    Mockito.when(bioEntity.getNotes()).thenReturn("");
    parser.processNotes(node.getFirstChild(), bioEntity);
  }

  @Test
  public void testProcessInvalidNotes() throws Exception {
    RestAnnotationParser parser = new RestAnnotationParser();
    String str = super.readFile("testFiles/xmlNodeTestExamples/notes5.xml");

    parser.processNotes(str, new GenericProtein("id"));

    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testProcessNotes() throws Exception {
    String newNotes = "new notes";
    String oldNotes = "old notes";
    RestAnnotationParser parser = new RestAnnotationParser();
    String str = "Description: " + newNotes + "\n" + oldNotes;
    GenericProtein protein = new GenericProtein("id");
    parser.processNotes(str, protein);

    assertTrue(protein.getNotes().contains(newNotes));
    assertTrue(protein.getNotes().contains(oldNotes));
    assertFalse(protein.getNotes().contains("Description"));
  }

  @Test
  public void testExtractBackgroundColor() throws Exception {
    RestAnnotationParser parser = new RestAnnotationParser();

    Color color = parser.getColor("dcxvxcvxcvx\nBackgroundColor=#ccffcc", NoteField.BACKGROUND_COLOR.getCommonName());
    assertNotNull(color);
  }

  @Test
  public void testExtractBackgroundColorWithColon() throws Exception {
    RestAnnotationParser parser = new RestAnnotationParser();
    Color color = parser.getColor("dcxvxcvxcvx\nBackgroundColor:#ccffcc", NoteField.BACKGROUND_COLOR.getCommonName());
    assertNotNull(color);
  }

  @Test
  public void testExtractNonExistingBackgroundColor() throws Exception {
    RestAnnotationParser parser = new RestAnnotationParser();
    Color color = parser.getColor("dcxvxcvxcvx\nBackgroundCffcc", NoteField.BACKGROUND_COLOR.getCommonName());
    assertNull(color);
  }

}
