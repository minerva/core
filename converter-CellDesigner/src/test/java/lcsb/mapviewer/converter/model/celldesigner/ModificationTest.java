package lcsb.mapviewer.converter.model.celldesigner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;
import lcsb.mapviewer.model.map.species.field.RegulatoryRegion;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;

public class ModificationTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testBindingRegion() throws Exception {
    Model model = getModelForFile("testFiles/modifications/protein_with_binding_region.xml");
    Protein protein = model.getElementByElementId("sa1");
    assertEquals(1, protein.getModificationResidues().size());
    ModificationResidue residue = protein.getModificationResidues().get(0);

    assertTrue(residue instanceof BindingRegion);
    BindingRegion bindingRegion = (BindingRegion) residue;
    assertEquals(bindingRegion.getCenter().getX(), protein.getX(), Configuration.EPSILON);
    assertTrue(bindingRegion.getWidth() < bindingRegion.getHeight());

    testXmlSerialization(model);
  }

  @Test
  public void testRegulatoryRegion() throws Exception {
    Model model = getModelForFile("testFiles/modifications/gene_with_regulatory_region.xml");
    Gene gene = model.getElementByElementId("sa1");
    assertEquals(1, gene.getModificationResidues().size());

    ModificationResidue residue = gene.getModificationResidues().get(0);
    assertTrue(residue instanceof RegulatoryRegion);
    RegulatoryRegion bindingRegion = (RegulatoryRegion) residue;
    assertEquals(bindingRegion.getCenter().getY(), gene.getY(), Configuration.EPSILON);
    assertTrue(bindingRegion.getWidth() > bindingRegion.getHeight());

    testXmlSerialization(model);
  }

  @Test
  public void testGeneCodingRegion() throws Exception {
    Model model = getModelForFile("testFiles/modifications/gene_with_coding_region.xml");
    Gene gene = model.getElementByElementId("sa1");
    assertEquals(1, gene.getModificationResidues().size());

    ModificationResidue residue = gene.getModificationResidues().get(0);
    assertTrue(residue instanceof CodingRegion);
    CodingRegion bindingRegion = (CodingRegion) residue;
    assertEquals(bindingRegion.getCenter().getY(), gene.getY(), Configuration.EPSILON);
    assertTrue(bindingRegion.getWidth() > bindingRegion.getHeight());

    testXmlSerialization(model);
  }

  @Test
  public void testGeneModificationSite() throws Exception {
    Model model = getModelForFile("testFiles/modifications/gene_with_modification_site.xml");
    Gene gene = model.getElementByElementId("sa1");
    assertEquals(1, gene.getModificationResidues().size());

    ModificationResidue residue = gene.getModificationResidues().get(0);
    assertTrue(residue instanceof ModificationSite);
    ModificationSite bindingRegion = (ModificationSite) residue;
    assertEquals(bindingRegion.getY() + bindingRegion.getHeight(), gene.getY(), Configuration.EPSILON);

    testXmlSerialization(model);
  }

  @Test
  public void testRegulatoryTranscriptionSiteRight() throws Exception {
    Model model = getModelForFile("testFiles/modifications/gene_with_transcription_site_right.xml");
    Gene gene = model.getElementByElementId("sa1");
    assertEquals(1, gene.getModificationResidues().size());
    ModificationResidue residue = gene.getModificationResidues().get(0);

    assertTrue(residue instanceof TranscriptionSite);
    TranscriptionSite transcriptionSite = (TranscriptionSite) residue;
    assertEquals(transcriptionSite.getY() + transcriptionSite.getHeight(), gene.getY(), Configuration.EPSILON);
    assertEquals("RIGHT", transcriptionSite.getDirection());
    assertTrue(transcriptionSite.getActive());

    testXmlSerialization(model);
  }

  @Test
  public void testRegulatoryTranscriptionSiteLeft() throws Exception {
    Model model = getModelForFile("testFiles/modifications/gene_with_transcription_site_left.xml");
    Gene gene = model.getElementByElementId("sa1");
    assertEquals(1, gene.getModificationResidues().size());
    ModificationResidue residue = gene.getModificationResidues().get(0);

    assertTrue(residue instanceof TranscriptionSite);
    TranscriptionSite transcriptionSite = (TranscriptionSite) residue;
    assertEquals(transcriptionSite.getY() + transcriptionSite.getHeight(), gene.getY(), Configuration.EPSILON);
    assertEquals("LEFT", transcriptionSite.getDirection());
    assertFalse(transcriptionSite.getActive());

    testXmlSerialization(model);
  }

  @Test
  public void testProteinWithResidues() throws Exception {
    Model model = getModelForFile("testFiles/modifications/protein_with_residues.xml");
    Protein protein = (Protein) model.getElementByElementId("sa1");
    assertEquals(14, protein.getModificationResidues().size());

    ModificationResidue residue = protein.getModificationResidues().get(0);

    assertTrue(residue instanceof Residue);

    testXmlSerialization(model);
  }

  @Test
  public void testRnaWithRegion() throws Exception {
    Model model = getModelForFile("testFiles/modifications/rna_with_region.xml");
    Rna rna = model.getElementByElementId("sa1");
    assertEquals(1, rna.getModificationResidues().size());
    assertTrue(rna.getModificationResidues().get(0) instanceof CodingRegion);

    rna = model.getElementByElementId("sa2");
    assertEquals(1, rna.getModificationResidues().size());
    assertTrue(rna.getModificationResidues().get(0) instanceof ProteinBindingDomain);

    rna = model.getElementByElementId("sa3");
    assertEquals(1, rna.getModificationResidues().size());
    assertTrue(rna.getModificationResidues().get(0) instanceof ModificationSite);

    testXmlSerialization(model);

  }

  @Test
  public void testAntisenseRnaWithRegion() throws Exception {
    Model model = getModelForFile("testFiles/modifications/antisense_rna_with_region.xml");
    AntisenseRna rna = model.getElementByElementId("sa1");
    assertEquals(1, rna.getModificationResidues().size());
    assertTrue(rna.getModificationResidues().get(0) instanceof CodingRegion);

    rna = model.getElementByElementId("sa2");
    assertEquals(1, rna.getModificationResidues().size());
    assertTrue(rna.getModificationResidues().get(0) instanceof ProteinBindingDomain);
    ModificationResidue mr = rna.getModificationResidues().get(0);
    assertEquals(rna.getY(), mr.getCenter().getY(), EPSILON);

    rna = model.getElementByElementId("sa3");
    assertEquals(1, rna.getModificationResidues().size());
    assertTrue(rna.getModificationResidues().get(0) instanceof ModificationSite);
    mr = rna.getModificationResidues().get(0);
    assertEquals(rna.getY(), mr.getY() + mr.getHeight(), EPSILON);

    rna = model.getElementByElementId("sa4");
    assertEquals(1, rna.getModificationResidues().size());
    assertTrue(rna.getModificationResidues().get(0) instanceof ModificationSite);
    ModificationSite modificationSite = (ModificationSite) rna.getModificationResidues().get(0);
    assertEquals(ModificationState.PHOSPHORYLATED, modificationSite.getState());
  }

  @Test
  public void testPhosporylatedProteinToXml() throws Exception {
    Model model = getModelForFile("testFiles/problematic/phosphorylated_protein.xml");
    testXmlSerialization(model);
  }

}
