package lcsb.mapviewer.converter.model.celldesigner.alias;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerCompartment;
import lcsb.mapviewer.model.map.compartment.BottomSquareCompartment;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.LeftSquareCompartment;
import lcsb.mapviewer.model.map.compartment.RightSquareCompartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.compartment.TopSquareCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class CompartmentAliasXmlParserTest extends CellDesignerTestFunctions {

  private CompartmentAliasXmlParser parser;
  private Model model;
  private CellDesignerElementCollection elements;

  @Before
  public void setUp() throws Exception {
    model = new ModelFullIndexed(null);
    elements = new CellDesignerElementCollection();
    parser = new CompartmentAliasXmlParser(elements, model);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testToXml() throws Exception {
    Compartment compartment = createCompartment();
    String xmlString = parser.toXml(compartment);
    assertNotNull(xmlString);

    elements.addElement(new CellDesignerCompartment(elements.getElementId(compartment)));

    Compartment alias2 = parser.parseXmlAlias(xmlString);
    assertEquals(0, getWarnings().size());

    assertEquals(compartment.getElementId(), alias2.getElementId());
    assertNotNull(compartment.getName());
    assertEquals(compartment.getFontSize(), alias2.getFontSize(), 1e-6);
    assertEquals(compartment.getHeight(), alias2.getHeight(), 1e-6);
    assertEquals(compartment.getWidth(), alias2.getWidth(), 1e-6);
    assertEquals(compartment.getX(), alias2.getX(), 1e-6);
    assertEquals(compartment.getY(), alias2.getY(), 1e-6);
    assertEquals(compartment.getNameX(), alias2.getNameX(), 1e-6);
    assertEquals(compartment.getNameY(), alias2.getNameY(), 1e-6);
    assertEquals(compartment.getNameWidth(), alias2.getNameWidth(), 1e-6);
    assertEquals(compartment.getNameHeight(), alias2.getNameHeight(), 1e-6);
    assertNotNull(alias2.getNameHorizontalAlign());
    assertNotNull(alias2.getNameVerticalAlign());
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void parseInvalidAlias() throws Exception {
    Model model = new ModelFullIndexed(null);
    parser = new CompartmentAliasXmlParser(elements, model);
    String xmlString = readFile("testFiles/xmlNodeTestExamples/cd_compartment_alias.xml");
    parser.parseXmlAlias(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void parseInvalidAlias2() throws Exception {
    Model model = new ModelFullIndexed(null);
    CellDesignerCompartment comp = new CellDesignerCompartment();
    comp.setElementId("c1");
    elements.addElement(comp);
    parser = new CompartmentAliasXmlParser(elements, model);
    String xmlString = readFile("testFiles/invalid/compartment_alias.xml");
    parser.parseXmlAlias(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void parseInvalidAlias3() throws Exception {
    Model model = new ModelFullIndexed(null);
    CellDesignerCompartment comp = new CellDesignerCompartment();
    comp.setElementId("c1");
    elements.addElement(comp);
    parser = new CompartmentAliasXmlParser(elements, model);
    String xmlString = readFile("testFiles/invalid/compartment_alias2.xml");
    parser.parseXmlAlias(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void parseInvalidAlias4() throws Exception {
    Model model = new ModelFullIndexed(null);
    CellDesignerCompartment comp = new CellDesignerCompartment();
    comp.setElementId("c1");
    elements.addElement(comp);
    parser = new CompartmentAliasXmlParser(elements, model);
    String xmlString = readFile("testFiles/invalid/compartment_alias3.xml");
    parser.parseXmlAlias(xmlString);
  }

  @Test
  public void testParseXmlAliasNode() throws Exception {
    elements.addElement(new CellDesignerCompartment("c1"));

    String xmlString = readFile("testFiles/xmlNodeTestExamples/cd_compartment_alias.xml");
    Compartment alias = parser.parseXmlAlias(xmlString);
    assertEquals(0, getWarnings().size());
    assertNotNull(alias);
    assertEquals("ca1", alias.getElementId());
    assertNotNull(alias.getName());
    assertTrue(alias instanceof SquareCompartment);
    assertEquals(139.0, alias.getX(), 1e-6);
    assertEquals(55.0, alias.getY(), 1e-6);
    assertEquals(522.0, alias.getWidth(), 1e-6);
    assertEquals(392.5, alias.getNameX(), 1e-6);
    assertEquals(196.5, alias.getNameY(), 1e-6);
    assertEquals(268.5, alias.getNameWidth(), 1e-6);
    assertEquals(0xffcccc00, alias.getFillColor().getRGB());
  }

  @Test
  public void testGetters() {
    parser.setCommonParser(null);

    assertNull(parser.getCommonParser());
  }

  @Test
  public void testLeftToXml() throws Exception {
    Compartment alias = new LeftSquareCompartment("id");
    assignCoordinates(alias);
    String xml = parser.toXml(alias);
    assertNotNull(xml);
  }

  @Test
  public void testRightToXml() throws Exception {
    Compartment alias = new RightSquareCompartment("id");
    assignCoordinates(alias);
    String xml = parser.toXml(alias);
    assertNotNull(xml);
  }

  @Test
  public void testTopToXml() throws Exception {
    Compartment alias = new TopSquareCompartment("id");
    assignCoordinates(alias);
    String xml = parser.toXml(alias);
    assertNotNull(xml);
  }

  @Test
  public void testBotttomToXml() throws Exception {
    Compartment alias = new BottomSquareCompartment("id");
    assignCoordinates(alias);
    String xml = parser.toXml(alias);
    assertNotNull(xml);
  }

  @Test(expected = NotImplementedException.class)
  public void testUnknownToXml() throws Exception {
    Compartment alias = Mockito.mock(BottomSquareCompartment.class);
    Mockito.when(alias.getElementId()).thenReturn("some id");
    parser.toXml(alias);
  }

  @Test
  public void testToXmlInvalidInnerWidth() throws Exception {
    Compartment compartment = createCompartment();
    compartment.setInnerWidth(0);
    parser.toXml(compartment);
    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testToXmlInvalidOuterWidth() throws Exception {
    Compartment compartment = createCompartment();
    compartment.setOuterWidth(compartment.getInnerWidth());
    parser.toXml(compartment);
    assertEquals(1, getWarnings().size());
  }

  @Test
  public void testToXmlInvalidThickness() throws Exception {
    Compartment compartment = createCompartment();
    compartment.setThickness(0);
    parser.toXml(compartment);
    assertEquals(1, getWarnings().size());
  }

}
