package lcsb.mapviewer.converter.model.celldesigner.reaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.geom.Line2D;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class ReactionCollectionXmlParserTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseXmlReactionCollection() throws Exception {
    Model model = getModelFilledWithSpecies();

    CellDesignerElementCollection elements = new CellDesignerElementCollection();
    ReactionCollectionXmlParser parser = new ReactionCollectionXmlParser(model, elements, false, false);

    String xmlString = readFile("testFiles/reactions/listOfReactions.xml");
    Node node = getNodeFromXmlString(xmlString);
    List<Reaction> reactions = parser.parseXmlReactionCollection(node);

    assertEquals(3, reactions.size());
  }

  @Test
  public void testReactionCollectionToXmlString() throws Exception {
    Model model = getModelFilledWithSpecies();

    CellDesignerElementCollection elements = new CellDesignerElementCollection();
    ReactionCollectionXmlParser parser = new ReactionCollectionXmlParser(model, elements, false, false);

    String xmlString = readFile("testFiles/reactions/listOfReactions.xml");
    Node node = getNodeFromXmlString(xmlString);
    List<Reaction> reactions = parser.parseXmlReactionCollection(node);

    elements = new CellDesignerElementCollection();
    parser = new ReactionCollectionXmlParser(model, elements, false, false);
    String xmlString2 = parser.reactionCollectionToXmlString(reactions);
    assertNotNull(xmlString2);

    elements = new CellDesignerElementCollection();
    parser = new ReactionCollectionXmlParser(model, elements, false, false);
    node = getNodeFromXmlString(xmlString2);
    List<Reaction> reactions2 = parser.parseXmlReactionCollection(node);

    assertEquals(reactions.size(), reactions2.size());
    for (int i = 0; i < reactions.size(); i++) {
      Reaction a = reactions.get(0);
      Reaction b = reactions2.get(0);
      
      logger.debug(a.getProducts().get(0).getLine());
      logger.debug(a.getReactants().get(0).getLine());
      logger.debug("--");
      logger.debug(b.getProducts().get(0).getLine());
      logger.debug(b.getReactants().get(0).getLine());

      List<Line2D> linesA = a.getLines();
      List<Line2D> linesB = b.getLines();
      for (int j = 0; j < linesA.size(); j++) {
        Line2D lineA = linesA.get(j);
        Line2D lineB = linesB.get(j);
        assertEquals(lineA.getX1(), lineB.getX1(), EPSILON);
        assertEquals(lineA.getY1(), lineB.getY1(), EPSILON);
        assertEquals(lineA.getX2(), lineB.getX2(), EPSILON);
        assertEquals(lineA.getY2(), lineB.getY2(), EPSILON);
      }
    }
  }

  @Test
  public void testReactionToCompartmentCollectionToXml() throws Exception {
    ReactionCollectionXmlParser parser = new ReactionCollectionXmlParser(null, new CellDesignerElementCollection(),
        false, false);

    Reaction reaction = new TransportReaction("re");
    reaction.addReactant(new Reactant(new PathwayCompartment("id1")));
    reaction.addProduct(new Product(new PathwayCompartment("id2")));
    String xmlString2 = parser.reactionCollectionToXmlString(Arrays.asList(reaction));
    assertNotNull(xmlString2);
    assertEquals(1, super.getWarnings().size());
  }

  private Model getModelFilledWithSpecies() {
    Model model = new ModelFullIndexed(null);

    Species sa1 = new GenericProtein("sa1");
    sa1.setX(100.0);
    sa1.setY(200.0);
    sa1.setWidth(300.0);
    sa1.setHeight(400.0);
    model.addElement(sa1);

    Species sa2 = new GenericProtein("sa2");
    sa2.setX(1050.0);
    sa2.setY(2050.0);
    sa2.setWidth(300.0);
    sa2.setHeight(450.0);
    model.addElement(sa2);

    Species sa3 = new GenericProtein("sa3");
    sa3.setX(600.0);
    sa3.setY(250.0);
    sa3.setWidth(300.0);
    sa3.setHeight(400.0);
    model.addElement(sa3);

    Species sa4 = new GenericProtein("sa4");
    sa4.setX(550.0);
    sa4.setY(350.0);
    sa4.setWidth(300.0);
    sa4.setHeight(450.0);
    model.addElement(sa4);

    Species sa5 = new GenericProtein("sa5");
    sa5.setX(10.0);
    sa5.setY(250.0);
    sa5.setWidth(300.0);
    sa5.setHeight(450.0);
    model.addElement(sa5);

    Species sa6 = new GenericProtein("sa6");
    sa6.setX(10.0);
    sa6.setY(250.0);
    sa6.setWidth(300.0);
    sa6.setHeight(450.0);
    model.addElement(sa6);

    Species sa10 = new GenericProtein("sa10");
    sa10.setX(210.0);
    sa10.setY(220.0);
    sa10.setWidth(320.0);
    sa10.setHeight(250.0);
    model.addElement(sa10);

    Species sa11 = new GenericProtein("sa11");
    sa11.setX(11.0);
    sa11.setY(320.0);
    sa11.setWidth(321.0);
    sa11.setHeight(150.0);
    model.addElement(sa11);

    Species sa12 = new GenericProtein("sa12");
    sa12.setX(12.0);
    sa12.setY(20.0);
    sa12.setWidth(321.0);
    sa12.setHeight(150.0);
    model.addElement(sa12);

    Species sa13 = new GenericProtein("sa13");
    sa13.setX(513.0);
    sa13.setY(20.0);
    sa13.setWidth(321.0);
    sa13.setHeight(150.0);
    model.addElement(sa13);

    Species sa14 = new GenericProtein("sa14");
    sa14.setX(14.0);
    sa14.setY(820.0);
    sa14.setWidth(321.0);
    sa14.setHeight(150.0);
    model.addElement(sa14);

    Species sa15 = new GenericProtein("sa15");
    sa15.setX(815.0);
    sa15.setY(620.0);
    sa15.setWidth(321.0);
    sa15.setHeight(150.0);
    model.addElement(sa15);

    return model;
  }

}
