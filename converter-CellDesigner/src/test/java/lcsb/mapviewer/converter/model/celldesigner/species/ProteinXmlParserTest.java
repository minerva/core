package lcsb.mapviewer.converter.model.celldesigner.species;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGenericProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerProtein;
import lcsb.mapviewer.model.map.species.Protein;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ProteinXmlParserTest extends CellDesignerTestFunctions {

  private ProteinXmlParser proteinParser;
  private final String testProteinFile = "testFiles/xmlNodeTestExamples/protein.xml";

  private CellDesignerElementCollection elements;

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();
    proteinParser = new ProteinXmlParser(elements);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseXmlSpecies() throws Exception {
    final String xmlString = readFile(testProteinFile);
    final Pair<String, CellDesignerProtein<?>> result = proteinParser.parseXmlElement(xmlString);
    final CellDesignerProtein<?> protein = result.getRight();
    assertEquals("pr23", result.getLeft());
    assertEquals("SDHA", protein.getName());
    assertTrue(protein instanceof CellDesignerGenericProtein);
    assertEquals(1, protein.getModificationResidues().size());
    assertEquals("rs1", protein.getModificationResidues().get(0).getIdModificationResidue());
    assertEquals("S176 bla bla", protein.getModificationResidues().get(0).getName());
    assertEquals("Difference to big", 3.141592653589793, protein.getModificationResidues().get(0).getAngle(), 1e-6);
    assertEquals("none", protein.getModificationResidues().get(0).getSide());
    assertTrue(protein.getNotes().contains("UniProtKB\tP31040\tSDHA\t\tGO:0005749\tGO_REF:0000024"));
  }

  @Test
  public void testToXml() throws Exception {
    final String xmlString = readFile(testProteinFile);

    final Pair<String, CellDesignerProtein<?>> result = proteinParser.parseXmlElement(xmlString);
    final CellDesignerProtein<?> protein = result.getRight();

    final Protein species = protein.createModelElement("id");
    proteinParser.initializeProteinData(Collections.singletonList(species));

    final String transformedXml = proteinParser.toXml(species);
    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    final DocumentBuilder builder = factory.newDocumentBuilder();
    final InputSource is = new InputSource(new StringReader(transformedXml));
    final Document doc = builder.parse(is);
    final NodeList root = doc.getChildNodes();
    assertEquals("celldesigner:protein", root.item(0).getNodeName());

    elements = new CellDesignerElementCollection();
    proteinParser = new ProteinXmlParser(elements);
    final Protein proteinWithLayout = protein.createModelElement("id");
    proteinWithLayout.setWidth(10);
    proteinWithLayout.setHeight(10);
    protein.updateModelElementAfterLayoutAdded(proteinWithLayout);

    proteinParser.initializeProteinData(Collections.singletonList(proteinWithLayout));
    final Pair<String, CellDesignerProtein<?>> result2 = proteinParser
        .parseXmlElement(proteinParser.toXml(proteinWithLayout));

    final CellDesignerProtein<?> protein2 = result2.getRight();
    assertEquals(protein.getName(), protein2.getName());
    assertEquals(protein.getClass(), protein2.getClass());
    assertEquals(protein.getModificationResidues().size(), protein2.getModificationResidues().size());
    assertEquals(protein.getModificationResidues().get(0).getName(),
        protein2.getModificationResidues().get(0).getName());
    assertEquals("Difference to big", protein.getModificationResidues().get(0).getAngle(),
        protein2.getModificationResidues().get(0).getAngle(), 1e-6);
  }

  @Test
  public void testToXmlShouldNotContainNotes() throws Exception {
    final String xmlString = readFile(testProteinFile);

    final Pair<String, CellDesignerProtein<?>> result = proteinParser.parseXmlElement(xmlString);
    final CellDesignerProtein<?> protein = result.getRight();

    final Protein species = protein.createModelElement("id");
    proteinParser.initializeProteinData(Collections.singletonList(species));
    final String transformedXml = proteinParser.toXml(species);

    assertFalse(transformedXml.contains("celldesigner:notes"));
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXml() throws Exception {
    final String xmlString = readFile("testFiles/invalid/protein.xml");
    proteinParser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXml2() throws Exception {
    final String xmlString = readFile("testFiles/invalid/protein2.xml");
    proteinParser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXml3() throws Exception {
    final String xmlString = readFile("testFiles/invalid/protein3.xml");
    proteinParser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXml4() throws Exception {
    final String xmlString = readFile("testFiles/invalid/protein4.xml");
    proteinParser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidProteinToXml() throws Exception {
    final Protein mock = Mockito.mock(Protein.class);
    proteinParser.initializeProteinData(Collections.singletonList(mock));
    proteinParser.toXml(mock);
  }

  @Test
  public void testToXmlMergeResidues() throws Exception {

    final Protein protein1 = createProtein();
    protein1.setName("SNCA");
    protein1.addResidue(createResidue(protein1));
    protein1.addResidue(createResidue(protein1));

    final Protein protein2 = createProtein();
    protein2.setName("SNCA");
    protein2.addResidue(createResidue(protein1));

    final Protein protein3 = createProtein();
    protein3.setName("SNCA");

    final List<Protein> proteins = new ArrayList<>();
    proteins.add(protein1);
    proteins.add(protein2);
    proteins.add(protein3);

    elements = new CellDesignerElementCollection();
    elements.getElementId(protein1);
    elements.getElementId(protein2);
    elements.getElementId(protein3);
    proteinParser = new ProteinXmlParser(elements);
    proteinParser.initializeProteinData(proteins);

    final String exportedXml1 = proteinParser.toXml(protein1);
    final String exportedXml2 = proteinParser.toXml(protein2);
    final String exportedXml3 = proteinParser.toXml(protein3);

    assertEquals(exportedXml1, exportedXml2);
    assertEquals(exportedXml1, exportedXml3);
  }

}
