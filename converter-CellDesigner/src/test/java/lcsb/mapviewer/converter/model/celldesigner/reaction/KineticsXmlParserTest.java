package lcsb.mapviewer.converter.model.celldesigner.reaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class KineticsXmlParserTest extends CellDesignerTestFunctions {

  @Test
  public void testParseElements() throws InvalidXmlSchemaException, IOException {
    Map<String, Element> elements = getTestElementMap();
    Model model = new ModelFullIndexed(null);
    Node node = super.getXmlDocumentFromFile("testFiles/kinetics/simple.xml").getFirstChild();

    KineticsXmlParser parser = new KineticsXmlParser(model);
    SbmlKinetics kinetics = parser.parseKinetics(node, elements);
    assertNotNull(kinetics);
    assertEquals(2, kinetics.getArguments().size());
  }

  @Test
  public void testParseDefinition() throws InvalidXmlSchemaException, IOException {
    Map<String, Element> elements = getTestElementMap();
    Model model = new ModelFullIndexed(null);
    Node node = super.getXmlDocumentFromFile("testFiles/kinetics/simple.xml").getFirstChild();

    KineticsXmlParser parser = new KineticsXmlParser(model);
    SbmlKinetics kinetics = parser.parseKinetics(node, elements);
    assertNotNull(kinetics.getDefinition());
    assertFalse(kinetics.getDefinition().isEmpty());
    assertTrue("Arguments of function are not used as part of xml formula definition",
        kinetics.getDefinition().indexOf(">" + kinetics.getArguments().get(0).getElementId() + "<") >= 0);
  }

  @Test
  public void testParseParameters() throws InvalidXmlSchemaException, IOException {
    Map<String, Element> elements = getTestElementMap();
    Model model = new ModelFullIndexed(null);
    Node node = super.getXmlDocumentFromFile("testFiles/kinetics/with_parameter.xml").getFirstChild();

    KineticsXmlParser parser = new KineticsXmlParser(model);
    SbmlKinetics kinetics = parser.parseKinetics(node, elements);
    assertNotNull(kinetics);
    assertEquals(1, kinetics.getParameters().size());
  }

  @Test
  public void testToXmlParameters() throws Exception {
    Map<String, Element> elements = getTestElementMap();
    Model model = new ModelFullIndexed(null);
    Node node = super.getXmlDocumentFromFile("testFiles/kinetics/with_parameter.xml").getFirstChild();

    KineticsXmlParser parser = new KineticsXmlParser(model);
    SbmlKinetics kinetics = parser.parseKinetics(node, elements);

    CellDesignerElementCollection cellDesignerElements = new CellDesignerElementCollection();
    for (final Element element : model.getElements()) {
      cellDesignerElements.getElementId(element);
    }

    String xml = parser.toXml(kinetics, cellDesignerElements);

    assertNotNull(xml);
    assertTrue(xml.indexOf(kinetics.getParameters().iterator().next().getElementId()) >= 0);
    assertTrue(xml.indexOf("listOfParameters") >= 0);
  }

  @Test
  public void testToXmlGlobalParameters() throws Exception {
    Map<String, Element> elements = getTestElementMap();
    Model model = new ModelFullIndexed(null);
    model.addParameter(new SbmlParameter("k1"));
    Node node = super.getXmlDocumentFromFile("testFiles/kinetics/with_global_parameter.xml").getFirstChild();

    KineticsXmlParser parser = new KineticsXmlParser(model);
    SbmlKinetics kinetics = parser.parseKinetics(node, elements);

    CellDesignerElementCollection cellDesignerElements = new CellDesignerElementCollection();
    for (final Element element : model.getElements()) {
      cellDesignerElements.getElementId(element);
    }

    String xml = parser.toXml(kinetics, cellDesignerElements);

    assertNotNull(xml);
    assertFalse(xml.indexOf("listOfParameters") >= 0);
  }

  @Test
  public void testToXmlDefinition() throws Exception {
    Model model = super.getModelForFile("testFiles/reactions/kinetics.xml");
    CellDesignerElementCollection elements = new CellDesignerElementCollection();
    for (final Element element : model.getElements()) {
      elements.getElementId(element);
    }
    SbmlKinetics kinetics = model.getReactionByReactionId("re1").getKinetics();

    KineticsXmlParser parser = new KineticsXmlParser(model);
    String xml = parser.toXml(model.getReactionByReactionId("re1").getKinetics(), elements);

    assertNotNull(xml);
    assertTrue(xml.indexOf(elements.getElementId(kinetics.getElements().iterator().next())) >= 0);
  }

  @Test
  public void testParseWithUsedParameters() throws InvalidXmlSchemaException, IOException {
    Map<String, Element> elements = getTestElementMap();
    Model model = new ModelFullIndexed(null);
    Node node = super.getXmlDocumentFromFile("testFiles/kinetics/with_used_parameter.xml").getFirstChild();

    KineticsXmlParser parser = new KineticsXmlParser(model);
    SbmlKinetics kinetics = parser.parseKinetics(node, elements);
    assertNotNull(kinetics);
    assertEquals(1, kinetics.getParameters().size());
  }

  @Test
  public void testParseGlobalParameters() throws InvalidXmlSchemaException, IOException {
    Map<String, Element> elements = getTestElementMap();
    Model model = new ModelFullIndexed(null);
    model.addParameter(new SbmlParameter("k1"));
    Node node = super.getXmlDocumentFromFile("testFiles/kinetics/with_global_parameter.xml").getFirstChild();

    KineticsXmlParser parser = new KineticsXmlParser(model);
    SbmlKinetics kinetics = parser.parseKinetics(node, elements);
    assertNotNull(kinetics);
    assertEquals(1, kinetics.getParameters().size());
  }

  @Test
  public void testParseGlobalFunction() throws InvalidXmlSchemaException, IOException {
    Map<String, Element> elements = getTestElementMap();
    Model model = new ModelFullIndexed(null);
    model.addFunction(new SbmlFunction("fun"));
    Node node = super.getXmlDocumentFromFile("testFiles/kinetics/with_function.xml").getFirstChild();

    KineticsXmlParser parser = new KineticsXmlParser(model);
    SbmlKinetics kinetics = parser.parseKinetics(node, elements);
    assertNotNull(kinetics);
    assertEquals(1, kinetics.getFunctions().size());
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseNonExistingElements() throws InvalidXmlSchemaException, IOException {
    Map<String, Element> elements = new HashMap<>();
    Model model = new ModelFullIndexed(null);
    Node node = super.getXmlDocumentFromFile("testFiles/kinetics/simple.xml").getFirstChild();

    KineticsXmlParser parser = new KineticsXmlParser(model);
    parser.parseKinetics(node, elements);
  }

  @Test
  public void testParseElementDuplicate() throws InvalidXmlSchemaException, IOException {
    Model model = new ModelFullIndexed(null);
    Map<String, Element> elements = getTestElementMap();
    Node node = super.getXmlDocumentFromFile("testFiles/kinetics/math_using_one_element.xml").getFirstChild();

    KineticsXmlParser parser = new KineticsXmlParser(model);
    SbmlKinetics kinetics = parser.parseKinetics(node, elements);
    assertNotNull(kinetics);
    assertEquals(1, kinetics.getArguments().size());
  }

  private Map<String, Element> getTestElementMap() {
    Map<String, Element> elements = new HashMap<>();
    elements.put("s1", new GenericProtein("s1"));
    elements.put("s2", new GenericProtein("s2"));
    return elements;
  }

}
