package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;

public class CellDesignerPolygonTransformationTest extends CellDesignerTestFunctions {
  private CellDesignerPolygonTransformation tranformation = new CellDesignerPolygonTransformation();

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetPointOnInvalidPolygonByAnchor() {
    tranformation.getPointOnPolygonByAnchor(new ArrayList<>(), null);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetPointOnPolygonByInvalidAnchor() {
    List<Point2D> list = new ArrayList<>();
    for (int i = 0; i < CellDesignerAnchor.DIFFERENT_ANCHORS / 2; i++) {
      list.add(new Point2D.Double());
    }
    tranformation.getPointOnPolygonByAnchor(list, null);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetPointOnPolygonByInvalidAnchor2() {
    List<Point2D> list = new ArrayList<>();
    for (int i = 0; i < CellDesignerAnchor.DIFFERENT_ANCHORS; i++) {
      list.add(new Point2D.Double());
    }
    tranformation.getPointOnPolygonByAnchor(list, null);
    tranformation.getPointOnPolygonByAnchor(new ArrayList<>(), null);
  }

}
