package lcsb.mapviewer.converter.model.celldesigner.reaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.OrOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.DissociationReaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.reaction.type.TriggerReaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class ReactionToXmlTest extends CellDesignerTestFunctions {

  private CellDesignerElementCollection elements;
  private ReactionToXml toXmlParser;

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();
    toXmlParser = new ReactionToXml(elements, false);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testInvalidModification() throws InconsistentModelException {
    Model model = new ModelFullIndexed(null);
    Species protein1 = new GenericProtein("2");
    Species protein2 = new GenericProtein("3");
    protein2.setX(100);
    protein2.setY(100);
    Species protein3 = new GenericProtein("4");

    model.addElement(protein1);
    model.addElement(protein2);

    Reaction reaction = new TransportReaction("re");

    Reactant reactant = new Reactant(protein1);
    reactant.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(10, 0)));
    Product product = new Product(protein2);
    product.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(20, 0)));

    reaction.addReactant(reactant);
    reaction.addProduct(product);

    Modifier modifier = new Catalysis(protein3);
    modifier.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(30, 0)));

    Modifier modifier2 = new Catalysis(protein3);
    List<Point2D> points = new ArrayList<>();
    points.add(new Point2D.Double(0, 0));
    points.add(new Point2D.Double(30, 30));
    points.add(new Point2D.Double(20, 20));
    points.add(new Point2D.Double(20, 10));
    points.add(new Point2D.Double(40, 0));
    modifier2.setLine(new PolylineData(points));

    NodeOperator andOperator = new AndOperator();
    andOperator.addInput(modifier);
    andOperator.addInput(modifier2);
    points = new ArrayList<>();
    points.add(new Point2D.Double(1, 0));
    points.add(new Point2D.Double(30, 30));
    points.add(new Point2D.Double(20, 20));
    points.add(new Point2D.Double(20, 10));
    points.add(new Point2D.Double(30, 0));
    andOperator.setLine(new PolylineData(points));

    reaction.addModifier(modifier);
    reaction.addModifier(modifier2);
    reaction.addNode(andOperator);
    reaction.setLine(new PolylineData(new Point2D.Double(1, 0), new Point2D.Double(12, 0)));

    int warningCount = getWarnings().size();

    toXmlParser.toXml(reaction);
    assertEquals(2, getWarnings().size() - warningCount);

  }

  @Test
  public void testModificationFromInsideComplex() throws InconsistentModelException {

    Model model = new ModelFullIndexed(null);
    Species protein1 = new GenericProtein("2");
    protein1.setX(100);
    Species protein2 = new GenericProtein("3");

    model.addElement(protein1);
    model.addElement(protein2);

    Complex complex = new Complex("4");
    complex.addSpecies(protein1);

    protein1.setComplex(complex);

    model.addElement(complex);

    Reaction reaction = new TransportReaction("re");

    Reactant reactant = new Reactant(protein1);
    reactant.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(10, 0)));
    Product product = new Product(protein2);
    product.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(20, 0)));

    reaction.addReactant(reactant);
    reaction.addProduct(product);

    Modifier modifier = new Catalysis(protein1);
    modifier.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(30, 0)));

    reaction.addModifier(modifier);
    reaction.setLine(new PolylineData(new Point2D.Double(1, 0), new Point2D.Double(12, 0)));

    String xml = toXmlParser.toXml(reaction);

    assertTrue(xml.contains("species=\"" + elements.getElementId(complex) + "\""));

  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidToXml() throws InconsistentModelException {

    Model model = new ModelFullIndexed(null);

    Complex complex = new Complex("4");
    complex.setComplex(complex);
    model.addElement(complex);

    Species protein1 = new GenericProtein("2");

    model.addElement(protein1);

    Reaction reaction = new TransportReaction("re");

    Reactant reactant = new Reactant(complex);
    reactant.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(10, 0)));
    Product product = new Product(protein1);
    product.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(20, 0)));

    reaction.addReactant(reactant);
    reaction.addProduct(product);

    toXmlParser.toXml(reaction);

  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidReaction() throws InconsistentModelException {

    Model model = new ModelFullIndexed(null);
    Species protein1 = new GenericProtein("2");
    Species protein2 = new GenericProtein("3");

    model.addElement(protein1);
    model.addElement(protein2);

    Reaction reaction = new Reaction("re");

    Reactant reactant = new Reactant(protein1);
    reactant.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(10, 0)));
    Product product = new Product(protein2);
    product.setLine(new PolylineData(new Point2D.Double(), new Point2D.Double(20, 0)));

    reaction.addReactant(reactant);
    reaction.addProduct(product);

    toXmlParser.toXml(reaction);

  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetConnectSchemeXmlStringForReaction() {
    toXmlParser.getConnectSchemeXmlStringForReaction(new Reaction("re"));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetConnectSchemeXmlStringForReaction2() {
    // test internal implementation
    toXmlParser = new ReactionToXml(elements, false) {
      @Override
      String getEditPointsXmlStringForReaction(final Reaction reaction) {
        return "";
      }
    };
    toXmlParser.getConnectSchemeXmlStringForReaction(new DissociationReaction("re"));
  }

  @Test
  public void testExportColorReactionWithModifierOperator() throws Exception {
    Model model = getModelForFile("testFiles/colorfull_reaction_2.xml");
    testXmlSerialization(model);
  }

  @Test
  public void testGetConnectSchemeXmlStringForLines() {
    // this test checks currently not used part of the code (for test coverage
    // purpose)
    String xml = toXmlParser.getConnectSchemeXmlStringForLines(
        new PolylineData[] { new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(1, 0)),
            new PolylineData() });
    assertTrue(xml.contains("arm"));

  }

  @Test
  public void testGetCellDesignerReactionTypeStringForSimpleReaction() {
    String type = toXmlParser.getCellDesignerReactionTypeString(new TriggerReaction("re"));
    assertFalse("BOOLEAN_LOGIC_GATE".equals(type));
  }

  @Test
  public void testGetCellDesignerReactionTypeStringForSimpleReactionWithOrOperator() {
    Reaction r = new TriggerReaction("re");
    NodeOperator operator = new OrOperator();
    operator.addInput(new Reactant(createProtein()));
    r.addNode(operator);
    String type = toXmlParser.getCellDesignerReactionTypeString(r);
    assertTrue("BOOLEAN_LOGIC_GATE".equals(type));
  }

  @Test
  public void testGetCellDesignerReactionTypeStringForSimpleReactionWithSplitOperator() {
    Reaction r = new TriggerReaction("re");
    NodeOperator operator = new AndOperator();
    operator.addOutput(new Product(createProtein()));
    r.addNode(operator);
    String type = toXmlParser.getCellDesignerReactionTypeString(r);
    assertFalse("BOOLEAN_LOGIC_GATE".equals(type));
  }

  @Test
  public void testGetEditPointsXmlStringForSimpleReaction() {
    Reaction r = new TriggerReaction("re");
    Species productSpecies = new GenericProtein("id1");
    productSpecies.setX(20);
    productSpecies.setY(20);
    productSpecies.setWidth(20);
    productSpecies.setHeight(20);
    Product product = new Product(productSpecies);
    PolylineData productLine = new PolylineData(new Point2D.Double(10, 10), new Point2D.Double(20, 20));
    product.setLine(productLine);
    Species reactantSpecies = new GenericProtein("id1");
    reactantSpecies.setWidth(10);
    reactantSpecies.setHeight(10);
    Reactant reactant = new Reactant(reactantSpecies);
    PolylineData reactantLine = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
    reactant.setLine(reactantLine);
    r.addProduct(product);
    r.addReactant(reactant);
    String result = toXmlParser.getEditPointsXmlStringForSimpleReaction(r);
    assertTrue(result.isEmpty());
  }

  @Test
  public void testGetEditPointsXmlStringForSimpleReactionWhenNotImportedFromCD() {
    Reaction r = new TriggerReaction("re");
    Species productSpecies = new GenericProtein("id1");
    productSpecies.setX(20);
    productSpecies.setY(20);
    productSpecies.setWidth(20);
    productSpecies.setHeight(20);
    Product product = new Product(productSpecies);
    PolylineData productLine = new PolylineData(new Point2D.Double(10, 10), new Point2D.Double(20, 10));
    product.setLine(productLine);
    Species reactantSpecies = new GenericProtein("id1");
    reactantSpecies.setWidth(10);
    reactantSpecies.setHeight(10);
    Reactant reactant = new Reactant(reactantSpecies);
    PolylineData reactantLine = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
    reactant.setLine(reactantLine);
    r.addProduct(product);
    r.addReactant(reactant);
    String result = toXmlParser.getEditPointsXmlStringForSimpleReaction(r);
    assertFalse(result.isEmpty());
  }

}
