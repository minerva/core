package lcsb.mapviewer.converter.model.celldesigner;

import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class NestedComplexParsingTests extends CellDesignerTestFunctions {
  private final ElementUtils eu = new ElementUtils();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testDefaultInsideComplex() throws Exception {
    Model model = getModelForFile("testFiles/xmlNodeTestExamples/nested_complex.xml");

    for (final Species species : model.getSpeciesList()) {
      assertTrue(eu.getElementTag(species) + "Neither initial concentration nor amount is defined for element",
          species.getInitialConcentration() != null || species.getInitialAmount() != null);
    }
  }

  @Test
  public void testParseCompartments() throws Exception {
    Model model = getModelForFile("testFiles/xmlNodeTestExamples/nested_complex.xml");

    Element s1 = model.getElementByElementId("csa1");
    Element s2 = model.getElementByElementId("csa2");
    Element s3 = model.getElementByElementId("csa3");
    Element s4 = model.getElementByElementId("csa4");
    Element s5 = model.getElementByElementId("csa5");
    Element s6 = model.getElementByElementId("csa6");
    Element s7 = model.getElementByElementId("sa1");
    Element s8 = model.getElementByElementId("sa2");
    Element s9 = model.getElementByElementId("sa3");
    Element s10 = model.getElementByElementId("sa4");
    Element s11 = model.getElementByElementId("sa5");
    Element s12 = model.getElementByElementId("sa6");
    assertTrue(s1 instanceof Complex);
    assertTrue(s2 instanceof Complex);
    assertTrue(s3 instanceof Complex);
    assertTrue(s4 instanceof Complex);
    assertTrue(s5 instanceof Complex);
    assertTrue(s6 instanceof Complex);
    Complex cs1 = (Complex) s1;
    Complex cs2 = (Complex) s2;
    Complex cs3 = (Complex) s3;
    Complex cs4 = (Complex) s4;
    Complex cs5 = (Complex) s5;
    Complex cs6 = (Complex) s6;
    assertEquals(2, cs1.getElements().size());
    assertEquals(2, cs2.getElements().size());
    assertEquals(VerticalAlign.BOTTOM, cs2.getNameVerticalAlign());
    assertEquals(0, cs3.getElements().size());
    assertEquals(VerticalAlign.MIDDLE, cs3.getNameVerticalAlign());
    assertEquals(0, cs4.getElements().size());
    assertEquals(2, cs5.getElements().size());
    assertEquals(3, cs6.getElements().size());

    assertTrue(cs1.getElements().contains(s9));
    assertTrue(cs1.getElements().contains(s3));

    assertTrue(cs2.getElements().contains(s7));
    assertTrue(cs2.getElements().contains(s8));

    assertTrue(cs5.getElements().contains(s10));
    assertTrue(cs5.getElements().contains(s11));

    assertTrue(cs6.getElements().contains(s4));
    assertTrue(cs6.getElements().contains(s5));
    assertTrue(cs6.getElements().contains(s12));

    assertEquals(1, cs1.getAllSimpleChildren().size());
    assertEquals(2, cs2.getAllSimpleChildren().size());
    assertEquals(0, cs3.getAllSimpleChildren().size());
    assertEquals(0, cs4.getAllSimpleChildren().size());
    assertEquals(2, cs5.getAllSimpleChildren().size());
    assertEquals(3, cs6.getAllSimpleChildren().size());
  }
}
