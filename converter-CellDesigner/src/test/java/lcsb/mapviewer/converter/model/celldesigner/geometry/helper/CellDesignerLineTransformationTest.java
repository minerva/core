package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.graphics.PolylineData;

public class CellDesignerLineTransformationTest extends CellDesignerTestFunctions {

  private CellDesignerLineTransformation lineTransformation = new CellDesignerLineTransformation();
  private PointTransformation pointTransformation = new PointTransformation();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetPointsFromLine() {
    Point2D startPoint = new Point2D.Double(1, 7);
    Point2D endPoint = new Point2D.Double(3, 5);

    List<Point2D> points = new ArrayList<Point2D>();
    points.add(new Point2D.Double(1.5, 2.5));
    points.add(new Point2D.Double(12, 13));
    points.add(new Point2D.Double(30, 40));

    PolylineData line = new PolylineData(lineTransformation.getLinePointsFromPoints(startPoint, endPoint, points));

    List<Point2D> newPoints = lineTransformation.getPointsFromLine(line);
    assertNotNull(newPoints);

    assertEquals(points.size(), newPoints.size());

    for (int i = 0; i < points.size(); i++) {
      double dist = points.get(i).distance(newPoints.get(i));
      assertTrue("Distance to big: " + dist + " for points " + points.get(i) + " and " + newPoints.get(i),
          dist < 1e-6);
      assertTrue(pointTransformation.isValidPoint(newPoints.get(i)));
    }
  }

  @Test
  public void testGetPointsFromLineShouldCreateDefensiveCopy() {
    Point2D startPoint = new Point2D.Double(1, 7);
    Point2D endPoint = new Point2D.Double(3, 5);

    List<Point2D> points = new ArrayList<>();
    points.add(new Point2D.Double(1.5, 2.5));
    points.add(new Point2D.Double(12, 13));
    points.add(new Point2D.Double(30, 40));

    PolylineData line = new PolylineData(lineTransformation.getLinePointsFromPoints(startPoint, endPoint, points));

    List<Point2D> newPoints = lineTransformation.getPointsFromLine(line);
    List<Point2D> newPoints2 = lineTransformation.getPointsFromLine(line);
    for (final Point2D point : newPoints2) {
      for (final Point2D point2 : newPoints) {
        assertFalse(point == point2);
      }
    }
  }

  @Test
  public void testGetLinePointsFromPointsShouldCreateDefensiveCopy() {
    Point2D startPoint = new Point2D.Double(1, 7);
    Point2D endPoint = new Point2D.Double(3, 5);

    List<Point2D> points = new ArrayList<>();
    points.add(new Point2D.Double(1.5, 2.5));
    points.add(new Point2D.Double(12, 13));
    points.add(new Point2D.Double(30, 40));

    List<Point2D> newPoints = lineTransformation.getLinePointsFromPoints(startPoint, endPoint, points);
    List<Point2D> newPoints2 = lineTransformation.getLinePointsFromPoints(startPoint, endPoint, points);

    for (final Point2D point : newPoints2) {
      for (final Point2D point2 : newPoints) {
        assertFalse(point == point2);
      }
    }
  }

  @Test
  public void testGetProblematicPointsFromLine() {
    Point2D startPoint = new Point2D.Double(1, 7);
    Point2D endPoint = new Point2D.Double(1, 5);

    List<Point2D> points = new ArrayList<Point2D>();
    points.add(new Point2D.Double(1.5, 2.5));
    points.add(new Point2D.Double(12, 13));
    points.add(new Point2D.Double(30, 40));

    PolylineData line = new PolylineData(lineTransformation.getLinePointsFromPoints(startPoint, endPoint, points));

    List<Point2D> newPoints = lineTransformation.getPointsFromLine(line);
    assertNotNull(newPoints);

    assertEquals(points.size(), newPoints.size());

    for (int i = 0; i < points.size(); i++) {
      assertTrue("Invalid point after transformation: " + newPoints.get(i),
          pointTransformation.isValidPoint(newPoints.get(i)));
      double dist = points.get(i).distance(newPoints.get(i));
      assertTrue("Distance to big: " + dist + " for points " + points.get(i) + " and " + newPoints.get(i),
          dist < 1e-6);
      assertTrue(pointTransformation.isValidPoint(newPoints.get(i)));
    }
  }

  @Test
  public void testGetProblematicPointsFromLine2() {
    Point2D startPoint = new Point2D.Double(5, 1);
    Point2D endPoint = new Point2D.Double(70, 1);

    List<Point2D> points = new ArrayList<Point2D>();
    points.add(new Point2D.Double(1.5, 2.5));
    points.add(new Point2D.Double(12, 13));
    points.add(new Point2D.Double(-12, -13));
    points.add(new Point2D.Double(30, 40));

    PolylineData line = new PolylineData(lineTransformation.getLinePointsFromPoints(startPoint, endPoint, points));

    List<Point2D> newPoints = lineTransformation.getPointsFromLine(line);
    assertNotNull(newPoints);

    assertEquals(points.size(), newPoints.size());

    for (int i = 0; i < points.size(); i++) {
      assertTrue("Invalid point after transformation: " + newPoints.get(i),
          pointTransformation.isValidPoint(newPoints.get(i)));
      double dist = points.get(i).distance(newPoints.get(i));
      assertTrue("Distance to big: " + dist + " for points " + points.get(i) + " and " + newPoints.get(i),
          dist < 1e-6);
      assertTrue(pointTransformation.isValidPoint(newPoints.get(i)));
    }
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetLinePointsFromInvalidPoints() {
    Point2D startPoint = new Point2D.Double(Double.POSITIVE_INFINITY, 1);
    Point2D endPoint = new Point2D.Double(70, 1);

    lineTransformation.getLinePointsFromPoints(startPoint, endPoint, new ArrayList<>());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetLinePointsFromInvalidPoints2() {
    Point2D endPoint = new Point2D.Double(Double.POSITIVE_INFINITY, 1);
    Point2D startPoint = new Point2D.Double(70, 1);

    lineTransformation.getLinePointsFromPoints(startPoint, endPoint, new ArrayList<>());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetLinePointsFromInvalidPoints3() {
    Point2D endPoint = new Point2D.Double(70, 2);
    Point2D startPoint = new Point2D.Double(70, 1);
    List<Point2D> list = new ArrayList<>();
    list.add(new Point2D.Double(Double.POSITIVE_INFINITY, 1));

    lineTransformation.getLinePointsFromPoints(startPoint, endPoint, list);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetPointsFromInvalidLine() {
    lineTransformation.getPointsFromLine(null);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetPointsFromInvalidLine2() {
    lineTransformation.getPointsFromLine(new PolylineData());
  }

  @Test
  public void testGetPointsFromLineStartingAndEndingInTheSamePoint() {
    Point2D startPoint = new Point2D.Double(1, 5);
    Point2D endPoint = new Point2D.Double(1, 5);

    List<Point2D> points = new ArrayList<Point2D>();
    points.add(new Point2D.Double(1.5, 2.5));
    points.add(new Point2D.Double(12, 13));
    points.add(new Point2D.Double(30, 40));

    PolylineData line = new PolylineData(lineTransformation.getLinePointsFromPoints(startPoint, endPoint, points));

    for (int i = 0; i < points.size(); i++) {
      Point2D point = line.getLines().get(i).getP2();

      assertEquals(startPoint.getX() + points.get(i).getX(), point.getX(), Configuration.EPSILON);
      assertEquals(startPoint.getY() + points.get(i).getY(), point.getY(), Configuration.EPSILON);
    }
  }

}
