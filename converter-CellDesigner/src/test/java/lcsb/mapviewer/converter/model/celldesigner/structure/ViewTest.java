package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.converter.model.celldesigner.structure.fields.View;

public class ViewTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    Point2D innerPosition = new Point2D.Double();
    View view = new View();
    view.setInnerPosition(innerPosition);
    assertEquals(innerPosition, view.getInnerPosition());
  }

}
