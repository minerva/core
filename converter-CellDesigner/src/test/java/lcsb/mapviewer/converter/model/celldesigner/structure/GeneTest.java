package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;

public class GeneTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CellDesignerGene());
  }

  @Test
  public void testConstructor1() {
    CellDesignerGene original = new CellDesignerGene();
    original.addModificationResidue(new CellDesignerModificationResidue());
    CellDesignerGene gene = new CellDesignerGene(original);
    assertNotNull(gene);
  }

  @Test
  public void testGetters() {
    List<CellDesignerModificationResidue> modificationResidues = new ArrayList<>();
    CellDesignerGene gene = new CellDesignerGene(new CellDesignerSpecies<>());
    gene.setModificationResidues(modificationResidues);
    assertEquals(modificationResidues, gene.getModificationResidues());
  }

  @Test
  public void testCopy() {
    CellDesignerGene degraded = new CellDesignerGene().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    CellDesignerGene gene = Mockito.spy(CellDesignerGene.class);
    gene.copy();
  }

  @Test
  public void testUpdate() {
    CellDesignerGene gene = new CellDesignerGene();
    CellDesignerGene gene2 = new CellDesignerGene();
    List<CellDesignerModificationResidue> residues = new ArrayList<>();
    residues.add(new CellDesignerModificationResidue());

    gene2.setModificationResidues(residues);

    gene.update(gene2);
    assertEquals(1, gene.getModificationResidues().size());
  }

  @Test
  public void testAddModificationResidue() {
    CellDesignerGene gene = new CellDesignerGene();
    CellDesignerModificationResidue mr = new CellDesignerModificationResidue("id1");
    CellDesignerModificationResidue mr2 = new CellDesignerModificationResidue("id1");
    CellDesignerModificationResidue mr3 = new CellDesignerModificationResidue("id2");

    gene.addModificationResidue(mr);
    assertEquals(1, gene.getModificationResidues().size());
    gene.addModificationResidue(mr2);
    assertEquals(1, gene.getModificationResidues().size());
    gene.addModificationResidue(mr3);
    assertEquals(2, gene.getModificationResidues().size());
  }

}
