package lcsb.mapviewer.converter.model.celldesigner.annotation;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class NoteFieldTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testVallues() {
    for (final NoteField field : NoteField.values()) {
      assertNotNull(NoteField.valueOf(field.toString()));
      assertNotNull(field.getClazz());
    }
  }

}
