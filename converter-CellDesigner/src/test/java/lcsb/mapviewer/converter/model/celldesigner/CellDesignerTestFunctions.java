package lcsb.mapviewer.converter.model.celldesigner;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.tests.TestUtils;
import lcsb.mapviewer.common.tests.UnitTestFailedWatcher;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerSpecies;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.compartment.BottomSquareCompartment;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.LeftSquareCompartment;
import lcsb.mapviewer.model.map.compartment.RightSquareCompartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.compartment.TopSquareCompartment;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.geom.Point2D;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;

public abstract class CellDesignerTestFunctions extends TestUtils {

  protected static Logger logger = LogManager.getLogger();

  private static int identifierCounter = 0;

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();

  protected static Model getModelForFile(final String fileName) throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    return parser.createModel(new ConverterParams().filename(fileName).sizeAutoAdjust(false));
  }

  protected void testXmlSerialization(final Model model)
      throws InconsistentModelException, UnsupportedEncodingException, InvalidInputDataExecption {
    final Model model2 = serializeModel(model);

    final ModelComparator comparator = new ModelComparator();

    // import of unbounded compartments is problematic
    for (final Compartment compartment : model.getCompartments()) {
      if (compartment instanceof BottomSquareCompartment
          || compartment instanceof TopSquareCompartment
          || compartment instanceof LeftSquareCompartment
          || compartment instanceof RightSquareCompartment) {
        model2.getElementByElementId(compartment.getElementId()).setWidth(compartment.getWidth());
        model2.getElementByElementId(compartment.getElementId()).setHeight(compartment.getHeight());
      }
    }
    assertEquals(0, comparator.compare(model, model2));
  }

  protected Model serializeModel(final Model model)
      throws InconsistentModelException, UnsupportedEncodingException, InvalidInputDataExecption {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final String xmlString = parser.model2String(model);
    final InputStream is = new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8));

    final Model model2 = parser.createModel(new ConverterParams().inputStream(is, model.getName()).sizeAutoAdjust(false));
    for (final Drawable bioEntity : model2.getDrawables()) {
      bioEntity.setZ(null);
    }
    for (final Drawable bioEntity : model.getDrawables()) {
      bioEntity.setZ(null);
    }
    return model2;
  }

  private static int idCounter = 0;

  protected static GenericProtein createProtein() {
    final GenericProtein protein = new GenericProtein("id" + (idCounter++));
    protein.setActivity(true);
    protein.setFontSize(4);
    protein.setStateLabel("xxx");
    protein.setStatePrefix("yyy");

    assignCoordinates(30, 40, 200, 20, protein);

    return protein;
  }

  protected static Rna createRna() {
    final Rna rna = new Rna("id" + (idCounter++));

    assignCoordinates(30, 40, 10, 20, rna);

    return rna;
  }

  protected static Gene createGene() {
    final Gene rna = new Gene("id" + (idCounter++));

    assignCoordinates(30, 40, 10, 20, rna);

    return rna;
  }

  protected SimpleMolecule createSimpleMolecule() {
    final SimpleMolecule result = new SimpleMolecule("id" + (idCounter++));
    assignCoordinates(50, 60, 20, 20, result);
    return result;
  }

  protected static Compartment createCompartment() {
    final Compartment compartment = new SquareCompartment("comp_id" + (idCounter++));
    compartment.setName("name");
    assignCoordinates(13, 14, 100, 120, compartment);
    compartment.setNameVerticalAlign(VerticalAlign.TOP);
    compartment.setNameHorizontalAlign(HorizontalAlign.LEFT);

    return compartment;
  }

  protected static LayerText createText() {
    final LayerText layerText = new LayerText();
    layerText.setX(256.0);
    layerText.setY(79.0);
    layerText.setWidth(233.0);
    layerText.setHeight(188.0);
    layerText.setZ(0);
    layerText.setNotes("asd as");
    layerText.setColor(Color.BLACK);
    return layerText;
  }

  protected static SbmlFunction createFunction() throws InvalidXmlSchemaException {
    SbmlFunction fun = new SbmlFunction("fun_id" + (idCounter++));
    fun.setDefinition("<math xmlns=\"http://www.w3.org/1998/Math/MathML\">\n"
        + "<lambda>\n"
        + "<apply>\n"
        + "<plus/>\n"
        + "<ci> 1 </ci>\n"
        + "<ci> 2 </ci>\n"
        + "</apply>\n"
        + "</lambda>\n"
        + "</math>\n");
    fun.setName("cool function name");
    return fun;
  }

  protected static SbmlParameter createParameter() {
    final SbmlParameter param = new SbmlParameter("param_id" + (idCounter++));
    param.setName("param name");
    param.setValue(1.0);
    return param;
  }

  protected static StructuralState createStructuralState(final Species species, final String value) {
    final StructuralState state = new StructuralState();

    state.setZ(zIndex++);
    state.setFontSize(CellDesignerSpecies.STRUCTURAL_STATE_FONT_SIZE);
    state.setName(value);

    final Font font = new Font(Font.SANS_SERIF, 0, CellDesignerSpecies.STRUCTURAL_STATE_FONT_SIZE);
    final Canvas c = new Canvas();
    final FontMetrics fm = c.getFontMetrics(font);

    double width = CellDesignerSpecies.MIN_STRUCTURAL_STATE_WIDTH;
    final double height = CellDesignerSpecies.MIN_STRUCTURAL_STATE_HEIGHT;
    width = Math.max(CellDesignerSpecies.MIN_STRUCTURAL_STATE_WIDTH,
        fm.stringWidth(state.getName()) + CellDesignerSpecies.TEXT_MARGIN_FOR_STRUCTURAL_STATE_DESC);

    width = Math.min(width, species.getWidth());

    state.setWidth(width);
    state.setHeight(height);
    state.setPosition(new Point2D.Double(species.getCenterX() - state.getWidth() / 2, species.getY() - state.getHeight() / 2));

    return state;
  }

  protected StructuralState createStructuralState(final Element element) {
    final StructuralState structuralState = new StructuralState("state" + (identifierCounter++));
    structuralState.setName("xxx" + (identifierCounter++));
    structuralState.setPosition(new Point2D.Double(element.getX(), element.getY() - 10));
    structuralState.setWidth(element.getWidth());
    structuralState.setHeight(20.0);
    structuralState.setFontSize(10.0);
    structuralState.setZ(element.getZ() + 2);
    structuralState.setBorderColor(Color.GREEN);
    return structuralState;
  }

  protected static SbmlUnit createUnit() {
    final SbmlUnit unit = new SbmlUnit("volume");
    unit.setName("volume");
    return unit;
  }

  private static int zIndex = 10;

  protected static void assignCoordinates(final Element element) {
    assignCoordinates(10, 20, 30, 40, element);
  }

  protected static void assignCoordinates(final double x, final double y, final double width, final double height,
                                          final Element element) {
    element.setX(x);
    element.setY(y);
    element.setZ(zIndex++);
    element.setWidth(width);
    element.setHeight(height);
    element.setNameX(x);
    element.setNameY(y);
    element.setNameWidth(width);
    element.setNameHeight(height);
    element.setNameVerticalAlign(VerticalAlign.MIDDLE);
    element.setNameHorizontalAlign(HorizontalAlign.CENTER);
  }

  protected static Model createEmptyModel() {
    final Model model = new ModelFullIndexed(null);
    model.setIdModel("as");
    model.setWidth(100);
    model.setHeight(100);
    return model;
  }

  protected Residue createResidue(final Protein protein) {
    final Residue residue = new Residue();
    residue.setIdModificationResidue("mr" + idCounter++);
    residue.setName("residue");
    residue.setHeight(10);
    residue.setWidth(10);
    residue.setPosition(protein.getX(), protein.getY());
    residue.setNameX(protein.getX());
    residue.setNameY(protein.getY());
    return residue;
  }

  protected BindingRegion createBindingRegion(final Protein protein) {
    final BindingRegion bindingRegion = new BindingRegion();
    bindingRegion.setIdModificationResidue("mr" + idCounter++);
    bindingRegion.setName("residue");
    bindingRegion.setHeight(10);
    bindingRegion.setWidth(10);
    bindingRegion.setPosition(protein.getX(), protein.getY());
    bindingRegion.setNameX(protein.getX());
    bindingRegion.setNameY(protein.getY());

    return bindingRegion;
  }

  protected static Layer createLayer() {
    final Layer result = new Layer();
    result.setLayerId(idCounter++);
    result.setName(faker.name().name());
    result.setZ(faker.number().numberBetween(1, 100));
    return result;
  }
}
