package lcsb.mapviewer.converter.model.celldesigner.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerAntisenseRna;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerComplexSpecies;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGene;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGenericProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerRna;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerSpecies;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;

public class SpeciesCollectionXmlParserTest extends CellDesignerTestFunctions {
  private SpeciesCollectionXmlParser parser = null;

  private String testRnaListXmlFile = "testFiles/xmlNodeTestExamples/cd_rna_collection.xml";
  private String testGeneListXmlFile = "testFiles/xmlNodeTestExamples/cd_gene_collection.xml";
  private String testAntisenseRnaListXmlFile = "testFiles/xmlNodeTestExamples/cd_antisense_rna_collection.xml";
  private String testIncludedListXmlFile = "testFiles/xmlNodeTestExamples/cd_included_species_collection.xml";
  private String testProteinListXmlFile = "testFiles/xmlNodeTestExamples/cd_protein_collection.xml";
  private String testSbmlListXmlFile = "testFiles/xmlNodeTestExamples/cd_sbml_collection.xml";

  private CellDesignerElementCollection elements;

  private int idCounter = 0;

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();
    parser = new SpeciesCollectionXmlParser(elements, new HashSet<>());
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseXmlRnaCollection() throws Exception {
    String xmlString = readFile(testRnaListXmlFile);
    Node node = getNodeFromXmlString(xmlString);
    List<Pair<String, ? extends CellDesignerSpecies<?>>> list = parser.parseXmlRnaCollection(node);
    assertEquals(2, list.size());
    assertTrue(list.get(0).getRight() instanceof CellDesignerRna);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlRnaCollection() throws Exception {
    String xmlString = readFile("testFiles/invalid/rna_collection.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseXmlRnaCollection(node);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlGeneCollection() throws Exception {
    String xmlString = readFile("testFiles/invalid/gene_collection.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseXmlGeneCollection(node);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlAntisenseRnaCollection() throws Exception {
    String xmlString = readFile("testFiles/invalid/antisense_rna_collection.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseXmlAntisenseRnaCollection(node);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlPRoteinCollection() throws Exception {
    String xmlString = readFile("testFiles/invalid/protein_collection.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseXmlProteinCollection(node);
  }

  @Test
  public void testToXmlStringRnaCollection() throws Exception {
    String xmlString = readFile(testRnaListXmlFile);
    Node node = getNodeFromXmlString(xmlString);
    List<Pair<String, ? extends CellDesignerSpecies<?>>> list = parser.parseXmlRnaCollection(node);
    List<Rna> arList = new ArrayList<>();
    for (Pair<String, ? extends CellDesignerSpecies<?>> pair : list) {
      arList.add((Rna) pair.getRight().createModelElement("" + (idCounter++)));
    }
    String convertedString = parser.rnaCollectionToXmlString(arList);
    assertNotNull(convertedString);
    node = getNodeFromXmlString(convertedString);
    assertEquals("celldesigner:listOfRNAs", node.getNodeName());
    List<Pair<String, ? extends CellDesignerSpecies<?>>> list2 = parser.parseXmlRnaCollection(node);
    assertNotNull(list2);
    assertEquals(list.size(), list2.size());
    assertEquals(list.get(0).getClass().getName(), list2.get(0).getClass().getName());
  }

  @Test
  public void testParseXmlGeneCollection() throws Exception {
    String xmlString = readFile(testGeneListXmlFile);
    Node node = getNodeFromXmlString(xmlString);
    List<Pair<String, ? extends CellDesignerSpecies<?>>> list = parser.parseXmlGeneCollection(node);
    assertEquals(2, list.size());
    assertTrue(list.get(0).getRight() instanceof CellDesignerGene);
  }

  @Test
  public void testToXmlStringGeneCollection() throws Exception {
    String xmlString = readFile(testGeneListXmlFile);
    Node node = getNodeFromXmlString(xmlString);
    List<Pair<String, ? extends CellDesignerSpecies<?>>> list = parser.parseXmlGeneCollection(node);
    List<Gene> arList = new ArrayList<>();
    for (Pair<String, ? extends CellDesignerSpecies<?>> pair : list) {
      arList.add((Gene) pair.getRight().createModelElement("" + (idCounter++)));
    }
    String convertedString = parser.geneCollectionToXmlString(arList);
    assertNotNull(convertedString);
    node = getNodeFromXmlString(convertedString);
    assertEquals("celldesigner:listOfGenes", node.getNodeName());
    List<Pair<String, ? extends CellDesignerSpecies<?>>> list2 = parser.parseXmlGeneCollection(node);
    assertNotNull(list2);
    assertEquals(list.size(), list2.size());
    assertEquals(list.get(0).getClass().getName(), list2.get(0).getClass().getName());
  }

  @Test
  public void testParseXmlProteinCollection() throws Exception {
    String xmlString = readFile(testProteinListXmlFile);
    Node node = getNodeFromXmlString(xmlString);
    List<Pair<String, ? extends CellDesignerSpecies<?>>> list = parser.parseXmlProteinCollection(node);
    assertEquals(4, list.size());
    assertTrue(list.get(0).getRight() instanceof CellDesignerProtein);
  }

  @Test
  public void testToXmlStringProteinCollection() throws Exception {
    String xmlString = readFile(testProteinListXmlFile);
    Node node = getNodeFromXmlString(xmlString);
    List<Pair<String, ? extends CellDesignerSpecies<?>>> list = parser.parseXmlProteinCollection(node);
    List<Protein> arList = new ArrayList<>();
    for (Pair<String, ? extends CellDesignerSpecies<?>> pair : list) {
      arList.add((Protein) pair.getRight().createModelElement("" + idCounter++));
    }
    String convertedString = parser.proteinCollectionToXmlString(arList);
    assertNotNull(convertedString);
    node = getNodeFromXmlString(convertedString);
    assertEquals("celldesigner:listOfProteins", node.getNodeName());
    List<Pair<String, ? extends CellDesignerSpecies<?>>> list2 = parser.parseXmlProteinCollection(node);
    assertNotNull(list2);
    assertEquals(list.size(), list2.size());
    assertEquals(list.get(0).getClass().getName(), list2.get(0).getClass().getName());
  }

  @Test
  public void testParseXmlAntisenseRnaCollection() throws Exception {
    String xmlString = readFile(testAntisenseRnaListXmlFile);
    Node node = getNodeFromXmlString(xmlString);
    List<Pair<String, ? extends CellDesignerSpecies<?>>> list = parser.parseXmlAntisenseRnaCollection(node);
    assertEquals(2, list.size());
    assertTrue(list.get(0).getRight() instanceof CellDesignerAntisenseRna);
  }

  @Test
  public void testToXmlStringAntisenseRnaCollection() throws Exception {
    String xmlString = readFile(testAntisenseRnaListXmlFile);
    Node node = getNodeFromXmlString(xmlString);
    List<Pair<String, ? extends CellDesignerSpecies<?>>> list = parser.parseXmlAntisenseRnaCollection(node);
    List<AntisenseRna> arList = new ArrayList<>();
    for (Pair<String, ? extends CellDesignerSpecies<?>> pair : list) {
      arList.add((AntisenseRna) pair.getRight().createModelElement("" + (idCounter++)));
    }
    String convertedString = parser.antisenseRnaCollectionToXmlString(arList);
    assertNotNull(convertedString);
    node = getNodeFromXmlString(convertedString);
    assertEquals("celldesigner:listOfAntisenseRNAs", node.getNodeName());
    List<Pair<String, ? extends CellDesignerSpecies<?>>> list2 = parser.parseXmlAntisenseRnaCollection(node);
    assertNotNull(list2);
    assertEquals(list.size(), list2.size());
    assertEquals(list.get(0).getClass().getName(), list2.get(0).getClass().getName());
  }

  @Test
  public void testParseSbmlSpeciesCollection() throws Exception {
    String xmlString = readFile(testSbmlListXmlFile);
    Node node = getNodeFromXmlString(xmlString);
    List<Pair<String, ? extends CellDesignerSpecies<?>>> list = parser.parseSbmlSpeciesCollection(node);
    assertEquals(9, list.size());
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidSbmlSpeciesCollection() throws Exception {
    String xmlString = readFile("testFiles/invalid/sbml_collection.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseSbmlSpeciesCollection(node);
  }

  @Test
  public void testToSbmlStringSpeciesCollection() throws Exception {
    String xmlString = readFile(testSbmlListXmlFile);
    Node node = getNodeFromXmlString(xmlString);
    List<Pair<String, ? extends CellDesignerSpecies<?>>> list = parser.parseSbmlSpeciesCollection(node);
    Model model = new ModelFullIndexed(null);
    int x = 0;
    List<Species> speciesList = new ArrayList<>();
    for (Pair<String, ? extends CellDesignerSpecies<?>> el : list) {
      CellDesignerSpecies<?> species = el.getRight();
      if (species.getClass() == CellDesignerProtein.class) {
        species = new CellDesignerGenericProtein(species);
      }

      Species alias = species.createModelElement("alias" + (x++));
      model.addElement(alias);
      speciesList.add(alias);
    }
    model.addElement(new Compartment("default"));
    String convertedString = parser.speciesCollectionToSbmlString(speciesList);
    assertNotNull(convertedString);
    node = getNodeFromXmlString(convertedString);
    assertEquals("listOfSpecies", node.getNodeName());
    List<Pair<String, ? extends CellDesignerSpecies<?>>> list2 = parser.parseSbmlSpeciesCollection(node);
    assertNotNull(list2);
    assertEquals(list.size(), list2.size());
    assertEquals(list.get(0).getClass().getName(), list2.get(0).getClass().getName());
  }

  @Test
  public void testParseIncludedSpeciesCollection() throws Exception {
    String xmlString = readFile(testIncludedListXmlFile);
    Node node = getNodeFromXmlString(xmlString);
    List<Pair<String, ? extends CellDesignerSpecies<?>>> list = parser.parseIncludedSpeciesCollection(node);
    assertEquals(3, list.size());
    int complexes = 0;
    int proteins = 0;
    for (Pair<String, ? extends CellDesignerSpecies<?>> species : list) {
      if (species.getRight() instanceof CellDesignerComplexSpecies) {
        complexes++;
      }
      if (species.getRight() instanceof CellDesignerProtein) {
        proteins++;
      }
    }
    assertEquals(1, complexes);
    assertEquals(2, proteins);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidIncludedSpeciesCollection() throws Exception {
    String xmlString = readFile("testFiles/invalid/included_species_collection.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseIncludedSpeciesCollection(node);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidIncludedSpeciesCollection2() throws Exception {
    String xmlString = readFile("testFiles/invalid/included_species_collection2.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseIncludedSpeciesCollection(node);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidIncludedSpeciesCollection3() throws Exception {
    String xmlString = readFile("testFiles/invalid/included_species_collection3.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseIncludedSpeciesCollection(node);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidIncludedSpeciesCollection4() throws Exception {
    String xmlString = readFile("testFiles/invalid/included_species_collection4.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseIncludedSpeciesCollection(node);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidIncludedSpeciesCollection5() throws Exception {
    String xmlString = readFile("testFiles/invalid/included_species_collection5.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseIncludedSpeciesCollection(node);
  }

  @Test
  public void artifitialTest() throws Exception {
    // tests some parts of the code that should never be called and therefore
    // throw exceptions

    // this is only for test coverage purpose

    SpeciesCollectionXmlParser parser = new SpeciesCollectionXmlParser(elements, new HashSet<>());
    Field field = SpeciesCollectionXmlParser.class.getDeclaredField("helpParser");
    field.setAccessible(true);
    try {
      ((AbstractElementXmlParser<?, ?>) field.get(parser)).toXml(null);
      fail("Exception expected");
    } catch (final NotImplementedException e) {

    }
    try {
      ((AbstractElementXmlParser<?, ?>) field.get(parser)).parseXmlElement((Node) null);
      fail("Exception expected");
    } catch (final NotImplementedException e) {

    }
  }

}
