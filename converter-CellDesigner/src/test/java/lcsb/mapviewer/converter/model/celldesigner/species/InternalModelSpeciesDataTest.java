package lcsb.mapviewer.converter.model.celldesigner.species;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerSpecies;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class InternalModelSpeciesDataTest extends CellDesignerTestFunctions {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    InternalModelSpeciesData data = new InternalModelSpeciesData();
    assertNotNull(data.getAll());
    assertNotNull(data.getAntisenseRnas());
    assertNotNull(data.getGenes());
    assertNotNull(data.getProteins());
    assertNotNull(data.getRnas());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testUpdateUnknownSpecies() {
    InternalModelSpeciesData data = new InternalModelSpeciesData();
    data.updateSpecies(new CellDesignerSpecies<GenericProtein>(), null);
  }

}
