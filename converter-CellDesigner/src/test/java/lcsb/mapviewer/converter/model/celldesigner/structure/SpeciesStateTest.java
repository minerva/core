package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.SpeciesState;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationSite;

public class SpeciesStateTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    List<CellDesignerModificationResidue> modifications = new ArrayList<>();
    SpeciesState state = new SpeciesState();
    state.setModifications(modifications);
    assertEquals(modifications, state.getModifications());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetHomodimer() {
    SpeciesState state = new SpeciesState();

    state.setHomodimer("inv");
  }

  @Test
  public void testConstructorFromRna() {
    Rna rna = new Rna("1");
    rna.setWidth(60);
    rna.setHeight(10);
    CodingRegion mr = new CodingRegion();
    mr.setPosition(new Point2D.Double(10, 40));
    rna.addCodingRegion(mr);
    SpeciesState state = new SpeciesState(rna);
    assertEquals(1, state.getModifications().size());
  }

  @Test
  public void testConstructorFromGene() {
    Gene gene = new Gene("2");
    gene.setWidth(60);
    gene.setHeight(10);
    ModificationSite mr = new ModificationSite();
    mr.setPosition(new Point2D.Double(10, 40));
    gene.addModificationSite(mr);
    SpeciesState state = new SpeciesState(gene);
    assertEquals(1, state.getModifications().size());
  }

  @Test
  public void testAddModifResidue() {
    SpeciesState state = new SpeciesState();
    state.addModificationResidue(new CellDesignerModificationResidue());
    CellDesignerModificationResidue mr = new CellDesignerModificationResidue();
    mr.setName("a");
    state.addModificationResidue(mr);
    assertEquals(1, state.getModifications().size());
    assertEquals("a", state.getModifications().get(0).getName());
  }

}
