package lcsb.mapviewer.converter.model.celldesigner.alias;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerCompartment;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerComplexSpecies;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGenericProtein;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class AliasCollectionXmlParserTest extends CellDesignerTestFunctions {

  private AliasCollectionXmlParser parser;
  private Model model;
  private CellDesignerElementCollection elements;

  @Before
  public void setUp() throws Exception {
    model = new ModelFullIndexed(null);

    elements = new CellDesignerElementCollection();

    parser = new AliasCollectionXmlParser(elements, model);

  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseXmlSpeciesAliasCollection() throws Exception {
    elements.addElement(new CellDesignerGenericProtein("s2594"));
    elements.addElement(new CellDesignerGenericProtein("s2595"));
    elements.addElement(new CellDesignerGenericProtein("s2596"));
    elements.addElement(new CellDesignerGenericProtein("s2506"));
    String xmlString = readFile("testFiles/xmlNodeTestExamples/cd_species_alias_collection.xml");
    Node node = getNodeFromXmlString(xmlString);
    List<Species> list = parser.parseXmlSpeciesAliasCollection(node);
    assertEquals(0, getWarnings().size());
    assertNotNull(list);
    assertEquals(4, list.size());
    Species aliasSa8 = null;
    Species aliasSa9 = null;
    Species aliasSa10 = null;
    Species aliasSa11 = null;
    for (final Species alias : list) {
      if (alias.getElementId().equals("sa8")) {
        aliasSa8 = alias;
      }
      if (alias.getElementId().equals("sa9")) {
        aliasSa9 = alias;
      }
      if (alias.getElementId().equals("sa10")) {
        aliasSa10 = alias;
      }
      if (alias.getElementId().equals("sa11")) {
        aliasSa11 = alias;
      }
    }
    assertNotNull(aliasSa8);
    assertNotNull(aliasSa9);
    assertNotNull(aliasSa10);
    assertNotNull(aliasSa11);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlSpeciesAliasCollection() throws Exception {
    String xmlString = readFile("testFiles/invalid/species_alias_collection.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseXmlSpeciesAliasCollection(node);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlComplexAliasCollection() throws Exception {
    String xmlString = readFile("testFiles/invalid/complex_alias_collection.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseXmlComplexAliasCollection(node);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlCompartmentAliasCollection() throws Exception {
    String xmlString = readFile("testFiles/invalid/compartment_alias_collection.xml");
    Node node = getNodeFromXmlString(xmlString);
    parser.parseXmlCompartmentAliasCollection(node);
  }

  @Test
  public void testSpeciesAliasCollectionToXmlString() throws Exception {
    List<Species> originalLlist = createSpeciesList();
    String xmlString2 = parser.speciesAliasCollectionToXmlString(originalLlist);
    assertNotNull(xmlString2);

    for (final Species alias : originalLlist) {
      elements.addElement(new CellDesignerGenericProtein(elements.getElementId(alias)));
    }

    Node node = getNodeFromXmlString(xmlString2);
    List<Species> list = parser.parseXmlSpeciesAliasCollection(node);
    assertEquals(0, getWarnings().size());
    assertNotNull(list);
    assertEquals(3, list.size());
    for (final Species species : originalLlist) {
      boolean found = false;
      for (final Species alias : list) {
        if (alias.getElementId().equals(species.getElementId())) {
          found = true;
        }
      }
      assertTrue(found);
    }
  }

  private List<Species> createSpeciesList() {
    List<Species> result = new ArrayList<>();
    result.add(createProtein("sa1"));
    result.add(createProtein("sa2"));
    result.add(createProtein("sa3"));
    return result;
  }

  private Species createProtein(final String id) {
    GenericProtein protein = new GenericProtein(id);
    protein.setActivity(true);
    protein.setFontSize(4);
    protein.setHeight(10);
    protein.setWidth(20);
    protein.setX(30);
    protein.setY(40);
    protein.setName(id + "name");
    protein.setStateLabel("xxx");
    protein.setStatePrefix("yyy");
    return protein;
  }

  @Test
  public void testParseXmlComplexAliasCollection() throws Exception {
    elements.addElement(new CellDesignerComplexSpecies("s2597"));
    elements.addElement(new CellDesignerComplexSpecies("s2598"));
    elements.addElement(new CellDesignerComplexSpecies("s2599"));
    String xmlString = readFile("testFiles/xmlNodeTestExamples/cd_complex_alias_collection.xml");
    Node node = getNodeFromXmlString(xmlString);
    List<Complex> list = parser.parseXmlComplexAliasCollection(node);
    assertEquals(0, getWarnings().size());
    assertNotNull(list);
    assertEquals(3, list.size());
    Complex aliasSa1 = null;
    Complex aliasSa2 = null;
    Complex aliasSa3 = null;
    for (final Complex alias : list) {
      if (alias.getElementId().equals("csa1")) {
        aliasSa1 = alias;
      }
      if (alias.getElementId().equals("csa2")) {
        aliasSa2 = alias;
      }
      if (alias.getElementId().equals("csa3")) {
        aliasSa3 = alias;
      }
    }
    assertNotNull(aliasSa1);
    assertNotNull(aliasSa2);
    assertNotNull(aliasSa3);
  }

  @Test
  public void testComplexAliasCollectionToXmlString() throws Exception {
    List<Complex> list = createComplexAliasList();
    String xmlString2 = parser.complexAliasCollectionToXmlString(list);
    assertNotNull(xmlString2);

    for (final Complex complexAlias : list) {
      elements.addElement(new CellDesignerComplexSpecies(elements.getElementId(complexAlias)));
    }

    Node node = getNodeFromXmlString(xmlString2);
    list = parser.parseXmlComplexAliasCollection(node);
    assertEquals(0, getWarnings().size());
    assertNotNull(list);
    assertEquals(3, list.size());
    Complex aliasSa1 = null;
    Complex aliasSa2 = null;
    Complex aliasSa3 = null;
    for (final Complex alias : list) {
      if (alias.getElementId().equals("csa1")) {
        aliasSa1 = alias;
      }
      if (alias.getElementId().equals("csa2")) {
        aliasSa2 = alias;
      }
      if (alias.getElementId().equals("csa3")) {
        aliasSa3 = alias;
      }
    }
    assertNotNull(aliasSa1);
    assertNotNull(aliasSa2);
    assertNotNull(aliasSa3);
  }

  private List<Complex> createComplexAliasList() {
    List<Complex> result = new ArrayList<>();
    result.add(createComplex("csa1"));
    result.add(createComplex("csa2"));
    result.add(createComplex("csa3"));
    return result;
  }

  private Complex createComplex(final String id) {
    Complex complex = new Complex(id);
    complex.setFontSize(13.5);
    complex.setHeight(90);
    complex.setWidth(80);
    complex.setName("name_112_" + id);
    complex.setX(32);
    complex.setY(42);
    return complex;
  }

  @Test
  public void testParseXmlCompartmentAliasCollection() throws Exception {
    elements.addElement(new CellDesignerCompartment("c1"));
    elements.addElement(new CellDesignerCompartment("c2"));
    elements.addElement(new CellDesignerCompartment("c3"));

    String xmlString = readFile("testFiles/xmlNodeTestExamples/cd_compartment_alias_collection.xml");
    Node node = getNodeFromXmlString(xmlString);
    List<Compartment> list = parser.parseXmlCompartmentAliasCollection(node);
    assertEquals(0, getWarnings().size());
    assertNotNull(list);
    assertEquals(3, list.size());
    Compartment aliasCa1 = null;
    Compartment aliasCa2 = null;
    Compartment aliasCa3 = null;
    for (final Compartment alias : list) {
      if (alias.getElementId().equals("ca1")) {
        aliasCa1 = alias;
      }
      if (alias.getElementId().equals("ca2")) {
        aliasCa2 = alias;
      }
      if (alias.getElementId().equals("ca3")) {
        aliasCa3 = alias;
      }
    }
    assertNotNull(aliasCa1);
    assertNotNull(aliasCa2);
    assertNotNull(aliasCa3);
  }

  @Test
  public void testCompartmentAliasCollectionToXmlString() throws Exception {
    List<Compartment> originalLlist = createCompartmentList();
    for (final Compartment alias : originalLlist) {
      elements.addElement(new CellDesignerCompartment(elements.getElementId(alias)));
    }

    String xmlString2 = parser.compartmentAliasCollectionToXmlString(originalLlist);

    Node node = getNodeFromXmlString(xmlString2);
    List<Compartment> list = parser.parseXmlCompartmentAliasCollection(node);
    assertEquals(0, getWarnings().size());

    assertNotNull(list);
    assertEquals(originalLlist.size(), list.size());
    for (final Compartment species : originalLlist) {
      boolean found = false;
      for (final Compartment alias : list) {
        if (alias.getElementId().equals(species.getElementId())) {
          found = true;
        }
      }
      assertTrue(found);
    }
  }

  private List<Compartment> createCompartmentList() {
    List<Compartment> result = new ArrayList<>();
    result.add(createCompartment("ca1"));
    result.add(createCompartment("ca2"));
    result.add(createCompartment("ca3"));
    return result;
  }

  private Compartment createCompartment(final String id) {
    Compartment compartment = new SquareCompartment(id);
    compartment.setName("name" + id);
    assignCoordinates(13, 14, 100, 120, compartment);

    return compartment;
  }

}
