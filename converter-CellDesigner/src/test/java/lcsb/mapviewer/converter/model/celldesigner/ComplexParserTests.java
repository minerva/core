package lcsb.mapviewer.converter.model.celldesigner;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.AbstractSiteModification;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ComplexParserTests extends CellDesignerTestFunctions {

  private final ElementUtils eu = new ElementUtils();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseFunctions() throws Exception {
    final Model model = getModelForFile("testFiles/function.xml");

    assertEquals(1, model.getFunctions().size());
  }

  @Test
  public void testParseModelAnnotations() throws Exception {
    final Model model = getModelForFile("testFiles/model_with_annotations.xml");

    assertEquals(2, model.getMiriamData().size());
  }

  @Test
  public void testExportModelAnnotations() throws Exception {
    final Model model = getModelForFile("testFiles/model_with_annotations.xml");
    model.setName("Unknown");

    final String xml = new CellDesignerXmlParser().model2String(model);
    final ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());
    final Model model2 = new CellDesignerXmlParser()
        .createModel(new ConverterParams().sizeAutoAdjust(false).inputStream(bais));

    final ModelComparator modelComparator = new ModelComparator();
    assertEquals(0, modelComparator.compare(model, model2));
  }

  @Test
  public void testParseTextWithBackground() throws Exception {
    final Model model = getModelForFile("testFiles/layer_text_with_background.xml");

    final Layer layer = model.getLayers().iterator().next();
    boolean notGrayFound = false;
    for (final LayerText text : layer.getTexts()) {
      if (text.getBackgroundColor().equals(Color.LIGHT_GRAY)) {
        assertFalse(text.getNotes().contains("BackgroundColor"));
      } else {
        notGrayFound = true;
      }
    }
    assertTrue(notGrayFound);
  }

  @Test
  public void testParseParameters() throws Exception {
    final Model model = getModelForFile("testFiles/parameter.xml");

    assertEquals(1, model.getParameters().size());
  }

  @Test
  public void testParseElementKinetics() throws Exception {
    final Model model = getModelForFile("testFiles/elements_with_kinetic_data.xml");

    final GenericProtein protein1 = model.getElementByElementId("sa1");
    assertEquals(2.5, protein1.getInitialAmount(), Configuration.EPSILON);
    assertTrue("hasOnlySubstance property is not parsed properly", protein1.hasOnlySubstanceUnits());
    assertTrue("boundaryCondition property is not parsed properly", protein1.isBoundaryCondition());
    assertTrue("constant property is not parsed properly", protein1.isConstant());
    assertEquals(SbmlUnitType.GRAM.getCommonName(), protein1.getSubstanceUnits().getName());
  }

  @Test
  public void testParseAndSerializeKineticsElements() throws Exception {
    final Model model = getModelForFile("testFiles/elements_with_kinetic_data.xml");
    model.setName("Unknown");

    final String xml = new CellDesignerXmlParser().model2String(model);
    final ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());
    final Model model2 = new CellDesignerXmlParser()
        .createModel(new ConverterParams().sizeAutoAdjust(false).inputStream(bais));

    final ModelComparator modelComparator = new ModelComparator();
    assertEquals(0, modelComparator.compare(model, model2));
  }

  @Test
  public void testParseAndSerializeKineticsReaction() throws Exception {
    final Model model = getModelForFile("testFiles/reactions/kinetics.xml");
    model.setName("Unknown");

    final String xml = new CellDesignerXmlParser().model2String(model);
    final ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());
    final Model model2 = new CellDesignerXmlParser()
        .createModel(new ConverterParams().sizeAutoAdjust(false).inputStream(bais));

    final ModelComparator modelComparator = new ModelComparator();
    assertEquals(0, modelComparator.compare(model, model2));
  }

  @Test
  public void testParseUnits() throws Exception {
    final Model model = getModelForFile("testFiles/unit.xml");

    assertEquals(1, model.getUnits().size());
  }

  @Test
  public void testParseCompartments() throws Exception {
    final Model model = getModelForFile("testFiles/reactions/centeredAnchorInModifier.xml");

    assertEquals(0, model.getCompartments().size());
  }

  @Test
  public void testParseBubbles() throws Exception {
    final Model model = getModelForFile("testFiles/bubbles.xml");

    final Element species = model.getElementByElementId("sa6");
    assertNotNull(species);
    final Protein protein = (Protein) species;
    assertFalse(protein.getModificationResidues().isEmpty());
    boolean stateFound = false;
    for (final ModificationResidue mr : protein.getModificationResidues()) {
      if (mr instanceof StructuralState) {
        stateFound = true;
        assertNotEquals("", mr.getName());
      }
    }
    assertTrue(stateFound);
  }

  @Test
  public void testParseCompartmentsRelation() throws Exception {
    final Model model = getModelForFile("testFiles/problematic/elements_in_compartments.xml");

    final List<Element> aliases = new ArrayList<>();
    aliases.add(model.getElementByElementId("csa3"));
    aliases.add(model.getElementByElementId("csa4"));
    aliases.add(model.getElementByElementId("csa5"));
    aliases.add(model.getElementByElementId("csa6"));
    aliases.add(model.getElementByElementId("sa3"));
    aliases.add(model.getElementByElementId("sa5"));
    aliases.add(model.getElementByElementId("sa6"));
    aliases.add(model.getElementByElementId("sa7"));
    aliases.add(model.getElementByElementId("sa8"));
    aliases.add(model.getElementByElementId("sa9"));
    aliases.add(model.getElementByElementId("sa10"));
    aliases.add(model.getElementByElementId("sa11"));
    aliases.add(model.getElementByElementId("sa12"));
    aliases.add(model.getElementByElementId("sa13"));
    aliases.add(model.getElementByElementId("sa14"));
    aliases.add(model.getElementByElementId("sa15"));
    aliases.add(model.getElementByElementId("sa16"));
    aliases.add(model.getElementByElementId("sa17"));
    aliases.add(model.getElementByElementId("sa18"));
    aliases.add(model.getElementByElementId("sa19"));
    aliases.add(model.getElementByElementId("sa20"));
    for (final Element alias : aliases) {
      assertNotNull(eu.getElementTag(alias) + " does not contain info about compartment", alias.getCompartment());
    }
  }

  @Test
  public void testParseProteins() throws Exception {
    final Model model = getModelForFile("testFiles/problematic/acetyled_protein.xml");

    final Set<ModificationState> residues = new HashSet<>();
    for (final Element element : model.getElements()) {
      if (element instanceof Species) {
        final Protein protein = (Protein) element;
        for (final ModificationResidue mr : protein.getModificationResidues()) {
          if (mr instanceof AbstractSiteModification) {
            residues.add(((AbstractSiteModification) mr).getState());
          }
        }
      }
    }
    // we have a protein which is acetylated and not acetylated so two types
    // of residues
    assertEquals(2, residues.size());

    final AbstractSiteModification modification = (AbstractSiteModification) ((Protein) model.getElementByElementId("sa2"))
        .getModificationResidues().get(0);
    assertEquals(ModificationState.ACETYLATED, modification.getState());
  }

  @Test
  public void testNotesComplexesInNested() throws Exception {
    final Model model = getModelForFile("testFiles/includedSpecies.xml");
    final Element species = model.getElementByElementId("sa1");
    assertNotNull(species);
    assertNotNull(species.getNotes());
    assertTrue("Wrong notes: " + species.getNotes(), species.getNotes().contains("hello world"));
  }

  @Test
  public void testHypotheticalComplex() throws Exception {
    final Model model = getModelForFile("testFiles/problematic/hypothetical_complex.xml");
    final Complex species = model.getElementByElementId("csa1");
    assertTrue(species.isHypothetical());
    assertEquals(VerticalAlign.MIDDLE, species.getNameVerticalAlign());
  }

  @Test
  public void testProblematicAcetylation() throws Exception {
    final Model model = getModelForFile("testFiles/problematic/problematic_acetylation.xml");
    final Protein p1 = model.getElementByElementId("sa73");
    final Protein p2 = model.getElementByElementId("sa27");
    assertEquals(ModificationState.ACETYLATED, ((Residue) p1.getModificationResidues().get(0)).getState());
    assertNotEquals(ModificationState.ACETYLATED, ((Residue) p2.getModificationResidues().get(0)).getState());
  }

  @Test
  public void testDuplicateMiriam() throws Exception {
    final Model model = getModelForFile("testFiles/problematic/duplicated_miriam.xml");
    final Protein p1 = model.getElementByElementId("sa1");
    final Set<String> ids = new HashSet<>();

    assertFalse(p1.getMiriamData().isEmpty());
    for (final MiriamData md : p1.getMiriamData()) {
      if (md.getDataType().equals(MiriamType.PUBMED)) {
        assertFalse("Protein contains double pubmed annotation for pubmed id: " + md.getResource(),
            ids.contains(md.getResource()));
        ids.add(md.getResource());
      }
    }
    assertFalse(ids.isEmpty());
    assertEquals(2, getWarnings().size());
  }

  @Test
  public void testKappaInDescription() throws Exception {
    final Model model = getModelForFile("testFiles/problematic/kappa_example.xml");

    final Element species = model.getElementByElementId("sa1");

    assertFalse(species.getName().toLowerCase().contains("kappa"));
  }

  @Test
  public void testMissingXmlNodes() throws Exception {
    final Model model = getModelForFile("testFiles/missing_xml_nodes.xml");

    assertTrue(((Species) model.getElementByElementId("sa3")).isHypothetical());
    assertTrue(((Species) model.getElementByElementId("sa4")).isHypothetical());
    assertTrue(((Species) model.getElementByElementId("sa5")).isHypothetical());

    assertFalse(((Species) model.getElementByElementId("sa12")).isHypothetical());
    assertFalse(((Species) model.getElementByElementId("sa13")).isHypothetical());
    assertFalse(((Species) model.getElementByElementId("sa14")).isHypothetical());
    assertFalse(((Species) model.getElementByElementId("sa15")).isHypothetical());
  }

  @Test
  public void testParsingSpecialCharactersInName() throws Exception {
    final Model model = createEmptyModel();
    final GenericProtein alias = createProtein();
    alias.setName("name & no-name");
    model.addElement(alias);

    final Model model2 = serializeModel(model);

    final ModelComparator comparator = new ModelComparator();

    assertEquals(0, comparator.compare(model, model2));
  }

  @Test
  public void testParsingEndLineInName() throws Exception {
    final Model model = createEmptyModel();
    final GenericProtein alias = createProtein();
    alias.setName("name\rno-name");
    model.addElement(alias);

    final Model model2 = serializeModel(model);

    final ModelComparator comparator = new ModelComparator();

    assertEquals(0, comparator.compare(model, model2));
  }

  @Test
  public void testWarningInParser() throws Exception {
    getModelForFile("testFiles/problematic/invalid_elements_name.xml");
    assertFalse(getWarnings().isEmpty());
  }

  @Test
  public void testExportVcard() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/model_with_vcard.xml"));
    testXmlSerialization(model);
  }

  @Test
  public void testComplexNoBorder() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/problematic/complex_no_border.xml"));
    assertTrue(model.getElementByElementId("csa1").getWidth() > 0);
    testXmlSerialization(model);
  }

  @Test
  public void testParseCompactComplex() throws Exception {
    final Model model = getModelForFile("testFiles/complex_compact.xml");

    assertTrue(model.getElementByElementId("sa6").getX() > 218);
  }

}
