package lcsb.mapviewer.converter.model.celldesigner;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.PointComparator;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitTypeFactor;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.ModifierReactionNotation;
import lcsb.mapviewer.model.map.reaction.type.ReducedNotation;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.awt.Color;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class CellDesignerXmlParserTest extends CellDesignerTestFunctions {

  private final ModelComparator modelComparator = new ModelComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testOpenFromInputStream() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final FileInputStream fis = new FileInputStream("testFiles/sample.xml");
    final Model model = parser.createModel(new ConverterParams().inputStream(fis));
    assertNotNull(model);
  }

  @Test
  public void testZIndexAvailableForBioEntities() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/sample.xml"));
    for (final BioEntity bioEntity : model.getBioEntities()) {
      assertNotNull(bioEntity.getZ());
    }
  }

  @Test
  public void testOpenFromFile() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/sample.xml"));
    assertNotNull(model);
    assertEquals("sample", model.getName());
  }

  @Test
  public void testSubstanceUnitAsType() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser
        .createModel(new ConverterParams().filename("testFiles/problematic/substance_defined_as_type.xml")
            .sizeAutoAdjust(false));
    assertNotNull(model);
    assertNotNull(model.getSpeciesList().get(0).getSubstanceUnits());
    super.testXmlSerialization(model);
  }

  @Test
  public void testCellDesigner2_5() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser
        .createModel(new ConverterParams().filename("testFiles/problematic/MYO_signaling_pathway.xml")
            .sizeAutoAdjust(false));
    assertNotNull(model);
    super.testXmlSerialization(model);
  }

  @Test
  public void testUnknownCatalysisWithGate() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser
        .createModel(new ConverterParams().filename("testFiles/problematic/unknown_catalysis_with_gate.xml")
            .sizeAutoAdjust(false));

    final Reaction reaction = model.getReactionByReactionId("re1");
    final NodeOperator operator = reaction.getOperators().get(0);
    for (final Modifier modifiers : reaction.getModifiers()) {
      assertEquals(operator.getLine().getType(), modifiers.getLine().getType());
    }
    super.testXmlSerialization(model);
  }

  @Test
  public void testParseTransparency() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/model_with_transparency.xml"));
    assertFalse(model.getCompartments().get(0).getTransparencyLevel().isEmpty());
  }

  @Test
  public void testParseVcard() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/model_with_vcard.xml"));
    assertEquals("There is one author defined", 1, model.getAuthors().size());
    final Author author = model.getAuthors().get(0);
    assertNotNull("Author data cannot be null", author);
    assertEquals("Piotr", author.getFirstName());
    assertEquals("Gawron", author.getLastName());
    assertEquals("piotr.gawron@uni.lu", author.getEmail());
    assertEquals("LCSB", author.getOrganisation());
    assertEquals("Modification date is not defined", 1, model.getModificationDates().size());
    assertNotNull("Creation date is not defined", model.getCreationDate());
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testOpenFromInvalidFile() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.createModel(new ConverterParams().filename("testFiles/invalid/sample.xml"));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testOpenFromInvalidFile2() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.createModel(new ConverterParams().filename("testFiles/invalid/sample2.xml"));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testOpenFromInvalidFile3() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.createModel(new ConverterParams().filename("testFiles/invalid/sample10.xml"));
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseAnnotationComplexAliasesConnections() throws Exception {
    final Model model = new ModelFullIndexed(null);
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.parseAnnotationComplexAliasesConnections(model,
        super.getNodeFromXmlString(readFile("testFiles/invalid/complex_species_alias.xml")));
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseAnnotationComplexAliasesConnections2() throws Exception {
    final Model model = new ModelFullIndexed(null);
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.parseAnnotationComplexAliasesConnections(model,
        super.getNodeFromXmlString(readFile("testFiles/invalid/complex_species_alias2.xml")));
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseAnnotationAliasesConnections() throws Exception {
    final Model model = new ModelFullIndexed(null);
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.parseAnnotationAliasesConnections(model,
        super.getNodeFromXmlString(readFile("testFiles/invalid/list_of_species_alias.xml")));
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseAnnotationAliasesConnections2() throws Exception {
    final Model model = new ModelFullIndexed(null);
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.parseAnnotationAliasesConnections(model,
        super.getNodeFromXmlString(readFile("testFiles/invalid/list_of_species_alias2.xml")));
  }

  @Test
  public void testEmptyModelToXml() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final FileInputStream fis = new FileInputStream("testFiles/empty.xml");
    final Model model = parser.createModel(new ConverterParams().inputStream(fis));
    final String xmlString = parser.model2String(model);
    final InputStream is = new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8));

    final Model model2 = parser.createModel(new ConverterParams().inputStream(is));

    assertNotNull(model2);

    final ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(model, model2));

    // default compartment definition looks like (but properties order might
    // be different)
    // <compartment metaid="default" id="default" size="1" units="volume"/>

    assertTrue("There is no default compartment", xmlString.indexOf("id=\"default\"") >= 0);
  }

  @Test
  public void testSimpleModelToXml() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final FileInputStream fis = new FileInputStream("testFiles/sample.xml");
    final Model model = parser.createModel(new ConverterParams().inputStream(fis));
    final String xmlString = parser.model2String(model);

    final InputStream is = new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8));

    final Model model2 = parser.createModel(new ConverterParams().inputStream(is));

    // we have to replace size of the map because of auto resizing during
    // reading
    model2.setWidth(model.getWidth());
    model2.setHeight(model.getHeight());

    assertNotNull(model2);

    final ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(model, model2));
  }

  @Test
  public void testModifierReactionModelToXml() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final FileInputStream fis = new FileInputStream("testFiles/reactions/modification_reaction/catalysis.xml");
    final Model model = parser.createModel(new ConverterParams().inputStream(fis));
    final String xmlString = parser.model2String(model);

    final InputStream is = new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8));

    final Model model2 = parser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));
    final ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(model, model2));
  }

  @Test
  public void testAutoMapWidth() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final FileInputStream fis = new FileInputStream("testFiles/autoMapWidth.xml");
    final Model model = parser.createModel(new ConverterParams().inputStream(fis));
    assertTrue(model.getWidth() > 700);
    assertTrue(model.getHeight() > 500);
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testThrowIOException() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final InputStream fis = Mockito.mock(InputStream.class);
    when(fis.read()).thenThrow(new IOException());

    parser.createModel(new ConverterParams().inputStream(fis));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testInvalidInputFile1() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final FileInputStream fis = new FileInputStream("testFiles/invalid/invalid_CD_1.xml");
    parser.createModel(new ConverterParams().inputStream(fis));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testInvalidInputFile2() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final FileInputStream fis = new FileInputStream("testFiles/invalid/invalid_CD_2.xml");
    parser.createModel(new ConverterParams().inputStream(fis));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testInvalidInputFile3() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final FileInputStream fis = new FileInputStream("testFiles/invalid/invalid_CD_3.xml");
    parser.createModel(new ConverterParams().inputStream(fis));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testInvalidInputFile4() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final FileInputStream fis = new FileInputStream("testFiles/invalid/invalid_CD_4.xml");
    parser.createModel(new ConverterParams().inputStream(fis));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testInvalidBrokenTypeReaction() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final FileInputStream fis = new FileInputStream("testFiles/invalid/broken_type_reaction.xml");
    parser.createModel(new ConverterParams().inputStream(fis));
  }

  @Test
  public void testToXmlAfterAnnotating() throws Exception {
    final Model model = new ModelFullIndexed(null);
    model.setIdModel("as");
    model.setWidth(10);
    model.setHeight(10);
    final Species speciesAlias = createProtein();
    speciesAlias.setName("ROS");
    final RestAnnotationParser rap = new RestAnnotationParser();
    rap.processNotes(
        "Symbol: ROS1\r\nName: c-ros oncogene 1 , receptor tyrosine kinase\r\n"
            + "Description: RecName: Full=Proto-oncogene tyrosine-protein kinase ROS; EC=2.7.10.1; "
            + "AltName: Full=Proto-oncogene c-Ros; AltName: Full=Proto-oncogene c-Ros-1; "
            + "AltName: Full=Receptor tyrosine kinase c-ros oncogene 1; "
            + "AltName: Full=c-Ros receptor tyrosine kinase; Flags: Precursor;\r\n"
            + "Previous Symbols:\r\nSynonyms: ROS, MCF3",
        speciesAlias);
    model.addElement(speciesAlias);

    final GenericProtein alias = createProtein();
    alias.setName("ROS2");
    model.addElement(alias);

    final SimpleMolecule speciesAlias2 = createSimpleMolecule();
    speciesAlias2.setName("PDK1");
    rap.processNotes(
        "Symbol: ROS1\r\nName: c-ros oncogene 1 , receptor tyrosine kinase\r\n"
            + "Description: RecName: Full=Proto-oncogene tyrosine-protein kinase ROS; EC=2.7.10.1; "
            + "AltName: Full=Proto-oncogene c-Ros; AltName: Full=Proto-oncogene c-Ros-1; "
            + "AltName: Full=Receptor tyrosine kinase c-ros oncogene 1; AltName: Full=c-Ros receptor tyrosine kinase; Flags: Precursor;\r\n"
            + "Previous Symbols:\r\nSynonyms: ROS, MCF3",
        speciesAlias2);
    model.addElement(speciesAlias2);

    final SimpleMolecule alias2 = createSimpleMolecule();
    alias2.setName("PDK2");
    model.addElement(alias2);

    final Model model2 = serializeModel(model);

    assertEquals(0, modelComparator.compare(model, model2));
  }

  @Test
  public void testModelWithNullNotesToXml() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = new ModelFullIndexed(null);
    model.setWidth(20);
    model.setHeight(20);
    model.setNotes(null);
    model.setIdModel("id");
    final String xmlString = parser.model2String(model);
    final InputStream is = new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8));

    final Model model2 = parser.createModel(new ConverterParams().inputStream(is));

    assertNotNull(model2);

    assertTrue(model2.getNotes() == null || model2.getNotes().isEmpty());
  }

  @Test
  public void testExportXmlForEmptyModel() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = new ModelFullIndexed(null);
    model.setWidth(20);
    model.setHeight(20);
    model.setNotes(null);
    model.setIdModel("id");
    final Species alias = new GenericProtein("a");
    alias.setName("AA");
    model.addElement(alias);

    parser.model2String(model);
  }

  @Test
  public void testExportXmlForEmptyModel2() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = new ModelFullIndexed(null);
    model.setWidth(20);
    model.setHeight(20);
    model.setNotes(null);
    model.setIdModel("id");

    parser.model2String(model);
  }

  @Test
  public void testExportImportModelWithSpecialCharacterInNotes() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = new ModelFullIndexed(null);
    model.setWidth(20);
    model.setHeight(20);
    model.setNotes(">");
    model.setIdModel("id");
    final String xmlString = parser.model2String(model);
    final InputStream is = new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8));

    final Model model2 = parser.createModel(new ConverterParams().inputStream(is));

    assertNotNull(model2);

    final ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(model, model2));
  }

  @Test
  public void testHomodimerSpecies() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final FileInputStream fis = new FileInputStream("testFiles/homodimer.xml");
    final Model model = parser.createModel(new ConverterParams().inputStream(fis));
    for (final Element element : model.getElements()) {
      if (element instanceof Species) {
        assertTrue("Homodimer value for class" + element.getClass() + " not upadted",
            ((Species) element).getHomodimer() > 1);
      }
    }
  }

  @Test
  public void testReactionWithNegativeCoords() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final FileInputStream fis = new FileInputStream("testFiles/negativeCoords.xml");
    final Model model = parser.createModel(new ConverterParams().inputStream(fis));
    assertTrue(model.getWidth() >= 0);
    assertTrue(model.getHeight() >= 0);
  }

  @Test
  public void testaAnnotations() throws Exception {
    final CellDesignerXmlParser cellDesignerXmlParser = new CellDesignerXmlParser();
    final Model model = cellDesignerXmlParser
        .createModel(new ConverterParams().filename("testFiles/problematic/invalidAlias.xml"));
    assertNotNull(model);
  }

  @Test
  public void testProblematicModification() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser
        .createModel(new ConverterParams().filename("testFiles/problematic/problematic_modification.xml"));
    assertNotNull(model);
  }

  @Test
  public void testReactionStartAndEndEqual() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser
        .createModel(new ConverterParams().filename("testFiles/problematic/reaction_start_and_end_are_equal.xml")
            .sizeAutoAdjust(false));
    testXmlSerialization(model);
    final Reaction r = model.getReactions().iterator().next();
    assertTrue(r.getReactants().get(0).getLine().length() > 0);
    assertTrue(r.getProducts().get(0).getLine().length() > 0);
    assertTrue(r.getLine().length() > 0);
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testInvalidModifierType() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.createModel(new ConverterParams().filename("testFiles/problematic/problematic_modifier_type.xml"));
  }

  @Test
  public void testHtmlTagInSymbolName() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/notes_with_html_coding.xml"));

    final Element p = model.getElementByElementId("sa1");

    assertEquals(">symbol<", p.getSymbol());
  }

  @Test
  public void testModelBound() {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();

    final Model model = new ModelFullIndexed(null);
    model.setWidth(0);
    model.setHeight(0);

    final Rectangle2D bound = parser.getModelBound(model);
    assertEquals(0, bound.getWidth(), EPSILON);
    assertEquals(0, bound.getHeight(), EPSILON);
  }

  @Test
  public void testModelBoundWithLine() {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();

    final Layer layer = createLayer();
    final PolylineData line = new PolylineData(new Point2D.Double(2, 3), new Point2D.Double(20, 30));
    layer.addLayerLine(line);

    final Model model = new ModelFullIndexed(null);
    model.addLayer(layer);
    model.setWidth(100);
    model.setHeight(100);

    final Rectangle2D bound = parser.getModelBound(model);
    assertEquals(18, bound.getWidth(), EPSILON);
    assertEquals(27, bound.getHeight(), EPSILON);
  }

  @Test
  public void testModelBoundWithRectangle() {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();

    final Layer layer = createLayer();
    final LayerRect rect = new LayerRect();
    rect.setX(10.0);
    rect.setY(20.0);
    rect.setWidth(30.0);
    rect.setHeight(45.0);
    layer.addLayerRect(rect);

    final Model model = new ModelFullIndexed(null);
    model.addLayer(layer);
    model.setWidth(100);
    model.setHeight(100);

    final Rectangle2D bound = parser.getModelBound(model);
    assertEquals(30, bound.getWidth(), EPSILON);
    assertEquals(45, bound.getHeight(), EPSILON);
  }

  @Test
  public void testModelBoundWithOval() {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();

    final Layer layer = createLayer();
    final LayerOval oval = new LayerOval();
    oval.setX(11.0);
    oval.setY(21.0);
    oval.setWidth(31.0);
    oval.setHeight(46.0);
    layer.addLayerOval(oval);

    final Model model = new ModelFullIndexed(null);
    model.addLayer(layer);
    model.setWidth(100);
    model.setHeight(100);

    final Rectangle2D bound = parser.getModelBound(model);
    assertEquals(31, bound.getWidth(), EPSILON);
    assertEquals(46, bound.getHeight(), EPSILON);
  }

  @Test
  public void testModelBoundWithText() {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();

    final Layer layer = createLayer();
    final LayerText text = new LayerText();
    text.setX(12.0);
    text.setY(31.0);
    text.setWidth(56.0);
    text.setHeight(52.0);
    layer.addLayerText(text);

    final Model model = new ModelFullIndexed(null);
    model.addLayer(layer);
    model.setWidth(100);
    model.setHeight(100);

    final Rectangle2D bound = parser.getModelBound(model);
    assertEquals(56, bound.getWidth(), EPSILON);
    assertEquals(52, bound.getHeight(), EPSILON);
  }

  @Test
  public void testGetters() {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    assertNotNull(parser.getCommonName());
    assertNotNull(parser.getMimeType());
    assertTrue(parser.getFileExtensions().size() > 0);
  }

  @Test
  public void testToXmlWithGene() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = new ModelFullIndexed(null);
    model.setWidth(100);
    model.setHeight(100);
    final Gene gene = new Gene("gene_id_1");
    gene.setName("geneNAME");
    model.addElement(gene);
    final String xmlString = parser.model2String(model);
    assertTrue(xmlString.contains("gene_id_1"));
    assertTrue(xmlString.contains("geneNAME"));
  }

  @Test
  public void testToInputString() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = new ModelFullIndexed(null);
    model.setWidth(100);
    model.setHeight(100);
    final InputStream is = parser.model2InputStream(model);
    final BufferedReader in = new BufferedReader(new InputStreamReader(is));
    final String str = in.readLine();
    assertNotNull(str);
    assertFalse(str.isEmpty());
    is.close();
  }

  @Test
  public void testToFile() throws Exception {
    final String filename = "tmp.xml";
    if (new File(filename).exists()) {
      new File(filename).delete();
    }
    try {
      final CellDesignerXmlParser parser = new CellDesignerXmlParser();
      final Model model = new ModelFullIndexed(null);
      model.setWidth(100);
      model.setHeight(100);
      parser.model2File(model, filename);

      final File file = new File(filename);
      assertTrue(file.exists());
    } finally {
      if (new File(filename).exists()) {
        new File(filename).delete();
      }
    }
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testOpenFromInvalidSpeciesStateFile() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.createModel(new ConverterParams().filename("testFiles/invalid/sample5.xml"));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testOpenFromInvalidSpeciesStateFile2() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.createModel(new ConverterParams().filename("testFiles/invalid/sample6.xml"));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testOpenFromInvalidSpeciesStateFile3() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.createModel(new ConverterParams().filename("testFiles/invalid/sample7.xml"));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testOpenFromInvalidSpeciesStateFile5() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.createModel(new ConverterParams().filename("testFiles/invalid/sample8.xml"));
  }

  @Test(expected = InvalidInputDataExecption.class)
  public void testOpenFromInvalidSpeciesStateFile6() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    parser.createModel(new ConverterParams().filename("testFiles/invalid/sample9.xml"));
  }

  @Test
  public void testOpenProblematicFile() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/problematic/problematic_notes.xml"));
    final Element element = model.getElementByElementId("sa2338");
    assertFalse("Element note cannot contain head html tag", element.getNotes().contains("</head>"));
  }

  @Test
  public void testNestedComp() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/compartment/nested_compartments.xml"));

    assertNotNull(model.getElementByElementId("ca2").getCompartment());
    assertNotNull(model.getElementByElementId("sa1").getCompartment());

    final String xml = parser.model2String(model);

    final Model model2 = parser.createModel(
        new ConverterParams().inputStream(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8))));

    final ModelComparator comparator = new ModelComparator();

    model.setName("Unknown");
    assertEquals(0, comparator.compare(model, model2));
  }

  @Test
  public void testCompartmentWithNotes() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/compartment_with_notes.xml"));
    model.setName("Unknown");
    assertNotNull(model);
    final String str = parser.model2String(model);
    final Model model2 = parser.createModel(
        new ConverterParams().inputStream(new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8))));
    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void testCompartmentWithSubcompartments() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(
        new ConverterParams().filename("testFiles/compartment/nested_compartments_in_few_compartments.xml"));
    final Compartment c3 = model.getElementByElementId("ca3");
    final Compartment c4 = model.getElementByElementId("ca4");
    final Compartment c1 = model.getElementByElementId("ca1");
    final Compartment c2 = model.getElementByElementId("ca2");
    assertEquals("ca1", c3.getCompartment().getElementId());
    assertEquals("ca2", c4.getCompartment().getElementId());

    assertEquals(1, c1.getElements().size());
    assertEquals(1, c2.getElements().size());
    assertEquals(0, c3.getElements().size());
    assertEquals(0, c4.getElements().size());
  }

  @Test
  public void testReactionWithStochiometry() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/stochiometry.xml"));
    final Reaction reaction = model.getReactionByReactionId("re1");

    assertEquals(2.0, reaction.getReactants().get(0).getStoichiometry(), Configuration.EPSILON);
    assertNull(reaction.getProducts().get(0).getStoichiometry());
  }

  @Test
  public void testModelWithSelfReactionToXml() throws Exception {
    final Model model = new ModelFullIndexed(null);
    model.setIdModel("as");
    model.setWidth(10);
    model.setHeight(10);
    final Species protein = new GenericProtein("id1");
    protein.setName("ROS");
    model.addElement(protein);

    final Reaction reaction = new StateTransitionReaction("re");
    reaction.setIdReaction("re1");
    final Product product = new Product(protein);
    product.setLine(new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(20, 20)));
    reaction.addProduct(product);
    final Reactant reactant = new Reactant(protein);
    reactant.setLine(new PolylineData(new Point2D.Double(20, 20), new Point2D.Double(0, 0)));
    reaction.addReactant(reactant);
    model.addReaction(reaction);

    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final String xmlString = parser.model2String(model);

    assertNotNull(xmlString);
    assertTrue(xmlString.contains("omitted"));
    assertTrue(xmlString.contains("re1"));
  }

  @Test
  public void testSpeciesWithSpecialSynonym() throws Exception {
    final Model model = new ModelFullIndexed(null);
    model.setIdModel("as");
    model.setWidth(10);
    model.setHeight(10);
    final Species protein = createProtein();
    protein.setName("ROS");
    protein.addSynonym("&");
    model.addElement(protein);

    final Model model2 = serializeModel(model);

    assertEquals(0, modelComparator.compare(model, model2));
  }

  @Test
  public void testReactionCoordsEqual() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/reaction_coords_different.xml"));

    final String xmlString = parser.model2String(model);
    final InputStream is = new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8));

    final Model model2 = parser.createModel(new ConverterParams().inputStream(is));

    final Reaction r1 = model.getReactions().iterator().next();
    final Reaction r2 = model2.getReactions().iterator().next();

    final List<Line2D> lines1 = r1.getLines();
    final List<Line2D> lines2 = r2.getLines();
    for (int i = 0; i < lines1.size(); i++) {
      final Line2D line1 = lines1.get(i);
      final Line2D line2 = lines2.get(i);
      assertEquals("Distance between points too big:" + line1.getP1() + ";" + line2.getP1(), 0,
          line1.getP1().distance(line2.getP1()), Configuration.EPSILON);
      assertEquals("Distance between points too big:" + line1.getP2() + ";" + line2.getP2(), 0,
          line1.getP2().distance(line2.getP2()), Configuration.EPSILON);
    }
  }

  @Test
  public void testParseReactionWithColors() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/reactions/coloring.xml"));

    final Reaction r1 = model.getReactionByReactionId("re1");
    final Set<Color> colors = new HashSet<>();
    for (final AbstractNode node : r1.getNodes()) {
      colors.add(node.getLine().getColor());
    }
    assertEquals("Four different colors were used to draw the reaction", 4, colors.size());
    assertFalse("Black wasn't used in reaction coloring", colors.contains(Color.BLACK));
  }

  @Test
  public void testExportReactionWithColors() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/reactions/coloring.xml"));

    model.setName("Unknown");
    assertNotNull(model);
    final String str = parser.model2String(model);
    final Model model2 = parser.createModel(
        new ConverterParams().inputStream(new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8))));
    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void testParseBooleanReactionWithColors() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/reactions/boolean-colors.xml"));

    final Reaction r1 = model.getReactionByReactionId("re1");
    final Set<Color> colors = new HashSet<>();
    for (final AbstractNode node : r1.getNodes()) {
      colors.add(node.getLine().getColor());
    }
    assertEquals("Three different colors were used to draw the reaction", 3, colors.size());
    assertFalse("Black wasn't used in reaction coloring", colors.contains(Color.BLACK));
  }

  @Test
  public void testExportBooleanReactioWithColors() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/reactions/boolean-colors.xml"));

    model.setName("Unknown");
    assertNotNull(model);
    final String str = parser.model2String(model);
    final Model model2 = parser.createModel(
        new ConverterParams().inputStream(new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8))));
    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void testParseDottedBooleanReaction() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/reactions/boolean-colors.xml"));

    final Reaction r1 = model.getReactionByReactionId("re1");
    final Set<LineType> reactionTypes = new HashSet<>();
    for (final AbstractNode node : r1.getNodes()) {
      reactionTypes.add(node.getLine().getType());
    }
    assertEquals("Whole reaction should use the same reaction type", 1, reactionTypes.size());
  }

  @Test
  public void testExportReactoinWithLinesNotAttachedToSpecies() throws Exception {
    final Model model = new ModelFullIndexed(null);
    model.setIdModel("as");
    model.setWidth(1000);
    model.setHeight(1000);

    final Species protein = createSimpleMolecule();
    protein.setX(383);
    protein.setY(584);
    protein.setWidth(140);
    protein.setHeight(60);
    model.addElement(protein);

    final Species protein2 = createSimpleMolecule();
    protein2.setX(351);
    protein2.setY(697);
    protein2.setWidth(100);
    protein2.setHeight(60);
    model.addElement(protein2);

    final Reaction reaction = new StateTransitionReaction("re1");
    reaction.setLine(new PolylineData(new Point2D.Double(401.0, 673.0), new Point2D.Double(401.0, 673.0)));

    final Reactant reactant = new Reactant(protein);
    reactant.setLine(new PolylineData(Arrays.asList(
        new Point2D.Double(420.0, 644.0),
        new Point2D.Double(420.0, 654.0),
        new Point2D.Double(401.0, 654.0),
        new Point2D.Double(401.0, 665.0))));
    reaction.addReactant(reactant);
    model.addReaction(reaction);

    final Product product = new Product(protein2);
    product.setLine(new PolylineData(new Point2D.Double(401.0, 673.0), new Point2D.Double(401.0, 697.0)));
    reaction.addProduct(product);

    final Model model2 = serializeModel(model);

    final Reaction reaction2 = model2.getReactionByReactionId("re1");

    final Reactant newReactant = reaction2.getReactants().get(0);

    // center part of the line shouldn't change - edges should be aligned to
    // touch
    // species
    final PointComparator pc = new PointComparator(Configuration.EPSILON);
    assertEquals(0,
        pc.compare(newReactant.getLine().getLines().get(1).getP1(), reactant.getLine().getLines().get(1).getP1()));
    assertEquals(0,
        pc.compare(newReactant.getLine().getLines().get(1).getP2(), reactant.getLine().getLines().get(1).getP2()));
  }

  @Test
  public void testTransparentComplex() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/problematic/transparent_complex.xml"));
    final Color c1 = model.getElementByElementId("csa1").getFillColor();
    final Color c2 = model.getElementByElementId("csa2").getFillColor();
    assertNotEquals(c1.getAlpha(), c2.getAlpha());
    assertEquals(c1.getRed(), c2.getRed());
    assertEquals(c1.getGreen(), c2.getGreen());
    assertEquals(c1.getBlue(), c2.getBlue());
  }

  @Test
  public void testTrigger() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/problematic/unknown-rections.xml"));
    for (final Reaction reaction : model.getReactions()) {
      if (reaction instanceof ReducedNotation || reaction instanceof ModifierReactionNotation) {
        assertFalse("Reaction cannot be reversible: " + reaction, reaction.isReversible());
      }
    }
  }

  @Test
  public void test() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/two_authors.xml"));
    assertEquals(2, model.getAuthors().size());
  }

  @Test
  public void testOrthogonalReactionToPhenotype() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/orthogonal_reaction_to_phenotype.xml"));
    final Reaction r = model.getReactionByReactionId("re2");
    assertEquals(r.getReactants().get(0).getLine().getStartPoint().getY(),
        r.getProducts().get(0).getLine().getStartPoint().getY(), Configuration.EPSILON);
  }

  @Test
  public void testOrthogonalReactionToDrug() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser.createModel(new ConverterParams().filename("testFiles/orthogonal_reaction_to_drug.xml"));
    final Reaction r = model.getReactionByReactionId("re3");
    assertEquals(r.getReactants().get(0).getLine().getStartPoint().getY(),
        r.getProducts().get(0).getLine().getStartPoint().getY(), Configuration.EPSILON);
  }

  @Test
  public void testAvogadroIgnoredOnExport() throws Exception {
    final Model model = new ModelFullIndexed(null);
    model.setIdModel("as");
    model.setWidth(10);
    model.setHeight(10);

    final Species protein = createProtein();
    model.addElement(protein);

    final Species protein2 = createProtein();
    model.addElement(protein2);

    final Reaction reaction = new StateTransitionReaction("re");
    reaction.setIdReaction("re1");
    final Product product = new Product(protein);
    product.setLine(new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(20, 20)));
    reaction.addProduct(product);
    final Reactant reactant = new Reactant(protein2);
    reactant.setLine(new PolylineData(new Point2D.Double(10, 20), new Point2D.Double(0, 0)));
    reaction.addReactant(reactant);

    reaction.setLine(new PolylineData(new Point2D.Double(10, 20), new Point2D.Double(20, 20)));

    final SbmlUnit unit = new SbmlUnit("unit");
    unit.addUnitTypeFactor(new SbmlUnitTypeFactor(SbmlUnitType.AVOGADRO, 1, 1, 1));
    model.addUnit(unit);

    model.addReaction(reaction);

    protein.setSubstanceUnits(unit);

    final SbmlKinetics kinetics = new SbmlKinetics();
    kinetics.addElement(protein);
    reaction.setKinetics(kinetics);
    kinetics.setDefinition(
        "<math><lambda>" + "<bvar><ci> " + protein.getElementId() + " </ci></bvar>" + "<bvar><ci> " + protein.getElementId() + " </ci></bvar>"
            + "<apply><plus/><ci> x </ci><ci> x </ci><cn type=\"integer\"> 2 </cn></apply>" + "</lambda></math>\n\n");
    final SbmlParameter parameter = new SbmlParameter("x");
    parameter.setUnits(unit);
    parameter.setValue(2.0);
    kinetics.addParameter(parameter);

    final Model model2 = serializeModel(model);

    assertNull(((Species) model2.getElementByElementId(protein.getElementId())).getSubstanceUnits());
    assertNull(((Species) model2.getElementByElementId(protein2.getElementId())).getSubstanceUnits());
  }

  @Test
  public void testNamespace() throws Exception {
    final CellDesignerXmlParser parser = new CellDesignerXmlParser();
    final Model model = parser
        .createModel(new ConverterParams().filename("testFiles/problematic/namespace.xml")
            .sizeAutoAdjust(false));
    assertEquals("", model.getNotes());
  }

}
