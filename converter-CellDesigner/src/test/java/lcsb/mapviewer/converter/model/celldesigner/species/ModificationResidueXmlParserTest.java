package lcsb.mapviewer.converter.model.celldesigner.species;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.geom.Point2D;

import org.apache.commons.lang3.math.NumberUtils;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.field.ModificationSite;

public class ModificationResidueXmlParserTest extends CellDesignerTestFunctions {

  private ModificationResidueXmlParser parser;

  private int counter = 0;

  @Before
  public void setUp() throws Exception {
    parser = new ModificationResidueXmlParser(new CellDesignerElementCollection());
  }

  @Test
  public void testGeneModificationResidueToXml() throws Exception {
    Gene gene = createGene();
    ModificationSite mr = createModificationSite(gene);
    String xmlString = parser.toXml(mr);
    assertNotNull(xmlString);
  }

  private ModificationSite createModificationSite(final Gene gene) {
    ModificationSite mr = new ModificationSite();
    mr.setIdModificationResidue("" + counter++);
    mr.setName("a");
    mr.setPosition(new Point2D.Double(gene.getX(), gene.getY()));
    gene.addModificationSite(mr);
    return mr;
  }

  @Test
  public void testResidueId() throws Exception {
    Gene gene = createGene();
    ModificationSite mr = createModificationSite(gene);
    ModificationSite mr2 = createModificationSite(gene);
    String id1 = parser.computeModificationResidueId(mr);
    assertFalse(NumberUtils.isParsable(id1));
    String id2 = parser.computeModificationResidueId(mr2);
    assertNotEquals(id1, id2);
  }

}
