package lcsb.mapviewer.converter.model.celldesigner;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

@RunWith(Parameterized.class)
public class CellDesignerSerializationTest extends CellDesignerTestFunctions {

  private final Model model;

  public CellDesignerSerializationTest(final String testName, final Model model) {
    this.model = model;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException, InvalidXmlSchemaException {
    Collection<Object[]> data = new ArrayList<>();

    data.add(createTestCompartmentWithSpecialCharacter());
    data.add(createTestLayerTextWithSpecialCharacter());
    data.add(createTestLayerNameWithSpecialCharacter());
    data.add(createTestUnitNameWithSpecialCharacter());
    data.add(createTestFunctionNameWithSpecialCharacter());
    data.add(createTestParameterNameWithSpecialCharacter());
    data.add(createTestStructuralStateNameWithSpecialCharacter());
    return data;
  }

  private static Object[] createTestCompartmentWithSpecialCharacter() {
    Model model = createEmptyModel();
    Compartment compartment = createCompartment();
    compartment.setName("A&B");
    model.addElement(compartment);
    return new Object[]{"compartment name with '&'", model};
  }

  private static Object[] createTestFunctionNameWithSpecialCharacter() throws InvalidXmlSchemaException {
    Model model = createEmptyModel();
    SbmlFunction fun = createFunction();

    fun.setName("A&B");
    model.addFunction(fun);
    return new Object[]{"function name with '&'", model};
  }

  private static Object[] createTestParameterNameWithSpecialCharacter() {
    Model model = createEmptyModel();
    SbmlParameter param = createParameter();

    param.setName("A&B");
    model.addParameter(param);
    return new Object[]{"parameter name with '&'", model};
  }

  private static Object[] createTestStructuralStateNameWithSpecialCharacter() {
    Model model = createEmptyModel();
    GenericProtein protein = createProtein();
    StructuralState state = createStructuralState(protein, "A&B");
    protein.addStructuralState(state);

    model.addElement(protein);
    return new Object[]{"structural state name with '&'", model};
  }

  private static Object[] createTestLayerTextWithSpecialCharacter() {
    Model model = createEmptyModel();
    LayerText text = createText();
    text.setNotes("A&B");
    Layer layer = createLayer();
    model.addLayer(layer);
    return new Object[]{"layer text with '&'", model};
  }

  private static Object[] createTestLayerNameWithSpecialCharacter() {
    Model model = createEmptyModel();
    Layer layer = createLayer();
    layer.setName("A&B");
    model.addLayer(layer);
    return new Object[]{"layer with '&'", model};
  }

  private static Object[] createTestUnitNameWithSpecialCharacter() {
    Model model = createEmptyModel();
    SbmlUnit unit = createUnit();
    unit.setName("A&B");

    model.addUnit(unit);
    return new Object[]{"unit name with '&'", model};
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() throws Exception {

    testXmlSerialization(model);
  }
}
