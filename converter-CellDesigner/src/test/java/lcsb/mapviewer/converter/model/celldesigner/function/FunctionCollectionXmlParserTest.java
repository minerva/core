package lcsb.mapviewer.converter.model.celldesigner.function;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import org.junit.Test;

import lcsb.mapviewer.common.comparator.SetComparator;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlFunctionComparator;

public class FunctionCollectionXmlParserTest extends CellDesignerTestFunctions {

  @Test
  public void testToXml() throws Exception {
    FunctionCollectionXmlParser parser = new FunctionCollectionXmlParser();
    Set<SbmlFunction> functions = parser
        .parseXmlFunctionCollection(
            super.getXmlDocumentFromFile("testFiles/function/function_list.xml").getFirstChild());
    String xml = parser.toXml(functions);
    Set<SbmlFunction> functions2 = parser.parseXmlFunctionCollection(super.getNodeFromXmlString(xml));

    SetComparator<SbmlFunction> comparator = new SetComparator<>(new SbmlFunctionComparator());
    assertEquals(0, comparator.compare(functions, functions2));
  }

}
