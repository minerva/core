package lcsb.mapviewer.converter.model.celldesigner;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Dimension2D;

import org.junit.Test;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.XmlParser;

public class CommonXmlParserTest {


  @Test
  public void testParseDimension() throws Exception {
    Node node = XmlParser.getXmlDocumentFromString("<celldesigner:boxSize width=\"51.25\" height=\"20.0\"/>")
        .getFirstChild();
    CommonXmlParser parser = new CommonXmlParser();
    Dimension2D d = parser.getDimension(node);
    assertEquals(51.25, d.getWidth(), lcsb.mapviewer.common.Configuration.EPSILON);
    assertEquals(20, d.getHeight(), lcsb.mapviewer.common.Configuration.EPSILON);
  }

  @Test
  public void testGetNotesXmlContent() throws Exception {
    CommonXmlParser parser = new CommonXmlParser();

    String notes = "<p>bla</p>";
    assertEquals(notes, parser.getNotesXmlContent(notes));
  }

  @Test
  public void testGetNotesXmlContentWithXmlInvalidTags() throws Exception {
    CommonXmlParser parser = new CommonXmlParser();

    String notes = "bla < bla2";
    assertEquals("bla &lt; bla2", parser.getNotesXmlContent(notes));
  }

  @Test
  public void testGetNotesXmlContentWithoutTags() throws Exception {
    CommonXmlParser parser = new CommonXmlParser();

    String notes = "bla > bla2";
    assertEquals(notes, parser.getNotesXmlContent(notes));
  }
}
