package lcsb.mapviewer.converter.model.celldesigner.types;

import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.reaction.NodeOperator;

public class OperatorTypeUtilsTest extends CellDesignerTestFunctions {

  private OperatorTypeUtils utils = new OperatorTypeUtils();

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetUnknownOperatorTypeForClazz() {
    OperatorType result = utils
        .getOperatorTypeForClazz(Mockito.mock(NodeOperator.class, Mockito.CALLS_REAL_METHODS).getClass());

    assertNull(result);
  }

  @Test
  public void testGetUnkownOperatorTypeForStringType() {
    OperatorType result = utils.getOperatorTypeForStringType("unknown");

    assertNull(result);
  }

  @Test
  public void testGetUnknownStringTypeByOperator() {

    String result = utils.getStringTypeByOperator(Mockito.mock(NodeOperator.class, Mockito.CALLS_REAL_METHODS));

    assertNull(result);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testCreateUnknownModifierForStringType() {
    utils.createModifierForStringType("blabla");
  }

}
