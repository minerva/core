package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

public class ElementTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor() {
    CellDesignerSpecies<?> element = Mockito.spy(CellDesignerSpecies.class);
    element.addMiriamData(new MiriamData());
    CellDesignerElement<?> copy = new CellDesignerGenericProtein(element);
    assertNotNull(copy);
  }

  @Test
  public void testAddMiriamCollection() {
    List<MiriamData> list = new ArrayList<>();
    list.add(new MiriamData(MiriamType.CAS, "1"));
    list.add(new MiriamData(MiriamType.CAS, "1"));
    CellDesignerElement<?> element = Mockito.spy(CellDesignerElement.class);
    element.addMiriamData(list);
    assertEquals(1, element.getMiriamData().size());
    assertEquals(1, getWarnings().size());

    element.addMiriamData(list);
    assertEquals(3, getWarnings().size());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetNotes() {
    CellDesignerElement<?> element = Mockito.spy(CellDesignerElement.class);
    element.setNotes("</html>");
  }

  @Test
  public void testAddSynonym() {
    CellDesignerElement<?> element = Mockito.spy(CellDesignerElement.class);
    List<String> synonyms = new ArrayList<>();
    synonyms.add("syn");
    element.addSynonyms(synonyms);
    assertEquals("syn", element.getSynonyms().get(0));
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < 300; i++) {
      sb.append("a");
    }
    String syn = sb.toString();
    synonyms = new ArrayList<>();
    synonyms.add(syn);
    element.addSynonyms(synonyms);
    assertFalse(syn.equals(element.getSynonyms().get(1)));
  }

  @Test
  public void testGetters() {
    CellDesignerElement<?> element = Mockito.spy(CellDesignerElement.class);
    List<String> synonyms = new ArrayList<>();
    List<String> formerSymbols = new ArrayList<>();
    CellDesignerComplexSpecies complex = new CellDesignerComplexSpecies();
    String formula = "str";
    String abbreviation = "abr";

    element.setSynonyms(synonyms);
    element.setFormerSymbols(formerSymbols);
    element.setComplex(complex);
    element.setFormula(formula);
    element.setAbbreviation(abbreviation);

    assertEquals(synonyms, element.getSynonyms());
    assertEquals(formerSymbols, element.getFormerSymbols());
    assertEquals(complex, element.getComplex());
    assertEquals(formula, element.getFormula());
    assertEquals(abbreviation, element.getAbbreviation());
  }

}
