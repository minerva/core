package lcsb.mapviewer.converter.model.celldesigner;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CellDesignerParserUtilsTest {

  @Test
  public void test() {
    assertTrue(Double.isInfinite(new CellDesignerParserUtils().parseDouble("INF")));
  }

}
