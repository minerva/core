package lcsb.mapviewer.converter.model.celldesigner.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerRna;

public class RnaXmlParserTest extends CellDesignerTestFunctions {
  protected Logger logger = LogManager.getLogger();

  private RnaXmlParser rnaParser;
  private String testRnaFile = "testFiles/xmlNodeTestExamples/rna.xml";
  private String testRnaFile2 = "testFiles/xmlNodeTestExamples/rna_with_region.xml";

  private CellDesignerElementCollection elements;

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();
    rnaParser = new RnaXmlParser(elements);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseXmlSpecies() throws Exception {
    String xmlString = readFile(testRnaFile);
    Pair<String, CellDesignerRna> result = rnaParser.parseXmlElement(xmlString);
    CellDesignerRna rna = result.getRight();
    assertEquals("rn36", result.getLeft());
    assertEquals("BCL6", rna.getName());
    assertTrue(rna.getNotes().contains("B-cell CLL/lymphoma "));
  }

  @Test
  public void testParseXmlSpeciesWithRegion() throws Exception {
    String xmlString = readFile(testRnaFile2);
    Pair<String, CellDesignerRna> result = rnaParser.parseXmlElement(xmlString);
    CellDesignerRna rna = result.getRight();
    assertNotNull(rna);
    assertNotNull(rna.getRegions());
    assertEquals(1, rna.getRegions().size());
    assertEquals("tr1", rna.getRegions().get(0).getIdModificationResidue());
  }

  @Test
  public void testToXml() throws Exception {
    String xmlString = readFile(testRnaFile);
    Pair<String, CellDesignerRna> result = rnaParser.parseXmlElement(xmlString);
    CellDesignerRna rna = result.getRight();

    String transformedXml = rnaParser.toXml(rna.createModelElement("id"));
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    InputSource is = new InputSource(new StringReader(transformedXml));
    Document doc = builder.parse(is);
    NodeList root = doc.getChildNodes();
    assertEquals("celldesigner:RNA", root.item(0).getNodeName());

    Pair<String, CellDesignerRna> result2 = rnaParser.parseXmlElement(rnaParser.toXml(rna.createModelElement("id")));
    CellDesignerRna rna2 = result2.getRight();
    assertEquals(rna.getName(), rna2.getName());
  }

  @Test
  public void testToXmlShouldNotContainNotes() throws Exception {
    String xmlString = readFile(testRnaFile);
    Pair<String, CellDesignerRna> result = rnaParser.parseXmlElement(xmlString);
    CellDesignerRna rna = result.getRight();

    String transformedXml = rnaParser.toXml(rna.createModelElement("id"));

    assertFalse(transformedXml.contains("celldesigner:notes"));
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlSpecies() throws Exception {
    String xmlString = readFile("testFiles/invalid/rna.xml");
    rnaParser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlSpecies2() throws Exception {
    String xmlString = readFile("testFiles/invalid/rna2.xml");
    rnaParser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalidXmlSpecies3() throws Exception {
    String xmlString = readFile("testFiles/invalid/rna3.xml");
    rnaParser.parseXmlElement(xmlString);
  }

}
