package lcsb.mapviewer.converter.model.celldesigner.geometry;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.ReceptorProtein;

public class ProteinConverterTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetAnchorForAlias() throws Exception {
    GenericProtein alias = new GenericProtein("id");
    alias.setWidth(200);
    alias.setHeight(300);
    alias.setX(100.0);
    alias.setY(50.0);

    CellDesignerAliasConverter converter = new CellDesignerAliasConverter(alias, false);

    for (final CellDesignerAnchor anchor : CellDesignerAnchor.values()) {
      Point2D point = converter.getPointCoordinates(alias, anchor);
      CellDesignerAnchor newAnchor = converter.getAnchorForCoordinates(alias, point);
      assertEquals(anchor, newAnchor);
    }
  }

  @Test
  public void testAngleConversion() {
    Protein protein = new GenericProtein("id");
    protein.setWidth(10);
    protein.setHeight(60);
    protein.setX(200);
    protein.setY(500);

    CellDesignerAliasConverter converter = new CellDesignerAliasConverter(protein, false);

    for (double angle = 0.0; angle < Math.PI * 2; angle += 0.1) {
      Point2D point = converter.getResidueCoordinates(protein, angle);
      double angle2 = converter.getAngleForPoint(protein, point);
      assertEquals(angle, angle2, Configuration.EPSILON);
    }
  }

  @Test
  public void testAngleConversionForReceptor() {
    Protein protein = new ReceptorProtein("id");
    protein.setWidth(10);
    protein.setHeight(60);
    protein.setX(200);
    protein.setY(500);

    CellDesignerAliasConverter converter = new CellDesignerAliasConverter(protein, false);

    for (double angle = 0.0; angle < Math.PI * 2; angle += 0.1) {
      Point2D point = converter.getResidueCoordinates(protein, angle);
      double angle2 = converter.getAngleForPoint(protein, point);
      assertEquals(angle, angle2, Configuration.EPSILON);
    }
  }

  @Test
  public void testGetResidueCoords() throws Exception {
    GenericProtein protein = new GenericProtein("id");
    protein.setX(135);
    protein.setY(194.0);
    protein.setWidth(130);
    protein.setHeight(67);

    CellDesignerAliasConverter conv = new CellDesignerAliasConverter(protein, false);
    assertEquals(135.0, conv.getResidueCoordinates(protein, 3.141592653589793).getX(), 2);
    assertEquals(265., conv.getResidueCoordinates(protein, 0.0).getX(), 2);
    assertEquals(135.0, conv.getResidueCoordinates(protein, 2.41).getX(), 2);
    assertEquals(194.0, conv.getResidueCoordinates(protein, 1.98).getY(), 2);
    assertEquals(194.0, conv.getResidueCoordinates(protein, 1.59).getY(), 2);
    assertEquals(265.0, conv.getResidueCoordinates(protein, 6.28).getX(), 2);
    assertEquals(261.0, conv.getResidueCoordinates(protein, 4.13).getY(), 2);
    assertEquals(261.0, conv.getResidueCoordinates(protein, 4.86).getY(), 2);
    assertEquals(265.0, conv.getResidueCoordinates(protein, 5.69).getX(), 2);
    assertEquals(194.0, conv.getResidueCoordinates(protein, 0.99).getY(), 2);
  }

}
