package lcsb.mapviewer.converter.model.celldesigner.species;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerAntisenseRna;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerComplexSpecies;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerDegraded;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerDrug;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGene;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGenericProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerIon;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerPhenotype;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerRna;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerSimpleMolecule;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerSpecies;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerUnknown;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.SpeciesState;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitTypeFactor;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.util.Collections;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class SpeciesSbmlParserTest extends CellDesignerTestFunctions {
  private SpeciesSbmlParser parser;
  private ProteinXmlParser proteinXmlParser;
  private final String testGeneFile = "testFiles/xmlNodeTestExamples/sbml_gene.xml";
  private final String testDegradedFile = "testFiles/xmlNodeTestExamples/sbml_degraded.xml";
  private final String testDrugFile = "testFiles/xmlNodeTestExamples/sbml_drug.xml";
  private final String testIonFile = "testFiles/xmlNodeTestExamples/sbml_ion.xml";
  private final String testPhenotypeFile = "testFiles/xmlNodeTestExamples/sbml_phenotype.xml";
  private final String testProteinFile = "testFiles/xmlNodeTestExamples/sbml_protein.xml";
  private final String testRnaFile = "testFiles/xmlNodeTestExamples/sbml_rna.xml";
  private final String testSimpleMoleculeFile = "testFiles/xmlNodeTestExamples/sbml_simple_molecule.xml";
  private final String testUnknownFile = "testFiles/xmlNodeTestExamples/sbml_unknown.xml";
  private CellDesignerElementCollection elements;
  private int idCounter = 0;

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();
    proteinXmlParser = new ProteinXmlParser(elements);
    parser = new SpeciesSbmlParser(elements, new HashSet<>(), proteinXmlParser);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseXmlSpeciesAntisenseRna() throws Exception {
    final String xmlString = readFile("testFiles/xmlNodeTestExamples/sbml_antisense_rna.xml");
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerAntisenseRna species = (CellDesignerAntisenseRna) result.getRight();
    assertEquals("s2", species.getElementId());
    assertEquals("s3", species.getName());
    assertEquals(2, species.getInitialAmount(), Configuration.EPSILON);
    assertEquals(Boolean.TRUE, species.hasOnlySubstanceUnits());
    assertEquals(Integer.valueOf(0), species.getCharge());
  }

  @Test
  public void testParseXmlSpeciesWithKineticsData() throws Exception {
    final String xmlString = readFile("testFiles/kinetics/species_with_kinetics_param.xml");
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerProtein<?> species = (CellDesignerProtein<?>) result.getRight();
    assertEquals(3.7, species.getInitialConcentration(), Configuration.EPSILON);
    assertEquals(SbmlUnitType.MOLE.getCommonName(), species.getSubstanceUnits().getName());
  }

  @Test
  public void testSpeciesUnitsToXml() throws Exception {
    final GenericProtein protein = new GenericProtein("s1");
    final SbmlUnit unit = new SbmlUnit("mole_id");
    unit.addUnitTypeFactor(new SbmlUnitTypeFactor(SbmlUnitType.MOLE, 1, 1, 1));
    protein.setSubstanceUnits(unit);
    proteinXmlParser.initializeProteinData(Collections.singletonList(protein));
    final String xml = parser.toXml(protein);
    assertTrue("Cannot find substance unit in xml", xml.indexOf("mole_id") >= 0);
  }

  @Test
  public void testSpeciesConstantToXml() throws Exception {
    final GenericProtein protein = new GenericProtein("s1");
    protein.setConstant(true);
    proteinXmlParser.initializeProteinData(Collections.singletonList(protein));
    final String xml = parser.toXml(protein);
    assertTrue("Cannot find constant in xml", xml.indexOf("constant") >= 0);
  }

  @Test
  public void testSpeciesBoundaryConditionToXml() throws Exception {
    final GenericProtein protein = new GenericProtein("s1");
    protein.setBoundaryCondition(true);
    proteinXmlParser.initializeProteinData(Collections.singletonList(protein));
    final String xml = parser.toXml(protein);
    assertTrue("Cannot find boundary condition in xml", xml.indexOf("boundaryCondition") >= 0);
  }

  @Test
  public void testParseXmlSpeciesWithKineticsData2() throws Exception {
    final String xmlString = readFile("testFiles/kinetics/species_with_kinetics_param2.xml");
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerProtein<?> species = (CellDesignerProtein<?>) result.getRight();
    assertEquals(2.5, species.getInitialAmount(), Configuration.EPSILON);
    assertEquals(SbmlUnitType.GRAM.getCommonName(), species.getSubstanceUnits().getName());
    assertEquals(Boolean.TRUE, species.hasOnlySubstanceUnits());
    assertEquals("Boundary condition wasn't parsed", Boolean.TRUE, species.isBoundaryCondition());
    assertEquals("Constant property wasn't parsed", Boolean.TRUE, species.isConstant());
    assertEquals(Integer.valueOf(0), species.getCharge());
  }

  @Test
  public void testToXmlAntisenseRna() throws Exception {
    final String xmlString = readFile("testFiles/xmlNodeTestExamples/sbml_antisense_rna.xml");
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerAntisenseRna species = (CellDesignerAntisenseRna) result.getRight();

    final String transformedXml = parser.toXml(species.createModelElement());
    assertNotNull(transformedXml);
    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    final DocumentBuilder builder = factory.newDocumentBuilder();
    final InputSource is = new InputSource(new StringReader(transformedXml));
    final Document doc = builder.parse(is);
    final NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    final Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(species.createModelElement()));
    final CellDesignerAntisenseRna species2 = (CellDesignerAntisenseRna) result2.getRight();

    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
    assertEquals(species.getPositionToCompartment(), species2.getPositionToCompartment());
  }

  @Test
  public void testParseXmlSpeciesComplex() throws Exception {
    final String xmlString = readFile("testFiles/xmlNodeTestExamples/sbml_complex.xml");
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerComplexSpecies species = (CellDesignerComplexSpecies) result.getRight();
    assertNotNull(species);
    assertEquals("s6549", species.getElementId());
    assertEquals("LC3-II", species.getName());
    assertEquals(0.0, species.getInitialAmount(), Configuration.EPSILON);
  }

  @Test
  public void testToXmlComplex() throws Exception {
    final String xmlString = readFile("testFiles/xmlNodeTestExamples/sbml_complex.xml");
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerComplexSpecies species = (CellDesignerComplexSpecies) result.getRight();

    final String transformedXml = parser.toXml(species.createModelElement());
    assertNotNull(transformedXml);
    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    final DocumentBuilder builder = factory.newDocumentBuilder();
    final InputSource is = new InputSource(new StringReader(transformedXml));
    final Document doc = builder.parse(is);
    final NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    final Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(species.createModelElement()));
    final CellDesignerComplexSpecies species2 = (CellDesignerComplexSpecies) result2.getRight();

    assertNotNull(species2);
    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
  }

  @Test
  public void testParseXmlSpeciesDegraded() throws Exception {
    final String xmlString = readFile(testDegradedFile);
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerDegraded species = (CellDesignerDegraded) result.getRight();
    assertNotNull(species);
    assertEquals("s1275", species.getElementId());
    assertEquals("s1275", species.getName());
    assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
  }

  @Test
  public void testToXmlDegraded() throws Exception {
    final String xmlString = readFile(testDegradedFile);
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerDegraded species = (CellDesignerDegraded) result.getRight();

    final String transformedXml = parser.toXml(species.createModelElement());
    assertNotNull(transformedXml);
    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    final DocumentBuilder builder = factory.newDocumentBuilder();
    final InputSource is = new InputSource(new StringReader(transformedXml));
    final Document doc = builder.parse(is);
    final NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    final Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(species.createModelElement()));
    final CellDesignerDegraded species2 = (CellDesignerDegraded) result2.getRight();

    assertNotNull(species2);
    assertEquals(" ", species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
  }

  @Test
  public void testParseXmlSpeciesDrug() throws Exception {
    final String xmlString = readFile(testDrugFile);
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerDrug species = (CellDesignerDrug) result.getRight();
    assertEquals("s6104", species.getElementId());
    assertEquals("geldanamycin", species.getName());
    assertEquals(Integer.valueOf(0), species.getCharge());
  }

  @Test
  public void testToXmlDrug() throws Exception {
    final String xmlString = readFile(testDrugFile);
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerDrug species = (CellDesignerDrug) result.getRight();

    final String transformedXml = parser.toXml(species.createModelElement());
    assertNotNull(transformedXml);
    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    final DocumentBuilder builder = factory.newDocumentBuilder();
    final InputSource is = new InputSource(new StringReader(transformedXml));
    final Document doc = builder.parse(is);
    final NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    final Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(species.createModelElement()));
    final CellDesignerDrug species2 = (CellDesignerDrug) result2.getRight();

    assertNotNull(species2);
    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
  }

  @Test
  public void testParseXmlSpeciesGene() throws Exception {
    final String xmlString = readFile(testGeneFile);
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerGene species = (CellDesignerGene) result.getRight();

    assertEquals("s5916", species.getElementId());
    assertEquals("Ptgr1", species.getName());
    assertEquals(Integer.valueOf(0), species.getCharge());
    assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
  }

  @Test
  public void testParseXmlSpeciesGeneWithModelUpdate() throws Exception {
    final CellDesignerSpecies<?> gene = new CellDesignerGene();
    gene.setElementId("s5916");
    final InternalModelSpeciesData modelData = new InternalModelSpeciesData();
    modelData.updateSpecies(gene, "");

    final SpeciesSbmlParser complexParser = new SpeciesSbmlParser(elements, new HashSet<>(), proteinXmlParser);
    final CellDesignerGene oldGene = new CellDesignerGene();
    oldGene.setElementId("s5916");
    oldGene.setName("Ptgr1");
    modelData.updateSpecies(oldGene, "gn95");

    final String xmlString = readFile(testGeneFile);
    final Pair<String, ? extends CellDesignerSpecies<?>> result = complexParser.parseXmlElement(xmlString);
    final CellDesignerGene species = (CellDesignerGene) result.getRight();
    modelData.updateSpecies(species, result.getLeft());

    assertEquals("s5916", species.getElementId());
    assertEquals("Ptgr1", species.getName());
    assertTrue(species.getNotes().contains("prostaglandin reductase 1"));
  }

  @Test
  public void testToXmlGene() throws Exception {
    final String xmlString = readFile(testGeneFile);
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerGene species = (CellDesignerGene) result.getRight();

    final String transformedXml = parser.toXml(species.createModelElement());
    assertNotNull(transformedXml);
    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    final DocumentBuilder builder = factory.newDocumentBuilder();
    final InputSource is = new InputSource(new StringReader(transformedXml));
    final Document doc = builder.parse(is);
    final NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    final Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(species.createModelElement()));
    final CellDesignerGene species2 = (CellDesignerGene) result2.getRight();

    assertNotNull(species2);
    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
    assertTrue(species2.getNotes().replaceAll("\\s", "").contains(species.getNotes().replaceAll("\\s", "")));
    assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());
  }

  @Test
  public void testParseXmlSpeciesIon() throws Exception {
    final String xmlString = readFile(testIonFile);
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerIon species = (CellDesignerIon) result.getRight();

    assertEquals("s6029", species.getElementId());
    assertEquals("Pi", species.getName());
    assertEquals(Integer.valueOf(0), species.getCharge());
    assertEquals(0, species.getInitialConcentration(), Configuration.EPSILON);
  }

  @Test
  public void testToXmlIon() throws Exception {
    final String xmlString = readFile(testIonFile);
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerIon species = (CellDesignerIon) result.getRight();

    final String transformedXml = parser.toXml(species.createModelElement());
    assertNotNull(transformedXml);
    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    final DocumentBuilder builder = factory.newDocumentBuilder();
    final InputSource is = new InputSource(new StringReader(transformedXml));
    final Document doc = builder.parse(is);
    final NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    final Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser.parseXmlElement(xmlString);
    final CellDesignerIon species2 = (CellDesignerIon) result2.getRight();

    assertNotNull(species2);
    assertEquals(species.getElementId(), species2.getElementId());
    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
    assertEquals(species.getInitialConcentration(), species2.getInitialConcentration());
    assertEquals(species.getNotes().trim(), species2.getNotes().trim());
    assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());
  }

  @Test
  public void testParseXmlSpeciesPhenotype() throws Exception {
    final String xmlString = readFile(testPhenotypeFile);
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerPhenotype species = (CellDesignerPhenotype) result.getRight();

    assertEquals("s5462", species.getElementId());
    assertEquals("Neuronal damage and death", species.getName());
    assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
  }

  @Test
  public void testToXmlPhenotype() throws Exception {
    final String xmlString = readFile(testPhenotypeFile);
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerPhenotype species = (CellDesignerPhenotype) result.getRight();

    final String transformedXml = parser.toXml(species.createModelElement());
    assertNotNull(transformedXml);
    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    final DocumentBuilder builder = factory.newDocumentBuilder();
    final InputSource is = new InputSource(new StringReader(transformedXml));
    final Document doc = builder.parse(is);
    final NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    final Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(species.createModelElement()));
    final CellDesignerPhenotype species2 = (CellDesignerPhenotype) result2.getRight();

    assertNotNull(species2);
    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
    assertEquals(species.getInitialConcentration(), species2.getInitialConcentration());
    assertEquals(species.getNotes(), species2.getNotes());
    assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());
  }

  @Test
  public void testParseXmlSpeciesProtein() throws Exception {
    final String xmlString = readFile(testProteinFile);
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerProtein<?> species = (CellDesignerProtein<?>) result.getRight();

    assertEquals("s5456", species.getElementId());
    assertEquals("PTPRC", species.getName());
    assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
    assertEquals(Integer.valueOf(0), species.getCharge());
    assertTrue(species.getNotes().contains("protein tyrosine phosphatase, receptor type, C"));
    assertNull(species.getStructuralState());
  }

  @Test
  public void testToXmlProtein() throws Exception {
    final String xmlString = readFile(testProteinFile);
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerGenericProtein species = new CellDesignerGenericProtein(result.getRight());

    final GenericProtein protein1 = species.createModelElement("" + idCounter++);
    proteinXmlParser.initializeProteinData(Collections.singletonList(protein1));
    final String transformedXml = parser.toXml(protein1);
    assertNotNull(transformedXml);
    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    final DocumentBuilder builder = factory.newDocumentBuilder();
    final InputSource is = new InputSource(new StringReader(transformedXml));
    final Document doc = builder.parse(is);
    final NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    final GenericProtein protein2 = species.createModelElement("" + idCounter++);
    final Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(protein2));
    final CellDesignerProtein<?> species2 = (CellDesignerProtein<?>) result2.getRight();

    assertNotNull(species2);
    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
    assertEquals(species.getInitialConcentration(), species2.getInitialConcentration());
    assertTrue(species2.getNotes().replaceAll("\\s", "").contains(species.getNotes().replaceAll("\\s", "")));
    assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());
  }

  @Test
  public void testParseXmlSpeciesRna() throws Exception {
    final String xmlString = readFile(testRnaFile);
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerRna species = (CellDesignerRna) result.getRight();

    assertEquals("s5914", species.getElementId());
    assertEquals("Fmo3", species.getName());
    assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
    assertEquals(Integer.valueOf(0), species.getCharge());
    assertTrue(species.getNotes().contains("Dimethylaniline monooxygenase [N-oxide-forming] 3"));
  }

  @Test
  public void testToXmlRna() throws Exception {
    final String xmlString = readFile(testRnaFile);
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerRna species = (CellDesignerRna) result.getRight();

    final String transformedXml = parser.toXml(species.createModelElement());
    assertNotNull(transformedXml);
    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    final DocumentBuilder builder = factory.newDocumentBuilder();
    final InputSource is = new InputSource(new StringReader(transformedXml));
    final Document doc = builder.parse(is);
    final NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    final Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(species.createModelElement()));
    final CellDesignerRna species2 = (CellDesignerRna) result2.getRight();

    assertNotNull(species2);
    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
    assertEquals(species.getInitialConcentration(), species2.getInitialConcentration());
    assertTrue(species2.getNotes().replaceAll("\\s", "").contains(species.getNotes().replaceAll("\\s", "")));
    assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());
  }

  @Test
  public void testParseXmlSpeciesSimpleMolecule() throws Exception {
    final String xmlString = readFile(testSimpleMoleculeFile);
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerSimpleMolecule species = (CellDesignerSimpleMolecule) result.getRight();
    assertEquals("s5463", species.getElementId());
    assertEquals("Peroxides", species.getName());
    assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
  }

  @Test
  public void testToXmlSimpleMolecule() throws Exception {
    final String xmlString = readFile(testSimpleMoleculeFile);
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerSimpleMolecule species = (CellDesignerSimpleMolecule) result.getRight();

    final String transformedXml = parser.toXml(species.createModelElement("" + idCounter++));
    assertNotNull(transformedXml);
    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    final DocumentBuilder builder = factory.newDocumentBuilder();
    final InputSource is = new InputSource(new StringReader(transformedXml));
    final Document doc = builder.parse(is);
    final NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    final Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(species.createModelElement("" + idCounter++)));
    final CellDesignerSimpleMolecule species2 = (CellDesignerSimpleMolecule) result2.getRight();

    assertNotNull(species2);
    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
    assertEquals(species.getInitialConcentration(), species2.getInitialConcentration());
    assertEquals(species.getNotes(), species2.getNotes());
    assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());
  }

  @Test
  public void testParseXmlSpeciesUnknown() throws Exception {
    final String xmlString = readFile(testUnknownFile);
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerUnknown species = (CellDesignerUnknown) result.getRight();
    assertEquals("s1356", species.getElementId());
    assertEquals("unidentified caspase acting on Occludin", species.getName());
    assertEquals(0, species.getInitialAmount(), Configuration.EPSILON);
    assertEquals(Integer.valueOf(0), species.getCharge());
  }

  @Test
  public void testToXmlUnknown() throws Exception {
    final String xmlString = readFile(testUnknownFile);
    final Pair<String, ? extends CellDesignerSpecies<?>> result = parser.parseXmlElement(xmlString);
    final CellDesignerUnknown species = (CellDesignerUnknown) result.getRight();

    final String transformedXml = parser.toXml(species.createModelElement());
    assertNotNull(transformedXml);
    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    final DocumentBuilder builder = factory.newDocumentBuilder();
    final InputSource is = new InputSource(new StringReader(transformedXml));
    final Document doc = builder.parse(is);
    final NodeList root = doc.getChildNodes();
    assertEquals("species", root.item(0).getNodeName());

    final Pair<String, ? extends CellDesignerSpecies<?>> result2 = parser
        .parseXmlElement(parser.toXml(species.createModelElement()));
    final CellDesignerUnknown species2 = (CellDesignerUnknown) result2.getRight();

    assertNotNull(species2);
    assertEquals(species.getName(), species2.getName());
    assertEquals(species.getParent(), species2.getParent());
    assertEquals(species.getInitialAmount(), species2.getInitialAmount());
    assertEquals(species.hasOnlySubstanceUnits(), species2.hasOnlySubstanceUnits());
    assertEquals(species.getCharge(), species2.getCharge());
    assertEquals(species.getInitialConcentration(), species2.getInitialConcentration());
    assertEquals(species.getNotes(), species2.getNotes());
    assertEquals(species.getMiriamData().size(), species2.getMiriamData().size());
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid() throws Exception {
    final String xmlString = readFile("testFiles/invalid/invalid_sbml_protein.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid2() throws Exception {
    final String xmlString = readFile("testFiles/invalid/invalid_sbml_protein2.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid3() throws Exception {
    final String xmlString = readFile("testFiles/invalid/invalid_sbml_protein3.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid4() throws Exception {
    final String xmlString = readFile("testFiles/invalid/invalid_sbml_protein4.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid5() throws Exception {
    final String xmlString = readFile("testFiles/invalid/invalid_sbml_protein5.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid6() throws Exception {
    final String xmlString = readFile("testFiles/invalid/invalid_sbml_protein6.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid7() throws Exception {
    final String xmlString = readFile("testFiles/invalid/invalid_sbml_protein7.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid8() throws Exception {
    final String xmlString = readFile("testFiles/invalid/invalid_sbml_protein8.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid9() throws Exception {
    final String xmlString = readFile("testFiles/invalid/invalid_sbml_protein9.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid10() throws Exception {
    final String xmlString = readFile("testFiles/invalid/invalid_sbml_protein10.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid11() throws Exception {
    final String xmlString = readFile("testFiles/invalid/invalid_sbml_protein11.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid12() throws Exception {
    final String xmlString = readFile("testFiles/invalid/invalid_sbml_protein12.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid13() throws Exception {
    final String xmlString = readFile("testFiles/invalid/invalid_sbml_protein13.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test(expected = InvalidXmlSchemaException.class)
  public void testParseInvalid14() throws Exception {
    final String xmlString = readFile("testFiles/invalid/invalid_sbml_protein14.xml");
    parser.parseXmlElement(xmlString);
  }

  @Test
  public void testToXmlWithDefaultCompartment() throws Exception {
    final CellDesignerGenericProtein species = new CellDesignerGenericProtein();
    final GenericProtein protein = species.createModelElement("EL_ID");
    proteinXmlParser.initializeProteinData(Collections.singletonList(protein));
    final String xml = parser.toXml(protein);
    assertTrue(xml.contains("EL_ID"));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidSpeciesIdentityToXml() throws Exception {
    final Species species = Mockito.mock(Species.class);
    parser.speciesIdentityToXml(species);
  }

  @Test
  public void testSpeciesIdentityToXml() throws Exception {
    final GenericProtein protein = new GenericProtein("xx");
    protein.setHypothetical(true);
    proteinXmlParser.initializeProteinData(Collections.singletonList(protein));
    final String xml = parser.speciesIdentityToXml(protein);
    assertTrue(xml.contains("<celldesigner:hypothetical>true</celldesigner:hypothetical>"));
  }

  @Test
  public void testSpeciesStateToXml() throws Exception {
    final SpeciesState state = new SpeciesState();
    state.setHomodimer(2);
    state.setStructuralState("xxxState");
    final String xml = parser.speciesStateToXml(state);
    assertTrue(xml.contains("xxxState"));
    assertTrue(xml.contains("celldesigner:homodimer"));
  }

  @Test(expected = NotImplementedException.class)
  public void testProcessInvalidStateDataInSpecies() throws Exception {
    final SpeciesState state = new SpeciesState();
    state.addModificationResidue(new CellDesignerModificationResidue());
    final CellDesignerComplexSpecies species = new CellDesignerComplexSpecies();

    parser.processStateDataInSpecies(species, state);
  }

  @Test(expected = NotImplementedException.class)
  public void testProcessInvalidStateDataInSpecies2() throws Exception {
    final SpeciesState state = new SpeciesState();
    state.setStructuralState("state");
    final CellDesignerGene species = new CellDesignerGene();

    parser.processStateDataInSpecies(species, state);
  }

  @Test(expected = NotImplementedException.class)
  public void testProcessInvalidStateDataInSpecies3() throws Exception {
    final SpeciesState state = new SpeciesState();
    state.setStructuralState("state");
    final CellDesignerRna species = new CellDesignerRna();

    parser.processStateDataInSpecies(species, state);
  }

  @Test(expected = NotImplementedException.class)
  public void testProcessInvalidStateDataInSpecies4() throws Exception {
    final SpeciesState state = new SpeciesState();
    state.addModificationResidue(new CellDesignerModificationResidue());
    final CellDesignerSimpleMolecule species = new CellDesignerSimpleMolecule();

    parser.processStateDataInSpecies(species, state);
  }

  @Test(expected = NotImplementedException.class)
  public void testProcessInvalidStateDataInSpecies5() throws Exception {
    final SpeciesState state = new SpeciesState();
    state.setStructuralState("state");
    final CellDesignerSimpleMolecule species = new CellDesignerSimpleMolecule();

    parser.processStateDataInSpecies(species, state);
  }

  @Test(expected = NotImplementedException.class)
  public void testProcessInvalidStateDataInSpecies6() throws Exception {
    final SpeciesState state = new SpeciesState();
    state.setStructuralState("state");
    final CellDesignerAntisenseRna species = new CellDesignerAntisenseRna();

    parser.processStateDataInSpecies(species, state);
  }

  @Test(expected = NotImplementedException.class)
  public void testProcessInvalidStateDataInSpecies7() throws Exception {
    final SpeciesState state = new SpeciesState();
    state.setStructuralState("state");
    final CellDesignerSpecies<?> species = new CellDesignerSpecies<Gene>();

    parser.processStateDataInSpecies(species, state);
  }

  @Test(expected = NotImplementedException.class)
  public void testProcessInvalidStateDataInSpecies8() throws Exception {
    final SpeciesState state = new SpeciesState();
    state.addModificationResidue(new CellDesignerModificationResidue());
    final CellDesignerSpecies<?> species = new CellDesignerSpecies<Gene>();

    parser.processStateDataInSpecies(species, state);
  }

  @Test
  public void testProcessAntisenseRnaStateDataInSpecies() throws Exception {
    final SpeciesState state = new SpeciesState();
    state.addModificationResidue(new CellDesignerModificationResidue());
    final CellDesignerAntisenseRna species = new CellDesignerAntisenseRna();

    parser.processStateDataInSpecies(species, state);

    assertEquals(1, species.getRegions().size());
  }

  @Test
  public void testSpeciesWithTwoStructuralState() throws Exception {
    final GenericProtein protein = createProtein();
    final StructuralState state1 = createStructuralState(protein);
    final StructuralState state2 = createStructuralState(protein);
    protein.addStructuralState(state1);
    protein.addStructuralState(state2);

    proteinXmlParser.initializeProteinData(Collections.singletonList(protein));

    final String xml = parser.toXml(protein);
    assertTrue(xml.contains(state1.getName()));
    assertFalse(xml.contains(state2.getName()));
    assertEquals(1, getWarnings().size());
  }

}
