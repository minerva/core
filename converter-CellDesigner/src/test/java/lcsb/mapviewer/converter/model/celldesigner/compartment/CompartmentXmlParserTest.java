package lcsb.mapviewer.converter.model.celldesigner.compartment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerCompartment;
import lcsb.mapviewer.model.map.compartment.Compartment;

public class CompartmentXmlParserTest extends CellDesignerTestFunctions {
  protected Logger logger = LogManager.getLogger();

  private CompartmentXmlParser compartmentParser;
  private CellDesignerElementCollection elements;
  private String testProteinFile = "testFiles/xmlNodeTestExamples/compartment.xml";

  @Before
  public void setUp() throws Exception {
    elements = new CellDesignerElementCollection();
    compartmentParser = new CompartmentXmlParser(elements);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParseXmlCompartment() throws Exception {
    String xmlString = readFile(testProteinFile);

    Pair<String, CellDesignerCompartment> result = compartmentParser.parseXmlElement(xmlString);
    CellDesignerCompartment compartment = result.getRight();
    assertNotNull(compartment);
    assertEquals("c1", compartment.getElementId());
    assertEquals(1, compartment.getMiriamData().size());
    assertEquals("c1", compartment.getName());
  }

  @Test
  public void testToXml() throws Exception {
    String xmlString = readFile(testProteinFile);

    Pair<String, CellDesignerCompartment> result = compartmentParser.parseXmlElement(xmlString);
    CellDesignerCompartment compartment = result.getRight();
    Compartment alias = compartment.createModelElement("id");
    String transformedXml = compartmentParser.toXml(alias);
    assertNotNull(transformedXml);
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    InputSource is = new InputSource(new StringReader(transformedXml));
    Document doc = builder.parse(is);
    NodeList root = doc.getChildNodes();
    assertEquals("compartment", root.item(0).getNodeName());

    Pair<String, CellDesignerCompartment> result2 = compartmentParser.parseXmlElement(compartmentParser.toXml(alias));
    CellDesignerCompartment compartment2 = result2.getRight();
    assertEquals(alias.getMiriamData().size(), compartment2.getMiriamData().size());
    assertEquals(alias.getName(), compartment2.getName());
  }

  @Test(expected = CompartmentParserException.class)
  public void testParseInvalidCompartment() throws Exception {
    compartmentParser.parseXmlElement(readFile("testFiles/invalid/compartment.xml"));
  }

  @Test(expected = CompartmentParserException.class)
  public void testParseInvalidCompartment2() throws Exception {
    compartmentParser.parseXmlElement(readFile("testFiles/invalid/compartment2.xml"));
  }

  @Test(expected = CompartmentParserException.class)
  public void testParseInvalidCompartment3() throws Exception {
    compartmentParser.parseXmlElement(readFile("testFiles/invalid/compartment3.xml"));
  }

  @Test
  public void testToXmlWithParent() throws Exception {
    Compartment child = new Compartment("c_1");
    Compartment parent = new Compartment("p_1");
    child.setCompartment(parent);

    String xml = compartmentParser.toXml(child);
    assertTrue(xml.contains(parent.getElementId()));
  }

}
