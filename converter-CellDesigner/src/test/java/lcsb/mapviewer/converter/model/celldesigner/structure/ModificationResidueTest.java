package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.model.map.species.IonChannelProtein;
import lcsb.mapviewer.model.map.species.field.ModificationState;

public class ModificationResidueTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CellDesignerModificationResidue());
  }

  @Test
  public void testConstructor() {
    CellDesignerModificationResidue original = new CellDesignerModificationResidue();
    CellDesignerModificationResidue copy = new CellDesignerModificationResidue(original);
    assertNotNull(copy);
  }

  @Test
  public void testUpdate() {
    CellDesignerModificationResidue original = new CellDesignerModificationResidue();
    CellDesignerModificationResidue update = new CellDesignerModificationResidue();

    String name = "n";
    String side = "s";
    ModificationState state = ModificationState.ACETYLATED;
    Double angle = 3.9;
    Double size = 5.0;

    update.setName(name);
    update.setSide(side);
    update.setAngle(angle);
    update.setSize(size);
    update.setState(state);

    original.update(update);

    assertEquals(name, original.getName());
    assertEquals(side, original.getSide());
    assertEquals(angle, original.getAngle());
    assertEquals(size, original.getSize());
    assertEquals(state, original.getState());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidAngle() {
    CellDesignerModificationResidue original = new CellDesignerModificationResidue();
    original.setAngle("a2.0");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidSize() {
    CellDesignerModificationResidue original = new CellDesignerModificationResidue();
    original.setSize("a2.0");
  }

  @Test
  public void testGetters() {
    CellDesignerModificationResidue original = new CellDesignerModificationResidue();

    String doubleStr = "2.0";
    String nullStr = null;
    Double angle = 2.0;
    CellDesignerSpecies<?> species = new CellDesignerSpecies<IonChannelProtein>();

    original.setAngle(doubleStr);
    assertEquals(angle, original.getAngle(), Configuration.EPSILON);
    original.setAngle(nullStr);
    assertNull(original.getAngle());

    original.setSize(doubleStr);
    assertEquals(angle, original.getSize(), Configuration.EPSILON);
    original.setSize(doubleStr);
    original.setSize(nullStr);
    assertNull(original.getSize());

    original.setSpecies(species);
    assertEquals(species, original.getSpecies());
  }

  @Test
  public void testCopy() {
    CellDesignerModificationResidue degraded = new CellDesignerModificationResidue().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(CellDesignerModificationResidue.class).copy();
  }

}
