package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class CellDesignerChemicalTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    String smiles = "smiles";
    String inChI = "inchi";
    String inChIKey = "key";

    CellDesignerChemical<?> chemical = new CellDesignerIon();
    chemical.setSmiles(smiles);
    chemical.setInChI(inChI);
    chemical.setInChIKey(inChIKey);

    assertEquals(smiles, chemical.getSmiles());
    assertEquals(inChI, chemical.getInChI());
    assertEquals(inChIKey, chemical.getInChIKey());
  }

}
