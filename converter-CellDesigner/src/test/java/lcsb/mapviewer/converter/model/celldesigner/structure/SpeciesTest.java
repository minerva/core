package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Rna;

public class SpeciesTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CellDesignerSpecies<GenericProtein>());
  }

  @Test
  public void testGetters() {
    CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
    String elementId = "51";
    Double initialAmount = 54.0;
    Double initialConcentration = 58.0;
    Integer charge = 59;
    Boolean onlySubstanceUnits = true;

    String trueStr = "true";
    assertFalse(species.isHypothetical());
    species.setHypothetical(trueStr);
    assertTrue(species.isHypothetical());

    species.setElementId(elementId);
    assertEquals(elementId, species.getElementId());

    species.setInitialAmount(initialAmount);
    assertEquals(initialAmount, species.getInitialAmount());

    species.setOnlySubstanceUnits(onlySubstanceUnits);
    assertEquals(onlySubstanceUnits, species.getOnlySubstanceUnits());

    species.setInitialConcentration(initialConcentration);
    assertEquals(initialConcentration, species.getInitialConcentration());

    species.setCharge(charge);

    assertEquals(charge, species.getCharge());

    species.setElementId("");
    assertEquals("", species.getElementId());
  }

  @Test
  public void testConstructor() {
    String id = "as_id";
    CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>(id);
    assertEquals(id, species.getElementId());
  }

  @Test
  public void testCopy() {
    String id = "as_id";
    CellDesignerSpecies<GenericProtein> species = new CellDesignerSpecies<>(id);
    species.setBoundaryCondition(true);
    species.setConstant(true);
    species.setSubstanceUnits(new SbmlUnit("id"));
    CellDesignerSpecies<?> copy = species.copy();
    assertEquals(id, species.getElementId());
    assertEquals(Boolean.TRUE, copy.isBoundaryCondition());
    assertEquals(Boolean.TRUE, copy.isConstant());
    assertNotNull(copy.getSubstanceUnits());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetId() {
    CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
    species.setElementId("");
    species.setElementId("xx");
    species.setElementId("yy");
  }

  @Test
  public void testUpdate1() {
    int warningsCount = getWarnings().size();
    CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
    species.setName("A");
    species.setNotes("XXX");
    CellDesignerSpecies<?> species2 = new CellDesignerSpecies<GenericProtein>();
    species2.setName("B");
    species.update(species2);
    int warningsCount2 = getWarnings().size();
    assertEquals(1, warningsCount2 - warningsCount);
  }

  @Test
  public void testUpdate2() {
    CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
    species.setNotes("XXX");
    CellDesignerSpecies<?> species2 = new CellDesignerSpecies<GenericProtein>();
    species2.addMiriamData(new MiriamData());
    species.update(species2);
    int warningsCount = getWarnings().size();
    assertEquals(0, warningsCount);
  }

  @Test
  public void testUpdate3() {
    CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
    species.setNotes("XXX");
    CellDesignerSpecies<?> species2 = new CellDesignerSpecies<GenericProtein>();
    species2.setNotes("xx");
    species.update(species2);
    int warningsCount = getWarnings().size();
    assertEquals(0, warningsCount);
  }

  @Test
  public void testUpdate4() {
    CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
    species.setNotes("XX");
    CellDesignerSpecies<?> species2 = new CellDesignerSpecies<GenericProtein>();
    species2.setNotes("xxX");
    species.update(species2);
    int warningsCount = getWarnings().size();
    assertEquals(0, warningsCount);
    assertEquals(3, species.getNotes().length());
  }

  @Test
  public void testUpdate5() {
    CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
    species.setNotes("XX");
    CellDesignerSpecies<?> species2 = new CellDesignerSpecies<GenericProtein>();
    species2.setNotes("a as x");
    species2.setHypothetical(true);
    species2.setSymbol("sym");
    species2.setHomodimer(2);
    species.update(species2);
    int warningsCount = getWarnings().size();
    assertEquals(0, warningsCount);
  }

  @Test
  public void testUpdate6() {
    CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
    species.setSymbol("sym1");
    species.setFullName("a_sym1");
    CellDesignerSpecies<?> species2 = new CellDesignerSpecies<GenericProtein>();
    species2.setSymbol("sym2");
    species2.setFullName("b_sym1");
    species.update(species2);
    int warningsCount = getWarnings().size();
    assertEquals(2, warningsCount);
  }

  @Test
  public void testUpdate7() {
    CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
    CellDesignerSpecies<?> species2 = new CellDesignerSpecies<GenericProtein>();
    species2.addSynonym("syn");
    species2.addFormerSymbol("sym");
    species.update(species2);
    int warningsCount = getWarnings().size();
    assertEquals(0, warningsCount);
  }

  @Test
  public void testUpdateNotes() {
    String notes1 = "XXX A";
    String notes2 = "Notes B";
    CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
    species.setNotes(notes1);
    CellDesignerSpecies<?> species2 = new CellDesignerSpecies<GenericProtein>();
    species2.setNotes(notes2);
    species.update(species2);
    assertEquals(0, getWarnings().size());
    assertNotEquals(notes1 + notes2, species.getNotes());
    assertNotEquals(notes2 + notes1, species.getNotes());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidAmount() {
    CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
    species.setInitialAmount("a1");
  }

  @Test
  public void testSetInitialAmount() {
    CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
    species.setInitialAmount("1");
    assertEquals(1, species.getInitialAmount(), Configuration.EPSILON);
    species.setInitialAmount("1");
    species.setInitialAmount((String) null);
    assertNull(species.getInitialAmount());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidCharge() {
    CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
    species.setCharge("a1");
  }

  @Test
  public void testSetCharge() {
    CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
    species.setCharge("1");
    assertEquals((Integer) 1, species.getCharge());
    species.setCharge("1");
    species.setCharge((String) null);
    assertNull(species.getCharge());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidOnlySubstanceUnits() {
    CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
    species.setOnlySubstanceUnits("a1");
  }

  @Test
  public void testSetOnlySubstanceUnits() {
    CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
    species.setOnlySubstanceUnits("true");
    assertTrue(species.getOnlySubstanceUnits());
    species.setOnlySubstanceUnits("false");
    assertFalse(species.getOnlySubstanceUnits());

    species.setOnlySubstanceUnits("true");
    species.setOnlySubstanceUnits((String) null);
    assertFalse(species.getOnlySubstanceUnits());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidInitialConcentration() {
    CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
    species.setInitialConcentration("a1");
  }

  @Test
  public void testSetInitialConcentration() {
    CellDesignerSpecies<?> species = new CellDesignerSpecies<GenericProtein>();
    species.setInitialConcentration("1");
    assertEquals(1, species.getInitialConcentration(), Configuration.EPSILON);
    species.setInitialConcentration((String) null);
    assertNull(species.getInitialConcentration());
  }

  @Test(expected = NotImplementedException.class)
  public void testCreateInvalidElement() {
    CellDesignerSpecies<?> complex = new CellDesignerSpecies<Rna>();
    complex.createModelElement("id");
  }

}
