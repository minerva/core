package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.species.SimpleMolecule;

public class SimpleMoleculeTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CellDesignerSimpleMolecule());
  }

  @Test
  public void testConstructor1() {
    CellDesignerSimpleMolecule degraded = new CellDesignerSimpleMolecule(new CellDesignerSpecies<SimpleMolecule>());
    assertNotNull(degraded);
  }

  @Test
  public void testCopy() {
    CellDesignerSimpleMolecule degraded = new CellDesignerSimpleMolecule().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    CellDesignerSimpleMolecule simpleMolecule = Mockito.spy(CellDesignerSimpleMolecule.class);
    simpleMolecule.copy();
  }

}
