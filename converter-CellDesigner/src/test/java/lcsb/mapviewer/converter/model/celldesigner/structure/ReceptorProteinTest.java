package lcsb.mapviewer.converter.model.celldesigner.structure;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.species.ReceptorProtein;

public class ReceptorProteinTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CellDesignerReceptorProtein());
  }

  @Test
  public void testConstructor() {
    CellDesignerReceptorProtein species = new CellDesignerReceptorProtein(new CellDesignerSpecies<ReceptorProtein>());
    assertNotNull(species);
  }

  @Test
  public void testCopy1() {
    CellDesignerReceptorProtein species = new CellDesignerReceptorProtein(new CellDesignerSpecies<ReceptorProtein>())
        .copy();
    assertNotNull(species);
  }

  @Test(expected = NotImplementedException.class)
  public void testCopy2() {
    CellDesignerReceptorProtein protein = Mockito.spy(CellDesignerReceptorProtein.class);
    protein.copy();
  }

}
