package lcsb.mapviewer.converter.model.celldesigner.species;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;

public class AbstractElementXmlParserTest extends CellDesignerTestFunctions {

  private AbstractElementXmlParser<?, ?> parser = new ProteinXmlParser(null);
  
  @Test
  public void testEncode() {
    String encoded = parser.encodeName("\n");
    assertTrue(encoded.contains("_br_"));
  }

}
