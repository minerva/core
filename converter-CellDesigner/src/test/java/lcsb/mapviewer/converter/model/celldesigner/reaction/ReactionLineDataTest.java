package lcsb.mapviewer.converter.model.celldesigner.reaction;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.mutable.MutableBoolean;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownTransitionReaction;

public class ReactionLineDataTest extends CellDesignerTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetByLineType() {
    assertNull(ReactionLineData.getByLineType(null, null));
  }

  @Test
  public void test() {
    assertNotNull(ReactionLineData.getByCellDesignerString("UNKNOWN_REDUCED_MODULATION"));
  }

  @Test
  public void testAllValues() {
    for (final ReactionLineData value : ReactionLineData.values()) {
      assertNotNull(ReactionLineData.valueOf(value.toString()));
    }
  }

  @Test(expected = ReactionParserException.class)
  public void testCreateInvalidReaction() throws Exception {
    ReactionLineData.TRANSPORT.createReaction(Mockito.mock(Reaction.class));
  }

  @Test
  public void testGetByReactionTypeMultithreaded() throws InterruptedException {
    ReflectionTestUtils.setField(ReactionLineData.class, "map", null);

    MutableBoolean exceptionHappened = new MutableBoolean(false);

    List<Thread> threads = new ArrayList<>();
    for (int j = 0; j < 100; j++) {
      Thread thread = new Thread() {
        @Override
        public void run() {
          try {
            assertNotNull(ReactionLineData.getByReactionType(UnknownTransitionReaction.class));
          } catch (Throwable e) {
            exceptionHappened.setTrue();
            e.printStackTrace();
          }
        }
      };
      threads.add(thread);
    }
    for (Thread thread : threads) {
      thread.start();
    }
    for (Thread thread : threads) {
      thread.join();
    }
    assertFalse(exceptionHappened.booleanValue());

  }

  @Test
  public void testGetByCellDesignerStringMultithreaded() throws InterruptedException {
    ReflectionTestUtils.setField(ReactionLineData.class, "cellDesignerMap", null);

    MutableBoolean exceptionHappened = new MutableBoolean(false);

    List<Thread> threads = new ArrayList<>();
    for (int j = 0; j < 100; j++) {
      Thread thread = new Thread() {
        @Override
        public void run() {
          try {
            assertNotNull(ReactionLineData.getByCellDesignerString("UNKNOWN_TRANSITION"));
          } catch (Throwable e) {
            exceptionHappened.setTrue();
            e.printStackTrace();
          }
        }
      };
      threads.add(thread);
    }
    for (Thread thread : threads) {
      thread.start();
    }
    for (Thread thread : threads) {
      thread.join();
    }
    assertFalse(exceptionHappened.booleanValue());

  }

}
