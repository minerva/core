package lcsb.mapviewer.converter.model.celldesigner.geometry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;

import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationSite;

public class GeneCellDesignerAliasConverterTest {

  private GeneCellDesignerAliasConverter converter = new GeneCellDesignerAliasConverter(false);

  @Test
  public void testGetInvalidAliasPointCoordinates() {
    Gene alias = new Gene("id");
    alias.setX(1);
    alias.setY(12);
    alias.setWidth(10);
    alias.setHeight(10);
    assertNotNull(converter.getPointCoordinates(alias, null));
  }

  @Test
  public void testGetPath() {
    Model model = new ModelFullIndexed(null);
    Gene alias = new Gene("id");
    alias.setX(1);
    alias.setY(12);
    alias.setWidth(10);
    alias.setHeight(10);

    alias.setModel(model);
    PathIterator path = converter.getBoundPathIterator(alias);

    assertNotNull(path);
  }

  @Test
  public void testGetPath2() {
    GeneCellDesignerAliasConverter converter = new GeneCellDesignerAliasConverter(true);

    Model model = new ModelFullIndexed(null);
    Gene alias = new Gene("id");
    alias.setX(1);
    alias.setY(12);
    alias.setWidth(10);
    alias.setHeight(10);

    alias.setModel(model);
    PathIterator path = converter.getBoundPathIterator(alias);

    assertNotNull(path);
  }

  @Test
  public void testGetAngleForPoint2() {
    Gene protein = new Gene("id");
    protein.setWidth(51);
    protein.setHeight(20);
    protein.setX(721.506302521008);
    protein.setY(996.8130252100841);

    Point2D point = new Point2D.Double(745.8351251000822, 1016.8130493164062);
    double angle = converter.getAngleForPoint(protein, point);
    Point2D newPoint = converter.getResidueCoordinates(protein, angle);
    assertEquals(0, point.distance(newPoint), 1e-4);
  }

  @Test
  public void testCodingRegionCoordinates() {
    Gene alias = new Gene("id");
    alias.setX(1);
    alias.setY(12);
    alias.setWidth(200);
    alias.setHeight(10);

    CodingRegion region = new CodingRegion();
    region.setWidth(15);
    region.setSpecies(alias);

    for (double pos = 0.1; pos < 0.9; pos += 0.1) {
      Point2D center = converter.getCoordinatesByPosition(alias, pos, region.getWidth());

      region.setPosition(center.getX() - region.getWidth() / 2, center.getY() - region.getHeight() / 2);

      double newPos = converter.getCellDesignerPositionByCoordinates(region);

      assertEquals(pos, newPos, Configuration.EPSILON);
    }
  }

  @Test
  public void testModificationSiteCoordinates() {
    Gene gene = new Gene("id");
    gene.setX(1);
    gene.setY(12);
    gene.setWidth(200);
    gene.setHeight(10);

    ModificationSite site = new ModificationSite();
    site.setSpecies(gene);

    for (double pos = 0.1; pos < 0.9; pos += 0.1) {
      Point2D center = converter.getCoordinatesByPosition(gene, pos, site.getWidth());

      site.setPosition(center.getX() - site.getWidth() / 2, center.getY() - site.getHeight() / 2);

      double newPos = converter.getCellDesignerPositionByCoordinates(site);

      assertEquals(pos, newPos, Configuration.EPSILON);
    }
  }

}
