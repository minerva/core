package lcsb.mapviewer.converter.model.celldesigner.geometry;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Unknown;

public class UnknownCellDesignerAliasConverterTest {
  private UnknownCellDesignerAliasConverter converter = new UnknownCellDesignerAliasConverter(false);

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetBoundPathIterator() {
    try {
      converter.getBoundPathIterator(null);
    } catch (final NotImplementedException e) {
      assertTrue(e.getMessage().contains("This class doesn't provide boundPath"));
    }
  }

  @Test
  public void testGetAnchorPointCoordinates() {
    Unknown unknown = new Unknown("id");
    unknown.setWidth(0);
    unknown.setHeight(0);
    Point2D point = converter.getAnchorPointCoordinates(unknown, 0);
    assertNotNull(point);
  }

}
