package lcsb.mapviewer.converter.model.celldesigner.geometry;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Point2D;

import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerTestFunctions;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;

public class ProteinCellDesignerAliasConverterTest extends CellDesignerTestFunctions {

  private ProteinCellDesignerAliasConverter converter = new ProteinCellDesignerAliasConverter(false);

  @Test(expected = NotImplementedException.class)
  public void testNotImplementedMethod() {
    Protein alias = Mockito.mock(Protein.class);
    converter.getBoundPathIterator(alias);
  }

  @Test(expected = NotImplementedException.class)
  public void testGetPointCoordinates() {
    Protein alias = Mockito.spy(Protein.class);
    alias.setWidth(10);
    alias.setHeight(10);
    converter.getPointCoordinates(alias, CellDesignerAnchor.E);
  }

  @Test
  public void testGetAngleForPoint() {
    Protein protein = new GenericProtein("id");
    protein.setWidth(200);
    protein.setHeight(30);
    double angle = 0;
    while (angle <= Math.PI * 2) {
      Point2D point = converter.getResidueCoordinates(protein, angle);
      double newAngle = converter.getAngleForPoint(protein, point);
      assertEquals(angle, newAngle, Configuration.EPSILON);

      angle += 0.1;
    }
  }

}
