package lcsb.mapviewer.converter.model.celldesigner.annotation;

import java.awt.Color;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.comparator.IntegerComparator;
import lcsb.mapviewer.common.comparator.ListComparator;
import lcsb.mapviewer.common.comparator.SetComparator;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.CommonXmlParser;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * This class is a converter of annotation provided by lcsb in raw text format
 * into set of MiriamData that can be used later on.
 * 
 * @author Piotr Gawron
 * 
 */
public class RestAnnotationParser {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Pattern used to find rdf node in string xml.
   */
  private Pattern rdfNodePattern = Pattern.compile("(?<=<rdf:RDF)([\\s\\S]*?)(?=</rdf:RDF>)");

  /**
   * Parser used for parsing annotations in rdf format.
   */
  private XmlAnnotationParser xmlAnnotationParser;

  /**
   * Class used for some simple operations on {@link BioEntity} elements.
   */
  private ElementUtils elementUtils = new ElementUtils();

  public RestAnnotationParser() {
    xmlAnnotationParser = new XmlAnnotationParser(CommonXmlParser.RELATION_TYPES_SUPPORTED_BY_CELL_DESIGNER);
  }

  /**
   * This method parse the string with annotations provided by the lcsb team.
   * Parsing has been prepared based on the information provided by Kazuhiro
   * Fujita, kaf@sbi.jp.
   *
   * @param annotationString
   *          - string with data to parse
   * @return list of {@link MiriamData annotations}
   */
  public Set<MiriamData> getMiriamData(final String annotationString) {
    Set<MiriamData> result = new HashSet<MiriamData>();

    // if the string is empty, then clean the state of the class and return
    if (annotationString == null) {
      return result;
    }

    String[] lines = annotationString.split("\n");
    for (int i = 0; i < lines.length; i++) {
      String line = lines[i];
      for (final NoteField field : NoteField.values()) {
        if (field.getMiriamType() != null) {
          ArrayList<String> ids = getIds(line, field.getCommonName() + ":");
          if (ids != null) {
            Set<MiriamData> hgncData = idsToData(MiriamRelationType.BQ_BIOL_HAS_VERSION, field.getMiriamType(), ids);
            result.addAll(hgncData);
          }
        }
      }
    }
    return result;
  }

  /**
   * Creates note string with structural information about element.
   *
   * @param element
   *          element for which notes are created
   * @return note string with structural information about element
   */
  public String createAnnotationString(final Element element) {
    return createAnnotationString(element, false);
  }

  /**
   * Creates note string with structural information about element.
   *
   * @param element
   *          element for which notes are created
   * @param forceFullInfo
   *          when true annotation string will contain information about empty
   *          fields
   * @return note string with structural information about element
   */
  public String createAnnotationString(final Element element, final boolean forceFullInfo) {
    Set<MiriamData> data = element.getMiriamData();
    StringBuilder sb = new StringBuilder();
    sb.append(createEntry(NoteField.SYMBOL, element.getSymbol(), forceFullInfo));
    sb.append(createEntry(NoteField.NAME, element.getFullName(), forceFullInfo));
    sb.append(createEntry(NoteField.PREVIOUS_SYMBOLS, element.getFormerSymbols(), forceFullInfo));
    sb.append(createEntry(NoteField.SYNONYMS, element.getSynonyms(), forceFullInfo));
    sb.append(createEntry(NoteField.HGNC, filterMiriam(data, MiriamType.HGNC), forceFullInfo));
    sb.append(createEntry(NoteField.ENTREZ, filterMiriam(data, MiriamType.ENTREZ), forceFullInfo));
    sb.append(createEntry(NoteField.REFSEQ, filterMiriam(data, MiriamType.REFSEQ), forceFullInfo));
    sb.append(createEntry(NoteField.REACTOME, filterMiriam(data, MiriamType.REACTOME), forceFullInfo));
    sb.append(createEntry(NoteField.PUBMED, filterMiriam(data, MiriamType.PUBMED), forceFullInfo));
    sb.append(createEntry(NoteField.KEGG_GENES, filterMiriam(data, MiriamType.KEGG_GENES), forceFullInfo));
    sb.append(createEntry(NoteField.PANTHER, filterMiriam(data, MiriamType.PANTHER), forceFullInfo));
    sb.append(createEntry(NoteField.DESCRIPTION, null, forceFullInfo));
    sb.append(createEntry(NoteField.ABBREVIATION, element.getAbbreviation(), forceFullInfo));
    sb.append(createEntry(NoteField.SEMANTIC_ZOOM_LEVEL_VISIBILITY, element.getVisibilityLevel(), forceFullInfo));
    sb.append(createEntry(NoteField.TRANSPARENCY_ZOOM_LEVEL_VISIBILITY, element.getTransparencyLevel(), forceFullInfo));

    sb.append(createEntry(NoteField.CHARGED_FORMULA, element.getFormula(), forceFullInfo));
    if (element instanceof Species) {
      sb.append(createEntry(NoteField.CHARGE, ((Species) element).getCharge(), forceFullInfo));
    }

    sb.append("\n");
    return sb.toString();
  }

  private List<MiriamData> filterMiriam(final Collection<MiriamData> miriamDataSet, final MiriamType type) {
    List<MiriamData> result = new ArrayList<>();
    for (final MiriamData miriamData : miriamDataSet) {
      if (miriamData.getDataType().equals(type)) {
        result.add(miriamData);
      }
    }
    return result;
  }

  private String createEntry(final NoteField type, final Object value, final boolean forceFullInfo) {
    if (isFieldAnnotated(type, ImportOnly.class)) {
      return "";
    }
    if (value == null) {
      if (forceFullInfo) {
        return type.getCommonName() + ": \n";
      } else {
        return "";
      }
    } else if (value instanceof String) {
      String string = (String) value;
      if (!(string.trim().isEmpty()) || forceFullInfo) {
        return type.getCommonName() + ": " + XmlParser.escapeXml(string) + "\n";
      } else {
        return "";
      }
    } else if (value instanceof Integer) {
      return type.getCommonName() + ": " + value + "\n";
    } else if (value instanceof Collection) {
      Collection<?> collection = (Collection<?>) value;
      if (collection.size() > 0 || forceFullInfo) {
        String result = "";
        for (final Object object : collection) {
          if (!result.equals("")) {
            result = result + ", ";
          }
          if (object instanceof MiriamData) {
            result += ((MiriamData) object).getResource();
          } else {
            result += XmlParser.escapeXml(object.toString());
          }

        }
        return type.getCommonName() + ": " + result + "\n";
      } else {
        return "";
      }
    } else {
      throw new InvalidArgumentException("Unknown class type: " + value.getClass());
    }
  }

  /**
   * Returns value for the type of the structured annotation. Structured
   * annotation is in format:
   *
   * <pre>
   * TYPE1:description1
   * TYPE2:descripton2
   * ...
   * </pre>
   *
   * @param annotationString
   *          whole annotation string
   * @param prefix
   *          prefix used for identifying line
   * @return value for the given type in structured annotation
   */
  private String getParamByPrefix(final String annotationString, final String prefix) {
    if (annotationString == null) {
      return null;
    }

    String[] lines = annotationString.split("\n");
    for (int i = 0; i < lines.length; i++) {
      String line = lines[i];
      if (line.startsWith(prefix)) {
        String result = StringEscapeUtils.unescapeHtml4(line.substring(prefix.length()).trim());
        if (result.equals("")) {
          return null;
        } else {
          return result;
        }
      }
    }
    return null;
  }

  /**
   * Returns list of symbols from the annotation string.
   *
   * @param annotationString
   *          annotation string
   * @return list of symbol
   */
  public List<String> getSynonyms(final String annotationString) {
    List<String> result = new ArrayList<String>();
    String synonyms = getParamByPrefix(annotationString, NoteField.SYNONYMS.getCommonName() + ":");
    if (synonyms != null) {
      for (final String string : synonyms.split(",")) {
        if (!string.trim().equals("")) {
          result.add(string.trim());
        }
      }
    }
    return result;
  }

  /**
   * Returns list of former symbols from the annotation string.
   *
   * @param annotationString
   *          annotation string
   * @return list of former symbol
   */
  public List<String> getFormerSymbols(final String annotationString) {
    List<String> result = new ArrayList<String>();
    String formerSymbols = getParamByPrefix(annotationString, NoteField.PREVIOUS_SYMBOLS.getCommonName() + ":");
    if (formerSymbols != null) {
      for (final String string : formerSymbols.split(",")) {
        if (!string.trim().equals("")) {
          result.add(string.trim());
        }
      }
    }
    return result;
  }

  /**
   * This method parse a string with id in a format:<br>
   * database_ID: idfield1, idfield2, ..., idfieldn<br>
   * into a vector of string ids.
   *
   * @param line
   *          - a line to be parsed
   * @param baseId
   *          - database string id
   * @return vector of string ids
   */
  protected ArrayList<String> getIds(final String line, final String baseId) {
    if (line.indexOf(baseId) >= 0) {
      ArrayList<String> result = new ArrayList<String>();
      String tmpLine = line.substring(baseId.length());
      String[] ids = tmpLine.split(",");
      for (final String string : ids) {
        // only non-empty ids
        if (!string.trim().equals("")) {
          result.add(string.trim());
        }
      }
      return result;
    } else {
      return null;
    }
  }

  /**
   * This method transform the vector of ids into a set of MiriamData.
   *
   * @param type
   *          - relationtype of the annotaion ids
   * @param miriamType
   *          {@link MiriamType type} of the reference resource
   * @param ids
   *          - list of ids
   * @return set of miriam data
   */
  private Set<MiriamData> idsToData(final MiriamRelationType type, final MiriamType miriamType, final ArrayList<String> ids) {
    Set<MiriamData> result = new HashSet<MiriamData>();
    for (final String id : ids) {
      MiriamData md = new MiriamData(type, miriamType, id);
      result.add(md);
    }
    return result;
  }

  /**
   * Process element notes and assign structural information from it.
   *
   * @param node
   *          node with notes about element
   * @param element
   *          where the structural data should be put
   */
  public void processNotes(final Node node, final BioEntity element) {
    String notes = getNotes(node);
    processNotes(notes, element);
  }

  /**
   * Process notes and assign structural information from it.
   *
   * @param element
   *          object with notes to be processed
   */
  public void processNotes(final Element element) {
    processNotes(element.getNotes(), element);
  }

  /**
   * Process notes and assign structural information from it.
   *
   * @param notes
   *          notes about element
   * @param object
   *          where the structural data should be put
   */
  public void processNotes(final String notes, final Drawable object) {
    StringBuilder annotations = new StringBuilder();

    String[] string = notes.split("\n");
    StringBuilder newNotes = new StringBuilder("");
    for (final String string2 : string) {
      boolean remove = false;
      for (final NoteField field : NoteField.values()) {
        if (string2.startsWith(field.getCommonName() + ":") && field.getClazz().isAssignableFrom(object.getClass())) {
          remove = true;
        } else if (string2.startsWith(field.getCommonName() + "=")
            && field.getClazz().isAssignableFrom(object.getClass())) {
          remove = true;
        }

      }
      if (remove) {
        annotations.append(string2 + "\n");
      } else {
        newNotes.append(string2 + "\n");
      }
    }
    String ann = annotations.toString();
    setZIndex(object, ann);
    if (object instanceof LayerText) {
      LayerText layerText = (LayerText) object;
      setBackgroundColor(layerText, ann);
      setBorderColor(layerText, ann);
      layerText.setNotes(newNotes.toString().trim());
    }
    if (object instanceof BioEntity) {
      BioEntity bioEntity = (BioEntity) object;
      bioEntity.setNotes(newNotes.toString().trim());

      setNotes(bioEntity, ann);
      setSymbol(bioEntity, ann);
      setSynonyms(bioEntity, ann);
      setSemanticZoomLevelVisibility(bioEntity, ann);
      setAbbreviation(bioEntity, ann);
      setFormula(bioEntity, ann);
      if (object instanceof Reaction) {
        Reaction reaction = (Reaction) object;
        setMechanicalConfidenceScoreToReaction(reaction, ann);
        setLowerBoundToReaction(reaction, ann);
        setUpperBoundToReaction(reaction, ann);
        setSubsystemToReaction(reaction, ann);
        setGeneProteinReactionToReaction(reaction, ann);
      } else if (object instanceof Element) {
        setTransparencyZoomLevelVisibility((Element) object, ann);
        setFullNameToSpecies((Element) object, ann);
        setFormerSymbolsToSpecies((Element) object, ann);
        if (object instanceof Species) {
          setCharge((Species) object, ann);
        }
      } else {
        throw new NotImplementedException("Don't know how to process class: " + object.getClass());
      }

      try {
        processRdfDescription(bioEntity);
      } catch (final InvalidXmlSchemaException e) {
        String warning = elementUtils.getElementTag(object) + " Problem with processing notes. Invalid RDF node.";
        logger.warn(warning);
      }
    }
  }

  /**
   * Transforms xml node into notes.
   *
   * @param node
   *          xml node with notes
   * @return string with notes
   */
  public String getNotes(final Node node) {
    String notes = "";

    if (!node.getNodeName().contains("notes")) {
      throw new InvalidArgumentException("Invalid notes node");
    }

    Node htmlNode = XmlParser.getNode("html", node.getChildNodes(), true);
    if (htmlNode == null) {
      notes = XmlParser.nodeToString(node).trim();
    } else {
      Node bodyNode = XmlParser.getNode("body", htmlNode.getChildNodes(), true);
      if (bodyNode != null) {
        notes = XmlParser.nodeToString(bodyNode).trim();
      }
    }

    if (notes.indexOf("</head>") >= 0) {
      notes = notes.substring(notes.indexOf("</head>") + "</head>".length());
    }
    notes = notes.replace("xmlns=\"http://www.w3.org/1999/xhtml\"", "");
    return notes.replaceAll("&amp;", "&");
  }

  /**
   * Assigns synonyms to the element from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setSynonyms(final BioEntity element, final String annotationString) {
    List<String> synonyms = getSynonyms(annotationString);
    if (synonyms.size() == 0) {
      return;
    }
    if (element.getSynonyms() == null || element.getSynonyms().size() == 0) {
      element.setSynonyms(synonyms);
    } else {
      SetComparator<String> stringSetComparator = new SetComparator<>(new StringComparator());
      Set<String> set1 = new HashSet<String>();
      Set<String> set2 = new HashSet<String>();
      set1.addAll(element.getSynonyms());
      set2.addAll(synonyms);
      if (stringSetComparator.compare(set1, set2) != 0) {
        logger.warn(elementUtils.getElementTag(element) + "Synonyms list different than default [" + synonyms + "]["
            + element.getSynonyms() + "]. Ignoring.");
      }
    }
  }

  /**
   * Assigns list of symbols to the element from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setFormerSymbolsToSpecies(final Element element, final String annotationString) {
    List<String> formerSymbols = getFormerSymbols(annotationString);
    if (formerSymbols.size() == 0) {
      return;
    }
    if (element.getFormerSymbols() == null || element.getFormerSymbols().size() == 0) {
      element.setFormerSymbols(formerSymbols);
    } else {
      ListComparator<String> stringListComparator = new ListComparator<>(new StringComparator());
      if (stringListComparator.compare(element.getFormerSymbols(), formerSymbols) != 0) {
        logger.warn(elementUtils.getElementTag(element) + " Former symbols list different than default ["
            + formerSymbols + "][" + element.getFormerSymbols() + "]. Ignoring.");
      }
    }
  }

  /**
   * Assigns full name to the element from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setFullNameToSpecies(final Element element, final String annotationString) {
    StringComparator stringComparator = new StringComparator();
    String fullName = getFullName(annotationString);
    if (fullName == null) {
      return;
    }
    if (element.getFullName() == null) {
      element.setFullName(fullName);
    } else if (stringComparator.compare(element.getFullName(), fullName, true) != 0) {
      logger.warn(elementUtils.getElementTag(element) + " New full name different than default [" + fullName + "]["
          + element.getFullName() + "]. Ignoring.");
    }
  }

  public String getFullName(final String annotationString) {
    return getParamByPrefix(annotationString, NoteField.NAME.getCommonName() + ":");
  }

  /**
   * Assigns abbreviation to the element from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setAbbreviation(final BioEntity element, final String annotationString) {
    StringComparator stringComparator = new StringComparator();
    String abbreviation = getAbbreviation(annotationString);
    if (abbreviation == null) {
      return;
    }
    if (element.getAbbreviation() == null) {
      element.setAbbreviation(abbreviation);
    } else if (stringComparator.compare(element.getAbbreviation(), abbreviation, true) != 0) {
      logger.warn(elementUtils.getElementTag(element) + " Abbreviation different than default [" + abbreviation + "]["
          + element.getAbbreviation() + "]. Ignoring.");
    }
  }

  public String getAbbreviation(final String annotationString) {
    return getParamByPrefix(annotationString, NoteField.ABBREVIATION.getCommonName() + ":");
  }

  /**
   * Assigns subsystem to the reaction from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setSubsystemToReaction(final Reaction element, final String annotationString) {
    StringComparator stringComparator = new StringComparator();
    String abbreviation = getParamByPrefix(annotationString, NoteField.SUBSYSTEM.getCommonName() + ":");
    if (abbreviation == null) {
      return;
    }
    if (element.getSubsystem() == null) {
      element.setSubsystem(abbreviation);
    } else if (stringComparator.compare(element.getSubsystem(), abbreviation, true) != 0) {
      logger.warn(elementUtils.getElementTag(element) + " Subsystem different than default [" + abbreviation + "]["
          + element.getSubsystem() + "]. Ignoring.");
    }
  }

  /**
   * Assigns gene protein reaction to the reaction from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setGeneProteinReactionToReaction(final Reaction element, final String annotationString) {
    StringComparator stringComparator = new StringComparator();
    String abbreviation = getParamByPrefix(annotationString, NoteField.GENE_PROTEIN_REACTION.getCommonName() + ":");
    if (abbreviation == null) {
      return;
    }
    if (element.getGeneProteinReaction() == null) {
      element.setGeneProteinReaction(abbreviation);
    } else if (stringComparator.compare(element.getGeneProteinReaction(), abbreviation, true) != 0) {
      logger.warn(elementUtils.getElementTag(element) + " GeneProteinReaction different than default [" + abbreviation
          + "][" + element.getGeneProteinReaction() + "]. Ignoring.");
    }
  }

  /**
   * Assigns formula to the element from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setFormula(final BioEntity element, final String annotationString) {
    StringComparator stringComparator = new StringComparator();
    String formula = getFormula(annotationString);
    if (formula == null) {
      formula = getParamByPrefix(annotationString, NoteField.CHARGED_FORMULA.getCommonName() + ":");
      if (formula == null) {
        return;
      }
    }
    if (element.getFormula() == null) {
      element.setFormula(formula);
    } else if (stringComparator.compare(element.getFormula(), formula, true) != 0) {
      logger.warn(elementUtils.getElementTag(element) + " Formula different than default [" + formula + "]["
          + element.getFormula() + "]. Ignoring.");
    }
  }

  public String getFormula(final String annotationString) {
    return getParamByPrefix(annotationString, NoteField.FORMULA.getCommonName() + ":");
  }

  /**
   * Assigns mechanical confidence score to the reaction from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setMechanicalConfidenceScoreToReaction(final Reaction element, final String annotationString) {
    IntegerComparator integerComparator = new IntegerComparator();

    String formula = getParamByPrefix(annotationString, NoteField.MECHANICAL_CONFIDENCE_SCORE.getCommonName() + ":");
    if (formula == null) {
      return;
    }
    Integer charge = Integer.valueOf(formula);
    if (element.getMechanicalConfidenceScore() == null) {
      element.setMechanicalConfidenceScore(charge);
    } else if (integerComparator.compare(element.getMechanicalConfidenceScore(), charge) != 0) {
      logger.warn(elementUtils.getElementTag(element) + " MechanicalConfidenceScore different than default [" + formula
          + "][" + element.getFormula() + "]. Ignoring.");
    }
  }

  /**
   * Assigns charge to the element from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setCharge(final Species element, final String annotationString) {
    IntegerComparator integerComparator = new IntegerComparator();
    String value = getParamByPrefix(annotationString, NoteField.CHARGE.getCommonName() + ":");
    if (value == null || value.trim().equals("") || value.trim().equals("-")) {
      return;
    }
    try {
      Integer charge = Integer.valueOf(value);
      if (element.getCharge() == null || element.getCharge() == 0) {
        element.setCharge(charge);
      } else if (integerComparator.compare(element.getCharge(), charge) != 0) {
        logger.warn(elementUtils.getElementTag(element) + " Charge different than default [" + value + "]["
            + element.getCharge() + "]. Ignoring.");
      }
    } catch (final NumberFormatException e) {
      logger.warn(elementUtils.getElementTag(element) + " Invalid charge (integer value expected): " + value);
    }
  }

  /**
   * Assigns lower bound to the reaction from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setLowerBoundToReaction(final Reaction element, final String annotationString) {
    DoubleComparator doubleComparator = new DoubleComparator();
    String formula = getParamByPrefix(annotationString, NoteField.LOWER_BOUND.getCommonName() + ":");
    if (formula == null || formula.trim().equals("")) {
      return;
    }
    Double value = Double.valueOf(formula);
    if (element.getLowerBound() == null) {
      element.setLowerBound(value);
    } else if (doubleComparator.compare(element.getLowerBound(), value) != 0) {
      logger.warn(elementUtils.getElementTag(element) + " LowerBound different than default [" + formula + "]["
          + element.getLowerBound() + "]. Ignoring.");
    }
  }

  /**
   * Assigns upper bound to the reaction from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setUpperBoundToReaction(final Reaction element, final String annotationString) {
    DoubleComparator doubleComparator = new DoubleComparator();
    String formula = getParamByPrefix(annotationString, NoteField.UPPER_BOUND.getCommonName() + ":");
    if (formula == null || formula.trim().equals("")) {
      return;
    }
    Double value = Double.valueOf(formula);
    if (element.getUpperBound() == null) {
      element.setUpperBound(value);
    } else if (doubleComparator.compare(element.getUpperBound(), value) != 0) {
      logger.warn(elementUtils.getElementTag(element) + " UpperBound different than default [" + formula + "]["
          + element.getUpperBound() + "]. Ignoring.");
    }
  }

  /**
   * Assigns symbol to the element from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setSymbol(final BioEntity element, final String annotationString) {
    String symbol = getSymbol(annotationString);
    if (symbol == null) {
      return;
    }
    if (element.getSymbol() == null) {
      element.setSymbol(symbol);
    } else if (!element.getSymbol().equals(symbol)) {
      logger.warn(elementUtils.getElementTag(element) + " New symbol different than default [" + symbol + "]["
          + element.getSymbol() + "]. Ignoring.");
    }
  }

  public String getSymbol(final String annotationString) {
    return getParamByPrefix(annotationString, NoteField.SYMBOL.getCommonName() + ":");
  }

  /**
   * Assigns semanticZoomingLevel to the element from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setSemanticZoomLevelVisibility(final BioEntity element, final String annotationString) {
    String zoomLevelVisibility = getParamByPrefix(annotationString,
        NoteField.SEMANTIC_ZOOM_LEVEL_VISIBILITY.getCommonName() + ":");
    if (zoomLevelVisibility == null) {
      return;
    }
    if (element.getVisibilityLevel() == null || element.getVisibilityLevel().isEmpty()) {
      element.setVisibilityLevel(zoomLevelVisibility);
    } else if (!element.getVisibilityLevel().equals(zoomLevelVisibility)) {
      logger.warn(elementUtils.getElementTag(element) + " New "
          + NoteField.SEMANTIC_ZOOM_LEVEL_VISIBILITY.getCommonName() + " different than default [" + zoomLevelVisibility
          + "][" + element.getVisibilityLevel() + "]. Ignoring.");
    }
  }

  private void setTransparencyZoomLevelVisibility(final Element element, final String annotationString) {
    String transparencyZoomLevelVisibility = getParamByPrefix(annotationString,
        NoteField.TRANSPARENCY_ZOOM_LEVEL_VISIBILITY.getCommonName() + ":");
    if (element.getTransparencyLevel() == null || element.getTransparencyLevel().isEmpty()) {
      element.setTransparencyLevel(transparencyZoomLevelVisibility);
    } else if (!element.getTransparencyLevel().equals(transparencyZoomLevelVisibility)) {
      logger.warn(elementUtils.getElementTag(element) + " New "
          + NoteField.TRANSPARENCY_ZOOM_LEVEL_VISIBILITY.getCommonName() + " different than default ["
          + transparencyZoomLevelVisibility + "][" + element.getVisibilityLevel() + "]. Ignoring.");
    }
  }

  private void setZIndex(final Drawable element, final String annotationString) {
    String stringValue = getParamByPrefix(annotationString, NoteField.Z_INDEX.getCommonName() + ":");
    if (stringValue != null) {
      try {
        Integer z = Integer.valueOf(stringValue);
        if (element.getZ() == null) {
          element.setZ(z);
        } else if (!element.getZ().equals(z)) {
          logger.warn(elementUtils.getElementTag(element) + " New " + NoteField.Z_INDEX.getCommonName()
              + " different than default [" + stringValue + "][" + element.getZ() + "]. Ignoring.");
        }
      } catch (final NumberFormatException e) {
        logger.warn("Invalid z index", e);
      }
    }
  }

  /**
   * Assigns notes to the element from notes string. This might look strange.
   * The idea is that sometimes we have notes from more then one source. And the
   * data from these sources should be merged. So, this method in fact merges
   * notes in the element and description extracted from notes string.
   *
   * @param element
   *          element to which data should be put to
   * @param annotationString
   *          notes string
   */
  private void setNotes(final BioEntity element, final String annotationString) {
    String description = getDescription(annotationString);
    if (description == null) {
      return;
    }
    if (element.getNotes().trim().equals("")) {
      element.setNotes(description);
    } else if (element.getNotes().contains(description)) {
      return;
    } else {
      element.setNotes(element.getNotes().trim() + "\n" + description + "\n");
    }
  }

  public String getDescription(final String annotationString) {
    return getParamByPrefix(annotationString, NoteField.DESCRIPTION.getCommonName() + ":");
  }

  private void setBackgroundColor(final LayerText layerText, final String ann) {
    Color color = getColor(ann, NoteField.BACKGROUND_COLOR.getCommonName());
    if (color == null) {
      return;
    }
    if (layerText.getBackgroundColor() == null || layerText.getBackgroundColor().equals(Color.LIGHT_GRAY)) {
      layerText.setBackgroundColor(color);
    } else if (!layerText.getBackgroundColor().equals(color)) {
      logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, layerText),
          " New background color different than default [" + color + "]["
              + layerText.getBackgroundColor() + "]. Ignoring.");
    }

  }

  private void setBorderColor(final LayerText layerText, final String ann) {
    Color color = getColor(ann, NoteField.BORDER_COLOR.getCommonName());
    if (color == null) {
      return;
    }
    if (layerText.getBorderColor() == null || layerText.getBorderColor().equals(Color.LIGHT_GRAY)) {
      layerText.setBorderColor(color);
    } else if (!layerText.getBorderColor().equals(color)) {
      logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, layerText),
          " New border color different than default [" + color + "]["
              + layerText.getBorderColor() + "]. Ignoring.");
    }

  }

  Color getColor(final String notes, final String string) {
    String[] lines = notes.split("[\n\r]+");
    for (final String line : lines) {
      if (line.startsWith(string + "=") || line.startsWith(string + ":")) {
        String colorString = line.replace(string + "=", "").replace(string + ":", "");
        return new ColorParser().parse(colorString);
      }
    }
    return null;
  }

  /**
   * Process RDF description from notes, removes it from the description and
   * adds appropriate information to miriam data set.
   *
   * @param element
   *          notes of this element will be processed
   * @throws InvalidXmlSchemaException
   *           thrown when there is a problem with xml
   */
  void processRdfDescription(final BioEntity element) throws InvalidXmlSchemaException {
    String notes = element.getNotes();

    Matcher nodeMatcher = rdfNodePattern.matcher(notes);
    if (nodeMatcher.find()) {
      String rdfString = "<rdf:RDF" + nodeMatcher.group(1) + "</rdf:RDF>";
      element.addMiriamData(
          xmlAnnotationParser.parse(rdfString, new LogMarker(ProjectLogEntryType.PARSING_ISSUE, element)));

      notes = notes.substring(0, nodeMatcher.start() - "<rdf:RDF".length())
          + notes.substring(nodeMatcher.end() + "</rdf:RDF>".length());
      element.setNotes(notes);
    }
  }

  boolean isFieldAnnotated(final NoteField field, final Class<? extends Annotation> annotationClass) {
    try {
      Field f = field.getClass().getField(field.name());
      if (f.isAnnotationPresent(annotationClass)) {
        return true;
      }
    } catch (final NoSuchFieldException | SecurityException e) {
    }
    return false;
  }
}
