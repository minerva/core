package lcsb.mapviewer.converter.model.celldesigner.structure;

import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;

/**
 * Class representing CellDesigner {@link AntisenseRna}.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerAntisenseRna extends CellDesignerSpecies<AntisenseRna> {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * List of antisense rna regions (some rna sequences) in this object.
   */
  private List<CellDesignerModificationResidue> regions = new ArrayList<>();

  /**
   * Constructor that copies the data from species given in the argument.
   * 
   * @param species
   *          parent species from which we want to copy data
   */
  public CellDesignerAntisenseRna(final CellDesignerSpecies<?> species) {
    super(species);
    if (species instanceof CellDesignerAntisenseRna) {
      CellDesignerAntisenseRna asRna = (CellDesignerAntisenseRna) species;
      for (final CellDesignerModificationResidue region : asRna.getRegions()) {
        addRegion(new CellDesignerModificationResidue(region));
      }
    }
  }

  /**
   * Default constructor.
   */
  public CellDesignerAntisenseRna() {
    super();
  }

  /**
   * Add antisense rna region (part of rna sequence that has some meaning) to
   * the object. If the region with given id exists then the data of this region
   * is copied to the one that is already in the
   * {@link CellDesignerAntisenseRna}.
   *
   *
   * @param region
   *          region to add
   */
  public void addRegion(final CellDesignerModificationResidue region) {
    for (final CellDesignerModificationResidue region2 : regions) {
      if (region2.getIdModificationResidue().equals(region.getIdModificationResidue())) {
        region2.update(region);
        return;
      }
    }
    regions.add(region);
    region.setSpecies(this);
  }

  @Override
  public void update(final CellDesignerSpecies<?> sp) {
    super.update(sp);
    if (sp instanceof CellDesignerAntisenseRna) {
      CellDesignerAntisenseRna rna = (CellDesignerAntisenseRna) sp;
      for (final CellDesignerModificationResidue region : rna.getRegions()) {
        updateRegion(region);
      }
    }
  }

  @Override
  public CellDesignerAntisenseRna copy() {
    if (this.getClass() == CellDesignerAntisenseRna.class) {
      return new CellDesignerAntisenseRna(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public AntisenseRna createModelElement(final String aliasId) {
    AntisenseRna result = new AntisenseRna(aliasId);
    super.setModelObjectFields(result);
    return result;
  }

  @Override
  public void updateModelElementAfterLayoutAdded(final Species species) {
    AntisenseRna antisenseRna = (AntisenseRna) species;
    for (final CellDesignerModificationResidue region : regions) {
      ModificationResidue mr = region.createModificationResidue(antisenseRna);
      if (mr instanceof CodingRegion) {
        antisenseRna.addCodingRegion((CodingRegion) mr);
      } else if (mr instanceof ModificationSite) {
        antisenseRna.addModificationSite((ModificationSite) mr);
      } else if (mr instanceof ProteinBindingDomain) {
        antisenseRna.addProteinBindingDomain((ProteinBindingDomain) mr);
      } else {
        throw new InvalidArgumentException("Cannot add modification residue to element: " + mr.getClass());
      }
    }
  }

  /**
   * Method update antisense rna region from the object in params (if the object
   * with the same id already exists). If there is no object with given id then
   * new object is added to antisense rna.
   *
   * @param param
   *          - object with new data from where update should be performed
   */
  private void updateRegion(final CellDesignerModificationResidue param) {
    for (final CellDesignerModificationResidue region : regions) {
      if (region.getIdModificationResidue().equals(param.getIdModificationResidue())) {
        region.update(param);
        return;
      }
    }
    regions.add(param);
  }

  /**
   * @return the regions
   * @see #regions
   */
  public List<CellDesignerModificationResidue> getRegions() {
    return regions;
  }

  /**
   * @param regions
   *          the regions to set
   * @see #regions
   */
  public void setRegions(final List<CellDesignerModificationResidue> regions) {
    this.regions = regions;
  }

}
