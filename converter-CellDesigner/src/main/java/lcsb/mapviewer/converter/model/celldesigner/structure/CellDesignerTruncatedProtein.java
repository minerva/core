package lcsb.mapviewer.converter.model.celldesigner.structure;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.TruncatedProtein;

/**
 * Class representing CellDesigner {@link TruncatedProtein}.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerTruncatedProtein extends CellDesignerProtein<TruncatedProtein> {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public CellDesignerTruncatedProtein() {
    super();
  }

  /**
   * Constructor that creates a copy of species.
   * 
   * @param species
   *          original species
   */
  public CellDesignerTruncatedProtein(final CellDesignerSpecies<?> species) {
    super(species);
  }

  @Override
  public CellDesignerTruncatedProtein copy() {
    if (this.getClass().equals(CellDesignerTruncatedProtein.class)) {
      return new CellDesignerTruncatedProtein(this);
    } else {
      throw new NotImplementedException("Copy method for " + this.getClass() + " class not implemented");
    }
  }

  @Override
  public TruncatedProtein createModelElement(final String aliasId) {
    TruncatedProtein result = new TruncatedProtein(aliasId);
    super.setModelObjectFields(result);

    return result;
  }

}
