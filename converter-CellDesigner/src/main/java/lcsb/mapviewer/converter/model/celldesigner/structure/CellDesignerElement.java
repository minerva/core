package lcsb.mapviewer.converter.model.celldesigner.structure;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.species.Element;

/**
 * Generic CellDesigner element map. It is a root object in inheritance three
 * for every element ({@link CellDesignerSpecies} or
 * {@link CellDesignerCompartment} ).
 * 
 * @param <T>
 *          type of the {@link Element} in the model that is describe by this
 *          CellDesigner class
 * @author Piotr Gawron
 * 
 */
public abstract class CellDesignerElement<T extends Element> implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Maximum length of the valid synonym name.
   */
  private static final int MAX_SYNONYM_LENGTH = 255;

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Notes describing this element.
   */
  private String notes;

  /**
   * Symbol of the element.
   */
  private String symbol;

  /**
   * Full name of the element.
   */
  private String fullName;

  /**
   * Abbreviation associated with the element.
   */
  private String abbreviation;

  /**
   * Formula associated with the element.
   */
  private String formula;

  /**
   * Short name of the element.
   */
  private String name = "";

  /**
   * Lists of all synonyms used for describing this element.
   */
  private List<String> synonyms = new ArrayList<>();

  /**
   * List of former symbols used to describe this element.
   */
  private List<String> formerSymbols = new ArrayList<>();

  /**
   * Where this element lies on.
   */
  private CellDesignerCompartment parent;

  /**
   * In which complex this element is located.
   */
  private CellDesignerComplexSpecies complex;

  /**
   * Set of miriam annotations for this element.
   */
  private Set<MiriamData> miriamData = new HashSet<>();

  /**
   * Default constructor that initialize the data with the information given in
   * the parameter.
   * 
   * @param element
   *          original object from which data should be initialized
   */
  protected CellDesignerElement(final CellDesignerElement<?> element) {
    this.parent = element.parent;
    this.complex = element.complex;
    this.notes = element.notes;
    this.symbol = element.symbol;
    this.fullName = element.fullName;
    this.name = element.getName();
    this.getSynonyms().addAll(element.getSynonyms());
    this.getFormerSymbols().addAll(element.getFormerSymbols());

    for (final MiriamData md : element.getMiriamData()) {
      addMiriamData(new MiriamData(md));
    }
    this.abbreviation = element.getAbbreviation();
    this.formula = element.getFormula();
  }

  /**
   * Default constructor.
   */
  protected CellDesignerElement() {
    this.notes = "";
  }

  /**
   * Adds set of {@link MiriamData} to the object.
   * 
   * @param miriamData
   *          objects to be added
   */
  public void addMiriamData(final Collection<MiriamData> miriamData) {
    for (final MiriamData md : miriamData) {
      addMiriamData(md);
    }
  }

  /**
   * Adds {@link MiriamData} to the element.
   *
   * @param md
   *          object to be added
   */
  public void addMiriamData(final MiriamData md) {
    if (this.miriamData.contains(md)) {
      logger.warn("Miriam data (" + md.getDataType() + ": " + md.getResource() + ") for " + getElementId()
          + " already exists. Ignoring...");
    } else {
      this.miriamData.add(md);
    }

  }

  /**
   * Returns identifier of the species.
   * 
   * @return identifier of the species
   */
  public abstract String getElementId();

  /**
   * Sets element identifier.
   *
   * @param elementId
   *          element identifier
   */
  public abstract void setElementId(final String elementId);

  /**
   * Makes a copy of the element.
   * 
   * @return copy of the object
   */
  public abstract CellDesignerElement<T> copy();

  /**
   * Adds synonyms to the element.
   * 
   * @param synonyms
   *          list of synonyms to be added
   */
  public void addSynonyms(final List<String> synonyms) {
    for (final String string : synonyms) {
      if (string.length() > MAX_SYNONYM_LENGTH) {
        String message = " <Synonym too long. Only " + MAX_SYNONYM_LENGTH + " characters are allowed>";
        this.getSynonyms().add(string.substring(0, MAX_SYNONYM_LENGTH - message.length()) + message);
      } else {
        this.getSynonyms().add(string);
      }
    }

  }

  /**
   * Returns notes about the object.
   * 
   * @return notes about the object
   */
  public String getNotes() {
    return notes;
  }

  /**
   * Sets notes about the object.
   *
   * @param notes
   *          new notes
   */
  public void setNotes(final String notes) {
    if (notes != null) {
      if (notes.contains("</html>")) {
        throw new InvalidArgumentException("Notes cannot contain html tags...");
      }
    }
    this.notes = notes;
  }

  /**
   * Returns the symbol of the element.
   *
   * @return the symbol of the element
   */
  public String getSymbol() {
    return symbol;
  }

  /**
   * Sets symbol of the element.
   *
   * @param symbol
   *          new symbol
   */
  public void setSymbol(final String symbol) {
    this.symbol = symbol;
  }

  /**
   * @return the fullName
   * @see #fullName
   */
  public String getFullName() {
    return fullName;
  }

  /**
   * @param fullName
   *          the fullName to set
   * @see #fullName
   */
  public void setFullName(final String fullName) {
    this.fullName = fullName;
  }

  /**
   * Returns the name of the object.
   *
   * @return name of the object
   *
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * Get list of synonyms.
   *
   * @return list of synonyms
   */
  public List<String> getSynonyms() {
    return synonyms;
  }

  /**
   * Sets list of synonyms to the element.
   *
   * @param synonyms
   *          new list
   */
  public void setSynonyms(final List<String> synonyms) {
    this.synonyms = synonyms;
  }

  /**
   * @return the formerSymbols
   * @see #formerSymbols
   */
  public List<String> getFormerSymbols() {
    return formerSymbols;
  }

  /**
   * @param formerSymbols
   *          the formerSymbols to set
   * @see #formerSymbols
   */
  public void setFormerSymbols(final List<String> formerSymbols) {
    this.formerSymbols = formerSymbols;
  }

  /**
   * @return the parent
   * @see #parent
   */
  public CellDesignerCompartment getParent() {
    return parent;
  }

  /**
   * @param parent
   *          the parent to set
   * @see #parent
   */
  public void setParent(final CellDesignerCompartment parent) {
    this.parent = parent;
  }

  /**
   * @return the complex
   * @see #complex
   */
  public CellDesignerComplexSpecies getComplex() {
    return complex;
  }

  /**
   * @param complex
   *          the complex to set
   * @see #complex
   */
  public void setComplex(final CellDesignerComplexSpecies complex) {
    this.complex = complex;
  }

  /**
   * Returns list of {@link MiriamData annotations} for the object.
   *
   * @return list of {@link MiriamData annotations} for the object
   */
  public Set<MiriamData> getMiriamData() {
    return miriamData;
  }

  /**
   * Returns the abbreviation.
   *
   * @return the abbreviation
   */
  public String getAbbreviation() {
    return abbreviation;
  }

  /**
   * Sets abbreviation.
   *
   * @param abbreviation
   *          new abbreviation
   */
  public void setAbbreviation(final String abbreviation) {
    this.abbreviation = abbreviation;
  }

  /**
   * Returns the formula.
   *
   * @return the formula
   */
  public String getFormula() {
    return formula;
  }

  /**
   * Sets formula.
   *
   * @param formula
   *          new formula
   */
  public void setFormula(final String formula) {
    this.formula = formula;
  }

  /**
   * Adds synonym.
   *
   * @param synonym
   *          synonym to add
   */
  public void addSynonym(final String synonym) {
    this.synonyms.add(synonym);
  }

  /**
   * Adds former symbol.
   *
   * @param formerSymbol
   *          former symbol to add
   */
  public void addFormerSymbol(final String formerSymbol) {
    this.formerSymbols.add(formerSymbol);
  }

  /**
   * Creates model object from this CellDesigner structure.
   * 
   * @return model object from this CellDesigner structure
   */
  public T createModelElement() {
    return createModelElement(null);
  }

  /**
   * Creates model object from this CellDesigner structure.
   * 
   * @param modelElementId
   *          identifier of new model object
   * @return model object from this CellDesigner structure
   */
  public abstract T createModelElement(final String modelElementId);

  /**
   * Sets values from this cell designer structure into model object.
   * 
   * @param result
   *          object to which values should be assigned
   */
  protected void setModelObjectFields(final T result) {
    result.setNotes(notes);
    result.setSymbol(symbol);
    result.setFullName(fullName);
    result.setAbbreviation(abbreviation);
    result.setFormula(formula);
    result.setName(name);
    result.addSynonyms(synonyms);
    result.addFormerSymbols(formerSymbols);
    result.addMiriamData(miriamData);
  }
}
