package lcsb.mapviewer.converter.model.celldesigner.structure.fields;

import lcsb.mapviewer.common.exception.InvalidArgumentException;

public enum ModificationType {
  BINDING_REGION(null),
  CODING_REGION("CodingRegion"),
  PROTEIN_BINDING_DOMAIN("proteinBindingDomain"),
  RESIDUE(null),
  REGULATORY_REGION("RegulatoryRegion"),
  TRANSCRIPTION_SITE_RIGHT("transcriptionStartingSiteR"),
  TRANSCRIPTION_SITE_LEFT("transcriptionStartingSiteL"),
  MODIFICATION_SITE("Modification Site");

  private String cellDesignerName;

  ModificationType(final String cellDesignerName) {
    this.cellDesignerName = cellDesignerName;
  }

  public static ModificationType getByCellDesignerName(final String name) {
    for (final ModificationType type : ModificationType.values()) {
      if (type.getCellDesignerName() != null && type.getCellDesignerName().equals(name)) {
        return type;
      }
    }
    throw new InvalidArgumentException("Unknown CellDesigner name: " + name);
  }

  public String getCellDesignerName() {
    return cellDesignerName;
  }
}
