package lcsb.mapviewer.converter.model.celldesigner;

import java.awt.Color;
import java.awt.geom.Dimension2D;
import java.awt.geom.Point2D;
import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.geometry.DoubleDimension;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.SingleLine;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.View;
import lcsb.mapviewer.model.map.MiriamRelationType;

/**
 * Class with parsers for common CellDesigner objects.
 * 
 * @author Piotr Gawron
 * 
 */
public class CommonXmlParser {

  public static final Set<MiriamRelationType> RELATION_TYPES_SUPPORTED_BY_CELL_DESIGNER;

  static {
    Set<MiriamRelationType> types = new HashSet<>();
    types.addAll(Arrays.asList(MiriamRelationType.values()));
    types.remove(MiriamRelationType.BQ_MODEL_IS_DERIVED_FROM);
    types.remove(MiriamRelationType.BQ_BIOL_HAS_PROPERTY);
    types.remove(MiriamRelationType.BQ_BIOL_IS_PROPERTY_OF);
    types.remove(MiriamRelationType.BQ_MODEL_IS_INSTANCE_OF);
    types.remove(MiriamRelationType.BQ_MODEL_HAS_INSTANCE);
    types.remove(MiriamRelationType.BQ_BIOL_HAS_TAXON);
    RELATION_TYPES_SUPPORTED_BY_CELL_DESIGNER = Collections.unmodifiableSet(types);
  }


  private DocumentBuilderFactory dbFactory;
  private DocumentBuilder documentBuilder;

  public CommonXmlParser() {
    try {
      dbFactory = DocumentBuilderFactory.newInstance();
      documentBuilder = dbFactory.newDocumentBuilder();
      documentBuilder.setErrorHandler(new ErrorHandler() {
        @Override
        public void warning(final SAXParseException e) throws SAXException {
          ;
        }

        @Override
        public void fatalError(final SAXParseException e) throws SAXException {
          throw e;
        }

        @Override
        public void error(final SAXParseException e) throws SAXException {
          throw e;
        }
      });

    } catch (final ParserConfigurationException e) {
      throw new InvalidStateException(e);
    }
  }

  /**
   * Parse xml representation of position into Poin2D object.
   * 
   * @param node
   *          xml node to parse
   * @return Point2D object
   */
  public Point2D getPosition(final Node node) {
    double x = Double.parseDouble(XmlParser.getNodeAttr("x", node));
    double y = Double.parseDouble(XmlParser.getNodeAttr("y", node));
    return new Point2D.Double(x, y);

  }

  /**
   * Parse xml representation of dimension.
   * 
   * @param node
   *          xml node to parse
   * @return dimension (with width and height fields)
   */
  public Dimension2D getDimension(final Node node) {
    double width = Double.parseDouble(XmlParser.getNodeAttr("width", node));
    double height = Double.parseDouble(XmlParser.getNodeAttr("height", node));
    DoubleDimension result = new DoubleDimension(width, height);
    return result;
  }

  /**
   * Parse xml representation of CellDesigner SingleLine.
   * 
   * @param node
   *          xml node to parse
   * @return single line object (with width field)
   */
  public SingleLine getSingleLine(final Node node) {
    SingleLine result = new SingleLine();
    result.setWidth(Double.parseDouble(XmlParser.getNodeAttr("width", node)));
    return result;
  }

  /**
   * Parse xml representation of CellDesigner color.
   * 
   * @param node
   *          xml node to parse
   * @return Color value stored in xml
   */
  public Color getColor(final Node node) {
    String color = XmlParser.getNodeAttr("color", node);
    // cellDesigner has a bug that removes info about alpha when alpha is 0
    // (#1103)
    color = StringUtils.leftPad(color, 8, "0");

    return XmlParser.stringToColor(color);
  }

  /**
   * Parse xml representation of CellDesigner view.
   * 
   * @param node
   *          xml node to parse
   * @return view parsed from xml
   * @see View
   */
  public View getView(final Node node) {
    View result = new View();
    Node tmpNode = XmlParser.getNode("celldesigner:innerPosition", node.getChildNodes());
    if (tmpNode != null) {
      result.setInnerPosition(getPosition(tmpNode));
    }

    tmpNode = XmlParser.getNode("celldesigner:boxSize", node.getChildNodes());
    if (tmpNode != null) {
      result.setBoxSize(getDimension(tmpNode));
    }

    tmpNode = XmlParser.getNode("celldesigner:singleLine", node.getChildNodes());
    if (tmpNode != null) {
      result.setSingleLine(getSingleLine(tmpNode));
    }

    tmpNode = XmlParser.getNode("celldesigner:paint", node.getChildNodes());
    if (tmpNode != null) {
      result.setColor(getColor(tmpNode));
    }
    return result;
  }

  public String getNotesXmlContent(final String notes) {
    String result = notes;
    try {
      ByteArrayInputStream bais = new ByteArrayInputStream(("<p>" + result + "</p>").getBytes());
      documentBuilder.parse(bais);
    } catch (final Exception e) {
      result = XmlParser.escapeXml(result);
    }

    return result;
  }

}
