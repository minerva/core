package lcsb.mapviewer.converter.model.celldesigner.structure;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Unknown;

/**
 * Class representing CellDesigner {@link Unknown} element.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerUnknown extends CellDesignerSpecies<Unknown> {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Constructor that initializes unknown with the data passed in the argument.
   * 
   * @param species
   *          original species used for data initialization
   */
  public CellDesignerUnknown(final CellDesignerSpecies<?> species) {
    super(species);
  }

  /**
   * Default constructor.
   */
  public CellDesignerUnknown() {
  }

  @Override
  public CellDesignerUnknown copy() {
    if (this.getClass() == CellDesignerUnknown.class) {
      return new CellDesignerUnknown(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public Unknown createModelElement(final String aliasId) {
    Unknown result = new Unknown(aliasId);
    super.setModelObjectFields(result);
    return result;
  }

}
