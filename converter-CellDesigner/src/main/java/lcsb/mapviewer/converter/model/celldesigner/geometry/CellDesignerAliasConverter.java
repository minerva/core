package lcsb.mapviewer.converter.model.celldesigner.geometry;

import java.awt.geom.Point2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * This class is designed to obtain CellDesigner specific data from
 * {@link Element}.
 * 
 * @author Piotr Gawron
 * 
 */

public class CellDesignerAliasConverter implements ICellDesignerAliasConverter<Element> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  /**
   * Class helping with transforming objects into meaningful identifiers.
   */
  private static ElementUtils eu = new ElementUtils();
  /**
   * Converter used for operations on the {@link Element} given in constructor.
   */
  @SuppressWarnings("rawtypes")
  private ICellDesignerAliasConverter converter = null;

  /**
   * Default constructor.
   *
   * @param sbgn
   *          Should the converter use SBGN standard
   * @param element
   *          element for which this converter will be used
   */
  public CellDesignerAliasConverter(final Element element, final boolean sbgn) {
    converter = getConverterForAlias(element, sbgn);
  }

  /**
   * Returns a converter for given {@link Element}. If converter doesn't exist
   * exception is thrown.
   *
   * @param element
   *          element for which we are looking for a converter
   * @param sbgn
   *          Should the converter use SBGN standard
   * @return converter that can be applied for the given element
   */
  private ICellDesignerAliasConverter<? extends Element> getConverterForAlias(final Element element, final boolean sbgn) {
    if (element == null) {
      throw new InvalidArgumentException("element cannot be null");
    }
    if (element instanceof Protein) {
      return new ProteinCellDesignerAliasConverter(sbgn);
    } else if (element instanceof Degraded) {
      return new DegradedCellDesignerAliasConverter(sbgn);
    } else if (element instanceof Complex) {
      return new ComplexCellDesignerAliasConverter(sbgn);
    } else if (element instanceof SimpleMolecule) {
      return new SimpleMoleculeCellDesignerAliasConverter(sbgn);
    } else if (element instanceof Drug) {
      return new DrugCellDesignerAliasConverter(sbgn);
    } else if (element instanceof Ion) {
      return new IonCellDesignerAliasConverter(sbgn);
    } else if (element instanceof Phenotype) {
      return new PhenotypeCellDesignerAliasConverter(sbgn);
    } else if (element instanceof Rna) {
      return new RnaCellDesignerAliasConverter(sbgn);
    } else if (element instanceof AntisenseRna) {
      return new AntisenseRnaCellDesignerAliasConverter(sbgn);
    } else if (element instanceof Gene) {
      return new GeneCellDesignerAliasConverter(sbgn);
    } else if (element instanceof Unknown) {
      return new UnknownCellDesignerAliasConverter(sbgn);
    } else {
      throw new NotImplementedException(
          eu.getElementTag(element) + "Unknown converter for class " + element.getClass());
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public CellDesignerAnchor getAnchorForCoordinates(final Element element, final Point2D point) {
    return converter.getAnchorForCoordinates(element, point);
  }

  @SuppressWarnings("unchecked")
  @Override
  public Point2D getPointCoordinates(final Element element, final CellDesignerAnchor anchor) {
    return converter.getPointCoordinates(element, anchor);
  }

  @SuppressWarnings("unchecked")
  @Override
  public Point2D getAnchorPointCoordinates(final Element element, final CellDesignerAnchor anchor, final PolylineData line) {
    return converter.getAnchorPointCoordinates(element, anchor, line);
  }

  @SuppressWarnings("unchecked")
  @Override
  public Point2D getResidueCoordinates(final Element species, final double angle) {
    return converter.getResidueCoordinates(species, angle);
  }

  @SuppressWarnings("unchecked")
  @Override
  public Double getAngleForPoint(final Element element, final Point2D position) {
    return converter.getAngleForPoint(element, position);
  }

  @Override
  public Double getCellDesignerPositionByCoordinates(final ModificationResidue mr) {
    return converter.getCellDesignerPositionByCoordinates(mr);
  }

  @Override
  public Point2D getCoordinatesByPosition(final Element element, final Double pos, final Double width) {
    return converter.getCoordinatesByPosition(element, pos, width);
  }

  @Override
  public Double getCellDesignerSize(final ModificationResidue mr) {
    return converter.getCellDesignerSize(mr);
  }

  @Override
  public Double getWidthBySize(final Element element, final Double size) {
    return converter.getWidthBySize(element, size);
  }

}
