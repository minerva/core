package lcsb.mapviewer.converter.model.celldesigner.geometry;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.common.geometry.LineTransformation;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerEllipseTransformation;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerPolygonTransformation;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerRectangleTransformation;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;

/**
 * This abstract class is an interface for setting up graphical data for alias
 * from CellDesigner specific format into standard layout.
 *
 * @author Piotr Gawron
 */
public abstract class AbstractCellDesignerAliasConverter<T extends Element> implements ICellDesignerAliasConverter<T> {

  /**
   * PI value.
   */
  private static final double PI = Math.PI;
  /**
   * Constant determining angle of the top right corner.
   */
  protected static final double RIGHT_TOP_RESIDUE_MAX_ANGLE = 0.25 * PI;
  /**
   * Constant determining angle of the top left corner.
   */
  protected static final double TOP_RESIDUE_MAX_ANGLE = 0.75 * PI;
  /**
   * Constant determining angle of the bottom left corner.
   */
  protected static final double LEFT_RESIDUE_MAX_ANGLE = 1.25 * PI;
  /**
   * Constant determining angle of the bottom right corner.
   */
  protected static final double BOTTOM_RESIDUE_MAX_ANGLE = 1.75 * PI;
  /**
   * Default class logger.
   */
  private static final Logger logger = LogManager.getLogger();
  /**
   * CellDesigner graphical helper with polygon transformation functions.
   */
  private CellDesignerPolygonTransformation polygonTransformation = new CellDesignerPolygonTransformation();
  /**
   * CellDesigner graphical helper with line transformation functions.
   */
  private LineTransformation lineTransformation = new LineTransformation();
  /**
   * CellDesigner graphical helper with ellipse transformation functions.
   */
  private CellDesignerEllipseTransformation ellipseTransformation = new CellDesignerEllipseTransformation();
  /**
   * CellDesigner graphical helper with rectangle transformation functions.
   */
  private CellDesignerRectangleTransformation rectangleTransformation = new CellDesignerRectangleTransformation();
  /**
   * Should the converter use SBGN standard.
   */
  private boolean sbgn;

  /**
   * Default constructor that prevents from instantiation of the class.
   *
   * @param sbgn Should the converter use SBGN standard
   */
  protected AbstractCellDesignerAliasConverter(final boolean sbgn) {
    this.sbgn = sbgn;
  }

  protected AbstractCellDesignerAliasConverter() {
  }

  @Override
  public CellDesignerAnchor getAnchorForCoordinates(final T alias, final Point2D point) {
    double dist = Double.MAX_VALUE;
    CellDesignerAnchor result = null;
    for (final CellDesignerAnchor anchor : CellDesignerAnchor.values()) {
      double newDist = getPointCoordinates(alias, anchor).distance(point);
      if (newDist < dist) {
        dist = newDist;
        result = anchor;
      }
    }
    // if the distance to all known anchors is too big then assume center
    if (dist > 1) {
      return null;
    }
    return result;
  }

  @Override
  public abstract Point2D getPointCoordinates(final T alias, final CellDesignerAnchor anchor);

  @Override
  public Point2D getAnchorPointCoordinates(final T alias, final CellDesignerAnchor anchor, final PolylineData ld) {
    if (anchor != null && anchor.getAngle() != null) {
      return getPointCoordinates(alias, anchor);
    } else {
      Point2D p1 = ld.getLines().get(0).getP2();
      Point2D p2 = ld.getLines().get(0).getP1();
      double dx = p2.getX() - p1.getX();
      double dy = p2.getY() - p1.getY();
      double angle = Math.atan2(dy, dx);
      return getAnchorPointCoordinates(alias, angle);
    }
  }

  /**
   * This method computes coordinates that should be associated with the angle
   * on the border of the alias.
   *
   * @param alias alias to be investigated
   * @param angle angle on the border of the alias
   * @return coordinates on the border of the alias described by the angle
   */
  protected Point2D getAnchorPointCoordinates(final T alias, final double angle) {
    Point2D result = null;
    if (alias.getWidth() == 0 && alias.getHeight() == 0) {
      result = alias.getCenter();
    } else {
      double dist = Math.max(alias.getWidth(), alias.getHeight()) * 2;
      Point2D startPoint = alias.getCenter();
      double x = startPoint.getX() - Math.cos(angle) * dist;
      double y = startPoint.getY() - Math.sin(angle) * dist;
      Point2D endPoint = new Point2D.Double(x, y);
      Line2D line = new Line2D.Double(startPoint, endPoint);
      result = lineTransformation.getIntersectionWithPathIterator(line, getBoundPathIterator(alias));
      if (result == null) {
        logger.warn("Unknown crossing point: " + line.getP1() + "; " + line.getP2());
        result = alias.getCenter();
      }
    }
    return result;
  }

  /**
   * Returns coordinates on the {@link Species} border for given angle for
   * residues.
   *
   * @param species object on border which the point is looked for
   * @param angle   CellDEsigner specific angle defining coordinates (;/)
   * @return coordinates on the alias border that correspond to the angle
   */
  @Override
  public Point2D getResidueCoordinates(final T species, final double angle) {
    double boundedAngle = angle;
    Point2D result = null;
    if (species.getWidth() == 0 && species.getHeight() == 0) {
      result = species.getCenter();
    } else {
      double x = 0;
      double y = 0;
      while (boundedAngle > 2 * PI) {
        boundedAngle -= 2 * PI;
      }
      while (boundedAngle < 0) {
        boundedAngle += 2 * PI;
      }
      if (boundedAngle < RIGHT_TOP_RESIDUE_MAX_ANGLE) {
        double ratio = 0.5 + boundedAngle / (PI / 2);
        x = species.getX() + species.getWidth();
        y = species.getY() + species.getHeight() * (1 - ratio);
      } else if (boundedAngle < TOP_RESIDUE_MAX_ANGLE) {
        double ratio = (boundedAngle - RIGHT_TOP_RESIDUE_MAX_ANGLE) / (PI / 2);
        y = species.getY();
        x = species.getX() + species.getWidth() * (1 - ratio);
      } else if (boundedAngle < LEFT_RESIDUE_MAX_ANGLE) {
        double ratio = (boundedAngle - TOP_RESIDUE_MAX_ANGLE) / (PI / 2);
        y = species.getY() + species.getHeight() * (ratio);
        x = species.getX();
      } else if (boundedAngle < BOTTOM_RESIDUE_MAX_ANGLE) {
        double ratio = (boundedAngle - LEFT_RESIDUE_MAX_ANGLE) / (PI / 2);
        y = species.getY() + species.getHeight();
        x = species.getX() + species.getWidth() * ratio;
      } else if (boundedAngle <= 2 * PI + Configuration.EPSILON) {
        double ratio = (boundedAngle - BOTTOM_RESIDUE_MAX_ANGLE) / (PI / 2);
        y = species.getY() + species.getHeight() * (1 - ratio);
        x = species.getX() + species.getWidth();
      } else {
        throw new InvalidStateException();
      }
      Point2D center = species.getCenter();
      double correctedAngle = -Math.atan2((y - center.getY()), (x - center.getX()));
      result = getPointCoordinatesOnBorder(species, correctedAngle);
    }
    return result;

  }

  @Override
  public Double getAngleForPoint(final T element, final Point2D position) {
    double result = -1;
    Point2D pointOnBorder = getClosestPositionOnBorder(element, position);

    double epsilon = Configuration.EPSILON;
    double distanceRight = Math.abs(pointOnBorder.getX() - element.getX() - element.getWidth());
    double distanceTop = Math.abs(pointOnBorder.getY() - element.getY());
    double distanceLeft = Math.abs(pointOnBorder.getX() - element.getX());
    double distanceBottom = Math.abs(pointOnBorder.getY() - element.getY() - element.getHeight());

    double minDist = Math.min(Math.min(distanceLeft, distanceRight), Math.min(distanceTop, distanceBottom));

    if (distanceRight == minDist
        && pointOnBorder.getY() - element.getY() - element.getHeight() / 2 < epsilon) {
      double ratio = 1 - (pointOnBorder.getY() - element.getY()) / element.getHeight();
      result = (ratio - 0.5) * (PI / 2);
    } else if (distanceTop == minDist) {
      double ratio = 1 - (pointOnBorder.getX() - element.getX()) / element.getWidth();
      result = ratio * (PI / 2) + RIGHT_TOP_RESIDUE_MAX_ANGLE;
    } else if (distanceLeft == minDist) {
      double ratio = (pointOnBorder.getY() - element.getY()) / element.getHeight();
      result = ratio * (PI / 2) + TOP_RESIDUE_MAX_ANGLE;
    } else if (distanceBottom == minDist) {
      double ratio = (pointOnBorder.getX() - element.getX()) / element.getWidth();
      result = ratio * (PI / 2) + LEFT_RESIDUE_MAX_ANGLE;
    } else if (distanceRight == minDist
        && pointOnBorder.getY() - element.getY() - element.getHeight() < epsilon) {
      double ratio = 1 - (pointOnBorder.getY() - element.getY()) / element.getHeight();
      result = ratio * (PI / 2) + BOTTOM_RESIDUE_MAX_ANGLE;
    } else {
      result = PI / 2;
      logger.warn("Problem with finding angle. Using default");
    }

    return result;
  }

  @Override
  public Double getCellDesignerPositionByCoordinates(final ModificationResidue mr) {
    return null;
  }

  @Override
  public Point2D getCoordinatesByPosition(final Element element, final Double pos, final Double width) {
    throw new NotImplementedException("Not implemented for: " + this.getClass());
  }

  @Override
  public Double getCellDesignerSize(final ModificationResidue mr) {
    throw new NotImplementedException("Not implemented for: " + this.getClass());
  }

  @Override
  public Double getWidthBySize(final Element element, final Double size) {
    throw new NotImplementedException("Not implemented for: " + this.getClass());
  }

  /**
   * This method returns border of the alias as a PathIterator.
   *
   * @param alias object for which we want to find a border
   * @return border of the alias
   */
  protected abstract PathIterator getBoundPathIterator(final T alias);

  /**
   * Checks if anchor is valid for the alias to find a point on the border.
   *
   * @param alias  object to be checked
   * @param anchor anchor point
   * @return true if the conditions are ok (alias size >0 and angle != null)
   */
  protected boolean invalidAnchorPosition(final T alias, final CellDesignerAnchor anchor) {
    if (anchor == null || anchor.getAngle() == null) {
      return true;
    }
    return alias.getWidth() == 0 && alias.getHeight() == 0;
    // TODO handle with one of params equal to 0
  }

  /**
   * @return the polygonTransformation
   */
  protected CellDesignerPolygonTransformation getPolygonTransformation() {
    return polygonTransformation;
  }

  /**
   * @param polygonTransformation the polygonTransformation to set
   */
  protected void setPolygonTransformation(final CellDesignerPolygonTransformation polygonTransformation) {
    this.polygonTransformation = polygonTransformation;
  }

  /**
   * @return the lineTransformation
   */
  protected LineTransformation getLineTransformation() {
    return lineTransformation;
  }

  /**
   * @param lineTransformation the lineTransformation to set
   */
  protected void setLineTransformation(final LineTransformation lineTransformation) {
    this.lineTransformation = lineTransformation;
  }

  /**
   * @return the ellipseTransformation
   */
  protected CellDesignerEllipseTransformation getEllipseTransformation() {
    return ellipseTransformation;
  }

  /**
   * @param ellipseTransformation the ellipseTransformation to set
   */
  protected void setEllipseTransformation(final CellDesignerEllipseTransformation ellipseTransformation) {
    this.ellipseTransformation = ellipseTransformation;
  }

  /**
   * @return the rectangleTransformation
   */
  protected CellDesignerRectangleTransformation getRectangleTransformation() {
    return rectangleTransformation;
  }

  /**
   * @param rectangleTransformation the rectangleTransformation to set
   */
  protected void setRectangleTransformation(final CellDesignerRectangleTransformation rectangleTransformation) {
    this.rectangleTransformation = rectangleTransformation;
  }

  /**
   * @return the sbgn
   * @see #sbgn
   */
  protected boolean isSbgn() {
    return sbgn;
  }

  /**
   * Returns coordinates on the {@link Element} border for given angle.
   *
   * @param elemnt {@link Element} on border which the point is looked for
   * @param angle  angle between X axis center point of {@link Species} and point
   *               that we are looking for
   * @return coordinates on the {@link Species} border that correspond to the angle
   */
  protected Point2D getPointCoordinatesOnBorder(final T elemnt, final double angle) {
    Point2D result = null;
    if (elemnt.getWidth() == 0 && elemnt.getHeight() == 0) {
      result = elemnt.getCenter();
    } else {
      double dist = Math.max(elemnt.getWidth(), elemnt.getHeight()) * 2;
      Point2D startPoint = elemnt.getCenter();
      double x = startPoint.getX() + Math.cos(angle) * dist;
      double y = startPoint.getY() - Math.sin(angle) * dist;
      Point2D endPoint = new Point2D.Double(x, y);
      Line2D line = new Line2D.Double(startPoint, endPoint);
      result = lineTransformation.getIntersectionWithPathIterator(line, getBoundPathIterator(elemnt));
    }
    return result;

  }

  private Point2D getClosestPositionOnBorder(final T element, final Point2D position) {
    double correctedAngle = -Math.atan2((position.getY() - element.getCenterY()),
        (position.getX() - element.getCenterX()));

    double dist = Math.max(element.getWidth(), element.getHeight()) * 2;
    Point2D startPoint = element.getCenter();
    double x = startPoint.getX() + Math.cos(correctedAngle) * dist;
    double y = startPoint.getY() - Math.sin(correctedAngle) * dist;
    Point2D endPoint = new Point2D.Double(x, y);
    Line2D line = new Line2D.Double(startPoint, endPoint);

    GeneralPath path = new GeneralPath(GeneralPath.WIND_EVEN_ODD);

    path.moveTo(element.getX(), element.getY());
    path.lineTo(element.getX(), element.getY() + element.getHeight());
    path.lineTo(element.getX() + element.getWidth(), element.getY() + element.getHeight());
    path.lineTo(element.getX() + element.getWidth(), element.getY());
    path.closePath();

    return lineTransformation.getIntersectionWithPathIterator(line, path.getPathIterator(new AffineTransform()));
  }

}
