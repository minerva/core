package lcsb.mapviewer.converter.model.celldesigner.structure.fields;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.CellDesignerAliasConverter;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerSpecies;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;
import lcsb.mapviewer.model.map.species.field.RegulatoryRegion;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.io.Serializable;

/**
 * This class represent modification residue in a Species.
 *
 * @author Piotr Gawron
 */
public class CellDesignerModificationResidue implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  private static final Logger logger = LogManager.getLogger();

  /**
   * Identifier of the modification. Must be unique in single map model.
   */
  private String idModificationResidue = "";

  /**
   * Name of the modification.
   */
  private String name = "";

  private Boolean active = null;

  private ModificationType modificationType;

  /**
   * Position on the species in graphic representation.
   */
  private Double pos;

  /**
   * Some strange parameter in CellDesigner. No idea what is it for.
   */
  private String side = "";

  /**
   * State in which this modification is.
   */
  private ModificationState state = null;

  /**
   * Where this modification is located (on which side of the border).
   */
  private Double angle = null;

  /**
   * How big is this modification (used only for some types of the
   * modification).
   */
  private Double size = null;

  /**
   * Species to which this modification belong to.
   */
  private CellDesignerSpecies<?> species;

  /**
   * Default constructor.
   */
  public CellDesignerModificationResidue() {
  }

  /**
   * Constructor that initialize object with the data taken from the parameter.
   *
   * @param mr original object from which data is taken
   */
  public CellDesignerModificationResidue(final CellDesignerModificationResidue mr) {
    this.idModificationResidue = mr.idModificationResidue;
    this.name = mr.name;
    this.angle = mr.angle;
    this.active = mr.active;
    this.size = mr.size;
    this.side = mr.side;
    this.pos = mr.pos;
    this.state = mr.state;
    this.modificationType = mr.modificationType;
  }

  /**
   * Constructor that creates object from model representation of modification.
   *
   * @param mr model representation of {@link ModificationResidue}
   */
  public CellDesignerModificationResidue(final ModificationResidue mr) {
    final CellDesignerAliasConverter converter = new CellDesignerAliasConverter(mr.getSpecies(), false);
    this.idModificationResidue = mr.getIdModificationResidue();
    this.name = mr.getName();
    this.angle = converter.getAngleForPoint(mr.getSpecies(), mr.getCenter());
    this.pos = converter.getCellDesignerPositionByCoordinates(mr);
    if (mr instanceof Residue) {
      this.modificationType = ModificationType.RESIDUE;
      this.state = ((Residue) mr).getState();
    } else if (mr instanceof ModificationSite) {
      this.modificationType = ModificationType.MODIFICATION_SITE;
      this.state = ((ModificationSite) mr).getState();
    } else if (mr instanceof CodingRegion) {
      this.size = converter.getCellDesignerSize(mr);
      this.modificationType = ModificationType.CODING_REGION;
    } else if (mr instanceof ProteinBindingDomain) {
      this.size = converter.getCellDesignerSize(mr);
      this.modificationType = ModificationType.PROTEIN_BINDING_DOMAIN;
    } else if (mr instanceof RegulatoryRegion) {
      this.size = converter.getCellDesignerSize(mr);
      this.modificationType = ModificationType.REGULATORY_REGION;
    } else if (mr instanceof TranscriptionSite) {
      final TranscriptionSite transcriptionSite = (TranscriptionSite) mr;
      this.size = converter.getCellDesignerSize(mr);
      this.active = transcriptionSite.getActive();
      if (transcriptionSite.getDirection().equals("LEFT")) {
        this.modificationType = ModificationType.TRANSCRIPTION_SITE_LEFT;
      } else {
        this.modificationType = ModificationType.TRANSCRIPTION_SITE_RIGHT;
      }
    } else if (mr instanceof BindingRegion) {
      this.modificationType = ModificationType.BINDING_REGION;
      if (Math.abs(mr.getCenter().getX() - mr.getSpecies().getX()) < Configuration.EPSILON) {
        this.size = mr.getHeight() / mr.getSpecies().getHeight();
      } else if (Math
          .abs(mr.getCenter().getX() - mr.getSpecies().getX() - mr.getSpecies().getWidth()) < Configuration.EPSILON) {
        this.size = mr.getHeight() / mr.getSpecies().getHeight();
      } else {
        this.size = mr.getWidth() / mr.getSpecies().getWidth();
      }

    } else {
      throw new InvalidArgumentException("Unknown modification type: " + mr.getClass());
    }
  }

  /**
   * Default constructor.
   *
   * @param id identifier of the modification residue
   */
  public CellDesignerModificationResidue(final String id) {
    this.idModificationResidue = id;
  }

  /**
   * Updates fields in the object with the data given in the parameter
   * modification.
   *
   * @param mr modification residue from which data will be used to update
   */
  public void update(final CellDesignerModificationResidue mr) {
    if (mr.getName() != null && !mr.getName().isEmpty()) {
      this.name = mr.name;
    }
    if (mr.getAngle() != null) {
      this.angle = mr.angle;
    }
    if (mr.getActive() != null) {
      this.active = mr.active;
    }
    if (mr.getSize() != null) {
      this.size = mr.size;
    }
    if (mr.getSide() != null && !mr.getSide().isEmpty()) {
      this.side = mr.side;
    }
    if (mr.getState() != null) {
      this.state = mr.state;
    }
    if (mr.getPos() != null) {
      this.setPos(mr.getPos());
    }
    if (mr.getModificationType() != null) {
      this.setModificationType(mr.modificationType);
    }
  }

  @Override
  public String toString() {
    return getIdModificationResidue() + "," + getName() + "," + getState() + "," + getAngle() + "," + getSize()
        + getPos() + "," + "," + getSide() + ",";
  }

  /**
   * Creates copy of the object.
   *
   * @return copy of the object.
   */
  public CellDesignerModificationResidue copy() {
    if (this.getClass() == CellDesignerModificationResidue.class) {
      return new CellDesignerModificationResidue(this);
    } else {
      throw new NotImplementedException("Method copy() should be overridden in class " + this.getClass());
    }
  }

  /**
   * @return the id
   * @see #idModificationResidue
   */
  public String getIdModificationResidue() {
    return idModificationResidue;
  }

  /**
   * @param idModificationResidue the id to set
   * @see #idModificationResidue
   */
  public void setIdModificationResidue(final String idModificationResidue) {
    this.idModificationResidue = idModificationResidue;
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   * @see #name
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * @return the side
   * @see #side
   */
  public String getSide() {
    return side;
  }

  /**
   * @param side the side to set
   * @see #side
   */
  public void setSide(final String side) {
    this.side = side;
  }

  /**
   * @return the state
   * @see #state
   */
  public ModificationState getState() {
    return state;
  }

  /**
   * @param state the state to set
   * @see #state
   */
  public void setState(final ModificationState state) {
    this.state = state;
  }

  /**
   * @return the angle
   * @see #angle
   */
  public Double getAngle() {
    return angle;
  }

  /**
   * Sets {@link #angle} .
   *
   * @param text angle in text format
   */
  public void setAngle(final String text) {
    try {
      if (text != null && !text.isEmpty()) {
        angle = Double.parseDouble(text);
      } else {
        angle = null;
      }
    } catch (final NumberFormatException e) {
      throw new InvalidArgumentException("Invalid angle: " + text, e);
    }

  }

  /**
   * @param angle the angle to set
   * @see #angle
   */
  public void setAngle(final Double angle) {
    this.angle = angle;
  }

  /**
   * @return the size
   * @see #size
   */
  public Double getSize() {
    return size;
  }

  /**
   * Sets {@link #size}.
   *
   * @param text size in text format.
   */
  public void setSize(final String text) {
    try {
      if (text != null && !text.isEmpty()) {
        size = Double.parseDouble(text);
      } else {
        size = null;
      }
    } catch (final NumberFormatException e) {
      throw new InvalidArgumentException("Invalid size: " + text, e);
    }
  }

  /**
   * @param size the size to set
   * @see #size
   */
  public void setSize(final Double size) {
    this.size = size;
  }

  /**
   * @return the species
   * @see #species
   */
  public CellDesignerSpecies<?> getSpecies() {
    return species;
  }

  /**
   * @param species the species to set
   * @see #species
   */
  public void setSpecies(final CellDesignerSpecies<?> species) {
    this.species = species;
  }

  /**
   * Creates model representation of {@link ModificationResidue}.
   *
   * @return {@link ModificationResidue} representing this object in a model
   */
  public ModificationResidue createModificationResidue(final Element element) {
    final CellDesignerAliasConverter converter = new CellDesignerAliasConverter(element, false);

    if (modificationType == null) {
      throw new InvalidArgumentException("No type information for modification: " + idModificationResidue);
    } else if (modificationType.equals(ModificationType.RESIDUE)) {
      return createResidue(element, converter);
    } else if (modificationType.equals(ModificationType.BINDING_REGION)) {
      return createBindingRegion(element, converter);
    } else if (modificationType.equals(ModificationType.MODIFICATION_SITE)) {
      return createModificationSite(element, converter);
    } else if (modificationType.equals(ModificationType.CODING_REGION)) {
      return createCodingRegion(element, converter);
    } else if (modificationType.equals(ModificationType.PROTEIN_BINDING_DOMAIN)) {
      return createProteinBindingDomain(element, converter);
    } else if (modificationType.equals(ModificationType.REGULATORY_REGION)) {
      return createRegulatoryRegion(element, converter);
    } else if (modificationType.equals(ModificationType.TRANSCRIPTION_SITE_LEFT)
        || modificationType.equals(ModificationType.TRANSCRIPTION_SITE_RIGHT)) {
      return createTranscriptionSite(element, converter);
    } else {
      throw new InvalidArgumentException("Unknown modification type: " + modificationType);
    }

  }

  private ProteinBindingDomain createProteinBindingDomain(final Element element, final CellDesignerAliasConverter converter) {
    final ProteinBindingDomain result = new ProteinBindingDomain();
    result.setWidth(converter.getWidthBySize(element, size));
    result.setIdModificationResidue(idModificationResidue);
    result.setName(name);
    final Point2D position = converter.getCoordinatesByPosition(element, pos, result.getWidth());
    result.setPosition(position.getX() - result.getWidth() / 2, position.getY() - result.getHeight() / 2);
    result.setNameX(result.getX());
    result.setNameY(result.getY());
    result.setNameHeight(result.getHeight());
    result.setNameWidth(result.getWidth());
    result.setBorderColor(element.getBorderColor());
    return result;
  }

  private BindingRegion createBindingRegion(final Element element, final CellDesignerAliasConverter converter) {
    final BindingRegion result = new BindingRegion();
    result.setIdModificationResidue(idModificationResidue);
    result.setName(name);
    final Point2D point = converter.getResidueCoordinates(element, angle);
    if (Math.abs(point.getX() - element.getX()) < Configuration.EPSILON) {
      result.setHeight(element.getHeight() * size);
    } else if (Math.abs(point.getX() - element.getX() - element.getWidth()) < Configuration.EPSILON) {
      result.setHeight(element.getHeight() * size);
    } else {
      result.setWidth(element.getWidth() * size);
    }
    result.setNameHeight(result.getHeight());
    result.setNameWidth(result.getWidth());

    result.setPosition(point.getX() - result.getWidth() / 2, point.getY() - result.getHeight() / 2);
    result.setNameX(result.getX());
    result.setNameY(result.getY());
    result.setBorderColor(element.getBorderColor());
    return result;
  }

  private CodingRegion createCodingRegion(final Element element, final CellDesignerAliasConverter converter) {
    final CodingRegion result = new CodingRegion();
    result.setWidth(converter.getWidthBySize(element, size));
    result.setIdModificationResidue(idModificationResidue);
    result.setName(name);
    if (pos == null) {
      pos = angle;
    }
    final Point2D point = converter.getCoordinatesByPosition(element, pos, result.getWidth());
    result.setPosition(point.getX() - result.getWidth() / 2, point.getY() - result.getHeight() / 2);
    result.setNameX(result.getX());
    result.setNameY(result.getY());
    result.setNameHeight(result.getHeight());
    result.setNameWidth(result.getWidth());
    result.setBorderColor(element.getBorderColor());
    return result;
  }

  private RegulatoryRegion createRegulatoryRegion(final Element element, final CellDesignerAliasConverter converter) {
    final RegulatoryRegion result = new RegulatoryRegion();
    result.setWidth(converter.getWidthBySize(element, size));
    result.setIdModificationResidue(idModificationResidue);
    result.setName(name);
    final Point2D point = converter.getCoordinatesByPosition(element, angle, result.getWidth());
    result.setPosition(point.getX() - result.getWidth() / 2, point.getY() - result.getHeight() / 2);
    result.setNameX(result.getX());
    result.setNameY(result.getY());
    result.setNameWidth(result.getWidth());
    result.setNameHeight(result.getHeight());
    result.setBorderColor(element.getBorderColor());
    return result;
  }

  private TranscriptionSite createTranscriptionSite(final Element element, final CellDesignerAliasConverter converter) {
    final TranscriptionSite result = new TranscriptionSite();
    result.setWidth(converter.getWidthBySize(element, size));
    result.setIdModificationResidue(idModificationResidue);
    result.setName(name);
    result.setActive(active);

    final Point2D point = converter.getCoordinatesByPosition(element, angle, result.getWidth());
    result.setPosition(point.getX() - result.getWidth() / 2, point.getY() - result.getHeight());
    result.setNameX(result.getX());
    result.setNameY(result.getY() - 12);
    result.setNameHeight(result.getHeight());
    result.setNameWidth(result.getWidth());
    result.setNameVerticalAlign(VerticalAlign.TOP);
    if (modificationType.equals(ModificationType.TRANSCRIPTION_SITE_LEFT)) {
      result.setNameHorizontalAlign(HorizontalAlign.LEFT);
      result.setDirection("LEFT");
    } else {
      result.setNameHorizontalAlign(HorizontalAlign.RIGHT);
      result.setDirection("RIGHT");
    }
    result.setBorderColor(element.getBorderColor());
    if (result.getActive() != null) {
      if (result.getActive()) {
        result.setBorderColor(Color.RED);
      } else {
        result.setBorderColor(Color.BLACK);
      }
    }
    result.setFillColor(result.getBorderColor());
    return result;
  }

  private ModificationSite createModificationSite(final Element element, final CellDesignerAliasConverter converter) {
    final ModificationSite result = new ModificationSite();
    result.setState(this.getState());
    result.setIdModificationResidue(this.getIdModificationResidue());
    result.setName(this.getName());
    if (angle == null) {
      angle = pos;
    }
    if (angle == null) {
      logger.warn("Angle is not defined using 0 as default");
      angle = 0.0;
    }
    final Point2D point = converter.getCoordinatesByPosition(element, angle, result.getWidth());
    result.setPosition(point.getX() - result.getWidth() / 2, point.getY() - result.getHeight());
    result.setNameX(result.getX());
    result.setNameY(result.getY());
    result.setBorderColor(element.getBorderColor());
    return result;
  }

  private Residue createResidue(final Element element, final CellDesignerAliasConverter converter) {
    final Residue result = new Residue();
    result.setState(this.getState());
    result.setIdModificationResidue(this.getIdModificationResidue());
    result.setName(this.getName());
    if (angle == null) {
      logger.warn("Angle is not defined using 0 as default");
      angle = 0.0;
    }
    final Point2D point = converter.getResidueCoordinates(element, angle);
    result.setPosition(point.getX() - result.getWidth() / 2, point.getY() - result.getHeight() / 2);
    result.setNameX(result.getX());
    result.setNameY(result.getY());
    result.setBorderColor(element.getBorderColor());
    return result;
  }

  public ModificationType getModificationType() {
    return modificationType;
  }

  public void setModificationType(final ModificationType modificationType) {
    this.modificationType = modificationType;
  }

  /**
   * @return the pos
   * @see #pos
   */
  public Double getPos() {
    return pos;
  }

  /**
   * Sets position from the string.
   *
   * @param text position to parse and set
   * @see #pos
   */
  public void setPos(final String text) {
    try {
      pos = Double.parseDouble(text);
    } catch (final NumberFormatException e) {
      throw new InvalidArgumentException("Invalid pos: " + text, e);
    }
  }

  /**
   * @param pos the pos to set
   * @see #pos
   */
  public void setPos(final Double pos) {
    this.pos = pos;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(final Boolean active) {
    this.active = active;
  }

  public void setActive(final String text) {
    if (text == null) {
      this.active = null;
    } else {
      this.active = "true".equalsIgnoreCase(text);
    }
  }

}
