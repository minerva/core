package lcsb.mapviewer.converter.model.celldesigner.species;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.species.field.ModificationState;

public enum ModificationStateMapping {
  PHOSPHORYLATED(ModificationState.PHOSPHORYLATED, "phosphorylated"),
  ACETYLATED(ModificationState.ACETYLATED, "acetylated"),
  UBIQUITINATED(ModificationState.UBIQUITINATED, "ubiquitinated"),
  METHYLATED(ModificationState.METHYLATED, "methylated"),
  HYDROXYLATED(ModificationState.HYDROXYLATED, "hydroxylated"),
  MYRISTOYLATED(ModificationState.MYRISTOYLATED, "myristoylated"),
  SULFATED(ModificationState.SULFATED, "sulfated"),
  PRENYLATED(ModificationState.PRENYLATED, "prenylated"),
  GLYCOSYLATED(ModificationState.GLYCOSYLATED, "glycosylated"),
  PALMYTOYLATED(ModificationState.PALMYTOYLATED, "palmytoylated"),
  UNKNOWN(ModificationState.UNKNOWN, "unknown"),
  EMPTY(ModificationState.EMPTY, "empty"),
  PROTONATED(ModificationState.PROTONATED, "protonated"),
  DONT_CARE(ModificationState.DONT_CARE, "don't care");

  private ModificationState state;
  private String text;

  private ModificationStateMapping(final ModificationState modificationState, final String text) {
    this.state = modificationState;
    this.text = text;
  }

  public static ModificationState getStateByName(final String name) {
    for (ModificationStateMapping mapping : ModificationStateMapping.values()) {
      if (mapping.text.equalsIgnoreCase(name)) {
        return mapping.state;
      }
    }
    return null;
  }

  public static String getStateName(final ModificationState state) {
    for (ModificationStateMapping mapping : ModificationStateMapping.values()) {
      if (mapping.state.equals(state)) {
        return mapping.text;
      }
    }
    throw new InvalidArgumentException("Unknown state: " + state);
  }
}
