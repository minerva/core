package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import lcsb.mapviewer.common.geometry.PointTransformation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.geom.Point2D;

/**
 * Class for basic point transformations.
 *
 * @author Piotr Gawron
 */
public class CellDesignerPointTransformation extends PointTransformation {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static final Logger logger = LogManager.getLogger();

  /**
   * This method transform coordinates of pointP in CellDesigner format (base:
   * pointA, pointB, pointC) into normal x,y coordinates.
   * <p>
   * In CellDesigner some points are in different base consisted from three
   * points. In this base vector between pointA and pointC is 1 unit on X axis
   * (in normal world); and vector between pointB and pointC is 1 unit on Y
   * axis.
   * </p>
   *
   * @param pointA central point of CellDesigner base
   * @param pointB first point of CellDesigner base
   * @param pointC second point of CellDesigner base
   * @param pointP point to be transformed into normal coordinates
   * @return standard x,y coordinate
   */
  public Point2D getCoordinatesInNormalBase(final Point2D pointA,
                                            final Point2D pointB,
                                            final Point2D pointC,
                                            final Point2D pointP) {

    double pointCx = pointC.getX();
    double pointCy = pointC.getY();

    if (arePointsCollinear(pointA, pointB, pointC)) {
      pointCx += 1;
      pointCy += 1;
    }

    double dx1 = pointA.getX() - pointCx;
    double dy1 = pointA.getY() - pointCy;

    double dx2 = pointB.getX() - pointCx;
    double dy2 = pointB.getY() - pointCy;

    double x = pointCx + dx1 * pointP.getX() + dx2 * pointP.getY();
    double y = pointCy + dy1 * pointP.getX() + dy2 * pointP.getY();

    return new Point2D.Double(x, y);
  }

  /**
   * This method transform coordinates of pointP in x,y coordinates into
   * celldesigner format (base: pointA, pointB, pointC)
   * <p>
   * In CellDesigner some points are in different base consisted from three
   * points. In this base vector between pointA and pointC is 1 unit on X axis
   * (in normal world); and vector between pointB and pointC is 1 unit on Y
   * axis.
   * </p>
   *
   * @param pointA central point of CellDesigner base
   * @param pointB first point of CellDesigner base
   * @param pointC second point of CellDesigner base
   * @param pointO point to be transformed
   * @return point in CellDesigner base
   */
  public Point2D getCoordinatesInCellDesignerBase(final Point2D pointA,
                                                  final Point2D pointB,
                                                  final Point2D pointC,
                                                  final Point2D pointO) {
    double pointCx = pointC.getX();
    double pointCy = pointC.getY();

    if (arePointsCollinear(pointA, pointB, pointC)) {
      pointCx += 1;
      pointCy += 1;
    }

    double dx1 = pointA.getX() - pointCx;
    double dy1 = pointA.getY() - pointCy;

    double dx2 = pointB.getX() - pointCx;
    double dy2 = pointB.getY() - pointCy;

    double y = (dy1 * (pointO.getX() - pointCx) + dx1 * (pointCy - pointO.getY()))
        / (dx2 * dy1 - dx1 * dy2);
    double x = (dy2 * (pointCx - pointO.getX()) + dx2 * (pointO.getY() - pointCy))
        / (dx2 * dy1 - dx1 * dy2);

    return new Point2D.Double(x, y);
  }

}
