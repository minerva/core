package lcsb.mapviewer.converter.model.celldesigner.function;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;

public class FunctionXmlParser {
  /**
   * Default class logger
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  public SbmlFunction parseFunction(final Node functionNode) throws InvalidXmlSchemaException {
    String functionId = XmlParser.getNodeAttr("id", functionNode);

    SbmlFunction result = new SbmlFunction(functionId);
    result.setName(XmlParser.getNodeAttr("name", functionNode));
    Node mathDefinition = XmlParser.getNode("math", functionNode);
    if (mathDefinition == null) {
      throw new InvalidXmlSchemaException("Function " + functionId + " doesn't contain MathML definition (math node)");
    }
    String definition = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\">"
        + XmlParser.nodeToString(mathDefinition).replace(" xmlns=\"http://www.w3.org/1998/Math/MathML\"", "")
        + "</math>";
    result.setDefinition(definition);
    return result;
  }

  public String toXml(final SbmlFunction fun) {
    StringBuilder result = new StringBuilder();
    result.append("<functionDefinition ");
    result.append("id=\"" + fun.getElementId() + "\" ");
    result.append("name=\"" + XmlParser.escapeXml(fun.getName()) + "\" ");
    result.append(">\n");
    result.append(fun.getDefinition());
    result.append("</functionDefinition>\n");
    return result.toString();
  }

}
