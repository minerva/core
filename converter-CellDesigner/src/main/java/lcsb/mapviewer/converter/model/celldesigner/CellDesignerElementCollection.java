package lcsb.mapviewer.converter.model.celldesigner;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.AbstractSiteModification;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationResidue;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * This structure contains information about {@link CellDesignerElement
 * CellDesigner elements} parsed from the file. The information about this
 * elements is used in different places in the parsing process.
 */
public class CellDesignerElementCollection {

  /**
   * Default class logger
   */
  private static final Logger logger = LogManager.getLogger();

  /**
   * Element by element identifier (it's CellDesigner identifier).
   */
  private final Map<String, CellDesignerElement<?>> elementById = new HashMap<>();
  /**
   *
   */
  private final Map<String, String> sbmlIdByElement = new HashMap<>();
  private final Map<String, String> modificationResidueIdByHash = new HashMap<>();
  private final Set<String> usedModificationResidueIds = new HashSet<>();

  /**
   * Returns element by given identifier.
   *
   * @param speciesId element identifier
   * @param <T>       type of returned object
   * @return element by given identifier
   */
  @SuppressWarnings("unchecked")
  public <T extends CellDesignerElement<?>> T getElementByElementId(final String speciesId) {
    return (T) elementById.get(speciesId);
  }

  /**
   * Returns element identifier that should be used for model element when
   * creating cell designer xml file.
   *
   * @param modelElement model element for which we want to obtain identifier
   * @return identifier of cell designer element that will be exported
   */
  public String getElementId(final Element modelElement) {
    if ("default".equals(modelElement.getElementId())) {
      return modelElement.getElementId();
    } else {
      final String sbmlId = getSbmlId(modelElement, true);
      if (sbmlIdByElement.get(sbmlId) == null) {
        final String normalizedId = normalizeIdToCellDesigner(modelElement.getElementId());
        if (!Objects.equals(normalizedId, modelElement.getElementId())) {
          logger.warn(new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, modelElement), "Invalid element id");
        }
        final String id = "s_id_" + normalizeIdToCellDesigner(modelElement.getElementId());
        if (sbmlIdByElement.containsValue(id)) {
          throw new InvalidArgumentException("id duplicates");
        }
        sbmlIdByElement.put(sbmlId, id);
      }
      return sbmlIdByElement.get(sbmlId);

    }
  }

  /**
   * Creates a String that identifies element as distinct SBML entity.
   *
   * @param modelElement element that we want to identify
   * @param useComplex   should we use identifier of a complex. This should be used by
   *                     default (because if the complex is different then element should
   *                     have different identifier). However, when element asks complex for
   *                     id, complex will try to resolve identifiers of children (because
   *                     this is what defines complex identity), and in such situation it
   *                     should disable resolving complex, because there will by infinity
   *                     cyclic calls and stack overflow error will be thrown.
   * @return unique String for SBML entity
   */
  private String getSbmlId(final Element modelElement, final boolean useComplex) {
    String compartmenName = "default";
    if (modelElement.getCompartment() != null) {
      compartmenName = modelElement.getCompartment().getName();
    }

    String modifications = "";
    final List<ModificationResidue> regions = new ArrayList<>();
    if (modelElement instanceof SpeciesWithModificationResidue) {
      regions.addAll(((SpeciesWithModificationResidue) modelElement).getModificationResidues());
    }
    for (final ModificationResidue region : regions) {
      if (region instanceof AbstractSiteModification) {
        modifications += ((AbstractSiteModification) region).getState() + "," + region.getName() + ";";
      } else if (region instanceof StructuralState) {
        modifications = region.getName();
      }
    }

    String complexId = "";
    String homodimer = "";
    if (modelElement instanceof Species) {
      homodimer = ((Species) modelElement).getHomodimer() + "";
      if (((Species) modelElement).getComplex() != null) {
        if (useComplex) {
          if (!isCyclicNesting(((Species) modelElement).getComplex())) {
            complexId = getElementId(((Species) modelElement).getComplex());
          } else {
            throw new InvalidArgumentException(
                "Cycling nested structure found in element: " + modelElement.getElementId());
          }
        } else {
          complexId = ((Species) modelElement).getComplex().getName();
        }
      }
    }
    String childrenId = "";
    if (modelElement instanceof Complex) {
      final Complex asComplex = (Complex) modelElement;
      final List<String> childIds = new ArrayList<>();
      for (final Species child : asComplex.getAllChildren()) {
        childIds.add(getSbmlId(child, false));
      }
      Collections.sort(childIds);
      for (final String string : childIds) {
        childrenId += string + "\n";
      }
    }

    // identifier that distinguish elements in SBML depends only on type,
    // name, compartment, modifications, homodimer, state, complex where it's
    // located, children of the complex
    String sbmlId = compartmenName + "\n" + modelElement.getName() + "\n" + modelElement.getStringType() + "\n"
        + modifications + "\n" + complexId + "\n" + homodimer + "\n" + childrenId;

    if (modelElement instanceof Degraded) {
      sbmlId += "\n" + modelElement.getElementId();
    }
    return sbmlId;
  }

  /**
   * Checks if complex parenting is cyclic.
   *
   * @param sourceComplex complex for which data is checked
   * @return true if parent of the complex is also a (grand)child of this complex, false otherwise
   */
  private boolean isCyclicNesting(final Complex sourceComplex) {
    Complex complex = sourceComplex;

    final Set<Complex> foundComplexes = new HashSet<>();
    while (complex != null) {
      if (foundComplexes.contains(complex)) {
        return true;
      }
      foundComplexes.add(complex);
      complex = complex.getComplex();
    }
    return false;
  }

  /**
   * Adds cell designer structures.
   *
   * @param elements list of objects to add
   */
  public void addElements(final List<? extends CellDesignerElement<?>> elements) {
    for (final CellDesignerElement<?> element : elements) {
      addElement(element);
    }
  }

  /**
   * Adds cell designer object.
   *
   * @param element object to add
   */
  public void addElement(final CellDesignerElement<?> element) {
    addElement(element, element.getElementId());
  }

  /**
   * Adds CellDesigner element with custom id (instead the one obtained from
   * CellDesigner structure).
   *
   * @param element element to be add
   * @param id      id that should be used for identifying element
   */
  public void addElement(final CellDesignerElement<?> element, final String id) {
    if (elementById.get(id) != null) {
      throw new InvalidArgumentException("[" + element.getClass().getSimpleName() + " " + element.getElementId() + "]\t"
          + "Element with given id alread exists. ID: " + id);
    }
    elementById.put(id, element);
  }

  /**
   * Adds CellDesigner structure in a way that it would be accessed via identifier
   * for model structure. Method used only for unit test.
   *
   * @param modelElement model element that will create identifier
   * @param element      element to be added
   */
  public void addModelElement(final Element modelElement, final CellDesignerElement<?> element) {
    addElement(element, getElementId(modelElement));
    if (getElementByElementId(element.getElementId()) == null) {
      addElement(element);
    }
  }

  /**
   * This method computes modificationResidueId that should be used when exporting
   * modification residue to CellDesigner. The identifier relies on type and
   * position on the list of modification
   *
   * @param modificationResidue {@link ModificationResidue} for which we want to find out identifier
   * @param number              position on which this {@link ModificationResidue} is located in
   *                            species
   * @return identifier that can be used in CellDesigner
   */
  public String getModificationResidueId(final ModificationResidue modificationResidue, final int number) {
    if (modificationResidue.getIdModificationResidue().startsWith("mr_")) {
      return modificationResidue.getIdModificationResidue(); //custom id numbering
    }
    final String hash = modificationResidue.getClass().getSimpleName() + "\n" + number;
    String result = modificationResidueIdByHash.get(hash);
    if (result == null) {
      if (!usedModificationResidueIds.contains(modificationResidue.getIdModificationResidue())
          // CellDesigner does not allow to use numeric identifiers for
          // modification residues
          && !NumberUtils.isParsable(modificationResidue.getIdModificationResidue())) {
        result = modificationResidue.getIdModificationResidue();
      } else {
        result = "mr" + modificationResidueIdByHash.keySet().size();
      }
      modificationResidueIdByHash.put(hash, result);
      usedModificationResidueIds.add(result);
    }
    return result;
  }

  public String getModificationResidueId(final CellDesignerModificationResidue mr, final int number) {
    return getModificationResidueId(mr.createModificationResidue(new Gene("X")), number);
  }

  public String getCompartmentAliasId(final Compartment ca) {
    if (ca == null) {
      return null;
    }
    if (ca.getElementId().length() >= 2) {
      return normalizeIdToCellDesigner(ca.getElementId());
    }
    return "ca_prefix_issue_" + normalizeIdToCellDesigner(ca.getElementId());
  }

  public String getModelId(final Model model) {
    if (model.getIdModel() == null) {
      return null;
    }
    return normalizeIdToCellDesigner(model.getIdModel());
  }

  protected String normalizeIdToCellDesigner(final String id) {
    if (id == null) {
      return null;
    }
    return id.replaceAll("[:\\.-]", "_");
  }

}
