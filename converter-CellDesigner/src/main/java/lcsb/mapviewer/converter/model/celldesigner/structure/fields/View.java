package lcsb.mapviewer.converter.model.celldesigner.structure.fields;

import java.awt.Color;
import java.awt.geom.Dimension2D;
import java.awt.geom.Point2D;

/**
 * This class represents some layout information in CellDesigner format.
 * 
 * @author Piotr Gawron
 * 
 */
public class View {

  /**
   * No idea what this parameter describes.
   */
  private Point2D innerPosition;

  /**
   * Size of the element.
   */
  private Dimension2D boxSize;

  /**
   * Width of the line.
   */
  private SingleLine singleLine;

  /**
   * Color of the element.
   */
  private Color color;

  /**
   * @return the innerPosition
   */
  public Point2D getInnerPosition() {
    return innerPosition;
  }

  /**
   * @param innerPosition
   *          the innerPosition to set
   */
  public void setInnerPosition(final Point2D innerPosition) {
    this.innerPosition = innerPosition;
  }

  /**
   * @return the boxSize
   */
  public Dimension2D getBoxSize() {
    return boxSize;
  }

  /**
   * @param boxSize
   *          the boxSize to set
   */
  public void setBoxSize(final Dimension2D boxSize) {
    this.boxSize = boxSize;
  }

  /**
   * @return the singleLine
   */
  public SingleLine getSingleLine() {
    return singleLine;
  }

  /**
   * @param singleLine
   *          the singleLine to set
   */
  public void setSingleLine(final SingleLine singleLine) {
    this.singleLine = singleLine;
  }

  /**
   * @return the color
   */
  public Color getColor() {
    return color;
  }

  /**
   * @param color
   *          the color to set
   */
  public void setColor(final Color color) {
    this.color = color;
  }
}
