package lcsb.mapviewer.converter.model.celldesigner.types;

import lcsb.mapviewer.converter.model.celldesigner.geometry.ReactionCellDesignerConverter;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.ArrowTypeData;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.modifier.Inhibition;
import lcsb.mapviewer.model.map.modifier.Modulation;
import lcsb.mapviewer.model.map.modifier.PhysicalStimulation;
import lcsb.mapviewer.model.map.modifier.Trigger;
import lcsb.mapviewer.model.map.modifier.UnknownCatalysis;
import lcsb.mapviewer.model.map.modifier.UnknownInhibition;
import lcsb.mapviewer.model.map.reaction.Modifier;

/**
 * This enum defines how different types of CellDesigner modifiers should be
 * translated into the reaction {@link Modifier} class.
 * 
 * @author Piotr Gawron
 * 
 */
public enum ModifierType {

  /**
   * Catalysis.
   */
  CATALYSIS("CATALYSIS", Catalysis.class, ArrowType.CIRCLE, 8.0),

  /**
   * Inhibition.
   */
  INHIBITION("INHIBITION", Inhibition.class, ArrowType.CROSSBAR, 8.0, ReactionCellDesignerConverter.RECT_SIZE / 2 - 1),

  /**
   * Unknown catalysis.
   */
  UNKNOWN_CATALYSIS("UNKNOWN_CATALYSIS", UnknownCatalysis.class, ArrowType.CIRCLE, 8.0, 0.0, LineType.DASHED),

  /**
   * Unknown inhibition.
   */
  UNKNOWN_INHIBITION("UNKNOWN_INHIBITION", UnknownInhibition.class, ArrowType.CROSSBAR, 8.0,
      ReactionCellDesignerConverter.RECT_SIZE / 2 - 1, LineType.DASHED),

  /**
   * Physical stimulation.
   */
  PHYSICAL_STIMULATION("PHYSICAL_STIMULATION", PhysicalStimulation.class, ArrowType.BLANK),

  /**
   * Modulation.
   */
  MODULATION_STRING("MODULATION", Modulation.class, ArrowType.DIAMOND),

  /**
   * Trigger.
   */
  TRIGGER_STRING("TRIGGER", Trigger.class, ArrowType.BLANK_CROSSBAR),
  
  /**
   * CellDesigner 2.5 modification type.
   */
  TRANSCRIPTIONAL_ACTIVATION("TRANSCRIPTIONAL_ACTIVATION", Trigger.class, ArrowType.BLANK_CROSSBAR),
  
  /**
   * CellDesigner 2.5 modification type.
   */
  TRANSCRIPTIONAL_INHIBITION("TRANSCRIPTIONAL_INHIBITION", Inhibition.class, ArrowType.CROSSBAR, 8.0, 
      ReactionCellDesignerConverter.RECT_SIZE / 2 - 1),
  
  ;

  /**
   * CellDesigner string describing this type.
   */
  private String stringName;
  /**
   * Which class should be used for this type.
   */
  private Class<? extends Modifier> clazz;
  /**
   * How the arrow head of the line should look like.
   */
  private ArrowTypeData atd = new ArrowTypeData();
  /**
   * How much should be the arrow line trimmed.
   */
  private double trimLength = 0;
  /**
   * What line type should be used for this modifier.
   */
  private LineType lineType = LineType.SOLID;

  /**
   * Constructor used to create and initialize this enum.
   * 
   * @param string
   *          {@link #stringName}
   * @param clazz
   *          {@link #clazz}
   * @param at
   *          type of the arrow
   */
  ModifierType(final String string, final Class<? extends Modifier> clazz, final ArrowType at) {
    this(string, clazz, at, null, null, null);
  }

  /**
   * Constructor used to create and initialize this enum.
   * 
   * @param string
   *          {@link #stringName}
   * @param clazz
   *          {@link #clazz}
   * @param at
   *          type of the arrow
   * @param arrowLength
   *          length of the arrow
   */
  ModifierType(final String string, final Class<? extends Modifier> clazz, final ArrowType at, final Double arrowLength) {
    this(string, clazz, at, arrowLength, null, null);
  }

  /**
   * Constructor used to create and initialize this enum.
   * 
   * @param string
   *          {@link #stringName}
   * @param clazz
   *          {@link #clazz}
   * @param at
   *          type of the arrow
   * @param arrowLength
   *          length of the arrow
   * @param trimLength
   *          {@link #trimLength}
   */
  ModifierType(final String string, final Class<? extends Modifier> clazz, final ArrowType at, final Double arrowLength, final Double trimLength) {
    this(string, clazz, at, arrowLength, trimLength, null);
  }

  /**
   * Constructor used to create and initialize this enum.
   * 
   * @param string
   *          {@link #stringName}
   * @param clazz
   *          {@link #clazz}
   * @param at
   *          type of the arrow
   * @param arrowLength
   *          length of the arrow
   * @param trimLength
   *          {@link #trimLength}
   * @param lt
   *          {@link #lineType}
   */
  ModifierType(final String string, final Class<? extends Modifier> clazz, final ArrowType at, final Double arrowLength, final Double trimLength,
      final LineType lt) {
    stringName = string;
    this.clazz = clazz;
    if (at != null) {
      atd.setArrowType(at);
    }
    if (arrowLength != null) {
      atd.setLen(arrowLength);
    }
    if (trimLength != null) {
      this.trimLength = trimLength;
    }
    if (lt != null) {
      lineType = lt;
    }

  }

  /**
   * 
   * @return {@link #stringName}
   */
  public String getStringName() {
    return stringName;
  }

  /**
   * 
   * @return {@link #clazz}
   */
  public Class<? extends Modifier> getClazz() {
    return clazz;
  }

  /**
   * 
   * @return {@link #atd}
   */
  public ArrowTypeData getAtd() {
    return atd;
  }

  /**
   * 
   * @return {@link #trimLength}
   */
  public double getTrimLength() {
    return trimLength;
  }

  /**
   * 
   * @return {@link #lineType}
   */
  public LineType getLineType() {
    return lineType;
  }
}
