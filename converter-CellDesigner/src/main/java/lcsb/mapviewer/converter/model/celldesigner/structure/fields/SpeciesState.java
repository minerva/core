package lcsb.mapviewer.converter.model.celldesigner.structure.fields;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.model.celldesigner.species.SpeciesWithModificationData;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationResidue;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Structure for storing the state of the Species in CellDesigner format.
 *
 * @author Piotr Gawron
 */
public class SpeciesState {

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  /**
   * How many dimers are in the species.
   */
  private int homodimer = 1;

  /**
   * String state description.
   */
  private String structuralState = null;

  /**
   * List of species modification.
   */
  private List<CellDesignerModificationResidue> modifications = new ArrayList<>();

  /**
   * Default constructor.
   */
  public SpeciesState() {
  }

  /**
   * Creates species state description from species element.
   *
   * @param species object for which description is created
   */
  public SpeciesState(final Species species) {
    if (species instanceof SpeciesWithModificationResidue) {
      final SpeciesWithModificationResidue protein = (SpeciesWithModificationResidue) species;
      for (final ModificationResidue mr : protein.getModificationResidues()) {
        if (mr instanceof StructuralState) {
          setStructuralState(mr.getName());
        } else {
          addModificationResidue(new CellDesignerModificationResidue(mr));
        }
      }
    }
    setHomodimer(species.getHomodimer());
  }

  public SpeciesState(final Species species, final SpeciesWithModificationData speciesWithModificationData) {
    if (species instanceof SpeciesWithModificationResidue) {
      final SpeciesWithModificationResidue protein = (SpeciesWithModificationResidue) species;
      for (final ModificationResidue mr : protein.getModificationResidues()) {
        if (mr instanceof StructuralState) {
          setStructuralState(mr.getName());
        } else {
          final CellDesignerModificationResidue cellDesignerModificationResidue = new CellDesignerModificationResidue(mr);
          cellDesignerModificationResidue.setIdModificationResidue(speciesWithModificationData.getModificationResidueIdFromOriginal(mr));
          addModificationResidue(cellDesignerModificationResidue);
        }
      }
    }
    setHomodimer(species.getHomodimer());
  }

  /**
   * Adds modification to the state.
   *
   * @param modificationResidue modification to add
   */
  public void addModificationResidue(final CellDesignerModificationResidue modificationResidue) {
    for (final CellDesignerModificationResidue modification : modifications) {
      if (modification.getIdModificationResidue().equals(modificationResidue.getIdModificationResidue())) {
        modification.update(modificationResidue);
        return;
      }
    }
    modifications.add(modificationResidue);
  }

  /**
   * @return the homodimer
   * @see #homodimer
   */
  public int getHomodimer() {
    return homodimer;
  }

  /**
   * @param homodimer new {@link #homodimer} value to set (in string format)
   */
  public void setHomodimer(final String homodimer) {
    try {
      this.homodimer = Integer.parseInt(homodimer);
    } catch (final NumberFormatException e) {
      throw new InvalidArgumentException("Invalid homodir value: " + homodimer);
    }
  }

  /**
   * @param homodimer the homodimer to set
   * @see #homodimer
   */
  public void setHomodimer(final int homodimer) {
    this.homodimer = homodimer;
  }

  /**
   * @return the structuralState
   * @see #structuralState
   */
  public String getStructuralState() {
    return structuralState;
  }

  /**
   * @param structuralState the structuralState to set
   * @see #structuralState
   */
  public void setStructuralState(final String structuralState) {
    if (this.structuralState != null && !Objects.equals(this.structuralState, structuralState)) {
      logger.warn("More than one structural state. Ignoring: " + structuralState);
    } else {
      this.structuralState = structuralState;
    }
  }

  /**
   * @return the modifications
   * @see #modifications
   */
  public List<CellDesignerModificationResidue> getModifications() {
    return modifications;
  }

  /**
   * @param modifications the modifications to set
   * @see #modifications
   */
  public void setModifications(final List<CellDesignerModificationResidue> modifications) {
    this.modifications = modifications;
  }

}
