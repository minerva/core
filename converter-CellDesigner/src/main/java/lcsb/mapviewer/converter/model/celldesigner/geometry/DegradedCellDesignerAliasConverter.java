package lcsb.mapviewer.converter.model.celldesigner.geometry;

import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Class that provides CellDesigner specific graphical information for Degraded
 * elements. It's used for conversion from xml to normal x,y coordinates.
 * 
 * @author Piotr Gawron
 * 
 */
public class DegradedCellDesignerAliasConverter extends AbstractCellDesignerAliasConverter<Species> {

  /**
   * Part of height of the line used to cross degraded circle that goes behind
   * this circle.
   */
  private static final int DEGRADED_MARGIN = 7;

  /**
   * Default constructor.
   *
   * @param sbgn
   *          Should the converter use sbgn standard
   */
  protected DegradedCellDesignerAliasConverter(final boolean sbgn) {
    super(sbgn);
  }

  @Override
  public Point2D getPointCoordinates(final Species alias, final CellDesignerAnchor anchor) {
    double diameter = getDiameter(alias);
    double x = getXCoord(alias, diameter);
    double y = getYCoord(alias);
    if (invalidAnchorPosition(alias, anchor)) {
      return alias.getCenter();
    }

    return getEllipseTransformation().getPointOnEllipseByAnchor(x, y, diameter, diameter, anchor);
  }

  @Override
  public Point2D getAnchorPointCoordinates(final Species alias, final double angle) {
    if (alias.getWidth() == 0 && alias.getHeight() == 0) {
      return alias.getCenter();
    }
    double diameter = getDiameter(alias);
    double x = getXCoord(alias, diameter);
    double y = getYCoord(alias);
    Point2D result = getEllipseTransformation().getPointOnEllipseByRadian(x, y, diameter, diameter, angle);
    return result;

  }

  @Override
  public PathIterator getBoundPathIterator(final Species alias) {
    throw new NotImplementedException("This class doesn't have bound");
  }

  /**
   * Returns transformed y coordinate for the degraded alias.
   *
   * @param alias
   *          object alias to to which we are looking for y coordinate
   * @return y coordinate of the alias
   */
  private double getYCoord(final Element alias) {
    double y = alias.getY() + DEGRADED_MARGIN;
    return y;
  }

  /**
   * Returns transformed x coordinate for the degraded alias.
   *
   * @param alias
   *          object alias to to which we are looking for x coordinate
   * @param diameter
   *          diameter of cross line used in this alias
   * @return x coordinate of the alias
   */
  private double getXCoord(final Element alias, final double diameter) {
    double x = alias.getX() + (alias.getWidth() - diameter) / 2;
    return x;
  }

  /**
   * Computes diameter of cross line for the degraded alias.
   *
   * @param alias
   *          object alias to to which we are looking for diameter.
   * @return diameter of the cross line
   */
  private double getDiameter(final Element alias) {
    double diameter = Math.min(alias.getWidth(), alias.getHeight()) - 2 * DEGRADED_MARGIN;
    if (diameter < 0) {
      diameter = 0;
    }
    return diameter;
  }

}
