package lcsb.mapviewer.converter.model.celldesigner.geometry;

import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.AbstractRegionModification;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

/**
 * Class that provides CellDesigner specific graphical information for
 * AntisenseRna. It's used for conversion from xml to normal x,y coordinates.
 * 
 * @author Piotr Gawron
 * 
 */
public class AntisenseRnaCellDesignerAliasConverter extends AbstractCellDesignerAliasConverter<Species> {

  /**
   * How big should be the arc in rectangle for nucleic acid feature
   * representation.
   */
  private static final int RECTANGLE_CORNER_ARC_SIZE = 5;

  /**
   * Default constructor.
   *
   * @param sbgn
   *          Should the converter use sbgn standard
   */
  protected AntisenseRnaCellDesignerAliasConverter(final boolean sbgn) {
    super(sbgn);
  }

  @Override
  public Point2D getPointCoordinates(final Species alias, final CellDesignerAnchor anchor) {
    if (invalidAnchorPosition(alias, anchor)) {
      return alias.getCenter();
    }
    if (isSbgn()) {
      return getRectangleTransformation().getPointOnRectangleByAnchor(alias.getX(), alias.getY(), alias.getWidth(),
          alias.getHeight(), anchor);
    }
    ArrayList<Point2D> points = getPoints(alias);
    return getPolygonTransformation().getPointOnPolygonByAnchor(points, anchor);
  }

  @Override
  public Double getCellDesignerPositionByCoordinates(final ModificationResidue mr) {
    return (mr.getCenter().getX() - mr.getSpecies().getX()) / (mr.getSpecies().getWidth() * 3.0 / 4.0);
  }

  @Override
  public Point2D getCoordinatesByPosition(final Element element, final Double pos, final Double width) {
    double x = element.getX() + element.getWidth() * 3.0 / 4.0 * pos;
    x = Math.max(element.getX() + width / 2, x);
    x = Math.min(element.getX() + element.getWidth() * 3.0 / 4.0 - width / 2, x);
    return new Point2D.Double(x, element.getY());
  }

  @Override
  public Double getCellDesignerSize(final ModificationResidue mr) {
    if (mr instanceof AbstractRegionModification) {
      return ((AbstractRegionModification) mr).getWidth() / (mr.getSpecies().getWidth() * 3.0 / 4.0);
    }
    throw new NotImplementedException("Not implemented for: " + this.getClass() + ", " + mr.getClass());
  }

  @Override
  public Double getWidthBySize(final Element element, final Double size) {
    return size * (element.getWidth() * 3.0 / 4.0);
  }

  @Override
  protected PathIterator getBoundPathIterator(final Species alias) {
    return getAntisebseRnaPath(alias).getPathIterator(new AffineTransform());
  }

  /**
   * Returns shape of the AntisenseRna as a list of points.
   *
   * @param alias
   *          alias for which we are looking for a border
   * @return list of points defining border of the given alias
   */
  private ArrayList<Point2D> getPoints(final Element alias) {
    double x = alias.getX();
    double y = alias.getY();
    double width = alias.getWidth();
    double height = alias.getHeight();
    ArrayList<Point2D> points = new ArrayList<Point2D>();

    points.add(new Point2D.Double(x + width / 8, y + height / 2));
    points.add(new Point2D.Double(x, y));
    points.add(new Point2D.Double(x + width * 3 / 8, y));
    points.add(new Point2D.Double(x + width * 3 / 4, y));
    points.add(new Point2D.Double(x + width * 7 / 8, y + height / 2));
    points.add(new Point2D.Double(x + width, y + height));
    points.add(new Point2D.Double(x + width * 5 / 8, y + height));
    points.add(new Point2D.Double(x + width / 4, y + height));
    return points;
  }

  /**
   * Returns shape of the AntisenseRna as a GeneralPath object.
   *
   * @param alias
   *          alias for which we are looking for a border
   * @return GeneralPath object defining border of the given alias
   */
  private GeneralPath getAntisebseRnaPath(final Element alias) {
    GeneralPath path;
    if (!isSbgn()) {
      path = new GeneralPath(GeneralPath.WIND_EVEN_ODD, 4);
      path.moveTo(alias.getX(), alias.getY());
      path.lineTo(alias.getX() + alias.getWidth() * 3 / 4, alias.getY());
      path.lineTo(alias.getX() + alias.getWidth(), alias.getY() + alias.getHeight());
      path.lineTo(alias.getX() + alias.getWidth() / 4, alias.getY() + alias.getHeight());
      path.closePath();
    } else {
      path = new GeneralPath(GeneralPath.WIND_EVEN_ODD, 6);
      double x = alias.getX();
      double y = alias.getY();
      double width = alias.getWidth();
      double height = alias.getHeight();

      path.moveTo(x, y);
      path.lineTo(x, y + height - RECTANGLE_CORNER_ARC_SIZE);
      path.curveTo(x, y + height, x + RECTANGLE_CORNER_ARC_SIZE, y + height, x + RECTANGLE_CORNER_ARC_SIZE, y + height);
      path.lineTo(x + width - RECTANGLE_CORNER_ARC_SIZE, y + height);
      path.curveTo(x + width, y + height, x + width, y + height - RECTANGLE_CORNER_ARC_SIZE, x + width,
          y + height - RECTANGLE_CORNER_ARC_SIZE);
      path.lineTo(x + width, y);
      path.closePath();
    }
    return path;
  }

}
