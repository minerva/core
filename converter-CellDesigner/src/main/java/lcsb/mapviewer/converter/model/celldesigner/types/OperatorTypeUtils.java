package lcsb.mapviewer.converter.model.celldesigner.types;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.map.reaction.NodeOperator;

/**
 * Util class that helps in transformation of CellDesigner operators into
 * {@link NodeOperator} object in our model.
 * 
 * @author Piotr Gawron
 * 
 */
public class OperatorTypeUtils {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  /**
   * Return {@link OperatorType} for the operator class.
   * 
   * @param clazz
   *          clazz for which we are looking for operator type
   * @return {@link OperatorType} corresponding to the given class
   */
  public OperatorType getOperatorTypeForClazz(final Class<? extends NodeOperator> clazz) {
    for (final OperatorType modType : OperatorType.values()) {
      if (clazz.equals(modType.getClazz())) {
        return modType;
      }
    }
    return null;
  }

  /**
   * Return {@link OperatorType} for the CellDesigner string representing
   * operator.
   * 
   * @param type
   *          CellDesigner string representing operator
   * @return {@link OperatorType} corresponding to the given type
   */
  protected OperatorType getOperatorTypeForStringType(final String type) {
    for (final OperatorType modType : OperatorType.values()) {
      if (type.equalsIgnoreCase(modType.getStringName())) {
        return modType;
      }
    }
    return null;
  }

  /**
   * Creates operator for the type identified by CellDesigner string.
   * 
   * @param type
   *          CellDesigner string identifying operator type.
   * @return empty {@link NodeOperator} of the type given in parameter
   */
  public NodeOperator createModifierForStringType(final String type) {
    NodeOperator operator = null;
    OperatorType opType = getOperatorTypeForStringType(type);
    if (opType == null) {
      throw new InvalidArgumentException("Unknown modifier type: " + type);
    }
    try {
      operator = opType.getClazz().getConstructor().newInstance();
    } catch (final Exception e) {
      throw new InvalidStateException("Problem with instantiation of NodeOperator class: " + type, e);
    }
    return operator;
  }

  /**
   * Returns CellDesigner string for the operator type.
   * 
   * @param modifierOperator
   *          operator for which CellDesigner string is looked for
   * @return CellDesigner string for the operator type
   */
  public String getStringTypeByOperator(final NodeOperator modifierOperator) {
    OperatorType operatorType = getOperatorTypeForClazz(modifierOperator.getClass());
    if (operatorType != null) {
      return operatorType.getStringName();
    }
    return null;
  }
}
