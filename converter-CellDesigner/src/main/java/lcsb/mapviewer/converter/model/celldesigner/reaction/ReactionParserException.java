package lcsb.mapviewer.converter.model.celldesigner.reaction;

import lcsb.mapviewer.converter.model.celldesigner.CellDesignerParserException;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * Exception thrown when problem with parsing
 * {@link lcsb.mapviewer.model.map.reaction.Reaction} is encountered.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactionParserException extends CellDesignerParserException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Identifier of reaction that was a reason for this exception.
   */
  private String reactionId;

  /**
   * Default constructor.
   * 
   * @param message
   *          message thrown
   * @param reactionId
   *          {@link #reactionId}
   */
  public ReactionParserException(final String message, final String reactionId) {
    super(message);
    this.reactionId = reactionId;
  }

  /**
   * Default constructor.
   * 
   * @param message
   *          message thrown
   * @param reaction
   *          {@link #reactionId}
   */
  public ReactionParserException(final String message, final Reaction reaction) {
    super(message);
    if (reaction != null) {
      setReactionId(reaction.getIdReaction());
    }
  }

  /**
   * Default constructor.
   * 
   * @param message
   *          message thrown
   * @param reaction
   *          {@link #reactionId}
   * @param e
   *          super exception
   */
  public ReactionParserException(final String message, final Reaction reaction, final Exception e) {
    super(message, e);
    if (reaction != null) {
      setReactionId(reaction.getIdReaction());
    }
  }

  /**
   * Default constructor.
   * 
   * @param reaction
   *          {@link #reactionId}
   * @param e
   *          super exception
   */
  public ReactionParserException(final Reaction reaction, final Throwable e) {
    super(e);
    if (reaction != null) {
      setReactionId(reaction.getIdReaction());
    }
  }

  /**
   * @return the reactionId
   * @see #reactionId
   */
  public String getReactionId() {
    return reactionId;
  }

  /**
   * @param reactionId
   *          the reactionId to set
   * @see #reactionId
   */
  public void setReactionId(final String reactionId) {
    this.reactionId = reactionId;
  }

  @Override
  public String getMessageContext() {
    if (reactionId != null) {
      return "[Reaction: " + reactionId + "]";
    }
    return null;
  }

}
