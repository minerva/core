package lcsb.mapviewer.converter.model.celldesigner.parameter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerParserUtils;
import lcsb.mapviewer.converter.model.celldesigner.unit.UnitXmlParser;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.model.Model;

public class ParameterXmlParser {
  /**
   * Default class logger
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  private Model model;

  private UnitXmlParser unitParser = new UnitXmlParser();

  public ParameterXmlParser(final Model model) {
    this.model = model;
  }

  public SbmlParameter parseParameter(final Node unitNode) throws InvalidXmlSchemaException {
    String parameterId = XmlParser.getNodeAttr("id", unitNode);

    SbmlParameter result = new SbmlParameter(parameterId);
    result.setName(XmlParser.getNodeAttr("name", unitNode));
    result.setValue(new CellDesignerParserUtils().parseDouble(XmlParser.getNodeAttr("value", unitNode)));
    result.setUnits(model.getUnitsByUnitId(XmlParser.getNodeAttr("units", unitNode)));
    return result;
  }

  public String toXml(final SbmlParameter sbmlFunction) {
    StringBuilder result = new StringBuilder();
    result.append("<parameter ");
    result.append("id=\"" + sbmlFunction.getElementId() + "\" ");
    result.append("name=\"" + XmlParser.escapeXml(sbmlFunction.getName()) + "\" ");
    result.append("value=\"" + sbmlFunction.getValue() + "\" ");
    if (sbmlFunction.getUnits() != null
        && unitParser.isSupported(sbmlFunction.getUnits())) {
      result.append("units=\"" + sbmlFunction.getUnits().getUnitId() + "\" ");
    }
    result.append("/>");

    return result.toString();
  }
}
