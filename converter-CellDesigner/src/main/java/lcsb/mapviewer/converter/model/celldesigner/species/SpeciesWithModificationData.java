package lcsb.mapviewer.converter.model.celldesigner.species;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationResidue;
import lcsb.mapviewer.model.map.species.field.StructuralState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SpeciesWithModificationData {
  private String name;
  private final String proteinId;

  private final Map<String, List<Residue>> residuesByNameAndCoordinates = new LinkedHashMap<>();

  private final Map<String, List<BindingRegion>> regionsByNameAndCoordinates = new LinkedHashMap<>();

  public SpeciesWithModificationData(final SpeciesWithModificationResidue protein, final String proteinId) {
    this(proteinId, protein.getName());
    addInfoFromSpecies(protein);
  }

  public SpeciesWithModificationData(final String proteinId, final String name) {
    this.proteinId = proteinId;
    this.name = name;
  }

  public void addInfoFromSpecies(final SpeciesWithModificationResidue species) {
    final List<Residue> residueList = new ArrayList<>();
    final List<BindingRegion> bindingRegionList = new ArrayList<>();
    for (final ModificationResidue mr : species.getModificationResidues()) {
      if (mr instanceof Residue) {
        residueList.add((Residue) mr);
      } else if (mr instanceof BindingRegion) {
        bindingRegionList.add((BindingRegion) mr);
      } else if (mr instanceof StructuralState) {
        continue;
      } else {
        throw new NotImplementedException();
      }
    }
    addInfoAboutResidues(residueList);
    addInfoAboutBindingRegions(bindingRegionList);
  }

  private void addInfoAboutBindingRegions(final List<BindingRegion> bindingRegionList) {
    if (!bindingRegionList.isEmpty()) {
      final Map<String, List<BindingRegion>> currentRegionsByNameAndCoordinates = new HashMap<>();

      for (final BindingRegion region : bindingRegionList) {
        final String residueId = getBindingRegionKey(region);
        if (!currentRegionsByNameAndCoordinates.containsKey(residueId)) {
          currentRegionsByNameAndCoordinates.put(residueId, new ArrayList<>());
        }
        final BindingRegion copy = region.copy();
        copy.setSpecies(region.getSpecies().copy());
        currentRegionsByNameAndCoordinates.get(residueId).add(copy);
      }
      mergeBindingRegionMaps(regionsByNameAndCoordinates, currentRegionsByNameAndCoordinates);
    }
  }

  private void addInfoAboutResidues(final List<Residue> residueList) {
    if (!residueList.isEmpty()) {
      final Map<String, List<Residue>> currentResiduesByNameAndCoordinates = new HashMap<>();

      for (final Residue residue : residueList) {
        final String residueId = getResidueKey(residue);
        if (!currentResiduesByNameAndCoordinates.containsKey(residueId)) {
          currentResiduesByNameAndCoordinates.put(residueId, new ArrayList<>());
        }
        final Residue copy = residue.copy();
        copy.setSpecies(residue.getSpecies().copy());
        currentResiduesByNameAndCoordinates.get(residueId).add(copy);
      }
      mergeResidueMaps(residuesByNameAndCoordinates, currentResiduesByNameAndCoordinates);
    }
  }

  private void mergeResidueMaps(final Map<String, List<Residue>> targetMap, final Map<String, List<Residue>> additionalMap) {
    for (final Map.Entry<String, List<Residue>> entry : additionalMap.entrySet()) {
      if (targetMap.containsKey(entry.getKey())) {
        if (targetMap.get(entry.getKey()).size() < entry.getValue().size()) {
          targetMap.put(entry.getKey(), entry.getValue());
        }
      } else {
        targetMap.put(entry.getKey(), entry.getValue());
      }
    }
    int iter1 = 1;
    for (final Map.Entry<String, List<Residue>> entry : targetMap.entrySet()) {
      int iter2 = 1;
      for (final Residue residue : entry.getValue()) {
        residue.setIdModificationResidue("mr_re_" + (iter1) + "_" + (iter2++));
      }
      iter1++;
    }
  }

  private void mergeBindingRegionMaps(final Map<String, List<BindingRegion>> targetMap, final Map<String, List<BindingRegion>> additionalMap) {
    for (final Map.Entry<String, List<BindingRegion>> entry : additionalMap.entrySet()) {
      if (targetMap.containsKey(entry.getKey())) {
        if (targetMap.get(entry.getKey()).size() < entry.getValue().size()) {
          targetMap.put(entry.getKey(), entry.getValue());
        }
      } else {
        targetMap.put(entry.getKey(), entry.getValue());
      }
    }
    int iter1 = 1;
    for (final Map.Entry<String, List<BindingRegion>> entry : targetMap.entrySet()) {
      int iter2 = 1;
      for (final BindingRegion bindingRegion : entry.getValue()) {
        bindingRegion.setIdModificationResidue("mr_br_" + (iter1) + "_" + (iter2++));
      }
      iter1++;
    }
  }

  private String getResidueKey(final Residue residue) {
    final int angle = (int) (new CellDesignerModificationResidue(residue).getAngle() * 1000);
    return residue.getName() + "_" + angle;
  }

  private String getBindingRegionKey(final BindingRegion residue) {
    final int angle = (int) (new CellDesignerModificationResidue(residue).getAngle() * 1000);
    final int width = (int) residue.getWidth();
    final int height = (int) residue.getHeight();
    return residue.getName() + "_" + angle + "_" + width + "_" + height;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getProteinId() {
    return proteinId;
  }

  public List<Residue> getResidues() {
    final List<Residue> result = new ArrayList<>();
    for (final List<Residue> residues : residuesByNameAndCoordinates.values()) {
      result.addAll(residues);
    }
    return result;
  }

  public List<BindingRegion> getBindingRegions() {
    final List<BindingRegion> result = new ArrayList<>();
    for (final List<BindingRegion> residues : regionsByNameAndCoordinates.values()) {
      result.addAll(residues);
    }
    return result;
  }

  public String getModificationResidueIdFromOriginal(final ModificationResidue mr) {
    final SpeciesWithModificationResidue species = (SpeciesWithModificationResidue) mr.getSpecies();

    final String key = getModificationResidueKey(mr);
    int counter = 0;
    for (final ModificationResidue modificationResidue : species.getModificationResidues()) {
      if (modificationResidue == mr) {
        if (mr instanceof Residue) {
          return residuesByNameAndCoordinates.get(key).get(counter).getIdModificationResidue();
        } else if (mr instanceof BindingRegion) {
          return regionsByNameAndCoordinates.get(key).get(counter).getIdModificationResidue();
        } else {
          throw new NotImplementedException();
        }
      }
      if (key.equals(getModificationResidueKey(modificationResidue))) {
        counter++;
      }
    }
    throw new NotImplementedException();
  }

  private String getModificationResidueKey(final ModificationResidue mr) {
    if (mr instanceof Residue) {
      return getResidueKey((Residue) mr);
    } else if (mr instanceof BindingRegion) {
      return getBindingRegionKey((BindingRegion) mr);
    } else if (mr instanceof StructuralState) {
      return getStructuralStateKey((StructuralState) mr);
    } else {
      throw new NotImplementedException("Not implemented for " + mr.getClass());
    }
  }

  private String getStructuralStateKey(final StructuralState mr) {
    return "state";
  }
}
