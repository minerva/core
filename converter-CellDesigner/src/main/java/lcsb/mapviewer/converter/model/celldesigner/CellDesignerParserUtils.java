package lcsb.mapviewer.converter.model.celldesigner;

public class CellDesignerParserUtils {
  public Double parseDouble(final String input) {
    if (input == null) {
      return null;
    }
    if (input.equalsIgnoreCase("INF")) {
      return Double.POSITIVE_INFINITY;
    }
    if (input.equalsIgnoreCase("-INF")) {
      return Double.NEGATIVE_INFINITY;
    }
    return Double.parseDouble(input);
  }
}
