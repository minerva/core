package lcsb.mapviewer.converter.model.celldesigner.reaction;

/**
 * Exception thrown when unknown
 * {@link lcsb.mapviewer.model.map.reaction.Modifier} class found.
 * 
 * @author Piotr Gawron
 * 
 */
public class UnknownModifierClassException extends ReactionParserException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Type of reaction that was a reason for this exception.
   */
  private String modificationType;

  /**
   * Default constructor.
   * 
   * @param message
   *          message thrown
   * @param type
   *          {@link #modificationType}
   * @param reactionId
   *          {@link #reactionId}
   */
  public UnknownModifierClassException(final String message, final String type, final String reactionId) {
    super(message, reactionId);
    this.modificationType = type;
  }

  /**
   * @return the modificationType
   * @see #modificationType
   */
  public String getModificationType() {
    return modificationType;
  }

  /**
   * @param modificationType
   *          the modificationType to set
   * @see #modificationType
   */
  public void setModificationType(final String modificationType) {
    this.modificationType = modificationType;
  }

}
