package lcsb.mapviewer.converter.model.celldesigner.structure;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.compartment.Compartment;

/**
 * Class representing CellDesigner {@link Compartment}.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerCompartment extends CellDesignerElement<Compartment>
    implements Comparable<CellDesignerCompartment> {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  /**
   * Identifier of the compartment. Unique in the model
   */
  private String compartmentId = "";

  /**
   * List of aggregated elements.
   */
  private Set<CellDesignerElement<?>> elements = new HashSet<>();

  /**
   * Default constructor with identifier given as a parameter.
   * 
   * @param id
   *          identifier of the compartment
   */
  public CellDesignerCompartment(final String id) {
    compartmentId = id;
    setName(id);
  }

  /**
   * Default constructor.
   */
  public CellDesignerCompartment() {
    super();
  }

  /**
   * Constructor that initialize the data using compartment given as a parameter.
   * 
   * @param compartment
   *          object which is used for data initialization
   */
  public CellDesignerCompartment(final CellDesignerCompartment compartment) {
    super(compartment);
    setElementId(compartment.getElementId());
    for (CellDesignerElement<?> element : compartment.getElements()) {
      addElement(element.copy());
    }
  }

  @Override
  public int compareTo(final CellDesignerCompartment param) {
    if (param == null) {
      return -1;
    }
    if (!compartmentId.equals(param.getElementId())) {
      return compartmentId.compareTo(param.getElementId());
    }
    if (!getName().equals(param.getName())) {
      return getName().compareTo(param.getName());
    }
    return 0;
  }

  @Override
  public int hashCode() {
    String result = compartmentId + "_" + getName();
    return result.hashCode();
  }

  @Override
  public boolean equals(final Object param) {
    if (this == param) {
      return true;
    }
    if (!(param instanceof CellDesignerCompartment)) {
      return false;
    }

    return compareTo((CellDesignerCompartment) param) == 0;
  }

  @Override
  public String getElementId() {
    return compartmentId;
  }

  @Override
  public void setElementId(final String id) {
    this.compartmentId = id;
  }

  @Override
  public CellDesignerCompartment copy() {
    if (this.getClass() != CellDesignerCompartment.class) {
      throw new NotImplementedException("this method should be overloaded");
    }
    return new CellDesignerCompartment(this);
  }

  @Override
  public Compartment createModelElement(final String aliasId) {
    Compartment result = new Compartment(aliasId);
    super.setModelObjectFields(result);
    if (elements.size() > 0) {
      throw new NotImplementedException();
    }
    return result;
  }

  /**
   * Adds element.
   *
   * @param element
   *          element to add
   */
  public void addElement(final CellDesignerElement<?> element) {
    elements.add(element);
    element.setParent(this);
  }

  /**
   *
   * @return {@link #elements}
   */
  public Set<CellDesignerElement<?>> getElements() {
    return elements;
  }

}
