package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.geometry.LineTransformation;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.model.graphics.PolylineData;

/**
 * Class with basic operations on lines with special functions for CellDesigner
 * format.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerLineTransformation extends LineTransformation {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  /**
   * Class used for transformations on {@link Point2D} class.
   */
  private static PointTransformation pt = new PointTransformation();

  /**
   * Transform data in CellDesigner format into typical x,y coordinates for all
   * points.
   * <p>
   * CellDesigner format stores coordinates of a line in a different base. In
   * this base we have two points that define a base: vector between these
   * points is 1 unit in first coordinate, and orthogonal vector to the one
   * mentioned before is 1 unit in the second coordinate. Moreover, these two
   * points defining base belong to this line.
   * </p>
   * 
   * @param startPoint
   *          first point defining CellDesigner base
   * @param endPoint
   *          second point defining CellDesigner base
   * @param midPoints
   *          points defining line
   * @return line in standard x,y format
   */
  public List<Point2D> getLinePointsFromPoints(final Point2D startPoint, final Point2D endPoint,
      final List<Point2D> midPoints) {
    if (!pt.isValidPoint(startPoint)) {
      throw new InvalidArgumentException("Invalid start point: " + startPoint);
    }
    if (!pt.isValidPoint(endPoint)) {
      throw new InvalidArgumentException("Invalid end point: " + endPoint);
    }
    List<Point2D> result = new ArrayList<>();

    double dx1 = endPoint.getX() - startPoint.getX();
    double dy1 = endPoint.getY() - startPoint.getY();

    double dx2 = -dy1;
    double dy2 = dx1;

    double distDx = dx1 * dx1 + dy1 * dy1;

    // this is special case if end and start point are equal then CellDesigner
    // uses
    // relative coordinates
    if (distDx <= Configuration.EPSILON) {
      dx1 = 1;
      dy2 = 1;
    }

    result.add(new Point2D.Double(startPoint.getX(), startPoint.getY()));

    if (midPoints != null) {
      for (final Point2D p : midPoints) {
        if (!pt.isValidPoint(p)) {
          throw new InvalidArgumentException("Invalid point: " + p);
        }

        double x = startPoint.getX() + dx1 * p.getX() + dx2 * p.getY();
        double y = startPoint.getY() + dy1 * p.getX() + dy2 * p.getY();
        result.add(new Point2D.Double(x, y));
      }
    }

    result.add(new Point2D.Double(endPoint.getX(), endPoint.getY()));
    return result;

  }

  /**
   * Transform line of typical x,y coordinates into coordinates used by
   * CellDesigner.
   * <p>
   * CellDesigner format stores coordinates of a line in a different base. In
   * this base we have two points that define a base: vector between these
   * points is 1 unit in first coordinate, and orthogonal vector to the one
   * mentioned before is 1 unit in the second coordinate. These two points are
   * the first and the last.
   * </p>
   * 
   * @param line
   *          line to be transformed into CellDesigner format
   * @return points of the line in CellDesigner format (without the first and
   *         the last)
   */
  public List<Point2D> getPointsFromLine(final PolylineData line) {
    if (line == null) {
      throw new InvalidArgumentException("Line cannot be null.");
    }
    if (line.getLines().size() == 0) {
      throw new InvalidArgumentException("Invalid line passed as an argument.");
    }

    double ax = line.getStartPoint().getX();
    double ay = line.getStartPoint().getY();

    double bx = line.getEndPoint().getX();
    double by = line.getEndPoint().getY();

    double dx = bx - ax;
    double dy = by - ay;

    List<Point2D> result = new ArrayList<Point2D>();

    if (dy * dy + dx * dx == 0) {
      // this is special case - if start and end point are equals then cell
      // designer
      // requires relative coordinates
      for (int i = 1; i < line.getLines().size(); i++) {
        double ox = line.getLines().get(i).getX1();
        double oy = line.getLines().get(i).getY1();

        result.add(new Point2D.Double(ox - ax, oy - ay));
      }
    } else {
      for (int i = 1; i < line.getLines().size(); i++) {
        double ox = line.getLines().get(i).getX1();
        double oy = line.getLines().get(i).getY1();

        double py = (dy * (ax - ox) + dx * (oy - ay)) / (dy * dy + dx * dx);
        double px = (dx * (ox - ax) + dy * (oy - ay)) / (dx * dx + dy * dy);
        result.add(new Point2D.Double(px, py));
      }
    }
    return result;
  }
}
