package lcsb.mapviewer.converter.model.celldesigner.reaction;

import org.w3c.dom.Node;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * Parser used to transform CellDesigner xml node into Reaction. It allows also
 * to transform {@link Reaction} into CellDesigner node.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactionXmlParser {

  /**
   * Class that allows to export data into CellDesigner xml node.
   */
  private ReactionToXml reactionToXml;

  /**
   * Class that allows to import data from CellDesigner xml node.
   */
  private ReactionFromXml reactionFromXml;

  /**
   * Default constructor. Model is required because some nodes require access to
   * other parts of the model.
   * 
   * @param sbgn
   *          Should SBGN standard be used
   * @param elements
   *          collection of {@link CellDesignerElement cell designer elements}
   *          parsed from xml
   */
  public ReactionXmlParser(final CellDesignerElementCollection elements, final boolean sbgn, final boolean oldCellDesignerVersion) {
    reactionToXml = new ReactionToXml(elements, sbgn);
    reactionFromXml = new ReactionFromXml(elements, sbgn, oldCellDesignerVersion);
  }

  /**
   * Returns {@link Reaction} object from CellDesigner xml node.
   * 
   * @param node
   *          xml node
   * @param model
   *          model where the reaction is placed
   * @return reaction from xml node
   * @throws ReactionParserException
   *           thrown when the xml is invalid
   * @throws InvalidXmlSchemaException
   *           thrown when node is invalid xml
   */
  public Reaction getReaction(final Node node, final Model model) throws ReactionParserException, InvalidXmlSchemaException {
    return reactionFromXml.getReaction(node, model);
  }

  /**
   * Transform reaction into CellDesigner xml representation.
   * 
   * @param reaction
   *          reaction to transform
   * @return xml representation of reaction
   */
  public String toXml(final Reaction reaction) throws InconsistentModelException {
    return reactionToXml.toXml(reaction);
  }
}
