package lcsb.mapviewer.converter.model.celldesigner.function;

import java.util.HashSet;
import java.util.Set;

import org.w3c.dom.Node;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;

public class FunctionCollectionXmlParser {

  private FunctionXmlParser functionParser = new FunctionXmlParser();

  public Set<SbmlFunction> parseXmlFunctionCollection(final Node functionsNode) throws InvalidXmlSchemaException {
    Set<SbmlFunction> result = new HashSet<>();
    for (final Node node : XmlParser.getNodes("functionDefinition", functionsNode.getChildNodes())) {
      result.add(functionParser.parseFunction(node));
    }
    return result;
  }

  public String toXml(final Set<SbmlFunction> functions) {
    StringBuilder result = new StringBuilder();
    if (functions.size() > 0) {
      result.append("<listOfFunctionDefinitions>\n");
      for (final SbmlFunction function : functions) {
        result.append(functionParser.toXml(function));
      }

      result.append("</listOfFunctionDefinitions>\n");
    }
    return result.toString();
  }

}
