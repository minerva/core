package lcsb.mapviewer.converter.model.celldesigner.geometry;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Class that provides CellDesigner specific graphical information for Drug.
 * It's used for conversion from xml to normal x,y coordinates.
 * 
 * @author Piotr Gawron
 * 
 */
public class DrugCellDesignerAliasConverter extends AbstractCellDesignerAliasConverter<Species> {

  /**
   * How big should be the arc in rectangle for drug representation.
   */
  private static final int RECTANGLE_CORNER_ARC_SIZE = 40;

  /**
   * Default constructor.
   *
   * @param sbgn
   *          Should the converter use sbgn standard
   */
  protected DrugCellDesignerAliasConverter(final boolean sbgn) {
    super(sbgn);
  }

  @Override
  public Point2D getPointCoordinates(final Species alias, final CellDesignerAnchor anchor) {
    if (invalidAnchorPosition(alias, anchor)) {
      return alias.getCenter();
    }
    List<Point2D> list = getDrugPoints(alias);

    return getPolygonTransformation().getPointOnPolygonByAnchor(list, anchor);
  }

  @Override
  protected PathIterator getBoundPathIterator(final Species alias) {
    return getDrugShape(alias).getPathIterator(new AffineTransform());
  }

  /**
   * Returns shape of the Drug as a list of points.
   * 
   * @param alias
   *          alias for which we are looking for a border
   * @return list of points defining border of the given alias
   */
  protected List<Point2D> getDrugPoints(final Species alias) {
    List<Point2D> list = new ArrayList<>();

    double x = alias.getX();
    double y = alias.getY();
    double width = alias.getWidth();
    double height = alias.getHeight();

    double cutSize = width * 1 / 5;

    double arcWidth = Math.min(Math.min(height / 2, width / 2), RECTANGLE_CORNER_ARC_SIZE);

    double distFromCorner = arcWidth * (1 - Math.sin(Math.PI / 4));

    // CHECKSTYLE:OFF
    list.add(new Point2D.Double(x, y + height / 2));
    list.add(new Point2D.Double(x + distFromCorner, y + distFromCorner));
    list.add(new Point2D.Double(x + cutSize, y));
    list.add(new Point2D.Double(x + (cutSize + width / 2) / 2, y));
    list.add(new Point2D.Double(x + width / 2, y));
    list.add(new Point2D.Double(x + (width - cutSize + width / 2) / 2, y));
    list.add(new Point2D.Double(x + width - cutSize, y));
    list.add(new Point2D.Double(x + width - distFromCorner, y + distFromCorner));
    list.add(new Point2D.Double(x + width, y + height / 2));
    list.add(new Point2D.Double(x + width - distFromCorner, y + height - distFromCorner));
    list.add(new Point2D.Double(x + width - cutSize, y + height));
    list.add(new Point2D.Double(x + (width - cutSize + width / 2) / 2, y + height));
    list.add(new Point2D.Double(x + width / 2, y + height));
    list.add(new Point2D.Double(x + (cutSize + width / 2) / 2, y + height));
    list.add(new Point2D.Double(x + cutSize, y + height));
    list.add(new Point2D.Double(x + distFromCorner, y + height - distFromCorner));
    // CHECKSTYLE:ON
    return list;
  }

  /**
   * Returns shape of the Drug.
   * 
   * @param alias
   *          alias for which we are looking for a border
   * @return Shape object defining given alias
   */
  private Shape getDrugShape(final Element alias) {
    return new RoundRectangle2D.Double(alias.getX(), alias.getY(), alias.getWidth(), alias.getHeight(),
        RECTANGLE_CORNER_ARC_SIZE, RECTANGLE_CORNER_ARC_SIZE);
  }

}
