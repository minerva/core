package lcsb.mapviewer.converter.model.celldesigner.compartment;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.converter.model.celldesigner.CellDesignerParserException;
import lcsb.mapviewer.model.map.compartment.Compartment;

/**
 * Exception thrown when problem with parsing {@link Compartment} is
 * encountered.
 * 
 * @author Piotr Gawron
 * 
 */
public class CompartmentParserException extends CellDesignerParserException {
  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();
  /**
   * Identifier of compartment that was a reason for this exception.
   */
  private String compartmentId;

  /**
   * Default constructor.
   * 
   * @param message
   *          message thrown
   * @param compartmentId
   *          {@link #compartmentId}
   */
  public CompartmentParserException(final String message, final String compartmentId) {
    super(message);
    this.compartmentId = compartmentId;
  }

  /**
   * Default constructor.
   * 
   * @param message
   *          message thrown
   * @param compartment
   *          {@link #compartmentId}
   */
  public CompartmentParserException(final String message, final Compartment compartment) {
    super(message);
    if (compartment != null) {
      setCompartmentId(compartment.getElementId());
    }
  }

  /**
   * Default constructor.
   * 
   * @param message
   *          message thrown
   * @param compartment
   *          {@link #compartmentId}
   * @param e
   *          super exception
   */
  public CompartmentParserException(final String message, final Compartment compartment, final Exception e) {
    super(message, e);
    if (compartment != null) {
      setCompartmentId(compartment.getElementId());
    }
  }

  /**
   * Default constructor.
   * 
   * @param compartment
   *          {@link #compartmentId}
   * @param e
   *          super exception
   */
  public CompartmentParserException(final Compartment compartment, final Throwable e) {
    super(e);
    if (compartment != null) {
      setCompartmentId(compartment.getElementId());
    }
  }

  /**
   * Default constructor.
   * 
   * @param compartmentId
   *          {@link #compartmentId}
   * @param e
   *          super exception
   */
  public CompartmentParserException(final String compartmentId, final Throwable e) {
    super(e);
    setCompartmentId(compartmentId);
  }

  /**
   * @return the compartmentId
   * @see #compartmentId
   */
  public String getCompartmentId() {
    return compartmentId;
  }

  /**
   * @param compartmentId
   *          the compartmentId to set
   * @see #compartmentId
   */
  public void setCompartmentId(final String compartmentId) {
    this.compartmentId = compartmentId;
  }

  @Override
  public String getMessageContext() {
    if (compartmentId != null) {
      return "[Compartment: " + compartmentId + "]";
    }
    return null;
  }

}
