package lcsb.mapviewer.converter.model.celldesigner.species;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CommonXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerAntisenseRna;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerCompartment;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerComplexSpecies;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGene;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerRna;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerSimpleMolecule;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerSpecies;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.SpeciesState;
import lcsb.mapviewer.converter.model.celldesigner.unit.UnitXmlParser;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitTypeFactor;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.PositionToCompartment;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Collection;

/**
 * Parser for sbml part of CellDesigner format. It's used for retrieving Species
 * from SBML.
 *
 * @author Piotr Gawron
 */
public class SpeciesSbmlParser extends AbstractElementXmlParser<CellDesignerSpecies<?>, Species> {

  private static final DoubleComparator doubleComparator = new DoubleComparator();

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed from
   * xml.
   */
  private final CellDesignerElementCollection elements;

  private final Collection<SbmlUnit> units;

  private final UnitXmlParser unitXmlParser = new UnitXmlParser();

  private final ProteinXmlParser proteinXmlParser;

  /**
   * Default constructor. Model is required because some nodes require access to
   * other parts of the model.
   *
   * @param elements collection of {@link CellDesignerElement cell designer elements}
   *                 parsed from xml
   */
  public SpeciesSbmlParser(final CellDesignerElementCollection elements, final Collection<SbmlUnit> units, final ProteinXmlParser proteinXmlParser) {
    this.elements = elements;
    this.units = units;
    this.proteinXmlParser = proteinXmlParser;
  }

  @Override
  public Pair<String, CellDesignerSpecies<?>> parseXmlElement(final Node spieciesNode) throws InvalidXmlSchemaException {

    NodeList list = spieciesNode.getChildNodes();

    Node annotationNode = null;
    Node notesNode = null;
    for (int i = 0; i < list.getLength(); i++) {
      final Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equals("annotation")) {
          annotationNode = node;
        } else if (node.getNodeName().equals("notes")) {
          notesNode = node;
        } else {
          throw new InvalidXmlSchemaException("Unknown element of annotation " + node.getNodeName());
        }
      }
    }

    if (annotationNode == null) {
      throw new InvalidXmlSchemaException("No annotation node in SBML/model/listOfSpecies/species");
    }

    list = annotationNode.getChildNodes();
    Node extensionNode = null;
    Node rdfNode = null;
    Node speciesIdentity = null;
    Node positionToCompartment = null;

    for (int i = 0; i < list.getLength(); i++) {
      final Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equals("celldesigner:extension")) {
          extensionNode = node;
        } else if (node.getNodeName().equals("rdf:RDF")) {
          rdfNode = node;
        } else if (node.getNodeName().equals("celldesigner:positionToCompartment")) {
          positionToCompartment = node;
        } else if (node.getNodeName().equals("celldesigner:speciesIdentity")) {
          speciesIdentity = node;
        } else if (node.getNodeName().equals("celldesigner:listOfCatalyzedReactions")) {
          // we can ignore it - every node on the map contains information about
          // itself
          continue;
        } else {
          logger.debug(XmlParser.nodeToString(spieciesNode, true));
          throw new InvalidXmlSchemaException("Unknown element of annotation " + node.getNodeName());
        }
      }
    }

    if (extensionNode != null) {
      list = extensionNode.getChildNodes();
      for (int i = 0; i < list.getLength(); i++) {
        final Node node = list.item(i);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          if (node.getNodeName().equals("celldesigner:speciesIdentity")) {
            speciesIdentity = node;
          } else if (node.getNodeName().equals("celldesigner:positionToCompartment")) {
            positionToCompartment = node;
          } else if (node.getNodeName().equals("celldesigner:listOfCatalyzedReactions")) {
            // we can ignore it - every node on the map contains information
            // about
            // itself
            continue;
          } else {
            throw new InvalidXmlSchemaException("[" + XmlParser.getNodeAttr("id", spieciesNode)
                + "]\tUnknown element of celldesigner:extension " + node.getNodeName());
          }
        }
      }
    } else {
      // speciesIdentity was placed in the parent node in CD 2.5
      if (speciesIdentity == null) {
        throw new InvalidXmlSchemaException(
            "No celldesigner:extension node in SBML/model/listOfSpecies/species/annotation");
      }
    }

    if (speciesIdentity == null) {
      throw new InvalidXmlSchemaException(
          "No celldesigner:speciesIdentity node in SBML/model/listOfSpecies/species/annotation/extension");
    }

    final Pair<String, CellDesignerSpecies<?>> result = parseSpeciesIdentity(speciesIdentity);

    final CellDesignerSpecies<?> species = result.getRight();
    if (positionToCompartment != null) {
      final PositionToCompartment position = PositionToCompartment.getByString(XmlParser.getNodeValue(positionToCompartment));
      if (position == null) {
        throw new InvalidXmlSchemaException(
            "Unknown position on compartment: " + XmlParser.getNodeValue(positionToCompartment));
      }
      species.setPositionToCompartment(position);
    }

    // we ignore metaid - it's useless and obstruct data model
    // species.setMetaId(XmlParser.getNodeAttr("metaid", spieciesNode));
    species.setElementId(XmlParser.getNodeAttr("id", spieciesNode));
    species.setName(decodeName(XmlParser.getNodeAttr("name", spieciesNode)));
    species.setInitialAmount(XmlParser.getNodeAttr("initialAmount", spieciesNode));
    final String substanceUnits = XmlParser.getNodeAttr("substanceUnits", spieciesNode);
    if (substanceUnits != null && !substanceUnits.isEmpty()) {
      SbmlUnit sbmlUnit = null;
      for (final SbmlUnit unit : units) {
        if (unit.getUnitId().equals(substanceUnits)) {
          sbmlUnit = unit;
        }
      }
      if (sbmlUnit != null) {
        species.setSubstanceUnits(sbmlUnit);
      } else {
        final SbmlUnitType unitType = SbmlUnitType.valueOf(substanceUnits.toUpperCase());
        for (final SbmlUnit unit : units) {
          if (unit.getUnitTypeFactors().size() == 1) {
            final SbmlUnitTypeFactor factor = unit.getUnitTypeFactors().iterator().next();
            if (doubleComparator.compare(factor.getMultiplier(), 1.0) == 0
                && factor.getScale() == 1
                && factor.getExponent() == 1
                && factor.getUnitType() == unitType) {
              sbmlUnit = unit;
            }
          }
        }
        if (sbmlUnit == null) {
          sbmlUnit = new SbmlUnit(substanceUnits + "_id");
          sbmlUnit.setName(unitType.getCommonName());
          sbmlUnit.addUnitTypeFactor(new SbmlUnitTypeFactor(unitType, 1, 1, 1));
          logger.debug("Adding sbml unit for type: " + unitType);
          units.add(sbmlUnit);
        }
        species.setSubstanceUnits(sbmlUnit);
      }
    }
    species.setInitialAmount(XmlParser.getNodeAttr("initialAmount", spieciesNode));
    species.setInitialConcentration(XmlParser.getNodeAttr("initialConcentration", spieciesNode));
    if (species.getInitialAmount() == null && species.getInitialConcentration() == null) {
      species.setInitialConcentration(0.0);
    }
    species.setCharge(XmlParser.getNodeAttr("charge", spieciesNode));
    species.setOnlySubstanceUnits(XmlParser.getNodeAttr("hasOnlySubstanceUnits", spieciesNode));
    species.setBoundaryCondition(XmlParser.getNodeAttr("boundaryCondition", spieciesNode));
    species.setConstant(XmlParser.getNodeAttr("constant", spieciesNode));

    final CellDesignerCompartment compartment = elements
        .getElementByElementId(XmlParser.getNodeAttr("compartment", spieciesNode));
    if (compartment != null) {
      species.setParent(compartment);
    }

    if (notesNode != null) {
      species.setNotes(getRap().getNotes(notesNode));
    }

    if (rdfNode != null) {
      final XmlAnnotationParser xmlAnnotationParser = new XmlAnnotationParser(
          CommonXmlParser.RELATION_TYPES_SUPPORTED_BY_CELL_DESIGNER);
      species.addMiriamData(xmlAnnotationParser.parseRdfNode(rdfNode, new LogMarker(ProjectLogEntryType.PARSING_ISSUE,
          species.getClass().getSimpleName(), species.getElementId(), "")));
    }

    return result;
  }

  @Override
  public String toXml(final Species species) {
    final StringBuilder builder = new StringBuilder();
    final StringBuilder attributesBuilder = new StringBuilder();
    attributesBuilder.append(" name=\"" + XmlParser.escapeXml(encodeName(species.getName())) + "\"");
    attributesBuilder.append(" id=\"" + elements.getElementId(species) + "\"");
    attributesBuilder.append(" metaid=\"" + elements.getElementId(species) + "\"");
    if (species.getInitialAmount() != null) {
      attributesBuilder.append(" initialAmount=\"" + species.getInitialAmount() + "\"");
    }
    if (species.getInitialConcentration() != null) {
      attributesBuilder.append(" initialConcentration=\"" + species.getInitialConcentration() + "\"");
    }
    if (species.getCharge() != null) {
      attributesBuilder.append(" charge=\"" + species.getCharge() + "\"");
    }
    if (species.hasOnlySubstanceUnits() != null) {
      attributesBuilder.append(" hasOnlySubstanceUnits=\"" + species.hasOnlySubstanceUnits() + "\"");
    }
    if (species.getSubstanceUnits() != null) {
      if (unitXmlParser.isSupported(species.getSubstanceUnits())) {
        attributesBuilder
            .append(" substanceUnits=\"" + species.getSubstanceUnits().getUnitId().toLowerCase() + "\"");
      }
    }
    if (species.getConstant() != null) {
      attributesBuilder.append(" constant=\"" + species.getConstant().toString().toLowerCase() + "\"");
    }
    if (species.getBoundaryCondition() != null) {
      attributesBuilder
          .append(" boundaryCondition=\"" + species.getBoundaryCondition().toString().toLowerCase() + "\"");
    }

    Compartment comp = null;
    // we have to exclude artificial compartment aliases, because they aren't
    // exported to CellDesigner file
    if (species.getCompartment() != null && !(species.getCompartment() instanceof PathwayCompartment)) {
      comp = species.getCompartment();
    } else if (species.getComplex() == null) {
      final ModelData modelData = species.getModelData();
      if (modelData != null) {
        for (final Compartment compartment : modelData.getModel().getCompartments()) {
          if (!(compartment instanceof PathwayCompartment) && compartment.cross(species)) {
            if (comp == null) {
              comp = compartment;
            } else if (comp.getSize() > compartment.getSize()) {
              comp = compartment;
            }
          }
        }
      }
    }

    if (comp != null) {
      attributesBuilder.append(" compartment=\"" + elements.getElementId(comp) + "\"");
    } else {
      attributesBuilder.append(" compartment=\"default\"");
    }
    builder.append("<species " + attributesBuilder + ">");

    if ((species.getNotes() != null && !species.getNotes().equals("")) || species.getSymbol() != null
        || species.getFullName() != null || species.getSynonyms().size() > 0) {
      builder.append("<notes>");
      builder.append("<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><title/></head><body>");
      final RestAnnotationParser rap = new RestAnnotationParser();
      builder.append(rap.createAnnotationString(species));
      if (species.getNotes() != null) {
        builder.append(species.getNotes());
      }
      builder.append("</body></html>");
      builder.append("</notes>\n");
    }
    builder.append("<annotation>");
    builder.append("<celldesigner:extension>");
    if (species.getPositionToCompartment() != null) {
      builder.append("<celldesigner:positionToCompartment>" + species.getPositionToCompartment().getStringName()
          + "</celldesigner:positionToCompartment>");
    }
    builder.append(speciesIdentityToXml(species));

    builder.append("</celldesigner:extension>\n");

    final XmlAnnotationParser xmlAnnotationParser = new XmlAnnotationParser(
        CommonXmlParser.RELATION_TYPES_SUPPORTED_BY_CELL_DESIGNER);
    builder.append(xmlAnnotationParser.dataSetToXmlString(species.getMiriamData(), elements.getElementId(species)));

    builder.append("</annotation>\n");

    builder.append("</species>\n");
    return builder.toString();
  }

  /**
   * Creates speciesIdentity xml node for the {@link CellDesignerSpecies}.
   *
   * @param species species to be transformed into xml
   * @return speciesIdentity xml node
   */
  String speciesIdentityToXml(final Species species) {
    SpeciesState state = null;
    final StringBuilder sb = new StringBuilder();
    String classType = "";
    final SpeciesMapping mapping = SpeciesMapping.getMappingByModelClass(species.getClass());
    if (mapping != null) {
      classType = mapping.getCellDesignerString();
    } else {
      throw new InvalidArgumentException("Invalid species class type: " + species.getClass().getName());
    }
    sb.append("<celldesigner:speciesIdentity>");
    sb.append("<celldesigner:class>" + classType + "</celldesigner:class>\n");
    if (species instanceof Rna) {
      sb.append("<celldesigner:rnaReference>r_" + elements.getElementId(species) + "</celldesigner:rnaReference>\n");
    } else if (species instanceof Protein) {
      state = new SpeciesState(species, proteinXmlParser.getProteinData(species.getName()));
      sb.append(
          "<celldesigner:proteinReference>" + proteinXmlParser.getProteinId((Protein) species) + "</celldesigner:proteinReference>\n");
    } else if (species instanceof Gene) {
      sb.append("<celldesigner:geneReference>g_" + elements.getElementId(species) + "</celldesigner:geneReference>\n");
    } else if (species instanceof AntisenseRna) {
      sb.append("<celldesigner:antisensernaReference>ar_" + elements.getElementId(species)
          + "</celldesigner:antisensernaReference>\n");
    }
    if (state == null) {
      state = new SpeciesState(species);
    }

    sb.append(speciesStateToXml(state));
    if (!species.getName().isEmpty()) {
      sb.append("<celldesigner:name>" + XmlParser.escapeXml(encodeName(species.getName())) + "</celldesigner:name>\n");
    }

    final Boolean hypothetical = species.getHypothetical();
    if (hypothetical != null) {
      sb.append("<celldesigner:hypothetical>" + hypothetical + "</celldesigner:hypothetical>\n");
    }

    sb.append("</celldesigner:speciesIdentity>\n");
    return sb.toString();
  }

  /**
   * Creates CellDesigner species state into xml node.
   *
   * @param state state of the species.
   * @return CellDesigner xml node used for description of the state
   */
  String speciesStateToXml(final SpeciesState state) {
    final StringBuilder sb = new StringBuilder();
    if (state.getHomodimer() != 1) {
      sb.append("<celldesigner:homodimer>" + state.getHomodimer() + "</celldesigner:homodimer>\n");
    }
    if (state.getStructuralState() != null && !state.getStructuralState().equals("")) {
      sb.append("<celldesigner:listOfStructuralStates>\n");
      sb.append("<celldesigner:structuralState structuralState=\"" + XmlParser.escapeXml(state.getStructuralState()) + "\">"
          + XmlParser.escapeXml(state.getStructuralState()) + "</celldesigner:structuralState>");
      sb.append("</celldesigner:listOfStructuralStates>\n");
    }
    if (state.getModifications().size() > 0) {
      sb.append("<celldesigner:listOfModifications>\n");
      int counter = 0;
      for (final CellDesignerModificationResidue mr : state.getModifications()) {
        sb.append(modificationResidueToXml(mr, counter++));
      }
      sb.append("</celldesigner:listOfModifications>\n");
    }
    if (sb.length() > 0) {
      sb.insert(0, "<celldesigner:state>\n");
      sb.append("</celldesigner:state>\n");
    }
    return sb.toString();
  }

  public Pair<String, CellDesignerSpecies<?>> parseSpeciesIdentity(final Node rootNode) throws InvalidXmlSchemaException {
    final NodeList nodes = rootNode.getChildNodes();
    CellDesignerSpecies<?> result = new CellDesignerSpecies<Species>();
    final Node classNode = XmlParser.getNode("celldesigner:class", nodes);
    if (classNode == null) {
      throw new InvalidXmlSchemaException("Species node in Sbml model doesn't contain node \"celldesigner:class\".");
    } else {
      final String value = XmlParser.getNodeValue(classNode);
      final SpeciesMapping mapping = SpeciesMapping.getMappingByString(value);
      if (mapping != null) {
        result = mapping.createSpecies(result);
      } else {
        throw new InvalidXmlSchemaException("Species node in Sbml model is of unknown type: " + value);
      }
    }
    String identifier = "";
    for (int x = 0; x < nodes.getLength(); x++) {
      final Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:class")) {
          continue;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:proteinReference")) {
          if (result instanceof CellDesignerProtein) {
            identifier = XmlParser.getNodeValue(node);
          } else {
            throw new InvalidXmlSchemaException(
                "Wrong class type for protein reference: " + result.getClass().getName());
          }
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:geneReference")) {
          if (result instanceof CellDesignerGene) {
            identifier = XmlParser.getNodeValue(node);
          } else {
            throw new InvalidXmlSchemaException("Wrong class type for gene reference: " + result.getClass().getName());
          }
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:rnaReference")) {
          if (result instanceof CellDesignerRna) {
            identifier = XmlParser.getNodeValue(node);
          } else {
            throw new InvalidXmlSchemaException("Wrong class type for rna reference: " + result.getClass().getName());
          }
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:antisensernaReference")) {
          if (result instanceof CellDesignerAntisenseRna) {
            identifier = XmlParser.getNodeValue(node);
          } else {
            throw new InvalidXmlSchemaException(
                "Wrong class type for antisense rna reference: " + result.getClass().getName());
          }
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:hypothetical")) {
          result.setHypothetical(XmlParser.getNodeValue(node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:name")) {
          result.setName(decodeName(XmlParser.getNodeValue(node)));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:state")) {
          final SpeciesState state = parseXmlSpeciesState(node);
          processStateDataInSpecies(result, state);
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:speciesIdentity: " + node.getNodeName());
        }
      }

    }
    return new Pair<String, CellDesignerSpecies<?>>(identifier, result);
  }

  /**
   * Set structural state to species. StructuralState is a generic class with some
   * information about the species element.
   *
   * @param state   state to be set
   * @param species where the data should be saved
   */
  void processStateDataInSpecies(final CellDesignerSpecies<?> species, final SpeciesState state) {
    species.setHomodimer(state.getHomodimer());

    if (species instanceof CellDesignerComplexSpecies) {
      final CellDesignerComplexSpecies complex = (CellDesignerComplexSpecies) species;
      complex.setStructuralState(state.getStructuralState());
      if (state.getModifications().size() != 0) {
        throw new NotImplementedException("Modification not supported in Complex");
      }
    } else if (species instanceof CellDesignerGene) {
      final CellDesignerGene gene = (CellDesignerGene) species;
      if (state.getStructuralState() != null) {
        throw new NotImplementedException("StructuralState not supported in Gene");
      }
      for (final CellDesignerModificationResidue mr : state.getModifications()) {
        gene.addModificationResidue(mr);
      }
    } else if (species instanceof CellDesignerProtein) {
      final CellDesignerProtein<?> protein = (CellDesignerProtein<?>) species;
      protein.setStructuralState(state.getStructuralState());
      for (final CellDesignerModificationResidue mr : state.getModifications()) {
        protein.addModificationResidue(mr);
      }
    } else if (species instanceof CellDesignerRna) {
      final CellDesignerRna rna = (CellDesignerRna) species;
      if (state.getStructuralState() != null && !state.getStructuralState().isEmpty()) {
        throw new NotImplementedException("Structural state not supported in RNA");
      }

      for (final CellDesignerModificationResidue mr : state.getModifications()) {
        rna.addRegion(mr);
      }
    } else if (species instanceof CellDesignerSimpleMolecule) {
      if (state.getStructuralState() != null && !state.getStructuralState().isEmpty()) {
        throw new NotImplementedException("Structural state not supported in SimpleMolecule");
      }
      if (state.getModifications().size() != 0) {
        throw new NotImplementedException("Modification not supported in SimpleMolecule");
      }

    } else if (species instanceof CellDesignerAntisenseRna) {
      final CellDesignerAntisenseRna antisenseRna = (CellDesignerAntisenseRna) species;
      if (state.getStructuralState() != null && !state.getStructuralState().isEmpty()) {
        throw new NotImplementedException("Structural state not supported in AntisenseRna");
      }

      for (final CellDesignerModificationResidue mr : state.getModifications()) {
        antisenseRna.addRegion(mr);
      }
    } else {
      if (state.getStructuralState() != null && !state.getStructuralState().isEmpty()) {
        throw new NotImplementedException("Structural state not supported in " + species.getClass().getSimpleName());
      }
      if (state.getModifications().size() != 0) {
        throw new NotImplementedException("Modification not supported in " + species.getClass().getSimpleName());
      }
    }

  }

  /**
   * Creates {@link SpeciesState} from xml node.
   *
   * @param stateNode xml node
   * @return {@link SpeciesState} retrieved from xml node
   * @throws InvalidXmlSchemaException thrown when xml node contains data that is not supported by xml
   *                                   schema
   */
  private SpeciesState parseXmlSpeciesState(final Node stateNode) throws InvalidXmlSchemaException {
    final NodeList nodes = stateNode.getChildNodes();
    final SpeciesState result = new SpeciesState();
    for (int x = 0; x < nodes.getLength(); x++) {
      final Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:homodimer")) {
          result.setHomodimer(XmlParser.getNodeValue(node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfStructuralStates")) {
          final NodeList structureNodeList = node.getChildNodes();
          for (int y = 0; y < structureNodeList.getLength(); y++) {
            final Node structureNode = structureNodeList.item(y);
            if (structureNode.getNodeType() == Node.ELEMENT_NODE) {
              if (structureNode.getNodeName().equalsIgnoreCase("celldesigner:structuralState")) {
                final String state = XmlParser.getNodeAttr("structuralState", structureNode);
                result.setStructuralState(state);
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfStructuralStates: " + node.getNodeName());
              }

            }

          }

        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfModifications")) {
          final NodeList modificationNodeList = node.getChildNodes();
          for (int y = 0; y < modificationNodeList.getLength(); y++) {
            final Node structureNode = modificationNodeList.item(y);
            if (structureNode.getNodeType() == Node.ELEMENT_NODE) {
              if (structureNode.getNodeName().equalsIgnoreCase("celldesigner:modification")) {
                final CellDesignerModificationResidue modification = getSpeciesModification(structureNode);

                result.addModificationResidue(modification);
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfModifications: " + node.getNodeName());
              }
            }
          }
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:state: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Creates {@link CellDesignerModificationResidue} from the appropriate xml
   * node.
   *
   * @param rootNode xml node
   * @return {@link CellDesignerModificationResidue} retrieved from the node
   * @throws InvalidXmlSchemaException thrown when xml node contains data that is not supported by xml
   *                                   schema
   */
  private CellDesignerModificationResidue getSpeciesModification(final Node rootNode) throws InvalidXmlSchemaException {
    final CellDesignerModificationResidue modification = new CellDesignerModificationResidue();
    modification.setIdModificationResidue(XmlParser.getNodeAttr("residue", rootNode));
    final String state = XmlParser.getNodeAttr("state", rootNode);
    if (state != null) {
      final ModificationState modificationState = ModificationStateMapping.getStateByName(state);
      if (modificationState == null) {
        throw new InvalidXmlSchemaException("[" + modification.getClass().getSimpleName() + "\t"
            + modification.getIdModificationResidue() + "] Unknown modification state: " + state);
      }
      modification.setState(modificationState);
    }
    modification.setName(XmlParser.getNodeAttr("name", rootNode));
    modification.setSize(XmlParser.getNodeAttr("size", rootNode));
    modification.setAngle(XmlParser.getNodeAttr("pos", rootNode));
    final NodeList list = rootNode.getChildNodes();
    for (int y = 0; y < list.getLength(); y++) {
      final Node node = list.item(y);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        throw new InvalidXmlSchemaException(
            "[" + modification.getClass().getSimpleName() + "\t" + modification.getIdModificationResidue()
                + "] Unknown element of celldesigner:modification: " + node.getNodeName());
      }
    }
    return modification;
  }

  /**
   * Creates xml node representing {@link CellDesignerModificationResidue}.
   *
   * @param mr object to be transformed into xml
   * @return xml node representing param object
   */
  private String modificationResidueToXml(final CellDesignerModificationResidue mr, final int number) {
    String state = "";
    if (mr.getState() != null) {
      state = ModificationStateMapping.getStateName(mr.getState());
    }
    if (state == null || state.equals("")) {
      return "";
    }
    return "<celldesigner:modification residue=\"" + elements.getModificationResidueId(mr, number)
        + "\" state=\"" + state + "\"> </celldesigner:modification>\n";
  }
}
