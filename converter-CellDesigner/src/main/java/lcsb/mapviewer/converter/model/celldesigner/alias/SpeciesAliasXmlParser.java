package lcsb.mapviewer.converter.model.celldesigner.alias;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerComplexSpecies;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerSpecies;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.View;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Species;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Parser of CellDesigner xml used for parsing SpeciesAlias.
 *
 * @author Piotr Gawron
 * @see Complex
 */
public class SpeciesAliasXmlParser extends AbstractAliasXmlParser<Species> {
  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed
   * from xml.
   */
  private final CellDesignerElementCollection elements;

  /**
   * Model for which we parse elements.
   */
  private final Model model;

  /**
   * Default constructor with model object for which we parse data.
   *
   * @param model    model for which we parse elements
   * @param elements collection of {@link CellDesignerElement cell designer elements}
   *                 parsed from xml
   */
  public SpeciesAliasXmlParser(final CellDesignerElementCollection elements, final Model model) {
    this.elements = elements;
    this.model = model;
  }

  @Override
  Species parseXmlAlias(final Node aliasNode) throws InvalidXmlSchemaException {

    final String speciesId = XmlParser.getNodeAttr("species", aliasNode);
    final String aliasId = XmlParser.getNodeAttr("id", aliasNode);

    final String errorPrefix = "[" + speciesId + "," + aliasId + "]\t";
    final CellDesignerSpecies<?> species = elements.getElementByElementId(speciesId);
    if (species == null) {
      throw new InvalidXmlSchemaException("Unknown species for alias (speciesId: " + speciesId + ")");
    }
    if (species instanceof CellDesignerComplexSpecies) {
      logger.warn(
          errorPrefix + "Species is defined as a complex, but alias is not a complex. Changing alias to complex.");
    }

    elements.addElement(species, aliasId);
    try {
      final Species result = species.createModelElement(aliasId);

      String state = "usual";
      final NodeList nodes = aliasNode.getChildNodes();
      View usualView = null;
      @SuppressWarnings("unused")
      View briefView = null;
      for (int x = 0; x < nodes.getLength(); x++) {
        final Node node = nodes.item(x);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          if (node.getNodeName().equalsIgnoreCase("celldesigner:activity")) {
            result.setActivity(XmlParser.getNodeValue(node).equalsIgnoreCase("active"));
          } else if (node.getNodeName().equalsIgnoreCase("celldesigner:bounds")) {
            result.setX(XmlParser.getNodeAttr("X", node));
            result.setY(XmlParser.getNodeAttr("Y", node));
          } else if (node.getNodeName().equalsIgnoreCase("celldesigner:font")) {
            result.setFontSize(XmlParser.getNodeAttr("size", node));
          } else if (node.getNodeName().equalsIgnoreCase("celldesigner:view")) {
            state = XmlParser.getNodeAttr("state", node);
          } else if (node.getNodeName().equalsIgnoreCase("celldesigner:usualView")) {
            usualView = getCommonParser().getView(node);
          } else if (node.getNodeName().equalsIgnoreCase("celldesigner:briefView")) {
            briefView = getCommonParser().getView(node);
          } else if (node.getNodeName().equalsIgnoreCase("celldesigner:info")) {
            processAliasState(node, result);
          } else if (node.getNodeName().equalsIgnoreCase("celldesigner:structuralState")) {
            if (species instanceof CellDesignerProtein) {
              ((CellDesignerProtein<?>) species)
                  .setStructuralStateAngle(Double.parseDouble(XmlParser.getNodeAttr("angle", node)));
            }
          } else {
            throw new InvalidXmlSchemaException(
                errorPrefix + "Unknown element of celldesigner:speciesAlias: " + node.getNodeName());
          }
        }
      }

      View view = null;
      if (state.equalsIgnoreCase("usual")) {
        view = usualView;
      } else if (state.equalsIgnoreCase("brief")) {
        view = usualView;
      }

      if (view != null) {
        // inner position defines the position in compartment
        // result.moveBy(view.innerPosition);
        result.setWidth(view.getBoxSize().getWidth());
        if (result instanceof Degraded) {
          result.setHeight(Math.min(view.getBoxSize().getHeight(), view.getBoxSize().getWidth()));
        } else {
          result.setHeight(view.getBoxSize().getHeight());
        }
        result.setLineWidth(view.getSingleLine().getWidth());
        result.setFillColor(view.getColor());
        if (view.getInnerPosition() != null && state.equalsIgnoreCase("brief")) {
          result.setX(result.getX() + view.getInnerPosition().getX());
          result.setY(result.getY() + view.getInnerPosition().getY());
        }
      } else {
        throw new InvalidXmlSchemaException(errorPrefix + "No view in Alias");
      }
      result.setState(state);
      final String compartmentAliasId = XmlParser.getNodeAttr("compartmentAlias", aliasNode);
      if (!compartmentAliasId.isEmpty()) {
        final Compartment compartment = model.getElementByElementId(compartmentAliasId);
        if (compartment == null) {
          throw new InvalidXmlSchemaException(errorPrefix + "CompartmentAlias does not exist: " + compartmentAliasId);
        } else {
          result.setCompartment(compartment);
          compartment.addElement(result);
        }
      }
      final String complexAliasId = XmlParser.getNodeAttr("complexSpeciesAlias", aliasNode);
      if (!complexAliasId.isEmpty()) {
        final Complex complex = model.getElementByElementId(complexAliasId);
        if (complex == null) {
          throw new InvalidXmlSchemaException(
              errorPrefix + "ComplexAlias does not exist: " + complexAliasId + ", current: " + result.getElementId());
        } else {
          result.setComplex(complex);
          complex.addSpecies(result);
          complex.setNameVerticalAlign(VerticalAlign.BOTTOM);
        }
      }

      species.updateModelElementAfterLayoutAdded(result);
      result.setNameX(result.getX());
      result.setNameY(result.getY());
      result.setNameWidth(result.getWidth());
      result.setNameHeight(result.getHeight());
      result.setNameHorizontalAlign(HorizontalAlign.CENTER);
      result.setNameVerticalAlign(VerticalAlign.MIDDLE);
      if (result.getHomodimer() > 1) {
        double width = result.getWidth() - (result.getHomodimer() - 1) * Configuration.HOMODIMER_OFFSET;
        result.setNameWidth(Math.max(1, width));
        double height = result.getHeight() - (result.getHomodimer() - 1) * Configuration.HOMODIMER_OFFSET;
        result.setNameHeight(Math.max(1, height));
      }
      return result;
    } catch (final InvalidArgumentException e) {
      throw new InvalidXmlSchemaException(errorPrefix + e.getMessage(), e);
    } catch (final NotImplementedException e) {
      throw new InvalidXmlSchemaException(errorPrefix + "Problem with creating species", e);
    }
  }

  @Override
  public String toXml(final Species species) {
    Compartment ca = null;
    // artificial compartment aliases should be excluded
    if (species.getCompartment() != null && !(species.getCompartment() instanceof PathwayCompartment)) {
      ca = species.getCompartment();
    } else if (species.getComplex() == null) {
      final ModelData model = species.getModelData();
      if (model != null) {
        for (final Compartment compartment : model.getModel().getCompartments()) {
          if (!(compartment instanceof PathwayCompartment) && compartment.cross(species)) {
            if (ca == null) {
              ca = compartment;
            } else if (ca.getSize() > compartment.getSize()) {
              ca = compartment;
            }
          }
        }
      }
    }

    final Complex complex = species.getComplex();

    final String compartmentAliasId = elements.getCompartmentAliasId(ca);

    final StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:speciesAlias ");
    sb.append("id=\"").append(species.getElementId()).append("\" ");
    sb.append("species=\"").append(elements.getElementId(species)).append("\" ");

    if (complex != null) {
      sb.append("complexSpeciesAlias=\"").append(complex.getElementId()).append("\" ");
    } else if (compartmentAliasId != null) {
      // you cannot have both if there are both then CellDesigner behaves
      // improperly
      sb.append("compartmentAlias=\"").append(compartmentAliasId).append("\" ");
    }
    sb.append(">\n");

    if (species.getActivity() != null) {
      if (species.getActivity()) {
        sb.append("<celldesigner:activity>active</celldesigner:activity>\n");
      } else {
        sb.append("<celldesigner:activity>inactive</celldesigner:activity>\n");
      }
    }

    sb.append("<celldesigner:bounds ");
    sb.append("x=\"").append(species.getX()).append("\" ");
    sb.append("y=\"").append(species.getY()).append("\" ");
    sb.append("w=\"").append(species.getWidth()).append("\" ");
    sb.append("h=\"" + species.getHeight() + "\" ");
    sb.append("/>\n");

    sb.append(createFontTag(species));

    // TODO to be improved
    sb.append("<celldesigner:view state=\"usual\"/>\n");
    sb.append("<celldesigner:usualView>");
    sb.append("<celldesigner:innerPosition x=\"0\" y=\"0\"/>");
    sb.append("<celldesigner:boxSize width=\"" + species.getWidth() + "\" height=\"" + species.getHeight() + "\"/>");
    sb.append("<celldesigner:singleLine width=\"" + species.getLineWidth() + "\"/>");
    sb.append(
        "<celldesigner:paint color=\"" + XmlParser.colorToString(species.getFillColor()) + "\" scheme=\"Color\"/>");
    sb.append("</celldesigner:usualView>\n");
    sb.append("<celldesigner:briefView>");
    sb.append("<celldesigner:innerPosition x=\"0\" y=\"0\"/>");
    sb.append("<celldesigner:boxSize width=\"" + species.getWidth() + "\" height=\"" + species.getHeight() + "\"/>");
    sb.append("<celldesigner:singleLine width=\"" + species.getLineWidth() + "\"/>");
    sb.append(
        "<celldesigner:paint color=\"" + XmlParser.colorToString(species.getFillColor()) + "\" scheme=\"Color\"/>");
    sb.append("</celldesigner:briefView>\n");
    if (species.getStateLabel() != null || species.getStatePrefix() != null) {
      sb.append("<celldesigner:info state=\"open\" prefix=\"" + species.getStatePrefix() + "\" label=\""
          + species.getStateLabel() + "\"/>\n");
    }
    sb.append(createStructuralStateTag(species));
    sb.append("</celldesigner:speciesAlias>\n");
    return sb.toString();
  }

  /**
   * Process node with information about alias state and puts data into alias.
   *
   * @param node  node where information about alias state is stored
   * @param alias alias object to be modified if necessary
   */
  private void processAliasState(final Node node, final Species alias) {
    final String state = XmlParser.getNodeAttr("state", node);
    if ("open".equalsIgnoreCase(state)) {
      final String prefix = XmlParser.getNodeAttr("prefix", node);
      final String label = XmlParser.getNodeAttr("label", node);
      alias.setStatePrefix(prefix);
      alias.setStateLabel(label);
    } else if ("empty".equalsIgnoreCase(state)) {
      //return
    } else if (state == null || state.isEmpty()) {
      //return
    } else {
      throw new NotImplementedException("[Alias: " + alias.getElementId() + "] Unkown alias state: " + state);
    }

  }
}
