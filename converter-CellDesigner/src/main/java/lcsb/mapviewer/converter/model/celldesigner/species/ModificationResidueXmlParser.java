package lcsb.mapviewer.converter.model.celldesigner.species;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.geometry.CellDesignerAliasConverter;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.ModificationType;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;
import lcsb.mapviewer.model.map.species.field.RegulatoryRegion;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;

import java.util.List;

public class ModificationResidueXmlParser {

  private final CellDesignerElementCollection elements;

  public ModificationResidueXmlParser(final CellDesignerElementCollection elements) {
    this.elements = elements;
  }

  /**
   * Transforms ModificationResidue into CellDssigner xml representation.
   *
   * @param region object to be transformed
   * @return xml representation of the given region
   */
  public String toXml(final ModificationResidue region) {
    if (region.getSpecies() instanceof AntisenseRna) {
      return antisenseRnaModificationResidueToXml(region);
    } else if (region.getSpecies() instanceof Gene) {
      return geneModificationResidueToXml(region);
    } else if (region.getSpecies() instanceof Rna) {
      return rnaModificationResidueToXml(region);
    } else if (region.getSpecies() instanceof Protein) {
      if (region instanceof BindingRegion) {
        return proteinModificationResidueToXml((BindingRegion) region);
      } else if (region instanceof Residue) {
        return proteinModificationResidueToXml((Residue) region);
      } else {
        throw new NotImplementedException("model2String not implemented for " + region);
      }
    } else {
      throw new NotImplementedException("model2String not implemented for " + region.getSpecies());
    }
  }

  private String antisenseRnaModificationResidueToXml(final ModificationResidue region) {
    final CellDesignerModificationResidue cellDesignerModificationResidue = new CellDesignerModificationResidue(region);

    String result = "";
    String attributes = " id=\"" + computeModificationResidueId(region) + "\"";
    if (!region.getName().equals("")) {
      attributes += " name=\"" + XmlParser.escapeXml(region.getName()) + "\"";
    }
    String type = null;
    if (region instanceof CodingRegion) {
      attributes += " size=\"" + cellDesignerModificationResidue.getSize() + "\"";
      type = "CodingRegion";
    } else if (region instanceof ModificationSite) {
      type = "Modification Site";
    } else if (region instanceof ProteinBindingDomain) {
      attributes += " size=\"" + cellDesignerModificationResidue.getSize() + "\"";
      type = "proteinBindingDomain";
    } else {
      throw new InvalidArgumentException("Unknown modificatin type: " + region.getClass());
    }

    attributes += " pos=\"" + cellDesignerModificationResidue.getPos() + "\"";
    attributes += " type=\"" + type + "\"";
    result += "<celldesigner:region " + attributes + ">";
    result += "</celldesigner:region>\n";

    return result;
  }

  /**
   * This method computes modificationResidueId that should be used when
   * exporting modification residue to CellDesigner. It cannot be taken from
   * data structure because we have to merge elements when exporting to
   * CellDesigner and this might introduce inconsistent identifiers.
   *
   * @param region {@link ModificationResidue} for which we want to find out
   *               identifier
   * @return identifier that can be used in CellDesigner
   */
  String computeModificationResidueId(final ModificationResidue region) {
    final List<? extends ModificationResidue> modificationResidues;
    if (region.getSpecies() instanceof Protein) {
      return region.getIdModificationResidue();
    } else if (region.getSpecies() instanceof Rna) {
      modificationResidues = ((Rna) region.getSpecies()).getModificationResidues();
    } else if (region.getSpecies() instanceof AntisenseRna) {
      modificationResidues = ((AntisenseRna) region.getSpecies()).getModificationResidues();
    } else if (region.getSpecies() instanceof Gene) {
      modificationResidues = ((Gene) region.getSpecies()).getModificationResidues();
    } else {
      throw new NotImplementedException();
    }
    int number = -1;
    int i = 0;
    for (final ModificationResidue mr : modificationResidues) {
      if (mr.getIdModificationResidue().equals(region.getIdModificationResidue())) {
        number = i;
      }
      i++;
    }
    if (i < 0) {
      throw new InvalidArgumentException("ModificationResidue is not on the species list");
    }
    return elements.getModificationResidueId(region, number);
  }

  /**
   * Generates CellDesigner xml for {@link CellDesignerModificationResidue}.
   *
   * @param mr object to transform into xml
   * @return CellDesigner xml for {@link CellDesignerModificationResidue}
   */
  String geneModificationResidueToXml(final ModificationResidue mr) {
    final CellDesignerModificationResidue cellDesignerModificationResidue = new CellDesignerModificationResidue(mr);

    String result = "";
    String attributes = " id=\"" + computeModificationResidueId(mr) + "\"";
    if (!mr.getName().equals("")) {
      attributes += " name=\"" + XmlParser.escapeXml(mr.getName()) + "\"";
    }
    String type = null;
    if (mr instanceof ModificationSite) {
      type = ModificationType.MODIFICATION_SITE.getCellDesignerName();
    } else if (mr instanceof RegulatoryRegion) {
      type = ModificationType.REGULATORY_REGION.getCellDesignerName();
      attributes += " size=\"" + cellDesignerModificationResidue.getSize() + "\"";
    } else if (mr instanceof CodingRegion) {
      type = ModificationType.CODING_REGION.getCellDesignerName();
      attributes += " size=\"" + cellDesignerModificationResidue.getSize() + "\"";
    } else if (mr instanceof TranscriptionSite) {
      final TranscriptionSite transcriptionSite = (TranscriptionSite) mr;
      if (transcriptionSite.getDirection().equals("RIGHT")) {
        type = ModificationType.TRANSCRIPTION_SITE_RIGHT.getCellDesignerName();
      } else {
        type = ModificationType.TRANSCRIPTION_SITE_LEFT.getCellDesignerName();
      }
      attributes += " size=\"" + cellDesignerModificationResidue.getSize() + "\"";
      attributes += " active=\"" + transcriptionSite.getActive() + "\"";
    } else {
      throw new InvalidArgumentException("Don't know how to handle: " + mr.getClass());
    }
    attributes += " type=\"" + type + "\"";
    attributes += " pos=\"" + cellDesignerModificationResidue.getPos() + "\"";
    result += "<celldesigner:region " + attributes + ">";
    result += "</celldesigner:region>\n";

    return result;
  }

  /**
   * Generates CellDesigner xml for {@link Residue}.
   *
   * @param mr object to transform into xml
   * @return CellDesigner xml for {@link Residue}
   */
  private String proteinModificationResidueToXml(final Residue mr) {
    final CellDesignerAliasConverter converter = new CellDesignerAliasConverter(mr.getSpecies(), false);

    String result = "";
    String attributes = " id=\"" + computeModificationResidueId(mr) + "\"";
    if (!mr.getName().equals("")) {
      attributes += " name=\"" + XmlParser.escapeXml(mr.getName()) + "\"";
    }
    attributes += " angle=\"" + converter.getAngleForPoint(mr.getSpecies(), mr.getCenter()) + "\"";
    result += "<celldesigner:modificationResidue " + attributes + ">";
    result += "</celldesigner:modificationResidue>";

    return result;
  }

  /**
   * Generates CellDesigner xml for {@link BindingRegion}.
   *
   * @param mr object to transform into xml
   * @return CellDesigner xml for {@link BindingRegion}
   */
  private String proteinModificationResidueToXml(final BindingRegion mr) {

    final CellDesignerModificationResidue cellDesignerModificationResidue = new CellDesignerModificationResidue(mr);
    String result = "";
    String attributes = " id=\"" + computeModificationResidueId(mr) + "\"";
    if (!mr.getName().equals("")) {
      attributes += " name=\"" + XmlParser.escapeXml(mr.getName()) + "\"";
    }
    attributes += " angle=\"" + cellDesignerModificationResidue.getAngle() + "\"";
    attributes += " size=\"" + cellDesignerModificationResidue.getSize() + "\"";
    result += "<celldesigner:bindingRegion " + attributes + ">";
    result += "</celldesigner:bindingRegion>";

    return result;
  }

  /**
   * Generates CellDesigner xml for {@link CellDesignerModificationResidue}.
   *
   * @param mr object to transform into xml
   * @return CellDesigner xml for {@link CellDesignerModificationResidue}
   */
  String rnaModificationResidueToXml(final ModificationResidue mr) {
    final CellDesignerModificationResidue cellDesignerModificationResidue = new CellDesignerModificationResidue(mr);

    String result = "";
    String attributes = " id=\"" + computeModificationResidueId(mr) + "\"";
    if (!mr.getName().equals("")) {
      attributes += " name=\"" + XmlParser.escapeXml(mr.getName()) + "\"";
    }
    String type = null;
    if (mr instanceof ModificationSite) {
      type = ModificationType.MODIFICATION_SITE.getCellDesignerName();
    } else if (mr instanceof CodingRegion) {
      type = ModificationType.CODING_REGION.getCellDesignerName();
      attributes += " size=\"" + cellDesignerModificationResidue.getSize() + "\"";
    } else if (mr instanceof ProteinBindingDomain) {
      type = ModificationType.PROTEIN_BINDING_DOMAIN.getCellDesignerName();
      attributes += " size=\"" + cellDesignerModificationResidue.getSize() + "\"";
    } else {
      throw new InvalidArgumentException("Don't know how to handle: " + mr.getClass());
    }
    attributes += " type=\"" + type + "\"";
    attributes += " pos=\"" + cellDesignerModificationResidue.getPos() + "\"";
    result += "<celldesigner:region " + attributes + ">";
    result += "</celldesigner:region>\n";

    return result;
  }

}
