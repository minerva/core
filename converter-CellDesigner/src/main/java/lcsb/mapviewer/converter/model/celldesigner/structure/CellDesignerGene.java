package lcsb.mapviewer.converter.model.celldesigner.structure;

import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.RegulatoryRegion;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;

/**
 * Class representing CellDesigner {@link Gene}.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerGene extends CellDesignerSpecies<Gene> {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * List of modifications for the Gene.
   */
  private List<CellDesignerModificationResidue> modificationResidues = new ArrayList<>();

  /**
   * Constructor that initializes gene with the data passed in the argument.
   * 
   * @param species
   *          original species used for data initialization
   */
  public CellDesignerGene(final CellDesignerSpecies<?> species) {
    super(species);
    if (species instanceof CellDesignerGene) {
      CellDesignerGene gene = (CellDesignerGene) species;
      for (final CellDesignerModificationResidue mr : gene.getModificationResidues()) {
        addModificationResidue(new CellDesignerModificationResidue(mr));
      }
    }
  }

  /**
   * Default constructor.
   */
  public CellDesignerGene() {
    super();
  }

  @Override
  public CellDesignerGene copy() {
    if (this.getClass() == CellDesignerGene.class) {
      return new CellDesignerGene(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public void update(final CellDesignerSpecies<?> species) {
    super.update(species);
    if (species instanceof CellDesignerGene) {
      CellDesignerGene gene = (CellDesignerGene) species;

      for (final CellDesignerModificationResidue mr : gene.getModificationResidues()) {
        addModificationResidue(mr);
      }
    }
  }

  @Override
  public Gene createModelElement(final String aliasId) {
    Gene result = new Gene(aliasId);
    super.setModelObjectFields(result);
    return result;
  }

  @Override
  public void updateModelElementAfterLayoutAdded(final Species species) {
    Gene gene = (Gene) species;
    for (final CellDesignerModificationResidue region : modificationResidues) {
      ModificationResidue mr = region.createModificationResidue(gene);
      if (mr instanceof CodingRegion) {
        gene.addCodingRegion((CodingRegion) mr);
      } else if (mr instanceof ModificationSite) {
        gene.addModificationSite((ModificationSite) mr);
      } else if (mr instanceof RegulatoryRegion) {
        gene.addRegulatoryRegion((RegulatoryRegion) mr);
      } else if (mr instanceof TranscriptionSite) {
        gene.addTranscriptionSite((TranscriptionSite) mr);
      } else {
        throw new InvalidArgumentException("Cannot add modification residue to element: " + mr.getClass());
      }
    }
  }

  /**
   * Adds modification to the gene.
   *
   * @param modificationResidue
   *          modification to add
   */
  public void addModificationResidue(final CellDesignerModificationResidue modificationResidue) {
    for (final CellDesignerModificationResidue mr : modificationResidues) {
      if (mr.getIdModificationResidue().equals(modificationResidue.getIdModificationResidue())) {
        mr.update(modificationResidue);
        return;
      }
    }
    modificationResidues.add(modificationResidue);
    modificationResidue.setSpecies(this);

  }

  /**
   * @return the modificationResidues
   * @see #modificationResidues
   */
  public List<CellDesignerModificationResidue> getModificationResidues() {
    return modificationResidues;
  }

  /**
   * @param modificationResidues
   *          the modificationResidues to set
   * @see #modificationResidues
   */
  public void setModificationResidues(final List<CellDesignerModificationResidue> modificationResidues) {
    this.modificationResidues = modificationResidues;
  }
}
