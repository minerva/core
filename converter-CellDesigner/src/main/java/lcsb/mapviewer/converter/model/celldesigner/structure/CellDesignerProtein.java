package lcsb.mapviewer.converter.model.celldesigner.structure;

import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.Residue;

/**
 * Class representing CellDesigner {@link Protein} object.
 * 
 * @param <T>
 *          type of a {@link Protein} modeled by this class
 * @author Piotr Gawron
 * 
 */
public class CellDesignerProtein<T extends Protein> extends CellDesignerSpecies<T> {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * State of the protein.
   */
  private String structuralState = null;

  private Double structuralStateAngle = null;

  /**
   * List of modifications for the Protein.
   */
  private List<CellDesignerModificationResidue> modificationResidues = new ArrayList<CellDesignerModificationResidue>();

  /**
   * Constructor that initializes protein with the data passed in the argument.
   * 
   * @param species
   *          original species used for data initialization
   */
  public CellDesignerProtein(final CellDesignerSpecies<?> species) {
    super(species);
    if (species instanceof CellDesignerProtein) {
      CellDesignerProtein<?> protein = (CellDesignerProtein<?>) species;
      if (protein.getStructuralState() != null) {
        setStructuralState(new String(protein.getStructuralState()));
      }
      for (final CellDesignerModificationResidue mr : protein.getModificationResidues()) {
        addModificationResidue(new CellDesignerModificationResidue(mr));
      }
    }
  }

  /**
   * Default constructor.
   */
  public CellDesignerProtein() {
    super();
  }

  @Override
  public CellDesignerProtein<T> copy() {
    if (this.getClass().equals(CellDesignerProtein.class)) {
      return new CellDesignerProtein<T>(this);
    } else {
      throw new NotImplementedException("Copy method for " + this.getClass() + " class not implemented");
    }
  }

  @Override
  public void update(final CellDesignerSpecies<?> species) {
    super.update(species);
    if (species instanceof CellDesignerProtein) {
      CellDesignerProtein<?> protein = (CellDesignerProtein<?>) species;
      if (getStructuralState() == null || getStructuralState().equals("")) {
        setStructuralState(protein.getStructuralState());
      }
      for (final CellDesignerModificationResidue mr : protein.getModificationResidues()) {
        addModificationResidue(mr);
      }
    }
  }

  @Override
  protected void setModelObjectFields(final T result) {
    super.setModelObjectFields(result);
  }

  @Override
  public void updateModelElementAfterLayoutAdded(final Species species) {
    Protein protein = (Protein) species;
    for (final CellDesignerModificationResidue region : modificationResidues) {
      ModificationResidue mr = region.createModificationResidue(protein);
      if (mr instanceof Residue) {
        protein.addResidue((Residue) mr);
      } else if (mr instanceof BindingRegion) {
        protein.addBindingRegion((BindingRegion) mr);
      } else {
        throw new InvalidArgumentException("Cannot add modification residue to element: " + mr.getClass());
      }
    }
    if (structuralState != null) {
      protein.addStructuralState(createStructuralState(species, structuralState, structuralStateAngle));
    }
  }

  /**
   * Adds modification to the protein.
   *
   * @param modificationResidue
   *          modification to add
   */
  public void addModificationResidue(final CellDesignerModificationResidue modificationResidue) {
    for (final CellDesignerModificationResidue mr : modificationResidues) {
      if (mr.getIdModificationResidue().equals(modificationResidue.getIdModificationResidue())) {
        mr.update(modificationResidue);
        return;
      }
    }
    modificationResidues.add(modificationResidue);
    modificationResidue.setSpecies(this);
  }

  /**
   * @return the structuralState
   * @see #structuralState
   */
  public String getStructuralState() {
    return structuralState;
  }

  /**
   * @param structuralState
   *          the structuralState to set
   * @see #structuralState
   */
  public void setStructuralState(final String structuralState) {
    if (this.structuralState != null && !this.structuralState.equals("")
        && !this.structuralState.equals(structuralState)) {
      logger.warn("replacing structural state, Old: " + this.structuralState + " into new: " + structuralState);
    }
    this.structuralState = structuralState;
  }

  /**
   * @return the modificationResidues
   * @see #modificationResidues
   */
  public List<CellDesignerModificationResidue> getModificationResidues() {
    return modificationResidues;
  }

  /**
   * @param modificationResidues
   *          the modificationResidues to set
   * @see #modificationResidues
   */
  public void setModificationResidues(final List<CellDesignerModificationResidue> modificationResidues) {
    this.modificationResidues = modificationResidues;
  }

  public Double getStructuralStateAngle() {
    return structuralStateAngle;
  }

  public void setStructuralStateAngle(final Double structuralStateAngle) {
    this.structuralStateAngle = structuralStateAngle;
  }

}
