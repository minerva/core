package lcsb.mapviewer.converter.model.celldesigner.geometry;

import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.map.species.SimpleMolecule;

/**
 * Class that provides CellDesigner specific graphical information for
 * SimpleMolecule. It's used for conversion from xml to normal x,y coordinates.
 * 
 * @author Piotr Gawron
 * 
 */
public class SimpleMoleculeCellDesignerAliasConverter extends AbstractCellDesignerAliasConverter<SimpleMolecule> {

  private static Logger logger = LogManager.getLogger();

  /**
   * Default constructor.
   * 
   * @param sbgn
   *          Should the converter use sbgn standard
   */
  protected SimpleMoleculeCellDesignerAliasConverter(final boolean sbgn) {
    super(sbgn);
  }

  @Override
  public Point2D getPointCoordinates(final SimpleMolecule alias, final CellDesignerAnchor anchor) {
    double x = alias.getX();
    double y = alias.getY();
    double width = Math.max(alias.getWidth(), 1);
    double height = Math.max(alias.getHeight(), 1);
    if (invalidAnchorPosition(alias, anchor)) {
      return alias.getCenter();
    }
    if (isSbgn()) {
      return getEllipseTransformation().getPointOnEllipseByAnchor(x + (width - height) / 2, y, height, height, anchor);
    }
    return getEllipseTransformation().getPointOnEllipseByAnchor(x, y, width, height, anchor);
  }

  @Override
  public Point2D getAnchorPointCoordinates(final SimpleMolecule alias, final double angle) {
    if (alias.getWidth().equals(0.0) && alias.getHeight().equals(0.0)) {
      return alias.getCenter();
    }
    Point2D result;
    if (!isSbgn()) {
      result = getEllipseTransformation().getPointOnEllipseByRadian(alias.getX(), alias.getY(), alias.getWidth(),
          alias.getHeight(), angle);
    } else {
      result = getEllipseTransformation().getPointOnEllipseByRadian(
          alias.getX() + (alias.getWidth() - alias.getHeight()) / 2, alias.getY(), alias.getHeight(), alias.getHeight(),
          angle);
    }
    return result;

  }

  @Override
  public PathIterator getBoundPathIterator(final SimpleMolecule alias) {
    throw new NotImplementedException("This class doesn't provide boundPath");
  }

  @Override
  public Point2D getPointCoordinatesOnBorder(final SimpleMolecule simpleMolecule, final double angle) {
    if (simpleMolecule.getWidth() == 0 && simpleMolecule.getHeight() == 0) {
      logger.warn("Looking for coordinates on border of alias of size 0");
      return simpleMolecule.getCenter();
    }
    Point2D result;
    result = getEllipseTransformation().getPointOnEllipseByRadian(simpleMolecule.getX(), simpleMolecule.getY(),
        simpleMolecule.getWidth(), simpleMolecule.getHeight(), angle);
    return result;

  }

}
