package lcsb.mapviewer.converter.model.celldesigner.species;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerRna;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.ModificationType;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class that performs parsing of the CellDesigner xml for
 * {@link CellDesignerRna} object.
 *
 * @author Piotr Gawron
 */

public class RnaXmlParser extends AbstractElementXmlParser<CellDesignerRna, Rna> {

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed from
   * xml.
   */
  private final CellDesignerElementCollection elements;

  private final ModificationResidueXmlParser modificationResidueXmlParser;

  /**
   * Default constructor. Model is required because some nodes require access to
   * other parts of the model.
   *
   * @param elements collection of {@link CellDesignerElement cell designer elements}
   *                 parsed from xml
   */
  public RnaXmlParser(final CellDesignerElementCollection elements) {
    this.elements = elements;
    this.modificationResidueXmlParser = new ModificationResidueXmlParser(elements);
  }

  @Override
  public Pair<String, CellDesignerRna> parseXmlElement(final Node rnaNode) throws InvalidXmlSchemaException {
    final CellDesignerRna rna = new CellDesignerRna();
    final String identifier = XmlParser.getNodeAttr("id", rnaNode);
    rna.setName(decodeName(XmlParser.getNodeAttr("name", rnaNode)));
    final NodeList list = rnaNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      final Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equals("celldesigner:notes")) {
          rna.setNotes(getRap().getNotes(node));
        } else if (node.getNodeName().equals("celldesigner:listOfRegions")) {
          final NodeList residueList = node.getChildNodes();
          for (int j = 0; j < residueList.getLength(); j++) {
            final Node residueNode = residueList.item(j);
            if (residueNode.getNodeType() == Node.ELEMENT_NODE) {
              if (residueNode.getNodeName().equalsIgnoreCase("celldesigner:region")) {
                rna.addRegion(getRnaRegion(residueNode));
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfRegions " + residueNode.getNodeName());
              }
            }
          }
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:rna " + node.getNodeName());
        }
      }
    }
    return new Pair<String, CellDesignerRna>(identifier, rna);
  }

  @Override
  public String toXml(final Rna rna) {
    String attributes = " type=\"RNA\" ";
    final StringBuilder result = new StringBuilder();
    attributes += " id=\"r_" + elements.getElementId(rna) + "\"";
    if (!rna.getName().isEmpty()) {
      attributes += " name=\"" + XmlParser.escapeXml(encodeName(rna.getName())) + "\"";
    }
    result.append("<celldesigner:RNA").append(attributes).append(">");

    if (!rna.getModificationResidues().isEmpty()) {
      result.append("<celldesigner:listOfRegions>");
      for (final ModificationResidue mr : rna.getModificationResidues()) {
        result.append(modificationResidueXmlParser.toXml(mr));
      }
      result.append("</celldesigner:listOfRegions>");
    }

    result.append("</celldesigner:RNA>");
    return result.toString();
  }

  /**
   * Parses CellDesigner xml node for RnaRegion.
   *
   * @param residueNode xml node to parse
   * @return {@link CellDesignerModificationResidue} object created from the node
   * @throws InvalidXmlSchemaException thrown when input xml node doesn't follow defined schema
   */
  private CellDesignerModificationResidue getRnaRegion(final Node residueNode) throws InvalidXmlSchemaException {
    final CellDesignerModificationResidue residue = new CellDesignerModificationResidue();
    residue.setIdModificationResidue(XmlParser.getNodeAttr("id", residueNode));
    residue.setSize(XmlParser.getNodeAttr("size", residueNode));
    residue.setPos(XmlParser.getNodeAttr("pos", residueNode));
    final String typeString = XmlParser.getNodeAttr("type", residueNode);
    if (typeString != null) {
      residue.setModificationType(ModificationType.getByCellDesignerName(typeString));
    }
    final NodeList list = residueNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      final Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        throw new InvalidXmlSchemaException("Unknown element of celldesigner:region " + node.getNodeName());
      }
    }
    return residue;
  }

}
