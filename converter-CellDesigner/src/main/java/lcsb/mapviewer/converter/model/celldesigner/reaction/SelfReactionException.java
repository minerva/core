package lcsb.mapviewer.converter.model.celldesigner.reaction;

import lcsb.mapviewer.model.map.InconsistentModelException;

/**
 * Exception thrown when CellDesigner exporter tries to export reaction that
 * uses the same element as product and reactant.
 * 
 * @author Piotr Gawron
 * 
 */
public class SelfReactionException extends InconsistentModelException {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor with a message passed in the argument.
   * 
   * @param message
   *          text message of this exception
   */
  public SelfReactionException(final String message) {
    super(message);
  }

  public SelfReactionException(final Exception e) {
    super(e);
  }
}
