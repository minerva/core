package lcsb.mapviewer.converter.model.celldesigner.structure;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Ion;

/**
 * Class representing CellDesigner {@link Ion}.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerIon extends CellDesignerChemical<Ion> {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Constructor that initializes ion with the data passed in the argument.
   * 
   * @param species
   *          original species used for data initialization
   */
  public CellDesignerIon(final CellDesignerSpecies<?> species) {
    super(species);
  }

  /**
   * Default constructor.
   */
  public CellDesignerIon() {
    super();
  }

  @Override
  public CellDesignerIon copy() {
    if (this.getClass() == CellDesignerIon.class) {
      return new CellDesignerIon(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public Ion createModelElement(final String aliasId) {
    Ion result = new Ion(aliasId);
    super.setModelObjectFields(result);
    return result;
  }
}
