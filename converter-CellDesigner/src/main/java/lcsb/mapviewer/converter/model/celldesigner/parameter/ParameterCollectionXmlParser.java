package lcsb.mapviewer.converter.model.celldesigner.parameter;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.model.Model;

public class ParameterCollectionXmlParser {
  /**
   * Default class logger
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  private ParameterXmlParser parameterParser;

  public ParameterCollectionXmlParser(final Model model) {
    parameterParser = new ParameterXmlParser(model);
  }

  public Set<SbmlParameter> parseXmlParameterCollection(final Node functionsNode) throws InvalidXmlSchemaException {
    Set<SbmlParameter> result = new HashSet<>();
    for (final Node node : XmlParser.getNodes("parameter", functionsNode.getChildNodes())) {
      result.add(parameterParser.parseParameter(node));
    }
    return result;
  }

  public String toXml(final Set<SbmlParameter> parameters) {
    StringBuilder result = new StringBuilder();
    if (parameters.size() > 0) {
      result.append("<listOfParameters>");
      for (final SbmlParameter sbmlFunction : parameters) {
        result.append(parameterParser.toXml(sbmlFunction));
      }
      result.append("</listOfParameters>");
    }
    return result.toString();
  }

}
