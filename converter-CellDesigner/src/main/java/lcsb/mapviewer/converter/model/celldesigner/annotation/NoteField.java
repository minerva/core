package lcsb.mapviewer.converter.model.celldesigner.annotation;

import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Describes structural information that appears in the cell designer notes.
 * 
 * @author Piotr Gawron
 * 
 */
public enum NoteField {
  /**
   * List of {@link MiriamType#HGNC} identifiers.
   */
  HGNC("HGNC_ID", BioEntity.class, MiriamType.HGNC),

  /**
   * List of {@link MiriamType#REFSEQ} identifiers.
   */
  REFSEQ("RefSeq_ID", BioEntity.class, MiriamType.REFSEQ),

  /**
   * List of {@link MiriamType#ENTREZ} identifiers.
   */
  ENTREZ("EntrezGene_ID", BioEntity.class, MiriamType.ENTREZ),

  /**
   * List of {@link MiriamType#REACTOME} identifiers.
   */
  REACTOME("Reactome_ID", BioEntity.class, MiriamType.REACTOME),

  /**
   * List of {@link MiriamType#PUBMED} identifiers.
   */
  PUBMED("Pubmed_ID", BioEntity.class, MiriamType.PUBMED),

  /**
   * List of {@link MiriamType#KEGG_GENES} identifiers.
   */
  KEGG_GENES("KEGG_ID", BioEntity.class, MiriamType.KEGG_GENES),

  /**
   * List of {@link MiriamType#PANTHER} identifiers.
   */
  PANTHER("PANTHER", BioEntity.class, MiriamType.PANTHER),

  /**
   * {@link Element#symbol}.
   */
  SYMBOL("Symbol", BioEntity.class, null),

  /**
   * {@link Element#fullName}.
   */
  NAME("Name", BioEntity.class, null),

  /**
   * {@link Element#notes} or {@link Reaction#notes} .
   */
  DESCRIPTION("Description", BioEntity.class, null),

  /**
   * {@link Element#formerSymbols}.
   */
  PREVIOUS_SYMBOLS("Previous Symbols", Element.class, null),

  /**
   * {@link Element#synonyms} or {@link Reaction#synonyms}.
   */
  SYNONYMS("Synonyms", BioEntity.class, null),

  /**
   * {@link Element#abbreviation} or {@link Reaction#abbreviation}.
   */
  ABBREVIATION("Abbreviation", BioEntity.class, null),

  /**
   * {@link Reaction#formula}.
   */
  FORMULA("Formula", Reaction.class, null),

  /**
   * {@link Reaction#mechanicalConfidenceScore}.
   */
  @ImportOnly
  MECHANICAL_CONFIDENCE_SCORE("MechanicalConfidenceScore", Reaction.class, null),

  /**
   * {@link Reaction#lowerBound}.
   */
  @ImportOnly
  LOWER_BOUND("LowerBound", Reaction.class, null),

  /**
   * {@link Reaction#upperBound}.
   */
  @ImportOnly
  UPPER_BOUND("UpperBound", Reaction.class, null),

  /**
   * {@link Reaction#subsystem}.
   */
  @ImportOnly
  SUBSYSTEM("Subsystem", Reaction.class, null),

  /**
   * {@link Reaction#geneProteinReaction}.
   */
  @ImportOnly
  GENE_PROTEIN_REACTION("GeneProteinReaction", Reaction.class, null),

  /**
   * {@link Element#formula}.
   */
  CHARGED_FORMULA("ChargedFormula", Element.class, null),

  /**
   * {@link Species#charge}.
   */
  CHARGE("Charge", Species.class, null),

  /**
   * {@link Element#getSemanticZoomLevelVisibility()}.
   */
  @ImportOnly
  SEMANTIC_ZOOM_LEVEL_VISIBILITY("SemanticZoomLevelVisibility", BioEntity.class, null),

  /**
   * {@link Element#getSemanticZoomLevelVisibility()}.
   */
  @ImportOnly
  TRANSPARENCY_ZOOM_LEVEL_VISIBILITY("SemanticZoomLevelTransparency", Element.class, null),

  @ImportOnly
  BACKGROUND_COLOR("BackgroundColor", LayerText.class, null),

  @ImportOnly
  BORDER_COLOR("BorderColor", LayerText.class, null),

  @ImportOnly
  Z_INDEX("Z-Index", Drawable.class, null),

  ;

  /**
   * Name used in the notes to distinguish fields.
   */
  private String commonName;

  /**
   * What object class can have this field.
   */
  private Class<? extends Drawable> clazz;

  /**
   * What {@link MiriamType} is associated with the field.
   */
  private MiriamType miriamType;

  /**
   * Default constructor.
   * 
   * @param name
   *          {@link #commonName}
   * @param clazz
   *          {@link #clazz}
   */
  NoteField(final String name, final Class<? extends Drawable> clazz) {
    this.commonName = name;
    this.clazz = clazz;
  }

  /**
   * Default constructor.
   * 
   * @param name
   *          {@link #commonName}
   * @param clazz
   *          {@link #clazz}
   * @param type
   *          {@link #miriamType}
   */
  NoteField(final String name, final Class<? extends Drawable> clazz, final MiriamType type) {
    this(name, clazz);
    this.miriamType = type;
  }

  /**
   * @return the miriamType
   * @see #miriamType
   */
  public MiriamType getMiriamType() {
    return miriamType;
  }

  /**
   * @return the commonName
   * @see #commonName
   */
  public String getCommonName() {
    return commonName;
  }

  /**
   * @return the clazz
   * @see #clazz
   */
  public Class<?> getClazz() {
    return clazz;
  }

}
