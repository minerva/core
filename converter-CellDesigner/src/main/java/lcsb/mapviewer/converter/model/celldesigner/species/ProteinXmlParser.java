package lcsb.mapviewer.converter.model.celldesigner.species;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerProtein;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.ModificationType;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationResidue;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class that performs parsing of the CellDesigner xml for
 * {@link CellDesignerProtein} object.
 *
 * @author Piotr Gawron
 */

public class ProteinXmlParser extends AbstractElementXmlParser<CellDesignerProtein<?>, Protein> {

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed
   * from xml.
   */
  private final CellDesignerElementCollection elements;

  private final ModificationResidueXmlParser modificationResidueXmlParser;

  private int proteinIdCounter = 0;

  /**
   * Default constructor. Model is required because some nodes require access to
   * other parts of the model.
   *
   * @param elements collection of {@link CellDesignerElement cell designer elements}
   *                 parsed from xml
   */
  public ProteinXmlParser(final CellDesignerElementCollection elements) {
    this.elements = elements;
    this.modificationResidueXmlParser = new ModificationResidueXmlParser(elements);
  }

  @Override
  public Pair<String, CellDesignerProtein<?>> parseXmlElement(final Node proteinNode) throws InvalidXmlSchemaException {
    CellDesignerProtein<?> protein = null;
    final String value = XmlParser.getNodeAttr("type", proteinNode);
    final ProteinMapping mapping = ProteinMapping.getMappingByString(value);
    if (mapping != null) {
      protein = mapping.createProtein();
    } else {
      throw new InvalidXmlSchemaException("Protein node in Sbml model is of unknown type: " + value);
    }

    final String identifier = XmlParser.getNodeAttr("id", proteinNode);
    protein.setName(decodeName(XmlParser.getNodeAttr("name", proteinNode)));
    final NodeList list = proteinNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      final Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equals("celldesigner:listOfModificationResidues")) {
          final NodeList residueList = node.getChildNodes();
          for (int j = 0; j < residueList.getLength(); j++) {
            final Node residueNode = residueList.item(j);
            if (residueNode.getNodeType() == Node.ELEMENT_NODE) {
              if (residueNode.getNodeName().equalsIgnoreCase("celldesigner:modificationResidue")) {
                protein.addModificationResidue(getModificationResidue(residueNode));
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfModificationResidues " + residueNode.getNodeName());
              }
            }
          }
        } else if (node.getNodeName().equals("celldesigner:listOfBindingRegions")) {
          final NodeList residueList = node.getChildNodes();
          for (int j = 0; j < residueList.getLength(); j++) {
            final Node residueNode = residueList.item(j);
            if (residueNode.getNodeType() == Node.ELEMENT_NODE) {
              if (residueNode.getNodeName().equalsIgnoreCase("celldesigner:bindingRegion")) {
                protein.addModificationResidue(getBindingRegion(residueNode));
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfModificationResidues " + residueNode.getNodeName());
              }
            }
          }
        } else if (node.getNodeName().equals("celldesigner:notes")) {
          protein.setNotes(getRap().getNotes(node));
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:protein " + node.getNodeName());
        }
      }
    }

    return new Pair<String, CellDesignerProtein<?>>(identifier, protein);
  }

  @Override
  public String toXml(final Protein originalProtein) {
    final SpeciesWithModificationData protein = getProteinData(originalProtein.getName());

    String attributes = "";
    String result = "";
    attributes += " id=\"" + protein.getProteinId() + "\"";

    if (protein.getName() != null && !protein.getName().isEmpty()) {
      attributes += " name=\"" + XmlParser.escapeXml(encodeName(protein.getName())) + "\"";
    }
    String type = null;
    final ProteinMapping mapping = ProteinMapping.getMappingByClass(originalProtein.getClass());
    if (mapping != null) {
      type = mapping.getCellDesignerString();
    } else {
      throw new InvalidArgumentException("Invalid protein class type: " + protein.getClass().getName());
    }
    attributes += " type=\"" + type + "\"";
    result += "<celldesigner:protein" + attributes + ">\n";

    final List<Residue> residues = protein.getResidues();
    final List<BindingRegion> bindingRegions = protein.getBindingRegions();

    if (!residues.isEmpty()) {
      result += "<celldesigner:listOfModificationResidues>";
      for (final Residue mr : residues) {
        result += modificationResidueXmlParser.toXml(mr);
      }
      result += "</celldesigner:listOfModificationResidues>\n";
    }
    if (!bindingRegions.isEmpty()) {
      result += "<celldesigner:listOfBindingRegions>";
      for (final BindingRegion mr : bindingRegions) {
        result += modificationResidueXmlParser.toXml(mr);
      }
      result += "</celldesigner:listOfBindingRegions>\n";
    }
    result += "</celldesigner:protein>\n";
    return result;
  }

  private Map<String, SpeciesWithModificationData> proteinDataByName;

  public String getProteinId(final Protein protein) {
    return getProteinData(protein.getName()).getProteinId();
  }

  public SpeciesWithModificationData getProteinData(final String name) {
    return proteinDataByName.get(name);
  }

  private CellDesignerModificationResidue getModificationResidue(final Node residueNode) throws InvalidXmlSchemaException {
    final CellDesignerModificationResidue residue = new CellDesignerModificationResidue();
    residue.setIdModificationResidue(XmlParser.getNodeAttr("id", residueNode));
    residue.setName(XmlParser.getNodeAttr("name", residueNode));
    residue.setSide(XmlParser.getNodeAttr("side", residueNode));
    residue.setAngle(XmlParser.getNodeAttr("angle", residueNode));
    residue.setModificationType(ModificationType.RESIDUE);
    final NodeList list = residueNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      final Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        throw new InvalidXmlSchemaException(
            "Unknown element of celldesigner:modificationResidue " + node.getNodeName());
      }
    }
    return residue;
  }

  private CellDesignerModificationResidue getBindingRegion(final Node residueNode) throws InvalidXmlSchemaException {
    final CellDesignerModificationResidue residue = new CellDesignerModificationResidue();
    residue.setIdModificationResidue(XmlParser.getNodeAttr("id", residueNode));
    residue.setName(XmlParser.getNodeAttr("name", residueNode));
    residue.setSize(XmlParser.getNodeAttr("size", residueNode));
    residue.setAngle(XmlParser.getNodeAttr("angle", residueNode));
    residue.setModificationType(ModificationType.BINDING_REGION);
    final NodeList list = residueNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      final Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        throw new InvalidXmlSchemaException(
            "Unknown element of celldesigner:modificationResidue " + node.getNodeName());
      }
    }
    return residue;
  }

  public void initializeProteinData(final Collection<? extends SpeciesWithModificationResidue> collection) {
    if (proteinDataByName == null) {
      proteinDataByName = new HashMap<>();
    }

    for (final SpeciesWithModificationResidue species : collection) {
      if (proteinDataByName.get(species.getName()) == null) {
        proteinDataByName.put(species.getName(), new SpeciesWithModificationData("p_" + proteinIdCounter++, species.getName()));
      }
      proteinDataByName.get(species.getName()).addInfoFromSpecies(species);
    }
  }
}
