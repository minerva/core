package lcsb.mapviewer.converter.model.celldesigner.structure;

import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;

/**
 * Class representing CellDesigner {@link Rna}.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerRna extends CellDesignerSpecies<Rna> {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * List of rna regions (some rna sequences) in this object.
   */
  private List<CellDesignerModificationResidue> regions = new ArrayList<>();

  /**
   * Constructor that initializes rna with the data passed in the argument.
   * 
   * @param species
   *          original species used for data initialization
   */
  public CellDesignerRna(final CellDesignerSpecies<?> species) {
    super(species);
    if (species instanceof CellDesignerRna) {
      CellDesignerRna rna = (CellDesignerRna) species;
      for (final CellDesignerModificationResidue region : rna.getRegions()) {
        addRegion(new CellDesignerModificationResidue(region));
      }
    }
  }

  /**
   * Default constructor.
   */
  public CellDesignerRna() {
    super();
  }

  @Override
  public CellDesignerRna copy() {
    if (this.getClass() == CellDesignerRna.class) {
      return new CellDesignerRna(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public void update(final CellDesignerSpecies<?> species) {
    super.update(species);
    if (species instanceof CellDesignerRna) {
      CellDesignerRna rna = (CellDesignerRna) species;

      for (final CellDesignerModificationResidue region : rna.getRegions()) {
        updateRegion(region);
      }
    }
  }

  @Override
  public Rna createModelElement(final String aliasId) {
    Rna result = new Rna(aliasId);
    super.setModelObjectFields(result);
    return result;
  }

  @Override
  public void updateModelElementAfterLayoutAdded(final Species species) {
    Rna rna = (Rna) species;
    for (final CellDesignerModificationResidue region : regions) {
      ModificationResidue mr = region.createModificationResidue(rna);
      if (mr instanceof CodingRegion) {
        rna.addCodingRegion((CodingRegion) mr);
      } else if (mr instanceof ModificationSite) {
        rna.addModificationSite((ModificationSite) mr);
      } else if (mr instanceof ProteinBindingDomain) {
        rna.addProteinBindingDomain((ProteinBindingDomain) mr);
      } else {
        throw new InvalidArgumentException("Cannot add modification residue to element: " + mr.getClass());
      }
    }
  }

  /**
   * Updates region in the rna. If region doesn't exist then it is added.
   *
   * @param param
   *          region that with the data to update
   */
  private void updateRegion(final CellDesignerModificationResidue param) {
    for (final CellDesignerModificationResidue region : regions) {
      if (region.getIdModificationResidue().equals(param.getIdModificationResidue())) {
        region.update(param);
        return;
      }
    }
    addRegion(new CellDesignerModificationResidue(param));
  }

  /**
   * Adds region. If the region with given id exists then the data of this
   * region is copied to the one that is already in the {@link CellDesignerRna}.
   *
   * @param rnaRegion
   *          region to add
   */
  public void addRegion(final CellDesignerModificationResidue rnaRegion) {
    for (final CellDesignerModificationResidue region2 : regions) {
      if (region2.getIdModificationResidue().equals(rnaRegion.getIdModificationResidue())) {
        region2.update(rnaRegion);
        return;
      }
    }

    regions.add(rnaRegion);
    rnaRegion.setSpecies(this);

  }

  /**
   * @return the regions
   * @see #regions
   */
  public List<CellDesignerModificationResidue> getRegions() {
    return regions;
  }

  /**
   * @param regions
   *          the regions to set
   * @see #regions
   */
  public void setRegions(final List<CellDesignerModificationResidue> regions) {
    this.regions = regions;
  }

}
