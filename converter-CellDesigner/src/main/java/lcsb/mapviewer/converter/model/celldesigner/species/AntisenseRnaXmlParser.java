package lcsb.mapviewer.converter.model.celldesigner.species;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerAntisenseRna;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.ModificationType;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class that performs parsing of the CellDesigner xml for AntisenseRna object.
 *
 * @author Piotr Gawron
 */
public class AntisenseRnaXmlParser extends AbstractElementXmlParser<CellDesignerAntisenseRna, AntisenseRna> {

  /**
   * List of {@link CellDesignerElement celldesigner elements} obtained during
   * parsing process.
   */
  private final CellDesignerElementCollection elements;

  private final ModificationResidueXmlParser modificationResidueXmlParser;

  /**
   * Default constructor.
   *
   * @param elements list of {@link CellDesignerElement celldesigner elements} obtained
   *                 during parsing process
   */
  public AntisenseRnaXmlParser(final CellDesignerElementCollection elements) {
    this.elements = elements;
    this.modificationResidueXmlParser = new ModificationResidueXmlParser(elements);
  }

  @Override
  public Pair<String, CellDesignerAntisenseRna> parseXmlElement(final Node antisenseRnaNode)
      throws InvalidXmlSchemaException {
    final CellDesignerAntisenseRna antisenseRna = new CellDesignerAntisenseRna();
    final String identifier = XmlParser.getNodeAttr("id", antisenseRnaNode);
    antisenseRna.setName(decodeName(XmlParser.getNodeAttr("name", antisenseRnaNode)));
    final NodeList list = antisenseRnaNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      final Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equals("celldesigner:notes")) {
          antisenseRna.setNotes(getRap().getNotes(node));
        } else if (node.getNodeName().equals("celldesigner:listOfRegions")) {
          final NodeList residueList = node.getChildNodes();
          for (int j = 0; j < residueList.getLength(); j++) {
            final Node residueNode = residueList.item(j);
            if (residueNode.getNodeType() == Node.ELEMENT_NODE) {
              if (residueNode.getNodeName().equalsIgnoreCase("celldesigner:region")) {
                antisenseRna.addRegion(getAntisenseRnaRegion(residueNode));
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfRegions " + residueNode.getNodeName());
              }
            }
          }
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:antisenseRna " + node.getNodeName());
        }
      }
    }
    final Pair<String, CellDesignerAntisenseRna> result = new Pair<String, CellDesignerAntisenseRna>(identifier,
        antisenseRna);
    return result;
  }

  @Override
  public String toXml(final AntisenseRna antisenseRna) {
    String attributes = " type=\"ANTISENSE_RNA\" ";
    final StringBuilder result = new StringBuilder();
    attributes += " id=\"ar_" + elements.getElementId(antisenseRna) + "\"";
    if (!antisenseRna.getName().isEmpty()) {
      attributes += " name=\"" + XmlParser.escapeXml(encodeName(antisenseRna.getName())) + "\"";
    }
    result.append("<celldesigner:AntisenseRNA").append(attributes).append(">");

    if (!antisenseRna.getModificationResidues().isEmpty()) {
      result.append("<celldesigner:listOfRegions>");
      for (final ModificationResidue region : antisenseRna.getModificationResidues()) {
        result.append(modificationResidueXmlParser.toXml(region));
      }
      result.append("</celldesigner:listOfRegions>");
    }
    result.append("</celldesigner:AntisenseRNA>");
    return result.toString();
  }

  /**
   * Method that parse xml node into AntisenseRnaRegion element.
   *
   * @param regionNode xml node to parse
   * @return AntisenseRnaRegion object from xml node
   * @throws InvalidXmlSchemaException thrown when input xml node doesn't follow defined schema
   */
  private CellDesignerModificationResidue getAntisenseRnaRegion(final Node regionNode) throws InvalidXmlSchemaException {
    final CellDesignerModificationResidue residue = new CellDesignerModificationResidue();
    residue.setIdModificationResidue(XmlParser.getNodeAttr("id", regionNode));
    residue.setName(XmlParser.getNodeAttr("name", regionNode));
    residue.setSize(XmlParser.getNodeAttr("size", regionNode));
    residue.setPos(XmlParser.getNodeAttr("pos", regionNode));
    final String typeString = XmlParser.getNodeAttr("type", regionNode);
    if (typeString != null) {
      try {
        residue.setModificationType(ModificationType.getByCellDesignerName(typeString));
      } catch (final InvalidArgumentException e) {
        throw new InvalidXmlSchemaException(e);
      }
    }

    final NodeList list = regionNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      final Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        throw new InvalidXmlSchemaException("Unknown element of celldesigner:region " + node.getNodeName());
      }
    }
    return residue;
  }

}
