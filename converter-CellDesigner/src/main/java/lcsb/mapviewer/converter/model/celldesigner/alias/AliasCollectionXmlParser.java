package lcsb.mapviewer.converter.model.celldesigner.alias;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This class contains function to parse CellDesigner xml nodes containing
 * collection of aliases. It also contains functionality to do reverse operation
 * - transform set of aliases into xml node.
 * 
 * @author Piotr Gawron
 * 
 */
public class AliasCollectionXmlParser {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger();

  /**
   * Single SpeciesAlias parser for CellDesigner node.
   */
  private SpeciesAliasXmlParser speciesAliasParser = null;

  /**
   * Single ComplexAlias parser for CellDesigner node.
   */
  private ComplexAliasXmlParser complexAliasParser = null;

  /**
   * Single CompartmentAlias parser for CellDesigner node.
   */
  private CompartmentAliasXmlParser compartmentAliasParser = null;

  /**
   * Annotation parser.
   */
  private RestAnnotationParser rap = new RestAnnotationParser();

  /**
   * Default parser. As an parameter model object for which parsing is done is
   * required. This is due to the fact that CellDesigner model is very tangled and
   * very often data are distributed in many different places and in other places
   * they are missing...
   * 
   * @param model
   *          map model for which parsing is performed
   * @param elements
   *          collection of {@link CellDesignerElement cell designer elements}
   *          parsed from xml
   */
  public AliasCollectionXmlParser(final CellDesignerElementCollection elements, final Model model) {
    speciesAliasParser = new SpeciesAliasXmlParser(elements, model);
    complexAliasParser = new ComplexAliasXmlParser(elements, model);
    compartmentAliasParser = new CompartmentAliasXmlParser(elements, model);
  }

  /**
   * Parses xml node containing list of species aliases into collection of
   * SpeciesAlias.
   * 
   * @param aliasListNode
   *          xml node to be parsed
   * @return collection of SpeciesAlias obtained from xml node
   * @throws InvalidXmlSchemaException
   *           thrown when there is a problem with xml
   */
  public List<Species> parseXmlSpeciesAliasCollection(final Node aliasListNode) throws InvalidXmlSchemaException {
    List<Species> result = new ArrayList<Species>();
    NodeList nodes = aliasListNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:speciesAlias")) {
          Species alias = speciesAliasParser.parseXmlAlias(node);
          rap.processNotes(alias);
          result.add(alias);
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:listOfSpeciesAliases: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Creates xml string representing collection of SpeciesAlias.
   * 
   * @param collection
   *          collection of SpeciesAlias
   * @return xml string representing collection of SpeciesAlias
   */
  public String speciesAliasCollectionToXmlString(final Collection<Species> collection) {
    StringBuilder sb = new StringBuilder("<celldesigner:listOfSpeciesAliases>\n");
    for (final Species alias : collection) {
      sb.append(speciesAliasParser.toXml(alias));
    }
    sb.append("</celldesigner:listOfSpeciesAliases>\n");
    return sb.toString();
  }

  /**
   * Parses xml node containing list of complex aliases into collection of
   * ComplexAlias.
   * 
   * @param aliasListNode
   *          xml node to be parsed
   * @return collection of ComplexAlias obtained from xml node
   * @throws InvalidXmlSchemaException
   *           thrown when xmlString is invalid
   */
  public List<Complex> parseXmlComplexAliasCollection(final Node aliasListNode) throws InvalidXmlSchemaException {
    List<Complex> result = new ArrayList<Complex>();
    NodeList nodes = aliasListNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:complexSpeciesAlias")) {
          Complex alias = complexAliasParser.parseXmlAlias(node);
          result.add(alias);
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:listOfComplexSpeciesAliases: " + node.getNodeName());
        }
      }
    }
    for (final Complex complexAlias : result) {
      complexAliasParser.addReference(complexAlias);
    }
    return result;
  }

  /**
   * Creates xml string representing collection of ComplexAlias.
   * 
   * @param collection
   *          collection of ComplexAlias
   * @return xml string representing collection of ComplexAlias
   */
  public String complexAliasCollectionToXmlString(final Collection<Complex> collection) {
    StringBuilder sb = new StringBuilder("<celldesigner:listOfComplexSpeciesAliases>\n");
    for (final Complex alias : collection) {
      sb.append(complexAliasParser.toXml(alias));
    }
    sb.append("</celldesigner:listOfComplexSpeciesAliases>\n");
    return sb.toString();
  }

  /**
   * Parses xml node containing list of compartment aliases into collection of
   * CompartmentAlias.
   * 
   * @param aliasListNode
   *          xml node to be parsed
   * @return collection of CompartmentAlias obtained from xml node
   * @throws InvalidXmlSchemaException
   *           thrown when xmlString is invalid
   */
  public List<Compartment> parseXmlCompartmentAliasCollection(final Node aliasListNode) throws InvalidXmlSchemaException {
    List<Compartment> result = new ArrayList<>();
    NodeList nodes = aliasListNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:compartmentAlias")) {
          Compartment alias = compartmentAliasParser.parseXmlAlias(node);
          result.add(alias);
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:listOfCompartmentAliases: " + node.getNodeName());
        }
      }
    }
    assignParents(result);

    return result;
  }

  /**
   * Assing compartment parents for list of compartments.
   * 
   * @param compartments
   *          compartments that are looking for parents
   */
  private void assignParents(final List<Compartment> compartments) {
    Compartment nullParent = new Compartment("null");
    nullParent.setWidth(Double.MAX_VALUE);
    nullParent.setHeight(Double.MAX_VALUE);
    nullParent.setX(0.0);
    nullParent.setY(0.0);
    for (final Compartment child : compartments) {
      Compartment parent = nullParent;
      for (final Compartment potentialParent : compartments) {
        if (potentialParent.contains(child)) {
          if (parent.getSize() > potentialParent.getSize()) {
            parent = potentialParent;
          }
        }
      }
      if (parent != nullParent) {
        child.setCompartment(parent);
        parent.addElement(child);
      }
    }
  }

  /**
   * Creates xml string representing collection of CompartmentAlias.
   * 
   * @param collection
   *          collection of CompartmentAlias
   * @return xml string representing collection of CompartmentAlias
   */
  public String compartmentAliasCollectionToXmlString(final Collection<Compartment> collection) {
    StringBuilder sb = new StringBuilder("<celldesigner:listOfCompartmentAliases>\n");
    for (final Compartment compartment : collection) {
      // artifitial compartment aliases cannot be exported
      if (!(compartment instanceof PathwayCompartment)) {
        sb.append(compartmentAliasParser.toXml(compartment));
      }
    }
    sb.append("</celldesigner:listOfCompartmentAliases>\n");
    return sb.toString();
  }
}
