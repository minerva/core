package lcsb.mapviewer.converter.model.celldesigner.geometry;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.field.AbstractRegionModification;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

/**
 * Class that provides CellDesigner specific graphical information for Gene.
 * It's used for conversion from xml to normal x,y coordinates.
 * 
 * @author Piotr Gawron
 * 
 */
public class GeneCellDesignerAliasConverter extends AbstractCellDesignerAliasConverter<Gene> {

  /**
   * How big should be the arc in rectangle for nucleic acid feature
   * representation.
   */
  private static final int RECTANGLE_CORNER_ARC_SIZE = 5;

  /**
   * Default constructor.
   *
   * @param sbgn
   *          Should the converter use SBGN standard
   */
  protected GeneCellDesignerAliasConverter(final boolean sbgn) {
    super(sbgn);
  }

  @Override
  public Point2D getPointCoordinates(final Gene alias, final CellDesignerAnchor anchor) {
    if (invalidAnchorPosition(alias, anchor)) {
      return alias.getCenter();
    }
    return getRectangleTransformation().getPointOnRectangleByAnchor(alias.getX(), alias.getY(), alias.getWidth(),
        alias.getHeight(), anchor);
  }

  @Override
  public PathIterator getBoundPathIterator(final Gene alias) {
    return getGeneShape(alias).getPathIterator(new AffineTransform());
  }

  @Override
  public Double getCellDesignerPositionByCoordinates(final ModificationResidue mr) {
    Double result = (mr.getX() - mr.getSpecies().getX()) / mr.getSpecies().getWidth();
    result = Math.min(result, 1.0);
    result = Math.max(result, 0.0);
    return result;
  }

  @Override
  public Point2D getCoordinatesByPosition(final Element element, final Double pos, final Double modificationWidth) {
    double x = element.getX() + element.getWidth() * pos + modificationWidth / 2;

    Point2D result = new Point2D.Double(x, element.getY());
    return result;
  }

  @Override
  public Double getCellDesignerSize(final ModificationResidue mr) {
    if (mr instanceof AbstractRegionModification) {
      return ((AbstractRegionModification) mr).getWidth() / mr.getSpecies().getWidth();
    }
    throw new NotImplementedException("Not implemented for: " + this.getClass() + ", " + mr.getClass());
  }

  @Override
  public Double getWidthBySize(final Element element, final Double size) {
    return size * element.getWidth();
  }

  /**
   * Shape representation of the gene alias.
   *
   * @param alias
   *          alias for which we are looking for a Shape
   * @return Shape object that represents alias
   */
  private Shape getGeneShape(final Element alias) {
    if (!isSbgn()) {
      Shape shape;
      shape = new Rectangle2D.Double(alias.getX(), alias.getY(), alias.getWidth(), alias.getHeight());
      return shape;
    } else {
      GeneralPath path = new GeneralPath(GeneralPath.WIND_EVEN_ODD);
      double x = alias.getX();
      double y = alias.getY();
      double width = alias.getWidth();
      double height = alias.getHeight();

      path.moveTo(x, y);
      path.lineTo(x, y + height - RECTANGLE_CORNER_ARC_SIZE);
      path.curveTo(x, y + height, x + RECTANGLE_CORNER_ARC_SIZE, y + height, x + RECTANGLE_CORNER_ARC_SIZE, y + height);
      path.lineTo(x + width - RECTANGLE_CORNER_ARC_SIZE, y + height);
      path.curveTo(x + width, y + height, x + width, y + height - RECTANGLE_CORNER_ARC_SIZE, x + width,
          y + height - RECTANGLE_CORNER_ARC_SIZE);
      path.lineTo(x + width, y);
      path.closePath();
      return path;
    }
  }

}
