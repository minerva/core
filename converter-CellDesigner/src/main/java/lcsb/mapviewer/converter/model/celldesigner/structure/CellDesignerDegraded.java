package lcsb.mapviewer.converter.model.celldesigner.structure;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Degraded;

/**
 * Class representing CellDesigner {@link Degraded}.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerDegraded extends CellDesignerSpecies<Degraded> {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Constructor that copies the data from species given in the argument.
   * 
   * @param species
   *          parent species from which we want to copy data
   */
  public CellDesignerDegraded(final CellDesignerSpecies<?> species) {
    super(species);
  }

  /**
   * Default constructor.
   */
  public CellDesignerDegraded() {
  }

  @Override
  public CellDesignerDegraded copy() {
    if (this.getClass() == CellDesignerDegraded.class) {
      return new CellDesignerDegraded(super.copy());
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public Degraded createModelElement(final String aliasId) {
    Degraded result = new Degraded(aliasId);
    super.setModelObjectFields(result);
    return result;
  }

}
