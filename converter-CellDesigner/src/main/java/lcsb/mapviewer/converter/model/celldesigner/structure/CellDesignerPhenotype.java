package lcsb.mapviewer.converter.model.celldesigner.structure;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Phenotype;

/**
 * Class representing CellDesigner {@link Phenotype} object.
 * 
 * @author Piotr Gawron
 * 
 */
public class CellDesignerPhenotype extends CellDesignerSpecies<Phenotype> {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Constructor that initializes phenotype with the data passed in the argument.
   * 
   * @param species
   *          original species used for data initialization
   */
  public CellDesignerPhenotype(final CellDesignerSpecies<?> species) {
    super(species);
  }

  /**
   * Default constructor.
   */
  public CellDesignerPhenotype() {
  }

  @Override
  public CellDesignerPhenotype copy() {
    if (this.getClass() == CellDesignerPhenotype.class) {
      return new CellDesignerPhenotype(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public Phenotype createModelElement(final String aliasId) {
    Phenotype result = new Phenotype(aliasId);
    super.setModelObjectFields(result);
    return result;
  }

}
