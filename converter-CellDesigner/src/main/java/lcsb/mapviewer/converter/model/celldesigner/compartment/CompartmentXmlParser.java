package lcsb.mapviewer.converter.model.celldesigner.compartment;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CommonXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.species.AbstractElementXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerCompartment;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;

/**
 * Parser of CellDesginer xml nodes for compartment elements.
 * 
 * @author Piotr Gawron
 * 
 */
public class CompartmentXmlParser extends AbstractElementXmlParser<CellDesignerCompartment, Compartment> {

  /**
   * Parser used to retrieve Miriam data for the element.
   */
  private XmlAnnotationParser xmlAnnotationParser;

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed
   * from xml.
   */
  private CellDesignerElementCollection elements;

  /**
   * Xml parser used for processing notes into structured data.
   */
  private RestAnnotationParser rap = new RestAnnotationParser();

  /**
   * Default constructor.
   * 
   * @param elements
   *          collection of {@link CellDesignerElement cell designer elements}
   *          parsed from xml
   */
  public CompartmentXmlParser(final CellDesignerElementCollection elements) {
    this.elements = elements;
    xmlAnnotationParser = new XmlAnnotationParser(CommonXmlParser.RELATION_TYPES_SUPPORTED_BY_CELL_DESIGNER);
  }

  @Override
  public Pair<String, CellDesignerCompartment> parseXmlElement(final Node compartmentNode) throws CompartmentParserException {
    CellDesignerCompartment compartment = new CellDesignerCompartment();
    // we ignore metaid - it's useless and obstruct data model
    // compartment.setMetaId(XmlParser.getNodeAttr("metaid", compartmentNode));
    compartment.setElementId(XmlParser.getNodeAttr("id", compartmentNode));
    compartment.setName(decodeName(XmlParser.getNodeAttr("name", compartmentNode)));
    try {
      NodeList nodes = compartmentNode.getChildNodes();
      for (int x = 0; x < nodes.getLength(); x++) {
        Node node = nodes.item(x);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          if (node.getNodeName().equalsIgnoreCase("annotation")) {
            parseAnnotationNode(compartment, node);
          } else if (node.getNodeName().equalsIgnoreCase("notes")) {
            compartment.setNotes(rap.getNotes(node));
          } else {
            throw new InvalidXmlSchemaException("Unknown element of Compartment: " + node.getNodeName());
          }
        }
      }
      return new Pair<String, CellDesignerCompartment>(compartment.getElementId(), compartment);
    } catch (final InvalidXmlSchemaException e) {
      throw new CompartmentParserException(compartment.getElementId(), e);
    }
  }

  @Override
  public String toXml(final Compartment compartment) {
    StringBuilder sb = new StringBuilder("");
    sb.append("<compartment ");
    sb.append("metaid=\"" + elements.getElementId(compartment) + "\" ");
    sb.append("id=\"" + elements.getElementId(compartment) + "\" ");
    sb.append("name=\"" + XmlParser.escapeXml(encodeName(compartment.getName())) + "\" ");
    sb.append("size=\"1\" ");
    sb.append("units=\"volume\" ");
    String parentName = "default";

    if (compartment.getCompartment() != null && !(compartment.getCompartment() instanceof PathwayCompartment)) {
      parentName = elements.getElementId(compartment.getCompartment());
    }

    // default is the top level compartment
    if (!compartment.getElementId().equals("default")) {
      sb.append("outside=\"" + parentName + "\" ");
    }
    sb.append(">\n");
    sb.append("<notes>");
    sb.append("<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><title/></head><body>");
    RestAnnotationParser rap = new RestAnnotationParser();
    sb.append(rap.createAnnotationString(compartment));
    if (compartment.getNotes() != null) {
      sb.append(compartment.getNotes());
    }
    sb.append("</body></html>");
    sb.append("</notes>\n");
    sb.append("<annotation>\n");
    sb.append("<celldesigner:extension>\n");
    if (!compartment.getName().isEmpty()) {
      sb.append("<celldesigner:name>" + XmlParser.escapeXml(encodeName(compartment.getName())) + "</celldesigner:name>\n");
    }
    sb.append("</celldesigner:extension>\n");
    sb.append(xmlAnnotationParser.dataSetToXmlString(compartment.getMiriamData(), elements.getElementId(compartment)));
    sb.append("</annotation>\n");
    sb.append("</compartment>\n");
    return sb.toString();
  }

  /**
   * Process annotation part of the xml node.
   *
   * @param compartment
   *          object that we create
   * @param xmlNode
   *          annotation xml node
   * @throws InvalidXmlSchemaException
   *           thrown when there is a problem with xml
   */
  private void parseAnnotationNode(final CellDesignerCompartment compartment, final Node xmlNode) throws InvalidXmlSchemaException {
    NodeList annotationNodes = xmlNode.getChildNodes();
    for (int y = 0; y < annotationNodes.getLength(); y++) {
      Node node = annotationNodes.item(y);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("rdf:RDF")) {
          compartment.addMiriamData(xmlAnnotationParser.parseRdfNode(node,
              new LogMarker(ProjectLogEntryType.PARSING_ISSUE, compartment.getClass().getSimpleName(),
                  compartment.getElementId(), "")));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:extension")) {
          parseExtensionNode(compartment, node);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:name")) {
          compartment.setName(decodeName(XmlParser.getNodeValue(node)));
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown xml tag of compartment->annotation: " + node.getNodeName());
        }
      }
    }
  }

  /**
   * Process extension part of the xml node.
   *
   * @param compartment
   *          object that we create
   * @param xmlNode
   *          annotation xml node
   * @throws InvalidXmlSchemaException
   *           thrown when xmlNode is invalid
   */
  private void parseExtensionNode(final CellDesignerCompartment compartment, final Node xmlNode) throws InvalidXmlSchemaException {
    NodeList annotationNodes = xmlNode.getChildNodes();
    for (int y = 0; y < annotationNodes.getLength(); y++) {
      Node annotationNode = annotationNodes.item(y);
      if (annotationNode.getNodeType() == Node.ELEMENT_NODE) {
        if (annotationNode.getNodeName().equalsIgnoreCase("celldesigner:name")) {
          // ignore it (we already have this name)
          continue;
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of Compartment/annotation/extension: " + annotationNode.getNodeName());
        }
      }
    }
  }
}
