package lcsb.mapviewer.converter.model.celldesigner;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.layout.BlockDiagram;
import lcsb.mapviewer.model.map.layout.ElementGroup;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Parser used for parsing CellDesigner xml to get {@link Layer} object.
 *
 * @author Piotr Gawron
 */
public class LayerXmlParser {

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  /**
   * Parser for common CellDesigner structures.
   */
  private final CommonXmlParser commonParser = new CommonXmlParser();

  private int problematicIdCounter = 1000;

  /**
   * Parses CellDesigner xml node with collection of layers into collection of
   * Layers.
   *
   * @param layersNode xml node
   * @return collection of layers
   * @throws InvalidXmlSchemaException thrown when xml node contains data that is not supported by xml
   *                                   schema
   */
  public Collection<Layer> parseLayers(final Node layersNode) throws InvalidXmlSchemaException {
    List<Layer> result = new ArrayList<Layer>();
    NodeList nodes = layersNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:layer")) {
          Layer layer = getLayer(node);
          result.add(layer);
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:listOfLayers: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Transforms collection of layers into CellDesigner xml node.
   *
   * @param layers collection of layers
   * @return xml node representing layers collection
   */
  public String layerCollectionToXml(final Collection<Layer> layers) {
    for (final Layer layer : layers) {
      if (NumberUtils.isParsable(layer.getLayerId())) {
        problematicIdCounter = Math.max(problematicIdCounter, Integer.parseInt(layer.getLayerId()));
      }
    }
    StringBuilder result = new StringBuilder();
    result.append("<celldesigner:listOfLayers>");
    for (final Layer layer : layers) {
      result.append(layerToXml(layer));
    }
    result.append("</celldesigner:listOfLayers>\n");
    return result.toString();
  }

  /**
   * Parses collection of block diagrams and adds them into the model.
   *
   * @param model   model where the data is stored.
   * @param xmlNode xml node
   * @throws InvalidXmlSchemaException thrown when xml node contains data that is not supported by xml
   *                                   schema
   */
  public void parseBlocks(final Model model, final Node xmlNode) throws InvalidXmlSchemaException {
    NodeList nodes = xmlNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:blockDiagram")) {
          model.addBlockDiagream(getBlockDiagram(node, model));
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:listOfBlockDiagrams: " + node.getNodeName());
        }
      }
    }
  }

  /**
   * Parses block diagrams from CellDesigner xml. Important! It's not yet
   * implemented.
   *
   * @param node  xml node to parse
   * @param model model where data is stored
   * @return parsed block diagram
   */
  private BlockDiagram getBlockDiagram(final Node node, final Model model) {
    logger.warn("BlockDiagrams are not implemented");
    return null;
  }

  /**
   * Parses CellDesigner node with collection of Alias Group and adds the groups
   * into this model.
   *
   * @param model   model where the data is stored
   * @param xmlNode xml node
   * @throws InvalidXmlSchemaException thrown when xml node contains data that is not supported by xml
   *                                   schema
   */
  public void parseGroups(final Model model, final Node xmlNode) throws InvalidXmlSchemaException {
    NodeList nodes = xmlNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:group")) {
          model.addElementGroup(getAliasGroup(node, model));
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:listOfGroups: " + node.getNodeName());
        }
      }
    }
  }

  /**
   * Parses xml node for alias group.
   *
   * @param groupNode xml node
   * @param model     model where the data is stored
   * @return parsed alias group
   * @throws InvalidXmlSchemaException thrown when xml node contains data that is not supported by xml
   *                                   schema
   */
  ElementGroup getAliasGroup(final Node groupNode, final Model model) throws InvalidXmlSchemaException {
    ElementGroup result = new ElementGroup();
    String id = XmlParser.getNodeAttr("id", groupNode);
    result.setIdGroup(id);
    String members = XmlParser.getNodeAttr("members", groupNode);

    String[] list = members.split(",");
    for (final String string : list) {
      Element alias = model.getElementByElementId(string);
      if (alias == null) {
        throw new InvalidGroupException("Group \"" + id + "\" contains alias with id: \"" + string
            + "\", but such alias doesn't exist in the model.");
      }
      result.addElement(alias);
    }

    NodeList nodes = groupNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        throw new InvalidXmlSchemaException("Unknown element of celldesigner:group: " + node.getNodeName());
      }
    }

    return result;
  }

  /**
   * Parses CellDesigner xml node for single layer.
   *
   * @param layerNode xml node
   * @return Layer that corresponds to CellDesigner xml node.
   * @throws InvalidXmlSchemaException thrown when xml node contains data that is not supported by xml
   *                                   schema
   */
  Layer getLayer(final Node layerNode) throws InvalidXmlSchemaException {
    Layer layer = new Layer();
    layer.setLayerId(XmlParser.getNodeAttr("id", layerNode));
    if (NumberUtils.isCreatable(layer.getLayerId())) {
      layer.setZ(Integer.parseInt(layer.getLayerId()));
    } else {
      layer.setZ(1);
    }
    layer.setName(XmlParser.getNodeAttr("name", layerNode));
    layer.setLocked(XmlParser.getNodeAttr("locked", layerNode));
    layer.setVisible(XmlParser.getNodeAttr("visible", layerNode));
    NodeList list = layerNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equals("celldesigner:listOfTexts")) {
          NodeList residueList = node.getChildNodes();
          for (int j = 0; j < residueList.getLength(); j++) {
            Node textNode = residueList.item(j);
            if (textNode.getNodeType() == Node.ELEMENT_NODE) {
              if (textNode.getNodeName().equalsIgnoreCase("celldesigner:layerSpeciesAlias")) {
                layer.addLayerText(getLayerText(textNode));
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfTexts " + textNode.getNodeName());
              }
            }
          }
        } else if (node.getNodeName().equals("celldesigner:listOfSquares")) {
          NodeList residueList = node.getChildNodes();
          for (int j = 0; j < residueList.getLength(); j++) {
            Node textNode = residueList.item(j);
            if (textNode.getNodeType() == Node.ELEMENT_NODE) {
              if (textNode.getNodeName().equalsIgnoreCase("celldesigner:layerCompartmentAlias")) {
                if (XmlParser.getNodeAttr("type", textNode).equalsIgnoreCase("SQUARE")) {
                  layer.addLayerRect(getLayerRect(textNode));
                } else if (XmlParser.getNodeAttr("type", textNode).equalsIgnoreCase("OVAL")) {
                  layer.addLayerOval(getLayerOval(textNode));
                } else {
                  throw new InvalidXmlSchemaException(
                      "Unknown celldesigner:layerCompartmentAlias type: " + XmlParser.getNodeAttr("type", textNode));
                }
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfSquares " + textNode.getNodeName());
              }
            }
          }
        } else if (node.getNodeName().equals("celldesigner:listOfFreeLines")) {
          NodeList residueList = node.getChildNodes();
          for (int j = 0; j < residueList.getLength(); j++) {
            Node textNode = residueList.item(j);
            if (textNode.getNodeType() == Node.ELEMENT_NODE) {
              if (textNode.getNodeName().equalsIgnoreCase("celldesigner:layerFreeLine")) {
                layer.addLayerLine(getLayerLine(textNode));
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfFreeLines " + textNode.getNodeName());
              }
            }
          }
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:layer " + node.getNodeName());
        }
      }
    }

    return layer;
  }

  /**
   * Creates CellDesigner xml for a layer.
   *
   * @param layer object to be transformed into xml
   * @return CellDesigner xml representation for the layer
   */
  String layerToXml(final Layer layer) {
    StringBuilder result = new StringBuilder();
    String id = layer.getLayerId();
    if (!NumberUtils.isParsable(id)) {
      id = (problematicIdCounter++) + "";
    }
    result.append("<celldesigner:layer id =\"" + id + "\" ");
    result.append(" name =\"" + XmlParser.escapeXml(layer.getName()) + "\" ");
    result.append(" locked =\"" + layer.isLocked() + "\" ");
    result.append(" visible =\"" + layer.isVisible() + "\">\n");

    result.append("<celldesigner:listOfTexts>\n");
    for (final LayerText layerText : layer.getTexts()) {
      result.append(layerTextToXml(layerText));
    }
    result.append("</celldesigner:listOfTexts>\n");

    result.append("<celldesigner:listOfSquares>\n");
    for (final LayerRect layerRect : layer.getRectangles()) {
      result.append(layerRectToXml(layerRect));
    }
    for (final LayerOval layerOval : layer.getOvals()) {
      result.append(layerOvalToXml(layerOval));
    }
    result.append("</celldesigner:listOfSquares>\n");

    result.append("<celldesigner:listOfFreeLines>\n");
    for (final PolylineData layerLine : layer.getLines()) {
      result.append(layerLineToXml(layerLine));
    }
    result.append("</celldesigner:listOfFreeLines>\n");

    result.append("</celldesigner:layer>\n");
    return result.toString();
  }

  /**
   * Parses CellDesigner xml node into LayerRect object.
   *
   * @param textNode CellDesigner xml node for layer text object.
   * @return LayerRect instance representing the xml node
   * @throws InvalidXmlSchemaException thrown when xml node contains data that is not supported by xml
   *                                   schema
   */
  LayerRect getLayerRect(final Node textNode) throws InvalidXmlSchemaException {
    LayerRect result = new LayerRect();
    NodeList nodes = textNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:paint")) {
          result.setBorderColor(commonParser.getColor(node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:bounds")) {
          result.setX(XmlParser.getNodeAttr("x", node));
          result.setY(XmlParser.getNodeAttr("y", node));
          result.setWidth(XmlParser.getNodeAttr("w", node));
          result.setHeight(XmlParser.getNodeAttr("h", node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:ispaint")) {
          // ???
          continue;
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:layerCompartmentAlias: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Transforms LayerRect object into CellDesigner xml node.
   *
   * @param layer object to be transformed into xml
   * @return CellDesigner xml node for LayerRect
   */
  String layerRectToXml(final LayerRect layer) {
    return "<celldesigner:layerCompartmentAlias type=\"Square\">"
        + "<celldesigner:paint color=\"" + XmlParser.colorToString(layer.getBorderColor()) + "\"/>"
        + "<celldesigner:bounds x=\"" + layer.getX() + "\" "
        + " y=\"" + layer.getY() + "\" "
        + " w=\"" + layer.getWidth() + "\" "
        + " h=\"" + layer.getHeight() + "\"/>"
        + "</celldesigner:layerCompartmentAlias>";
  }

  /**
   * Parses CellDesigner xml node into LayerLine object.
   *
   * @param lineNode CellDesigner xml node for layer text object.
   * @return LayerLine instance representing the xml node
   * @throws InvalidXmlSchemaException thrown when xml node contains data that is not supported by xml
   *                                   schema
   */
  PolylineData getLayerLine(final Node lineNode) throws InvalidXmlSchemaException {
    PolylineData ld = new PolylineData();

    if (XmlParser.getNodeAttr("isDotted", lineNode).equalsIgnoreCase("true")) {
      ld.getEndAtd().setArrowLineType(LineType.DOTTED);
    }
    if (XmlParser.getNodeAttr("isArrow", lineNode).equalsIgnoreCase("true")) {
      ld.getEndAtd().setArrowType(ArrowType.FULL);
    }
    NodeList nodes = lineNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:bounds")) {
          double sx = Double.parseDouble(XmlParser.getNodeAttr("sx", node));
          double sy = Double.parseDouble(XmlParser.getNodeAttr("sy", node));
          double ex = Double.parseDouble(XmlParser.getNodeAttr("ex", node));
          double ey = Double.parseDouble(XmlParser.getNodeAttr("ey", node));
          Point2D startPoint = new Point2D.Double(sx, sy);
          Point2D endPoint = new Point2D.Double(ex, ey);
          ld.addLine(startPoint, endPoint);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:line")) {
          ld.setColor(XmlParser.stringToColor(XmlParser.getNodeAttr("color", node)));
          ld.setWidth(XmlParser.getNodeAttr("width", node));
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:layerFreeLine: " + node.getNodeName());
        }
      }
    }
    return ld;
  }

  /**
   * Transforms LayerLine object into CellDesigner xml node.
   *
   * @param layer object to be transformed into xml
   * @return CellDesigner xml node for LayerLine
   */
  String layerLineToXml(final PolylineData layer) {
    return "<celldesigner:layerFreeLine "
        + " isDotted=\"" + layer.getEndAtd().getArrowLineType().equals(LineType.DOTTED) + "\" "
        + " isArrow=\"" + layer.getEndAtd().getArrowType().equals(ArrowType.FULL) + "\" >"
        + "<celldesigner:bounds sx=\"" + layer.getStartPoint().getX() + "\" "
        + " sy=\"" + layer.getStartPoint().getY() + "\" "
        + " ex=\"" + layer.getEndPoint().getX() + "\" "
        + " ey=\"" + layer.getEndPoint().getY() + "\" />"
        + "<celldesigner:line "
        + " width=\"" + layer.getWidth() + "\" "
        + " color=\"" + XmlParser.colorToString(layer.getColor()) + "\"/>"
        + "</celldesigner:layerFreeLine>\n";
  }

  /**
   * Parses CellDesigner xml node into LayerOval object.
   *
   * @param textNode CellDesigner xml node for layer text object.
   * @return LayerOval instance representing the xml node
   * @throws InvalidXmlSchemaException thrown when xml node contains data that is not supported by xml
   *                                   schema
   */
  LayerOval getLayerOval(final Node textNode) throws InvalidXmlSchemaException {
    LayerOval result = new LayerOval();
    NodeList nodes = textNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:paint")) {
          result.setColor(commonParser.getColor(node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:bounds")) {
          result.setX(XmlParser.getNodeAttr("x", node));
          result.setY(XmlParser.getNodeAttr("y", node));
          result.setWidth(XmlParser.getNodeAttr("w", node));
          result.setHeight(XmlParser.getNodeAttr("h", node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:ispaint")) {
          // ???
          continue;
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:layerCompartmentAlias: " + node.getNodeName());
        }
      }
    }
    return result;
  }

  /**
   * Transforms LayerOval object into CellDesigner xml node.
   *
   * @param layer object to be transformed into xml
   * @return CellDesigner xml node for LayerOval
   */
  String layerOvalToXml(final LayerOval layer) {
    return "<celldesigner:layerCompartmentAlias type=\"Oval\">"
        + "<celldesigner:paint color=\"" + XmlParser.colorToString(layer.getColor()) + "\"/>"
        + "<celldesigner:bounds x=\"" + layer.getX() + "\" "
        + " y=\"" + layer.getY() + "\" "
        + " w=\"" + layer.getWidth() + "\" "
        + " h=\"" + layer.getHeight() + "\"/>"
        + "</celldesigner:layerCompartmentAlias>";
  }

  /**
   * Parses CellDesigner xml node into LayerText object.
   *
   * @param textNode CellDesigner xml node for layer text object.
   * @return LayerText instance representing the xml node
   * @throws InvalidXmlSchemaException thrown when xml node contains data that is not supported by xml
   *                                   schema
   */
  LayerText getLayerText(final Node textNode) throws InvalidXmlSchemaException {
    LayerText result = new LayerText();
    NodeList nodes = textNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:layerNotes")) {
          String notes = XmlParser.getNodeValue(node).trim();
          result.setNotes(notes);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:paint")) {
          result.setColor(commonParser.getColor(node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:bounds")) {
          result.setX(XmlParser.getNodeAttr("x", node));
          result.setY(XmlParser.getNodeAttr("y", node));
          result.setWidth(XmlParser.getNodeAttr("w", node));
          result.setHeight(XmlParser.getNodeAttr("h", node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:font")) {
          result.setFontSize(XmlParser.getNodeAttr("size", node));
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:layerSpeciesAlias: " + node.getNodeName());
        }
      }
    }
    new RestAnnotationParser().processNotes(result.getNotes(), result);
    return result;
  }

  String removeBackgroundColor(final String notes) {
    return removeColor(notes, "BackgroundColor");
  }

  String removeBorderColor(final String notes) {
    return removeColor(notes, "BorderColor");
  }

  private String removeColor(final String notes, final String string) {
    String[] lines = notes.split("[\n\r]+");
    StringBuilder result = new StringBuilder();
    for (final String line : lines) {
      if (!line.startsWith(string + "=") && !line.startsWith(string + ":")) {
        result.append(line + "\n");
      }
    }
    return result.toString();
  }

  /**
   * Transforms LayerText object into CellDesigner xml node.
   *
   * @param layer object to be transformed into xml
   * @return CellDesigner xml node for LayerText
   */
  String layerTextToXml(final LayerText layer) {
    StringBuilder result = new StringBuilder();
    result.append("<celldesigner:layerSpeciesAlias>");
    result.append("<celldesigner:layerNotes>\n");
    String notes = commonParser.getNotesXmlContent(layer.getNotes());
    if (notes == null) {
      notes = "";
    }
    if (!layer.getBackgroundColor().equals(Color.LIGHT_GRAY)) {
      notes += "\nBackgroundColor:" + new ColorParser().colorToHtml(layer.getBackgroundColor());
    }
    if (!layer.getBorderColor().equals(Color.LIGHT_GRAY)) {
      notes += "\nBorderColor:" + new ColorParser().colorToHtml(layer.getBorderColor());
    }
    result.append(notes);
    result.append("\n</celldesigner:layerNotes>");
    result.append("<celldesigner:paint color=\"" + XmlParser.colorToString(layer.getColor()) + "\"/>");
    result.append("<celldesigner:bounds x=\"" + layer.getX() + "\" ");
    result.append(" y=\"" + layer.getY() + "\" ");
    result.append(" w=\"" + layer.getWidth() + "\" ");
    result.append(" h=\"" + layer.getHeight() + "\"/>");
    result.append("<celldesigner:font size=\"" + layer.getFontSize().intValue() + "\"/>");
    result.append("</celldesigner:layerSpeciesAlias>\n");
    return result.toString();
  }

}
