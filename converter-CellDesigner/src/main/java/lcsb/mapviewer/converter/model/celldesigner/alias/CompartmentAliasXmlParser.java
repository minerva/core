package lcsb.mapviewer.converter.model.celldesigner.alias;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerCompartment;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.compartment.BottomSquareCompartment;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.LeftSquareCompartment;
import lcsb.mapviewer.model.map.compartment.OvalCompartment;
import lcsb.mapviewer.model.map.compartment.RightSquareCompartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.compartment.TopSquareCompartment;
import lcsb.mapviewer.model.map.model.Model;

/**
 * Parser for CellDesigner xml for compartment aliases.
 * 
 * @author Piotr Gawron
 * 
 */
public class CompartmentAliasXmlParser extends AbstractAliasXmlParser<Compartment> {

  private static final double MIN_INNER_WIDTH = 1.0;

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger();

  /**
   * Model for which we are parsing aliases.
   */
  private Model model = null;

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed from
   * xml.
   */
  private CellDesignerElementCollection elements;

  /**
   * Default constructor.
   * 
   * @param elements
   *          collection of {@link CellDesignerElement cell designer elements}
   *          parsed from xml
   * @param model
   *          model for which this parser will be used
   */
  public CompartmentAliasXmlParser(final CellDesignerElementCollection elements, final Model model) {
    this.model = model;
    this.elements = elements;
  }

  @Override
  Compartment parseXmlAlias(final Node aliasNode) throws InvalidXmlSchemaException {

    String compartmentId = XmlParser.getNodeAttr("compartment", aliasNode);
    CellDesignerCompartment compartment = elements.getElementByElementId(compartmentId);
    if (compartment == null) {
      throw new InvalidXmlSchemaException("Compartment does not exist in a model: " + compartmentId);
    }
    String aliasId = XmlParser.getNodeAttr("id", aliasNode);
    Compartment result = compartment.createModelElement(aliasId);
    elements.addElement(compartment, aliasId);

    NodeList nodes = aliasNode.getChildNodes();
    String type = null;
    for (int i = 0; i < nodes.getLength(); i++) {
      Node node = nodes.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:class")) {
          type = XmlParser.getNodeValue(node);
          if (type.equalsIgnoreCase("SQUARE")) {
            result = new SquareCompartment(result);
          } else if (type.equalsIgnoreCase("SQUARE_CLOSEUP_NORTH")) {
            result = new BottomSquareCompartment(result, model);
          } else if (type.equalsIgnoreCase("SQUARE_CLOSEUP_SOUTH")) {
            result = new TopSquareCompartment(result, model);
          } else if (type.equalsIgnoreCase("SQUARE_CLOSEUP_WEST")) {
            result = new RightSquareCompartment(result, model);
          } else if (type.equalsIgnoreCase("SQUARE_CLOSEUP_EAST")) {
            result = new LeftSquareCompartment(result, model);
          } else if (type.equalsIgnoreCase("SQUARE_CLOSEUP_NORTHWEST")) {
            result = new SquareCompartment(result);
          } else if (type.equalsIgnoreCase("SQUARE_CLOSEUP_NORTHEAST")) {
            result = new SquareCompartment(result);
          } else if (type.equalsIgnoreCase("SQUARE_CLOSEUP_SOUTHEAST")) {
            result = new SquareCompartment(result);
          } else if (type.equalsIgnoreCase("SQUARE_CLOSEUP_SOUTHWEST")) {
            result = new SquareCompartment(result);
          } else if (type.equalsIgnoreCase("OVAL")) {
            result = new OvalCompartment(result);
          } else {
            throw new InvalidXmlSchemaException("Unknown compartment type: " + type);
          }
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:bounds")) {
          result.setX(XmlParser.getNodeAttr("x", node));
          result.setY(XmlParser.getNodeAttr("y", node));
          result.setWidth(XmlParser.getNodeAttr("w", node));
          result.setHeight(XmlParser.getNodeAttr("h", node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:doubleLine")) {
          result.setLineThickness(XmlParser.getNodeAttr("thickness", node));
          result.setLineOuterWidth(XmlParser.getNodeAttr("outerWidth", node));
          result.setLineInnerWidth(XmlParser.getNodeAttr("innerWidth", node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:paint")) {
          Color color = getCommonParser().getColor(node);
          result.setFillColor(color);
          if (Objects.equals(color, Color.WHITE)) {
            result.setBorderColor(Color.BLACK);
          } else {
            result.setBorderColor(color);
          }
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:info")) {
          // not handled
          continue;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:point")) {
          if (result instanceof BottomSquareCompartment) {
            result.setY(XmlParser.getNodeAttr("y", node));
          } else if (result instanceof LeftSquareCompartment) {
            result.setWidth(XmlParser.getNodeAttr("x", node));
            result.setX(0.0);
          } else if (result instanceof RightSquareCompartment) {
            result.setX(XmlParser.getNodeAttr("x", node));
            result.setWidth(result.getWidth() - result.getX());
          } else if (result instanceof TopSquareCompartment) {
            result.setHeight(XmlParser.getNodeAttr("y", node));
            result.setY(0.0);
          } else if ("SQUARE_CLOSEUP_NORTHWEST".equals(type)) {
            result.setX(XmlParser.getNodeAttr("x", node));
            result.setY(XmlParser.getNodeAttr("y", node));
            result.setWidth(model.getWidth() - result.getX());
            result.setHeight(model.getHeight() - result.getY());
          } else if ("SQUARE_CLOSEUP_NORTHEAST".equals(type)) {
            result.setWidth(XmlParser.getNodeAttr("x", node));
            result.setY(XmlParser.getNodeAttr("y", node));
            result.setX(0);
            result.setHeight(model.getHeight() - result.getY());
          } else if ("SQUARE_CLOSEUP_SOUTHEAST".equals(type)) {
            result.setWidth(XmlParser.getNodeAttr("x", node));
            result.setHeight(XmlParser.getNodeAttr("y", node));
            result.setX(0);
            result.setY(0);
          } else if ("SQUARE_CLOSEUP_SOUTHWEST".equals(type)) {
            result.setX(XmlParser.getNodeAttr("x", node));
            result.setHeight(XmlParser.getNodeAttr("y", node));
            result.setWidth(model.getWidth() - result.getX());
            result.setY(0);
          } else {
            throw new InvalidXmlSchemaException(
                "Don't know what to do with celldesigner:point for class: " + result.getClass());
          }
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:namePoint")) {
          Point2D position = getCommonParser().getPosition(node);
          result.setNameX(position.getX());
          result.setNameY(position.getY());
          result.setNameWidth(result.getWidth() - (result.getNameX() - result.getX()));
          result.setNameHeight(result.getHeight() - (result.getNameY() - result.getY()));
          result.setNameHorizontalAlign(HorizontalAlign.LEFT);
          result.setNameVerticalAlign(VerticalAlign.TOP);
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:compartmentAlias: " + node.getNodeName());
        }
      }
    }
    if (result.getNameX() == null || result.getNameY() == null || result.getNameWidth() == null
        || result.getNameHeight() == null) {
      logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, result), "Name point is not defined");
      result.setNameX(result.getX());
      result.setNameY(result.getY());
      result.setNameWidth(result.getWidth());
      result.setNameHeight(result.getHeight());
      result.setNameHorizontalAlign(HorizontalAlign.LEFT);
      result.setNameVerticalAlign(VerticalAlign.TOP);
    }
    return result;
  }

  @Override
  public String toXml(final Compartment compartment) {
    StringBuilder sb = new StringBuilder("");

    sb.append("<celldesigner:compartmentAlias ");
    sb.append("id=\"" + elements.getCompartmentAliasId(compartment) + "\" ");
    sb.append("compartment=\"" + elements.getElementId(compartment) + "\">\n");

    boolean bounds = true;

    if (compartment.getClass().getName().equals(SquareCompartment.class.getName())) {
      sb.append("<celldesigner:class>SQUARE</celldesigner:class>\n");
    } else if (compartment.getClass().getName().equals(OvalCompartment.class.getName())) {
      sb.append("<celldesigner:class>OVAL</celldesigner:class>\n");
    } else if (compartment.getClass().getName().equals(BottomSquareCompartment.class.getName())) {
      sb.append("<celldesigner:class>SQUARE_CLOSEUP_NORTH</celldesigner:class>\n");
      bounds = false;
      sb.append("<celldesigner:point x=\"10\" y=\"" + compartment.getY() + "\"/>");
    } else if (compartment.getClass().getName().equals(TopSquareCompartment.class.getName())) {
      sb.append("<celldesigner:class>SQUARE_CLOSEUP_SOUTH</celldesigner:class>\n");
      bounds = false;
      sb.append("<celldesigner:point x=\"10\" y=\"" + compartment.getY() + "\"/>");
    } else if (compartment.getClass().getName().equals(LeftSquareCompartment.class.getName())) {
      sb.append("<celldesigner:class>SQUARE_CLOSEUP_EAST</celldesigner:class>\n");
      bounds = false;
      sb.append("<celldesigner:point x=\"" + compartment.getWidth() + "\" y=\"10\"/>");
    } else if (compartment.getClass().getName().equals(RightSquareCompartment.class.getName())) {
      sb.append("<celldesigner:class>SQUARE_CLOSEUP_WEST</celldesigner:class>\n");
      bounds = false;
      sb.append("<celldesigner:point x=\"" + compartment.getX() + "\" y=\"10\"/>");
    } else {
      throw new NotImplementedException("Unknown compartment class: " + compartment.getClass());
    }
    if (bounds) {
      sb.append("<celldesigner:bounds ");
      sb.append("x=\"" + compartment.getX() + "\" ");
      sb.append("y=\"" + compartment.getY() + "\" ");
      sb.append("w=\"" + compartment.getWidth() + "\" ");
      sb.append("h=\"" + compartment.getHeight() + "\"/>\n");
    }

    sb.append("<celldesigner:namePoint ");
    sb.append("x=\"" + compartment.getNameX() + "\" ");
    sb.append("y=\"" + compartment.getNameY() + "\"/>\n");

    sb.append(getDoubleLineNode(compartment));

    sb.append("<celldesigner:paint ");
    sb.append("color=\"" + XmlParser.colorToString(compartment.getFillColor()) + "\" scheme=\"Color\"/>\n");

    sb.append("</celldesigner:compartmentAlias>\n");

    return sb.toString();
  }

  private String getDoubleLineNode(final Compartment compartment) {
    StringBuilder result = new StringBuilder();
    result.append("<celldesigner:doubleLine ");
    double innerWidth = getInnerWidth(compartment);
    if (innerWidth != compartment.getInnerWidth()) {
      logger.warn(new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, compartment),
          "CellDesigner requires innerWidth to be at least: " + innerWidth);
    }
    double outerWidth = getOuterWidth(compartment);
    if (outerWidth != compartment.getOuterWidth()) {
      logger.warn(new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, compartment),
          "CellDesigner requires outerWidth to be at least: " + outerWidth);
    }
    double thickness = getThickness(compartment);
    if (thickness != compartment.getThickness()) {
      logger.warn(new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, compartment),
          "CellDesigner requires thickness to be at least: " + thickness);
    }
    result.append("thickness=\"" + thickness + "\" ");
    result.append("outerWidth=\"" + outerWidth + "\" ");
    result.append("innerWidth=\"" + innerWidth + "\"/>\n");

    return result.toString();
  }

  private double getInnerWidth(final Compartment compartment) {
    return Math.max(MIN_INNER_WIDTH, compartment.getInnerWidth());
  }

  private double getOuterWidth(final Compartment compartment) {
    return Math.max(getInnerWidth(compartment) + 1, compartment.getOuterWidth());
  }

  private double getThickness(final Compartment compartment) {
    return Math.max((getInnerWidth(compartment) + getOuterWidth(compartment)) / 2 + 1, compartment.getThickness());
  }

}
