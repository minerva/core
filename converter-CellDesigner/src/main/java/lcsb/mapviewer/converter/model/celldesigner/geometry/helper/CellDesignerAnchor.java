package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

/**
 * List of all possible anchor values in CellDesigner. It starts in the east
 * anchor (center; max to the right point). Next anchors are listed in clockwise
 * order.
 * 
 * @author Piotr Gawron
 * 
 */
public enum CellDesignerAnchor {

  /**
   * East direction.
   */
  E("E", 0.0),

  /**
   * East south-east direction.
   */
  ESE("ESE", 22.5),

  /**
   * South-east direction.
   */
  SE("SE", 45.0),

  /**
   * South south-east direction.
   */
  SSE("SSE", 67.5),

  /**
   * South direction.
   */
  S("S", 90.0),

  /**
   * South south-west direction.
   */
  SSW("SSW", 112.5),

  /**
   * South-west direction.
   */
  SW("SW", 135.0),

  /**
   * West south-west direction.
   */
  WSW("WSW", 157.5),

  /**
   * West direction.
   */
  W("W", 180.0),

  /**
   * West northwest direction.
   */
  WNW("WNW", 202.5),

  /**
   * Northwest direction.
   */
  NW("NW", 225.0),

  /**
   * North northwest direction.
   */
  NNW("NNW", 247.5),

  /**
   * North direction.
   */
  N("N", 270.0),

  /**
   * North northeast direction.
   */
  NNE("NNE", 292.5),

  /**
   * Northeast direction.
   */
  NE("NE", 315.0),

  /**
   * East northeast direction.
   */
  ENE("ENE", 337.5),

  /**
   * Unknown direction.
   */
  INACTIVE("INACTIVE", null); // UNDOCUMENTED

  /**
   * We have this number of different valid anchors in CellDesigner.
   */
  static final int DIFFERENT_ANCHORS = 16;

  /**
   * Name of the anchor.
   */
  private String name;

  /**
   * Angle (in radians) on the border where the anchor is localized.
   */
  private Double angle;

  /**
   * Default constructor with name and angle value.
   * 
   * @param name
   *          name of the anchor
   * @param angleDegrees
   *          angle (in degrees) on the border where the anchor is localized
   */
  CellDesignerAnchor(final String name, final Double angleDegrees) {
    this.name = name;
    if (angleDegrees == null) {
      this.angle = null;
    } else {
      this.angle = Math.toRadians(angleDegrees);
    }
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @return the angle
   * @see #angle
   */
  public Double getAngle() {
    return angle;
  }
}
