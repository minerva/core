package lcsb.mapviewer.converter.model.celldesigner.types;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.converter.model.celldesigner.geometry.ReactionCellDesignerConverter;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Util class that helps in transformation of CellDesigner modifiers into
 * {@link Modifier} object in our model.
 * 
 * @author Piotr Gawron
 * 
 */
public class ModifierTypeUtils {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Returns {@link ModifierType} that reference to the clazz.
   * 
   * @param clazz
   *          class type for which result is returned
   * @return {@link ModifierType} that reference to the clazz
   */
  public ModifierType getModifierTypeForClazz(final Class<? extends Modifier> clazz) {
    for (final ModifierType modType : ModifierType.values()) {
      if (clazz.equals(modType.getClazz())) {
        return modType;
      }
    }
    return null;
  }

  /**
   * Returns {@link ModifierType} that reference to the CellDesigner string
   * 'type'.
   * 
   * @param type
   *          CellDesigner string defining modifier type
   * @return {@link ModifierType} that reference to the CellDesigner 'type'
   */
  public ModifierType getModifierTypeForStringType(final String type) {
    for (final ModifierType modType : ModifierType.values()) {
      if (type.equalsIgnoreCase(modType.getStringName())) {
        return modType;
      }
    }
    return null;
  }

  /**
   * Creates {@link Modifier} for the CellDesigner type.
   * 
   * @param type
   *          CellDesigner type of the modifier
   * @param alias
   *          alias to which modifier point to
   * @return {@link Modifier} for the CellDesigner type
   */
  public Modifier createModifierForStringType(final String type, final Species alias) {
    ModifierType modType = getModifierTypeForStringType(type);
    if (modType == null) {
      throw new InvalidArgumentException("Unknown modifier type: " + type);
    }
    try {
      Modifier mod = modType.getClazz().getConstructor(Element.class).newInstance(alias);
      return mod;
    } catch (final Exception e) {
      throw new InvalidStateException("Problem with instantiation of Modifier class: " + type);
    }
  }

  /**
   * Returns CellDesigner string used for modifier creation.
   * 
   * @param modifier
   *          modifier for which CellDesigner string is looked for
   * @return CellDesigner string used for modifier creation
   */
  public String getStringTypeByModifier(final Modifier modifier) {
    ModifierType modifierType = getModifierTypeForClazz(modifier.getClass());
    if (modifierType != null) {
      return modifierType.getStringName();
    }
    return null;
  }

  /**
   * Returns string that identfies TargetLine index for the modifier. TargetLine
   * index is a CellDesigner String that identifies to which point on the
   * rectangle in the center line modifier line is connected.
   * 
   * @param modifier
   *          modifier for target line
   * @return string that identfies TargetLine index for the modifier
   */
  public String getTargetLineIndexByModifier(final Modifier modifier) {
    ModifierType modType = getModifierTypeForClazz(modifier.getClass());
    if (modType == null) {
      throw new InvalidArgumentException("Unknown modifier class: " + modifier.getClass());
    }

    Reaction reaction = modifier.getReaction();
    PolylineData line = modifier.getLine();

    String result = getTragteLineIndex(modType, reaction, line);
    return result;
  }

  /**
   * Returns string that identfies TargetLine index for the modifier operator.
   * TargetLine index is a CellDesigner String that identifies to which point on
   * the rectangle in the center line modifier line is connected.
   * 
   * @param modifier
   *          modifier operator for target line
   * @return string that identfies TargetLine index for the modifier
   */
  public String getTargetLineIndexByModifier(final NodeOperator modifier) {
    if (!(modifier.getInputs().get(0) instanceof Modifier)) {
      throw new InvalidArgumentException("Invalid NodeOperator");
    } else {
      ModifierType modType = getModifierTypeForClazz(((Modifier) modifier.getInputs().get(0)).getClass());

      if (modType == null) {
        throw new InvalidArgumentException("Unknown modifier class: " + modifier.getClass());
      }

      Reaction reaction = modifier.getReaction();
      PolylineData line = modifier.getLine();

      String result = getTragteLineIndex(modType, reaction, line);
      return result;
    }
  }

  /**
   * Returns string that identfies TargetLine index for the modifier line
   * connected to reaction (target line index is explained here:
   * {@link #getTargetLineIndexByModifier(Modifier)}).
   * 
   * 
   * @param modType
   *          type of modifier for the line
   * @param reaction
   *          reaction on which target line index is looked for
   * @param originalLine
   *          line that connects modifier to the reaction
   * @return string that identifies TargetLine index for modifier line
   */
  private String getTragteLineIndex(final ModifierType modType, final Reaction reaction, final PolylineData originalLine) {
    PolylineData line = new PolylineData(originalLine);
    line.trimEnd(-modType.getTrimLength());

    String[] possibleValues = new String[] { "0,2", "0,3", "0,4", "0,5", "0,6", "0,7" };
    Point2D closestPoint = new Point2D.Double(Double.MAX_VALUE, Double.MAX_VALUE);
    Point2D checkedPoint = line.getEndPoint();
    String result = "0,0";
    for (final String string : possibleValues) {
      Point2D point = getAnchorPointOnReactionRect(reaction, string);
      if (point.distance(checkedPoint) < closestPoint.distance(checkedPoint)) {
        closestPoint = point;
        result = string;
      }
    }
    return result;
  }

  /**
   * This method get anchor point on the reaction line (the center point of this
   * line is given as a parameter). The anchor point depends on the line and
   * lineConnectionType associated with the modification.
   * 
   * @param reaction
   *          reaction on which we are looking for a point
   * @param lineConnectionType
   *          point of connection to the reaction center rectangle
   * @return coordinates of point on the center rectangle
   */
  public Point2D getAnchorPointOnReactionRect(final Reaction reaction, final String lineConnectionType) {
    int countLines = reaction.getLine().getLines().size();
    Line2D centerLine = reaction.getLine().getLines().get(countLines / 2);
    double dx = centerLine.getX2() - centerLine.getX1();
    double dy = centerLine.getY2() - centerLine.getY1();
    double angle = Math.atan2(dy, dx);
    Point2D mid = new Point2D.Double((centerLine.getX1() + centerLine.getX2()) / 2,
        (centerLine.getY1() + centerLine.getY2()) / 2);

    String id = lineConnectionType;
    String[] spr = id.split(",");
    double x = mid.getX();
    double y = mid.getY();
    if (spr.length > 1) {
      id = spr[1];
      if (id.equals("2")) {
        y -= ReactionCellDesignerConverter.RECT_SIZE / 2;
      } else if (id.equals("3")) {
        y += ReactionCellDesignerConverter.RECT_SIZE / 2;
      } else if (id.equals("4")) {
        y -= ReactionCellDesignerConverter.RECT_SIZE / 2;
        x -= ReactionCellDesignerConverter.RECT_SIZE / 2;
      } else if (id.equals("5")) {
        y -= ReactionCellDesignerConverter.RECT_SIZE / 2;
        x += ReactionCellDesignerConverter.RECT_SIZE / 2;
      } else if (id.equals("6")) {
        y += ReactionCellDesignerConverter.RECT_SIZE / 2;
        x -= ReactionCellDesignerConverter.RECT_SIZE / 2;
      } else if (id.equals("7")) {
        y += ReactionCellDesignerConverter.RECT_SIZE / 2;
        x += ReactionCellDesignerConverter.RECT_SIZE / 2;
      } else if (id.equals("0")) {
        // this is only workaround...
        y += ReactionCellDesignerConverter.RECT_SIZE / 2;
      } else if (id.equals("1")) {
        // this is only workaround...
        y += ReactionCellDesignerConverter.RECT_SIZE / 2;
      } else {
        throw new InvalidArgumentException("Unknown targetLineIndex: " + id);
      }

    } else {
      logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, reaction), "Invalid targetLineIndex: " + id);
      y += ReactionCellDesignerConverter.RECT_SIZE / 2;
    }
    Point2D p = new Point2D.Double(x, y);
    PointTransformation pointTransformation = new PointTransformation();
    p = pointTransformation.rotatePoint(p, angle, mid);
    return p;
  }

  /**
   * Update line parameters to make it look like a proper modifier.
   * 
   * @param modifier
   *          modifier for which line will be modified
   */
  public void updateLineEndPoint(final Modifier modifier) {
    ModifierType modType = getModifierTypeForClazz(modifier.getClass());
    if (modType == null) {
      throw new InvalidArgumentException("Unknown modifier class: " + modifier.getClass());
    }
    PolylineData line = modifier.getLine();
    updateLineByType(modType, line);
  }

  /**
   * Updates line parameters of modifier operator to make it look like a proper
   * modifier.
   * 
   * @param operator
   *          modifier operator for which line will be modified
   */
  @SuppressWarnings("unchecked")
  public void updateLineEndPoint(final NodeOperator operator) {
    Class<? extends AbstractNode> clazz = operator.getInputs().get(1).getClass();
    if (Modifier.class.isAssignableFrom(clazz)) {
      ModifierType modType = getModifierTypeForClazz((Class<? extends Modifier>) clazz);
      if (modType == null) {
        throw new InvalidArgumentException("Unknown modifier class: " + clazz);
      }
      PolylineData line = operator.getLine();
      updateLineByType(modType, line);
    } else {
      throw new InvalidArgumentException("Operator contains invalid input: " + clazz);
    }
  }

  /**
   * Updates line parameters to make it look like a proper modifier.
   * 
   * @param modType
   *          type of modifier
   * @param line
   *          line to update
   */
  private void updateLineByType(final ModifierType modType, final PolylineData line) {
    line.setEndAtd(modType.getAtd().copy());
    line.setType(modType.getLineType());
    line.trimEnd(modType.getTrimLength());
  }

  /**
   * Returns type of the modifier in modifier operator.
   * 
   * @param modifierOperator
   *          opertor for which type will be returned
   * @return type of the modifier in modifier operator
   */
  public ModifierType getModifierTypeForOperator(final NodeOperator modifierOperator) {
    Modifier mod = (Modifier) modifierOperator.getInputs().get(0);
    return getModifierTypeForClazz(mod.getClass());
  }

}
