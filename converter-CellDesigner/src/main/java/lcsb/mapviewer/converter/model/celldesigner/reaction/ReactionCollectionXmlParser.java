package lcsb.mapviewer.converter.model.celldesigner.reaction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerParserException;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * Parser used for extracting collections of reaction from CellDesigner xml.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactionCollectionXmlParser {

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger();

  /**
   * Model for which parsing (or transformation to xml) is being done.
   */
  private Model model = null;

  /**
   * Object used for parsing single reaction.
   */
  private ReactionXmlParser xmlStructureFactory;

  /**
   * Default constructor. Model is required because some nodes require access to
   * other parts of the model.
   * 
   * @param sbgn
   *          should sbgn standard be used
   * @param elements
   *          collection of {@link CellDesignerElement cell designer elements}
   *          parsed from xml
   * @param model
   *          model that is parsed/transformed into xml
   */
  public ReactionCollectionXmlParser(final Model model, final CellDesignerElementCollection elements, final boolean sbgn,
      final boolean oldCellDesignerVersion) {
    this.model = model;
    xmlStructureFactory = new ReactionXmlParser(elements, sbgn, oldCellDesignerVersion);
  }

  /**
   * Parse CellDesigner xml node with set of reactions into list of
   * {@link Reaction}.
   * 
   * @param reactionsNode
   *          xml node
   * @return list of reaction taken from xml node
   * @throws CellDesignerParserException
   *           thrown when there is a problem with xml node
   * @throws InvalidXmlSchemaException
   *           thrown when reactionsNode is invalid xml
   */
  public List<Reaction> parseXmlReactionCollection(final Node reactionsNode)
      throws CellDesignerParserException, InvalidXmlSchemaException {
    List<Reaction> result = new ArrayList<Reaction>();
    NodeList nodes = reactionsNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("reaction")) {
          Reaction reaction = xmlStructureFactory.getReaction(node, model);
          result.add(reaction);
        } else {
          throw new InvalidXmlSchemaException("Unknown element of model/listOfReactions: " + node.getNodeName());
        }
      }
    }

    return result;
  }

  /**
   * Transforms set of reactions into CellDesigner xml string.
   * 
   * @param collection
   *          set of reactions
   * @return CellDesigner xml string representing set of reactions
   * @throws InconsistentModelException
   *           thrown when there is a problem with exporting {@link Reaction}
   */
  public String reactionCollectionToXmlString(final Collection<Reaction> collection) throws InconsistentModelException {
    StringBuilder result = new StringBuilder("");
    if (collection.size() > 0) {
      result.append("<listOfReactions>\n");
      for (final Reaction reaction : collection) {
        boolean reactionToCompartment = false;
        for (final ReactionNode node : reaction.getReactionNodes()) {
          if (node.getElement() instanceof Compartment) {
            reactionToCompartment = true;
          }
        }
        if (reactionToCompartment) {
          logger.warn(new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, reaction),
              "Reaction to compartment is not supported");
        } else {
          try {
            result.append(xmlStructureFactory.toXml(reaction));
          } catch (final InvalidArgumentException e) {
            throw new InconsistentModelException(
                new ElementUtils().getElementTag(reaction) + "Problem with exporting reaction", e);
          } catch (final SelfReactionException e) {
            logger.warn(new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, reaction),
                "Reaction omitted: " + e.getMessage());
          }
        }
      }
      result.append("</listOfReactions>\n");
    }
    return result.toString();
  }
}
