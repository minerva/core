package lcsb.mapviewer.converter.model.celldesigner.types;

import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.NandOperator;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.OrOperator;
import lcsb.mapviewer.model.map.reaction.UnknownOperator;

/**
 * This enum defines how different types of CellDesigner operators should be
 * translated into the reaction {@link NodeOperator} class.
 * 
 * @author Piotr Gawron
 * 
 */
public enum OperatorType {

  /**
   * Boolean and operator.
   */
  AND_OPERATOR_STRING("BOOLEAN_LOGIC_GATE_AND", AndOperator.class),

  /**
   * Boolean not and operator.
   */
  NAND_OPERATOR_STRING("BOOLEAN_LOGIC_GATE_NOT", NandOperator.class),

  /**
   * Boolean or operator.
   */
  OR_OPERATOR_STRING("BOOLEAN_LOGIC_GATE_OR", OrOperator.class),

  /**
   * Boolean unknown operator.
   */
  UNKNOWN_OPERATOR_STRING("BOOLEAN_LOGIC_GATE_UNKNOWN", UnknownOperator.class);

  /**
   * CellDEsigner string identifing this operator.
   */
  private String stringName;
  /**
   * Class in our model representing this operator.
   */
  private Class<? extends NodeOperator> clazz;

  /**
   * Default constructor. Initialize enum with data.
   * 
   * @param string
   *          {@link #stringName}
   * @param clazz
   *          {@link #clazz}
   */
  OperatorType(final String string, final Class<? extends NodeOperator> clazz) {
    stringName = string;
    this.clazz = clazz;
  }

  /**
   * Returns {@link OperatorType} that should be applied to a class given in the
   * parameter.
   *
   * @param clazz
   *          class for which we are looking for {@link OperatorType}
   * @return {@link OperatorType} that should be applied to a class given in the
   *         parameter
   */
  public static OperatorType getTypeByClass(final Class<? extends NodeOperator> clazz) {
    for (final OperatorType type : OperatorType.values()) {
      if (type.getClazz().equals(clazz)) {
        return type;
      }
    }
    return null;
  }

  /**
   * @return the stringName
   * @see #stringName
   */
  public String getStringName() {
    return stringName;
  }

  /**
   * @return the clazz
   * @see #clazz
   */
  public Class<? extends NodeOperator> getClazz() {
    return clazz;
  }

}
