/**
 * Defines enums and utils that helps transforming CellDesigner types into types
 * used in our model.
 */
package lcsb.mapviewer.converter.model.celldesigner.types;
