package lcsb.mapviewer.converter.model.celldesigner.geometry;

import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.map.species.Unknown;

/**
 * Class that provides CellDesigner specific graphical information for
 * {@link lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerUnknown.model.map.species.Unknown
 * Unknown}. It's used for conversion from xml to normal x,y coordinates.
 * 
 * @author Piotr Gawron
 * 
 */
public class UnknownCellDesignerAliasConverter extends AbstractCellDesignerAliasConverter<Unknown> {

  private static Logger logger = LogManager.getLogger();

  /**
   * Default constructor.
   * 
   * @param sbgn
   *          Should the converter use sbgn standard
   */
  protected UnknownCellDesignerAliasConverter(final boolean sbgn) {
    super(sbgn);
  }

  @Override
  public Point2D getPointCoordinates(final Unknown alias, final CellDesignerAnchor anchor) {
    if (invalidAnchorPosition(alias, anchor)) {
      return alias.getCenter();
    }
    return getEllipseTransformation().getPointOnEllipseByAnchor(alias.getX(), alias.getY(), alias.getWidth(),
        alias.getHeight(), anchor);
  }

  @Override
  public Point2D getAnchorPointCoordinates(final Unknown alias, final double angle) {
    if (alias.getWidth() == 0 && alias.getHeight() == 0) {
      return alias.getCenter();
    }
    Point2D result;
    result = getEllipseTransformation().getPointOnEllipseByRadian(alias.getX(), alias.getY(), alias.getWidth(),
        alias.getHeight(), angle);
    return result;

  }

  @Override
  public PathIterator getBoundPathIterator(final Unknown alias) {
    throw new NotImplementedException("This class doesn't provide boundPath");
  }

  @Override
  public Point2D getPointCoordinatesOnBorder(final Unknown unknown, final double angle) {
    if (unknown.getWidth() == 0 && unknown.getHeight() == 0) {
      logger.warn("Looking for coordinates for unknown of 0 size");
      return unknown.getCenter();
    }
    Point2D result;
    result = getEllipseTransformation().getPointOnEllipseByRadian(unknown.getX(), unknown.getY(), unknown.getWidth(),
        unknown.getHeight(), angle);
    return result;
  }
}
