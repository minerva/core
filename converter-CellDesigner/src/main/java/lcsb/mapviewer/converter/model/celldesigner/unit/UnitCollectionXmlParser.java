package lcsb.mapviewer.converter.model.celldesigner.unit;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;

public class UnitCollectionXmlParser {

  private Logger logger = LogManager.getLogger();

  private UnitXmlParser unitParser = new UnitXmlParser();

  public Set<SbmlUnit> parseXmlUnitCollection(final Node functionsNode) throws InvalidXmlSchemaException {
    Set<SbmlUnit> result = new HashSet<>();
    for (final Node node : XmlParser.getNodes("unitDefinition", functionsNode.getChildNodes())) {
      result.add(unitParser.parseFunction(node));
    }
    return result;
  }

  public String toXml(final Set<SbmlUnit> units) {
    if (units.size() > 0) {
      StringBuilder builder = new StringBuilder();
      builder.append("<listOfUnitDefinitions>\n");
      for (final SbmlUnit unit : units) {
        if (unitParser.isSupported(unit)) {
          builder.append(unitParser.toXml(unit));
        } else {
          logger.warn(unit.getUnitId() + " is not supported by CellDesigner. Ignored unit types: " + UnitXmlParser.NOT_SUPPORTED_TYPES);
        }
      }
      builder.append("</listOfUnitDefinitions>\n");
      return builder.toString();
    } else {
      return "";
    }
  }

}
