package lcsb.mapviewer.converter.model.celldesigner.structure.fields;

import java.awt.Color;

import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.model.graphics.PolylineData;

/**
 * Structure used in CellDEsigner model to store information about line
 * properties.
 * 
 * @author Piotr Gawron
 * 
 */
public class LineProperties {

  /**
   * Width of the line.
   */
  private double width;

  /**
   * Color of the line.
   */
  private Color color;

  /**
   * Line type (no idea what is the format).
   */
  private String type;

  /**
   * Default constructor.
   */
  public LineProperties() {
    width = 1;
    color = Color.BLACK;
    type = "";
  }

  /**
   * Create properties from {@link PolylineData}.
   */
  public LineProperties(final PolylineData line) {
    width = line.getWidth();
    color = line.getColor();
    type = "";
  }

  /**
   * @return the width
   * @see #width
   */
  public double getWidth() {
    return width;
  }

  /**
   * @param width
   *          the width to set
   * @see #width
   */
  public void setWidth(final double width) {
    this.width = width;
  }

  /**
   * @return the color
   * @see #color
   */
  public Color getColor() {
    return color;
  }

  /**
   * @param color
   *          the color to set
   * @see #color
   */
  public void setColor(final Color color) {
    this.color = color;
  }

  /**
   * @return the type
   * @see #type
   */
  public String getType() {
    return type;
  }

  /**
   * @param type
   *          the type to set
   * @see #type
   */
  public void setType(final String type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return "[" + this.getClass().getSimpleName() + " w=" + width + "; c=" + new ColorParser().colorToHtml(color) + "]";
  }

}
