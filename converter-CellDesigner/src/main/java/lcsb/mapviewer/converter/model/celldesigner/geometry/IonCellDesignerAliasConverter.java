package lcsb.mapviewer.converter.model.celldesigner.geometry;

import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Ion;

/**
 * Class that provides CellDesigner specific graphical information for Ion. It's
 * used for conversion from xml to normal x,y coordinates.
 * 
 * @author Piotr Gawron
 * 
 */
public class IonCellDesignerAliasConverter extends AbstractCellDesignerAliasConverter<Ion> {

  private static Logger logger = LogManager.getLogger();

  /**
   * Default constructor.
   * 
   * @param sbgn
   *          Should the converter use SBGN standard
   */
  protected IonCellDesignerAliasConverter(final boolean sbgn) {
    super(sbgn);
  }

  @Override
  public Point2D getPointCoordinates(final Ion alias, final CellDesignerAnchor anchor) {
    double diameter = getDiameter(alias);
    double x = getXCoord(alias, diameter);
    double y = getYCoord(alias);
    if (invalidAnchorPosition(alias, anchor)) {
      return alias.getCenter();
    }

    return getEllipseTransformation().getPointOnEllipseByAnchor(x, y, diameter, diameter, anchor);
  }

  @Override
  public Point2D getAnchorPointCoordinates(final Ion alias, final double angle) {
    if (alias.getWidth() == 0 && alias.getHeight() == 0) {
      return alias.getCenter();
    }
    double diameter = getDiameter(alias);
    double x = getXCoord(alias, diameter);
    double y = getYCoord(alias);
    Point2D result = getEllipseTransformation().getPointOnEllipseByRadian(x, y, diameter, diameter, angle);
    return result;

  }

  @Override
  protected PathIterator getBoundPathIterator(final Ion alias) {
    throw new NotImplementedException("This class doesn't have bound");
  }

  @Override
  public Point2D getPointCoordinatesOnBorder(final Ion ion, final double angle) {
    if (ion.getWidth() == 0 && ion.getHeight() == 0) {
      logger.warn("Looking for coordinates for the alias with 0 size");
      return ion.getCenter();
    }
    double diameter = getDiameter(ion);
    double x = getXCoord(ion, diameter);
    double y = getYCoord(ion);
    Point2D result = getEllipseTransformation().getPointOnEllipseByRadian(x, y, diameter, diameter, angle);
    return result;

  }

  /**
   * Returns transformed y coordinate for the ion alias.
   *
   * @param alias
   *          object alias to to which we are looking for y coordinate
   * @return y coordinate of the alias
   */
  private double getYCoord(final Element alias) {
    double y = alias.getY();
    return y;
  }

  /**
   * Returns transformed x coordinate for the ion alias.
   *
   * @param alias
   *          object alias to to which we are looking for x coordinate
   * @param diameter
   *          diameter of the circle representing alias
   * @return x coordinate of the alias
   */
  private double getXCoord(final Element alias, final double diameter) {
    double x = alias.getX() + (alias.getWidth() - diameter) / 2;
    return x;
  }

  /**
   * Returns diameter of the ion alias.
   *
   * @param alias
   *          object alias to to which we are looking for diameter.
   * @return diameter of the alias
   */
  private double getDiameter(final Element alias) {
    double diameter = Math.min(alias.getWidth(), alias.getHeight());
    if (diameter < 0) {
      diameter = 0;
    }
    return diameter;
  }
}
