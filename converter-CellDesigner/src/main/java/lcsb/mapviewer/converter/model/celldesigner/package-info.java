/**
 * This package contains converter from CellDesigner xml fromat into our Model.
 * Take a look at the class
 * {@link lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser
 * CellDesignerXmlParser} to see how it works.
 */
package lcsb.mapviewer.converter.model.celldesigner;
