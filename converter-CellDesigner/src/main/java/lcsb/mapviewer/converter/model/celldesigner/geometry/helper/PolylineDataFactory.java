package lcsb.mapviewer.converter.model.celldesigner.geometry.helper;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.geometry.LineTransformation;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.EditPoints;
import lcsb.mapviewer.model.graphics.PolylineData;

/**
 * Factory util that creates {@link PolylineData} objects from CellDesigner
 * structure.
 * 
 * @author Piotr Gawron
 * 
 */
public final class PolylineDataFactory {

  /**
   * Default constructor that prevent instantiation.
   */
  private PolylineDataFactory() {
  }

  /**
   * Creates {@link PolylineData} object from two CellDesigner base points and
   * list of points (in CellDesigner format).
   * 
   * @param baseA
   *          first base point
   * @param baseB
   *          second base point
   * @param points
   *          intermediate points in CellDesigner format
   * @return {@link PolylineData} object representing input data
   */
  public static PolylineData createPolylineDataFromEditPoints(final Point2D baseA, final Point2D baseB, final List<Point2D> points) {
    List<Point2D> pointsList = new CellDesignerLineTransformation().getLinePointsFromPoints(baseA, baseB, points);
    return new PolylineData(pointsList);
  }

  /**
   * Creates {@link PolylineData} object from two CellDesigner base points and
   * list of points (in CellDesigner format).
   * 
   * @param baseA
   *          first base point
   * @param baseB
   *          second base point
   * @param points
   *          structure with intermediate points in CellDesigner format
   * @return {@link PolylineData} object representing input data
   */
  public static PolylineData createPolylineDataFromEditPoints(final Point2D baseA, final Point2D baseB, final EditPoints points) {
    if (points != null) {
      return createPolylineDataFromEditPoints(baseA, baseB, points.getPoints());
    } else {
      return createPolylineDataFromEditPoints(baseA, baseB, new ArrayList<Point2D>());
    }
  }

  /**
   * Creates {@link PolylineData} object from the parameter that doesn't contain
   * collinear points.
   * 
   * @param ld
   *          original line
   * @return line without collinear points
   */
  public static PolylineData removeCollinearPoints(final PolylineData ld) {
    LineTransformation lt = new LineTransformation();
    PolylineData result = new PolylineData(ld);
    int pointNumber = 1;
    while (pointNumber < result.getLines().size()) {
      Point2D previousPoint = result.getLines().get(pointNumber - 1).getP1();

      Point2D currentPoint = result.getLines().get(pointNumber).getP1();
      Point2D nextPoint = result.getLines().get(pointNumber).getP2();

      Point2D closestPointOnLine = lt.closestPointOnSegmentLineToPoint(previousPoint, nextPoint, currentPoint);
      if (closestPointOnLine.distance(currentPoint) <= Configuration.EPSILON) {
        result.removeLine(pointNumber);
        result.setLine(pointNumber - 1, previousPoint, nextPoint);
      } else {
        pointNumber++;
      }
    }
    return result;
  }

  public static Rectangle2D getBounds(final PolylineData line) {
    double x1 = line.getStartPoint().getX();
    double x2 = line.getStartPoint().getX();
    double y1 = line.getStartPoint().getY();
    double y2 = line.getStartPoint().getY();
    for (final Line2D l : line.getLines()) {
      x1 = Math.min(l.getX2(), x1);
      x2 = Math.max(l.getX2(), x2);
      y1 = Math.min(l.getY2(), y1);
      y2 = Math.max(l.getY2(), y2);
    }
    return new Rectangle2D.Double(x1, y1, x2 - x1, y2 - y1);
  }
}
