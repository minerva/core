package lcsb.mapviewer.converter.model.celldesigner.reaction;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.CommonXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.geometry.CellDesignerAliasConverter;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerAnchor;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerLineTransformation;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.CellDesignerPointTransformation;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.PolylineDataFactory;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.types.ModifierType;
import lcsb.mapviewer.converter.model.celldesigner.types.ModifierTypeUtils;
import lcsb.mapviewer.converter.model.celldesigner.types.OperatorType;
import lcsb.mapviewer.converter.model.celldesigner.types.OperatorTypeUtils;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.reaction.type.ReducedNotation;
import lcsb.mapviewer.model.map.reaction.type.SimpleReactionInterface;
import lcsb.mapviewer.model.map.reaction.type.TwoProductReactionInterface;
import lcsb.mapviewer.model.map.reaction.type.TwoReactantReactionInterface;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * This is a part of {@link ReactionXmlParser} class functionality that allows
 * to export reaction into CellDesigner xml node.
 * 
 * @author Piotr Gawron
 * 
 */
public class ReactionToXml {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Helper object used for manipulation on the point coordinates in
   * CellDesigner format.
   */
  private CellDesignerPointTransformation pointTransformation = new CellDesignerPointTransformation();

  /**
   * Helper object used for manipulation on the line structures in CellDesigner
   * format.
   */
  private CellDesignerLineTransformation lineTransformation = new CellDesignerLineTransformation();

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed
   * from xml.
   */
  private CellDesignerElementCollection elements;

  /**
   * Helps in providing human readable identifiers of elements for logging.
   */
  private ElementUtils eu = new ElementUtils();

  private int metaIdCounter = 0;

  /**
   * Defines if SBGN standard should be used.
   */
  private boolean sbgn;

  /**
   * Default constructor. Model is required because some nodes require access to
   * other parts of the model.
   * 
   * @param sbgn
   *          Should the converter use SBGN standard
   * @param elements
   *          collection of {@link CellDesignerElement cell designer elements}
   *          parsed from xml
   */
  public ReactionToXml(final CellDesignerElementCollection elements, final boolean sbgn) {
    this.elements = elements;
    this.sbgn = sbgn;
  }

  /**
   * Transform reaction into CellDesigner xml representation.
   * 
   * @param reaction
   *          reaction to transform
   * @return xml representation of reaction
   */
  public String toXml(final Reaction reaction) throws InconsistentModelException {
    if (reaction.getReactants().get(0).getElement().equals(reaction.getProducts().get(0).getElement())) {
      throw new SelfReactionException("Reaction " + reaction.getElementId() + " is a self reference for element "
          + reaction.getProducts().get(0).getElement().getElementId());
    }
    StringBuilder sb = new StringBuilder();
    sb.append("<reaction ");
    sb.append("metaid=\"" + reaction.getIdReaction() + "\" ");
    sb.append("id=\"" + reaction.getIdReaction() + "\" ");
    if (reaction.getName() != null && !reaction.getName().isEmpty()) {
      sb.append("name=\"" + XmlParser.escapeXml(reaction.getName()) + "\" ");
    }
    sb.append("reversible=\"" + reaction.isReversible() + "\" ");
    sb.append(">\n");
    if (reaction.getNotes() != null && !reaction.getNotes().equals("")) {
      sb.append("<notes>");

      sb.append("<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><title/></head><body>");
      sb.append(reaction.getNotes());
      sb.append("</body></html>");

      sb.append("</notes>");

    }

    sb.append(getAnnotationXmlStringForReaction(reaction));
    sb.append(getListOfReactantsXmlStringForReaction(reaction));
    sb.append(getListOfProductsXmlStringForReaction(reaction));
    sb.append(getSbmlListOfModificationsXmlStringForReaction(reaction));
    sb.append(getKineticsLawXml(reaction));

    sb.append("</reaction>\n");
    return sb.toString();
  }

  private String getKineticsLawXml(final Reaction reaction) throws InconsistentModelException {
    if (reaction.getKinetics() != null) {
      KineticsXmlParser xmlParser = new KineticsXmlParser(reaction.getModel());
      return xmlParser.toXml(reaction.getKinetics(), elements);
    } else {
      return "";
    }
  }

  /**
   * Returns xml node with list of modification.
   * 
   * @param reaction
   *          reaction for which list is generated
   * @return xml node with list of modification
   */
  private String getListOfModificationsXmlStringForReaction(final Reaction reaction) throws InconsistentModelException {
    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:listOfModification>\n");

    // to preserve order of modifier we have to make it a little complicated

    Map<Modifier, NodeOperator> operators = new HashMap<Modifier, NodeOperator>();
    Set<NodeOperator> usedOperators = new HashSet<NodeOperator>();

    for (final NodeOperator node : reaction.getOperators()) {
      if (node.isModifierOperator()) {
        for (final AbstractNode modNode : node.getInputs()) {
          Modifier modifier = (Modifier) modNode;
          operators.put(modifier, node);
          if (node.getLine().getStartPoint().distance(modifier.getLine().getEndPoint()) > Configuration.EPSILON) {
            logger.warn("Modifier should end in the same point as operator starts, but: "
                + node.getLine().getStartPoint() + ", " + modifier.getLine().getEndPoint());
          }
        }
      }
    }
    for (final Modifier modifier : reaction.getModifiers()) {
      NodeOperator node = operators.get(modifier);
      if (node != null) {
        if (!usedOperators.contains(node)) {
          usedOperators.add(node);
          sb.append(getModifierXmlString(node));
        }
      }
      sb.append(getModifierXmlString(modifier, node));
    }
    sb.append("</celldesigner:listOfModification>\n");
    return sb.toString();
  }

  /**
   * Returns SBML node with list of modifications.
   * 
   * @param reaction
   *          reaction for which list is generated
   * @return SBML node with list of modifications
   */
  private String getSbmlListOfModificationsXmlStringForReaction(final Reaction reaction) {
    if (reaction.getModifiers().size() == 0) {
      return "";
    }
    StringBuilder sb = new StringBuilder();
    sb.append("<listOfModifiers>\n");
    for (final Modifier modifier : reaction.getModifiers()) {
      sb.append(getModifierReferenceXmlString(modifier));
    }
    sb.append("</listOfModifiers>\n");
    return sb.toString();
  }

  /**
   * Creates modifierSpeciesReference SBML node for given modifier.
   * 
   * @param modifier
   *          modifier to be transformed
   * @return modifierSpeciesReference SBML node for given modifier
   */
  private String getModifierReferenceXmlString(final Modifier modifier) {
    StringBuilder sb = new StringBuilder();
    Species element = (Species) modifier.getElement();

    Set<Element> parents = new HashSet<>();

    // we need top parent species
    while (element.getComplex() != null) {
      element = element.getComplex();
      if (parents.contains(element)) {
        throw new InvalidArgumentException(eu.getElementTag(element) + " Complex information is cyclic");
      }
      parents.add(element);
    }

    sb.append("<modifierSpeciesReference ");
    sb.append("metaid=\"MSR" + (metaIdCounter++) + "\" ");

    sb.append("species=\"" + elements.getElementId(element) + "\">\n");
    sb.append("<annotation>\n");
    sb.append("<celldesigner:extension>\n");
    sb.append("<celldesigner:alias>" + modifier.getElement().getElementId() + "</celldesigner:alias>\n");
    sb.append("</celldesigner:extension>\n");
    sb.append("</annotation>\n");
    sb.append("</modifierSpeciesReference>\n");

    return sb.toString();
  }

  /**
   * Creates xml node for given modifier operator.
   * 
   * @param modifierOperator
   *          modifier operator to be transformed
   * @return xml node for given modifier operator
   */
  private String getModifierXmlString(final NodeOperator modifierOperator) {
    StringBuilder sb = new StringBuilder();
    OperatorTypeUtils otu = new OperatorTypeUtils();
    ModifierTypeUtils mtu = new ModifierTypeUtils();
    ModifierType modifierType = mtu.getModifierTypeForOperator(modifierOperator);
    sb.append("<celldesigner:modification ");
    String type = otu.getStringTypeByOperator(modifierOperator);
    sb.append(" type=\"" + type + "\" ");
    sb.append(" modificationType=\"" + modifierType.getStringName() + "\" ");
    String modifiers = "";
    String aliases = "";
    for (final AbstractNode modifier : modifierOperator.getInputs()) {
      if (modifier instanceof Modifier) {
        Modifier mod = (Modifier) modifier;
        if (modifiers.equals("")) {
          modifiers = elements.getElementId(mod.getElement());
          aliases = mod.getElement().getElementId();
        } else {
          modifiers += "," + elements.getElementId(mod.getElement());
          aliases += "," + mod.getElement().getElementId();
        }

      }
    }
    sb.append(" modifiers=\"" + modifiers + "\" ");
    sb.append(" aliases=\"" + aliases + "\" ");
    sb.append(" targetLineIndex=\"" + mtu.getTargetLineIndexByModifier(modifierOperator) + "\" ");
    PolylineData line = new PolylineData(modifierOperator.getLine());

    line.trimEnd(-modifierType.getTrimLength());

    // TODO
    // check the order (reverse or not, begin or end point)
    line = line.reverse();
    List<Point2D> points = lineTransformation.getPointsFromLine(line);
    sb.append(" editPoints=\"");
    for (int i = 0; i < points.size(); i++) {
      sb.append(points.get(i).getX() + "," + points.get(i).getY() + " ");
    }
    sb.append(line.getEndPoint().getX() + "," + line.getEndPoint().getY());
    sb.append("\"");

    sb.append(">\n");
    PolylineData[] lines = new PolylineData[] { modifierOperator.getLine() };
    sb.append(getConnectSchemeXmlStringForLines(lines));

    sb.append(getLineXmlStringForLines(modifierOperator.getLine()));

    sb.append("</celldesigner:modification>\n");
    return sb.toString();
  }

  /**
   * Creates xml node for given modifier.
   * 
   * @param modifier
   *          modifier to be transformed
   * @param gate
   *          operator to which modifier is connected (if any)
   * @return xml node for given modifier
   */
  private String getModifierXmlString(final Modifier modifier, final NodeOperator gate) throws InconsistentModelException {
    StringBuilder sb = new StringBuilder();
    ModifierTypeUtils modifierTypeUtils = new ModifierTypeUtils();
    sb.append("<celldesigner:modification ");
    String type = modifierTypeUtils.getStringTypeByModifier(modifier);
    sb.append(" type=\"" + type + "\" ");
    sb.append(" modifiers=\"" + elements.getElementId(modifier.getElement()) + "\" ");
    sb.append(" aliases=\"" + modifier.getElement().getElementId() + "\" ");
    sb.append(" targetLineIndex=\"" + modifierTypeUtils.getTargetLineIndexByModifier(modifier) + "\" ");
    PolylineData line = new PolylineData(modifier.getLine());

    line.trimEnd(-modifierTypeUtils.getModifierTypeForClazz(modifier.getClass()).getTrimLength());
    if (gate != null) {
      line.setEndPoint(gate.getLine().getStartPoint());
      CellDesignerAliasConverter converter = new CellDesignerAliasConverter(modifier.getElement(), sbgn);
      CellDesignerAnchor anchor = converter.getAnchorForCoordinates(modifier.getElement(), line.getStartPoint());
      Point2D start = converter.getPointCoordinates(modifier.getElement(), anchor);
      line.setStartPoint(start);
    }

    try {
      List<Point2D> points = lineTransformation.getPointsFromLine(line);
      if (points.size() > 0) {

        boolean first = true;
        sb.append(" editPoints=\"");

        for (final Point2D point : points) {
          if (first) {
            first = false;
          } else {
            sb.append(" ");
          }
          sb.append(point.getX() + "," + point.getY());
        }
        sb.append("\"");
      }
    } catch (final InvalidArgumentException e) {
      throw new InconsistentModelException(new ElementUtils().getElementTag(modifier) + "Problem with exporting line",
          e);
    }

    sb.append(">\n");
    PolylineData[] lines = new PolylineData[] { modifier.getLine() };
    sb.append(getConnectSchemeXmlStringForLines(lines));
    sb.append(getLinkTargetXmlString(modifier));

    sb.append(getLineXmlStringForLines(modifier.getLine()));

    sb.append("</celldesigner:modification>\n");
    return sb.toString();
  }

  private String getLineXmlStringForLines(final PolylineData line) {
    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:line ");
    sb.append("width=\"" + line.getWidth() + "\" ");
    sb.append("color=\"" + XmlParser.colorToString(line.getColor()) + "\" ");
    sb.append("type=\"Straight\" ");
    sb.append("/>\n");
    return sb.toString();
  }

  /**
   * Gets target link string for given modifier.
   * 
   * @param modifier
   *          modifier to be transformed
   * @return anchor xml node representing modifier connection to reaction
   */
  private String getLinkTargetXmlString(final ReactionNode modifier) {
    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:linkTarget species=\"" + elements.getElementId(modifier.getElement()) + "\" alias=\""
        + modifier.getElement().getElementId() + "\">\n");
    sb.append(getAnchorXml(modifier.getElement(), modifier.getLine().getStartPoint()));
    sb.append("</celldesigner:linkTarget>\n");

    return sb.toString();
  }

  /**
   * Creates SBML node for list of products.
   * 
   * @param reaction
   *          reaction from which products are taken
   * @return SBML node representing list of products
   */
  private String getListOfProductsXmlStringForReaction(final Reaction reaction) {
    StringBuilder sb = new StringBuilder();
    sb.append("<listOfProducts>\n");
    for (final Product product : reaction.getProducts()) {
      // metaid is missing, maybe it will work ;)

      Species element = (Species) product.getElement();

      Set<Element> parents = new HashSet<>();

      // we need top parent species
      while (element.getComplex() != null) {
        element = element.getComplex();
        if (parents.contains(element)) {
          throw new InvalidArgumentException(eu.getElementTag(element) + " Complex information is cyclic");
        }
        parents.add(element);
      }

      sb.append("<speciesReference species=\"" + elements.getElementId(element) + "\" ");
      if (product.getStoichiometry() != null) {
        sb.append(" stoichiometry=\"" + product.getStoichiometry() + "\" ");
      }
      sb.append(">\n");

      sb.append("<annotation>\n");
      sb.append("<celldesigner:extension>\n");
      sb.append("<celldesigner:alias>" + product.getElement().getElementId() + "</celldesigner:alias>\n");
      sb.append("</celldesigner:extension>\n");
      sb.append("</annotation>\n");
      sb.append("</speciesReference>\n");
    }
    sb.append("</listOfProducts>\n");
    return sb.toString();
  }

  /**
   * Creates sbml node for list of reactants.
   * 
   * @param reaction
   *          reaction from which products are taken
   * @return sbml node representing list of reactants
   */
  private String getListOfReactantsXmlStringForReaction(final Reaction reaction) {
    StringBuilder sb = new StringBuilder();
    sb.append("<listOfReactants>\n");
    for (final Reactant reactant : reaction.getReactants()) {
      // metaid is missing, maybe it will work ;)
      Species species = (Species) reactant.getElement();

      Set<Element> parents = new HashSet<>();

      // we need top parent species
      while (species.getComplex() != null) {
        species = species.getComplex();
        if (parents.contains(species)) {
          throw new InvalidArgumentException(eu.getElementTag(species) + " Complex information is cyclic");
        }
        parents.add(species);
      }

      sb.append("<speciesReference species=\"" + elements.getElementId(species) + "\" ");
      if (reactant.getStoichiometry() != null) {
        sb.append(" stoichiometry=\"" + reactant.getStoichiometry() + "\" ");
      }
      sb.append(">\n");
      sb.append("<annotation>\n");
      sb.append("<celldesigner:extension>\n");
      sb.append("<celldesigner:alias>" + reactant.getElement().getElementId() + "</celldesigner:alias>\n");
      sb.append("</celldesigner:extension>\n");
      sb.append("</annotation>\n");
      sb.append("</speciesReference>\n");
    }
    sb.append("</listOfReactants>\n");
    return sb.toString();
  }

  /**
   * Creates annotation xml node for reaction.
   * 
   * @param reaction
   *          reaction to be processed
   * @return xml node representing annotation part
   */
  private String getAnnotationXmlStringForReaction(final Reaction reaction) throws InconsistentModelException {
    String reactionClassString = getCellDesignerReactionTypeString(reaction);
    StringBuilder sb = new StringBuilder();
    sb.append("<annotation>\n");
    sb.append("<celldesigner:extension>\n");
    sb.append("<celldesigner:reactionType>" + reactionClassString + "</celldesigner:reactionType>\n");
    sb.append(getBaseReactantsXmlStringForReaction(reaction));
    sb.append(getBaseProductsXmlStringForReaction(reaction));
    sb.append(getListOfReactantLinksXmlStringForReaction(reaction));
    sb.append(getListOfProductLinksXmlStringForReaction(reaction));
    sb.append(getConnectSchemeXmlStringForReaction(reaction));
    sb.append(getEditPointsXmlStringForReaction(reaction));
    sb.append(getListOfModificationsXmlStringForReaction(reaction));
    sb.append(getListOfGateMembersXmlStringForReaction(reaction));

    sb.append(getLineXmlStringForLines(reaction.getLine()));

    XmlAnnotationParser xmlAnnotationParser = new XmlAnnotationParser(
        CommonXmlParser.RELATION_TYPES_SUPPORTED_BY_CELL_DESIGNER);
    sb.append("</celldesigner:extension>\n");
    sb.append(xmlAnnotationParser.dataSetToXmlString(reaction.getMiriamData(), reaction.getIdReaction()));
    sb.append("</annotation>\n");
    return sb.toString();
  }

  String getCellDesignerReactionTypeString(final Reaction reaction) {
    for (final NodeOperator operator : reaction.getOperators()) {
      // in cell designer and operator can be mapped both as boolean logic gate
      // and
      // normal reaction, but reactions with any other operator type must be
      // exported
      // to BOOLEAN_LOGIC_GATE
      if (!(operator instanceof AndOperator || !operator.isReactantOperator())) {
        return "BOOLEAN_LOGIC_GATE";
      } else if (operator instanceof AndOperator && operator.isReactantOperator()
          && reaction instanceof ReducedNotation) {
        // check the geometry - we might need to use BOOLEAN_LOGIC_GATE if the
        // joining
        // point is not colinear with first reactant line and operator line
        AbstractNode firstReactant = operator.getInputs().get(0);
        // if we have operator then take operator as input checking colinear
        // points
        for (final AbstractNode node : operator.getInputs()) {
          if (node instanceof NodeOperator) {
            firstReactant = node;
            break;
          }
        }
        PolylineData inputLine = firstReactant.getLine();
        PolylineData operatorLine = operator.getLine();
        double dist = lineTransformation.distBetweenPointAndLineSegment(
            inputLine.getLines().get(inputLine.getLines().size() - 1).getP1(), operatorLine.getLines().get(0).getP2(),
            operatorLine.getLines().get(0).getP1());

        if (dist > Configuration.EPSILON) {
          return "BOOLEAN_LOGIC_GATE";
        }
      }
    }
    ReactionLineData rdl = ReactionLineData.getByReactionType(reaction.getClass());
    if (rdl == null) {
      throw new InvalidArgumentException("Unknown reaction type: " + reaction.getClass());
    }
    return rdl.getCellDesignerString();
  }

  /**
   * Returns xml node with a list of gate members for reaction. For now only
   * {@link TwoReactantReactionInterface} is supported.
   * 
   * @param reaction
   *          reaction for which the xml will be returned
   * @return xml string with list of gate members for reaction
   */
  private StringBuilder getListOfGateMembersXmlStringForReaction(final Reaction reaction) {
    StringBuilder result = new StringBuilder();

    if (getCellDesignerReactionTypeString(reaction).equals("BOOLEAN_LOGIC_GATE")) {
      Product product = reaction.getProducts().get(0);
      NodeOperator operator = reaction.getOperators().get(0);
      OperatorType type = OperatorType.getTypeByClass(operator.getClass());

      // if type is not defined then we are working on something that doesn't
      // use gates
      if (type != null) {
        result.append("<celldesigner:listOfGateMember>\n");
        ReactionLineData lineData = ReactionLineData.getByReactionType(reaction.getClass());

        String aliases = reaction.getReactants().get(0).getElement().getElementId();
        for (int i = 1; i < reaction.getReactants().size(); i++) {
          aliases += "," + reaction.getReactants().get(i).getElement().getElementId();
        }
        // product line
        result.append("<celldesigner:GateMember type=\"" + type.getStringName() + "\"");
        result.append(" aliases=\"" + aliases + "\"");
        result.append(" modificationType=\"" + lineData.getCellDesignerString() + "\"");

        CellDesignerLineTransformation clt = new CellDesignerLineTransformation();
        List<Point2D> points = clt.getPointsFromLine(getLineBeforeTrimming(product));

        String editPoints = getEditPointsXmlStringForBooleanGateReaction(reaction)
            .replace("<celldesigner:editPoints>", "").replace("</celldesigner:editPoints>\n", "");
        result.append(" editPoints=\"" + editPoints + "\">\n");

        result.append("<celldesigner:connectScheme connectPolicy=\"direct\">\n");
        result.append("<celldesigner:listOfLineDirection>\n");
        for (int i = 0; i < product.getLine().getLines().size(); i++) {
          result.append("<celldesigner:lineDirection index=\"" + i + "\" value=\"unknown\"/>\n");
        }
        result.append("</celldesigner:listOfLineDirection>\n");
        result.append("</celldesigner:connectScheme>\n");
        result.append(getLineXmlStringForLines(operator.getLine()));

        result.append("</celldesigner:GateMember>\n");

        // reactant line
        for (final Reactant reactant : reaction.getReactants()) {

          result.append("<celldesigner:GateMember type=\"" + lineData.getCellDesignerString() + "\"");
          result.append(" aliases=\"" + reactant.getElement().getElementId() + "\"");

          points = clt.getPointsFromLine(reactant.getLine());

          result.append(" editPoints=\"");
          for (int i = 0; i < points.size(); i++) {
            result.append(points.get(i).getX() + "," + points.get(i).getY() + " ");
          }
          result.append("\">\n");

          result.append("<celldesigner:connectScheme connectPolicy=\"direct\">\n");
          result.append("<celldesigner:listOfLineDirection>\n");
          for (int i = 0; i < reactant.getLine().getLines().size(); i++) {
            result.append("<celldesigner:lineDirection index=\"" + i + "\" value=\"unknown\"/>\n");
          }
          result.append("</celldesigner:listOfLineDirection>\n");
          result.append("</celldesigner:connectScheme>\n");
          result.append(getLinkTargetXmlString(reactant));
          result.append(getLineXmlStringForLines(reactant.getLine()));

          result.append("</celldesigner:GateMember>\n");
        }

        result.append("</celldesigner:listOfGateMember>\n");
      }
    }
    return result;
  }

  /**
   * Creates xml node (listOfProductLinks) with list of products.
   * 
   * @param reaction
   *          reaction for which list of products is created
   * @return xml node (listOfProductLinks) with list of products
   */
  private String getListOfProductLinksXmlStringForReaction(final Reaction reaction) {
    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:listOfProductLinks>\n");
    int firstElement = 1;
    if (reaction instanceof TwoProductReactionInterface) {
      firstElement = 2;
    }
    for (int i = firstElement; i < reaction.getProducts().size(); i++) {
      sb.append(getProductLinkXmlString(reaction.getProducts().get(i)));
    }
    sb.append("</celldesigner:listOfProductLinks>\n");
    return sb.toString();
  }

  /**
   * Creates xml node (listOfReactantLinks) with list of reactants.
   * 
   * @param reaction
   *          reaction for which list of reactants is created
   * @return xml node (listOfReactantLinks) with list of reactants
   */
  private String getListOfReactantLinksXmlStringForReaction(final Reaction reaction) {
    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:listOfReactantLinks>\n");
    int firstElement = 1;
    if (reaction instanceof TwoReactantReactionInterface) {
      firstElement = 2;
    } else if (getCellDesignerReactionTypeString(reaction).equals("BOOLEAN_LOGIC_GATE")) {
      firstElement = reaction.getReactants().size();
    }
    for (int i = firstElement; i < reaction.getReactants().size(); i++) {
      sb.append(getReactantLinkXmlString(reaction.getReactants().get(i)));
    }
    sb.append("</celldesigner:listOfReactantLinks>\n");
    return sb.toString();
  }

  /**
   * Creates rectantLink xml node for reactant.
   * 
   * @param reactant
   *          reactant to be transformed
   * @return xml node describing reactantLink
   */
  private String getReactantLinkXmlString(final Reactant reactant) {
    Element alias = reactant.getElement();
    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:reactantLink ");
    sb.append("reactant=\"" + elements.getElementId(alias) + "\" ");
    sb.append("alias=\"" + alias.getElementId() + "\" ");
    // targetLineIndex is missing (maybe it's unimportant :))
    sb.append(">\n");

    sb.append(getAnchorXml(alias, reactant.getLine().getStartPoint()));

    sb.append(getConnectSchemeXmlStringForLines(new PolylineData[] { reactant.getLine() }));
    sb.append(getEditPointsXmlStringForLine(new PolylineData[] { reactant.getLine() }, 0));
    sb.append(getLineXmlStringForLines(reactant.getLine()));
    sb.append("</celldesigner:reactantLink>\n");
    return sb.toString();
  }

  /**
   * Creates productLink xml node for product.
   * 
   * @param product
   *          product to be transformed
   * @return xml node describing productLink
   */
  private String getProductLinkXmlString(final Product product) {
    Element alias = product.getElement();
    PolylineData[] lines = new PolylineData[] { getLineBeforeTrimming(product) };

    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:productLink ");
    sb.append("product=\"" + elements.getElementId(alias) + "\" ");
    sb.append("alias=\"" + alias.getElementId() + "\" ");
    // targetLineIndex is missing (maybe it's unimportant :))
    sb.append(">\n");

    sb.append(getAnchorXml(alias, getLineBeforeTrimming(product).getEndPoint()));

    sb.append(getConnectSchemeXmlStringForLines(lines));

    sb.append(getEditPointsXmlStringForLine(lines, 0));
    sb.append(getLineXmlStringForLines(product.getLine()));
    sb.append("</celldesigner:productLink>\n");
    return sb.toString();
  }

  /**
   * Creates valid xml node with connectScheme for the set of lines.
   * 
   * @param lines
   *          list of lines to be included in connectScheme. As far as I
   *          remember only one or three lines could be provided.
   * @return xml node with connectScheme
   */
  String getConnectSchemeXmlStringForLines(final PolylineData[] lines) {
    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:connectScheme ");
    sb.append("connectPolicy=\"direct\" ");
    sb.append(">\n");
    sb.append("<celldesigner:listOfLineDirection>\n");
    int armId = 0;
    for (final PolylineData line : lines) {
      String arm = "";
      if (lines.length > 1) {
        arm = " arm\"" + armId + "\" ";
        armId++;
      }
      for (int i = 0; i < line.getLines().size(); i++) {
        sb.append("<celldesigner:lineDirection " + arm + " index=\"" + i + "\" value=\"unknown\"/>\n");
      }
    }
    sb.append("</celldesigner:listOfLineDirection>\n");
    sb.append("</celldesigner:connectScheme>\n");
    return sb.toString();
  }

  /**
   * Creates
   * {@link lcsb.mapviewer.converter.model.celldesigner.structure.fields.EditPoints}
   * structure representing lines in CellDesigner.
   * 
   * @param lines
   *          lines to be transformed into single
   *          {@link lcsb.mapviewer.converter.model.celldesigner.structure.fields.EditPoints}
   *          structure
   * @param centerIndex
   *          where the central line is positioned
   * @return xml node
   */
  private String getEditPointsXmlStringForLine(final PolylineData[] lines, final Integer centerIndex) {
    return getEditPointsXmlStringForLine(lines, null, centerIndex);
  }

  /**
   * Creates
   * {@link lcsb.mapviewer.converter.model.celldesigner.structure.fields.EditPoints}
   * structure representing lines in CellDesigner.
   * 
   * @param lines
   *          lines to be transformed into single
   *          {@link lcsb.mapviewer.converter.model.celldesigner.structure.fields.EditPoints}
   *          structure
   * @param centerPoint
   *          central point when list of lines is greater than 1
   * @param centerIndex
   *          where the central line is positioned
   * @return xml node
   */
  private String getEditPointsXmlStringForLine(final PolylineData[] lines, final Point2D centerPoint, final Integer centerIndex) {
    StringBuilder sb = new StringBuilder();

    sb.append("<celldesigner:editPoints");
    if (lines.length > 1) {
      for (int i = 0; i < lines.length; i++) {
        sb.append(" num" + i + "=\"" + (lines[i].getLines().size() - 1) + "\"");
      }
      if (centerIndex != null) {
        sb.append(" tShapeIndex=\"" + centerIndex + "\"");
      }
    }
    sb.append(">");

    boolean first = true;
    for (final PolylineData pd : lines) {
      List<Point2D> points = lineTransformation.getPointsFromLine(pd);

      for (final Point2D point : points) {
        if (first) {
          first = false;
        } else {
          sb.append(" ");
        }
        sb.append(point.getX() + "," + point.getY());
      }
    }
    if (centerPoint != null) {
      if (first) {
        first = false;
      } else {
        sb.append(" ");
      }
      sb.append(centerPoint.getX() + "," + centerPoint.getY());
    }
    sb.append("</celldesigner:editPoints>\n");

    // when we don't have any points then we cannot create editPoints node (CD
    // throws null pointer exception)
    if (first && centerPoint == null) {
      return "";
    }
    return sb.toString();
  }

  /**
   * Returns xml node representing
   * {@link lcsb.mapviewer.converter.model.celldesigner.structure.fields.EditPoints}
   * structure.
   * 
   * @param reaction
   *          reaction for which xml node is generated
   * @return xml node representing
   *         {@link lcsb.mapviewer.converter.model.celldesigner.structure.fields.EditPoints}
   *         structure
   */
  String getEditPointsXmlStringForReaction(final Reaction reaction) {
    if (getCellDesignerReactionTypeString(reaction).equals("BOOLEAN_LOGIC_GATE")) {
      return getEditPointsXmlStringForBooleanGateReaction(reaction);
    } else if (reaction instanceof SimpleReactionInterface) {
      return getEditPointsXmlStringForSimpleReaction(reaction);
    } else if (reaction instanceof TwoReactantReactionInterface) {
      return getEditPointsXmlStringForTwoReactantReaction(reaction);
    } else if (reaction instanceof TwoProductReactionInterface) {
      return getEditPointsXmlStringForTwoProductReaction(reaction);
    } else {
      throw new InvalidArgumentException("Unhandled reaction type: " + reaction.getClass());
    }
  }

  private String getEditPointsXmlStringForTwoProductReaction(final Reaction reaction) {
    List<Point2D> points = new ArrayList<Point2D>();
    Reactant reactant = reaction.getReactants().get(0);
    Product product1 = reaction.getProducts().get(0);
    Product product2 = reaction.getProducts().get(1);
    for (final Line2D line : reactant.getLine().getLines()) {
      points.add(line.getP1());
    }

    Point2D startPoint = points.get(0);
    CellDesignerAliasConverter converter = new CellDesignerAliasConverter(reactant.getElement(), sbgn);
    CellDesignerAnchor anchor = converter.getAnchorForCoordinates(reactant.getElement(), startPoint);
    if (anchor == null) { // when we have no anchor, then end point is set
      // to
      // the center of alias
      points.set(points.size() - 1, reactant.getElement().getCenter());
    }
    int index = points.size();
    for (final NodeOperator operator : reaction.getOperators()) {
      if (operator.isProductOperator()) {
        boolean process = true;
        for (final AbstractNode node : operator.getOutputs()) {
          if (!(node instanceof Product)) {
            process = false;
          }
        }
        if (process) {
          for (int i = operator.getLine().getLines().size() - 1; i >= 0; i--) {
            points.add(operator.getLine().getLines().get(i).getP1());
          }
        }
      }
    }
    index = points.size() - index - 1;

    PolylineData line1 = new PolylineData(points);

    PolylineData line2 = getLineBeforeTrimming(product1);
    PolylineData line3 = getLineBeforeTrimming(product2);

    Point2D pointA = product1.getElement().getCenter();
    Point2D pointB = product2.getElement().getCenter();
    Point2D pointC = reactant.getElement().getCenter();

    Point2D pointO = line2.getStartPoint();

    Point2D centerPoint = pointTransformation.getCoordinatesInCellDesignerBase(pointA, pointB, pointC, pointO);

    return getEditPointsXmlStringForLine(new PolylineData[] { line1.reverse(), line2, line3 }, centerPoint, index);
  }

  private String getEditPointsXmlStringForTwoReactantReaction(final Reaction reaction) {
    Reactant reactant1 = reaction.getReactants().get(0);
    Reactant reactant2 = reaction.getReactants().get(1);
    PolylineData line1 = reactant1.getLine();
    PolylineData line2 = reactant2.getLine();
    List<Point2D> points = new ArrayList<>();
    for (final NodeOperator operator : reaction.getOperators()) {
      if (operator.isReactantOperator()) {
        boolean process = true;
        for (final AbstractNode node : operator.getInputs()) {
          if (!(node instanceof Reactant)) {
            process = false;
          }
        }
        if (process) {
          for (final Line2D line : operator.getLine().getLines()) {
            points.add(line.getP1());
          }
        }
      }
    }
    Product product = reaction.getProducts().get(0);
    PolylineData productLine = getLineBeforeTrimming(product);
    for (final Line2D line : productLine.getLines()) {
      points.add(line.getP2());
    }

    Point2D endPoint = points.get(points.size() - 1);
    CellDesignerAliasConverter converter = new CellDesignerAliasConverter(product.getElement(), sbgn);
    CellDesignerAnchor anchor = converter.getAnchorForCoordinates(product.getElement(), endPoint);
    if (anchor == null) { // when we have no anchor, then end point is set
      // to
      // the center of alias
      points.set(points.size() - 1, product.getElement().getCenter());
    }

    PolylineData line3 = new PolylineData(points);

    Point2D pointC = reactant1.getElement().getCenter();
    Point2D pointA = reactant2.getElement().getCenter();
    Point2D pointB = product.getElement().getCenter();

    Point2D pointO = line3.getStartPoint();

    Point2D centerPoint = pointTransformation.getCoordinatesInCellDesignerBase(pointA, pointB, pointC, pointO);

    return getEditPointsXmlStringForLine(
        new PolylineData[] { line1.reverse(), line2.reverse(), line3.reverse().reverse() }, centerPoint,
        line3.getLines().size() - product.getLine().getLines().size());
  }

  private String getEditPointsXmlStringForBooleanGateReaction(final Reaction reaction) {
    Reactant r = reaction.getReactants().get(0);
    Point2D centerPoint = r.getLine().getEndPoint();
    return getEditPointsXmlStringForLine(
        new PolylineData[] { reaction.getProducts().get(0).getLine() }, centerPoint,
        0);
  }

  String getEditPointsXmlStringForSimpleReaction(final Reaction reaction) {
    Product product = reaction.getProducts().get(0);
    Reactant reactant = reaction.getReactants().get(0);

    List<Point2D> points = new ArrayList<>();
    for (int i = 0; i < reactant.getLine().getLines().size(); i++) {
      points.add(reactant.getLine().getLines().get(i).getP1());
    }
    int centerPosition = reactant.getLine().getLines().size();
    PolylineData productLine = getLineBeforeTrimming(product);

    PolylineData testLine = new PolylineData();
    testLine.addLine(reactant.getLine().getLines().get(reactant.getLine().getLines().size() - 1));
    testLine.addLine(reactant.getLine().getEndPoint(), productLine.getStartPoint());
    testLine.addLine(productLine.getLines().get(0));

    testLine = PolylineDataFactory.removeCollinearPoints(testLine);

    if (testLine.getLines().size() > 1) {
      points.add(reactant.getLine().getEndPoint());
      centerPosition++;
      points.add(productLine.getStartPoint());
    }
    for (final Line2D line : productLine.getLines()) {
      points.add(line.getP2());
    }

    PolylineData pd = new PolylineData(points);
    pd.setStartPoint(getAnchorPoint(reactant.getElement(), reactant.getLine().getStartPoint()));
    pd.setEndPoint(getAnchorPoint(product.getElement(), productLine.getEndPoint()));
    return getEditPointsXmlStringForLine(new PolylineData[] { pd }, centerPosition);
  }

  private Point2D getAnchorPoint(final Element element, final Point2D originalPoint) {
    CellDesignerAliasConverter converter = new CellDesignerAliasConverter(element, sbgn);

    CellDesignerAnchor anchor = converter.getAnchorForCoordinates(element, originalPoint);
    return converter.getPointCoordinates(element, anchor);
  }

  /**
   * Creates valid xml node with connectScheme for the reaction.
   * 
   * @param reaction
   *          reaction for which the connectSchema is going to be created
   * @return xml node with connectScheme
   */
  String getConnectSchemeXmlStringForReaction(final Reaction reaction) {
    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:connectScheme ");
    sb.append("connectPolicy=\"direct\" ");
    int rectangleIndex = -1;
    if (reaction instanceof SimpleReactionInterface) {
      rectangleIndex = reaction.getReactants().get(0).getLine().getLines().size() - 1;
      sb.append("rectangleIndex=\"" + rectangleIndex + "\" ");
    }
    sb.append(">\n");
    sb.append("<celldesigner:listOfLineDirection>\n");
    if (reaction instanceof SimpleReactionInterface) {
      int size = reaction.getReactants().get(0).getLine().getLines().size()
          + reaction.getProducts().get(0).getLine().getLines().size() + 1;
      for (int i = 0; i < size; i++) {
        sb.append("<celldesigner:lineDirection index=\"" + i + "\" value=\"unknown\"/>\n");
      }
    } else if (reaction instanceof TwoProductReactionInterface || reaction instanceof TwoReactantReactionInterface) {
      String string = getEditPointsXmlStringForReaction(reaction);
      Pattern p0 = Pattern.compile("num0=\"([0-9]+)");
      Pattern p1 = Pattern.compile("num1=\"([0-9]+)");
      Pattern p2 = Pattern.compile("num2=\"([0-9]+)");
      Matcher m0 = p0.matcher(string);
      Matcher m1 = p1.matcher(string);
      Matcher m2 = p2.matcher(string);
      int num0 = 0;
      int num1 = 0;
      int num2 = 0;

      if (m0.find() && m1.find() && m2.find()) {
        num0 = Integer.parseInt(m0.group(1));
        num1 = Integer.parseInt(m1.group(1));
        num2 = Integer.parseInt(m2.group(1));
      } else {
        throw new InvalidArgumentException("Invalid editPoints string" + string);
      }

      for (int i = 0; i <= num0; i++) {
        sb.append("<celldesigner:lineDirection arm=\"0\" index=\"" + i + "\" value=\"unknown\"/>\n");
      }
      for (int i = 0; i <= num1; i++) {
        sb.append("<celldesigner:lineDirection arm=\"1\" index=\"" + i + "\" value=\"unknown\"/>\n");
      }
      for (int i = 0; i <= num2; i++) {
        sb.append("<celldesigner:lineDirection arm=\"2\" index=\"" + i + "\" value=\"unknown\"/>\n");
      }
    } else {
      throw new InvalidArgumentException("Unknown reaction type: " + reaction.getClass());

    }
    sb.append("</celldesigner:listOfLineDirection>\n");
    sb.append("</celldesigner:connectScheme>\n");
    return sb.toString();
  }

  /**
   * Returns xml node with list of base products.
   * 
   * @param reaction
   *          reaction for which the node is created
   * @return xml node with list of base products
   */
  private String getBaseProductsXmlStringForReaction(final Reaction reaction) {
    List<Product> products = new ArrayList<>();
    products.add(reaction.getProducts().get(0));
    if (reaction instanceof TwoProductReactionInterface) {
      products.add(reaction.getProducts().get(1));
    }

    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:baseProducts>\n");
    for (final Product product : products) {
      Element element = product.getElement();
      sb.append("<celldesigner:baseProduct ");
      sb.append("species=\"" + elements.getElementId(element) + "\" ");
      sb.append("alias=\"" + element.getElementId() + "\" ");
      sb.append(">\n");
      sb.append(getAnchorXml(element, getLineBeforeTrimming(product).getEndPoint()));

      sb.append("</celldesigner:baseProduct>\n");
    }
    sb.append("</celldesigner:baseProducts>\n");
    return sb.toString();
  }

  /**
   * Returns xml node with list of base reactants.
   * 
   * @param reaction
   *          reaction for which the node is created
   * @return xml node with list of base reactants
   */
  private String getBaseReactantsXmlStringForReaction(final Reaction reaction) {
    List<Reactant> reactants = new ArrayList<Reactant>();
    reactants.add(reaction.getReactants().get(0));
    if (reaction instanceof TwoReactantReactionInterface) {
      reactants.add(reaction.getReactants().get(1));
    } else if (getCellDesignerReactionTypeString(reaction).equals("BOOLEAN_LOGIC_GATE")) {
      for (int i = 1; i < reaction.getReactants().size(); i++) {
        reactants.add(reaction.getReactants().get(i));
      }
    }

    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:baseReactants>\n");
    for (final Reactant reactant : reactants) {
      Element alias = reactant.getElement();
      sb.append("<celldesigner:baseReactant ");
      sb.append("species=\"" + elements.getElementId(reactant.getElement()) + "\" ");
      sb.append("alias=\"" + reactant.getElement().getElementId() + "\" ");
      sb.append(">\n");
      sb.append(getAnchorXml(alias, reactant.getLine().getStartPoint()));

      sb.append("</celldesigner:baseReactant>\n");
    }
    sb.append("</celldesigner:baseReactants>\n");
    return sb.toString();
  }

  /**
   * Returns xml node that describes anchor point.
   * 
   * @param alias
   *          alias on which we are looking for anchor
   * @param point
   *          point on the alias that should be aligned to the closest anchor
   *          point
   * @return xml node with anchor point
   */
  private String getAnchorXml(final Element alias, final Point2D point) {
    CellDesignerAliasConverter converter = new CellDesignerAliasConverter(alias, sbgn);
    CellDesignerAnchor anchor = converter.getAnchorForCoordinates(alias, point);
    if (anchor != null) {
      return "<celldesigner:linkAnchor position=\"" + anchor + "\"/>\n";
    } else {
      return "";
    }
  }

  private PolylineData getLineBeforeTrimming(final ReactionNode node) {
    if (node instanceof Product) {
      // line was trimmed for some reaction types
      PolylineData line = node.getLine().copy();
      ReactionLineData rld = ReactionLineData.getByReactionType(node.getReaction().getClass());
      line.trimEnd(-rld.getProductLineTrim());
      return line;
    } else {
      return node.getLine();
    }
  }

}
