package lcsb.mapviewer.converter.model.celldesigner;

import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.ZIndexPopulator;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.alias.AliasCollectionXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.compartment.CompartmentCollectionXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.function.FunctionCollectionXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.parameter.ParameterCollectionXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.reaction.ReactionCollectionXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.species.InternalModelSpeciesData;
import lcsb.mapviewer.converter.model.celldesigner.species.SpeciesCollectionXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerCompartment;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerSpecies;
import lcsb.mapviewer.converter.model.celldesigner.unit.UnitCollectionXmlParser;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.modelutils.map.LogFormatter;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.xerces.parsers.DOMParser;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class is a parser for CellDesigner files. There are two typical use
 * cases:
 * <ul>
 * <li>CellDesigner xml -> our model. To perform this action
 * {@link Converter#createModel(ConverterParams)}} method should be called.</li>
 * <li>our model -> CellDesigner xml. To perform this action
 * {@link #model2String(Model)} method should be called.</li>
 * </ul>
 * <p/>
 * CellDEsigner format is the extension of <a href="http://sbml.org">SBML</a>.
 * More information about this format could be found <a href=
 * "http://www.celldesigner.org/documents/CellDesigner4ExtensionTagSpecificationE.pdf"
 * >here</a>.
 *
 * @author Piotr Gawron
 */
@Component
public class CellDesignerXmlParser extends Converter {

  /**
   * Default class logger.
   */
  private static final Logger logger = LogManager.getLogger();

  /**
   * CellDesigner parser for layers.
   */
  private final LayerXmlParser layerParser = new LayerXmlParser();

  /**
   * CellDesigner parser for compartments collections.
   */
  private CompartmentCollectionXmlParser compartmentCollectionXmlParser;

  /**
   * CellDesigner parser for species collections.
   */
  private SpeciesCollectionXmlParser speciesSbmlParser;

  /**
   * CellDesigner parser for alias collections.
   */
  private AliasCollectionXmlParser aliasCollectionParser;

  /**
   * Annotation parser.
   */
  private final RestAnnotationParser rap = new RestAnnotationParser();

  private boolean oldCellDesignerVersion;

  @Override
  public Model createModel(final ConverterParams params) throws InvalidInputDataExecption {
    oldCellDesignerVersion = false;
    final CellDesignerElementCollection elements = new CellDesignerElementCollection();
    final FunctionCollectionXmlParser functionParser = new FunctionCollectionXmlParser();
    final UnitCollectionXmlParser unitParser = new UnitCollectionXmlParser();

    final Model model = new ModelFullIndexed(null);

    if (params.getFilename() != null) {
      model.setName(FilenameUtils.getBaseName(params.getFilename()));
    }
    speciesSbmlParser = new SpeciesCollectionXmlParser(elements, model.getUnits());
    aliasCollectionParser = new AliasCollectionXmlParser(elements, model);
    compartmentCollectionXmlParser = new CompartmentCollectionXmlParser(elements);

    final ParameterCollectionXmlParser parameterParser = new ParameterCollectionXmlParser(model);

    final DOMParser parser = new DOMParser();
    try {
      parser.parse(params.getSource());
    } catch (final Exception e) {
      throw new InvalidInputDataExecption("Problem with a file: " + params.getSource().getSystemId(), e);
    }
    final Document doc = parser.getDocument();
    try {

      // Get the document's root XML node
      final NodeList root = doc.getChildNodes();

      // Navigate down the hierarchy to get to the CEO node
      final Node sbmlNode = XmlParser.getNode("SBML", root);
      if (sbmlNode == null) {
        throw new InvalidInputDataExecption("No SBML node");
      }

      final Node modelNode = XmlParser.getNode("model", sbmlNode.getChildNodes());
      if (modelNode == null) {
        throw new InvalidInputDataExecption("No model node in SBML");
      }
      // we ignore metaid - it's useless and obstruct data model
      // model.setMetaId(XmlParser.getNodeAttr("metaId", modelNode));
      final String modelId = XmlParser.getNodeAttr("id", modelNode);
      if (modelId != null && !modelId.isEmpty()) {
        model.setIdModel(modelId);
      }

      Node reactionsNode = null;
      Node functionsNode = null;
      Node unitsNode = null;
      Node parametersNode = null;

      final NodeList nodes = modelNode.getChildNodes();
      for (int x = 0; x < nodes.getLength(); x++) {
        final Node node = nodes.item(x);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          if (node.getNodeName().equalsIgnoreCase("annotation")) {
            continue;
          } else if (node.getNodeName().equalsIgnoreCase("listOfSpecies")) {
            continue;
          } else if (node.getNodeName().equalsIgnoreCase("listOfReactions")) {
            reactionsNode = node;
          } else if (node.getNodeName().equalsIgnoreCase("listOfCompartments")) {
            // we already parsed compartments
            continue;
          } else if (node.getNodeName().equalsIgnoreCase("notes")) {
            String notes = rap.getNotes(node);
            if (notes != null) {
              notes = StringEscapeUtils.unescapeHtml4(notes);
            }
            model.setNotes(notes);
          } else if (node.getNodeName().equalsIgnoreCase("listOfUnitDefinitions")) {
            unitsNode = node;
          } else if (node.getNodeName().equalsIgnoreCase("listOfFunctionDefinitions")) {
            functionsNode = node;
          } else if (node.getNodeName().equalsIgnoreCase("listOfParameters")) {
            parametersNode = node;
          } else {
            throw new InvalidInputDataExecption("Unknown element of model: " + node.getNodeName());
          }
        }
      }

      if (unitsNode != null) {
        model.addUnits(unitParser.parseXmlUnitCollection(unitsNode));
      }

      if (parametersNode != null) {
        model.addParameters(parameterParser.parseXmlParameterCollection(parametersNode));
      }

      final Node compartmentNode = XmlParser.getNode("listOfCompartments", modelNode.getChildNodes());
      if (compartmentNode != null) {
        final List<CellDesignerCompartment> compartments = compartmentCollectionXmlParser
            .parseXmlCompartmentCollection(compartmentNode);
        elements.addElements(compartments);
      }

      final InternalModelSpeciesData modelData = new InternalModelSpeciesData();

      final Node speciesNode = XmlParser.getNode("listOfSpecies", modelNode.getChildNodes());
      if (speciesNode != null) {
        final List<Pair<String, ? extends CellDesignerSpecies<?>>> species = speciesSbmlParser
            .parseSbmlSpeciesCollection(speciesNode);
        modelData.updateSpecies(species);
      }

      final Node annotationNode = XmlParser.getNode("annotation", modelNode.getChildNodes());
      if (annotationNode == null) {
        throw new InvalidInputDataExecption("No annotation node in SBML/model");
      }

      parseAnnotation(model, annotationNode, modelData, elements);

      if (speciesNode != null) {
        final List<Pair<String, ? extends CellDesignerSpecies<?>>> species = speciesSbmlParser
            .parseSbmlSpeciesCollection(speciesNode);
        modelData.updateSpecies(species);
      }
      if (functionsNode != null) {
        model.addFunctions(functionParser.parseXmlFunctionCollection(functionsNode));
      }

      if (reactionsNode != null) {
        final ReactionCollectionXmlParser reactionCollectionXmlParser = new ReactionCollectionXmlParser(model, elements,
            params.isSbgnFormat(), oldCellDesignerVersion);
        final List<Reaction> reactions = reactionCollectionXmlParser.parseXmlReactionCollection(reactionsNode);
        model.addReactions(reactions);
      }

      if (params.isSizeAutoAdjust()) {
        final Rectangle2D bound = getModelBound(model);
        final double width = bound.getWidth() + 2 * (Math.max(0, bound.getX()));
        final double height = bound.getHeight() + 2 * (Math.max(0, bound.getY()));

        model.setWidth(width);
        model.setHeight(height);
      }
      new ZIndexPopulator().populateZIndex(model);
    } catch (final InvalidXmlSchemaException e) {
      throw new InvalidInputDataExecption(e);
    } catch (final CellDesignerParserException e) {
      throw new InvalidInputDataExecption(e.getMessageWithContext(), e);
    }

    return model;
  }

  @Override
  public String model2String(final Model model) throws InconsistentModelException {
    return model2Xml(model, true);
  }

  @Override
  public String getCommonName() {
    return "CellDesigner SBML";
  }

  @Override
  public MimeType getMimeType() {
    return MimeType.SBML;
  }

  @Override
  public List<String> getFileExtensions() {
    return Collections.singletonList("xml");
  }

  /**
   * Computes bound of the model.
   *
   * @param model object for which computation is done
   * @return bound of the model
   */
  Rectangle2D getModelBound(final Model model) {
    double maxX = 0;
    double maxY = 0;
    double minX = model.getWidth();
    double minY = model.getHeight();
    for (final Element alias : model.getElements()) {
      maxX = Math.max(maxX, alias.getWidth() + alias.getX());
      maxY = Math.max(maxY, alias.getHeight() + alias.getY());

      minX = Math.min(minX, alias.getX());
      minY = Math.min(minY, alias.getY());

    }

    for (final Reaction reaction : model.getReactions()) {
      for (final Line2D line : reaction.getLines()) {
        maxX = Math.max(maxX, line.getX1());
        maxX = Math.max(maxX, line.getX2());
        maxY = Math.max(maxY, line.getY1());
        maxY = Math.max(maxY, line.getY2());

        minX = Math.min(minX, line.getX1());
        minX = Math.min(minX, line.getX2());
        minY = Math.min(minY, line.getY1());
        minY = Math.min(minY, line.getY2());
      }
    }

    for (final Layer layer : model.getLayers()) {
      for (final PolylineData lline : layer.getLines()) {
        for (final Line2D line : lline.getLines()) {
          maxX = Math.max(maxX, line.getX1());
          maxX = Math.max(maxX, line.getX2());
          maxY = Math.max(maxY, line.getY1());
          maxY = Math.max(maxY, line.getY2());

          minX = Math.min(minX, line.getX1());
          minX = Math.min(minX, line.getX2());
          minY = Math.min(minY, line.getY1());
          minY = Math.min(minY, line.getY2());
        }
      }
      for (final LayerRect rect : layer.getRectangles()) {
        maxX = Math.max(maxX, rect.getX());
        maxX = Math.max(maxX, rect.getX() + rect.getWidth());
        maxY = Math.max(maxY, rect.getY());
        maxY = Math.max(maxY, rect.getY() + rect.getHeight());

        minX = Math.min(minX, rect.getX());
        minX = Math.min(minX, rect.getX() + rect.getWidth());
        minY = Math.min(minY, rect.getY());
        minY = Math.min(minY, rect.getY() + rect.getHeight());
      }
      for (final LayerOval oval : layer.getOvals()) {
        maxX = Math.max(maxX, oval.getX());
        maxX = Math.max(maxX, oval.getX() + oval.getWidth());
        maxY = Math.max(maxY, oval.getY());
        maxY = Math.max(maxY, oval.getY() + oval.getHeight());

        minX = Math.min(minX, oval.getX());
        minX = Math.min(minX, oval.getX() + oval.getWidth());
        minY = Math.min(minY, oval.getY());
        minY = Math.min(minY, oval.getY() + oval.getHeight());
      }
      for (final LayerText text : layer.getTexts()) {
        maxX = Math.max(maxX, text.getX());
        maxX = Math.max(maxX, text.getX() + text.getWidth());
        maxY = Math.max(maxY, text.getY());
        maxY = Math.max(maxY, text.getY() + text.getHeight());

        minX = Math.min(minX, text.getX());
        minX = Math.min(minX, text.getX() + text.getWidth());
        minY = Math.min(minY, text.getY());
        minY = Math.min(minY, text.getY() + text.getHeight());
      }
    }

    final Rectangle2D result = new Rectangle2D.Double(minX, minY, maxX - minX, maxY - minY);
    return result;
  }

  /**
   * Parse annotation part of CellDesigner xml.
   *
   * @param model          model that is parsed (and will be updated)
   * @param modelData      object containing information about species during CellDesigner
   *                       parsing
   * @param elements       collection of {@link CellDesignerElement cell designer elements}
   *                       parsed from xml
   * @param annotationNode xml node to parse
   * @throws InvalidXmlSchemaException thrown when xmlString is invalid
   */
  private void parseAnnotation(final Model model, final Node annotationNode, final InternalModelSpeciesData modelData,
                               final CellDesignerElementCollection elements) throws InvalidXmlSchemaException {
    final NodeList nodes = annotationNode.getChildNodes();
    Node extensionNode = null;

    for (int x = 0; x < nodes.getLength(); x++) {
      final Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:extension")) {
          extensionNode = node;
        } else if (node.getNodeName().equalsIgnoreCase("rdf:RDF")) {
          processModelRdfNode(model, node);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:modelVersion")) {
          logger.warn("Old version of cell designer found: " + XmlParser.getNodeValue(node));
          oldCellDesignerVersion = true;
          extensionNode = annotationNode;
        } else if (!oldCellDesignerVersion) {
          throw new InvalidXmlSchemaException("Unknown element of annotation: " + node.getNodeName());
        }
      }
    }
    if (extensionNode == null) {
      throw new InvalidXmlSchemaException("No celldesigner:extension node in SBML/model/annotation");
    }

    parseAnnotationExtension(model, modelData, elements, extensionNode);

  }

  private void processModelRdfNode(final Model model, final Node node) throws InvalidXmlSchemaException {
    final XmlAnnotationParser xmlAnnotationParser = new XmlAnnotationParser(
        CommonXmlParser.RELATION_TYPES_SUPPORTED_BY_CELL_DESIGNER);
    model.addMiriamData(xmlAnnotationParser.parseRdfNode(node, null));
    model.addAuthors(xmlAnnotationParser.getAuthorsFromRdf(node));
    model.setCreationDate(xmlAnnotationParser.getCreateDateFromRdf(node));
    model.addModificationDates(xmlAnnotationParser.getModificationDatesFromRdf(node));
  }

  private void parseAnnotationExtension(final Model model, final InternalModelSpeciesData modelData,
                                        final CellDesignerElementCollection elements, final Node extensionNode) throws InvalidXmlSchemaException {
    final SpeciesCollectionXmlParser parser = new SpeciesCollectionXmlParser(elements, model.getUnits());

    final NodeList nodes = extensionNode.getChildNodes();
    Node includedSpecies = null;
    Node listofSpeciesAlias = null;
    Node listOfComplexSpeciesAlias = null;
    Node listOfComparmentAlias = null;
    Node listOfProteins = null;
    Node listOfGenes = null;
    Node listOfRnas = null;
    Node listOfGroups = null;
    Node listOfAntisenseRnas = null;
    for (int x = 0; x < nodes.getLength(); x++) {
      final Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:modelVersion")) {
          // we ignore map version (there is no use for us)
          // model.setVersion(getNodeValue(node));
          continue;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:modelDisplay")) {
          model.setWidth(XmlParser.getNodeAttr("sizeX", node));
          model.setHeight(XmlParser.getNodeAttr("sizeY", node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfSpeciesAliases")) {
          listofSpeciesAlias = node;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfComplexSpeciesAliases")) {
          listOfComplexSpeciesAlias = node;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfProteins")) {
          listOfProteins = node;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfGenes")) {
          listOfGenes = node;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfRNAs")) {
          listOfRnas = node;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfAntisenseRNAs")) {
          listOfAntisenseRnas = node;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfIncludedSpecies")) {
          includedSpecies = node;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfCompartmentAliases")) {
          listOfComparmentAlias = node;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfLayers")) {
          model.addLayers(layerParser.parseLayers(node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfGroups")) {
          listOfGroups = node;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:listOfBlockDiagrams")) {
          layerParser.parseBlocks(model, node);
        } else if (node.getNodeName().equalsIgnoreCase("rdf:RDF")) {
          processModelRdfNode(model, node);
        } else {
          final String message = "Unknown element of annotation: " + node.getNodeName();
          if (!oldCellDesignerVersion) {
            throw new InvalidXmlSchemaException(message);
          } else {
            logger.warn(message);
          }
        }
      }
    }

    if (listOfComparmentAlias != null) {
      final List<Compartment> aliases = aliasCollectionParser.parseXmlCompartmentAliasCollection(listOfComparmentAlias);
      for (final Element alias : aliases) {
        rap.processNotes(alias);
        model.addElement(alias);
      }
    }

    if (includedSpecies != null) {
      final List<Pair<String, ? extends CellDesignerSpecies<?>>> species = parser
          .parseIncludedSpeciesCollection(includedSpecies);
      modelData.updateSpecies(species);
    }

    if (listOfProteins != null) {
      final List<Pair<String, ? extends CellDesignerSpecies<?>>> species = parser.parseXmlProteinCollection(listOfProteins);
      modelData.updateSpecies(species);
    }
    if (listOfGenes != null) {
      final List<Pair<String, ? extends CellDesignerSpecies<?>>> species = parser.parseXmlGeneCollection(listOfGenes);
      modelData.updateSpecies(species);
    }
    if (listOfRnas != null) {
      final List<Pair<String, ? extends CellDesignerSpecies<?>>> species = parser.parseXmlRnaCollection(listOfRnas);
      modelData.updateSpecies(species);
    }
    if (listOfAntisenseRnas != null) {
      final List<Pair<String, ? extends CellDesignerSpecies<?>>> species = parser
          .parseXmlAntisenseRnaCollection(listOfAntisenseRnas);
      modelData.updateSpecies(species);
    }

    for (final CellDesignerSpecies<?> species : modelData.getAll()) {
      if (!species.getElementId().equals("")) {
        elements.addElement(species);
      } else {
        logger.warn("Species (class: " + species.getClass().getName() + ", name: " + species.getName()
            + ") exists in CD file, but is never instantiated. It's CellDesigner file problem.");
      }
    }

    if (listOfComplexSpeciesAlias != null) {
      final List<Complex> aliases = aliasCollectionParser.parseXmlComplexAliasCollection(listOfComplexSpeciesAlias);
      for (final Element alias : aliases) {
        rap.processNotes(alias);
        model.addElement(alias);
      }
    }

    if (listofSpeciesAlias != null) {
      final List<Species> aliases = aliasCollectionParser.parseXmlSpeciesAliasCollection(listofSpeciesAlias);
      for (final Element alias : aliases) {
        rap.processNotes(alias);
        model.addElement(alias);
      }
    }

    if (includedSpecies != null) {
      parseAnnotationAliasesConnections(model, XmlParser.getNode("celldesigner:listOfSpeciesAliases", nodes));
      parseAnnotationComplexAliasesConnections(model,
          XmlParser.getNode("celldesigner:listOfComplexSpeciesAliases", nodes));
    }

    if (listOfGroups != null) {
      layerParser.parseGroups(model, listOfGroups);
    }
  }

  /**
   * Parses celldesigner:listOfComplexSpeciesAliases node for annotation part of
   * the CellDEsigner format.
   *
   * @param model     model that is parsed
   * @param aliasNode node to parse
   * @throws InvalidXmlSchemaException thrown when xml node contains data that is not supported by xml
   *                                   schema
   */
  void parseAnnotationComplexAliasesConnections(final Model model, final Node aliasNode) throws InvalidXmlSchemaException {
    final NodeList nodes = aliasNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      final Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:complexSpeciesAlias")) {
          final String aliasId = XmlParser.getNodeAttr("id", node);
          final String complexId = XmlParser.getNodeAttr("complexSpeciesAlias", node);
          final Complex alias = model.getElementByElementId(aliasId);
          if (alias == null) {
            throw new InvalidXmlSchemaException("Alias does not exist " + aliasId);
          }
          final Complex parentComplex = model.getElementByElementId(complexId);
          if (parentComplex != null) {
            parentComplex.addSpecies(alias);
            alias.setComplex(parentComplex);
            parentComplex.setNameVerticalAlign(VerticalAlign.BOTTOM);
          }
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:listOfComplexSpeciesAliases: " + node.getNodeName());
        }
      }
    }

  }

  /**
   * Parses celldesigner:listOfSpeciesAliases node for annotation part of the
   * CellDesigner format.
   *
   * @param model     model that is parsed
   * @param aliasNode node to parse
   * @throws InvalidXmlSchemaException thrown when xml node contains data that is not supported by xml
   *                                   schema
   */
  void parseAnnotationAliasesConnections(final Model model, final Node aliasNode) throws InvalidXmlSchemaException {
    final NodeList nodes = aliasNode.getChildNodes();
    for (int x = 0; x < nodes.getLength(); x++) {
      final Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:speciesAlias")) {
          final String aliasId = XmlParser.getNodeAttr("id", node);
          final String complexId = XmlParser.getNodeAttr("complexSpeciesAlias", node);
          final Element alias = model.getElementByElementId(aliasId);
          if (alias == null) {
            throw new InvalidXmlSchemaException("Alias does not exist " + aliasId);
          } else if (alias instanceof Species) {
            final Species species = ((Species) alias);
            final Complex complex = model.getElementByElementId(complexId);
            if (complex != null) {
              species.setComplex(complex);
              complex.addSpecies(species);
              complex.setNameVerticalAlign(VerticalAlign.BOTTOM);
            }
          }
        } else {
          throw new InvalidXmlSchemaException(
              "Unknown element of celldesigner:listOfSpeciesAliases: " + node.getNodeName());
        }
      }
    }
  }

  /**
   * Transforms model into CellDesigner xml.
   *
   * @param model model that should be transformed
   * @return CellDesigner xml string for the model
   * @throws InconsistentModelException thrown when then model is invalid
   */
  public String model2Xml(final Model model, final boolean appendWarnings) throws InconsistentModelException {
    final MinervaLoggerAppender appender = MinervaLoggerAppender.createAppender();
    try {
      final CellDesignerElementCollection elements = new CellDesignerElementCollection();

      final SpeciesCollectionXmlParser speciesCollectionXmlParser = new SpeciesCollectionXmlParser(elements,
          model.getUnits());
      final ReactionCollectionXmlParser reactionCollectionXmlParser = new ReactionCollectionXmlParser(model, elements, false,
          oldCellDesignerVersion);
      final UnitCollectionXmlParser unitCollectionXmlParser = new UnitCollectionXmlParser();
      final FunctionCollectionXmlParser functionCollectionXmlParser = new FunctionCollectionXmlParser();
      final ParameterCollectionXmlParser parameterCollectionXmlParser = new ParameterCollectionXmlParser(model);

      aliasCollectionParser = new AliasCollectionXmlParser(elements, model);

      final StringBuilder result = new StringBuilder();
      result.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
      result.append("<sbml xmlns=\"http://www.sbml.org/sbml/level2/version4\" "
          + "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" level=\"2\" version=\"4\">\n");
      result.append("<model ");
      if (model.getIdModel() != null) {
        final String id = elements.getModelId(model);
        if (!id.equals(model.getIdModel())) {
          logger.warn("Invalid map id: '" + model.getIdModel() + "' changed to '" + id + "'");
        }
        result.append("metaid=\"" + id + "\" id=\"" + id + "\"");
      }
      result.append(">\n");

      result.append(functionCollectionXmlParser.toXml(model.getFunctions()));
      result.append(unitCollectionXmlParser.toXml(model.getUnits()));
      // notes must be inserted here, but we don't know the warnings yet
      final int notesPosition = result.length();
      result.append(annotationToXml(model, elements));

      compartmentCollectionXmlParser = new CompartmentCollectionXmlParser(elements);

      result.append(compartmentCollectionXmlParser.toXml(model.getCompartments()));

      result.append(speciesCollectionXmlParser.speciesCollectionToSbmlString(model.getSpeciesList()));

      result.append(parameterCollectionXmlParser.toXml(model.getParameters()));
      result.append(reactionCollectionXmlParser.reactionCollectionToXmlString(model.getReactions()));

      if (model.getNotes() != null || !appender.getWarnings().isEmpty()) {
        final StringBuilder notes = new StringBuilder();
        notes.append("<notes>");
        notes.append("<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><title/></head><body>");
        if (model.getNotes() != null) {
          notes.append(new CommonXmlParser().getNotesXmlContent(model.getNotes()));
        }
        if (appendWarnings) {
          for (final String entry : new LogFormatter().createFormattedWarnings(appender.getWarnings())) {
            notes.append("\n" + entry);
          }
        }
        notes.append("</body></html>");
        notes.append("</notes>");
        result.insert(notesPosition, notes);
      }

      result.append("</model>");
      result.append("</sbml>");
      return result.toString();
    } finally {
      MinervaLoggerAppender.unregisterLogEventStorage(appender);
    }
  }

  /**
   * Generates xml node that should be in annotation part of the model.
   *
   * @param model    model to transform
   * @param elements collection of {@link CellDesignerElement cell designer elements}
   *                 parsed from xml
   * @return annotation xml string for the model
   */
  private String annotationToXml(final Model model, final CellDesignerElementCollection elements) {
    final List<Protein> proteins = new ArrayList<>();

    final SpeciesCollectionXmlParser speciesCollectionXmlParser = new SpeciesCollectionXmlParser(elements, model.getUnits());

    final StringBuilder result = new StringBuilder();
    result.append("<annotation>\n");
    result.append("<celldesigner:extension>\n");
    result.append("<celldesigner:modelVersion>4.0</celldesigner:modelVersion>\n");
    result.append("<celldesigner:modelDisplay sizeX=\"" + (int) (model.getWidth()) + "\" sizeY=\""
        + (int) (model.getHeight()) + "\"/>\n");

    result.append(speciesCollectionXmlParser.speciesCollectionToXmlIncludedString(model.getSpeciesList()));

    result.append(aliasCollectionParser.compartmentAliasCollectionToXmlString(model.getCompartments()));
    result.append(aliasCollectionParser.complexAliasCollectionToXmlString(model.getComplexList()));
    result.append(aliasCollectionParser.speciesAliasCollectionToXmlString(model.getNotComplexSpeciesList()));

    final List<Gene> genes = new ArrayList<>();
    final List<Rna> rnas = new ArrayList<>();
    final List<AntisenseRna> antisenseRnas = new ArrayList<>();
    for (final Element element : model.getSortedElements()) {
      if (element instanceof Protein) {
        proteins.add((Protein) element);
      } else if (element instanceof Gene) {
        genes.add((Gene) element);
      } else if (element instanceof AntisenseRna) {
        antisenseRnas.add((AntisenseRna) element);
      } else if (element instanceof Rna) {
        rnas.add((Rna) element);
      }
    }

    result.append(speciesCollectionXmlParser.proteinCollectionToXmlString(proteins));
    result.append(speciesCollectionXmlParser.geneCollectionToXmlString(genes));
    result.append(speciesCollectionXmlParser.rnaCollectionToXmlString(rnas));
    result.append(speciesCollectionXmlParser.antisenseRnaCollectionToXmlString(antisenseRnas));
    result.append(layerParser.layerCollectionToXml(model.getLayers()));

    result.append("</celldesigner:extension>\n");

    final XmlAnnotationParser xmlAnnotationParser = new XmlAnnotationParser(
        CommonXmlParser.RELATION_TYPES_SUPPORTED_BY_CELL_DESIGNER);
    result.append(xmlAnnotationParser.dataSetToXmlString(model.getMiriamData(), model.getAuthors(),
        model.getCreationDate(), model.getModificationDates(), model.getIdModel()));
    result.append("</annotation>\n");
    return result.toString();
  }
}
