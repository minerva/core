/**
 * This package contains parsers used for parsing CellDesiger xml for
 * {@link lcsb.mapviewer.db.model.map.species.CellDesignerSpecies Species}.
 */
package lcsb.mapviewer.converter.model.celldesigner.species;
