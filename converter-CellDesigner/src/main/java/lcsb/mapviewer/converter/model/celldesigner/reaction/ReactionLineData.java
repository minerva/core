package lcsb.mapviewer.converter.model.celldesigner.reaction;

import java.util.HashMap;
import java.util.Map;

import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.CatalysisReaction;
import lcsb.mapviewer.model.map.reaction.type.DissociationReaction;
import lcsb.mapviewer.model.map.reaction.type.HeterodimerAssociationReaction;
import lcsb.mapviewer.model.map.reaction.type.InhibitionReaction;
import lcsb.mapviewer.model.map.reaction.type.KnownTransitionOmittedReaction;
import lcsb.mapviewer.model.map.reaction.type.ModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.NegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.PhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.PositiveInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedPhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedTriggerReaction;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.reaction.type.TranscriptionReaction;
import lcsb.mapviewer.model.map.reaction.type.TranslationReaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.reaction.type.TriggerReaction;
import lcsb.mapviewer.model.map.reaction.type.TruncationReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownCatalysisReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownInhibitionReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownNegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownPositiveInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownReducedModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownReducedPhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownReducedTriggerReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownTransitionReaction;

/**
 * This class represent graphical information about lines/arrows that are
 * related to CellDesigner {@link Reaction} type.
 * 
 * @author Piotr Gawron
 * 
 */
public enum ReactionLineData {

  /**
   * Information about arrows/lines for {@link CatalysisReaction} class.
   */
  CATALYSIS(CatalysisReaction.class, "CATALYSIS", LineType.SOLID, ArrowType.CIRCLE),

  /**
   * Information about arrows/lines for {@link DissociationReaction} class.
   */
  DISSOCIATION(DissociationReaction.class, "DISSOCIATION"),

  /**
   * Information about arrows/lines for {@link HeterodimerAssociationReaction}
   * class.
   */
  HETERODIMER_ASSOCIATION(HeterodimerAssociationReaction.class, "HETERODIMER_ASSOCIATION"),

  /**
   * Information about arrows/lines for {@link InhibitionReaction} class.
   */
  INHIBITION(InhibitionReaction.class, "INHIBITION", LineType.SOLID, ArrowType.CROSSBAR, 3),

  /**
   * Information about arrows/lines for {@link KnownTransitionOmittedReaction}
   * class.
   */
  KNOWN_TRANSITION_OMITTED(KnownTransitionOmittedReaction.class, "KNOWN_TRANSITION_OMITTED"),

  /**
   * Information about arrows/lines for {@link ModulationReaction} class.
   */
  MODULATION(ModulationReaction.class, "MODULATION", LineType.SOLID, ArrowType.DIAMOND),

  /**
   * Information about arrows/lines for {@link NegativeInfluenceReaction} class.
   */
  NEGATIVE_INFLUENCE(NegativeInfluenceReaction.class, "NEGATIVE_INFLUENCE", LineType.SOLID, ArrowType.CROSSBAR, 3),

  /**
   * Information about arrows/lines for {@link PhysicalStimulationReaction} class.
   */
  PHYSICAL_STIMULATION(PhysicalStimulationReaction.class, "PHYSICAL_STIMULATION", LineType.SOLID, ArrowType.BLANK),

  /**
   * Information about arrows/lines for {@link PositiveInfluenceReaction} class.
   */
  POSITIVE_INFLUENCE(PositiveInfluenceReaction.class, "POSITIVE_INFLUENCE", LineType.SOLID, ArrowType.OPEN),

  /**
   * Information about arrows/lines for {@link ReducedModulationReaction} class.
   */
  REDUCED_MODULATION(ReducedModulationReaction.class, "REDUCED_MODULATION", LineType.SOLID, ArrowType.DIAMOND),

  /**
   * Information about arrows/lines for {@link ReducedPhysicalStimulationReaction}
   * class.
   */
  REDUCED_PHYSICAL_STIMULATION(ReducedPhysicalStimulationReaction.class, "REDUCED_PHYSICAL_STIMULATION", LineType.SOLID,
      ArrowType.BLANK),

  /**
   * Information about arrows/lines for {@link ReducedTriggerReaction} class.
   */
  REDUCED_TRIGGER(ReducedTriggerReaction.class, "REDUCED_TRIGGER", LineType.SOLID, ArrowType.BLANK_CROSSBAR),

  /**
   * Information about arrows/lines for {@link StateTransitionReaction} class.
   */
  STATE_TRANSITION(StateTransitionReaction.class, "STATE_TRANSITION"),

  /**
   * Information about arrows/lines for {@link TranscriptionReaction} class.
   */
  TRANSCRIPTION(TranscriptionReaction.class, "TRANSCRIPTION", LineType.DASH_DOT_DOT),

  /**
   * Information about arrows/lines for {@link TranslationReaction} class.
   */
  TRANSLATION(TranslationReaction.class, "TRANSLATION", LineType.DASH_DOT),

  /**
   * Information about arrows/lines for {@link TransportReaction} class.
   */
  TRANSPORT(TransportReaction.class, "TRANSPORT", LineType.SOLID, ArrowType.FULL_CROSSBAR),

  /**
   * Information about arrows/lines for {@link TriggerReaction} class.
   */
  TRIGGER(TriggerReaction.class, "TRIGGER", LineType.SOLID, ArrowType.BLANK_CROSSBAR),

  /**
   * Information about arrows/lines for {@link TruncationReaction} class.
   */
  TRUNCATION(TruncationReaction.class, "TRUNCATION"),

  /**
   * Information about arrows/lines for {@link UnknownCatalysisReaction} class.
   */
  UNKNOWN_CATALYSIS(UnknownCatalysisReaction.class, "UNKNOWN_CATALYSIS", LineType.DASHED, ArrowType.CIRCLE),

  /**
   * Information about arrows/lines for {@link UnknownInhibitionReaction} class.
   */
  UNKNOWN_INHIBITION(UnknownInhibitionReaction.class, "UNKNOWN_INHIBITION", LineType.DASHED, ArrowType.CROSSBAR, 3),

  /**
   * Information about arrows/lines for {@link UnknownNegativeInfluenceReaction}
   * class.
   */
  UNKNOWN_NEGATIVE_INFLUENCE(UnknownNegativeInfluenceReaction.class, "UNKNOWN_NEGATIVE_INFLUENCE", LineType.DASHED,
      ArrowType.CROSSBAR, 3),

  /**
   * Information about arrows/lines for {@link UnknownPositiveInfluenceReaction}
   * class.
   */
  UNKNOWN_POSITIVE_INFLUENCE(UnknownPositiveInfluenceReaction.class, "UNKNOWN_POSITIVE_INFLUENCE", LineType.DASHED,
      ArrowType.OPEN),

  /**
   * Information about arrows/lines for {@link UnknownReducedModulationReaction}
   * class.
   */
  UNKNOWN_REDUCED_MODEULATION(UnknownReducedModulationReaction.class, "UNKNOWN_REDUCED_MODULATION", LineType.DASHED,
      ArrowType.DIAMOND),

  /**
   * Information about arrows/lines for
   * {@link UnknownReducedPhysicalStimulationReaction} class.
   */
  UNKNOWN_REDUCED_PHYSICAL_STIMULATION(UnknownReducedPhysicalStimulationReaction.class,
      "UNKNOWN_REDUCED_PHYSICAL_STIMULATION", LineType.DASHED,
      ArrowType.BLANK),

  /**
   * Information about arrows/lines for {@link UnknownReducedTriggerReaction}
   * class.
   */
  UNKNOWN_REDUCED_TRIGGER(UnknownReducedTriggerReaction.class, "UNKNOWN_REDUCED_TRIGGER", LineType.DASHED,
      ArrowType.BLANK_CROSSBAR),

  /**
   * Information about arrows/lines for {@link UnknownTransitionReaction} class.
   */
  UNKNOWN_TRANSITION(UnknownTransitionReaction.class, "UNKNOWN_TRANSITION");

  /**
   * Map used for reverse lookup between {@link Reaction} types and
   * {@link ReactionLineData}.
   */
  private static Map<Class<? extends Reaction>, ReactionLineData> map = null;
  /**
   * Map used for reverse lookup between {@link #cellDesignerString} types and
   * {@link ReactionLineData}.
   */
  private static Map<String, ReactionLineData> cellDesignerMap = null;
  /**
   * {@link Reaction} class for which the data is stored.
   */
  private Class<? extends Reaction> reactionClass;
  /**
   * Type of arrow that should be used by products.
   */
  private ArrowType productArrowType;
  /**
   * Line types that should be used by the reaction.
   */
  private LineType lineType;
  /**
   * How much product line should be trimmed at the end.
   */
  private double productLineTrim;
  /**
   * String used by CellDesigner to represent reaction.
   */
  private String cellDesignerString;

  /**
   *
   * @param clazz
   *          {@link #reactionClass}
   * @param cellDesignerString
   *          {@link #cellDesignerString}
   */
  ReactionLineData(final Class<? extends Reaction> clazz, final String cellDesignerString) {
    this(clazz, cellDesignerString, LineType.SOLID);

  }

  /**
   *
   * @param clazz
   *          {@link #reactionClass}
   * @param cellDesignerString
   *          {@link #cellDesignerString}
   * @param lineType
   *          {@link #lineType}
   */
  ReactionLineData(final Class<? extends Reaction> clazz, final String cellDesignerString, final LineType lineType) {
    this(clazz, cellDesignerString, lineType, ArrowType.FULL);
  }

  /**
   *
   * @param clazz
   *          {@link #reactionClass}
   * @param cellDesignerString
   *          {@link #cellDesignerString}
   * @param lineType
   *          {@link #lineType}
   * @param productArrowType
   *          {@link #productArrowType}
   */
  ReactionLineData(final Class<? extends Reaction> clazz, final String cellDesignerString, final LineType lineType,
      final ArrowType productArrowType) {
    this(clazz, cellDesignerString, lineType, productArrowType, 0.0);
  }

  /**
   *
   * @param clazz
   *          {@link #reactionClass}
   * @param cellDesignerString
   *          {@link #cellDesignerString}
   * @param lineType
   *          {@link #lineType}
   * @param productArrowType
   *          {@link #productArrowType}
   * @param productLineTrim
   *          {@link #productLineTrim}
   */
  ReactionLineData(final Class<? extends Reaction> clazz, final String cellDesignerString, final LineType lineType,
      final ArrowType productArrowType, final double productLineTrim) {
    this.reactionClass = clazz;
    this.lineType = lineType;
    this.productArrowType = productArrowType;
    this.productLineTrim = productLineTrim;
    this.cellDesignerString = cellDesignerString;
  }

  /**
   * Method that finds {@link ReactionLineData} or given {@link Reaction} type.
   *
   * @param clazz
   *          type for which {@link ReactionLineData} will be returned
   * @return {@link ReactionLineData} or given {@link Reaction} type
   */
  public static ReactionLineData getByReactionType(final Class<? extends Reaction> clazz) {
    if (map == null) {
      Map<Class<? extends Reaction>, ReactionLineData> result = new HashMap<>();
      for (final ReactionLineData rld : values()) {
        result.put(rld.getReactionClass(), rld);
      }
      map = result;
    }
    return map.get(clazz);
  }

  /**
   * Returns {@link ReactionLineData} identified by CellDEsigner string that
   * identifies this Reaction type.
   *
   * @param type
   *          {@link #cellDesignerString}
   * @return {@link ReactionLineData} identified by CellDEsigner string that
   *         identifies this Reaction type
   */
  public static ReactionLineData getByCellDesignerString(final String type) {
    if (cellDesignerMap == null) {
      Map<String, ReactionLineData> result = new HashMap<>();
      for (final ReactionLineData rld : values()) {
        result.put(rld.getCellDesignerString(), rld);
      }
      cellDesignerMap = result;
    }
    return cellDesignerMap.get(type);
  }

  /**
   * Returns {@link ReactionLineData} that can be applied for lines with given
   * line type and arrow type. If there are more then one possibility, one of them
   * will be returned (nothing is guaranteed).
   *
   * @param lineType
   *          type of the line for which are looking for {@link ReactionLineData}
   * @param productArrowType
   *          type of the arrow for which are looking for {@link ReactionLineData}
   * @return {@link ReactionLineData} that can be applied for lines with given
   *         line type and arrow type. If there are more then one possibility, one
   *         of them will be returned (nothing is guaranteed)
   */
  public static ReactionLineData getByLineType(final LineType lineType, final ArrowType productArrowType) {
    for (final ReactionLineData rld : values()) {
      if (rld.getLineType().equals(lineType) && rld.getProductArrowType().equals(productArrowType)) {
        return rld;
      }
    }
    return null;
  }

  /**
   * @return the reactionClass
   * @see #reactionClass
   */
  public Class<? extends Reaction> getReactionClass() {
    return reactionClass;
  }

  /**
   * @return the productArrowType
   * @see #productArrowType
   */
  public ArrowType getProductArrowType() {
    return productArrowType;
  }

  /**
   * @return the lineType
   * @see #lineType
   */
  public LineType getLineType() {
    return lineType;
  }

  /**
   * @return the productLineTrim
   * @see #productLineTrim
   */
  public double getProductLineTrim() {
    return productLineTrim;
  }

  /**
   * @return the cellDesignerString
   * @see #cellDesignerString
   */
  public String getCellDesignerString() {
    return cellDesignerString;
  }

  /**
   * Creates instance of {@link Reaction} represented by this enum and copies the
   * data from result parameter.
   *
   * @param result
   *          original data
   * @return instance of {@link Reaction} represented by this enum and copies the
   *         data from result parameter
   */
  public Reaction createReaction(final Reaction result) throws ReactionParserException {
    try {
      return reactionClass.getConstructor(Reaction.class).newInstance(result);
    } catch (final Exception e) {
      throw new ReactionParserException("Problem with creation the new instance of reaction. ", result, e);
    }
  }

}
