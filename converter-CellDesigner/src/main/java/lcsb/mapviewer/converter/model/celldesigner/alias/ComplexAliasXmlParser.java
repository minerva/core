package lcsb.mapviewer.converter.model.celldesigner.alias;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerComplexSpecies;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.View;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.HashMap;
import java.util.Map;

/**
 * Parser of CellDesigner xml used for parsing complex aliases. Important: Only
 * one instance per model should be used.
 *
 * @author Piotr Gawron
 * @see Complex
 */
public class ComplexAliasXmlParser extends AbstractAliasXmlParser<Complex> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger();

  /**
   * Because of the CellDesigner xml model we have to store information about all
   * processed Complexes. This information later on is used for connecting
   * complexes in hierarchical view.
   */
  private final Map<String, Complex> complexAliasesMapById = new HashMap<String, Complex>();

  /**
   * Because of the CellDesigner xml model we have to store information about
   * parents of all Complexes. This information later on is used for connecting
   * complexes in hierarchical view. We cannot do it immediately because some
   * complexes doesn't exist yet.
   */
  private final Map<String, String> parents = new HashMap<String, String>();

  /**
   * Model for which we are parsing aliases.
   */
  private Model model = null;

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed from
   * xml.
   */
  private final CellDesignerElementCollection elements;

  /**
   * Default constructor with model object for which we parse data.
   *
   * @param model    model for which we parse elements
   * @param elements collection of {@link CellDesignerElement cell designer elements}
   *                 parsed from xml
   */
  public ComplexAliasXmlParser(final CellDesignerElementCollection elements, final Model model) {
    this.model = model;
    this.elements = elements;
  }

  @Override
  Complex parseXmlAlias(final Node aliasNode) throws InvalidXmlSchemaException {

    String aliasId = XmlParser.getNodeAttr("id", aliasNode);

    String speciesId = XmlParser.getNodeAttr("species", aliasNode);
    CellDesignerComplexSpecies species = elements.getElementByElementId(speciesId);
    if (species == null) {
      throw new InvalidXmlSchemaException(
          "No species with id=\"" + speciesId + "\" for complex alias \"" + aliasId + "\"");
    }
    Complex result = species.createModelElement(aliasId);
    elements.addElement(species, aliasId);

    String state = "usual";
    NodeList nodes = aliasNode.getChildNodes();
    View usualView = null;
    View briefView = null;
    for (int x = 0; x < nodes.getLength(); x++) {
      Node node = nodes.item(x);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equalsIgnoreCase("celldesigner:activity")) {
          result.setActivity(XmlParser.getNodeValue(node).equalsIgnoreCase("active"));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:bounds")) {
          result.setX(XmlParser.getNodeAttr("X", node));
          result.setY(XmlParser.getNodeAttr("Y", node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:font")) {
          result.setFontSize(XmlParser.getNodeAttr("size", node));
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:view")) {
          state = XmlParser.getNodeAttr("state", node);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:usualView")) {
          usualView = getCommonParser().getView(node);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:briefView")) {
          briefView = getCommonParser().getView(node);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:backupView")) {
          // not handled
          continue;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:info")) {
          processAliasState(node, result);
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:backupSize")) {
          // not handled
          continue;
        } else if (node.getNodeName().equalsIgnoreCase("celldesigner:structuralState")) {
          species.setStructuralStateAngle(Double.parseDouble(XmlParser.getNodeAttr("angle", node)));
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:speciesAlias: " + node.getNodeName());
        }
      }
    }

    View view = null;
    if (state.equalsIgnoreCase("usual")) {
      view = usualView;
    } else if (state.equalsIgnoreCase("brief")) {
      view = usualView;
    } else if (state.equalsIgnoreCase("complexnoborder")) {
      view = usualView;
    } else if (state.equalsIgnoreCase("complexparentbrief")) {
      view = briefView;
    } else {
      throw new InvalidXmlSchemaException(
          new ElementUtils().getElementTag(result) + "Don't know how to process state: " + state);
    }

    if (view != null) {
      // inner position defines the position in compartment or complexAlias
      // result.moveBy(view.innerPosition);
      result.setWidth(view.getBoxSize().getWidth());
      result.setHeight(view.getBoxSize().getHeight());
      result.setLineWidth(view.getSingleLine().getWidth());
      result.setFillColor(view.getColor());
      if (view.getInnerPosition() != null && state.equalsIgnoreCase("brief")) {
        result.setX(result.getX() + view.getInnerPosition().getX());
        result.setY(result.getY() + view.getInnerPosition().getY());
      }
    } else if (!state.equalsIgnoreCase("complexnoborder")) {
      throw new InvalidXmlSchemaException("No view (" + state + ") in ComplexAlias for " + result.getElementId());
    }
    result.setState(state);
    String compartmentAliasId = XmlParser.getNodeAttr("compartmentAlias", aliasNode);
    if (!compartmentAliasId.isEmpty()) {
      Compartment ca = model.getElementByElementId(compartmentAliasId);
      if (ca == null) {
        throw new InvalidXmlSchemaException("CompartmentAlias does not exist: " + compartmentAliasId);
      } else {
        result.setCompartment(ca);
        ca.addElement(result);
      }
    }
    String complexSpeciesAliasId = XmlParser.getNodeAttr("complexSpeciesAlias", aliasNode);
    if (!complexSpeciesAliasId.isEmpty()) {
      parents.put(result.getElementId(), complexSpeciesAliasId);
    }
    complexAliasesMapById.put(result.getElementId(), result);
    species.updateModelElementAfterLayoutAdded(result);
    result.setNameX(result.getX());
    result.setNameY(result.getY());
    result.setNameWidth(result.getWidth());
    result.setNameHeight(result.getHeight());
    result.setNameHorizontalAlign(HorizontalAlign.CENTER);
    result.setNameVerticalAlign(VerticalAlign.MIDDLE);
    return result;
  }

  @Override
  public String toXml(final Complex complex) {
    Compartment ca = null;
    // we have to exclude artificial compartment aliases, because they aren't
    // exported to CellDesigner file
    if (complex.getCompartment() != null && !(complex.getCompartment() instanceof PathwayCompartment)) {
      ca = complex.getCompartment();
    } else if (complex.getComplex() == null) {
      ModelData modelData = complex.getModelData();
      if (modelData != null) {
        for (final Compartment compartment : modelData.getModel().getCompartments()) {
          if (!(compartment instanceof PathwayCompartment) && compartment.cross(complex)) {
            if (ca == null) {
              ca = compartment;
            } else if (ca.getSize() > compartment.getSize()) {
              ca = compartment;
            }
          }
        }
      }
    }

    Complex complexAlias = complex.getComplex();

    String compartmentAliasId = elements.getCompartmentAliasId(ca);

    StringBuilder sb = new StringBuilder();
    sb.append("<celldesigner:complexSpeciesAlias ");
    sb.append("id=\"").append(complex.getElementId()).append("\" ");
    sb.append("species=\"").append(elements.getElementId(complex)).append("\" ");
    if (compartmentAliasId != null) {
      sb.append("compartmentAlias=\"").append(compartmentAliasId).append("\" ");
    }
    if (complexAlias != null) {
      sb.append("complexSpeciesAlias=\"").append(complexAlias.getElementId()).append("\"");
    }
    sb.append(">\n");

    if (complex.getActivity() != null) {
      if (complex.getActivity()) {
        sb.append("<celldesigner:activity>active</celldesigner:activity>\n");
      } else {
        sb.append("<celldesigner:activity>inactive</celldesigner:activity>\n");
      }
    }

    sb.append("<celldesigner:bounds ");
    sb.append("x=\"").append(complex.getX()).append("\" ");
    sb.append("y=\"").append(complex.getY()).append("\" ");
    sb.append("w=\"").append(complex.getWidth()).append("\" ");
    sb.append("h=\"").append(complex.getHeight()).append("\" ");
    sb.append("/>\n");

    sb.append(createFontTag(complex));

    sb.append("<celldesigner:view state=\"usual\"/>\n");
    sb.append("<celldesigner:usualView>");
    sb.append("<celldesigner:innerPosition x=\"0\" y=\"0\"/>");
    sb.append("<celldesigner:boxSize width=\"").append(complex.getWidth()).append("\" height=\"").append(complex.getHeight()).append("\"/>");
    sb.append("<celldesigner:singleLine width=\"").append(complex.getLineWidth()).append("\"/>");
    sb.append("<celldesigner:paint color=\"").append(XmlParser.colorToString(complex.getFillColor())).append("\" scheme=\"Color\"/>");
    sb.append("</celldesigner:usualView>\n");
    sb.append("<celldesigner:briefView>");
    sb.append("<celldesigner:innerPosition x=\"0\" y=\"0\"/>");
    sb.append("<celldesigner:boxSize width=\"").append(complex.getWidth()).append("\" height=\"").append(complex.getHeight()).append("\"/>");
    sb.append("<celldesigner:singleLine width=\"").append(complex.getWidth()).append("\"/>");
    sb.append("<celldesigner:paint color=\"").append(XmlParser.colorToString(complex.getFillColor())).append("\" scheme=\"Color\"/>");
    sb.append("</celldesigner:briefView>\n");

    sb.append(createStructuralStateTag(complex));
    sb.append("</celldesigner:complexSpeciesAlias>\n");
    return sb.toString();
  }

  /**
   * Process node with information about alias state and puts data into alias.
   *
   * @param node  node where information about alias state is stored
   * @param alias alias object to be modified if necessary
   */
  private void processAliasState(final Node node, final Species alias) {
    String state = XmlParser.getNodeAttr("state", node);
    if ("open".equalsIgnoreCase(state)) {
      String prefix = XmlParser.getNodeAttr("prefix", node);
      String label = XmlParser.getNodeAttr("label", node);
      alias.setStatePrefix(prefix);
      alias.setStateLabel(label);
    } else if (!"empty".equalsIgnoreCase(state) && state != null && !state.isEmpty()) {
      throw new NotImplementedException("[Alias: " + alias.getElementId() + "] Unknown alias state: " + state);
    }

  }

  /**
   * Adds parent reference for the complexAlias.
   *
   * @param alias alias for which we want to add parent information
   */
  public void addReference(final Complex alias) {
    String parentId = parents.get(alias.getElementId());
    if (parentId != null) {
      Complex ca = complexAliasesMapById.get(parentId);
      if (ca == null) {
        throw new InvalidArgumentException(
            "Parent complex alias does not exist: " + parentId + " for alias: " + alias.getElementId());
      } else {
        alias.setComplex(ca);
        ca.setNameVerticalAlign(VerticalAlign.BOTTOM);
        ca.addSpecies(alias);
      }
    }
  }
}
