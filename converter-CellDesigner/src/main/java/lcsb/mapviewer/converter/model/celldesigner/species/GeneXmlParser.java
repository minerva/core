package lcsb.mapviewer.converter.model.celldesigner.species;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerElementCollection;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerElement;
import lcsb.mapviewer.converter.model.celldesigner.structure.CellDesignerGene;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.CellDesignerModificationResidue;
import lcsb.mapviewer.converter.model.celldesigner.structure.fields.ModificationType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Class that performs parsing of the CellDesigner xml for
 * {@link CellDesignerGene} object.
 *
 * @author Piotr Gawron
 */
public class GeneXmlParser extends AbstractElementXmlParser<CellDesignerGene, Gene> {

  /**
   * Collection of {@link CellDesignerElement cell designer elements} parsed from
   * xml.
   */
  private final CellDesignerElementCollection elements;

  private final ModificationResidueXmlParser modificationResidueXmlParser;

  /**
   * Default constructor. Model is required because some nodes require access to
   * other parts of the model.
   *
   * @param elements collection of {@link CellDesignerElement cell designer elements}
   *                 parsed from xml
   */
  public GeneXmlParser(final CellDesignerElementCollection elements) {
    this.elements = elements;
    this.modificationResidueXmlParser = new ModificationResidueXmlParser(elements);
  }

  @Override
  public Pair<String, CellDesignerGene> parseXmlElement(final Node geneNode) throws InvalidXmlSchemaException {
    final CellDesignerGene gene = new CellDesignerGene();
    final String identifier = XmlParser.getNodeAttr("id", geneNode);
    gene.setName(decodeName(XmlParser.getNodeAttr("name", geneNode)));
    final NodeList list = geneNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      final Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        if (node.getNodeName().equals("celldesigner:notes")) {
          gene.setNotes(getRap().getNotes(node));
        } else if (node.getNodeName().equals("celldesigner:listOfRegions")) {
          final NodeList residueList = node.getChildNodes();
          for (int j = 0; j < residueList.getLength(); j++) {
            final Node residueNode = residueList.item(j);
            if (residueNode.getNodeType() == Node.ELEMENT_NODE) {
              if (residueNode.getNodeName().equalsIgnoreCase("celldesigner:region")) {
                gene.addModificationResidue(getModificationResidue(residueNode));
              } else {
                throw new InvalidXmlSchemaException(
                    "Unknown element of celldesigner:listOfRegions " + residueNode.getNodeName());
              }
            }
          }
        } else {
          throw new InvalidXmlSchemaException("Unknown element of celldesigner:gene " + node.getNodeName());
        }
      }
    }
    return new Pair<String, CellDesignerGene>(identifier, gene);
  }

  @Override
  public String toXml(final Gene gene) {
    String attributes = " type=\"GENE\" ";
    final StringBuilder result = new StringBuilder();
    attributes += " id=\"g_" + elements.getElementId(gene) + "\"";
    if (!gene.getName().isEmpty()) {
      attributes += " name=\"" + XmlParser.escapeXml(encodeName(gene.getName())) + "\"";
    }
    result.append("<celldesigner:gene").append(attributes).append(">");

    if (!gene.getModificationResidues().isEmpty()) {
      result.append("<celldesigner:listOfRegions>\n");
      for (final ModificationResidue mr : gene.getModificationResidues()) {
        result.append(modificationResidueXmlParser.toXml(mr));
      }
      result.append("</celldesigner:listOfRegions>\n");
    }

    result.append("</celldesigner:gene>");
    return result.toString();
  }

  /**
   * Parse modification for a gene.
   *
   * @param residueNode source xml node
   * @return object representing modification
   * @throws InvalidXmlSchemaException thrown when input xml node doesn't follow defined schema
   */
  CellDesignerModificationResidue getModificationResidue(final Node residueNode) throws InvalidXmlSchemaException {
    final CellDesignerModificationResidue residue = new CellDesignerModificationResidue();
    residue.setIdModificationResidue(XmlParser.getNodeAttr("id", residueNode));
    residue.setName(XmlParser.getNodeAttr("name", residueNode));
    residue.setSide(XmlParser.getNodeAttr("side", residueNode));
    residue.setSize(XmlParser.getNodeAttr("size", residueNode));
    residue.setActive(XmlParser.getNodeAttr("active", residueNode));
    residue.setAngle(XmlParser.getNodeAttr("pos", residueNode));
    final String type = XmlParser.getNodeAttr("type", residueNode);
    try {
      residue.setModificationType(ModificationType.getByCellDesignerName(type));
    } catch (final InvalidArgumentException e) {
      throw new InvalidXmlSchemaException(e);
    }
    final NodeList list = residueNode.getChildNodes();
    for (int i = 0; i < list.getLength(); i++) {
      final Node node = list.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        throw new InvalidXmlSchemaException("Unknown element of celldesigner:region " + node.getNodeName());
      }
    }
    return residue;
  }

}
