#!/bin/bash
result=0

while read filename
do
  html=''
  case $filename in  *.html)
	  html=" --mode html ";;
  esac
  lines=`cat $filename | aspell $html --lang=en --encoding=utf-8 --add-extra-dicts=./.aspell.en.pws --add-extra-dicts=./.aspell-api-exceptions.pws list|wc -l`
  if [ $lines -ne 0 ]
  then
    echo "[$filename] Unknown words:"
    echo
    cat $filename | aspell --lang=en $html --encoding=utf-8 --add-extra-dicts=./.aspell.en.pws --add-extra-dicts=./.aspell-api-exceptions.pws list |sort |uniq
    result=1
  fi
done <<< "$(ls web/target/generated-docs/*.html CHANGELOG -c1)"

exit $result
