package lcsb.mapviewer.converter.model.sbgnml.parser;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbgn.GlyphClazz;
import org.sbgn.bindings.Glyph;

@SuppressWarnings("deprecation")
public abstract class GlyphParser {

  static final Set<GlyphClazz> ELEMENT_GLYPHS;
  static final Set<GlyphClazz> MODIFICATION_GLYPHS;
  static final Set<GlyphClazz> REACTION_NODE_GLYPHS;
  static final Set<GlyphClazz> REACTION_GLYPHS;

  static {
    Set<GlyphClazz> elementGlyphs = new HashSet<>();
    elementGlyphs.add(GlyphClazz.BIOLOGICAL_ACTIVITY);
    elementGlyphs.add(GlyphClazz.COMPARTMENT);
    elementGlyphs.add(GlyphClazz.COMPLEX);
    elementGlyphs.add(GlyphClazz.COMPLEX_MULTIMER);
    elementGlyphs.add(GlyphClazz.EQUIVALENCE);
    elementGlyphs.add(GlyphClazz.ENTITY);
    elementGlyphs.add(GlyphClazz.MACROMOLECULE);
    elementGlyphs.add(GlyphClazz.MACROMOLECULE_MULTIMER);
    elementGlyphs.add(GlyphClazz.NUCLEIC_ACID_FEATURE);
    elementGlyphs.add(GlyphClazz.NUCLEIC_ACID_FEATURE_MULTIMER);
    elementGlyphs.add(GlyphClazz.OBSERVABLE);
    elementGlyphs.add(GlyphClazz.OUTCOME);
    elementGlyphs.add(GlyphClazz.PERTURBING_AGENT);
    elementGlyphs.add(GlyphClazz.PHENOTYPE);
    elementGlyphs.add(GlyphClazz.SIMPLE_CHEMICAL);
    elementGlyphs.add(GlyphClazz.SIMPLE_CHEMICAL_MULTIMER);
    elementGlyphs.add(GlyphClazz.SOURCE_AND_SINK);
    elementGlyphs.add(GlyphClazz.TAG);
    elementGlyphs.add(GlyphClazz.TERMINAL);
    elementGlyphs.add(GlyphClazz.SUBMAP);
    elementGlyphs.add(GlyphClazz.UNSPECIFIED_ENTITY);
    ELEMENT_GLYPHS = Collections.unmodifiableSet(elementGlyphs);

    Set<GlyphClazz> reactionNodeGlyphs = new HashSet<>();
    reactionNodeGlyphs.add(GlyphClazz.AND);
    reactionNodeGlyphs.add(GlyphClazz.NOT);
    reactionNodeGlyphs.add(GlyphClazz.OR);
    reactionNodeGlyphs.add(GlyphClazz.IMPLICIT_XOR);
    reactionNodeGlyphs.add(GlyphClazz.STOICHIOMETRY);
    REACTION_NODE_GLYPHS = Collections.unmodifiableSet(reactionNodeGlyphs);

    Set<GlyphClazz> reactionGlyphs = new HashSet<>();
    reactionGlyphs.add(GlyphClazz.ASSOCIATION);
    reactionGlyphs.add(GlyphClazz.DELAY);
    reactionGlyphs.add(GlyphClazz.DISSOCIATION);
    reactionGlyphs.add(GlyphClazz.EXISTENCE);
    reactionGlyphs.add(GlyphClazz.INTERACTION);
    reactionGlyphs.add(GlyphClazz.LOCATION);
    reactionGlyphs.add(GlyphClazz.OMITTED_PROCESS);
    reactionGlyphs.add(GlyphClazz.PROCESS);
    reactionGlyphs.add(GlyphClazz.PERTURBATION);
    reactionGlyphs.add(GlyphClazz.UNCERTAIN_PROCESS);
    REACTION_GLYPHS = Collections.unmodifiableSet(reactionGlyphs);

    Set<GlyphClazz> modificationGlyphs = new HashSet<>();
    modificationGlyphs.add(GlyphClazz.ANNOTATION);
    modificationGlyphs.add(GlyphClazz.CARDINALITY);
    modificationGlyphs.add(GlyphClazz.STATE_VARIABLE);
    modificationGlyphs.add(GlyphClazz.UNIT_OF_INFORMATION);
    modificationGlyphs.add(GlyphClazz.VARIABLE_VALUE);
    MODIFICATION_GLYPHS = Collections.unmodifiableSet(modificationGlyphs);

  }

  protected Logger logger = LogManager.getLogger();

  public boolean isElementGlyph(final Glyph glyph) {
    return ELEMENT_GLYPHS.contains(GlyphClazz.fromClazz(glyph.getClazz()));
  }

  public boolean isReactionNodeGlyph(final Glyph glyph) {
    return REACTION_NODE_GLYPHS.contains(GlyphClazz.fromClazz(glyph.getClazz()));
  }

  public boolean isReactionGlyph(final Glyph glyph) {
    return REACTION_GLYPHS.contains(GlyphClazz.fromClazz(glyph.getClazz()));
  }

  public boolean isModificationGlyph(final Glyph glyph) {
    return MODIFICATION_GLYPHS.contains(GlyphClazz.fromClazz(glyph.getClazz()));
  }
}
