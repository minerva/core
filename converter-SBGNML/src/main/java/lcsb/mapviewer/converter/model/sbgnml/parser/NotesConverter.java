package lcsb.mapviewer.converter.model.sbgnml.parser;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.converter.model.celldesigner.CommonXmlParser;
import lcsb.mapviewer.converter.model.celldesigner.annotation.RestAnnotationParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbgn.bindings.SBGNBase;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;

public class NotesConverter {

  private final Logger logger = LogManager.getLogger();

  private final RestAnnotationParser rap = new RestAnnotationParser();

  public SBGNBase.Notes createNotesNode(final String notes) {
    SBGNBase.Notes result = new SBGNBase.Notes();
    try {
      final StringBuilder notesXml = new StringBuilder();
      notesXml.append("<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><title/></head><body>");
      if (notes != null) {
        notesXml.append(new CommonXmlParser().getNotesXmlContent(notes));
      }
      notesXml.append("</body></html>");

      final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      dbf.setNamespaceAware(true);
      final DocumentBuilder db;
      db = dbf.newDocumentBuilder();
      final Document doc = db.parse(new ByteArrayInputStream(notesXml.toString().getBytes()));

      result.getAny().add((org.w3c.dom.Element) (doc.getFirstChild()));
    } catch (final Exception e) {
      logger.warn("Problem with processing notes");
    }
    return result;
  }

  public String parseNotes(final SBGNBase.Notes notesNode) {
    if (notesNode != null && !notesNode.getAny().isEmpty()) {
      try {
        String notes = "<notes>" + XmlParser.nodeToString(notesNode.getAny().get(0).getParentNode(), true)
            + "</notes>";
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db;
        db = dbf.newDocumentBuilder();
        Document doc = db.parse(new ByteArrayInputStream(notes.getBytes()));

        return rap.getNotes(doc.getFirstChild());
      } catch (final Exception e) {
        logger.warn("Problem with extracting map notes", e);
      }
    }
    return "";
  }
}
