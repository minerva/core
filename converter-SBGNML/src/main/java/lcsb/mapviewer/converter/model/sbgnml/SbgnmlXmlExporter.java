package lcsb.mapviewer.converter.model.sbgnml;

import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.converter.graphics.bioentity.reaction.ReactionConverter;
import lcsb.mapviewer.converter.model.sbgnml.parser.NotesConverter;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.modifier.Inhibition;
import lcsb.mapviewer.model.map.modifier.Modulation;
import lcsb.mapviewer.model.map.modifier.PhysicalStimulation;
import lcsb.mapviewer.model.map.modifier.Trigger;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.OrOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.reaction.type.CatalysisReaction;
import lcsb.mapviewer.model.map.reaction.type.DissociationReaction;
import lcsb.mapviewer.model.map.reaction.type.HeterodimerAssociationReaction;
import lcsb.mapviewer.model.map.reaction.type.InhibitionReaction;
import lcsb.mapviewer.model.map.reaction.type.KnownTransitionOmittedReaction;
import lcsb.mapviewer.model.map.reaction.type.ModifierReactionNotation;
import lcsb.mapviewer.model.map.reaction.type.ModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.NegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.PhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.PositiveInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedNotation;
import lcsb.mapviewer.model.map.reaction.type.ReducedPhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedTriggerReaction;
import lcsb.mapviewer.model.map.reaction.type.TriggerReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownCatalysisReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownInhibitionReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownNegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownPositiveInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownReducedModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownReducedPhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownReducedTriggerReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownTransitionReaction;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.TruncatedProtein;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.model.map.species.field.AbstractSiteModification;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbgn.ArcClazz;
import org.sbgn.GlyphClazz;
import org.sbgn.Language;
import org.sbgn.RenderUtil;
import org.sbgn.bindings.Arc;
import org.sbgn.bindings.Arc.End;
import org.sbgn.bindings.Arc.Next;
import org.sbgn.bindings.Arc.Start;
import org.sbgn.bindings.Bbox;
import org.sbgn.bindings.Glyph;
import org.sbgn.bindings.Label;
import org.sbgn.bindings.Map;
import org.sbgn.bindings.Port;
import org.sbgn.bindings.RenderInformation;
import org.sbgn.bindings.SBGNBase;
import org.sbgn.bindings.Sbgn;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.awt.Color;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class used to export SBGN-ML files from the model.
 *
 * @author Michał Kuźma
 */
public class SbgnmlXmlExporter {

  /**
   * Side margin for units of information.
   */
  private static final double UNIT_OF_INFORMATION_MARGIN = 10.0;
  /**
   * Height of generated units of information.
   */
  private static final double UNIT_OF_INFORMATION_HEIGHT = 12.0;
  /**
   * Height and width of generated operators.
   */
  private static final double OPERATOR_SIZE = 40.0;
  /**
   * Distance between operator circle and port point.
   */
  private static final double OPERATOR_PORT_DISTANCE = 20.0;
  /**
   * Distance between process glyph and port point.
   */
  private static final double PROCESS_PORT_DISTANCE = 10.0;
  /**
   * Length of random alphabetic string added in the beginning of ID, if it is a
   * number. SBGN-ML doesn't accept numbers as ID.
   */
  private static final int ID_RANDOM_STRING_LENGTH = 5;

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();
  /**
   * Counter of the arcs parsed so far, used in generating arc's id.
   */
  private int arcCounter;
  /**
   * Set of all the operators parsed so far, used in generating operator's id.
   */
  private Set<NodeOperator> parsedOperators;
  /**
   * Map of operator IDs used when parsing arcs targeting an operator.
   */
  private java.util.Map<NodeOperator, String> operatorIds;
  /**
   * Map of all glyphs and ports generated so far with id as key.
   */
  private java.util.Map<String, Object> sourceTargetMap;

  private RenderInformation renderInformation;

  private final ColorParser colorParser = new ColorParser();

  private final NotesConverter notesConverter = new NotesConverter();

  private final XmlAnnotationParser xap = new XmlAnnotationParser(new ArrayList<>(), true);

  private final java.util.Map<Element, String> exportElementIds = new HashMap<>();

  /**
   * Transforms model into SBGN-ML xml.
   *
   * @param model model that should be transformed
   * @return SBGM-ML xml string for the model
   */
  public Sbgn toSbgnml(final Model model) throws ConverterException {
    // Reset global variables
    arcCounter = 0;
    parsedOperators = new HashSet<>();
    operatorIds = new HashMap<>();
    sourceTargetMap = new HashMap<>();
    renderInformation = new RenderInformation();

    final Map map = new Map();
    map.setId(model.getIdModel());
    if (model.getIdModel() == null) {
      map.setId("map_" + model.getId());
    }
    map.setLanguage(Language.PD.getName());
    final Bbox box = new Bbox();
    box.setX(0);
    box.setY(0);
    box.setW((float) model.getWidth());
    box.setH((float) model.getHeight());
    map.setBbox(box);

    final Bbox bbox = new Bbox();
    bbox.setX(0);
    bbox.setY(0);
    bbox.setW((float) model.getWidth());
    bbox.setH((float) model.getHeight());

    map.setBbox(bbox);

    addExtensions(map, model);

    final Sbgn sbgnData = new Sbgn();

    for (final Element element : model.getSortedElements()) {
      if (element instanceof Species) {
        if (((Species) element).getComplex() == null) {
          final Glyph newGlyph = elementToGlyph(element);
          map.getGlyph().add(newGlyph);
        }
      } else if (element instanceof Compartment) {
        final Glyph newGlyph = elementToGlyph(element);
        map.getGlyph().add(newGlyph);
      }
    }
    for (final Species element : model.getSpeciesList()) {
      final Glyph speciesGlyph = ((Glyph) (sourceTargetMap.get(element.getElementId())));
      if (element.getCompartment() != null) {
        final Glyph compartmentGlyph = ((Glyph) (sourceTargetMap.get(element.getCompartment().getElementId())));
        speciesGlyph.setCompartmentRef(compartmentGlyph);
      }
    }

    for (final Reaction reaction : model.getReactions()) {
      try {
        final Glyph glyph = getProcessGlyphFromReaction(reaction);
        if (glyph != null) {
          map.getGlyph().add(glyph);
          map.getArc().addAll(getArcsFromReaction(reaction, map.getGlyph()));
        } else {
          final Arc arc = getArcFromReducedReaction(reaction);
          if (arc != null) {
            map.getArc().add(arc);
          }
        }
      } catch (final Exception ex) {
        logger.warn(new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, reaction),
            "Unexpected problem with exporting reaction.", ex);
      }
    }
    try {
      RenderUtil.setRenderInformation(map, renderInformation);
    } catch (final XMLStreamException | JAXBException | ParserConfigurationException | SAXException | IOException e) {
      throw new ConverterException("Problem with providing render information");
    }

    map.setNotes(notesConverter.createNotesNode(model.getNotes()));

    sbgnData.getMap().add(map);
    return sbgnData;
  }

  /**
   * Creates new glyph element with all parameters from given alias.
   *
   * @param element alias with all parameters for the glyph
   * @return newly created glyph
   */
  private Glyph elementToGlyph(final Element element) {
    final Glyph newGlyph = new Glyph();
    final String elementId = getExportElementId(element);
    newGlyph.setId(elementId);
    newGlyph.setClazz(getGlyphClazzFromElement(element).getClazz());
    newGlyph.setLabel(getGlyphLabelFromAlias(element));
    newGlyph.setNotes(notesConverter.createNotesNode(element.getNotes()));

    final Bbox bbox = new Bbox();
    bbox.setX(element.getX().floatValue());
    bbox.setY(element.getY().floatValue());
    bbox.setW(element.getWidth().floatValue());
    bbox.setH(element.getHeight().floatValue());
    newGlyph.setBbox(bbox);

    if (element instanceof Compartment) {
      if (element.getZ() != null) {
        newGlyph.setCompartmentOrder(element.getZ().floatValue());
      }
    } else if (element instanceof Species) {
      final Species species = (Species) element;
      if (element instanceof Protein) {
        final Protein protein = (Protein) element;
        for (final ModificationResidue mr : protein.getModificationResidues()) {
          final Glyph stateVariableGlyph;
          if (mr instanceof StructuralState) {
            stateVariableGlyph = createStateVariableForStructuralState(mr);
          } else {
            stateVariableGlyph = parseStateVariable(mr);
            stateVariableGlyph.setId(newGlyph.getId().concat("-").concat(stateVariableGlyph.getId()));
          }
          newGlyph.getGlyph().add(stateVariableGlyph);
          addRenderInformation(stateVariableGlyph, mr);
        }
      }

      final Glyph unitOfInformationGlyph = getUnitOfInformationGlyph(species);
      if (unitOfInformationGlyph != null) {
        newGlyph.getGlyph().add(unitOfInformationGlyph);
      }

      if (element instanceof Complex) {
        final Complex complex = (Complex) element;
        for (final Species child : complex.getElements()) {
          final Glyph childGlyph = elementToGlyph(child);
          newGlyph.getGlyph().add(childGlyph);
        }
        for (final ModificationResidue mr : complex.getModificationResidues()) {
          final Glyph stateVariableGlyph;
          if (mr instanceof StructuralState) {
            stateVariableGlyph = createStateVariableForStructuralState(mr);
          } else {
            stateVariableGlyph = parseStateVariable(mr);
            stateVariableGlyph.setId(newGlyph.getId().concat("-").concat(stateVariableGlyph.getId()));
          }
          newGlyph.getGlyph().add(stateVariableGlyph);
          addRenderInformation(stateVariableGlyph, mr);
        }
      }
    }
    addRenderInformation(newGlyph, element);
    addExtensions(newGlyph, element);

    sourceTargetMap.put(element.getElementId(), newGlyph);
    return newGlyph;
  }

  private String getExportElementId(final Element element) {
    if (element == null) {
      return null;
    }
    String elementId = exportElementIds.get(element);
    if (elementId == null) {
      final boolean idIsANumber = StringUtils.isNumeric(element.getElementId().substring(0, 1));
      if (idIsANumber) {
        elementId = RandomStringUtils.randomAlphabetic(ID_RANDOM_STRING_LENGTH).concat(element.getElementId());
      } else {
        elementId = element.getElementId();
      }
      exportElementIds.put(element, elementId);
    }
    return elementId;
  }

  private void addExtensions(final SBGNBase newGlyph, final Element element) {
    final SBGNBase.Extension extension = new SBGNBase.Extension();
    newGlyph.setExtension(extension);
    final org.w3c.dom.Element annotation = getAnnotationExtension(element);
    if (annotation != null) {
      extension.getAny().add(annotation);
    }
  }

  private void addExtensions(final SBGNBase newGlyph, final Model element) {
    final SBGNBase.Extension extension = new SBGNBase.Extension();
    newGlyph.setExtension(extension);
    final org.w3c.dom.Element annotation = getAnnotationExtension(element);
    if (annotation != null) {
      extension.getAny().add(annotation);
    }
  }

  private org.w3c.dom.Element getAnnotationExtension(final Element element) {
    try {
      return getAnnotation(element.getMiriamData(), new ArrayList<>(), new ArrayList<>(), null, element.getElementId());
    } catch (final Exception e) {
      logger.error(new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, element), "Problem with exporting annotations", e);
      return null;
    }
  }

  private org.w3c.dom.Element getAnnotationExtension(final Model element) {
    try {
      return getAnnotation(element.getMiriamData(), element.getAuthors(), element.getModificationDates(), element.getCreationDate(),
          element.getIdModel());
    } catch (final Exception e) {
      logger.error(new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, "Map", "", element.getName()), "Problem with exporting annotations", e);
      return null;
    }
  }

  private org.w3c.dom.Element getAnnotation(final Set<MiriamData> miriamData, final Collection<Author> authors,
                                            final List<Calendar> modificationDates, final Calendar creationDate, final String elementId)
      throws ParserConfigurationException, SAXException, IOException {
    String content = xap.dataSetToXmlString(miriamData, authors, creationDate, modificationDates, elementId);

    content = "<annotation>" + content + "</annotation>";
    final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    dbf.setNamespaceAware(true);
    final DocumentBuilder db;
    db = dbf.newDocumentBuilder();
    final Document doc = db.parse(new ByteArrayInputStream(content.getBytes()));

    return doc.getDocumentElement();
  }

  private void addRenderInformation(final Glyph glyph, final Element element) {
    float lineWidth = 1.0f;
    if (element instanceof Species) {
      lineWidth = ((Species) element).getLineWidth().floatValue();
    } else if (element instanceof Compartment) {
      lineWidth = (float) ((Compartment) element).getThickness();
    }
    RenderUtil.addStyle(renderInformation, glyph.getId(), colorParser.colorToHtml(element.getFillColor()),
        colorParser.colorToHtml(element.getBorderColor()), lineWidth);
  }

  private void addRenderInformation(final Glyph glyph, final ModificationResidue element) {
    final float lineWidth = element.getSpecies().getLineWidth().floatValue();
    RenderUtil.addStyle(renderInformation, glyph.getId(), colorParser.colorToHtml(Color.WHITE),
        colorParser.colorToHtml(element.getBorderColor()), lineWidth);
  }

  private void addRenderInformation(final Glyph glyph, final PolylineData line) {
    final float lineWidth = (float) line.getWidth();
    RenderUtil.addStyle(renderInformation, glyph.getId(), colorParser.colorToHtml(Color.WHITE),
        colorParser.colorToHtml(line.getColor()), lineWidth);
  }

  private void addRenderInformation(final Arc arc, final PolylineData line) {
    final float lineWidth = (float) line.getWidth();
    RenderUtil.addStyle(renderInformation, arc.getId(), colorParser.colorToHtml(Color.WHITE),
        colorParser.colorToHtml(line.getColor()), lineWidth);
  }

  private Glyph createStateVariableForStructuralState(final ModificationResidue structuralState) {
    final Glyph glyph = new Glyph();
    glyph.setId(structuralState.getSpecies().getElementId() + "-state");
    glyph.setClazz(GlyphClazz.STATE_VARIABLE.getClazz());

    final Glyph.State state = new Glyph.State();
    if (structuralState.getName().contains("@")) {
      state.setValue(structuralState.getName().substring(0, structuralState.getName().indexOf("@")));
      state.setVariable(structuralState.getName().substring(structuralState.getName().indexOf("@") + 1));
    } else {
      state.setValue(structuralState.getName());
    }

    glyph.setState(state);

    glyph.setBbox(getBoxForModificationResidue(structuralState));

    return glyph;
  }

  /**
   * Creates new glyph element with all parameters from given operator.
   *
   * @param operator operator with all parameters for the glyph
   * @return newly created glyph
   */
  private Glyph operatorToGlyph(final NodeOperator operator) {
    final Glyph newGlyph = new Glyph();
    newGlyph.setId("operator".concat(Integer.toString(parsedOperators.size())));
    if (operator instanceof AndOperator) {
      newGlyph.setClazz(GlyphClazz.AND.getClazz());
    } else if (operator instanceof OrOperator) {
      newGlyph.setClazz(GlyphClazz.OR.getClazz());
    } else {
      throw new InvalidArgumentException("Cannot export operator: " + operator.getClass());
    }
    parsedOperators.add(operator);
    operatorIds.put(operator, newGlyph.getId());

    final Bbox bbox = new Bbox();
    bbox.setX((float) (operator.getLine().getStartPoint().getX() - OPERATOR_SIZE / 2));
    bbox.setY((float) (operator.getLine().getStartPoint().getY() - OPERATOR_SIZE / 2));
    bbox.setW((float) OPERATOR_SIZE);
    bbox.setH((float) OPERATOR_SIZE);
    newGlyph.setBbox(bbox);

    final Port port2 = new Port();
    port2.setId(newGlyph.getId().concat(".2"));
    final Point2D centerPoint = operator.getLine().getStartPoint();
    final Point2D nextPoint = operator.getLine().getLines().get(0).getP2();
    final double dx = nextPoint.getX() - centerPoint.getX();
    final double dy = nextPoint.getY() - centerPoint.getY();
    final double portToCenterDistance = OPERATOR_SIZE / 2 + OPERATOR_PORT_DISTANCE;

    final double dx2;
    final double dy2;
    if (dx != 0) {
      dx2 = Math.sqrt(Math.pow(portToCenterDistance, 2) / (1 + Math.pow(dy / dx, 2)));
      dy2 = dx2 * dy / dx;
    } else {
      dx2 = 0;
      if (dy > 0) {
        dy2 = portToCenterDistance;
      } else {
        dy2 = -portToCenterDistance;
      }
    }

    port2.setX((float) (centerPoint.getX() + dx2));
    port2.setY((float) (centerPoint.getY() + dy2));
    sourceTargetMap.put(port2.getId(), port2);
    newGlyph.getPort().add(port2);

    final Port port1 = new Port();
    port1.setId(newGlyph.getId().concat(".1"));
    final Point2D portPoint1 = new Point2D.Double(2 * centerPoint.getX() - port2.getX(),
        2 * centerPoint.getY() - port2.getY());
    port1.setX((float) portPoint1.getX());
    port1.setY((float) portPoint1.getY());
    sourceTargetMap.put(port1.getId(), port1);
    newGlyph.getPort().add(port1);

    sourceTargetMap.put(newGlyph.getId(), newGlyph);

    return newGlyph;
  }

  /**
   * Returns GlyphClazz adequate for given element, based on it's class.
   *
   * @param element element to extract GlyphCLazz from
   * @return GlyphClazz adequate for given alias
   */
  private GlyphClazz getGlyphClazzFromElement(final Element element) {
    if (element instanceof Protein) {
      final Protein protein = (Protein) element;
      if (protein.getHomodimer() == 1) {
        return GlyphClazz.MACROMOLECULE;
      } else {
        return GlyphClazz.MACROMOLECULE_MULTIMER;
      }
    }

    if (element instanceof SimpleMolecule) {
      final SimpleMolecule simpleMolecule = (SimpleMolecule) element;
      if (simpleMolecule.getHomodimer() == 1) {
        return GlyphClazz.SIMPLE_CHEMICAL;
      } else {
        return GlyphClazz.SIMPLE_CHEMICAL_MULTIMER;
      }
    }

    if (element instanceof Ion) {
      final Ion ion = (Ion) element;
      if (ion.getHomodimer() == 1) {
        return GlyphClazz.SIMPLE_CHEMICAL;
      } else {
        return GlyphClazz.SIMPLE_CHEMICAL_MULTIMER;
      }
    }

    if (element instanceof Gene) {
      final Gene gene = (Gene) element;
      if (gene.getHomodimer() == 1) {
        return GlyphClazz.NUCLEIC_ACID_FEATURE;
      } else {
        return GlyphClazz.NUCLEIC_ACID_FEATURE_MULTIMER;
      }
    }

    if (element instanceof Rna) {
      final Rna rna = (Rna) element;
      if (rna.getHomodimer() == 1) {
        return GlyphClazz.NUCLEIC_ACID_FEATURE;
      } else {
        return GlyphClazz.NUCLEIC_ACID_FEATURE_MULTIMER;
      }
    }

    if (element instanceof AntisenseRna) {
      final AntisenseRna rna = (AntisenseRna) element;
      if (rna.getHomodimer() == 1) {
        return GlyphClazz.NUCLEIC_ACID_FEATURE;
      } else {
        return GlyphClazz.NUCLEIC_ACID_FEATURE_MULTIMER;
      }
    }

    if (element instanceof Complex) {
      final Complex complexSpecies = (Complex) element;
      if (complexSpecies.getHomodimer() == 1) {
        return GlyphClazz.COMPLEX;
      } else {
        return GlyphClazz.COMPLEX_MULTIMER;
      }
    }

    if (element instanceof Degraded) {
      return GlyphClazz.SOURCE_AND_SINK;
    }

    if (element instanceof Phenotype) {
      return GlyphClazz.PHENOTYPE;
    }

    if (element instanceof Compartment) {
      return GlyphClazz.COMPARTMENT;
    }
    if (element instanceof Unknown) {
      return GlyphClazz.UNSPECIFIED_ENTITY;
    }

    logger.warn(new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, element),
        "Element type is not supported by SBGN-ML format. Unspecified Entity type assigned.");
    return GlyphClazz.UNSPECIFIED_ENTITY;
  }

  /**
   * Returns label for a glyph with name extracted from given element.
   *
   * @param element element with the name
   * @return label for a glyph
   */
  private Label getGlyphLabelFromAlias(final Element element) {
    if (element instanceof Degraded) {
      return null;
    }
    final Label label = new Label();
    label.setText(element.getName());
    final Bbox box = new Bbox();
    box.setX(element.getNameX().floatValue());
    box.setY(element.getNameY().floatValue());
    box.setW(element.getNameWidth().floatValue());
    box.setH(element.getNameHeight().floatValue());
    label.setBbox(box);
    return label;
  }

  /**
   * Returns state variable glyph parsed from {@link ModificationResidue}.
   *
   * @param mr {@link ModificationResidue} to be parsed
   * @return state variable glyph
   */
  private Glyph parseStateVariable(final ModificationResidue mr) {
    final Glyph glyph = new Glyph();
    glyph.setId(mr.getIdModificationResidue());
    glyph.setClazz(GlyphClazz.STATE_VARIABLE.getClazz());

    if (mr instanceof AbstractSiteModification) {
      final AbstractSiteModification modification = (AbstractSiteModification) mr;
      final Glyph.State state = new Glyph.State();
      state.setVariable(mr.getName());
      glyph.setState(state);
      if (modification.getState() != null) {
        state.setValue(modification.getState().getAbbreviation());
      }
    }

    glyph.setBbox(getBoxForModificationResidue(mr));

    return glyph;
  }

  private Bbox getBoxForModificationResidue(final ModificationResidue mr) {
    final Bbox bbox = new Bbox();

    bbox.setH((float) mr.getHeight());
    bbox.setW((float) mr.getWidth());

    bbox.setX(mr.getX().floatValue());
    bbox.setY(mr.getY().floatValue());
    return bbox;
  }

  private Glyph getUnitOfInformationGlyph(final Species species) {
    Glyph uoiGlyph = null;

    String uoiText = "";
    final int homodir = species.getHomodimer();
    if (homodir > 1) {
      uoiText = "N:".concat(Integer.toString(homodir));
    }

    if (species instanceof TruncatedProtein) {
      if (!uoiText.isEmpty()) {
        uoiText = uoiText.concat("; ");
      }
      uoiText = uoiText.concat("ct:truncatedProtein");
    }

    if ((species.getStateLabel() != null) && (species.getStatePrefix() != null)) {
      if (!uoiText.isEmpty()) {
        uoiText = uoiText.concat("; ");
      }
      if (!species.getStatePrefix().equals("free input")) {
        uoiText = uoiText.concat(species.getStatePrefix()).concat(":");
      }
      uoiText = uoiText.concat(species.getStateLabel());
    }

    if (!uoiText.contains("ct:")) {
      if (species instanceof Rna) {
        if (!uoiText.isEmpty()) {
          uoiText = uoiText.concat("; ");
        }
        uoiText = uoiText.concat("ct:RNA");
      }
      if (species instanceof AntisenseRna) {
        if (!uoiText.isEmpty()) {
          uoiText = uoiText.concat("; ");
        }
        uoiText = uoiText.concat("ct:antisenseRNA");
      }
      if (species instanceof Gene) {
        if (!uoiText.isEmpty()) {
          uoiText = uoiText.concat("; ");
        }
        uoiText = uoiText.concat("ct:gene");
      }
    }

    if (!uoiText.isEmpty()) {
      uoiGlyph = new Glyph();
      uoiGlyph.setClazz(GlyphClazz.UNIT_OF_INFORMATION.getClazz());
      uoiGlyph.setId(species.getElementId().concat("uoi"));

      final Label label = new Label();
      label.setText(uoiText);
      uoiGlyph.setLabel(label);

      final Bbox bbox = new Bbox();
      bbox.setX((float) (species.getX() + UNIT_OF_INFORMATION_MARGIN));
      bbox.setY((float) (species.getY() - UNIT_OF_INFORMATION_HEIGHT / 2));
      bbox.setH((float) UNIT_OF_INFORMATION_HEIGHT);
      bbox.setW((float) (species.getWidth() - 2 * UNIT_OF_INFORMATION_MARGIN));
      uoiGlyph.setBbox(bbox);
    }

    return uoiGlyph;
  }

  /**
   * Returns process glyph created based on reaction's center point.
   *
   * @param reaction reaction to be parsed
   * @return process glyph for given reaction
   */
  private Glyph getProcessGlyphFromReaction(final Reaction reaction) {
    final Glyph processGlyph = new Glyph();
    processGlyph.setId(reaction.getIdReaction());
    final GlyphClazz clazz = getGlyphClazzFromReaction(reaction);
    if (clazz == null) {
      return null;
    }
    processGlyph.setClazz(clazz.getClazz());
    final Bbox bbox = new Bbox();

    final PolylineData line = reaction.getLine();
    final Point2D startPoint = line.getStartPoint();
    final Point2D endPoint = line.getEndPoint();

    final double pointX = (startPoint.getX() + endPoint.getX()) / 2;
    final double pointY = (startPoint.getY() + endPoint.getY()) / 2;
    final Point2D pointNW = new Point2D.Double(pointX - ReactionConverter.RECT_SIZE / 2,
        pointY - ReactionConverter.RECT_SIZE / 2);
    bbox.setX((float) pointNW.getX());
    bbox.setY((float) pointNW.getY());
    bbox.setH((float) ReactionConverter.RECT_SIZE);
    bbox.setW((float) ReactionConverter.RECT_SIZE);

    processGlyph.setBbox(bbox);

    final Port reactantPort = new Port();
    reactantPort.setId(reaction.getIdReaction().concat(".1"));
    sourceTargetMap.put(reactantPort.getId(), reactantPort);

    final Port productPort = new Port();
    productPort.setId(reaction.getIdReaction().concat(".2"));
    sourceTargetMap.put(productPort.getId(), productPort);

    // Set glyph orientation and ports' coordinates.
    double reactantDxAverage = 0.0;
    double reactantDyAverage = 0.0;
    double productDxAverage = 0.0;
    double productDyAverage = 0.0;

    for (final Reactant r : reaction.getReactants()) {
      final Point2D reactantPoint;
      final PolylineData reactantArcPoints = r.getLine();
      if (reaction instanceof HeterodimerAssociationReaction) {
        reactantPoint = reactantArcPoints.getEndPoint();
      } else {
        reactantPoint = reactantArcPoints.getLines().get(reactantArcPoints.getLines().size() - 1).getP1();
      }
      reactantDxAverage += reactantPoint.getX() - pointX;
      reactantDyAverage += reactantPoint.getY() - pointY;
    }
    reactantDxAverage /= reaction.getReactants().size();
    reactantDyAverage /= reaction.getReactants().size();

    for (final Product p : reaction.getProducts()) {
      final PolylineData productArcPoints = p.getLine();
      final Point2D productPoint;
      if (reaction instanceof DissociationReaction) {
        productPoint = productArcPoints.getStartPoint();
      } else {
        productPoint = productArcPoints.getLines().get(0).getP2();
      }
      productDxAverage += productPoint.getX() - pointX;
      productDyAverage += productPoint.getY() - pointY;
    }
    productDxAverage /= reaction.getProducts().size();
    productDyAverage /= reaction.getProducts().size();

    final boolean horizontalOrientation;
    if ((reactantDxAverage * productDxAverage < 0) && (reactantDyAverage * productDyAverage < 0)) {
      horizontalOrientation = (Math.abs(productDxAverage - reactantDxAverage) > Math
          .abs(productDyAverage - reactantDyAverage));
    } else {
      horizontalOrientation = (reactantDxAverage * productDxAverage < 0);
    }
    if (horizontalOrientation) {
      processGlyph.setOrientation("horizontal");
      reactantPort.setY((float) pointY);
      productPort.setY((float) pointY);
      if (reactantDxAverage < 0) {
        reactantPort.setX((float) (pointX - PROCESS_PORT_DISTANCE));
        productPort.setX((float) (pointX + PROCESS_PORT_DISTANCE));
      } else {
        reactantPort.setX((float) (pointX + PROCESS_PORT_DISTANCE));
        productPort.setX((float) (pointX - PROCESS_PORT_DISTANCE));
      }
    } else {
      reactantPort.setX((float) pointX);
      productPort.setX((float) pointX);
      if (reactantDyAverage < 0) {
        reactantPort.setY((float) (pointY - PROCESS_PORT_DISTANCE));
        productPort.setY((float) (pointY + PROCESS_PORT_DISTANCE));
      } else {
        reactantPort.setY((float) (pointY + PROCESS_PORT_DISTANCE));
        productPort.setY((float) (pointY - PROCESS_PORT_DISTANCE));
      }
      processGlyph.setOrientation("vertical");
    }

    processGlyph.getPort().add(reactantPort);
    processGlyph.getPort().add(productPort);

    sourceTargetMap.put(processGlyph.getId(), processGlyph);

    addRenderInformation(processGlyph, reaction.getLine());

    return processGlyph;
  }

  /**
   * Returns {@link GlyphClazz} appropriate to given reaction.
   *
   * @param reaction {@link Reaction} to extract {@link GlyphClazz} from
   * @return {@link GlyphClazz} appropriate to given reaction
   */
  GlyphClazz getGlyphClazzFromReaction(final Reaction reaction) {
    if (reaction instanceof HeterodimerAssociationReaction) {
      return GlyphClazz.ASSOCIATION;
    }
    if (reaction instanceof DissociationReaction) {
      return GlyphClazz.DISSOCIATION;
    }
    if (reaction instanceof KnownTransitionOmittedReaction) {
      return GlyphClazz.OMITTED_PROCESS;
    }
    if (reaction instanceof UnknownTransitionReaction) {
      return GlyphClazz.UNCERTAIN_PROCESS;
    }
    if (reaction instanceof ReducedNotation) {
      return null;
    }
    if (reaction instanceof ModifierReactionNotation) {
      return null;
    }
    return GlyphClazz.PROCESS;
  }

  /**
   * Returns arc extracted from given reduced notation reaction.
   *
   * @param reaction reduced notation reaction
   * @return arc extracted from given reduced notation reaction
   */
  Arc getArcFromReducedReaction(final Reaction reaction) {
    if ((reaction.getReactants().size() != 1) || (reaction.getProducts().size() != 1)) {
      logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, reaction),
          "Reduced notation does not allow to have multiple reactants/products. Reaction skipped");
      return null;
    }
    final Arc arc = new Arc();
    arc.setId(reaction.getIdReaction());

    final ArcClazz clazz = getArcClassForReducedNotationReaction(reaction);
    if (clazz != null) {
      arc.setClazz(clazz.getClazz());
    } else {
      logger.warn(new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, reaction), "Invalid arc type."
          + " Reduced notation reaction found of type that is not compliant with SBGN-ML format.");
      return null;
    }

    arc.setSource(sourceTargetMap.get(reaction.getReactants().get(0).getElement().getElementId()));
    arc.setTarget(sourceTargetMap.get(reaction.getProducts().get(0).getElement().getElementId()));

    final List<Point2D> pointList = new ArrayList<>();
    pointList.add(reaction.getReactants().get(0).getLine().getStartPoint());
    for (final Line2D line : reaction.getReactants().get(0).getLine().getLines()) {
      pointList.add(line.getP2());
    }

    pointList.add(reaction.getProducts().get(0).getLine().getStartPoint());
    for (final Line2D line : reaction.getProducts().get(0).getLine().getLines()) {
      pointList.add(line.getP2());
    }

    removeRedundantPoints(pointList);

    final Start start = new Start();
    start.setX((float) pointList.get(0).getX());
    start.setY((float) pointList.get(0).getY());
    arc.setStart(start);

    final End end = new End();
    end.setX((float) pointList.get(pointList.size() - 1).getX());
    end.setY((float) pointList.get(pointList.size() - 1).getY());
    arc.setEnd(end);

    for (int i = 1; i < pointList.size() - 1; i++) {
      final Point2D nextPoint = pointList.get(i);
      final Next next = new Next();
      next.setX((float) nextPoint.getX());
      next.setY((float) nextPoint.getY());
      arc.getNext().add(next);
    }

    return arc;
  }

  ArcClazz getArcClassForReducedNotationReaction(final Reaction reaction) {
    if (reaction instanceof NegativeInfluenceReaction) {
      return ArcClazz.INHIBITION;
    } else if (reaction instanceof ReducedModulationReaction) {
      return ArcClazz.MODULATION;
    } else if (reaction instanceof ReducedTriggerReaction) {
      return ArcClazz.NECESSARY_STIMULATION;
    } else if (reaction instanceof ReducedPhysicalStimulationReaction) {
      return ArcClazz.STIMULATION;
    } else if (reaction instanceof CatalysisReaction) {
      return ArcClazz.CATALYSIS;
    } else if (reaction instanceof InhibitionReaction) {
      return ArcClazz.INHIBITION;
    } else if (reaction instanceof ModulationReaction) {
      return ArcClazz.MODULATION;
    } else if (reaction instanceof PhysicalStimulationReaction) {
      return ArcClazz.STIMULATION;
    } else if (reaction instanceof PositiveInfluenceReaction) {
      return ArcClazz.STIMULATION;
    } else if (reaction instanceof TriggerReaction) {
      return ArcClazz.NECESSARY_STIMULATION;
    } else if (reaction instanceof UnknownCatalysisReaction) {
      return ArcClazz.CATALYSIS;
    } else if (reaction instanceof UnknownInhibitionReaction) {
      return ArcClazz.INHIBITION;
    } else if (reaction instanceof UnknownNegativeInfluenceReaction) {
      return ArcClazz.INHIBITION;
    } else if (reaction instanceof UnknownReducedModulationReaction) {
      return ArcClazz.MODULATION;
    } else if (reaction instanceof UnknownReducedPhysicalStimulationReaction) {
      return ArcClazz.STIMULATION;
    } else if (reaction instanceof UnknownPositiveInfluenceReaction) {
      return ArcClazz.STIMULATION;
    } else if (reaction instanceof UnknownReducedTriggerReaction) {
      return ArcClazz.NECESSARY_STIMULATION;
    }
    return null;
  }

  /**
   * Removes redundant points from the list.
   *
   * @param pointList list of points to be fixed
   */
  private void removeRedundantPoints(final List<Point2D> pointList) {
    boolean allDone = false;
    while (!allDone) {
      allDone = true;
      for (int i = 1; i < pointList.size() - 1; i++) {
        final double dx1 = pointList.get(i).getX() - pointList.get(i - 1).getX();
        final double dy1 = pointList.get(i).getY() - pointList.get(i - 1).getY();
        final double dx2 = pointList.get(i + 1).getX() - pointList.get(i).getX();
        final double dy2 = pointList.get(i + 1).getY() - pointList.get(i).getY();

        final DoubleComparator doubleComparator = new DoubleComparator();
        if (((doubleComparator.compare(dy1 / dx1, dy2 / dx2) == 0)
            || (pointList.get(i - 1).getY() == pointList.get(i).getY())
            && (pointList.get(i).getY() == pointList.get(i + 1).getY()))
            && between(pointList.get(i).getX(), pointList.get(i - 1).getX(), pointList.get(i + 1).getX())) {
          pointList.remove(i);
          allDone = false;
        }
      }
    }
  }

  /**
   * Checks if argument x is between s1 and s2.
   *
   * @param x  number to checked if it is in the middle
   * @param s1 one side argument
   * @param s2 second side argument
   * @return true if x is between s1 and s2
   */
  private boolean between(final double x, final double s1, final double s2) {
    if ((x >= s1) && (x <= s2)) {
      return true;
    }
    return (x <= s1) && (x >= s2);
  }

  /**
   * Returns set of all arcs used in the reaction.
   *
   * @param reaction  the reaction to extract arcs from
   * @param glyphList list of all glyphs in the map; used only for parsing operators
   * @return list of all arcs used in the reaction
   */
  private List<Arc> getArcsFromReaction(final Reaction reaction, final List<Glyph> glyphList) {
    final List<Arc> arcList = new ArrayList<>();

    // Parse all nodes except NodeOperators
    for (final ReactionNode node : reaction.getReactionNodes()) {
      try {
        arcList.add(getArcFromNode(node, glyphList));
      } catch (final InvalidArgumentException ex) {
        logger.warn(new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, node),
            new ElementUtils().getElementTag(node.getElement())
                + "Node skipped in export process, since it is not compliant with SBGN-ML format.");
        continue;
      }
    }

    // Now parse NodeOperators
    for (final NodeOperator node : reaction.getOperators()) {
      if (node.isModifierOperator()) {
        try {
          arcList.add(getArcFromNode(node, glyphList));
        } catch (final InvalidArgumentException ex) {
          logger.warn(new LogMarker(ProjectLogEntryType.EXPORT_ISSUE, reaction),
              "Operator skipped in export process, since it is not compliant with SBGN-ML format. ("
                  + node.getClass().getSimpleName() + ")");
          continue;
        }
      }
    }

    return arcList;

  }

  /**
   * Returns arc for given node.
   *
   * @param node      node to parse arc from
   * @param glyphList list of all glyphs in the map; used only for parsing operators
   * @return SBGN-ML arc for given node
   */
  private Arc getArcFromNode(final AbstractNode node, final List<Glyph> glyphList) {
    final Arc arc = new Arc();
    arc.setId("arc".concat(Integer.toString(arcCounter)));
    arcCounter += 1;

    boolean logicArc = false;
    if (node instanceof Modifier) {
      for (final NodeOperator operator : node.getReaction().getOperators()) {
        if (operator.getInputs().contains(node)) {
          logicArc = true;
          break;
        }
      }
    }
    if (logicArc) {
      arc.setClazz(ArcClazz.LOGIC_ARC.getClazz());
    } else {
      arc.setClazz(getArcClazzFromNode(node).getClazz());
    }

    if (node instanceof Reactant) {
      arc.setSource(sourceTargetMap.get(((Reactant) node).getElement().getElementId()));
      arc.setTarget(sourceTargetMap.get(node.getReaction().getIdReaction().concat(".1")));
    } else if (node instanceof Product) {
      arc.setSource(sourceTargetMap.get(node.getReaction().getIdReaction().concat(".2")));
      arc.setTarget(sourceTargetMap.get(((Product) node).getElement().getElementId()));
    } else if (node instanceof Modifier) {
      arc.setSource(sourceTargetMap.get(((Modifier) node).getElement().getElementId()));
      if (!node.getLine().getEndAtd().getArrowType().equals(ArrowType.NONE)) {
        arc.setTarget(sourceTargetMap.get(node.getReaction().getIdReaction()));
      } else {
        boolean found = false;
        for (final NodeOperator operator : node.getReaction().getOperators()) {
          if (operator.getInputs().contains(node)) {
            if (!parsedOperators.contains(operator)) {
              final Glyph newOperator = operatorToGlyph(operator);
              glyphList.add(newOperator);
            }
            arc.setTarget(sourceTargetMap.get(operatorIds.get(operator).concat(".1")));
            found = true;
          }
        }
        if (!found) {
          throw new InvalidArgumentException(
              new ElementUtils().getElementTag((Modifier) node) + "Problem with exporting modifier");
        }
      }
    } else if (node instanceof NodeOperator) {
      if (operatorIds.get(node) == null) {
        throw new InvalidArgumentException("Operator not supported" + node.getClass());
      }
      arc.setSource(sourceTargetMap.get(operatorIds.get(node).concat(".2")));
      if (!node.getLine().getEndAtd().getArrowType().equals(ArrowType.NONE)) {
        arc.setTarget(sourceTargetMap.get(node.getReaction().getIdReaction()));
      } else {
        for (final NodeOperator operator : node.getReaction().getOperators()) {
          if (operator.getInputs().contains(node)) {
            if (!parsedOperators.contains(operator)) {
              final Glyph newOperator = operatorToGlyph(operator);
              glyphList.add(newOperator);
            }
            arc.setTarget(sourceTargetMap.get(operatorIds.get(operator).concat(".1")));
          }
        }
      }
    }

    final List<Point2D> arcPoints = new ArrayList<>();
    arcPoints.add(node.getLine().getStartPoint());
    for (final Line2D line : node.getLine().getLines()) {
      arcPoints.add(line.getP2());
    }
    final Start start = new Start();
    if ((node instanceof Product) || (node instanceof NodeOperator)) {
      final Port sourcePort = (Port) arc.getSource();
      start.setX(sourcePort.getX());
      start.setY(sourcePort.getY());
    } else {
      start.setX((float) arcPoints.get(0).getX());
      start.setY((float) arcPoints.get(0).getY());
    }
    arc.setStart(start);

    final End end = new End();
    if ((node instanceof Reactant) || ((node instanceof Modifier) && (arc.getTarget() instanceof Port))) {
      final Port targetPort = (Port) arc.getTarget();
      end.setX(targetPort.getX());
      end.setY(targetPort.getY());
    } else {
      final Point2D lastPoint = arcPoints.get(arcPoints.size() - 1);
      end.setX((float) lastPoint.getX());
      end.setY((float) lastPoint.getY());
    }
    arc.setEnd(end);

    if ((node instanceof Product) && (node.getReaction() instanceof DissociationReaction)) {
      final Point2D nextPoint = arcPoints.get(0);
      final Next next = new Next();
      next.setX((float) nextPoint.getX());
      next.setY((float) nextPoint.getY());
      arc.getNext().add(next);
    }

    final DoubleComparator doubleComparator = new DoubleComparator();
    for (int i = 1; i < arcPoints.size() - 1; i++) {
      final Point2D nextPoint = arcPoints.get(i);
      if ((doubleComparator.compare(nextPoint.getX(), (double) start.getX()) == 0)
          && (doubleComparator.compare(nextPoint.getY(), (double) start.getY()) == 0)) {
        arc.getNext().clear();
        continue;
      }
      if ((doubleComparator.compare(nextPoint.getX(), (double) end.getX()) == 0)
          && (doubleComparator.compare(nextPoint.getY(), (double) end.getY()) == 0)) {
        break;
      }
      final Next next = new Next();
      next.setX((float) nextPoint.getX());
      next.setY((float) nextPoint.getY());
      arc.getNext().add(next);
    }

    if ((node instanceof Reactant) && (node.getReaction() instanceof HeterodimerAssociationReaction)) {
      final Point2D nextPoint = arcPoints.get(arcPoints.size() - 1);
      final Next next = new Next();
      next.setX((float) nextPoint.getX());
      next.setY((float) nextPoint.getY());
      arc.getNext().add(next);
    }

    addRenderInformation(arc, node.getLine());
    return arc;
  }

  /**
   * Returns {@link ArcClazz} for given node.
   *
   * @param node Node to extract {@link ArcClazz} from
   * @return {@link ArcClazz} for given node
   */
  private ArcClazz getArcClazzFromNode(final AbstractNode node) {
    if (node instanceof Reactant) {
      return ArcClazz.CONSUMPTION;
    }
    if (node instanceof Product) {
      return ArcClazz.PRODUCTION;
    }
    if (node instanceof Catalysis) {
      return ArcClazz.CATALYSIS;
    }
    if (node instanceof Inhibition) {
      return ArcClazz.INHIBITION;
    }
    if (node instanceof Modulation) {
      return ArcClazz.MODULATION;
    }
    if (node instanceof Trigger) {
      return ArcClazz.NECESSARY_STIMULATION;
    }
    if (node instanceof PhysicalStimulation) {
      return ArcClazz.STIMULATION;
    }
    if (node instanceof NodeOperator) {
      final ArrowType arrowType = node.getLine().getEndAtd().getArrowType();
      switch (arrowType) {
        case BLANK:
          return ArcClazz.STIMULATION;
        case BLANK_CROSSBAR:
          return ArcClazz.NECESSARY_STIMULATION;
        case CIRCLE:
          return ArcClazz.CATALYSIS;
        case CROSSBAR:
          return ArcClazz.INHIBITION;
        case DIAMOND:
          return ArcClazz.MODULATION;
        case NONE:
          return ArcClazz.LOGIC_ARC;
        default:
          throw new InvalidArgumentException();
      }
    }
    throw new InvalidArgumentException();
  }
}
