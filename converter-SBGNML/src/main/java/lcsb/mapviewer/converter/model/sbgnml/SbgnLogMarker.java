package lcsb.mapviewer.converter.model.sbgnml;

import org.sbgn.bindings.Arc;
import org.sbgn.bindings.Glyph;

import lcsb.mapviewer.converter.model.sbgnml.structures.Process;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.map.model.Model;

public class SbgnLogMarker extends LogMarker {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public SbgnLogMarker(final LogMarker marker) {
    super(marker);
  }

  public SbgnLogMarker(final ProjectLogEntryType type, final Arc arc, final Model model) {
    super(type, arc.getClazz(), arc.getId(), model.getName());
  }

  public SbgnLogMarker(final ProjectLogEntryType type, final Glyph glyph, final Model model) {
    this(type, glyph, model.getName());
  }

  public SbgnLogMarker(final ProjectLogEntryType type, final Glyph glyph, final String model) {
    super(type, glyph.getClazz(), glyph.getId(), model);
  }

  public SbgnLogMarker(final ProjectLogEntryType type, final Process p, final Model model) {
    super(type, "PROCESS", p.getCentralPoint().getId(), model.getName());
  }

}
