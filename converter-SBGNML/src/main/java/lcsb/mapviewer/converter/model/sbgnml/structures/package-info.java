/**
 * lcsb.mapviewer.converter.model.sbgnml.structures is a group of additional
 * structures used in the process of parsing SBGN-ML file.
 */
package lcsb.mapviewer.converter.model.sbgnml.structures;