package lcsb.mapviewer.converter.model.sbgnml.parser;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.converter.model.celldesigner.geometry.CellDesignerAliasConverter;
import lcsb.mapviewer.converter.model.sbgnml.SbgnLogMarker;
import lcsb.mapviewer.converter.model.sbgnml.parser.newt.NewtExtension;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationResidue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithResidue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithStructuralState;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbgn.GlyphClazz;
import org.sbgn.bindings.Glyph;
import org.sbgn.bindings.Map;
import org.w3c.dom.Node;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ElementParser extends GlyphParser {

  /**
   * Default margin for container name.
   */
  private static final double CONTAINER_NAME_MARGIN = 5.0;

  /**
   * Part of height of the line used to cross degraded circle that goes behind
   * this circle.
   */
  private static final int CROSS_LINE_EXTENDED_LENGTH = 7;

  /**
   * Default color for parsed compartments.
   */
  private static final Color COMPARTMENT_COLOR = new Color(0.5f, 0.5f, 1.0f);

  private final Set<String> ignoredGlyphs = new HashSet<>();

  private final Logger logger = LogManager.getLogger();

  private final RenderParser renderParser;

  private final Model model;

  private final NotesConverter notesConverter = new NotesConverter();

  public ElementParser(final RenderParser renderParser, final Model model) {
    this.renderParser = renderParser;
    this.model = model;
  }

  /**
   * Method used to parse a single glyph.
   *
   * @param glyph glyph to be parsed
   */
  @SuppressWarnings("deprecation")
  public Element glyphToElement(final Glyph glyph) {
    if (!super.isElementGlyph(glyph)) {
      return null;
    }
    Element result = null;
    switch (GlyphClazz.fromClazz(glyph.getClazz())) {
      case BIOLOGICAL_ACTIVITY:
        logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, glyph, model),
            glyph.getClazz() + " is not supported yet and was changed to Generic Protein.");
        result = parseSpecies(glyph, GenericProtein.class, true);
        break;
      case COMPARTMENT:
        result = parseCompartment(glyph);
        break;
      case COMPLEX:
        result = parseComplex(glyph, null, true);
        break;
      case COMPLEX_MULTIMER:
        result = parseComplex(glyph, null, false);
        break;
      case ENTITY:
        logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, glyph, model),
            glyph.getClazz() + " is not supported yet and was changed to Unknown.");
        result = parseSpecies(glyph, Unknown.class, true);
        break;
      case MACROMOLECULE:
        result = parseSpecies(glyph, GenericProtein.class, true);
        break;
      case MACROMOLECULE_MULTIMER:
        result = parseSpecies(glyph, GenericProtein.class, false);
        break;
      case NUCLEIC_ACID_FEATURE:
      case NUCLEIC_ACID_FEATURE_MULTIMER:
        if (isAntisenseRNA(glyph)) {
          result = parseSpecies(glyph, AntisenseRna.class, true);
        } else if (isRNA(glyph)) {
          result = parseSpecies(glyph, Rna.class, true);
        } else {
          result = parseSpecies(glyph, Gene.class, true);
        }
        break;
      case OUTCOME:
        logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, glyph, model),
            glyph.getClazz() + " is not supported yet and was changed to Phenotype.");
        result = parseSpecies(glyph, Phenotype.class, true);
        break;
      case PERTURBING_AGENT:
      case OBSERVABLE:
      case PHENOTYPE:
        result = parseSpecies(glyph, Phenotype.class, true);
        break;
      case SIMPLE_CHEMICAL:
        result = parseSpecies(glyph, SimpleMolecule.class, true);
        break;
      case SIMPLE_CHEMICAL_MULTIMER:
        result = parseSpecies(glyph, SimpleMolecule.class, false);
        break;
      case SOURCE_AND_SINK:
        result = parseSpecies(glyph, Degraded.class, true);
        break;
      case TAG:
      case TERMINAL:
      case SUBMAP:
        logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, glyph, model), "Submaps are not supported.");
        break;
      case UNSPECIFIED_ENTITY:
        result = parseSpecies(glyph, Unknown.class, true);
        break;
      default:
        ignoredGlyphs.add(glyph.getId());
        logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, glyph, model),
            "The glyph has not been parsed, since it is invalid for SBGN PD map.");
        break;
    }
    return result;
  }

  /**
   * Method used for parsing compartments.
   *
   * @param glyph compartment glyph from sbgn-ml file
   */
  private Compartment parseCompartment(final Glyph glyph) {

    final Compartment compartment = new SquareCompartment(glyph.getId());
    if (glyph.getLabel() != null) {
      compartment.setName(glyph.getLabel().getText());
    }
    compartment.setHeight(new Double(glyph.getBbox().getH()));
    compartment.setWidth(new Double(glyph.getBbox().getW()));
    compartment.setX(new Double(glyph.getBbox().getX()));
    compartment.setY(new Double(glyph.getBbox().getY()));
    compartment.setThickness(1.0);
    compartment.setFillColor(COMPARTMENT_COLOR);
    compartment.setBorderColor(COMPARTMENT_COLOR);

    if (glyph.getCompartmentOrder() != null) {
      compartment.setZ(Math.round(glyph.getCompartmentOrder()));
    }

    if (glyph.getLabel() != null && glyph.getLabel().getBbox() != null) {
      compartment.setNameX(glyph.getLabel().getBbox().getX());
      compartment.setNameY(glyph.getLabel().getBbox().getY());
      compartment.setNameWidth(glyph.getLabel().getBbox().getW());
      compartment.setNameHeight(glyph.getLabel().getBbox().getH());
      compartment.setNameHorizontalAlign(HorizontalAlign.LEFT);
      compartment.setNameVerticalAlign(VerticalAlign.TOP);
    } else {
      compartment.setNameX(compartment.getX() + compartment.getThickness() + CONTAINER_NAME_MARGIN);
      compartment.setNameY(compartment.getY() + compartment.getThickness() + CONTAINER_NAME_MARGIN);
      compartment.setNameWidth(compartment.getWidth() - 2 * (compartment.getThickness() + CONTAINER_NAME_MARGIN));
      compartment.setNameHeight(compartment.getHeight() - 2 * (compartment.getThickness() + CONTAINER_NAME_MARGIN));

      compartment.setNameHorizontalAlign(HorizontalAlign.LEFT);
      compartment.setNameVerticalAlign(VerticalAlign.TOP);
    }

    renderParser.assignRenderInformation(compartment, glyph);

    return compartment;
  }

  /**
   * Method used for parsing complex species.
   *
   * @param complexGlyph         complex species glyph from sbgn-ml file
   * @param parentComplexSpecies parent complex species
   * @param isHomodimer          set if the complex is a homodimer
   */
  private Complex parseComplex(final Glyph complexGlyph, final Complex parentComplexSpecies, final boolean isHomodimer) {
    final Complex complexSpecies = (Complex) parseSpecies(complexGlyph, Complex.class, isHomodimer);
    if (parentComplexSpecies != null) {
      complexSpecies.setComplex(parentComplexSpecies);
    }

    for (final Glyph child : complexGlyph.getGlyph()) {
      final Element newSpecies = glyphToElement(child);
      if (newSpecies != null) {
        if (newSpecies instanceof Species) {
          complexSpecies.addSpecies((Species) newSpecies);
        }
      }
    }
    return complexSpecies;
  }

  private Element parseSpecies(final Glyph g, final Class<? extends Element> clazz, final boolean isHomodimer) {
    try {
      final Element element = clazz.getConstructor(String.class).newInstance(g.getId());
      if (g.getLabel() != null) {
        element.setName(g.getLabel().getText());
        if (g.getLabel().getBbox() != null) {
          element.setNameX(g.getLabel().getBbox().getX());
          element.setNameY(g.getLabel().getBbox().getY());
          element.setNameWidth(g.getLabel().getBbox().getW());
          element.setNameHeight(g.getLabel().getBbox().getH());
          element.setNameHorizontalAlign(HorizontalAlign.CENTER);
          if (element instanceof Complex) {
            element.setNameVerticalAlign(VerticalAlign.BOTTOM);
          } else {
            element.setNameVerticalAlign(VerticalAlign.MIDDLE);
          }
        }
      } else {
        element.setName("");
      }

      element.setNotes(notesConverter.parseNotes(g.getNotes()));
      if (g.getCompartmentOrder() != null) {
        element.setZ(Math.round(g.getCompartmentOrder()));
      }

      if (element instanceof Species) {
        final Species newSpecies = (Species) element;
        // Add species to parent complex if there is one
        if (newSpecies.getComplex() != null) {
          newSpecies.getComplex().addSpecies(newSpecies);
        }
        // If the species is a multimer, set it's cardinality.
        if (!isHomodimer) {
          try {
            newSpecies.setHomodimer(getMultimerCardinality(g));
          } catch (final Exception ex) {
            newSpecies.setHomodimer(2);
            logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, g, model),
                ex.getMessage() + " Set the default value of 2.");
          }
        }

        final List<Glyph> children = g.getGlyph();
        for (final Glyph child : children) {
          if (GlyphClazz.fromClazz(child.getClazz()).equals(GlyphClazz.STATE_VARIABLE)) {
            if (child.getState() == null || child.getState().getVariable() != null) {
              boolean canAssignStructuralState = false;
              if (newSpecies instanceof SpeciesWithStructuralState) {
                canAssignStructuralState = true;
                for (final ModificationResidue mr : ((SpeciesWithStructuralState) newSpecies).getModificationResidues()) {
                  if (mr instanceof StructuralState) {
                    canAssignStructuralState = false;
                    break;
                  }
                }
              }
              final Residue residue = stateVariableToResidue(child);
              if (residue != null && (!canAssignStructuralState || residue.getState() != null
                  || (child.getState() == null || child.getState().getValue() == null))) {
                if (newSpecies instanceof SpeciesWithResidue) {
                  ((SpeciesWithResidue) newSpecies).addResidue(residue);
                } else if (newSpecies instanceof SpeciesWithStructuralState) {
                  final StructuralState state = createStructuralState(child);
                  if (state != null) {
                    ((SpeciesWithStructuralState) newSpecies).addStructuralState(state);
                  }
                } else {
                  logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, child, model),
                      "Only macromolecule elements can have state variables.");
                }
              } else {
                final StructuralState state = createStructuralState(child);
                if (state != null) {
                  if (newSpecies instanceof SpeciesWithStructuralState) {
                    ((SpeciesWithStructuralState) newSpecies).addStructuralState(state);
                  } else {
                    logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, child, model),
                        "Only macromolecule elements can have state.");
                  }
                }
              }
            } else {
              final StructuralState structuralState = createStructuralState(child);
              if (structuralState != null) {
                if (newSpecies instanceof SpeciesWithStructuralState) {
                  ((SpeciesWithStructuralState) newSpecies).addStructuralState(structuralState);
                } else {
                  logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, child, model), "State is not supported");
                }
              }
            }
          }
        }
      }
      parseSpecies(g, element);

      adjustModificationCoordinates(element);
      return element;
    } catch (final InstantiationException | IllegalAccessException | IllegalArgumentException
                   | InvocationTargetException | NoSuchMethodException | SecurityException e) {
      logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, g, model.getIdModel()), "Cannot create element", e);
      return null;
    }
  }

  /**
   * Method used to create a new alias from SBGN-ML glyph.
   *
   * @param glyph   SBGN-ML glyph representing the alias
   * @param species species of the alias
   */
  private void parseSpecies(final Glyph glyph, final Element species) {
    if (glyph.getExtension() != null) {
      for (final org.w3c.dom.Element element : glyph.getExtension().getAny()) {
        parseExtensionNode(element, species);
      }
    }

    species.setHeight(new Double(glyph.getBbox().getH()));
    species.setWidth(new Double(glyph.getBbox().getW()));
    species.setX(new Double(glyph.getBbox().getX()));
    species.setY(new Double(glyph.getBbox().getY()));
    if (species.getNameX() == null || species.getNameY() == null) {
      species.setNameX(species.getX());
      species.setNameY(species.getY());
      species.setNameWidth(species.getWidth());
      species.setNameHorizontalAlign(HorizontalAlign.CENTER);
      if (species instanceof Complex) {
        species.setNameVerticalAlign(VerticalAlign.BOTTOM);
        species.setNameHeight(species.getHeight() - 2);
      } else {
        species.setNameHeight(species.getHeight());
        species.setNameVerticalAlign(VerticalAlign.MIDDLE);
      }
    }

    Compartment parentCompartment = null;
    if (glyph.getCompartmentRef() != null) {
      final Glyph compartmentGlyph = (Glyph) glyph.getCompartmentRef();
      parentCompartment = model.getElementByElementId(compartmentGlyph.getId());
    }
    if (parentCompartment != null) {
      species.setCompartment(parentCompartment);
      parentCompartment.addElement(species);
    }

    // Parse units of information
    for (final Glyph child : glyph.getGlyph()) {
      if (GlyphClazz.fromClazz(child.getClazz()).equals(GlyphClazz.UNIT_OF_INFORMATION)) {
        if (species instanceof Species) {
          parseUnitOfInformation(child, (Species) species);
        }
      }
    }

    renderParser.assignRenderInformation(species, glyph);
  }

  private boolean isAntisenseRNA(final Glyph g) {
    boolean isAntiseneRna = false;
    // Check all the children nodes looking for unit of information
    final List<Glyph> children = g.getGlyph();
    for (final Glyph child : children) {
      if (GlyphClazz.fromClazz(child.getClazz()) == GlyphClazz.UNIT_OF_INFORMATION) {
        if (child.getLabel().getText().toLowerCase().contains("antisenserna")) {
          isAntiseneRna = true;
        }
      }
    }

    return isAntiseneRna;
  }

  /**
   * Method used to decide if Nucleic-acid feature should be translated to RNA.
   *
   * @param g Nucleic-acid feature glyph
   * @return true if input is RNA
   */
  private boolean isRNA(final Glyph g) {
    boolean isRna = false;
    // Check all the children nodes looking for unit of information
    final List<Glyph> children = g.getGlyph();
    for (final Glyph child : children) {
      if (GlyphClazz.fromClazz(child.getClazz()) == GlyphClazz.UNIT_OF_INFORMATION) {
        if (child.getLabel().getText().toLowerCase().contains("rna")) {
          isRna = true;
        }
      }
    }

    return isRna;
  }

  /**
   * Method used to retrieve multimer cardinality from a multimer glyph.
   *
   * @param g multimer glyph
   * @return multimer cardinality
   * @throws Exception Exception is thrown if no proper unit of information with
   *                   cardinality was found
   */
  private int getMultimerCardinality(final Glyph g) throws Exception {
    int multimerCardinality = 0;
    // Check all the children nodes looking for unit of information with
    // cardinality
    final List<Glyph> children = g.getGlyph();
    for (final Glyph child : children) {
      if (GlyphClazz.fromClazz(child.getClazz()) == GlyphClazz.UNIT_OF_INFORMATION) {
        final String[] splitLabel = child.getLabel().getText().split(":");
        multimerCardinality = Integer.parseInt(splitLabel[1]);
      }
    }
    // If no unit of information was found, or the cardinality is invalid,
    // raise exception
    if (multimerCardinality <= 0) {
      throw new InvalidArgumentException(
          "No proper unit of information with multimer cardinality was found." + " Glyph: " + g.getId());
    }

    return multimerCardinality;
  }

  /**
   * Method used to parse state variable.
   *
   * @param stateVariable unit of information glyph from sbgn-ml file
   */
  private Residue stateVariableToResidue(final Glyph stateVariable) {
    if (stateVariable.getState() != null && (stateVariable.getState().getVariable() == null
        || stateVariable.getState().getVariable().trim().isEmpty())) {
      return null;

    }

    final Residue mr = new Residue();

    mr.setIdModificationResidue(stateVariable.getId());
    if (stateVariable.getState() != null) {
      // If State variable consists of value and variable
      mr.setName(stateVariable.getState().getVariable());
      for (final ModificationState ms : ModificationState.values()) {
        if (ms.getAbbreviation().equals(stateVariable.getState().getValue())) {
          mr.setState(ms);
        }
      }
    }

    assignCoordinates(stateVariable, mr);

    renderParser.assignRenderInformation(mr, stateVariable);

    return mr;
  }

  private void assignCoordinates(final Glyph glyph, final ModificationResidue mr) {
    mr.setX(glyph.getBbox().getX());
    mr.setY(glyph.getBbox().getY());
    mr.setWidth(glyph.getBbox().getW());
    mr.setHeight(glyph.getBbox().getH());
    mr.setNameX((double) glyph.getBbox().getX());
    mr.setNameY((double) glyph.getBbox().getY());
    mr.setNameWidth((double) glyph.getBbox().getW());
    mr.setNameHeight((double) glyph.getBbox().getH());
  }

  private StructuralState createStructuralState(final Glyph glyph) {
    final StructuralState structuralState = new StructuralState();

    structuralState.setFontSize(10);
    String result = "";
    if (glyph.getState().getValue() != null) {
      result += glyph.getState().getValue();
    }
    if (glyph.getState().getVariable() != null && !glyph.getState().getVariable().isEmpty()) {
      result += "@";
      result += glyph.getState().getVariable();
    }

    structuralState.setName(result);

    assignCoordinates(glyph, structuralState);

    renderParser.assignRenderInformation(structuralState, glyph);

    return structuralState;
  }

  private void parseExtensionNode(final org.w3c.dom.Element node, final Element element) {
    if (node.getNodeName().equals("annotation")) {
      parseAnnotationNode(node, element);
    } else if (node.getNodeName().equals(NewtExtension.IGNORED_NWT_EXTRA_INFO_TAG)) {
      logger.debug("Ignore tag {}", node.getNodeName());
    } else {
      logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, element),
          "Unknown extension: " + node.getNodeName());
    }
  }

  private void parseAnnotationNode(final org.w3c.dom.Element node, final Element element) {
    final Node rdf = XmlParser.getNode("rdf:RDF", node);
    if (rdf == null) {
      logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, element),
          "No rdf node provided in annotation extension");
    } else {
      try {
        element.addMiriamData(
            new XmlAnnotationParser().parseRdfNode(rdf, new LogMarker(ProjectLogEntryType.PARSING_ISSUE, element)));
      } catch (final InvalidXmlSchemaException e) {
        logger.error(e, e);
      }
    }
  }

  /**
   * {@link ModificationResidue} in element might have slightly off coordinates
   * (due to different symbol shapes). For that we need to align them to match
   * our model.
   */
  protected void adjustModificationCoordinates(final Element species) {
    if (species instanceof SpeciesWithModificationResidue) {
      final SpeciesWithModificationResidue protein = (SpeciesWithModificationResidue) species;
      final CellDesignerAliasConverter converter = new CellDesignerAliasConverter(species, true);
      for (final ModificationResidue mr : protein.getModificationResidues()) {
        final double angle = converter.getAngleForPoint(species, mr.getCenter());
        final Point2D point = converter.getResidueCoordinates(species, angle);
        if (mr instanceof Residue) {
          mr.setWidth(Residue.DEFAULT_SIZE);
          mr.setHeight(Residue.DEFAULT_SIZE);
          mr.setNameWidth(Residue.DEFAULT_SIZE);
          mr.setNameHeight(Residue.DEFAULT_SIZE);
        }
        mr.setPosition(point.getX() - mr.getWidth() / 2, point.getY() - mr.getHeight() / 2);
        mr.setNameX(mr.getX());
        mr.setNameY(mr.getY());
      }

    }
  }

  /**
   * Method used for parsing units of information.
   *
   * @param unitOfInformationGlyph unit of information glyph from sbgn-ml file
   * @param alias                  alias that the unit of information concerns
   */
  private void parseUnitOfInformation(final Glyph unitOfInformationGlyph, final Species alias) {
    final String unitOfInformationText = unitOfInformationGlyph.getLabel().getText();
    if (unitOfInformationText.contains(":") && !unitOfInformationText.startsWith("N:")) {
      final String unitOfInformationPrefix = unitOfInformationText.substring(0, unitOfInformationText.indexOf(':'));
      final String unitOfInformationSuffix = unitOfInformationText.substring(unitOfInformationText.indexOf(':') + 1);
      alias.setStatePrefix(unitOfInformationPrefix);
      alias.setStateLabel(unitOfInformationSuffix);
    } else if (!unitOfInformationText.startsWith("N:")) {
      alias.setStateLabel(unitOfInformationText);
      alias.setStatePrefix("free input");
    }
  }

  public void adjustSizeOfElements(final Map map) {
    for (final Glyph g : map.getGlyph()) {
      final GlyphClazz glyphClazz = GlyphClazz.fromClazz(g.getClazz());
      if (glyphClazz != null) {
        switch (glyphClazz) {
          case SOURCE_AND_SINK:
            g.getBbox().setH(g.getBbox().getH() + 2 * CROSS_LINE_EXTENDED_LENGTH);
            g.getBbox().setW(g.getBbox().getW() + 2 * CROSS_LINE_EXTENDED_LENGTH);
            g.getBbox().setX(g.getBbox().getX() - CROSS_LINE_EXTENDED_LENGTH);
            g.getBbox().setY(g.getBbox().getY() - CROSS_LINE_EXTENDED_LENGTH);
            break;
          default:
            break;
        }
      }
    }
  }

}
