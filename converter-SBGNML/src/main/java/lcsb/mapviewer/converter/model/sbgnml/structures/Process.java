package lcsb.mapviewer.converter.model.sbgnml.structures;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.sbgn.bindings.Arc;
import org.sbgn.bindings.Glyph;
import org.sbgn.bindings.Port;

import lcsb.mapviewer.common.comparator.DoubleComparator;

/**
 * This class represents a reaction in the map.
 * 
 * @author Michał Kuźma
 *
 */
public class Process {

  /**
   * Central point of a reaction represented by a glyph.
   */
  private Glyph centralPoint;

  /**
   * Product arcs.
   */
  private List<Arc> productArcs = new ArrayList<Arc>();

  /**
   * Reagent arcs.
   */
  private List<Arc> reagentArcs = new ArrayList<Arc>();

  /**
   * Modifier arcs.
   */
  private List<Arc> modifierArcs = new ArrayList<Arc>();

  /**
   * Port for all reagents.
   */
  private Port reagentsPort;

  /**
   * Port for all products.
   */
  private Port productsPort;

  /**
   * Is the process reversible.
   */
  private boolean reversible;

  /**
   * Default constructor.
   * 
   * @param centralPoint
   *          central point of the process
   */
  public Process(final Glyph centralPoint) {
    this.reversible = false;
    this.productsPort = null;
    this.reagentsPort = null;
    this.centralPoint = centralPoint;
  }

  /**
   * Adds a product arc to the process.
   * 
   * @param a
   *          the arc to be added
   */
  public void addProductArc(final Arc a) {
    if (this.getCentralPoint().getPort().size() == 2) {
      double dx = a.getEnd().getX() - a.getStart().getX();
      double dy = a.getEnd().getY() - a.getStart().getY();
      double centralDx = this.getCentralPoint().getPort().get(0).getX()
          - this.getCentralPoint().getPort().get(1).getX();
      double centralDy = this.getCentralPoint().getPort().get(0).getY()
          - this.getCentralPoint().getPort().get(1).getY();
      DoubleComparator comparator = new DoubleComparator();
      if (comparator.compare(dy / dx, centralDy / centralDx) == 0) {
        productArcs.add(0, a);
      } else {
        productArcs.add(a);
      }
    } else {
      productArcs.add(a);
    }
  }

  /**
   * Adds a reagent arc to the process.
   * 
   * @param a
   *          the arc to be added
   */
  public void addReagentArc(final Arc a) {
    if (this.getCentralPoint().getPort().size() == 2) {
      double dx = a.getEnd().getX() - a.getStart().getX();
      double dy = a.getEnd().getY() - a.getStart().getY();
      double centralDx = this.getCentralPoint().getPort().get(0).getX()
          - this.getCentralPoint().getPort().get(1).getX();
      double centralDy = this.getCentralPoint().getPort().get(0).getY()
          - this.getCentralPoint().getPort().get(1).getY();
      DoubleComparator comparator = new DoubleComparator();
      if (comparator.compare(dy / dx, centralDy / centralDx) == 0) {
        reagentArcs.add(0, a);
      } else {
        reagentArcs.add(a);
      }
    } else {
      reagentArcs.add(a);
    }
  }

  /**
   * Adds a influence arc to the process.
   * 
   * @param a
   *          the arc to be added
   */
  public void addInfluenceArc(final Arc a) {
    modifierArcs.add(a);
  }

  /**
   * @return the reagentArcs
   * @see #reagentArcs
   */
  public List<Arc> getReagentArcs() {
    return reagentArcs;
  }

  /**
   * @return the productArcs
   * @see #productArcs
   */
  public List<Arc> getProductArcs() {
    return productArcs;
  }

  /**
   * @return the centralPoint
   * @see #centralPoint
   */
  public Glyph getCentralPoint() {
    return centralPoint;
  }

  /**
   * @param centralPoint
   *          the centralPoint to set
   * @see #centralPoint
   */
  public void setCentralPoint(final Glyph centralPoint) {
    this.centralPoint = centralPoint;
  }

  /**
   * @return the modifierArcs
   * @see #modifierArcs
   */
  public List<Arc> getModifierArcs() {
    return modifierArcs;
  }

  /**
   * @return the reagentsPort
   * @see #reagentsPort
   */
  public Port getReagentsPort() {
    return reagentsPort;
  }

  /**
   * @param reagentsPort
   *          the reagentsPort to set
   * @see #reagentsPort
   */
  public void setReagentsPort(final Port reagentsPort) {
    this.reagentsPort = reagentsPort;
  }

  /**
   * @return the productsPort
   * @see #productsPort
   */
  public Port getProductsPort() {
    return productsPort;
  }

  /**
   * @param productsPort
   *          the productsPort to set
   * @see #productsPort
   */
  public void setProductsPort(final Port productsPort) {
    this.productsPort = productsPort;
  }

  /**
   * @return the reversible
   * @see #reversible
   */
  public boolean isReversible() {
    return reversible;
  }

  /**
   * @param reversible
   *          the reversible to set
   * @see #reversible
   */
  public void setReversible(final boolean reversible) {
    this.reversible = reversible;
  }

  /**
   * Method used to retrieve product arcs in reversible process.
   * 
   * @return list of product arcs
   */
  public List<Arc> getRevProductArcs() {
    List<Arc> result = productArcs.stream().filter(a -> a.getSource().equals(productsPort))
        .collect(Collectors.toList());
    return result;
  }

  /**
   * Method used to retrieve reagent arcs in reversible process.
   * 
   * @return list of reagent arcs
   */
  public List<Arc> getRevReagentArcs() {
    List<Arc> result = productArcs.stream().filter(a -> a.getSource().equals(reagentsPort))
        .collect(Collectors.toList());
    return result;
  }

}
