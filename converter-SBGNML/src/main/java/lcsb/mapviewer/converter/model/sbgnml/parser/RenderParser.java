package lcsb.mapviewer.converter.model.sbgnml.parser;

import java.awt.Color;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbgn.RenderUtil;
import org.sbgn.bindings.Arc;
import org.sbgn.bindings.ColorDefinition;
import org.sbgn.bindings.Glyph;
import org.sbgn.bindings.LinearGradient;
import org.sbgn.bindings.RenderInformation;
import org.sbgn.bindings.Style;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

public class RenderParser {

  private Logger logger = LogManager.getLogger();

  private ColorParser colorParser = new ColorParser();

  private RenderInformation renderInformation;

  public RenderParser(final RenderInformation renderInformation) {
    this.renderInformation = renderInformation;
  }

  public Color extractColor(final String fill) {
    Color color = getRenderColor(fill);
    if (color == null) {
      color = colorParser.parse(fill);
    }
    return color;
  }

  private Color getRenderColor(final String color) {
    ColorDefinition colorDefinition = RenderUtil.getColorDefinition(renderInformation, color);
    if (colorDefinition != null) {
      return colorParser.parse(colorDefinition.getValue());
    }
    LinearGradient gradient = RenderUtil.getGradient(renderInformation, color);
    if (gradient != null) {
      if (gradient.getStop().size() > 0) {
        return colorParser.parse(gradient.getStop().get(0).getStopColor());
      }
    }

    return null;
  }

  public void assignRenderInformation(final ModificationResidue element, final Glyph glyph) {
    if (renderInformation != null) {
      Style style = RenderUtil.getStyle(renderInformation, glyph);
      if (style != null) {
        try {
          element.setBorderColor(extractColor(style.getG().getStroke()));
        } catch (final InvalidArgumentException e) {
          logger.warn("Invalid border color: " + style.getG().getStroke());
        }
      }
    }
    if (element.getBorderColor() == null) {
      element.setBorderColor(Color.BLACK);
    }
  }

  public void assignRenderInformation(final Element element, final Glyph glyph) {
    if (renderInformation != null) {
      Style style = RenderUtil.getStyle(renderInformation, glyph);
      if (style != null) {
        try {
          element.setFillColor(extractColor(style.getG().getFill()));
        } catch (final InvalidArgumentException e) {
          logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, element),
              "Invalid fill color: " + style.getG().getFill());
        }
        try {
          element.setBorderColor(extractColor(style.getG().getStroke()));
        } catch (final InvalidArgumentException e) {
          logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, element),
              "Invalid border color: " + style.getG().getStroke());
        }
        if (style.getG().getStrokeWidth() != null) {
          if (element instanceof Species) {
            ((Species) element).setLineWidth((double) style.getG().getStrokeWidth());
          } else if (element instanceof Compartment) {
            ((Compartment) element).setThickness(style.getG().getStrokeWidth());
          }
        }
      }
    }
  }

  public void assignRenderInformation(final PolylineData line, final Glyph glyph) {
    if (renderInformation != null) {
      Style style = RenderUtil.getStyle(renderInformation, glyph);
      assignRenderInformation(line, style);
    }
  }

  public void assignRenderInformation(final PolylineData line, final Arc glyph) {
    if (renderInformation != null) {
      Style style = RenderUtil.getStyle(renderInformation, glyph);
      assignRenderInformation(line, style);
    }
  }

  public void assignRenderInformation(final PolylineData line, final Style style) {
    if (style != null) {
      try {
        line.setColor(extractColor(style.getG().getStroke()));
      } catch (final InvalidArgumentException e) {
        logger.warn("Invalid line color: " + style.getG().getStroke());
      }
      if (style.getG().getStrokeWidth() != null) {
        double lineWidth = style.getG().getStrokeWidth();
        line.setWidth(lineWidth);
      }
    }
  }

}
