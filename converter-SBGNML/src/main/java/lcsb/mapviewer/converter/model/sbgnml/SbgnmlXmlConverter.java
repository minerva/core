package lcsb.mapviewer.converter.model.sbgnml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbgn.SbgnUtil;
import org.sbgn.bindings.Sbgn;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.common.MinervaLoggerAppender;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.modelutils.map.LogFormatter;

@Component
public class SbgnmlXmlConverter extends Converter {

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  @Override
  public Model createModel(final ConverterParams params) throws InvalidInputDataExecption, ConverterException {
    SbgnmlXmlParser parser = new SbgnmlXmlParser();
    File inputFile = null;
    try {
      inputFile = createFileWithModifiedStyleIdList(params.getInputStream());
      return parser.createModel(params.getFilename(), inputFile);
    } catch (final IOException e) {
      throw new ConverterException(e.getMessage(), e);
    } finally {
      assert inputFile == null || inputFile.delete();
    }
  }

  private File createFileWithModifiedStyleIdList(final InputStream inputStream) throws ConverterException, IOException {

    try {
      TransformerFactory factory = TransformerFactory.newInstance();

      ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

      // Use the factory to create a template containing the xsl file that deals
      // with
      // these issues:
      // https://github.com/iVis-at-Bilkent/newt/issues/536
      // https://git-r3lab.uni.lu/minerva/core/issues/1127
      Templates template = factory
          .newTemplates(new StreamSource(classLoader.getResourceAsStream("rename_id_list_attribute.xsl")));

      Transformer xformer = template.newTransformer();

      ByteArrayOutputStream output = new ByteArrayOutputStream();
      Source source = new StreamSource(inputStream);
      Result result = new StreamResult(output);

      xformer.transform(source, result);
      return inputStream2File(output.toInputStream());

    } catch (final Exception e) {
      throw new ConverterException(e.getMessage(), e);
    }
  }

  @Override
  public String model2String(final Model model) throws ConverterException {
    MinervaLoggerAppender appender = MinervaLoggerAppender.createAppender();
    Sbgn exportedData;
    StringBuilder notes = new StringBuilder();
    try {
      SbgnmlXmlExporter exporter = new SbgnmlXmlExporter();
      exportedData = exporter.toSbgnml(model);
      if (!appender.getWarnings().isEmpty()) {
        for (final String entry : new LogFormatter().createFormattedWarnings(appender.getWarnings())) {
          notes.append("\n" + entry);
        }
      }
    } finally {
      MinervaLoggerAppender.unregisterLogEventStorage(appender);
    }
    try {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      SbgnUtil.writeTo(exportedData, baos);

      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
      factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
      Document input = factory
          .newDocumentBuilder()
          .parse(baos.toInputStream());

      if (notes.length() > 0) {
        Element bodyNode = input.createElement("body");
        bodyNode.setAttribute("xmlns", "http://www.w3.org/1999/xhtml");
        bodyNode.setTextContent(notes.toString());

        Element notesNode = input.createElement("notes");
        notesNode.appendChild(bodyNode);

        input.getFirstChild().insertBefore(notesNode, input.getFirstChild().getFirstChild());
      }

      TransformerFactory transformerFactory = TransformerFactory.newInstance();
      transformerFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
      Transformer xformer = transformerFactory.newTransformer();
      xformer.setOutputProperty(OutputKeys.INDENT, "yes");
      StringWriter output = new StringWriter();
      xformer.transform(new DOMSource(input), new StreamResult(output));

      return output.toString();
    } catch (final Exception e) {
      throw new ConverterException(e.getMessage(), e);
    }
  }

  @Override
  public String getCommonName() {
    return "SBGN-ML";
  }

  @Override
  public MimeType getMimeType() {
    return MimeType.TEXT;
  }

  @Override
  public List<String> getFileExtensions() {
    return Arrays.asList("sbgn");
  }

}
