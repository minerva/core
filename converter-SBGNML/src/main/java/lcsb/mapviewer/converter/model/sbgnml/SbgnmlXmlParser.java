package lcsb.mapviewer.converter.model.sbgnml;

import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.ZIndexPopulator;
import lcsb.mapviewer.converter.annotation.XmlAnnotationParser;
import lcsb.mapviewer.converter.model.sbgnml.parser.ElementParser;
import lcsb.mapviewer.converter.model.sbgnml.parser.NotesConverter;
import lcsb.mapviewer.converter.model.sbgnml.parser.ReactionParser;
import lcsb.mapviewer.converter.model.sbgnml.parser.RenderParser;
import lcsb.mapviewer.converter.model.sbgnml.structures.Process;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbgn.GlyphClazz;
import org.sbgn.RenderUtil;
import org.sbgn.SbgnUtil;
import org.sbgn.bindings.Arc;
import org.sbgn.bindings.Arc.End;
import org.sbgn.bindings.Arc.Next;
import org.sbgn.bindings.Arc.Start;
import org.sbgn.bindings.Bbox;
import org.sbgn.bindings.Glyph;
import org.sbgn.bindings.Map;
import org.sbgn.bindings.RenderInformation;
import org.sbgn.bindings.Sbgn;
import org.w3c.dom.Node;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is a parser for SBGN-ML files.
 *
 * @author Michał Kuźma
 */
public class SbgnmlXmlParser {

  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();

  /**
   * List of all arcs that should be translated as activity flow reactions.
   */
  private final List<Arc> activityFlowArcs = new ArrayList<>();

  private Model model;

  private RenderInformation renderInformation;

  private RenderParser renderParser;

  private ElementParser elementParser;

  private ReactionParser reactionParser;

  private final NotesConverter notesConverter = new NotesConverter();

  /**
   * Method used to create a model from SBGN-ML file.
   *
   * @param filename        The filename of the input file.
   * @param inputSbgnmlFile the input file.
   * @return valid model parsed from the file
   * @throws InvalidInputDataExecption thrown when input file is invalid
   */
  public Model createModel(final String filename, final File inputSbgnmlFile) throws InvalidInputDataExecption {

    model = new ModelFullIndexed(null);
    if (filename != null) {
      model.setIdModel(FilenameUtils.getBaseName(filename).replaceAll("-", "_"));
    }

    Sbgn sbgnData;
    Map map = null;
    // Check if input file has valid SBGN-ML data
    try {
      if (!SbgnUtil.isValid(inputSbgnmlFile)) {
        logger.warn("Given input is not a valid SBGN-ML file.");
      }

      // Read data from file
      sbgnData = SbgnUtil.readFromFile(inputSbgnmlFile);
      renderInformation = RenderUtil.getRenderInformation(sbgnData);
      // Extract map with all the glyphs and arcs
      map = sbgnData.getMap().get(0);
      if (renderInformation == null) {
        renderInformation = RenderUtil.getRenderInformation(map);
      }

    } catch (final Exception ex) {
      throw new InvalidInputDataExecption("Unable to read given file.", ex);
    }
    if (map.getExtension() != null) {
      for (org.w3c.dom.Element element : map.getExtension().getAny()) {
        parseExtensionNode(element, model);
      }
    }

    renderParser = new RenderParser(renderInformation);
    elementParser = new ElementParser(renderParser, model);
    reactionParser = new ReactionParser(renderParser, model);

    model.setNotes(notesConverter.parseNotes(map.getNotes()));

    elementParser.adjustSizeOfElements(map);

    setModelSize(map, model);

    // Iterate over every glyph
    for (final Glyph glyph : map.getGlyph()) {
      if (elementParser.isElementGlyph(glyph)) {
        Element element = elementParser.glyphToElement(glyph);
        if (element != null) {
          model.addElement(element);
          if (element instanceof Complex) {
            model.addElements(((Complex) element).getAllChildren());
          }
        }
      } else if (reactionParser.isReactionNodeGlyph(glyph)) {
        reactionParser.parseReactionNodeGlyph(glyph);
      } else if (reactionParser.isReactionGlyph(glyph)) {
        reactionParser.parseReactionGlyph(glyph);
      } else if (elementParser.isModificationGlyph(glyph)) {
        logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, glyph, model), "Modification glyph is not allowed here");
      }
    }

    // Iterate over every arc in the map
    for (final Arc a : map.getArc()) {
      if (reactionParser.isActivityFlowArc(a)) {
        logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, a, model),
            "Activity flow reaction found in PD");
        activityFlowArcs.add(a);
      } else {
        Reaction reaction = reactionParser.parseArc(a);
        if (reaction != null) {
          model.addReaction(reaction);
        }
      }
    }

    // Parse all processes created before
    for (final Process process : reactionParser.getProcesses()) {
      try {
        Reaction reaction = reactionParser.parseProcess(process);
        if (reaction != null) {
          model.addReaction(reaction);
        }
      } catch (final Exception e) {
        throw new InvalidInputDataExecption("Unable to parse the process: " + process.getCentralPoint().getId(), e);
      }
    }

    for (final Arc arc : activityFlowArcs) {
      try {
        Reaction reaction = reactionParser.createActivityFlowReaction(arc);
        if (reaction != null) {
          model.addReaction(reaction);
        }
      } catch (Exception e) {
        throw new InvalidInputDataExecption("Unable to parse the reaction: " + arc.getId(), e);
      }
    }

    for (final Element element : model.getElements()) {
      if (element.getCompartment() == null) {
        Compartment parent = null;
        if (element instanceof Species && ((Species) element).getComplex() == null) {
          parent = findParentCompartment(element, model);
        } else if (element instanceof Compartment) {
          parent = findParentCompartment(element, model);
        }
        if (parent != null) {
          parent.addElement(element);
        }
      }
      if (element instanceof Complex && ((Complex) element).getElements().isEmpty()) {
        element.setNameVerticalAlign(VerticalAlign.MIDDLE);
      }
    }

    new ZIndexPopulator().populateZIndex(model);

    return model;
  }

  /**
   * Method used to compute height and width of the model.
   *
   * @param map   Map parsed from SBGN-ML file
   * @param model Model to be updated
   */
  private void setModelSize(final Map map, final Model model) {
    if (map.getBbox() != null) {
      model.setWidth(map.getBbox().getX() + map.getBbox().getW());
      model.setHeight(map.getBbox().getY() + map.getBbox().getH());
      return;
    }
    double minX = Double.MAX_VALUE;
    double minY = Double.MAX_VALUE;
    double maxX = 0;
    double maxY = 0;

    // Iterate over every glyph
    for (final Glyph g : map.getGlyph()) {
      if (GlyphClazz.fromClazz(g.getClazz()).equals(GlyphClazz.ANNOTATION)
          || GlyphClazz.fromClazz(g.getClazz()).equals(GlyphClazz.TAG)) {
        continue;
      }
      Bbox bbox = g.getBbox();
      if (bbox != null) {
        if (bbox.getX() < minX) {
          minX = bbox.getX();
        }
        if (bbox.getY() < minY) {
          minY = bbox.getY();
        }
        if (bbox.getX() + bbox.getW() > maxX) {
          maxX = bbox.getX() + bbox.getW();
        }
        if (bbox.getY() + bbox.getH() > maxY) {
          maxY = bbox.getY() + bbox.getH();
        }
      }
    }

    // Iterate over every arc
    for (final Arc a : map.getArc()) {
      // Since submaps are not yet supported, the arcs connecting them or
      // tags cannot be used when computing width and height of the model.
      boolean connectsTagOrSubmap = false;
      if (a.getSource() instanceof Glyph) {
        Glyph sourceGlyph = (Glyph) a.getSource();
        if (GlyphClazz.fromClazz(sourceGlyph.getClazz()).equals(GlyphClazz.TAG)
            || GlyphClazz.fromClazz(sourceGlyph.getClazz()).equals(GlyphClazz.SUBMAP)) {
          connectsTagOrSubmap = true;
        }
      }
      if (a.getTarget() instanceof Glyph) {
        Glyph targetGlyph = (Glyph) a.getTarget();
        if (GlyphClazz.fromClazz(targetGlyph.getClazz()).equals(GlyphClazz.TAG)
            || GlyphClazz.fromClazz(targetGlyph.getClazz()).equals(GlyphClazz.SUBMAP)) {
          connectsTagOrSubmap = true;
        }
      }
      // If the arc is not connecting a submap or a tag, check it's points
      if (!connectsTagOrSubmap) {
        Start start = a.getStart();
        if (start.getX() < minX) {
          minX = start.getX();
        }
        if (start.getX() > maxX) {
          maxX = start.getX();
        }
        if (start.getY() < minY) {
          minY = start.getY();
        }
        if (start.getY() > maxY) {
          maxY = start.getY();
        }
        End end = a.getEnd();
        if (end.getX() < minX) {
          minX = end.getX();
        }
        if (end.getX() > maxX) {
          maxX = end.getX();
        }
        if (end.getY() < minY) {
          minY = end.getY();
        }
        if (end.getY() > maxY) {
          maxY = end.getY();
        }
        for (final Next next : a.getNext()) {
          if (next.getX() < minX) {
            minX = next.getX();
          }
          if (next.getX() > maxX) {
            maxX = next.getX();
          }
          if (next.getY() < minY) {
            minY = next.getY();
          }
          if (next.getY() > maxY) {
            maxY = next.getY();
          }
        }
      }
    }

    if (minX > maxX) {
      minX = maxX;
      maxX = 1;
    }
    if (minY > maxY) {
      minY = maxY;
      maxY = 1;
    }

    double width = maxX + minX;
    double height = maxY + minY;
    if (map.getBbox() != null) {
      width = Math.max(width, map.getBbox().getW());
      height = Math.max(height, map.getBbox().getH());
    }
    model.setWidth(width);
    model.setHeight(height);
  }

  /**
   * Finds a compartment where element should be located (base on the
   * coordinates).
   *
   * @param child {@link Element} for which we want to find compartment
   * @param model {@link Model} where we look for a compartment
   * @return parent {@link Compartment}
   */
  private Compartment findParentCompartment(final Element child, final Model model) {
    Compartment nullParent = new Compartment("null");
    nullParent.setWidth(Double.MAX_VALUE);
    nullParent.setHeight(Double.MAX_VALUE);
    nullParent.setX(0.0);
    nullParent.setY(0.0);
    nullParent.setNameX(10);
    nullParent.setNameY(10);
    nullParent.setNameWidth(Double.MAX_VALUE);
    nullParent.setNameHeight(Double.MAX_VALUE);
    nullParent.setNameHorizontalAlign(HorizontalAlign.LEFT);
    nullParent.setNameVerticalAlign(VerticalAlign.TOP);
    Compartment parent = nullParent;
    for (final Compartment potentialParent : model.getCompartments()) {
      if (!potentialParent.equals(child)) {
        if (potentialParent.contains(child)) {
          if (parent.getSize() > potentialParent.getSize()) {
            parent = potentialParent;
          }
        }
      }
    }
    if (parent != nullParent) {
      return parent;
    } else {
      return null;
    }
  }

  private void parseExtensionNode(final org.w3c.dom.Element node, final Model map) {
    if (node.getNodeName().equals("annotation")) {
      parseAnnotationNode(node, map);
    }
  }

  private void parseAnnotationNode(final org.w3c.dom.Element node, final Model map) {
    LogMarker logMarker = new LogMarker(ProjectLogEntryType.PARSING_ISSUE, null, null, map.getName());
    Node rdf = XmlParser.getNode("rdf:RDF", node);
    if (rdf == null) {
      logger.warn(logMarker, "No rdf node provided in annotation extension");
    } else {
      try {
        XmlAnnotationParser parser = new XmlAnnotationParser();
        for (MiriamData md : parser.parseRdfNode(rdf, logMarker)) {
          map.addMiriamData(md);
        }
        map.addAuthors(parser.getAuthorsFromRdf(rdf));
        map.addModificationDates(parser.getModificationDatesFromRdf(rdf));
        map.setCreationDate(parser.getCreateDateFromRdf(rdf));
      } catch (final InvalidXmlSchemaException e) {
        logger.error(e, e);
      }
    }
  }

}
