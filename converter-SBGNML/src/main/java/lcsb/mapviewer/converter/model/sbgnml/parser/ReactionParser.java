package lcsb.mapviewer.converter.model.sbgnml.parser;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.model.celldesigner.geometry.helper.PolylineDataFactory;
import lcsb.mapviewer.converter.model.celldesigner.types.ModifierType;
import lcsb.mapviewer.converter.model.sbgnml.SbgnLogMarker;
import lcsb.mapviewer.converter.model.sbgnml.structures.Process;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.graphics.ArrowType;
import lcsb.mapviewer.model.graphics.ArrowTypeData;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.modifier.Inhibition;
import lcsb.mapviewer.model.map.modifier.Modulation;
import lcsb.mapviewer.model.map.modifier.PhysicalStimulation;
import lcsb.mapviewer.model.map.modifier.Trigger;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.OrOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.CatalysisReaction;
import lcsb.mapviewer.model.map.reaction.type.KnownTransitionOmittedReaction;
import lcsb.mapviewer.model.map.reaction.type.NegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.PositiveInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedPhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedTriggerReaction;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownReducedModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownTransitionReaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import org.sbgn.ArcClazz;
import org.sbgn.GlyphClazz;
import org.sbgn.bindings.Arc;
import org.sbgn.bindings.Arc.Next;
import org.sbgn.bindings.Glyph;
import org.sbgn.bindings.Port;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class ReactionParser extends GlyphParser {

  /**
   * List of all logic operator glyphs parsed so far.
   */
  private List<Glyph> logicOperators = new ArrayList<>();

  /**
   * List of all processes to be parsed.
   */
  private List<Process> processes = new ArrayList<>();

  /**
   * List of all logic arcs parsed so far.
   */
  private List<Arc> logicArcs = new ArrayList<>();

  private final PointTransformation pt = new PointTransformation();

  private RenderParser renderParser;

  private Model model;

  public ReactionParser(final RenderParser renderParser, final Model model) {
    this.renderParser = renderParser;
    this.model = model;
  }

  public void parseReactionNodeGlyph(final Glyph glyph) {
    if (!isReactionNodeGlyph(glyph)) {
      return;
    }
    switch (GlyphClazz.fromClazz(glyph.getClazz())) {
      case AND:
      case NOT:
      case OR:
        logicOperators.add(glyph);
        break;
      default:
        logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, glyph, model), "Reaction node glyph is not supported.");

    }
  }

  public void parseReactionGlyph(final Glyph glyph) {
    if (!isReactionGlyph(glyph)) {
      return;
    }
    switch (GlyphClazz.fromClazz(glyph.getClazz())) {
      case ASSOCIATION:
      case DISSOCIATION:
      case INTERACTION:
      case OMITTED_PROCESS:
      case PROCESS:
      case UNCERTAIN_PROCESS:
        processes.add(new Process(glyph));
        break;
      default:
        logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, glyph, model), "Reaction glyph is not supported.");

    }
  }

  public List<Process> getProcesses() {
    return processes;
  }

  public List<Glyph> getLogicOperators() {
    return logicOperators;
  }

  /**
   * Returns proper Reaction object based on given glyph clazz.
   *
   * @param glyphClazz clazz of the process glyph
   * @return Reaction object based on given glyph clazz
   */
  Reaction getReactionFromProcessGlyphClazz(final String glyphClazz, final String elementId) {
    switch (GlyphClazz.fromClazz(glyphClazz)) {
      case INTERACTION:
        return new KnownTransitionOmittedReaction(elementId);
      case ASSOCIATION:
      case DISSOCIATION:
      case PROCESS:
        return new StateTransitionReaction(elementId);
      case OMITTED_PROCESS:
        return new KnownTransitionOmittedReaction(elementId);
      case UNCERTAIN_PROCESS:
        return new UnknownTransitionReaction(elementId);
      default:
        throw new InvalidArgumentException();

    }
  }

  /**
   * Method used for parsing processes into reactions.
   *
   * @param p process to be parsed
   * @throws Exception thrown when the process was invalid
   */
  public Reaction parseProcess(final Process p) throws Exception {
    if (p.getProductArcs().isEmpty()) {
      logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, p, model),
              "The process must have at least one outgoing arc.");
      return null;
    }

    p.setProductsPort(getSourcePort(p.getProductArcs().get(0)));

    if (p.getProductArcs().size() >= 2 && p.getReagentArcs().size() == 0) {
      p.setReversible(true);
    }
    Set<Port> ports = new HashSet<>();
    for (final Arc a : p.getProductArcs()) {
      ports.add(getTargetPort(a));
    }
    for (final Arc a : p.getReagentArcs()) {
      ports.add(getSourcePort(a));
    }
    if (ports.size() < 2) {
      logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, p, model), "Ports are not used in process");
    }

    if (p.getReagentsPort() == null && !p.getReagentArcs().isEmpty()) {
      Port port = getTargetPort(p.getReagentArcs().get(0));
      p.setReagentsPort(port);
    }

    if (p.getCentralPoint() == null) {
      throw new InvalidArgumentException("Process has no central point.");
    }
    Reaction reaction = getReactionFromProcessGlyphClazz(p.getCentralPoint().getClazz(), p.getCentralPoint().getId());

    reaction.setReversible(p.isReversible());

    // If there are multiple inputs, add "AND" operator
    AndOperator andOperator = null;
    if (p.getReagentArcs().size() > 1 || p.getRevReagentArcs().size() > 1) {
      andOperator = getReactionPortInputAndOperator(p);
      reaction.addNode(andOperator);
    }

    // If there are multiple outputs, add Split operator
    AndOperator splitOperator = null;
    if ((p.getProductArcs().size() > 1 && !p.isReversible()) || (p.getProductArcs().size() > 2)) {
      splitOperator = getReactionPortOutputOperator(p);
      reaction.addNode(splitOperator);
    }

    for (final Arc a : p.getReagentArcs()) {
      Glyph source = getSourceGlyph(a);
      Reactant reactant = new Reactant(model.getElementByElementId(source.getId()));
      reactant.setReaction(reaction);
      List<Point2D> pointList = getLinePoints(a);
      PolylineData line = parseLine(a, pointList);
      reactant.setLine(line);
      if (andOperator != null) {
        andOperator.addInput(reactant);
      }

      reaction.addReactant(reactant);
      renderParser.assignRenderInformation(reactant.getLine(), a);
    }

    for (final Arc a : p.getProductArcs()) {
      if (Objects.equals(getSourcePort(a), p.getProductsPort()) && ports.size() == 2
              || reaction.getProducts().size() == 0) {
        Glyph target = getTargetGlyph(a);
        Product product = new Product(model.getElementByElementId(target.getId()));
        List<Point2D> pointList = getLinePoints(a);
        PolylineData line = parseLine(a, pointList);
        product.setLine(line);

        if (splitOperator != null) {
          splitOperator.addOutput(product);
        }
        renderParser.assignRenderInformation(product.getLine(), a);
        reaction.addProduct(product);
      } else {
        Glyph source = (Glyph) a.getTarget();
        Reactant reactant = new Reactant(model.getElementByElementId(source.getId()));
        List<Point2D> pointList = getLinePoints(a);
        PolylineData line = parseLine(a, pointList);
        line = line.reverse();
        reactant.setLine(line);
        if (andOperator != null) {
          andOperator.addInput(reactant);
        }

        renderParser.assignRenderInformation(reactant.getLine(), a);
        reaction.addReactant(reactant);
      }
    }

    Product firstProduct = reaction.getProducts().get(0);
    boolean shouldReverse = reaction.getProducts().size() > 1;
    for (final Product product : reaction.getProducts()) {
      if (product.getLine().getStartPoint().distance(firstProduct.getLine().getStartPoint()) < Configuration.EPSILON
              && product.getLine().getEndPoint().distance(firstProduct.getLine().getEndPoint()) > Configuration.EPSILON) {
        shouldReverse = false;
      }
    }
    if (shouldReverse) {
      logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, "process-arcs", p.getCentralPoint().getId(),
              model.getIdModel()), "Product lines should be reversed");
      for (final Product product : reaction.getProducts()) {
        List<Line2D> lines = new ArrayList<>(product.getLine().getLines());
        product.getLine().removeLines();
        Collections.reverse(lines);
        for (final Line2D l : lines) {
          product.getLine().addLine(l.getP2(), l.getP1());
        }
      }
      splitOperator.getLine().setStartPoint(reaction.getProducts().get(0).getLine().getStartPoint());
    }
    if (reaction.getReactants().size() == 0) {
      logger.warn(new LogMarker(ProjectLogEntryType.PARSING_ISSUE, reaction), "At least one reactant is required");
      return null;
    }

    Point2D centerPointStart = reaction.getReactants().get(0).getLine().getEndPoint();
    if (andOperator != null) {
      centerPointStart = andOperator.getLine().getEndPoint();
    }

    Point2D centerPointEnd = reaction.getProducts().get(0).getLine().getStartPoint();
    if (splitOperator != null) {
      centerPointEnd = splitOperator.getLine().getEndPoint();
    }

    PolylineData centerLine = new PolylineData(pt.copyPoint(centerPointStart), pt.copyPoint(centerPointEnd));
    reaction.setLine(centerLine);

    for (final Arc arc : p.getModifierArcs()) {
      if (arc.getSource() instanceof Glyph) {
        Glyph sourceGlyph = (Glyph) arc.getSource();
        Species modifierElement = model.getElementByElementId(sourceGlyph.getId());

        try {
          Modifier modifier = getModifierFromArcClazz(arc, modifierElement);

          List<Point2D> pointList = getLinePoints(arc);
          PolylineData line = parseLine(arc, pointList);
          modifier.setLine(line);

          reaction.addModifier(modifier);
          renderParser.assignRenderInformation(modifier.getLine(), arc);
        } catch (final Exception ex) {
          logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, arc, model), "Unable to add modifier", ex);
          continue;
        }
      } else if (arc.getSource() instanceof Port) {
        // Logic operator
        try {
          parseLogicOperator(arc, reaction, null);
        } catch (final Exception ex) {
          logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, arc, model), ex.getMessage());
        }
      }
    }

    renderParser.assignRenderInformation(reaction.getLine(), p.getCentralPoint());
    if (andOperator != null) {
      renderParser.assignRenderInformation(andOperator.getLine(), p.getCentralPoint());
    }
    if (splitOperator != null) {
      renderParser.assignRenderInformation(splitOperator.getLine(), p.getCentralPoint());
    }
    return reaction;
  }

  private Port getTargetPort(final Arc a) {
    if (a.getTarget() instanceof Port) {
      return (Port) a.getTarget();
    } else if (a.getSource() instanceof Port) {
      return (Port) a.getSource();
    } else if (a.getTarget() instanceof Glyph && ((Glyph) a.getTarget()).getPort().size() > 0) {
      return ((Glyph) a.getTarget()).getPort().get(0);
    } else if (a.getSource() instanceof Glyph && ((Glyph) a.getSource()).getPort().size() > 0) {
      return ((Glyph) a.getSource()).getPort().get(0);
    } else {
      return null;
    }
  }

  private Port getSourcePort(final Arc a) {
    if (a.getSource() instanceof Port) {
      return (Port) a.getSource();
    } else if (a.getTarget() instanceof Port) {
      return (Port) a.getTarget();
    } else if (a.getSource() instanceof Glyph && ((Glyph) a.getSource()).getPort().size() > 0) {
      return ((Glyph) a.getSource()).getPort().get(0);
    } else if (a.getTarget() instanceof Glyph && ((Glyph) a.getTarget()).getPort().size() > 0) {
      return ((Glyph) a.getTarget()).getPort().get(0);
    } else {
      return null;
    }
  }

  /**
   * Returns {@link AndOperator} for the reaction's reagents port.
   *
   * @param p process of the reaction
   * @return operator for the reaction port
   */
  private AndOperator getReactionPortInputAndOperator(final Process p) {
    AndOperator andOperator = new AndOperator();
    Glyph centralPoint = p.getCentralPoint();
    Double centralPointX = new Double(centralPoint.getBbox().getX() + centralPoint.getBbox().getW() / 2);
    Double centralPointY = new Double(centralPoint.getBbox().getY() + centralPoint.getBbox().getH() / 2);
    Point2D centerOfReactionPoint = new Point2D.Double(centralPointX, centralPointY);
    Point2D portPoint;
    if (p.getReagentArcs().size() > 1) {
      portPoint = new Point2D.Double(p.getReagentArcs().get(0).getEnd().getX(),
              p.getReagentArcs().get(0).getEnd().getY());
    } else {
      portPoint = new Point2D.Double(p.getRevReagentArcs().get(0).getStart().getX(),
              p.getRevReagentArcs().get(0).getStart().getY());
    }
    PolylineData line = new PolylineData(portPoint, centerOfReactionPoint);
    ArrowTypeData atd = new ArrowTypeData();
    atd.setArrowType(ArrowType.NONE);
    atd.setArrowLineType(LineType.SOLID);
    line.setEndAtd(atd);
    andOperator.setLine(line);
    return andOperator;
  }

  /**
   * Returns {@link AndOperator} for the reaction's products port.
   *
   * @param p process of the reaction
   * @return operator for the reaction port
   */
  private AndOperator getReactionPortOutputOperator(final Process p) {
    AndOperator splitOperator = new AndOperator();
    Glyph centralPoint = p.getCentralPoint();
    Double centralPointX = new Double(centralPoint.getBbox().getX() + centralPoint.getBbox().getW() / 2);
    Double centralPointY = new Double(centralPoint.getBbox().getY() + centralPoint.getBbox().getH() / 2);
    Point2D centerOfReactionPoint = new Point2D.Double(centralPointX, centralPointY);
    Point2D portPoint;
    if (!p.isReversible()) {
      portPoint = new Point2D.Double(p.getProductArcs().get(0).getStart().getX(),
              p.getProductArcs().get(0).getStart().getY());
    } else {
      portPoint = new Point2D.Double(p.getRevProductArcs().get(0).getStart().getX(),
              p.getRevProductArcs().get(0).getStart().getY());
    }
    PolylineData line = new PolylineData(portPoint, centerOfReactionPoint);
    ArrowTypeData atd = new ArrowTypeData();
    atd.setArrowType(ArrowType.NONE);
    atd.setArrowLineType(LineType.SOLID);
    line.setEndAtd(atd);
    splitOperator.setLine(line);
    return splitOperator;
  }

  /**
   * Method used for parsing logic operators.
   *
   * @param arc            arc with a starting point in the logic operator
   * @param reaction       reaction that the logic operator is a part of
   * @param targetOperator target logic operator
   * @throws Exception thrown when parsed logic operator is invalid
   */
  private void parseLogicOperator(final Arc arc, final Reaction reaction, final NodeOperator targetOperator)
          throws Exception {
    Port operatorPort = (Port) arc.getSource();
    Glyph logicOperator = null;
    for (final Glyph lo : getLogicOperators()) {
      if (lo.getPort().contains(operatorPort)) {
        logicOperator = lo;
      }
    }
    if (logicOperator == null) {
      throw new InvalidArgumentException("Missing logic operator for logic arc: " + arc.getId());
    }

    // LogicOperator is valid for CellDesigner only if it has exactly 2
    // inputs of Species
    final Glyph tempLogicOperator = logicOperator;
    boolean isCellDesignerValidLogicOperator = logicArcs.stream()
            .filter(a -> tempLogicOperator.getPort().contains(a.getTarget()) && !(a.getSource() instanceof Port))
            .count() == 2;
    if (!isCellDesignerValidLogicOperator) {
      throw new InvalidArgumentException("Parsed operator is not valid for CellDesigner: " + logicOperator.getId());
    }

    NodeOperator operator;
    switch (GlyphClazz.fromClazz(logicOperator.getClazz())) {
      case AND:
        operator = new AndOperator();
        break;
      case OR:
        operator = new OrOperator();
        break;
      case NOT:
        logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, logicOperator, model),
                "NOT gates are not implemented in the platform.");
        return;
      default:
        throw new InvalidArgumentException("Wrong logic operator class.");
    }

    // Parse line from arc and operator glyph
    Point2D operatorCenterPoint = new Point2D.Double(
            logicOperator.getBbox().getX() + logicOperator.getBbox().getW() / 2,
            logicOperator.getBbox().getY() + logicOperator.getBbox().getH() / 2);

    List<Point2D> linePoints = getLinePoints(arc);
    new ArrayList<Point2D>();

    // Check if operator port is in the line from center point of the
    // operator. If so, remove that redundant point.
    double dx = linePoints.get(0).getX() - operatorCenterPoint.getX();
    double dy = linePoints.get(0).getY() - operatorCenterPoint.getY();
    double dx2;
    double dy2;
    if (arc.getNext().isEmpty()) {
      dx2 = linePoints.get(linePoints.size() - 1).getX() - linePoints.get(0).getX();
      dy2 = linePoints.get(linePoints.size() - 1).getY() - linePoints.get(0).getY();
    } else {
      dx2 = arc.getNext().get(0).getX() - linePoints.get(0).getX();
      dy2 = arc.getNext().get(0).getY() - linePoints.get(0).getY();
    }
    DoubleComparator doubleComparator = new DoubleComparator();
    if (doubleComparator.compare(dy / dx, dy2 / dx2) == 0) {
      linePoints.remove(0);
    }
    linePoints.add(0, operatorCenterPoint);

    PolylineData line = new PolylineData(linePoints);
    ArrowTypeData atd = getAtdFromArcClazz(ArcClazz.fromClazz(arc.getClazz()));

    atd.setArrowLineType(LineType.SOLID);
    line.setEndAtd(atd);

    operator.setLine(line);
    operator.setReaction(reaction);

    if (targetOperator != null) {
      operator.addOutput(targetOperator);
      targetOperator.addInput(operator);
    }

    for (final Arc logicArc : logicArcs) {
      if (logicOperator.getPort().contains(logicArc.getTarget())) {
        if (logicArc.getSource() instanceof Port) {
          // The arc has source in logic operator
          logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, logicArc, model),
                  "Logic operators trees are not compatible with CellDesigner. Therefore they are not supported.");
          continue;
          // parseLogicOperator(logicArc, reaction, modifierClass,
          // operator, model);
        } else {
          Glyph sourceGlyph = (Glyph) logicArc.getSource();
          Modifier modifier = getModifierFromArcClazz(arc, model.getElementByElementId(sourceGlyph.getId()));

          List<Point2D> pointList = getLinePoints(logicArc);
          pointList.add(operatorCenterPoint);
          PolylineData newLine = parseLine(logicArc, pointList);
          modifier.setLine(newLine);

          operator.addInput(modifier);

          reaction.addModifier(modifier);
        }
      }
    }
    reaction.addNode(operator);
  }

  /**
   * Returns instance of {@link Modifier} based on given {@link ArcClazz}.
   *
   * @param arc {@link ArcClazz} defining the result
   * @return {@link Modifier} of class adequate to given ac
   * @throws Exception thrown when no adequate Modifier has been found
   */
  private Modifier getModifierFromArcClazz(final Arc arc, final Element element) throws Exception {
    ArcClazz ac = ArcClazz.fromClazz(arc.getClazz());
    switch (ac) {
      case ABSOLUTE_INHIBITION:
        logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, arc, model),
                ac + " modifier is not supported. Changing to " + Inhibition.class.getSimpleName());
        return new Inhibition(element);
      case ABSOLUTE_STIMULATION:
        logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, arc, model),
                ac + " modifier is not supported. Changing to " + PhysicalStimulation.class.getSimpleName());
        return new PhysicalStimulation(element);
      case CATALYSIS:
        return new Catalysis(element);
      case INHIBITION:
        return new Inhibition(element);
      case MODULATION:
        return new Modulation(element);
      case NECESSARY_STIMULATION:
        return new Trigger(element);
      case STIMULATION:
        return new PhysicalStimulation(element);
      default:
        logger.warn("Modifier arc of invalid class.");
        throw new InvalidArgumentException("Wrong arc class.");
    }
  }

  private Glyph getSourceGlyph(final Arc a) {
    if (a.getSource() instanceof Glyph) {
      return (Glyph) a.getSource();
    } else if (a.getTarget() instanceof Glyph) {
      return (Glyph) a.getTarget();
    } else {
      throw new InvalidArgumentException("Arc is not connected to glyph: " + a.getId());
    }
  }

  private Glyph getTargetGlyph(final Arc a) {
    if (a.getTarget() instanceof Glyph) {
      return (Glyph) a.getTarget();
    } else if (a.getSource() instanceof Glyph) {
      return (Glyph) a.getSource();
    } else {
      throw new InvalidArgumentException("Arc is not connected to glyph: " + a.getId());
    }
  }

  /**
   * Returns {@link ArrowTypeData} based on given {@link ArcClazz}.
   *
   * @param ac input arc class
   * @return ArrowTypeData based on input arrow class
   * @throws Exception thrown when invalid arc class was given on input
   */
  private ArrowTypeData getAtdFromArcClazz(final ArcClazz ac) throws Exception {
    ArrowTypeData atd = new ArrowTypeData();
    switch (ac) {
      case CATALYSIS:
        atd = ModifierType.CATALYSIS.getAtd();
        break;
      case CONSUMPTION:
        atd.setArrowType(ArrowType.NONE);
        break;
      case INHIBITION:
        atd = ModifierType.INHIBITION.getAtd();
        break;
      case MODULATION:
        atd = ModifierType.MODULATION_STRING.getAtd();
        break;
      case NECESSARY_STIMULATION:
        atd = ModifierType.TRIGGER_STRING.getAtd();
        break;
      case PRODUCTION:
        atd.setArrowType(ArrowType.FULL);
        break;
      case STIMULATION:
        atd = ModifierType.PHYSICAL_STIMULATION.getAtd();
        break;
      case LOGIC_ARC:
        atd.setArrowType(ArrowType.NONE);
        break;
      default:
        throw new InvalidArgumentException("Wrong arc class.");
    }
    return atd.copy();
  }

  public Reaction createActivityFlowReaction(final Arc arc) {
    Reaction result;
    switch (ArcClazz.fromClazz(arc.getClazz())) {
      case PRODUCTION:
        logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, arc, model),
                "Activity flow arc type is invalid. Changing to " + StateTransitionReaction.class.getSimpleName());
        result = new StateTransitionReaction(arc.getId());
        break;
      default:
        throw new InvalidArgumentException("Don't know how to translate to activity flow reaction");
    }

    List<Point2D> pointList = getLinePoints(arc);
    List<Point2D> reactantPoints = new ArrayList<>();
    List<Point2D> centerPoints = new ArrayList<>();
    List<Point2D> productPoints = new ArrayList<>();
    if (pointList.size() > 3) {
      reactantPoints.addAll(pointList.subList(0, pointList.size() / 2));
      centerPoints.addAll(pointList.subList(pointList.size() / 2 - 1, pointList.size() / 2 + 1));
      productPoints.addAll(pointList.subList(pointList.size() / 2, pointList.size() + 1));
    } else {
      reactantPoints.add(pointList.get(0));
      reactantPoints.add(new PointTransformation().getPointOnLine(pointList.get(0), pointList.get(1), 0.3));
      centerPoints.add(new PointTransformation().getPointOnLine(pointList.get(0), pointList.get(1), 0.3));
      centerPoints.add(new PointTransformation().getPointOnLine(pointList.get(0), pointList.get(1), 0.7));
      productPoints.add(new PointTransformation().getPointOnLine(pointList.get(0), pointList.get(1), 0.7));
      for (int i = 1; i < pointList.size(); i++) {
        productPoints.add(pointList.get(i));
      }
    }

    Glyph source = getSourceGlyph(arc);
    if (model.getElementByElementId(source.getId()) == null) {
      logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, arc, model), "Cannot find source element. Skipping arc.");
      return null;
    }
    Reactant reactant = new Reactant(model.getElementByElementId(source.getId()));
    reactant.setLine(new PolylineData(reactantPoints));
    result.addReactant(reactant);

    Glyph target = getTargetGlyph(arc);
    if (model.getElementByElementId(target.getId()) == null) {
      logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, arc, model), "Cannot find target element. Skipping arc.");
      return null;
    }
    Product product = new Product(model.getElementByElementId(target.getId()));
    product.setLine(parseLine(arc, productPoints));
    result.addProduct(product);

    result.setLine(new PolylineData(centerPoints));

    return result;
  }

  /**
   * Method used for parsing lines from sbgn-ml arcs.
   *
   * @param a         SBGN-ML arc
   * @param pointList list of points for the line
   * @return line parsed from SBGN-ML arc
   */
  private PolylineData parseLine(final Arc a, final List<Point2D> pointList) {
    PolylineData line = new PolylineData(pointList);
    line = PolylineDataFactory.removeCollinearPoints(line);
    ArrowTypeData atd = extractArrowTypeDataFromArc(a);
    line.setEndAtd(atd.copy());
    line.setType(atd.getArrowLineType());
    return line;
  }

  private ArrowTypeData extractArrowTypeDataFromArc(final Arc a) {
    ArrowTypeData atd = new ArrowTypeData();
    atd.setArrowLineType(LineType.SOLID);

    switch (ArcClazz.fromClazz(a.getClazz())) {
      case CATALYSIS:
        atd = ModifierType.CATALYSIS.getAtd();
        break;
      case CONSUMPTION:
        atd.setArrowType(ArrowType.NONE);
        break;
      case ABSOLUTE_INHIBITION:
      case INHIBITION:
        atd = ModifierType.INHIBITION.getAtd();
        break;
      case INTERACTION:
        atd.setArrowType(ArrowType.FULL);
        break;
      case MODULATION:
        atd = ModifierType.MODULATION_STRING.getAtd();
        break;
      case NECESSARY_STIMULATION:
        atd = ModifierType.TRIGGER_STRING.getAtd();
        break;
      case PRODUCTION:
        atd.setArrowType(ArrowType.FULL);
        break;
      case ABSOLUTE_STIMULATION:
      case STIMULATION:
        atd = ModifierType.PHYSICAL_STIMULATION.getAtd();
        break;
      case LOGIC_ARC:
        atd.setArrowType(ArrowType.NONE);
        break;
      case POSITIVE_INFLUENCE:
        atd.setArrowType(ArrowType.OPEN);
        break;
      case NEGATIVE_INFLUENCE:
        atd.setArrowType(ArrowType.CROSSBAR);
        atd.setLen(3);
        break;
      case UNKNOWN_INFLUENCE:
        atd.setArrowType(ArrowType.DIAMOND);
        atd.setArrowLineType(LineType.DASHED);
        break;
      default:
        throw new InvalidArgumentException("Wrong arc class.");
    }
    return atd;
  }

  /**
   * Method used to parse arc going to or from a phenotype.
   *
   * @param arc arc to be parsed
   */
  private Reaction parsePhenotypeArc(final Arc arc) {
    Reaction reaction;

    switch (ArcClazz.fromClazz(arc.getClazz())) {
      case INHIBITION:
        reaction = new NegativeInfluenceReaction(arc.getId());
        break;
      case INTERACTION:
        reaction = new KnownTransitionOmittedReaction(arc.getId());
        break;
      case MODULATION:
        reaction = new ReducedModulationReaction(arc.getId());
        break;
      case NECESSARY_STIMULATION:
        reaction = new ReducedTriggerReaction(arc.getId());
        break;
      case STIMULATION:
        reaction = new ReducedPhysicalStimulationReaction(arc.getId());
        break;
      case CATALYSIS:
        reaction = new CatalysisReaction(arc.getId());
        break;
      case POSITIVE_INFLUENCE:
        reaction = new PositiveInfluenceReaction(arc.getId());
        break;
      case NEGATIVE_INFLUENCE:
        reaction = new NegativeInfluenceReaction(arc.getId());
        break;
      case UNKNOWN_INFLUENCE:
        reaction = new UnknownReducedModulationReaction(arc.getId());
        break;
      default:
        throw new InvalidArgumentException();
    }

    Glyph source = (Glyph) arc.getSource();

    Glyph target = (Glyph) arc.getTarget();
    List<Point2D> productPointList = getLinePoints(arc);
    if (productPointList.size() == 2) {
      Point2D secondPoint = pt.getPointOnLine(productPointList.get(0), productPointList.get(1), 0.4);
      Point2D thirdPoint = pt.getPointOnLine(productPointList.get(0), productPointList.get(1), 0.6);
      productPointList.add(1, secondPoint);
      productPointList.add(2, thirdPoint);
    }

    if (productPointList.size() == 3) {
      if (productPointList.get(0).distance(productPointList.get(1)) > productPointList.get(1)
              .distance(productPointList.get(2))) {
        Point2D newPoint = pt.getPointOnLine(productPointList.get(0), productPointList.get(1), 0.5);
        productPointList.add(1, newPoint);
      } else {
        Point2D newPoint = pt.getPointOnLine(productPointList.get(1), productPointList.get(2), 0.5);
        productPointList.add(2, newPoint);
      }
    }
    int reactantPointEnds = (productPointList.size() - 1) / 2;
    int productPointStarts = reactantPointEnds + 1;

    Reactant reactant = new Reactant(model.getElementByElementId(source.getId()));
    Product product = new Product(model.getElementByElementId(target.getId()));

    PolylineData reactantLine = new PolylineData(productPointList.subList(0, reactantPointEnds + 1)).copy();
    reactantLine.setEndAtd(createDefaultArrowTypeData());
    reactant.setLine(reactantLine);

    PolylineData centerLine = new PolylineData(productPointList.subList(reactantPointEnds, productPointStarts + 1))
            .copy();
    reaction.setLine(centerLine);

    ArrowTypeData productAtd = extractArrowTypeDataFromArc(arc);
    PolylineData productLine = new PolylineData(
            productPointList.subList(productPointStarts, productPointList.size())).copy();
    productLine.setEndAtd(productAtd);
    product.setLine(productLine);

    reaction.addReactant(reactant);
    reaction.addProduct(product);

    reaction.getLine().setType(productAtd.getArrowLineType());
    reactant.getLine().setType(productAtd.getArrowLineType());
    product.getLine().setType(productAtd.getArrowLineType());

    renderParser.assignRenderInformation(reaction.getLine(), arc);
    renderParser.assignRenderInformation(reactant.getLine(), arc);
    renderParser.assignRenderInformation(product.getLine(), arc);
    return reaction;
  }

  private ArrowTypeData createDefaultArrowTypeData() {
    ArrowTypeData atd = new ArrowTypeData();
    atd.setArrowType(ArrowType.NONE);
    atd.setArrowLineType(LineType.SOLID);
    return atd;
  }

  /**
   * Method used to parse line points from SBGN-ML arc.
   *
   * @param arc SBGN-ML arc
   * @return list of line points
   */
  private List<Point2D> getLinePoints(final Arc arc) {
    List<Point2D> pointList = new ArrayList<>();
    Point2D startPoint = new Point2D.Double(arc.getStart().getX(), arc.getStart().getY());
    Point2D endPoint = new Point2D.Double(arc.getEnd().getX(), arc.getEnd().getY());
    pointList.add(startPoint);
    for (final Next nextPoint : arc.getNext()) {
      pointList.add(new Point2D.Double(nextPoint.getX(), nextPoint.getY()));
    }
    pointList.add(endPoint);
    return pointList;
  }

  /**
   * Method used to parse a single arc.
   *
   * @param a arc to be parsed
   */
  public Reaction parseArc(final Arc a) throws InvalidInputDataExecption {
    boolean processFound;
    switch (ArcClazz.fromClazz(a.getClazz())) {
      case CONSUMPTION:
        parseConsumption(a);
        break;
      case PRODUCTION:
        processFound = false;
        Port arcSourcePort = null;
        if (a.getSource() instanceof Port) {
          arcSourcePort = (Port) a.getSource();
        } else if (a.getTarget() instanceof Port) {
          logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, a, model), "Production is going to process");
          arcSourcePort = (Port) a.getTarget();
        } else {
          for (Process p : getProcesses()) {
            if (p.getCentralPoint().equals(a.getTarget())) {
              p.addReagentArc(a);
              logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, a, model), "Invalid target port");
              processFound = true;
              break;
            }
            if (p.getCentralPoint().equals(a.getSource())) {
              p.addProductArc(a);
              logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, a, model), "Invalid source port");
              processFound = true;
              break;
            }
          }
        }
        if (!processFound) {
          for (Process p : getProcesses()) {
            if (p.getCentralPoint().getPort().contains(arcSourcePort)) {
              p.addProductArc(a);
              break;
            }
          }
        }
        break;
      case EQUIVALENCE_ARC:
        logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, a, model), "Submaps are not supported.");
        break;
      case LOGIC_ARC:
        logicArcs.add(a);
        break;
      case ABSOLUTE_INHIBITION:
      case ABSOLUTE_STIMULATION:
      case CATALYSIS:
      case INHIBITION:
      case INTERACTION:
      case MODULATION:
      case NECESSARY_STIMULATION:
      case NEGATIVE_INFLUENCE:
      case POSITIVE_INFLUENCE:
      case STIMULATION:
      case UNKNOWN_INFLUENCE:
        if (a.getTarget() instanceof Glyph) {
          Glyph targetGlyph = (Glyph) a.getTarget();
          if (GlyphClazz.fromClazz(targetGlyph.getClazz()).equals(GlyphClazz.PHENOTYPE)) {
            if (!(a.getSource() instanceof Glyph)) {
              logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, a, model), "Invalid phenotype arc.");
              break;
            }
            try {
              return parsePhenotypeArc(a);
            } catch (Exception e) {
              logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, a, model), "Probelm with importing arc",
                      e);
            }
            break;
          }
          if (a.getSource() instanceof Glyph) {
            Glyph sourceGlyph = (Glyph) a.getSource();
            if (GlyphClazz.fromClazz(sourceGlyph.getClazz()).equals(GlyphClazz.PHENOTYPE)
                    && (model.getElementByElementId(targetGlyph.getId()) instanceof Species)) {
              try {
                return parsePhenotypeArc(a);
              } catch (InvalidArgumentException ex) {
                logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, a, model),
                        "The arc is not a valid reduced notation arc.", ex);
              }
              break;
            }
          }
          boolean found = false;
          for (Process p : getProcesses()) {
            if (p.getCentralPoint().getId().equals(targetGlyph.getId())) {
              p.addInfluenceArc(a);
              found = true;
              break;
            }
          }
          if (!found) {
            logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, a, model),
                    "Expected PHENOTYPE in reduced notation arc. Arc skipped.");
          }
          break;
        } else {
          Port targetPort = (Port) a.getTarget();
          boolean found = false;
          if (targetPort != null) {
            for (Process p : getProcesses()) {
              Set<String> ids = new HashSet<>();
              ids.add(p.getCentralPoint().getId());
              for (Port port : p.getCentralPoint().getPort()) {
                ids.add(port.getId());
              }
              if (ids.contains(targetPort.getId())) {
                p.addInfluenceArc(a);
                found = true;
                break;
              }
            }
          }
          if (!found) {
            logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, a, model),
                    "Cannot find a target for arc: " + a.getId() + ". Skipping");
          }
        }
        break;
      default:
        logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, a, model),
                "The arc has not been parsed, since it is invalid for SBGN PD map.");
    }
    return null;
  }

  private void parseConsumption(final Arc a) throws InvalidInputDataExecption {
    boolean processFound;
    processFound = false;
    Port arcTargetPort = null;
    if (a.getTarget() instanceof Port) {
      arcTargetPort = (Port) a.getTarget();
    } else if (a.getSource() instanceof Port) {
      logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, a, model), "Consumption is going from process");
      arcTargetPort = (Port) a.getSource();
    } else {
      for (Process p : getProcesses()) {
        if (p.getCentralPoint().equals(a.getTarget())) {
          p.addReagentArc(a);
          logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, a, model), "Invalid target port");
          processFound = true;
          break;
        }
        if (p.getCentralPoint().equals(a.getSource())) {
          p.addProductArc(a);
          logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, a, model), "Invalid source port");
          processFound = true;
          break;
        }
      }
      if (!processFound) {
        logger.warn(new SbgnLogMarker(ProjectLogEntryType.PARSING_ISSUE, a, model), "Consumption must be connected to a Port. Ignoring arc.");
        return;
      }
    }
    for (Process p : getProcesses()) {
      if (p.getCentralPoint().getPort().contains(arcTargetPort)) {
        p.addReagentArc(a);
        break;
      }
    }
  }

  public boolean isActivityFlowArc(final Arc a) {
    boolean processFound = false;
    if (a.getClazz().equals(ArcClazz.PRODUCTION.getClazz())) {
      if (!(a.getSource() instanceof Port) && !(a.getTarget() instanceof Port)) {
        for (Process p : getProcesses()) {
          if (p.getCentralPoint().equals(a.getTarget()) || p.getCentralPoint().equals(a.getSource())) {
            processFound = true;
            break;
          }
        }
        return !processFound;
      }
    }
    return false;
  }

}
