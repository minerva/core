<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="/*[name()='sbgn']/*[name()='map']/*[name()='extension']/*[name()='renderInformation']/*[name()='listOfStyles']/*[name()='style']/@id-list">
		<xsl:attribute name="idList">
      <xsl:value-of select="." />
   </xsl:attribute>
	</xsl:template>

</xsl:stylesheet>
