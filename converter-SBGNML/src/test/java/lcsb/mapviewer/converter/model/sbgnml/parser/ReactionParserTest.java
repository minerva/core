package lcsb.mapviewer.converter.model.sbgnml.parser;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.sbgnml.SbgnmlTestFunctions;
import lcsb.mapviewer.converter.model.sbgnml.SbgnmlXmlConverter;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.reaction.Reaction;

public class ReactionParserTest extends SbgnmlTestFunctions {

  @Test
  public void testParsingInteractionGlyph() throws Exception {
    Converter converter = new SbgnmlXmlConverter();
    Model model = converter
        .createModel(new ConverterParams()
            .filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/interaction_test.sbgn"));
    assertEquals(1, model.getReactions().size());
    assertEquals(1, super.getWarnings().size());
  }

  @Test
  public void testParsingInteractionArc() throws Exception {
    Converter converter = new SbgnmlXmlConverter();
    Model model = converter
        .createModel(new ConverterParams()
            .filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/phenotype_interaction.sbgn"));
    assertEquals(1, model.getReactions().size());
    assertEquals(1, super.getWarnings().size());
  }

  @Test
  public void testParsingNegativeInfluenceArc() throws Exception {
    Converter converter = new SbgnmlXmlConverter();
    Model model = converter
        .createModel(new ConverterParams()
            .filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/phenotype_negative_influence.sbgn"));
    assertEquals(1, model.getReactions().size());
    assertEquals(1, super.getWarnings().size());
  }

  @Test
  public void testParsingPositiveInfluenceArc() throws Exception {
    Converter converter = new SbgnmlXmlConverter();
    Model model = converter
        .createModel(new ConverterParams()
            .filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/phenotype_positive_influence.sbgn"));
    assertEquals(1, model.getReactions().size());
    assertEquals(1, super.getWarnings().size());
  }

  @Test
  public void testParsingUnknownInfluenceArc() throws Exception {
    Converter converter = new SbgnmlXmlConverter();
    Model model = converter
        .createModel(new ConverterParams()
            .filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/phenotype_unknown_influence.sbgn"));
    assertEquals(1, model.getReactions().size());
    assertEquals(1, super.getWarnings().size());
  }

  @Test
  public void testParsingAbsoluteInhibitionModifierArc() throws Exception {
    Converter converter = new SbgnmlXmlConverter();
    Model model = converter
        .createModel(new ConverterParams()
            .filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/process_absolute_inhibition.sbgn"));
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(1, reaction.getModifiers().size());
    assertEquals(2, super.getWarnings().size());
  }

  @Test
  public void testParsingAbsoluteStimulationModifierArc() throws Exception {
    Converter converter = new SbgnmlXmlConverter();
    Model model = converter
        .createModel(new ConverterParams()
            .filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/process_absolute_stimulation.sbgn"));
    assertEquals(1, model.getReactions().size());
    Reaction reaction = model.getReactions().iterator().next();
    assertEquals(1, reaction.getModifiers().size());
    assertEquals(2, super.getWarnings().size());
  }

}
