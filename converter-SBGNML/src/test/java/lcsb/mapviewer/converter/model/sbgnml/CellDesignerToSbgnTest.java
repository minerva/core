package lcsb.mapviewer.converter.model.sbgnml;

import lcsb.mapviewer.common.comparator.SetComparator;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CellDesignerToSbgnTest extends SbgnmlTestFunctions {
  private final ElementUtils eu = new ElementUtils();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSample() throws Exception {
    final Converter converter = new CellDesignerXmlParser();
    final Converter converter2 = new SbgnmlXmlConverter();

    final Model model = converter.createModel(new ConverterParams().filename("testFiles/cellDesigner/sample.xml"));

    final String output = File.createTempFile("temp-sbgn-output", ".sbgn").getAbsolutePath();
    converter2.model2File(model, output);

    try (final FileInputStream inputStream = new FileInputStream(output)) {
      final String everything = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
      assertTrue("Warnings are not exported into sbgn", everything.contains("Element type is not supported"));
    }
    converter2.createModel(new ConverterParams().filename(output));

    new File(output).delete();
  }

  @Test
  public void testSample2() throws Exception {
    final Converter converter = new CellDesignerXmlParser();
    final Converter converter2 = new SbgnmlXmlConverter();

    final Model model = converter.createModel(new ConverterParams().filename("testFiles/cellDesigner/bubbles.xml"));

    final String output = File.createTempFile("temp-sbgn-output", ".sbgn").getAbsolutePath();
    converter2.model2File(model, output);
    new File(output).delete();
  }

  @Test
  public void testCompartmentsExport() throws Exception {
    final Converter converter = new CellDesignerXmlParser();
    final Converter converter2 = new SbgnmlXmlConverter();

    final Model model = converter.createModel(new ConverterParams().filename("testFiles/cellDesigner/neuron.xml"));

    final String output = File.createTempFile("temp-sbgn-output", ".sbgn").getAbsolutePath();
    converter2.model2File(model, output);

    converter2.createModel(new ConverterParams().filename(output));

    final String fileContent;
    try (final FileInputStream inputStream = new FileInputStream(output)) {
      fileContent = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
    }

    for (final Element element : model.getElements()) {
      if (element instanceof Compartment) {
        assertTrue(eu.getElementTag(element) + " compartment is not exported",
            fileContent.contains(element.getElementId()));
      }
    }

    new File(output).delete();
  }

  @Test
  public void testProteinState() throws Exception {
    final Converter converter = new CellDesignerXmlParser();

    final Model model = converter.createModel(new ConverterParams().filename("testFiles/cellDesigner/state.xml"));

    final Model model2 = serializeAndCleanOverSbgn(model);

    final StringComparator comparator = new StringComparator();

    Protein protein1 = model.getElementByElementId("sa1");
    Protein protein2 = model2.getElementByElementId("sa1");
    for (int i = 0; i < Math.max(protein1.getModificationResidues().size(), protein2.getModificationResidues().size()); i++) {
      final ModificationResidue mr1 = protein1.getModificationResidues().get(i);
      final ModificationResidue mr2 = protein2.getModificationResidues().get(i);
      assertEquals(0, comparator.compare(mr1.toString(), mr2.toString()));
    }

    protein1 = model.getElementByElementId("sa2");
    protein2 = model2.getElementByElementId("sa2");

    for (int i = 0; i < Math.max(protein1.getModificationResidues().size(), protein2.getModificationResidues().size()); i++) {
      final ModificationResidue mr1 = protein1.getModificationResidues().get(i);
      final ModificationResidue mr2 = protein2.getModificationResidues().get(i);
      assertEquals(0, comparator.compare(mr1.toString(), mr2.toString()));
    }
  }

  @Test
  public void testModifications() throws Exception {
    final Converter converter = new CellDesignerXmlParser();

    final Model model = converter.createModel(new ConverterParams().filename("testFiles/cellDesigner/modifications.xml"));

    final Model model2 = serializeAndCleanOverSbgn(model);

    final Set<String> set1 = new HashSet<>();
    final Set<String> set2 = new HashSet<>();

    for (final ModificationResidue region : ((Protein) model.getElementByElementId("sa3988")).getModificationResidues()) {
      set1.add(region.toString());
    }

    for (final ModificationResidue region : ((Protein) model2.getElementByElementId("sa3988")).getModificationResidues()) {
      set2.add(region.toString());
    }

    final SetComparator<String> comparator = new SetComparator<>(new StringComparator());

    assertEquals(0, comparator.compare(set1, set2));
  }

  @Test
  public void serializeAntisenseRna() throws Exception {
    final Converter converter = new CellDesignerXmlParser();

    final Model model = converter.createModel(new ConverterParams().filename("testFiles/cellDesigner/antisense_rna.xml"));

    final Model model2 = serializeAndCleanOverSbgn(model);

    assertTrue(model2.getElements().iterator().next() instanceof AntisenseRna);
  }

  @Test
  public void testProblematicNode() throws Exception {
    final Converter converter = new CellDesignerXmlParser();

    final Model model = converter.createModel(new ConverterParams().filename("testFiles/cellDesigner/unknown_boolean_gate.xml"));

    final Model model2 = serializeAndCleanOverSbgn(model);

    assertEquals(1, model2.getReactions().size());


  }

}
