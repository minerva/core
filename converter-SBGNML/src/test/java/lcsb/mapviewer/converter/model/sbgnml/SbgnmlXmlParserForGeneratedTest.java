package lcsb.mapviewer.converter.model.sbgnml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator;
import lcsb.mapviewer.converter.graphics.NormalImageGenerator;
import lcsb.mapviewer.converter.graphics.PngImageGenerator;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.reaction.ReactionComparator;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.ElementComparator;

@RunWith(Parameterized.class)
public class SbgnmlXmlParserForGeneratedTest extends SbgnmlTestFunctions {

  private Path filePath;

  public SbgnmlXmlParserForGeneratedTest(final Path filePath) {
    this.filePath = filePath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<Object[]>();
    Files.walk(Paths.get("testFiles/sbgnmlParserTestFiles/generated")).forEach(filePath -> {
      if (Files.isRegularFile(filePath)) {
        data.add(new Object[] { filePath });
      }
    });
    return data;
  }

  @Test
  public void createModelTest() throws Exception {
    String dir = Files.createTempDirectory("sbgn-temp-images-dir").toFile().getAbsolutePath();

    Converter converter = new SbgnmlXmlConverter();

    Model model = converter.createModel(new ConverterParams().filename(filePath.toString()));

    // Create and display image of parsed map
    AbstractImageGenerator.Params params = new AbstractImageGenerator.Params().height(model.getHeight())
        .width(model.getWidth()).nested(true).scale(1).level(20).x(0).y(0).model(model);
    NormalImageGenerator nig = new PngImageGenerator(params);
    String pngFilePath = dir + "/"
        .concat(filePath.getFileName().toString().substring(0, filePath.getFileName().toString().indexOf(".sbgn")))
        .concat(".png");
    nig.saveToFile(pngFilePath);

    CellDesignerXmlParser cellDesignerXmlParser = new CellDesignerXmlParser();
    String xmlString = cellDesignerXmlParser.model2String(model);

    String cellDesignerFilePath = dir + "/"
        .concat(filePath.getFileName().toString().substring(0, filePath.getFileName().toString().indexOf(".sbgn")))
        .concat(".xml");
    PrintWriter out = new PrintWriter(cellDesignerFilePath);
    out.print(xmlString);
    out.close();

    InputStream is = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));

    Model model2 = cellDesignerXmlParser.createModel(new ConverterParams().inputStream(is).sizeAutoAdjust(false));

    AbstractImageGenerator.Params params2 = new AbstractImageGenerator.Params().height(model2.getHeight())
        .width(model2.getWidth()).nested(true).scale(1).level(20).x(0).y(0).model(model2);
    NormalImageGenerator nig2 = new PngImageGenerator(params2);
    String pngFilePath2 = dir + "/"
        .concat(filePath.getFileName().toString().substring(0, filePath.getFileName().toString().indexOf(".sbgn")))
        .concat("_2.png");
    nig2.saveToFile(pngFilePath2);

    assertNotNull(model2);
    ModelComparator comparator = new ModelComparator(1.0);
    comparator.getReactionSetComparator().setObjectComparator(new ReactionComparator(1.0, true));

    model2.setNotes(model.getNotes());

    for (Element element : model.getElements()) {
      element.setNameX(0);
      element.setNameY(0);
      element.setNameWidth(0);
      element.setNameHeight(0);
      element.setNameHorizontalAlign(null);
      element.setNameVerticalAlign(null);
    }
    for (Element element : model2.getElements()) {
      element.setNameX(0);
      element.setNameY(0);
      element.setNameWidth(0);
      element.setNameHeight(0);
      element.setNameHorizontalAlign(null);
      element.setNameVerticalAlign(null);
    }

    // export to CellDesigner changes line width
    comparator.setElementComparator(new ElementComparator(3));
    assertEquals(0, comparator.compare(model, model2));
    FileUtils.deleteDirectory(new File(dir));
  }

}
