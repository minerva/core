package lcsb.mapviewer.converter.model.sbgnml.utils;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import lcsb.mapviewer.persist.SpringPersistConfig;

@Configuration
@Import({ SpringPersistConfig.class })
@ComponentScan(basePackages = { "lcsb.mapviewer.converter.model.sbgnml.utils" })
public class SpringSBGNMLConverterTestConfig {

}
