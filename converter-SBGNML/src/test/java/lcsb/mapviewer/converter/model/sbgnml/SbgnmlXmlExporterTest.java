package lcsb.mapviewer.converter.model.sbgnml;

import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.ModifierReactionNotation;
import lcsb.mapviewer.model.map.reaction.type.ReducedNotation;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import org.junit.Test;
import org.sbgn.ArcClazz;
import org.sbgn.GlyphClazz;
import org.sbgn.SbgnUtil;

import java.awt.geom.Point2D;
import java.io.File;
import java.io.PrintWriter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class SbgnmlXmlExporterTest extends SbgnmlTestFunctions {

  @Test
  public void testGetGlyphClazzFromReaction() throws Exception {
    ElementUtils eu = new ElementUtils();
    SbgnmlXmlExporter exporter = new SbgnmlXmlExporter();
    for (final Class<? extends Reaction> clazz : eu.getAvailableReactionSubclasses()) {
      Reaction reaction = createReaction(clazz);
      GlyphClazz glyph = exporter.getGlyphClazzFromReaction(reaction);
      if (reaction instanceof ReducedNotation) {
        assertNull(eu.getElementTag(reaction) + "Reduced notation should be not treated as PROCESS", glyph);
      } else if (reaction instanceof ModifierReactionNotation) {
        assertNull(eu.getElementTag(reaction) + "Modifier reactions should be not treated as PROCESS", glyph);
      } else {
        assertNotNull(eu.getElementTag(reaction) + "No glyph found", glyph);
      }
    }
  }

  @Test
  public void testGetArcClassForReducedNotationReaction() throws Exception {
    ElementUtils eu = new ElementUtils();
    SbgnmlXmlExporter exporter = new SbgnmlXmlExporter();
    for (final Class<? extends Reaction> clazz : eu.getAvailableReactionSubclasses()) {
      Reaction reaction = createReaction(clazz);
      ArcClazz arc = exporter.getArcClassForReducedNotationReaction(reaction);
      if (reaction instanceof ReducedNotation) {
        assertNotNull(eu.getElementTag(reaction) + "Reduced notation is not translated properly", arc);
      } else if (reaction instanceof ModifierReactionNotation) {
        assertNotNull(eu.getElementTag(reaction) + "Modifier reaction is not translated properly", arc);
      }
    }
  }

  private static int counter = 0;

  private static Reaction createReaction(final Class<? extends Reaction> clazz) throws Exception {

    Reaction reaction = clazz.getConstructor(String.class).newInstance("re" + (counter++));

    Ion ion = createIon(counter++);
    Ion ion2 = createIon(counter);

    PointTransformation pt = new PointTransformation();

    Point2D p1 = ion.getCenter();
    Point2D p4 = ion.getCenter();

    Point2D p2 = pt.getPointOnLine(p1, p4, 0.3);
    Point2D p3 = pt.getPointOnLine(p1, p4, 0.6);

    reaction.setLine(new PolylineData(p2, p3));

    Reactant reactant = createReactant(ion);
    reactant.setLine(new PolylineData(p1, p2));
    reaction.addReactant(reactant);

    Product product = createProduct(ion2);
    product.setLine(new PolylineData(p3, p4));
    reaction.addProduct(product);

    return reaction;
  }

  private static Reactant createReactant(final Ion ion) {
    Reactant result = new Reactant(ion);
    Point2D point = ion.getCenter();
    point.setLocation(point.getX() + 300, point.getY());
    result.setLine(new PolylineData(ion.getCenter(), point));
    return result;
  }

  private static Product createProduct(final Ion ion) {
    Product result = new Product(ion);
    Point2D point = ion.getCenter();
    point.setLocation(point.getX() + 300, point.getY());
    result.setLine(new PolylineData(ion.getCenter(), point));
    return result;
  }

  private static Ion createIon(final int id) {
    Ion ion = new Ion("x" + id);
    ion.setName("ion " + id);
    ion.setWidth(100);
    ion.setHeight(100);
    ion.setX(200 * (id % 2 + 1));
    ion.setY(50 * (id / 2 + 1));
    ion.setNameBorder(ion.getBorder());
    ion.setOnlySubstanceUnits(true);
    ion.setConstant(true);
    ion.setInitialAmount(2.0);
    ion.setBoundaryCondition(true);
    return ion;
  }

  @Test
  public void importExportHomodimer() throws Exception {
    GenericProtein protein = createProtein();
    protein.setHomodimer(3);
    Model model = new ModelFullIndexed(null);
    model.setWidth(2000);
    model.setHeight(2000);
    model.addElement(protein);

    Model model2 = serializeAndCleanOverSbgn(model);
    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void importExportNotes() throws Exception {
    GenericProtein protein = createProtein();
    protein.setNotes("hello world");
    Model model = new ModelFullIndexed(null);
    model.setWidth(2000);
    model.setHeight(2000);
    model.addElement(protein);

    Model model2 = serializeAndCleanOverSbgn(model);
    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void testExportWithWarning() throws Exception {
    Converter sbgnmlConverter = new SbgnmlXmlConverter();

    Drug drug = createDrug();
    Model model = new ModelFullIndexed(null);
    model.setWidth(2000);
    model.setHeight(2000);
    model.addElement(drug);

    String sbgn = sbgnmlConverter.model2String(model);

    File tempFile = File.createTempFile("sbgn-test-", ".sbgn");
    tempFile.deleteOnExit();
    PrintWriter out = new PrintWriter(tempFile);
    out.print(sbgn);
    out.close();

    assertTrue(SbgnUtil.isValid(tempFile));
  }

  @Test
  public void importExportZ() throws Exception {
    Compartment compartment = createCompartment();
    compartment.setZ(4);

    Model model = new ModelFullIndexed(null);
    model.setWidth(2000);
    model.setHeight(2000);
    model.addElement(compartment);

    Model model2 = serializeAndCleanOverSbgn(model);
    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void importExportDegraded() throws Exception {
    Degraded degraded = createDegraded();

    Model model = new ModelFullIndexed(null);
    model.setWidth(2000);
    model.setHeight(2000);
    model.addElement(degraded);

    serializeAndCleanOverSbgn(model);
  }

}
