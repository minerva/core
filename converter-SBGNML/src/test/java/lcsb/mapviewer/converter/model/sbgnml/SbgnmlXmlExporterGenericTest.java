package lcsb.mapviewer.converter.model.sbgnml;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.model.map.model.Model;

@RunWith(Parameterized.class)
public class SbgnmlXmlExporterGenericTest extends SbgnmlTestFunctions {


  private Path filePath;

  public SbgnmlXmlExporterGenericTest(final Path filePath) {
    this.filePath = filePath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<Object[]>();
    Files.walk(Paths.get("testFiles/sbgnmlParserTestFiles/sbgnmlFiles")).forEach(filePath -> {
      if (Files.isRegularFile(filePath) && filePath.toString().endsWith(".sbgn")) {
        data.add(new Object[] { filePath});
      }
    });
    Files.walk(Paths.get("testFiles/sbgnmlCellDesignerInompatible")).forEach(filePath -> {
      if (Files.isRegularFile(filePath) && filePath.toString().endsWith(".sbgn")) {
        data.add(new Object[] { filePath});
      }
    });
    return data;
  }

  private void parseAndExport(final Path filePath) throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter.createModel(new ConverterParams()
        .filename(filePath.toAbsolutePath().toString()));

    converter.model2File(model, File.createTempFile("temp-sbgn-output", ".sbgn").getAbsolutePath());
  }

  @Test
  public void test() throws Exception {
    parseAndExport(filePath);
  }

}
