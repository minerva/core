package lcsb.mapviewer.converter.model.sbgnml.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;

import org.junit.Test;
import org.sbgn.GlyphClazz;
import org.sbgn.bindings.Glyph;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.model.sbgnml.SbgnmlTestFunctions;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.field.Residue;

public class ElementParserTest extends SbgnmlTestFunctions {
  private ElementParser parser = new ElementParser(new RenderParser(null), new ModelFullIndexed(null));

  @Test
  public void testAdjustModificationCoordinates() throws Exception {
    GenericProtein protein = createProtein();
    Residue mr = new Residue();
    Point2D position = new Point2D.Double(100, 20);
    mr.setPosition(position);
    protein.addResidue(mr);
    parser.adjustModificationCoordinates(protein);

    assertTrue(mr.getCenter().distance(position) > Configuration.EPSILON);

  }

  @Test
  public void testParseFromBiologicalActivity() throws Exception {
    Glyph glyph = createGlyph(GlyphClazz.BIOLOGICAL_ACTIVITY);
    Element element = parser.glyphToElement(glyph);
    assertNotNull(element);
    assertEquals(1, super.getWarnings().size());
  }

  @Test
  public void testParseFromEntity() throws Exception {
    Glyph glyph = createGlyph(GlyphClazz.ENTITY);
    Element element = parser.glyphToElement(glyph);
    assertNotNull(element);
    assertEquals(1, super.getWarnings().size());
  }

  @Test
  public void testParseFromOutcome() throws Exception {
    Glyph glyph = createGlyph(GlyphClazz.OUTCOME);
    Element element = parser.glyphToElement(glyph);
    assertNotNull(element);
    assertEquals(1, super.getWarnings().size());
  }

  @Test
  public void testParseFromObservable() throws Exception {
    @SuppressWarnings("deprecation")
    Glyph glyph = createGlyph(GlyphClazz.OBSERVABLE);
    Element element = parser.glyphToElement(glyph);
    assertNotNull(element);
    assertEquals(0, super.getWarnings().size());
  }

}
