package lcsb.mapviewer.converter.model.sbgnml.parser;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.mockito.Mockito;
import org.sbgn.GlyphClazz;
import org.sbgn.bindings.Glyph;

import lcsb.mapviewer.converter.model.sbgnml.SbgnmlTestFunctions;

public class GlyphParserTest extends SbgnmlTestFunctions {

  @Test
  public void testClazzMapping() {
    GlyphParser parser = Mockito.mock(GlyphParser.class, Mockito.CALLS_REAL_METHODS);
    for (GlyphClazz clazz : GlyphClazz.values()) {
      Glyph glyph = new Glyph();
      glyph.setClazz(clazz.getClazz());
      assertTrue(clazz.name() + " is not supported", parser.isElementGlyph(glyph)
          || parser.isModificationGlyph(glyph)
          || parser.isReactionGlyph(glyph)
          || parser.isReactionNodeGlyph(glyph));
    }
  }

}
