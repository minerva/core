package lcsb.mapviewer.converter.model.sbgnml;

import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ParseNewtExtensionTest extends SbgnmlTestFunctions {

  @Test
  public void createModelTest() throws Exception {

    final SbgnmlXmlConverter parser = new SbgnmlXmlConverter();

    final Model model = parser.createModel(new ConverterParams().filename("testFiles/newt/annotation.nwt"));

    assertEquals(1, model.getElements().iterator().next().getMiriamData().size());

    final Model model2 = serializeAndCleanOverSbgn(model);

    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void ignoreNewtExtraInfos() throws Exception {

    final SbgnmlXmlConverter parser = new SbgnmlXmlConverter();

    parser.createModel(new ConverterParams().filename("testFiles/newt/prefixes.nwt"));

    assertTrue(super.getWarnings().size() <= 1);
  }

}
