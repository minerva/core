package lcsb.mapviewer.converter.model.sbgnml;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Point2D;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.reaction.type.TranscriptionReaction;
import lcsb.mapviewer.model.map.reaction.type.TranslationReaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.Ion;

@RunWith(Parameterized.class)
public class SbgnmlXmlExporterGeneric2Test extends SbgnmlTestFunctions {


  private Model model;

  public SbgnmlXmlExporterGeneric2Test(final String name, final Model model) {
    this.model = model;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws Exception {
    Collection<Object[]> data = new ArrayList<>();
    data.add(createTestEntry(createReaction(TransportReaction.class)));
    data.add(createTestEntry(createReaction(TranslationReaction.class)));
    data.add(createTestEntry(createReaction(TranscriptionReaction.class)));
    return data;
  }

  @Test
  public void test() throws InconsistentModelException, ConverterException, InvalidInputDataExecption {
    Converter converter = new SbgnmlXmlConverter();
    String content = converter.model2String(model);
    assertEquals(0, super.getWarnings().size());

    InputStream is = new ByteArrayInputStream(content.getBytes());

    Model model2 = converter.createModel(new ConverterParams().inputStream(is));
    assertEquals(1, model2.getReactions().size());

  }

  private static Object[] createTestEntry(final Reaction element) {
    Model result = new ModelFullIndexed(null);
    result.setIdModel("X");
    result.setWidth(200);
    result.setHeight(200);
    for (final ReactionNode node : ((Reaction) element).getReactionNodes()) {
      result.addElement(node.getElement());
    }
    result.addReaction((Reaction) element);
    return new Object[] { element.getClass().getSimpleName(), result };
  }

  private static int counter = 0;

  private static Reaction createReaction(final Class<? extends Reaction> clazz) throws Exception {

    Reaction reaction = clazz.getConstructor(String.class).newInstance(new Object[] { "re" + (counter++) });

    Ion ion = createIon(counter++);
    Ion ion2 = createIon(counter);

    PointTransformation pt = new PointTransformation();

    Point2D p1 = ion.getCenter();
    Point2D p4 = ion.getCenter();

    Point2D p2 = pt.getPointOnLine(p1, p4, 0.3);
    Point2D p3 = pt.getPointOnLine(p1, p4, 0.6);

    reaction.setLine(new PolylineData(p2, p3));

    Reactant reactant = createReactant(ion);
    reactant.setLine(new PolylineData(p1, p2));
    reaction.addReactant(reactant);

    Product product = createProduct(ion2);
    product.setLine(new PolylineData(p3, p4));
    reaction.addProduct(product);

    return reaction;
  }

  private static Reactant createReactant(final Ion ion) {
    Reactant result = new Reactant(ion);
    Point2D point = ion.getCenter();
    point.setLocation(point.getX() + 300, point.getY());
    result.setLine(new PolylineData(ion.getCenter(), point));
    return result;
  }

  private static Product createProduct(final Ion ion) {
    Product result = new Product(ion);
    Point2D point = ion.getCenter();
    point.setLocation(point.getX() + 300, point.getY());
    result.setLine(new PolylineData(ion.getCenter(), point));
    return result;
  }

  private static Ion createIon(final int id) {
    Ion ion = new Ion("x" + id);
    ion.setName("ion " + id);
    ion.setWidth(100);
    ion.setHeight(100);
    ion.setX(200 * (id % 2 + 1));
    ion.setY(50 * (id / 2 + 1));
    ion.setNameBorder(ion.getBorder());
    ion.setOnlySubstanceUnits(true);
    ion.setConstant(true);
    ion.setInitialAmount(2.0);
    ion.setBoundaryCondition(true);
    return ion;
  }

}
