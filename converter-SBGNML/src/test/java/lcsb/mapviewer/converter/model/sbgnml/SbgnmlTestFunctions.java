package lcsb.mapviewer.converter.model.sbgnml;

import lcsb.mapviewer.common.tests.TestUtils;
import lcsb.mapviewer.common.tests.UnitTestFailedWatcher;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterException;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.InvalidInputDataExecption;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator;
import lcsb.mapviewer.converter.graphics.NormalImageGenerator;
import lcsb.mapviewer.converter.graphics.PngImageGenerator;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.InconsistentModelException;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;
import org.sbgn.GlyphClazz;
import org.sbgn.bindings.Bbox;
import org.sbgn.bindings.Glyph;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.geom.Point2D;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class SbgnmlTestFunctions extends TestUtils {

  protected static Logger logger = LogManager.getLogger();
  private static int counter = 0;
  private final Converter sbgnmlConverter = new SbgnmlXmlConverter();
  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();

  protected void showImage(final Model model) throws Exception {
    final String dir = Files.createTempDirectory("sbml-temp-images-dir").toFile().getAbsolutePath();
    final AbstractImageGenerator.Params params = new AbstractImageGenerator.Params().height(model.getHeight())
        .width(model.getWidth()).nested(true).scale(1).level(20).x(0).y(0).model(model);
    NormalImageGenerator nig = new PngImageGenerator(params);
    String pathWithoutExtension = dir + "/" + model.getName();
    String pngFilePath = pathWithoutExtension.concat(".png");
    nig.saveToFile(pngFilePath);
    Desktop.getDesktop().open(new File(pngFilePath));
  }

  protected Glyph createGlyph(final GlyphClazz clazz) {
    final Glyph glyph = new Glyph();
    final Bbox bbox = new Bbox();
    bbox.setX(10);
    bbox.setY(20);
    bbox.setW(30);
    bbox.setH(40);
    glyph.setBbox(bbox);
    glyph.setClazz(clazz.getClazz());
    return glyph;
  }

  protected GenericProtein createProtein() {
    final GenericProtein protein = new GenericProtein("id" + (counter++));
    assignLayout(protein);
    return protein;
  }

  protected Degraded createDegraded() {
    Degraded degraded = new Degraded("id" + (counter++));
    assignLayout(degraded);
    degraded.setWidth(degraded.getHeight());
    degraded.setNameWidth(degraded.getHeight());
    return degraded;
  }

  protected Compartment createCompartment() {
    SquareCompartment compartment = new SquareCompartment("id" + (counter++));
    assignLayout(compartment);
    compartment.setNameHorizontalAlign(HorizontalAlign.LEFT);
    compartment.setNameVerticalAlign(VerticalAlign.TOP);

    return compartment;
  }

  protected Drug createDrug() {
    final Drug drug = new Drug("id" + (counter++));
    assignLayout(drug);
    return drug;
  }

  protected Complex createComplex() {
    final Complex complex = new Complex("id" + (counter++));
    assignLayout(complex);
    complex.setNameVerticalAlign(VerticalAlign.MIDDLE);
    return complex;
  }

  private void assignLayout(final Element element) {
    element.setName("name_" + (counter++));
    element.setWidth(50);
    element.setHeight(30);
    element.setX(200);
    element.setY(100);
    element.setZ(1);
    element.setNameX(element.getX());
    element.setNameY(element.getY());
    element.setNameWidth(element.getWidth());
    element.setNameHeight(element.getHeight());
    element.setNameHorizontalAlign(HorizontalAlign.CENTER);
    element.setNameVerticalAlign(VerticalAlign.MIDDLE);
  }

  protected StructuralState createStructuralState(final Element element) {
    final StructuralState structuralState = new StructuralState("state" + (counter++));
    structuralState.setName("xxx" + (counter++));
    structuralState.setPosition(new Point2D.Double(element.getX(), element.getY() - 10));
    structuralState.setWidth(element.getWidth());
    structuralState.setHeight(20.0);
    structuralState.setFontSize(10.0);
    structuralState.setZ(element.getZ() + 2);
    structuralState.setBorderColor(Color.GREEN);
    return structuralState;
  }

  protected Model serializeAndCleanOverSbgn(final Model model) throws InconsistentModelException, ConverterException, InvalidInputDataExecption {
    final String sbgn = sbgnmlConverter.model2String(model);
    // logger.debug(sbgn);
    final Model model2 = sbgnmlConverter.createModel(
        new ConverterParams().inputStream(new ByteArrayInputStream(sbgn.getBytes(StandardCharsets.UTF_8))));

    model2.setIdModel(model.getIdModel());
    model2.setName(model.getName());
    for (final Drawable d : model.getDrawables()) {
      if (!(d instanceof Compartment)) {
        d.setZ(null);
      }
    }
    for (final Drawable d : model2.getDrawables()) {
      if (!(d instanceof Compartment)) {
        d.setZ(null);
      }
    }
    for (final Species e : model.getSpeciesList()) {
      e.setInitialAmount(null);
      e.setInitialConcentration(null);
      e.setCharge(null);
      e.setPositionToCompartment(null);
    }
    model.getUnits().clear();

    for (final Species e : model2.getSpeciesList()) {
      e.setInitialAmount(null);
      e.setInitialConcentration(null);
      e.setCharge(null);
      e.setPositionToCompartment(null);
      if (e instanceof Unknown) {
        e.setBorderColor(Unknown.DEFAULT_BORDER_COLOR);
      }
    }
    return model2;
  }

}
