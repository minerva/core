package lcsb.mapviewer.converter.model.sbgnml;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import lcsb.mapviewer.converter.model.sbgnml.utils.DbOperations;
import lcsb.mapviewer.converter.model.sbgnml.utils.SpringSBGNMLConverterTestConfig;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

@RunWith(Parameterized.class)
public class DbSerializationTest extends SbgnmlTestFunctions {

  private static ApplicationContext applicationContext;

  private String fileName;

  public DbSerializationTest(final String filePath) {
    this.fileName = filePath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    applicationContext = new AnnotationConfigApplicationContext(SpringSBGNMLConverterTestConfig.class);

    Collection<Object[]> data = new ArrayList<Object[]>();
    data.add(new Object[] { "testFiles/sbgnmlParserTestFiles/sbgnmlFiles/VANTEDdiagram.sbgn" });
    data.add(new Object[] { "testFiles/sbgnmlParserTestFiles/sbgnmlFiles/activated_stat1alpha_induction_of_the_irf1_gene.sbgn" });
    data.add(new Object[] { "testFiles/sbgnmlParserTestFiles/sbgnmlFiles/adh.sbgn" });
    data.add(new Object[] { "testFiles/sbgnmlParserTestFiles/sbgnmlFiles/clone-marker.sbgn" });
    data.add(new Object[] { "testFiles/sbgnmlParserTestFiles/sbgnmlFiles/glycolysis.sbgn" });
    data.add(new Object[] { "testFiles/sbgnmlCellDesignerInompatible/insulin-like_growth_factor_signaling.sbgn" });
    data.add(new Object[] { "testFiles/sbgnmlCellDesignerInompatible/neuronal_muscle_signalling.sbgn" });
    data.add(new Object[] { "testFiles/sbgnmlParserTestFiles/sbgnmlFiles/phenotypeTest.sbgn" });
    return data;
  }

  @Test
  public void dbSerializationTest() throws Exception {
    DbOperations modelDao = applicationContext.getBean(DbOperations.class);

    SbgnmlXmlParser parser = new SbgnmlXmlParser();

    Model model = parser.createModel(fileName, new File(fileName));

    modelDao.addModelData(model.getModelData());
    Model model2 = new ModelFullIndexed(modelDao.getModelDataById(model.getId()));

    ModelComparator comparator = new ModelComparator();
    assertEquals(0, comparator.compare(model, model2));
    modelDao.deleteModelData(model2.getModelData());
  }

}
