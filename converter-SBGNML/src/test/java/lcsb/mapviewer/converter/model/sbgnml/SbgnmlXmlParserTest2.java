package lcsb.mapviewer.converter.model.sbgnml;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithStructuralState;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import org.junit.Test;

import java.awt.Color;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class SbgnmlXmlParserTest2 extends SbgnmlTestFunctions {

  @Test
  public void createModelWithCompartmentsTest() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter
        .createModel(new ConverterParams()
            .filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/elements_inside_compartment.xml"));
    Complex complexInsideCompartment = model.getElementByElementId("csa1830");
    assertNotNull("Complex inside compartment has undefined compartment", complexInsideCompartment.getCompartment());
    Complex complexOutsideCompartment = model.getElementByElementId("csa1831");
    assertNull("Complex outside compartment has not null compartment", complexOutsideCompartment.getCompartment());
    Compartment compartment = model.getElementByElementId("ca107");
    assertNull("Top compartment has not null compartment", compartment.getCompartment());
  }

  @Test
  public void parseModelWithXSDProblems() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    converter.createModel(new ConverterParams().filename("testFiles/file_that_does_not_pass_xsd.sbgn"));
    assertEquals(1, getWarnings().size());
  }

  @Test
  public void parseModelWithAuthors() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter.createModel(new ConverterParams().filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/annotation_with_author.sbgn"));
    assertEquals(3, model.getMiriamData().size());
    assertEquals(2, model.getAuthors().size());
    assertEquals(1, model.getModificationDates().size());
    assertNotNull(model.getCreationDate());
    Model model2 = serializeAndCleanOverSbgn(model);
    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void createModelWithZIndex() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter
        .createModel(new ConverterParams()
            .filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/elements_inside_compartment.xml"));
    Complex complex = model.getElementByElementId("csa1830");
    assertNotNull(complex.getZ());
    assertEquals(VerticalAlign.BOTTOM, complex.getNameVerticalAlign());
  }

  @Test
  public void testReactionWithoutReactant() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter
        .createModel(new ConverterParams().filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/no_reactants.sbgn"));
    assertEquals(0, model.getReactions().size());
  }

  @Test
  public void testCoordinatesOfOperators() throws Exception {
    Converter converter = new SbgnmlXmlConverter();
    Model model = converter
        .createModel(new ConverterParams().filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/clone-marker.sbgn"));

    Reaction reaction = model.getReactionByReactionId("glyph9");

    Reactant r1 = reaction.getReactants().get(0);
    Reactant r2 = reaction.getReactants().get(1);

    Product p1 = reaction.getProducts().get(0);
    Product p2 = reaction.getProducts().get(1);

    NodeOperator inputOperator = null;
    NodeOperator outputOperator = null;
    for (final NodeOperator operator : reaction.getOperators()) {
      if (operator.isProductOperator()) {
        outputOperator = operator;
      } else {
        inputOperator = operator;
      }
    }

    assertEquals(r1.getLine().getEndPoint(), inputOperator.getLine().getStartPoint());
    assertEquals(r2.getLine().getEndPoint(), inputOperator.getLine().getStartPoint());

    assertEquals(p1.getLine().getStartPoint(), outputOperator.getLine().getStartPoint());
    assertEquals(p2.getLine().getStartPoint(), outputOperator.getLine().getStartPoint());
  }

  @Test
  public void testProteinState() throws Exception {
    Converter converter = new SbgnmlXmlConverter();
    Model model = converter
        .createModel(new ConverterParams().filename("testFiles/sbgnmlCellDesignerInompatible/stateVariable.sbgn"));

    Protein protein = model.getElementByElementId("glyph_n20");
    assertEquals("inactive", protein.getModificationResidues().get(0).getName());
    assertEquals(StructuralState.class, protein.getModificationResidues().get(0).getClass());
  }

  @Test
  public void testParseColors() throws Exception {
    Converter converter = new SbgnmlXmlConverter();
    Model model = converter
        .createModel(new ConverterParams()
            .filename("testFiles/sbgnmlCellDesignerInompatible/neuronal_muscle_signalling_color.sbgn"));
    assertFalse(model.getElementByElementId("glyph0").getFillColor().equals(Color.WHITE));
    assertTrue(super.getWarnings().size() <= 1);
  }

  @Test
  public void testProblematicProduct() throws Exception {
    Converter converter = new SbgnmlXmlConverter();
    Model model = converter
        .createModel(new ConverterParams()
            .filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/problematic_product_line.sbgn"));

    Reaction r = model.getReactionByReactionId("reactionVertex_10725167_27892");
    Product p = r.getProducts().get(0);
    for (final Product product : r.getProducts()) {
      assertEquals(1, product.getLine().getLines().size());
      assertEquals("Product lines should start at the same point", 0,
          product.getLine().getStartPoint().distance(p.getLine().getStartPoint()), Configuration.EPSILON);
    }
    NodeOperator operator = null;
    for (final NodeOperator o : r.getOperators()) {
      if (o.isProductOperator()) {
        operator = o;
      }
    }

    assertEquals(0, operator.getLine().getStartPoint().distance(p.getLine().getStartPoint()), Configuration.EPSILON);

  }

  @Test
  public void testImportExportStateWithVariable() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter.createModel(
        new ConverterParams().filename("testFiles/sbgnmlCellDesignerInompatible/state_with_variable.sbgn"));
    String sbgn = converter.model2String(model);

    assertTrue("State variable is missing", sbgn.contains("state variable"));
    assertTrue("State variable doesn't contain variable", sbgn.contains("variable=\"g\""));

  }

  @Test
  public void testStimulationModification() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter.createModel(
        new ConverterParams().filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/stimulation_modification.sbgn"));
    for (final Reaction r : model.getReactions()) {
      assertEquals(1, r.getModifiers().size());
    }
  }

  @Test
  public void testExportStimulationModification() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter.createModel(
        new ConverterParams().filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/stimulation_modification.sbgn"));

    InputStream is = converter.model2InputStream(model);

    Model model2 = converter.createModel(new ConverterParams().inputStream(is));

    for (final Reaction r : model2.getReactions()) {
      assertEquals(1, r.getModifiers().size());
    }

  }

  @Test
  public void testComplexName() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter.createModel(
        new ConverterParams()
            .filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/complex_with_and_without_name.sbgn"));

    Complex complexWithName = model.getElementByElementId("c1");
    Complex complexWithEmptyName = model.getElementByElementId("c2");
    Complex complexWithoutName = model.getElementByElementId("c3");

    assertEquals("comp1", complexWithName.getName());
    assertEquals("", complexWithoutName.getName());
    assertEquals("", complexWithEmptyName.getName());
    assertEquals("", complexWithEmptyName.getName());

    assertEquals(VerticalAlign.MIDDLE, complexWithoutName.getNameVerticalAlign());
    assertEquals(VerticalAlign.BOTTOM, complexWithName.getNameVerticalAlign());

  }

  @Test
  public void testActivityFlowReactionInPd() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter.createModel(
        new ConverterParams().filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/af_in_pd.sbgn"));

    assertEquals(1, model.getReactions().size());
    Reaction r = model.getReactions().iterator().next();
    assertEquals(1, r.getReactants().size());
    assertEquals(1, r.getProducts().size());
    assertNotNull(r.getLine());

  }

  @Test
  public void testReactionWithoutProduct() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter.createModel(
        new ConverterParams().filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/reaction_without_product.sbgn"));

    assertEquals(0, model.getReactions().size());
  }

  @Test
  public void testInvalidTargetPort() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter.createModel(
        new ConverterParams().filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/target_port_invalid.sbgn"));

    assertEquals(1, model.getReactions().size());
    Reaction r = model.getReactions().iterator().next();
    assertEquals(1, r.getReactants().size());
    assertEquals(1, r.getProducts().size());
    assertNotNull(r.getLine());

  }

  @Test
  public void testStateWithInvalidType() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter.createModel(
        new ConverterParams().filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/state_with_invalid_type.sbgn"));

    SpeciesWithStructuralState species = (SpeciesWithStructuralState) model.getSpeciesList().get(0);
    StructuralState state = null;
    for (ModificationResidue mr : species.getModificationResidues()) {
      if (mr instanceof StructuralState) {
        state = (StructuralState) mr;
      }
    }
    assertNotNull(state);
  }

  @Test
  public void testReactionWithoutPort() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter.createModel(
        new ConverterParams().filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/reaction_without_port.sbgn"));

    assertEquals(1, model.getReactions().size());
    Reaction r = model.getReactions().iterator().next();
    assertEquals(2, r.getReactants().size());
    assertEquals(1, r.getProducts().size());

  }

  @Test
  public void testNewtColor() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter.createModel(
        new ConverterParams().filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/nwt-color.sbgn"));

    Element element = model.getElements().iterator().next();
    assertEquals(new ColorParser().parse("#c8d8ebff"), element.getFillColor());

  }

  @Test
  public void testReactionToPhenotypeColor() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter.createModel(
        new ConverterParams().filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/phenotype-catalysis.sbgn"));

    assertEquals(2, model.getReactions().size());

  }

  @Test
  public void testProcessWithoutNodes() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter.createModel(
        new ConverterParams().filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/process-without-anything.sbgn"));

    assertEquals(0, model.getReactions().size());

  }

  @Test
  public void parseModelNotes() throws Exception {
    Converter converter = new SbgnmlXmlConverter();

    Model model = converter.createModel(new ConverterParams().filename("testFiles/sbgnmlParserTestFiles/sbgnmlFiles/model_notes.sbgn"));
    assertFalse(model.getNotes().isEmpty());
    model.setIdModel(null);
    Model model2 = serializeAndCleanOverSbgn(model);
    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void exportMultipleStructuralStates() throws Exception {

    Model model = new ModelFullIndexed(null);
    model.setWidth(640);
    model.setHeight(480);
    Protein protein = createProtein();
    protein.addStructuralState(createStructuralState(protein));
    protein.addStructuralState(createStructuralState(protein));
    model.addElement(protein);

    Complex complex = createComplex();
    complex.addStructuralState(createStructuralState(complex));
    complex.addStructuralState(createStructuralState(complex));
    model.addElement(complex);

    Model model2 = serializeAndCleanOverSbgn(model);
    assertEquals(0, new ModelComparator().compare(model, model2));
  }

  @Test
  public void exportEmptyMap() throws Exception {

    Model model = new ModelFullIndexed(null);
    model.setWidth(640);
    model.setHeight(480);

    Model model2 = serializeAndCleanOverSbgn(model);
    assertEquals(0, new ModelComparator().compare(model, model2));
  }

}
