package lcsb.mapviewer.converter.model.sbgnml.utils;

import java.util.ArrayList;
import java.util.HashSet;

import javax.transaction.Transactional;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.persist.dao.map.ModelDao;

@Transactional
@Component
public class DbOperations {

  @Autowired
  private ModelDao modelDao;

  public void addModelData(final ModelData modelData) {
    modelDao.add(modelData);
  }

  public ModelData getModelDataById(final int id) {
    ModelData result = modelDao.getById(id);
    Hibernate.initialize(result.getElements());
    Hibernate.initialize(result.getReactions());
    Hibernate.initialize(result.getProject());
    Hibernate.initialize(result.getFunctions());
    Hibernate.initialize(result.getUnits());
    Hibernate.initialize(result.getLayers());

    result.getSubmodels().clear();
    result.getParentModels().clear();

    for (Element element : result.getElements()) {
      if (element.getGlyph() != null) {
        Hibernate.initialize(element.getGlyph().getFile());
      }
      if (element instanceof Complex) {
        ((Complex) element).setElemets(new ArrayList<>());
      }
      if (element instanceof Compartment) {
        ((Compartment) element).setElements(new HashSet<>());
      }
      element.setSubmodel(null);
    }
    for (Element element : result.getElements()) {
      if (element instanceof Species) {
        if (((Species) element).getComplex() != null) {
          ((Species) element).getComplex().addSpecies((Species) element);
        }
      }
      if (element.getCompartment() != null) {
        element.getCompartment().addElement(element);
      }
    }
    return result;
  }

  public void deleteModelData(final ModelData modelData) {
    modelDao.delete(modelData);
  }
}
