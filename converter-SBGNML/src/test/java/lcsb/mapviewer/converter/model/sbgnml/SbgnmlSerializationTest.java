package lcsb.mapviewer.converter.model.sbgnml;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelComparator;

@RunWith(Parameterized.class)
public class SbgnmlSerializationTest extends SbgnmlTestFunctions {

  private Path filePath;

  public SbgnmlSerializationTest(final Path filePath) {
    this.filePath = filePath;
  }

  @Parameters(name = "{index} : {0}")
  public static Collection<Object[]> data() throws IOException {
    Collection<Object[]> data = new ArrayList<Object[]>();
    Files.walk(Paths.get("testFiles/CellDesigner-serializable")).forEach(filePath -> {
      if (Files.isRegularFile(filePath)) {
        data.add(new Object[] { filePath });
      }
    });
    return data;
  }

  @Test
  public void modelSerialization() throws Exception {
    Converter converter = new CellDesignerXmlParser();

    Model model = converter.createModel(new ConverterParams().filename(filePath.toString()));

    Model model2 = serializeAndCleanOverSbgn(model);

    assertEquals("Import/export shouldn't produce any warnings", 0, super.getWarnings().size());
    ModelComparator mc = new ModelComparator(0.001);
    assertEquals(0, mc.compare(model, model2));
  }

}
