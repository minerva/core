MINERVA installation instructions using RPM Package Manager
===========================================================

These instructions guide you through the installation process of
`MINERVA <https://git-r3lab.uni.lu/piotr.gawron/minerva>`_ on Red Hat
Enterprise Linux 7 or CentOS 7. In these instructions the server will
also host the database required for the functioning of MINERVA, but
using a different server is possible as well.


Overview
--------

The main steps are

* Install `PostgreSQL <https://www.postgresql.org/>`_ and set up a
  database.
* Install and configure `Apache Tomcat 8.5 or higher <https://tomcat.apache.org/>`_.
* Install the MINERVA RPM.
* Data migration from version 13.2.x and lower.


PostgreSQL
----------

Install PostgreSQL and initialize it with

.. code:: shell

    yum install -y https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-centos96-9.6-3.noarch.rpm
    yum install -y postgresql96-server
    /usr/pgsql-9.6/bin/postgresql96-setup initdb

Ensure that the database authentication on IPv4 on ``localhost`` is
done with md5-based passwords by adding the following line to
``/var/lib/pgsql/9.6/data/pg_hba.conf``

.. code::

    host    all             all             127.0.0.1/32            md5


Enable and start postgresql

.. code:: shell

    systemctl enable postgresql-9.6
    systemctl start postgresql-9.6

Create the MINERVA database user and the database

.. code:: shell

    su - postgres -c "createuser -d -r -s map_viewer"
    su - postgres -c "echo \"ALTER USER map_viewer WITH PASSWORD 'yourSecretPasswordHere';\"| psql"
    su - postgres -c "createdb -O map_viewer map_viewer"


Apache Tomcat
-------------

Create a new tomcat group and tomcat user

.. code:: shell

    sudo groupadd tomcat
    sudo useradd -M -s /bin/nologin -g tomcat -d /opt/tomcat tomcat

Download Tomcat Binary

.. code:: shell

    wget https://www-eu.apache.org/dist/tomcat/tomcat-8/v8.5.46/bin/apache-tomcat-8.5.46.tar.gz
    sudo mkdir /opt/tomcat
    sudo tar xvf apache-tomcat-8*tar.gz -C /opt/tomcat --strip-components=1

Update Permissions

.. code:: shell

    cd /opt/tomcat
    sudo chgrp -R tomcat /opt/tomcat
    sudo chmod -R g+r conf
    sudo chmod g+x conf
    sudo chown -R tomcat webapps/ work/ temp/ logs/

Install Systemd Unit File. Create a file `/etc/systemd/system/tomcat.service` (you might want to change memory settings specified in CATALINA_OPTS):

    # Systemd unit file for tomcat
    [Unit]
    Description=Apache Tomcat Web Application Container
    After=syslog.target network.target

    [Service]
    Type=forking

    Environment=JAVA_HOME=/usr/lib/jvm/jre
    Environment=CATALINA_PID=/opt/tomcat/temp/tomcat.pid
    Environment=CATALINA_HOME=/opt/tomcat
    Environment=CATALINA_BASE=/opt/tomcat
    Environment='CATALINA_OPTS=-Xms2048M -Xmx4096M -server -XX:+UseParallelGC'
    Environment='JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'

    ExecStart=/opt/tomcat/bin/startup.sh
    ExecStop=/bin/kill -15 $MAINPID

    User=tomcat
    Group=tomcat
    UMask=0007
    RestartSec=10
    Restart=always

    [Install]
    WantedBy=multi-user.target

Reload Systemd to load the Tomcat unit file

.. code:: shell

    sudo systemctl daemon-reload

Enable and start Tomcat

.. code:: shell

    sudo systemctl enable tomcat
    sudo systemctl start tomcat

Make sure to open the port (by default port 8080) to allow incoming
connections, e.g. using ``firewalld`` this can be accomplished with

.. code:: shell

    firewall-cmd --permanent --zone=public --add-port=8080/tcp

.. warning::

    Currently the initial administrator password of MINERVA is
    hardcoded, therefore make sure the MINERVA cannot be accessed from
    distrusted hosts until the password was changed.

MINERVA
-------

Create a configuration file at ``/etc/minerva/db.properties`` so
MINERVA knows how to connect to the database:

.. code::

    database.uri=jdbc:postgresql://localhost:5432/map_viewer
    database.username=map_viewer
    database.password=yourSecretPasswordHere

Install MINERVA with

.. code:: shell

    yum install -y minerva-X.Y.Z-1.el7.noarch.rpm

Tomcat will automatically deploy the MINERVA web archive, and after a
short time it will have initialised the database and MINERVA will be
running. Point point your browser to the newly installed service,
e.g. on a local network this could be
`<http://192.168.0.42:8080/minerva/>`_.

Login with the username ``admin`` and the password ``admin``. Click on
the ``INFO`` tab in the left panel, and click on ``MANUAL`` to get
more information about administrating and using MINERVA.

Migration from MINERVA 13.2 and lower
-------

MINERVA 13.2 and earlier versions used tomcat7 that was in most cases installed
from rpm. Starting from MINERVA 14.0.0 tomcat8 or higher is required. Before
you install new version of minerva you would need to remove old tomcat:
.. code::
  sudo yum remove tomcat

And install new tomcat manually as described in the section above. Tomcat7
stored data in different place than manually installed tomcat8. Move the data
to tomcat8 folder:

.. code::
  sudo mv /var/lib/tomcat/webapps/map_images /opt/tomcat/webapps/
  sudo mv /var/lib/tomcat/webapps/minerva-big /opt/tomcat/webapps/
