#!/bin/bash

set -e

MINERVA_SRC_DIR="$(dirname "$(dirname "$0")")"

# file that should be deployed on tomcat; if an argument is given to this script, use it as war-file
TOMCAT_FILE="${1:-$MINERVA_SRC_DIR/web/target/web-1.0.war}"

endswith() { case $2 in *"$1") true;; *) false;; esac; }

if ! endswith ".war" "$TOMCAT_FILE"; then
    echo "first argument should be a file ending in '.war'"
    exit -1
fi
if [ ! -f "$TOMCAT_FILE" ]; then
    echo "file does not exist: $TOMCAT_FILE"
    exit -1
fi

# get the version number from the changelog in war file
current_version=$(unzip -p $TOMCAT_FILE CHANGELOG | head -1 | sed 's/^minerva (\(.*\)).*/\1/')
if [ -z "$current_version" ]; then
    echo "could not extract version number from CHANGELOG in $TOMCAT_FILE"
    exit -1
fi

# where generated files will be written
RPMBUILD_TEMP="${RPMBUILD_TEMP:-$MINERVA_SRC_DIR/rpm/rpmbuildtemp}"

if [[ "$RPMBUILD_TEMP" =~ ' ' ]]; then
    echo "RPMBUILD_TEMP contains whitespace: '$RPMBUILD_TEMP'"
    echo "This is not allowed. Please provide a different directory to create the RPM with"
    echo "    RPMBUILD_TEMP=/path/without/whitespace/ \"$0\""
    exit 1
fi

# current date (for automatic changelog)
CURDATE=$(LC_ALL=C date +"%a %b %d %Y")

# clean build directories
rm -rf "$RPMBUILD_TEMP"

# create directory
mkdir -p "$RPMBUILD_TEMP/"{BUILD,SPECS}

# copy war file and other files
cp "$TOMCAT_FILE" "$RPMBUILD_TEMP/BUILD/minerva.war"
cp "$MINERVA_SRC_DIR/README.md" "$MINERVA_SRC_DIR/CHANGELOG" "$MINERVA_SRC_DIR/rpm/INSTALL.rst" "$MINERVA_SRC_DIR/rpm/logrotate_minerva" "$RPMBUILD_TEMP/BUILD"

# create RPM spec file
cp "$MINERVA_SRC_DIR/rpm/minerva.spec.in" "$RPMBUILD_TEMP/SPECS/minerva.spec"
sed -i "s/__CURRENT_VERSION__/$current_version/g" "$RPMBUILD_TEMP/SPECS/minerva.spec"
sed -i "s/__DATE__/$CURDATE/g" "$RPMBUILD_TEMP/SPECS/minerva.spec"

# build RPM
RPMBUILD_TEMP_ABS="$(readlink -f "$RPMBUILD_TEMP")"
echo "****** Building miverva $current_version RPM in $RPMBUILD_TEMP_ABS ****** "
set -x
rpmbuild -bb --define "_topdir $RPMBUILD_TEMP_ABS" "$RPMBUILD_TEMP_ABS/SPECS/minerva.spec"
set +x
echo "****** Finished building RPM ****** "
