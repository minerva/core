package lcsb.mapviewer.model;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.hibernate.proxy.HibernateProxy;

public class LogMarker implements Marker {

  private static final Logger logger = LogManager.getLogger(LogMarker.class);
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  public static final Marker IGNORE = new IgnoredLogMarker();

  private final ProjectLogEntry entry;

  public LogMarker(final ProjectLogEntryType type, final BioEntity bioEntity) {
    entry = new ProjectLogEntry();
    entry.setType(type);
    if (bioEntity != null) {
      entry.setObjectIdentifier(bioEntity.getElementId());
      entry.setObjectClass(bioEntity.getClass().getSimpleName());
      if (bioEntity.getModelData() != null && !(bioEntity.getModelData() instanceof HibernateProxy)) {
        entry.setMapName(bioEntity.getModelData().getName());
      }
    }
  }

  public LogMarker(final ProjectLogEntryType type, final LayerText layerText) {
    entry = new ProjectLogEntry();
    entry.setType(type);
    if (layerText != null) {
      entry.setObjectIdentifier(layerText.getElementId());
      entry.setObjectClass(layerText.getClass().getSimpleName());
    }
  }

  public LogMarker(final ProjectLogEntryType type, final ReactionNode node) {
    entry = new ProjectLogEntry();
    entry.setType(type);
    if (node != null && node.getReaction() != null) {
      entry.setObjectIdentifier(node.getReaction().getElementId());
      entry.setObjectClass(node.getReaction().getClass().getSimpleName());
      if (node.getReaction().getModel() != null) {
        entry.setMapName(node.getReaction().getModel().getName());
      }
    }
  }

  public LogMarker(final LogMarker marker) {
    if (marker == null) {
      logger.warn("Marker not defined");
      entry = new ProjectLogEntry();
    } else {
      entry = new ProjectLogEntry(marker.getEntry());
    }
  }

  public LogMarker(final ProjectLogEntryType type, final String objectClass, final String objectIdentifier, final String mapName) {
    entry = new ProjectLogEntry();
    entry.setType(type);
    entry.setObjectIdentifier(objectIdentifier);
    entry.setObjectClass(objectClass);
    entry.setMapName(mapName);
  }

  @Override
  public Marker addParents(final Marker... markers) {
    throw new NotImplementedException();
  }

  @Override
  public String getName() {
    throw new NotImplementedException();
  }

  @Override
  public Marker[] getParents() {
    throw new NotImplementedException();
  }

  @Override
  public boolean hasParents() {
    throw new NotImplementedException();
  }

  @Override
  public boolean isInstanceOf(final Marker m) {
    throw new NotImplementedException();
  }

  @Override
  public boolean isInstanceOf(final String name) {
    throw new NotImplementedException();
  }

  @Override
  public boolean remove(final Marker marker) {
    throw new NotImplementedException();
  }

  @Override
  public Marker setParents(final Marker... markers) {
    throw new NotImplementedException();
  }

  public ProjectLogEntry getEntry() {
    return entry;
  }
}
