package lcsb.mapviewer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

public interface MinervaEntity extends Serializable {
  int getId();

  long getEntityVersion();

  @JsonIgnore
  EntityType getEntityType();
}
