package lcsb.mapviewer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lcsb.mapviewer.common.exception.NotImplementedException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import java.io.Serializable;

@Entity
public class ProjectLogEntry implements Serializable, MinervaEntity {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JsonIgnore
  private Project project;

  @Column(nullable = false)
  private String severity;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private ProjectLogEntryType type;

  private String objectIdentifier;

  private String objectClass;

  private String mapName;

  private String source;

  @Column(nullable = false, columnDefinition = "TEXT")
  private String content;

  @Version
  @JsonIgnore
  private long entityVersion;

  public ProjectLogEntry() {

  }

  public ProjectLogEntry(final ProjectLogEntry entry) {
    this.project = entry.getProject();
    this.severity = entry.getSeverity();
    this.type = entry.getType();
    this.objectIdentifier = entry.getObjectIdentifier();
    this.objectClass = entry.getObjectClass();
    this.mapName = entry.getMapName();
    this.source = entry.getSource();
    this.content = entry.getContent();
  }

  public String getContent() {
    return content;
  }

  public void setContent(final String content) {
    this.content = content;
  }

  public String getMapName() {
    return mapName;
  }

  public void setMapName(final String mapName) {
    this.mapName = mapName;
  }

  public String getObjectClass() {
    return objectClass;
  }

  public void setObjectClass(final String objectClass) {
    this.objectClass = objectClass;
  }

  public String getObjectIdentifier() {
    return objectIdentifier;
  }

  public void setObjectIdentifier(final String objectIdentifier) {
    this.objectIdentifier = objectIdentifier;
  }

  public String getSeverity() {
    return severity;
  }

  public void setSeverity(final String severity) {
    this.severity = severity;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(final Project project) {
    this.project = project;
  }

  public String getSource() {
    return source;
  }

  public void setSource(final String source) {
    this.source = source;
  }

  public ProjectLogEntryType getType() {
    return type;
  }

  public void setType(final ProjectLogEntryType type) {
    this.type = type;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
