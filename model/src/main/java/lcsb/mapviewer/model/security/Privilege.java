package lcsb.mapviewer.model.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import java.util.Objects;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"type", "objectId"}))
public class Privilege implements MinervaEntity {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @JsonIgnore
  private Integer id;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  @JsonProperty("privilegeType")
  private PrivilegeType type;

  @JsonProperty("objectId")
  private String objectId;

  @Version
  @JsonIgnore
  private long entityVersion;

  protected Privilege() {
  }

  public Privilege(final PrivilegeType type) {
    this.type = type;
  }

  public Privilege(final PrivilegeType type, final String objectId) {
    this.type = type;
    this.objectId = objectId;
  }

  @Override
  public int hashCode() {
    if (type == null) {
      return 0;
    }

    if (isObjectPrivilege()) {
      return Objects.hash(type, objectId);
    } else {
      return type.hashCode();
    }
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Privilege)) {
      return false;
    }
    Privilege that = (Privilege) o;
    return type == that.type
        && Objects.equals(objectId, that.objectId);
  }

  @Override
  public String toString() {
    if (isObjectPrivilege()) {
      return type.name() + ":" + objectId;
    } else {
      return type.name();
    }
  }

  @JsonIgnore
  public boolean isObjectPrivilege() {
    return objectId != null;
  }

  @Override
  public int getId() {
    return id;
  }

  public void setId(final Integer id) {
    this.id = id;
  }

  public PrivilegeType getType() {
    return type;
  }

  public void setType(final PrivilegeType type) {
    this.type = type;
  }

  public String getObjectId() {
    return objectId;
  }

  public void setObjectId(final String objectId) {
    this.objectId = objectId;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
