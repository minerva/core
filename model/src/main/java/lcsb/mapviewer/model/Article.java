package lcsb.mapviewer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lcsb.mapviewer.common.exception.NotImplementedException;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OrderColumn;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This class represents <a href="http://www.ncbi.nlm.nih.gov/pubmed">pubmed</a>
 * article.
 *
 * @author Piotr Gawron
 */
@Entity
@XmlRootElement
public class Article implements Serializable, MinervaEntity {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @JsonIgnore
  private int id;

  /**
   * Title of the article.
   */
  @Column(length = 255)
  private String title;

  /**
   * List of authors.
   */
  @ElementCollection(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SUBSELECT)
  @CollectionTable(name = "article_author", joinColumns = @JoinColumn(name = "article_id"))
  @Column(name = "author", length = 255)
  @OrderColumn(name = "idx")
  @Cascade({org.hibernate.annotations.CascadeType.ALL})
  private List<String> authors = new ArrayList<>();

  /**
   * Journal.
   */
  @Column(length = 255)
  private String journal;

  /**
   * Year of issue.
   */
  private Integer year;

  /**
   * Url that points to this article.
   */
  @Column(length = 255)
  private String link;

  /**
   * Pubmed identifier of the article.
   */
  @Column(nullable = false)
  private String pubmedId;

  /**
   * How many citations were made to this article.
   */
  private int citationCount;

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * @return {@link #title}
   */
  public String getTitle() {
    return title;
  }

  protected Article() {
  }

  public Article(final String pubmedId) {
    this.pubmedId = pubmedId;
  }

  /**
   * @param title new {@link #title} value
   */
  @XmlElement
  public void setTitle(final String title) {
    this.title = title;
  }

  /**
   * @return {@link #authors}
   */
  public List<String> getAuthors() {
    return authors;
  }

  /**
   * @param authors new {@link #title} value
   */
  public void setAuthors(final List<String> authors) {
    this.authors = authors;
  }

  public void addAuthor(final String author) {
    this.authors.add(author);
  }

  /**
   * @return {@link #journal}
   */
  public String getJournal() {
    return journal;
  }

  /**
   * @param journal new {@link #journal} value
   */
  @XmlElement
  public void setJournal(final String journal) {
    this.journal = journal;
  }

  /**
   * @return {@link #year}
   */
  public Integer getYear() {
    return year;
  }

  /**
   * @param year new {@link #year}
   */
  @XmlElement
  public void setYear(final Integer year) {
    this.year = year;
  }

  /**
   * @return comma separated string with authors
   */
  @JsonIgnore
  public String getStringAuthors() {
    String result = "";
    for (final String string : authors) {
      if (!result.equalsIgnoreCase("")) {
        result += ", ";
      }
      result += string;
    }
    return result;
  }

  /**
   * @return {@link #link}
   */
  public String getLink() {
    return link;
  }

  /**
   * @param link new {@link #link}
   */
  public void setLink(final String link) {
    this.link = link;
  }

  /**
   * @return {@link #pubmedId}
   */
  public String getPubmedId() {
    return pubmedId;
  }

  /**
   * @param pubmedId new {@link #pubmedId} value
   */
  public void setPubmedId(final String pubmedId) {
    this.pubmedId = pubmedId;
  }

  /**
   * @return {@link #citationCount}
   */
  public int getCitationCount() {
    return citationCount;
  }

  /**
   * @param citationCount new {@link #citationCount} value
   */
  public void setCitationCount(final int citationCount) {
    this.citationCount = citationCount;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
