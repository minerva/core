package lcsb.mapviewer.model.cache;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 * Defines types of objects stored in the cache and interfaces used for
 * refreshing them.
 *
 * @author Piotr Gawron
 */
@Entity
public class CacheType implements MinervaEntity {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Class of object that will refresh the data of given type.
   */
  private String className;

  /**
   * Unique local database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * How many days cached value is valid.
   */
  private int validity;

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * @return the validity
   * @see #validity
   */
  public int getValidity() {
    return validity;
  }

  /**
   * @param validity the validity to set
   * @see #validity
   */
  public void setValidity(final int validity) {
    this.validity = validity;
  }

  /**
   * @return the className
   * @see #className
   */
  public String getClassName() {
    return className;
  }

  /**
   * @param className the className to set
   * @see #className
   */
  public void setClassName(final String className) {
    this.className = className;
  }

  /**
   * @return the id
   * @see #id
   */
  @Override
  public int getId() {
    return id;
  }

  /**
   * @param id the id to set
   * @see #id
   */
  public void setId(final int id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return className + " [id: " + id + "]";
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
