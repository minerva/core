package lcsb.mapviewer.model.cache;

import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Calendar;

/**
 * Database object representing big file cached in file system.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("BIG_FILE_ENTRY")
public class BigFileEntry extends FileEntry implements Serializable, MinervaEntity {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Url from which file was obtained.
   */
  @Column(columnDefinition = "TEXT")
  private String url;

  /**
   * When file was downloaded.
   */
  private Calendar downloadDate;

  /**
   * What is the download progress (value between 0 and
   * {@link IProgressUpdater#MAX_PROGRESS 100}).
   */
  private Double downloadProgress;

  /**
   * {@link Thread} identifier that is downloading the file.
   */
  private Long downloadThreadId;

  /**
   * @return the url
   * @see #url
   */
  public String getUrl() {
    return url;
  }

  /**
   * @param url the url to set
   * @see #url
   */
  public void setUrl(final String url) {
    this.url = url;
  }

  /**
   * @return the downloadDate
   * @see #downloadDate
   */
  public Calendar getDownloadDate() {
    return downloadDate;
  }

  /**
   * @param downloadDate the downloadDate to set
   * @see #downloadDate
   */
  public void setDownloadDate(final Calendar downloadDate) {
    this.downloadDate = downloadDate;
  }

  /**
   * @return the downloadProgress
   * @see #downloadProgress
   */
  public Double getDownloadProgress() {
    return downloadProgress;
  }

  /**
   * @param downloadProgress the downloadProgress to set
   * @see #downloadProgress
   */
  public void setDownloadProgress(final Double downloadProgress) {
    this.downloadProgress = downloadProgress;
  }

  /**
   * @return the downloadThreadId
   * @see #downloadThreadId
   */
  public Long getDownloadThreadId() {
    return downloadThreadId;
  }

  /**
   * @param downloadThreadId the downloadThreadId to set
   * @see #downloadThreadId
   */
  public void setDownloadThreadId(final Long downloadThreadId) {
    this.downloadThreadId = downloadThreadId;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
