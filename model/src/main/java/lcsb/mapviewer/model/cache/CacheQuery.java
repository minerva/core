package lcsb.mapviewer.model.cache;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import org.hibernate.cache.spi.entry.CacheEntry;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.Calendar;

/**
 * Object that defines cached value retrieved from external resource. The key in
 * external resource can be any String.
 *
 * @author Piotr Gawron
 * @see CacheEntry
 */
@Entity
public class CacheQuery implements Serializable, MinervaEntity {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique local database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Unique external identifier. It can contain any String (for instance url to
   * a webpage), however shorter strings are preferred due to performance
   * reasons.
   */
  @Column(columnDefinition = "TEXT")
  private String query;

  /**
   * Value in the external resource.
   */
  @Column(columnDefinition = "TEXT")
  private String value;

  /**
   * What kind of resource. Type allows to refresh value from original resource
   * if needed
   */
  @Column(nullable = false)
  private Integer type;

  /**
   * When the value expires.
   */
  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Calendar expires = Calendar.getInstance();

  /**
   * When this value was accessed.
   */
  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Calendar accessed = Calendar.getInstance();

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * @return the id
   * @see #id
   */
  @Override
  public int getId() {
    return id;
  }

  /**
   * @param id the id to set
   * @see #id
   */
  public void setId(final int id) {
    this.id = id;
  }

  /**
   * @return the query
   * @see #query
   */
  public String getQuery() {
    return query;
  }

  /**
   * @param query the query to set
   * @see #query
   */
  public void setQuery(final String query) {
    this.query = query;
  }

  /**
   * @return the value
   * @see #value
   */
  public String getValue() {
    return value;
  }

  /**
   * @param value the value to set
   * @see #value
   */
  public void setValue(final String value) {
    this.value = value;
  }

  /**
   * @return the type
   * @see #type
   */
  public Integer getType() {
    return type;
  }

  /**
   * @param type the type to set
   * @see #type
   */
  public void setType(final Integer type) {
    this.type = type;
  }

  /**
   * @param type2 new {@link #type} value
   */
  public void setType(final CacheType type2) {
    this.type = type2.getId();
  }

  /**
   * @return the expires
   * @see #expires
   */
  public Calendar getExpires() {
    return expires;
  }

  /**
   * @param expires the expires to set
   * @see #expires
   */
  public void setExpires(final Calendar expires) {
    this.expires = expires;
  }

  /**
   * @return the accessed
   * @see #accessed
   */
  public Calendar getAccessed() {
    return accessed;
  }

  /**
   * @param accessed the accessed to set
   * @see #accessed
   */
  public void setAccessed(final Calendar accessed) {
    this.accessed = accessed;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
