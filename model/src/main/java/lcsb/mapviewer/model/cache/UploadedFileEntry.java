package lcsb.mapviewer.model.cache;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.modelutils.serializer.model.cache.UploadedFileEntrySerializer;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

/**
 * Database object representing file uploaded into system.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("UPLOADED_FILE_ENTRY")
@JsonSerialize(using = UploadedFileEntrySerializer.class)
public class UploadedFileEntry extends FileEntry implements MinervaEntity {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Length of the file.
   */
  private long length;

  @ManyToOne(fetch = FetchType.LAZY)
  private User owner;

  /**
   * Default constructor.
   */
  public UploadedFileEntry() {

  }

  /**
   * Constructor that copies data from the parameter.
   *
   * @param original original object from which data will be copied
   */
  public UploadedFileEntry(final UploadedFileEntry original) {
    super(original);
  }

  public long getLength() {
    return length;
  }

  public void setLength(final long length) {
    this.length = length;
  }

  public User getOwner() {
    return owner;
  }

  public void setOwner(final User owner) {
    this.owner = owner;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
