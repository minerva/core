package lcsb.mapviewer.model.map.reaction.type;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines a standard CellDesigner known transition omitted reaction.
 * It must have at least one reactant and one product.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("KNOWN_TRANSITION_OMMITED")
public class KnownTransitionOmittedReaction extends Reaction implements SimpleReactionInterface {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  protected KnownTransitionOmittedReaction() {
    super();
  }

  public KnownTransitionOmittedReaction(final String elementId) {
    super(elementId);
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   *
   * @param result parent reaction from which we copy data
   */
  public KnownTransitionOmittedReaction(final Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Known transition omitted";
  }

  @Override
  public ReactionRect getReactionRect() {
    return ReactionRect.RECT_SLASH;
  }

  @Override
  public KnownTransitionOmittedReaction copy() {
    if (this.getClass() == KnownTransitionOmittedReaction.class) {
      return new KnownTransitionOmittedReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermReactionType.KNOWN_TRANSITION_OMITTED.getSBO();
  }
}
