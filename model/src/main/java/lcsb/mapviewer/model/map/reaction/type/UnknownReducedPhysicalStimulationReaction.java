package lcsb.mapviewer.model.map.reaction.type;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines a standard CellDesigner unknown reduced physical
 * stimulation reaction. It must have at least one reactant and one product.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("U_REDUCED_PHYSICAL_STIM")
public class UnknownReducedPhysicalStimulationReaction extends Reaction
    implements SimpleReactionInterface, ReducedNotation {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  protected UnknownReducedPhysicalStimulationReaction() {
    super();
  }

  public UnknownReducedPhysicalStimulationReaction(final String elementId) {
    super(elementId);
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   *
   * @param result parent reaction from which we copy data
   */
  public UnknownReducedPhysicalStimulationReaction(final Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Unknown reduced physical stimulation";
  }

  @Override
  public ReactionRect getReactionRect() {
    return null;
  }

  @Override
  public UnknownReducedPhysicalStimulationReaction copy() {
    if (this.getClass() == UnknownReducedPhysicalStimulationReaction.class) {
      return new UnknownReducedPhysicalStimulationReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermReactionType.UNKNOWN_REDUCED_PHYSICAL_STIMULATION.getSBO();
  }
}
