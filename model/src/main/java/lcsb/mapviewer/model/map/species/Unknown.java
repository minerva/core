package lcsb.mapviewer.model.map.species;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.sbo.SBOTermSpeciesType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.awt.Color;

/**
 * Entity representing unknown element on the map.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("UNKNOWN")
public class Unknown extends Species {

  public static final Color DEFAULT_BORDER_COLOR = new Color(0, 0, 0, 0);
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Empty constructor required by hibernate.
   */
  Unknown() {
  }

  /**
   * Constructor that creates a copy of the element given in the parameter.
   *
   * @param original original object that will be used for creating copy
   */
  public Unknown(final Unknown original) {
    super(original);
  }

  /**
   * Default constructor.
   *
   * @param elementId unique (within model) element identifier
   */
  public Unknown(final String elementId) {
    setElementId(elementId);
    setBorderColor(DEFAULT_BORDER_COLOR);
  }

  @Override
  public Unknown copy() {
    if (this.getClass() == Unknown.class) {
      return new Unknown(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  @JsonIgnore
  public String getStringType() {
    return "Unknown";
  }

  @Override
  public String getSboTerm() {
    return SBOTermSpeciesType.UNKNOWN.getSBO();
  }

}
