package lcsb.mapviewer.model.map.reaction.type;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines a standard CellDesigner reduced trigger reaction. It must
 * have at least one reactant and one product.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("REDUCED_TRIGGER_REACTION")
public class ReducedTriggerReaction extends Reaction implements SimpleReactionInterface, ReducedNotation {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  protected ReducedTriggerReaction() {
    super();
  }

  public ReducedTriggerReaction(final String elementId) {
    super(elementId);
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   *
   * @param result parent reaction from which we copy data
   */
  public ReducedTriggerReaction(final Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Reduced trigger";
  }

  @Override
  public ReactionRect getReactionRect() {
    return null;
  }

  @Override
  public ReducedTriggerReaction copy() {
    if (this.getClass() == ReducedTriggerReaction.class) {
      return new ReducedTriggerReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermReactionType.REDUCED_TRIGGER.getSBO();
  }
}
