package lcsb.mapviewer.model.map.kinetics;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.XmlParser;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.modelutils.serializer.MathMLLambdaSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Representation of a single SBML function
 *
 * @author Piotr Gawron
 */
@Entity
@XmlRootElement
public class SbmlFunction implements MinervaEntity, SbmlArgument {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static final Logger logger = LogManager.getLogger();
  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(length = 255)
  private String functionId;

  @Column(length = 255)
  private String name;

  @Column(columnDefinition = "TEXT")
  private String definition;

  @ElementCollection(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SUBSELECT)
  @CollectionTable(name = "sbml_function_arguments", joinColumns = @JoinColumn(name = "sbml_function_id"))
  @OrderColumn(name = "idx")
  @Column(name = "argument_name", length = 255)
  private List<String> arguments = new ArrayList<>();

  /**
   * Map model object to which function belongs to.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JsonIgnore
  private ModelData model;

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * Constructor required by hibernate.
   */
  SbmlFunction() {
    super();
  }

  public SbmlFunction(final String functionId) {
    this.functionId = functionId;
  }

  public SbmlFunction(final SbmlFunction original) {
    this.functionId = original.getFunctionId();
    this.definition = original.getDefinition();
    this.name = original.getName();
    for (final String argument : original.getArguments()) {
      this.addArgument(argument);
    }
  }

  public String getFunctionId() {
    return functionId;
  }

  public void setFunctionId(final String functionId) {
    this.functionId = functionId;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public List<String> getArguments() {
    return arguments;
  }

  private void setArguments(final List<String> arguments) {
    this.arguments = arguments;
  }

  public String getDefinition() {
    return definition;
  }

  @JsonProperty("mathMlPresentation")
  @JsonSerialize(using = MathMLLambdaSerializer.class)
  private String getMathMLDefinition() {
    return definition;
  }

  public void setDefinition(final String definition) throws InvalidXmlSchemaException {
    this.definition = definition;
    List<String> result = new ArrayList<>();
    Document document = XmlParser.getXmlDocumentFromString(definition);
    List<Node> variables = XmlParser.getAllNotNecessirellyDirectChild("bvar", document);
    for (final Node node : variables) {
      result.add(XmlParser.getNodeValue(XmlParser.getNode("ci", node)).trim());
    }
    this.setArguments(result);
  }

  private void addArgument(final String argument) {
    arguments.add(argument);
  }

  @Override
  public SbmlFunction copy() {
    return new SbmlFunction(this);
  }

  @Override
  @JsonIgnore
  public String getElementId() {
    return getFunctionId();
  }

  public ModelData getModel() {
    return model;
  }

  public void setModel(final ModelData model) {
    this.model = model;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }

}
