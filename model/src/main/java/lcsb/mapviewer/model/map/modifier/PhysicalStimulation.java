package lcsb.mapviewer.model.map.modifier;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.species.Element;

/**
 * This class defines physical stimulation modifier in the reaction.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("PHYSICAL_STIMULATION_MODIFIER")
public class PhysicalStimulation extends Modifier {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  protected PhysicalStimulation() {
    super();
  }

  /**
   * Constructor that creates physical stimulation modifier for given element.
   * 
   * @param element
   *          element object to which this modifier is assigned
   */
  public PhysicalStimulation(final Element element) {
    super(element);
    if (element == null) {
      throw new InvalidArgumentException("Element cannot be null");
    }
  }

  /**
   * Constructor that creates object with data taken from parameter physical
   * stimulation.
   * 
   * @param physicalStimulation
   *          object from which data are initialized
   */
  public PhysicalStimulation(final PhysicalStimulation physicalStimulation) {
    super(physicalStimulation);
  }

  @Override
  public PhysicalStimulation copy() {
    if (this.getClass() == PhysicalStimulation.class) {
      return new PhysicalStimulation(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
