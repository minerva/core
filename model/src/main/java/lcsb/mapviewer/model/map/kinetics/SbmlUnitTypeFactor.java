package lcsb.mapviewer.model.map.kinetics;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Representation of a single SBML unit factor. For example unit for velocity is
 * m/s. It means that we have two unit types here:
 * <ul>
 * <li>metre^1</li>
 * <li>second^-1</li>
 * </ul>
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@XmlRootElement
public class SbmlUnitTypeFactor implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();
  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Enumerated(EnumType.STRING)
  private SbmlUnitType unitType;

  private int exponent = 1;
  private int scale = 0;
  private double multiplier = 1.0;

  @ManyToOne(fetch = FetchType.LAZY)
  @JsonIgnore
  private SbmlUnit unit;

  /**
   * Constructor required by hibernate.
   */
  SbmlUnitTypeFactor() {
    super();
  }

  public SbmlUnitTypeFactor(final SbmlUnitType unitType, final int exponent, final int scale, final double multiplier) {
    this.unitType = unitType;
    this.exponent = exponent;
    this.scale = scale;
    this.multiplier = multiplier;
  }

  public SbmlUnitTypeFactor(final SbmlUnitTypeFactor original) {
    this(original.getUnitType(), original.getExponent(), original.getScale(), original.getMultiplier());
  }

  public SbmlUnitType getUnitType() {
    return unitType;
  }

  public void setUnitType(final SbmlUnitType unitType) {
    this.unitType = unitType;
  }

  public int getExponent() {
    return exponent;
  }

  public void setExponent(final int exponent) {
    this.exponent = exponent;
  }

  public int getScale() {
    return scale;
  }

  public void setScale(final int scale) {
    this.scale = scale;
  }

  public double getMultiplier() {
    return multiplier;
  }

  public void setMultiplier(final double multiplier) {
    this.multiplier = multiplier;
  }

  public SbmlUnit getUnit() {
    return unit;
  }

  public void setUnit(final SbmlUnit unit) {
    this.unit = unit;
  }

  public SbmlUnitTypeFactor copy() {
    return new SbmlUnitTypeFactor(this);
  }

  public int getId() {
    return id;
  }

}
