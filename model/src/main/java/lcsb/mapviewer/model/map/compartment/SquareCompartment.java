package lcsb.mapviewer.model.map.compartment;

import lcsb.mapviewer.common.exception.NotImplementedException;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines compartment with oval shape.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("SQUARE_COMPARTMENT")
public class SquareCompartment extends Compartment {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Empty constructor required by hibernate.
   */
  SquareCompartment() {
    super();
  }

  /**
   * Constructor that creates a compartment with the new shape and takes the
   * reference data from the compartment given as parameter.
   *
   * @param original original compartment where the data was kept
   */
  public SquareCompartment(final Compartment original) {
    super(original);
  }

  /**
   * Default constructor.
   *
   * @param elementId identifier of the compartment
   */
  public SquareCompartment(final String elementId) {
    super();
    setElementId(elementId);
  }

  @Override
  public SquareCompartment copy() {
    if (this.getClass() == SquareCompartment.class) {
      return new SquareCompartment(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getShape() {
    return "SQUARE_COMPARTMENT";
  }

}
