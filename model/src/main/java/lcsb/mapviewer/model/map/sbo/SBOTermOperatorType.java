package lcsb.mapviewer.model.map.sbo;

import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.NandOperator;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.OrOperator;
import lcsb.mapviewer.model.map.reaction.UnknownOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public enum SBOTermOperatorType {
  AND(AndOperator.class, new String[]{"SBO:0000173"}),
  OR(OrOperator.class, new String[]{"SBO:0000174"}),
  NAND(NandOperator.class, new String[]{"SBO:0000238"}),
  UNKNOWN(UnknownOperator.class, new String[]{"SBO:0000237"}),
  ;

  private static Logger logger = LogManager.getLogger();

  private Class<? extends NodeOperator> clazz;
  private List<String> sboTerms = new ArrayList<>();

  private SBOTermOperatorType(final Class<? extends NodeOperator> clazz, final String[] inputSboTerms) {
    this.clazz = clazz;
    for (final String string : inputSboTerms) {
      sboTerms.add(string);
    }
  }

  public static Class<? extends NodeOperator> getClazzFromSboTerm(final String sboTerm) {
    Class<? extends NodeOperator> result = null;
    for (final SBOTermOperatorType term : values()) {
      for (final String string : term.sboTerms) {
        if (string.equalsIgnoreCase(sboTerm)) {
          result = term.clazz;
        }
      }
    }
    return result;
  }

  public static String getTermByType(final Class<? extends NodeOperator> clazz) {
    for (final SBOTermOperatorType term : values()) {
      if (clazz.equals(term.clazz)) {
        return term.getSBO();
      }
    }
    logger.warn("Cannot find SBO term for class: " + clazz);
    return null;
  }

  public String getSBO() {
    if (!sboTerms.isEmpty()) {
      return sboTerms.get(0);
    }
    logger.warn("Cannot find SBO term for class: " + clazz);
    return null;
  }
}
