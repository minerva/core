package lcsb.mapviewer.model.map.reaction;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * Class representing "not and operator" between two or more nodes in the
 * reaction.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("NAND_OPERATOR_NODE")
public class NandOperator extends NodeOperator {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param operator
   *          parent operator from which we copy data
   */
  public NandOperator(final NandOperator operator) {
    super(operator);
  }

  /**
   * Default constructor.
   */
  public NandOperator() {
    super();
  }

  @Override
  public String getOperatorText() {
    return "~";
  }

  @Override
  public NandOperator copy() {
    if (this.getClass() == NandOperator.class) {
      return new NandOperator(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
