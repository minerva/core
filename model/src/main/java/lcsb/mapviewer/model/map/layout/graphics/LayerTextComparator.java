package lcsb.mapviewer.model.map.layout.graphics;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.ColorComparator;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.comparator.IntegerComparator;
import lcsb.mapviewer.common.comparator.StringComparator;

/**
 * Comparator of {@link LayerText} class.
 * 
 * @author Piotr Gawron
 * 
 * 
 */
public class LayerTextComparator extends Comparator<LayerText> {

  @SuppressWarnings("unused")
  private Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public LayerTextComparator(final double epsilon) {
    super(LayerText.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public LayerTextComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(final LayerText arg0, final LayerText arg1) {
    DoubleComparator doubleComparator = new DoubleComparator(epsilon);
    StringComparator stringComparator = new StringComparator();
    ColorComparator colorComparator = new ColorComparator();
    IntegerComparator integerComparator = new IntegerComparator();

    if (doubleComparator.compare(arg0.getWidth(), arg1.getWidth()) != 0) {
      return doubleComparator.compare(arg0.getWidth(), arg1.getWidth(), "width");
    }

    if (doubleComparator.compare(arg0.getHeight(), arg1.getHeight()) != 0) {
      return doubleComparator.compare(arg0.getHeight(), arg1.getHeight(), "height");
    }

    if (doubleComparator.compare(arg0.getX(), arg1.getX()) != 0) {
      return doubleComparator.compare(arg0.getX(), arg1.getX(), "x");
    }

    if (doubleComparator.compare(arg0.getY(), arg1.getY()) != 0) {
      return doubleComparator.compare(arg0.getY(), arg1.getY(), "y");
    }

    if (colorComparator.compare(arg0.getColor(), arg1.getColor()) != 0) {
      return colorComparator.compare(arg0.getColor(), arg1.getColor(), "color");
    }

    if (stringComparator.compare(arg0.getNotes(), arg1.getNotes()) != 0) {
      return stringComparator.compare(arg0.getNotes(), arg1.getNotes(), "notes");
    }

    if (doubleComparator.compare(arg0.getFontSize(), arg1.getFontSize()) != 0) {
      return doubleComparator.compare(arg0.getFontSize(), arg1.getFontSize(), "fontSize");
    }

    if (integerComparator.compare(arg0.getZ(), arg1.getZ()) != 0) {
      return integerComparator.compare(arg0.getZ(), arg1.getZ(), "z");
    }

    return 0;
  }

}
