package lcsb.mapviewer.model.map.compartment;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.model.Model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines compartment that covers right part of the model up to some
 * border on the left side.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("RIGHT_SQUARE_COMPARTMENT")
public class RightSquareCompartment extends Compartment {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Empty constructor required by hibernate.
   */
  RightSquareCompartment() {
    super();
  }

  /**
   * Constructor that creates a compartment with the new shape but takes the
   * reference data from the compartment given as parameter.
   *
   * @param original original compartment where the data was kept
   * @param model    model object to which the compartment will be assigned
   */
  public RightSquareCompartment(final Compartment original, final Model model) {
    super(original);
    setX(0.0);
    setWidth(model.getWidth() * 2);
    setY(0.0);
    setHeight(model.getHeight() * 2);
  }

  /**
   * Constructor that creates a compartment with the new shape and takes the
   * reference data from the compartment given as parameter.
   *
   * @param original original compartment where the data was kept
   */
  public RightSquareCompartment(final RightSquareCompartment original) {
    super(original);
  }

  /**
   * Default constructor.
   *
   * @param elementId identifier of the compartment
   */
  public RightSquareCompartment(final String elementId) {
    setElementId(elementId);
  }

  /**
   * Sets the CellDesigner point coordinates. In the implementation of
   * BottomSquare it should define left border.
   *
   * @param y to be ignored
   * @param x left border
   */
  public void setPoint(final String x, final String y) {
    // set left border
    setX(x);
    setWidth(getWidth() - getX());
  }

  @Override
  public RightSquareCompartment copy() {
    if (this.getClass() == RightSquareCompartment.class) {
      return new RightSquareCompartment(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getShape() {
    return "RIGHT_SQUARE_COMPARTMENT";
  }

}
