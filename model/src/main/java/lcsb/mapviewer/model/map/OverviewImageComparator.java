package lcsb.mapviewer.model.map;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.IntegerComparator;
import lcsb.mapviewer.common.comparator.StringComparator;

/**
 * This class implements comparator interface for {@link OverviewImage}.
 * 
 * @author Piotr Gawron
 * 
 */
public class OverviewImageComparator extends Comparator<OverviewImage> {
  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public OverviewImageComparator(final double epsilon) {
    super(OverviewImage.class);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public OverviewImageComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(final OverviewImage arg0, final OverviewImage arg1) {
    StringComparator stringComparator = new StringComparator();

    if (stringComparator.compare(arg0.getFilename(), arg1.getFilename()) != 0) {
      logger.debug("filename different: " + arg0.getFilename() + ", " + arg1.getFilename());
      return stringComparator.compare(arg0.getFilename(), arg1.getFilename());
    }

    IntegerComparator integerComparator = new IntegerComparator();

    if (integerComparator.compare(arg0.getWidth(), arg1.getWidth()) != 0) {
      logger.debug("width different: " + arg0.getWidth() + ", " + arg1.getWidth());
      return integerComparator.compare(arg0.getWidth(), arg1.getWidth());
    }

    if (integerComparator.compare(arg0.getHeight(), arg1.getHeight()) != 0) {
      logger.debug("height different: " + arg0.getHeight() + ", " + arg1.getHeight());
      return integerComparator.compare(arg0.getHeight(), arg1.getHeight());
    }

    if (compareLinks(arg0.getLinks(), arg1.getLinks()) != 0) {
      return compareLinks(arg0.getLinks(), arg1.getLinks());
    }

    return 0;
  }

  /**
   * Compare two list of {@link OverviewLink links}.
   * 
   * @param links
   *          first list to compare
   * @param links2
   *          second list to compare
   * @return 0 if the list are identical, -1/1 otherwise
   */
  private int compareLinks(final List<OverviewLink> links, final List<OverviewLink> links2) {
    IntegerComparator integerComparator = new IntegerComparator();
    OverviewLinkComparator ovc = new OverviewLinkComparator(epsilon);
    if (integerComparator.compare(links.size(), links2.size()) != 0) {
      return integerComparator.compare(links.size(), links2.size());
    }
    for (int i = 0; i < links.size(); i++) {
      if (ovc.compare(links.get(i), links2.get(i)) != 0) {
        return ovc.compare(links.get(i), links2.get(i));
      }
    }
    return 0;
  }

}
