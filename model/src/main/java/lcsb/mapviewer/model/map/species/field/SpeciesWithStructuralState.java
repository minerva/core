package lcsb.mapviewer.model.map.species.field;

/**
 * Interface implemented by species that support {@link StructuralState}
 * modification.
 * 
 * @author Piotr Gawron
 *
 */
public interface SpeciesWithStructuralState extends SpeciesWithModificationResidue {

  /**
   * Adds a {@link StructuralState} to the species.
   * 
   * @param structuralState
   *          {@link StructuralState} to add
   */
  void addStructuralState(final StructuralState structuralState);

}
