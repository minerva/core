package lcsb.mapviewer.model.map.species;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.sbo.SBOTermSpeciesType;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.RegulatoryRegion;
import lcsb.mapviewer.model.map.species.field.SpeciesWithCodingRegion;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationSite;
import lcsb.mapviewer.model.map.species.field.SpeciesWithRegulatoryRegion;
import lcsb.mapviewer.model.map.species.field.SpeciesWithTranscriptionSite;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;

/**
 * Entity representing gene element on the map.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@DiscriminatorValue("GENE")
public class Gene extends Species implements SpeciesWithCodingRegion, SpeciesWithModificationSite,
    SpeciesWithRegulatoryRegion, SpeciesWithTranscriptionSite {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * List of modifications for the Gene.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "species", orphanRemoval = true)
  @LazyCollection(LazyCollectionOption.FALSE)
  @Fetch(FetchMode.SUBSELECT)
  private List<ModificationResidue> modificationResidues = new ArrayList<>();

  /**
   * Empty constructor required by hibernate.
   */
  Gene() {
  }

  /**
   * Constructor that creates a copy of the element given in the parameter.
   * 
   * @param original
   *          original object that will be used for creating copy
   */
  public Gene(final Gene original) {
    super(original);
    for (final ModificationResidue mr : original.getModificationResidues()) {
      addModificationResidue(mr.copy());
    }
  }

  /**
   * Default constructor.
   * 
   * @param elementId
   *          unique (within model) element identifier
   */
  public Gene(final String elementId) {
    setElementId(elementId);
  }

  /**
   * Adds {@link ModificationResidue}.
   * 
   * @param modificationResidue
   *          {@link ModificationResidue} to be added
   */
  private void addModificationResidue(final ModificationResidue modificationResidue) {
    modificationResidues.add(modificationResidue);
    modificationResidue.setSpecies(this);

  }

  @Override
  public void addCodingRegion(final CodingRegion codingRegion) {
    this.addModificationResidue(codingRegion);
  }

  @Override
  public void addModificationSite(final ModificationSite modificationSite) {
    this.addModificationResidue(modificationSite);
  }

  @Override
  public void addRegulatoryRegion(final RegulatoryRegion regulatoryRegion) {
    this.addModificationResidue(regulatoryRegion);
  }

  @Override
  public void addTranscriptionSite(final TranscriptionSite transcriptionSite) {
    this.addModificationResidue(transcriptionSite);
  }

  @Override
  public Gene copy() {
    if (this.getClass() == Gene.class) {
      return new Gene(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * @return the modificationResidues
   * @see #modificationResidues
   */
  @Override
  public List<ModificationResidue> getModificationResidues() {
    return modificationResidues;
  }

  /**
   * @param modificationResidues
   *          the modificationResidues to set
   * @see #modificationResidues
   */
  public void setModificationResidues(final List<ModificationResidue> modificationResidues) {
    this.modificationResidues = modificationResidues;
  }

  @Override
  @JsonIgnore
  public String getStringType() {
    return "Gene";
  }

  @Override
  public String getSboTerm() {
    return SBOTermSpeciesType.GENE.getSBO();
  }

}
