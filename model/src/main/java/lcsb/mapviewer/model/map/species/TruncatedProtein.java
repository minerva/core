package lcsb.mapviewer.model.map.species;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.sbo.SBOTermSpeciesType;

/**
 * Entity representing truncated protein element on the map.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@DiscriminatorValue("TRUNCATED_PROTEIN")
public class TruncatedProtein extends Protein {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Empty constructor required by hibernate.
   */
  TruncatedProtein() {
  }

  /**
   * Constructor that creates a copy of the element given in the parameter.
   * 
   * @param original
   *          original object that will be used for creating copy
   */
  public TruncatedProtein(final TruncatedProtein original) {
    super(original);
  }

  /**
   * Default constructor.
   * 
   * @param elementId
   *          unique (within model) element identifier
   */
  public TruncatedProtein(final String elementId) {
    super(elementId);
  }

  @Override
  public TruncatedProtein copy() {
    if (this.getClass() == TruncatedProtein.class) {
      return new TruncatedProtein(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermSpeciesType.TRUNCATED_PROTEIN.getSBO();
  }

}
