package lcsb.mapviewer.model.map.compartment;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.comparator.IntegerComparator;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.ElementComparator;

/**
 * This class implements comparator interface for {@link Compartment} objects.
 * 
 * @author Piotr Gawron
 * 
 * @see Compartment
 * 
 */
public class CompartmentComparator extends Comparator<Compartment> {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public CompartmentComparator(final double epsilon) {
    super(Compartment.class);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public CompartmentComparator() {
    this(Configuration.EPSILON);
  }

  protected Comparator<?> getParentComparator() {
    return new ElementComparator(epsilon);
  }

  @Override
  protected int internalCompare(final Compartment arg0, final Compartment arg1) {
    ElementComparator elementComparator = new ElementComparator(epsilon);
    DoubleComparator doubleComparator = new DoubleComparator(epsilon);
    IntegerComparator integerComparator = new IntegerComparator();

    if (doubleComparator.compare(arg0.getThickness(), arg1.getThickness()) != 0) {
      logger.debug("Thickness different: " + arg0.getThickness() + ", " + arg1.getThickness());
      return doubleComparator.compare(arg0.getThickness(), arg1.getThickness());
    }

    if (doubleComparator.compare(arg0.getOuterWidth(), arg1.getOuterWidth()) != 0) {
      logger.debug("Outer width different: " + arg0.getOuterWidth() + ", " + arg1.getOuterWidth());
      return doubleComparator.compare(arg0.getOuterWidth(), arg1.getOuterWidth());
    }

    if (doubleComparator.compare(arg0.getInnerWidth(), arg1.getInnerWidth()) != 0) {
      logger.debug("Inner width different: " + arg0.getInnerWidth() + ", " + arg1.getInnerWidth());
      return doubleComparator.compare(arg0.getInnerWidth(), arg1.getInnerWidth());
    }

    if (integerComparator.compare(arg0.getElements().size(), arg1.getElements().size()) != 0) {
      logger.debug("Elements number different: " + arg0.getElements().size() + ", " + arg1.getElements().size());
      return integerComparator.compare(arg0.getElements().size(), arg1.getElements().size());
    }

    Map<String, Element> map1 = new HashMap<>();
    Map<String, Element> map2 = new HashMap<>();

    for (final Element element : arg0.getElements()) {
      if (map1.get(element.getElementId()) != null) {
        throw new InvalidArgumentException("Few elements with the same id: " + element.getElementId());
      }
      map1.put(element.getElementId(), element);
    }

    for (final Element element : arg1.getElements()) {
      if (map2.get(element.getElementId()) != null) {
        throw new InvalidArgumentException("Few elements with the same id: " + element.getElementId());
      }
      map2.put(element.getElementId(), element);
    }

    for (final Element element : arg0.getElements()) {
      Element element2 = map2.get(element.getElementId());
      int status = elementComparator.compare(element, element2);
      if (status != 0) {
        logger.debug("Couldn't match element: " + element.getElementId() + ", " + element2);
        return status;
      }
    }

    return 0;
  }
}
