package lcsb.mapviewer.model.map.reaction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.graphics.PolylineData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * This class defines abstract node of the reaction. By the design there are two
 * main classes of nodes (that are extended further):
 * <ul>
 * <li>end nodes with linked elements
 * {@link lcsb.mapviewer.model.map.reaction.ReactionNode ReactionNode} - defines
 * what elements participate in the reaction and in what role (product,
 * reactant)</li>
 * <li>operator nodes {@link lcsb.mapviewer.model.map.reaction.NodeOperator
 * NodeOperator} - defines relation between different participants of the
 * reaction (and operator, split/dissociation, etc.)</li>
 * </ul>
 *
 * @author Piotr Gawron
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "node_type_db", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("GENERIC_NODE")
@Table(name = "reaction_node_table")
public abstract class AbstractNode implements Serializable, MinervaEntity {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  protected static Logger logger = LogManager.getLogger();

  /**
   * Database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * This object define the node that is next in the schema (in the flow from
   * reactants to products). In case the node is terminal product this object is
   * set to null.
   */
  @ManyToOne(cascade = javax.persistence.CascadeType.ALL, optional = true)
  @JsonIgnore
  private NodeOperator nodeOperatorForInput = null;

  /**
   * This object define the node that is before in the schema (in the flow from
   * reactants to products). In case the node is reactant this object is set to
   * null.
   */
  @ManyToOne(cascade = javax.persistence.CascadeType.ALL, optional = true)
  @JsonIgnore
  private NodeOperator nodeOperatorForOutput = null;

  /**
   * Reaction object where this node is located.
   */
  @ManyToOne
  @JsonIgnore
  private Reaction reaction = null;

  /**
   * Line that represent this node in graphical representation.
   */
  @Cascade({CascadeType.ALL})
  @OneToOne(fetch = FetchType.EAGER, optional = false)
  @NotNull
  private PolylineData line;

  @Version
  @JsonIgnore
  private long entityVersion;

  protected AbstractNode() {

  }

  /**
   * Constructor that copies the data from the original node in the parameter.
   *
   * @param node original node based on which this one will be created
   */
  public AbstractNode(final AbstractNode node) {
    this.reaction = node.reaction;
    this.line = new PolylineData(node.getLine());
  }

  /**
   * @return the reaction
   * @see #reaction
   */
  public Reaction getReaction() {
    return reaction;
  }

  /**
   * @param reaction the reaction to set
   * @see #reaction
   */
  public void setReaction(final Reaction reaction) {
    this.reaction = reaction;
  }

  /**
   * @return the line
   * @see #line
   */
  public PolylineData getLine() {
    return line;
  }

  /**
   * @param line the line to set
   * @see #line
   */
  public void setLine(final PolylineData line) {
    this.line = line;
  }

  /**
   * Creates a copy of the object.
   *
   * @return copy of the object
   */
  public abstract AbstractNode copy();

  /**
   * @return the nodeOperatorForInput
   * @see #nodeOperatorForInput
   */
  public NodeOperator getNodeOperatorForInput() {
    return nodeOperatorForInput;
  }

  /**
   * @param nodeOperatorForInput the nodeOperatorForInput to set
   * @see #nodeOperatorForInput
   */
  public void setNodeOperatorForInput(final NodeOperator nodeOperatorForInput) {
    this.nodeOperatorForInput = nodeOperatorForInput;
  }

  /**
   * @return the nodeOperatorForOutput
   * @see #nodeOperatorForOutput
   */
  public NodeOperator getNodeOperatorForOutput() {
    return nodeOperatorForOutput;
  }

  /**
   * @param nodeOperatorForOutput the nodeOperatorForOutput to set
   * @see #nodeOperatorForOutput
   */
  public void setNodeOperatorForOutput(final NodeOperator nodeOperatorForOutput) {
    this.nodeOperatorForOutput = nodeOperatorForOutput;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
