package lcsb.mapviewer.model.map;

import com.fasterxml.jackson.annotation.JsonProperty;
import lcsb.mapviewer.common.comparator.IntegerComparator;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.Reaction;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 * Interface that describes bio entity on the map (like Protein or Reaction).
 *
 * @author Piotr Gawron
 */
public interface BioEntity extends Drawable, MinervaEntity {
  Comparator<? super BioEntity> ID_COMPARATOR = new Comparator<BioEntity>() {

    @Override
    public int compare(final BioEntity o1, final BioEntity o2) {
      Integer id1 = null;
      Integer id2 = null;
      if (o1 != null) {
        id1 = o1.getId();
        if (o1 instanceof Reaction) {
          id1 *= -1;
        }
      }
      if (o2 != null) {
        id2 = o2.getId();
        if (o2 instanceof Reaction) {
          id2 *= -1;
        }
      }
      return new IntegerComparator().compare(id1, id2);
    }
  };

  /**
   * Returns list of {@link MiriamData annotations} for the object.
   *
   * @return list of {@link MiriamData annotations} for the object
   */
  Collection<MiriamData> getMiriamData();

  /**
   * Adds miriam annotations to the element.
   *
   * @param miriamData set of miriam annotations to be added
   */
  void addMiriamData(final Collection<MiriamData> miriamData);

  /**
   * Adds miriam annotation to the element.
   *
   * @param md miriam annotation to be added
   */
  void addMiriamData(final MiriamData md);

  /**
   * Returns the name of the object.
   *
   * @return the name of the object
   */
  String getName();

  /**
   * Sets the name to the object.
   *
   * @param name name of the object
   */
  void setName(final String name);

  /**
   * Returns notes about the object.
   *
   * @return notes about the object
   */
  String getNotes();

  /**
   * Sets notes about the object.
   *
   * @param notes new notes
   */
  void setNotes(final String notes);

  /**
   * Returns the symbol of the element.
   *
   * @return the symbol of the element
   */
  String getSymbol();

  /**
   * Sets symbol of the element.
   *
   * @param symbol new symbol
   */
  void setSymbol(final String symbol);

  /**
   * Get list of synonyms.
   *
   * @return list of synonyms
   */
  List<String> getSynonyms();

  /**
   * Sets list of synonyms to the element.
   *
   * @param synonyms new list
   */
  void setSynonyms(final List<String> synonyms);

  /**
   * Returns the abbreviation.
   *
   * @return the abbreviation
   */
  String getAbbreviation();

  /**
   * Sets abbreviation.
   *
   * @param abbreviation new abbreviation
   */
  void setAbbreviation(final String abbreviation);

  /**
   * Returns the formula.
   *
   * @return the formula
   */
  String getFormula();

  /**
   * Sets formula.
   *
   * @param formula new formula
   */
  void setFormula(final String formula);

  /**
   * Returns database identifier of the object.
   *
   * @return database identifier of the object
   */
  @Override
  int getId();

  /**
   * Return human readable {@link String} representing class.
   *
   * @return human readable {@link String} representing class
   */
  String getStringType();

  /**
   * Returns semantic zoom level visibility.
   *
   * @return semantic zoom level visibility
   */
  String getVisibilityLevel();

  /**
   * Sets semantic zoom level visibility.
   *
   * @param zoomLevelVisibility semantic zoom level visibility
   */
  void setVisibilityLevel(final String zoomLevelVisibility);

  /**
   * Sets semantic zoom level visibility.
   *
   * @param zoomLevelVisibility semantic zoom level visibility
   */
  void setVisibilityLevel(final Integer zoomLevelVisibility);

  /**
   * Returns the {@link Model} where BioEntity is located.
   *
   * @return the model
   */
  Model getModel();

  BioEntity copy();

  ModelData getModelData();

  @JsonProperty("sboTerm")
  String getSboTerm();
}
