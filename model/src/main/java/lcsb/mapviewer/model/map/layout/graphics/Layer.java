package lcsb.mapviewer.model.map.layout.graphics;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.model.ModelData;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Object representing layer with additional graphics element (like, lines,
 * texts, etc.) in the model.
 *
 * @author Piotr Gawron
 */
@Entity
public class Layer implements MinervaEntity {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  private static final Logger logger = LogManager.getLogger();

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Layer identifier (unique in single model).
   */
  @Column(length = 255)
  @JsonIgnore
  private String layerId;

  /**
   * Layer name.
   */
  @Column(length = 255)
  private String name;

  /**
   * Is the layer visible.
   */
  private boolean visible;

  /**
   * Is the layer locked (can be edited).
   */
  private boolean locked;

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * List of text objects on the layer.
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "layer")
  @JsonIgnore
  private Set<LayerText> texts = new HashSet<>();

  /**
   * List of line objects on the layer.
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = "layer_table_lines",
      joinColumns = {
          @JoinColumn(name = "layer_table_id", referencedColumnName = "id", nullable = false, updatable = false)},
      inverseJoinColumns = {@JoinColumn(name = "lines_id")}
  )
  @JsonIgnore
  private List<PolylineData> lines = new ArrayList<>();

  /**
   * List of rectangle objects on the layer.
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "layer")
  @JsonIgnore
  private Set<LayerRect> rectangles = new HashSet<>();

  /**
   * List of oval objects on the layer.
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "layer")
  @JsonIgnore
  private Set<LayerOval> ovals = new HashSet<>();

  /**
   * List of oval objects on the layer.
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "layer")
  @JsonIgnore
  private final Set<LayerImage> images = new HashSet<>();

  /**
   * ModelData to which layer belongs to.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JsonIgnore
  private ModelData model;

  @NotNull
  private Integer z;

  public Layer() {
  }

  /**
   * Constructor that copies data from the parameter.
   *
   * @param layer from this parameter layer data will be copied
   */
  public Layer(final Layer layer) {
    layerId = layer.getLayerId();
    name = layer.getName();
    visible = layer.isVisible();
    locked = layer.isLocked();
    z = layer.getZ();

    for (final LayerText lt : layer.getTexts()) {
      addLayerText(lt.copy());
    }

    for (final PolylineData lt : layer.getLines()) {
      addLayerLine(lt.copy());
    }

    for (final LayerRect lt : layer.getRectangles()) {
      addLayerRect(lt.copy());
    }

    for (final LayerOval lt : layer.getOvals()) {
      addLayerOval(lt.copy());
    }

    for (final LayerImage image : layer.getImages()) {
      addLayerImage(image.copy());
    }

    model = layer.getModel();
  }

  /**
   * Makes copy of the layer.
   *
   * @return copy of the layer
   */
  public Layer copy() {
    if (this.getClass() == Layer.class) {
      return new Layer(this);
    } else {
      throw new NotImplementedException("Method copy() should be overridden in class " + this.getClass());
    }
  }

  /**
   * @return the model
   * @see #model
   */
  public ModelData getModel() {
    return model;
  }

  /**
   * @param model the model to set
   * @see #model
   */
  public void setModel(final ModelData model) {
    this.model = model;
  }

  /**
   * @return the ovals
   * @see #ovals
   */
  public Set<LayerOval> getOvals() {
    return ovals;
  }

  /**
   * @param ovals the ovals to set
   * @see #ovals
   */
  public void setOvals(final Set<LayerOval> ovals) {
    this.ovals = ovals;
  }

  /**
   * @return the rectangles
   * @see #rectangles
   */
  public Set<LayerRect> getRectangles() {
    return rectangles;
  }

  /**
   * @param rectangles the rectangles to set
   * @see #rectangles
   */
  public void setRectangles(final Set<LayerRect> rectangles) {
    this.rectangles = rectangles;
  }

  /**
   * @return the lines
   * @see #lines
   */
  public List<PolylineData> getLines() {
    return lines;
  }

  /**
   * @param lines the lines to set
   * @see #lines
   */
  public void setLines(final List<PolylineData> lines) {
    this.lines = lines;
  }

  /**
   * @return the texts
   * @see #texts
   */
  public Set<LayerText> getTexts() {
    return texts;
  }

  /**
   * @param texts the texts to set
   * @see #texts
   */
  public void setTexts(final Set<LayerText> texts) {
    this.texts = texts;
  }

  /**
   * @return the locked
   * @see #locked
   */
  public boolean isLocked() {
    return locked;
  }

  /**
   * @param locked the locked to set
   * @see #locked
   */
  public void setLocked(final boolean locked) {
    this.locked = locked;
  }

  /**
   * Sets locked param from the text input.
   *
   * @param param text representing true/false
   * @see #locked
   */
  public void setLocked(final String param) {
    locked = param.equalsIgnoreCase("TRUE");
  }

  /**
   * @return the visible
   * @see #visible
   */
  public boolean isVisible() {
    return visible;
  }

  /**
   * @param visible the visible to set
   * @see #visible
   */
  public void setVisible(final boolean visible) {
    this.visible = visible;
  }

  /**
   * Sets visible param from the text input.
   *
   * @param param text representing true/false
   * @see #visible
   */
  public void setVisible(final String param) {
    visible = param.equalsIgnoreCase("TRUE");
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   * @see #name
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * Adds text to the layer.
   *
   * @param layerText text to add
   */
  public void addLayerText(final LayerText layerText) {
    texts.add(layerText);
    layerText.setLayer(this);
  }

  /**
   * Adds rectangle to the layer.
   *
   * @param layerRect rectangle to add
   */
  public void addLayerRect(final LayerRect layerRect) {
    rectangles.add(layerRect);
    layerRect.setLayer(this);
  }

  /**
   * Adds oval to the layer.
   *
   * @param layerOval oval to add
   */
  public void addLayerOval(final LayerOval layerOval) {
    ovals.add(layerOval);
    layerOval.setLayer(this);
  }

  /**
   * Adds line to the layer.
   *
   * @param layerLine line to add
   */
  public void addLayerLine(final PolylineData layerLine) {
    lines.add(layerLine);
  }

  /**
   * @return the layerId
   * @see #layerId
   */
  public String getLayerId() {
    return layerId;
  }

  /**
   * @param layerId the layerId to set
   * @see #layerId
   */
  public void setLayerId(final int layerId) {
    this.layerId = layerId + "";
  }

  public void setLayerId(final String layerId) {
    if (!NumberUtils.isDigits(layerId)) {
      logger.warn("LayerId should be numeric");
    }
    this.layerId = layerId;
  }

  /**
   * Adds lines to the layer.
   *
   * @param lines lines to add
   */
  public void addLayerLines(final Collection<PolylineData> lines) {
    for (final PolylineData layerLine : lines) {
      addLayerLine(layerLine);
    }
  }

  /**
   * Removes {@link LayerText} from {@link Layer}.
   *
   * @param toRemove object to remove
   */
  public void removeLayerText(final LayerText toRemove) {
    texts.remove(toRemove);
  }

  @JsonIgnore
  public boolean isEmpty() {
    return this.getOvals().isEmpty() && this.getRectangles().isEmpty() && this.getTexts().isEmpty()
        && this.getLines().isEmpty();
  }

  @JsonIgnore
  public Set<Drawable> getDrawables() {
    final Set<Drawable> result = new HashSet<>();
    result.addAll(lines);
    result.addAll(ovals);
    result.addAll(rectangles);
    result.addAll(texts);
    return result;
  }

  @Override
  public String toString() {
    return String.format("[%s;id:%s;name:%s]", this.getClass().getSimpleName(), getLayerId(), this.getName());
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  public Set<LayerImage> getImages() {
    return images;
  }

  public void addLayerImage(final LayerImage layerImage) {
    images.add(layerImage);
    layerImage.setLayer(this);
  }

  @Override
  public EntityType getEntityType() {
    return EntityType.LAYER;
  }

  public Integer getZ() {
    return z;
  }

  public void setZ(final Integer z) {
    this.z = z;
  }
}
