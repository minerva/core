package lcsb.mapviewer.model.map.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.StringComparator;

/**
 * This class implements comparator interface for {@link SubmodelConnection}. It
 * handles comparison of subclasses of {@link SubmodelConnection} class.
 * 
 * @author Piotr Gawron
 * 
 */
public class SubmodelConnectionComparator extends Comparator<SubmodelConnection> {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public SubmodelConnectionComparator(final double epsilon) {
    super(SubmodelConnection.class, true);
    this.epsilon = epsilon;
    addSubClassComparator(new ModelSubmodelConnectionComparator(epsilon));
    addSubClassComparator(new ElementSubmodelConnectionComparator(epsilon));
  }

  /**
   * Default constructor.
   */
  public SubmodelConnectionComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(final SubmodelConnection arg0, final SubmodelConnection arg1) {
    StringComparator stringComparator = new StringComparator();
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }
    } else if (arg1 == null) {
      return -1;
    }

    if (stringComparator.compare(arg0.getName(), arg1.getName()) != 0) {
      logger.debug("Name different: " + arg0.getName() + ", " + arg1.getName());
      return stringComparator.compare(arg0.getName(), arg1.getName());
    }

    if (arg0.getType() == null && arg1.getType() != null) {
      logger.debug("Type different: " + arg0.getType() + ", " + arg1.getType());
      return -1;
    }
    if (arg0.getType() != null) {
      if (arg1.getType() == null) {
        logger.debug("Type different: " + arg0.getType() + ", " + arg1.getType());
        return -1;
      }
      if (arg0.getType().compareTo(arg1.getType()) != 0) {
        logger.debug("Type different: " + arg0.getType() + ", " + arg1.getType());
        return arg0.getType().compareTo(arg1.getType());
      }
    }

    ModelComparator modelComparator = new ModelComparator(epsilon);
    if (arg0.getSubmodel() != null && arg1.getSubmodel() != null) {
      int status = modelComparator.compare(arg0.getSubmodel().getModel(), arg1.getSubmodel().getModel());
      if (status != 0) {
        logger.debug("Model different: " + arg0.getSubmodel() + ", " + arg1.getSubmodel());
        return status;
      }
    } else {
      if (arg0.getSubmodel() != null) {
        logger.debug("Model different: " + arg0.getSubmodel() + ", " + arg1.getSubmodel());
        return 1;
      }
      if (arg1.getSubmodel() != null) {
        logger.debug("Model different: " + arg0.getSubmodel() + ", " + arg1.getSubmodel());
        return -1;
      }
    }

    return 0;
  }

}
