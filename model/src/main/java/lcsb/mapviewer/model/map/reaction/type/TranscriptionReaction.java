package lcsb.mapviewer.model.map.reaction.type;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines a standard CellDesigner transcription reaction. It must
 * have at least one reactant and one product.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("TRANSCRIPTION_REACTION")
public class TranscriptionReaction extends Reaction implements SimpleReactionInterface {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  protected TranscriptionReaction() {
    super();
  }

  public TranscriptionReaction(final String elementId) {
    super(elementId);
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   *
   * @param result parent reaction from which we copy data
   */
  public TranscriptionReaction(final Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Transcription";
  }

  @Override
  public ReactionRect getReactionRect() {
    return ReactionRect.RECT_EMPTY;
  }

  @Override
  public TranscriptionReaction copy() {
    if (this.getClass() == TranscriptionReaction.class) {
      return new TranscriptionReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermReactionType.TRANSCRIPTION.getSBO();
  }
}
