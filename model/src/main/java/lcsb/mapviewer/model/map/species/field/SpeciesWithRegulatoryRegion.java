package lcsb.mapviewer.model.map.species.field;

public interface SpeciesWithRegulatoryRegion extends SpeciesWithModificationResidue {

  /**
   * Adds a {@link RegulatoryRegion} to the species.
   * 
   * @param regulatoryRegion
   *          {@link RegulatoryRegion} to add
   */
  void addRegulatoryRegion(final RegulatoryRegion regulatoryRegion);

}
