package lcsb.mapviewer.model.map.reaction.type;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines a standard CellDesigner unknown inhibition reaction.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("UNKNOWN_INHIBITION_REACTION")
public class UnknownInhibitionReaction extends Reaction implements SimpleReactionInterface, ModifierReactionNotation {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  protected UnknownInhibitionReaction() {
    super();
  }

  public UnknownInhibitionReaction(final String elementId) {
    super(elementId);
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   *
   * @param result parent reaction from which we copy data
   */
  public UnknownInhibitionReaction(final Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Unknown inhibition";
  }

  @Override
  public ReactionRect getReactionRect() {
    return null;
  }

  @Override
  public UnknownInhibitionReaction copy() {
    if (this.getClass() == UnknownInhibitionReaction.class) {
      return new UnknownInhibitionReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermReactionType.UNKNOWN_INHIBITION.getSBO();
  }
}
