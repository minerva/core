package lcsb.mapviewer.model.map.reaction.type;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines a standard CellDesigner negative influence reaction. It
 * must have at least one reactant and one product.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("NEGATIVE_INFLUENCE_REACTION")
public class NegativeInfluenceReaction extends Reaction implements SimpleReactionInterface, ReducedNotation {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  protected NegativeInfluenceReaction() {
    super();
  }

  public NegativeInfluenceReaction(final String elementId) {
    super(elementId);
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   *
   * @param result parent reaction from which we copy data
   */
  public NegativeInfluenceReaction(final Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Negative influence";
  }

  @Override
  public ReactionRect getReactionRect() {
    return null;
  }

  @Override
  public NegativeInfluenceReaction copy() {
    if (this.getClass() == NegativeInfluenceReaction.class) {
      return new NegativeInfluenceReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermReactionType.NEGATIVE_INFLUENCE.getSBO();
  }
}
