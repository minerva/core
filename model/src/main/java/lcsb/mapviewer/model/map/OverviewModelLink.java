package lcsb.mapviewer.model.map;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.modelutils.serializer.model.map.ModelAsIdSerializer;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

/**
 * Link used in {@link OverviewImage parent OverviewImage} to link it to
 * {@link ModelData (sub)model}.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("MODEL_LINK")
public class OverviewModelLink extends OverviewLink {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Model to which this links is going.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JsonSerialize(using = ModelAsIdSerializer.class)
  private ModelData linkedModel;

  /**
   * Zoom level at which model should be opened.
   */
  private Integer zoomLevel;

  /**
   * X coordinate on {@link #linkedModel} where this links point to.
   */
  // CHECKSTYLE.OFF
  private Integer xCoord;
  // CHECKSTYLE.ON

  /**
   * Y coordinate on {@link #linkedModel} where this links point to.
   */
  // CHECKSTYLE.OFF
  private Integer yCoord;
  // CHECKSTYLE.ON

  /**
   * Default constructor that creates copy of the parameter object.
   *
   * @param original original object that will be copied to this one
   */
  public OverviewModelLink(final OverviewModelLink original) {
    super(original);
    this.linkedModel = original.linkedModel;
    this.xCoord = original.xCoord;
    this.yCoord = original.yCoord;
    this.zoomLevel = original.zoomLevel;
  }

  /**
   * Default constructor.
   */
  public OverviewModelLink() {
    super();
  }

  /**
   * @return the model
   * @see #linkedModel
   */
  public ModelData getLinkedModel() {
    return linkedModel;
  }

  /**
   * @param model the model to set
   * @see #linkedModel
   */
  public void setLinkedModel(final Model model) {
    if (model != null) {
      this.linkedModel = model.getModelData();
    } else {
      this.linkedModel = null;
    }
  }

  public void setLinkedModel(final ModelData model) {
    this.linkedModel = model;
  }

  @Override
  public OverviewModelLink copy() {
    if (this.getClass() == OverviewModelLink.class) {
      return new OverviewModelLink(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * @return the zoomLevel
   * @see #zoomLevel
   */
  public Integer getZoomLevel() {
    return zoomLevel;
  }

  /**
   * @param zoomLevel the zoomLevel to set
   * @see #zoomLevel
   */
  public void setZoomLevel(final Integer zoomLevel) {
    this.zoomLevel = zoomLevel;
  }

  /**
   * @return the xCoord
   * @see #xCoord
   */
  public Integer getxCoord() {
    return xCoord;
  }

  /**
   * @param x the xCoord to set
   * @see #xCoord
   */
  public void setxCoord(final Integer x) {
    this.xCoord = x;
  }

  /**
   * Sets {@link #xCoord} value. Value will be trimmed to {@link Integer}.
   *
   * @param value the xCoord to set
   * @see #xCoord
   */
  public void setxCoord(final Double value) {
    if (value == null) {
      this.xCoord = null;
    } else {
      this.xCoord = value.intValue();
    }
  }

  /**
   * @return the yCoord
   * @see #yCoord
   */
  public Integer getyCoord() {
    return yCoord;
  }

  /**
   * @param y the yCoord to set
   * @see #yCoord
   */
  public void setyCoord(final Integer y) {
    this.yCoord = y;
  }

  /**
   * Sets {@link #yCoord} value. Value will be trimmed to {@link Integer}.
   *
   * @param value the yCoord to set
   * @see #yCoord
   */
  public void setyCoord(final Double value) {
    if (value == null) {
      this.yCoord = null;
    } else {
      this.yCoord = value.intValue();
    }
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
