package lcsb.mapviewer.model.map.sbo;

import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.IonChannelProtein;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.ReceptorProtein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.TruncatedProtein;
import lcsb.mapviewer.model.map.species.Unknown;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public enum SBOTermSpeciesType {
  ANTISENSE_RNA(AntisenseRna.class, new String[]{"SBO:0000334"}),
  COMPLEX(Complex.class, new String[]{"SBO:0000253", "SBO:0000297"}),
  HYPOTHETICAL_COMPLEX(Complex.class, new String[]{"SBO:0000289"}, true),
  DEGRADED(Degraded.class, new String[]{"SBO:0000291"}),
  DRUG(Drug.class, new String[]{"SBO:0000298"}),
  GENE(Gene.class, new String[]{"SBO:0000243"}),
  GENERIC_PROTEIN(GenericProtein.class, new String[]{"SBO:0000252"}),
  TRUNCATED_PROTEIN(TruncatedProtein.class, new String[]{"SBO:0000421"}),
  ION(Ion.class, new String[]{"SBO:0000327"}),
  ION_CHANNEL(IonChannelProtein.class, new String[]{"SBO:0000284"}),
  PHENOTYPE(Phenotype.class, new String[]{"SBO:0000358"}),
  RECEPTOR(ReceptorProtein.class, new String[]{"SBO:0000244"}),
  RNA(Rna.class, new String[]{"SBO:0000278"}),
  SIMPLE_MOLECULE(SimpleMolecule.class, new String[]{"SBO:0000247", "SBO:0000299"}),
  UNKNOWN(Unknown.class, new String[]{"SBO:0000285"}),
  ;

  private static final Logger logger = LogManager.getLogger();

  private final Class<? extends Species> clazz;
  private final List<String> sboTerms = new ArrayList<>();

  private boolean hypothetical = false;

  SBOTermSpeciesType(final Class<? extends Species> clazz, final String[] inputSboTerms) {
    this.clazz = clazz;
    for (final String string : inputSboTerms) {
      getSboTerms().add(string);
    }
  }

  SBOTermSpeciesType(final Class<? extends Species> clazz, final String[] inputSboTerms, final boolean hypothetical) {
    this.clazz = clazz;
    for (final String string : inputSboTerms) {
      getSboTerms().add(string);
    }
    this.hypothetical = hypothetical;
  }

  public static SBOTermSpeciesType getTypeSBOTerm(final String sboTerm, final Marker logMarker) {
    if (sboTerm == null || sboTerm.isEmpty()) {
      return SIMPLE_MOLECULE;
    }
    SBOTermSpeciesType result = null;
    for (final SBOTermSpeciesType term : values()) {
      for (final String string : term.getSboTerms()) {
        if (string.equalsIgnoreCase(sboTerm)) {
          result = term;
          break;
        }
      }
    }
    if (result == null) {
      logger.warn(logMarker, "Don't know how to handle SBOTerm " + sboTerm);
      result = SIMPLE_MOLECULE;
    }
    return result;
  }

  public static Species createElementForSBOTerm(final String sboTerm, final String id, final Marker logMarker) {
    try {
      final SBOTermSpeciesType type = getTypeSBOTerm(sboTerm, logMarker);
      final Class<? extends Species> clazz = type.clazz;
      final Species result = clazz.getConstructor(String.class).newInstance(id);
      if (type.hypothetical) {
        result.setHypothetical(true);
      }
      return result;
    } catch (final SecurityException | NoSuchMethodException | InstantiationException | IllegalAccessException
                   | IllegalArgumentException | InvocationTargetException e) {
      throw new InvalidStateException(e);
    }
  }

  public static String getTermByType(final Element element) {
    String result = null;

    for (final SBOTermSpeciesType term : values()) {
      if (element.getClass().equals(term.clazz)) {
        if (element instanceof Species) {
          if (((Species) element).isHypothetical() == term.hypothetical) {
            return term.getSBO();
          } else {
            result = term.getSBO();
          }
        } else {
          result = term.getSBO();
        }
      }
    }
    if (result != null) {
      return result;
    }
    logger.warn("Cannot find SBO term for class: " + element.getClass());
    return null;
  }

  public String getSBO() {
    if (!getSboTerms().isEmpty()) {
      return getSboTerms().get(0);
    }
    logger.warn("Cannot find SBO term for class: " + clazz);
    return null;
  }

  public List<String> getSboTerms() {
    return sboTerms;
  }
}
