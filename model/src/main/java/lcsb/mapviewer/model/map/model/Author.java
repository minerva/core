package lcsb.mapviewer.model.map.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Class representing author object.
 *
 * @author Piotr Gawron
 */
@Entity
@XmlRootElement
public class Author implements Serializable, MinervaEntity {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static final Logger logger = LogManager.getLogger();
  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * First name.
   */
  @Column(length = 255)
  private String firstName;

  /**
   * Last name.
   */
  @Column(length = 255)
  private String lastName;

  /**
   * Email address.
   */
  @Column(length = 255)
  private String email;

  /**
   * Organization.
   */
  @Column(length = 255)
  private String organisation;

  @Version
  @JsonIgnore
  private long entityVersion;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinTable(name = "model_data_table_authors",
      joinColumns = {
          @JoinColumn(name = "author_id", referencedColumnName = "id", nullable = true, updatable = false, insertable = false)
      },
      inverseJoinColumns = {
          @JoinColumn(name = "model_data_id", referencedColumnName = "id", nullable = false, updatable = false, insertable = false)
      })
  @JsonIgnore
  private ModelData modelData;

  /**
   * Default constructor
   *
   * @param firstName first name
   * @param lastName  last name
   */
  public Author(final String firstName, final String lastName) {
    setFirstName(firstName);
    setLastName(lastName);
  }

  /**
   * Empty constructor for hibernate.
   */
  protected Author() {

  }

  /**
   * Constructor that creates a copy of the author.
   *
   * @param author original object from which we copy
   */
  public Author(final Author author) {
    setFirstName(author.getFirstName());
    setLastName(author.getLastName());
    setEmail(author.getEmail());
    setOrganisation(author.getOrganisation());
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(final String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public String getOrganisation() {
    return organisation;
  }

  public void setOrganisation(final String organisation) {
    this.organisation = organisation;
  }

  public Author copy() {
    return new Author(this);
  }

  @Override
  public String toString() {
    return "[" + this.getClass() + "] " + firstName + " " + lastName + "; " + email + "; " + organisation;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public boolean equals(final Object that) {
    if (that == null) {
      return false;
    }
    if (!this.getClass().equals(that.getClass())) {
      return false;
    }
    return this.toString().equals(that.toString());
  }

  @Override
  public int hashCode() {
    return this.toString().hashCode();
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
