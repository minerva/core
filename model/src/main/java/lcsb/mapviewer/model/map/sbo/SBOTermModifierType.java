package lcsb.mapviewer.model.map.sbo;

import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.modifier.Inhibition;
import lcsb.mapviewer.model.map.modifier.Modulation;
import lcsb.mapviewer.model.map.modifier.PhysicalStimulation;
import lcsb.mapviewer.model.map.modifier.Trigger;
import lcsb.mapviewer.model.map.modifier.UnknownCatalysis;
import lcsb.mapviewer.model.map.modifier.UnknownInhibition;
import lcsb.mapviewer.model.map.reaction.Modifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;

import java.util.ArrayList;
import java.util.List;

public enum SBOTermModifierType {
  CATALYSIS(Catalysis.class, new String[]{"SBO:0000013"}),
  INHIBITION(Inhibition.class, new String[]{"SBO:0000537"}),
  MODULATION(Modulation.class, new String[]{"SBO:0000594"}),
  PHYSICAL_STIMULATION(PhysicalStimulation.class, new String[]{"SBO:0000459"}),
  TRIGGER(Trigger.class, new String[]{"SBO:0000461"}),
  UNKNOWN_CATALYSIS(UnknownCatalysis.class, new String[]{"SBO:0000462"}),
  UNKNOWN_INHIBITION(UnknownInhibition.class, new String[]{"SBO:0000536"}),
  ;

  private static Logger logger = LogManager.getLogger();

  private final Class<? extends Modifier> clazz;
  private final List<String> sboTerms = new ArrayList<>();

  private SBOTermModifierType(final Class<? extends Modifier> clazz, final String[] inputSboTerms) {
    this.clazz = clazz;
    for (final String string : inputSboTerms) {
      sboTerms.add(string);
    }
  }

  public static Class<? extends Modifier> getTypeSBOTerm(final String sboTerm, final Marker logMarker) {
    if (sboTerm == null || sboTerm.isEmpty()) {
      return Modifier.class;
    }
    Class<? extends Modifier> result = getClazzFromSboTerm(sboTerm);

    if (result == null) {
      logger.warn(logMarker, "Don't know how to handle SBOTerm " + sboTerm + " for modifier");
      result = Modifier.class;
    }
    return result;
  }

  public static Class<? extends Modifier> getClazzFromSboTerm(final String sboTerm) {
    Class<? extends Modifier> result = null;
    for (final SBOTermModifierType term : values()) {
      for (final String string : term.sboTerms) {
        if (string.equalsIgnoreCase(sboTerm)) {
          result = term.clazz;
        }
      }
    }
    return result;
  }

  public static String getTermByType(final Class<? extends Modifier> clazz) {
    for (final SBOTermModifierType term : values()) {
      if (clazz.equals(term.clazz)) {
        return term.getSBO();
      }
    }
    logger.warn("Cannot find SBO term for class: " + clazz);
    return null;
  }

  public String getSBO() {
    if (sboTerms.size() != 0) {
      return sboTerms.get(0);
    }
    logger.warn("Cannot find SBO term for class: " + clazz);
    return null;
  }
}
