package lcsb.mapviewer.model.map.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * This abstract class defines connection between models. It points to submodel
 * and describe type of the connection.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "submodel_connections_type_db", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("SUBMODEL_CONNECTION")
public abstract class SubmodelConnection implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  /**
   * Database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Sub model.
   */
  @Cascade({ CascadeType.SAVE_UPDATE})
  @ManyToOne(fetch = FetchType.LAZY)
  private ModelData submodel;

  /**
   * Name of the connection.
   */
  @Column(length = 255)
  private String name;

  /**
   * Type of the connection.
   */
  @Enumerated(EnumType.STRING)
  private SubmodelType type;

  /**
   * Default constructor.
   */
  public SubmodelConnection() {
  }

  /**
   * Default constructor that initialize some fields.
   * 
   * @param submodel
   *          {@link SubmodelConnection#submodel}
   * @param type
   *          {@link SubmodelConnection#type}
   */
  public SubmodelConnection(final ModelData submodel, final SubmodelType type) {
    setSubmodel(submodel);
    setType(type);
  }

  /**
   * Constructor that creates copy of the {@link SubmodelConnection} object.
   * 
   * @param original
   *          original object from which copy is prepared
   */
  public SubmodelConnection(final SubmodelConnection original) {
    setSubmodel(original.getSubmodel());
    setName(original.getName());
    setType(original.getType());
  }

  /**
   * Default constructor that initialize some fields.
   * 
   * @param submodel
   *          {@link SubmodelConnection#submodel}
   * @param type
   *          {@link SubmodelConnection#type}
   * @param name
   *          {@link SubmodelConnection#name}
   */
  public SubmodelConnection(final Model submodel, final SubmodelType type, final String name) {
    setSubmodel(submodel);
    setName(name);
    setType(type);
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name
   *          the name to set
   * @see #name
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * @return the type
   * @see #type
   */
  public SubmodelType getType() {
    return type;
  }

  /**
   * @param type
   *          the type to set
   * @see #type
   */
  public void setType(final SubmodelType type) {
    this.type = type;
  }

  /**
   * @return the submodel
   * @see #submodel
   */
  public ModelData getSubmodel() {
    return submodel;
  }

  /**
   * @param submodel
   *          the submodel to set
   * @see #submodel
   */
  public void setSubmodel(final ModelData submodel) {
    // sometimes we change submodel in the runtime (for instance when we do
    // create a copy), then we should clean information about connection from
    // both sides
    if (this.submodel != null) {
      this.submodel.getParentModels().remove(this);
    }
    this.submodel = submodel;
    submodel.getParentModels().add(this);
  }

  /**
   * Sets submodel to which this connection points to.
   *
   * @param submodel
   *          submodel to set.
   * @see #submodel
   */
  @JsonIgnore
  public void setSubmodel(final Model submodel) {
    setSubmodel(submodel.getModelData());
  }

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(final int id) {
    this.id = id;
  }

  /**
   * Creates a copy of this object.
   * 
   * @return copy of the object
   */
  public abstract SubmodelConnection copy();

  /**
   * This method assign values from the original model. It's different than
   * default constructor - doesn't interfere with the models.
   * 
   * @param original
   *          original connection from which data is assigned
   */
  protected void assignValuesFromOriginal(final SubmodelConnection original) {
    this.submodel = original.getSubmodel();
    this.name = original.getName();
    this.type = original.getType();
  }
}
