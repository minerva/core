package lcsb.mapviewer.model.map.reaction;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * Class representing boolean, but unknown operator between two or more nodes in
 * the reaction.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("UNKNOWN_OPERATOR_NODE")
public class UnknownOperator extends NodeOperator {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param operator
   *          parent operator from which we copy data
   */
  public UnknownOperator(final UnknownOperator operator) {
    super(operator);
  }

  /**
   * Default constructor.
   */
  public UnknownOperator() {
    super();
  }

  @Override
  public String getOperatorText() {
    return "?";
  }

  @Override
  public UnknownOperator copy() {
    if (this.getClass() == UnknownOperator.class) {
      return new UnknownOperator(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
