package lcsb.mapviewer.model.map.reaction;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Represents reactant of the reaction.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("REACTANT_NODE")
public class Reactant extends ReactionNode {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  protected Reactant() {
    super();
  }

  /**
   * Constructor that creates reactant for a given {@link Species}. These
   * elements reference to the objects in the
   * {@link lcsb.mapviewer.model.map.model.db.model.map.Model Model} that
   * represents this reactant.
   * 
   * @param element
   *          element that represent this reactant
   */
  public Reactant(final Element element) {
    super(element);
    if (element == null) {
      throw new InvalidArgumentException("Element cannot be null");
    }
  }

  /**
   * Constructor that initialize the reactant with data from parameter.
   * 
   * @param original
   *          original reactant used for data initalization
   */
  public Reactant(final Reactant original) {
    super(original);
  }

  @Override
  public Reactant copy() {
    if (this.getClass() == Reactant.class) {
      return new Reactant(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }
}
