package lcsb.mapviewer.model.map.kinetics;

public enum SbmlUnitType {
  AMPERE,
  AVOGADRO,
  BECQUEREL,
  CANDELA,
  COULUMB,
  DIMENSIONLESS,
  FARAD,
  GRAM,
  GRAY,
  HENRY,
  HERTZ,
  ITEM,
  JOULE,
  KATAL,
  KELVIN,
  KILOGRAM,
  LITRE,
  LUMEN,
  LUX,
  METRE,
  MOLE,
  NEWTON,
  OHM,
  PASCAL,
  RADIAN,
  SECOND,
  SIEMENS,
  SIEVERT,
  STERADIAN,
  TESLA,
  VOLT,
  WATT,
  WEBER,;

  public String getCommonName() {
    return this.name().toLowerCase();
  }
}
