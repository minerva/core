package lcsb.mapviewer.model.map.compartment;

import lcsb.mapviewer.common.exception.NotImplementedException;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines compartment with oval shape.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("OVAL_COMPARTMENT")
public class OvalCompartment extends Compartment {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Empty constructor required by hibernate.
   */
  OvalCompartment() {
    super();
  }

  /**
   * Constructor that creates a compartment with the new shape and takes the
   * reference data from the compartment given as parameter.
   *
   * @param original original compartment where the data was kept
   */
  public OvalCompartment(final Compartment original) {
    super(original);
  }

  /**
   * Default constructor.
   *
   * @param elementId identifier of the compartment
   */
  public OvalCompartment(final String elementId) {
    super();
    setElementId(elementId);
  }

  @Override
  public OvalCompartment copy() {
    if (this.getClass() == OvalCompartment.class) {
      return new OvalCompartment(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getShape() {
    return "OVAL_COMPARTMENT";
  }
}
