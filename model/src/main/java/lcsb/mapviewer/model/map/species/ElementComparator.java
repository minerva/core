package lcsb.mapviewer.model.map.species;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.ColorComparator;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.comparator.EnumComparator;
import lcsb.mapviewer.common.comparator.IntegerComparator;
import lcsb.mapviewer.common.comparator.ListComparator;
import lcsb.mapviewer.common.comparator.SetComparator;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamDataComparator;
import lcsb.mapviewer.model.map.compartment.CompartmentComparator;
import lcsb.mapviewer.model.map.model.ElementSubmodelConnectionComparator;

/**
 * Comparator class used for comparing {@link Element} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class ElementComparator extends Comparator<Element> {
  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public ElementComparator(final double epsilon) {
    super(Element.class, true);
    this.epsilon = epsilon;
    addSubClassComparator(new ComplexComparator(epsilon));
    addSubClassComparator(new CompartmentComparator(epsilon));
    addSubClassComparator(new SpeciesComparator(epsilon));
  }

  /**
   * Default constructor.
   */
  public ElementComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(final Element arg0, final Element arg1) {
    if (arg0 == null) {
      if (arg1 == null) {
        return 0;
      } else {
        return 1;
      }
    } else if (arg1 == null) {
      return -1;
    }

    StringComparator stringComparator = new StringComparator();
    ColorComparator colorComparator = new ColorComparator();
    DoubleComparator doubleComparator = new DoubleComparator(epsilon);

    if (stringComparator.compare(arg0.getElementId(), arg1.getElementId()) != 0) {
      logger.debug("ElementId different: " + arg0.getElementId() + ", " + arg1.getElementId());
      return stringComparator.compare(arg0.getElementId(), arg1.getElementId());
    }

    if (doubleComparator.compare(arg0.getX(), arg1.getX()) != 0) {
      logger.debug("X different: " + arg0.getX() + ", " + arg1.getX());
      return doubleComparator.compare(arg0.getX(), arg1.getX());
    }

    if (doubleComparator.compare(arg0.getY(), arg1.getY()) != 0) {
      logger.debug("Y different: " + arg0.getY() + ", " + arg1.getY());
      return doubleComparator.compare(arg0.getY(), arg1.getY());
    }

    IntegerComparator integerComparator = new IntegerComparator();
    if (integerComparator.compare(arg0.getZ(), arg1.getZ()) != 0) {
      logger.debug("Z-index different: " + arg0.getZ() + ", " + arg1.getZ());
      return integerComparator.compare(arg0.getZ(), arg1.getZ());
    }

    if (doubleComparator.compare(arg0.getWidth(), arg1.getWidth()) != 0) {
      logger.debug("Width different: " + arg0.getWidth() + ", " + arg1.getWidth());
      return doubleComparator.compare(arg0.getWidth(), arg1.getWidth());
    }

    if (doubleComparator.compare(arg0.getHeight(), arg1.getHeight()) != 0) {
      logger.debug("Height different: " + arg0.getHeight() + ", " + arg1.getHeight());
      return doubleComparator.compare(arg0.getHeight(), arg1.getHeight());
    }

    if (doubleComparator.compare(arg0.getFontSize(), arg1.getFontSize()) != 0) {
      logger.debug("Font size different: " + arg0.getFontSize() + ", " + arg1.getFontSize());
      return doubleComparator.compare(arg0.getFontSize(), arg1.getFontSize());
    }

    if (stringComparator.compare(arg0.getVisibilityLevel(), arg1.getVisibilityLevel()) != 0) {
      logger.debug("Visibility level different: " + arg0.getVisibilityLevel() + ", " + arg1.getVisibilityLevel());
      return stringComparator.compare(arg0.getVisibilityLevel(), arg1.getVisibilityLevel());
    }

    if (colorComparator.compare(arg0.getFillColor(), arg1.getFillColor()) != 0) {
      logger.debug("Fill color different: " + arg0.getFillColor() + ", " + arg1.getFillColor());
      return colorComparator.compare(arg0.getFillColor(), arg1.getFillColor());
    }

    if (colorComparator.compare(arg0.getBorderColor(), arg1.getBorderColor()) != 0) {
      logger.debug("Border color different: " + arg0.getBorderColor() + ", " + arg1.getBorderColor());
      return colorComparator.compare(arg0.getBorderColor(), arg1.getBorderColor());
    }

    if (colorComparator.compare(arg0.getFontColor(), arg1.getFontColor()) != 0) {
      logger.debug("Font color different: " + arg0.getFontColor() + ", " + arg1.getFontColor());
      return colorComparator.compare(arg0.getFontColor(), arg1.getFontColor());
    }

    // this should be somehow modified, because it can create a situation where
    // comparison will fall into infinite loop (in cyclic submodels)
    ElementSubmodelConnectionComparator ascc = new ElementSubmodelConnectionComparator(epsilon);
    int status = ascc.compare(arg0.getSubmodel(), arg1.getSubmodel());
    if (status != 0) {
      logger.debug("Element submodel different: " + arg0.getSubmodel() + ", " + arg1.getSubmodel());
      return status;
    }

    if (stringComparator.compare(arg0.getName(), arg1.getName()) != 0) {
      logger.debug("Name different: " + arg0.getName() + ", " + arg1.getName());
      return stringComparator.compare(arg0.getName(), arg1.getName());
    }

    if (stringComparator.compare(arg0.getNotes(), arg1.getNotes(), true) != 0) {
      logger.debug("notes different: \n\"" + arg0.getNotes() + "\"\n\"" + arg1.getNotes() + "\"");
      return stringComparator.compare(arg0.getNotes(), arg1.getNotes());
    }

    if (stringComparator.compare(arg0.getSymbol(), arg1.getSymbol()) != 0) {
      logger.debug("symbol different: \"" + arg0.getSymbol() + "\", \"" + arg1.getSymbol() + "\"");
      return stringComparator.compare(arg0.getSymbol(), arg1.getSymbol());
    }

    if (stringComparator.compare(arg0.getFullName(), arg1.getFullName()) != 0) {
      logger.debug("full name different: \"" + arg0.getFullName() + "\", \"" + arg1.getFullName() + "\"");
      return stringComparator.compare(arg0.getFullName(), arg1.getFullName());
    }

    if (stringComparator.compare(arg0.getAbbreviation(), arg1.getAbbreviation()) != 0) {
      logger.debug("Abbreviation different: \"" + arg0.getAbbreviation() + "\", \"" + arg1.getAbbreviation() + "\"");
      return stringComparator.compare(arg0.getAbbreviation(), arg1.getAbbreviation());
    }

    if (stringComparator.compare(arg0.getFormula(), arg1.getFormula()) != 0) {
      logger.debug("formula different: \"" + arg0.getFormula() + "\", \"" + arg1.getFormula() + "\"");
      return stringComparator.compare(arg0.getFormula(), arg1.getFormula());
    }

    ListComparator<String> stringListComparator = new ListComparator<>(stringComparator);

    if (stringListComparator.compare(arg0.getSynonyms(), arg1.getSynonyms()) != 0) {
      logger.debug("List of synonyms different");
      return stringListComparator.compare(arg0.getSynonyms(), arg1.getSynonyms());
    }

    if (stringListComparator.compare(arg0.getFormerSymbols(), arg1.getFormerSymbols()) != 0) {
      logger.debug("List of former symbols different");
      return stringListComparator.compare(arg0.getFormerSymbols(), arg1.getFormerSymbols());
    }

    SetComparator<MiriamData> miriamDataSetComparator = new SetComparator<>(new MiriamDataComparator());

    status = miriamDataSetComparator.compare(arg0.getMiriamData(), arg1.getMiriamData());
    if (status != 0) {
      logger.debug("miriam data different");
      logger.debug(arg0.getMiriamData());
      logger.debug(arg1.getMiriamData());
      return status;
    }

    if (doubleComparator.compare(arg0.getNameX(), arg1.getNameX()) != 0) {
      logger.debug("NameX different: " + arg0.getNameX() + ", " + arg1.getNameX());
      return doubleComparator.compare(arg0.getNameX(), arg1.getNameX());
    }

    if (doubleComparator.compare(arg0.getNameY(), arg1.getNameY()) != 0) {
      logger.debug("NameY different: " + arg0.getNameY() + ", " + arg1.getNameY());
      return doubleComparator.compare(arg0.getNameY(), arg1.getNameY());
    }

    if (doubleComparator.compare(arg0.getNameWidth(), arg1.getNameWidth()) != 0) {
      logger.debug("NameWidth different: " + arg0.getNameWidth() + ", " + arg1.getNameWidth());
      return doubleComparator.compare(arg0.getNameWidth(), arg1.getNameWidth());
    }

    if (doubleComparator.compare(arg0.getNameHeight(), arg1.getNameHeight()) != 0) {
      logger.debug("NameHeight different: " + arg0.getNameHeight() + ", " + arg1.getNameHeight());
      return doubleComparator.compare(arg0.getNameHeight(), arg1.getNameHeight());
    }

    EnumComparator<VerticalAlign> verticalAlignComparator = new EnumComparator<>();
    if (verticalAlignComparator.compare(arg0.getNameVerticalAlign(), arg1.getNameVerticalAlign()) != 0) {
      logger.debug("Name vertical alignment different: " + arg0.getNameVerticalAlign() + ", "
          + arg1.getNameVerticalAlign());
      return verticalAlignComparator.compare(arg0.getNameVerticalAlign(), arg1.getNameVerticalAlign());
    }

    EnumComparator<HorizontalAlign> horizontalAlignComparator = new EnumComparator<>();
    if (horizontalAlignComparator.compare(arg0.getNameHorizontalAlign(), arg1.getNameHorizontalAlign()) != 0) {
      logger.debug("Name horizontal alignment different: " + arg0.getNameHorizontalAlign() + ", "
          + arg1.getNameHorizontalAlign());
      return horizontalAlignComparator.compare(arg0.getNameHorizontalAlign(), arg1.getNameHorizontalAlign());
    }

    return 0;
  }

}
