package lcsb.mapviewer.model.map.species.field;

public interface SpeciesWithTranscriptionSite extends SpeciesWithModificationResidue {

  /**
   * Adds a {@link TranscriptionSite} to the species.
   * 
   * @param transcriptionSite
   *          {@link TranscriptionSite} to add
   */
  void addTranscriptionSite(final TranscriptionSite transcriptionSite);

}
