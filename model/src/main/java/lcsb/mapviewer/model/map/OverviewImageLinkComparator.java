package lcsb.mapviewer.model.map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.IntegerComparator;

/**
 * This class implements comparator interface for {@link OverviewImageLink}.
 * 
 * @author Piotr Gawron
 * 
 */
public class OverviewImageLinkComparator extends Comparator<OverviewImageLink> {
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public OverviewImageLinkComparator(final double epsilon) {
    super(OverviewImageLink.class);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public OverviewImageLinkComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected Comparator<?> getParentComparator() {
    return new OverviewLinkComparator(epsilon);
  }

  @Override
  protected int internalCompare(final OverviewImageLink arg0, final OverviewImageLink arg1) {
    int result = 0;
    IntegerComparator integerComparator = new IntegerComparator();

    if (arg0.getLinkedOverviewImage() == null) {
      if (arg1.getLinkedOverviewImage() == null) {
        result = 0;
      } else {
        result = 1;
      }
    } else if (arg1.getLinkedOverviewImage() == null) {
      result = -1;
    } else {
      result = integerComparator.compare(arg0.getLinkedOverviewImage().getId(), arg1.getLinkedOverviewImage().getId());
    }

    if (result != 0) {
      return result;
    }

    return 0;

  }

}
