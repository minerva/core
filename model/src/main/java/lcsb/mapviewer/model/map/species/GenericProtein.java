package lcsb.mapviewer.model.map.species;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.sbo.SBOTermSpeciesType;

/**
 * Entity representing generic protein element on the map.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@DiscriminatorValue("GENERIC_PROTEIN")
public class GenericProtein extends Protein {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Empty constructor required by hibernate.
   */
  GenericProtein() {
  }

  /**
   * Default constructor.
   * 
   * @param elementId
   *          unique (within model) element identifier
   */
  public GenericProtein(final String elementId) {
    super(elementId);
  }

  /**
   * Constructor that creates a copy of the element given in the parameter.
   * 
   * @param original
   *          original object that will be used for creating copy
   */
  public GenericProtein(final GenericProtein original) {
    super(original);
  }

  @Override
  public GenericProtein copy() {
    if (this.getClass() == GenericProtein.class) {
      return new GenericProtein(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermSpeciesType.GENERIC_PROTEIN.getSBO();
  }

}
