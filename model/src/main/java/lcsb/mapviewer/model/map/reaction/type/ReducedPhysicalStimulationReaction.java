package lcsb.mapviewer.model.map.reaction.type;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines a standard CellDesigner reduced physical stimulation
 * reaction. It must have at least one reactant and one product.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("REDUCED_PHYSICAL_STIMULATION")
public class ReducedPhysicalStimulationReaction extends Reaction implements SimpleReactionInterface, ReducedNotation {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  protected ReducedPhysicalStimulationReaction() {
    super();
  }

  public ReducedPhysicalStimulationReaction(final String elementId) {
    super(elementId);
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   *
   * @param result parent reaction from which we copy data
   */
  public ReducedPhysicalStimulationReaction(final Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Reduced physical stimulation";
  }

  @Override
  public ReactionRect getReactionRect() {
    return null;
  }

  @Override
  public ReducedPhysicalStimulationReaction copy() {
    if (this.getClass() == ReducedPhysicalStimulationReaction.class) {
      return new ReducedPhysicalStimulationReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermReactionType.REDUCED_PHYSICAL_STIMULATION.getSBO();
  }
}
