package lcsb.mapviewer.model.map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.modelutils.serializer.model.map.ModelAsIdSerializer;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import java.awt.geom.Point2D;

/**
 * Class representing comments on the map.
 *
 * @author Piotr Gawron
 */
@Entity
public class Comment implements MinervaEntity {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Is the feedback removed.
   */
  private boolean deleted = false;

  /**
   * What was the reason of removal.
   */
  @Column(length = 255)
  @JsonIgnore
  private String removeReason = "";

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * The model that was commented.
   */
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JsonSerialize(using = ModelAsIdSerializer.class)
  @JsonProperty(value = "map")
  private ModelData model;

  /**
   * User who gave the feedback (if logged in).
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JsonIgnore
  private User user;

  /**
   * Email of the user who gave feedback.
   */
  @Column(length = 255)
  @JsonIgnore
  private String email;

  /**
   * Content of the feedback.
   */
  @Column(columnDefinition = "TEXT", nullable = false)
  private String content;

  /**
   * Where on map the feedback is located.
   */
  @Type(type = "lcsb.mapviewer.persist.mapper.Point2DMapper")
  private Point2D coordinates;

  @ManyToOne(optional = true)
  @JsonIgnore
  private Reaction reaction;

  @ManyToOne(optional = true)
  @JsonIgnore
  private Element element;

  /**
   * Determines if comment should be visible on the map.
   */
  @JsonProperty(value = "visible")
  private boolean pinned = false;

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * @return the coordinates
   * @see #coordinates
   */
  public Point2D getCoordinates() {
    return coordinates;
  }

  /**
   * @param coordinates the coordinates to set
   * @see #coordinates
   */
  public void setCoordinates(final Point2D coordinates) {
    this.coordinates = coordinates;
  }

  /**
   * @return the content
   * @see #content
   */
  public String getContent() {
    return content;
  }

  /**
   * @param content the content to set
   * @see #content
   */
  public void setContent(final String content) {
    this.content = content;
  }

  /**
   * @return the email
   * @see #email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   * @see #email
   */
  public void setEmail(final String email) {
    this.email = email;
  }

  /**
   * @return the user
   * @see #user
   */
  public User getUser() {
    return user;
  }

  /**
   * @param user the user to set
   * @see #user
   */
  public void setUser(final User user) {
    this.user = user;
  }

  /**
   * @return the model
   * @see #model
   */
  @JsonIgnore
  public ModelData getModelData() {
    return model;
  }

  /**
   * @param model the model to set
   * @see #model
   */
  public void setModelData(final ModelData model) {
    this.model = model;
  }

  /**
   * @return the id
   * @see #id
   */
  @Override
  public int getId() {
    return id;
  }

  /**
   * @param id the id to set
   * @see #id
   */
  public void setId(final int id) {
    this.id = id;
  }

  /**
   * @return the removeReason
   * @see #removeReason
   */
  public String getRemoveReason() {
    return removeReason;
  }

  /**
   * @param removeReason the removeReason to set
   * @see #removeReason
   */
  public void setRemoveReason(final String removeReason) {
    this.removeReason = removeReason;
  }

  /**
   * @return the deleted
   * @see #deleted
   */
  public boolean isDeleted() {
    return deleted;
  }

  /**
   * @param deleted the deleted to set
   * @see #deleted
   */
  public void setDeleted(final boolean deleted) {
    this.deleted = deleted;
  }

  /**
   * @return the pinned
   * @see #pinned
   */
  public boolean isPinned() {
    return pinned;
  }

  /**
   * @param pinned the pinned to set
   * @see #pinned
   */
  public void setPinned(final boolean pinned) {
    this.pinned = pinned;
  }

  /**
   * Sets model.
   *
   * @param model2 model to set
   */
  public void setModel(final Model model2) {
    this.model = model2.getModelData();
  }

  public void setModel(final ModelData model2) {
    this.model = model2;
  }

  public Reaction getReaction() {
    return reaction;
  }

  public void setReaction(final Reaction reaction) {
    this.reaction = reaction;
  }

  public Element getElement() {
    return element;
  }

  public void setElement(final Element element) {
    this.element = element;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
