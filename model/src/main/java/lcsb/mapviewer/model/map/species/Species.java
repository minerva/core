package lcsb.mapviewer.model.map.species;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.species.field.PositionToCompartment;
import lcsb.mapviewer.model.map.species.field.UniprotRecord;
import lcsb.mapviewer.modelutils.serializer.model.map.ElementAsIdSerializer;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

/**
 * Structure used for representing information about single element.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("SPECIES")
public abstract class Species extends Element {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Is the element active.
   */
  private Boolean activity;

  /**
   * Width of the element border.
   */
  @Column(nullable = false)
  private Double lineWidth;

  /**
   * State of the element visualization (should the inside be presented in details
   * or not). TODO this should be transformed into enum
   */
  @Column(length = 255)
  @JsonIgnore
  private String state;

  /**
   * State (free text describing element in some way) of the element is split into
   * {@link #statePrefix prefix} and {@link #stateLabel label}. This value
   * represent the first part of the state.
   */
  @Column(length = 255)
  @JsonIgnore
  private String statePrefix;

  /**
   * State (free text describing element in some way) of the element is split into
   * {@link #statePrefix prefix} and {@link #stateLabel label}. This value
   * represent the second part of the state.
   */
  @Column(length = 255)
  @JsonIgnore
  private String stateLabel;

  /**
   * Complex to which this element belongs to. Null if such complex doesn't exist.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @Cascade({CascadeType.ALL})
  @JoinColumn(name = "idcomplexdb")
  @JsonSerialize(using = ElementAsIdSerializer.class)
  private Complex complex;

  /**
   * Initial amount of species.
   */
  private Double initialAmount = null;

  /**
   * Charge of the species.
   */
  private Integer charge = null;

  /**
   * Initial concentration of species.
   */
  private Double initialConcentration = 0.0;

  /**
   * Is only substance units allowed.
   */
  private Boolean onlySubstanceUnits = false;

  /**
   * How many dimers are in this species.
   */
  private int homodimer = 1;

  /**
   * Position on the compartment.
   */
  @Enumerated(EnumType.STRING)
  @JsonIgnore
  private PositionToCompartment positionToCompartment = null;

  /**
   * Is species hypothetical.
   */
  private Boolean hypothetical = null;

  /**
   * List of uniprot records which are associated with this species.
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "species", orphanRemoval = true)
  @Fetch(FetchMode.SUBSELECT)
  @JsonIgnore
  private Set<UniprotRecord> uniprots = new HashSet<>();

  private Boolean boundaryCondition = false;

  private Boolean constant = false;

  @ManyToOne
  @Cascade({CascadeType.SAVE_UPDATE})
  private SbmlUnit substanceUnits;

  /**
   * Constructor that set element identifier.
   *
   * @param elementId {@link Element#elementId}
   */
  public Species(final String elementId) {
    this();
    setElementId(elementId);
    setName(elementId);
  }

  /**
   * Empty constructor required by hibernate.
   */
  Species() {
    super();
    activity = false;
    lineWidth = 1.0;
    state = "usual";
  }

  /**
   * Constructor that initializes data with the information given in the
   * parameter.
   *
   * @param original object from which data will be initialized
   */
  protected Species(final Species original) {
    super(original);
    activity = original.getActivity();
    lineWidth = original.getLineWidth();
    state = original.getState();

    complex = original.getComplex();
    stateLabel = original.getStateLabel();
    statePrefix = original.getStatePrefix();

    initialAmount = original.getInitialAmount();
    charge = original.getCharge();
    initialConcentration = original.getInitialConcentration();
    onlySubstanceUnits = original.getOnlySubstanceUnits();
    homodimer = original.getHomodimer();
    positionToCompartment = original.getPositionToCompartment();
    hypothetical = original.getHypothetical();
    boundaryCondition = original.getBoundaryCondition();
    constant = original.getConstant();
    substanceUnits = original.substanceUnits;

    uniprots = original.getUniprots();

    // don't copy reaction nodes
  }

  /**
   * @return the activity
   * @see #activity
   */
  public Boolean getActivity() {
    return activity;
  }

  /**
   * @param activity the activity to set
   * @see #activity
   */
  public void setActivity(final Boolean activity) {
    this.activity = activity;
  }

  /**
   * @return the lineWidth
   * @see #lineWidth
   */
  public Double getLineWidth() {
    return lineWidth;
  }

  /**
   * @param lineWidth the lineWidth to set
   * @see #lineWidth
   */
  public void setLineWidth(final Double lineWidth) {
    this.lineWidth = lineWidth;
  }

  /**
   * @return the state
   * @see #state
   */
  public String getState() {
    return state;
  }

  /**
   * @param state the state to set
   * @see #state
   */
  public void setState(final String state) {
    this.state = state;
  }

  /**
   * @return the complex
   * @see #complex
   */
  public Complex getComplex() {
    return complex;
  }

  /**
   * @param complex the complex to set
   * @see #complex
   */
  public void setComplex(final Complex complex) {
    this.complex = complex;
  }

  /**
   * @return the statePrefix
   * @see #statePrefix
   */
  public String getStatePrefix() {
    return statePrefix;
  }

  /**
   * @param statePrefix the statePrefix to set
   * @see #statePrefix
   */
  public void setStatePrefix(final String statePrefix) {
    this.statePrefix = statePrefix;
  }

  /**
   * @return the stateLabel
   * @see #stateLabel
   */
  public String getStateLabel() {
    return stateLabel;
  }

  /**
   * @param stateLabel the stateLabel to set
   * @see #stateLabel
   */
  public void setStateLabel(final String stateLabel) {
    this.stateLabel = stateLabel;
  }

  @Override
  public abstract Species copy();

  /**
   * @return the initialAmount
   * @see #initialAmount
   */
  public Double getInitialAmount() {
    return initialAmount;
  }

  /**
   * @param initialAmount the initialAmount to set
   * @see #initialAmount
   */
  public void setInitialAmount(final Double initialAmount) {
    this.initialAmount = initialAmount;
    if (initialAmount != null) {
      this.setInitialConcentration(null);
    }
  }

  /**
   * @return the charge
   * @see #charge
   */
  public Integer getCharge() {
    return charge;
  }

  /**
   * @param charge the charge to set
   * @see #charge
   */
  public void setCharge(final Integer charge) {
    this.charge = charge;
  }

  /**
   * @return the initialConcentration
   * @see #initialConcentration
   */
  public Double getInitialConcentration() {
    return initialConcentration;
  }

  /**
   * @param initialConcentration the initialConcentration to set
   * @see #initialConcentration
   */
  public void setInitialConcentration(final Double initialConcentration) {
    this.initialConcentration = initialConcentration;
    if (initialConcentration != null) {
      this.setInitialAmount(null);
    }
  }

  /**
   * @return the onlySubstanceUnits
   * @see #onlySubstanceUnits
   */
  public Boolean getOnlySubstanceUnits() {
    return onlySubstanceUnits;
  }

  /**
   * @param onlySubstanceUnits the onlySubstanceUnits to set
   * @see #onlySubstanceUnits
   */
  public void setOnlySubstanceUnits(final Boolean onlySubstanceUnits) {
    this.onlySubstanceUnits = onlySubstanceUnits;
  }

  /**
   * @return the homodimer
   * @see #homodimer
   */
  public int getHomodimer() {
    return homodimer;
  }

  /**
   * @param homodimer the homodimer to set
   * @see #homodimer
   */
  public void setHomodimer(final int homodimer) {
    this.homodimer = homodimer;
  }

  /**
   * @return the positionToCompartment
   * @see #positionToCompartment
   */
  public PositionToCompartment getPositionToCompartment() {
    return positionToCompartment;
  }

  /**
   * @param positionToCompartment the positionToCompartment to set
   * @see #positionToCompartment
   */
  public void setPositionToCompartment(final PositionToCompartment positionToCompartment) {
    this.positionToCompartment = positionToCompartment;
  }

  /**
   * @return the hypothetical
   * @see #hypothetical
   */
  public Boolean getHypothetical() {
    return hypothetical;
  }

  /**
   * @return the uniprot
   * @see #uniprots
   */
  public Set<UniprotRecord> getUniprots() {
    return uniprots;
  }

  /**
   * @param uniprots set of uniprot records for this species
   * @see #uniprots
   */
  public void setUniprots(final Set<UniprotRecord> uniprots) {
    this.uniprots = uniprots;
  }

  public void addUniprot(final UniprotRecord uniprot) {
    this.uniprots.add(uniprot);
    uniprot.setSpecies(this);
  }

  /**
   * @return the onlySubstanceUnits
   * @see #onlySubstanceUnits
   */
  public Boolean hasOnlySubstanceUnits() {
    return onlySubstanceUnits;
  }

  /**
   * Is species hypothetical or not.
   *
   * @return <code>true</code> if species is hypothetical, <code>false</code> otherwise
   */
  public boolean isHypothetical() {
    if (hypothetical == null) {
      return false;
    }
    return hypothetical;
  }

  /**
   * @param hypothetical the hypothetical to set
   * @see #hypothetical
   */
  public void setHypothetical(final Boolean hypothetical) {
    this.hypothetical = hypothetical;
  }

  public boolean isBoundaryCondition() {
    if (boundaryCondition == null) {
      return false;
    }
    return boundaryCondition;
  }

  public Boolean getBoundaryCondition() {
    return boundaryCondition;
  }

  public void setBoundaryCondition(final Boolean boundaryCondition) {
    this.boundaryCondition = boundaryCondition;
  }

  public boolean isConstant() {
    if (constant == null) {
      return false;
    }
    return constant;
  }

  public Boolean getConstant() {
    return constant;
  }

  public void setConstant(final Boolean constant) {
    this.constant = constant;
  }

  public SbmlUnit getSubstanceUnits() {
    return this.substanceUnits;
  }

  public void setSubstanceUnits(final SbmlUnit unit) {
    this.substanceUnits = unit;
  }

  public LineType getBorderLineType() {
    if (!isHypothetical()) {
      return LineType.SOLID;
    } else {
      return LineType.DASHED;
    }
  }

}
