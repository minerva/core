package lcsb.mapviewer.model.map.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * This class defines connection between super-model (supermap) and sub-model
 * (submap).
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("MODEL_SUBMODEL_LINK")
public class ModelSubmodelConnection extends SubmodelConnection {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  
  /**
   * Super (parent) model.
   */
  @Cascade({ CascadeType.SAVE_UPDATE})
  @ManyToOne(fetch = FetchType.LAZY)
  private ModelData parentModel;

  /**
   * Default constructor.
   */
  public ModelSubmodelConnection() {

  }

  /**
   * Default constructor that initialize some fields.
   * 
   * @param submodel
   *          {@link SubmodelConnection#submodel}
   * @param type
   *          {@link SubmodelConnection#type}
   */
  public ModelSubmodelConnection(final ModelData submodel, final SubmodelType type) {
    super(submodel, type);
  }

  /**
   * Default constructor that initialize some fields.
   * 
   * @param model
   *          {@link SubmodelConnection#submodel}
   * @param type
   *          {@link SubmodelConnection#type}
   */
  public ModelSubmodelConnection(final Model model, final SubmodelType type) {
    super(model.getModelData(), type);
  }

  /**
   * Constructor that creates copy of the {@link ModelSubmodelConnection} object.
   * 
   * @param original
   *          original object from which copy is prepared
   */
  public ModelSubmodelConnection(final ModelSubmodelConnection original) {
    super(original);
    this.setParentModel(original.getParentModel());
  }

  /**
   * Default constructor that initialize some fields.
   * 
   * @param submodel
   *          {@link SubmodelConnection#submodel}
   * @param type
   *          {@link SubmodelConnection#type}
   * @param name
   *          {@link SubmodelConnection#name}
   */
  public ModelSubmodelConnection(final Model submodel, final SubmodelType type, final String name) {
    super(submodel, type, name);
  }

  /**
   * @return the parentModel
   * @see #parentModel
   */
  public ModelData getParentModel() {
    return parentModel;
  }

  /**
   * @param parentModel
   *          the parentModel to set
   * @see #parentModel
   */
  public void setParentModel(final ModelData parentModel) {
    this.parentModel = parentModel;
  }

  /**
   * @param model
   *          the parent model to set
   * @see #parentModel
   */
  @JsonIgnore
  public void setParentModel(final Model model) {
    setParentModel(model.getModelData());
  }

  @Override
  public ModelSubmodelConnection copy() {
    if (this.getClass() == ModelSubmodelConnection.class) {
      return new ModelSubmodelConnection(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }
}
