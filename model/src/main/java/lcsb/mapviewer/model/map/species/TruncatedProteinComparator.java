package lcsb.mapviewer.model.map.species;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;

/**
 * Comparator class used for comparing {@link TruncatedProtein} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class TruncatedProteinComparator extends Comparator<TruncatedProtein> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public TruncatedProteinComparator(final double epsilon) {
    super(TruncatedProtein.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public TruncatedProteinComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected Comparator<?> getParentComparator() {
    return new ProteinComparator(epsilon);
  }

  @Override
  protected int internalCompare(final TruncatedProtein arg0, final TruncatedProtein arg1) {
    return 0;
  }
}
