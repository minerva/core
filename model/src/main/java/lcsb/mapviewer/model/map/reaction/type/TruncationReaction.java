package lcsb.mapviewer.model.map.reaction.type;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines a standard CellDesigner truncation reaction. It must have
 * at least one reactant and two products.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("TRUNCATION_REACTION_REACTION")
public class TruncationReaction extends Reaction implements TwoProductReactionInterface {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  protected TruncationReaction() {
    super();
  }

  public TruncationReaction(final String elementId) {
    super(elementId);
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   *
   * @param result parent reaction from which we copy data
   */
  public TruncationReaction(final Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Truncation";
  }

  @Override
  public ReactionRect getReactionRect() {
    return ReactionRect.RECT_BOLT;
  }

  @Override
  public TruncationReaction copy() {
    if (this.getClass() == TruncationReaction.class) {
      return new TruncationReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermReactionType.TRUNCATION.getSBO();
  }
}
