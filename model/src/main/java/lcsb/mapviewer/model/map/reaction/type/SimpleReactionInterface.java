package lcsb.mapviewer.model.map.reaction.type;

/**
 * This interface is only a marker which points out the type of reaction deigned
 * in CD.
 * 
 * @author Piotr Gawron
 * 
 */
public interface SimpleReactionInterface {

}
