package lcsb.mapviewer.model.map.modifier;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.species.Element;

/**
 * This class defines modulation modifier in the reaction.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("MODULATION_MODIFIER")
public class Modulation extends Modifier {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  protected Modulation() {
    super();
  }

  /**
   * Constructor that creates modulation modifier for given element.
   * 
   * @param element
   *          element object to which this modifier is assigned
   */
  public Modulation(final Element element) {
    super(element);
    if (element == null) {
      throw new InvalidArgumentException("Element cannot be null");
    }
  }

  /**
   * Constructor that creates object with data taken from parameter modulation.
   * 
   * @param modulation
   *          object from which data are initialized
   */
  public Modulation(final Modulation modulation) {
    super(modulation);
  }

  @Override
  public Modulation copy() {
    if (this.getClass() == Modulation.class) {
      return new Modulation(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
