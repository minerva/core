package lcsb.mapviewer.model.map.modifier;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.species.Element;

/**
 * This class defines trigger modifier in the reaction.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("TRIGGER_MODIFIER")
public class Trigger extends Modifier {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  protected Trigger() {
    super();
  }

  /**
   * Constructor that creates trigger modifier for given element.
   * 
   * @param element
   *          element object to which this modifier is assigned
   */
  public Trigger(final Element element) {
    super(element);
    if (element == null) {
      throw new InvalidArgumentException("Element cannot be null");
    }
  }

  /**
   * Constructor that creates object with data taken from parameter trigger.
   * 
   * @param trigger
   *          object from which data are initialized
   */
  public Trigger(final Trigger trigger) {
    super(trigger);
  }

  @Override
  public Trigger copy() {
    if (this.getClass() == Trigger.class) {
      return new Trigger(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
