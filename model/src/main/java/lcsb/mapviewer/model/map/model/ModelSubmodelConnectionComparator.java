package lcsb.mapviewer.model.map.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;

/**
 * This class implements comparator interface for
 * {@link ModelSubmodelConnection}.
 * 
 * @author Piotr Gawron
 * 
 */
public class ModelSubmodelConnectionComparator extends Comparator<ModelSubmodelConnection> {
  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public ModelSubmodelConnectionComparator(final double epsilon) {
    super(ModelSubmodelConnection.class);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public ModelSubmodelConnectionComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected Comparator<?> getParentComparator() {
    return new SubmodelConnectionComparator(epsilon);
  }

  @Override
  protected int internalCompare(final ModelSubmodelConnection arg0, final ModelSubmodelConnection arg1) {
    ModelComparator comparator = new ModelComparator(epsilon);

    if (arg0.getParentModel() != null && arg1.getParentModel() != null) {
      int status = comparator.compare(arg0.getParentModel().getModel(), arg1.getParentModel().getModel());
      if (status != 0) {
        logger.debug("Model different: " + arg0.getParentModel() + ", " + arg1.getParentModel());
        return status;
      }
    } else {
      if (arg0.getParentModel() != null) {
        logger.debug("Model different: " + arg0.getParentModel() + ", " + arg1.getParentModel());
        return -1;
      }
      if (arg1.getParentModel() != null) {
        logger.debug("Model different: " + arg0.getParentModel() + ", " + arg1.getParentModel());
        return 1;
      }
    }

    return 0;
  }

}
