package lcsb.mapviewer.model.map.species.field;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.sbo.SBOTermModificationResidueType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Species;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.awt.Color;
import java.io.Serializable;

/**
 * This structure contains information about Transcription Site for one of the
 * following {@link Species}:
 * <ul>
 * <li>{@link Gene}</li>
 * </ul>
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("TRANSCRIPTION_SITE")
public class TranscriptionSite extends AbstractRegionModification implements Serializable {

  private static final long serialVersionUID = 1L;

  @Column(length = 255)
  private String direction = null;

  private Boolean active = false;

  /**
   * Default constructor.
   */
  public TranscriptionSite() {
    setFillColor(Color.BLACK);
  }

  public TranscriptionSite(final String id) {
    super(id);
    setFillColor(Color.BLACK);
  }

  /**
   * Constructor that initialize object with the data from the parameter.
   *
   * @param original object from which we initialize data
   */
  public TranscriptionSite(final TranscriptionSite original) {
    super(original);
    this.direction = original.getDirection();
    this.active = original.getActive();
  }

  /**
   * Creates a copy of current object.
   *
   * @return copy of the object
   */
  @Override
  public TranscriptionSite copy() {
    if (this.getClass() == TranscriptionSite.class) {
      return new TranscriptionSite(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }

  }

  public String getDirection() {
    return direction;
  }

  public void setDirection(final String direction) {
    this.direction = direction;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(final Boolean active) {
    this.active = active;
  }

  @Override
  public String toString() {
    return super.toString() + "," + getActive() + "," + getDirection();
  }

  @Override
  public String getSboTerm() {
    return SBOTermModificationResidueType.TRANSCRIPTION_SITE.getSBO();
  }

}
