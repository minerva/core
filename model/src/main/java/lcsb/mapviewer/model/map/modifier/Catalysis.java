package lcsb.mapviewer.model.map.modifier;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.species.Element;

/**
 * This class defines catalysis modifier in the reaction.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("CATALYSIS_MODIFIER")
public class Catalysis extends Modifier {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  protected Catalysis() {
    super();
  }

  /**
   * Constructor that creates {@link Catalysis} modifier for given element.
   * 
   * @param element
   *          element object to which this modifier is assigned
   */
  public Catalysis(final Element element) {
    super(element);
    if (element == null) {
      throw new InvalidArgumentException("Element cannot be null");
    }
  }

  /**
   * Constructor that creates object with data taken from parameter catalysis.
   * 
   * @param catalysis
   *          object from which data are initialized
   */
  public Catalysis(final Catalysis catalysis) {
    super(catalysis);
  }

  @Override
  public Catalysis copy() {
    if (this.getClass() == Catalysis.class) {
      return new Catalysis(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
