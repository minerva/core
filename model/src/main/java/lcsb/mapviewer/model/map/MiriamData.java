package lcsb.mapviewer.model.map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.Article;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.modelutils.serializer.model.map.MiriamDataSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Representation of a single species or reaction annotation. Miriam format is a
 * standard described <a href="http://www.ebi.ac.uk/miriam/main/">here</a>
 *
 * @author Piotr Gawron
 */
@Entity
@XmlRootElement
@JsonSerialize(using = MiriamDataSerializer.class)
public class MiriamData implements Comparable<MiriamData>, MinervaEntity {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  private static final Logger logger = LogManager.getLogger();

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * What is the connection between element and the annotation.
   */
  private MiriamRelationType relationType = null;

  /**
   * Type of database.
   */
  @Enumerated(EnumType.STRING)
  @JsonProperty(value = "type")
  private MiriamType dataType;

  /**
   * Resource identifier in the database.
   */
  @Column(nullable = false, length = 512)
  private String resource = "";

  /**
   * The annotator which created the miriam data or which should be associated
   * with it.
   */
  private Class<?> annotator = null;

  private String link = null;

  @ManyToOne
  private Article article = null;

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * Default constructor.
   */
  public MiriamData() {
    super();
  }

  /**
   * Constructor that initialize the data by information from param.
   *
   * @param md original miriam data object
   */
  public MiriamData(final MiriamData md) {
    setRelationType(md.relationType);
    setDataType(md.dataType);
    setResource(md.resource);
    setAnnotator(md.annotator);
  }

  /**
   * Constructor that initialize the data by information from params.
   *
   * @param relationType {@link #relationType}
   * @param mt           type of the miriam data (see: {@link MiriamType})
   * @param resource     {@link #resource}
   * @param annotator    {@link #annotator}
   */
  public MiriamData(final MiriamRelationType relationType, final MiriamType mt, final String resource, final Class<?> annotator) {
    if (mt == null) {
      throw new InvalidArgumentException("MiriamType cannot be null");
    }
    if (relationType == null) {
      throw new InvalidArgumentException("MiriamRelationType cannot be null");
    }
    setRelationType(relationType);
    setDataType(mt);
    setResource(resource);
    setAnnotator(annotator);
  }

  /**
   * Constructor that initialize the data by information from params.
   *
   * @param relationType {@link #relationType}
   * @param mt           type of the miriam data (see: {@link MiriamType})
   * @param resource     {@link #resource}
   */
  public MiriamData(final MiriamRelationType relationType, final MiriamType mt, final String resource) {
    this(relationType, mt, resource, null);
  }

  /**
   * Constructor that initialize the data by information from params.
   *
   * @param mt       type of the miriam data (see: {@link MiriamType})
   * @param resource {@link #resource}
   */
  public MiriamData(final MiriamType mt, final String resource, final Class<?> annotator) {
    this(MiriamRelationType.BQ_BIOL_IS, mt, resource, annotator);
  }

  /**
   * Constructor that initialize the data by information from params.
   *
   * @param mt       type of the miriam data (see: {@link MiriamType})
   * @param resource {@link #resource}
   */
  public MiriamData(final MiriamType mt, final String resource) {
    this(mt, resource, null);
  }

  /**
   * @param identifier string represents identifier.
   * @return {@link #resource}
   */
  public static String getIdFromIdentifier(final String identifier) {
    String result = identifier;
    if (result != null) {
      final int index = result.indexOf(":");
      if (index != -1) {
        result = result.trim().substring(index + 1);
      }
    }
    return result;
  }

  /**
   * @return {@link #resource}
   */
  public String getResource() {
    return resource;
  }

  /**
   * Sets new {@link #resource}.
   *
   * @param resource new {@link #resource}
   */
  public void setResource(final String resource) {
    if (resource == null) {
      this.resource = null;
    } else {
      this.resource = resource.replace("%3A", ":");
      if (this.dataType == MiriamType.HGNC && this.resource.toLowerCase().startsWith("hgnc:")) {
        final String newResource = this.resource.substring(5);
        logger.debug("hgnc identifier: \"" + this.resource + "\" is changed to: \"" + newResource + "\"");
        this.resource = newResource;
      }
    }
  }

  @Override
  public int hashCode() {
    return (dataType + resource).hashCode();
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }

    if (!(o instanceof MiriamData)) {
      return false;
    }

    final MiriamData that = (MiriamData) o;

    return this.compareTo(that) == 0;
  }

  @Override
  public String toString() {
    final String annotatorClass = annotator != null ? ":" + annotator.getName() : "";
    if (relationType != null) {
      return "[" + relationType.getStringRepresentation() + "] " + dataType + ":" + resource + annotatorClass;
    } else {
      return "[UNKNOWN] " + dataType + ":" + resource + annotatorClass;

    }
  }

  @Override
  public int compareTo(final MiriamData other) {
    final String name = annotator != null ? annotator.getName() : "";
    final String otherName = other.annotator != null ? other.annotator.getName() : "";
    return (dataType + ":" + resource + ":" + name).toLowerCase()
        .compareTo((other.dataType + ":" + other.resource + ":" + otherName).toLowerCase());
  }

  /**
   * @return the id
   * @see #id
   */
  @Override
  public int getId() {
    return id;
  }

  /**
   * @param id the id to set
   * @see #id
   */
  public void setId(final int id) {
    this.id = id;
  }

  /**
   * @return the dataType
   * @see #dataType
   */
  public MiriamType getDataType() {
    return dataType;
  }

  /**
   * @param dataType the dataType to set
   * @see #dataType
   */
  public void setDataType(final MiriamType dataType) {
    this.dataType = dataType;
  }

  /**
   * @return the relationType
   * @see #relationType
   */
  public MiriamRelationType getRelationType() {
    return relationType;
  }

  /**
   * @param relationType the relationType to set
   * @see #relationType
   */
  public void setRelationType(final MiriamRelationType relationType) {
    this.relationType = relationType;
  }

  /**
   * @return {@link #annotator}
   */
  public Class<?> getAnnotator() {
    return annotator;
  }

  public void setAnnotator(final Class<?> annotator) {
    this.annotator = annotator;
  }

  /**
   * Returns identifier in the new identifiers.org format.
   *
   * @return
   */
  public String getPrefixedIdentifier() {
    String prefix = "null";
    if (getDataType() != null && getDataType().getNamespace() != null) {
      prefix = getDataType().getNamespace();
    }
    return prefix + ":" + getResource();
  }

  public String getLink() {
    return link;
  }

  public void setLink(final String link) {
    this.link = link;
  }

  public Article getArticle() {
    return article;
  }

  public void setArticle(final Article article) {
    this.article = article;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
