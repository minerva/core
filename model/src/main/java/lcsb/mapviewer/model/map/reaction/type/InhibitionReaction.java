package lcsb.mapviewer.model.map.reaction.type;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines a standard CellDesigner inhibition reaction.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("INHIBITION_REACTION")
public class InhibitionReaction extends Reaction implements SimpleReactionInterface, ModifierReactionNotation {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  protected InhibitionReaction() {
    super();
  }

  public InhibitionReaction(final String elementId) {
    super(elementId);
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   *
   * @param result parent reaction from which we copy data
   */
  public InhibitionReaction(final Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Inhibition";
  }

  @Override
  public ReactionRect getReactionRect() {
    return null;
  }

  @Override
  public InhibitionReaction copy() {
    if (this.getClass() == InhibitionReaction.class) {
      return new InhibitionReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermReactionType.INHIBITION.getSBO();
  }

}
