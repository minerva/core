package lcsb.mapviewer.model.map.layout;

/**
 * Defines type of reference genome.
 * 
 * @author Piotr Gawron
 *
 */
public enum ReferenceGenomeType {
  /**
   * UCSC reference genome database.
   */
  UCSC("https://genome.ucsc.edu/");

  /**
   * Homepage of the reference genome database.
   */
  private String homepage;

  /**
   * Default constructor.
   * 
   * @param homepage
   *          {@link #homepage}
   */
  ReferenceGenomeType(final String homepage) {
    this.homepage = homepage;
  }

  /**
   * @return the homepage
   * @see #homepage
   */
  public String getHomepage() {
    return homepage;
  }
}
