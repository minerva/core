package lcsb.mapviewer.model.map.species;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithBindingRegion;
import lcsb.mapviewer.model.map.species.field.SpeciesWithResidue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithStructuralState;
import lcsb.mapviewer.model.map.species.field.StructuralState;

/**
 * Entity representing protein element on the map.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@DiscriminatorValue("PROTEIN")
public abstract class Protein extends Species implements SpeciesWithBindingRegion, SpeciesWithResidue, SpeciesWithStructuralState {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * List of modifications for the Protein.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "species", orphanRemoval = true)
  @LazyCollection(LazyCollectionOption.FALSE)
  @Fetch(FetchMode.SUBSELECT)
  private List<ModificationResidue> modificationResidues = new ArrayList<>();

  /**
   * Empty constructor required by hibernate.
   */
  Protein() {
  }

  /**
   * Constructor that creates a copy of the element given in the parameter.
   * 
   * @param original
   *          original object that will be used for creating copy
   */
  protected Protein(final Protein original) {
    super(original);
    for (final ModificationResidue mr : original.getModificationResidues()) {
      addModificationResidue(mr.copy());
    }
  }

  /**
   * Default constructor.
   * 
   * @param elementId
   *          unique (within model) element identifier
   */
  protected Protein(final String elementId) {
    super(elementId);
  }

  /**
   * Adds modification to the protein.
   * 
   * @param modificationResidue
   *          modification to add
   */
  private void addModificationResidue(final ModificationResidue modificationResidue) {
    modificationResidues.add(modificationResidue);
    modificationResidue.setSpecies(this);
  }

  @Override
  public void addBindingRegion(final BindingRegion bindingRegion) {
    this.addModificationResidue(bindingRegion);
  }

  @Override
  public void addResidue(final Residue residue) {
    this.addModificationResidue(residue);
  }

  /**
   * @return the modificationResidues
   * @see #modificationResidues
   */
  @Override
  public List<ModificationResidue> getModificationResidues() {
    return modificationResidues;
  }

  /**
   * @param modificationResidues
   *          the modificationResidues to set
   * @see #modificationResidues
   */
  public void setModificationResidues(final List<ModificationResidue> modificationResidues) {
    this.modificationResidues = modificationResidues;
  }

  @Override
  @JsonIgnore
  public String getStringType() {
    return "Protein";
  }

  @Override
  public void addStructuralState(final StructuralState state) {
    this.addModificationResidue(state);
  }

}
