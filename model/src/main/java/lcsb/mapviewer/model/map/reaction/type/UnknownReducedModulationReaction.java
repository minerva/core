package lcsb.mapviewer.model.map.reaction.type;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines a standard CellDesigner unknown reduced modulation
 * reaction. It must have at least one reactant and one product.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("U_REDUCED_MODULATION")
public class UnknownReducedModulationReaction extends Reaction implements SimpleReactionInterface, ReducedNotation {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  protected UnknownReducedModulationReaction() {
    super();
  }

  public UnknownReducedModulationReaction(final String elementId) {
    super(elementId);
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   *
   * @param result parent reaction from which we copy data
   */
  public UnknownReducedModulationReaction(final Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Unknown reduced modulation";
  }

  @Override
  public ReactionRect getReactionRect() {
    return null;
  }

  @Override
  public UnknownReducedModulationReaction copy() {
    if (this.getClass() == UnknownReducedModulationReaction.class) {
      return new UnknownReducedModulationReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermReactionType.UNKNOWN_REDUCED_MODULATION.getSBO();
  }
}
