package lcsb.mapviewer.model.map.compartment;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.model.Model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines compartment that covers left part of the model up to some
 * border on the right side.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("LEFT_SQUARE_COMPARTMENT")
public class LeftSquareCompartment extends Compartment {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Empty constructor required by hibernate.
   */
  LeftSquareCompartment() {
    super();
  }

  /**
   * Constructor that creates a compartment with the new shape but takes the
   * reference data from the compartment given as parameter.
   *
   * @param original original compartment where the data was kept
   * @param model    model object to which the compartment will be assigned
   */

  public LeftSquareCompartment(final Compartment original, final Model model) {
    super(original);
    setX(0.0);
    setWidth(model.getWidth() * 2);
    setY(0.0);
    setHeight(model.getHeight() * 2);
  }

  /**
   * Constructor that creates a compartment with the new shape and takes the
   * reference data from the compartment given as parameter.
   *
   * @param original original compartment where the data was kept
   */
  public LeftSquareCompartment(final LeftSquareCompartment original) {
    super(original);
  }

  /**
   * Default constructor.
   *
   * @param elementId identifier of the compartment
   */
  public LeftSquareCompartment(final String elementId) {
    setElementId(elementId);
  }

  @Override
  public LeftSquareCompartment copy() {
    if (this.getClass() == LeftSquareCompartment.class) {
      return new LeftSquareCompartment(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getShape() {
    return "LEFT_SQUARE_COMPARTMENT";
  }
}
