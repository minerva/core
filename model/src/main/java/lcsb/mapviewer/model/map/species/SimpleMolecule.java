package lcsb.mapviewer.model.map.species;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.sbo.SBOTermSpeciesType;

/**
 * Entity representing simple molecule element on the map.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@DiscriminatorValue("SIMPLE_MOLECULE")
public class SimpleMolecule extends Chemical {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Empty constructor required by hibernate.
   */
  SimpleMolecule() {
  }

  /**
   * Constructor that creates a copy of the element given in the parameter.
   * 
   * @param original
   *          original object that will be used for creating copy
   */
  public SimpleMolecule(final SimpleMolecule original) {
    super(original);
  }

  /**
   * Default constructor.
   * 
   * @param elementId
   *          unique (within model) element identifier
   */
  public SimpleMolecule(final String elementId) {
    setElementId(elementId);
  }

  @Override
  public SimpleMolecule copy() {
    if (this.getClass() == SimpleMolecule.class) {
      return new SimpleMolecule(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  @JsonIgnore
  public String getStringType() {
    return "Simple molecule";
  }

  @Override
  public String getSboTerm() {
    return SBOTermSpeciesType.SIMPLE_MOLECULE.getSBO();
  }

}
