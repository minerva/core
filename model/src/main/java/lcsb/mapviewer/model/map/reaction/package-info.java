/**
 * Contains structures used for modeling reactions. Main class representing
 * reaction is {@link lcsb.mapviewer.model.map.reaction.Reaction Reaction}. It
 * is extended by several different types of reactions that were put in the
 * {@link lcsb.mapviewer.model.map.reaction.type type} sub-package.
 * <p>
 * Every reaction contains set of nodes that extend
 * {@link lcsb.mapviewer.model.map.reaction.AbstractNode AbstractNode} class.
 * </p>
 * 
 */
package lcsb.mapviewer.model.map.reaction;
