package lcsb.mapviewer.model.map.compartment;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.model.Model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines compartment that covers bottom part of the model up to
 * some border (the top is limited).
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("BOTTOM_SQUARE_COMPARTMENT")
public class BottomSquareCompartment extends Compartment {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Constructor that creates an compartment with the new shape but takes the
   * reference data from the compartment given as parameter.
   *
   * @param original original compartment where the data was kept
   * @param model    model object to which the compartment will be assigned
   */
  public BottomSquareCompartment(final Compartment original, final Model model) {
    super(original);
    setX(0.0);
    setWidth(model.getWidth() * 2);
    setY(0.0);
    setHeight(model.getHeight() * 2);
  }

  /**
   * Constructor that creates an compartment with the new shape and takes the
   * reference data from the compartment given as parameter.
   *
   * @param original original compartment where the data was kept
   */
  public BottomSquareCompartment(final Compartment original) {
    super(original);
  }

  /**
   * Empty constructor required by hibernate.
   */
  BottomSquareCompartment() {
    super();
  }

  /**
   * Default constructor.
   *
   * @param elementId identifier of the compartment
   */
  public BottomSquareCompartment(final String elementId) {
    setElementId(elementId);
  }

  @Override
  public BottomSquareCompartment copy() {
    if (this.getClass() == BottomSquareCompartment.class) {
      return new BottomSquareCompartment(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getShape() {
    return "BOTTOM_SQUARE_COMPARTMENT";
  }

}
