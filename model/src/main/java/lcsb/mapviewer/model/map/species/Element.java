package lcsb.mapviewer.model.map.species;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.kinetics.SbmlArgument;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.ElementSubmodelConnection;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.modelutils.serializer.model.map.ElementAsIdSerializer;
import lcsb.mapviewer.modelutils.serializer.model.map.ModelAsIdSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlTransient;
import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Abstract class representing objects in the model. Elements are objects that
 * contains graphical representation data for element and meta information about
 * element (like symbol, name, etc.).
 *
 * @author Piotr Gawron
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "element_type_db", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("GENERIC")
public abstract class Element implements BioEntity, MinervaEntity, SbmlArgument {

  /**
   * Comparator of elements that takes into consideration size (width*height) of
   * elements.
   */
  public static final Comparator<Element> SIZE_COMPARATOR = new Comparator<Element>() {
    @Override
    public int compare(final Element element1, final Element element2) {

      final double size = element1.getWidth() * element1.getHeight();
      final double size2 = element2.getWidth() * element2.getHeight();
      if (size < size2) {
        return 1;
      } else if (size > size2) {
        return -1;
      } else {
        return 0;
      }
    }
  };
  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default font size for element description.
   */
  private static final double DEFAULT_FONT_SIZE = 12.0;
  /**
   * Maximum length of the valid synonym name.
   */
  private static final int MAX_SYNONYM_LENGTH = 255;

  protected static Logger logger = LogManager.getLogger();

  /**
   * Database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Map model object to which element belongs to.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JsonSerialize(using = ModelAsIdSerializer.class)
  private ModelData model;

  /**
   * When defined this represent glyph that should be used for drawing this
   * element.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  private Glyph glyph;

  /**
   * Submodel that is extension of these element.
   */
  @ManyToOne(fetch = FetchType.LAZY, cascade = javax.persistence.CascadeType.ALL)
  private ElementSubmodelConnection submodel;

  /**
   * {@link Compartment} where element is located. When element lies outside
   * every compartment then null value is assigned.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @Cascade({CascadeType.SAVE_UPDATE})
  @JsonSerialize(using = ElementAsIdSerializer.class)
  private Compartment compartment;

  @ManyToOne(fetch = FetchType.LAZY)
  @Cascade({CascadeType.SAVE_UPDATE})
  @JsonSerialize(using = ElementAsIdSerializer.class)
  private PathwayCompartment pathway;

  /**
   * Unique string identifier within one model object (usually imported from
   * external source from which map was imported).
   */
  @Column(length = 255)
  private String elementId;

  /**
   * X coordinate on the map where element is located (top left corner).
   */
  private Double x;

  /**
   * Y coordinate on the map where element is located (top left corner).
   */
  private Double y;

  /**
   * Z-index of elements on the map.
   */
  @Column(nullable = false)
  private Integer z = null;

  /**
   * Width of the element.
   */
  private Double width;

  /**
   * Height of the element.
   */
  private Double height;

  /**
   * Size of the font used for description.
   */
  @Column(nullable = false)
  private Double fontSize;

  /**
   * Color of the font.
   */
  @Type(type = "lcsb.mapviewer.persist.mapper.ColorMapper")
  @Column(nullable = false)
  private Color fontColor = Color.BLACK;

  /**
   * Color of filling.
   */
  @Type(type = "lcsb.mapviewer.persist.mapper.ColorMapper")
  @Column(nullable = false)
  private Color fillColor;

  @Type(type = "lcsb.mapviewer.persist.mapper.ColorMapper")
  @Column(nullable = false)
  private Color borderColor = Color.BLACK;

  /**
   * From which level element should be visible.
   *
   * @see #transparencyLevel
   * @see lcsb.mapviewer.converter.graphics.AbstractImageGenerator.Params#level AbstractImageGenerator.Params#level
   */
  private String visibilityLevel = "";

  /**
   * From which level element should be transparent.
   *
   * @see #visibilityLevel
   * @see AbstractImageGenerator.Params#level
   */
  private String transparencyLevel = "";

  /**
   * Notes describing this element.
   */
  @Column(columnDefinition = "TEXT")
  private String notes = "";

  /**
   * Symbol of the element.
   */
  @Column(length = 255)
  private String symbol;

  /**
   * Full name of the element.
   */
  @Column(length = 255)
  private String fullName;

  /**
   * Abbreviation associated with the element.
   */
  @Column(length = 255)
  private String abbreviation;

  /**
   * Formula associated with the element.
   */
  @Column(length = 255)
  private String formula;

  /**
   * Short name of the element.
   */
  @Column(length = 255)
  private String name = "";

  @Column(nullable = false, name = "name_x")
  private Double nameX;

  @Column(nullable = false, name = "name_y")
  private Double nameY;

  @Column(nullable = false)
  private Double nameWidth;

  @Column(nullable = false)
  private Double nameHeight;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private VerticalAlign nameVerticalAlign;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private HorizontalAlign nameHorizontalAlign;

  /**
   * Lists of all synonyms used for describing this element.
   */
  @ElementCollection(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SUBSELECT)
  @CollectionTable(name = "element_synonyms", joinColumns = @JoinColumn(name = "id"))
  @Column(name = "synonym", length = 512)
  @OrderColumn(name = "idx")
  @Cascade({org.hibernate.annotations.CascadeType.ALL})
  private List<String> synonyms = new ArrayList<>();

  /**
   * Lists of all synonyms used for describing this element.
   */
  @ElementCollection(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SUBSELECT)
  @CollectionTable(name = "element_former_symbols", joinColumns = @JoinColumn(name = "id"))
  @Column(name = "symbol", length = 255)
  @OrderColumn(name = "idx")
  @Cascade({org.hibernate.annotations.CascadeType.ALL})
  private List<String> formerSymbols = new ArrayList<>();

  /**
   * Set of miriam annotations for this element.
   */
  @Cascade({CascadeType.ALL})
  @Fetch(FetchMode.SUBSELECT)
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "element_miriam", joinColumns = {
      @JoinColumn(name = "element_id", referencedColumnName = "id", nullable = false, updatable = false)},
      inverseJoinColumns = {
          @JoinColumn(name = "miriam_id", referencedColumnName = "id", nullable = true, updatable = true)})
  @JsonProperty(value = "references")
  private Set<MiriamData> miriamData = new HashSet<>();

  @Version
  @JsonIgnore
  private long entityVersion;


  @JsonProperty(value = "immediateLink")
  @Column(nullable = true)
  private String immediateLink;

  /**
   * Constructor that creates a copy of the element given in the parameter.
   *
   * @param original original object that will be used for creating copy
   */
  protected Element(final Element original) {
    this.id = original.id;
    this.nameHorizontalAlign = original.getNameHorizontalAlign();
    this.nameVerticalAlign = original.getNameVerticalAlign();
    this.nameX = original.getNameX();
    this.nameY = original.getNameY();
    this.nameWidth = original.getNameWidth();
    this.nameHeight = original.getNameHeight();

    elementId = original.getElementId();
    x = original.getX();
    y = original.getY();
    z = original.getZ();
    width = original.getWidth();
    height = original.getHeight();
    fontSize = original.getFontSize();
    fillColor = original.getFillColor();
    fontColor = original.getFontColor();
    if (original.getSubmodel() != null) {
      setSubmodel(original.getSubmodel().copy());
    }

    this.notes = original.notes;
    this.symbol = original.symbol;
    this.fullName = original.fullName;
    this.name = original.getName();
    addSynonyms(original.getSynonyms());
    addFormerSymbols(original.getFormerSymbols());

    addMiriamData(original.getMiriamData());
    this.abbreviation = original.getAbbreviation();
    this.formula = original.getFormula();
    setVisibilityLevel(original.getVisibilityLevel());
    setTransparencyLevel(original.getTransparencyLevel());
    if (original.getGlyph() != null) {
      setGlyph(new Glyph(original.getGlyph()));
    }
    setBorderColor(original.getBorderColor());
  }

  /**
   * Empty constructor required by hibernate.
   */
  protected Element() {
    super();
    elementId = "";
    x = 0.0;
    y = 0.0;
    width = 0.0;
    height = 0.0;
    fontSize = DEFAULT_FONT_SIZE;
    fillColor = Color.white;
  }

  /**
   * Adds list of former symbol to the object.
   *
   * @param formerSymbols list of former symbols to add
   */
  public void addFormerSymbols(final List<String> formerSymbols) {
    this.formerSymbols.addAll(formerSymbols);
  }

  /**
   * Returns x coordinate of the element center.
   *
   * @return x coordinate of the element center
   */
  @JsonIgnore
  public double getCenterX() {
    return getX() + getWidth() / 2;
  }

  /**
   * Returns y coordinate of the element center.
   *
   * @return y coordinate of the element center
   */
  @JsonIgnore
  public double getCenterY() {
    return getY() + getHeight() / 2;
  }

  /**
   * Returns coordinates of the element center point.
   *
   * @return coordinates of the element center point
   */
  @JsonIgnore
  public Point2D getCenter() {
    return new Point2D.Double(getCenterX(), getCenterY());
  }

  /**
   * Methods that increase size of the element by increaseSize. It influence x, y,
   * width and height fields of the element.
   *
   * @param increaseSize by how many units we want to increase size of the element
   */
  public void increaseBorder(final double increaseSize) {
    x -= increaseSize;
    y -= increaseSize;
    this.width += increaseSize * 2;
    this.height += increaseSize * 2;
  }

  /**
   * This method computes the distance between point and the element. It assumes
   * that element is a rectangle.
   *
   * @param point point from which we are looking for a distance
   * @return distance between point and this element
   */
  public double getDistanceFromPoint(final Point2D point) {
    if (contains(point)) {
      return 0;
    } else if (getX() <= point.getX() && getX() + getWidth() >= point.getX()) {
      return Math.min(Math.abs(point.getY() - getY()), Math.abs(point.getY() - (getY() + getHeight())));
    } else if (getY() <= point.getY() && getY() + getHeight() >= point.getY()) {
      return Math.min(Math.abs(point.getX() - getX()), Math.abs(point.getX() - (getY() + getWidth())));
    } else {
      final double distance1 = point.distance(getX(), getY());
      final double distance2 = point.distance(getX() + getWidth(), getY());
      final double distance3 = point.distance(getX(), getY() + getHeight());
      final double distance4 = point.distance(getX() + getWidth(), getY() + getHeight());
      return Math.min(Math.min(distance1, distance2), Math.min(distance3, distance4));
    }
  }

  /**
   * Checks if element contains a point. It assumes that element is a rectangle.
   *
   * @param point point to check
   * @return <i>true</i> if point belongs to the element, <i>false</i> otherwise
   */
  public boolean contains(final Point2D point) {
    return getX() <= point.getX() && getY() <= point.getY() && getX() + getWidth() >= point.getX()
        && getY() + getHeight() >= point.getY();
  }

  /**
   * Checks if element contains a {@link LayerText}.
   */
  public boolean contains(final LayerText lt) {
    return getX() < lt.getX() && getY() < lt.getY() && getX() + getWidth() > lt.getX() + lt.getWidth()
        && getY() + getHeight() > lt.getY() + lt.getHeight();
  }

  /**
   * Checks if the element2 (given as parameter) is placed inside this element.
   * This method should be overridden by all inheriting classes - it should
   * properly handle other shapes than rectangle.
   *
   * @param element2 object to be checked
   * @return true if element2 lies in this object, false otherwise
   */
  public boolean contains(final Element element2) {
    if (element2 instanceof Species) {
      final Point2D p1 = new Point2D.Double(element2.getX(), element2.getY());
      final Point2D p2 = new Point2D.Double(element2.getX(), element2.getY() + element2.getHeight());
      final Point2D p3 = new Point2D.Double(element2.getX() + element2.getWidth(), element2.getY());
      final Point2D p4 = new Point2D.Double(element2.getX() + element2.getWidth(), element2.getY() + element2.getHeight());
      return contains(p1) || contains(p2) || contains(p3) || contains(p4);
    } else {
      return getX() < element2.getX() && getY() < element2.getY()
          && getX() + getWidth() > element2.getX() + element2.getWidth()
          && getY() + getHeight() > element2.getY() + element2.getHeight();
    }
  }

  /**
   * Returns a rectangle that determines a rectangle border.
   *
   * @return rectangle border
   */
  @JsonIgnore
  public Rectangle2D getBorder() {
    if (x == null || y == null || width == null || height == null || width == 0.0 || height == 0.0) {
      return null;
    }
    return new Rectangle2D.Double(x, y, width, height);
  }

  @JsonIgnore
  public Rectangle2D getNameBorder() {
    if (nameX == null || nameY == null || nameWidth == null || nameHeight == null || nameWidth == 0.0
        || nameHeight == 0.0) {
      logger.warn("Name border is not defined");
      return getBorder();
    }
    return new Rectangle2D.Double(nameX, nameY, nameWidth, nameHeight);
  }

  public void setBorder(final Rectangle2D border) {
    setX(border.getX());
    setY(border.getY());
    setWidth(border.getWidth());
    setHeight(border.getHeight());
  }

  public void setNameBorder(final Rectangle2D border) {
    setNameX(border.getX());
    setNameY(border.getY());
    setNameWidth(border.getWidth());
    setNameHeight(border.getHeight());
  }

  /**
   * @return the x
   * @see #x
   */
  public Double getX() {
    return x;
  }

  public void setX(final String string) {
    setX(Double.parseDouble(string));
  }

  public void setX(final int x) {
    setX((double) x);
  }

  public void setX(final Double x) {
    this.x = x;
  }

  public Double getY() {
    return y;
  }

  public void setY(final String string) {
    this.y = Double.parseDouble(string);
  }

  public void setY(final int y) {
    setY((double) y);
  }

  public void setY(final Double y) {
    this.y = y;
  }

  @Override
  public abstract Element copy();

  /**
   * @return the width
   * @see #width
   */
  public Double getWidth() {
    return width;
  }

  /**
   * Parse and set width.
   *
   * @param string text representing width
   * @see #width
   */
  public void setWidth(final String string) {
    try {
      width = Double.parseDouble(string);
    } catch (final NumberFormatException e) {
      throw new InvalidArgumentException("Invalid width format: " + string, e);
    }
  }

  /**
   * @param width the width value to set
   * @see #width
   */
  public void setWidth(final int width) {
    this.width = (double) width;
  }

  /**
   * @param width the width to set
   * @see #width
   */
  public void setWidth(final Double width) {
    this.width = width;
  }

  /**
   * @return the height
   * @see #height
   */
  public Double getHeight() {
    return height;
  }

  /**
   * Parse and set height.
   *
   * @param string text representing height
   * @see #height
   */
  public void setHeight(final String string) {
    try {
      height = Double.parseDouble(string);
    } catch (final Exception e) {
      throw new InvalidArgumentException("Invalid height format: " + string, e);
    }
  }

  /**
   * @param height the height value to set
   * @see #height
   */
  public void setHeight(final int height) {
    this.height = (double) height;
  }

  /**
   * @param height the height to set
   * @see #height
   */
  public void setHeight(final Double height) {
    this.height = height;
  }

  /**
   * @return the fontSize
   * @see #fontSize
   */
  public Double getFontSize() {
    return fontSize;
  }

  /**
   * Parse and set font size.
   *
   * @param string text representing font size
   * @see #fontSize
   */
  public void setFontSize(final String string) {
    this.fontSize = Double.parseDouble(string);
  }

  /**
   * @param fontSize the fontSize to set
   * @see #fontSize
   */
  public void setFontSize(final Double fontSize) {
    this.fontSize = fontSize;
  }

  /**
   * @param fontSize the fontSize to set
   * @see #fontSize
   */
  public void setFontSize(final int fontSize) {
    setFontSize((double) fontSize);
  }

  /**
   * @return the model
   * @see #model
   */
  @Override
  @JsonIgnore
  public ModelData getModelData() {
    return model;
  }

  /**
   * @param model the model to set
   * @see #model
   */
  public void setModelData(final ModelData model) {
    this.model = model;
  }

  /**
   * @return the compartment
   * @see #compartment
   */
  public Compartment getCompartment() {
    return compartment;
  }

  public PathwayCompartment getPathway() {
    return pathway;
  }

  @Override
  public String getVisibilityLevel() {
    return visibilityLevel;
  }

  /**
   * @param compartment the compartment to set
   * @see #compartment
   */
  public void setCompartment(final Compartment compartment) {
    if (compartment instanceof PathwayCompartment) {
      throw new InvalidArgumentException("Pathway cannot be set as a parent compartment");
    }
    if (this.compartment != null && compartment != this.compartment) {
      final Compartment formerCompartment = this.compartment;
      this.compartment = null;
      if (Hibernate.isInitialized(formerCompartment)) {
        formerCompartment.removeElement(this);
      }
    }
    this.compartment = compartment;
  }

  public void setPathway(final PathwayCompartment pathway) {
    this.pathway = pathway;
  }

  @Override
  public void setVisibilityLevel(final Integer visibilityLevel) {
    if (visibilityLevel == null) {
      this.visibilityLevel = null;
    } else {
      this.visibilityLevel = visibilityLevel + "";
    }
  }

  @Override
  public void setVisibilityLevel(final String visibilityLevel) {
    this.visibilityLevel = visibilityLevel;
  }

  /**
   * @return the color
   * @see #fillColor
   */
  public Color getFillColor() {
    return fillColor;
  }

  /**
   * @param color the color to set
   * @see #fillColor
   */
  public void setFillColor(final Color color) {
    this.fillColor = color;
  }

  /**
   * @return the transparencyLevel
   * @see #transparencyLevel
   */
  public String getTransparencyLevel() {
    return transparencyLevel;
  }

  /**
   * @param transparencyLevel the transparencyLevel to set
   * @see #transparencyLevel
   */
  public void setTransparencyLevel(final String transparencyLevel) {
    this.transparencyLevel = transparencyLevel;
  }

  /**
   * @return the submodel
   * @see #submodel
   */
  public ElementSubmodelConnection getSubmodel() {
    return submodel;
  }

  /**
   * @param submodel the submodel to set
   * @see #submodel
   */
  public void setSubmodel(final ElementSubmodelConnection submodel) {
    this.submodel = submodel;
    if (submodel != null) {
      this.submodel.setFromElement(this);
    }
  }

  /**
   * @return the fullName
   * @see #fullName
   */
  public String getFullName() {
    return fullName;
  }

  /**
   * @param fullName the fullName to set
   * @see #fullName
   */
  public void setFullName(final String fullName) {
    this.fullName = fullName;
  }

  @XmlTransient
  @Override
  @JsonIgnore
  public Model getModel() {
    if (model == null) {
      return null;
    }
    return model.getModel();
  }

  /**
   * @return the formerSymbols
   * @see #formerSymbols
   */
  public List<String> getFormerSymbols() {
    return formerSymbols;
  }

  /**
   * @param model the model to set
   * @see #model
   */
  public void setModel(final Model model) {
    this.model = model.getModelData();
  }

  /**
   * @param formerSymbols the formerSymbols to set
   * @see #formerSymbols
   */
  public void setFormerSymbols(final List<String> formerSymbols) {
    this.formerSymbols = formerSymbols;
  }

  /**
   * @return the miriamData
   * @see #miriamData
   */
  @Override
  public Set<MiriamData> getMiriamData() {
    return miriamData;
  }

  /**
   * @param miriamData the miriamData to set
   * @see #miriamData
   */
  public void setMiriamData(final Set<MiriamData> miriamData) {
    this.miriamData = miriamData;
  }

  @Override
  public void addMiriamData(final Collection<MiriamData> miriamData) {
    for (final MiriamData md : miriamData) {
      addMiriamData(md);
    }
  }

  @Override
  public void addMiriamData(final MiriamData md) {
    if (this.miriamData.contains(md)) {
      logger.warn("Miriam data (" + md.getDataType() + ": " + md.getResource() + ") for " + getElementId()
          + " already exists. Ignoring...");
    } else {
      this.miriamData.add(md);
    }
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * @return the id
   * @see #id
   */
  @Override
  public int getId() {
    return id;
  }

  /**
   * @param id the id to set
   * @see #id
   */
  public void setId(final int id) {
    this.id = id;
  }

  @Override
  public String getNotes() {
    return notes;
  }

  @Override
  public void setNotes(final String notes) {
    if (notes != null) {
      if (notes.contains("</html>")) {
        throw new InvalidArgumentException("Notes cannot contain html tags...");
      }
    }
    this.notes = notes;
  }

  /**
   * @return the symbol
   * @see #symbol
   */
  @Override
  public String getSymbol() {
    return symbol;
  }

  /**
   * @param symbol the symbol to set
   * @see #symbol
   */
  @Override
  public void setSymbol(final String symbol) {
    this.symbol = symbol;
  }

  /**
   * @return the synonyms
   * @see #synonyms
   */
  @Override
  public List<String> getSynonyms() {
    return synonyms;
  }

  /**
   * @return the formula
   * @see #formula
   */
  @Override
  public String getFormula() {
    return formula;
  }

  /**
   * @param synonyms the synonyms to set
   * @see #synonyms
   */
  @Override
  public void setSynonyms(final List<String> synonyms) {
    this.synonyms = synonyms;
  }

  /**
   * @param formula the formula to set
   * @see #formula
   */
  @Override
  public void setFormula(final String formula) {
    this.formula = formula;
  }

  /**
   * @return the abbreviation
   * @see #abbreviation
   */
  @Override
  public String getAbbreviation() {
    return abbreviation;
  }

  /**
   * @param abbreviation the abbreviation to set
   * @see #abbreviation
   */
  @Override
  public void setAbbreviation(final String abbreviation) {
    this.abbreviation = abbreviation;
  }

  /**
   * Adds synonyms to the element.
   *
   * @param synonyms list of synonyms to be added
   */
  public void addSynonyms(final List<String> synonyms) {
    for (final String string : synonyms) {
      if (string.length() > MAX_SYNONYM_LENGTH) {
        final String message = " <Synonym too long. Only " + MAX_SYNONYM_LENGTH + " characters are allowed>";
        this.getSynonyms().add(string.substring(0, MAX_SYNONYM_LENGTH - message.length()) + message);
      } else {
        this.getSynonyms().add(string);
      }
    }

  }

  /**
   * Adds former symbol to the object.
   *
   * @param formerSymbol former symbol to add
   */
  public void addFormerSymbol(final String formerSymbol) {
    formerSymbols.add(formerSymbol);
  }

  /**
   * Adds synonym to object.
   *
   * @param synonym synonym to be added
   */
  public void addSynonym(final String synonym) {
    synonyms.add(synonym);
  }

  public Color getFontColor() {
    return fontColor;
  }

  public void setFontColor(final Color fontColor) {
    this.fontColor = fontColor;
  }

  @Override
  public Integer getZ() {
    return z;
  }

  @Override
  public void setZ(final Integer z) {
    this.z = z;
  }

  @Override
  public String getElementId() {
    return elementId;
  }

  /**
   * Returns size of the element in square units.
   *
   * @return size of the element
   */
  @Override
  @JsonIgnore
  public double getSize() {
    return getWidth() * getHeight();
  }

  /**
   * @param elementId the elementId to set
   * @see #elementId
   */
  public void setElementId(final String elementId) {
    this.elementId = elementId;
  }

  public Glyph getGlyph() {
    return glyph;
  }

  public void setGlyph(final Glyph glyph) {
    this.glyph = glyph;
  }

  @Override
  public Color getBorderColor() {
    return borderColor;
  }

  @Override
  public void setBorderColor(final Color borderColor) {
    this.borderColor = borderColor;
  }

  public HorizontalAlign getNameHorizontalAlign() {
    return nameHorizontalAlign;
  }

  public void setNameHorizontalAlign(final HorizontalAlign nameHorizontalAlign) {
    this.nameHorizontalAlign = nameHorizontalAlign;
  }

  public VerticalAlign getNameVerticalAlign() {
    return nameVerticalAlign;
  }

  public void setNameVerticalAlign(final VerticalAlign nameVerticalAlign) {
    this.nameVerticalAlign = nameVerticalAlign;
  }

  public Double getNameX() {
    return nameX;
  }

  public void setNameX(final double nameX) {
    this.nameX = nameX;
  }

  public Double getNameY() {
    return nameY;
  }

  public void setNameY(final double nameY) {
    this.nameY = nameY;
  }

  public Double getNameWidth() {
    return nameWidth;
  }

  public void setNameWidth(final double nameWidth) {
    this.nameWidth = nameWidth;
  }

  public Double getNameHeight() {
    return nameHeight;
  }

  public void setNameHeight(final double nameHeight) {
    this.nameHeight = nameHeight;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }

  public void setImmediateLink(final String immediateLink) {
    this.immediateLink = immediateLink;
  }

  public String getImmediateLink() {
    return immediateLink;
  }
}