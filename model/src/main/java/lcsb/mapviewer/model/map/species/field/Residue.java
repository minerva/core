package lcsb.mapviewer.model.map.species.field;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.sbo.SBOTermModificationResidueType;
import lcsb.mapviewer.model.map.species.Protein;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This structure contains information about Residue for one of {@link Protein}.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("RESIDUE")
public class Residue extends AbstractSiteModification {

  public static final double DEFAULT_SIZE = 15;

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  public Residue() {
    super();
    this.setWidth(DEFAULT_SIZE);
    this.setHeight(DEFAULT_SIZE);
    this.setNameWidth(DEFAULT_SIZE);
    this.setNameHeight(DEFAULT_SIZE);
  }

  public Residue(final Residue residue) {
    super(residue);
  }

  public Residue(final String modificationId) {
    super(modificationId);
    this.setWidth(DEFAULT_SIZE);
    this.setHeight(DEFAULT_SIZE);
    this.setNameWidth(DEFAULT_SIZE);
    this.setNameHeight(DEFAULT_SIZE);
  }

  /**
   * Creates copy of the object.
   *
   * @return copy of the object.
   */
  @Override
  public Residue copy() {
    if (this.getClass() == Residue.class) {
      return new Residue(this);
    } else {
      throw new NotImplementedException("Method copy() should be overridden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermModificationResidueType.RESIDUE.getSBO();
  }

}
