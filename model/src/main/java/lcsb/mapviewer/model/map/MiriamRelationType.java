package lcsb.mapviewer.model.map;

/**
 * Encodes list of biomodel qualifiers used for description in miriam
 * references. More information can be found
 * <a href="http://co.mbine.org/standards/qualifiers">here</a>.
 * 
 * @author Piotr Gawron
 * 
 */
public enum MiriamRelationType {

  /**
   * Take a look <a href="http://co.mbine.org/standards/qualifiers">here</a>.
   */
  BQ_MODEL_IS("bqmodel:is"),

  /**
   * Take a look <a href="http://co.mbine.org/standards/qualifiers">here</a>.
   */
  BQ_MODEL_IS_DESCRIBED_BY("bqmodel:isDescribedBy"),

  /**
   * Take a look <a href="http://co.mbine.org/standards/qualifiers">here</a>.
   */
  BQ_BIOL_IS("bqbiol:is"),

  /**
   * Take a look <a href="http://co.mbine.org/standards/qualifiers">here</a>.
   */
  BQ_BIOL_HAS_PART("bqbiol:hasPart"),

  /**
   * Take a look <a href="http://co.mbine.org/standards/qualifiers">here</a>.
   */
  BQ_BIOL_IS_PART_OF("bqbiol:isPartOf"),

  /**
   * Take a look <a href="http://co.mbine.org/standards/qualifiers">here</a>.
   */
  BQ_BIOL_IS_VERSION_OF("bqbiol:isVersionOf"),

  /**
   * Take a look <a href="http://co.mbine.org/standards/qualifiers">here</a>.
   */
  BQ_BIOL_HAS_VERSION("bqbiol:hasVersion"),

  /**
   * Take a look <a href="http://co.mbine.org/standards/qualifiers">here</a>.
   */
  BQ_BIOL_IS_HOMOLOG_TO("bqbiol:isHomologTo"),

  /**
   * Take a look <a href="http://co.mbine.org/standards/qualifiers">here</a>.
   */
  BQ_BIOL_IS_DESCRIBED_BY("bqbiol:isDescribedBy"),

  /**
   * Take a look <a href="http://co.mbine.org/standards/qualifiers">here</a>.
   */
  BQ_BIOL_IS_ENCODED_BY("bqbiol:isEncodedBy"),

  /**
   * Take a look <a href="http://co.mbine.org/standards/qualifiers">here</a>.
   */
  BQ_BIOL_ENCODES("bqbiol:encodes"),

  /**
   * Take a look <a href="http://co.mbine.org/standards/qualifiers">here</a>.
   */
  BQ_BIOL_OCCURES("bqbiol:occures"),

  BQ_BIOL_HAS_TAXON("bqbiol:hasTaxon"),

  BQ_BIOL_IS_PROPERTY_OF("bqbiol:isPropertyOf"),
  
  BQ_BIOL_HAS_PROPERTY("bqbiol:hasProperty"),
  
  BQ_BIOL_OCCURS_IN("bqbiol:occursIn"),
  
  BQ_MODEL_IS_DERIVED_FROM("bqmodel:isDerivedFrom"),

  BQ_MODEL_IS_INSTANCE_OF("bqmodel:isInstanceOf"),
  
  BQ_MODEL_HAS_INSTANCE("bqmodel:hasInstance"),
  ;

  /**
   * String representing relation. List of all possibilities can be found
   * <a href="http://co.mbine.org/standards/qualifiers">here</a>.
   */
  private String stringRepresentation;

  /**
   * Default constructor.
   * 
   * @param stringRepresentation
   *          {@link #stringRepresentation}
   */
  MiriamRelationType(final String stringRepresentation) {
    this.stringRepresentation = stringRepresentation;
  }

  /**
   * Returns {@link MiriamRelationType} associated with #stringRepresentation.
   *
   * @param string
   *          #stringRepresentation
   * @return {@link MiriamRelationType} for given #stringRepresentation
   */
  public static MiriamRelationType getTypeByStringRepresentation(final String string) {
    for (final MiriamRelationType mrt : MiriamRelationType.values()) {
      if (mrt.getStringRepresentation().equalsIgnoreCase(string)) {
        return mrt;
      }
    }
    return null;
  }

  /**
   * @return the stringRepresentation
   * @see #stringRepresentation
   */
  public String getStringRepresentation() {
    return stringRepresentation;
  }
}
