package lcsb.mapviewer.model.map.reaction.type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines a standard CellDesigner dissociation. It must have at
 * least one reactant and two products.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("DISSOCIATION_REACTION")
public class DissociationReaction extends Reaction implements TwoProductReactionInterface {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  protected DissociationReaction() {
    super();
  }

  public DissociationReaction(final String elementId) {
    super(elementId);
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   *
   * @param result parent reaction from which we copy data
   */
  public DissociationReaction(final Reaction result) {
    super(result);
    if (result.getProducts().size() < 2) {
      throw new InvalidArgumentException(
          "Reaction cannot be transformed to dissociation: number of products must be greater than 1");
    }
  }

  @Override
  public String getStringType() {
    return "Dissociation";
  }

  @Override
  @JsonIgnore
  public ReactionRect getReactionRect() {
    return ReactionRect.RECT_EMPTY;
  }

  @Override
  public DissociationReaction copy() {
    if (this.getClass() == DissociationReaction.class) {
      return new DissociationReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermReactionType.DISSOCIATION.getSBO();
  }

}
