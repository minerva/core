package lcsb.mapviewer.model.map.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.ElementComparator;

/**
 * This class implements comparator interface for
 * {@link ElementSubmodelConnection}.
 * 
 * @author Piotr Gawron
 * 
 */
public class ElementSubmodelConnectionComparator extends Comparator<ElementSubmodelConnection> {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public ElementSubmodelConnectionComparator(final double epsilon) {
    super(ElementSubmodelConnection.class);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public ElementSubmodelConnectionComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected Comparator<?> getParentComparator() {
    return new SubmodelConnectionComparator(epsilon);
  }

  @Override
  protected int internalCompare(final ElementSubmodelConnection arg0, final ElementSubmodelConnection arg1) {
    ElementComparator elementComparator = new ElementComparator(epsilon);
    Element element1 = arg0.getFromElement();
    Element element2 = arg1.getFromElement();
    arg0.setFromElement(null);
    arg1.setFromElement(null);
    int status = elementComparator.compare(element1, element2);
    arg0.setFromElement(element1);
    arg1.setFromElement(element2);
    if (status != 0) {
      logger.debug("from element different: " + arg0.getFromElement() + ", " + arg1.getFromElement());
      return status;
    }
    element1 = arg0.getToElement();
    element2 = arg1.getToElement();
    arg0.setToElement(null);
    arg1.setToElement(null);
    status = elementComparator.compare(element1, element2);
    arg0.setToElement(element1);
    arg1.setToElement(element2);
    if (status != 0) {
      logger.debug("to element different: " + arg0.getToElement() + ", " + arg1.getToElement());
      return status;
    }

    return 0;
  }

}
