package lcsb.mapviewer.model.map.species;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Entity representing chemical element on the map.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@DiscriminatorValue("CHEMICAL")
public abstract class Chemical extends Species {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * <a href=
   * "http://en.wikipedia.org/wiki/Simplified_molecular-input_line-entry_system"
   * >Smiles</a> parameter for the chemical.
   */
  @Column(columnDefinition = "TEXT")
  @JsonIgnore
  private String smiles;

  /**
   * Empty constructor required by hibernate.
   */
  Chemical() {
  }

  /**
   * Constructor that creates a copy of the element given in the parameter.
   * 
   * @param original
   *          original object that will be used for creating copy
   */
  public Chemical(final Chemical original) {
    super(original);
    smiles = original.getSmiles();
  }

  /**
   * @return the smiles
   * @see #smiles
   */
  public String getSmiles() {
    return smiles;
  }

  /**
   * @param smiles
   *          the smiles to set
   * @see #smiles
   */
  public void setSmiles(final String smiles) {
    this.smiles = smiles;
  }

}
