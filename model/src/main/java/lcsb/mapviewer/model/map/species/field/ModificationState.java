package lcsb.mapviewer.model.map.species.field;

/**
 * Defines a type of modification (in protein or rna). Possible values are:
 * <ul>
 * <li>{@link ModificationState#ACETYLATED ACETYLATED},</li>
 * <li>{@link ModificationState#DONT_CARE DON'T CARE},</li>
 * <li>{@link ModificationState#EMPTY EMPTY},</li>
 * <li>{@link ModificationState#GLYCOSYLATED GLYCOSYLATED},</li>
 * <li>{@link ModificationState#HYDROXYLATED HYDROXYLATED},</li>
 * <li>{@link ModificationState#METHYLATED METHYLATED},</li>
 * <li>{@link ModificationState#MYRISTOYLATED MYRISTOYLATED},</li>
 * <li>{@link ModificationState#PALMYTOYLATED PALMYTOYLATED},</li>
 * <li>{@link ModificationState#PHOSPHORYLATED PHOSPHORYLATED},</li>
 * <li>{@link ModificationState#PRENYLATED PRENYLATED},</li>
 * <li>{@link ModificationState#PROTONATED PROTONATED},</li>
 * <li>{@link ModificationState#SULFATED SULFATED},</li>
 * <li>{@link ModificationState#UBIQUITINATED UBIQUITINATED},</li>
 * <li>{@link ModificationState#UNKNOWN UNKNOWN}.</li>
 * </ul>
 *
 * @author Piotr Gawron
 */
public enum ModificationState {

  /**
   * Phosphorylated state.
   */
  PHOSPHORYLATED("phosphorylated", "P"),

  /**
   * Acetylated state.
   */
  ACETYLATED("acetylated", "Ac"),

  /**
   * Ubiquitinated state.
   */
  UBIQUITINATED("ubiquitinated", "Ub"),

  /**
   * Methylated state.
   */
  METHYLATED("methylated", "Me"),

  /**
   * Hydroxylated state.
   */
  HYDROXYLATED("hydroxylated", "OH"),

  /**
   * Myristoylated state.
   */
  MYRISTOYLATED("myristoylated", "My"),

  /**
   * Sulfated state.
   */
  SULFATED("sulfated", "S"),

  /**
   * Prenylated state.
   */
  PRENYLATED("prenylated", "Pr"),

  /**
   * Glycosylated state.
   */
  GLYCOSYLATED("glycosylated", "G"),

  /**
   * Palmytoylated state.
   */
  PALMYTOYLATED("palmytoylated", "Pa"),

  /**
   * Unknown state.
   */
  UNKNOWN("unknown", "?"),

  /**
   * Empty state.
   */
  EMPTY("empty", ""),

  /**
   * Protonated state.
   */
  PROTONATED("protonated", "H"),

  /**
   * We don't care in which state it is.
   */
  DONT_CARE("unspecified", "*");

  /**
   * Full name of the modification.
   */

  private final String fullName;
  /**
   * Abbreviation used for the modification.
   */
  private final String abbreviation;

  /**
   * Default constructor with the name and abbreviation.
   *
   * @param name         name used for this state
   * @param abbreviation abbreviation used in this state
   */
  ModificationState(final String name, final String abbreviation) {
    this.fullName = name;
    this.abbreviation = abbreviation;
  }

  /**
   * @return the fullName
   * @see #fullName
   */
  public String getFullName() {
    return fullName;
  }

  /**
   * @return the abbreviation
   * @see #abbreviation
   */
  public String getAbbreviation() {
    return abbreviation;
  }

}
