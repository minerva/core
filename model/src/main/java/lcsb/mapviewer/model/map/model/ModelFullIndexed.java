package lcsb.mapviewer.model.map.model;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.layout.BlockDiagram;
import lcsb.mapviewer.model.map.layout.ElementGroup;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationResidue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class implements {@link Model} interface. It's is very simple
 * implementation containing structures that index the data from
 * {@link ModelData} structure and provide access method to this indexed data.
 *
 * @author Piotr Gawron
 */
public class ModelFullIndexed implements Model {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static final Logger logger = LogManager.getLogger();

  /**
   * Object that map {@link Element#elementId element identifier} into
   * {@link Element}.
   */
  private final Map<String, Element> elementByElementId = new HashMap<>();

  /**
   * Object that map {@link Element#id element database identifier} into
   * {@link Element}.
   */
  private final Map<Integer, Element> elementByDbId = new HashMap<>();

  /**
   * Object that map {@link Reaction#idReaction reaction identifier} into
   * {@link Reaction}.
   */
  private final Map<String, Reaction> reactionByReactionId = new HashMap<>();

  /**
   * Object that map {@link Reaction#id reaction database identifier} into
   * {@link Reaction}.
   */
  private final Map<Integer, Reaction> reactionByDbId = new HashMap<>();

  /**
   * {@link ModelData} object containing "raw" data about the model.
   */
  private ModelData modelData = null;

  /**
   * Default constructor.
   *
   * @param model {@link ModelData} object containing "raw" data about the model
   */
  public ModelFullIndexed(final ModelData model) {
    if (model == null) {
      this.modelData = new ModelData();
    } else {
      this.modelData = model;
      for (final Element element : model.getElements()) {
        elementByElementId.put(element.getElementId(), element);
        elementByDbId.put(element.getId(), element);
      }
      for (final Reaction reaction : model.getReactions()) {
        reactionByReactionId.put(reaction.getIdReaction(), reaction);
        reactionByDbId.put(reaction.getId(), reaction);
      }
      if (getProject() != null) {
        getProject().getProjectId();
      }

      for (final ModelSubmodelConnection connection : model.getSubmodels()) {
        if (connection.getSubmodel().getModel() == null) {
          connection.getSubmodel().setModel(new ModelFullIndexed(connection.getSubmodel()));
        }
      }
    }
    modelData.setModel(this);
  }

  @Override
  public void addElement(final Element element) {
    if (element.getElementId() == null || element.getElementId().isEmpty()) {
      throw new InvalidArgumentException("Element identifier cannot be empty");
    }
    Element element2 = elementByElementId.get(element.getElementId());
    if (element2 == null) {
      modelData.addElement(element);
      elementByElementId.put(element.getElementId(), element);
      elementByDbId.put(element.getId(), element);
    } else {
      throw new InvalidArgumentException("Element with duplicated id: " + element.getElementId());
    }
  }

  @Override
  public double getWidth() {
    return modelData.getWidth();
  }

  @Override
  public void setWidth(final double width) {
    modelData.setWidth(width);
  }

  @Override
  public void setWidth(final String text) {
    modelData.setWidth(Double.parseDouble(text));
  }

  @Override
  public void setWidth(final int width) {
    setWidth(Double.valueOf(width));
  }

  @Override
  public double getHeight() {
    return modelData.getHeight();
  }

  @Override
  public void setHeight(final double height) {
    modelData.setHeight(height);
  }

  @Override
  public void setHeight(final String text) {
    modelData.setHeight(Double.parseDouble(text));
  }

  @Override
  public void setHeight(final int height) {
    setHeight(Double.valueOf(height));
  }

  @Override
  public Set<Element> getElements() {
    return modelData.getElements();
  }

  @Override
  public void setElements(final Set<Element> elements) {
    this.modelData.setElements(elements);
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T extends Element> T getElementByElementId(final String elementId) {
    return (T) elementByElementId.get(elementId);
  }

  @Override
  public void addReaction(final Reaction reaction) {
    modelData.addReaction(reaction);
    if (reactionByReactionId.get(reaction.getIdReaction()) != null) {
      throw new InvalidArgumentException("Reaction with id already exists: " + reaction.getElementId());
    }
    reactionByReactionId.put(reaction.getIdReaction(), reaction);
    reactionByDbId.put(reaction.getId(), reaction);
  }

  @Override
  public Set<Reaction> getReactions() {
    return modelData.getReactions();
  }

  @Override
  public List<Compartment> getCompartments() {
    List<Compartment> result = new ArrayList<>();
    for (final Element element : modelData.getElements()) {
      if (element instanceof Compartment) {
        result.add((Compartment) element);
      }
    }
    result.sort(new ElementByIdComparator());
    return result;
  }

  @Override
  public void addLayer(final Layer layer) {
    modelData.addLayer(layer);
  }

  @Override
  public Set<Layer> getLayers() {
    return modelData.getLayers();
  }

  @Override
  public void addElements(final Collection<? extends Element> elements) {
    for (final Element element : elements) {
      addElement(element);
    }
  }

  @Override
  public String getNotes() {
    return modelData.getNotes();
  }

  @Override
  public void setNotes(final String notes) {
    if (notes != null && notes.contains("<html")) {
      throw new InvalidArgumentException("notes cannot contain <html> tag");
    }
    modelData.setNotes(notes);
  }

  @Override
  public Reaction getReactionByReactionId(final String idReaction) {
    return reactionByReactionId.get(idReaction);
  }

  @Override
  public void addLayers(final Collection<Layer> layers) {
    for (final Layer layer : layers) {
      addLayer(layer);
    }
  }

  @Override
  public void addElementGroup(final ElementGroup elementGroup) {
    modelData.addElementGroup(elementGroup);
  }

  @Override
  public void addBlockDiagream(final BlockDiagram blockDiagram) {
    modelData.addBlockDiagream(blockDiagram);
  }

  @Override
  public String getIdModel() {
    return modelData.getIdModel();
  }

  @Override
  public void setIdModel(final String idModel) {
    this.modelData.setIdModel(idModel);
  }

  @Override
  public int getTileSize() {
    return modelData.getTileSize();
  }

  @Override
  public void setTileSize(final int tileSize) {
    this.modelData.setTileSize(tileSize);
  }

  @Override
  public int getZoomLevels() {
    return modelData.getZoomLevels();
  }

  @Override
  public void setZoomLevels(final int zoomLevels) {
    this.modelData.setZoomLevels(zoomLevels);
  }

  @Override
  public void removeReaction(final Reaction reaction) {
    modelData.removeReaction(reaction);
    reactionByReactionId.remove(reaction.getIdReaction());
    reactionByDbId.remove(reaction.getId());
  }

  @Override
  public void removeElement(final Element element) {
    modelData.removeElement(element);
    elementByElementId.remove(element.getElementId());
    elementByDbId.remove(element.getId());

    if (element.getCompartment() != null) {
      Compartment ca = element.getCompartment();
      ca.removeElement(element);
    }

    if (element instanceof Species) {
      Species al = (Species) element;
      if (al.getComplex() != null) {
        Complex ca = ((Species) element).getComplex();
        ca.removeElement(al);
      }
    }
  }

  @Override
  public Collection<Species> getNotComplexSpeciesList() {
    List<Species> result = new ArrayList<>();
    for (final Element element : modelData.getElements()) {
      if (element instanceof Species && !(element instanceof Complex)) {
        result.add((Species) element);
      }
    }
    result.sort(new ElementByIdComparator());
    return result;
  }

  @Override
  public List<Species> getSpeciesList() {
    List<Species> result = new ArrayList<>();
    for (final Element element : modelData.getElements()) {
      if (element instanceof Species) {
        result.add((Species) element);
      }
    }
    result.sort(new ElementByIdComparator());
    return result;
  }

  @Override
  public Collection<Complex> getComplexList() {
    List<Complex> result = new ArrayList<>();
    for (final Element element : modelData.getElements()) {
      if (element instanceof Complex) {
        result.add((Complex) element);
      }
    }
    result.sort(new ElementByIdComparator());
    return result;
  }

  @Override
  public void addReactions(final List<Reaction> reactions2) {
    for (final Reaction reaction : reactions2) {
      addReaction(reaction);
    }
  }

  @Override
  public List<Element> getElementsByName(final String name) {
    List<Element> result = new ArrayList<>();
    for (final Element element : getElements()) {
      if (element.getName().equalsIgnoreCase(name)) {
        result.add(element);
      }
    }
    return result;
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T extends Element> T getElementByDbId(final Integer dbId) {
    return (T) elementByDbId.get(dbId);
  }

  @Override
  public Reaction getReactionByDbId(final Integer dbId) {
    return reactionByDbId.get(dbId);
  }

  @Override
  public List<Compartment> getSortedCompartments() {
    List<Compartment> result = getCompartments();
    Collections.sort(result, Element.SIZE_COMPARATOR);
    return result;
  }

  @Override
  public List<Element> getElementsSortedBySize() {
    List<Element> sortedElements = new ArrayList<>();
    sortedElements.addAll(getElements());
    Collections.sort(sortedElements, Element.SIZE_COMPARATOR);
    return sortedElements;
  }

  @Override
  public Project getProject() {
    return modelData.getProject();
  }

  @Override
  public void setProject(final Project project) {
    modelData.setProject(project);
  }

  @Override
  public ModelData getModelData() {
    return modelData;
  }

  @Override
  public int getId() {
    return modelData.getId();
  }

  @Override
  public void setId(final int id) {
    modelData.setId(id);
  }

  @Override
  public void addSubmodelConnection(final ModelSubmodelConnection submodel) {
    modelData.addSubmodel(submodel);
    submodel.setParentModel(this);
  }

  @Override
  public Collection<ModelSubmodelConnection> getSubmodelConnections() {
    return modelData.getSubmodels();
  }

  @Override
  public String getName() {
    return modelData.getName();
  }

  @Override
  public void setName(final String name) {
    modelData.setName(name);
  }

  @Override
  public Model getSubmodelById(final Integer id) {
    if (id == null) {
      return null;
    }
    if (id.equals(getId())) {
      return this;
    }
    SubmodelConnection connection = getSubmodelConnectionById(id);
    if (connection != null) {
      return connection.getSubmodel().getModel();
    } else {
      return null;
    }
  }

  @Override
  public Model getSubmodelById(final String identifier) {
    if (identifier == null) {
      return null;
    }
    Integer id = Integer.parseInt(identifier);
    return getSubmodelById(id);
  }

  @Override
  public Collection<SubmodelConnection> getParentModels() {
    return modelData.getParentModels();
  }

  @Override
  public Model getSubmodelByConnectionName(final String name) {
    if (name == null) {
      return null;
    }
    for (final ModelSubmodelConnection connection : getSubmodelConnections()) {
      if (name.equals(connection.getName())) {
        return connection.getSubmodel().getModel();
      }
    }
    return null;
  }

  @Override
  public SubmodelConnection getSubmodelConnectionById(final Integer id) {
    for (final ModelSubmodelConnection connection : getSubmodelConnections()) {
      if (id.equals(connection.getSubmodel().getId())) {
        return connection;
      }
    }
    return null;
  }

  @Override
  public Collection<Model> getSubmodels() {
    List<Model> models = new ArrayList<Model>();
    for (final ModelSubmodelConnection connection : getSubmodelConnections()) {
      models.add(connection.getSubmodel().getModel());
    }
    return models;
  }

  @Override
  public Model getSubmodelByName(final String name) {
    if (name == null) {
      return null;
    }
    for (final ModelSubmodelConnection connection : getSubmodelConnections()) {
      if (name.equals(connection.getSubmodel().getName())) {
        return connection.getSubmodel().getModel();
      }
    }
    if (name.equals(getName())) {
      return this;
    }
    return null;
  }

  @Override
  public List<BioEntity> getBioEntities() {
    List<BioEntity> result = new ArrayList<>();
    result.addAll(getElements());
    result.addAll(getReactions());
    return result;
  }

  @Override
  public Double getDefaultCenterX() {
    return modelData.getDefaultCenterX();
  }

  @Override
  public void setDefaultCenterX(final Double defaultCenterX) {
    modelData.setDefaultCenterX(defaultCenterX);
  }

  @Override
  public Double getDefaultCenterY() {
    return modelData.getDefaultCenterY();
  }

  @Override
  public void setDefaultCenterY(final Double defaultCenterY) {
    modelData.setDefaultCenterY(defaultCenterY);
  }

  @Override
  public Integer getDefaultZoomLevel() {
    return modelData.getDefaultZoomLevel();
  }

  @Override
  public void setDefaultZoomLevel(final Integer defaultZoomLevel) {
    modelData.setDefaultZoomLevel(defaultZoomLevel);
  }

  @Override
  public Set<SbmlFunction> getFunctions() {
    return modelData.getFunctions();
  }

  @Override
  public void addFunctions(final Collection<SbmlFunction> functions) {
    modelData.addFunctions(functions);
  }

  @Override
  public Set<SbmlParameter> getParameters() {
    return modelData.getParameters();
  }

  @Override
  public void addUnits(final Collection<SbmlUnit> units) {
    modelData.addUnits(units);
  }

  @Override
  public Set<SbmlUnit> getUnits() {
    return modelData.getUnits();
  }

  @Override
  public void addUnit(final SbmlUnit unit) {
    modelData.addUnit(unit);

  }

  @Override
  public SbmlUnit getUnitsByUnitId(final String unitId) {
    for (final SbmlUnit unit : getUnits()) {
      if (unit.getUnitId().equals(unitId)) {
        return unit;
      }
    }
    return null;
  }

  @Override
  public void addParameters(final Collection<SbmlParameter> sbmlParameters) {
    modelData.addParameters(sbmlParameters);
  }

  @Override
  public void addParameter(final SbmlParameter sbmlParameter) {
    modelData.addParameter(sbmlParameter);
  }

  @Override
  public SbmlParameter getParameterById(final String parameterId) {
    for (final SbmlParameter parameter : getParameters()) {
      if (parameter.getParameterId().equals(parameterId)) {
        return parameter;
      }
    }
    return null;
  }

  @Override
  public void addFunction(final SbmlFunction sbmlFunction) {
    modelData.addFunction(sbmlFunction);
  }

  @Override
  public SbmlFunction getFunctionById(final String functionId) {
    for (final SbmlFunction function : getFunctions()) {
      if (function.getFunctionId().equals(functionId)) {
        return function;
      }
    }
    return null;
  }

  @Override
  public void removeReactions(final Collection<Reaction> reactions) {
    Set<Reaction> reactionsToRemove = new HashSet<>();
    reactionsToRemove.addAll(reactions);
    for (final Reaction reaction : reactionsToRemove) {
      removeReaction(reaction);
    }
  }

  @Override
  public Set<MiriamData> getMiriamData() {
    return modelData.getMiriamData();
  }

  @Override
  public void addMiriamData(final MiriamData md) {
    modelData.addMiriamData(md);
  }

  @Override
  public void addMiriamData(final Collection<MiriamData> miriamData) {
    for (final MiriamData md : miriamData) {
      addMiriamData(md);
    }
  }

  @Override
  public void addAuthor(final Author author) {
    modelData.addAuthor(author);
  }

  @Override
  public void addAuthors(final Collection<Author> authors) {
    for (final Author author : authors) {
      addAuthor(author);
    }
  }

  @Override
  public List<Author> getAuthors() {
    return modelData.getAuthors();
  }

  @Override
  public Calendar getCreationDate() {
    return modelData.getCreationDate();
  }

  @Override
  public void setCreationDate(final Calendar creationDate) {
    modelData.setCreationDate(creationDate);
  }

  @Override
  public void addModificationDate(final Calendar modificationDate) {
    modelData.addModificationDate(modificationDate);
  }

  @Override
  public List<Calendar> getModificationDates() {
    return modelData.getModificationDates();
  }

  @Override
  public void addModificationDates(final Collection<Calendar> modificationDates) {
    for (final Calendar calendar : modificationDates) {
      addModificationDate(calendar);
    }
  }

  @Override
  public void removeBioEntity(final BioEntity bioEntity) {
    if (bioEntity instanceof Reaction) {
      removeReaction((Reaction) bioEntity);
    } else if (bioEntity instanceof Element) {
      removeElement((Element) bioEntity);
    } else {
      throw new InvalidArgumentException("Unknown class type:" + bioEntity);
    }
  }

  @Override
  public Set<Drawable> getDrawables() {
    return getDrawables(false);
  }

  @Override
  public Set<Drawable> getDrawables(final boolean onlyVisible) {
    Set<Drawable> result = new HashSet<>();
    result.addAll(getBioEntities());
    for (Species species : getSpeciesList()) {
      if (species instanceof SpeciesWithModificationResidue) {
        result.addAll(((SpeciesWithModificationResidue) species).getModificationResidues());
      }
    }
    for (final Layer layer : getLayers()) {
      if (layer.isVisible() || !onlyVisible) {
        result.addAll(layer.getDrawables());
      }
    }
    return result;
  }

  @Override
  public Collection<Element> getSortedElements() {
    List<Element> result = new ArrayList<>(getElements());
    result.sort(new ElementByIdComparator());
    return result;
  }
}
