package lcsb.mapviewer.model.map.layout;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.cache.BigFileEntry;
import lcsb.mapviewer.modelutils.serializer.model.map.layout.ReferenceGenomeGeneMappingSerializer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

/**
 * This object defines information about mapping genes to gene. This data is
 * stored in external file (from some database, server, etc.). For now this file
 * should be in
 * <a href="https://genome.ucsc.edu/goldenpath/help/bigBed.html">big bed
 * format</a>.
 *
 * @author Piotr Gawron
 */
@Entity
@JsonSerialize(using = ReferenceGenomeGeneMappingSerializer.class)
public class ReferenceGenomeGeneMapping implements MinervaEntity {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique local database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * {@link ReferenceGenome Reference genome} for which gene mapping is
   * provided.
   */
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  private ReferenceGenome referenceGenome;

  @ManyToOne(optional = true)
  private BigFileEntry file;

  /**
   * Name of the mapping.
   */
  @Column(nullable = false, length = 255)
  private String name;

  /**
   * Url to source file which provides mapping.
   */
  @Column(nullable = false, columnDefinition = "TEXT")
  private String sourceUrl;

  /**
   * What is the progress of obtaining file.
   */
  private double downloadProgress;

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * @return the id
   * @see #id
   */
  @Override
  public int getId() {
    return id;
  }

  /**
   * @param id the id to set
   * @see #id
   */
  public void setId(final int id) {
    this.id = id;
  }

  /**
   * @return the referenceGenome
   * @see #referenceGenome
   */
  public ReferenceGenome getReferenceGenome() {
    return referenceGenome;
  }

  /**
   * @param referenceGenome the referenceGenome to set
   * @see #referenceGenome
   */
  public void setReferenceGenome(final ReferenceGenome referenceGenome) {
    this.referenceGenome = referenceGenome;
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   * @see #name
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * @return the sourceUrl
   * @see #sourceUrl
   */
  public String getSourceUrl() {
    return sourceUrl;
  }

  /**
   * @param sourceUrl the sourceUrl to set
   * @see #sourceUrl
   */
  public void setSourceUrl(final String sourceUrl) {
    this.sourceUrl = sourceUrl;
  }

  /**
   * @return the downloadProgress
   * @see #downloadProgress
   */
  public double getDownloadProgress() {
    return downloadProgress;
  }

  /**
   * @param downloadProgress the downloadProgress to set
   * @see #downloadProgress
   */
  public void setDownloadProgress(final double downloadProgress) {
    this.downloadProgress = downloadProgress;
  }

  public BigFileEntry getFile() {
    return file;
  }

  public void setFile(final BigFileEntry file) {
    this.file = file;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
