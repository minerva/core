package lcsb.mapviewer.model.map;

import lcsb.mapviewer.common.comparator.IntegerComparator;

import java.awt.Color;
import java.util.Comparator;

public interface Drawable {

  Comparator<? super Drawable> Z_INDEX_COMPARATOR = new Comparator<Drawable>() {
    private final IntegerComparator integerComparator = new IntegerComparator();

    @Override
    public int compare(final Drawable o1, final Drawable o2) {
      final int id1 = o1.getZ();
      final int id2 = o2.getZ();
      return integerComparator.compare(id1, id2);
    }
  };

  /**
   * Returns z-index of the graphical representation.
   *
   * @return z-index of the graphical representation
   */
  Integer getZ();

  /**
   * Sets z-index of the graphical representation.
   *
   * @param z z-index of the graphical representation
   */
  void setZ(final Integer z);

  String getElementId();

  double getSize();

  Color getBorderColor();

  void setBorderColor(Color color);

  Color getFillColor();

  void setFillColor(Color color);

}
