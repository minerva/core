package lcsb.mapviewer.model.map.species;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.sbo.SBOTermSpeciesType;

/**
 * Entity representing ion channel protein element on the map.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@DiscriminatorValue("ION_CHANNEL_PROTEIN")
public class IonChannelProtein extends Protein {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Empty constructor required by hibernate.
   */
  IonChannelProtein() {
  }

  /**
   * Constructor that creates a copy of the element given in the parameter.
   * 
   * @param original
   *          original object that will be used for creating copy
   */
  public IonChannelProtein(final IonChannelProtein original) {
    super(original);
  }

  /**
   * Default constructor.
   * 
   * @param elementId
   *          unique (within model) element identifier
   */
  public IonChannelProtein(final String elementId) {
    setElementId(elementId);
  }

  @Override
  public IonChannelProtein copy() {
    if (this.getClass() == IonChannelProtein.class) {
      return new IonChannelProtein(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermSpeciesType.ION_CHANNEL.getSBO();
  }

}
