package lcsb.mapviewer.model.map.species;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;

/**
 * Comparator class used for comparing {@link GenericProtein} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class GenericProteinComparator extends Comparator<GenericProtein> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public GenericProteinComparator(final double epsilon) {
    super(GenericProtein.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public GenericProteinComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected Comparator<?> getParentComparator() {
    return new ProteinComparator(epsilon);
  }

  @Override
  protected int internalCompare(final GenericProtein arg0, final GenericProtein arg1) {
    return 0;
  }
}
