package lcsb.mapviewer.model.map.species;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.SetComparator;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.Residue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

/**
 * Comparator class used for comparing {@link Protein} objects.
 *
 * @author Piotr Gawron
 */
public class ProteinComparator extends Comparator<Protein> {

  /**
   * Default class logger.
   */
  private static final Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private final double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   *
   * @param epsilon {@link #epsilon}
   */
  public ProteinComparator(final double epsilon) {
    super(Protein.class, true);
    this.epsilon = epsilon;
    addSubClassComparator(new GenericProteinComparator(epsilon));
    addSubClassComparator(new IonChannelProteinComparator(epsilon));
    addSubClassComparator(new ReceptorProteinComparator(epsilon));
    addSubClassComparator(new TruncatedProteinComparator(epsilon));
  }

  /**
   * Default constructor.
   */
  public ProteinComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected Comparator<?> getParentComparator() {
    return new SpeciesComparator(epsilon);
  }

  @Override
  protected int internalCompare(final Protein arg0, final Protein arg1) {
    final SetComparator<String> stringSetComparator = new SetComparator<>(new StringComparator());

    final Set<String> set1 = new HashSet<>();
    final Set<String> set2 = new HashSet<>();

    for (final ModificationResidue region : arg0.getModificationResidues()) {
      if (considerResidueForComparison(region)) {
        set1.add(region.toString());
      }
    }

    for (final ModificationResidue region : arg1.getModificationResidues()) {
      if (considerResidueForComparison(region)) {
        set2.add(region.toString());
      }
    }

    if (stringSetComparator.compare(set1, set2) != 0) {
      logger.debug(set1);
      logger.debug(set2);
      logger.debug("modification residues different");
      return stringSetComparator.compare(set1, set2);
    }

    return 0;
  }

  private boolean considerResidueForComparison(final ModificationResidue mr) {
    if (mr instanceof Residue) {
      return ((Residue) mr).getState() != null; //nulls are not represented anyway (they are leftover from import)
    }
    return true;
  }
}
