package lcsb.mapviewer.model.map.species;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;

/**
 * Comparator class used for comparing {@link Phenotype} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class PhenotypeComparator extends Comparator<Phenotype> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public PhenotypeComparator(final double epsilon) {
    super(Phenotype.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public PhenotypeComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected Comparator<?> getParentComparator() {
    return new SpeciesComparator(epsilon);
  }

  @Override
  protected int internalCompare(final Phenotype arg0, final Phenotype arg1) {
    return 0;
  }
}
