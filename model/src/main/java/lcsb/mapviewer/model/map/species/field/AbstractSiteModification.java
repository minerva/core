package lcsb.mapviewer.model.map.species.field;

import com.fasterxml.jackson.annotation.JsonProperty;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@DiscriminatorValue("ABSTRACT_SITE_MODIFICATION")
public abstract class AbstractSiteModification extends ModificationResidue {

  private static final long serialVersionUID = 1L;

  /**
   * State in which this modification is.
   */
  @Enumerated(EnumType.STRING)
  protected ModificationState state = null;

  public AbstractSiteModification() {
    super();
    this.setNameHorizontalAlign(HorizontalAlign.CENTER);
    this.setNameVerticalAlign(VerticalAlign.MIDDLE);
  }

  public AbstractSiteModification(final AbstractSiteModification mr) {
    super(mr);
    this.state = mr.getState();
  }

  public AbstractSiteModification(final String modificationId) {
    super(modificationId);
  }

  public ModificationState getState() {
    return state;
  }

  @JsonProperty
  public String getStateAbbreviation() {
    if (state == null) {
      return null;
    }
    return state.getAbbreviation();
  }

  public void setState(final ModificationState state) {
    this.state = state;
  }

  @Override
  public String toString() {
    return super.toString() + "," + getState();
  }
}