package lcsb.mapviewer.model.map.layout;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.cache.BigFileEntry;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.modelutils.serializer.model.map.layout.ReferenceGenomeSerializer;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Version;
import java.util.ArrayList;
import java.util.List;

/**
 * Reference genome describes data used as reference genome. This data is
 * obtained usually from external server, database. Right now we support only
 * .2bit file format.
 *
 * @author Piotr Gawron
 */
@Entity
@JsonSerialize(using = ReferenceGenomeSerializer.class)
public class ReferenceGenome implements MinervaEntity {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique local database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Organism for which the genome is provided.
   */
  @Cascade({CascadeType.ALL})
  @OneToOne(optional = false)
  private MiriamData organism;

  @ManyToOne(optional = true)
  private BigFileEntry file;

  /**
   * Type of reference genome. This describe source (database) from which
   * reference genome was taken.
   */
  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private ReferenceGenomeType type;

  /**
   * Version of the reference genome (databases have different releases, this
   * string represent specific release of data).
   */
  @Column(length = 255)
  private String version;

  /**
   * How much of the file we already downloaded.
   */
  private double downloadProgress;

  /**
   * Source url used to obtain data.
   */
  @Column(length = 255)
  private String sourceUrl;

  /**
   * List of different mappings to this genome. Reference genome is a string
   * containing nucleotides. However interpretation of these nucleotides is a
   * different thing. Many databases provides different mappings between genes
   * and genome (even for the same genome). Therefore we allow to have more gene
   * mappings.
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "referenceGenome", orphanRemoval = true)
  @OrderBy("id")
  private List<ReferenceGenomeGeneMapping> geneMapping = new ArrayList<>();

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * @return the type
   * @see #type
   */
  public ReferenceGenomeType getType() {
    return type;
  }

  /**
   * @param type the type to set
   * @see #type
   */
  public void setType(final ReferenceGenomeType type) {
    this.type = type;
  }

  /**
   * @return the organism
   * @see #organism
   */
  public MiriamData getOrganism() {
    return organism;
  }

  /**
   * @param organism the organism to set
   * @see #organism
   */
  public void setOrganism(final MiriamData organism) {
    this.organism = organism;
  }

  /**
   * @return the version
   * @see #version
   */
  public String getVersion() {
    return version;
  }

  /**
   * @param version the version to set
   * @see #version
   */
  public void setVersion(final String version) {
    this.version = version;
  }

  /**
   * @return the sourceUrl
   * @see #sourceUrl
   */
  public String getSourceUrl() {
    return sourceUrl;
  }

  /**
   * @param sourceUrl the sourceUrl to set
   * @see #sourceUrl
   */
  public void setSourceUrl(final String sourceUrl) {
    this.sourceUrl = sourceUrl;
  }

  /**
   * @return the downloadProgress
   * @see #downloadProgress
   */
  public double getDownloadProgress() {
    return downloadProgress;
  }

  /**
   * @param downloadProgress the downloadProgress to set
   * @see #downloadProgress
   */
  public void setDownloadProgress(final double downloadProgress) {
    this.downloadProgress = downloadProgress;
  }

  /**
   * @return the id
   * @see #id
   */
  @Override
  public int getId() {
    return id;
  }

  /**
   * @param id the id to set
   * @see #id
   */
  public void setId(final int id) {
    this.id = id;
  }

  /**
   * Adds {@link ReferenceGenomeGeneMapping gene mapping} to the reference
   * genome.
   *
   * @param mapping mapping to add
   */
  public void addReferenceGenomeGeneMapping(final ReferenceGenomeGeneMapping mapping) {
    geneMapping.add(mapping);
    mapping.setReferenceGenome(this);
  }

  /**
   * @return the geneMapping
   * @see #geneMapping
   */
  public List<ReferenceGenomeGeneMapping> getGeneMapping() {
    return geneMapping;
  }

  /**
   * @param geneMapping the geneMapping to set
   * @see #geneMapping
   */
  public void setGeneMapping(final List<ReferenceGenomeGeneMapping> geneMapping) {
    this.geneMapping = geneMapping;
  }

  public BigFileEntry getFile() {
    return file;
  }

  public void setFile(final BigFileEntry file) {
    this.file = file;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
