package lcsb.mapviewer.model.map.species.field;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.sbo.SBOTermModificationResidueType;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This structure contains information about Coding Region for one of the
 * following {@link Species}:
 * <ul>
 * <li>{@link Rna}</li>
 * <li>{@link AntisenseRna}</li>
 * <li>{@link Gene}</li>
 * </ul>
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("CODING_REGION")
public class CodingRegion extends AbstractRegionModification implements Serializable {

  private static final long serialVersionUID = 1L;

  public CodingRegion() {

  }

  public CodingRegion(final String id) {
    super(id);
  }

  /**
   * Constructor that initialize object with the data from the parameter.
   * 
   * @param original
   *          object from which we initialize data
   */
  public CodingRegion(final CodingRegion original) {
    super(original);
  }

  /**
   * Creates a copy of current object.
   * 
   * @return copy of the object
   */
  @Override
  public CodingRegion copy() {
    if (this.getClass() == CodingRegion.class) {
      return new CodingRegion(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }

  }

  @Override
  public String getSboTerm() {
    return SBOTermModificationResidueType.CODING_REGION.getSBO();
  }

}
