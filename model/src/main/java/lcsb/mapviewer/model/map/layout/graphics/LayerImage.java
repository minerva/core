package lcsb.mapviewer.model.map.layout.graphics;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.modelutils.serializer.id.MinervaEntityAsIdSerializer;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import java.awt.Color;
import java.awt.geom.Rectangle2D;

@Entity
public class LayerImage implements MinervaEntity, Drawable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  private double x = 0.0;

  private double y = 0.0;

  private int z;

  private double width = 0.0;

  private double height = 0.0;

  @ManyToOne(fetch = FetchType.LAZY)
  @JsonSerialize(using = MinervaEntityAsIdSerializer.class)
  private Glyph glyph = null;

  @ManyToOne(fetch = FetchType.LAZY)
  @JsonSerialize(using = MinervaEntityAsIdSerializer.class)
  private Layer layer;

  @Version
  @JsonIgnore
  private long entityVersion;

  public LayerImage(final LayerImage layerImage) {
    this.x = layerImage.x;
    this.y = layerImage.y;
    this.z = layerImage.z;
    this.width = layerImage.width;
    this.height = layerImage.height;
    this.glyph = layerImage.glyph;
  }

  public LayerImage() {

  }

  /**
   * Prepares a copy of the object.
   *
   * @return copy of LayerText
   */
  public LayerImage copy() {
    if (this.getClass() == LayerImage.class) {
      return new LayerImage(this);
    } else {
      throw new NotImplementedException("Method copy() should be overridden in class " + this.getClass());
    }
  }

  public double getX() {
    return x;
  }

  public void setX(final double x) {
    this.x = x;
  }

  public double getY() {
    return y;
  }

  public void setY(final double y) {
    this.y = y;
  }

  public double getWidth() {
    return width;
  }

  public void setWidth(final double width) {
    this.width = width;
  }

  public double getHeight() {
    return height;
  }

  public void setHeight(final double height) {
    this.height = height;
  }

  @JsonIgnore
  public Rectangle2D getBorder() {
    return new Rectangle2D.Double(x, y, width, height);
  }

  @Override
  public String toString() {
    return "[" + this.getBorder() + "]";
  }

  @Override
  public Integer getZ() {
    return z;
  }

  @Override
  public void setZ(final Integer z) {
    if (z == null) {
      this.z = 0;
    } else {
      this.z = z;
    }
  }

  @JsonIgnore
  @Override
  public String getElementId() {
    return "x=" + x + ";y=" + y + "; w=" + width + ", h=" + height;
  }

  @JsonIgnore
  @Override
  public double getSize() {
    return width * height;
  }

  @JsonIgnore
  @Override
  public Color getBorderColor() {
    return Color.GRAY;
  }

  @Override
  public void setBorderColor(final Color color) {
    throw new NotImplementedException();
  }

  @JsonIgnore
  @Override
  public Color getFillColor() {
    return Color.WHITE;
  }

  @Override
  public void setFillColor(final Color color) {
    throw new NotImplementedException();
  }

  public Glyph getGlyph() {
    return glyph;
  }

  public void setGlyph(final Glyph glyph) {
    this.glyph = glyph;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  public Layer getLayer() {
    return layer;
  }

  public void setLayer(final Layer layer) {
    this.layer = layer;
  }

  @Override
  public EntityType getEntityType() {
    return EntityType.LAYER_IMAGE;
  }

}
