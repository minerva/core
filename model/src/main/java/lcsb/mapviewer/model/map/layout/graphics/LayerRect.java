package lcsb.mapviewer.model.map.layout.graphics;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.modelutils.serializer.id.MinervaEntityAsIdSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import java.awt.Color;
import java.awt.geom.Rectangle2D;

/**
 * This class describes rectangle in the layer.
 *
 * @author Piotr Gawron
 */
@Entity
public class LayerRect implements MinervaEntity, Drawable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static final Logger logger = LogManager.getLogger();

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Color of the rectangle border.
   */
  @Type(type = "lcsb.mapviewer.persist.mapper.ColorMapper")
  @Column(nullable = false)
  private Color borderColor;

  @Type(type = "lcsb.mapviewer.persist.mapper.ColorMapper")
  @Column(nullable = false)
  private Color fillColor = Color.LIGHT_GRAY;

  /**
   * X coordinate of top left corner.
   */
  private Double x = 0.0;

  /**
   * Y coordinate of top left corner.
   */
  private Double y = 0.0;

  /**
   * Z index.
   */
  private Integer z;

  /**
   * Width of the rectangle.
   */
  private Double width = 0.0;

  /**
   * Height of the rectangle.
   */
  private Double height = 0.0;

  @Version
  @JsonIgnore
  private long entityVersion;

  @ManyToOne(fetch = FetchType.LAZY)
  @JsonSerialize(using = MinervaEntityAsIdSerializer.class)
  private Layer layer;

  /**
   * Default constructor.
   */
  public LayerRect() {

  }

  /**
   * Constructor that copies data from the parameter.
   *
   * @param layerRect from this parameter line data will be copied
   */
  public LayerRect(final LayerRect layerRect) {
    fillColor = layerRect.getFillColor();
    borderColor = layerRect.getBorderColor();
    x = layerRect.getX();
    y = layerRect.getY();
    z = layerRect.getZ();
    width = layerRect.getWidth();
    height = layerRect.getHeight();
  }

  public LayerRect(final Rectangle2D rect) {
    x = rect.getX();
    y = rect.getY();
    width = rect.getWidth();
    height = rect.getHeight();
  }

  /**
   * Prepares a copy of the object.
   *
   * @return copy of LayerRect
   */
  public LayerRect copy() {
    if (this.getClass() == LayerRect.class) {
      return new LayerRect(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * @return the x
   * @see #x
   */
  public Double getX() {
    return x;
  }

  /**
   * Set x from string containing double value.
   *
   * @param param x of the line in text format
   */
  public void setX(final String param) {
    try {
      x = Double.parseDouble(param);
    } catch (final NumberFormatException e) {
      throw new InvalidArgumentException("Invalid x value: " + param, e);
    }
  }

  /**
   * @param x the x to set
   * @see #x
   */
  public void setX(final Double x) {
    this.x = x;
  }

  /**
   * @return the y
   * @see #y
   */
  public Double getY() {
    return y;
  }

  /**
   * Set y from string containing double value.
   *
   * @param param y of the line in text format
   */
  public void setY(final String param) {
    try {
      y = Double.parseDouble(param);
    } catch (final NumberFormatException e) {
      throw new InvalidArgumentException("Invalid y value: " + param, e);
    }
  }

  /**
   * @param y the y to set
   * @see #y
   */
  public void setY(final Double y) {
    this.y = y;
  }

  /**
   * @return the width
   * @see #width
   */
  public Double getWidth() {
    return width;
  }

  /**
   * Set width from string containing double value.
   *
   * @param param width of the line in text format
   */
  public void setWidth(final String param) {
    try {
      width = Double.parseDouble(param);
    } catch (final NumberFormatException e) {
      throw new InvalidArgumentException("Invalid width value: " + param, e);
    }
  }

  /**
   * @param width the width to set
   * @see #width
   */
  public void setWidth(final Double width) {
    this.width = width;
  }

  /**
   * @return the height
   * @see #height
   */
  public Double getHeight() {
    return height;
  }

  /**
   * Set height from string containing double value.
   *
   * @param param height of the line in text format
   */
  public void setHeight(final String param) {
    try {
      height = Double.parseDouble(param);
    } catch (final NumberFormatException e) {
      throw new InvalidArgumentException("Invalid height value: " + param, e);
    }
  }

  /**
   * @param height the height to set
   * @see #height
   */
  public void setHeight(final Double height) {
    this.height = height;
  }

  @Override
  public Integer getZ() {
    return z;
  }

  @Override
  public void setZ(final Integer z) {
    this.z = z;
  }

  @Override
  public String getElementId() {
    return "x=" + x + ";y=" + y + "; w=" + width + ", h=" + height;
  }

  @Override
  public double getSize() {
    return width * height;
  }

  @Override
  public Color getBorderColor() {
    return borderColor;
  }

  @Override
  public void setBorderColor(final Color borderColor) {
    this.borderColor = borderColor;
  }

  @Override
  public Color getFillColor() {
    return fillColor;
  }

  @Override
  public void setFillColor(final Color fillColor) {
    this.fillColor = fillColor;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  public void setLayer(final Layer layer) {
    this.layer = layer;
  }

  public double getLineWidth() {
    return 1.0;
  }

  @Override
  public EntityType getEntityType() {
    return EntityType.LAYER_RECTANGLE;
  }

  public Layer getLayer() {
    return layer;
  }
}
