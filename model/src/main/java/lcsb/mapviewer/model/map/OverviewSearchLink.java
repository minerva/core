package lcsb.mapviewer.model.map;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Link used in {@link OverviewImage parent OverviewImage} to link it to
 * {@link OverviewImage child OverviewImage}.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("SEARCH_LINK")
public class OverviewSearchLink extends OverviewLink {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Results of this search query in the map will be a target for this link.
   */
  private String query;

  /**
   * Default constructor that copies object from the parameter.
   *
   * @param original original object to be copied
   */
  public OverviewSearchLink(final OverviewSearchLink original) {
    super(original);
    this.query = original.query;

  }

  /**
   * Default constructor.
   */
  public OverviewSearchLink() {
    super();
  }

  @Override
  public OverviewSearchLink copy() {
    if (this.getClass() == OverviewSearchLink.class) {
      return new OverviewSearchLink(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * @return the query
   * @see #query
   */
  public String getQuery() {
    return query;
  }

  /**
   * @param query the query to set
   * @see #query
   */
  public void setQuery(final String query) {
    this.query = query;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
