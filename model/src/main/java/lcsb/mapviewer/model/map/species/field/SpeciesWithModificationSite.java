package lcsb.mapviewer.model.map.species.field;

public interface SpeciesWithModificationSite extends SpeciesWithModificationResidue {

  /**
   * Adds a {@link ModificationSite } to the species.
   * 
   * @param modificationSite
   *          {@link ModificationSite } to add
   */
  void addModificationSite(final ModificationSite modificationSite);

}
