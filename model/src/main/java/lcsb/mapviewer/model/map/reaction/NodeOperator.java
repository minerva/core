package lcsb.mapviewer.model.map.reaction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.modelutils.serializer.id.AbstractNodeAsIdSerializer;

/**
 * One of two known types of nodes in the {@link Reaction}. It describes
 * relation between two (or more) nodes in the reaction. There are few types of
 * relations that are implemented:
 * <ul>
 * <li>{@link AndOperator} - when the output node is a result of logic "and
 * operator" between two or more input nodes</li>
 * <li>{@link AssociationOperator} - when the output node is a result of logic
 * "and operator" between two or more input nodes</li>
 * <li>{@link DissociationOperator} - when two (or more) output nodes is a
 * result of dissociation of the input</li>
 * <li>{@link NandOperator} - when the output node is a result of logic "nand
 * operator" between two or more input nodes</li>
 * <li>{@link OrOperator} - when the output node is a result of logic "or
 * operator" between two or more input nodes</li>
 * <li>{@link SplitOperator} - when two (or more) output nodes is a result of
 * spliting (dissociation???) of the input</li>
 * <li>{@link TruncationOperator} - when the output nodes are a result of
 * truncation of the input. As a result we will get truncated node and the
 * reminder part.</li>
 * <li>{@link UnknownOperator} - when the output node is a result of logic
 * operator between two or more input nodes, but the logic is unknown</li>
 * </ul>
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("GENERIC_OPERATOR_NODE")
public abstract class NodeOperator extends AbstractNode {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * List of input nodes.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "nodeOperatorForInput")
  @OrderBy("id")
  @Fetch(FetchMode.SUBSELECT)
  @JsonSerialize(contentUsing = AbstractNodeAsIdSerializer.class)
  private List<AbstractNode> inputs = new ArrayList<>();

  /**
   * List of output nodes.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "nodeOperatorForOutput")
  @OrderBy("id")
  @Fetch(FetchMode.SUBSELECT)
  @JsonSerialize(contentUsing = AbstractNodeAsIdSerializer.class)
  private List<AbstractNode> outputs = new ArrayList<>();

  /**
   * Default constructor.
   */
  public NodeOperator() {
  }

  /**
   * Creates operator using the data from the original operator in the
   * parameter.
   * 
   * @param operator
   *          original operator from which data is initialized
   */
  public NodeOperator(final NodeOperator operator) {
    super(operator);

    for (final AbstractNode node : operator.getInputs()) {
      addInput(node);
    }
    for (final AbstractNode node : operator.getOutputs()) {
      addOutput(node);
    }
  }

  /**
   * Adds input node.
   * 
   * @param node
   *          node to add
   */
  public void addInput(final AbstractNode node) {
    node.setNodeOperatorForInput(this);
    if (inputs.contains(node)) {
      throw new InvalidArgumentException("Node already on the list: " + node);
    }
    inputs.add(node);
  }

  /**
   * Adds set of inputs.
   * 
   * @param newInputs
   *          set of input nodes to add
   */
  public void addInputs(final Collection<? extends AbstractNode> newInputs) {
    for (final AbstractNode input : newInputs) {
      addInput(input);
    }
  }

  /**
   * Checks if the operator connects several inputs to one output.
   * 
   * @return <code>true</code> if the operator connects several inputs to one
   *         output, <code>false</code> otherwise
   */
  public boolean isReactantOperator() {
    for (final AbstractNode node : inputs) {
      if (node instanceof Reactant) {
        return true;
      }
      if (node instanceof NodeOperator) {
        if (((NodeOperator) node).isReactantOperator()) {
          return true;
        }
      }

    }
    return false;
  }

  /**
   * Adds output to the operator.
   * 
   * @param node
   *          output node to add
   */
  public void addOutput(final AbstractNode node) {
    node.setNodeOperatorForOutput(this);
    if (outputs.contains(node)) {
      throw new InvalidArgumentException("Node already on the list: " + node);
    }
    outputs.add(node);
  }

  /**
   * Checks if the operator connects one input to several outputs.
   * 
   * @return <code>true</code> if the operator connects one input to several
   *         outputs, <code>false</code> otherwise
   */
  public boolean isProductOperator() {
    for (final AbstractNode node : outputs) {
      if (node instanceof Product) {
        return true;
      }
      if (node instanceof NodeOperator) {
        if (((NodeOperator) node).isProductOperator()) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Returns one character length description of the operator.
   * 
   * @return one character length description of the operator
   */
  public abstract String getOperatorText();

  /**
   * Checks if the operator connects several inputs (that are modifiers) to one
   * output.
   * 
   * @return <code>true</code> if the operator connects several inputs (that are
   *         modifiers) to one output, <code>false</code> otherwise
   */
  public boolean isModifierOperator() {
    for (final AbstractNode node : inputs) {
      if (node instanceof Modifier) {
        return true;
      }
      if (node instanceof NodeOperator) {
        if (((NodeOperator) node).isModifierOperator()) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Adds outputs to the operator.
   * 
   * @param newOutputs
   *          output nodes to add
   */
  public void addOutputs(final Collection<? extends AbstractNode> newOutputs) {
    for (final AbstractNode output : newOutputs) {
      addOutput(output);
    }
  }

  /**
   * @return the inputs
   * @see #inputs
   */
  public List<AbstractNode> getInputs() {
    return inputs;
  }

  /**
   * @param inputs
   *          the inputs to set
   * @see #inputs
   */
  public void setInputs(final List<AbstractNode> inputs) {
    this.inputs = inputs;
  }

  /**
   * @return the outputs
   * @see #outputs
   */
  public List<AbstractNode> getOutputs() {
    return outputs;
  }

  /**
   * @param outputs
   *          the outputs to set
   * @see #outputs
   */
  public void setOutputs(final List<AbstractNode> outputs) {
    this.outputs = outputs;
  }

}
