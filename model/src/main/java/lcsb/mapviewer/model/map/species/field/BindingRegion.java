package lcsb.mapviewer.model.map.species.field;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.sbo.SBOTermModificationResidueType;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This structure contains information about Binding Region for one of the
 * following {@link Species}:
 * <ul>
 * <li>{@link Protein}</li>
 * </ul>
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("BINDING_REGION")
public class BindingRegion extends AbstractRegionModification implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public BindingRegion() {

  }

  public BindingRegion(final String id) {
    super(id);
  }

  /**
   * Constructor that initialize object with the data from the parameter.
   * 
   * @param original
   *          object from which we initialize data
   */
  public BindingRegion(final BindingRegion original) {
    super(original);
  }

  /**
   * Creates a copy of current object.
   * 
   * @return copy of the object
   */
  @Override
  public BindingRegion copy() {
    if (this.getClass() == BindingRegion.class) {
      return new BindingRegion(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }

  }

  @Override
  public String getSboTerm() {
    return SBOTermModificationResidueType.BINDING_REGION.getSBO();
  }
}
