package lcsb.mapviewer.model.map.species.field;

import java.util.List;

/**
 * Interface implemented by species that support {@link ModificationResidue}.
 *
 * @author Piotr Gawron
 */
public interface SpeciesWithModificationResidue {

  List<ModificationResidue> getModificationResidues();

  String getName();
}
