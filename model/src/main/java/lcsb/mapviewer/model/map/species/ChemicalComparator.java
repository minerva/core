package lcsb.mapviewer.model.map.species;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.StringComparator;

/**
 * Comparator class used for comparing {@link Chemical} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class ChemicalComparator extends Comparator<Chemical> {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public ChemicalComparator(final double epsilon) {
    super(Chemical.class, true);
    this.epsilon = epsilon;
    addSubClassComparator(new IonComparator(epsilon));
    addSubClassComparator(new SimpleMoleculeComparator(epsilon));
  }

  /**
   * Default constructor.
   */
  public ChemicalComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected Comparator<?> getParentComparator() {
    return new SpeciesComparator(epsilon);
  }

  @Override
  protected int internalCompare(final Chemical arg0, final Chemical arg1) {
    StringComparator stringComparator = new StringComparator();

    if (stringComparator.compare(arg0.getSmiles(), arg1.getSmiles()) != 0) {
      logger.debug("Smiles different: " + arg0.getSmiles() + ", " + arg1.getSmiles());
      return stringComparator.compare(arg0.getSmiles(), arg1.getSmiles());
    }

    return 0;
  }
}
