package lcsb.mapviewer.model.map.modifier;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.species.Element;

/**
 * This class defines unknown catalysis modifier in the reaction.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("UNKNOWN_CATALYSIS_MODIFIER")
public class UnknownCatalysis extends Modifier {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  protected UnknownCatalysis() {
    super();
  }

  /**
   * Constructor that creates unknown catalysis modifier for given element.
   * 
   * @param element
   *          element object to which this modifier is assigned
   */
  public UnknownCatalysis(final Element element) {
    super(element);
    if (element == null) {
      throw new InvalidArgumentException("Element cannot be null");
    }
  }

  /**
   * Constructor that creates object with data taken from parameter unknown
   * catalysis.
   * 
   * @param unknownCatalysis
   *          object from which data are initialized
   */
  public UnknownCatalysis(final UnknownCatalysis unknownCatalysis) {
    super(unknownCatalysis);
  }

  @Override
  public UnknownCatalysis copy() {
    if (this.getClass() == UnknownCatalysis.class) {
      return new UnknownCatalysis(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
