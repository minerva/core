package lcsb.mapviewer.model.map.layout.graphics;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.modelutils.serializer.id.MinervaEntityAsIdSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import java.awt.Color;
import java.awt.geom.Rectangle2D;

/**
 * This class describes single text in the layer.
 *
 * @author Piotr Gawron
 */
@Entity
public class LayerText implements MinervaEntity, Drawable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default font size of the text.
   */
  private static final double DEFAULT_LAYER_FONT_SIZE = 11.0;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static final Logger logger = LogManager.getLogger();

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Text color.
   */
  @Type(type = "lcsb.mapviewer.persist.mapper.ColorMapper")
  @Column(nullable = false)
  private Color color = Color.BLACK;

  /**
   * Text background color.
   */
  @Type(type = "lcsb.mapviewer.persist.mapper.ColorMapper")
  @Column(nullable = false)
  private Color backgroundColor = Color.LIGHT_GRAY;

  @Type(type = "lcsb.mapviewer.persist.mapper.ColorMapper")
  @Column(nullable = false)
  private Color borderColor = Color.LIGHT_GRAY;

  /**
   * X coordinate of text start point.
   */
  private Double x = 0.0;

  /**
   * Y coordinate of text start point.
   */
  private Double y = 0.0;

  /**
   * Z-index.
   */
  private Integer z;

  /**
   * Width of the rectangle where text is drawn.
   */
  private Double width = 0.0;

  /**
   * Height of the rectangle where text is drawn.
   */
  private Double height = 0.0;

  /**
   * Text.
   */
  @Column(columnDefinition = "TEXT")
  private String notes;

  /**
   * Font size.
   */
  private Double fontSize = DEFAULT_LAYER_FONT_SIZE;

  /**
   * Glyph used for drawing (instead of text).
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JsonSerialize(using = MinervaEntityAsIdSerializer.class)
  @JsonIgnore
  private Glyph glyph = null;

  @ManyToOne(fetch = FetchType.LAZY)
  @JsonSerialize(using = MinervaEntityAsIdSerializer.class)
  private Layer layer;

  @Version
  @JsonIgnore
  private long entityVersion;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private VerticalAlign verticalAlign = VerticalAlign.TOP;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private HorizontalAlign horizontalAlign = HorizontalAlign.LEFT;

  /**
   * Default constructor.
   */
  public LayerText() {

  }

  /**
   * Default constructor.
   *
   * @param bound bounds in which text is placed
   * @param text  {@link #notes text}
   */
  public LayerText(final Rectangle2D bound, final String text) {
    setX(bound.getX());
    setY(bound.getY());
    setWidth(bound.getWidth());
    setHeight(bound.getHeight());
    setNotes(text);
  }

  /**
   * Constructor that copies data from the parameter.
   *
   * @param layerText from this parameter line data will be copied
   */
  public LayerText(final LayerText layerText) {
    color = layerText.getColor();
    x = layerText.getX();
    y = layerText.getY();
    z = layerText.getZ();
    width = layerText.getWidth();
    height = layerText.getHeight();
    notes = layerText.getNotes();
    fontSize = layerText.getFontSize();
    if (layerText.getGlyph() != null) {
      setGlyph(new Glyph(layerText.getGlyph()));
    }
    setBackgroundColor(layerText.getBackgroundColor());
    setBorderColor(layerText.getBorderColor());
    setVerticalAlign(layerText.getVerticalAlign());
    setHorizontalAlign(layerText.getHorizontalAlign());
  }

  /**
   * Prepares a copy of the object.
   *
   * @return copy of LayerText
   */
  public LayerText copy() {
    if (this.getClass() == LayerText.class) {
      return new LayerText(this);
    } else {
      throw new NotImplementedException("Method copy() should be overridden in class " + this.getClass());
    }
  }

  /**
   * @return the color
   * @see #color
   */
  public Color getColor() {
    return color;
  }

  /**
   * @param color the color to set
   * @see #color
   */
  public void setColor(final Color color) {
    this.color = color;
  }

  /**
   * @return the x
   * @see #x
   */
  public Double getX() {
    return x;
  }

  /**
   * Set x from string containing double value.
   *
   * @param param x of the line in text format
   */
  public void setX(final String param) {
    try {
      x = Double.parseDouble(param);
    } catch (final NumberFormatException e) {
      throw new InvalidArgumentException("Invalid x value: " + param, e);
    }
  }

  /**
   * @param x the x to set
   * @see #x
   */
  public void setX(final Double x) {
    this.x = x;
  }

  /**
   * @return the y
   * @see #y
   */
  public Double getY() {
    return y;
  }

  /**
   * Set y from string containing double value.
   *
   * @param param y of the line in text format
   */
  public void setY(final String param) {
    try {
      y = Double.parseDouble(param);
    } catch (final NumberFormatException e) {
      throw new InvalidArgumentException("Invalid y value: " + param, e);
    }
  }

  /**
   * @param y the y to set
   * @see #y
   */
  public void setY(final Double y) {
    this.y = y;
  }

  /**
   * @return the width
   * @see #width
   */
  public Double getWidth() {
    return width;
  }

  /**
   * Set width from string containing double value.
   *
   * @param param width of the line in text format
   */
  public void setWidth(final String param) {
    try {
      width = Double.parseDouble(param);
    } catch (final NumberFormatException e) {
      throw new InvalidArgumentException("Invalid width value: " + param, e);
    }
  }

  /**
   * @param width the width to set
   * @see #width
   */
  public void setWidth(final Double width) {
    this.width = width;
  }

  /**
   * @return the height
   * @see #height
   */
  public Double getHeight() {
    return height;
  }

  /**
   * Set height from string containing double value.
   *
   * @param param height of the line in text format
   */
  public void setHeight(final String param) {
    try {
      height = Double.parseDouble(param);
    } catch (final NumberFormatException e) {
      throw new InvalidArgumentException("Invalid height value: " + param, e);
    }
  }

  /**
   * @param height the height to set
   * @see #height
   */
  public void setHeight(final Double height) {
    this.height = height;
  }

  /**
   * @return the notes
   * @see #notes
   */
  public String getNotes() {
    return notes;
  }

  /**
   * @param notes the notes to set
   * @see #notes
   */
  public void setNotes(final String notes) {
    this.notes = notes;
  }

  /**
   * @return the fontSize
   * @see #fontSize
   */
  public Double getFontSize() {
    return fontSize;
  }

  /**
   * Set font size from string containing double value.
   *
   * @param param font size of the line in text format
   */
  public void setFontSize(final String param) {
    try {
      fontSize = Double.parseDouble(param);
    } catch (final NumberFormatException e) {
      throw new InvalidArgumentException("Invalid fontSize value: " + param, e);
    }

  }

  /**
   * @param fontSize the fontSize to set
   * @see #fontSize
   */
  public void setFontSize(final Double fontSize) {
    this.fontSize = fontSize;
  }

  public void setFontSize(final Integer fontSize) {
    this.fontSize = fontSize.doubleValue();
  }

  /**
   * Returns a rectangle that determines a rectangle border.
   *
   * @return rectangle border
   */
  @JsonIgnore
  public Rectangle2D getBorder() {
    return new Rectangle2D.Double(x, y, width, height);
  }

  public Color getBackgroundColor() {
    return backgroundColor;
  }

  public void setBackgroundColor(final Color backgroundColor) {
    this.backgroundColor = backgroundColor;
  }

  @Override
  public String toString() {
    return "[" + this.getClass().getSimpleName() + ": " + this.getNotes() + "]";
  }

  @Override
  public Integer getZ() {
    return z;
  }

  @Override
  public void setZ(final Integer z) {
    this.z = z;
  }

  @Override
  @JsonIgnore
  public String getElementId() {
    return "x=" + x + ";y=" + y + "; w=" + width + ", h=" + height;
  }

  @Override
  @JsonIgnore
  public double getSize() {
    return width * height;
  }

  @JsonIgnore
  public Glyph getGlyph() {
    return glyph;
  }

  public void setGlyph(final Glyph glyph) {
    this.glyph = glyph;
  }

  public Color getBorderColor() {
    return borderColor;
  }

  public void setBorderColor(final Color borderColor) {
    this.borderColor = borderColor;
  }

  @Override
  @JsonIgnore
  public Color getFillColor() {
    return getBackgroundColor();
  }

  @Override
  public void setFillColor(final Color color) {
    setBackgroundColor(color);
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  public void setLayer(final Layer layer) {
    this.layer = layer;
  }

  public Layer getLayer() {
    return layer;
  }

  public VerticalAlign getVerticalAlign() {
    return verticalAlign;
  }

  public void setVerticalAlign(final VerticalAlign verticalAlign) {
    this.verticalAlign = verticalAlign;
  }

  public HorizontalAlign getHorizontalAlign() {
    return horizontalAlign;
  }

  public void setHorizontalAlign(final HorizontalAlign horizontalAlign) {
    this.horizontalAlign = horizontalAlign;
  }

  @Override
  public EntityType getEntityType() {
    return EntityType.LAYER_TEXT;
  }

}
