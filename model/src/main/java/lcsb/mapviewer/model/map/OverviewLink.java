package lcsb.mapviewer.model.map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lcsb.mapviewer.model.MinervaEntity;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class representing link that connects {@link OverviewImage parent
 * OverviewImage} with some other piece of data. Right now there are two
 * implementations:
 * <ul>
 * <li>{@link OverviewImageLink} - connects to another {@link OverviewImageLink}
 * </li>
 * <li>{@link OverviewModelLink} - connects to a
 * {@link lcsb.mapviewer.model.map.model.Model Model}</li>
 * </ul>
 *
 * @author Piotr Gawron
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "link_type", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("GENERIC_VIEW")
public abstract class OverviewLink implements MinervaEntity {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Parent {@link OverviewImage} from which this link is outgoing.
   */
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JsonIgnore
  private OverviewImage overviewImage;

  /**
   * String representing polygon area on the image that should be clickable and
   * that corresponds to this link. This string should be space separated list of
   * coordinates. Example "10,10 20,20 100,0".
   */
  @JsonIgnore
  private String polygon;

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * Default constructor.
   */
  public OverviewLink() {

  }

  /**
   * Default constructor that creates a copy of the object from parameter.
   *
   * @param original original object to copy
   */
  public OverviewLink(final OverviewLink original) {
    this.id = original.id;
    this.overviewImage = original.overviewImage;
    this.polygon = original.polygon;
    this.entityVersion = 0;
  }

  /**
   * @return the polygon
   * @see #polygon
   */
  public String getPolygon() {
    return polygon;
  }

  /**
   * @param polygon the polygon to set
   * @see #polygon
   */
  public void setPolygon(final String polygon) {
    this.polygon = polygon;
  }

  public void setPolygon(final List<? extends Point2D> coordinates) {
    StringBuilder polygon = new StringBuilder();
    for (Point2D point2d : coordinates) {
      polygon.append(point2d.getX()).append(",").append(point2d.getY()).append(" ");
    }
    polygon.deleteCharAt(polygon.length() - 1);
    this.polygon = polygon.toString();
  }

  /**
   * Transforms {@link #polygon} into list of points.
   *
   * @return list of points representing {@link #polygon} area.
   */
  @JsonProperty("polygon")
  public List<Point2D> getPolygonCoordinates() {
    List<Point2D> coordinates = new ArrayList<>();
    String[] stringCoordinates = polygon.split(" ");
    for (final String string : stringCoordinates) {
      String[] coord = string.split(",");
      double x = Double.parseDouble(coord[0]);
      double y = Double.parseDouble(coord[1]);
      Point2D point = new Point2D.Double(x, y);
      coordinates.add(point);
    }
    return coordinates;
  }

  /**
   * @return the overviewImage
   * @see #overviewImage
   */
  public OverviewImage getOverviewImage() {
    return overviewImage;
  }

  /**
   * @param overviewImage the overviewImage to set
   * @see #overviewImage
   */
  public void setOverviewImage(final OverviewImage overviewImage) {
    this.overviewImage = overviewImage;
  }

  /**
   * @return the id
   * @see #id
   */
  @Override
  public int getId() {
    return id;
  }

  /**
   * @param id the id to set
   * @see #id
   */
  public void setId(final int id) {
    this.id = id;
  }

  /**
   * Copies the object and returns a copy.
   *
   * @return copy of the object
   */
  public abstract OverviewLink copy();

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @JsonProperty("type")
  public String getType() {
    return getClass().getSimpleName();
  }
}
