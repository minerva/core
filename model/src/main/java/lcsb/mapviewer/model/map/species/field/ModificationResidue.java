package lcsb.mapviewer.model.map.species.field;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.modelutils.serializer.model.map.ElementAsIdSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import java.awt.Color;
import java.awt.geom.Point2D;
import java.io.Serializable;
import java.text.DecimalFormat;

/**
 * This class represent modification residue in a {@link Species}.
 *
 * @author Piotr Gawron
 */

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "modification_type", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("GENERIC_MODIFICATION_RESIDUE")
public abstract class ModificationResidue implements Serializable, Drawable, MinervaEntity {

  private static final long serialVersionUID = 1L;

  protected static Logger logger = LogManager.getLogger();

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Identifier of the modification. Must be unique in single map model.
   */
  @Column(length = 255)
  private String idModificationResidue = "";

  /**
   * Name of the modification.
   */
  @Column(length = 255)
  private String name = "";

  @Column(nullable = false)
  private Double x;

  @Column(nullable = false)
  private Double y;

  @Column(nullable = false)
  private Integer z;

  @NotNull
  private Double width;

  @NotNull
  private Double height;

  @Column(nullable = false, name = "name_x")
  private Double nameX;

  @Column(nullable = false, name = "name_y")
  private Double nameY;

  @Column(nullable = false)
  private Double nameWidth;

  @Column(nullable = false)
  private Double nameHeight;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private VerticalAlign nameVerticalAlign;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private HorizontalAlign nameHorizontalAlign;

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * Species to which this modification belong to.
   */
  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "idSpeciesDb", nullable = false)
  @JsonSerialize(using = ElementAsIdSerializer.class)
  private Species species;

  @Type(type = "lcsb.mapviewer.persist.mapper.ColorMapper")
  @Column(nullable = false)
  private Color borderColor;

  @Type(type = "lcsb.mapviewer.persist.mapper.ColorMapper")
  @Column(nullable = false)
  private Color fillColor = Color.WHITE;

  @Column(nullable = false)
  private Double fontSize = 10.0;

  /**
   * Default constructor.
   */
  public ModificationResidue() {
    nameVerticalAlign = VerticalAlign.MIDDLE;
    nameHorizontalAlign = HorizontalAlign.CENTER;
  }

  /**
   * Constructor that initialize object with the data taken from the parameter.
   *
   * @param mr original object from which data is taken
   */
  public ModificationResidue(final ModificationResidue mr) {
    this.idModificationResidue = mr.idModificationResidue;
    this.name = mr.name;
    this.x = mr.x;
    this.y = mr.y;
    this.z = mr.z;
    this.borderColor = mr.borderColor;
    this.fillColor = mr.fillColor;
    this.fontSize = mr.fontSize;
    this.width = mr.getWidth();
    this.height = mr.getHeight();
    this.nameVerticalAlign = mr.nameVerticalAlign;
    this.nameHorizontalAlign = mr.nameHorizontalAlign;
    this.nameX = mr.nameX;
    this.nameY = mr.nameY;
    this.nameWidth = mr.nameWidth;
    this.nameHeight = mr.nameHeight;
  }

  public ModificationResidue(final String modificationId) {
    this();
    this.idModificationResidue = modificationId;
  }

  /**
   * @return the idModificationResidue
   * @see #id
   */
  @Override
  public int getId() {
    return id;
  }

  /**
   * @param id the idModificationResidue to set
   * @see #id
   */
  public void setId(final int id) {
    this.id = id;
  }

  /**
   * @return the id
   * @see #idModificationResidue
   */
  public String getIdModificationResidue() {
    return idModificationResidue;
  }

  /**
   * @param idModificationResidue the id to set
   * @see #idModificationResidue
   */
  public void setIdModificationResidue(final String idModificationResidue) {
    this.idModificationResidue = idModificationResidue;
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   * @see #name
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * @return the species
   * @see #species
   */
  public Species getSpecies() {
    return species;
  }

  /**
   * @param species the species to set
   * @see #species
   */
  public void setSpecies(final Species species) {
    this.species = species;
  }

  public void setPosition(final double x, final double y) {
    this.x = x;
    this.y = y;
  }

  public void setPosition(final Point2D position) {
    this.x = position.getX();
    this.y = position.getY();
  }

  public ModificationResidue copy() {
    throw new NotImplementedException();
  }

  @Override
  public Integer getZ() {
    return z;
  }

  @Override
  public void setZ(final Integer z) {
    this.z = z;
  }

  @Override
  public String getElementId() {
    return getIdModificationResidue();
  }

  @Override
  public Color getBorderColor() {
    return borderColor;
  }

  @Override
  public void setBorderColor(final Color borderColor) {
    this.borderColor = borderColor;
  }

  @Override
  public Color getFillColor() {
    return fillColor;
  }

  @Override
  public void setFillColor(final Color fillColor) {
    this.fillColor = fillColor;
  }

  public Double getFontSize() {
    return fontSize;
  }

  public void setFontSize(final Double fontSize) {
    this.fontSize = fontSize;
  }

  public void setFontSize(final int fontSize) {
    this.fontSize = (double) fontSize;
  }

  public double getWidth() {
    return width;
  }

  public void setWidth(final double width) {
    this.width = width;
  }

  public double getHeight() {
    return height;
  }

  public void setHeight(final double height) {
    this.height = height;
  }

  @JsonIgnore
  public Point2D getCenter() {
    return new Point2D.Double(getX() + getWidth() / 2, getY() + getHeight() / 2);
  }

  @Override
  public double getSize() {
    return width * height;
  }

  public Double getY() {
    return y;
  }

  public void setY(final double y) {
    this.y = y;
  }

  public Double getX() {
    return x;
  }

  public void setX(final double x) {
    this.x = x;
  }

  @Override
  public String toString() {
    final DecimalFormat format = new DecimalFormat("#.##");
    final String position;
    if (getX() == null || getY() == null) {
      position = null;
    } else {
      position = "Point2D[" + format.format(getX()) + "," + format.format(getY()) + "]";
    }
    return "[" + this.getClass().getSimpleName() + "]: " + getName() + "," + getWidth() + "," + getHeight()
        + "," + position + "," + getZ();
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  public abstract String getSboTerm();

  public Double getNameX() {
    return nameX;
  }

  public void setNameX(final Double nameX) {
    this.nameX = nameX;
  }

  public Double getNameY() {
    return nameY;
  }

  public void setNameY(final Double nameY) {
    this.nameY = nameY;
  }

  public Double getNameWidth() {
    return nameWidth;
  }

  public void setNameWidth(final Double nameWidth) {
    this.nameWidth = nameWidth;
  }

  public Double getNameHeight() {
    return nameHeight;
  }

  public void setNameHeight(final Double nameHeight) {
    this.nameHeight = nameHeight;
  }

  public VerticalAlign getNameVerticalAlign() {
    return nameVerticalAlign;
  }

  public void setNameVerticalAlign(final VerticalAlign nameVerticalAlign) {
    this.nameVerticalAlign = nameVerticalAlign;
  }

  public HorizontalAlign getNameHorizontalAlign() {
    return nameHorizontalAlign;
  }

  public void setNameHorizontalAlign(final HorizontalAlign nameHorizontalAlign) {
    this.nameHorizontalAlign = nameHorizontalAlign;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
