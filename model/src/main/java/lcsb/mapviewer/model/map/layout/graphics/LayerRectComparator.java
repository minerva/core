package lcsb.mapviewer.model.map.layout.graphics;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.ColorComparator;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.comparator.IntegerComparator;

/**
 * Comparator of {@link LayerRect} class.
 * 
 * @author Piotr Gawron
 * 
 * 
 */
public class LayerRectComparator extends Comparator<LayerRect> {

  private Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public LayerRectComparator(final double epsilon) {
    super(LayerRect.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public LayerRectComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(final LayerRect arg0, final LayerRect arg1) {
    DoubleComparator doubleComparator = new DoubleComparator(epsilon);
    ColorComparator colorComparator = new ColorComparator();
    IntegerComparator integerComparator = new IntegerComparator();

    if (doubleComparator.compare(arg0.getWidth(), arg1.getWidth()) != 0) {
      return doubleComparator.compare(arg0.getWidth(), arg1.getWidth());
    }

    if (doubleComparator.compare(arg0.getHeight(), arg1.getHeight()) != 0) {
      return doubleComparator.compare(arg0.getHeight(), arg1.getHeight());
    }

    if (doubleComparator.compare(arg0.getX(), arg1.getX()) != 0) {
      return doubleComparator.compare(arg0.getX(), arg1.getX());
    }

    if (doubleComparator.compare(arg0.getY(), arg1.getY()) != 0) {
      return doubleComparator.compare(arg0.getY(), arg1.getY());
    }

    if (colorComparator.compare(arg0.getFillColor(), arg1.getFillColor()) != 0) {
      logger.debug("Fill color different: " + arg0.getFillColor() + "; " + arg1.getFillColor());
      return colorComparator.compare(arg0.getFillColor(), arg1.getFillColor());
    }

    if (colorComparator.compare(arg0.getBorderColor(), arg1.getBorderColor()) != 0) {
      logger.debug("Border color different: " + arg0.getBorderColor() + "; " + arg1.getBorderColor());
      return colorComparator.compare(arg0.getBorderColor(), arg1.getBorderColor());
    }

    if (integerComparator.compare(arg0.getZ(), arg1.getZ()) != 0) {
      return integerComparator.compare(arg0.getZ(), arg1.getZ());
    }

    return 0;
  }

}
