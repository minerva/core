package lcsb.mapviewer.model.map;

import lcsb.mapviewer.common.Comparator;

/**
 * {@link Comparator} implementation for {@link MiriamType} that uses
 * {@link MiriamType#commonName} as a key to comparison.
 * 
 * @author Piotr Gawron
 *
 */
public class MiriamTypeNameComparator extends Comparator<MiriamType> {

  public MiriamTypeNameComparator() {
    super(MiriamType.class);
  }

  @Override
  protected int internalCompare(final MiriamType arg0, final MiriamType arg1) {
    return arg0.getCommonName().compareTo(arg1.getCommonName());
  }

}
