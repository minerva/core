package lcsb.mapviewer.model.map.reaction.type;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines a standard CellDesigner translation reaction. It must have
 * at least one reactant and one product.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("TRANSLATION_REACTION")
public class TranslationReaction extends Reaction implements SimpleReactionInterface {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  protected TranslationReaction() {
    super();
  }

  public TranslationReaction(final String elementId) {
    super(elementId);
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   *
   * @param result parent reaction from which we copy data
   */
  public TranslationReaction(final Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Translation";
  }

  @Override
  public ReactionRect getReactionRect() {
    return ReactionRect.RECT_EMPTY;
  }

  @Override
  public TranslationReaction copy() {
    if (this.getClass() == TranslationReaction.class) {
      return new TranslationReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermReactionType.TRANSLATION.getSBO();
  }
}
