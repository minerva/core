package lcsb.mapviewer.model.map.species;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.BooleanComparator;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.comparator.IntegerComparator;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitComparator;

/**
 * Comparator class used for comparing {@link Species} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class SpeciesComparator extends Comparator<Species> {

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public SpeciesComparator(final double epsilon) {
    super(Species.class, true);
    this.epsilon = epsilon;
    addSubClassComparator(new AntisenseRnaComparator(epsilon));
    addSubClassComparator(new ChemicalComparator(epsilon));
    addSubClassComparator(new ComplexComparator(epsilon));
    addSubClassComparator(new DegradedComparator(epsilon));
    addSubClassComparator(new DrugComparator(epsilon));
    addSubClassComparator(new GeneComparator(epsilon));
    addSubClassComparator(new PhenotypeComparator(epsilon));
    addSubClassComparator(new ProteinComparator(epsilon));
    addSubClassComparator(new RnaComparator(epsilon));
    addSubClassComparator(new UnknownComparator(epsilon));
  }

  /**
   * Default constructor.
   */
  public SpeciesComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(final Species arg0, final Species arg1) {
    ElementComparator elementComparator = new ElementComparator(epsilon);
    int result = elementComparator.internalCompare(arg0, arg1);
    if (result != 0) {
      return result;
    }

    DoubleComparator doubleComparator = new DoubleComparator(epsilon);
    BooleanComparator booleanComparator = new BooleanComparator();
    IntegerComparator integerComparator = new IntegerComparator();

    if (booleanComparator.compare(arg0.getActivity(), arg1.getActivity()) != 0) {
      logger.debug("activity different: " + arg0.getActivity() + ", " + arg1.getActivity());
      return booleanComparator.compare(arg0.getActivity(), arg1.getActivity());
    }

    if (doubleComparator.compare(arg0.getLineWidth(), arg1.getLineWidth()) != 0) {
      logger.debug("line width different: " + arg0.getLineWidth() + ", " + arg1.getLineWidth());
      return doubleComparator.compare(arg0.getLineWidth(), arg1.getLineWidth());
    }
    StringComparator stringComparator = new StringComparator();
    if (stringComparator.compare(arg0.getStateLabel(), arg1.getStateLabel()) != 0) {
      logger.debug("state label different: " + arg0.getStateLabel() + ", " + arg1.getStateLabel());
      return stringComparator.compare(arg0.getStateLabel(), arg1.getStateLabel());
    }

    if (stringComparator.compare(arg0.getStatePrefix(), arg1.getStatePrefix()) != 0) {
      logger.debug("state prefix different: " + arg0.getStatePrefix() + ", " + arg1.getStatePrefix());
      return stringComparator.compare(arg0.getStatePrefix(), arg1.getStatePrefix());
    }

    if (doubleComparator.compare(arg0.getInitialAmount(), arg1.getInitialAmount()) != 0) {
      logger.debug("Initial amount different: " + arg0.getInitialAmount() + ", " + arg1.getInitialAmount());
      return doubleComparator.compare(arg0.getInitialAmount(), arg1.getInitialAmount());
    }

    if (integerComparator.compare(arg0.getCharge(), arg1.getCharge()) != 0) {
      logger.debug("Charge different: " + arg0.getCharge() + ", " + arg1.getCharge());
      return integerComparator.compare(arg0.getCharge(), arg1.getCharge());
    }

    if (integerComparator.compare(arg0.getHomodimer(), arg1.getHomodimer()) != 0) {
      logger.debug("Homodimer different: " + arg0.getHomodimer() + ", " + arg1.getHomodimer());
      return integerComparator.compare(arg0.getHomodimer(), arg1.getHomodimer());
    }

    if (doubleComparator.compare(arg0.getInitialConcentration(), arg1.getInitialConcentration()) != 0) {
      logger.debug(
          "Initial concentration different: " + arg0.getInitialConcentration() + ", " + arg1.getInitialConcentration());
      return doubleComparator.compare(arg0.getInitialConcentration(), arg1.getInitialConcentration());
    }

    if (booleanComparator.compare(arg0.hasOnlySubstanceUnits(), arg1.hasOnlySubstanceUnits()) != 0) {
      logger
          .debug("OnlySubstanceUnits different: " + arg0.hasOnlySubstanceUnits() + ", " + arg1.hasOnlySubstanceUnits());
      return booleanComparator.compare(arg0.hasOnlySubstanceUnits(), arg1.hasOnlySubstanceUnits());
    }

    if (arg0.getPositionToCompartment() != null || arg1.getPositionToCompartment() != null) {
      Integer pos0 = null;
      Integer pos1 = null;
      if (arg0.getPositionToCompartment() != null) {
        pos0 = arg0.getPositionToCompartment().ordinal();
      }
      if (arg1.getPositionToCompartment() != null) {
        pos1 = arg1.getPositionToCompartment().ordinal();
      }
      if (integerComparator.compare(pos0, pos1) != 0) {
        logger.debug("PositionToCompartment different: \"" + arg0.getPositionToCompartment() + "\", \""
            + arg1.getPositionToCompartment() + "\"");
        return integerComparator.compare(pos0, pos1);
      }
    }

    if (booleanComparator.compare(arg0.isHypothetical(), arg1.isHypothetical()) != 0) {
      logger.debug("Hypothetical different: \"" + arg0.isHypothetical() + "\", \"" + arg1.isHypothetical() + "\"");
      return booleanComparator.compare(arg0.isHypothetical(), arg1.isHypothetical());
    }

    if (booleanComparator.compare(arg0.isConstant(), arg1.isConstant()) != 0) {
      logger.debug("Constant different: \"" + arg0.isConstant() + "\", \"" + arg1.isConstant() + "\"");
      return booleanComparator.compare(arg0.isConstant(), arg1.isConstant());
    }

    if (booleanComparator.compare(arg0.isBoundaryCondition(), arg1.isBoundaryCondition()) != 0) {
      logger.debug("BoundaryCondition different: \"" + arg0.isBoundaryCondition() + "\", \""
          + arg1.isBoundaryCondition() + "\"");
      return booleanComparator.compare(arg0.isBoundaryCondition(), arg1.isBoundaryCondition());
    }

    SbmlUnitComparator unitComparator = new SbmlUnitComparator();

    SbmlUnit sbmlUnit1 = arg0.getSubstanceUnits();
    SbmlUnit sbmlUnit2 = arg1.getSubstanceUnits();
    if (unitComparator.compare(sbmlUnit1, sbmlUnit2) != 0) {
      logger.debug(
          "SubstanceUnits different: \"" + arg0.getSubstanceUnits() + "\", \"" + arg1.getSubstanceUnits() + "\"");
      return unitComparator.compare(sbmlUnit1, sbmlUnit2);
    }

    return 0;
  }
}
