package lcsb.mapviewer.model.map.species.field;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.sbo.SBOTermModificationResidueType;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Species;

/**
 * This structure contains information about Regulatory Region for one of the
 * following {@link Species}:
 * <ul>
 * <li>{@link Gene}</li>
 * </ul>
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("REGULATORY_REGION")
public class RegulatoryRegion extends AbstractRegionModification implements Serializable {

  private static final long serialVersionUID = 1L;

  public RegulatoryRegion() {

  }

  public RegulatoryRegion(final String id) {
    super(id);
  }

  /**
   * Constructor that initialize object with the data from the parameter.
   * 
   * @param original
   *          object from which we initialize data
   */
  public RegulatoryRegion(final RegulatoryRegion original) {
    super(original);
  }

  /**
   * Creates a copy of current object.
   * 
   * @return copy of the object
   */
  @Override
  public RegulatoryRegion copy() {
    if (this.getClass() == RegulatoryRegion.class) {
      return new RegulatoryRegion(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermModificationResidueType.REGULATORY_REGION.getSBO();
  }

}
