package lcsb.mapviewer.model.map.sbo;

import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;
import lcsb.mapviewer.model.map.species.field.RegulatoryRegion;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public enum SBOTermModificationResidueType {
  BINDING_REGION(BindingRegion.class, new String[]{"SBO:0000494"}),
  CODING_REGION(CodingRegion.class, new String[]{"SBO:0000335"}),
  MODIFICATION_SITE(ModificationSite.class, new String[]{"SBO:0000634"}),
  PROTEIN_BINDING_DOMAIN(ProteinBindingDomain.class, new String[]{"SBO:0000312"}),
  REGULATORY_REGION(RegulatoryRegion.class, new String[]{"SBO:0000369"}),
  RESIDUE(Residue.class, new String[]{"SBO:0000493"}),
  STRUCTURAL_STATE(StructuralState.class, new String[]{"SBO:0000181"}),
  TRANSCRIPTION_SITE(TranscriptionSite.class, new String[]{"SBO:0000329"}),
  ;

  private static final Logger logger = LogManager.getLogger();

  private final Class<? extends ModificationResidue> clazz;
  private final List<String> sboTerms = new ArrayList<>();

  SBOTermModificationResidueType(final Class<? extends ModificationResidue> clazz, final String[] inputSboTerms) {
    this.clazz = clazz;
    java.util.Collections.addAll(sboTerms, inputSboTerms);
  }

  public static SBOTermModificationResidueType getTypeSBOTerm(final String sboTerm, final Marker logMarker) {
    if (sboTerm == null || sboTerm.isEmpty()) {
      return STRUCTURAL_STATE;
    }
    SBOTermModificationResidueType result = null;
    for (final SBOTermModificationResidueType term : values()) {
      for (final String string : term.sboTerms) {
        if (string.equalsIgnoreCase(sboTerm)) {
          result = term;
          break;
        }
      }
    }
    if (result == null) {
      logger.warn(logMarker, "Don't know how to handle SBOTerm " + sboTerm);
      result = STRUCTURAL_STATE;
    }
    return result;
  }

  public static ModificationResidue createElementForSBOTerm(final String sboTerm, final String id, final Marker logMarker) {
    try {
      final SBOTermModificationResidueType type = getTypeSBOTerm(sboTerm, logMarker);
      final Class<? extends ModificationResidue> clazz = type.clazz;
      final ModificationResidue result = clazz.getConstructor(String.class).newInstance(id);
      return result;
    } catch (final SecurityException | NoSuchMethodException | InstantiationException | IllegalAccessException
                   | IllegalArgumentException | InvocationTargetException e) {
      throw new InvalidStateException(e);
    }
  }

  public static String getTermByType(final ModificationResidue residue) {
    for (final SBOTermModificationResidueType term : values()) {
      if (residue.getClass().equals(term.clazz)) {
        return term.getSBO();
      }
    }
    logger.warn("Cannot find SBO term for class: " + residue.getClass());
    return null;
  }

  public String getSBO() {
    if (!sboTerms.isEmpty()) {
      return sboTerms.get(0);
    }
    logger.warn("Cannot find SBO term for class: " + clazz);
    return null;
  }

  public List<String> getSboTerms() {
    return sboTerms;
  }
}
