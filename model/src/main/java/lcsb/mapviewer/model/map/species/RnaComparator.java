package lcsb.mapviewer.model.map.species;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.SetComparator;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

/**
 * Comparator class used for comparing {@link Rna} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class RnaComparator extends Comparator<Rna> {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public RnaComparator(final double epsilon) {
    super(Rna.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public RnaComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected Comparator<?> getParentComparator() {
    return new SpeciesComparator(epsilon);
  }

  @Override
  protected int internalCompare(final Rna arg0, final Rna arg1) {
    SetComparator<String> stringSetComparator = new SetComparator<>(new StringComparator());

    Set<String> set1 = new HashSet<>();
    Set<String> set2 = new HashSet<>();

    for (final ModificationResidue region : arg0.getModificationResidues()) {
      set1.add(region.toString());
    }

    for (final ModificationResidue region : arg1.getModificationResidues()) {
      set2.add(region.toString());
    }

    if (stringSetComparator.compare(set1, set2) != 0) {
      logger.debug("Regions different");
      logger.debug(set1);
      logger.debug(set2);
      return stringSetComparator.compare(set1, set2);
    }

    return 0;
  }
}
