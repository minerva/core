package lcsb.mapviewer.model.map.model;

import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.model.map.species.Element;

import java.util.Comparator;

public class ElementByIdComparator implements Comparator<Element> {
  private final StringComparator comparator = new StringComparator();

  @Override
  public int compare(final Element o1, final Element o2) {
    if (comparator.compare(o1.getElementId(), o2.getElementId()) == 0) {
      return o1.getId() - o2.getId();
    }
    return comparator.compare(o1.getElementId(), o2.getElementId());
  }
}
