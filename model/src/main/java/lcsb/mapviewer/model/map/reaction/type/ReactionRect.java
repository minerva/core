package lcsb.mapviewer.model.map.reaction.type;

/**
 * This enum describes inside of the central rectangle in the reaction.
 *
 * @author Piotr Gawron
 */
public enum ReactionRect {

  /**
   * Double slashed inside.
   */
  RECT_SLASH("//"),

  /**
   * Question mark (something is unknown).
   */
  RECT_QUESTION("?"),

  /**
   * Thunderbolt should be drawn.
   */
  RECT_BOLT(""),

  /**
   * Empty inside.
   */
  RECT_EMPTY("");

  /**
   * Text drawn in the inside of the rectangle.
   */
  private final String text;

  /**
   * Default constructor that initialized the text inside a specific type of
   * rectangle.
   *
   * @param text text describing inside
   */
  ReactionRect(final String text) {
    this.text = text;
  }

  /**
   * @return the text
   * @see #text
   */
  public String getText() {
    return text;
  }
}
