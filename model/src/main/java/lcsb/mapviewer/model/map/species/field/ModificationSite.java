package lcsb.mapviewer.model.map.species.field;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.sbo.SBOTermModificationResidueType;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.Species;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This structure contains information about Modification Site for one of the
 * following {@link Species}:
 * <ul>
 * <li>{@link Rna}</li>
 * <li>{@link AntisenseRna}</li>
 * <li>{@link Gene}</li>
 * </ul>
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("MODIFICATION_SITE")
public class ModificationSite extends AbstractSiteModification {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  public ModificationSite() {
    super();
    this.setWidth(15);
    this.setHeight(22.5);

    this.setNameWidth(getWidth());
    this.setNameHeight(getWidth());
  }

  public ModificationSite(final String id) {
    super(id);
  }

  public ModificationSite(final ModificationSite site) {
    super(site);
  }

  /**
   * Creates copy of the object.
   *
   * @return copy of the object.
   */
  @Override
  public ModificationSite copy() {
    if (this.getClass() == ModificationSite.class) {
      return new ModificationSite(this);
    } else {
      throw new NotImplementedException("Method copy() should be overridden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermModificationResidueType.MODIFICATION_SITE.getSBO();
  }

}
