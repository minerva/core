package lcsb.mapviewer.model.map.layout;

import java.io.Serializable;
import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.modelutils.serializer.id.ModelDataAsIdSerializer;
import lcsb.mapviewer.modelutils.serializer.id.ProjectBackgroundAsIdSerializer;

/**
 * This object represents set of images generated for background visualization
 * of {@link ProjectBackground}.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
public class ProjectBackgroundImageLayer implements Serializable {
  public static final Comparator<? super ProjectBackgroundImageLayer> ID_COMPARATOR = new Comparator<ProjectBackgroundImageLayer>() {

    @Override
    public int compare(final ProjectBackgroundImageLayer o1, final ProjectBackgroundImageLayer o2) {
      return o1.getId() - o2.getId();
    }
  };
  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Directory where the images are stored.
   */
  @Column(length = 255)
  @JsonProperty("path")
  private String directory;

  /**
   * {@link ModelData} for which the images were created.
   */
  @ManyToOne(optional = false)
  @JoinColumn(name = "model_id", updatable = false)
  @OnDelete(action = OnDeleteAction.CASCADE)
  @JsonSerialize(using = ModelDataAsIdSerializer.class)
  private ModelData model;

  /**
   * {@link ProjectBackground} for which the images were created.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @OnDelete(action = OnDeleteAction.CASCADE)
  @JsonSerialize(using = ProjectBackgroundAsIdSerializer.class)
  private ProjectBackground projectBackground;

  /**
   * Default constructor.
   */
  protected ProjectBackgroundImageLayer() {

  }

  /**
   * Constructor that initializes object with basic parameters.
   *
   * @param model
   *          map for which background is created
   * @param directory
   *          directory where the images are stored
   */
  public ProjectBackgroundImageLayer(final ModelData model, final String directory) {
    if (model == null) {
      throw new NullPointerException("Model data cannot be null");
    }
    this.directory = directory;
    this.model = model;
  }

  public ProjectBackgroundImageLayer(final Model model, final String directory) {
    this(model.getModelData(), directory);
  }

  /**
   * @return the id
   * @see #id
   */
  public int getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(final int id) {
    this.id = id;
  }

  /**
   * @return the directory
   * @see #directory
   */
  public String getDirectory() {
    return directory;
  }

  /**
   * @param directory
   *          the directory to set
   * @see #directory
   */
  public void setDirectory(final String directory) {
    this.directory = directory;
  }

  public ModelData getModel() {
    return model;
  }

  public void setModel(final ModelData model) {
    this.model = model;
  }

  public ProjectBackground getProjectBackground() {
    return projectBackground;
  }

  public void setProjectBackground(final ProjectBackground projectBackground) {
    this.projectBackground = projectBackground;
  }

}
