package lcsb.mapviewer.model.map.kinetics;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Representation of a single SBML parameter
 *
 * @author Piotr Gawron
 */
@Entity
@XmlRootElement
public class SbmlParameter implements MinervaEntity, SbmlArgument {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static final Logger logger = LogManager.getLogger();
  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(length = 255)
  private String parameterId;

  @Column(length = 255)
  private String name;

  private Double value;

  @ManyToOne()
  private SbmlUnit units;

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * Constructor required by hibernate.
   */
  SbmlParameter() {
    super();
  }

  public SbmlParameter(final String parameterId) {
    this.parameterId = parameterId;
  }

  public SbmlParameter(final SbmlParameter original) {
    this.parameterId = original.getParameterId();
    this.name = original.getName();
    this.value = original.getValue();
    this.units = original.getUnits();
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getParameterId() {
    return parameterId;
  }

  public void setParameterId(final String parameterId) {
    this.parameterId = parameterId;
  }

  public Double getValue() {
    return value;
  }

  public void setValue(final Double value) {
    this.value = value;
  }

  public SbmlUnit getUnits() {
    return units;
  }

  public void setUnits(final SbmlUnit units) {
    this.units = units;

  }

  @Override
  public SbmlParameter copy() {
    return new SbmlParameter(this);
  }

  @Override
  public String getElementId() {
    return getParameterId();
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }

}
