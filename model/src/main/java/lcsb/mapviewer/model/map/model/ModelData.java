package lcsb.mapviewer.model.map.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.layout.BlockDiagram;
import lcsb.mapviewer.model.map.layout.ElementGroup;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Representation of the model data. It contains all information about single
 * map:
 * <ul>
 * <li>species and compartments ({@link #elements} field)</li>
 * <li>list of reactions ({@link #reactions})</li>
 * <li>layers with additional graphical objects ({@link #layers})</li>
 * <li>different graphical visualizations of the whole map ({@link #layouts})
 * </li>
 * <li>some other meta data (like: creation date, version, etc)</li>
 * </ul>
 *
 * @author Piotr Gawron
 */
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "id"})
public class ModelData implements MinervaEntity {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  public static final Comparator<? super ModelData> ID_COMPARATOR = new Comparator<ModelData>() {

    @Override
    public int compare(final ModelData o1, final ModelData o2) {
      return o1.getId() - o2.getId();
    }
  };

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Set of all elements in the map.
   *
   * @see Element
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(mappedBy = "model", orphanRemoval = true)
  @JsonIgnore
  private Set<Element> elements = new HashSet<>();

  /**
   * Set of all layers in the map.
   *
   * @see Layer
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(mappedBy = "model", orphanRemoval = true)
  @JsonIgnore
  private Set<Layer> layers = new HashSet<>();

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @JsonProperty("idObject")
  private int id;

  /**
   * Collection of all reactions in the map.
   *
   * @see Reaction
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(mappedBy = "model", orphanRemoval = true)
  @JsonIgnore
  private Set<Reaction> reactions = new HashSet<>();

  /**
   * Collection of SBML functions in the map.
   *
   * @see SbmlFunction
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(mappedBy = "model", orphanRemoval = true)
  @JsonIgnore
  private Set<SbmlFunction> functions = new HashSet<>();

  @Cascade({CascadeType.ALL})
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "model_parameters_table", joinColumns = {
      @JoinColumn(name = "model_id", referencedColumnName = "id", nullable = false, updatable = false)},
      inverseJoinColumns = {
          @JoinColumn(name = "parameter_id", referencedColumnName = "id", nullable = true, updatable = true)})
  @JsonIgnore
  private Set<SbmlParameter> parameters = new HashSet<>();

  @Cascade({CascadeType.ALL})
  @OneToMany(mappedBy = "model", orphanRemoval = true)
  @JsonIgnore
  private Set<SbmlUnit> units = new HashSet<>();

  /**
   * Width of the map.
   */

  @JsonProperty("width")
  private double width;

  /**
   * Height of the map.
   */
  @JsonProperty("height")
  private double height;

  /**
   * X coordinate that should be used when initially showing map.
   */
  @JsonProperty("defaultCenterX")
  private Double defaultCenterX;

  /**
   * Y coordinate that should be used when initially showing map.
   */
  @JsonProperty("defaultCenterY")
  private Double defaultCenterY;

  /**
   * Description of the map.
   */
  @Column(columnDefinition = "TEXT", nullable = false)
  @JsonProperty("description")
  private String notes = "";

  /**
   * Name of the map.
   */
  @Column(length = 255, nullable = false)
  @JsonProperty("name")
  private String name = "Unknown";

  /**
   * Another CellDesigner identifier.
   */
  @JsonIgnore
  private String idModel;

  /**
   * How many hierarchical levels are in this map.
   */
  @JsonIgnore
  private int zoomLevels;

  /**
   * Zoom level that should be used when initially showing map.
   */
  @JsonProperty("defaultZoomLevel")
  private Integer defaultZoomLevel;

  /**
   * Size of the image tile that are used in this model.
   */
  @JsonProperty("tileSize")
  private int tileSize;

  /**
   * Project to which this model belong to.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JsonIgnore
  private Project project;

  // This field should be transient in hibernate and during the transformation
  // to xml
  /**
   * {@link Model} object that is a container where data is being placed.
   */
  @Transient
  @XmlTransient
  @JsonIgnore
  private Model model;

  /**
   * List of submodels.
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "parentModel", orphanRemoval = true)
  @JsonIgnore
  private Set<ModelSubmodelConnection> submodels = new HashSet<>();

  /**
   * List of connections with parent model (by definition one map can be a
   * submodel of few maps).
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "submodel")
  @JsonIgnore
  private Set<SubmodelConnection> parentModels = new HashSet<>();

  /**
   * Set of model annotations.
   */
  @Cascade({CascadeType.ALL})
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "model_miriam_data_table", joinColumns = {
      @JoinColumn(name = "model_id", referencedColumnName = "id", nullable = false, updatable = false)},
      inverseJoinColumns = {
          @JoinColumn(name = "miriam_data_id", referencedColumnName = "id", nullable = true, updatable = true)})
  @JsonProperty("references")
  private Set<MiriamData> miriamData = new HashSet<>();

  /**
   * List of authors who created map.
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(fetch = FetchType.EAGER)
  @OrderColumn(name = "idx")
  @JoinTable(name = "model_data_table_authors",
      joinColumns = {
          @JoinColumn(name = "model_data_id", referencedColumnName = "id", nullable = false, updatable = false)},
      inverseJoinColumns = {
          @JoinColumn(name = "author_id", referencedColumnName = "id", nullable = true, updatable = true)})
  @JsonProperty("authors")
  private List<Author> authors = new ArrayList<>();

  /**
   * When the map was created.
   */
  @JsonProperty("creationDate")
  private Calendar creationDate = null;

  /**
   * When the map was modified.
   */
  @ElementCollection(fetch = FetchType.EAGER)
  @CollectionTable(name = "model_data_modification_dates", joinColumns = @JoinColumn(name = "model_data_id"))
  @OrderColumn(name = "idx")
  @Column(name = "modification_date")
  @JsonProperty("modificationDates")
  private List<Calendar> modificationDates = new ArrayList<>();

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * Default constructor.
   */
  public ModelData() {
  }

  /**
   * Adds {@link Element} to model data.
   *
   * @param element element to add
   */
  public void addElement(final Element element) {
    element.setModelData(this);
    elements.add(element);
  }

  /**
   * Adds {@link Reaction} to model data.
   *
   * @param reaction reaction to add
   */
  public void addReaction(final Reaction reaction) {
    reaction.setModelData(this);
    reactions.add(reaction);
  }

  /**
   * Adds {@link Layer} to model data.
   *
   * @param layer layer to add
   */
  public void addLayer(final Layer layer) {
    layer.setModel(this);
    layers.add(layer);
  }

  /**
   * Adds collection of {@link Element elements} to model data.
   *
   * @param elements elements to add
   */
  public void addElements(final List<? extends Element> elements) {
    for (final Element element : elements) {
      addElement(element);
    }
  }

  /**
   * @return {@link #id}
   */
  @Override
  public int getId() {
    return id;
  }

  /**
   * @param id new {@link #id}
   */
  public void setId(final int id) {
    this.id = id;
  }

  /**
   * @return {@link #project}
   */
  public Project getProject() {
    return project;
  }

  /**
   * @param project new {@link #project}
   */
  public void setProject(final Project project) {
    this.project = project;
  }

  /**
   * Adds reactions to model.
   *
   * @param reactions2 list of reaction to add
   */
  public void addReactions(final List<Reaction> reactions2) {
    for (final Reaction reaction : reactions2) {
      addReaction(reaction);
    }
  }

  /**
   * Adds collection of {@link Layer layers} to the model data.
   *
   * @param layers objets to add
   */
  public void addLayers(final Collection<Layer> layers) {
    for (final Layer layer : layers) {
      addLayer(layer);
    }
  }

  /**
   * Adds {@link ElementGroup} to the model data.
   *
   * @param elementGroup object to add
   */
  public void addElementGroup(final ElementGroup elementGroup) {
    // for now we ignore this information
  }

  /**
   * Adds {@link BlockDiagram} to the model data.
   *
   * @param blockDiagram object to add
   */
  public void addBlockDiagream(final BlockDiagram blockDiagram) {
    // for now we ignore this information
  }

  /**
   * Removes {@link Element} from the model.
   *
   * @param element element to remove
   */
  public void removeElement(final Element element) {
    if (element == null) {
      throw new InvalidArgumentException("Cannot remove null");
    }
    if (!elements.contains(element)) {
      logger.warn("Element doesn't exist in the map: " + element.getElementId());
      return;
    }

    element.setModelData(null);
    elements.remove(element);
  }

  /**
   * Removes reaction from model.
   *
   * @param reaction reaction to remove
   */
  public void removeReaction(final Reaction reaction) {
    if (!reactions.contains(reaction)) {
      logger.warn("Reaction doesn't exist in the model: " + reaction.getIdReaction());
      return;
    }
    reaction.setModelData(null);
    reactions.remove(reaction);
  }

  /**
   * @return the zoomLevels
   * @see #zoomLevels
   */
  public int getZoomLevels() {
    return zoomLevels;
  }

  @JsonProperty("minZoom")
  public int getMinZoomLevel() {
    return Configuration.MIN_ZOOM_LEVEL;
  }

  @JsonProperty("maxZoom")
  public int getMaxZoomLevel() {
    return Configuration.MIN_ZOOM_LEVEL + zoomLevels;
  }

  /**
   * @param zoomLevels the zoomLevels to set
   * @see #zoomLevels
   */
  public void setZoomLevels(final int zoomLevels) {
    this.zoomLevels = zoomLevels;
  }

  /**
   * @return the tileSize
   * @see #tileSize
   */
  public int getTileSize() {
    return tileSize;
  }

  /**
   * @param tileSize the tileSize to set
   * @see #tileSize
   */
  public void setTileSize(final int tileSize) {
    this.tileSize = tileSize;
  }

  /**
   * @return the idModel
   * @see #idModel
   */
  public String getIdModel() {
    return idModel;
  }

  /**
   * @param idModel the idModel to set
   * @see #idModel
   */
  public void setIdModel(final String idModel) {
    this.idModel = idModel;
  }

  /**
   * @return the model
   * @see #model
   */
  @XmlTransient
  @JsonIgnore
  public Model getModel() {
    return model;
  }

  /**
   * @param model the model to set
   * @see #model
   */
  @JsonIgnore
  public void setModel(final Model model) {
    this.model = model;
  }

  /**
   * @return the elements
   * @see #elements
   */
  public Set<Element> getElements() {
    return elements;
  }

  /**
   * @param elements new {@link #elements} collection
   */
  public void setElements(final Set<Element> elements) {
    this.elements = elements;
  }

  /**
   * @return the layers
   * @see #layers
   */
  public Set<Layer> getLayers() {
    return layers;
  }

  /**
   * @param layers the layers to set
   * @see #layers
   */
  public void setLayers(final Set<Layer> layers) {
    this.layers = layers;
  }

  /**
   * @return the reactions
   * @see #reactions
   */
  public Set<Reaction> getReactions() {
    return reactions;
  }

  /**
   * @param reactions the reactions to set
   * @see #reactions
   */
  public void setReactions(final Set<Reaction> reactions) {
    this.reactions = reactions;
  }

  /**
   * @return the width
   * @see #width
   */
  public double getWidth() {
    return width;
  }

  /**
   * @param width new {@link #width}
   */
  @JsonIgnore
  public void setWidth(final int width) {
    setWidth(Double.valueOf(width));
  }

  /**
   * @param width the width to set
   * @see #width
   */
  public void setWidth(final double width) {
    this.width = width;
  }

  /**
   * @return the height
   * @see #height
   */
  public double getHeight() {
    return height;
  }

  /**
   * @param height new {@link #height}
   */
  @JsonIgnore
  public void setHeight(final int height) {
    setHeight(Double.valueOf(height));
  }

  /**
   * @param height the height to set
   * @see #height
   */
  public void setHeight(final double height) {
    this.height = height;
  }

  /**
   * @return the notes
   * @see #notes
   */
  public String getNotes() {
    return notes;
  }

  /**
   * @param notes the notes to set
   * @see #notes
   */
  public void setNotes(final String notes) {
    this.notes = notes;
  }

  /**
   * Adds submodel.
   *
   * @param submodel object to add
   */
  public void addSubmodel(final ModelSubmodelConnection submodel) {
    this.submodels.add(submodel);
    submodel.setParentModel(this);
  }

  /**
   * Returns collection of submodels.
   *
   * @return collection of submodels.
   */
  public Collection<ModelSubmodelConnection> getSubmodels() {
    return this.submodels;
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   * @see #name
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * @return the parentModels
   * @see #parentModels
   */
  public Set<SubmodelConnection> getParentModels() {
    return parentModels;
  }

  /**
   * @param parentModels the parentModels to set
   * @see #parentModels
   */
  public void setParentModels(final Set<SubmodelConnection> parentModels) {
    this.parentModels = parentModels;
  }

  public Double getDefaultCenterX() {
    return defaultCenterX;
  }

  public void setDefaultCenterX(final Double defaultCenterX) {
    this.defaultCenterX = defaultCenterX;
  }

  public Double getDefaultCenterY() {
    return defaultCenterY;
  }

  public void setDefaultCenterY(final Double defaultCenterY) {
    this.defaultCenterY = defaultCenterY;
  }

  public Integer getDefaultZoomLevel() {
    return defaultZoomLevel;
  }

  public void setDefaultZoomLevel(final Integer defaultZoomLevel) {
    this.defaultZoomLevel = defaultZoomLevel;
  }

  public Set<SbmlFunction> getFunctions() {
    return functions;
  }

  public Set<SbmlParameter> getParameters() {
    return parameters;
  }

  public void addUnits(final Collection<SbmlUnit> units) {
    for (final SbmlUnit sbmlUnit : units) {
      addUnit(sbmlUnit);
    }
  }

  public Set<SbmlUnit> getUnits() {
    return units;
  }

  public void addUnit(final SbmlUnit unit) {
    this.units.add(unit);
    unit.setModel(this);
  }

  public void addParameters(final Collection<SbmlParameter> sbmlParameters) {
    this.parameters.addAll(sbmlParameters);
  }

  public void addParameter(final SbmlParameter sbmlParameter) {
    this.parameters.add(sbmlParameter);
  }

  public void addFunction(final SbmlFunction sbmlFunction) {
    this.functions.add(sbmlFunction);
    sbmlFunction.setModel(this);
  }

  public void addFunctions(final Collection<SbmlFunction> functions) {
    for (final SbmlFunction sbmlFunction : functions) {
      addFunction(sbmlFunction);
    }
  }

  /**
   * Returns set of annotations for the model.
   *
   * @return set of annotations
   */
  public Set<MiriamData> getMiriamData() {
    return miriamData;
  }

  /**
   * Adds annotation to the model.
   *
   * @param md annotation to add
   */
  public void addMiriamData(final MiriamData md) {
    miriamData.add(md);
  }

  public void addAuthor(final Author author) {
    authors.add(author);
  }

  public List<Author> getAuthors() {
    return authors;
  }

  public Calendar getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(final Calendar creationDate) {
    this.creationDate = creationDate;
  }

  public void addModificationDate(final Calendar modificationDate) {
    modificationDates.add(modificationDate);
  }

  public List<Calendar> getModificationDates() {
    return modificationDates;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}