package lcsb.mapviewer.model.map.species.field;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.modelutils.serializer.model.map.ElementAsIdSerializer;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * This class stores basically only uniprot Id which is used as mapping between
 * species and uniprot record.
 *
 * @author David Hoksza
 */

@Entity
public class UniprotRecord implements MinervaEntity, Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * ID of the uniprot record
   */
  @Column(nullable = false, length = 255)
  private String uniprotId = "";

  /**
   * Species to which this uniprot record belongs to.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "element_id", nullable = false)
  @JsonSerialize(using = ElementAsIdSerializer.class)
  private Species species;

  /**
   * List of uniprot records which are associated with this species.
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "uniprot", orphanRemoval = true)
  @Fetch(FetchMode.SUBSELECT)
  private Set<Structure> structures = new HashSet<>();

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * Default constructor.
   */
  public UniprotRecord() {
  }

  /**
   * Constructor that initialize object with the data taken from the parameter.
   *
   * @param ur original object from which data is taken
   */
  public UniprotRecord(final UniprotRecord ur) {
    this.id = ur.id;
    this.uniprotId = ur.uniprotId;
    this.species = ur.species;
    this.entityVersion = 0;
    for (final Structure s : ur.structures) {
      this.structures.add(s.copy());
    }
  }

  /**
   * Creates copy of the object.
   *
   * @return copy of the object.
   */
  public UniprotRecord copy() {
    if (this.getClass() == UniprotRecord.class) {
      return new UniprotRecord(this);
    } else {
      throw new NotImplementedException("Method copy() should be overridden in class " + this.getClass());
    }
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "UniprotRecord [id=" + id + ", uniprotId=" + uniprotId + ", species=" + species + ", structures="
        + structures + "]";
  }

  /**
   * @return the idModificationResidue
   * @see #id
   */
  public int getId() {
    return id;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  /**
   * @param id the id to set
   * @see #id
   */
  public void setId(final int id) {
    this.id = id;
  }

  /**
   * @return the uniprot id
   * @see #uniprotId
   */
  public String getUniprotId() {
    return uniprotId;
  }

  public void setUniprotId(final String uniprotId) {
    this.uniprotId = uniprotId;
  }

  /**
   * @return the species
   * @see #species
   */
  public Species getSpecies() {
    return species;
  }

  /**
   * @param species species to which this uniprot record belongs
   * @see #species
   */
  public void setSpecies(final Species species) {
    this.species = species;
  }

  /**
   * @return the structures
   * @see #structures
   */
  public Set<Structure> getStructures() {
    return structures;
  }

  /**
   * @param structures set of structures mapped to this uniprot record
   * @see #structures
   */
  public void setStructures(final Set<Structure> structures) {
    this.structures = structures;
  }

  public void addStructures(final Collection<Structure> structures) {
    for (final Structure structure : structures) {
      addStructure(structure);
    }
  }

  public void addStructure(final Structure structure) {
    this.structures.add(structure);
    structure.setUniprot(this);
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
