package lcsb.mapviewer.model.map.species.field;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.model.map.sbo.SBOTermModificationResidueType;

@Entity
@DiscriminatorValue("STRUCTURAL_STATE")
public class StructuralState extends AbstractRegionModification {

  private static final long serialVersionUID = 1L;

  public StructuralState(final String idModificationResidue) {
    super(idModificationResidue);
  }

  public StructuralState() {
  }

  public StructuralState(final StructuralState mr) {
    super(mr);
  }

  @Override
  public StructuralState copy() {
    return new StructuralState(this);
  }

  @Override
  public String getSboTerm() {
    return SBOTermModificationResidueType.STRUCTURAL_STATE.getSBO();
  }

}
