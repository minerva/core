package lcsb.mapviewer.model.map.species;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.sbo.SBOTermSpeciesType;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;
import lcsb.mapviewer.model.map.species.field.SpeciesWithCodingRegion;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationSite;
import lcsb.mapviewer.model.map.species.field.SpeciesWithProteinBindingDomain;

/**
 * Entity representing antisense rna element on the map.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@DiscriminatorValue("ANTISENSE_RNA")
public class AntisenseRna extends Species
    implements SpeciesWithCodingRegion, SpeciesWithModificationSite, SpeciesWithProteinBindingDomain {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * List of {@link AntisenseRnaRegion regions} related to this
   * {@link AntisenseRna}.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "species")
  @LazyCollection(LazyCollectionOption.FALSE)
  @Fetch(FetchMode.SUBSELECT)
  private List<ModificationResidue> modificationResidues = new ArrayList<>();

  /**
   * Empty constructor required by hibernate.
   */
  AntisenseRna() {
  }

  /**
   * Constructor that creates a copy of the element given in the parameter.
   * 
   * @param original
   *          original object that will be used for creating copy
   */
  public AntisenseRna(final AntisenseRna original) {
    super(original);
    for (final ModificationResidue region : original.getModificationResidues()) {
      addModificationResidue(region.copy());
    }
  }

  /**
   * Default constructor.
   * 
   * @param elementId
   *          unique (within model) element identifier
   */
  public AntisenseRna(final String elementId) {
    setElementId(elementId);
  }

  /**
   * Adds {@link AntisenseRnaRegion} to the object.
   * 
   * @param antisenseRnaRegion
   *          region to be added
   */
  private void addModificationResidue(final ModificationResidue antisenseRnaRegion) {
    modificationResidues.add(antisenseRnaRegion);
    antisenseRnaRegion.setSpecies(this);
  }

  @Override
  public void addCodingRegion(final CodingRegion codingRegion) {
    this.addModificationResidue(codingRegion);
  }

  @Override
  public void addProteinBindingDomain(final ProteinBindingDomain codingRegion) {
    this.addModificationResidue(codingRegion);
  }

  @Override
  public void addModificationSite(final ModificationSite codingRegion) {
    this.addModificationResidue(codingRegion);
  }

  @Override
  public AntisenseRna copy() {
    if (this.getClass() == AntisenseRna.class) {
      return new AntisenseRna(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * @param regions
   *          the regions to set
   * @see #regions
   */
  public void setRegions(final List<ModificationResidue> regions) {
    this.modificationResidues = regions;
  }

  @Override
  public List<ModificationResidue> getModificationResidues() {
    return modificationResidues;
  }

  @Override
  @JsonIgnore
  public String getStringType() {
    return "Antisense RNA";
  }

  @Override
  public String getSboTerm() {
    return SBOTermSpeciesType.ANTISENSE_RNA.getSBO();
  }

}
