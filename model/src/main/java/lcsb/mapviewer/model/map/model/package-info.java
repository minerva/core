/**
 * Contains map structures and structures used for connecting maps and submaps.
 */
package lcsb.mapviewer.model.map.model;
