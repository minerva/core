package lcsb.mapviewer.model.map.sbo;

import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.CatalysisReaction;
import lcsb.mapviewer.model.map.reaction.type.DissociationReaction;
import lcsb.mapviewer.model.map.reaction.type.HeterodimerAssociationReaction;
import lcsb.mapviewer.model.map.reaction.type.InhibitionReaction;
import lcsb.mapviewer.model.map.reaction.type.KnownTransitionOmittedReaction;
import lcsb.mapviewer.model.map.reaction.type.ModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.NegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.PhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.PositiveInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedPhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.ReducedTriggerReaction;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.reaction.type.TranscriptionReaction;
import lcsb.mapviewer.model.map.reaction.type.TranslationReaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.reaction.type.TriggerReaction;
import lcsb.mapviewer.model.map.reaction.type.TruncationReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownCatalysisReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownInhibitionReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownNegativeInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownPositiveInfluenceReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownReducedModulationReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownReducedPhysicalStimulationReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownReducedTriggerReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownTransitionReaction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public enum SBOTermReactionType {
  CATALYSIS(CatalysisReaction.class, new String[]{"SBO:0000013"}),
  DISSOCIATION(DissociationReaction.class, new String[]{"SBO:0000180"}),
  HETERODIMER_ASSOCIATION(HeterodimerAssociationReaction.class, new String[]{"SBO:0000177"}),
  INHIBITION(InhibitionReaction.class, new String[]{"SBO:0000537"}),
  KNOWN_TRANSITION_OMITTED(KnownTransitionOmittedReaction.class, new String[]{"SBO:0000205"}),
  MODULATION(ModulationReaction.class, new String[]{"SBO:0000594"}),
  NEGATIVE_INFLUENCE(NegativeInfluenceReaction.class, new String[]{"SBO:0000407"}),
  PHYSICAL_STIMULATION(PhysicalStimulationReaction.class, new String[]{"SBO:0000459"}),
  POSITIVE_INFLUENCE(PositiveInfluenceReaction.class, new String[]{"SBO:0000171"}),
  REDUCED_MODULATION(ReducedModulationReaction.class, new String[]{"SBO:0000632"}),
  REDUCED_PHYSICAL_STIMULATION(ReducedPhysicalStimulationReaction.class, new String[]{"SBO:0000411"}),
  REDUCED_TRIGGER(ReducedTriggerReaction.class, new String[]{"SBO:0000533"}),
  STATE_TRANSITION(StateTransitionReaction.class, new String[]{"SBO:0000176"}),
  TRANSCRIPTION(TranscriptionReaction.class, new String[]{"SBO:0000183"}),
  TRANSLATION(TranslationReaction.class, new String[]{"SBO:0000184"}),
  TRANSPORT(TransportReaction.class, new String[]{"SBO:0000185"}),
  TRIGGER(TriggerReaction.class, new String[]{"SBO:0000461"}),
  TRUNCATION(TruncationReaction.class, new String[]{"SBO:0000178"}),
  UNKNOWN_CATALYSIS(UnknownCatalysisReaction.class, new String[]{"SBO:0000462"}),
  UNKNOWN_INHIBITION(UnknownInhibitionReaction.class, new String[]{"SBO:0000536"}),
  UNKNOWN_NEGATIVE_INFLUENCE(UnknownNegativeInfluenceReaction.class, new String[]{"SBO:0000169"}),
  UNKNOWN_POSITIVE_INFLUENCE(UnknownPositiveInfluenceReaction.class, new String[]{"SBO:0000172"}),
  UNKNOWN_REDUCED_MODULATION(UnknownReducedModulationReaction.class, new String[]{"SBO:0000631"}),
  UNKNOWN_REDUCED_PHYSICAL_STIMULATION(UnknownReducedPhysicalStimulationReaction.class, new String[]{"SBO:0000170"}),
  UNKNOWN_REDUCED_TRIGGER(UnknownReducedTriggerReaction.class, new String[]{"SBO:0000534"}),
  UNKNOWN_TRANSITION(UnknownTransitionReaction.class, new String[]{"SBO:0000396"}),
  ;

  private static final Logger logger = LogManager.getLogger();
  private final Class<? extends Reaction> clazz;
  private final List<String> sboTerms = new ArrayList<>();

  SBOTermReactionType(final Class<? extends Reaction> clazz, final String[] inputSboTerms) {
    this.clazz = clazz;
    Collections.addAll(sboTerms, inputSboTerms);
  }

  public static Class<? extends Reaction> getTypeSBOTerm(final String sboTerm, final Marker logMarker) {
    if (sboTerm == null || sboTerm.isEmpty()) {
      return StateTransitionReaction.class;
    }
    Class<? extends Reaction> result = null;
    for (final SBOTermReactionType term : values()) {
      for (final String string : term.sboTerms) {
        if (string.equalsIgnoreCase(sboTerm)) {
          result = term.clazz;
          break;
        }
      }
    }
    if (result == null) {
      logger.warn(logMarker, "Don't know how to handle SBOTerm " + sboTerm + " for reaction");
      result = StateTransitionReaction.class;
    }
    return result;
  }

  public static Class<? extends Reaction> getTypeForSBOTerm(final String sboTerm) {
    if (sboTerm == null || sboTerm.isEmpty()) {
      return StateTransitionReaction.class;
    }
    Class<? extends Reaction> result = null;
    for (final SBOTermReactionType term : values()) {
      for (final String string : term.sboTerms) {
        if (string.equalsIgnoreCase(sboTerm)) {
          result = term.clazz;
          break;
        }
      }
    }
    if (result == null) {
      result = StateTransitionReaction.class;
    }
    return result;
  }

  public String getSBO() {
    if (!sboTerms.isEmpty()) {
      return sboTerms.get(0);
    }
    logger.warn("Cannot find SBO term for class: {}", clazz);
    return null;
  }
}
