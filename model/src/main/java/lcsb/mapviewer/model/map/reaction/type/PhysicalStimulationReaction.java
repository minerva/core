package lcsb.mapviewer.model.map.reaction.type;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines a standard CellDesigner physical stimulation reaction.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("PHYSICAL_STIMULATION_REACTION")
public class PhysicalStimulationReaction extends Reaction implements SimpleReactionInterface, ModifierReactionNotation {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  protected PhysicalStimulationReaction() {
    super();
  }

  public PhysicalStimulationReaction(final String elementId) {
    super(elementId);
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   *
   * @param result parent reaction from which we copy data
   */
  public PhysicalStimulationReaction(final Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Physical stimulation";
  }

  @Override
  public ReactionRect getReactionRect() {
    return null;
  }

  @Override
  public PhysicalStimulationReaction copy() {
    if (this.getClass() == PhysicalStimulationReaction.class) {
      return new PhysicalStimulationReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermReactionType.PHYSICAL_STIMULATION.getSBO();
  }
}
