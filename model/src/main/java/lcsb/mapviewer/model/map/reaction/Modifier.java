package lcsb.mapviewer.model.map.reaction;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;

/**
 * Represents modifier (ie. catalyst) in the {@link Reaction}. It is an
 * implementation of more general {@link ReactionNode} class which describes the
 * node.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("MODIFIER_NODE")
public class Modifier extends ReactionNode {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  protected Modifier() {
    super();
  }

  /**
   * Constructor that creates modifier for given {@link Species}. These elements
   * reference to the objects in the
   * {@link lcsb.mapviewer.model.map.model.db.model.map.Model Model} that
   * represents this modifier.
   * 
   * @param element
   *          element that represent this modifier
   */
  public Modifier(final Element element) {
    super(element);
  }

  /**
   * Constructor that initialize the modifier with data from parameter.
   * 
   * @param original
   *          original modifier used for data initialization
   */
  protected Modifier(final Modifier original) {
    super(original);
  }

  @Override
  public Modifier copy() {
    if (this.getClass() == Modifier.class) {
      return new Modifier(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }
}
