package lcsb.mapviewer.model.map.model;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.CalendarComparator;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.comparator.IntegerComparator;
import lcsb.mapviewer.common.comparator.ListComparator;
import lcsb.mapviewer.common.comparator.SetComparator;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamDataComparator;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlFunctionComparator;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.kinetics.SbmlParameterComparator;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitComparator;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerComparator;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionComparator;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.ElementComparator;
import lcsb.mapviewer.modelutils.map.ElementUtils;

/**
 * Comparator for {@link Model} class.
 * 
 * @author Piotr Gawron
 * 
 */
public class ModelComparator extends Comparator<Model> {
  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  private SetComparator<SbmlUnit> unitSetComparator = new SetComparator<>(new SbmlUnitComparator());

  private SetComparator<Reaction> reactionSetComparator;

  private ElementComparator elementComparator;

  private ElementUtils eu = new ElementUtils();

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public ModelComparator(final double epsilon) {
    super(Model.class);
    this.epsilon = epsilon;
    reactionSetComparator = new SetComparator<>(new ReactionComparator(epsilon));
    elementComparator = new ElementComparator(epsilon);
  }

  /**
   * Default constructor.
   */
  public ModelComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(final Model arg0, final Model arg1) {
    StringComparator stringComparator = new StringComparator();
    DoubleComparator doubleComparator = new DoubleComparator(epsilon);
    IntegerComparator integerComparator = new IntegerComparator();

    if (stringComparator.compare(arg0.getIdModel(), arg1.getIdModel()) != 0) {
      logger.debug("Id different: " + arg0.getIdModel() + ", " + arg1.getIdModel());
      return stringComparator.compare(arg0.getIdModel(), arg1.getIdModel());
    }

    if (stringComparator.compare(arg0.getName(), arg1.getName()) != 0) {
      logger.debug("Name different: " + arg0.getName() + ", " + arg1.getName());
      return stringComparator.compare(arg0.getName(), arg1.getName());
    }

    if (stringComparator.compare(arg0.getNotes(), arg1.getNotes(), true) != 0) {
      logger.debug("Notes different:\n" + arg0.getNotes() + "\n---\n" + arg1.getNotes() + "\n---");
      return stringComparator.compare(arg0.getNotes(), arg1.getNotes(), true);
    }

    if (doubleComparator.compare(arg0.getWidth(), arg1.getWidth()) != 0) {
      logger.debug("Width different: " + arg0.getWidth() + ", " + arg1.getWidth());
      return doubleComparator.compare(arg0.getWidth(), arg1.getWidth());
    }

    if (doubleComparator.compare(arg0.getHeight(), arg1.getHeight()) != 0) {
      logger.debug("Height different: " + arg0.getHeight() + ", " + arg1.getHeight());
      return doubleComparator.compare(arg0.getHeight(), arg1.getHeight());
    }

    if (integerComparator.compare(arg0.getZoomLevels(), arg1.getZoomLevels()) != 0) {
      logger.debug("Zoom levels different: " + arg0.getZoomLevels() + ", " + arg1.getZoomLevels());
      return integerComparator.compare(arg0.getZoomLevels(), arg1.getZoomLevels());
    }

    if (integerComparator.compare(arg0.getTileSize(), arg1.getTileSize()) != 0) {
      logger.debug("Tile size different: " + arg0.getTileSize() + ", " + arg1.getTileSize());
      return integerComparator.compare(arg0.getTileSize(), arg1.getTileSize());
    }

    int status = compareElements(arg0.getElements(), arg1.getElements());
    if (status != 0) {
      logger.debug("Set of elements different");
      return status;
    }

    SetComparator<Layer> layerSetComparator = new SetComparator<Layer>(new LayerComparator(epsilon)); 

    status = layerSetComparator.compare(arg0.getLayers(), arg1.getLayers());
    if (status != 0) {
      logger.debug("Set of layers different");
      return status;
    }

    status = reactionSetComparator.compare(arg0.getReactions(), arg1.getReactions());
    if (status != 0) {
      logger.debug("Set of reactions different");
      return status;
    }

    status = compareSubmodels(arg0.getSubmodelConnections(), arg1.getSubmodelConnections());
    if (status != 0) {
      logger.debug("Set of submodels different");
      return status;
    }

    status = unitSetComparator.compare(arg0.getUnits(), arg1.getUnits());
    if (status != 0) {
      logger.debug("units different");
      return status;
    }
    SetComparator<SbmlFunction> functionSetComparator = new SetComparator<>(new SbmlFunctionComparator());
    status = functionSetComparator.compare(arg0.getFunctions(), arg1.getFunctions());
    if (status != 0) {
      logger.debug("functions different");
      return status;
    }
    SetComparator<SbmlParameter> parameterSetComparator = new SetComparator<>(new SbmlParameterComparator());
    status = parameterSetComparator.compare(arg0.getParameters(), arg1.getParameters());
    if (status != 0) {
      logger.debug("parameters different");
      return status;
    }

    SetComparator<MiriamData> miriamDataSetComparator = new SetComparator<>(new MiriamDataComparator());

    status = miriamDataSetComparator.compare(arg0.getMiriamData(), arg1.getMiriamData());
    if (status != 0) {
      logger.debug("miriam data different");
      logger.debug(arg0.getMiriamData());
      logger.debug(arg1.getMiriamData());
      return status;
    }

    ListComparator<Author> authorSetComparator = new ListComparator<>(new AuthorComparator());

    status = authorSetComparator.compare(arg0.getAuthors(), arg1.getAuthors());
    if (status != 0) {
      logger.debug("authors different");
      return status;
    }

    CalendarComparator calendarComparator = new CalendarComparator();

    status = calendarComparator.compare(arg0.getCreationDate(), arg1.getCreationDate());
    if (status != 0) {
      logger.debug("creation date different");
      return status;
    }

    ListComparator<Calendar> calendarListComparator = new ListComparator<>(calendarComparator);

    status = calendarListComparator.compare(arg0.getModificationDates(), arg1.getModificationDates());
    if (status != 0) {
      logger.debug("modification dates different");
      return status;
    }

    return 0;
  }

  /**
   * Compares two sets of elements.
   * 
   * @param elements
   *          first set of elements
   * @param elements2
   *          second set of elements
   * @return if sets are equal then returns 0. If they are different then -1/1
   *         is returned.
   */
  private int compareElements(final Set<Element> elements, final Set<Element> elements2) {

    Map<String, Element> map1 = new HashMap<>();
    Map<String, Element> map2 = new HashMap<>();

    if (elements.size() != elements2.size()) {
      logger.debug("Number of elements different: " + elements.size() + ", " + elements2.size());
      return ((Integer) elements.size()).compareTo(elements2.size());
    }

    for (final Element element : elements) {
      map1.put(element.getElementId(), element);
    }

    for (final Element element : elements2) {
      map2.put(element.getElementId(), element);
    }

    for (final Element element : elements) {
      int status = elementComparator.compare(element, map2.get(element.getElementId()));
      if (status != 0) {
        logger.debug(eu.getElementTag(element) + "Element doesn't have a match");
        return status;
      }
    }

    for (final Element element : elements2) {
      int status = elementComparator.compare(element, map1.get(element.getElementId()));
      if (status != 0) {
        logger.debug(eu.getElementTag(element) + "Element doesn't have a match");
        return status;
      }
    }
    return 0;
  }

  /**
   * Compares two collection of models.
   * 
   * @param collection1
   *          first collection to compare
   * @param collection2
   *          second collection to compare
   * @return 0 if the collections are identical, -1/1 otherwise
   */
  private int compareSubmodels(final Collection<ModelSubmodelConnection> collection1,
      final Collection<ModelSubmodelConnection> collection2) {
    IntegerComparator integerComparator = new IntegerComparator();
    if (integerComparator.compare(collection1.size(), collection2.size()) != 0) {
      logger.debug("collection of submodels doesn't match: " + collection1.size() + ", " + collection2.size());
      return integerComparator.compare(collection1.size(), collection2.size());
    }
    ModelSubmodelConnectionComparator comparator = new ModelSubmodelConnectionComparator(epsilon);
    for (final ModelSubmodelConnection submodel1 : collection1) {
      boolean found = false;
      for (final ModelSubmodelConnection submodel2 : collection2) {
        ModelData parent1 = submodel1.getParentModel();
        submodel1.setParentModel((ModelData) null);
        ModelData parent2 = submodel2.getParentModel();
        submodel2.setParentModel((ModelData) null);

        if (comparator.compare(submodel1, submodel2) == 0) {
          found = true;
        }
        submodel1.setParentModel(parent1);
        submodel2.setParentModel(parent2);
        if (found) {
          break;
        }
      }
      if (!found) {
        logger.debug("collection of submodels doesn't match. " + submodel1 + " cannot be found in the second model");
        return 1;
      }
    }

    for (final ModelSubmodelConnection submodel1 : collection2) {
      boolean found = false;
      for (final ModelSubmodelConnection submodel2 : collection1) {
        ModelData parent1 = submodel1.getParentModel();
        submodel1.setParentModel((ModelData) null);
        ModelData parent2 = submodel2.getParentModel();
        submodel2.setParentModel((ModelData) null);

        if (comparator.compare(submodel1, submodel2) == 0) {
          found = true;
        }
        submodel1.setParentModel(parent1);
        submodel2.setParentModel(parent2);
        if (found) {
          break;
        }
      }
      if (!found) {
        logger.debug("collection of submodels doesn't match. " + submodel1 + " cannot be found in the first model");
        return -1;
      }
    }

    return 0;
  }

  public SetComparator<SbmlUnit> getUnitSetComparator() {
    return unitSetComparator;
  }

  public void setUnitSetComparator(final SetComparator<SbmlUnit> unitSetComparator) {
    this.unitSetComparator = unitSetComparator;
  }

  public SetComparator<Reaction> getReactionSetComparator() {
    return reactionSetComparator;
  }

  public void setReactionSetComparator(final SetComparator<Reaction> reactionSetComparator) {
    this.reactionSetComparator = reactionSetComparator;
  }

  public ElementComparator getElementComparator() {
    return elementComparator;
  }

  public void setElementComparator(final ElementComparator elementComparator) {
    this.elementComparator = elementComparator;
  }
}
