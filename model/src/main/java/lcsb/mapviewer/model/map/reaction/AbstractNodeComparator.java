package lcsb.mapviewer.model.map.reaction;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.graphics.PolylineDataComparator;

/**
 * This class implements comparator interface for AbstractNode. It also handles
 * comparison of subclasses of AbstractNode class.
 * 
 * @author Piotr Gawron
 * 
 */
public class AbstractNodeComparator extends Comparator<AbstractNode> {
  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  private boolean ignoreLayout;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public AbstractNodeComparator(final double epsilon, final boolean ignoreLayout) {
    super(AbstractNode.class, true);
    this.epsilon = epsilon;
    this.ignoreLayout = ignoreLayout;
    addSubClassComparator(new NodeOperatorComparator(epsilon, ignoreLayout));
    addSubClassComparator(new ReactionNodeComparator(epsilon, ignoreLayout));
  }

  public AbstractNodeComparator(final double epsilon) {
    this(epsilon, false);
  }

  /**
   * Default constructor.
   */
  public AbstractNodeComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(final AbstractNode arg0, final AbstractNode arg1) {
    if (!ignoreLayout) {
      PolylineDataComparator pdComparator = new PolylineDataComparator(epsilon);
      if (pdComparator.compare(arg0.getLine(), arg1.getLine()) != 0) {
        logger.debug("Different lines:");
        logger.debug(arg0.getLine());
        logger.debug(arg1.getLine());
        return pdComparator.compare(arg0.getLine(), arg1.getLine());
      }
    }
    return 0;
  }

}
