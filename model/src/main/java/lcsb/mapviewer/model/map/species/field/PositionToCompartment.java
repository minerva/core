package lcsb.mapviewer.model.map.species.field;

/**
 * Defines possible position of
 * {@link lcsb.mapviewer.converter.model.celldesigner.structure.Species} on
 * {@link lcsb.mapviewer.model.map.agregator.converter.model.celldesigner.structure.Compartment}
 * .
 * 
 * @author Piotr Gawron
 * 
 */
public enum PositionToCompartment {

  /**
   * Species is inside compartment.
   */
  INSIDE("inside"),

  /**
   * Species is inside the border.
   */
  INSIDE_OF_MEMBRANE("insideOfMembrane"),

  /**
   * Species is crossing the border.
   */
  TRANSMEMBRANE("transmembrane"),

  /**
   * Species is on outer border of compartment.
   */
  OUTER_SURFACE("outerSurface"),

  /**
   * Species is on inner border of compartment.
   */
  INNER_SURFACE("innerSurface");

  /**
   * String representing the position.
   */
  private String stringName;

  /**
   * Default constructor.
   * 
   * @param stringName
   *          {@link #stringName}
   */
  PositionToCompartment(final String stringName) {
    this.stringName = stringName;
  }

  /**
   * Returns {@link PositionToCompartment} based on the {@link #stringName}.
   *
   * @param string
   *          {@link #stringName}
   * @return {@link PositionToCompartment} based on the {@link #stringName}
   */
  public static PositionToCompartment getByString(final String string) {
    for (final PositionToCompartment pos : PositionToCompartment.values()) {
      if (pos.getStringName().equalsIgnoreCase(string)) {
        return pos;
      }
    }
    return null;
  }

  /**
   * @return the stringName
   * @see #stringName
   */
  public String getStringName() {
    return stringName;
  }
}
