package lcsb.mapviewer.model.map.layout;

/**
 * Status available for the layouts (based on the processing progress).
 * 
 * @author Piotr Gawron
 * 
 */
public enum ProjectBackgroundStatus {

  /**
   * Default unknown status.
   */
  UNKNOWN("Unknown"),

  /**
   * {@link ProjectBackground} is not available.
   */
  NA("Not available"),

  /**
   * {@link ProjectBackground} is generating.
   */
  GENERATING("Generating"),

  /**
   * {@link ProjectBackground} is ready.
   */
  OK("OK"),

  /**
   * There was a problem during layout generation.
   */
  FAILURE("Failure");

  /**
   * Common name of the status.
   */
  private String commonName;

  /**
   * Default constructor.
   *
   * @param commonName
   *          {@link #commonName}
   */
  ProjectBackgroundStatus(final String commonName) {
    this.commonName = commonName;
  }

  /**
   *
   * @return {@link #commonName}
   */
  public String getCommonName() {
    return commonName;
  }

}
