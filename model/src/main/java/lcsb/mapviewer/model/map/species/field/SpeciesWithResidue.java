package lcsb.mapviewer.model.map.species.field;

/**
 * Interface implemented by species that support {@link Residue} modification.
 * 
 * @author Piotr Gawron
 *
 */
public interface SpeciesWithResidue extends SpeciesWithModificationResidue {

  /**
   * Adds a {@link Residue} to the species.
   * 
   * @param residue
   *          {@link Residue} to add
   */
  void addResidue(final Residue residue);

}
