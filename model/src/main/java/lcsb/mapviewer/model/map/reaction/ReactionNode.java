package lcsb.mapviewer.model.map.reaction;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.modelutils.serializer.model.map.ElementAsIdSerializer;

/**
 * One of two known types of nodes in the {@link Reaction}. It defines input or
 * output element of the reaction in the map model. {@link #element} define
 * which element on the map correspond to this node. There are three known
 * subclasses:
 * <ul>
 * <li>{@link Reactant} - input of the reaction,</li>
 * <li>{@link Product} - output of the reaction,</li>
 * <li>{@link Modifier} - some modifier of the reaction.</li>
 * </ul>
 * 
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("GENERIC_REACTION_NODE")
public abstract class ReactionNode extends AbstractNode {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  private Double stoichiometry = null;

  /**
   * {@link Element} that represents this node in the model.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JsonSerialize(using = ElementAsIdSerializer.class)
  private Element element;

  /**
   * Default constructor.
   */
  protected ReactionNode() {
    super();
  }

  /**
   * Constructor that creates a copy of the object in the parameter.
   * 
   * @param node
   *          original node
   */
  protected ReactionNode(final ReactionNode node) {
    super(node);
    this.element = node.getElement();
    this.stoichiometry = node.getStoichiometry();
  }

  /**
   * Constructor that creates node for given {@link #element}.
   * 
   * @param element
   *          {@link Element} to which this node refer to
   */
  public ReactionNode(final Element element) {
    this.element = element;
  }

  /**
   * @return the element
   * @see #element
   */
  public Element getElement() {
    return element;
  }

  /**
   * @param element
   *          the element to set
   * @see #element
   */
  public void setElement(final Element element) {
    this.element = element;
  }

  public Double getStoichiometry() {
    return stoichiometry;
  }

  public void setStoichiometry(final Double stoichiometry) {
    this.stoichiometry = stoichiometry;
  }

  @Override
  public String toString() {
    return new ElementUtils().getElementTag(this);
  }

}
