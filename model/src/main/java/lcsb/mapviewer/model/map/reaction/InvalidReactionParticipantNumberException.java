package lcsb.mapviewer.model.map.reaction;

import lcsb.mapviewer.common.exception.InvalidArgumentException;

/**
 * Exception to be thrown when number of reactants or products is invalid when
 * creating reaction.
 * 
 * @author Piotr Gawron
 * 
 */
public class InvalidReactionParticipantNumberException extends InvalidArgumentException {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor - initializes instance variable to unknown.
   */

  public InvalidReactionParticipantNumberException() {
    super(); // call superclass constructor
  }

  /**
   * Public constructor with parent exception that was caught.
   * 
   * @param e
   *          parent exception
   */

  public InvalidReactionParticipantNumberException(final String e) {
    super(e);
  }

  /**
   * Public constructor with parent exception that was caught.
   * 
   * @param e
   *          parent exception
   */
  public InvalidReactionParticipantNumberException(final Exception e) {
    super(e);
  }

  /**
   * Public constructor with parent exception that was caught.
   * 
   * @param message
   *          exception message
   * @param e
   *          parent exception
   */
  public InvalidReactionParticipantNumberException(final String message, final Exception e) {
    super(message, e);
  }

}
