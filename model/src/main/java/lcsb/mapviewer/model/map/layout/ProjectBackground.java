package lcsb.mapviewer.model.map.layout;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.modelutils.serializer.id.ProjectAsIdSerializer;
import lcsb.mapviewer.modelutils.serializer.id.UserAsIdSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

/**
 * This object represents type of visualization for the model.
 *
 * @author Piotr Gawron
 */
@Entity
public class ProjectBackground implements MinervaEntity {
  public static final Comparator<ProjectBackground> ID_COMPARATOR = new Comparator<ProjectBackground>() {

    @Override
    public int compare(final ProjectBackground o1, final ProjectBackground o2) {
      return o1.getId() - o2.getId();
    }
  };
  public static final Comparator<? super ProjectBackground> ORDER_COMPARATOR = new Comparator<ProjectBackground>() {

    @Override
    public int compare(final ProjectBackground o1, final ProjectBackground o2) {
      return o1.getOrderIndex() - o2.getOrderIndex();
    }
  };
  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static final Logger logger = LogManager.getLogger();

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Title of this layout.
   */
  @Column(length = 255)
  private String name;

  /**
   * Is the overlay considered as default (should it be open on startup).
   */
  private boolean defaultOverlay = false;

  /**
   * Does the layout present data in hierarchical view.
   */
  @JsonIgnore
  private boolean hierarchicalView = false;

  /**
   * Project to which this data overlay is assigned.
   */
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JsonSerialize(using = ProjectAsIdSerializer.class)
  private Project project;

  @ManyToOne(fetch = FetchType.EAGER, optional = false)
  @JsonSerialize(using = UserAsIdSerializer.class)
  private User creator;

  /**
   * Describes progress of layout uploading process.
   */
  @Enumerated(EnumType.STRING)
  private ProjectBackgroundStatus status = ProjectBackgroundStatus.UNKNOWN;

  /**
   * What is the progress.
   */
  private double progress = 0.0;

  private int orderIndex = 0;

  /**
   * List of image folders for generated background images.
   */
  @Cascade({CascadeType.SAVE_UPDATE})
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "projectBackground", orphanRemoval = true)
  private Set<ProjectBackgroundImageLayer> backgrounds = new HashSet<>();

  /**
   * Short description used for this layout.
   */
  @Column(columnDefinition = "TEXT")
  private String description;

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * Default constructor.
   */
  public ProjectBackground() {

  }

  /**
   * Constructor that initializes object with basic parameters.
   *
   * @param title title of the layout
   */
  public ProjectBackground(final String title) {
    this.name = title;
  }

  /**
   * Constructor that initializes object with the copy of the data from
   * parameter.
   *
   * @param layout from this object the data will be initialized
   */
  public ProjectBackground(final ProjectBackground layout) {
    this.description = layout.getDescription();
    this.name = layout.getName();
    this.status = layout.status;
    this.progress = layout.progress;
    this.hierarchicalView = layout.hierarchicalView;
    this.orderIndex = layout.orderIndex;
    for (final ProjectBackgroundImageLayer dataOverlayImageLayer : layout.getProjectBackgroundImageLayer()) {
      ProjectBackgroundImageLayer dataOverlayImageLayerCopy = new ProjectBackgroundImageLayer(
          dataOverlayImageLayer.getModel(),
          dataOverlayImageLayer.getDirectory());
      this.addProjectBackgroundImageLayer(dataOverlayImageLayerCopy);
    }
  }

  public void addProjectBackgroundImageLayer(final ProjectBackgroundImageLayer projectBackgroundImageLayer) {
    this.backgrounds.add(projectBackgroundImageLayer);
    projectBackgroundImageLayer.setProjectBackground(this);
  }

  /**
   * Prepares a copy of layout.
   *
   * @return copy of layout
   */
  public ProjectBackground copy() {
    if (this.getClass() == ProjectBackground.class) {
      return new ProjectBackground(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * @return the hierarchicalView
   * @see #hierarchicalView
   */
  public boolean isHierarchicalView() {
    return hierarchicalView;
  }

  /**
   * @param hierarchicalView the hierarchicalView to set
   * @see #hierarchicalView
   */
  public void setHierarchicalView(final boolean hierarchicalView) {
    this.hierarchicalView = hierarchicalView;
  }

  /**
   * @return the id
   * @see #id
   */
  @Override
  public int getId() {
    return id;
  }

  /**
   * @param id the id to set
   * @see #id
   */
  public void setId(final int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  /**
   * @return the creator
   * @see #creator
   */
  public User getCreator() {
    return creator;
  }

  /**
   * @param creator the creator to set
   * @see #creator
   */
  public void setCreator(final User creator) {
    this.creator = creator;
  }

  /**
   * @return the status
   * @see #status
   */
  public ProjectBackgroundStatus getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   * @see #status
   */
  public void setStatus(final ProjectBackgroundStatus status) {
    this.status = status;
  }

  /**
   * @return the progress
   * @see #progress
   */
  public double getProgress() {
    return progress;
  }

  /**
   * @param progress the progress to set
   * @see #progress
   */
  public void setProgress(final double progress) {
    this.progress = progress;
  }

  /**
   * @return the description
   * @see #description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description the description to set
   * @see #description
   */
  public void setDescription(final String description) {
    this.description = description;
  }

  public boolean isDefaultOverlay() {
    return defaultOverlay;
  }

  public void setDefaultOverlay(final boolean defaultOverlay) {
    this.defaultOverlay = defaultOverlay;
  }

  @JsonProperty("order")
  public int getOrderIndex() {
    return orderIndex;
  }

  public void setOrderIndex(final int orderIndex) {
    this.orderIndex = orderIndex;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(final Project project) {
    Project oldProject = this.project;
    this.project = project;
    if (project == null && oldProject != null) {
      oldProject.removeProjectBackground(this);
    }
  }

  @JsonProperty("images")
  public Set<ProjectBackgroundImageLayer> getProjectBackgroundImageLayer() {
    return backgrounds;
  }

  public void setProjectBackgroundImageLayer(final Set<ProjectBackgroundImageLayer> dataOverlayImageLayers) {
    this.backgrounds = dataOverlayImageLayers;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
