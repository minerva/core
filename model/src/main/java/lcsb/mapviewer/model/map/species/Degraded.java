package lcsb.mapviewer.model.map.species;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.sbo.SBOTermSpeciesType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Entity representing degraded element on the map.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("DEGRADED")
public class Degraded extends Species {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Empty constructor required by hibernate.
   */
  Degraded() {
  }

  /**
   * Constructor that creates a copy of the element given in the parameter.
   *
   * @param original original object that will be used for creating copy
   */
  public Degraded(final Degraded original) {
    super(original);
  }

  /**
   * Default constructor.
   *
   * @param elementId unique (within model) element identifier
   */
  public Degraded(final String elementId) {
    super.setElementId(elementId);
  }

  @Override
  public Degraded copy() {
    if (this.getClass() == Degraded.class) {
      return new Degraded(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  @JsonIgnore
  public String getStringType() {
    return "Degraded";
  }

  @Override
  public String getSboTerm() {
    return SBOTermSpeciesType.DEGRADED.getSBO();
  }

  @Override
  public String getName() {
    return " "; //blank name is required in some cases (degraded does not have a name)
  }

  @Override
  public void setName(final String name) {
    //degraded cannot have name
  }

}
