package lcsb.mapviewer.model.map.kinetics;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.comparator.EnumComparator;
import lcsb.mapviewer.common.comparator.IntegerComparator;

public class SbmlUnitTypeFactorComparator extends Comparator<SbmlUnitTypeFactor> {

  protected SbmlUnitTypeFactorComparator() {
    super(SbmlUnitTypeFactor.class);
  }

  @Override
  protected int internalCompare(final SbmlUnitTypeFactor arg0, final SbmlUnitTypeFactor arg1) {
    IntegerComparator integerComparator = new IntegerComparator();
    DoubleComparator doubleComparator = new DoubleComparator();
    EnumComparator<SbmlUnitType> enumComparator = new EnumComparator<>();
    if (integerComparator.compare(arg0.getExponent(), arg1.getExponent()) != 0) {
      return integerComparator.compare(arg0.getExponent(), arg1.getExponent());
    }
    if (integerComparator.compare(arg0.getScale(), arg1.getScale()) != 0) {
      return integerComparator.compare(arg0.getScale(), arg1.getScale());
    }

    if (doubleComparator.compare(arg0.getMultiplier(), arg1.getMultiplier()) != 0) {
      return doubleComparator.compare(arg0.getMultiplier(), arg1.getMultiplier());
    }

    if (enumComparator.compare(arg0.getUnitType(), arg1.getUnitType()) != 0) {
      return enumComparator.compare(arg0.getUnitType(), arg1.getUnitType());
    }

    return 0;
  }

}
