package lcsb.mapviewer.model.map.reaction.type;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines a standard CellDesigner trigger reaction.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("TRIGGER_REACTION")
public class TriggerReaction extends Reaction implements SimpleReactionInterface, ModifierReactionNotation {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  protected TriggerReaction() {
    super();
  }

  public TriggerReaction(final String elementId) {
    super(elementId);
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   *
   * @param result parent reaction from which we copy data
   */
  public TriggerReaction(final Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Trigger";
  }

  @Override
  public ReactionRect getReactionRect() {
    return null;
  }

  @Override
  public TriggerReaction copy() {
    if (this.getClass() == TriggerReaction.class) {
      return new TriggerReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermReactionType.TRIGGER.getSBO();
  }
}
