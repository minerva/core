package lcsb.mapviewer.model.map.species;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.sbo.SBOTermSpeciesType;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithStructuralState;
import lcsb.mapviewer.model.map.species.field.StructuralState;

/**
 * Class that represents complex in the model. Complex elements contain
 * sub-elements.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("COMPLEX")
public class Complex extends Species implements SpeciesWithStructuralState {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * List of elements that are in this complex (only elements that lies there
   * directly).
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "complex")
  @JsonIgnore
  private List<Species> elements = new ArrayList<>();

  /**
   * List of modifications for the Protein.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "species", orphanRemoval = true)
  @LazyCollection(LazyCollectionOption.FALSE)
  @Fetch(FetchMode.SUBSELECT)
  private List<ModificationResidue> modificationResidues = new ArrayList<>();

  /**
   * Empty constructor required by hibernate.
   */
  Complex() {
    super();
    setLineWidth(3.0);
  }

  /**
   * Constructor that creates object initialized by the object given in the
   * parameter.
   * 
   * @param original
   *          original complex used for initialization
   */
  public Complex(final Complex original) {
    super(original);

    for (final Species element : original.getElements()) {
      addSpecies(element.copy());
    }
    for (ModificationResidue mr : original.getModificationResidues()) {
      addStructuralState((StructuralState) mr.copy());
    }
  }

  /**
   * Default constructor.
   * 
   * @param elementId
   *          uniqe (within model) element identifier
   */
  public Complex(final String elementId) {
    super(elementId);
  }

  /**
   * Adds {@link Species} to the complex.
   * 
   * @param species
   *          object to add
   */
  public void addSpecies(final Species species) {
    boolean contains = false;
    for (final Species element2 : elements) {
      if (species.getElementId().equals(element2.getElementId())) {
        contains = true;
      }

    }
    if (!contains) {
      elements.add(species);
      species.setComplex(this);
    }
  }

  /**
   * Returns list of elements.
   * 
   * @return list of element inside complex
   */
  @JsonIgnore
  public List<Species> getElements() {
    return elements;
  }

  /**
   * Returns list of all {@link Species} that lies inside complex (direct and
   * indirect).
   * 
   * @return list of all {@link Species} inside {@link Complex}
   */
  @JsonIgnore
  public List<Species> getAllChildren() {
    List<Species> result = new ArrayList<>();
    result.addAll(elements);
    for (final Species species : elements) {
      if (species instanceof Complex) {
        result.addAll(((Complex) species).getAllChildren());
      }
    }
    return result;
  }

  @Override
  public Complex copy() {
    if (this.getClass() == Complex.class) {
      return new Complex(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }

  }

  /**
   * Removes {@link Species} from the complex.
   * 
   * @param element
   *          object to remove
   */
  public void removeElement(final Species element) {
    elements.remove(element);
    if (element.getComplex() != null) {
      if (element.getComplex() == this) {
        element.setComplex(null);
      } else {
        logger.warn("Removing element from complex that probably doesn't belong there");
      }
    }
  }

  /**
   * Returns all children and sub-children elements for this element. Only non
   * {@link Complex} children will be returned.
   * 
   * @return all children and sub-children elements for this element
   */
  @JsonIgnore
  public Set<Species> getAllSimpleChildren() {
    Set<Species> result = new HashSet<>();
    for (final Species element : getElements()) {
      if (element instanceof Complex) {
        result.addAll(((Complex) element).getAllSimpleChildren());
      } else {
        result.add(element);
      }
    }
    return result;
  }

  @Override
  @JsonIgnore
  public String getStringType() {
    return "Complex";
  }

  public void setElemets(final List<Species> list) {
    this.elements = list;
  }

  @Override
  public List<ModificationResidue> getModificationResidues() {
    return modificationResidues;
  }

  @Override
  public void addStructuralState(final StructuralState structuralState) {
    modificationResidues.add(structuralState);
    structuralState.setSpecies(this);
  }

  @Override
  public String getSboTerm() {
    if (isHypothetical()) {
      return SBOTermSpeciesType.HYPOTHETICAL_COMPLEX.getSBO();
    } else {
      return SBOTermSpeciesType.COMPLEX.getSBO();
    }
  }

}
