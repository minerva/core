package lcsb.mapviewer.model.map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.StringComparator;

/**
 * This class implements comparator interface for {@link OverviewLink}.
 * 
 * @author Piotr Gawron
 * 
 */
public class OverviewLinkComparator extends Comparator<OverviewLink> {

  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  @SuppressWarnings("unused")
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public OverviewLinkComparator(final double epsilon) {
    super(OverviewLink.class, true);
    this.epsilon = epsilon;
    addSubClassComparator(new OverviewImageLinkComparator(epsilon));
    addSubClassComparator(new OverviewModelLinkComparator(epsilon));
  }

  /**
   * Default constructor.
   */
  public OverviewLinkComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(final OverviewLink arg0, final OverviewLink arg1) {

    StringComparator stringComparator = new StringComparator();

    if (stringComparator.compare(arg0.getPolygon(), arg1.getPolygon()) != 0) {
      logger.debug("polygon different: " + arg0.getPolygon() + ", " + arg1.getPolygon());
      return stringComparator.compare(arg0.getPolygon(), arg1.getPolygon());
    }

    return 0;

  }

}
