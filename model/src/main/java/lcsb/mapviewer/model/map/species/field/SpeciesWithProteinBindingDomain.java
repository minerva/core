package lcsb.mapviewer.model.map.species.field;

public interface SpeciesWithProteinBindingDomain extends SpeciesWithModificationResidue {

  /**
   * Adds a {@link ProteinBindingDomain} to the species.
   * 
   * @param proteinBindingDomain
   *          {@link ProteinBindingDomain} to add
   */
  void addProteinBindingDomain(final ProteinBindingDomain proteinBindingDomain);

}
