package lcsb.mapviewer.model.map.species;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;

/**
 * Comparator class used for comparing {@link ReceptorProtein} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class ReceptorProteinComparator extends Comparator<ReceptorProtein> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public ReceptorProteinComparator(final double epsilon) {
    super(ReceptorProtein.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public ReceptorProteinComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected Comparator<?> getParentComparator() {
    return new ProteinComparator(epsilon);
  }

  @Override
  protected int internalCompare(final ReceptorProtein arg0, final ReceptorProtein arg1) {
    return 0;
  }
}
