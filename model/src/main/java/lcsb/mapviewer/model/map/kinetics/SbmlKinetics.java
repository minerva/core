package lcsb.mapviewer.model.map.kinetics;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.model.map.species.Element;

/**
 * Representation of a single SBML function
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@XmlRootElement
public class SbmlKinetics implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();
  @Cascade({ CascadeType.ALL })
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "kinetic_law_parameters", joinColumns = {
      @JoinColumn(name = "kinetic_law_id", referencedColumnName = "id", nullable = false, updatable = false) }, inverseJoinColumns = {
          @JoinColumn(name = "parameter_id", referencedColumnName = "id", nullable = true, updatable = true) })
  private Set<SbmlParameter> parameters = new HashSet<>();

  @Cascade({ CascadeType.ALL })
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "kinetic_law_functions", joinColumns = {
      @JoinColumn(name = "kinetic_law_id", referencedColumnName = "id", nullable = false, updatable = false) }, inverseJoinColumns = {
          @JoinColumn(name = "function_id", referencedColumnName = "id", nullable = true, updatable = true) })
  private Set<SbmlFunction> functions = new HashSet<>();

  @Cascade({ CascadeType.ALL })
  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "kinetic_law_elements", joinColumns = {
      @JoinColumn(name = "kinetic_law_id", referencedColumnName = "id", nullable = false, updatable = false) }, inverseJoinColumns = {
          @JoinColumn(name = "element_id", referencedColumnName = "id", nullable = true, updatable = true) })
  private Set<Element> elements = new HashSet<>();

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(columnDefinition = "TEXT")
  private String definition;

  public SbmlKinetics(final SbmlKinetics original) {
    this.definition = original.getDefinition();
    for (final SbmlArgument argument : original.getArguments()) {
      addArgument(argument);
    }
  }

  public SbmlKinetics() {
  }

  public void addParameter(final SbmlParameter parameter) {
    parameters.add(parameter);
  }

  public Set<SbmlParameter> getParameters() {
    return parameters;
  }

  public List<SbmlArgument> getArguments() {
    List<SbmlArgument> arguments = new ArrayList<>();
    arguments.addAll(parameters);
    arguments.addAll(elements);
    arguments.addAll(functions);
    return arguments;
  }

  public void addElement(final Element element) {
    this.elements.add(element);
  }

  public String getDefinition() {
    return definition;
  }

  public void setDefinition(final String definition) {
    this.definition = definition;
  }

  public void addElements(final Set<Element> elements) {
    for (final Element element : elements) {
      addElement(element);
    }
  }

  public void addParameters(final Collection<SbmlParameter> parameters) {
    for (final SbmlParameter parameter : parameters) {
      addParameter(parameter);
    }

  }

  public void addArguments(final Collection<SbmlArgument> arguments) {
    for (final SbmlArgument argument : arguments) {
      addArgument(argument);
    }

  }

  public void addArgument(final SbmlArgument argument) {
    if (argument instanceof SbmlParameter) {
      addParameter((SbmlParameter) argument);
    } else if (argument instanceof SbmlFunction) {
      addFunction((SbmlFunction) argument);
    } else {
      addElement((Element) argument);
    }

  }

  public void addFunction(final SbmlFunction argument) {
    functions.add(argument);
  }

  public SbmlParameter getParameterById(final String id) {
    for (final SbmlParameter parameter : parameters) {
      if (parameter.getParameterId().equals(id)) {
        return parameter;
      }
    }
    return null;
  }

  public Set<SbmlFunction> getFunctions() {
    return functions;
  }

  public Set<Element> getElements() {
    return elements;
  }

  public SbmlKinetics copy() {
    return new SbmlKinetics(this);
  }

  public void removeElement(final Element elementToRemove) {
    elements.remove(elementToRemove);
  }

  public void removeArguments(final Collection<SbmlArgument> arguments) {
    for (final SbmlArgument sbmlArgument : arguments) {
      removeArgument(sbmlArgument);
    }
  }

  private void removeArgument(final SbmlArgument argument) {
    if (argument instanceof SbmlParameter) {
      removeParameter((SbmlParameter) argument);
    } else if (argument instanceof SbmlFunction) {
      removeFunction((SbmlFunction) argument);
    } else {
      removeElement((Element) argument);
    }
  }

  private void removeFunction(final SbmlFunction function) {
    functions.remove(function);
  }

  private void removeParameter(final SbmlParameter parameter) {
    parameters.remove(parameter);

  }
}
