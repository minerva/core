package lcsb.mapviewer.model.map.reaction.type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines a standard CellDesigner catalysis reaction.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("CATALYSIS_REACTION")
public class CatalysisReaction extends Reaction implements SimpleReactionInterface, ModifierReactionNotation {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  protected CatalysisReaction() {
    super();
  }

  public CatalysisReaction(final String elementId) {
    super(elementId);
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   *
   * @param result parent reaction from which we copy data
   */
  public CatalysisReaction(final Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Catalysis";
  }

  @Override
  @JsonIgnore
  public ReactionRect getReactionRect() {
    return null;
  }

  @Override
  public CatalysisReaction copy() {
    if (this.getClass() == CatalysisReaction.class) {
      return new CatalysisReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermReactionType.CATALYSIS.getSBO();
  }
}
