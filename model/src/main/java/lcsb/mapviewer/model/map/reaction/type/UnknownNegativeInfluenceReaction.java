package lcsb.mapviewer.model.map.reaction.type;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class defines a standard CellDesigner unknown negative influence
 * reaction. It must have at least one reactant and one product.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("UNKNOWN_NEGATIVE_INFLUENCE")
public class UnknownNegativeInfluenceReaction extends Reaction implements SimpleReactionInterface, ReducedNotation {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  protected UnknownNegativeInfluenceReaction() {
    super();
  }

  public UnknownNegativeInfluenceReaction(final String elementId) {
    super(elementId);
  }

  /**
   * Constructor that copies data from the parameter given in the argument.
   *
   * @param result parent reaction from which we copy data
   */
  public UnknownNegativeInfluenceReaction(final Reaction result) {
    super(result);
  }

  @Override
  public String getStringType() {
    return "Unknown negative influence";
  }

  @Override
  public ReactionRect getReactionRect() {
    return null;
  }

  @Override
  public UnknownNegativeInfluenceReaction copy() {
    if (this.getClass() == UnknownNegativeInfluenceReaction.class) {
      return new UnknownNegativeInfluenceReaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public String getSboTerm() {
    return SBOTermReactionType.UNKNOWN_NEGATIVE_INFLUENCE.getSBO();
  }
}
