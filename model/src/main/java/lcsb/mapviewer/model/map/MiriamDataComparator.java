package lcsb.mapviewer.model.map;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.comparator.StringComparator;

/**
 * Comparator of {@link MiriamData} object.
 * 
 * @author Piotr Gawron
 *
 */
public class MiriamDataComparator extends Comparator<MiriamData> {

  /**
   * Default constructor.
   */
  public MiriamDataComparator() {
    super(MiriamData.class);
  }

  @Override
  protected int internalCompare(final MiriamData arg0, final MiriamData arg1) {
    StringComparator stringComparator = new StringComparator();
    if (stringComparator.compare(arg0.getResource(), arg1.getResource()) != 0) {
      return stringComparator.compare(arg0.getResource(), arg1.getResource());
    }
    if (arg0.getDataType() != arg1.getDataType()) {
      return -1;
    }
    if (arg0.getRelationType() != arg1.getRelationType()) {
      return -1;
    }
    if (arg0.getAnnotator() != arg1.getAnnotator()) {
      return -1;
    }

    return 0;
  }

}
