package lcsb.mapviewer.model.map.species;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.sbo.SBOTermSpeciesType;

/**
 * Entity representing phenotype element on the map.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
@DiscriminatorValue("PHENTOYPE")
public class Phenotype extends Species {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Empty constructor required by hibernate.
   */
  Phenotype() {
  }

  /**
   * Constructor that creates a copy of the element given in the parameter.
   * 
   * @param original
   *          original object that will be used for creating copy
   */
  public Phenotype(final Phenotype original) {
    super(original);
  }

  /**
   * Default constructor.
   * 
   * @param elementId
   *          unique (within model) element identifier
   */
  public Phenotype(final String elementId) {
    setElementId(elementId);
  }

  @Override
  public Phenotype copy() {
    if (this.getClass() == Phenotype.class) {
      return new Phenotype(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  @JsonIgnore
  public String getStringType() {
    return "Phenotype";
  }

  @Override
  public String getSboTerm() {
    return SBOTermSpeciesType.PHENOTYPE.getSBO();
  }

}
