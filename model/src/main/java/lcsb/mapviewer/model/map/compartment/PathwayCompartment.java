package lcsb.mapviewer.model.map.compartment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lcsb.mapviewer.common.exception.NotImplementedException;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.awt.Color;

/**
 * Compartment that represents a pathway.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("PATHWAY")
public class PathwayCompartment extends Compartment {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Empty constructor required by hibernate.
   */
  PathwayCompartment() {
    super();
  }

  /**
   * Default constructor.
   *
   * @param elementId identifier of the compartment
   */
  public PathwayCompartment(final String elementId) {
    super();
    setElementId(elementId);
    setFillColor(Color.LIGHT_GRAY);
  }

  /**
   * Constructor that initialize object with the data given in the parameter.
   *
   * @param original data for initialization
   */
  public PathwayCompartment(final PathwayCompartment original) {
    super(original);
  }

  @Override
  public PathwayCompartment copy() {
    if (this.getClass() == PathwayCompartment.class) {
      return new PathwayCompartment(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  @JsonIgnore
  public String getStringType() {
    return "Pathway";
  }

  @Override
  public String getShape() {
    return "PATHWAY";
  }

}
