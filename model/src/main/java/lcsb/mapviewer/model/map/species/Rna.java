package lcsb.mapviewer.model.map.species;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.sbo.SBOTermSpeciesType;
import lcsb.mapviewer.model.map.species.field.CodingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;
import lcsb.mapviewer.model.map.species.field.ProteinBindingDomain;
import lcsb.mapviewer.model.map.species.field.SpeciesWithCodingRegion;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationSite;
import lcsb.mapviewer.model.map.species.field.SpeciesWithProteinBindingDomain;

/**
 * Entity representing rna element on the map.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("RNA")
public class Rna extends Species
        implements SpeciesWithCodingRegion, SpeciesWithProteinBindingDomain, SpeciesWithModificationSite {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * List of rna regions (some rna sequences) in this object.
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(mappedBy = "species", orphanRemoval = true)
  @LazyCollection(LazyCollectionOption.FALSE)
  @Fetch(FetchMode.SUBSELECT)
  private List<ModificationResidue> modificationResidues = new ArrayList<>();

  /**
   * Empty constructor required by hibernate.
   */
  protected Rna() {
  }

  /**
   * Constructor that creates a copy of the element given in the parameter.
   *
   * @param original original object that will be used for creating copy
   */
  public Rna(final Rna original) {
    super(original);
    for (final ModificationResidue region : original.getModificationResidues()) {
      addModificationResidue(region.copy());
    }
  }

  /**
   * Default constructor.
   *
   * @param elementId uniqe (within model) element identifier
   */
  public Rna(final String elementId) {
    super();
    setElementId(elementId);
  }

  /**
   * Adds {@link ModificationResidue}.
   *
   * @param antisenseRnaRegion object to be added
   */
  private void addModificationResidue(final ModificationResidue antisenseRnaRegion) {
    modificationResidues.add(antisenseRnaRegion);
    antisenseRnaRegion.setSpecies(this);
  }

  @Override
  public void addCodingRegion(final CodingRegion codingRegion) {
    this.addModificationResidue(codingRegion);
  }

  @Override
  public void addProteinBindingDomain(final ProteinBindingDomain codingRegion) {
    this.addModificationResidue(codingRegion);
  }

  @Override
  public void addModificationSite(final ModificationSite codingRegion) {
    this.addModificationResidue(codingRegion);
  }

  @Override
  public Rna copy() {
    if (this.getClass() == Rna.class) {
      return new Rna(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  @Override
  public List<ModificationResidue> getModificationResidues() {
    return modificationResidues;
  }

  @Override
  @JsonIgnore
  public String getStringType() {
    return "RNA";
  }

  @Override
  public String getSboTerm() {
    return SBOTermSpeciesType.RNA.getSBO();
  }

}
