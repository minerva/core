package lcsb.mapviewer.model.map.layout.graphics;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.comparator.IntegerComparator;

public class LayerImageComparator extends Comparator<LayerImage> {

  private final double epsilon;

  public LayerImageComparator(final double epsilon) {
    super(LayerImage.class, true);
    this.epsilon = epsilon;
  }

  public LayerImageComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(final LayerImage arg0, final LayerImage arg1) {
    DoubleComparator doubleComparator = new DoubleComparator(epsilon);
    IntegerComparator integerComparator = new IntegerComparator();

    if (doubleComparator.compare(arg0.getWidth(), arg1.getWidth()) != 0) {
      return doubleComparator.compare(arg0.getWidth(), arg1.getWidth());
    }

    if (doubleComparator.compare(arg0.getHeight(), arg1.getHeight()) != 0) {
      return doubleComparator.compare(arg0.getHeight(), arg1.getHeight());
    }

    if (doubleComparator.compare(arg0.getX(), arg1.getX()) != 0) {
      return doubleComparator.compare(arg0.getX(), arg1.getX());
    }

    if (doubleComparator.compare(arg0.getY(), arg1.getY()) != 0) {
      return doubleComparator.compare(arg0.getY(), arg1.getY());
    }

    if (integerComparator.compare(arg0.getZ(), arg1.getZ()) != 0) {
      return integerComparator.compare(arg0.getZ(), arg1.getZ());
    }

    return 0;
  }

}
