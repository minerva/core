package lcsb.mapviewer.model.map.reaction;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * Class representing boolean "or operator" between two or more nodes in the
 * reaction.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("OR_OPERATOR_NODE")
public class OrOperator extends NodeOperator {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Constructor that copies data from the parameter given in the argument.
   * 
   * @param operator
   *          parent operator from which we copy data
   */
  public OrOperator(final OrOperator operator) {
    super(operator);
  }

  /**
   * Default constructor.
   */
  public OrOperator() {
    super();
  }

  @Override
  public String getOperatorText() {
    return "|";
  }

  @Override
  public OrOperator copy() {
    if (this.getClass() == OrOperator.class) {
      return new OrOperator(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

}
