package lcsb.mapviewer.model.map.kinetics;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.comparator.SetComparator;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.ElementComparator;

public class SbmlKineticsComparator extends Comparator<SbmlKinetics> {

  private static Logger logger = LogManager.getLogger();

  public SbmlKineticsComparator() {
    super(SbmlKinetics.class);
  }

  @Override
  protected int internalCompare(final SbmlKinetics arg0, final SbmlKinetics arg1) {
    SetComparator<SbmlFunction> functionComparator = new SetComparator<>(new SbmlFunctionComparator());
    if (functionComparator.compare(arg0.getFunctions(), arg1.getFunctions()) != 0) {
      logger.debug("Functions different");
      return functionComparator.compare(arg0.getFunctions(), arg1.getFunctions());
    }
    SetComparator<SbmlParameter> parameterComparator = new SetComparator<>(new SbmlParameterComparator());
    if (parameterComparator.compare(arg0.getParameters(), arg1.getParameters()) != 0) {
      logger.debug("Parameters different");
      return parameterComparator.compare(arg0.getParameters(), arg1.getParameters());
    }
    SetComparator<Element> elementComparator = new SetComparator<>(new ElementComparator());
    if (elementComparator.compare(arg0.getElements(), arg1.getElements()) != 0) {
      logger.debug("Elements different");
      return elementComparator.compare(arg0.getElements(), arg1.getElements());
    }
    StringComparator stringComparator = new StringComparator();
    if (stringComparator.compare(arg0.getDefinition(), arg1.getDefinition(), true) != 0) {
      logger.debug("Definition different");
      logger.debug(arg0.getDefinition());
      logger.debug(arg1.getDefinition());
      return stringComparator.compare(arg0.getDefinition(), arg1.getDefinition());
    }
    return 0;
  }

}
