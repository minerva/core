package lcsb.mapviewer.model.map.reaction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.common.geometry.LineTransformation;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.type.ReactionRect;
import lcsb.mapviewer.model.map.reaction.type.TwoProductReactionInterface;
import lcsb.mapviewer.model.map.reaction.type.TwoReactantReactionInterface;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.modelutils.serializer.model.map.ModelAsIdSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.OrderColumn;
import javax.persistence.PostLoad;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlTransient;
import java.awt.Color;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class describes reaction in the {@link Model}. Every reaction consists
 * of set of participants (nodes):
 * <ul>
 * <li>{@link Reactant reactants},</li>
 * <li>{@link Product products},</li>
 * <li>{@link Modifier modifiers}.</li>
 * </ul>
 * These nodes are connected using {@link NodeOperator operators} that define
 * relation among them.<br/>
 * <br/>
 * There are also other fields that describes reaction (like {@link #reversible}
 * , {@link #kinetics}, etc.).<br/>
 * <br/>
 * This class is general and shouldn't be used (except during initialization).
 * There are few extensions of this class (in sub-package:
 * {@link lcsb.mapviewer.model.map.reaction.type type}) that defines specific
 * reaction type.
 *
 * @author Piotr Gawron
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "reaction_type_db", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("GENERIC_REACTION")
public class Reaction implements BioEntity {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger.
   */

  private static final Logger logger = LogManager.getLogger();

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * List of {@link AbstractNode nodes} in the reaction.
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(mappedBy = "reaction", fetch = FetchType.EAGER, orphanRemoval = true)
  @OrderBy("id")
  @Fetch(FetchMode.SUBSELECT)
  @JsonIgnore
  private List<AbstractNode> nodes = new ArrayList<>();

  /**
   * Description of the reaction.
   */
  @Column(columnDefinition = "TEXT")
  private String notes = "";

  /**
   * Identifier of the reaction. Unique in the {@link Model}.
   */
  @Column(length = 255)
  private String idReaction = "";

  /**
   * Name of the reaction.
   */
  @Column(length = 255)
  private String name = "";

  /**
   * Is the reaction reversible.
   */
  private boolean reversible = false;

  /**
   * Symbol of the reaction (RECON).
   */
  @Column(length = 255)
  private String symbol = null;

  /**
   * Abbreviation of the reaction (RECON).
   */
  @Column(length = 255)
  private String abbreviation = null;

  /**
   * Formula (RECON).
   */
  @Column(length = 255)
  private String formula = null;

  /**
   * Mechanical confidence score (RECON).
   */
  private Integer mechanicalConfidenceScore = null;

  /**
   * Lower bound (RECON).
   */
  private Double lowerBound = null;

  /**
   * Upper bound (RECON).
   */
  private Double upperBound = null;

  /**
   * Subsystem (RECON).
   */
  @Column(length = 255)
  private String subsystem = null;

  /**
   * Gene protein reaction (RECON).
   */
  @Column(length = 255)
  private String geneProteinReaction = null;

  /**
   * Zoom level visibility for semantic zooming.
   */
  private String visibilityLevel = "";

  /**
   * Z-index of elements on the map.
   */
  @Column(nullable = false)
  private Integer z = null;

  /**
   * Lists of all synonyms used for describing this element.
   */
  @ElementCollection(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SUBSELECT)
  @CollectionTable(name = "reaction_synonyms", joinColumns = @JoinColumn(name = "id"))
  @Column(name = "synonym", length = 255)
  @OrderColumn(name = "idx")
  @Cascade({org.hibernate.annotations.CascadeType.ALL})
  private List<String> synonyms = new ArrayList<>();

  /**
   * Set of annotations (references to external resources) that describe this
   * reaction.
   */
  @Cascade({CascadeType.ALL})
  @ManyToMany(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SUBSELECT)
  @JoinTable(name = "reaction_miriam", joinColumns = {
      @JoinColumn(name = "reaction_id", referencedColumnName = "id", nullable = false, updatable = false)},
      inverseJoinColumns = {
          @JoinColumn(name = "miriam_id", referencedColumnName = "id", nullable = true, updatable = true)})
  @JsonProperty(value = "references")
  private Set<MiriamData> miriamData = new HashSet<>();

  /**
   * ModelData where reaction is placed.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JsonSerialize(using = ModelAsIdSerializer.class)
  private ModelData model;

  @Cascade({CascadeType.ALL})
  @OneToOne
  private SbmlKinetics kinetics;

  /**
   * Central line of the reaction.
   */
  @Cascade({CascadeType.ALL})
  @OneToOne(fetch = FetchType.EAGER, optional = false)
  private PolylineData line;

  /**
   * Coordinates of process on {@link #line} (optional).
   */
  @Type(type = "lcsb.mapviewer.persist.mapper.Point2DMapper")
  private Point2D processCoordinates;

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * Default constructor.
   */
  protected Reaction() {
  }

  @PostLoad
  protected void init() {
    // performance improvement - instead of loading from db compute it
    for (final NodeOperator operator : getOperators()) {
      operator.setInputs(new ArrayList<>());
      operator.setOutputs(new ArrayList<>());
    }
    for (final AbstractNode node : nodes) {
      if (node.getNodeOperatorForInput() != null) {
        node.getNodeOperatorForInput().addInput(node);
      }
      if (node.getNodeOperatorForOutput() != null) {
        node.getNodeOperatorForOutput().addOutput(node);
      }
    }
  }

  /**
   * Creates reaction based on the information from the reaction passed as
   * parameter.
   *
   * @param original original reaction
   */
  protected Reaction(final Reaction original) {
    // don't copy nodes - reactions refers to this objects (maybe it should be
    // refactored)
    for (final AbstractNode node : original.getNodes()) {
      addNode(node);
    }

    z = original.getZ();
    notes = original.getNotes();
    idReaction = original.getIdReaction();
    name = original.getName();
    reversible = original.reversible;
    miriamData = new HashSet<>();
    for (final MiriamData md : original.getMiriamData()) {
      miriamData.add(new MiriamData(md));
    }

    symbol = original.getSymbol();
    abbreviation = original.getAbbreviation();
    formula = original.getFormula();
    mechanicalConfidenceScore = original.getMechanicalConfidenceScore();
    lowerBound = original.getLowerBound();
    upperBound = original.getUpperBound();
    subsystem = original.getSubsystem();
    geneProteinReaction = original.getGeneProteinReaction();
    synonyms = new ArrayList<>();
    for (final String synonym : original.getSynonyms()) {
      synonyms.add(synonym);
    }
    setVisibilityLevel(original.getVisibilityLevel());
    if (original.getKinetics() != null) {
      setKinetics(original.getKinetics().copy());
    }

    int minProducts = 1;
    int minReactants = 1;
    if (this instanceof TwoReactantReactionInterface) {
      minReactants = 2;
    }
    if (this instanceof TwoProductReactionInterface) {
      minProducts = 2;
    }
    if (original.getProducts().size() < minProducts) {
      throw new InvalidReactionParticipantNumberException(new ElementUtils().getElementTag(original)
          + "Invalid source reaction: number of products must be at least " + minProducts);
    }

    if (original.getReactants().size() < minReactants) {
      throw new InvalidReactionParticipantNumberException(new ElementUtils().getElementTag(original)
          + "Invalid source reaction: number of reactants must be at least " + minReactants);
    }
    if (original.getLine() != null) {
      line = new PolylineData(original.getLine());
    }
    if (original.getProcessCoordinates() != null) {
      processCoordinates = new Point2D.Double(original.getProcessCoordinates().getX(),
          original.getProcessCoordinates().getY());
    }
  }

  public Reaction(final String reactionId) {
    setIdReaction(reactionId);
  }

  /**
   * Adds node to the reaction.
   *
   * @param node node to add
   */
  public void addNode(final AbstractNode node) {
    node.setReaction(this);
    nodes.add(node);
  }

  /**
   * Adds reactant.
   *
   * @param reactant reactant to add
   */
  public void addReactant(final Reactant reactant) {
    addNode(reactant);
  }

  /**
   * Adds product.
   *
   * @param product product to add
   */
  public void addProduct(final Product product) {
    addNode(product);
  }

  /**
   * Adds modifier.
   *
   * @param modifier modifier to add
   */
  public void addModifier(final Modifier modifier) {
    addNode(modifier);
  }

  /**
   * Returns list of reaction products.
   *
   * @return list of products
   */
  public List<Product> getProducts() {
    final List<Product> result = new ArrayList<>();
    for (final AbstractNode node : nodes) {
      if (node instanceof Product) {
        result.add((Product) node);
      }
    }
    return result;
  }

  /**
   * Returns list of reaction reactants.
   *
   * @return list of reactants
   */
  public List<Reactant> getReactants() {
    final List<Reactant> result = new ArrayList<>();
    for (final AbstractNode node : nodes) {
      if (node instanceof Reactant) {
        result.add((Reactant) node);
      }
    }
    return result;
  }

  /**
   * Returns list of reaction modifiers.
   *
   * @return list of modifiers
   */
  public List<Modifier> getModifiers() {
    final List<Modifier> result = new ArrayList<Modifier>();
    for (final AbstractNode node : nodes) {
      if (node instanceof Modifier) {
        result.add((Modifier) node);
      }
    }
    return result;
  }

  /**
   * This method return the minimum distance from the reaction representation (all
   * lines which describe reaction) and point given as a parameter.
   *
   * @param point from where we look for the shortest possible distance
   * @return the shortest possible distance from this reaction and point
   */
  public double getDistanceFromPoint(final Point2D point) {
    final LineTransformation lt = new LineTransformation();
    double dist = Double.MAX_VALUE;
    final List<Line2D> lines = getLines();
    for (final Line2D line : lines) {
      final double d2 = lt.distBetweenPointAndLineSegment(line, point);
      if (d2 < dist) {
        dist = d2;
      }
    }
    return dist;
  }

  /**
   * Return list of all lines in the reaction that describe it.
   *
   * @return list of all lines that describes every connection in the reaction
   */
  @JsonIgnore
  public List<Line2D> getLines() {
    final List<Line2D> result = new ArrayList<>();
    for (final AbstractNode node : nodes) {
      result.addAll(node.getLine().getLines());
    }
    return result;
  }

  /**
   * Returns point on the reaction that is as close as possible to the given
   * parameter point.
   */
  public Point2D getClosestPointTo(final Point2D point) {
    final LineTransformation lt = new LineTransformation();
    double dist = Double.MAX_VALUE;
    final List<Line2D> lines = getLines();
    Point2D result = null;
    for (final Line2D line : lines) {
      final double d2 = lt.distBetweenPointAndLineSegment(line, point);
      if (d2 < dist) {
        dist = d2;
        result = lt.closestPointOnSegmentLineToPoint(line, point);
      }
    }
    return result;
  }

  /**
   * Returns list of nodes (producst+rectants+modifiers) in the reaction.
   *
   * @return list of nodes (producst+rectants+modifiers) in the reaction
   */
  @JsonIgnore
  public List<ReactionNode> getReactionNodes() {
    final List<ReactionNode> result = new ArrayList<ReactionNode>();
    for (final AbstractNode node : nodes) {
      if (node instanceof ReactionNode) {
        result.add((ReactionNode) node);
      }
    }
    return result;
  }

  /**
   * Removes modifier.
   *
   * @param modifier modifier to remove
   */
  public void removeModifier(final Modifier modifier) {
    nodes.remove(modifier);
  }

  @Override
  public String getVisibilityLevel() {
    return visibilityLevel;
  }

  /**
   * Removes {@link AbstractNode}.
   *
   * @param node node to remove
   */
  public void removeNode(final AbstractNode node) {
    nodes.remove(node);
    if (node.getReaction() == this) {
      node.setReaction(null);
    }

    for (final NodeOperator operator : getOperators()) {
      operator.getInputs().remove(node);
      operator.getOutputs().remove(node);
    }
    node.setNodeOperatorForInput(null);
    node.setNodeOperatorForOutput(null);
  }

  @Override
  public void setVisibilityLevel(final String visibilityLevel) {
    this.visibilityLevel = visibilityLevel;
  }

  @Override
  public void setVisibilityLevel(final Integer zoomLevelVisibility) {
    if (zoomLevelVisibility == null) {
      this.visibilityLevel = null;
    } else {
      this.visibilityLevel = zoomLevelVisibility + "";
    }
  }

  /**
   * Returns {@link ReactionRect} object that defines a small object that should
   * be drawn on the central line of the reaction.
   */
  @JsonIgnore
  public ReactionRect getReactionRect() {
    return null;
  }

  /**
   * Check if one of the nodes reference to the element.
   */
  public boolean containsElement(final Element element) {
    for (final ReactionNode node : getReactionNodes()) {
      if (node.getElement().equals(element)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Returns list of all {@link NodeOperator operators}.
   *
   * @return list of all {@link NodeOperator operators}
   */
  public List<NodeOperator> getOperators() {
    final List<NodeOperator> result = new ArrayList<NodeOperator>();
    for (final AbstractNode node : getNodes()) {
      if (node instanceof NodeOperator) {
        result.add((NodeOperator) node);
      }
    }
    return result;
  }

  /**
   * @return the nodes
   * @see #nodes
   */
  @JsonIgnore
  public List<AbstractNode> getNodes() {
    return nodes;
  }

  /**
   * @param nodes the nodes to set
   * @see #nodes
   */
  public void setNodes(final List<AbstractNode> nodes) {
    this.nodes = nodes;
  }

  /**
   * @return the idReaction
   * @see #idReaction
   */
  public String getIdReaction() {
    return idReaction;
  }

  /**
   * Creates copy of the reaction.
   *
   * @return copy of the reaction
   */
  @Override
  public Reaction copy() {
    if (this.getClass() == Reaction.class) {
      return new Reaction(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * @param idReaction the idReaction to set
   * @see #idReaction
   */
  public void setIdReaction(final String idReaction) {
    this.idReaction = idReaction;
  }

  /**
   * @return the reversible
   * @see #reversible
   */
  public boolean isReversible() {
    return reversible;
  }

  /**
   * @param reversible the reversible to set
   * @see #reversible
   */
  public void setReversible(final boolean reversible) {
    this.reversible = reversible;
  }

  @Override
  public Set<MiriamData> getMiriamData() {
    return miriamData;
  }

  @Override
  public void addMiriamData(final Collection<MiriamData> miriamData) {
    for (final MiriamData md : miriamData) {
      addMiriamData(md);
    }
  }

  @Override
  public void addMiriamData(final MiriamData md) {
    if (this.miriamData.contains(md)) {
      logger.warn("Miriam data (" + md.getDataType() + ": " + md.getResource() + ") for " + getIdReaction()
          + " already exists. Ignoring...");
    } else {
      this.miriamData.add(md);
    }
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getNotes() {
    return notes;
  }

  @Override
  public void setNotes(final String notes) {
    this.notes = notes;
  }

  @Override
  public String getSymbol() {
    return symbol;
  }

  @Override
  public void setSymbol(final String symbol) {
    this.symbol = symbol;
  }

  /**
   * @return the synonyms
   * @see #synonyms
   */
  @Override
  public List<String> getSynonyms() {
    return synonyms;
  }

  /**
   * @param synonyms the synonyms to set
   * @see #synonyms
   */
  @Override
  public void setSynonyms(final List<String> synonyms) {
    this.synonyms = synonyms;
  }

  @Override
  public String getAbbreviation() {
    return abbreviation;
  }

  @Override
  public void setAbbreviation(final String abbreviation) {
    this.abbreviation = abbreviation;
  }

  /**
   * @param name the name to set
   * @see #name
   */
  @Override
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * @return the id
   * @see #id
   */
  @Override
  public int getId() {
    return id;
  }

  /**
   * Returns short string describing type of the reaction.
   *
   * @return short string describing type of the reaction
   */
  @Override
  @JsonIgnore
  public String getStringType() {
    return "Generic Reaction";
  }

  /**
   * @param id the id to set
   * @see #id
   */
  public void setId(final int id) {
    this.id = id;
  }

  @Override
  @JsonIgnore
  public ModelData getModelData() {
    return model;
  }

  /**
   * Sets model where the reaction is located.
   *
   * @param model2 model where the reaction is located
   */
  public void setModel(final Model model2) {
    this.model = model2.getModelData();
  }

  /**
   * @param model the model to set
   * @see #model
   */
  public void setModelData(final ModelData model) {
    this.model = model;
  }

  @XmlTransient
  @Override
  @JsonIgnore
  public Model getModel() {
    if (model == null) {
      return null;
    }
    return model.getModel();
  }

  /**
   * @return the mechanicalConfidenceScore
   * @see #mechanicalConfidenceScore
   */
  public Integer getMechanicalConfidenceScore() {
    return mechanicalConfidenceScore;
  }

  /**
   * @param mechanicalConfidenceScore the mechanicalConfidenceScore to set
   * @see #mechanicalConfidenceScore
   */
  public void setMechanicalConfidenceScore(final Integer mechanicalConfidenceScore) {
    this.mechanicalConfidenceScore = mechanicalConfidenceScore;
  }

  /**
   * @return the lowerBound
   * @see #lowerBound
   */
  public Double getLowerBound() {
    return lowerBound;
  }

  /**
   * @param lowerBound the lowerBound to set
   * @see #lowerBound
   */
  public void setLowerBound(final Double lowerBound) {
    this.lowerBound = lowerBound;
  }

  /**
   * @return the upperBound
   * @see #upperBound
   */
  public Double getUpperBound() {
    return upperBound;
  }

  @Override
  public String getFormula() {
    return formula;
  }

  /**
   * @param upperBound the upperBound to set
   * @see #upperBound
   */
  public void setUpperBound(final Double upperBound) {
    this.upperBound = upperBound;
  }

  @Override
  public void setFormula(final String formula) {
    this.formula = formula;
  }

  /**
   * @return the subsystem
   * @see #subsystem
   */
  public String getSubsystem() {
    return subsystem;
  }

  /**
   * @param subsystem the subsystem to set
   * @see #subsystem
   */
  public void setSubsystem(final String subsystem) {
    this.subsystem = subsystem;
  }

  /**
   * @return the geneProteinReaction
   * @see #geneProteinReaction
   */
  public String getGeneProteinReaction() {
    return geneProteinReaction;
  }

  /**
   * @param geneProteinReaction the geneProteinReaction to set
   * @see #geneProteinReaction
   */
  public void setGeneProteinReaction(final String geneProteinReaction) {
    this.geneProteinReaction = geneProteinReaction;
  }

  /**
   * Adds synonym to the {@link #synonyms}.
   *
   * @param synonym new synonym to add
   */
  public void addSynonym(final String synonym) {
    this.synonyms.add(synonym);
  }

  public SbmlKinetics getKinetics() {
    return kinetics;
  }

  public void setKinetics(final SbmlKinetics kinetics) {
    this.kinetics = kinetics;
  }

  @Override
  public Integer getZ() {
    return z;
  }

  @Override
  public void setZ(final Integer z) {
    this.z = z;
  }

  @Override
  public String getElementId() {
    return getIdReaction();
  }

  @Override
  @JsonIgnore
  public double getSize() {
    return 0;
  }

  @Override
  public String toString() {
    return new ElementUtils().getElementTag(this);
  }

  public PolylineData getLine() {
    return line;
  }

  public void setLine(final PolylineData line) {
    this.line = line;
  }

  public Point2D getProcessCoordinates() {
    return processCoordinates;
  }

  public void setProcessCoordinates(final Point2D processCoordinates) {
    this.processCoordinates = processCoordinates;
  }

  @Override
  @JsonIgnore
  public Color getBorderColor() {
    if (getLine() != null) {
      return getLine().getBorderColor();
    }
    return null;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public void setBorderColor(final Color color) {
    if (getLine() != null) {
      getLine().setBorderColor(color);
    }
  }

  @Override
  @JsonIgnore
  public Color getFillColor() {
    return getBorderColor();
  }

  @Override
  public void setFillColor(final Color color) {
    setBorderColor(color);
  }

  @Override
  public String getSboTerm() {
    throw new NotImplementedException();
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }

}
