/**
 * Contains structures used for map modeling. The most important class
 * representing model is {@link lcsb.mapviewer.model.map.model.Model}. All other
 * class represents fields or elements of the model (or model elements).
 */
package lcsb.mapviewer.model.map;
