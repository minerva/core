package lcsb.mapviewer.model.map.kinetics;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.map.model.ModelData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

/**
 * Representation of a single SBML unit
 *
 * @author Piotr Gawron
 */
@Entity
@XmlRootElement
public class SbmlUnit implements MinervaEntity {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static final Logger logger = LogManager.getLogger();
  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(length = 255)
  private String unitId;

  @Column(length = 255)
  private String name;

  @Cascade({CascadeType.ALL})
  @OneToMany(mappedBy = "unit", orphanRemoval = true, fetch = FetchType.EAGER)
  @Fetch(FetchMode.SUBSELECT)
  private final Set<SbmlUnitTypeFactor> unitTypeFactors = new HashSet<>();

  /**
   * Map model object to which unit belongs to.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JsonIgnore
  private ModelData model;

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * Constructor required by hibernate.
   */
  SbmlUnit() {
    super();
  }

  public SbmlUnit(final String unitId) {
    this.unitId = unitId;
  }

  public SbmlUnit(final SbmlUnit sbmlUnit) {
    this(sbmlUnit.getUnitId());
    this.setName(sbmlUnit.getName());
    for (final SbmlUnitTypeFactor factor : sbmlUnit.getUnitTypeFactors()) {
      this.addUnitTypeFactor(factor.copy());
    }
  }

  public String getUnitId() {
    return unitId;
  }

  public void setUnitId(final String unitId) {
    this.unitId = unitId;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public void addUnitTypeFactor(final SbmlUnitTypeFactor factor) {
    unitTypeFactors.add(factor);
    factor.setUnit(this);
  }

  public Set<SbmlUnitTypeFactor> getUnitTypeFactors() {
    return unitTypeFactors;
  }

  public ModelData getModel() {
    return model;
  }

  public void setModel(final ModelData model) {
    this.model = model;
  }

  public SbmlUnit copy() {
    return new SbmlUnit(this);
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }

}
