package lcsb.mapviewer.model.map.layout.graphics;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.BooleanComparator;
import lcsb.mapviewer.common.comparator.IntegerComparator;
import lcsb.mapviewer.common.comparator.ListComparator;
import lcsb.mapviewer.common.comparator.SetComparator;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.graphics.PolylineDataComparator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Comparator of {@link Layer} class.
 *
 * @author Piotr Gawron
 */
public class LayerComparator extends Comparator<Layer> {

  /**
   * Default class logger.
   */
  private static final Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private final double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   *
   * @param epsilon {@link #epsilon}
   */
  public LayerComparator(final double epsilon) {
    super(Layer.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public LayerComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(final Layer arg0, final Layer arg1) {
    final StringComparator stringComparator = new StringComparator();
    final BooleanComparator booleanComparator = new BooleanComparator();
    final IntegerComparator integerComparator = new IntegerComparator();

    if (stringComparator.compare(arg0.getLayerId(), arg1.getLayerId()) != 0) {
      logger.debug("layer ids different: " + arg0.getLayerId() + ", " + arg1.getLayerId());
      return stringComparator.compare(arg0.getLayerId(), arg1.getLayerId());
    }

    if (stringComparator.compare(arg0.getName(), arg1.getName()) != 0) {
      logger.debug("layer name different: " + arg0.getName() + ", " + arg1.getName());
      return stringComparator.compare(arg0.getName(), arg1.getName());
    }

    if (booleanComparator.compare(arg0.isVisible(), arg1.isVisible()) != 0) {
      logger.debug("layer visibility different: " + arg0.isVisible() + ", " + arg1.isVisible());
      return booleanComparator.compare(arg0.isVisible(), arg1.isVisible());
    }

    if (booleanComparator.compare(arg0.isLocked(), arg1.isLocked()) != 0) {
      logger.debug("layer locked different: " + arg0.isLocked() + ", " + arg1.isLocked());
      return booleanComparator.compare(arg0.isLocked(), arg1.isLocked());
    }

    if (integerComparator.compare(arg0.getTexts().size(), arg1.getTexts().size()) != 0) {
      logger.debug("layer texts different: " + arg0.getTexts().size() + ", " + arg1.getTexts().size());
      return integerComparator.compare(arg0.getTexts().size(), arg1.getTexts().size());
    }
    if (integerComparator.compare(arg0.getLines().size(), arg1.getLines().size()) != 0) {
      logger.debug("layer lines different: " + arg0.getLines().size() + ", " + arg1.getLines().size());
      return integerComparator.compare(arg0.getLines().size(), arg1.getLines().size());
    }
    if (integerComparator.compare(arg0.getRectangles().size(), arg1.getRectangles().size()) != 0) {
      logger.debug("layer rectangles different: " + arg0.getRectangles().size() + ", " + arg1.getRectangles().size());
      return integerComparator.compare(arg0.getRectangles().size(), arg1.getRectangles().size());
    }
    if (integerComparator.compare(arg0.getOvals().size(), arg1.getOvals().size()) != 0) {
      logger.debug("layer ovals different: " + arg0.getOvals().size() + ", " + arg1.getOvals().size());
      return integerComparator.compare(arg0.getOvals().size(), arg1.getOvals().size());
    }

    final SetComparator<LayerText> textComparator = new SetComparator<>(new LayerTextComparator(epsilon));
    int status = textComparator.compare(arg0.getTexts(), arg1.getTexts());
    if (status != 0) {
      logger.debug("layer texts different");
      return status;
    }

    final SetComparator<LayerImage> imageComparator = new SetComparator<>(new LayerImageComparator(epsilon));
    status = imageComparator.compare(arg0.getImages(), arg1.getImages());
    if (status != 0) {
      logger.debug("layer images different");
      return status;
    }

    final SetComparator<LayerOval> ovalComparator = new SetComparator<>(new LayerOvalComparator(epsilon));
    status = ovalComparator.compare(arg0.getOvals(), arg1.getOvals());
    if (status != 0) {
      logger.debug("layer ovals different");
      return status;
    }

    final SetComparator<LayerRect> rectComparator = new SetComparator<>(new LayerRectComparator(epsilon));
    status = rectComparator.compare(arg0.getRectangles(), arg1.getRectangles());
    if (status != 0) {
      logger.debug("layer rectangles different ");
      return status;
    }

    final ListComparator<PolylineData> lineComparator = new ListComparator<>(new PolylineDataComparator(epsilon));
    status = lineComparator.compare(arg0.getLines(), arg1.getLines());
    if (status != 0) {
      logger.debug("layer lines different ");
      return status;
    }

    return 0;
  }

}
