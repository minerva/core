package lcsb.mapviewer.model.map.species.field;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("ABSTRACT_REGION_MODIFICATION")
public abstract class AbstractRegionModification extends ModificationResidue {

  private static final double DEFAULT_SIZE = 10;

  private static final long serialVersionUID = 1L;

  public AbstractRegionModification() {
    super();
    this.setWidth(DEFAULT_SIZE);
    this.setHeight(DEFAULT_SIZE);
    this.setNameWidth(DEFAULT_SIZE);
    this.setNameHeight(DEFAULT_SIZE);
  }

  public AbstractRegionModification(final AbstractRegionModification mr) {
    super(mr);
  }

  public AbstractRegionModification(final String idModificationResidue) {
    super(idModificationResidue);
  }

}