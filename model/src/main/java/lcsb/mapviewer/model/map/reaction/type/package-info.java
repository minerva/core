/**
 * This package contains all types of reactions that are allowed in the system
 * (maybe it should be transformed to a kind of enum...).
 */
package lcsb.mapviewer.model.map.reaction.type;
