package lcsb.mapviewer.model.map.layout.graphics;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.modelutils.serializer.model.cache.UploadedFileEntryAsIdSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Version;
import java.io.Serializable;

/**
 * This class describes glyph used to represent {@link BioEntity} on the map.
 *
 * @author Piotr Gawron
 */
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Glyph implements MinervaEntity, Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static final Logger logger = LogManager.getLogger();

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * File that should be used for drawing a glyph.
   */
  @Cascade({CascadeType.SAVE_UPDATE})
  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "file_entry_id")
  @JsonSerialize(using = UploadedFileEntryAsIdSerializer.class)
  private UploadedFileEntry file;

  /**
   * Project in which this glyph is available.
   */
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JsonIgnore
  private Project project;

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * Default constructor.
   */
  public Glyph() {
  }

  /**
   * Constructor that creates copy of the element.
   *
   * @param original element to be copied
   */
  public Glyph(final Glyph original) {
    // we should reference to the same file
    setFile(original.getFile());
    if (original.getFile() != null) {
      // make sure that the content is available
      original.getFile().getFileContent();
    }
  }

  public UploadedFileEntry getFile() {
    return file;
  }

  public void setFile(final UploadedFileEntry file) {
    this.file = file;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(final Project project) {
    this.project = project;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public EntityType getEntityType() {
    return EntityType.GLYPH;
  }
}
