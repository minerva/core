package lcsb.mapviewer.model.map.species;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;

/**
 * Comparator class used for comparing {@link SimpleMolecule} objects.
 * 
 * @author Piotr Gawron
 *
 */
public class SimpleMoleculeComparator extends Comparator<SimpleMolecule> {

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public SimpleMoleculeComparator(final double epsilon) {
    super(SimpleMolecule.class, true);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public SimpleMoleculeComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected Comparator<?> getParentComparator() {
    return new ChemicalComparator(epsilon);
  }

  @Override
  protected int internalCompare(final SimpleMolecule arg0, final SimpleMolecule arg1) {
    return 0;
  }
}
