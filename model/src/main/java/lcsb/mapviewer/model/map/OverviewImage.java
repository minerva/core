package lcsb.mapviewer.model.map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.ModelData;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Version;
import java.util.ArrayList;
import java.util.List;

/**
 * This class describes single image that overview the model. Images can create
 * hierarchical structure via {@link OverviewLink} (link can point to another
 * image). The idea is to have set of images that nicely represent biology and
 * map it into map via {@link OverviewModelLink}.
 *
 * @author Piotr Gawron
 */
@Entity
public class OverviewImage implements MinervaEntity {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * The image belongs to this {@link Project}. Links can point to any
   * (sub)model in this project.
   */
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  private Project project;

  /**
   * Name of the file in file system that represent this overview image.
   */
  @Column(length = 255)
  private String filename;

  /**
   * Width of the image.
   */
  private Integer width;

  /**
   * Height of the image.
   */
  private Integer height;

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * List of links that should redirect from this image. They can refer to
   * another {@link OverviewImage} or {@link ModelData Model}.
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(fetch = FetchType.EAGER, mappedBy = "overviewImage")
  @OrderBy("id")
  private List<OverviewLink> links = new ArrayList<>();

  /**
   * Default constructor.
   */
  public OverviewImage() {

  }

  /**
   * Default constructor with original {@link OverviewImage} as a source of
   * data.
   *
   * @param overviewImage original {@link OverviewImage}
   */
  public OverviewImage(final OverviewImage overviewImage) {
    this.setId(overviewImage.getId());
    this.setProject(overviewImage.getProject());
    this.setFilename(overviewImage.getFilename());
    this.setWidth(overviewImage.getWidth());
    this.setHeight(overviewImage.getHeight());
    for (final OverviewLink ol : overviewImage.getLinks()) {
      this.addLink(ol.copy());
    }
  }

  /**
   * @return the filename
   * @see #filename
   */
  public String getFilename() {
    return filename;
  }

  /**
   * @param filename the filename to set
   * @see #filename
   */
  public void setFilename(final String filename) {
    this.filename = filename;
  }

  /**
   * @return the width
   * @see #width
   */
  public Integer getWidth() {
    return width;
  }

  /**
   * @param width the width to set
   * @see #width
   */
  public void setWidth(final Integer width) {
    this.width = width;
  }

  /**
   * @return the height
   * @see #height
   */
  public Integer getHeight() {
    return height;
  }

  /**
   * @param height the height to set
   * @see #height
   */
  public void setHeight(final Integer height) {
    this.height = height;
  }

  /**
   * @return the links
   * @see #links
   */
  public List<OverviewLink> getLinks() {
    return links;
  }

  /**
   * @param links the links to set
   * @see #links
   */
  public void setLinks(final List<OverviewLink> links) {
    this.links = links;
  }

  /**
   * @return the id
   * @see #id
   */
  @Override
  public int getId() {
    return id;
  }

  /**
   * @param id the id to set
   * @see #id
   */
  public void setId(final int id) {
    this.id = id;
  }

  /**
   * Adds {@link OverviewLink link } to {@link #links}.
   *
   * @param oml object to add
   */
  public void addLink(final OverviewLink oml) {
    links.add(oml);
    oml.setOverviewImage(this);
  }

  /**
   * Creates a copy of the object.
   *
   * @return copy of the object
   */
  public OverviewImage copy() {
    if (this.getClass() == OverviewImage.class) {
      return new OverviewImage(this);
    } else {
      throw new NotImplementedException("Method copy() should be overriden in class " + this.getClass());
    }
  }

  /**
   * @return the project
   * @see #project
   */
  public Project getProject() {
    return project;
  }

  /**
   * @param project the project to set
   * @see #project
   */
  public void setProject(final Project project) {
    this.project = project;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
