package lcsb.mapviewer.model;

/**
 * Defines possible statuses when uploading project into system.
 *
 * @author Piotr Gawron
 */
public enum ProjectStatus {

  /**
   * Unknown status.
   */
  UNKNOWN("uninitialized"),

  /**
   * Data are parsed by the CellDesigner parser.
   */
  PARSING_DATA("Parsing data"),

  /**
   * Model is being uploaded to the database.
   */
  UPLOADING_TO_DB("Uploading to db"),

  /**
   * Images for the projects are being generated.
   */
  GENERATING_IMAGES("Generating images"),

  /**
   * Project is ready to use.
   */
  DONE("Ok"),

  /**
   * There was problem during project creation.
   */
  FAIL("Failure"),

  /**
   * Miriam data validation.
   */
  VALIDATING_MIRIAM("Validating miriam data"),

  /**
   * Removing of the project.
   */
  REMOVING("Project removing"),

  ARCHIVED("Archived");

  /**
   * Message used to present the status on the client side.
   */
  private final String readableString;

  /**
   * Default constructor of the enum with {@link #readableString} parameter.
   *
   * @param readableForm {@link #readableString}
   */
  ProjectStatus(final String readableForm) {
    this.readableString = readableForm;
  }

  /**
   * @return {@link #readableString}
   */
  @Override
  public String toString() {
    return readableString;
  }

}
