package lcsb.mapviewer.model.user.annotator;

import java.io.Serializable;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lcsb.mapviewer.model.user.UserAnnotationSchema;
import lcsb.mapviewer.modelutils.serializer.model.user.annotator.AnnotatorParameterDeserializer;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type_distinguisher", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("ABSTRACT_PARAMETER")
@JsonDeserialize(using = AnnotatorParameterDeserializer.class)
public abstract class AnnotatorParameter implements Serializable, Comparable<AnnotatorParameter> {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @JsonIgnore
  private Integer id;

  /**
   * Order at which the element should appear on the {@link #annotatorData} list
   * of parameters.
   */
  @JsonProperty("order")
  private int orderPosition;

  /**
   * {@link UserAnnotationSchema} that defines which user is using this set of
   * annotators.
   */
  @ManyToOne
  @JsonIgnore
  private AnnotatorData annotatorData;

  /**
   * @return the id
   * @see #id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(final Integer id) {
    this.id = id;
  }

  /**
   * @return the annotationSchema
   * @see #annotatorData
   */
  public AnnotatorData getAnnotatorData() {
    return annotatorData;
  }

  /**
   * @param annotatorData
   *          the annotationSchema to set
   * @see #annotationSchema
   */
  public void setAnnotatorData(final AnnotatorData annotatorData) {
    this.annotatorData = annotatorData;
  }

  public int getOrderPosition() {
    return orderPosition;
  }

  public void setOrderPosition(final int order) {
    this.orderPosition = order;
  }

  @Override
  public int compareTo(final AnnotatorParameter o) {
    return this.getOrderPosition() - o.getOrderPosition();
  }

}
