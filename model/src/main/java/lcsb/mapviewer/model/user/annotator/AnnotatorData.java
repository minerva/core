package lcsb.mapviewer.model.user.annotator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lcsb.mapviewer.model.user.AnnotatorParamDefinition;
import lcsb.mapviewer.model.user.UserClassAnnotators;
import lcsb.mapviewer.modelutils.serializer.ClassNameSerializer;

@Entity
public class AnnotatorData implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  /**
   * Order of the annotator in the annotator list.
   */
  @JsonProperty("order")
  private int orderIndex;

  /**
   * Class name of the annotator {@link #parameterType parameter} of which is
   * being set to the {@link #paramValue value}.
   */
  @Column(nullable = false)
  @JsonProperty("annotatorClass")
  @JsonSerialize(using = ClassNameSerializer.class)
  private Class<?> annotatorClassName;

  @ManyToOne
  @JsonIgnore
  private UserClassAnnotators userClassAnnotators;

  /**
   * List of class annotators params.
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "annotatorData")
  @OrderBy("id")
  @JsonProperty("parameters")
  private List<AnnotatorParameter> annotatorParams = new ArrayList<>();

  /**
   * Constructor for hibernate.
   */
  protected AnnotatorData() {

  }

  public AnnotatorData(final Class<?> annotatorClassName) {
    setAnnotatorClassName(annotatorClassName);
  }

  /**
   * @return the id
   * @see #id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(final Integer id) {
    this.id = id;
  }

  /**
   * @return the className
   * @see #className
   */
  public Class<?> getAnnotatorClassName() {
    return annotatorClassName;
  }

  public void setAnnotatorClassName(final Class<?> annotatorClassName) {
    this.annotatorClassName = annotatorClassName;
  }

  public UserClassAnnotators getUserClassAnnotators() {
    return userClassAnnotators;
  }

  public void setUserClassAnnotators(final UserClassAnnotators userClassAnnotators) {
    this.userClassAnnotators = userClassAnnotators;
  }

  public int getOrderIndex() {
    return orderIndex;
  }

  public void setOrderIndex(final int orderIndex) {
    this.orderIndex = orderIndex;
  }

  public List<AnnotatorParameter> getAnnotatorParams() {
    return annotatorParams;
  }

  public void setAnnotatorParams(final List<AnnotatorParameter> annotatorParams) {
    this.annotatorParams = annotatorParams;
  }

  public AnnotatorData addAnnotatorParameter(final AnnotatorParameter parameter) {
    this.annotatorParams.add(parameter);
    parameter.setAnnotatorData(this);
    fixOrder();
    return this;
  }

  public AnnotatorData addAnnotatorParameter(final AnnotatorParamDefinition parameterType, final String paramValue) {
    for (final AnnotatorParameter annotatorParameter : annotatorParams) {
      if (annotatorParameter instanceof AnnotatorConfigParameter) {
        AnnotatorConfigParameter configParameter = (AnnotatorConfigParameter) annotatorParameter;
        configParameter.setValue(paramValue);
        return this;
      }
    }
    return addAnnotatorParameter(new AnnotatorConfigParameter(parameterType, paramValue));
  }

  public AnnotatorData addAnnotatorParameters(final List<? extends AnnotatorParameter> parameters) {
    for (final AnnotatorParameter object : parameters) {
      addAnnotatorParameter(object);
    }
    return this;
  }

  public String getValue(final AnnotatorParamDefinition type) {
    for (final AnnotatorParameter annotatorParameter : annotatorParams) {
      if (annotatorParameter instanceof AnnotatorConfigParameter) {
        AnnotatorConfigParameter configParameter = (AnnotatorConfigParameter) annotatorParameter;
        if (configParameter.getType().equals(type)) {
          return configParameter.getValue();
        }
      }
    }
    return null;
  }

  @JsonIgnore
  public List<AnnotatorInputParameter> getInputParameters() {
    List<AnnotatorInputParameter> result = new ArrayList<>();
    for (final AnnotatorParameter annotatorParameter : annotatorParams) {
      if (annotatorParameter instanceof AnnotatorInputParameter) {
        result.add((AnnotatorInputParameter) annotatorParameter);
      }
    }
    return result;
  }

  public boolean hasOutputField(final BioEntityField field) {
    for (final AnnotatorOutputParameter annotatorParameter : getOutputParameters()) {
      if (annotatorParameter.getField() != null && annotatorParameter.getField().equals(field)) {
        return true;
      }
    }
    return false;
  }

  @JsonIgnore
  public List<AnnotatorOutputParameter> getOutputParameters() {
    List<AnnotatorOutputParameter> result = new ArrayList<>();
    for (final AnnotatorParameter annotatorParameter : annotatorParams) {
      if (annotatorParameter instanceof AnnotatorOutputParameter) {
        result.add((AnnotatorOutputParameter) annotatorParameter);
      }
    }
    return result;
  }

  public void removeAnnotatorParameters(final ArrayList<AnnotatorParameter> parameters) {
    for (final AnnotatorParameter annotatorParameter : parameters) {
      removeAnnotatorParameter(annotatorParameter);
    }
  }

  private void removeAnnotatorParameter(final AnnotatorParameter parameter) {
    this.annotatorParams.remove(parameter);
    parameter.setAnnotatorData(null);
    fixOrder();
  }

  private void fixOrder() {
    for (int i = 0; i < annotatorParams.size(); i++) {
      annotatorParams.get(i).setOrderPosition(i);
    }
  }

}
