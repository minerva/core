package lcsb.mapviewer.model.user.annotator;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.BioEntity;

/**
 * This enum identifies field of the {@link BioEntity}.
 * 
 * @author Piotr Gawron
 *
 */
public enum BioEntityField {
  ABBREVIATION("Abbreviation"),
  CHARGE("Charge"),
  DESCRIPTION("Description"),
  FORMULA("Formula"),
  FULL_NAME("Full name"),
  NAME("Name"),
  MCS("Mechanical Confidence Score"),
  PREVIOUS_SYMBOLS("Previous Symbols"),
  SMILE("Smile"),
  SYMBOL("Symbol"),
  SYNONYMS("Synonyms"),
  SUBSYSTEM("Subsystem"),
  ;

  private String fieldName;

  private BioEntityField(final String fieldName) {
    this.fieldName = fieldName;
  }

  public static String getFieldValueForBioEntity(final BioEntity bioEntity, final BioEntityField field) {
    if (field.equals(ABBREVIATION)) {
      return bioEntity.getAbbreviation();
    } else if (field.equals(NAME)) {
      return bioEntity.getName();
    } else {
      throw new NotImplementedException("Field type not supported: " + field);
    }
  }

  public String getCommonName() {
    return fieldName;
  }

}
