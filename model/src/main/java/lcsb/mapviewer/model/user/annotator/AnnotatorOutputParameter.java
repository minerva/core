package lcsb.mapviewer.model.user.annotator;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.modelutils.serializer.model.user.annotator.AnnotatorOutputParameterSerializer;

/**
 * Definition of annotator output. It describes which property/identifier should
 * be set by the annotator.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("OUTPUT_PARAMETER")
@JsonSerialize(using = AnnotatorOutputParameterSerializer.class)
public class AnnotatorOutputParameter extends AnnotatorParameter {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private BioEntityField field;

  @Enumerated(EnumType.STRING)
  private MiriamType identifierType;

  /**
   * Default constructor for hibernate.
   */
  protected AnnotatorOutputParameter() {
  }

  public AnnotatorOutputParameter(final BioEntityField field) {
    this.field = field;
  }

  public AnnotatorOutputParameter(final MiriamType type) {
    this.identifierType = type;
  }

  public BioEntityField getField() {
    return field;
  }

  public MiriamType getIdentifierType() {
    return identifierType;
  }

  @Override
  public int hashCode() {
    return this.toString().hashCode();
  }

  @Override
  public boolean equals(final Object o) {
    if (o == null) {
      return false;
    }
    return this.toString().equals(o.toString());
  }

  @Override
  public String toString() {
    return "[" + field + "," + identifierType + "]";
  }
}
