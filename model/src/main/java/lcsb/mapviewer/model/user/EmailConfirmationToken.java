package lcsb.mapviewer.model.user;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Calendar;

@Entity
public class EmailConfirmationToken implements Serializable, MinervaEntity {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(nullable = false)
  private String token;

  @ManyToOne
  private User user;

  @Column(nullable = false)
  private Calendar expires;

  protected EmailConfirmationToken() {

  }

  public EmailConfirmationToken(final User user, final String token, final Calendar expires) {
    this.token = token;
    this.user = user;
    this.expires = expires;
  }

  public String getToken() {
    return token;
  }

  public User getUser() {
    return user;
  }

  public Calendar getExpires() {
    return expires;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public long getEntityVersion() {
    throw new NotImplementedException();
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
