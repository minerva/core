package lcsb.mapviewer.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.security.Privilege;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Version;
import java.awt.Color;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

@Entity
public class User implements MinervaEntity {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(unique = true, nullable = false)
  private String login;

  @Column(nullable = false, length = 255)
  @JsonIgnore
  private String cryptedPassword;

  @Column(length = 255, nullable = false)
  private String name;

  @Column(length = 255, nullable = false)
  private String surname;

  @Column(length = 255)
  private String email;

  @Column(length = 255)
  private String orcidId;

  /**
   * User defined color overriding system
   * {@link ConfigurationElementType#MIN_COLOR_VAL}. Used for coloring minimum
   * values in overlays.
   */
  @Type(type = "lcsb.mapviewer.persist.mapper.ColorMapper")
  private Color minColor;

  /**
   * User defined color overriding system
   * {@link ConfigurationElementType#MAX_COLOR_VAL}. Used for coloring maximum
   * values in overlays.
   */
  @Type(type = "lcsb.mapviewer.persist.mapper.ColorMapper")
  private Color maxColor;

  /**
   * User defined color overriding system
   * {@link ConfigurationElementType#NEUTRAL_COLOR_VAL}. Used for coloring neutral
   * values (0) in overlays.
   */
  @Type(type = "lcsb.mapviewer.persist.mapper.ColorMapper")
  private Color neutralColor;

  /**
   * User defined color overriding system
   * {@link ConfigurationElementType#SIMPLE_COLOR_VAL}. Used for coloring overlays
   * without values and colors.
   */
  @Type(type = "lcsb.mapviewer.persist.mapper.ColorMapper")
  private Color simpleColor;

  private boolean removed = false;

  private boolean connectedToLdap = false;

  private boolean termsOfUseConsent = false;

  @ElementCollection(fetch = FetchType.EAGER)
  @CollectionTable(name = "user_terms_of_use_consent", joinColumns = @JoinColumn(name = "user_id"))
  @Column(name = "date")
  @Cascade({org.hibernate.annotations.CascadeType.ALL})
  @JsonIgnore
  private Set<Calendar> termsOfUseConsentDates = new HashSet<>();

  @ManyToMany(cascade = CascadeType.ALL)
  @JoinTable(name = "user_privilege_map_table", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "privilege_id"))
  private Set<Privilege> privileges = new HashSet<>();

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn
  @JsonProperty("preferences")
  private UserAnnotationSchema annotationSchema;

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * Is user active (can user login)
   */
  private boolean active = true;

  /**
   * Is user account email confirmed.
   */
  private boolean confirmed = true;

  public User() {
  }

  public void addPrivilege(final Privilege privilege) {
    privileges.add(privilege);
  }

  public void removePrivilege(final Privilege privilege) {
    privileges.remove(privilege);
  }

  @Override
  public int getId() {
    return id;
  }

  public void setId(final Integer id) {
    this.id = id;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(final String login) {
    this.login = login;
  }

  public String getCryptedPassword() {
    return cryptedPassword;
  }

  public void setCryptedPassword(final String cryptedPassword) {
    this.cryptedPassword = cryptedPassword;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(final String surname) {
    this.surname = surname;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public Set<Privilege> getPrivileges() {
    return privileges;
  }

  public void setPrivileges(final Set<Privilege> privileges) {
    this.privileges = privileges;
  }

  public boolean isRemoved() {
    return removed;
  }

  public void setRemoved(final boolean removed) {
    this.removed = removed;
  }

  public UserAnnotationSchema getAnnotationSchema() {
    return annotationSchema;
  }

  public void setAnnotationSchema(final UserAnnotationSchema annotationSchema) {
    this.annotationSchema = annotationSchema;
    if (annotationSchema != null && !this.equals(annotationSchema.getUser())) {
      annotationSchema.setUser(this);
    }
  }

  @Override
  public String toString() {
    return "[" + this.getClass().getSimpleName() + "] " + getName() + " " + getSurname();
  }

  public Color getMinColor() {
    return minColor;
  }

  public void setMinColor(final Color minColor) {
    this.minColor = minColor;
  }

  public Color getMaxColor() {
    return maxColor;
  }

  public void setMaxColor(final Color maxColor) {
    this.maxColor = maxColor;
  }

  public Color getSimpleColor() {
    return simpleColor;
  }

  public void setSimpleColor(final Color simpleColor) {
    this.simpleColor = simpleColor;
  }

  public Color getNeutralColor() {
    return neutralColor;
  }

  public void setNeutralColor(final Color neutralColor) {
    this.neutralColor = neutralColor;
  }

  public boolean isTermsOfUseConsent() {
    return termsOfUseConsent;
  }

  public void setTermsOfUseConsent(final boolean termsOfUseConsent) {
    if (!this.termsOfUseConsent && termsOfUseConsent) {
      this.getTermsOfUseConsentDates().add(Calendar.getInstance());
    }
    this.termsOfUseConsent = termsOfUseConsent;
  }

  public boolean isConnectedToLdap() {
    return connectedToLdap;
  }

  public void setConnectedToLdap(final boolean connectedToLdap) {
    this.connectedToLdap = connectedToLdap;
  }

  public Set<Calendar> getTermsOfUseConsentDates() {
    return termsOfUseConsentDates;
  }

  public void setTermsOfUseConsentDates(final Set<Calendar> termsOfUseConsentDates) {
    this.termsOfUseConsentDates = termsOfUseConsentDates;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  public String getOrcidId() {
    return orcidId;
  }

  public void setOrcidId(final String orcidId) {
    this.orcidId = orcidId;
  }

  public void setActive(final boolean active) {
    this.active = active;

  }

  public boolean isActive() {
    return active;
  }

  public boolean isConfirmed() {
    return confirmed;
  }

  public void setConfirmed(final boolean confirmed) {
    this.confirmed = confirmed;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
