package lcsb.mapviewer.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.modelutils.serializer.model.user.ConfigurationOptionSerializer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 * This class represents one configurable parameter of the system.
 *
 * @author Piotr Gawron
 */
@Entity
@JsonSerialize(using = ConfigurationOptionSerializer.class)
public class ConfigurationOption implements MinervaEntity {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Type of the configuration element.
   */
  @Enumerated(EnumType.STRING)
  private ConfigurationElementType type;

  /**
   * What is the value of the configuration parameter.
   */
  @Column(length = 255)
  private String value;

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * @return the id
   * @see #id
   */
  @Override
  public int getId() {
    return id;
  }

  /**
   * @param id the id to set
   * @see #id
   */
  public void setId(final int id) {
    this.id = id;
  }

  /**
   * @return the type
   * @see #type
   */
  public ConfigurationElementType getType() {
    return type;
  }

  /**
   * @param type the type to set
   * @see #type
   */
  public void setType(final ConfigurationElementType type) {
    this.type = type;
  }

  /**
   * @return the value
   * @see #value
   */
  public String getValue() {
    return value;
  }

  /**
   * @param value the value to set
   * @see #value
   */
  public void setValue(final String value) {
    this.value = value;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
