package lcsb.mapviewer.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.modelutils.serializer.model.user.UserAnnotationSchemaDeserializer;
import lcsb.mapviewer.modelutils.serializer.model.user.UserAnnotationSchemaSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Version;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Annotation schema used by the user. It contains information about annotators
 * used by the user. Which annotations are valid/required for given object
 * types.
 *
 * @author Piotr Gawron
 */
@Entity
@JsonSerialize(using = UserAnnotationSchemaSerializer.class)
@JsonDeserialize(using = UserAnnotationSchemaDeserializer.class)
public class UserAnnotationSchema implements MinervaEntity {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * {@link User} for which annotation schema is defined.
   */
  @OneToOne
  private User user;

  /**
   * Should the miriam types be validated?
   */
  private Boolean validateMiriamTypes = false;

  private Boolean annotateModel = false;

  private Boolean autoResizeMap = true;

  private Boolean semanticZoomContainsMultipleOverlays = false;

  /**
   * Should map be visualized as sbgn?
   */
  private Boolean sbgnFormat = false;

  /**
   * Should the default view be a network (if not then it will be pathways and
   * compartments)?
   */
  private Boolean networkBackgroundAsDefault = false;

  /**
   * List of class annotators for specific object types.
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(mappedBy = "annotationSchema")
  @OrderBy("id")
  private List<UserClassAnnotators> classAnnotators = new ArrayList<>();

  @Cascade({CascadeType.ALL})
  @OneToMany(mappedBy = "annotationSchema")
  private Set<UserGuiPreference> guiPreferences = new HashSet<>();

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * @return the user
   * @see #user
   */
  public User getUser() {
    return user;
  }

  /**
   * @param user the user to set
   * @see #user
   */
  public void setUser(final User user) {
    this.user = user;
  }

  /**
   * @return the validateMiriamTypes
   * @see #validateMiriamTypes
   */
  public Boolean getValidateMiriamTypes() {
    return validateMiriamTypes;
  }

  /**
   * @param validateMiriamTypes the validateMiriamTypes to set
   * @see #validateMiriamTypes
   */
  public void setValidateMiriamTypes(final Boolean validateMiriamTypes) {
    this.validateMiriamTypes = validateMiriamTypes;
  }

  /**
   * @return the classAnnotators
   * @see #classAnnotators
   */
  public List<UserClassAnnotators> getClassAnnotators() {
    return classAnnotators;
  }

  /**
   * @param classAnnotators the classAnnotators to set
   * @see #classAnnotators
   */
  public void setClassAnnotators(final List<UserClassAnnotators> classAnnotators) {
    this.classAnnotators = classAnnotators;
  }

  /**
   * Adds (or updates) {@link UserClassAnnotators class annotator} information
   * to {@link #classAnnotators}.
   *
   * @param ca object to add/update
   */
  public void addClassAnnotator(final UserClassAnnotators ca) {
    if (ca.getClassName() == null) {
      throw new InvalidArgumentException("Class name cannot be null");
    }
    boolean replaced = false;
    for (int i = 0; i < this.classAnnotators.size(); i++) {
      UserClassAnnotators annotators = this.classAnnotators.get(i);
      if (annotators.getClassName().equals(ca.getClassName())) {
        annotators.setAnnotationSchema(null);
        this.classAnnotators.set(i, ca);
        replaced = true;
      }
    }
    if (!replaced) {
      classAnnotators.add(ca);
    }
    ca.setAnnotationSchema(this);
  }

  /**
   * Returns list of annotators for given object type.
   *
   * @param clazz type of object
   * @return list of annotators names
   */
  public List<AnnotatorData> getAnnotatorsForClass(final Class<?> clazz) {
    for (final UserClassAnnotators ca : classAnnotators) {
      if (ca.getClassName().equals(clazz.getCanonicalName())) {
        return ca.getAnnotators();
      }
    }
    return null;
  }

  /**
   * @return the sbgnFormat
   * @see #sbgnFormat
   */
  public Boolean getSbgnFormat() {
    return sbgnFormat;
  }

  /**
   * @param sbgnFormat the sbgnFormat to set
   * @see #sbgnFormat
   */
  public void setSbgnFormat(final Boolean sbgnFormat) {
    this.sbgnFormat = sbgnFormat;
  }

  public Boolean getNetworkBackgroundAsDefault() {
    return networkBackgroundAsDefault;
  }

  public void setNetworkBackgroundAsDefault(final Boolean networkBackgroundAsDefault) {
    this.networkBackgroundAsDefault = networkBackgroundAsDefault;
  }

  /**
   * @return the annotateModel
   * @see #annotateModel
   */
  public Boolean getAnnotateModel() {
    return annotateModel;
  }

  /**
   * @param annotateModel the annotateModel to set
   * @see #annotateModel
   */
  public void setAnnotateModel(final Boolean annotateModel) {
    this.annotateModel = annotateModel;
  }

  /**
   * @return the autoResizeMap
   * @see #autoResizeMap
   */
  public Boolean getAutoResizeMap() {
    return autoResizeMap;
  }

  /**
   * @param autoResizeMap the autoResizeMap to set
   * @see #autoResizeMap
   */
  public void setAutoResizeMap(final Boolean autoResizeMap) {
    this.autoResizeMap = autoResizeMap;
  }

  public Set<UserGuiPreference> getGuiPreferences() {
    return guiPreferences;
  }

  public void addGuiPreference(final UserGuiPreference option) {
    boolean updated = false;
    for (final UserGuiPreference userOption : guiPreferences) {
      if (userOption.getKey().equals(option.getKey())) {
        userOption.setValue(option.getValue());
        updated = true;
      }
    }
    if (!updated) {
      guiPreferences.add(option);
      option.setAnnotationSchema(this);
    }
  }

  public UserGuiPreference getGuiPreference(final String key) {
    for (final UserGuiPreference userOption : guiPreferences) {
      if (userOption.getKey().equals(key)) {
        return userOption;
      }
    }
    return null;
  }

  public void setGuiPreference(final String key, final String value) {
    UserGuiPreference option = getGuiPreference(key);
    if (option == null) {
      option = new UserGuiPreference();
      option.setKey(key);
      addGuiPreference(option);
    }
    option.setValue(value);
  }

  public Boolean getSemanticZoomContainsMultipleOverlays() {
    return semanticZoomContainsMultipleOverlays;
  }

  public void setSemanticZoomContainsMultipleOverlays(final Boolean semanticZoomContainsMultipleOverlays) {
    this.semanticZoomContainsMultipleOverlays = semanticZoomContainsMultipleOverlays;
  }

  @Override
  public int getId() {
    return id;
  }

  public void setId(final Integer id) {
    this.id = id;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
