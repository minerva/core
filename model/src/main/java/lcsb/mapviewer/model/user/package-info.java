/**
 * Contains structures used for modeling users, privileges, etc.
 */
package lcsb.mapviewer.model.user;
