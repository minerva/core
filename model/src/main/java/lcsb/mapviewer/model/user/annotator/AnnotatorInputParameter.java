package lcsb.mapviewer.model.user.annotator;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.modelutils.serializer.model.user.annotator.AnnotatorInputParameterSerializer;

/**
 * Definition of annotator input. It describes which property/identifier should
 * be used to identify element in the external database.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
@DiscriminatorValue("INPUT_PARAMETER")
@JsonSerialize(using = AnnotatorInputParameterSerializer.class)
public class AnnotatorInputParameter extends AnnotatorParameter {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private BioEntityField field;

  @Enumerated(EnumType.STRING)
  private MiriamType identifierType;

  /**
   * Default constructor for hibernate.
   */
  protected AnnotatorInputParameter() {
  }

  public AnnotatorInputParameter(final BioEntityField field) {
    this(field, null);
  }

  public AnnotatorInputParameter(final MiriamType type) {
    this(null, type);
  }

  public AnnotatorInputParameter(final BioEntityField field, final MiriamType identifierType) {
    this.field = field;
    this.identifierType = identifierType;
  }

  public BioEntityField getField() {
    return field;
  }

  public MiriamType getIdentifierType() {
    return identifierType;
  }

  @Override
  public String toString() {
    return "[" + field + "," + identifierType + "]";
  }
}
