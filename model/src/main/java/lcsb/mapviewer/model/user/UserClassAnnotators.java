package lcsb.mapviewer.model.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lcsb.mapviewer.model.user.annotator.AnnotatorData;

/**
 * This class defines set of default class annotators for a single class type
 * that are used by a user.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
public class UserClassAnnotators implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  /**
   * {@link UserAnnotationSchema} that defines which user is using this set of
   * annotators.
   */
  @ManyToOne
  private UserAnnotationSchema annotationSchema;

  /**
   * Class for which this set of annotators is defined.
   */
  @Column(length = 255)
  private String className;

  /**
   * List of strings defining set of annotators (canonical class names).
   */
  @Cascade({ CascadeType.ALL })
  @OneToMany(mappedBy = "userClassAnnotators", orphanRemoval = true)
  @OrderBy("order_index")
  private List<AnnotatorData> annotators = new ArrayList<>();

  /**
   * Default constructor.
   */
  public UserClassAnnotators() {

  }

  /**
   * Default constructor.
   * 
   * @param objectClass
   *          {@link #className}
   * @param annotators
   *          {@link #annotators}
   */
  public UserClassAnnotators(final Class<?> objectClass, final List<AnnotatorData> annotators) {
    setClassName(objectClass);
    for (final AnnotatorData annotator : annotators) {
      addAnnotator(annotator);
    }
  }

  /**
   * Default constructor.
   * 
   * @param objectClass
   *          {@link #className}
   */
  public UserClassAnnotators(final Class<?> objectClass) {
    this(objectClass, new ArrayList<>());
  }

  /**
   * @return the id
   * @see #id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id
   *          the id to set
   * @see #id
   */
  public void setId(final Integer id) {
    this.id = id;
  }

  /**
   * @return the annotationSchema
   * @see #annotationSchema
   */
  public UserAnnotationSchema getAnnotationSchema() {
    return annotationSchema;
  }

  /**
   * @param annotationSchema
   *          the annotationSchema to set
   * @see #annotationSchema
   */
  public void setAnnotationSchema(final UserAnnotationSchema annotationSchema) {
    this.annotationSchema = annotationSchema;
  }

  /**
   * @return the className
   * @see #className
   */
  public String getClassName() {
    return className;
  }

  /**
   * @param className
   *          the className to set
   * @see #className
   */
  public void setClassName(final String className) {
    this.className = className;
  }

  /**
   * Sets {@link #className}.
   *
   * @param clazz
   *          new {@link #className} value
   */
  public void setClassName(final Class<?> clazz) {
    setClassName(clazz.getCanonicalName());
  }

  /**
   * @return the annotators
   * @see #annotators
   */
  public List<AnnotatorData> getAnnotators() {
    return annotators;
  }

  /**
   * @param annotators
   *          the annotators to set
   * @see #annotators
   */
  public void setAnnotators(final List<AnnotatorData> annotators) {
    for (final AnnotatorData annotator : this.annotators) {
      annotator.setUserClassAnnotators(null);
    }
    this.annotators.clear();
    this.annotators.addAll(annotators);
    for (final AnnotatorData annotator : this.annotators) {
      annotator.setUserClassAnnotators(this);
    }
    fixAnnotatorsOrder();
  }

  /**
   * Adds annotator to {@link #annotators list of annotators}.
   * 
   * @param annotator
   *          object to add
   */
  public void addAnnotator(final AnnotatorData annotator) {
    annotators.add(annotator);
    annotator.setUserClassAnnotators(this);
    fixAnnotatorsOrder();
  }

  private void fixAnnotatorsOrder() {
    for (int i = 0; i < annotators.size(); i++) {
      annotators.get(i).setOrderIndex(i);
    }
  }
}
