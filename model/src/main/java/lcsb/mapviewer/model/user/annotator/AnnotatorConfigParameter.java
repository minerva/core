package lcsb.mapviewer.model.user.annotator;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lcsb.mapviewer.model.user.AnnotatorParamDefinition;
import lcsb.mapviewer.modelutils.serializer.model.user.annotator.AnnotatorConfigParameterSerializer;

/**
 * This class defines set of annotators parameters that are used by a user.
 * 
 * @author David Hoksza
 * 
 */
@Entity
@DiscriminatorValue("CONFIG_PARAMETER")
@JsonSerialize(using = AnnotatorConfigParameterSerializer.class)
public class AnnotatorConfigParameter extends AnnotatorParameter implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Parameter name to be set.
   */
  @Enumerated(EnumType.STRING)
  private AnnotatorParamDefinition type;

  /**
   * Parameter value to be set.
   */
  @Column(length = 255)
  private String value;

  /**
   * Default constructor for hibernate.
   */
  protected AnnotatorConfigParameter() {

  }

  /**
   * Default constructor.
   */
  public AnnotatorConfigParameter(final AnnotatorParamDefinition parameterType, final String paramValue) {
    setType(parameterType);
    setValue(paramValue);
  }

  /**
   * @return the parameter type
   * @see #type
   */
  public AnnotatorParamDefinition getType() {
    return type;
  }

  /**
   * @param paramName
   *          the {@link AnnotatorConfigParameter#type} to set
   */
  public void setType(final AnnotatorParamDefinition paramName) {
    this.type = paramName;
  }

  /**
   * @return the parameter value
   * @see #value
   */
  public String getValue() {
    return value;
  }

  /**
   * @param paramValue
   *          value the {@link AnnotatorConfigParameter#value} to set
   */
  public void setValue(final String paramValue) {
    this.value = paramValue;
  }
}
