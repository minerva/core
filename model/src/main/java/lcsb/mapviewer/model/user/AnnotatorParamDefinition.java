package lcsb.mapviewer.model.user;

/**
 * Definition of a single parameter of an annotator.
 * 
 * @author David Hoksza
 * 
 */
public enum AnnotatorParamDefinition {
  KEGG_ORGANISM_IDENTIFIER("KEGG organism identifier",
      String.class,
      "Space-delimited list of organisms codes for which homologous genes"
          + " (final GENE section in the KEGG enzyme record) should be imported."
          + " Currently only ATH (final Arabidopsis Thaliana) is supported.");
  private String name;

  private String description;

  private Class<?> type;

  AnnotatorParamDefinition(final String name, final Class<?> type, final String description) {
    this.name = name;
    this.description = description;
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public Class<?> getType() {
    return type;
  }

}
