package lcsb.mapviewer.model.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * This class defines GUI preference for the {@link User}.
 * 
 * @author Piotr Gawron
 * 
 */
@Entity
public class UserGuiPreference implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  /**
   * {@link UserAnnotationSchema} that defines which user is using this
   * parameter.
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "annotation_schema_id")
  private UserAnnotationSchema annotationSchema;

  /**
   * GUI parameter name.
   */
  @Column(nullable = false, length = 255)
  private String key;

  /**
   * GUI parameter value.
   */
  @Column(nullable = false, length = 255)
  private String value;

  public UserGuiPreference() {

  }

  public UserGuiPreference(final String key, final String value) {
    this.key = key;
    this.value = value;
  }

  public UserAnnotationSchema getAnnotationSchema() {
    return annotationSchema;
  }

  public void setAnnotationSchema(final UserAnnotationSchema annotationSchema) {
    this.annotationSchema = annotationSchema;
  }

  public String getKey() {
    return key;
  }

  public void setKey(final String key) {
    this.key = key;
  }

  public String getValue() {
    return value;
  }

  public void setValue(final String value) {
    this.value = value;
  }
}
