package lcsb.mapviewer.model.user;

public enum ConfigurationElementTypeGroup {
  EMAIL_NOTIFICATION("Email notification details"),
  LEGEND_AND_LOGO("Legend and logo"),
  OVERLAYS("Overlays"),
  POINT_AND_CLICK("Point and click"),
  SERVER_CONFIGURATION("Server configuration"),
  LDAP_CONFIGURATION("LDAP configuration"),
  SEARCH_VISIBLE_PARAMETERS("Search panel options"),
  DAPI("Data-API configuration"),
  OAUTH("OAuth conifguration"),

  ;

  private String commonName;

  ConfigurationElementTypeGroup(final String commonName) {
    this.commonName = commonName;
  }

  public String getCommonName() {
    return commonName;
  }

  public void setCommonName(final String commonName) {
    this.commonName = commonName;
  }
}
