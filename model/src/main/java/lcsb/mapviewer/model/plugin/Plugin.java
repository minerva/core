package lcsb.mapviewer.model.plugin;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.comparator.IntegerComparator;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.modelutils.serializer.model.plugin.PluginSerializer;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OrderBy;
import javax.persistence.Version;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Meta data of the plugin used in the system.
 *
 * @author Piotr Gawron
 */
@Entity
@JsonSerialize(using = PluginSerializer.class)
public class Plugin implements MinervaEntity {

  public static final Comparator<? super Plugin> ID_COMPARATOR = new Comparator<Plugin>() {

    @Override
    public int compare(final Plugin o1, final Plugin o2) {
      return new IntegerComparator().compare(o1.getId(), o2.getId());
    }
  };

  private static final long serialVersionUID = 1L;

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Hash of the plugin code used as identifier to distinguish between plugins.
   */
  @Column(length = 255, nullable = false)
  private String hash;

  /**
   * Name of the plugin.
   */
  @Column(length = 255, nullable = false)
  private String name;

  /**
   * Version of the plugin.
   */
  @Column(length = 255, nullable = false)
  private String version;

  /**
   * Is the plugin public.
   */
  private boolean isPublic = false;

  private boolean isDefault = false;

  /**
   * List of urls from which plugin can be downloaded.
   */
  @ElementCollection(fetch = FetchType.EAGER)
  @CollectionTable(name = "plugin_urls", joinColumns = @JoinColumn(name = "plugin_id"))
  @Column(name = "url")
  @OrderBy("idx")
  private List<String> urls = new ArrayList<>();

  @Version
  @JsonIgnore
  private long entityVersion;

  public String getHash() {
    return hash;
  }

  public void setHash(final String hash) {
    this.hash = hash;
  }

  public List<String> getUrls() {
    return urls;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(final String version) {
    this.version = version;
  }

  @Override
  public int getId() {
    return id;
  }

  public void setId(final int id) {
    this.id = id;
  }

  public boolean isPublic() {
    return isPublic;
  }

  public void setPublic(final boolean isPublic) {
    this.isPublic = isPublic;
  }

  public void addUrl(final String string) {
    if (!urls.contains(string)) {
      urls.add(string);
    }

  }

  public boolean isDefault() {
    return isDefault;
  }

  public void setDefault(final boolean isDefault) {
    this.isDefault = isDefault;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
