package lcsb.mapviewer.model.plugin;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.modelutils.serializer.model.plugin.PluginDataEntrySerializer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

/**
 * Single entry of data stored by the plugin.
 *
 * @author Piotr Gawron
 */
@Entity
@JsonSerialize(using = PluginDataEntrySerializer.class)
public class PluginDataEntry implements MinervaEntity {

  private static final long serialVersionUID = 1L;

  /**
   * Unique database identifier.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @ManyToOne(fetch = FetchType.LAZY, optional = true)
  private User user;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  private Plugin plugin;

  @Column(nullable = false, length = 1024)
  private String key;

  @Column(nullable = false, length = 1024 * 1024)
  private String value;

  @Version
  @JsonIgnore
  private long entityVersion;

  @Override
  public int getId() {
    return id;
  }

  public void setId(final int id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }

  public void setUser(final User user) {
    this.user = user;
  }

  public Plugin getPlugin() {
    return plugin;
  }

  public void setPlugin(final Plugin plugin) {
    this.plugin = plugin;
  }

  public String getKey() {
    return key;
  }

  public void setKey(final String key) {
    this.key = key;
  }

  public String getValue() {
    return value;
  }

  public void setValue(final String value) {
    this.value = value;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
