package lcsb.mapviewer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.model.map.layout.ProjectBackground;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.user.User;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
public class Project implements Serializable, MinervaEntity {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Version
  @JsonIgnore
  private long entityVersion;

  @Column(unique = true, nullable = false, length = 255)
  private String projectId;

  @Cascade({CascadeType.ALL})
  @OneToOne
  private MiriamData disease;

  @Column(length = 255, nullable = false)
  private String name = "";

  private String version;

  @ManyToOne(optional = false)
  private User owner;

  @Column(nullable = false)
  private String notifyEmail = "";

  private String directory;

  /**
   * Status of the project used during uploading the project to the system.
   */
  @Enumerated(EnumType.STRING)
  @Column(nullable = false)
  private ProjectStatus status = ProjectStatus.UNKNOWN;

  /**
   * Progress of single step of project uploading.
   */
  private double progress = 0;

  @Cascade({CascadeType.ALL})
  @OneToMany(mappedBy = "project", orphanRemoval = true)
  @LazyCollection(LazyCollectionOption.EXTRA)
  private Set<ProjectLogEntry> logEntries = new HashSet<>();

  private Calendar creationDate = Calendar.getInstance();

  /**
   * Top map of the project.
   */
  @Cascade({CascadeType.ALL})
  @OneToOne(fetch = FetchType.LAZY)
  private ModelData topModel;

  @Cascade({CascadeType.ALL})
  @OneToMany(mappedBy = "project")
  private Set<ModelData> models = new HashSet<>();

  @Cascade({CascadeType.ALL})
  @OneToMany(mappedBy = "project", orphanRemoval = true)
  @OrderBy("orderIndex")
  private List<ProjectBackground> projectBackgrounds = new ArrayList<>();

  @Cascade({CascadeType.ALL})
  @OneToMany(mappedBy = "project", orphanRemoval = true)
  @OrderBy("id")
  private List<DataOverlay> dataOverlays = new ArrayList<>();

  @Cascade({CascadeType.SAVE_UPDATE})
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "file_entry_id")
  private UploadedFileEntry inputData;

  @Cascade({CascadeType.ALL})
  @OneToOne
  private MiriamData organism;

  @ManyToOne
  private License license;

  @Column(length = 255)
  private String customLicenseUrl = "";

  @Column(length = 1023)
  private String customLicenseName = "";

  private boolean sbgnFormat = false;

  /**
   * List of overview images describing this {@link Project}. The elements on the
   * list can create a hierarchy that describe dependencies, however now matter if
   * the image is a top-level object or a leaf it should be placed on this list.
   */
  @Cascade({CascadeType.ALL})
  @OneToMany(mappedBy = "project", orphanRemoval = true, fetch = FetchType.EAGER)
  @OrderBy("id")
  @Fetch(FetchMode.SUBSELECT)
  private List<OverviewImage> overviewImages = new ArrayList<>();

  @Cascade({CascadeType.ALL})
  @OneToMany(mappedBy = "project", orphanRemoval = true)
  @OrderBy("id")
  @LazyCollection(LazyCollectionOption.FALSE)
  @Fetch(FetchMode.SUBSELECT)
  private List<Glyph> glyphs = new ArrayList<>();

  public Project() {
  }

  public Project(final String projectId) {
    this.projectId = projectId;
  }

  public void addModel(final ModelData model) {
    models.add(model);
    model.setProject(this);
    if (topModel == null) {
      topModel = model;
    }
  }

  public void addModel(final Model model) {
    addModel(model.getModelData());
  }

  public void removeModel(final ModelData model) {
    models.remove(model);
  }

  @Override
  public int getId() {
    return id;
  }

  public void setId(final int id) {
    this.id = id;
  }

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(final String projectId) {
    this.projectId = projectId;
  }

  public ProjectStatus getStatus() {
    return status;
  }

  public void setStatus(final ProjectStatus status) {
    this.status = status;
  }

  public double getProgress() {
    return progress;
  }

  public void setProgress(final double progress) {
    this.progress = progress;
  }

  public Set<ModelData> getModels() {
    return models;
  }

  public void setModels(final Set<ModelData> models) {
    this.models = models;
  }

  public String getDirectory() {
    return directory;
  }

  public void setDirectory(final String directory) {
    this.directory = directory;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public MiriamData getDisease() {
    return disease;
  }

  public void setDisease(final MiriamData disease) {
    this.disease = disease;
  }

  public MiriamData getOrganism() {
    return organism;
  }

  public void setOrganism(final MiriamData organism) {
    this.organism = organism;
  }

  public boolean isSbgnFormat() {
    return sbgnFormat;
  }

  public void setSbgnFormat(final boolean sbgnFormat) {
    this.sbgnFormat = sbgnFormat;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(final String version) {
    this.version = version;
  }

  public List<OverviewImage> getOverviewImages() {
    return overviewImages;
  }

  protected void setOverviewImages(final List<OverviewImage> overviewImages) {
    this.overviewImages = overviewImages;
  }

  public void addOverviewImage(final OverviewImage oi) {
    this.overviewImages.add(oi);
    oi.setProject(this);
  }

  public void addOverviewImages(final List<OverviewImage> parseOverviewLinks) {
    for (final OverviewImage overviewImage : parseOverviewLinks) {
      this.addOverviewImage(overviewImage);
    }

  }

  public UploadedFileEntry getInputData() {
    return inputData;
  }

  public void setInputData(final UploadedFileEntry inputData) {
    this.inputData = inputData;
  }

  public String getNotifyEmail() {
    return notifyEmail;
  }

  public void setNotifyEmail(final String notifyEmail) {
    this.notifyEmail = notifyEmail;
  }

  public void addProjectBackground(final ProjectBackground projectBackground) {
    projectBackgrounds.add(projectBackground);
    projectBackground.setProject(this);
  }

  public void addProjectBackground(final int index, final ProjectBackground projectBackground) {
    projectBackgrounds.add(index, projectBackground);
    projectBackground.setProject(this);
  }

  public void addProjectBackgrounds(final Collection<ProjectBackground> projectBackgrounds) {
    for (final ProjectBackground projectBackground : projectBackgrounds) {
      addProjectBackground(projectBackground);
    }
  }

  public List<ProjectBackground> getProjectBackgrounds() {
    return projectBackgrounds;
  }

  protected void setProjectBackgrounds(final List<ProjectBackground> projectBackgrounds) {
    this.projectBackgrounds = projectBackgrounds;
  }

  public void removeProjectBackground(final ProjectBackground projectBackground) {
    this.projectBackgrounds.remove(projectBackground);
    projectBackground.setProject(null);
  }

  public Calendar getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(final Calendar creationDate) {
    this.creationDate = creationDate;
  }

  public List<Glyph> getGlyphs() {
    return glyphs;
  }

  public void setGlyphs(final List<Glyph> glyphs) {
    this.glyphs = glyphs;
  }

  public void addGlyph(final Glyph parseGlyph) {
    this.glyphs.add(parseGlyph);
    parseGlyph.setProject(this);
  }

  public User getOwner() {
    return owner;
  }

  public void setOwner(final User owner) {
    this.owner = owner;
  }

  public Set<ProjectLogEntry> getLogEntries() {
    return logEntries;
  }

  public void setLogEntries(final Set<ProjectLogEntry> logEntries) {
    this.logEntries = logEntries;
  }

  public void addLogEntry(final ProjectLogEntry entry) {
    logEntries.add(entry);
    entry.setProject(this);

  }

  public void addLogEntries(final Collection<ProjectLogEntry> createLogEntries) {
    for (final ProjectLogEntry projectLogEntry : createLogEntries) {
      addLogEntry(projectLogEntry);
    }
  }

  public Model getTopModel() {
    if (topModel == null) {
      return null;
    }
    if (topModel.getModel() == null) {
      return new ModelFullIndexed(topModel);
    }
    return topModel.getModel();
  }

  public void setTopModel(final ModelData topModel) {
    this.topModel = topModel;
    addModel(topModel);
  }

  @JsonIgnore
  public void setTopModel(final Model topModel) {
    setTopModel(topModel.getModelData());
  }

  public ModelData getTopModelData() {
    return topModel;
  }

  public ModelData getMapById(final String id) {
    if (!StringUtils.isNumeric(id)) {
      throw new InvalidArgumentException("Invalid map id: " + id);
    }
    final int integerId = Integer.parseInt(id);
    for (final ModelData map : getModels()) {
      if (map.getId() == integerId) {
        return map;
      }
    }
    return null;
  }

  public Set<ModelData> getMapsById(final String mapId) {
    if (Objects.equals("*", mapId)) {
      return getModels();
    }

    final Set<ModelData> result = new HashSet<>();
    final ModelData map = getMapById(mapId);
    if (map != null) {
      result.add(map);
    }
    return result;
  }

  public List<DataOverlay> getDataOverlays() {
    return dataOverlays;
  }

  public void setDataOverlays(final List<DataOverlay> dataOverlays) {
    this.dataOverlays = dataOverlays;
  }

  public void addDataOverlay(final DataOverlay dataOverlay) {
    this.dataOverlays.add(dataOverlay);
    dataOverlay.setProject(this);
  }

  public License getLicense() {
    return license;
  }

  public void setLicense(final License license) {
    this.license = license;
  }

  public String getCustomLicenseUrl() {
    return customLicenseUrl;
  }

  public void setCustomLicenseUrl(final String customLicenseUrl) {
    this.customLicenseUrl = customLicenseUrl;
  }

  public String getCustomLicenseName() {
    return customLicenseName;
  }

  public void setCustomLicenseName(final String customLicenseName) {
    this.customLicenseName = customLicenseName;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }

}
