package lcsb.mapviewer.model.graphics;

import com.fasterxml.jackson.annotation.JsonProperty;
import lcsb.mapviewer.common.exception.NotImplementedException;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * This class define data for arrow ends.
 *
 * @author Piotr Gawron
 */
public class ArrowTypeData implements Serializable {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default angle of the arrow.
   */
  private static final double DEFAULT_ARROW_ANGLE = 0.875 * Math.PI;

  /**
   * Default length of the arrow.
   */
  private static final int DEFAULT_ARROW_LENGTH = 15;

  /**
   * Type of the arrow.
   */
  @Enumerated(EnumType.STRING)
  @NotNull
  private ArrowType arrowType;

  /**
   * Defines type of the line used to draw arrow end.
   */
  @Enumerated(EnumType.STRING)
  @JsonProperty(value = "lineType")
  @NotNull
  private LineType arrowLineType;

  /**
   * Defines length of the arrow head.
   */
  @JsonProperty(value = "length")
  private double len;

  /**
   * Defines angle of the arrow head (when necessary).
   */
  private double angle;

  /**
   * Default constructor - uses default values.
   */
  public ArrowTypeData() {
    arrowType = ArrowType.NONE;
    arrowLineType = LineType.SOLID;
    len = DEFAULT_ARROW_LENGTH;
    angle = DEFAULT_ARROW_ANGLE;
  }

  /**
   * Constructor that creates object with the data copied from param object.
   *
   * @param param object used for initialization of the data
   */
  public ArrowTypeData(final ArrowTypeData param) {
    arrowType = param.arrowType;
    arrowLineType = param.arrowLineType;
    len = param.len;
    angle = param.angle;
  }

  /**
   * Creates a copy of the object.
   *
   * @return object copy
   */
  public ArrowTypeData copy() {
    if (this.getClass() == ArrowTypeData.class) {
      return new ArrowTypeData(this);
    } else {
      throw new NotImplementedException("Method copy() should be overridden in class " + this.getClass());
    }
  }

  /**
   * @return the arrowType
   * @see #arrowType
   */
  public ArrowType getArrowType() {
    return arrowType;
  }

  /**
   * @param arrowType the arrowType to set
   * @see #arrowType
   */
  public void setArrowType(final ArrowType arrowType) {
    this.arrowType = arrowType;
  }

  /**
   * @return the arrowLineType
   * @see #arrowLineType
   */
  public LineType getArrowLineType() {
    return arrowLineType;
  }

  /**
   * @param arrowLineType the arrowLineType to set
   * @see #arrowLineType
   */
  public void setArrowLineType(final LineType arrowLineType) {
    this.arrowLineType = arrowLineType;
  }

  /**
   * @return the len
   * @see #len
   */
  public double getLen() {
    return len;
  }

  /**
   * @param len the len to set
   * @see #len
   */
  public void setLen(final double len) {
    this.len = len;
  }

  /**
   * @return the angle
   * @see #angle
   */
  public double getAngle() {
    return angle;
  }

  /**
   * @param angle the angle to set
   * @see #angle
   */
  public void setAngle(final double angle) {
    this.angle = angle;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof ArrowTypeData) {
      ArrowTypeData argument = (ArrowTypeData) obj;
      return getAngle() == argument.getAngle() && getLen() == argument.getLen()
          && getArrowLineType() == argument.getArrowLineType() && getArrowType() == argument.getArrowType();
    }
    return super.equals(obj);
  }
}
