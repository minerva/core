/**
 * Provides structures that represent graphical elements. The elements doesn't
 * have to belong to the map.
 */
package lcsb.mapviewer.model.graphics;
