package lcsb.mapviewer.model.graphics;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.common.Comparator;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.comparator.DoubleComparator;

/**
 * This class implements comparator interface for ArrowTypeData. It compares the
 * content, but skip database identifier.
 * 
 * @author Piotr Gawron
 * 
 */
public class ArrowTypeDataComparator extends Comparator<ArrowTypeData> {
  /**
   * Default class logger.
   */
  private static Logger logger = LogManager.getLogger();

  /**
   * Epsilon value used for comparison of doubles.
   */
  private double epsilon;

  /**
   * Constructor that requires {@link #epsilon} parameter.
   * 
   * @param epsilon
   *          {@link #epsilon}
   */
  public ArrowTypeDataComparator(final double epsilon) {
    super(ArrowTypeData.class);
    this.epsilon = epsilon;
  }

  /**
   * Default constructor.
   */
  public ArrowTypeDataComparator() {
    this(Configuration.EPSILON);
  }

  @Override
  protected int internalCompare(final ArrowTypeData arg0, final ArrowTypeData arg1) {
    DoubleComparator doubleComparator = new DoubleComparator(epsilon);

    if (arg0.getArrowType().compareTo(arg1.getArrowType()) != 0) {
      logger.debug("Different arrow type: " + arg0.getArrowType() + ", " + arg1.getArrowType());
      return arg0.getArrowType().compareTo(arg1.getArrowType());
    }

    if (arg0.getArrowLineType().compareTo(arg1.getArrowLineType()) != 0) {
      logger.debug("Different arrow line type: " + arg0.getArrowLineType() + ", " + arg1.getArrowLineType());
      return arg0.getArrowLineType().compareTo(arg1.getArrowLineType());
    }

    if (doubleComparator.compare(arg0.getLen(), arg1.getLen()) != 0) {
      logger.debug("Different length: " + arg0.getLen() + ", " + arg1.getLen());
      return doubleComparator.compare(arg0.getLen(), arg1.getLen());
    }

    if (doubleComparator.compare(arg0.getAngle(), arg1.getAngle()) != 0) {
      logger.debug("Different angle: " + arg0.getAngle() + ", " + arg1.getAngle());
      return doubleComparator.compare(arg0.getAngle(), arg1.getAngle());
    }
    return 0;
  }

}
