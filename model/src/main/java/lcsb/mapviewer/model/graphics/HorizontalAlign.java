package lcsb.mapviewer.model.graphics;

public enum HorizontalAlign {
  LEFT,
  RIGHT,
  CENTER
}
