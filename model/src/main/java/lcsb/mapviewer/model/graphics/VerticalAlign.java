package lcsb.mapviewer.model.graphics;

public enum VerticalAlign {
  TOP,
  MIDDLE,
  BOTTOM
}
