package lcsb.mapviewer.model.graphics;

import lcsb.mapviewer.common.comparator.FloatComparator;

import java.awt.BasicStroke;
import java.awt.Stroke;
import java.util.Arrays;
import java.util.List;

/**
 * Available types of lines in the system.
 *
 * @author Piotr Gawron
 */
public enum LineType {
  /**
   * Solid line.
   */
  SOLID(),

  /**
   * Dash-dot-dot line:
   *
   * <pre>
   *  - . . - . . - . . - . .
   * </pre>
   */
  DASH_DOT_DOT(new float[]{11.0f, 3.0f, 1.0f, 3.0f, 1.0f, 3.0f}),

  /**
   * Dash-dot line:
   *
   * <pre>
   *  - . - . - . - . - . - . - .
   * </pre>
   */
  DASH_DOT(new float[]{11.0f, 3.0f, 1.0f, 3.0f}),

  /**
   * Dashed line:
   *
   * <pre>
   *  - - - - - - -
   * </pre>
   */
  DASHED(new float[]{8.0f, 8.0f}),

  /**
   * Dotted line:
   *
   * <pre>
   *  . . . . . . . .
   * </pre>
   */
  DOTTED(new float[]{2.0f, 5.0f});

  /**
   * Default miterlimit: "the limit to trim the miter join. The miterlimit must be
   * greater than or equal to 1.0f " taken from
   * <a href="http://docs.oracle.com/javase/7/docs/api/java/awt/BasicStroke.html"
   * >oracle webpage</a>
   */
  private static final float DEFAULT_MITERLIMIT = 10.0f;

  /**
   * {@link java.awt.BasicStroke} defined for the line type.
   */
  private float[] pattern;

  LineType() {
  }

  /**
   * Constructor with line width and pattern as a parameter.
   *
   * @param pattern pattern of the line
   */
  LineType(final float[] pattern) {
    this.pattern = pattern;
  }

  public static LineType getTypeByDashArray(final List<Short> strokeDashArray) {
    FloatComparator doubleComparator = new FloatComparator();
    for (final LineType type : LineType.values()) {
      float[] pattern = type.pattern;
      if (pattern != null && strokeDashArray.size() == pattern.length) {
        boolean match = true;
        for (int i = 0; i < strokeDashArray.size(); i++) {
          if (doubleComparator.compare(pattern[i], (float) strokeDashArray.get(i)) != 0) {
            match = false;
            break;
          }
        }
        if (match) {
          return type;
        }
      }
    }
    return LineType.SOLID;
  }

  /**
   * Return the {@link BasicStroke} for this line type.
   *
   * @return the {@link BasicStroke} for this line type
   */
  public Stroke getStroke(final float width) {

    return new BasicStroke(width, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER, DEFAULT_MITERLIMIT, pattern,
        0.0f);

  }

  public Stroke getStroke(final double width) {
    return getStroke((float) width);
  }

  public float[] getPattern() {
    if (pattern == null) {
      return new float[0];
    }
    return Arrays.copyOf(pattern, pattern.length);
  }
}
