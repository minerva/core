package lcsb.mapviewer.model.graphics;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.common.geometry.ImmutableLine2D;
import lcsb.mapviewer.common.geometry.ImmutablePoint2D;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.map.Drawable;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Version;
import java.awt.Color;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class represents general line structure. Line can contain several
 * segments. Beginning and the end of the line could be finished with the arrow.
 * There are also color and line width associated with the object.
 *
 * @author Piotr Gawron
 */
@Entity
public class PolylineData implements MinervaEntity, Drawable {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Class used to perform some operation on points.
   */
  private static final PointTransformation pt = new PointTransformation();

  /**
   * Default class logger.
   */
  private static final Logger logger = LogManager.getLogger();

  /**
   * Unique identifier in the database.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * List of points that build the line.
   */
  @ElementCollection(fetch = FetchType.EAGER)
  @Fetch(FetchMode.SUBSELECT)
  @CollectionTable(name = "point_table", joinColumns = @JoinColumn(name = "id"))
  @OrderColumn(name = "idx")
  @Columns(columns = {@Column(name = "x1"), @Column(name = "y1"), @Column(name = "x2"), @Column(name = "y2")})
  @Type(type = "lcsb.mapviewer.persist.mapper.Line2DMapper")
  @Cascade({CascadeType.ALL})
  @JsonProperty("segments")
  private List<Line2D> segments = new ArrayList<>();

  /**
   * Arrow at the beginning of the line.
   */
  @Type(type = "lcsb.mapviewer.persist.mapper.ArrowTypeDataMapper")
  @JsonProperty(value = "startArrow")
  private ArrowTypeData beginAtd = new ArrowTypeData();

  /**
   * Arrow at the end of the line.
   */
  @Type(type = "lcsb.mapviewer.persist.mapper.ArrowTypeDataMapper")
  @JsonProperty(value = "endArrow")
  private ArrowTypeData endAtd = new ArrowTypeData();

  /**
   * Width of the line.
   */
  private double width = 1;

  /**
   * Color of the line.
   */
  @Type(type = "lcsb.mapviewer.persist.mapper.ColorMapper")
  @Column(nullable = false)
  private Color color = Color.BLACK;

  /**
   * Type of the line (pattern used to draw it).
   */
  @Enumerated(EnumType.STRING)
  @JsonProperty(value = "lineType")
  private LineType type = LineType.SOLID;

  @Version
  @JsonIgnore
  private long entityVersion;

  @Cascade({CascadeType.DETACH})
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinTable(
      name = "layer_table_lines",
      inverseJoinColumns = {@JoinColumn(name = "layer_table_id")},
      joinColumns = {@JoinColumn(name = "lines_id")}
  )
  @JsonIgnore
  private Layer layer;

  private int z = 0;

  /**
   * Default constructor for simple to points line.
   *
   * @param startPoint start point of the line
   * @param endPoint   end point of the line
   */
  public PolylineData(final Point2D startPoint, final Point2D endPoint) {
    addLine(new Line2D.Double(startPoint, endPoint));
  }

  /**
   * Default constructor.
   */
  public PolylineData() {
  }

  /**
   * Constructor that creates object initialized by the parameter object.
   *
   * @param param parameter used for data initialization
   */
  public PolylineData(final PolylineData param) {
    segments = new ArrayList<>();
    for (final Line2D line : param.getLines()) {
      addLine(new Line2D.Double(line.getP1(), line.getP2()));
    }
    beginAtd = new ArrowTypeData(param.getBeginAtd());
    endAtd = new ArrowTypeData(param.getEndAtd());
    width = param.getWidth();
    color = param.getColor();
    type = param.getType();
  }

  /**
   * Creates line from the list of points.
   *
   * @param pointsList list of points that represent the line
   */
  public PolylineData(final List<Point2D> pointsList) {
    if (pointsList.size() == 1) {
      throw new InvalidArgumentException("At least two points are required");
    }
    for (int i = 1; i < pointsList.size(); i++) {
      addLine(pointsList.get(i - 1), pointsList.get(i));
    }
  }

  public void addLine(final Line2D line) {
    if (!pt.isValidPoint(line.getP1())) {
      throw new InvalidArgumentException("Invalid coordinates: " + line.getP1());
    }
    if (!pt.isValidPoint(line.getP2())) {
      throw new InvalidArgumentException("Invalid coordinates: " + line.getP2());
    }
    segments.add(new Line2D.Double(line.getX1(), line.getY1(), line.getX2(), line.getY2()));
  }

  public void addLine(final Point2D p1, final Point2D p2) {
    addLine(new Line2D.Double(p1, p2));
  }

  public void addLine(final double x1, final double y1, final double x2, final double y2) {
    addLine(new Line2D.Double(x1, y1, x2, y2));
  }

  /**
   * Returns all segment lines as a list of {@link Line2D} objects.
   *
   * @return list of segment lines
   */
  @JsonIgnore
  public List<Line2D> getLines() {
    final List<Line2D> data = new ArrayList<>();
    for (final Line2D line2d : segments) {
      data.add(new ImmutableLine2D(line2d));
    }
    return Collections.unmodifiableList(data);
  }

  /**
   * Returns coordinates of the last point in the line.
   *
   * @return coordinates of the last point in the line
   */
  @JsonIgnore
  public Point2D getEndPoint() {
    return new ImmutablePoint2D(segments.get(segments.size() - 1).getP2());
  }

  /**
   * Transforms line representation into {@link GeneralPath} class.
   *
   * @return {@link GeneralPath} representation of line
   */
  public GeneralPath toGeneralPath() {
    final GeneralPath path = new GeneralPath();
    Point2D p = segments.get(0).getP1();
    path.moveTo(p.getX(), p.getY());
    for (final Line2D line : segments) {
      p = line.getP2();
      path.lineTo(p.getX(), p.getY());
    }
    return path;
  }

  /**
   * Trims the end of the line.
   *
   * @param distToTrim distance by which end of line should be trimmed
   */
  public double trimEnd(final double distToTrim) {
    final Line2D line = segments.get(segments.size() - 1);
    final Point2D last = line.getP2();
    final Point2D last2 = line.getP1();
    final double oldDist = last.distance(last2);
    if (distToTrim < 0 && oldDist <= Configuration.EPSILON) {
      logger.debug("Cannot extend empty line");
      line.setLine(last2, last2);
      return 0;
    } else if (oldDist <= distToTrim) {
      line.setLine(last2, last2);
      return oldDist;
    } else {
      final double newDistr = oldDist - distToTrim;
      final double ratio = newDistr / oldDist;
      double dx = last.getX() - last2.getX();
      double dy = last.getY() - last2.getY();
      dx *= ratio;
      dy *= ratio;
      line.setLine(last2.getX(), last2.getY(), last2.getX() + dx, last2.getY() + dy);
      return distToTrim;
    }
  }

  /**
   * Trims the beginning of the line.
   *
   * @param distToTrim distance by which beginning of line should be trimmed
   */
  public double trimBegin(final double distToTrim) {
    final Line2D line = segments.get(0);
    final Point2D last = line.getP1();
    final Point2D last2 = line.getP2();
    final double oldDist = last.distance(last2);
    if (oldDist <= distToTrim) {
      line.setLine(last2, last2);
      return oldDist;
    } else {
      final double newDistr = oldDist - distToTrim;
      final double ratio = newDistr / oldDist;
      double dx = last.getX() - last2.getX();
      double dy = last.getY() - last2.getY();
      dx *= ratio;
      dy *= ratio;
      line.setLine(last2.getX() + dx, last2.getY() + dy, last2.getX(), last2.getY());
      return distToTrim;
    }
  }

  /**
   * Returns the object with reversed order of line.
   *
   * @return reversed line
   */
  public PolylineData reverse() {
    final PolylineData result = new PolylineData(this);
    result.segments = new ArrayList<>();
    for (final Line2D line : getLines()) {
      result.addLine(new Line2D.Double(line.getP2(), line.getP1()));
    }
    Collections.reverse(result.segments);
    result.setBeginAtd(getEndAtd());
    result.setEndAtd(getBeginAtd());
    return result;
  }

  /**
   * Returns length of the whole line.
   *
   * @return length of the line
   */
  public double length() {
    double dist = 0;
    for (final Line2D line : segments) {
      dist += line.getP1().distance(line.getP2());
    }
    return dist;
  }

  /**
   * Sets first point in the line.
   *
   * @param point new first point in the line
   */
  @JsonIgnore
  public void setStartPoint(final Point2D point) {
    if (!pt.isValidPoint(point)) {
      throw new InvalidArgumentException("Invalid coordinates: " + point);
    }
    final Line2D line = segments.get(0);
    line.setLine(point, line.getP2());
  }

  public void setStartPoint(final double x, final double y) {
    setStartPoint(new Point2D.Double(x, y));
  }

  public void setEndPoint(final Point2D point) {
    if (!pt.isValidPoint(point)) {
      throw new InvalidArgumentException("Invalid coordinates: " + point);
    }
    final Line2D line = segments.get(segments.size() - 1);
    line.setLine(line.getP1(), point);
  }

  public void setEndPoint(final double x, final double y) {
    setEndPoint(new Point2D.Double(x, y));
  }

  /**
   * Returns first point in the line.
   *
   * @return first point in the line
   */
  @JsonIgnore
  public Point2D getStartPoint() {
    return new ImmutablePoint2D(segments.get(0).getP1());
  }

  /**
   * @return the beginAtd
   * @see #beginAtd
   */
  public ArrowTypeData getBeginAtd() {
    return beginAtd;
  }

  /**
   * @param beginAtd the beginAtd to set
   * @see #beginAtd
   */
  public void setBeginAtd(final ArrowTypeData beginAtd) {
    this.beginAtd = beginAtd;
  }

  /**
   * @return the endAtd
   * @see #endAtd
   */
  public ArrowTypeData getEndAtd() {
    return endAtd;
  }

  /**
   * @param endAtd the endAtd to set
   * @see #endAtd
   */
  public void setEndAtd(final ArrowTypeData endAtd) {
    this.endAtd = endAtd;
  }

  /**
   * @return the width
   * @see #width
   */
  public double getWidth() {
    return width;
  }

  /**
   * @param width the width to set
   * @see #width
   */
  public void setWidth(final double width) {
    this.width = width;
  }

  /**
   * @param string the width to set
   * @see #width
   */
  public void setWidth(final String string) {
    setWidth(Double.parseDouble(string));
  }

  /**
   * @return the color
   * @see #color
   */
  public Color getColor() {
    return color;
  }

  /**
   * @param color the color to set
   * @see #color
   */
  public void setColor(final Color color) {
    this.color = color;
  }

  /**
   * @return the type
   * @see #type
   */
  public LineType getType() {
    return type;
  }

  /**
   * @param type the type to set
   * @see #type
   */
  public void setType(final LineType type) {
    this.type = type;
  }

  /**
   * @return the id
   * @see #id
   */
  @Override
  public int getId() {
    return id;
  }

  /**
   * @param id the id to set
   * @see #id
   */
  public void setId(final int id) {
    this.id = id;
  }

  /**
   * Prepares a copy of the object.
   *
   * @return copy of {@link PolylineData}
   */
  public PolylineData copy() {
    if (this.getClass().equals(PolylineData.class)) {
      return new PolylineData(this);
    } else {
      throw new NotImplementedException("Copy method for " + this.getClass() + " class not implemented.");
    }
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append("[" + this.getClass().getSimpleName() + "]: ");
    for (final Line2D point : segments) {
      sb.append(point.getX1() + ", " + point.getY1() + "-" + point.getX2() + ", " + point.getY2() + "; ");
    }
    return sb.toString();
  }

  @Override
  public Integer getZ() {
    return z;
  }

  @Override
  public void setZ(final Integer z) {
    if (z == null) {
      logger.warn("z index cannot be null");
    } else {
      this.z = z;
    }
  }

  @Override
  @JsonIgnore
  public String getElementId() {
    return toString();
  }

  @Override
  @JsonIgnore
  public double getSize() {
    return 0;
  }

  public void addLines(final List<Line2D> lines) {
    for (final Line2D line : lines) {
      addLine(line);
    }
  }

  public void removeLine(final int index) {
    segments.remove(index);
  }

  public void setLine(final int index, final Point2D p1, final Point2D p2) {
    segments.get(index).setLine(p1, p2);
  }

  public void setLine(final int index, final double x1, final double y1, final double x2, final double y2) {
    segments.get(index).setLine(x1, y1, x2, y2);
  }

  public void removeLines() {
    segments.clear();
  }

  @Override
  @JsonIgnore
  public Color getBorderColor() {
    return getColor();
  }

  @Override
  public void setBorderColor(final Color color) {
    setColor(color);
  }

  @Override
  @JsonIgnore
  public Color getFillColor() {
    return getBorderColor();
  }

  @Override
  public void setFillColor(final Color color) {
    setBorderColor(color);
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  public void setLayer(final Layer layer) {
    this.layer = layer;
  }

  @Override
  public EntityType getEntityType() {
    return EntityType.LAYER_LINE;
  }

  public Layer getLayer() {
    return layer;
  }
}
