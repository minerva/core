package lcsb.mapviewer.model;

public enum EntityType {
  GLYPH,
  LAYER,
  LAYER_IMAGE,
  LAYER_LINE,
  LAYER_OVAL,
  LAYER_RECTANGLE,
  LAYER_TEXT
}
