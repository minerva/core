package lcsb.mapviewer.model;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.GenericGenerator;

@Entity
@XmlRootElement
public class Stacktrace implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(generator = "uuid-hibernate-generator")
  @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
  private String id;

  @Column(columnDefinition = "TEXT")
  private String content;

  private Calendar createdAt = Calendar.getInstance();

  protected Stacktrace() {
  }

  public Stacktrace(final String content) {
    this.content = content;
  }

  public Stacktrace(final Exception e) {
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);
    e.printStackTrace(pw);
    this.content = sw.toString();
  }

  public String getId() {
    return id;
  }

  public String getContent() {
    return content;
  }

  public Calendar getCreatedAt() {
    return createdAt;
  }

}
