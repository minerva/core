package lcsb.mapviewer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lcsb.mapviewer.common.exception.NotImplementedException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@XmlRootElement
public class License implements Serializable, MinervaEntity {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(length = 255)
  private String name;

  @Column(columnDefinition = "TEXT")
  private String content;

  @Column(length = 1024)
  private String url;

  protected License() {
  }

  public License(final String name, final String content, final String url) {
    this.name = name;
    this.content = content;
    this.url = url;
  }

  public int getId() {
    return id;
  }

  @Override
  @JsonIgnore
  public long getEntityVersion() {
    return -1;
  }

  public String getName() {
    return name;
  }

  public String getContent() {
    return content;
  }

  public String getUrl() {
    return url;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
