package lcsb.mapviewer.model.overlay;

/**
 * Type of the {@link DataOverlayEntry}.
 * 
 * @author Piotr Gawron
 *
 */
public enum DataOverlayType {
  /**
   * Generic color schema (used for expression levels, etc).
   */
  GENERIC,

  /**
   * Customized color schema used for highlighting genetic variants.
   */
  GENETIC_VARIANT
}
