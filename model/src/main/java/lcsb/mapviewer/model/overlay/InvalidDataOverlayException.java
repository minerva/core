package lcsb.mapviewer.model.overlay;

/**
 * Exception that should be thrown when
 * {@link lcsb.mapviewer.model.overlay.DataOverlayEntry} is invalid.
 * 
 * @author Piotr Gawron
 * 
 */
public class InvalidDataOverlayException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor with message passed in the argument.
   * 
   * @param string
   *          message of this exception
   */
  public InvalidDataOverlayException(final String string) {
    super(string);
  }

  /**
   * Constructor with message and super exception passed in the argument.
   * 
   * @param string
   *          message of this exception
   * @param e
   *          super exception that caused this
   */
  public InvalidDataOverlayException(final String string, final Exception e) {
    super(string, e);
  }

  /**
   * Constructor with exception passed in the argument.
   * 
   * @param e
   *          super exception that caused this
   */
  public InvalidDataOverlayException(final Exception e) {
    super(e);
  }
}
