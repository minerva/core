package lcsb.mapviewer.model.overlay;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Generic color schema used for visualization any data type.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("GENERIC_DATA_OVERLAY")
public class GenericDataOverlayEntry extends DataOverlayEntry {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor.
   */
  public GenericDataOverlayEntry() {
  }

  /**
   * Default constructor that copies data from parameter.
   *
   * @param original original object from which data is copied
   */
  public GenericDataOverlayEntry(final GenericDataOverlayEntry original) {
    super(original);
  }

  @Override
  public GenericDataOverlayEntry copy() {
    if (this.getClass().equals(GenericDataOverlayEntry.class)) {
      return new GenericDataOverlayEntry(this);
    } else {
      throw new NotImplementedException("Copy mechanism of class " + this.getClass() + " is not implemented");
    }

  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
