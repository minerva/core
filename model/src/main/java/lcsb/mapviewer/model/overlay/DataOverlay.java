package lcsb.mapviewer.model.overlay;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.modelutils.serializer.id.MinervaEntityAsIdSerializer;
import lcsb.mapviewer.modelutils.serializer.model.user.UserAsLoginSerializer;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Version;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class DataOverlay implements MinervaEntity {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  public static final Comparator<? super DataOverlay> ORDER_COMPARATOR = new Comparator<DataOverlay>() {

    @Override
    public int compare(final DataOverlay o1, final DataOverlay o2) {
      if (Objects.equals(o1.isPublic(), o2.isPublic())) {
        return o1.getOrderIndex() - o2.getOrderIndex();
      } else {
        if (o1.isPublic()) {
          return 1;
        } else {
          return -1;
        }
      }
    }
  };

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @JsonProperty("idObject")
  private int id;

  @Column(length = 255, nullable = false)
  private String name;

  @ManyToOne(fetch = FetchType.LAZY)
  @JsonSerialize(using = MinervaEntityAsIdSerializer.class)
  private DataOverlayGroup group;

  @JsonProperty("publicOverlay")
  private boolean isPublic = false;

  /**
   * Data overlay type. It can be null in such case it should be obtained from
   * {@link #inputData}.
   */
  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  @JsonProperty("type")
  private DataOverlayType colorSchemaType = null;

  /**
   * Project to which this data overlay is assigned.
   */
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JsonIgnore
  private Project project;

  /**
   * Who created the layout.
   */
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JsonSerialize(using = UserAsLoginSerializer.class)
  private User creator;

  @JsonProperty("order")
  private int orderIndex = 0;

  /**
   * Short description used for this layout.
   */
  @Column(columnDefinition = "TEXT")
  private String description;

  @Enumerated(EnumType.STRING)
  private ReferenceGenomeType genomeType;

  private String genomeVersion;

  /**
   * Here we store input file.
   */
  @Cascade({CascadeType.SAVE_UPDATE})
  @OneToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "file_entry_id")
  @JsonIgnore
  private UploadedFileEntry inputData;

  @Cascade({CascadeType.SAVE_UPDATE})
  @OneToMany(mappedBy = "dataOverlay", orphanRemoval = true)
  @JsonIgnore
  private Set<DataOverlayEntry> entries = new HashSet<>();

  @Version
  @JsonIgnore
  private long entityVersion;

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public DataOverlayType getColorSchemaType() {
    return colorSchemaType;
  }

  public void setColorSchemaType(final DataOverlayType colorSchemaType) {
    this.colorSchemaType = colorSchemaType;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(final Project project) {
    this.project = project;
  }

  public User getCreator() {
    return creator;
  }

  public void setCreator(final User creator) {
    this.creator = creator;
  }

  public int getOrderIndex() {
    return orderIndex;
  }

  public void setOrderIndex(final int orderIndex) {
    this.orderIndex = orderIndex;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public UploadedFileEntry getInputData() {
    return inputData;
  }

  public void setInputData(final UploadedFileEntry inputData) {
    this.inputData = inputData;
  }

  @Override
  @JsonProperty("id")
  public int getId() {
    return id;
  }

  @JsonIgnore
  public boolean isPublic() {
    return isPublic;
  }

  public void setPublic(final boolean isPublic) {
    this.isPublic = isPublic;
  }

  public Set<DataOverlayEntry> getEntries() {
    return entries;
  }

  public void setEntries(final Set<DataOverlayEntry> entries) {
    this.entries = entries;
  }

  public void addEntry(final DataOverlayEntry dataOverlayEntry) {
    this.entries.add(dataOverlayEntry);
    dataOverlayEntry.setDataOverlay(this);
  }

  public void addEntries(final Collection<DataOverlayEntry> entries) {
    for (final DataOverlayEntry dataOverlayEntry : entries) {
      addEntry(dataOverlayEntry);
    }

  }

  public ReferenceGenomeType getGenomeType() {
    return genomeType;
  }

  public void setGenomeType(final ReferenceGenomeType genomeType) {
    this.genomeType = genomeType;
  }

  public String getGenomeVersion() {
    return genomeVersion;
  }

  public void setGenomeVersion(final String genomeVersion) {
    this.genomeVersion = genomeVersion;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }

  public DataOverlayGroup getGroup() {
    return group;
  }

  public void setGroup(final DataOverlayGroup group) {
    this.group = group;
  }
}
