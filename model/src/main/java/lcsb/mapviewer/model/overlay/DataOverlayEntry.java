package lcsb.mapviewer.model.overlay;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.species.Element;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Type;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import java.awt.Color;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "DATA_OVERLAY_TYPE", discriminatorType = DiscriminatorType.STRING)
public abstract class DataOverlayEntry implements MinervaEntity {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Name of the {@link Element Element}. If empty then this field will be
   * skipped.
   */
  @Column(nullable = false)
  private String name = "";

  /**
   * Name of the {@link Model} to which this schema should be limited. If null
   * then this field will be skipped.
   */
  private String modelName = null;

  /**
   * Original identifier of the {@link BioEntity} to change the color.
   */
  private String elementId = null;

  /**
   * Should the direction of highlighted reaction be reversed.
   */
  private Boolean reverseReaction = null;

  /**
   * Width of the line in the reaction.
   */
  private Double lineWidth = null;

  /**
   * In which compartments (identified by name) the element can occur.
   */
  @ElementCollection()
  @CollectionTable(name = "data_overlay_entry_compartment_table", joinColumns = @JoinColumn(name = "data_overlay_entry_id"))
  @Column(name = "compartment", length = 255)
  @Cascade({org.hibernate.annotations.CascadeType.ALL})
  @JsonIgnore
  private Set<String> compartments = new HashSet<>();

  /**
   * What types of element should be identified by this entry.
   */
  @ElementCollection()
  @CollectionTable(name = "data_overlay_entry_type_table", joinColumns = @JoinColumn(name = "data_overlay_entry_id"))
  @Column(name = "type", length = 255)
  @Cascade({org.hibernate.annotations.CascadeType.ALL})
  @JsonIgnore
  private final Set<Class<? extends BioEntity>> types = new HashSet<>();

  /**
   * Value (-1..1 range) that is assigned to filtered elements (it will be
   * transformed into color later on). Only one of the {@link #value} and
   * {@link #color} can be set.
   */
  private Double value = null;

  /**
   * Color that is assigned to filtered elements. Only one of the {@link #value}
   * and {@link #color} can be set.
   */
  @Type(type = "lcsb.mapviewer.persist.mapper.ColorMapper")
  private Color color = null;

  /**
   * Set of identifiers that filter the elements.
   */
  @Cascade({CascadeType.ALL})
  @ManyToMany()
  @JoinTable(name = "data_overlay_entry_miriam_table", joinColumns = {
      @JoinColumn(name = "data_overlay_entry_id", referencedColumnName = "id", nullable = false, updatable = false)},
      inverseJoinColumns = {
          @JoinColumn(name = "miriam_id", referencedColumnName = "id", nullable = true, updatable = true)})
  @JsonIgnore
  private final Set<MiriamData> miriamData = new HashSet<>();

  /**
   * Short description of the entry.
   */
  private String description;

  @ManyToOne
  private DataOverlay dataOverlay;

  @Version
  @JsonIgnore
  private long entityVersion;

  /**
   * Default constructor.
   */
  protected DataOverlayEntry() {
  }

  /**
   * Initializes object by copying data from the parameter.
   *
   * @param original original object used for initialization
   */
  protected DataOverlayEntry(final DataOverlayEntry original) {
    this.setName(original.getName());
    this.setElementId(original.getElementId());
    this.setReverseReaction(original.getReverseReaction());
    this.setLineWidth(original.getLineWidth());
    this.addCompartments(original.getCompartments());
    this.addTypes(original.getTypes());
    this.setValue(original.getValue());
    this.setColor(original.getColor());
    this.miriamData.addAll(original.getMiriamData());
    this.setDescription(original.getDescription());
  }

  /**
   * Adds class types to {@link #types} list.
   *
   * @param types2 list of classes to add
   */
  public void addTypes(final Collection<Class<? extends BioEntity>> types2) {
    for (final Class<? extends BioEntity> clazz : types2) {
      addType(clazz);
    }
  }

  /**
   * Adds compartment names to {@link #compartments} list.
   *
   * @param compartments2 elements to add
   */
  public void addCompartments(final String[] compartments2) {
    Collections.addAll(compartments, compartments2);
  }

  /**
   * Adds compartment names to {@link #compartments} list.
   *
   * @param compartments2 elements to add
   */
  public void addCompartments(final Collection<String> compartments2) {
    for (final String string : compartments2) {
      compartments.add(string);
    }
  }

  @Override
  public String toString() {
    final StringBuilder result = new StringBuilder();
    result.append("[");
    if (name != null) {
      result.append(name + ",");
    }
    if (compartments.size() > 0) {
      result.append("(");
      for (final String comp : compartments) {
        result.append(comp + ",");
      }
      result.append("),");
    }
    if (types.size() > 0) {
      result.append("(");
      for (final Class<?> clazz : types) {
        result.append(clazz.getSimpleName() + ",");
      }
      result.append("),");
    }
    if (value != null) {
      result.append(value + ",");
    }
    if (color != null) {
      result.append(color + ",");
    }
    result.append(miriamData + ",");

    return result.toString();
  }

  /**
   * @return the name
   * @see #name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   * @see #name
   */
  public void setName(final String name) {
    if (name == null) {
      this.name = "";
    } else {
      this.name = name.trim();
    }
  }

  /**
   * @return the compartments
   * @see #compartments
   */
  public Set<String> getCompartments() {
    return compartments;
  }

  /**
   * @param compartments the compartments to set
   * @see #compartments
   */
  public void setCompartments(final Set<String> compartments) {
    this.compartments = compartments;
  }

  /**
   * @return the types
   * @see #types
   */
  public Set<Class<? extends BioEntity>> getTypes() {
    return types;
  }

  /**
   * @return the value
   * @see #value
   */
  public Double getValue() {
    return value;
  }

  /**
   * @param value the value to set
   * @see #value
   */
  public void setValue(final Double value) {
    this.value = value;
  }

  /**
   * @return the color
   * @see #color
   */
  public Color getColor() {
    return color;
  }

  /**
   * @param color the color to set
   * @see #color
   */
  public void setColor(final Color color) {
    this.color = color;
  }

  /**
   * Adds identifier to {@link #miriamData} set.
   */
  public void addMiriamData(final MiriamData md) {
    miriamData.add(md);
  }

  /**
   * @return the lineWidth
   * @see #lineWidth
   */
  public Double getLineWidth() {
    return lineWidth;
  }

  /**
   * @param lineWidth the lineWidth to set
   * @see #lineWidth
   */
  public void setLineWidth(final Double lineWidth) {
    this.lineWidth = lineWidth;
  }

  /**
   * @return the reverseReaction
   * @see #reverseReaction
   */
  public Boolean getReverseReaction() {
    return reverseReaction;
  }

  /**
   * @param reverseReaction the reverseReaction to set
   * @see #reverseReaction
   */
  public void setReverseReaction(final Boolean reverseReaction) {
    this.reverseReaction = reverseReaction;
  }

  /**
   * Adds compartment name to {@link #compartments}.
   *
   * @param name compartment name
   */
  public void addCompartment(final String name) {
    compartments.add(name);
  }

  /**
   * Adds class type to {@link #types} list.
   *
   * @param clazz class to add
   */
  public void addType(final Class<? extends BioEntity> clazz) {
    this.types.add(clazz);
  }

  /**
   * @return the description
   * @see #description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description the description to set
   * @see #description
   */
  public void setDescription(final String description) {
    this.description = description;
  }

  /**
   * Creates a copy of this object.
   *
   * @return copy of the object
   */
  public abstract DataOverlayEntry copy();

  public String getModelName() {
    return modelName;
  }

  public void setModelName(final String modelName) {
    this.modelName = modelName;
  }

  public String getElementId() {
    return elementId;
  }

  public void setElementId(final String elementId) {
    this.elementId = elementId;
  }

  public Set<MiriamData> getMiriamData() {
    return miriamData;
  }

  public int getId() {
    return id;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  public void setDataOverlay(final DataOverlay dataOverlay) {
    this.dataOverlay = dataOverlay;
  }
}
