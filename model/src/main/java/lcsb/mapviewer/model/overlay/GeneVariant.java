package lcsb.mapviewer.model.overlay;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lcsb.mapviewer.common.exception.NotImplementedException;

/**
 * Class describing single gene variation.
 * 
 * @author Piotr Gawron
 *
 */
@Entity
public class GeneVariant implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default class logger.
   */
  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  /**
   * Variation position in the genome.
   */
  @Column(nullable = false)
  private Long position;

  /**
   * Original DNA (from the reference genome).
   */
  @Column(nullable = false)
  private String originalDna;

  /**
   * Alternative DNA (suggested by the variant).
   */
  @Column(nullable = false)
  private String modifiedDna;

  /**
   * Contig where variant was observed.
   */
  @Column(nullable = false)
  private String contig;

  /**
   * Allele frequency of the variant.
   */
  private Double allelFrequency;

  /**
   * Amino acid change for corresponding protein.
   */
  private String aminoAcidChange;

  /**
   * Gene variant id.
   */
  private String variantIdentifier;

  @JsonIgnore
  @ManyToOne
  private GeneVariantDataOverlayEntry dataOverlayEntry;

  /**
   * Constructor that creates copy of the {@link GeneVariant}.
   * 
   * @param original
   *          original {@link GeneVariant}
   */
  protected GeneVariant(final GeneVariant original) {
    this.setPosition(original.getPosition());
    this.setOriginalDna(original.getOriginalDna());
    this.setModifiedDna(original.getModifiedDna());
    this.setContig(original.getContig());
    this.setAllelFrequency(original.getAllelFrequency());
    this.setVariantIdentifier(original.getVariantIdentifier());
    this.setAminoAcidChange(original.getAminoAcidChange());
  }

  /**
   * Default constructor.
   */
  public GeneVariant() {
  }

  /**
   * @return the position
   * @see #position
   */
  public Long getPosition() {
    return position;
  }

  /**
   * @param position
   *          the position to set
   * @see #position
   */
  public void setPosition(final Long position) {
    this.position = position;
  }

  /**
   * Sets {@link #position}.
   *
   * @param position
   *          new position value
   */
  public void setPosition(final int position) {
    this.position = (long) position;
  }

  /**
   * @return the originalDna
   * @see #originalDna
   */
  public String getOriginalDna() {
    return originalDna;
  }

  /**
   * @param originalDna
   *          the originalDna to set
   * @see #originalDna
   */
  public void setOriginalDna(final String originalDna) {
    this.originalDna = originalDna;
  }

  /**
   * @return the modifiedDna
   * @see #modifiedDna
   */
  public String getModifiedDna() {
    return modifiedDna;
  }

  /**
   * @param modifiedDna
   *          the modifiedDna to set
   * @see #modifiedDna
   */
  public void setModifiedDna(final String modifiedDna) {
    this.modifiedDna = modifiedDna;
  }

  /**
   * Creates copy of the object.
   *
   * @return copy of the object
   */
  public GeneVariant copy() {
    if (this.getClass().equals(GeneVariant.class)) {
      return new GeneVariant(this);
    } else {
      throw new NotImplementedException("Copy method not implemented for class: " + this.getClass());
    }
  }

  /**
   * @return the contig
   * @see #contig
   */
  public String getContig() {
    return contig;
  }

  /**
   * @param contig
   *          the contig to set
   * @see #contig
   */
  public void setContig(final String contig) {
    this.contig = contig;
  }

  /**
   * @return the allelFrequency
   * @see #allelFrequency
   */
  public Double getAllelFrequency() {
    return allelFrequency;
  }

  /**
   * @param allelFrequency
   *          the allelFrequency to set
   * @see #allelFrequency
   */
  public void setAllelFrequency(final Double allelFrequency) {
    this.allelFrequency = allelFrequency;
  }

  /**
   * @return the variantIdentifier
   * @see #variantIdentifier
   */
  public String getVariantIdentifier() {
    return variantIdentifier;
  }

  /**
   * @param variantIdentifier
   *          the variantIdentifier to set
   * @see #variantIdentifier
   */
  public void setVariantIdentifier(final String variantIdentifier) {
    this.variantIdentifier = variantIdentifier;
  }

  public String getAminoAcidChange() {
    return aminoAcidChange;
  }

  public void setAminoAcidChange(final String aminoAcidChange) {
    this.aminoAcidChange = aminoAcidChange;
  }

  public GeneVariantDataOverlayEntry getDataOverlayEntry() {
    return dataOverlayEntry;
  }

  public void setDataOverlayEntry(final GeneVariantDataOverlayEntry dataOverlayEntry) {
    this.dataOverlayEntry = dataOverlayEntry;
  }

}
