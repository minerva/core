package lcsb.mapviewer.model.overlay;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.user.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

@Entity
public class DataOverlayGroup implements MinervaEntity {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(length = 255, nullable = false)
  private String name;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JsonIgnore
  private Project project;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JsonIgnore
  private User owner;

  @JsonProperty("order")
  private int orderIndex = 0;

  @Version
  @JsonIgnore
  private long entityVersion;

  @Override
  public int getId() {
    return id;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }

  public void setId(final int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  @JsonIgnore
  public Project getProject() {
    return project;
  }

  public void setProject(final Project project) {
    this.project = project;
  }

  /**
   * Who created the layout.
   */
  public User getOwner() {
    return owner;
  }

  public void setOwner(final User owner) {
    this.owner = owner;
  }

  public int getOrderIndex() {
    return orderIndex;
  }

  public void setOrderIndex(final int orderIndex) {
    this.orderIndex = orderIndex;
  }
}
