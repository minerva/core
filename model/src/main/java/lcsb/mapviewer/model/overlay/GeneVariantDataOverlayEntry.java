package lcsb.mapviewer.model.overlay;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Information about coloring element according to genetic variation
 * information.
 *
 * @author Piotr Gawron
 */
@Entity
@DiscriminatorValue("GENE_VARIANT_DATA_OVERLAY")
public class GeneVariantDataOverlayEntry extends DataOverlayEntry {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  @Cascade({CascadeType.ALL})
  @OneToMany(mappedBy = "dataOverlayEntry", orphanRemoval = true)
  private final Set<GeneVariant> geneVariants = new HashSet<>();

  public GeneVariantDataOverlayEntry() {
  }

  /**
   * Constructor that creates copy of the object.
   *
   * @param schema original object from which data will be copied
   */
  public GeneVariantDataOverlayEntry(final GeneVariantDataOverlayEntry schema) {
    super(schema);
    this.addGeneVariants(schema.getGeneVariants());
  }

  /**
   * Adds {@link GeneVariant gene variations} to {@link #geneVariants}.
   *
   * @param geneVariants collection of {@link GeneVariant} objects to add
   */
  public void addGeneVariants(final Collection<GeneVariant> geneVariants) {
    for (final GeneVariant geneVariation : geneVariants) {
      addGeneVariant(geneVariation.copy());
    }
  }

  @Override
  public GeneVariantDataOverlayEntry copy() {
    if (this.getClass().equals(GeneVariantDataOverlayEntry.class)) {
      return new GeneVariantDataOverlayEntry(this);
    } else {
      throw new NotImplementedException("Copy not implemented for class: " + this.getClass());
    }
  }

  /**
   * @return the geneVariations
   * @see #geneVariants
   */
  public Set<GeneVariant> getGeneVariants() {
    return geneVariants;
  }

  /**
   * Adds {@link GeneVariant} to list gene variations.
   *
   * @param gv object to add
   */
  public void addGeneVariant(final GeneVariant gv) {
    this.geneVariants.add(gv);
    gv.setDataOverlayEntry(this);
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }
}
