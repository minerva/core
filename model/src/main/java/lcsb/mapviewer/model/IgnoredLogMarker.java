package lcsb.mapviewer.model;

import org.apache.logging.log4j.Marker;

import lcsb.mapviewer.common.exception.NotImplementedException;

public class IgnoredLogMarker implements Marker {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Override
  public Marker addParents(final Marker... markers) {
    throw new NotImplementedException();
  }

  @Override
  public String getName() {
    throw new NotImplementedException();
  }

  @Override
  public Marker[] getParents() {
    throw new NotImplementedException();
  }

  @Override
  public boolean hasParents() {
    throw new NotImplementedException();
  }

  @Override
  public boolean isInstanceOf(final Marker m) {
    throw new NotImplementedException();
  }

  @Override
  public boolean isInstanceOf(final String name) {
    throw new NotImplementedException();
  }

  @Override
  public boolean remove(final Marker marker) {
    throw new NotImplementedException();
  }

  @Override
  public Marker setParents(final Marker... markers) {
    throw new NotImplementedException();
  }

}
