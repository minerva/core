package lcsb.mapviewer.model.job;

public enum MinervaJobPriority {
  HIGH(1),
  MEDIUM(5),
  LOW(10),
  LOWEST(20);

  private int value;

  private MinervaJobPriority(final int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
