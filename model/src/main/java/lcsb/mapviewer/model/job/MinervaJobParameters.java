package lcsb.mapviewer.model.job;

import java.util.HashMap;
import java.util.Map;

import lcsb.mapviewer.common.exception.InvalidStateException;

public abstract class MinervaJobParameters {

  public MinervaJobParameters(final Map<String, Object> jobParameters) throws InvalidMinervaJobParameters {
    assignData(jobParameters);
  }

  public MinervaJobParameters() {
    try {
      assignData(new HashMap<>());
    } catch (InvalidMinervaJobParameters e) {
      throw new InvalidStateException("WTF", e);
    }
  }

  protected abstract void assignData(Map<String, Object> jobParameters) throws InvalidMinervaJobParameters;

  protected abstract Map<String, Object> getJobParameters();
}
