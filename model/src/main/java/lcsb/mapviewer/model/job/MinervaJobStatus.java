package lcsb.mapviewer.model.job;

public enum MinervaJobStatus {
  PENDING,
  RUNNING,
  FAILED,
  DONE,
  INTERRUPTED
}
