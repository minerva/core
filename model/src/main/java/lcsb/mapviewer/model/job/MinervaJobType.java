package lcsb.mapviewer.model.job;

public enum MinervaJobType {
  REFRESH_CACHE,
  REFRESH_MIRIAM_INFO,

  REFRESH_DRUG_INFO,

  REFRESH_CHEMICAL_INFO,
  REFRESH_CHEMICAL_SUGGESTED_QUERY_LIST,

  REFRESH_DRUG_BANK_SUGGESTED_QUERY_LIST,
  REFRESH_CHEMBL_SUGGESTED_QUERY_LIST,

  DELETE_PROJECT,
  CREATE_PROJECT(true),
  CREATE_PROJECT_FAILURE,

  DELETE_BACKGROUND,
  REVIVE_BACKGROUNDS,

  ANNOTATE_PROJECT,
  ;

  private final boolean allowDuplicates;

  MinervaJobType(final boolean allowDuplicates) {
    this.allowDuplicates = allowDuplicates;
  }

  MinervaJobType() {
    this(false);
  }

  public boolean allowDuplicates() {
    return this.allowDuplicates;
  }
}
