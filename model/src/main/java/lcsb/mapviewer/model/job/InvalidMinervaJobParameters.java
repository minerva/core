package lcsb.mapviewer.model.job;

public class InvalidMinervaJobParameters extends Exception {

  public InvalidMinervaJobParameters(final Exception e) {
    super(e);
  }

  private static final long serialVersionUID = 1L;

}
