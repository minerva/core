package lcsb.mapviewer.model.job;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.EntityType;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.modelutils.serializer.JpaMapConverterJson;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import java.util.Calendar;
import java.util.Collections;
import java.util.Map;

@Entity
public class MinervaJob implements MinervaEntity {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @JsonProperty(access = Access.READ_ONLY)
  private int id;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private MinervaJobType jobType;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private MinervaJobStatus jobStatus = MinervaJobStatus.PENDING;

  @Convert(converter = JpaMapConverterJson.class)
  @JsonIgnore
  private Map<String, Object> jobParameters;

  private Calendar jobStarted;

  private Calendar jobFinished;

  private Double progress;

  private Class<?> externalObjectClass;

  private Integer externalObjectId;

  @JsonIgnore
  private String logs;

  private int priority = MinervaJobPriority.LOWEST.getValue();

  @Version
  @JsonIgnore
  private long entityVersion;

  protected MinervaJob() {

  }

  public MinervaJob(final MinervaJobType jobType, final MinervaJobPriority priority, final MinervaJobParameters jobParameters) {
    this.jobType = jobType;
    this.priority = priority.getValue();
    this.jobParameters = jobParameters.getJobParameters();
  }

  public MinervaJobType getJobType() {
    return jobType;
  }

  @Override
  public int getId() {
    return id;
  }

  public Map<String, Object> getJobParameters() {
    return Collections.unmodifiableMap(jobParameters);
  }

  public MinervaJobStatus getJobStatus() {
    return jobStatus;
  }

  public void setJobStatus(final MinervaJobStatus jobStatus) {
    if (this.jobStatus != jobStatus) {
      if (jobStatus == MinervaJobStatus.RUNNING) {
        this.jobStarted = Calendar.getInstance();
      }
      if (jobStatus == MinervaJobStatus.FAILED
          || jobStatus == MinervaJobStatus.DONE
          || jobStatus == MinervaJobStatus.INTERRUPTED) {
        this.jobFinished = Calendar.getInstance();
      }
    }
    this.jobStatus = jobStatus;
  }

  public Calendar getJobStarted() {
    return jobStarted;
  }

  public Calendar getJobFinished() {
    return jobFinished;
  }

  public String getLogs() {
    return logs;
  }

  public void setLogs(final String logs) {
    this.logs = logs;
  }

  public int getPriority() {
    return priority;
  }

  @Override
  public long getEntityVersion() {
    return entityVersion;
  }

  @Override
  public EntityType getEntityType() {
    throw new NotImplementedException();
  }

  public Double getProgress() {
    return progress;
  }

  public void setProgress(final Double progress) {
    this.progress = progress;
  }

  public Class<?> getExternalObjectClass() {
    return externalObjectClass;
  }

  public void setExternalObjectClass(final Class<?> externalObjectClass) {
    this.externalObjectClass = externalObjectClass;
  }

  public Integer getExternalObjectId() {
    return externalObjectId;
  }

  public void setExternalObjectId(final Integer externalObjectId) {
    this.externalObjectId = externalObjectId;
  }

  public void setExternalObject(final MinervaEntity entity) {
    if (entity != null) {
      this.externalObjectClass = entity.getClass();
      this.externalObjectId = entity.getId();
    }
  }
}
