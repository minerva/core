package lcsb.mapviewer.model.job;

public interface MinervaJobExecutor {

  void execute(MinervaJob job) throws Exception;
}
