package lcsb.mapviewer.modelutils.map;

import java.util.Iterator;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import lcsb.mapviewer.common.exception.InvalidArgumentException;

public class ValidationUtil {

  public static <T> void validate(final T object) throws InvalidArgumentException {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator();
    Iterator<ConstraintViolation<T>> violations = validator.validate(object).iterator();
    if (violations.hasNext()) {
      ConstraintViolation<T> violation = violations.next();
      String error = violation.getPropertyPath() + ": " + violation.getMessage();
      throw new InvalidArgumentException(error);
    }
  }

}
