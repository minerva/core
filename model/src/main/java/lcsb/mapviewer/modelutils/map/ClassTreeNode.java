package lcsb.mapviewer.modelutils.map;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javassist.Modifier;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.reaction.Reaction;

/**
 * This class is used to represent inheritance hierarchy of classes.
 * 
 * @author Piotr Gawron
 * 
 */
public class ClassTreeNode {

  /**
   * Original class.
   */
  private Class<?> clazz;

  /**
   * Children classes.
   */
  private List<ClassTreeNode> children;

  /**
   * Parent node.
   */
  private ClassTreeNode parent;

  /**
   * Name used to present this class.
   */
  private String commonName;

  /**
   * Additional data in the node.
   */
  private Object data;

  /**
   * Default constructor.
   * 
   * @param class1
   *          class that should be put in this node
   */
  public ClassTreeNode(final Class<?> class1) {
    this.clazz = class1;
    this.children = new ArrayList<>();
    this.commonName = class1.getSimpleName();
    if (!Modifier.isAbstract(clazz.getModifiers())) {
      try {
        if (Reaction.class.isAssignableFrom(clazz)) {
          this.commonName = ((BioEntity) (clazz.getDeclaredConstructor(String.class).newInstance("id"))).getStringType();
        } else {
          this.commonName = ((BioEntity) (clazz.getDeclaredConstructor(String.class).newInstance("id")))
              .getStringType();
        }
      } catch (final InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
          | NoSuchMethodException | SecurityException e) {
      }
    }

    this.data = null;
  }

  /**
   * @return the clazz
   * @see #clazz
   */
  public Class<?> getClazz() {
    return clazz;
  }

  /**
   * @param clazz
   *          the clazz to set
   * @see #clazz
   */
  public void setClazz(final Class<?> clazz) {
    this.clazz = clazz;
  }

  /**
   * @return the children
   * @see #children
   */
  public List<ClassTreeNode> getChildren() {
    return children;
  }

  /**
   * @param children
   *          the children to set
   * @see #children
   */
  public void setChildren(final List<ClassTreeNode> children) {
    this.children = children;
  }

  /**
   * @return the commonName
   * @see #commonName
   */
  public String getCommonName() {
    return commonName;
  }

  /**
   * @param commonName
   *          the commonName to set
   * @see #commonName
   */
  public void setCommonName(final String commonName) {
    this.commonName = commonName;
  }

  /**
   * @return the data
   * @see #data
   */
  public Object getData() {
    return data;
  }

  /**
   * @param data
   *          the data to set
   * @see #data
   */
  public void setData(final Object data) {
    this.data = data;
  }

  /**
   * @return the parent
   * @see #parent
   */
  public ClassTreeNode getParent() {
    return parent;
  }

  /**
   * @param parent
   *          the parent to set
   * @see #parent
   */
  public void setParent(final ClassTreeNode parent) {
    this.parent = parent;
  }

}
