package lcsb.mapviewer.modelutils.map;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.core.LogEvent;

import lcsb.mapviewer.model.IgnoredLogMarker;
import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.ProjectLogEntry;
import lcsb.mapviewer.model.ProjectLogEntryType;

public class LogFormatter {
  private static final String DATA_UNAVAILABLE = "-";

  public List<String> createFormattedWarnings(final Collection<LogEvent> logs) {
    return createFormattedWarnings(logs, "[%I]\t%C");
  }

  /**
   * 
   * @param format
   *          string with formatting of the message. Tokens used for inserting
   *          data:
   *          <ul>
   *          <li>%T - type</li>
   *          <li>%M - map name</li>
   *          <li>%S - warning source</li>
   *          <li>%I - object identifier in format: *
   *          objectClass:objectIdentifier</li>
   *          <li>%C - text message</li>
   *          <li>%i - object identifier</li>
   *          <li>%c - object class</li>
   *          </ul>
   * @return
   */
  public List<String> createFormattedWarnings(final Collection<LogEvent> logs, final String format) {
    List<String> result = new ArrayList<>();
    for (final LogEvent logEvent : logs) {
      String warning = createFormattedString(logEvent, format);
      if (warning != null) {
        result.add(warning);
      }
    }
    return result;

  }

  /**
   * 
   * @param format
   *          string with formatting of the message. Tokens used for inserting
   *          data:
   *          <ul>
   *          <li>%T - type</li>
   *          <li>%M - map name</li>
   *          <li>%S - warning source</li>
   *          <li>%I - object identifier in format: *
   *          objectClass:objectIdentifier</li>
   *          <li>%C - text message</li>
   *          <li>%i - object identifier</li>
   *          <li>%c - object class</li>
   *          </ul>
   * 
   * @return
   */
  String createFormattedString(final LogEvent event, final String format) {
    if (event.getMarker() instanceof IgnoredLogMarker) {
      return null;
    }
    String result = format;
    String type = ProjectLogEntryType.OTHER.name();
    String source = DATA_UNAVAILABLE;
    String mapName = DATA_UNAVAILABLE;
    String objectClass = DATA_UNAVAILABLE;
    String objectIdentifier = DATA_UNAVAILABLE;
    if (event.getMarker() != null) {
      if (event.getMarker() instanceof LogMarker) {
        ProjectLogEntry entry = ((LogMarker) event.getMarker()).getEntry();
        if (entry.getSource() != null && entry.getSource().trim().isEmpty()) {
          source = entry.getSource();
        }
        if (entry.getType() != null) {
          type = entry.getType().name();
        }
        if (entry.getMapName() != null) {
          mapName = entry.getMapName();
        }
        if (entry.getObjectClass() != null) {
          objectClass = entry.getObjectClass();
        }
        if (entry.getObjectIdentifier() != null) {
          objectIdentifier = entry.getObjectIdentifier();
        }
      }
    }
    result = result.replace("%T", type);
    result = result.replace("%S", source);
    result = result.replace("%M", mapName);
    result = result.replace("%I", objectClass + ":" + objectIdentifier);
    result = result.replace("%i", objectIdentifier);
    result = result.replace("%c", objectClass);
    result = result.replace("%C", event.getMessage().getFormattedMessage());
    return result;
  }
}
