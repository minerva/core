package lcsb.mapviewer.modelutils.map;

import java.util.Comparator;

/**
 * Class used to compare classes using class names (SimpleName).
 * 
 * @author Piotr Gawron
 * 
 */
public class ClassNameComparator implements Comparator<Class<?>> {

  @Override
  public int compare(final Class<?> o1, final Class<?> o2) {
    return o1.getSimpleName().compareTo(o2.getSimpleName());
  }

}
