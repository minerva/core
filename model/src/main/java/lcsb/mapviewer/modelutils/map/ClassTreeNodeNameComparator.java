package lcsb.mapviewer.modelutils.map;

import java.util.Comparator;

import lcsb.mapviewer.common.comparator.StringComparator;

/**
 * Name {@link Comparator} for {@link ClassTreeNode} class.
 * 
 * @author Piotr Gawron
 *
 */
public class ClassTreeNodeNameComparator implements Comparator<ClassTreeNode> {

  /**
   * String comparator used for comparison (it's null safe).
   */
  private StringComparator stringComparator = new StringComparator();

  @Override
  public int compare(final ClassTreeNode o1, final ClassTreeNode o2) {
    return stringComparator.compare(o1.getCommonName(), o2.getCommonName());
  }

}
