package lcsb.mapviewer.modelutils.serializer.model.user.annotator;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;

import lcsb.mapviewer.common.exception.DeserializationException;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.user.AnnotatorParamDefinition;
import lcsb.mapviewer.model.user.annotator.AnnotatorConfigParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorInputParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorParameter;
import lcsb.mapviewer.model.user.annotator.BioEntityField;

public class AnnotatorParameterDeserializer extends StdDeserializer<AnnotatorParameter> {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  protected AnnotatorParameterDeserializer() {
    super(AnnotatorParameter.class);
  }

  @Override
  public AnnotatorParameter deserialize(final JsonParser parser, final DeserializationContext ctxt) throws IOException, JsonProcessingException {

    ObjectMapper mapper = (ObjectMapper) parser.getCodec();
    ObjectNode rootNode = mapper.readTree(parser);

    switch (rootNode.get("type").asText()) {
      case "CONFIG":
        return deserializeConfigParameter(rootNode);
      case "INPUT":
        return deserializeInputParameter(rootNode);
      case "OUTPUT":
        return deserializeOutputParameter(rootNode);
      default:
        throw new DeserializationException("Unknown type: " + rootNode.get("type").asText());
    }
  }

  private AnnotatorParameter deserializeOutputParameter(final ObjectNode rootNode) {
    BioEntityField field = getFieldType(rootNode);
    MiriamType type = getAnnotationType(rootNode);
    AnnotatorOutputParameter result;
    if (field != null) {
      result = new AnnotatorOutputParameter(field);
    } else {
      result = new AnnotatorOutputParameter(type);
    }
    result.setOrderPosition(rootNode.get("order").asInt());
    return result;
  }

  private AnnotatorParameter deserializeInputParameter(final ObjectNode rootNode) {
    BioEntityField field = getFieldType(rootNode);
    MiriamType type = getAnnotationType(rootNode);
    AnnotatorInputParameter result = new AnnotatorInputParameter(field, type);
    result.setOrderPosition(rootNode.get("order").asInt());
    return result;
  }

  private MiriamType getAnnotationType(final ObjectNode rootNode) {
    MiriamType type = null;
    if (rootNode.hasNonNull("annotation_type")) {
      type = MiriamType.valueOf(rootNode.get("annotation_type").asText());
    }
    return type;
  }

  private BioEntityField getFieldType(final ObjectNode rootNode) {
    BioEntityField field = null;
    if (rootNode.hasNonNull("field")) {
      field = BioEntityField.valueOf(rootNode.get("field").asText());
    }
    return field;
  }

  private AnnotatorConfigParameter deserializeConfigParameter(final ObjectNode rootNode) {
    AnnotatorParamDefinition type = null;
    if (rootNode.hasNonNull("name")) {
      type = AnnotatorParamDefinition.valueOf(rootNode.get("name").asText());
    }

    int order = rootNode.get("order").asInt(0);
    String parameterValue = rootNode.get("value").asText("");
    AnnotatorConfigParameter result = new AnnotatorConfigParameter(type, parameterValue);
    result.setOrderPosition(order);
    return result;
  }
}
