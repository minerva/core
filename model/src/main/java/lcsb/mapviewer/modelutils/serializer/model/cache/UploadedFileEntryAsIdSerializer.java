package lcsb.mapviewer.modelutils.serializer.model.cache;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.cache.UploadedFileEntry;

public class UploadedFileEntryAsIdSerializer extends JsonSerializer<UploadedFileEntry> {

  @Override
  public void serialize(final UploadedFileEntry fileEntry, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeNumber(fileEntry.getId());
  }
}
