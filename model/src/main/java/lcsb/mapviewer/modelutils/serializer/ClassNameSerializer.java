package lcsb.mapviewer.modelutils.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class ClassNameSerializer extends JsonSerializer<Class<?>> {

  @Override
  public void serialize(final Class<?> entry, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeString(entry.getName());
  }
}