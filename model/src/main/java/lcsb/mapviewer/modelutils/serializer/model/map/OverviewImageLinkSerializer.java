package lcsb.mapviewer.modelutils.serializer.model.map;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.map.OverviewImageLink;

public class OverviewImageLinkSerializer extends JsonSerializer<OverviewImageLink> {

  @Override
  public void serialize(final OverviewImageLink entry, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeNumberField("idObject", entry.getId());
    gen.writeObjectField("polygon", entry.getPolygonCoordinates());
    gen.writeNumberField("imageLinkId", entry.getLinkedOverviewImage().getId());
    gen.writeStringField("type", entry.getClass().getSimpleName());
    gen.writeEndObject();
  }
}