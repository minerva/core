package lcsb.mapviewer.modelutils.serializer.model.map;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.map.OverviewImage;

public class OverviewImageSerializer extends JsonSerializer<OverviewImage> {

  @Override
  public void serialize(final OverviewImage entry, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeNumberField("idObject", entry.getId());
    gen.writeStringField("filename", entry.getProject().getDirectory() + "/" + entry.getFilename());
    gen.writeNumberField("width", entry.getWidth());
    gen.writeNumberField("height", entry.getHeight());
    gen.writeObjectField("links", entry.getLinks());
    gen.writeEndObject();
  }
}