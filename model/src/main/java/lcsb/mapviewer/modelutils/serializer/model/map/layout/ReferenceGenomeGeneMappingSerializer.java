package lcsb.mapviewer.modelutils.serializer.model.map.layout;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.map.layout.ReferenceGenomeGeneMapping;

public class ReferenceGenomeGeneMappingSerializer extends JsonSerializer<ReferenceGenomeGeneMapping> {
  @Override
  public void serialize(final ReferenceGenomeGeneMapping mapping, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeNumberField("downloadProgress", mapping.getDownloadProgress());
    String localUrl = mapping.getSourceUrl();
    if (mapping.getFile() != null && mapping.getFile().getDownloadProgress() >= 100) {
      localUrl = "../" + mapping.getFile().getLocalPath();
    }
    gen.writeStringField("localUrl", localUrl);
    gen.writeStringField("sourceUrl", mapping.getSourceUrl());
    gen.writeStringField("name", mapping.getName());
    gen.writeNumberField("idObject", mapping.getId());
    gen.writeEndObject();

  }
}