package lcsb.mapviewer.modelutils.serializer.model.map.reaction;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.map.reaction.ReactionNode;

public class ReactionNodeSerializer extends JsonSerializer<ReactionNode> {

  @Override
  public void serialize(final ReactionNode node, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeNumberField("aliasId", node.getElement().getId());
    gen.writeObjectField("stoichiometry", node.getStoichiometry());
    gen.writeStringField("type", node.getClass().getSimpleName());
    gen.writeEndObject();
  }

}