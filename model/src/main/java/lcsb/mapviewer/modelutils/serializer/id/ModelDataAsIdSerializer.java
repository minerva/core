package lcsb.mapviewer.modelutils.serializer.id;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.map.model.ModelData;

public class ModelDataAsIdSerializer extends JsonSerializer<ModelData> {

  @Override
  public void serialize(final ModelData model, final JsonGenerator gen, final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeNumberField("id", model.getId());
    gen.writeEndObject();
  }
}