package lcsb.mapviewer.modelutils.serializer.model.user;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lcsb.mapviewer.model.user.UserAnnotationSchema;
import lcsb.mapviewer.model.user.UserClassAnnotators;
import lcsb.mapviewer.model.user.UserGuiPreference;

import java.io.IOException;

public class UserAnnotationSchemaSerializer extends JsonSerializer<UserAnnotationSchema> {

  @Override
  public void serialize(final UserAnnotationSchema schema, final JsonGenerator gen, final SerializerProvider serializers) throws IOException {
    gen.writeStartObject();

    gen.writeObjectFieldStart("project-upload");
    gen.writeBooleanField("validate-miriam", schema.getValidateMiriamTypes());
    gen.writeBooleanField("annotate-model", schema.getAnnotateModel());
    gen.writeBooleanField("auto-resize", schema.getAutoResizeMap());
    gen.writeBooleanField("semantic-zooming-contains-multiple-overlays", schema.getSemanticZoomContainsMultipleOverlays());
    gen.writeBooleanField("sbgn", schema.getSbgnFormat());
    gen.writeEndObject();

    gen.writeObjectFieldStart("element-annotators");
    for (final UserClassAnnotators userClassAnnotators : schema.getClassAnnotators()) {
      gen.writeObjectField(userClassAnnotators.getClassName(), userClassAnnotators.getAnnotators());
    }
    gen.writeEndObject();

    gen.writeObjectFieldStart("gui-preferences");
    for (final UserGuiPreference userGuiPreference : schema.getGuiPreferences()) {
      gen.writeObjectField(userGuiPreference.getKey(), userGuiPreference.getValue());
    }
    gen.writeEndObject();

    gen.writeEndObject();
  }
}