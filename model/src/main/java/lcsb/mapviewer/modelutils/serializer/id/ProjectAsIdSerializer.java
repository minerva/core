package lcsb.mapviewer.modelutils.serializer.id;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.Project;

public class ProjectAsIdSerializer extends JsonSerializer<Project> {

  @Override
  public void serialize(final Project project, final JsonGenerator gen, final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeStringField("projectId", project.getProjectId());
    gen.writeEndObject();
  }
}