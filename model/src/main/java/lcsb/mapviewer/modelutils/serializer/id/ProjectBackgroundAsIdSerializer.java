package lcsb.mapviewer.modelutils.serializer.id;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.map.layout.ProjectBackground;

public class ProjectBackgroundAsIdSerializer extends JsonSerializer<ProjectBackground> {

  @Override
  public void serialize(final ProjectBackground background, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeNumberField("id", background.getId());
    gen.writeEndObject();
  }
}