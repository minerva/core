package lcsb.mapviewer.modelutils.serializer.model.map;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;

public class MinifiedBioEntitySerializer extends JsonSerializer<BioEntity> {

  @Override
  public void serialize(final BioEntity object, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeNumberField("id", object.getId());
    gen.writeNumberField("modelId", object.getModelData().getId());
    if (object instanceof Element) {
      gen.writeObjectField("type", ElementIdentifierType.ALIAS);
    } else if (object instanceof Reaction) {
      gen.writeObjectField("type", ElementIdentifierType.REACTION);
    } else {
      throw new InvalidStateException("Unknown type of result: " + object.getClass());
    }
    gen.writeEndObject();
  }
}