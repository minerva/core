package lcsb.mapviewer.modelutils.serializer;

import java.awt.geom.Line2D;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class Line2DDeserializer extends JsonDeserializer<Line2D> {

  @Override
  public Line2D deserialize(final JsonParser parser, final DeserializationContext ctxt) throws IOException, JsonProcessingException {

    ObjectMapper mapper = (ObjectMapper) parser.getCodec();
    ObjectNode rootNode = mapper.readTree(parser);

    double x1 = rootNode.get("x1").asDouble();
    double y1 = rootNode.get("y1").asDouble();
    double x2 = rootNode.get("x2").asDouble();
    double y2 = rootNode.get("y2").asDouble();
    return new Line2D.Double(x1, y1, x2, y2);
  }
}
