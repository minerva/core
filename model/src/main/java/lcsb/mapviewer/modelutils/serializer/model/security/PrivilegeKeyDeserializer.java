package lcsb.mapviewer.modelutils.serializer.model.security;

import java.io.IOException;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;

import lcsb.mapviewer.common.exception.DeserializationException;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;

public class PrivilegeKeyDeserializer extends KeyDeserializer {
  @Override
  public Privilege deserializeKey(final String key, final DeserializationContext ctxt) throws IOException {
    String[] tokens = key.split(":");
    if (tokens.length > 2) {
      throw new DeserializationException("Invalid privilege: " + key);
    }
    if (tokens.length == 1) {
      return new Privilege(PrivilegeType.valueOf(tokens[0]));
    }
    return new Privilege(PrivilegeType.valueOf(tokens[0]), tokens[1]);
  }
}