package lcsb.mapviewer.modelutils.serializer.model.plugin;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.plugin.PluginDataEntry;

public class PluginDataEntrySerializer extends JsonSerializer<PluginDataEntry> {

  @Override
  public void serialize(final PluginDataEntry entry, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeStringField("key", entry.getKey());
    gen.writeStringField("value", entry.getValue());
    if (entry.getUser() != null) {
      gen.writeStringField("user", entry.getUser().getLogin());
    }
    gen.writeEndObject();
  }
}