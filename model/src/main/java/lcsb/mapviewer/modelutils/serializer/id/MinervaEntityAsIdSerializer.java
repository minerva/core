package lcsb.mapviewer.modelutils.serializer.id;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lcsb.mapviewer.model.MinervaEntity;

import java.io.IOException;

public class MinervaEntityAsIdSerializer extends JsonSerializer<MinervaEntity> {

  @Override
  public void serialize(final MinervaEntity entity, final JsonGenerator gen, final SerializerProvider serializers)
      throws IOException {
    gen.writeNumber(entity.getId());
  }
}
