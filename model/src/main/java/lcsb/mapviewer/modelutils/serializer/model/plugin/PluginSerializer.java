package lcsb.mapviewer.modelutils.serializer.model.plugin;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.plugin.Plugin;

public class PluginSerializer extends JsonSerializer<Plugin> {

  @Override
  public void serialize(final Plugin plugin, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeStringField("hash", plugin.getHash());
    gen.writeStringField("name", plugin.getName());
    gen.writeStringField("version", plugin.getVersion());
    gen.writeBooleanField("isPublic", plugin.isPublic());
    gen.writeBooleanField("isDefault", plugin.isDefault());
    gen.writeObjectField("urls", plugin.getUrls());
    gen.writeEndObject();
  }
}