package lcsb.mapviewer.modelutils.serializer.model.user.annotator;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.user.annotator.AnnotatorConfigParameter;

public class AnnotatorConfigParameterSerializer extends JsonSerializer<AnnotatorConfigParameter> {

  @Override
  public void serialize(final AnnotatorConfigParameter configParameter, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeStringField("type", configParameter.getType().name());
    gen.writeStringField("name", configParameter.getType().name());
    gen.writeStringField("commonName", configParameter.getType().getName());
    if (configParameter.getType().getType() != null) {
      gen.writeStringField("inputType", configParameter.getType().getType().getName());
    }
    gen.writeStringField("description", configParameter.getType().getDescription());
    gen.writeStringField("value", configParameter.getValue());
    gen.writeNumberField("order", configParameter.getOrderPosition());
    gen.writeStringField("type", "CONFIG");

    gen.writeEndObject();
  }
}