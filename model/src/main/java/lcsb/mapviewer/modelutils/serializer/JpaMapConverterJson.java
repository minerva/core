package lcsb.mapviewer.modelutils.serializer;

import java.io.IOException;
import java.util.Map;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Converter(autoApply = true)
public class JpaMapConverterJson implements AttributeConverter<Map<String, Object>, String> {

  private static Logger logger = LogManager.getLogger();

  private static final ObjectMapper objectMapper = new ObjectMapper();

  @Override
  public String convertToDatabaseColumn(final Map<String, Object> meta) {
    if (meta == null) {
      return null;
    }
    try {
      return objectMapper.writeValueAsString(meta);
    } catch (JsonProcessingException ex) {
      logger.error("Unexpected exception during serializing object to json: ", ex);
      return null;
    }
  }

  @Override
  public Map<String, Object> convertToEntityAttribute(final String dbData) {
    if (dbData == null) {
      return null;
    }
    try {
      return objectMapper.readValue(dbData, new TypeReference<Map<String, Object>>() {
      });
    } catch (IOException ex) {
      logger.error("Unexpected IOEx decoding json from database: " + dbData);
      return null;
    }
  }

}