package lcsb.mapviewer.modelutils.serializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class CalendarSerializer extends JsonSerializer<Calendar> {

  private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  @Override
  public void serialize(final Calendar entry, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeString(dateFormat.format(entry.getTime()));
  }
}