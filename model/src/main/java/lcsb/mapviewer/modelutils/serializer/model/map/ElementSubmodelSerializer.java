package lcsb.mapviewer.modelutils.serializer.model.map;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.map.model.ElementSubmodelConnection;

public class ElementSubmodelSerializer extends JsonSerializer<ElementSubmodelConnection> {

  @SuppressWarnings("unused")
  private static Logger logger = LogManager.getLogger();

  @Override
  public void serialize(final ElementSubmodelConnection connection, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {

    gen.writeStartObject();

    gen.writeObjectField("mapId", connection.getSubmodel().getId());
    gen.writeObjectField("type", connection.getType());

    gen.writeEndObject();
  }

}
