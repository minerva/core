package lcsb.mapviewer.modelutils.serializer.model.cache;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.cache.UploadedFileEntry;

public class UploadedFileEntrySerializer extends JsonSerializer<UploadedFileEntry> {

  @Override
  public void serialize(final UploadedFileEntry fileEntry, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeNumberField("id", fileEntry.getId());
    gen.writeStringField("filename", fileEntry.getOriginalFileName());
    gen.writeNumberField("length", fileEntry.getLength());
    gen.writeStringField("owner", fileEntry.getOwner() == null ? null : fileEntry.getOwner().getLogin());
    gen.writeNumberField("uploadedDataLength", fileEntry.getFileContent().length);
    gen.writeEndObject();
  }
}