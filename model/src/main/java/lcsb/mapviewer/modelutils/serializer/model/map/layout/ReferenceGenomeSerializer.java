package lcsb.mapviewer.modelutils.serializer.model.map.layout;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.map.layout.ReferenceGenome;

public class ReferenceGenomeSerializer extends JsonSerializer<ReferenceGenome> {

  @Override
  public void serialize(final ReferenceGenome genome, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeObjectField("organism", genome.getOrganism());
    gen.writeStringField("version", genome.getVersion());
    gen.writeObjectField("type", genome.getType());
    gen.writeNumberField("downloadProgress", genome.getDownloadProgress());
    gen.writeStringField("sourceUrl", genome.getSourceUrl());
    String localUrl = genome.getSourceUrl();
    if (genome.getFile() != null && genome.getFile().getDownloadProgress() >= 100) {
      localUrl = "../" + genome.getFile().getLocalPath();
    }
    gen.writeStringField("localUrl", localUrl);
    gen.writeNumberField("idObject", genome.getId());
    gen.writeObjectField("geneMapping", genome.getGeneMapping());
    gen.writeEndObject();

  }
}