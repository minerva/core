package lcsb.mapviewer.modelutils.serializer.model.user;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lcsb.mapviewer.common.exception.DeserializationException;
import lcsb.mapviewer.model.user.UserAnnotationSchema;
import lcsb.mapviewer.model.user.UserClassAnnotators;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.model.user.annotator.AnnotatorParameter;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class UserAnnotationSchemaDeserializer extends StdDeserializer<UserAnnotationSchema> {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  protected UserAnnotationSchemaDeserializer() {
    super(UserAnnotationSchema.class);
  }

  @Override
  public UserAnnotationSchema deserialize(final JsonParser parser, final DeserializationContext context) throws IOException {
    UserAnnotationSchema result = new UserAnnotationSchema();

    ObjectMapper mapper = (ObjectMapper) parser.getCodec();
    ObjectNode rootNode = mapper.readTree(parser);

    if (rootNode.hasNonNull("project-upload")) {
      JsonNode projectUpload = rootNode.get("project-upload");
      result.setValidateMiriamTypes(getBooleanValue(projectUpload, "validate-miriam"));
      result.setAnnotateModel(getBooleanValue(projectUpload, "annotate-model"));
      result.setAutoResizeMap(getBooleanValue(projectUpload, "auto-resize"));
      result.setSemanticZoomContainsMultipleOverlays(getBooleanValue(projectUpload, "semantic-zooming-contains-multiple-overlays"));
      result.setSbgnFormat(getBooleanValue(projectUpload, "sbgn"));
    }

    if (rootNode.hasNonNull("element-annotators")) {
      JsonNode elementAnnotators = rootNode.get("element-annotators");
      Iterator<String> field = elementAnnotators.fieldNames();
      while (field.hasNext()) {
        String className = field.next();
        try {
          UserClassAnnotators annotator = new UserClassAnnotators();
          List<AnnotatorData> annotators = parser.getCodec().readValue(elementAnnotators.get(className).traverse(parser.getCodec()),
              new TypeReference<List<AnnotatorData>>() {
              });
          for (final AnnotatorData annotatorData : annotators) {
            for (final AnnotatorParameter parameter : annotatorData.getAnnotatorParams()) {
              parameter.setAnnotatorData(annotatorData);
            }
          }
          annotator.setClassName(Class.forName(className));
          annotator.setAnnotators(annotators);
          result.addClassAnnotator(annotator);
        } catch (final ClassNotFoundException e) {
          throw new DeserializationException("Class not found: " + className);
        }
      }
    }
    if (rootNode.hasNonNull("gui-preferences")) {
      JsonNode guiPreferences = rootNode.get("gui-preferences");
      Iterator<String> field = guiPreferences.fieldNames();
      while (field.hasNext()) {
        String key = field.next();
        result.setGuiPreference(key, guiPreferences.get(key).asText());
      }
    }
    return result;
  }

  private Boolean getBooleanValue(final JsonNode node, final String string) {
    if (node.hasNonNull(string)) {
      return node.get(string).asBoolean();
    } else {
      return null;
    }
  }
}
