package lcsb.mapviewer.modelutils.serializer.model.map;

import java.awt.geom.Point2D;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.map.OverviewModelLink;

public class OverviewModelLinkSerializer extends JsonSerializer<OverviewModelLink> {

  @Override
  public void serialize(final OverviewModelLink entry, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeNumberField("idObject", entry.getId());
    gen.writeObjectField("polygon", entry.getPolygonCoordinates());
    gen.writeNumberField("zoomLevel", entry.getZoomLevel());
    gen.writeObjectField("modelPoint", new Point2D.Double(entry.getxCoord(), entry.getyCoord()));
    gen.writeNumberField("modelLinkId", entry.getLinkedModel().getId());
    gen.writeStringField("type", entry.getClass().getSimpleName());
    gen.writeEndObject();
  }
}