package lcsb.mapviewer.modelutils.serializer.model.user.annotator;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;

public class AnnotatorOutputParameterSerializer extends JsonSerializer<AnnotatorOutputParameter> {

  @Override
  public void serialize(final AnnotatorOutputParameter configParameter, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeObjectField("field", configParameter.getField());
    gen.writeObjectField("annotation_type", configParameter.getIdentifierType());
    gen.writeNumberField("order", configParameter.getOrderPosition());
    gen.writeStringField("type", "OUTPUT");

    gen.writeEndObject();
  }
}