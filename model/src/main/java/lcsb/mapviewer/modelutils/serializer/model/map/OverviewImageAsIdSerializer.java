package lcsb.mapviewer.modelutils.serializer.model.map;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.map.OverviewImage;

public class OverviewImageAsIdSerializer extends JsonSerializer<OverviewImage> {

  @Override
  public void serialize(final OverviewImage entry, final JsonGenerator gen, final SerializerProvider serializers)
      throws IOException {
    gen.writeNumber(entry.getId());
  }
}