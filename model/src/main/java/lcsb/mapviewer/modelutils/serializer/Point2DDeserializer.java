package lcsb.mapviewer.modelutils.serializer;

import java.awt.geom.Point2D;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class Point2DDeserializer extends JsonDeserializer<Point2D> {

  @Override
  public Point2D deserialize(final JsonParser parser, final DeserializationContext ctxt) throws IOException, JsonProcessingException {

    ObjectMapper mapper = (ObjectMapper) parser.getCodec();
    ObjectNode rootNode = mapper.readTree(parser);

    double x = rootNode.get("x").asDouble();
    double y = rootNode.get("y").asDouble();
    return new Point2D.Double(x, y);
  }
}
