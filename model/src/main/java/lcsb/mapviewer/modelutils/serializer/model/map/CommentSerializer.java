package lcsb.mapviewer.modelutils.serializer.model.map;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.PropertyFilter;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.geometry.PointTransformation;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.modelutils.serializer.CustomExceptFilter;

public class CommentSerializer extends JsonSerializer<Comment> {

  private PointTransformation pt = new PointTransformation();

  @Override
  public void serialize(final Comment comment, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    PropertyFilter filter = null;
    FilterProvider provider = serializers.getConfig().getFilterProvider();
    if (provider != null) {
      filter = provider.findPropertyFilter("commentFilter", null);
    }

    gen.writeStartObject();

    for (final String string : availableColumns()) {
      String column = string.toLowerCase();
      Object value = null;
      switch (column) {
        case "id":
        case "idobject":
          value = comment.getId();
          break;
        case "elementid":
          value = getId(comment);
          break;
        case "modelid":
          value = comment.getModelData().getId();
          break;
        case "title":
          value = getTitle(comment);
          break;
        case "pinned":
          value = comment.isPinned();
          break;
        case "content":
          value = comment.getContent();
          break;
        case "removed":
          value = comment.isDeleted();
          break;
        case "coord":
          value = getCoordinates(comment);
          break;
        case "removereason":
          value = comment.getRemoveReason();
          break;
        case "type":
          value = getType(comment);
          break;
        case "icon":
          value = "icons/comment.png?v=" + Configuration.getSystemBuildVersion(null);
          break;
        case "email":
          value = comment.getEmail();
          break;
        case "owner":
          if (comment.getUser() != null) {
            value = comment.getUser().getLogin();
          } else {
            value = null;
          }
          break;
        default:
          value = "Unknown column";
      }
      writeField(gen, string, value, filter);
    }

    gen.writeEndObject();
  }

  private void writeField(final JsonGenerator gen, final String field, final Object value, final PropertyFilter filter) throws IOException {
    if (filter == null
        || (filter instanceof CustomExceptFilter && ((CustomExceptFilter) filter).includeField(field))) {
      gen.writeObjectField(field, value);
    }
  }

  public static List<String> availableColumns() {
    return Arrays.asList(
        "title",
        "icon",
        "type",
        "content",
        "removed",
        "coord",
        "modelId",
        "elementId",
        "id",
        "pinned",
        "email",
        "owner",
        "removeReason");
  }

  private String getId(final Comment comment) {
    if (comment.getElement() != null) {
      return comment.getElement().getId() + "";
    }
    if (comment.getReaction() != null) {
      return comment.getReaction().getId() + "";
    }
    return String.format("%.2f", comment.getCoordinates().getX()) + ","
        + String.format("%.2f", comment.getCoordinates().getY());
  }

  private Point2D getCoordinates(final Comment comment) {
    Point2D coordinates = comment.getCoordinates();
    if (comment.getCoordinates() != null) {
      coordinates = comment.getCoordinates();
    }
    Reaction reaction = comment.getReaction();
    Element element = comment.getElement();
    if (reaction != null) {
      Line2D centerLine = reaction.getLine().getLines().get(reaction.getLine().getLines().size() / 2);
      coordinates = pt.getPointOnLine(centerLine.getP1(), centerLine.getP2(), 0.5);
    } else if (element != null) {
      coordinates = element.getCenter();
    }
    return coordinates;
  }

  private ElementIdentifierType getType(final Comment comment) {
    if (comment.getElement() != null) {
      return ElementIdentifierType.ALIAS;
    } else if (comment.getReaction() != null) {
      return ElementIdentifierType.REACTION;
    } else {
      return ElementIdentifierType.POINT;
    }
  }

  private String getTitle(final Comment comment) {
    String title = "";
    if (comment.getCoordinates() != null) {
      title = "Comment (coord: " + String.format("%.2f", comment.getCoordinates().getX()) + ", "
          + String.format("%.2f", comment.getCoordinates().getY()) + ")";
    }
    Reaction reaction = comment.getReaction();
    Element element = comment.getElement();
    if (reaction != null) {
      title = "Reaction " + reaction.getIdReaction();
    } else if (element != null) {
      title = element.getName();
    }
    return title;
  }

}