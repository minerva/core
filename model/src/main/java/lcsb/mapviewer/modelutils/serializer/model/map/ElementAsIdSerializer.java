package lcsb.mapviewer.modelutils.serializer.model.map;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.map.species.Element;

public class ElementAsIdSerializer extends JsonSerializer<Element> {

  @Override
  public void serialize(final Element entry, final JsonGenerator gen, final SerializerProvider serializers) throws IOException {
    gen.writeNumber(entry.getId());
  }
}
