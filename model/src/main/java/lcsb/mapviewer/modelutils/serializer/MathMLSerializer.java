package lcsb.mapviewer.modelutils.serializer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class MathMLSerializer extends JsonSerializer<String> {

  private Logger logger = LogManager.getLogger();

  private Transformer mathMlTransformer;

  @Override
  public void serialize(final String xmlContent, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {

    try {
      Transformer transformer = getMathMLTransformer();

      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      transformer.transform(new StreamSource(new ByteArrayInputStream(xmlContent.getBytes())), new StreamResult(baos));
      String result = baos.toString("UTF-8");
      if (result.startsWith("<?xml")) {
        result = result.substring(result.indexOf(">") + 1);
      }

      gen.writeString(result);
    } catch (final TransformerException e) {
      logger.error("Problem with serializing to MathML",e);
      gen.writeNull();
    }
  }

  private Transformer getMathMLTransformer()
      throws TransformerFactoryConfigurationError, IOException, TransformerConfigurationException {
    if (this.mathMlTransformer == null) {
      TransformerFactory factory = TransformerFactory.newInstance();

      Resource resource = new ClassPathResource("mathmlc2p.xsl");
      InputStream styleInputStream = resource.getInputStream();

      StreamSource stylesource = new StreamSource(styleInputStream);
      this.mathMlTransformer = factory.newTransformer(stylesource);
    }
    return this.mathMlTransformer;
  }

}