package lcsb.mapviewer.modelutils.serializer.id;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.user.User;

public class UserAsIdSerializer extends JsonSerializer<User> {

  @Override
  public void serialize(final User user, final JsonGenerator gen, final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeStringField("login", user.getLogin());
    gen.writeEndObject();
  }
}