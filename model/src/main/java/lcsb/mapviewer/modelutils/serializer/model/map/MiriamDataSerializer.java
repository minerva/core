package lcsb.mapviewer.modelutils.serializer.model.map;

import java.io.IOException;
import java.util.Objects;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

public class MiriamDataSerializer extends JsonSerializer<MiriamData> {

  @Override
  public void serialize(final MiriamData annotation, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeStringField("link", annotation.getLink());
    if (Objects.equals(annotation.getDataType(), MiriamType.PUBMED)) {
      gen.writeObjectField("article", annotation.getArticle());
    }
    gen.writeObjectField("type", annotation.getDataType());
    gen.writeStringField("resource", annotation.getResource());
    gen.writeNumberField("id", annotation.getId());

    if (annotation.getAnnotator() != null) {
      gen.writeStringField("annotatorClassName", annotation.getAnnotator().getName());
    } else {
      gen.writeStringField("annotatorClassName", "");
    }
    gen.writeEndObject();
  }
}