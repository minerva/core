package lcsb.mapviewer.modelutils.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.awt.Color;
import java.io.IOException;

public class ColorSerializer extends JsonSerializer<Color> {

  @Override
  public void serialize(final Color color, final JsonGenerator gen,
                        final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeNumberField("alpha", color.getAlpha());
    gen.writeNumberField("rgb", color.getRGB());
    gen.writeEndObject();
  }
}