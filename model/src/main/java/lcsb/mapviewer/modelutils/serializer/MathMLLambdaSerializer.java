package lcsb.mapviewer.modelutils.serializer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class MathMLLambdaSerializer extends JsonSerializer<String> {

  private Logger logger = LogManager.getLogger();

  private MathMLSerializer mathMLSerializer = new MathMLSerializer();

  @Override
  public void serialize(final String xmlContent, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {

    try {
      String result = xmlContent.replaceAll("<lambda>", "").replaceAll("</lambda>", "");

      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      Document document = dbf.newDocumentBuilder().parse(new InputSource(new ByteArrayInputStream(result.getBytes())));

      NodeList nodes = document.getElementsByTagName("bvar");
      for (int i = nodes.getLength() - 1; i >= 0; i--) {
        Node e = nodes.item(i);
        e.getParentNode().removeChild(e);
      }

      TransformerFactory tf = TransformerFactory.newInstance();
      Transformer t = tf.newTransformer();
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      t.transform(new DOMSource(document), new StreamResult(baos));

      mathMLSerializer.serialize(baos.toString(), gen, serializers);
    } catch (final Exception e) {
      logger.error("Problem with serializing to MathML", e);
      gen.writeNull();
    }
  }
}