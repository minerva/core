package lcsb.mapviewer.modelutils.serializer;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter.FilterExceptFilter;

public class CustomExceptFilter extends FilterExceptFilter {

  public CustomExceptFilter(final String... propertyArray) {
    super(new HashSet<>(propertyArray.length));
    Collections.addAll(_propertiesToInclude, propertyArray);
  }

  public CustomExceptFilter(final Collection<String> properties) {
    super(new HashSet<>(properties));
  }

  public boolean includeField(final String fieldName) {
    return _propertiesToInclude.contains(fieldName);
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

}
