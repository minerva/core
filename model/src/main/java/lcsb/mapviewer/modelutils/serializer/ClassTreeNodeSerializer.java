package lcsb.mapviewer.modelutils.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.modelutils.map.ClassTreeNode;

public class ClassTreeNodeSerializer extends JsonSerializer<ClassTreeNode> {

  @Override
  public void serialize(final ClassTreeNode node, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeStringField("className", node.getClazz().getName());
    gen.writeStringField("name", node.getCommonName());
    if (node.getParent() == null) {
      gen.writeStringField("parentClass", null);
    } else {
      gen.writeStringField("parentClass", node.getParent().getClazz().getName());
    }
    gen.writeEndObject();
  }
}