package lcsb.mapviewer.modelutils.serializer.model.user;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.user.ConfigurationElementEditType;
import lcsb.mapviewer.model.user.ConfigurationOption;

public class ConfigurationOptionSerializer extends JsonSerializer<ConfigurationOption> {

  @Override
  public void serialize(final ConfigurationOption option, final JsonGenerator gen, final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeNumberField("idObject", option.getId());
    gen.writeStringField("type", option.getType().name());
    gen.writeStringField("valueType", option.getType().getEditType().name());
    gen.writeStringField("commonName", option.getType().getCommonName());
    gen.writeBooleanField("isServerSide", option.getType().isServerSide());
    // don't send password over API
    if (!option.getType().getEditType().equals(ConfigurationElementEditType.PASSWORD)) {
      gen.writeStringField("value", option.getValue());
    }
    if (option.getType().getGroup() != null) {
      gen.writeStringField("group", option.getType().getGroup().getCommonName());
    }
    gen.writeEndObject();
  }
}