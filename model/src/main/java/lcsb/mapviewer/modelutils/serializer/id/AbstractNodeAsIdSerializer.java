package lcsb.mapviewer.modelutils.serializer.id;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.map.reaction.AbstractNode;

public class AbstractNodeAsIdSerializer extends JsonSerializer<AbstractNode> {

  @Override
  public void serialize(final AbstractNode model, final JsonGenerator gen, final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeNumberField("id", model.getId());
    gen.writeEndObject();
  }
}
