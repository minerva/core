package lcsb.mapviewer.modelutils.serializer;

import java.awt.geom.Line2D;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class Line2DSerializer extends JsonSerializer<Line2D> {

  @Override
  public void serialize(final Line2D line, final JsonGenerator gen,
      final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeNumberField("x1", line.getX1());
    gen.writeNumberField("y1", line.getY1());
    gen.writeNumberField("x2", line.getX2());
    gen.writeNumberField("y2", line.getY2());
    gen.writeEndObject();
  }
}
