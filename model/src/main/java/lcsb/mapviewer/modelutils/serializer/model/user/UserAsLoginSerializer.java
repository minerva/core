package lcsb.mapviewer.modelutils.serializer.model.user;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.user.User;

public class UserAsLoginSerializer extends JsonSerializer<User> {

  @Override
  public void serialize(final User entry, final JsonGenerator gen, final SerializerProvider serializers) throws IOException {
    gen.writeString(entry.getLogin());
  }
}