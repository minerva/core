package lcsb.mapviewer;

import lcsb.mapviewer.common.tests.TestUtils;
import lcsb.mapviewer.common.tests.UnitTestFailedWatcher;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;

import java.awt.geom.Point2D;

public class ModelTestFunctions extends TestUtils {

  protected static Logger logger = LogManager.getLogger();

  private static int elementCounter = 0;

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();

  public GenericProtein createProtein() {
    GenericProtein result = new GenericProtein("s" + (elementCounter++));
    result.setHomodimer(12);
    result.addStructuralState(new StructuralState());
    result.setHypothetical(true);

    Residue residue = new Residue();
    result.addResidue(residue);

    residue.setIdModificationResidue("a");
    residue.setName("name");
    residue.setPosition(new Point2D.Double(10, 20));
    residue.setState(ModificationState.DONT_CARE);
    return result;
  }

  protected Layer createLayer() {
    return createLayer(true);
  }

  protected Layer createLayer(final boolean random) {
    final Layer result = new Layer();
    if (random) {
      result.setLayerId("l" + elementCounter++);
      result.setName(faker.name().name());
      result.setZ(faker.number().numberBetween(1, 100));
    } else {
      result.setLayerId("l1");
      result.setName("test name");
      result.setZ(2);
    }
    return result;
  }

}
