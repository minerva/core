package lcsb.mapviewer.model.overlay;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.model.map.layout.ProjectBackgroundStatus;

public class LayoutStatusTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (final ProjectBackgroundStatus type : ProjectBackgroundStatus.values()) {
      assertNotNull(type);

      // for coverage tests
      ProjectBackgroundStatus.valueOf(type.toString());
      assertNotNull(type.getCommonName());
    }
  }

}
