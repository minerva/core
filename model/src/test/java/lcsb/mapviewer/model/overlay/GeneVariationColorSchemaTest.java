package lcsb.mapviewer.model.overlay;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class GeneVariationColorSchemaTest extends ModelTestFunctions {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new GeneVariantDataOverlayEntry());
  }

  @Test
  public void testConstructor() {
    GeneVariantDataOverlayEntry schema = new GeneVariantDataOverlayEntry();
    schema.setDescription("desc");
    GeneVariantDataOverlayEntry copy = new GeneVariantDataOverlayEntry(schema);
    assertEquals("desc", copy.getDescription());
  }

  @Test
  public void testCopy() {
    GeneVariantDataOverlayEntry schema = new GeneVariantDataOverlayEntry();
    schema.setDescription("desc");
    GeneVariantDataOverlayEntry copy = schema.copy();

    assertEquals("desc", copy.getDescription());
  }

  @Test
  public void testAddGeneVariations() {
    List<GeneVariant> geneVariations = new ArrayList<>();
    geneVariations.add(new GeneVariant());

    GeneVariantDataOverlayEntry schema = new GeneVariantDataOverlayEntry();
    schema.addGeneVariants(geneVariations);

    assertEquals(1, schema.getGeneVariants().size());
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(GeneVariantDataOverlayEntry.class).copy();
  }

}
