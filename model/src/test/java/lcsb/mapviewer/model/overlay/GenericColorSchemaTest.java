package lcsb.mapviewer.model.overlay;

import static org.junit.Assert.assertEquals;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class GenericColorSchemaTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new GenericDataOverlayEntry());
  }

  @Test
  public void testConstructor() {
    GenericDataOverlayEntry schema = new GenericDataOverlayEntry();
    schema.setDescription("desc");
    GenericDataOverlayEntry copy = new GenericDataOverlayEntry(schema);
    assertEquals("desc", copy.getDescription());
  }

  @Test
  public void testCopy() {
    GenericDataOverlayEntry schema = new GenericDataOverlayEntry();
    schema.setDescription("desc");
    GenericDataOverlayEntry copy = schema.copy();

    assertEquals("desc", copy.getDescription());
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(GenericDataOverlayEntry.class).copy();
  }
}
