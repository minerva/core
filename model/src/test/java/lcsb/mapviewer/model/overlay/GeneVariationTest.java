package lcsb.mapviewer.model.overlay;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class GeneVariationTest extends ModelTestFunctions {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() throws Exception {
    String modifiedDna = "str";

    int position = 12;

    GeneVariant gv = new GeneVariant();

    gv.setModifiedDna(modifiedDna);
    assertEquals(modifiedDna, gv.getModifiedDna());

    gv.setPosition(position);

    assertEquals((long) position, (long) gv.getPosition());
  }

  @Test
  public void testCopy() throws Exception {
    String modifiedDna = "str";

    GeneVariant gv = new GeneVariant();

    gv.setModifiedDna(modifiedDna);

    GeneVariant copy = gv.copy();

    assertEquals(gv.getModifiedDna(), copy.getModifiedDna());
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() throws Exception {
    Mockito.spy(GeneVariant.class).copy();
  }

}
