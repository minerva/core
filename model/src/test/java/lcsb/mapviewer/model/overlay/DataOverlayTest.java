package lcsb.mapviewer.model.overlay;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class DataOverlayTest {

  @Test
  public void testComparator() {
    DataOverlay userData1 = new DataOverlay();
    DataOverlay userData2 = new DataOverlay();
    DataOverlay globalData1 = new DataOverlay();
    DataOverlay globalData2 = new DataOverlay();
    globalData1.setPublic(true);
    globalData2.setPublic(true);

    List<DataOverlay> list = new ArrayList<>(Arrays.asList(userData1, globalData1, userData2, globalData2));
    list.sort(DataOverlay.ORDER_COMPARATOR);

    assertTrue(list.indexOf(userData1) <= 1);
    assertTrue(list.indexOf(userData2) <= 1);
    assertTrue(list.indexOf(globalData1) > 1);
    assertTrue(list.indexOf(globalData2) > 1);
  }

}
