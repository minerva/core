package lcsb.mapviewer.model.overlay;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.ModelTestFunctions;

public class InvalidColorSchemaExceptionTest extends ModelTestFunctions {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new InvalidDataOverlayException("x"));
  }

  @Test
  public void testConstructor1() {
    Exception e = new InvalidDataOverlayException(new Exception());
    assertNotNull(e);
  }

  @Test
  public void testConstructor2() {
    Exception e = new InvalidDataOverlayException("message", new Exception());
    assertNotNull(e);
  }

}
