package lcsb.mapviewer.model.overlay;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.layout.ProjectBackground;
import lcsb.mapviewer.model.map.layout.ProjectBackgroundStatus;

public class LayoutTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new ProjectBackground());
  }

  @Test
  public void testConstructor() {
    String title = "TIT";
    ProjectBackground layout = new ProjectBackground(title);
    assertEquals(title, layout.getName());
  }

  @Test
  public void testConstructor2() {
    String title = "TIT";
    ProjectBackground layout = new ProjectBackground(title);
    ProjectBackground layout2 = new ProjectBackground(layout);
    assertNotNull(layout2);
  }

  @Test
  public void testConstructorWithOrder() {
    ProjectBackground overlay1 = new ProjectBackground();
    overlay1.setOrderIndex(12);
    ProjectBackground overlay2 = new ProjectBackground(overlay1);
    assertEquals(overlay1.getOrderIndex(), overlay2.getOrderIndex());
  }

  @Test
  public void testCopy() {
    String title = "TIT";
    ProjectBackground layout = new ProjectBackground(title);
    ProjectBackground layout2 = layout.copy();
    assertNotNull(layout2);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(ProjectBackground.class).copy();
  }

  @Test
  public void testGetters() {
    boolean hierarchicalView = false;
    int id = 62;
    String description = "qds";
    String title = "tit";
    ProjectBackgroundStatus status = ProjectBackgroundStatus.FAILURE;
    double progress = 1.6;
    ProjectBackground layout = new ProjectBackground();
    layout.setHierarchicalView(hierarchicalView);
    assertEquals(hierarchicalView, layout.isHierarchicalView());
    layout.setId(id);
    assertEquals(id, layout.getId());
    layout.setDescription(description);
    assertEquals(description, layout.getDescription());
    layout.setName(title);
    assertEquals(title, layout.getName());
    layout.setCreator(null);
    assertNull(layout.getCreator());
    layout.setStatus(status);
    assertEquals(status, layout.getStatus());
    layout.setProgress(progress);
    assertEquals(progress, layout.getProgress(), Configuration.EPSILON);
  }

}
