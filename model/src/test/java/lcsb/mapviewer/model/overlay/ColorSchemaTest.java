package lcsb.mapviewer.model.overlay;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;

public class ColorSchemaTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSetGetters() throws Exception {
    DataOverlayEntry cs = new GenericDataOverlayEntry();
    String name = "S";
    Double lineWidth = 5.89;
    String reactionIdentifier = "re id";
    Boolean reverseReaction = true;

    cs.setName(name);
    assertEquals(name, cs.getName());

    cs.setLineWidth(lineWidth);
    assertEquals(lineWidth, cs.getLineWidth());

    cs.setElementId(reactionIdentifier);
    assertEquals(reactionIdentifier, cs.getElementId());

    cs.setReverseReaction(reverseReaction);
    assertEquals(reverseReaction, cs.getReverseReaction());
  }

  @Test
  public void testToString() throws Exception {
    DataOverlayEntry cs = new GenericDataOverlayEntry();
    assertNotNull(cs.toString());
    cs.setName("gene name");
    cs.addCompartment("A");
    cs.addType(Species.class);
    cs.setValue(1.2);
    cs.setColor(Color.BLACK);
    cs.addMiriamData(new MiriamData(MiriamType.CAS, "X"));
    assertNotNull(cs.toString());
  }

  @Test
  public void testAddCompartments() throws Exception {
    DataOverlayEntry cs = new GenericDataOverlayEntry();
    cs.addCompartments(new String[] { "a", "b" });
    assertEquals(2, cs.getCompartments().size());
  }

  @Test
  public void testAddCompartments2() throws Exception {
    DataOverlayEntry cs = new GenericDataOverlayEntry();
    List<String> compartments = new ArrayList<>();
    compartments.add("a");
    compartments.add("b");
    cs.addCompartments(compartments);
    assertEquals(2, cs.getCompartments().size());
  }

  @Test
  public void testAddIdentifgierColumns() throws Exception {
    MiriamData generalIdentifier = new MiriamData();
    DataOverlayEntry cs = new GenericDataOverlayEntry();
    List<Pair<MiriamType, String>> compartments = new ArrayList<>();
    compartments.add(new Pair<MiriamType, String>(MiriamType.CAS, "x"));
    cs.addMiriamData(generalIdentifier);

    assertEquals(1, cs.getMiriamData().size());
    assertEquals(generalIdentifier, cs.getMiriamData().iterator().next());
  }

  @Test
  public void testAddTypes() throws Exception {
    DataOverlayEntry cs = new GenericDataOverlayEntry();
    List<Class<? extends BioEntity>> compartments = new ArrayList<>();
    compartments.add(Species.class);
    compartments.add(Protein.class);
    cs.addTypes(compartments);
    assertEquals(2, cs.getTypes().size());
  }

}
