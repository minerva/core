package lcsb.mapviewer.model;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ProjectTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Project());
  }

  @Test
  public void testProject() {
    Project project = new Project("str");
    assertNotNull(project);
  }

  @Test
  public void testAddModel() {
    Model model = new ModelFullIndexed(null);
    Model model2 = new ModelFullIndexed(null);
    Project project = new Project();
    assertEquals(0, project.getModels().size());
    project.addModel(model);
    assertEquals(1, project.getModels().size());
    project.addModel(model);
    assertEquals(1, project.getModels().size());
    project.addModel(model2.getModelData());
    assertEquals(2, project.getModels().size());
    project.removeModel(model2.getModelData());
    assertEquals(1, project.getModels().size());
  }

  @Test
  public void testGetters() {
    MiriamData organism = new MiriamData();
    int id = faker.number().numberBetween(1, Integer.MAX_VALUE);
    String projectId = faker.numerify("P####");
    ProjectStatus status = faker.options().option(ProjectStatus.class);
    double progress = faker.number().randomDouble(2, 1, 100);
    Set<ModelData> models = new HashSet<>();
    String directory = faker.numerify("dir-####");
    String name = faker.name().fullName();
    MiriamData disease = new MiriamData();
    Project project = new Project("str");
    boolean sbgn = true;

    project.setSbgnFormat(sbgn);
    assertEquals(sbgn, project.isSbgnFormat());

    project.setId(id);
    assertEquals(id, project.getId());

    project.setProjectId(projectId);
    assertEquals(projectId, project.getProjectId());

    project.setStatus(status);
    assertEquals(status, project.getStatus());

    project.setProgress(progress);
    assertEquals(progress, project.getProgress(), Configuration.EPSILON);

    project.setModels(models);
    assertEquals(models, project.getModels());

    project.setDirectory(directory);
    assertEquals(directory, project.getDirectory());

    project.setName(name);
    assertEquals(name, project.getName());

    project.setDisease(disease);
    assertEquals(disease, project.getDisease());

    UploadedFileEntry entry = new UploadedFileEntry();
    project.setInputData(entry);
    assertEquals(entry, project.getInputData());

    project.setOrganism(organism);
    assertEquals(organism, project.getOrganism());
  }

  @Test
  public void testSetCreationDate() {
    Project project = new Project();
    Calendar creationDate = Calendar.getInstance();
    project.setCreationDate(creationDate);
    assertEquals(creationDate, project.getCreationDate());
  }

}
