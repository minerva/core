package lcsb.mapviewer.model.cache;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.ModelTestFunctions;

public class BigFileEntryTest extends ModelTestFunctions {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    Calendar downloadDate = Calendar.getInstance();
    int id = 120;
    double downloadProgress = 4.6;
    long downloadThreadId = 6;
    String localPath = "C:/";
    boolean removed = true;
    String url = "www.google.com";
    String filename = "tmp";
    byte[] content = new byte[] { 1, 2 };

    BigFileEntry element = new BigFileEntry();
    element.setDownloadDate(downloadDate);
    element.setDownloadProgress(downloadProgress);
    element.setDownloadThreadId(downloadThreadId);
    element.setId(id);
    element.setLocalPath(localPath);
    element.setRemoved(removed);
    element.setUrl(url);
    element.setOriginalFileName(filename);
    element.setFileContent(content);

    assertEquals(downloadDate, element.getDownloadDate());
    assertEquals((Double) downloadProgress, element.getDownloadProgress());
    assertEquals((Long) downloadThreadId, element.getDownloadThreadId());
    assertEquals(id, element.getId());
    assertEquals(localPath, element.getLocalPath());
    assertEquals(removed, element.isRemoved());
    assertEquals(url, element.getUrl());
    assertEquals(content, element.getFileContent());
    assertEquals(filename, element.getOriginalFileName());
  }

}
