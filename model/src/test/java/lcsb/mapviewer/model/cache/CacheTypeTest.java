package lcsb.mapviewer.model.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.ModelTestFunctions;

public class CacheTypeTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CacheType());
  }

  @Test
  public void testGetters() {
    int id = 4;
    int validity = 12;
    String className = "cname";
    CacheType ct = new CacheType();
    ct.setId(id);
    ct.setClassName(className);
    ct.setValidity(validity);
    assertEquals(id, ct.getId());
    assertEquals(className, ct.getClassName());
    assertEquals(validity, ct.getValidity());
    assertNotNull(ct.toString());
  }
}
