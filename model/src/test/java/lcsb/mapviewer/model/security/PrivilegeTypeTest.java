package lcsb.mapviewer.model.security;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PrivilegeTypeTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (final PrivilegeType type : PrivilegeType.values()) {
      assertNotNull(type);

      // for coverage tests
      PrivilegeType.valueOf(type.toString());
      assertNotNull(type.getDescription());
    }
  }

}
