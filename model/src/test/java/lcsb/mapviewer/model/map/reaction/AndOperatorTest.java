package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;

public class AndOperatorTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new AndOperator());
  }

  @Test
  public void testConstructor() {
    AndOperator op = new AndOperator();
    op.setLine(new PolylineData());
    AndOperator operator = new AndOperator(op);
    assertNotNull(operator);
  }

  @Test
  public void testGetters() {
    AndOperator operator = new AndOperator();

    NodeOperator nodeOperatorForInput = new AndOperator();
    NodeOperator nodeOperatorForOutput = new AndOperator();

    Reaction reaction = new Reaction();

    operator.setReaction(reaction);
    operator.setNodeOperatorForInput(nodeOperatorForInput);
    operator.setNodeOperatorForOutput(nodeOperatorForOutput);

    assertEquals(reaction, operator.getReaction());
    assertEquals(nodeOperatorForInput, operator.getNodeOperatorForInput());
    assertEquals(nodeOperatorForOutput, operator.getNodeOperatorForOutput());
  }

  @Test
  public void testCopy1() {
    AndOperator op = new AndOperator();
    op.setLine(new PolylineData());
    AndOperator operator = op.copy();
    assertNotNull(operator);
  }

  @Test(expected = NotImplementedException.class)
  public void testCopy2() {
    Mockito.mock(AndOperator.class, Mockito.CALLS_REAL_METHODS).copy();
  }
}
