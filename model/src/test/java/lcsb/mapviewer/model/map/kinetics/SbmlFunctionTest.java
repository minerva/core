package lcsb.mapviewer.model.map.kinetics;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;

public class SbmlFunctionTest {

  @Test
  public void testConstructor() {
    String id = "function_i";
    SbmlFunction function = new SbmlFunction(id);
    assertEquals(id, function.getFunctionId());
  }

  @Test
  public void testSetName() {
    String name = "name";
    SbmlFunction function = new SbmlFunction("");
    function.setName(name);
    assertEquals(name, function.getName());
  }

  @Test
  public void testSetDefinition() throws InvalidXmlSchemaException {
    String definition = "<lambda><bvar><ci> x </ci></bvar></lambda>";
    SbmlFunction function = new SbmlFunction("");
    function.setDefinition(definition);
    assertEquals(definition, function.getDefinition());
    assertEquals(Arrays.asList("x"), function.getArguments());
  }

  @Test
  public void testCopy() throws InvalidXmlSchemaException {

    String definition = "<lambda><bvar><ci> x </ci></bvar></lambda>";
    String name = "name";
    SbmlFunction function = new SbmlFunction("");
    function.setName(name);
    function.setDefinition(definition);
    SbmlFunction copy = function.copy();

    SbmlFunctionComparator comparator = new SbmlFunctionComparator();
    assertEquals(0, comparator.compare(function, copy));
  }

}
