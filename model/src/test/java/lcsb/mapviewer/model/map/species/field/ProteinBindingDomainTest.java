package lcsb.mapviewer.model.map.species.field;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.map.species.Rna;
import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ProteinBindingDomainTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new ProteinBindingDomain());
  }

  @Test
  public void testConstructor() {
    ProteinBindingDomain region = new ProteinBindingDomain(new ProteinBindingDomain());
    assertNotNull(region);
  }

  @Test
  public void testGetters() {
    ProteinBindingDomain region = new ProteinBindingDomain();
    double size = 2.5;
    int id = 58;
    Rna species = new Rna("id");
    region.setWidth(size);
    region.setSpecies(species);
    region.setId(id);
    assertEquals(id, region.getId());
    assertEquals(size, region.getWidth(), Configuration.EPSILON);
    assertEquals(species, region.getSpecies());
  }
}
