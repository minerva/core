package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class PhenotypeComparatorTest extends ModelTestFunctions {

  private PhenotypeComparator comparator = new PhenotypeComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    Phenotype phenotype1 = createPhenotype();
    Phenotype phenotype2 = createPhenotype();

    assertEquals(0, comparator.compare(phenotype1, phenotype1));

    assertEquals(0, comparator.compare(phenotype1, phenotype2));
    assertEquals(0, comparator.compare(phenotype2, phenotype1));
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    Phenotype phenotype1 = Mockito.mock(Phenotype.class);

    comparator.compare(phenotype1, phenotype1);
  }

  @Test
  public void testDifferent() {
    Phenotype phenotype2 = createPhenotype();
    assertTrue(comparator.compare(null, phenotype2) != 0);
    assertTrue(comparator.compare(phenotype2, null) != 0);
    assertTrue(comparator.compare(null, null) == 0);

    assertTrue(comparator.compare(phenotype2, Mockito.mock(Phenotype.class)) != 0);

    Phenotype phenotype = createPhenotype();
    phenotype2 = createPhenotype();
    phenotype.setName("n");
    assertTrue(comparator.compare(phenotype, phenotype2) != 0);
  }

  public Phenotype createPhenotype() {
    Phenotype result = new Phenotype();

    return result;
  }

}
