package lcsb.mapviewer.model.map.layout.graphics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.Color;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class LayerOvalComparatorTest extends ModelTestFunctions {
  private LayerOvalComparator comparator = new LayerOvalComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    LayerOval layer1 = getLayerOval();
    LayerOval layer2 = getLayerOval();

    assertEquals(0, comparator.compare(new LayerOval(), new LayerOval()));
    assertEquals(0, comparator.compare(layer1, layer2));
    assertEquals(0, comparator.compare(layer1, layer1));
    assertEquals(0, comparator.compare(null, null));
  }

  private LayerOval getLayerOval() {

    LayerOval line = new LayerOval();
    line.setColor(Color.YELLOW);
    line.setWidth(4.3);

    return line;
  }

  @Test
  public void testDifferent() {
    LayerOval layer1 = getLayerOval();
    LayerOval layer2 = getLayerOval();

    assertTrue(comparator.compare(layer1, new LayerOval()) != 0);

    assertTrue(comparator.compare(layer1, null) != 0);
    assertTrue(comparator.compare(null, layer1) != 0);

    layer1 = getLayerOval();
    layer2 = getLayerOval();

    layer1.setColor(Color.GREEN);

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);

    layer1 = getLayerOval();
    layer2 = getLayerOval();

    layer1.setHeight(78.0);

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);

    layer1 = getLayerOval();
    layer2 = getLayerOval();

    layer1.setHeight(1.2);

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);

    layer1 = getLayerOval();
    layer2 = getLayerOval();

    layer1.setX(33.4);

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);

    layer1 = getLayerOval();
    layer2 = getLayerOval();

    layer1.setY(33.4);

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);
  }

  @Test
  public void testDifferentZIndex() {
    LayerOval layer1 = getLayerOval();
    LayerOval layer2 = getLayerOval();

    layer1.setZ(12);

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);
  }

  @Test
  public void testDifferent2() {
    LayerOval layer1 = getLayerOval();

    assertTrue(comparator.compare(layer1, Mockito.spy(LayerOval.class)) != 0);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    LayerOval layer1 = Mockito.mock(LayerOval.class);
    LayerOval layer2 = Mockito.mock(LayerOval.class);

    comparator.compare(layer1, layer2);
  }

}
