package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.Unknown;

public class ReactionComparatorTest extends ModelTestFunctions {

  private ReactionComparator comparator = new ReactionComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    Reaction reaction1 = createReaction();
    Reaction reaction2 = createReaction();
    assertEquals(0, comparator.compare(reaction1, reaction2));
    assertEquals(0, comparator.compare(reaction2, reaction1));
    assertEquals(0, comparator.compare(reaction1, reaction1));
    assertEquals(0, comparator.compare(null, null));
  }

  @Test
  public void testDifferent3() {
    Reaction reaction1 = createReaction();
    Reaction reaction2 = createReaction();

    reaction1.setNotes("a");
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction1 = createReaction();
    reaction2 = createReaction();

    reaction1.setIdReaction("a");
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction1 = createReaction();
    reaction2 = createReaction();

    reaction1.setSymbol("a");
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction1 = createReaction();
    reaction2 = createReaction();

    reaction1.setFormula("a");
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction1 = createReaction();
    reaction2 = createReaction();

    reaction1.setSubsystem("a");
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction1 = createReaction();
    reaction2 = createReaction();

    reaction1.setGeneProteinReaction("a");
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction1 = createReaction();
    reaction2 = createReaction();

    reaction1.setMechanicalConfidenceScore(1);
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction1 = createReaction();
    reaction2 = createReaction();

    reaction1.setLowerBound(1.2);
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction1 = createReaction();
    reaction2 = createReaction();

    reaction1.setUpperBound(4.3);
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction1 = createReaction();
    reaction2 = createReaction();

    reaction1.addSynonym("syn");
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction1 = createReaction();
    reaction2 = createReaction();

    reaction1.addNode(new AndOperator());
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);
  }

  @Test
  public void testDifferent4() {
    Reaction reaction1 = createReaction();
    Reaction reaction2 = createReaction();

    reaction1.addNode(new AndOperator());
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);
  }

  @Test
  public void testDifferent2() {
    Reaction reaction1 = createReaction();
    assertTrue(comparator.compare(reaction1, null) != 0);
    assertTrue(comparator.compare(null, reaction1) != 0);
  }

  @SuppressWarnings("deprecation")
  @Test
  public void testDifferent() {
    assertTrue(comparator.compare(new TransportReaction("re"), new StateTransitionReaction("re")) != 0);

    Reaction reaction1 = createReaction();
    Reaction reaction2 = createReaction();

    reaction1.setReversible(true);
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction1 = createReaction();
    reaction2 = createReaction();

    reaction1.addModifier(new Modifier());
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction1 = createReaction();
    reaction2 = createReaction();

    reaction1.getModifiers().get(0).getElement().setElementId("dfshkj");
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction1 = createReaction();
    reaction2 = createReaction();

    reaction1.addReactant(new Reactant());
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction1 = createReaction();
    reaction2 = createReaction();

    reaction1.getReactants().get(0).getElement().setElementId("dfshkj");
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction1 = createReaction();
    reaction2 = createReaction();

    reaction1.addProduct(new Product());
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction1 = createReaction();
    reaction2 = createReaction();

    reaction1.getProducts().get(0).getElement().setElementId("dfshkj");
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction1 = createReaction();
    reaction2 = createReaction();

    reaction1.getOperators().get(0).getLine().addLine(new Point2D.Double(2, 2), new Point2D.Double(2, 2));
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction1 = createReaction();
    reaction2 = createReaction();

    reaction1.getMiriamData().add(new MiriamData(MiriamRelationType.BQ_BIOL_ENCODES, MiriamType.UNKNOWN, "c"));
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);
  }

  private Reaction createReaction() {
    Reaction reaction = new Reaction();
    Species protein = new GenericProtein("id1");
    protein.setName("ASD");
    Reactant reactant = new Reactant(protein);

    Species protein2 = new GenericProtein("id2");
    protein2.setName("ASD2");
    Reactant reactant2 = new Reactant(protein2);

    Species simpleMolecule = new SimpleMolecule("id3");
    simpleMolecule.setName("mol");
    Product product = new Product(simpleMolecule);

    Species unknown = new Unknown("id4");
    unknown.setName("unk");
    Modifier modifier = new Catalysis(unknown);

    AndOperator operator = new AndOperator();
    operator.setLine(new PolylineData(new Point2D.Double(2, 2), new Point2D.Double(4, 4)));
    operator.addInput(reactant);
    operator.addInput(reactant2);

    reaction.addModifier(modifier);
    reaction.addReactant(reactant);
    reaction.addReactant(reactant2);
    reaction.addProduct(product);
    reaction.addNode(operator);
    return reaction;
  }

  @Test
  public void testDifferentNewFields() throws Exception {
    Reaction reaction1 = createReaction();
    Reaction reaction2 = createReaction();
    reaction2.setAbbreviation("ABRR");

    assertTrue(comparator.compare(reaction1, reaction2) != 0);
  }

  @Test
  public void testDifferentKinetics() {
    Reaction reaction1 = createReaction();
    Reaction reaction2 = createReaction();

    reaction1.setKinetics(new SbmlKinetics());
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);
  }

  @Test
  public void testDifferentMiriamData() {
    Reaction reaction1 = createReaction();
    Reaction reaction2 = createReaction();

    reaction1.addMiriamData(new MiriamData(MiriamType.HGNC, "SNCA"));
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction2.addMiriamData(new MiriamData(MiriamType.HGNC, "SNCA"));
    assertEquals(0, comparator.compare(reaction1, reaction2));
    assertEquals(0, comparator.compare(reaction2, reaction1));
  }

  @Test
  public void testDifferentCenterLine() {
    Reaction reaction1 = createReaction();
    Reaction reaction2 = createReaction();

    reaction1.setLine(new PolylineData(new Point2D.Double(1, 2), new Point2D.Double(1, 5)));
    assertTrue("Center line is different", comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction2.setLine(new PolylineData());
    assertTrue("Center line is different", comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction2.setLine(new PolylineData(new Point2D.Double(1, 2), new Point2D.Double(1, 5)));
    assertEquals(0, comparator.compare(reaction1, reaction2));
    assertEquals(0, comparator.compare(reaction2, reaction1));
  }

  @Test
  public void testDifferentProcessPoint() {
    Reaction reaction1 = createReaction();
    Reaction reaction2 = createReaction();

    reaction1.setProcessCoordinates(new Point2D.Double(10, 20));
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);

    reaction2.setProcessCoordinates(new Point2D.Double(20, 20));
    assertTrue(comparator.compare(reaction1, reaction2) != 0);
    assertTrue(comparator.compare(reaction2, reaction1) != 0);
  }

}
