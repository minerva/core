package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationSite;

public class GeneTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Gene());
  }

  @Test
  public void testConstructor1() {
    Gene original = new Gene();
    original.addModificationSite(new ModificationSite());
    Gene gene = new Gene(original);
    assertNotNull(gene);
  }

  @Test
  public void testGetters() {
    List<ModificationResidue> modificationResidues = new ArrayList<>();
    Gene gene = new Gene("id");
    assertNotNull(gene.getStringType());
    gene.setModificationResidues(modificationResidues);
    assertEquals(modificationResidues, gene.getModificationResidues());
  }

  @Test
  public void testCopy() {
    Gene degraded = new Gene().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Gene gene = Mockito.spy(Gene.class);
    gene.copy();
  }

}
