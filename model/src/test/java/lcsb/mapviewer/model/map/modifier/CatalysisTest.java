package lcsb.mapviewer.model.map.modifier;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.species.AntisenseRna;

public class CatalysisTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Catalysis());
  }

  @Test
  public void testConstructor() {
    Catalysis modifier = new Catalysis(new AntisenseRna("unk_id"));
    assertNotNull(modifier);
  }

  @Test
  public void testConstructor2() {
    Catalysis modifier = new Catalysis(new AntisenseRna("unk_id"));
    modifier.setLine(new PolylineData());
    Catalysis modifier2 = new Catalysis(modifier);
    assertNotNull(modifier2);
  }

  @Test
  public void testCopy() {
    Catalysis modifier = new Catalysis(new AntisenseRna("unk_id"));
    modifier.setLine(new PolylineData());
    Catalysis modifier2 = modifier.copy();
    assertNotNull(modifier2);
  }

  @Test(expected = NotImplementedException.class)
  public void testCopyInvalid() {
    Mockito.mock(Catalysis.class, Mockito.CALLS_REAL_METHODS).copy();
  }
}
