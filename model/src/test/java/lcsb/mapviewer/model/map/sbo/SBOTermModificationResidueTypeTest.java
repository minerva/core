package lcsb.mapviewer.model.map.sbo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;

public class SBOTermModificationResidueTypeTest extends ModelTestFunctions {

  @Test
  public void testGetTypeSBOTerm() {
    for (SBOTermModificationResidueType type : SBOTermModificationResidueType.values()) {
      SBOTermModificationResidueType typeFromSBO = SBOTermModificationResidueType.getTypeSBOTerm(type.getSBO(), null);
      assertEquals(type, typeFromSBO);
    }
    assertEquals(0, super.getWarnings().size());
  }

  @Test
  public void testCreateElementForSBOTerm() {
    for (SBOTermModificationResidueType type : SBOTermModificationResidueType.values()) {
      ModificationResidue residue = (SBOTermModificationResidueType.createElementForSBOTerm(type.getSBO(), "id", null));
      assertNotNull(residue);
    }
    assertEquals(0, super.getWarnings().size());

  }
}
