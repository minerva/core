package lcsb.mapviewer.model.map.kinetics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import lcsb.mapviewer.model.map.species.GenericProtein;

public class SbmlKineticsComparatorTest {
  private SbmlKineticsComparator comparator = new SbmlKineticsComparator();

  @Test
  public void testCompareEqual() {
    SbmlKinetics kinetics = createKinetics();
    SbmlKinetics kinetics2 = createKinetics();
    assertEquals(0, comparator.compare(kinetics, kinetics2));
  }

  @Test
  public void testCompareDifferentFunction() {
    SbmlKinetics kinetics = createKinetics();
    SbmlKinetics kinetics2 = createKinetics();
    kinetics2.getFunctions().iterator().next().setFunctionId("new_fun_id");
    assertTrue("Kinetics object have different functions but comparison returned equal",
        comparator.compare(kinetics, kinetics2) != 0);
  }

  @Test
  public void testCompareDifferentParameters() {
    SbmlKinetics kinetics = createKinetics();
    SbmlKinetics kinetics2 = createKinetics();
    kinetics2.getParameters().iterator().next().setParameterId("new_fun_id");
    assertTrue("Kinetics object have different functions but comparison returned equal",
        comparator.compare(kinetics, kinetics2) != 0);
  }

  @Test
  public void testCompareDifferentDefinition() {
    SbmlKinetics kinetics = createKinetics();
    SbmlKinetics kinetics2 = createKinetics();
    kinetics2.setDefinition("xxx");
    assertTrue("Kinetics object have different definition but comparison returned equal",
        comparator.compare(kinetics, kinetics2) != 0);
  }

  @Test
  public void testCompareDifferentElements() {
    SbmlKinetics kinetics = createKinetics();
    SbmlKinetics kinetics2 = createKinetics();
    kinetics2.addElement(new GenericProtein("x1"));
    assertTrue("Kinetics object have different elements but comparison returned equal",
        comparator.compare(kinetics, kinetics2) != 0);
  }

  private SbmlKinetics createKinetics() {
    SbmlKinetics result = new SbmlKinetics();
    result.addElement(new GenericProtein("protein_id"));
    result.addFunction(new SbmlFunction("fun_id"));
    result.addParameter(new SbmlParameter("param_id"));
    result.setDefinition("<xml/>");
    return result;
  }

}
