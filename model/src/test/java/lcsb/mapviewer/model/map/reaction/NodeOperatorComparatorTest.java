package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class NodeOperatorComparatorTest extends ModelTestFunctions {

  private NodeOperatorComparator comparator = new NodeOperatorComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    assertEquals(0, comparator.compare(new AndOperator(), new AndOperator()));

    NodeOperator operator1 = createNodeOperator();
    NodeOperator operator2 = createNodeOperator();

    assertEquals(0, comparator.compare(operator1, operator2));

    assertEquals(0, comparator.compare(operator1, operator1));

    assertEquals(0, comparator.compare(null, null));
  }

  @Test
  public void testDifferentElement() {
    NodeOperator operator1 = createNodeOperator();
    NodeOperator operator2 = createNodeOperator();

    Reactant reactant = (Reactant) operator1.getInputs().get(0);
    reactant.setElement(new GenericProtein("id666"));
    assertTrue(comparator.compare(operator1, operator2) != 0);
    assertTrue(comparator.compare(operator2, operator1) != 0);
  }

  @Test
  public void testDifferent() {
    NodeOperator operator1 = createNodeOperator();
    NodeOperator operator2 = createNodeOperator();

    operator1.getLine().addLine(new Point2D.Double(1, 1), new Point2D.Double(1, 1));
    assertTrue(comparator.compare(operator1, operator2) != 0);
    assertTrue(comparator.compare(operator2, operator1) != 0);

    operator1 = createNodeOperator();
    operator2 = createNodeOperator();

    operator1.addInput(new Reactant());
    assertTrue(comparator.compare(operator1, operator2) != 0);
    assertTrue(comparator.compare(operator2, operator1) != 0);

    operator1 = createNodeOperator();
    operator2 = createNodeOperator();

    operator1.addOutput(new Product());
    assertTrue(comparator.compare(operator1, operator2) != 0);
    assertTrue(comparator.compare(operator2, operator1) != 0);

    operator1 = createNodeOperator();
    operator2 = createNodeOperator();

    Product product = (Product) operator1.getOutputs().get(0);
    product.setElement(new GenericProtein("id666"));
    assertTrue(comparator.compare(operator1, operator2) != 0);
    assertTrue(comparator.compare(operator2, operator1) != 0);

    operator1 = createNodeOperator();
    operator2 = createNodeOperator();

    Reactant reactant = (Reactant) operator1.getInputs().get(0);
    reactant.getElement().setName("bla");
    assertTrue(comparator.compare(operator1, operator2) != 0);
    assertTrue(comparator.compare(operator2, operator1) != 0);

    assertTrue(comparator.compare(null, operator1) != 0);
    assertTrue(comparator.compare(operator2, null) != 0);

    assertTrue(comparator.compare(operator2, new OrOperator()) != 0);

  }

  private NodeOperator createNodeOperator() {
    AndOperator result = new AndOperator();

    Species protein0 = new GenericProtein("id0");
    protein0.setName("protein a");
    protein0.setX(12.0);
    protein0.setY(1.0);
    protein0.setWidth(2.0);
    protein0.setHeight(3.0);

    Product product = new Product(protein0);

    result.addOutput(product);

    Species protein1 = new GenericProtein("id1");
    protein1.setName("protein b");
    protein1.setX(120.0);
    protein1.setY(10.0);
    protein1.setWidth(20.0);
    protein1.setHeight(30.0);

    Reactant reactant = new Reactant(protein1);

    result.addInput(reactant);

    Species protein2 = new Gene("id2");
    protein2.setName("gene b");
    protein2.setX(320.0);
    protein2.setY(30.0);
    protein2.setWidth(30.0);
    protein2.setHeight(40.0);

    Modifier modifier = new Modifier(protein2);

    result.addInput(modifier);

    PolylineData pd = new PolylineData(new Point2D.Double(1, 2),new Point2D.Double(1, 22));
    pd.addLine(new Point2D.Double(1, 22), new Point2D.Double(10, 22));
    result.setLine(pd);

    return result;
  }

}
