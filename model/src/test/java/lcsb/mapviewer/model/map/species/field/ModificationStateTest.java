package lcsb.mapviewer.model.map.species.field;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ModificationStateTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (final ModificationState type : ModificationState.values()) {
      assertNotNull(type);

      // for coverage tests
      ModificationState.valueOf(type.toString());
      assertNotNull(type.getFullName());
      assertNotNull(type.getAbbreviation());
    }
  }

}
