package lcsb.mapviewer.model.map.model;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.PathwayCompartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Calendar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class ModelComparatorTest extends ModelTestFunctions {

  private final ModelComparator comparator = new ModelComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    final Model model1 = getModel();
    final Model model2 = getModel();

    assertEquals(0, comparator.compare(new ModelFullIndexed(null), new ModelFullIndexed(null)));
    assertEquals(0, comparator.compare(model1, model2));
    assertEquals(0, comparator.compare(model1, model1));
    assertEquals(0, comparator.compare(null, null));
  }

  @Test
  public void testDifferent2() {
    final Model model1 = getModel();
    final Model model2 = Mockito.mock(Model.class);

    assertTrue(comparator.compare(model1, model2) != 0);
  }

  @Test
  public void testDifferent3() {
    final Model model1 = getModel();
    final Model model2 = getModel();

    Compartment compartment = new PathwayCompartment("1");
    Compartment compartment2 = new PathwayCompartment("12");
    model1.addElement(compartment);
    model1.addElement(compartment2);

    compartment = new PathwayCompartment("1");
    compartment2 = new PathwayCompartment("12");
    model2.addElement(compartment);
    model2.addElement(compartment2);
    compartment2.setElementId("1");

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);
  }

  @Test
  public void testDifferentReactionsInReactionSet() {
    final Model model1 = getModel();
    final Model model2 = getModel();

    final Reaction reaction1 = new Reaction("a");
    final Reaction reaction2 = new Reaction("b");
    final Reaction reaction3 = new Reaction("a");
    final Reaction reaction4 = new Reaction("b");

    model1.addReaction(reaction1);
    model1.addReaction(reaction2);
    model2.addReaction(reaction3);
    model2.addReaction(reaction4);

    reaction2.setIdReaction("a");

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);
  }

  @Test
  public void testDifferentReactionSet() {
    final Model model1 = getModel();
    final Model model2 = getModel();

    model1.addReaction(new TransportReaction("id"));

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    final Model model1 = getModel();
    final Model model2 = getModel();

    final Species mockSpecies = Mockito.mock(Species.class);
    when(mockSpecies.getElementId()).thenReturn("1");
    model1.addElement(mockSpecies);

    model2.addElement(mockSpecies);

    comparator.compare(model1, model2);
  }

  @Test
  public void testDifferent() throws Exception {
    Model model1 = getModel();
    Model model2 = getModel();

    assertTrue(comparator.compare(model1, new ModelFullIndexed(null)) != 0);
    assertTrue(comparator.compare(model1, null) != 0);
    assertTrue(comparator.compare(null, model1) != 0);

    model1 = getModel();
    model2 = getModel();

    model1.setNotes("ASDsaD");

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);

    model1 = getModel();
    model2 = getModel();

    final GenericProtein protein = new GenericProtein("SAd");

    model1.addElement(protein);

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);

    model1 = getModel();
    model2 = getModel();

    model1.getElements().iterator().next().setElementId("sdfsd");

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);

    model1 = getModel();
    model2 = getModel();

    model1.addElement(new Compartment("unk_id"));

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);

    model1 = getModel();
    model2 = getModel();

    model1.getElementByElementId("default").setName("tmpxx");

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);

    model1 = getModel();
    model2 = getModel();

    model1.addLayer(new Layer());

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);

    model1 = getModel();
    model2 = getModel();

    model1.getLayers().iterator().next().setName("buu");

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);

    model1 = getModel();
    model2 = getModel();

    model1.addElement(new GenericProtein("ASdas"));

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);

    model1 = getModel();
    model2 = getModel();

    model1.getElements().iterator().next()
        .addMiriamData(new MiriamData(MiriamRelationType.BQ_BIOL_HAS_PART, MiriamType.CHEBI, "c"));

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);

    model1 = getModel();
    model2 = getModel();

    model1.getReactions().iterator().next().setName("dsf");

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);

    model1 = getModel();
    model2 = getModel();

    model1.setIdModel("asdsdasd");

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);

    model1 = getModel();
    model2 = getModel();

    model1.setWidth(123);

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);

    model1 = getModel();
    model2 = getModel();

    model1.setHeight(636);

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);

    model1 = getModel();
    model2 = getModel();

    model1.setZoomLevels(1);

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);

    model1 = getModel();
    model2 = getModel();

    model1.setTileSize(129);

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);

    model1 = getModel();
    model2 = getModel();

  }

  private Model getModel() {
    final Model model = new ModelFullIndexed(null);
    model.setNotes("Some description");
    final GenericProtein protein = new GenericProtein("a_id");
    protein.setName("ad");

    model.addElement(protein);

    model.addElement(new Compartment("default"));

    final Layer layer = new Layer();
    layer.setName("layer name");
    model.addLayer(layer);

    model.addReaction(new Reaction("re"));
    return model;
  }

  @Test
  public void testCompareSubmodels() throws Exception {
    final Model model1 = getModel();
    final Model model2 = getModel();

    final Model model3 = getModel();

    final ModelSubmodelConnection submodelA = new ModelSubmodelConnection(model3, SubmodelType.DOWNSTREAM_TARGETS);

    model1.addSubmodelConnection(submodelA);

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);

    final Model model4 = getModel();

    final ModelSubmodelConnection submodelB = new ModelSubmodelConnection(model4, SubmodelType.DOWNSTREAM_TARGETS);

    model2.addSubmodelConnection(submodelB);

    assertEquals(0, comparator.compare(model1, model2));

    submodelB.setName("A");

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);

    submodelB.setName(null);
    assertEquals(0, comparator.compare(model1, model2));

    model4.setNotes("ASdasdhsjkadhask");
    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);
  }

  @Test
  public void testCompareSubmodels2() throws Exception {
    final Model model1 = getModel();
    final Model model2 = getModel();

    model1.addSubmodelConnection(new ModelSubmodelConnection(getModel(), SubmodelType.DOWNSTREAM_TARGETS));
    model1.addSubmodelConnection(new ModelSubmodelConnection(getModel(), SubmodelType.DOWNSTREAM_TARGETS));

    model2.addSubmodelConnection(new ModelSubmodelConnection(getModel(), SubmodelType.DOWNSTREAM_TARGETS));
    model2.addSubmodelConnection(new ModelSubmodelConnection(getModel(), SubmodelType.PATHWAY));

    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);
  }

  @Test
  public void testCompareName() throws Exception {
    final Model model1 = getModel();
    final Model model2 = getModel();
    model1.setName("ASD");
    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);
    model2.setName("A");
    assertTrue(comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);
    model1.setName("A");
    assertEquals(0, comparator.compare(model1, model2));
    assertEquals(0, comparator.compare(model2, model1));
  }

  @Test
  public void testCompareAnnotations() throws Exception {
    final Model model1 = getModel();
    final Model model2 = getModel();
    model1.addMiriamData(new MiriamData(MiriamType.CHEBI, "CHEBI:12345"));
    assertTrue("Models have different annotations", comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);
    model2.addMiriamData(new MiriamData(MiriamType.CHEBI, "CHEBI:12345"));
    assertEquals(0, comparator.compare(model1, model2));
    assertEquals(0, comparator.compare(model2, model1));
  }

  @Test
  public void testCompareAuthors() throws Exception {
    final Model model1 = getModel();
    final Model model2 = getModel();
    model1.addAuthor(new Author("x", "y"));
    assertTrue("Models have different authors", comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);
    model2.addAuthor(new Author(model1.getAuthors().iterator().next()));
    assertEquals(0, comparator.compare(model1, model2));
    assertEquals(0, comparator.compare(model2, model1));
  }

  @Test
  public void testCompareCreationDate() throws Exception {
    final Model model1 = getModel();
    final Model model2 = getModel();
    model1.setCreationDate(Calendar.getInstance());
    assertTrue("Models have different creation dates", comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);
    model2.setCreationDate(model1.getCreationDate());
    assertEquals(0, comparator.compare(model1, model2));
    assertEquals(0, comparator.compare(model2, model1));
  }

  @Test
  public void testCompareModificationDate() throws Exception {
    final Model model1 = getModel();
    final Model model2 = getModel();
    model1.addModificationDate(Calendar.getInstance());
    assertTrue("Models have different modification dates", comparator.compare(model1, model2) != 0);
    assertTrue(comparator.compare(model2, model1) != 0);
    model2.addModificationDate(model1.getModificationDates().get(0));
    assertEquals(0, comparator.compare(model1, model2));
    assertEquals(0, comparator.compare(model2, model1));
  }

}
