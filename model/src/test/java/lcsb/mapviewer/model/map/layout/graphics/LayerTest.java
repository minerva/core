package lcsb.mapviewer.model.map.layout.graphics;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.model.ModelData;
import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class LayerTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(createLayer());
  }

  @Test
  public void testConstructor() {
    final Layer layer = createLayer();
    layer.addLayerText(new LayerText());
    layer.addLayerLine(new PolylineData());
    layer.addLayerRect(new LayerRect());
    layer.addLayerOval(new LayerOval());
    final Layer copy = new Layer(layer);

    assertNotNull(copy);
  }

  @Test
  public void testCopy() {
    final Layer degraded = new Layer().copy();
    assertNotNull(degraded);
  }

  @Test
  public void testSetters() {
    final Layer layer = new Layer().copy();

    final ModelData model = new ModelData();
    final Set<LayerOval> ovals = new HashSet<>();
    final Set<LayerRect> rectangles = new HashSet<>();
    final List<PolylineData> lines = new ArrayList<>();
    final Set<LayerText> texts = new HashSet<>();

    final boolean locked = true;
    final boolean visible = true;
    final String name = "sdtr";
    final int layerId = 1;

    layer.setModel(model);
    layer.setOvals(ovals);
    layer.setTexts(texts);
    layer.setRectangles(rectangles);
    layer.setLines(lines);
    layer.setLocked(locked);
    layer.setVisible(visible);
    layer.setName(name);
    layer.setLayerId(layerId);

    assertEquals(model, layer.getModel());
    assertEquals(ovals, layer.getOvals());
    assertEquals(rectangles, layer.getRectangles());
    assertEquals(lines, layer.getLines());
    assertEquals(texts, layer.getTexts());
    assertEquals(locked, layer.isLocked());
    assertEquals(name, layer.getName());
    assertEquals(visible, layer.isVisible());
    assertEquals(layerId + "", layer.getLayerId());

    layer.setLocked("FALSE");
    layer.setVisible("FALSE");

    assertFalse(layer.isLocked());
    assertFalse(layer.isVisible());
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(Layer.class).copy();
  }

  @Test
  public void testRemoveLayerText() {
    final Layer layer = new Layer();
    layer.removeLayerText(new LayerText());
  }

  @Test
  public void testAddLines() {
    final Layer layer = new Layer();
    final List<PolylineData> lines = new ArrayList<>();
    lines.add(new PolylineData());
    layer.addLayerLines(lines);
    assertEquals(1, layer.getLines().size());
  }

}
