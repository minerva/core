package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class ReceptorProteinComparatorTest extends ModelTestFunctions {

  private ReceptorProteinComparator comparator = new ReceptorProteinComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    ReceptorProtein drug1 = createReceptorProtein();
    ReceptorProtein drug2 = createReceptorProtein();

    assertEquals(0, comparator.compare(drug1, drug1));

    assertEquals(0, comparator.compare(drug1, drug2));
    assertEquals(0, comparator.compare(drug2, drug1));
  }

  @Test
  public void testDifferent() {
    ReceptorProtein drug2 = createReceptorProtein();
    assertTrue(comparator.compare(null, drug2) != 0);
    assertTrue(comparator.compare(drug2, null) != 0);
    assertTrue(comparator.compare(null, null) == 0);

    ReceptorProtein drug = createReceptorProtein();
    drug.setName("n");
    assertTrue(comparator.compare(drug, drug2) != 0);

    assertTrue(comparator.compare(drug, Mockito.mock(ReceptorProtein.class)) != 0);
  }

  public ReceptorProtein createReceptorProtein() {
    ReceptorProtein result = new ReceptorProtein();
    return result;
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    ReceptorProtein object = Mockito.mock(ReceptorProtein.class);

    comparator.compare(object, object);
  }

}
