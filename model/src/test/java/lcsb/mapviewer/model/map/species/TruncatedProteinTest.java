package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class TruncatedProteinTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new TruncatedProtein());
  }

  @Test
  public void testConstructor() {
    TruncatedProtein species = new TruncatedProtein(new TruncatedProtein());
    assertNotNull(species);
  }

  @Test
  public void testCopy1() {
    TruncatedProtein species = new TruncatedProtein("id").copy();
    assertNotNull(species);
  }

  @Test(expected = NotImplementedException.class)
  public void testCopy2() {
    TruncatedProtein protein = Mockito.spy(TruncatedProtein.class);
    protein.copy();
  }

}
