package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;

public class ProductTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Product());
  }

  @Test
  public void testConstructor() {
    Product original = new Product();
    original.setLine(new PolylineData());
    Product product = new Product(original);
    assertNotNull(product);
  }

  @Test
  public void testCopy() {
    Product original = new Product();
    original.setLine(new PolylineData());
    Product product = original.copy();
    assertNotNull(product);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.mock(Product.class, Mockito.CALLS_REAL_METHODS).copy();
  }
}
