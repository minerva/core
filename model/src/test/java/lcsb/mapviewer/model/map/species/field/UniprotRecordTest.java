package lcsb.mapviewer.model.map.species.field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class UniprotRecordTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    UniprotRecord ur = new UniprotRecord();
    Species species = new GenericProtein("id");
    int id = 94;

    ur.setSpecies(species);
    ur.setId(id);

    assertEquals(species, ur.getSpecies());
    assertEquals(id, ur.getId());
  }

  @Test
  public void testCopy() {
    UniprotRecord ur = new UniprotRecord().copy();
    assertNotNull(ur);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    UniprotRecord ur = Mockito.spy(UniprotRecord.class);
    ur.copy();
  }

}
