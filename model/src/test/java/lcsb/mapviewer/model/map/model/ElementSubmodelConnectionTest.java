package lcsb.mapviewer.model.map.model;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class ElementSubmodelConnectionTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new ElementSubmodelConnection());
  }

  @Test
  public void testConstructor() {
    ElementSubmodelConnection connection = new ElementSubmodelConnection(new ModelData(),
        SubmodelType.DOWNSTREAM_TARGETS);
    assertNotNull(connection);
  }

  @Test
  public void testConstructor2() {
    ElementSubmodelConnection connection = new ElementSubmodelConnection(
        new ElementSubmodelConnection(new ModelData(), SubmodelType.DOWNSTREAM_TARGETS));
    assertNotNull(connection);
  }

  @Test
  public void testConstructor3() {
    ElementSubmodelConnection connection = new ElementSubmodelConnection(new ModelFullIndexed(null),
        SubmodelType.DOWNSTREAM_TARGETS, "sd");
    assertNotNull(connection);
  }

  @Test
  public void testCopy() {
    ElementSubmodelConnection connection = new ElementSubmodelConnection().copy();
    assertNotNull(connection);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(ElementSubmodelConnection.class).copy();
  }
}
