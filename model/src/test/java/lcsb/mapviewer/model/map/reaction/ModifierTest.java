package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;

public class ModifierTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Modifier());
  }

  @Test
  public void testConstructor() {
    Modifier original = new Modifier();
    original.setLine(new PolylineData());
    Modifier modifier = new Modifier(original);
    assertNotNull(modifier);
  }

  @Test
  public void testCopy() {
    Modifier original = new Modifier();
    original.setLine(new PolylineData());
    Modifier modifier = original.copy();
    assertNotNull(modifier);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.mock(Modifier.class, Mockito.CALLS_REAL_METHODS).copy();
  }

}
