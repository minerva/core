package lcsb.mapviewer.model.map.compartment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class SquareCompartmentTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new SquareCompartment());
  }

  @Test
  public void testConstructor() {
    SquareCompartment original = new SquareCompartment("id");
    original.setName("test name");
    SquareCompartment copy = new SquareCompartment(original);
    assertNotNull(copy);
    assertEquals("test name", copy.getName());
  }

  @Test
  public void testCopy() {
    SquareCompartment original = new SquareCompartment();
    SquareCompartment copy = original.copy();
    assertNotNull(copy);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    SquareCompartment compartment = Mockito.spy(SquareCompartment.class);
    compartment.copy();
  }

}
