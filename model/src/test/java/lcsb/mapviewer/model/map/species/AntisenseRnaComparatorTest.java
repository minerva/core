package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.CodingRegion;

public class AntisenseRnaComparatorTest extends ModelTestFunctions {

  private AntisenseRnaComparator comparator = new AntisenseRnaComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    AntisenseRna antisenseRna1 = createAntisenseRna();
    AntisenseRna antisenseRna2 = createAntisenseRna();

    assertEquals(0, comparator.compare(antisenseRna1, antisenseRna1));

    assertEquals(0, comparator.compare(antisenseRna1, antisenseRna2));
    assertEquals(0, comparator.compare(antisenseRna2, antisenseRna1));
  }

  @Test
  public void testDifferent() {
    AntisenseRna antisenseRna1 = createAntisenseRna();
    AntisenseRna antisenseRna2 = createAntisenseRna();
    antisenseRna1.getModificationResidues().get(0).setName("bla");
    assertTrue(comparator.compare(antisenseRna1, antisenseRna2) != 0);
    assertTrue(comparator.compare(antisenseRna2, antisenseRna1) != 0);

    antisenseRna1 = createAntisenseRna();
    antisenseRna2 = createAntisenseRna();
    antisenseRna1.getModificationResidues().clear();
    assertTrue(comparator.compare(antisenseRna1, antisenseRna2) != 0);
    assertTrue(comparator.compare(antisenseRna2, antisenseRna1) != 0);

    antisenseRna1 = createAntisenseRna();
    antisenseRna2 = createAntisenseRna();
    assertTrue(comparator.compare(null, antisenseRna2) != 0);
    assertTrue(comparator.compare(antisenseRna2, null) != 0);
    assertTrue(comparator.compare(null, null) == 0);

    AntisenseRna unknown = createAntisenseRna();
    unknown.setName("n");
    assertTrue(comparator.compare(unknown, antisenseRna2) != 0);

    assertTrue(comparator.compare(unknown, Mockito.mock(AntisenseRna.class)) != 0);
  }

  public AntisenseRna createAntisenseRna() {
    AntisenseRna result = new AntisenseRna();

    CodingRegion region1 = new CodingRegion();
    result.addCodingRegion(region1);
    region1.setIdModificationResidue("a");
    region1.setName("name");
    region1.setPosition(new Point2D.Double(0, 1));
    region1.setWidth(2);
    return result;
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    AntisenseRna element = Mockito.mock(AntisenseRna.class);
    comparator.compare(element, element);
  }
}
