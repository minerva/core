package lcsb.mapviewer.model.map;

import static org.junit.Assert.assertEquals;

import java.awt.geom.Point2D;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.user.User;

public class CommentTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Comment());
  }

  @Test
  public void testGetters() {
    Comment comment = new Comment();
    String content = "c";
    Point2D.Double coordinates = new Point2D.Double(1, 2);
    String email = "a";
    boolean deleted = true;
    int id = 85;
    Model model = new ModelFullIndexed(null);
    boolean pinned = true;
    String removeReason = "rr";
    User user = new User();

    comment.setContent(content);
    assertEquals(content, comment.getContent());
    comment.setCoordinates(coordinates);
    assertEquals(coordinates, comment.getCoordinates());
    comment.setDeleted(deleted);
    assertEquals(deleted, comment.isDeleted());
    comment.setEmail(email);
    assertEquals(email, comment.getEmail());
    comment.setId(id);
    assertEquals(id, comment.getId());
    comment.setModel(model);
    assertEquals(model, comment.getModelData().getModel());
    comment.setModelData(model.getModelData());
    assertEquals(model.getModelData(), comment.getModelData());
    comment.setPinned(pinned);
    assertEquals(pinned, comment.isPinned());
    comment.setRemoveReason(removeReason);
    assertEquals(removeReason, comment.getRemoveReason());
    comment.setUser(user);
    assertEquals(user, comment.getUser());
  }
}
