package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class SimpleMoleculeTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new SimpleMolecule());
  }

  @Test
  public void testConstructor1() {
    SimpleMolecule degraded = new SimpleMolecule("id");
    assertNotNull(degraded);
  }

  @Test
  public void testGetters() {
    SimpleMolecule degraded = new SimpleMolecule(new SimpleMolecule());
    assertNotNull(degraded.getStringType());
  }

  @Test
  public void testCopy() {
    SimpleMolecule degraded = new SimpleMolecule().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    SimpleMolecule simpleMolecule = Mockito.spy(SimpleMolecule.class);
    simpleMolecule.copy();
  }

}
