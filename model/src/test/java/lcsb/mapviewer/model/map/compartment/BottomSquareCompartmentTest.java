package lcsb.mapviewer.model.map.compartment;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class BottomSquareCompartmentTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new BottomSquareCompartment());
  }

  @Test
  public void testConstructor1() {
    BottomSquareCompartment compartment = new BottomSquareCompartment(new Compartment());
    assertNotNull(compartment);
  }

  @Test
  public void testConstructor3() {
    BottomSquareCompartment compartment = new BottomSquareCompartment("id");
    assertNotNull(compartment);
  }

  @Test
  public void testConstructor2() {
    Model model = new ModelFullIndexed(null);
    model.setWidth(1);
    model.setHeight(1);
    BottomSquareCompartment compartment = new BottomSquareCompartment(new Compartment(), model);
    assertNotNull(compartment);
  }

  @Test
  public void testCopy() {
    BottomSquareCompartment compartment = new BottomSquareCompartment().copy();
    assertNotNull(compartment);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    BottomSquareCompartment compartment = Mockito.spy(BottomSquareCompartment.class);
    compartment.copy();
  }
}
