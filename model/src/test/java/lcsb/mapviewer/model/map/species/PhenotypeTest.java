package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class PhenotypeTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Phenotype());
  }

  @Test
  public void testConstructor1() {
    Phenotype original = new Phenotype();
    Phenotype copy = new Phenotype(original);
    assertNotNull(copy);
  }

  @Test
  public void testConstructor2() {
    String id = "idd";
    Phenotype copy = new Phenotype(id);
    assertEquals(id, copy.getElementId());
  }

  @Test
  public void testGetters() {
    Phenotype copy = new Phenotype("id");
    assertNotNull(copy.getStringType());
  }

  @Test
  public void testCopy() {
    Phenotype degraded = new Phenotype().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Phenotype phenotype = Mockito.spy(Phenotype.class);
    phenotype.copy();
  }

}
