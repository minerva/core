package lcsb.mapviewer.model.map.species.field;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.awt.geom.Point2D;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CodingRegionTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new CodingRegion());
  }

  @Test
  public void testConstructor1() {
    CodingRegion antisenseRna = new CodingRegion();
    CodingRegion antisenseRna2 = new CodingRegion(antisenseRna);
    assertNotNull(antisenseRna2);
  }

  @Test
  public void testGetters() {
    CodingRegion region = new CodingRegion(new CodingRegion());
    int id = 91;
    AntisenseRna species = new AntisenseRna("id");
    Point2D position = new Point2D.Double(10, 20);
    double width = 5.3;
    String name = "nam";
    String idCodingRegion = "iddd";

    region.setId(id);
    region.setSpecies(species);
    region.setPosition(position);
    region.setWidth(width);
    region.setName(name);
    region.setIdModificationResidue(idCodingRegion);

    assertEquals(id, region.getId());
    assertEquals(species, region.getSpecies());
    assertEquals(position.getX(), region.getX(), Configuration.EPSILON);
    assertEquals(position.getY(), region.getY(), Configuration.EPSILON);
    assertEquals(width, region.getWidth(), Configuration.EPSILON);
    assertEquals(name, region.getName());
    assertEquals(idCodingRegion, region.getIdModificationResidue());
  }

  @Test
  public void testToString() {
    assertNotNull(new CodingRegion().toString());
  }

  @Test
  public void testCopy() {
    CodingRegion degraded = new CodingRegion().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.mock(CodingRegion.class, Mockito.CALLS_REAL_METHODS).copy();
  }

}
