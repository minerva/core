package lcsb.mapviewer.model.map;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;

public class OverviewLinkTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetPolygonCoord() {
    OverviewLink link = Mockito.spy(OverviewLink.class);
    String polygon = "1,1 2,2 4,0";
    link.setPolygon(polygon);
    assertEquals(3, link.getPolygonCoordinates().size());
  }

  @Test
  public void testGetters() {
    OverviewLink link = Mockito.spy(OverviewLink.class);
    OverviewImage image = new OverviewImage();
    int id = 309;
    link.setOverviewImage(image);
    link.setId(id);

    assertEquals(image, link.getOverviewImage());
    assertEquals(id, link.getId());
  }

}
