package lcsb.mapviewer.model.map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;

@RunWith(Parameterized.class)
public class MiriamTypeGenericTest extends ModelTestFunctions {
  private MiriamType miriamType;
  private boolean deprecated;
  private boolean urnExists;

  public MiriamTypeGenericTest(final MiriamType type, final boolean deprecated, final boolean urnExists) {
    this.miriamType = type;
    this.deprecated = deprecated;
    this.urnExists = urnExists;
  }

  @Parameters(name = "{0}")
  public static Collection<Object[]> data() throws IOException {
    List<Object[]> result = new ArrayList<>();

    for (final MiriamType mt : MiriamType.values()) {
      boolean deprecated = isAnnotatedBy(mt, Deprecated.class);
      boolean urnExists = !isAnnotatedBy(mt, NoMiriamUrn.class);

      result.add(new Object[] { mt, deprecated, urnExists });
    }
    return result;
  }

  private static boolean isAnnotatedBy(final MiriamType mt, final Class<? extends Annotation> clazz) {
    boolean deprecated = false;
    try {
      Field f = MiriamType.class.getField(mt.name());
      if (f.isAnnotationPresent(clazz)) {
        deprecated = true;
      }
    } catch (final NoSuchFieldException | SecurityException e) {
    }
    return deprecated;
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testProperties() {
    assertNotNull(miriamType.getUris());
    assertNotNull(miriamType.getValidClass());
    assertNotNull(miriamType.getRequiredClass());
    if (!deprecated) {
      assertNotNull("No homepage for: " + miriamType, miriamType.getDbHomepage());
    }
  }

  @Test
  public void checkUrnExists() {
    if (!deprecated && urnExists) {
      boolean urnUriExists = false;
      for (final String uri : miriamType.getUris()) {
        if (uri.toLowerCase().startsWith("urn:")) {
          urnUriExists = true;
        }
      }
      assertTrue(urnUriExists);
    }
  }

  @Test
  public void identifiersOrgUrlExists() {
    if (!deprecated) {
      boolean identifiersOrgUriExists = false;
      for (final String uri : miriamType.getUris()) {
        if ((uri.toLowerCase().startsWith("http://identifiers.org/")
            || uri.toLowerCase().startsWith("https://identifiers.org/"))
            && uri.endsWith("/")) {
          identifiersOrgUriExists = true;
        }
      }
      assertTrue(identifiersOrgUriExists);
    }
  }

  @Test
  public void httpAndHttpsForIdentifiersOrg() {
    if (!deprecated) {
      boolean httpIdentifiersOrgUriExists = false;
      boolean httpsIdentifiersOrgUriExists = false;
      for (final String uri : miriamType.getUris()) {
        if (uri.toLowerCase().startsWith("http://identifiers.org/") && uri.endsWith("/")) {
          httpIdentifiersOrgUriExists = true;
        }
        if (uri.toLowerCase().startsWith("https://identifiers.org/") && uri.endsWith("/")) {
          httpsIdentifiersOrgUriExists = true;
        }
      }
      assertTrue("Http identifiers.org url does not exists for: " + miriamType, httpIdentifiersOrgUriExists);
      assertTrue("Https identifiers.org url does not exists for: " + miriamType, httpsIdentifiersOrgUriExists);
    }
  }

  @Test
  public void testNewHttpAndHttpsForIdentifiersOrg() {
    if (!deprecated) {
      boolean httpIdentifiersOrgUriExists = false;
      boolean httpsIdentifiersOrgUriExists = false;
      for (final String uri : miriamType.getUris()) {
        if (uri.toLowerCase().startsWith("http://identifiers.org/") && !uri.endsWith("/")) {
          httpIdentifiersOrgUriExists = true;
        }
        if (uri.toLowerCase().startsWith("https://identifiers.org/") && !uri.endsWith("/")) {
          httpsIdentifiersOrgUriExists = true;
        }
      }
      assertTrue("Http identifiers.org url does not exists for: " + miriamType, httpIdentifiersOrgUriExists);
      assertTrue("Https identifiers.org url does not exists for: " + miriamType, httpsIdentifiersOrgUriExists);
    }
  }

  @Test
  public void testGetMiriamByIncompleteUri() throws Exception {
    for (final String uri : miriamType.getUris()) {
      try {
        MiriamType.getMiriamByUri(uri);
        fail("Exception expected for: " + uri);
      } catch (final InvalidArgumentException e) {
      }
    }
  }

}
