package lcsb.mapviewer.model.map.kinetics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class SbmlParameterComparatorTest {
  private SbmlParameterComparator comparator = new SbmlParameterComparator();

  private SbmlParameter createParameter() {
    SbmlParameter result = new SbmlParameter("param_id");
    return result;
  }

  @Test
  public void testCompareEqual() {
    SbmlParameter parameter = createParameter();
    SbmlParameter parameter2 = createParameter();
    assertEquals(0, comparator.compare(parameter, parameter2));
  }

  @Test
  public void testCompareDifferentParameterId() {
    SbmlParameter parameter = createParameter();
    SbmlParameter parameter2 = createParameter();
    parameter.setParameterId("xxx");
    assertTrue("Parameter object have different id but comparison returned equal",
        comparator.compare(parameter, parameter2) != 0);
  }

  @Test
  public void testCompareDifferentName() {
    SbmlParameter parameter = createParameter();
    SbmlParameter parameter2 = createParameter();
    parameter.setName("xxx");
    assertTrue("Parameter object have different name but comparison returned equal",
        comparator.compare(parameter, parameter2) != 0);
  }

  @Test
  public void testCompareDifferentDefinition() {
    SbmlParameter parameter = createParameter();
    SbmlParameter parameter2 = createParameter();
    parameter.setValue(13.0);
    assertTrue("Parameter object have different value but comparison returned equal",
        comparator.compare(parameter, parameter2) != 0);
  }

  @Test
  public void testCompareDifferentArguments() {
    SbmlParameter parameter = createParameter();
    SbmlParameter parameter2 = createParameter();
    parameter.setUnits(new SbmlUnit("x"));
    assertTrue("Parameter object have different unit but comparison returned equal",
        comparator.compare(parameter, parameter2) != 0);
  }

}
