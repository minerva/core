package lcsb.mapviewer.model.map.kinetics;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import lcsb.mapviewer.common.Configuration;

public class SbmlUnitTypeFactorTest {

  @Test
  public void testConstructor() {
    int exponent = 1;
    int scale = 1;
    double multiplier = 1.0;
    SbmlUnitType unitType = SbmlUnitType.METRE;
    SbmlUnitTypeFactor factor = new SbmlUnitTypeFactor(unitType, exponent, scale, multiplier);
    assertEquals(factor.getUnitType(), unitType);
    assertEquals(factor.getExponent(), exponent);
    assertEquals(factor.getScale(), scale);
    assertEquals(factor.getMultiplier(), multiplier, Configuration.EPSILON);

  }

}
