package lcsb.mapviewer.model.map.reaction.type;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;

public class UnknownReducedTriggerReactionTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new UnknownReducedTriggerReaction());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructorWithInvalidArg() {
    Reaction reaction = new Reaction("re");
    new UnknownReducedTriggerReaction(reaction);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructorWithInvalidArg2() {
    Reaction reaction = new Reaction("re");
    reaction.addProduct(new Product(createProtein()));
    new UnknownReducedTriggerReaction(reaction);
  }

  @Test
  public void testConstructor() {
    Reaction reaction = new Reaction("re");
    reaction.addProduct(new Product(createProtein()));
    reaction.addReactant(new Reactant(createProtein()));
    UnknownReducedTriggerReaction validReaction = new UnknownReducedTriggerReaction(reaction);
    assertNotNull(validReaction);
  }

  @Test
  public void testCopy() {
    UnknownReducedTriggerReaction original = new UnknownReducedTriggerReaction();
    original.addProduct(new Product(createProtein()));
    original.addReactant(new Reactant(createProtein()));
    original.addReactant(new Reactant(createProtein()));
    UnknownReducedTriggerReaction product = original.copy();
    assertNotNull(product);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(new UnknownReducedTriggerReaction()).copy();
  }

  @Test
  public void testGetters() {
    UnknownReducedTriggerReaction original = new UnknownReducedTriggerReaction();
    assertNull(original.getReactionRect());
    assertNotNull(original.getStringType());
  }
}
