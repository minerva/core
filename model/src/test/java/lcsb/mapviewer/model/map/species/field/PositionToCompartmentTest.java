package lcsb.mapviewer.model.map.species.field;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PositionToCompartmentTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (final PositionToCompartment type : PositionToCompartment.values()) {
      assertNotNull(type);
      assertNotNull(type.getStringName());

      // for coverage tests
      PositionToCompartment.valueOf(type.toString());
    }
  }

  @Test
  public void testGetByString() {
    assertNotNull(PositionToCompartment.getByString("insideOfMembrane"));
    assertNull(PositionToCompartment.getByString("insideOfasdasdas"));
  }

}
