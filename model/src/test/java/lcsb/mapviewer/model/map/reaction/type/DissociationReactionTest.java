package lcsb.mapviewer.model.map.reaction.type;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;

public class DissociationReactionTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new DissociationReaction());
  }

  @Test
  public void testGetters() {
    Reaction reaction = new DissociationReaction();
    assertNotNull(reaction.getStringType());
    assertNotNull(reaction.getReactionRect());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructorWithInvalidArg() {
    Reaction reaction = new Reaction("re");
    new DissociationReaction(reaction);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructorWithInvalidArg2() {
    Reaction reaction = new Reaction("re");
    reaction.addProduct(new Product(createProtein()));
    reaction.addProduct(new Product(createProtein()));
    new DissociationReaction(reaction);
  }

  @Test
  public void testConstructor() {
    Reaction reaction = new Reaction("re");
    reaction.addProduct(new Product(createProtein()));
    reaction.addProduct(new Product(createProtein()));
    reaction.addReactant(new Reactant(createProtein()));
    DissociationReaction validReaction = new DissociationReaction(reaction);
    assertNotNull(validReaction);
  }

  @Test
  public void testCopy() {
    DissociationReaction original = new DissociationReaction();
    original.addProduct(new Product(createProtein()));
    original.addProduct(new Product(createProtein()));
    original.addReactant(new Reactant(createProtein()));
    DissociationReaction product = original.copy();
    assertNotNull(product);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(new DissociationReaction()).copy();
  }

}
