package lcsb.mapviewer.model.map.layout;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.Configuration;

public class ReferenceGenomeGeneMappingTest extends ModelTestFunctions {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    int id = 96;
    ReferenceGenome genome = new ReferenceGenome();
    String name = "test_name_97";
    String url = "http://some.url/";
    double progress = 4;

    ReferenceGenomeGeneMapping mapping = new ReferenceGenomeGeneMapping();
    mapping.setId(id);
    mapping.setReferenceGenome(genome);
    mapping.setName(name);
    mapping.setSourceUrl(url);
    mapping.setDownloadProgress(progress);

    assertEquals(genome, mapping.getReferenceGenome());
    assertEquals(name, mapping.getName());
    assertEquals(url, mapping.getSourceUrl());
    assertEquals(progress, mapping.getDownloadProgress(), Configuration.EPSILON);
    assertEquals(id, mapping.getId());
  }

}
