package lcsb.mapviewer.model.map.layout;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

public class ReferenceGenomeTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new ReferenceGenome());
  }

  @Test
  public void testAddReferenceGenomeGeneMapping() {
    ReferenceGenome genome = new ReferenceGenome();
    assertEquals(0, genome.getGeneMapping().size());
    genome.addReferenceGenomeGeneMapping(new ReferenceGenomeGeneMapping());
    assertEquals(1, genome.getGeneMapping().size());
  }

  @Test
  public void testGetters() {
    int id = 16;
    MiriamData organism = new MiriamData(MiriamType.TAXONOMY, "9606");
    ReferenceGenomeType type = ReferenceGenomeType.UCSC;
    String version = "hg19";
    double downloadProgress = 3.0;
    String sourceUrl = "google.pl";
    List<ReferenceGenomeGeneMapping> geneMapping = new ArrayList<>();

    ReferenceGenome genome = new ReferenceGenome();

    genome.setDownloadProgress(downloadProgress);
    genome.setId(id);
    genome.setOrganism(organism);
    genome.setSourceUrl(sourceUrl);
    genome.setType(type);
    genome.setVersion(version);
    genome.setGeneMapping(geneMapping);

    assertEquals(downloadProgress, genome.getDownloadProgress(), Configuration.EPSILON);
    assertEquals(id, genome.getId());
    assertEquals(organism, genome.getOrganism());
    assertEquals(sourceUrl, genome.getSourceUrl());
    assertEquals(type, genome.getType());
    assertEquals(version, genome.getVersion());
    assertEquals(geneMapping, genome.getGeneMapping());
  }

}
