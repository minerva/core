package lcsb.mapviewer.model.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;

public class OverviewImageComparatorTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEqual() throws Exception {
    OverviewImageComparator oic = new OverviewImageComparator();

    OverviewImage oi = new OverviewImage();
    OverviewImage oi2 = new OverviewImage();

    assertEquals(0, oic.compare(oi, oi2));
    assertEquals(0, oic.compare(null, null));

    oi.addLink(new OverviewImageLink());
    oi2.addLink(new OverviewImageLink());
    assertEquals(0, oic.compare(oi, oi2));
  }

  @Test
  public void testDifferent() throws Exception {
    OverviewImageComparator oic = new OverviewImageComparator();

    OverviewImage oi = new OverviewImage();

    OverviewImage oi2 = new OverviewImage();
    oi2.setFilename("x");

    assertTrue(0 != oic.compare(oi, oi2));

    oi2 = new OverviewImage();
    oi2.setHeight(1);

    assertTrue(0 != oic.compare(oi, oi2));

    oi2 = new OverviewImage();
    oi2.setWidth(2);
    ;

    assertTrue(0 != oic.compare(oi, oi2));

    oi2 = new OverviewImage();
    oi2.addLink(new OverviewModelLink());

    assertTrue(0 != oic.compare(oi, oi2));

    assertTrue(0 != oic.compare(oi, null));
    assertTrue(0 != oic.compare(null, oi2));

    assertTrue(0 != oic.compare(Mockito.spy(OverviewImage.class), oi2));

    oi = new OverviewImage();
    oi2 = new OverviewImage();
    OverviewImageLink link = new OverviewImageLink();
    link.setPolygon("1,1");
    oi.addLink(link);
    link = new OverviewImageLink();
    link.setPolygon("1,2");
    oi2.addLink(link);

    assertTrue(0 != oic.compare(oi, oi2));
  }

}
