package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class IonChannelProteinTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new IonChannelProtein());
  }

  @Test
  public void testConstructor() {
    IonChannelProtein species = new IonChannelProtein(new IonChannelProtein());
    assertNotNull(species);
  }

  @Test
  public void testCopy1() {
    IonChannelProtein species = new IonChannelProtein("id").copy();
    assertNotNull(species);
  }

  @Test(expected = NotImplementedException.class)
  public void testCopy2() {
    IonChannelProtein protein = Mockito.spy(IonChannelProtein.class);
    protein.copy();
  }

}
