package lcsb.mapviewer.model.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class OverviewLinkComparatorTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEqual() throws Exception {
    OverviewLinkComparator omlc = new OverviewLinkComparator();

    OverviewModelLink oml = new OverviewModelLink();
    OverviewModelLink oml2 = new OverviewModelLink();

    assertEquals(0, omlc.compare(oml, oml2));
    assertEquals(0, omlc.compare(null, null));
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() throws Exception {

    OverviewLinkComparator omlc = new OverviewLinkComparator();

    OverviewLink oml = Mockito.mock(OverviewLink.class);

    omlc.compare(oml, oml);
  }

  @Test
  public void testDifferent() throws Exception {
    OverviewLinkComparator omlc = new OverviewLinkComparator();

    OverviewModelLink oml = new OverviewModelLink();
    OverviewImageLink iml = new OverviewImageLink();

    assertTrue(0 != omlc.compare(oml, iml));
    assertTrue(0 != omlc.compare(oml, null));
    assertTrue(0 != omlc.compare(null, iml));

    oml = new OverviewModelLink();
    OverviewModelLink oml2 = new OverviewModelLink();
    oml2.setLinkedModel(new ModelFullIndexed(new ModelData()));

    assertTrue(0 != omlc.compare(oml, oml2));
  }

}
