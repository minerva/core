package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class DrugComparatorTest extends ModelTestFunctions {

  private DrugComparator comparator = new DrugComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    Drug drug1 = createDrug();
    Drug drug2 = createDrug();

    assertEquals(0, comparator.compare(drug1, drug1));

    assertEquals(0, comparator.compare(drug1, drug2));
    assertEquals(0, comparator.compare(drug2, drug1));
  }

  @Test
  public void testDifferent() {
    Drug drug2 = createDrug();
    assertTrue(comparator.compare(null, drug2) != 0);
    assertTrue(comparator.compare(drug2, null) != 0);
    assertTrue(comparator.compare(null, null) == 0);

    Drug drug = createDrug();
    drug.setName("n");
    assertTrue(comparator.compare(drug, drug2) != 0);

    assertTrue(comparator.compare(drug, Mockito.mock(Drug.class)) != 0);
  }

  public Drug createDrug() {
    Drug result = new Drug();
    return result;
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    Drug object = Mockito.mock(Drug.class);

    comparator.compare(object, object);
  }

}
