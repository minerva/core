package lcsb.mapviewer.model.map.kinetics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;

public class SbmlFunctionComparatorTest {
  private SbmlFunctionComparator comparator = new SbmlFunctionComparator();

  @Test
  public void testCompareEqual() {
    SbmlFunction function = createFunction();
    SbmlFunction function2 = createFunction();
    assertEquals(0, comparator.compare(function, function2));
  }

  @Test
  public void testCompareDifferentFunctionId() {
    SbmlFunction function = createFunction();
    SbmlFunction function2 = createFunction();
    function.setFunctionId("xxx");
    assertTrue("Function object have different id but comparison returned equal",
        comparator.compare(function, function2) != 0);
  }

  @Test
  public void testCompareDifferentName() {
    SbmlFunction function = createFunction();
    SbmlFunction function2 = createFunction();
    function.setName("xxx");
    assertTrue("Function object have different name but comparison returned equal",
        comparator.compare(function, function2) != 0);
  }

  @Test
  public void testCompareDifferentDefinition() throws InvalidXmlSchemaException {
    SbmlFunction function = createFunction();
    SbmlFunction function2 = createFunction();
    function.setDefinition("<lambda><bvar><ci> x2 </ci></bvar></lambda>");
    assertTrue("Function object have different name but comparison returned equal",
        comparator.compare(function, function2) != 0);
  }

  private SbmlFunction createFunction() {
    SbmlFunction result = new SbmlFunction("fun_id");
    return result;
  }

}
