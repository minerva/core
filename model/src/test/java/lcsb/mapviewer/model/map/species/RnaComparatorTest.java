package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.CodingRegion;

public class RnaComparatorTest extends ModelTestFunctions {

  private RnaComparator comparator = new RnaComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    Rna antisenseRna1 = createRna();
    Rna antisenseRna2 = createRna();

    assertEquals(0, comparator.compare(antisenseRna1, antisenseRna1));

    assertEquals(0, comparator.compare(antisenseRna1, antisenseRna2));
    assertEquals(0, comparator.compare(antisenseRna2, antisenseRna1));
  }

  @Test
  public void testDifferent() {
    Rna antisenseRna1 = createRna();
    Rna antisenseRna2 = createRna();
    antisenseRna1.getModificationResidues().get(0).setName("X");
    assertTrue(comparator.compare(antisenseRna1, antisenseRna2) != 0);
    assertTrue(comparator.compare(antisenseRna2, antisenseRna1) != 0);

    antisenseRna1 = createRna();
    antisenseRna2 = createRna();
    antisenseRna1.getModificationResidues().clear();
    assertTrue(comparator.compare(antisenseRna1, antisenseRna2) != 0);
    assertTrue(comparator.compare(antisenseRna2, antisenseRna1) != 0);

    antisenseRna1 = createRna();
    antisenseRna2 = createRna();
    assertTrue(comparator.compare(null, antisenseRna2) != 0);
    assertTrue(comparator.compare(antisenseRna2, null) != 0);
    assertTrue(comparator.compare(null, null) == 0);

    Rna rna = createRna();
    rna.setName("n");
    assertTrue(comparator.compare(rna, antisenseRna2) != 0);

    assertTrue(comparator.compare(rna, Mockito.mock(Rna.class)) != 0);
  }

  public Rna createRna() {
    Rna result = new Rna();
    result.setHypothetical(true);

    CodingRegion region1 = new CodingRegion();
    result.addCodingRegion(region1);
    region1.setIdModificationResidue("a");
    region1.setPosition(new Point2D.Double(0, 10));
    region1.setWidth(2.0);
    return result;
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    Rna object = Mockito.mock(Rna.class);

    comparator.compare(object, object);
  }

}
