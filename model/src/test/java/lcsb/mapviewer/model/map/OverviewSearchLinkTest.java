package lcsb.mapviewer.model.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class OverviewSearchLinkTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new OverviewSearchLink());
  }

  @Test
  public void testConstructor() {
    OverviewSearchLink osl = new OverviewSearchLink(new OverviewSearchLink());
    assertNotNull(osl);
  }

  @Test
  public void testGetters() {
    OverviewSearchLink osl = new OverviewSearchLink(new OverviewSearchLink());
    String query = "st";
    osl.setQuery(query);
    assertEquals(query, osl.getQuery());
  }

  @Test
  public void testCopy() {
    OverviewSearchLink degraded = new OverviewSearchLink().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(OverviewSearchLink.class).copy();
  }

}
