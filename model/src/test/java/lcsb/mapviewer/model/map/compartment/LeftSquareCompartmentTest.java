package lcsb.mapviewer.model.map.compartment;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class LeftSquareCompartmentTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new LeftSquareCompartment());
  }

  @Test
  public void testConstructor1() {
    LeftSquareCompartment compartment = new LeftSquareCompartment(new LeftSquareCompartment());
    assertNotNull(compartment);
  }

  @Test
  public void testConstructor3() {
    LeftSquareCompartment compartment = new LeftSquareCompartment("id");
    assertNotNull(compartment);
  }

  @Test
  public void testConstructor2() {
    Model model = new ModelFullIndexed(null);
    model.setWidth(1);
    model.setHeight(1);
    LeftSquareCompartment compartment = new LeftSquareCompartment(new Compartment(), model);
    assertNotNull(compartment);
  }

  @Test
  public void testCopy() {
    LeftSquareCompartment compartment = new LeftSquareCompartment().copy();
    assertNotNull(compartment);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    LeftSquareCompartment compartment = Mockito.spy(LeftSquareCompartment.class);
    compartment.copy();
  }

}
