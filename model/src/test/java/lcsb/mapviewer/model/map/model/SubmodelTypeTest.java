package lcsb.mapviewer.model.map.model;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SubmodelTypeTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (final SubmodelType type : SubmodelType.values()) {
      assertNotNull(type);

      // for coverage tests
      SubmodelType.valueOf(type.toString());
      assertNotNull(type.getCommonName());
    }
  }

}
