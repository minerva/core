package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class SimpleMoleculeComparatorTest extends ModelTestFunctions {

  private SimpleMoleculeComparator comparator = new SimpleMoleculeComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    SimpleMolecule simpleMolecule1 = createSimpleMolecule();
    SimpleMolecule simpleMolecule2 = createSimpleMolecule();

    assertEquals(0, comparator.compare(simpleMolecule1, simpleMolecule1));

    assertEquals(0, comparator.compare(simpleMolecule1, simpleMolecule2));
    assertEquals(0, comparator.compare(simpleMolecule2, simpleMolecule1));
  }

  @Test
  public void testDifferent() {
    SimpleMolecule simpleMolecule1 = createSimpleMolecule();
    SimpleMolecule simpleMolecule2 = createSimpleMolecule();
    simpleMolecule1.setHomodimer(3);
    assertTrue(comparator.compare(simpleMolecule1, simpleMolecule2) != 0);
    assertTrue(comparator.compare(simpleMolecule2, simpleMolecule1) != 0);

    simpleMolecule1 = createSimpleMolecule();
    simpleMolecule2 = createSimpleMolecule();
    simpleMolecule1.setName("as");
    assertTrue(comparator.compare(simpleMolecule1, simpleMolecule2) != 0);
    assertTrue(comparator.compare(simpleMolecule2, simpleMolecule1) != 0);

    simpleMolecule1 = createSimpleMolecule();
    simpleMolecule2 = createSimpleMolecule();
    assertTrue(comparator.compare(null, simpleMolecule2) != 0);
    assertTrue(comparator.compare(simpleMolecule2, null) != 0);
    assertTrue(comparator.compare(null, null) == 0);

    assertTrue(comparator.compare(simpleMolecule1, Mockito.mock(SimpleMolecule.class)) != 0);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    SimpleMolecule simpleMolecule1 = Mockito.mock(SimpleMolecule.class);
    comparator.compare(simpleMolecule1, simpleMolecule1);
  }

  @Test
  public void testDifferentNewFields() throws Exception {
    SimpleMolecule species1 = createSimpleMolecule();
    SimpleMolecule species2 = createSimpleMolecule();

    species1.setSmiles("some symbol");
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);
  }

  public SimpleMolecule createSimpleMolecule() {
    SimpleMolecule result = new SimpleMolecule();
    result.setHomodimer(12);

    return result;
  }

}
