package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.graphics.PolylineDataComparator;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.kinetics.SbmlKineticsComparator;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;

public class ReactionTest extends ModelTestFunctions {

  private Species species;

  @Before
  public void setUp() throws Exception {
    species = new GenericProtein("sa1");
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testContainsElement() {
    Reaction reaction = new Reaction();
    assertFalse(reaction.containsElement(species));
    Modifier modifier = new Catalysis(species);
    reaction.addModifier(modifier);
    assertTrue(reaction.containsElement(species));
    assertFalse(reaction.containsElement(new GenericProtein("id")));
  }

  @Test
  public void testConstructor() {
    Reaction reaction1 = createReaction();
    reaction1.addMiriamData(new MiriamData());
    reaction1.addSynonym("s");
    Reaction reaction2 = new Reaction(reaction1);

    assertEquals(1, reaction2.getMiriamData().size());
    assertEquals(1, reaction2.getSynonyms().size());
  }

  @Test
  public void testGetDistanceFromPoint() {
    Reaction reaction1 = new Reaction();
    Reactant reactant = new Reactant();
    PolylineData line = new PolylineData(new Point2D.Double(0.0, 0.0), new Point2D.Double(2.0, 0.0));
    reactant.setLine(line);
    reaction1.addReactant(reactant);

    assertEquals(0.0, reaction1.getDistanceFromPoint(new Point2D.Double(0.0, 0.0)), Configuration.EPSILON);
    assertEquals(0.0, reaction1.getDistanceFromPoint(new Point2D.Double(1.0, 0.0)), Configuration.EPSILON);
    assertEquals(1.0, reaction1.getDistanceFromPoint(new Point2D.Double(1.0, 1.0)), Configuration.EPSILON);
  }

  @Test
  public void testGetClosestPointTo() {
    Reaction reaction1 = new Reaction();
    Reactant reactant = new Reactant();
    PolylineData line = new PolylineData(new Point2D.Double(0.0, 0.0), new Point2D.Double(2.0, 0.0));
    reactant.setLine(line);
    reaction1.addReactant(reactant);

    assertEquals(0.0,
        reaction1.getClosestPointTo(new Point2D.Double(0.0, 0.0)).distance(new Point2D.Double(0.0, 0.0)),
        Configuration.EPSILON);
    assertEquals(0.0,
        reaction1.getClosestPointTo(new Point2D.Double(1.0, 0.0)).distance(new Point2D.Double(1.0, 0.0)),
        Configuration.EPSILON);
    assertEquals(0.0,
        reaction1.getClosestPointTo(new Point2D.Double(1.0, 1.0)).distance(new Point2D.Double(1.0, 0.0)),
        Configuration.EPSILON);
  }

  @Test
  public void testAddMiriam() {
    Reaction reaction1 = new Reaction();

    MiriamData md = new MiriamData(MiriamType.CAS, "1");
    reaction1.addMiriamData(md);

    assertEquals(1, reaction1.getMiriamData().size());

    reaction1.addMiriamData(md);
    assertEquals(1, reaction1.getMiriamData().size());
    assertEquals(1, getWarnings().size());

    List<MiriamData> list = new ArrayList<>();
    list.add(md);

    reaction1.addMiriamData(list);
    assertEquals(2, getWarnings().size());
  }

  @Test
  public void testGetters() {
    Reaction reaction1 = new Reaction();

    assertNotNull(reaction1.getStringType());
    assertNull(reaction1.getReactionRect());

    int id = 61;
    List<AbstractNode> nodes = new ArrayList<>();
    String notes = "dfp";

    ModelData data = new ModelData();
    Model model = new ModelFullIndexed(data);
    String symbol = "sybm";
    String formula = "form";
    Integer mechanicalConfidenceScore = 9908;
    Double lowerBound = 9.2;
    Double upperBound = 9.2;
    String subsystem = "ssyst";
    String geneProteinReaction = "GPR";
    List<String> synonyms = new ArrayList<>();

    reaction1.setId(id);
    reaction1.setNodes(nodes);
    reaction1.setNotes(notes);
    reaction1.setModel(model);
    assertEquals(model, reaction1.getModel());
    reaction1.setModelData(data);
    assertEquals(data, reaction1.getModelData());
    reaction1.setSymbol(symbol);
    assertEquals(symbol, reaction1.getSymbol());
    reaction1.setFormula(formula);
    assertEquals(formula, reaction1.getFormula());
    reaction1.setMechanicalConfidenceScore(mechanicalConfidenceScore);
    assertEquals(mechanicalConfidenceScore, reaction1.getMechanicalConfidenceScore());
    reaction1.setLowerBound(lowerBound);
    assertEquals(lowerBound, reaction1.getLowerBound());
    reaction1.setUpperBound(upperBound);
    assertEquals(upperBound, reaction1.getUpperBound());
    reaction1.setSubsystem(subsystem);
    assertEquals(subsystem, reaction1.getSubsystem());
    reaction1.setGeneProteinReaction(geneProteinReaction);
    assertEquals(geneProteinReaction, reaction1.getGeneProteinReaction());
    reaction1.setSynonyms(synonyms);
    assertEquals(synonyms, reaction1.getSynonyms());

    assertEquals(id, reaction1.getId());
    assertEquals(nodes, reaction1.getNodes());
    assertEquals(notes, reaction1.getNotes());
  }

  @Test
  public void testVisibilityLevel() {
    Reaction reaction1 = new Reaction();
    assertEquals("", reaction1.getVisibilityLevel());

    reaction1.setVisibilityLevel(1);
    assertEquals("1", reaction1.getVisibilityLevel());
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Reaction());
  }

  @Test
  public void testRemoveModifier() {
    Reaction reaction = new Reaction();
    Modifier modifier = new Modifier();
    reaction.addModifier(modifier);
    assertEquals(1, reaction.getModifiers().size());
    reaction.removeModifier(modifier);
    assertEquals(0, reaction.getModifiers().size());
  }

  @Test
  public void testCopy() {
    Reaction original = createReaction();
    Reaction copy = original.copy();
    assertNotNull(copy);
  }

  private Reaction createReaction() {
    Reaction result = new Reaction();
    result.addProduct(new Product());
    result.addReactant(new Reactant());
    return result;
  }

  @Test
  public void testCopyWithKinetics() {
    Reaction original = createReaction();
    original.setKinetics(new SbmlKinetics());
    Reaction copy = original.copy();
    SbmlKineticsComparator kineticsComparator = new SbmlKineticsComparator();
    assertEquals(0, kineticsComparator.compare(original.getKinetics(), copy.getKinetics()));
  }

  @Test
  public void testCopyWithCenterLine() {
    Reaction original = createReaction();
    original.setKinetics(new SbmlKinetics());
    original.setLine(new PolylineData());
    Reaction copy = original.copy();
    PolylineDataComparator lineComparator = new PolylineDataComparator();
    assertEquals(0, lineComparator.compare(original.getLine(), copy.getLine()));
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(new Reaction()).copy();
  }

}
