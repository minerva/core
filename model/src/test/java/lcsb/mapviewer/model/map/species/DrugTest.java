package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class DrugTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Drug());
  }

  @Test
  public void testConstructor1() {
    Drug degraded = new Drug("");
    assertNotNull(degraded);
  }

  @Test
  public void testGetters() {
    Drug degraded = new Drug(new Drug());
    assertNotNull(degraded.getStringType());
  }

  @Test
  public void testCopy() {
    Drug degraded = new Drug().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Drug drug = Mockito.spy(Drug.class);
    drug.copy();
  }

}
