package lcsb.mapviewer.model.map.model;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class ModelTest extends ModelTestFunctions {

  private Species species;

  private Complex complex;
  private Model model;

  @Before
  public void setUp() throws Exception {
    model = new ModelFullIndexed(null);

    species = new GenericProtein("sa1");

    complex = new Complex("sa2");

  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAddElement() {
    model.addElement(species);
    Element element = model.getElementByElementId(species.getElementId());
    assertNotNull(element);
    element = model.getElementByElementId(species.getElementId() + "blbla");
    assertNull(element);
  }

  @Test
  public void testGetElements() {
    Set<Element> elements = model.getElements();
    assertEquals(0, elements.size());
    model.addElement(species);
    assertEquals(1, elements.size());

    Element element = model.getElementByElementId(species.getElementId());
    assertNotNull(element);
    element = model.getElementByElementId(species.getElementId() + "blbla");
    assertNull(element);

    element = model.getElementByElementId(complex.getElementId());
    assertNull(element);

    model.addElement(complex);
    assertEquals(2, elements.size());
    element = model.getElementByElementId(complex.getElementId());
    assertNotNull(element);
  }

  @Test
  public void testAddReaction() {
    Reaction reaction = new Reaction("re");
    model.addReaction(reaction);
    assertNotNull(model.getReactions());
    assertEquals(1, model.getReactions().size());
  }

  @Test
  public void testAddLayer() {
    Layer layer = createLayer();
    Set<Layer> list = model.getLayers();
    assertEquals(0, list.size());
    model.addLayer(layer);
    list = model.getLayers();
    assertEquals(1, list.size());
  }

  @Test
  public void testSorting() throws Exception {
    Model model = new ModelFullIndexed(null);

    Species species = new GenericProtein("2");
    species.setWidth(100);
    species.setHeight(100);
    model.addElement(species);

    species = new GenericProtein("3");
    species.setWidth(10);
    species.setHeight(10);
    model.addElement(species);

    species = new GenericProtein("4");
    species.setWidth(200);
    species.setHeight(100);
    model.addElement(species);

    Compartment compartment = new Compartment("5");
    compartment.setWidth(10);
    compartment.setHeight(20);
    model.addElement(compartment);

    compartment = new Compartment("6");
    compartment.setWidth(100);
    compartment.setHeight(200);
    model.addElement(compartment);

    compartment = new Compartment("7");
    compartment.setWidth(20);
    compartment.setHeight(30);
    model.addElement(compartment);

    List<Element> sortedElements = model.getElementsSortedBySize();
    if (sortedElements.size() > 0) {
      double last = (sortedElements.get(0)).getHeight() * (sortedElements.get(0)).getWidth();
      for (final Element a : sortedElements) {
        assertTrue(last >= a.getHeight() * a.getWidth());
      }
    }
  }
}
