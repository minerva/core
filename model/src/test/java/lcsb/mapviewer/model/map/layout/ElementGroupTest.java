package lcsb.mapviewer.model.map.layout;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class ElementGroupTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new ElementGroup());
  }

  @Test
  public void testGetters() {
    ElementGroup group = new ElementGroup();
    String idGroup = "str";
    List<Element> elements = new ArrayList<>();

    group.setIdGroup(idGroup);
    group.setElements(elements);

    assertEquals(idGroup, group.getIdGroup());
    assertEquals(elements, group.getElements());
  }

  @Test
  public void testAddElement() {
    ElementGroup group = new ElementGroup();
    group.addElement(new GenericProtein("id"));
    assertEquals(1, group.getElements().size());
  }
}
