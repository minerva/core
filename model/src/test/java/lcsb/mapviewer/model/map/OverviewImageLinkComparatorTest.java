package lcsb.mapviewer.model.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;

public class OverviewImageLinkComparatorTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEqual() throws Exception {
    OverviewImageLinkComparator oic = new OverviewImageLinkComparator();

    OverviewImageLink oi = new OverviewImageLink();
    OverviewImageLink oi2 = new OverviewImageLink();

    assertEquals(0, oic.compare(oi, oi2));
    assertEquals(0, oic.compare(null, null));

    oi2.setLinkedOverviewImage(new OverviewImage());
    oi.setLinkedOverviewImage(new OverviewImage());
    assertEquals(0, oic.compare(oi, oi2));
  }

  @Test
  public void testDifferent() throws Exception {
    OverviewImageLinkComparator oic = new OverviewImageLinkComparator();

    OverviewImageLink oi = new OverviewImageLink();

    OverviewImageLink oi2 = new OverviewImageLink();
    oi2.setLinkedOverviewImage(new OverviewImage());

    assertTrue(0 != oic.compare(oi, oi2));
    assertTrue(0 != oic.compare(oi2, oi));

    oi2 = new OverviewImageLink();
    oi2.setLinkedOverviewImage(new OverviewImage());

    assertTrue(0 != oic.compare(oi, oi2));
    assertTrue(0 != oic.compare(oi2, oi));

    oi2 = new OverviewImageLink();
    oi2.setPolygon("AS");

    assertTrue(0 != oic.compare(oi, oi2));
    assertTrue(0 != oic.compare(oi, null));
    assertTrue(0 != oic.compare(null, oi2));
    assertTrue(0 != oic.compare(Mockito.spy(OverviewImageLink.class), oi2));
  }

  @Test
  public void testDifferentPolygon() throws Exception {
    OverviewImageLinkComparator comparator = new OverviewImageLinkComparator();

    OverviewImageLink iml1 = new OverviewImageLink();
    OverviewImageLink iml2 = new OverviewImageLink();
    iml2.setPolygon("1,2");

    assertTrue(0 != comparator.compare(iml1, iml2));
    assertTrue(0 != comparator.compare(iml2, iml1));
  }

}
