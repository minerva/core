package lcsb.mapviewer.model.map.model;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.awt.Color;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ElementSubmodelConnectionComparatorTest extends ModelTestFunctions {

  private final ElementSubmodelConnectionComparator comparator = new ElementSubmodelConnectionComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() throws Exception {
    ElementSubmodelConnection connectionA = createConnection();
    ElementSubmodelConnection connectionB = createConnection();

    assertEquals(0, comparator.compare(new ElementSubmodelConnection(), new ElementSubmodelConnection()));
    assertEquals(0, comparator.compare(connectionA, connectionB));
    assertEquals(0, comparator.compare(connectionA, connectionA));
    assertEquals(0, comparator.compare(null, null));
  }

  @Test
  public void testDifferent() throws Exception {
    ElementSubmodelConnection connectionA = createConnection();
    ElementSubmodelConnection connectionB = createConnection();

    assertTrue(comparator.compare(null, connectionB) != 0);
    assertTrue(comparator.compare(connectionA, null) != 0);

    connectionA.setFromElement(null);
    assertTrue(comparator.compare(connectionA, connectionB) != 0);
    assertTrue(comparator.compare(connectionB, connectionA) != 0);

    connectionA = createConnection();
    connectionB = createConnection();

    connectionA.getFromElement().setWidth(1234567);

    assertTrue(comparator.compare(connectionA, connectionB) != 0);
    assertTrue(comparator.compare(connectionB, connectionA) != 0);

    connectionA = createConnection();
    connectionB = createConnection();

    connectionA.setToElement(null);
    assertTrue(comparator.compare(connectionA, connectionB) != 0);
    assertTrue(comparator.compare(connectionB, connectionA) != 0);

    connectionA = createConnection();
    connectionB = createConnection();

    connectionA.getToElement().setWidth(1234567);

    assertTrue(comparator.compare(connectionA, connectionB) != 0);
    assertTrue(comparator.compare(connectionB, connectionA) != 0);

    assertTrue(comparator.compare(connectionB, Mockito.spy(ElementSubmodelConnection.class)) != 0);
  }

  private Model getModel() {
    Model model = new ModelFullIndexed(null);

    model.setNotes("Some description");
    GenericProtein protein = new GenericProtein("A");
    protein.setName("ad");
    model.addElement(protein);

    GenericProtein protein2 = new GenericProtein("a_id");
    protein2.setName("ad");
    model.addElement(protein2);

    model.addElement(new Compartment("default"));

    Layer layer = createLayer(false);
    model.addLayer(layer);

    model.addReaction(new Reaction("re"));
    return model;
  }

  private ElementSubmodelConnection createConnection() {
    ElementSubmodelConnection result = new ElementSubmodelConnection(getModel(), SubmodelType.DOWNSTREAM_TARGETS);
    result.setName("name A");
    result.setFromElement(createElement());
    result.setToElement(createElement());
    return result;
  }

  private Element createElement() {
    Element protein = new GenericProtein("protein_id");
    protein.setFillColor(Color.BLACK);
    return protein;
  }

}
