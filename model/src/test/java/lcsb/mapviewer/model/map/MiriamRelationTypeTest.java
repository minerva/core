package lcsb.mapviewer.model.map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MiriamRelationTypeTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (final MiriamRelationType type : MiriamRelationType.values()) {
      assertNotNull(type);

      // for coverage tests
      MiriamRelationType.valueOf(type.toString());
      assertNotNull(type.getStringRepresentation());

    }
  }

  @Test
  public void testGetTypeByString() {
    assertNull(MiriamRelationType.getTypeByStringRepresentation("DSfsdfs"));
    assertNotNull(MiriamRelationType.getTypeByStringRepresentation("bqbiol:occures"));
  }

}
