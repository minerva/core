package lcsb.mapviewer.model.map.reaction.type;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;

public class ReducedPhysicalStimulationReactionTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new ReducedPhysicalStimulationReaction());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructorWithInvalidArg() {
    Reaction reaction = new Reaction("re");
    new ReducedPhysicalStimulationReaction(reaction);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructorWithInvalidArg2() {
    Reaction reaction = new Reaction("re");
    reaction.addProduct(new Product(createProtein()));
    new ReducedPhysicalStimulationReaction(reaction);
  }

  @Test
  public void testConstructor() {
    Reaction reaction = new Reaction("re");
    reaction.addProduct(new Product(createProtein()));
    reaction.addReactant(new Reactant(createProtein()));
    ReducedPhysicalStimulationReaction validReaction = new ReducedPhysicalStimulationReaction(reaction);
    assertNotNull(validReaction);
  }

  @Test
  public void testCopy() {
    ReducedPhysicalStimulationReaction original = new ReducedPhysicalStimulationReaction();
    original.addProduct(new Product(createProtein()));
    original.addReactant(new Reactant(createProtein()));
    original.addReactant(new Reactant(createProtein()));
    ReducedPhysicalStimulationReaction product = original.copy();
    assertNotNull(product);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(new ReducedPhysicalStimulationReaction()).copy();
  }

  @Test
  public void testGetters() {
    ReducedPhysicalStimulationReaction original = new ReducedPhysicalStimulationReaction();
    assertNull(original.getReactionRect());
    assertNotNull(original.getStringType());
  }
}
