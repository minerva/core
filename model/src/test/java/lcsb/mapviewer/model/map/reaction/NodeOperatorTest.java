package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.graphics.PolylineData;

public class NodeOperatorTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor() {
    AndOperator operator = new AndOperator();
    NodeOperator operator2 = new AndOperator();
    NodeOperator operator3 = new AndOperator();
    operator.addInput(operator2);
    operator.addOutput(operator3);
    operator.setLine(new PolylineData());

    NodeOperator result = new AndOperator(operator);
    assertEquals(1, result.getInputs().size());
    assertEquals(1, result.getOutputs().size());
  }

  @Test
  public void testAddInput() {
    NodeOperator operator = new AndOperator();
    NodeOperator operator2 = new AndOperator();
    operator.addInput(operator2);
    assertEquals(1, operator.getInputs().size());
  }

  @Test
  public void testAddInputs() {
    NodeOperator operator = new AndOperator();
    NodeOperator operator2 = new AndOperator();
    List<NodeOperator> list = new ArrayList<>();
    list.add(operator2);
    operator.addInputs(list);
    assertEquals(1, operator.getInputs().size());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testAddInput2() {
    NodeOperator operator = new AndOperator();
    NodeOperator operator2 = new AndOperator();
    operator.addInput(operator2);
    operator.addInput(operator2);
  }

  @Test
  public void testIsReactantOperator() {
    NodeOperator operator = new AndOperator();
    assertFalse(operator.isReactantOperator());

    AndOperator op1 = new AndOperator();
    operator.addInput(op1);
    assertFalse(operator.isReactantOperator());

    op1.addInput(new Reactant());
    assertTrue(operator.isReactantOperator());
  }

  @Test
  public void testAddOutputs() {
    NodeOperator operator = new AndOperator();
    NodeOperator operator2 = new AndOperator();
    List<NodeOperator> list = new ArrayList<>();
    list.add(operator2);
    operator.addOutputs(list);
    assertEquals(1, operator.getOutputs().size());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testAddOutput2() {
    NodeOperator operator = new AndOperator();
    NodeOperator operator2 = new AndOperator();
    operator.addOutput(operator2);
    operator.addOutput(operator2);
  }

  @Test
  public void testIsProductOperator() {
    NodeOperator operator = new AndOperator();
    assertFalse(operator.isReactantOperator());

    AndOperator op1 = new AndOperator();
    operator.addInput(op1);
    assertFalse(operator.isProductOperator());

    op1.addInput(new Reactant());
    assertFalse(operator.isProductOperator());

    AndOperator op2 = new AndOperator();
    operator.addOutput(op2);
    assertFalse(operator.isProductOperator());

    op2.addOutput(new Product());
    assertTrue(operator.isProductOperator());
  }

  @Test
  public void testGetters() {
    NodeOperator operator = new AndOperator();
    List<AbstractNode> inputs = new ArrayList<>();
    List<AbstractNode> outputs = new ArrayList<>();
    operator.setInputs(inputs);
    operator.setOutputs(outputs);
    assertEquals(inputs, operator.getInputs());
    assertEquals(outputs, operator.getOutputs());
  }

  @Test
  public void testIsModifierOperator() {
    NodeOperator operator = new AndOperator();
    assertFalse(operator.isReactantOperator());

    AndOperator op1 = new AndOperator();
    operator.addInput(op1);
    assertFalse(operator.isModifierOperator());

    op1.addInput(new Modifier());
    assertTrue(operator.isModifierOperator());
  }

}
