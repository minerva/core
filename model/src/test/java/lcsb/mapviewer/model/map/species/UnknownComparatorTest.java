package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class UnknownComparatorTest extends ModelTestFunctions {

  private UnknownComparator comparator = new UnknownComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    Unknown antisenseRna1 = createUnknown();
    Unknown antisenseRna2 = createUnknown();

    assertEquals(0, comparator.compare(antisenseRna1, antisenseRna1));

    assertEquals(0, comparator.compare(antisenseRna1, antisenseRna2));
    assertEquals(0, comparator.compare(antisenseRna2, antisenseRna1));
  }

  @Test
  public void testDifferent() {
    Unknown unknown2 = createUnknown();
    assertTrue(comparator.compare(null, unknown2) != 0);
    assertTrue(comparator.compare(unknown2, null) != 0);
    assertTrue(comparator.compare(null, null) == 0);

    Unknown unknown = createUnknown();
    unknown.setName("n");
    assertTrue(comparator.compare(unknown, unknown2) != 0);

    assertTrue(comparator.compare(unknown, Mockito.mock(Unknown.class)) != 0);
  }

  public Unknown createUnknown() {
    Unknown result = new Unknown();
    return result;
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    Unknown object = Mockito.mock(Unknown.class);

    comparator.compare(object, object);
  }

}
