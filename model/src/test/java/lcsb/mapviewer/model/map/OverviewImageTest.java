package lcsb.mapviewer.model.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class OverviewImageTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new OverviewImage());
  }

  @Test
  public void testCopy() {
    OverviewImage original = new OverviewImage();
    original.addLink(new OverviewImageLink());
    OverviewImage degraded = original.copy();
    assertNotNull(degraded);
  }

  @Test
  public void testSetters() {
    OverviewImage original = new OverviewImage();
    List<OverviewLink> links = new ArrayList<>();
    original.setLinks(links);

    assertEquals(links, original.getLinks());
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(OverviewImage.class).copy();
  }

}
