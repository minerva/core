package lcsb.mapviewer.model.map.layout.graphics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.awt.Color;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class LayerOvalTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(createOval());
  }

  @Test
  public void testConstructor1() {
    LayerOval layerOval = createOval();

    LayerOval copy = new LayerOval(layerOval);

    assertNotNull(copy);
    assertEquals(0, new LayerOvalComparator().compare(layerOval, copy));
  }

  private LayerOval createOval() {
    LayerOval result = new LayerOval();
    result.setX(10.0);
    result.setY(20.0);
    result.setZ(14);
    result.setColor(Color.YELLOW);
    result.setWidth(33.0);
    result.setHeight(24.0);
    result.setBorderColor(Color.GREEN);
    return result;
  }

  @Test
  public void testCopy() {
    LayerOval copy = createOval().copy();

    assertNotNull(copy);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(LayerOval.class).copy();
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidX() {
    LayerOval oval = createOval();
    oval.setX("a1.6");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidY() {
    LayerOval oval = createOval();
    oval.setY("a1.6");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidWidth() {
    LayerOval oval = createOval();
    oval.setWidth("a1.6");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidHeight() {
    LayerOval oval = createOval();
    oval.setHeight("a1.6");
  }

  @Test
  public void testGetters() {
    LayerOval oval = createOval();

    String stringY = "1.2";
    Double y = 1.2;

    String stringX = "2.2";
    Double x = 2.2;

    String widthParam = "10.2";
    Double width = 10.2;

    String heightParam = "72.2";
    Double height = 72.2;

    int id = 52;
    Color color = Color.BLACK;

    oval.setY(stringY);
    assertEquals(y, oval.getY(), Configuration.EPSILON);
    oval.setY((Double) null);
    assertNull(oval.getY());
    oval.setY(y);
    assertEquals(y, oval.getY(), Configuration.EPSILON);

    oval.setX(stringX);
    assertEquals(x, oval.getX(), Configuration.EPSILON);
    oval.setX((Double) null);
    assertNull(oval.getX());
    oval.setX(x);
    assertEquals(x, oval.getX(), Configuration.EPSILON);

    oval.setWidth(widthParam);
    assertEquals(width, oval.getWidth(), Configuration.EPSILON);
    oval.setWidth((Double) null);
    assertNull(oval.getWidth());
    oval.setWidth(width);
    assertEquals(width, oval.getWidth(), Configuration.EPSILON);

    oval.setHeight(heightParam);
    assertEquals(height, oval.getHeight(), Configuration.EPSILON);
    oval.setHeight((Double) null);
    assertNull(oval.getHeight());
    oval.setHeight(height);
    assertEquals(height, oval.getHeight(), Configuration.EPSILON);

    oval.setId(id);
    assertEquals(id, oval.getId());
    oval.setColor(color);
    assertEquals(color, oval.getColor());
  }

}
