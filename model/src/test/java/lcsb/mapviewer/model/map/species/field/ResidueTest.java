package lcsb.mapviewer.model.map.species.field;

import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import java.awt.Color;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ResidueTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    final Residue mr = new Residue();
    final Species species = new GenericProtein("id");
    final int id = 94;

    mr.setSpecies(species);
    mr.setId(id);

    assertEquals(species, mr.getSpecies());
    assertEquals(id, mr.getId());
  }

  @Test
  public void testCopy() {
    final Residue mr = new Residue();
    mr.setBorderColor(Color.PINK);
    mr.setFillColor(Color.YELLOW);
    final Residue mr2 = mr.copy();
    assertNotNull(mr2);
    assertNotNull(mr2.getBorderColor());
    assertNotNull(mr2.getFillColor());
  }

}
