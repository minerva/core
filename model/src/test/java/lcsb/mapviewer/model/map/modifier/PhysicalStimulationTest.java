package lcsb.mapviewer.model.map.modifier;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class PhysicalStimulationTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new PhysicalStimulation());
  }

  @Test
  public void testConstructor() {
    PhysicalStimulation modifier = new PhysicalStimulation(new GenericProtein("unk_id"));
    assertNotNull(modifier);
  }

  @Test
  public void testConstructor2() {
    PhysicalStimulation modifier = new PhysicalStimulation(new GenericProtein("unk_id"));
    modifier.setLine(new PolylineData());
    PhysicalStimulation modifier2 = new PhysicalStimulation(modifier);
    assertNotNull(modifier2);
  }

  @Test(expected = NotImplementedException.class)
  public void testCopyInvalid() {
    Mockito.mock(PhysicalStimulation.class, Mockito.CALLS_REAL_METHODS).copy();
  }

  @Test
  public void testCopy() {
    PhysicalStimulation modifier = new PhysicalStimulation(new GenericProtein("unk_id"));
    modifier.setLine(new PolylineData());
    PhysicalStimulation modifier2 = modifier.copy();
    assertNotNull(modifier2);
  }

}
