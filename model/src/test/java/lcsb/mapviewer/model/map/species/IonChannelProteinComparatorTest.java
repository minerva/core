package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class IonChannelProteinComparatorTest extends ModelTestFunctions {

  private IonChannelProteinComparator comparator = new IonChannelProteinComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    IonChannelProtein drug1 = createIonChannelProtein();
    IonChannelProtein drug2 = createIonChannelProtein();

    assertEquals(0, comparator.compare(drug1, drug1));

    assertEquals(0, comparator.compare(drug1, drug2));
    assertEquals(0, comparator.compare(drug2, drug1));
  }

  @Test
  public void testDifferent() {
    IonChannelProtein drug2 = createIonChannelProtein();
    assertTrue(comparator.compare(null, drug2) != 0);
    assertTrue(comparator.compare(drug2, null) != 0);
    assertTrue(comparator.compare(null, null) == 0);

    IonChannelProtein drug = createIonChannelProtein();
    drug.setName("n");
    assertTrue(comparator.compare(drug, drug2) != 0);

    assertTrue(comparator.compare(drug, Mockito.mock(IonChannelProtein.class)) != 0);
  }

  public IonChannelProtein createIonChannelProtein() {
    IonChannelProtein result = new IonChannelProtein();
    return result;
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    IonChannelProtein object = Mockito.mock(IonChannelProtein.class);

    comparator.compare(object, object);
  }

}
