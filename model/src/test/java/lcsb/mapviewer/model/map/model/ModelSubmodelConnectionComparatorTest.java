package lcsb.mapviewer.model.map.model;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Species;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ModelSubmodelConnectionComparatorTest extends ModelTestFunctions {

  private final ModelSubmodelConnectionComparator comparator = new ModelSubmodelConnectionComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() throws Exception {
    ModelSubmodelConnection connectionA = createConnection();
    ModelSubmodelConnection connectionB = createConnection();

    assertEquals(0, comparator.compare(new ModelSubmodelConnection(), new ModelSubmodelConnection()));
    assertEquals(0, comparator.compare(connectionA, connectionB));
    assertEquals(0, comparator.compare(connectionA, connectionA));
    assertEquals(0, comparator.compare(null, null));
  }

  @Test
  public void testDifferent() throws Exception {
    ModelSubmodelConnection connectionA = createConnection();
    ModelSubmodelConnection connectionB = createConnection();

    assertTrue(comparator.compare(null, connectionB) != 0);
    assertTrue(comparator.compare(connectionA, null) != 0);

    connectionA.getParentModel().setNotes("QQWECS");
    assertTrue(comparator.compare(connectionA, connectionB) != 0);
    assertTrue(comparator.compare(connectionB, connectionA) != 0);

    connectionA = createConnection();
    connectionB = createConnection();

    connectionA.setParentModel((ModelData) null);

    assertTrue(comparator.compare(connectionA, connectionB) != 0);
    assertTrue(comparator.compare(connectionB, connectionA) != 0);

    connectionA = createConnection();
    connectionB = createConnection();

    connectionA.setType(SubmodelType.UNKNOWN);

    assertTrue(comparator.compare(connectionA, connectionB) != 0);
    assertTrue(comparator.compare(connectionB, connectionA) != 0);

    assertTrue(comparator.compare(connectionB, Mockito.spy(ModelSubmodelConnection.class)) != 0);
  }

  private Model getModel() {
    Model model = new ModelFullIndexed(null);

    model.setNotes("Some description");

    Species species = new GenericProtein("a_id");
    species.setName("ad");
    model.addElement(species);

    model.addElement(new Compartment("default"));

    Layer layer = createLayer(false);
    model.addLayer(layer);

    model.addReaction(new Reaction("re"));
    return model;
  }

  private ModelSubmodelConnection createConnection() {
    ModelSubmodelConnection result = new ModelSubmodelConnection(getModel(), SubmodelType.DOWNSTREAM_TARGETS);
    result.setName("name A");
    result.setParentModel(getModel());
    return result;
  }

}
