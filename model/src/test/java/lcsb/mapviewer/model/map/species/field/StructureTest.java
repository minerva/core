package lcsb.mapviewer.model.map.species.field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;

public class StructureTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testGetters() {
    Structure s = new Structure();
    UniprotRecord ur = new UniprotRecord();
    ur.setId(100);
    ur.setUniprotId("P29373");

    int id = 101;
    s.setUniprot(ur);
    s.setId(id);

    ur.setStructures(new HashSet<Structure>() {
      private static final long serialVersionUID = 1L;
      {
        add(s);
      }
    });

    assertEquals(ur, s.getUniprot());
    assertEquals(id, s.getId());
    assertEquals(s, ur.getStructures().iterator().next());
  }

  @Test
  public void testCopy() {
    Structure s = new Structure().copy();
    assertNotNull(s);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Structure s = Mockito.spy(Structure.class);
    s.copy();
  }

}
