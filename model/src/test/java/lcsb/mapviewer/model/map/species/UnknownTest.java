package lcsb.mapviewer.model.map.species;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class UnknownTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Unknown());
  }

  @Test
  public void testConstructor1() {
    final Unknown unknown = new Unknown(new Unknown());
    assertNotNull(unknown);
  }

  @Test
  public void testDefaultBorderColor() {
    final Unknown unknown = new Unknown("");
    assertEquals(0, unknown.getBorderColor().getAlpha());

  }

  @Test
  public void testGetters() {
    final Unknown unknown = new Unknown("id");
    assertNotNull(unknown.getStringType());
  }

  @Test
  public void testCopy() {
    final Unknown unknown = new Unknown().copy();
    assertNotNull(unknown);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    final Unknown object = Mockito.spy(Unknown.class);
    object.copy();
  }

}
