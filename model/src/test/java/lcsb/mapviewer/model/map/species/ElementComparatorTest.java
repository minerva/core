package lcsb.mapviewer.model.map.species;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamRelationType;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.ElementSubmodelConnection;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.field.PositionToCompartment;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.awt.Color;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ElementComparatorTest extends ModelTestFunctions {

  private final ElementComparator comparator = new ElementComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test(expected = NotImplementedException.class)
  public void testCompareException() {
    comparator.compare(Mockito.mock(Element.class), Mockito.mock(Element.class));
  }

  @Test
  public void testDifferent() throws Exception {
    assertTrue(comparator.compare(new GenericProtein("id2"), new GenericProtein("id1")) != 0);

    assertTrue(comparator.compare(null, new GenericProtein()) != 0);
    assertTrue(comparator.compare(new GenericProtein(), null) != 0);
    assertTrue(comparator.compare(new GenericProtein(), new Complex()) != 0);

    Species sa1 = new GenericProtein("id2");
    Species sa2 = new GenericProtein("id2");
    sa2.setX(2);
    assertTrue(comparator.compare(sa1, sa2) != 0);

    sa1 = new GenericProtein("id2");
    sa2 = new GenericProtein("id2");
    sa2.setY(2);
    assertTrue(comparator.compare(sa1, sa2) != 0);

    sa1 = new GenericProtein("id2");
    sa2 = new GenericProtein("id2");
    sa2.setHeight(2);
    assertTrue(comparator.compare(sa1, sa2) != 0);

    sa1 = new GenericProtein("id2");
    sa2 = new GenericProtein("id2");
    sa2.setFontSize(2);
    assertTrue(comparator.compare(sa1, sa2) != 0);

    sa1 = new GenericProtein("id2");
    sa2 = new GenericProtein("id2");
    sa2.setFillColor(Color.BLUE);
    assertTrue(comparator.compare(sa1, sa2) != 0);

    sa1 = new GenericProtein("id2");
    sa2 = new GenericProtein("id2");
    sa2.setVisibilityLevel(6);
    assertTrue(comparator.compare(sa1, sa2) != 0);
  }

  @Test
  public void testCompareSubmodel() throws Exception {
    Element element1 = createElement();
    Element element2 = createElement();
    assertEquals(0, comparator.compare(element1, element2));

    element1.setSubmodel(null);
    assertTrue(comparator.compare(element1, element2) != 0);
    assertTrue(comparator.compare(element2, element1) != 0);

    element1 = createElement();
    element2 = createElement();

    element1.getSubmodel().setName("Na");

    assertTrue(comparator.compare(element1, element2) != 0);
    assertTrue(comparator.compare(element2, element1) != 0);
  }

  @Test
  public void testDifferentBorderColor() throws Exception {
    Element element1 = createElement();
    Element element2 = createElement();
    element1.setBorderColor(Color.YELLOW);

    assertTrue(comparator.compare(element1, element2) != 0);
    assertTrue(comparator.compare(element2, element1) != 0);
  }

  @Test
  public void testDifferentFontColor() throws Exception {
    Element element1 = createElement();
    Element element2 = createElement();
    element1.setFontColor(Color.YELLOW);

    assertTrue(comparator.compare(element1, element2) != 0);
    assertTrue(comparator.compare(element2, element1) != 0);
  }

  private Element createElement() {
    Element result = new GenericProtein("id2");
    ElementSubmodelConnection submodel = new ElementSubmodelConnection(getModel(), SubmodelType.DOWNSTREAM_TARGETS);
    result.setSubmodel(submodel);
    return result;
  }

  private Model getModel() {
    Model model = new ModelFullIndexed(null);

    model.setNotes("Some description");
    GenericProtein protein = new GenericProtein("A");
    protein.setName("ad");
    model.addElement(protein);

    Protein protein2 = new GenericProtein("a_id");
    protein2.setName("ad");
    model.addElement(protein2);

    model.addElement(new Compartment("default"));

    Layer layer = createLayer(false);
    model.addLayer(layer);

    model.addReaction(new Reaction("re"));
    return model;
  }

  @Test(expected = NotImplementedException.class)
  public void testException() {
    Element el = Mockito.mock(Element.class);
    comparator.compare(el, el);
  }

  @Test
  public void testInternalCompare() {
    SimpleMolecule species1 = createSimpleMolecule();
    assertTrue(comparator.internalCompare(species1, null) != 0);
    assertTrue(comparator.internalCompare(null, species1) != 0);

    assertEquals(0, comparator.internalCompare(null, null));
  }

  @Test
  public void testDifferent2() {
    SimpleMolecule species1 = createSimpleMolecule();
    SimpleMolecule species2 = createSimpleMolecule();

    species1.setCharge(99);
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();
    species1.setHomodimer(233);
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();
    species1.setFormula("a");
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();
    species1.setElementId("");
    species1.setElementId("ASD");
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();
    species1.setNotes("ASD");
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();
    species1.setPositionToCompartment(PositionToCompartment.INSIDE);
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();
    species1.getMiriamData().clear();
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();
    species1.getMiriamData().iterator().next().setRelationType(MiriamRelationType.BQ_BIOL_HAS_PART);
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();
    species1.addMiriamData(new MiriamData());
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    assertTrue(comparator.compare(species1, null) != 0);
    assertTrue(comparator.compare(null, species1) != 0);
  }

  @Test
  public void testDifferentNewFields() throws Exception {
    SimpleMolecule species1 = createSimpleMolecule();
    SimpleMolecule species2 = createSimpleMolecule();

    species1.setSymbol("some symbol");
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();

    species1.setFullName("some symbol");
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();

    species1.getSynonyms().add("asd");
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();

    species1.getFormerSymbols().add("asd");
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);
  }

  public SimpleMolecule createSimpleMolecule() {
    SimpleMolecule result = new SimpleMolecule("id");
    result.setHomodimer(12);
    result.setName("id");
    result.setInitialAmount(12.0);
    result.setCharge(13);
    result.setInitialConcentration(14.0);
    result.setOnlySubstanceUnits(true);
    result.setPositionToCompartment(PositionToCompartment.TRANSMEMBRANE);
    result.setNotes("id");
    MiriamData md = new MiriamData(MiriamType.PUBMED, "c");
    result.addMiriamData(md);
    return result;
  }

  @Test
  public void testDifferentNewReconFields() throws Exception {
    SimpleMolecule element1 = createSimpleMolecule();

    SimpleMolecule element2 = createSimpleMolecule();
    element2.setAbbreviation("ABRR");

    assertTrue(comparator.compare(element1, element2) != 0);
  }

  @Test
  public void testCompareZIndex() throws Exception {
    SimpleMolecule species1 = createSimpleMolecule();
    SimpleMolecule species2 = createSimpleMolecule();

    species1.setZ(12);
    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();

    species1.setZ(12);
    species2.setZ(13);

    assertTrue(comparator.compare(species1, species2) != 0);
    assertTrue(comparator.compare(species2, species1) != 0);

    species1 = createSimpleMolecule();
    species2 = createSimpleMolecule();

    species1.setZ(12);
    species2.setZ(12);

    assertEquals(0, comparator.compare(species1, species2));
    assertEquals(0, comparator.compare(species2, species1));
  }

}
