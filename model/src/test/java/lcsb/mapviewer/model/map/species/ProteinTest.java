package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.StructuralState;

public class ProteinTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new GenericProtein());
  }

  @Test
  public void testConstructor1() {
    GenericProtein protein = new GenericProtein();
    protein.addStructuralState(new StructuralState());
    List<ModificationResidue> residues = new ArrayList<>();
    residues.add(new Residue());

    protein.setModificationResidues(residues);
    Protein protein2 = new GenericProtein(protein);
    assertNotNull(protein2);
  }

  @Test
  public void testGetters() {
    GenericProtein protein = new GenericProtein("id");
    assertNotNull(protein.getStringType());
  }

  @Test
  public void testCopy() {
    GenericProtein protein = new GenericProtein().copy();
    assertNotNull(protein);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    GenericProtein mock = Mockito.spy(GenericProtein.class);
    mock.copy();
  }

}
