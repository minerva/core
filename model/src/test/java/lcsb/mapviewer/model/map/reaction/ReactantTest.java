package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;

public class ReactantTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Reactant());
  }

  @Test
  public void testConstructor() {
    Reactant original = new Reactant();
    original.setLine(new PolylineData());
    Reactant reactant = new Reactant(original);
    assertNotNull(reactant);
  }

  @Test
  public void testCopy() {
    Reactant original = new Reactant();
    original.setLine(new PolylineData());
    Reactant reactant = original.copy();
    assertNotNull(reactant);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.mock(Reactant.class, Mockito.CALLS_REAL_METHODS).copy();
  }

}
