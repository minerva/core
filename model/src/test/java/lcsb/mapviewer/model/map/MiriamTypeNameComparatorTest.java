package lcsb.mapviewer.model.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.ModelTestFunctions;

public class MiriamTypeNameComparatorTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testCompareNulls() {
    MiriamTypeNameComparator comparator = new MiriamTypeNameComparator();
    assertEquals(0, comparator.compare(null, null));
    assertTrue(comparator.compare(null, MiriamType.CAS) != 0);
    assertTrue(comparator.compare(MiriamType.CAS, null) != 0);
  }

  @Test
  public void testCompare() {
    MiriamTypeNameComparator comparator = new MiriamTypeNameComparator();
    assertTrue(comparator.compare(MiriamType.CHEBI, MiriamType.CAS) < 0);
    assertTrue(comparator.compare(MiriamType.CAS, MiriamType.CHEBI) > 0);
    assertTrue(comparator.compare(MiriamType.CAS, MiriamType.CAS) == 0);
  }

}
