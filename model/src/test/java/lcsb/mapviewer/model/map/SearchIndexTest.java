package lcsb.mapviewer.model.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;

public class SearchIndexTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new SearchIndex());
  }

  @Test
  public void testConstructor1() {
    SearchIndex index = new SearchIndex("str", 2);
    assertEquals("str", index.getValue());
    assertEquals((Integer) 2, index.getWeight());
  }

  @Test
  public void testConstructor2() {
    SearchIndex index = new SearchIndex("str");
    assertEquals("str", index.getValue());
    assertEquals((Integer) 1, index.getWeight());
  }

  @Test
  public void testConstructor3() {
    SearchIndex index = new SearchIndex("str");
    SearchIndex index2 = new SearchIndex(index);
    assertNotNull(index2);
  }

  @Test
  public void testCopy() {
    SearchIndex antisenseRna = new SearchIndex().copy();
    assertNotNull(antisenseRna);
  }

  @Test(expected = NotImplementedException.class)
  public void testCopyInvalid() {
    Mockito.spy(SearchIndex.class).copy();
  }

  @Test
  public void testGetters() {
    int id = 54;
    int weight = 8;
    String value = "val5";
    Element element = new Gene("gene_id");

    SearchIndex index = new SearchIndex();
    index.setId(id);
    assertEquals(id, index.getId());

    index.setValue(value);
    assertEquals(value, index.getValue());

    index.setSource(element);
    assertEquals(element, index.getSource());

    index.setWeight(weight);
    assertEquals((Integer) weight, index.getWeight());
  }

}
