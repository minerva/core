package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class IonTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Ion());
  }

  @Test
  public void testConstructor1() {
    Ion degraded = new Ion(new Ion());
    assertNotNull(degraded);
  }

  @Test
  public void testGetters() {
    Ion degraded = new Ion("id");
    assertNotNull(degraded.getStringType());
  }

  @Test
  public void testCopy() {
    Ion degraded = new Ion().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Ion ion = Mockito.spy(Ion.class);
    ion.copy();
  }

}
