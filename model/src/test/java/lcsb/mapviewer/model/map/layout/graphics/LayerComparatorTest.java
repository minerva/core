package lcsb.mapviewer.model.map.layout.graphics;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LayerComparatorTest extends ModelTestFunctions {
  private final LayerComparator comparator = new LayerComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    final Layer layer1 = getLayer();
    final Layer layer2 = getLayer();

    assertEquals(0, comparator.compare(new Layer(), new Layer()));
    assertEquals(0, comparator.compare(layer1, layer2));
    assertEquals(0, comparator.compare(layer1, layer1));
    assertEquals(0, comparator.compare(null, null));
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    class Tmp extends Layer {
      private static final long serialVersionUID = 1L;
    }

    final Tmp layer1 = new Tmp();
    final Tmp layer2 = new Tmp();

    comparator.compare(layer1, layer2);
  }

  private Layer getLayer() {
    final Layer layer = createLayer();

    layer.setLayerId(1);
    layer.setName("B");
    layer.setVisible(true);
    layer.setLocked(true);

    final PolylineData line = new PolylineData();
    layer.addLayerLine(line);

    final LayerText text = new LayerText();
    text.setWidth(32.1);
    text.setNotes("SAda");
    layer.addLayerText(text);

    final LayerRect rect = new LayerRect();
    rect.setHeight(4.3);
    layer.addLayerRect(rect);

    final LayerOval oval = new LayerOval();
    oval.setX(12.0);
    layer.addLayerOval(oval);

    return layer;
  }

  @Test
  public void testDifferent() throws Exception {
    Layer layer1 = getLayer();
    Layer layer2 = getLayer();

    assertTrue(comparator.compare(layer1, new Layer()) != 0);

    assertTrue(comparator.compare(layer1, null) != 0);
    assertTrue(comparator.compare(null, layer1) != 0);
    assertTrue(comparator.compare(layer1, Mockito.spy(Layer.class)) != 0);

    layer1 = getLayer();
    layer2 = getLayer();

    layer1.setName("ASdasdas");

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);

    layer1 = getLayer();
    layer2 = getLayer();

    layer1.getLines().add(new PolylineData());

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);

    layer1 = getLayer();
    layer2 = getLayer();

    layer1.getLines().get(0).setWidth(654);

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);

    layer1 = getLayer();
    layer2 = getLayer();

    layer1.getOvals().add(new LayerOval());

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);

    layer1 = getLayer();
    layer2 = getLayer();

    layer1.getOvals().iterator().next().setWidth("654");

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);

    layer1 = getLayer();
    layer2 = getLayer();

    layer1.getRectangles().add(new LayerRect());

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);

    layer1 = getLayer();
    layer2 = getLayer();

    layer1.getRectangles().iterator().next().setWidth("654");

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);

    layer1 = getLayer();
    layer2 = getLayer();

    layer1.getTexts().add(new LayerText());

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);

    layer1 = getLayer();
    layer2 = getLayer();

    layer1.getTexts().iterator().next().setWidth("654");

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);

    layer1 = getLayer();
    layer2 = getLayer();

    layer1.setVisible(false);

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);

    layer1 = getLayer();
    layer2 = getLayer();

    layer1.setLocked(false);

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);
  }
}
