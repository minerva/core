package lcsb.mapviewer.model.map.kinetics;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SbmlUnitTest {
  private SbmlUnitComparator comparator = new SbmlUnitComparator();

  @Test
  public void testConstructor() {
    String id = "id";
    SbmlUnit unit = new SbmlUnit(id);
    assertEquals(unit.getUnitId(), id);
  }

  @Test
  public void testSetName() {
    String name = "id";
    SbmlUnit unit = new SbmlUnit("");
    unit.setName(name);
    assertEquals(unit.getName(), name);
  }

  @Test
  public void testCopy() {
    SbmlUnit unit = new SbmlUnit("");
    unit.setName("nam");
    unit.addUnitTypeFactor(new SbmlUnitTypeFactor(SbmlUnitType.METRE, 2, 0, 1.0));

    SbmlUnit copy = unit.copy();
    assertEquals(0, comparator.compare(unit, copy));
  }

  @Test
  public void testAddUnitTypeFactor() {
    SbmlUnitTypeFactor factor = new SbmlUnitTypeFactor(SbmlUnitType.METRE, 2, 0, 1.0);
    SbmlUnit unit = new SbmlUnit("");
    unit.addUnitTypeFactor(factor);
    assertEquals(unit.getUnitTypeFactors().size(), 1);
    assertEquals(unit.getUnitTypeFactors().iterator().next(), factor);
  }

}
