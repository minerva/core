package lcsb.mapviewer.model.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.validator.routines.UrlValidator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;

public class MiriamTypeTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @SuppressWarnings("deprecation")
  @Test
  public void testIdUniqness() {
    Map<String, MiriamType> ids = new HashMap<>();
    Map<String, MiriamType> uris = new HashMap<>();

    for (final MiriamType mt : MiriamType.values()) {
      assertNull(mt.getCommonName() + " doesn't have a unique key", ids.get(mt.getRegistryIdentifier()));
      ids.put(mt.getRegistryIdentifier(), mt);

      for (String uri : mt.getUris()) {
        assertNull(uri + " is used more than one", uris.get(uri));
        uris.put(uri, mt);
      }

      // coverage tests
      assertNotNull(MiriamType.valueOf(mt.name()));
      assertNotNull(mt.getValidClass());
      assertNotNull(mt.getRequiredClass());
      if (mt != MiriamType.UNKNOWN) {
        assertNotNull("No homepage for: " + mt, mt.getDbHomepage());
        assertTrue("invalid website: " + mt.getDbHomepage(), UrlValidator.getInstance().isValid(mt.getDbHomepage()));
      }
    }
  }

  @Test
  public void testGetMiriamDataFromIdentifier() {
    MiriamData md = MiriamType.getMiriamDataFromIdentifier("HGNC Symbol:SNCA");
    assertEquals(new MiriamData(MiriamType.HGNC_SYMBOL, "SNCA"), md);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetMiriamDataFromInvalidIdentifier() {
    MiriamType.getMiriamDataFromIdentifier("xyz:SNCA");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetMiriamDataFromInvalidIdentifier2() {
    MiriamType.getMiriamDataFromIdentifier("xyz");
  }

  @Test
  public void testTypeByUri() {
    assertNotNull(MiriamType.getTypeByUri("urn:miriam:cas"));
    assertNull(MiriamType.getTypeByUri("xyz"));
  }

  @Test
  public void testTypeByCommonName() {
    assertNotNull(MiriamType.getTypeByCommonName("Consensus CDS"));
    assertNull(MiriamType.getTypeByCommonName("xyz"));
  }

  @Test
  public void testGetMiriamByUri1() throws Exception {
    MiriamData md = MiriamType.getMiriamByUri("urn:miriam:panther.family:PTHR19384:SF5");
    assertTrue(new MiriamData(MiriamType.PANTHER, "PTHR19384:SF5").equals(md));
  }

  @Test
  public void testGetMiriamForVmhMetabolite() throws Exception {
    MiriamData md = MiriamType.getMiriamByUri("urn:miriam:vmhmetabolite:o2");
    assertNotNull(md);
  }

  @Test
  public void testGetMiriamForUniprotUrl() throws Exception {
    MiriamData md = MiriamType.getMiriamByUri("https://purl.uniprot.org/uniprot/Q8NEZ2");
    assertNotNull(md);
  }

  @Test
  public void testGetMiriamByUri2() throws Exception {
    MiriamData md = MiriamType.getMiriamByUri("urn:miriam:panther.family:PTHR19384%3ASF5");
    assertTrue(new MiriamData(MiriamType.PANTHER, "PTHR19384:SF5").equals(md));
  }

  @Test
  public void testGetMiriamByUri3() throws Exception {
    MiriamData md = MiriamType.getMiriamByUri("urn:miriam:panther.family%3APTHR19384%3ASF5");
    assertTrue(new MiriamData(MiriamType.PANTHER, "PTHR19384:SF5").equals(md));
  }

  @Test
  public void testGetMiriamByIdentifiersOrgUri() throws Exception {
    MiriamData md = MiriamType.getMiriamByUri("http://identifiers.org/uniprot/P42224");
    assertEquals(new MiriamData(MiriamType.UNIPROT, "P42224"), md);
  }

  @Test
  public void testGetMiriamByChebiUri() throws Exception {
    MiriamData md = MiriamType.getMiriamByUri("urn:miriam:obo.chebi:CHEBI%3A15377");
    assertEquals(new MiriamData(MiriamType.CHEBI, "CHEBI:15377"), md);
  }

  @Test
  public void testGetMiriamByChebiIdentifiersOrgUri() throws Exception {
    MiriamData md = MiriamType.getMiriamByUri("http://identifiers.org/CHEBI:12345");
    assertEquals(new MiriamData(MiriamType.CHEBI, "CHEBI:12345"), md);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testGetMiriamByInvalidUri() throws Exception {
    MiriamType.getMiriamByUri("invalid_uri");
  }

  @Test
  public void testGetMiriamForDoiUri() {
    MiriamData md = MiriamType.getMiriamByUri("urn:miriam:doi:10.1109%2F5.771073");
    assertTrue(md.getResource().contains("/"));
  }

  @Test
  public void testGetMiriamByKeggCompoundIdentifiersOrgUri() throws Exception {
    MiriamData md = MiriamType.getMiriamByUri("https://identifiers.org/kegg.compound:197020");
    assertEquals(new MiriamData(MiriamType.KEGG_COMPOUND, "197020"), md);
  }

  @Test
  public void testGetMiriamByEcoIdentifiersOrgUri() throws Exception {
    MiriamData md = MiriamType.getMiriamByUri("https://identifiers.org/ECO:0000006");
    assertEquals(new MiriamData(MiriamType.ECO, "ECO:0000006"), md);
  }

  @Test
  public void testGetMiriamByMgiIdentifiersOrgUri() throws Exception {
    MiriamData md = MiriamType.getMiriamByUri("https://identifiers.org/MGI:2442292");
    assertEquals(new MiriamData(MiriamType.MGD, "MGI:2442292"), md);
  }

  @Test
  public void testGetMiriamByPatoIdentifiersOrgUri() throws Exception {
    MiriamData md = MiriamType.getMiriamByUri("https://identifiers.org/PATO:0001998");
    assertEquals(new MiriamData(MiriamType.PATO, "PATO:0001998"), md);
  }

  @Test
  public void testGetMiriamBySboIdentifiersOrgUri() throws Exception {
    MiriamData md = MiriamType.getMiriamByUri("https://identifiers.org/SBO:0000262");
    assertEquals(new MiriamData(MiriamType.SBO_TERM, "SBO:0000262"), md);
  }
}
