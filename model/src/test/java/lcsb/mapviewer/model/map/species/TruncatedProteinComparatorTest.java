package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class TruncatedProteinComparatorTest extends ModelTestFunctions {

  private TruncatedProteinComparator comparator = new TruncatedProteinComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    TruncatedProtein drug1 = createTruncatedProtein();
    TruncatedProtein drug2 = createTruncatedProtein();

    assertEquals(0, comparator.compare(drug1, drug1));

    assertEquals(0, comparator.compare(drug1, drug2));
    assertEquals(0, comparator.compare(drug2, drug1));
  }

  @Test
  public void testDifferent() {
    TruncatedProtein drug2 = createTruncatedProtein();
    assertTrue(comparator.compare(null, drug2) != 0);
    assertTrue(comparator.compare(drug2, null) != 0);
    assertTrue(comparator.compare(null, null) == 0);

    TruncatedProtein drug = createTruncatedProtein();
    drug.setName("n");
    assertTrue(comparator.compare(drug, drug2) != 0);

    assertTrue(comparator.compare(drug, Mockito.mock(TruncatedProtein.class)) != 0);
  }

  public TruncatedProtein createTruncatedProtein() {
    TruncatedProtein result = new TruncatedProtein();
    return result;
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    TruncatedProtein object = Mockito.mock(TruncatedProtein.class);

    comparator.compare(object, object);
  }

}
