package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.StructuralState;

public class ProteinComparatorTest extends ModelTestFunctions {

  private ProteinComparator comparator = new ProteinComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    GenericProtein protein1 = createProtein();
    GenericProtein protein2 = new GenericProtein(protein1);

    assertEquals(0, comparator.compare(protein1, protein1));

    assertEquals(0, comparator.compare(protein1, protein2));
    assertEquals(0, comparator.compare(protein2, protein1));

    assertEquals(0, comparator.compare(null, null));

    assertEquals(0, comparator.compare(new TruncatedProtein(), new TruncatedProtein()));
    assertEquals(0, comparator.compare(new ReceptorProtein(), new ReceptorProtein()));
    assertEquals(0, comparator.compare(new IonChannelProtein(), new IonChannelProtein()));
  }

  @Test(expected = NotImplementedException.class)
  public void testUnknownProteinImplementations() {
    Protein protein1 = Mockito.mock(Protein.class);
    assertEquals(0, comparator.compare(protein1, protein1));
  }

  @Test
  public void testDifferent() {
    GenericProtein protein1 = createProtein();
    GenericProtein protein2 = createProtein();
    protein1.getModificationResidues().clear();
    assertTrue(comparator.compare(protein1, protein2) != 0);
    assertTrue(comparator.compare(protein2, protein1) != 0);

    protein1 = createProtein();
    protein2 = new GenericProtein(protein1);
    assertTrue(comparator.compare(null, protein2) != 0);
    assertTrue(comparator.compare(protein2, null) != 0);
    assertTrue(comparator.compare(null, null) == 0);

    assertTrue(comparator.compare(protein2, new GenericProtein()) != 0);

    protein1 = createProtein();
    protein2 = new GenericProtein(protein1);
    protein1.setName("a");
    assertTrue(comparator.compare(protein1, protein2) != 0);
    assertTrue(comparator.compare(protein2, protein1) != 0);

    protein1 = createProtein();
    protein2 = new GenericProtein(protein1);
    protein1.setHomodimer(1);
    assertTrue(comparator.compare(protein1, protein2) != 0);
    assertTrue(comparator.compare(protein2, protein1) != 0);

    assertTrue(comparator.compare(new GenericProtein(), new TruncatedProtein()) != 0);
  }

  @Test
  public void testDifferentModificationResidue() {
    GenericProtein protein1 = createProtein();
    GenericProtein protein2 = createProtein();
    assertTrue(comparator.compare(null, protein2) != 0);
    assertTrue(comparator.compare(protein2, null) != 0);

    protein1.getModificationResidues().get(0).setName("bla");
    assertTrue(comparator.compare(protein1, protein2) != 0);
    assertTrue(comparator.compare(protein2, protein1) != 0);
  }

  @Override
  public GenericProtein createProtein() {
    GenericProtein result = new GenericProtein();
    result.setHomodimer(12);
    result.addStructuralState(new StructuralState());
    result.setHypothetical(true);

    Residue residue = new Residue();
    result.addResidue(residue);

    residue.setIdModificationResidue("a");
    residue.setName("name");
    residue.setPosition(new Point2D.Double(10, 20));
    residue.setState(ModificationState.DONT_CARE);
    return result;
  }

}
