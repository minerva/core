package lcsb.mapviewer.model.map.layout.graphics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.Color;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class LayerRectComparatorTest extends ModelTestFunctions {
  private LayerRectComparator comparator = new LayerRectComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    LayerRect layer1 = getLayerRect();
    LayerRect layer2 = getLayerRect();

    assertEquals(0, comparator.compare(new LayerRect(), new LayerRect()));
    assertEquals(0, comparator.compare(layer1, layer2));
    assertEquals(0, comparator.compare(layer1, layer1));
    assertEquals(0, comparator.compare(null, null));
  }

  private LayerRect getLayerRect() {

    LayerRect line = new LayerRect();
    line.setBorderColor(Color.YELLOW);
    line.setFillColor(Color.GREEN);
    line.setWidth(4.3);

    return line;
  }

  @Test
  public void testDifferentFillColor() {
    LayerRect layer1 = getLayerRect();
    LayerRect layer2 = getLayerRect();
    layer2.setFillColor(Color.PINK);

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);
  }

  public void testDifferent() {
    LayerRect layer1 = getLayerRect();
    LayerRect layer2 = getLayerRect();

    assertTrue(comparator.compare(layer1, new LayerRect()) != 0);

    assertTrue(comparator.compare(layer1, null) != 0);
    assertTrue(comparator.compare(null, layer1) != 0);

    layer1 = getLayerRect();
    layer2 = getLayerRect();

    layer1.setBorderColor(Color.PINK);

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);

    layer1 = getLayerRect();
    layer2 = getLayerRect();

    layer1.setHeight("23");

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);

    layer1 = getLayerRect();
    layer2 = getLayerRect();

    layer1.setHeight(1.2);

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);

    layer1 = getLayerRect();
    layer2 = getLayerRect();

    layer1.setX(33.4);

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);

    layer1 = getLayerRect();
    layer2 = getLayerRect();

    layer1.setY(33.4);

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);
  }

  @Test
  public void testDifferent2() {
    LayerRect layer1 = getLayerRect();

    assertTrue(comparator.compare(layer1, Mockito.spy(LayerRect.class)) != 0);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    LayerRect layer1 = Mockito.mock(LayerRect.class);
    LayerRect layer2 = Mockito.mock(LayerRect.class);

    comparator.compare(layer1, layer2);
  }

  @Test
  public void testDifferentZIndex() {
    LayerRect layer1 = getLayerRect();
    LayerRect layer2 = getLayerRect();

    layer1.setZ(12);

    assertTrue(comparator.compare(layer1, layer2) != 0);
    assertTrue(comparator.compare(layer2, layer1) != 0);
  }

}
