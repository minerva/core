package lcsb.mapviewer.model.map.species;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DegradedComparatorTest extends ModelTestFunctions {

  private final DegradedComparator comparator = new DegradedComparator();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testEquals() {
    Degraded degraded1 = createDegraded();
    Degraded degraded2 = createDegraded();

    assertEquals(0, comparator.compare(degraded1, degraded1));

    assertEquals(0, comparator.compare(degraded1, degraded2));
    assertEquals(0, comparator.compare(degraded2, degraded1));
  }

  @Test
  public void testDifferent() {
    Degraded degraded1 = createDegraded();
    Degraded degraded2 = createDegraded();
    degraded1.setCharge(54);
    assertTrue(comparator.compare(degraded1, degraded2) != 0);
    assertTrue(comparator.compare(degraded2, degraded1) != 0);

    degraded1 = createDegraded();
    degraded2 = createDegraded();
    assertTrue(comparator.compare(null, degraded2) != 0);
    assertTrue(comparator.compare(degraded2, null) != 0);
    assertEquals(0, comparator.compare(null, null));

    Degraded degraded = createDegraded();
    degraded.setName("n");
    assertEquals("Degraded does not have names", 0, comparator.compare(degraded, degraded1));

    assertTrue(comparator.compare(degraded, Mockito.mock(Degraded.class)) != 0);
  }

  public Degraded createDegraded() {
    Degraded result = new Degraded();
    result.setCharge(12);
    return result;
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalid() {
    Degraded object = Mockito.mock(Degraded.class);

    comparator.compare(object, object);
  }
}
