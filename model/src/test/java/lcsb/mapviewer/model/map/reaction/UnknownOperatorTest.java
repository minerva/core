package lcsb.mapviewer.model.map.reaction;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;

public class UnknownOperatorTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new UnknownOperator());
  }

  @Test
  public void testConstructor() {
    UnknownOperator op = new UnknownOperator();
    op.setLine(new PolylineData());
    UnknownOperator operator = new UnknownOperator(op);
    assertNotNull(operator);
  }

  @Test
  public void testGetters() {
    assertFalse("".equals(new UnknownOperator().getOperatorText()));
  }

  @Test
  public void testCopy1() {
    UnknownOperator op = new UnknownOperator();
    op.setLine(new PolylineData());
    UnknownOperator operator = op.copy();
    assertNotNull(operator);
  }

  @Test(expected = NotImplementedException.class)
  public void testCopy2() {
    Mockito.mock(UnknownOperator.class, Mockito.CALLS_REAL_METHODS).copy();
  }

}
