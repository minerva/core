package lcsb.mapviewer.model.map.kinetics;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SbmlParameterTest {

  @Test
  public void testConstructor() {
    String id = "param_id";
    SbmlParameter parameter = new SbmlParameter(id);
    assertEquals(id, parameter.getParameterId());
  }

  @Test
  public void testSetName() {
    String name = "name";
    SbmlParameter parameter = new SbmlParameter("");
    parameter.setName(name);
    assertEquals(name, parameter.getName());
  }

  @Test
  public void testSetValue() {
    Double value = 13.0;
    SbmlParameter parameter = new SbmlParameter("");
    parameter.setValue(value);
    assertEquals(value, parameter.getValue());
  }

  @Test
  public void testCopy() {
    Double value = 13.0;
    String name = "name";
    SbmlParameter parameter = new SbmlParameter("id");
    parameter.setName(name);
    parameter.setValue(value);

    SbmlParameter copy = parameter.copy();

    SbmlParameterComparator comparator = new SbmlParameterComparator();
    assertEquals(0, comparator.compare(parameter, copy));
  }

}
