package lcsb.mapviewer.model.map.modifier;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.species.GenericProtein;

public class ModulationTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Modulation());
  }

  @Test
  public void testConstructor() {
    Modulation modifier = new Modulation(new GenericProtein("unk_id"));
    assertNotNull(modifier);
  }

  @Test
  public void testConstructor2() {
    Modulation modifier = new Modulation(new GenericProtein("unk_id"));
    modifier.setLine(new PolylineData());
    Modulation modifier2 = new Modulation(modifier);
    assertNotNull(modifier2);
  }

  @Test(expected = NotImplementedException.class)
  public void testCopyInvalid() {
    Mockito.mock(Modulation.class, Mockito.CALLS_REAL_METHODS).copy();
  }

  @Test
  public void testCopy() {
    Modulation modifier = new Modulation(new GenericProtein("unk_id"));
    modifier.setLine(new PolylineData());
    Modulation modifier2 = modifier.copy();
    assertNotNull(modifier2);
  }

}
