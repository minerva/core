package lcsb.mapviewer.model.map.reaction.type;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;

public class KnownTransitionOmittedReactionTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new KnownTransitionOmittedReaction());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructorWithInvalidArg() {
    Reaction reaction = new Reaction("re");
    new KnownTransitionOmittedReaction(reaction);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testConstructorWithInvalidArg2() {
    Reaction reaction = new Reaction("re");
    reaction.addProduct(new Product(createProtein()));
    new KnownTransitionOmittedReaction(reaction);
  }

  @Test
  public void testConstructor() {
    Reaction reaction = new Reaction("re");
    reaction.addProduct(new Product(createProtein()));
    reaction.addReactant(new Reactant(createProtein()));
    KnownTransitionOmittedReaction validReaction = new KnownTransitionOmittedReaction(reaction);
    assertNotNull(validReaction);
  }

  @Test
  public void testCopy() {
    KnownTransitionOmittedReaction original = new KnownTransitionOmittedReaction();
    original.addProduct(new Product(createProtein()));
    original.addReactant(new Reactant(createProtein()));
    original.addReactant(new Reactant(createProtein()));
    KnownTransitionOmittedReaction product = original.copy();
    assertNotNull(product);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(new KnownTransitionOmittedReaction()).copy();
  }

  @Test
  public void testGetters() {
    KnownTransitionOmittedReaction original = new KnownTransitionOmittedReaction();
    assertNotNull(original.getReactionRect());
    assertNotNull(original.getStringType());
  }

}
