package lcsb.mapviewer.model.map.compartment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class PathwayCompartmentTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void test() throws Exception {
    CompartmentComparator comparator = new CompartmentComparator();
    Compartment pathway = new PathwayCompartment("id");
    Compartment copy = pathway.copy();

    assertEquals(0, comparator.compare(pathway, copy));
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new PathwayCompartment("id"));
  }

  @Test
  public void testGetters() {
    PathwayCompartment pathway = new PathwayCompartment("id");
    String title = "tit27";
    pathway.setName(title);
    assertEquals(title, pathway.getName());
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    PathwayCompartment comp = Mockito.spy(PathwayCompartment.class);
    comp.copy();
  }

  @Test
  public void testCopy() {
    PathwayCompartment degraded = new PathwayCompartment("id").copy();
    assertNotNull(degraded);
  }

}
