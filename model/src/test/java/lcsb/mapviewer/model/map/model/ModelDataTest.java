package lcsb.mapviewer.model.map.model;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.model.map.layout.BlockDiagram;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class ModelDataTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new ModelData());
  }

  @Test
  public void testAddElements() {
    ModelData md = new ModelData();
    List<Element> elements = new ArrayList<>();
    elements.add(new GenericProtein("unk_id"));
    md.addElements(elements);
    assertEquals(1, md.getElements().size());
  }

  @Test
  public void testAddReactions() {
    ModelData md = new ModelData();
    List<Reaction> reactions = new ArrayList<>();
    reactions.add(new Reaction("re"));
    md.addReactions(reactions);
    assertEquals(1, md.getReactions().size());
  }

  @Test
  public void testAddLayers() {
    ModelData md = new ModelData();
    List<Layer> layers = new ArrayList<>();
    layers.add(createLayer());
    md.addLayers(layers);
    assertEquals(1, md.getLayers().size());
  }

  @Test
  public void testAddElementGroup() {
    ModelData md = new ModelData();
    md.addElementGroup(null);
    // not implemented for now
  }

  @Test
  public void testAddBlockDiagram() {
    ModelData md = new ModelData();
    md.addBlockDiagream(Mockito.spy(BlockDiagram.class));
    // not implemented for now
  }

  @Test(expected = InvalidArgumentException.class)
  public void testRemoveElement1() {
    ModelData md = new ModelData();
    md.removeElement(null);
  }

  @Test
  public void testRemoveElement2() {
    ModelData md = new ModelData();
    md.removeElement(new GenericProtein("unk_id"));
  }

  @Test
  public void testRemoveElement3() {
    ModelData md = new ModelData();
    Element protein = new GenericProtein("unk_id");
    md.addElement(protein);
    assertEquals(1, md.getElements().size());
    md.removeElement(protein);
    assertEquals(0, md.getElements().size());
  }

  @Test
  public void testRemoveReaction2() {
    ModelData md = new ModelData();
    md.removeReaction(new Reaction("re"));
  }

  @Test
  public void testRemoveReaction3() {
    ModelData md = new ModelData();
    Reaction reaction = new Reaction("re");
    md.addReaction(reaction);
    assertEquals(1, md.getReactions().size());
    md.removeReaction(reaction);
    assertEquals(0, md.getReactions().size());
  }

  @Test
  public void testAddSubmodel() {
    ModelData md = new ModelData();
    md.addSubmodel(new ModelSubmodelConnection());
    assertEquals(1, md.getSubmodels().size());
  }

  @Test
  public void testAddAuthor() {
    ModelData md = new ModelData();
    md.addAuthor(new Author("Piotr", "Gawron"));
    assertEquals(1, md.getAuthors().size());
  }

  @Test
  public void testAddModificationDate() {
    ModelData md = new ModelData();
    md.addModificationDate(Calendar.getInstance());
    assertEquals(1, md.getModificationDates().size());
  }

  @Test
  public void testSetCreationDate() {
    ModelData md = new ModelData();
    assertNull(md.getCreationDate());
    md.setCreationDate(Calendar.getInstance());
    assertNotNull(md.getCreationDate());
  }

  @Test
  public void testGetters() {
    ModelData md = new ModelData();
    Set<Element> elements = new HashSet<>();
    Set<Layer> layers = new HashSet<>();
    Set<Reaction> reactions = new HashSet<>();
    Set<SubmodelConnection> parents = new HashSet<>();
    List<OverviewImage> overviewImages = new ArrayList<>();
    overviewImages.add(new OverviewImage());
    int id = 95;
    int width = 2;
    Double widthDouble = 2.0;
    int height = 29;
    int zoomLevels = 3;
    int tileSize = 512;
    String modelId = "modId";
    String notes = "nOT";
    String name = "n_ma";
    Double heightDouble = 29.0;
    Project project = new Project();

    Model model = new ModelFullIndexed(null);

    md.setId(id);
    assertEquals(id, md.getId());

    md.setElements(elements);
    assertEquals(elements, md.getElements());

    md.setProject(project);
    assertEquals(project, md.getProject());

    md.setWidth(width);
    assertEquals(widthDouble, md.getWidth(), Configuration.EPSILON);

    md.setHeight(height);
    assertEquals(heightDouble, md.getHeight(), Configuration.EPSILON);

    md.setZoomLevels(zoomLevels);
    assertEquals(zoomLevels, md.getZoomLevels());

    md.setTileSize(tileSize);
    assertEquals(tileSize, md.getTileSize());

    md.setIdModel(modelId);
    assertEquals(modelId, md.getIdModel());

    md.setNotes(notes);
    assertEquals(notes, md.getNotes());

    md.setName(name);
    assertEquals(name, md.getName());

    md.setModel(null);
    assertNull(md.getModel());
    md.setModel(model);
    assertEquals(model, md.getModel());

    md.setLayers(layers);
    assertEquals(layers, md.getLayers());

    md.setReactions(reactions);
    assertEquals(reactions, md.getReactions());

    md.setParentModels(parents);
    assertEquals(parents, md.getParentModels());

  }
}
