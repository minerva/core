package lcsb.mapviewer.model.map.compartment;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;

public class TopSquareCompartmentTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new TopSquareCompartment());
  }

  @Test
  public void testConstructor1() {
    TopSquareCompartment compartment = new TopSquareCompartment(new TopSquareCompartment());
    assertNotNull(compartment);
  }

  @Test
  public void testConstructor3() {
    TopSquareCompartment compartment = new TopSquareCompartment("id");
    assertNotNull(compartment);
  }

  @Test
  public void testConstructor2() {
    Model model = new ModelFullIndexed(null);
    model.setWidth(1);
    model.setHeight(1);
    TopSquareCompartment compartment = new TopSquareCompartment(new Compartment(), model);
    assertNotNull(compartment);
  }

  @Test
  public void testCopy() {
    TopSquareCompartment compartment = new TopSquareCompartment().copy();
    assertNotNull(compartment);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    TopSquareCompartment compartment = Mockito.spy(TopSquareCompartment.class);
    compartment.copy();
  }

}
