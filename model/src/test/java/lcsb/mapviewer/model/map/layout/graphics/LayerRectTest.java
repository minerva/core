package lcsb.mapviewer.model.map.layout.graphics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.awt.Color;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class LayerRectTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(createRect());
  }

  @Test
  public void testConstructor1() {
    LayerRect layerRect = createRect();

    LayerRect copy = new LayerRect(layerRect);

    assertNotNull(copy);
    assertEquals(0, new LayerRectComparator().compare(layerRect, copy));
  }

  private LayerRect createRect() {
    LayerRect result = new LayerRect();
    result.setZ(14);
    return result;
  }

  @Test
  public void testCopy() {
    LayerRect copy = createRect().copy();

    assertNotNull(copy);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(LayerRect.class).copy();
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidX() {
    LayerRect rect = createRect();
    rect.setX("a1.6");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidY() {
    LayerRect rect = createRect();
    rect.setY("a1.6");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidWidth() {
    LayerRect rect = createRect();
    rect.setWidth("a1.6");
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidHeight() {
    LayerRect rect = createRect();
    rect.setHeight("a1.6");
  }

  @Test
  public void testGetters() {
    LayerRect rect = createRect();

    String stringY = "1.2";
    Double y = 1.2;

    String stringX = "2.2";
    Double x = 2.2;

    String widthParam = "10.2";
    Double width = 10.2;

    String heightParam = "72.2";
    Double height = 72.2;

    Color color = Color.BLACK;

    rect.setY(stringY);
    assertEquals(y, rect.getY(), Configuration.EPSILON);
    rect.setY((Double) null);
    assertNull(rect.getY());
    rect.setY(y);
    assertEquals(y, rect.getY(), Configuration.EPSILON);

    rect.setX(stringX);
    assertEquals(x, rect.getX(), Configuration.EPSILON);
    rect.setX((Double) null);
    assertNull(rect.getX());
    rect.setX(x);
    assertEquals(x, rect.getX(), Configuration.EPSILON);

    rect.setWidth(widthParam);
    assertEquals(width, rect.getWidth(), Configuration.EPSILON);
    rect.setWidth((Double) null);
    assertNull(rect.getWidth());
    rect.setWidth(width);
    assertEquals(width, rect.getWidth(), Configuration.EPSILON);

    rect.setHeight(heightParam);
    assertEquals(height, rect.getHeight(), Configuration.EPSILON);
    rect.setHeight((Double) null);
    assertNull(rect.getHeight());
    rect.setHeight(height);
    assertEquals(height, rect.getHeight(), Configuration.EPSILON);

    rect.setBorderColor(color);
    assertEquals(color, rect.getBorderColor());
  }

}
