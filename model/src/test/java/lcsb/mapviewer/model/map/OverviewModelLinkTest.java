package lcsb.mapviewer.model.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class OverviewModelLinkTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new OverviewModelLink());
  }

  @Test
  public void testConstructor() {
    OverviewModelLink link = new OverviewModelLink(new OverviewModelLink());
    assertNotNull(link);
  }

  @Test
  public void testSetters() {
    OverviewModelLink link = new OverviewModelLink();
    link.setxCoord(2.0);
    link.setyCoord(3.0);

    assertEquals((Integer) 2, link.getxCoord());
    assertEquals((Integer) 3, link.getyCoord());

    link.setxCoord((Double) null);
    link.setyCoord((Double) null);
    assertNull(link.getxCoord());
    assertNull(link.getyCoord());
  }

  @Test
  public void testCopy() {
    OverviewModelLink degraded = new OverviewModelLink().copy();
    assertNotNull(degraded);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(OverviewModelLink.class).copy();
  }

}
