package lcsb.mapviewer.model.map.species;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.model.map.species.field.CodingRegion;

public class RnaTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new Rna());
  }

  @Test
  public void testConstructor1() {
    Rna rna = new Rna("d");
    assertNotNull(rna);
  }

  @Test
  public void testConstructor() {
    Rna rna = new Rna("d");
    rna.addCodingRegion(new CodingRegion());
    Rna rna2 = new Rna(rna);
    assertEquals(rna.getModificationResidues().size(), rna2.getModificationResidues().size());
  }

  @Test
  public void testGetters() {
    Rna rna = new Rna(new Rna());
    assertNotNull(rna.getStringType());
  }

  @Test
  public void testCopy() {
    Rna rna = new Rna().copy();
    assertNotNull(rna);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Rna object = Mockito.spy(Rna.class);
    object.copy();
  }

}
