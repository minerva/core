package lcsb.mapviewer.model;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ProjectStatusTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (final ProjectStatus type : ProjectStatus.values()) {
      assertNotNull(type);

      // for coverage tests
      ProjectStatus.valueOf(type.name());
      type.toString();
    }
  }

}
