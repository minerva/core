package lcsb.mapviewer.model.graphics;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.NotImplementedException;

public class ArrowTypeDataTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new ArrowTypeData());
  }

  @Test
  public void testCopy() {
    ArrowTypeData original = new ArrowTypeData();
    ArrowTypeData degraded = original.copy();
    assertNotNull(degraded);
  }

  @Test
  public void testEquals() {
    ArrowTypeData original = new ArrowTypeData();
    original.setAngle(12);
    ArrowTypeData copy = original.copy();

    assertTrue(original.equals(copy));
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() {
    Mockito.spy(ArrowTypeData.class).copy();
  }

}
