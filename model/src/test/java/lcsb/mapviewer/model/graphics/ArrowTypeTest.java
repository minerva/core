package lcsb.mapviewer.model.graphics;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ArrowTypeTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (final ArrowType type : ArrowType.values()) {
      assertNotNull(type);

      // for coverage tests
      ArrowType.valueOf(type.toString());
    }
  }

}
