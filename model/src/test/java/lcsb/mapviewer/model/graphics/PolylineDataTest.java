package lcsb.mapviewer.model.graphics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.common.geometry.PointTransformation;

public class PolylineDataTest extends ModelTestFunctions {

  private PointTransformation pr = new PointTransformation();

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testConstructor() throws Exception {
    PolylineData pd = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
    assertNotNull(pd);
  }

  @Test
  public void testConstructor2() throws Exception {
    PolylineData pd = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
    PolylineData pd2 = new PolylineData(pd);
    assertNotNull(pd2);
  }

  @Test
  public void testConstructor3() throws Exception {
    List<Point2D> points = new ArrayList<>();
    points.add(new Point2D.Double(1, 1));
    points.add(new Point2D.Double(5, 1));
    points.add(new Point2D.Double(5, 5));
    PolylineData pd2 = new PolylineData(points);
    assertNotNull(pd2);
    assertEquals(points.size(), pd2.getLines().size() + 1);
    assertEquals(8, pd2.length(), Configuration.EPSILON);
  }

  @Test
  public void testAddLine() throws Exception {
    PolylineData pd = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
    pd.addLine(new Point2D.Double(10, 10), new Point2D.Double(-10, -10));
    assertEquals(2, pd.getLines().size());
    assertTrue(pd.getEndPoint().getX() < 0);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testAddInvalidLine() throws Exception {
    PolylineData pd = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
    pd.addLine(new Point2D.Double(10, 10), new Point2D.Double(Double.NEGATIVE_INFINITY, -10));
  }

  @Test
  public void testGetLines() throws Exception {
    List<Point2D> points = new ArrayList<>();
    points.add(new Point2D.Double(1, 1));
    points.add(new Point2D.Double(5, 1));
    points.add(new Point2D.Double(5, 5));
    PolylineData pd2 = new PolylineData(points);
    assertEquals(2, pd2.getLines().size());
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvaliArgument() throws Exception {
    new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, Double.NaN));
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new PolylineData());
  }

  @Test
  public void testSetEndPoint() throws Exception {
    PolylineData pd = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
    pd.setEndPoint(new Point2D.Double(-10, -10));
    assertEquals(1, pd.getLines().size());
    assertTrue(pd.getEndPoint().getX() < 0);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidEndPoint() throws Exception {
    PolylineData pd = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
    pd.setEndPoint(new Point2D.Double(Double.NEGATIVE_INFINITY, -10));
  }

  @Test
  public void testSetStartPoint() throws Exception {
    PolylineData pd = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
    pd.setStartPoint(new Point2D.Double(-10, -10));
    assertEquals(1, pd.getLines().size());
    assertTrue(pd.getStartPoint().getX() < 0);
  }

  @Test(expected = InvalidArgumentException.class)
  public void testSetInvalidStartPoint() throws Exception {
    PolylineData pd = new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 10));
    pd.setStartPoint(new Point2D.Double(Double.NEGATIVE_INFINITY, -10));
  }

  @Test
  public void testToGeneralPath() throws Exception {
    List<Point2D> points = new ArrayList<>();
    points.add(new Point2D.Double(1, 1));
    points.add(new Point2D.Double(5, 1));
    points.add(new Point2D.Double(5, 5));
    PolylineData pd = new PolylineData(points);
    GeneralPath gp = pd.toGeneralPath();
    PathIterator pi = gp.getPathIterator(new AffineTransform());
    int count = 0;
    while (!pi.isDone()) {
      count++;
      pi.next();
    }
    assertEquals(3, count);
  }

  @Test
  public void testTrimEnd() throws Exception {
    PolylineData pd = new PolylineData(new Point2D.Double(1, 1), new Point2D.Double(11, 1));
    pd.trimEnd(3);

    assertEquals(8, pd.getEndPoint().getX(), Configuration.EPSILON);
  }

  @Test
  public void testLineFromPoints() throws Exception {
    List<Point2D> points = new ArrayList<>();
    points.add(new Point2D.Double(1, 1));
    points.add(new Point2D.Double(11, 1));
    PolylineData pd = new PolylineData(points);
    assertEquals(new Point2D.Double(1, 1), pd.getStartPoint());
    assertEquals(new Point2D.Double(11, 1), pd.getEndPoint());
  }

  @Test
  public void testTrimEndByNegativeValueOnEmptySegment() throws Exception {
    List<Point2D> points = new ArrayList<>();
    points.add(new Point2D.Double(1, 1));
    points.add(new Point2D.Double(1, 1));
    PolylineData pd = new PolylineData(points);
    pd.trimEnd(-3);
    for (final Line2D line: pd.getLines()) {
      assertTrue(pr.isValidPoint(line.getP1()));
      assertTrue(pr.isValidPoint(line.getP2()));
    }
  }

  @Test
  public void testTrimEndByNegativeValue() throws Exception {
    List<Point2D> points = new ArrayList<>();
    points.add(new Point2D.Double(1, 1));
    points.add(new Point2D.Double(10, 1));
    PolylineData pd = new PolylineData(points);
    pd.trimEnd(-3);
    for (final Line2D line: pd.getLines()) {
      assertTrue(pr.isValidPoint(line.getP1()));
      assertTrue(pr.isValidPoint(line.getP2()));
    }
  }

  @Test
  public void testTrimBegin() throws Exception {
    List<Point2D> points = new ArrayList<>();
    points.add(new Point2D.Double(1, 1));
    points.add(new Point2D.Double(11, 1));
    PolylineData pd = new PolylineData(points);
    pd.trimBegin(3);

    assertEquals(4, pd.getStartPoint().getX(), Configuration.EPSILON);
  }

  @Test
  public void testTrimEnd2() throws Exception {
    List<Point2D> points = new ArrayList<>();
    points.add(new Point2D.Double(1, 1));
    points.add(new Point2D.Double(11, 1));
    PolylineData pd = new PolylineData(points);
    pd.trimEnd(30);

    assertEquals(1, pd.getEndPoint().getX(), Configuration.EPSILON);
  }

  @Test
  public void testTrimBegin2() throws Exception {
    List<Point2D> points = new ArrayList<>();
    points.add(new Point2D.Double(1, 1));
    points.add(new Point2D.Double(11, 1));
    PolylineData pd = new PolylineData(points);
    pd.trimBegin(30);

    assertEquals(11, pd.getStartPoint().getX(), Configuration.EPSILON);
  }

  @Test
  public void testReverse() throws Exception {
    List<Point2D> points = new ArrayList<>();
    points.add(new Point2D.Double(1, 1));
    points.add(new Point2D.Double(11, 1));
    PolylineData pd = new PolylineData(points);
    PolylineData pd2 = pd.reverse();
    assertTrue(pd.getStartPoint().distance(pd2.getEndPoint()) <= Configuration.EPSILON);
    assertTrue(pd2.getStartPoint().distance(pd.getEndPoint()) <= Configuration.EPSILON);
  }

  @Test
  public void testEmptyLength() throws Exception {
    PolylineData pd = new PolylineData();
    assertEquals(0, pd.length(), Configuration.EPSILON);
  }

  @Test
  public void testGetters() throws Exception {
    PolylineData pd = new PolylineData();
    Color color = Color.BLACK;
    LineType type = LineType.DASH_DOT;
    String strWidth = "12";
    double width = 12.0;
    int id = 3;
    pd.setColor(color);
    pd.setId(id);
    pd.setType(type);
    pd.setWidth(strWidth);

    assertEquals(id, pd.getId());
    assertEquals(color, pd.getColor());
    assertEquals(width, pd.getWidth(), Configuration.EPSILON);
    assertEquals(type, pd.getType());
  }

  @Test
  public void testSimpleCopy() throws Exception {
    PolylineData pd = new PolylineData();
    PolylineData pd2 = pd.copy();

    assertNotNull(pd2);
  }

  @Test(expected = NotImplementedException.class)
  public void testInvalidCopy() throws Exception {
    Mockito.spy(PolylineData.class).copy();
  }

  @Test
  public void testAddLineOrder() throws Exception {
    PolylineData pd = new PolylineData();
    pd.addLine(new Point2D.Double(10, 10), new Point2D.Double(20, 20));
    pd.addLine(new Point2D.Double(20, 20), new Point2D.Double(20, 30));
    assertEquals(new Point2D.Double(10, 10), pd.getStartPoint());
    assertEquals(new Point2D.Double(20, 30), pd.getEndPoint());
  }

}
