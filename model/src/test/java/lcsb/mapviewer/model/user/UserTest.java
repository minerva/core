package lcsb.mapviewer.model.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.Color;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;

public class UserTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new User());
  }

  @Test
  public void testToString() {
    assertNotNull(new User().toString());
  }

  @Test
  public void testAddPrivilege() {
    User user = new User();
    user.addPrivilege(new Privilege(PrivilegeType.IS_ADMIN));
    assertEquals(1, user.getPrivileges().size());
  }

  @Test
  public void testGetters() {
    User user = new User();
    int id = 737;
    String login = "log 54";
    String cryptedPassword = "pa";
    String name = "ccr";
    String email = "a.a@pl.pl";
    String surname = "cccv";
    Set<Privilege> privileges = new HashSet<>();
    boolean removed = true;
    UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
    Color minColor = Color.BLACK;
    Color maxColor = Color.BLUE;

    user.setMaxColor(maxColor);
    assertEquals(maxColor, user.getMaxColor());

    user.setMinColor(minColor);
    assertEquals(minColor, user.getMinColor());

    user.setId(id);
    assertEquals(id, user.getId());

    user.setLogin(login);
    assertEquals(login, user.getLogin());

    user.setCryptedPassword(cryptedPassword);
    assertEquals(cryptedPassword, user.getCryptedPassword());

    user.setName(name);
    assertEquals(name, user.getName());

    user.setSurname(surname);
    assertEquals(surname, user.getSurname());

    user.setPrivileges(privileges);
    assertEquals(privileges, user.getPrivileges());

    user.setRemoved(removed);
    assertEquals(removed, user.isRemoved());

    user.setEmail(email);
    assertEquals(email, user.getEmail());

    user.setAnnotationSchema(annotationSchema);
    assertEquals(annotationSchema, user.getAnnotationSchema());
  }
}
