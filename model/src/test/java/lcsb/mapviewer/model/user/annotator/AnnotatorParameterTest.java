package lcsb.mapviewer.model.user.annotator;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

public class AnnotatorParameterTest {

  @Test
  public void testComparator() {
    List<AnnotatorParameter> list = new ArrayList<>();
    AnnotatorParameter p1 = new AnnotatorConfigParameter();
    p1.setOrderPosition(10);
    AnnotatorParameter p2 = new AnnotatorConfigParameter();
    p2.setOrderPosition(1);
    AnnotatorParameter p3 = new AnnotatorConfigParameter();
    p3.setOrderPosition(12);

    list.add(p1);
    list.add(p2);
    list.add(p3);

    Collections.sort(list);

    assertEquals(p2, list.get(0));
    assertEquals(p1, list.get(1));
    assertEquals(p3, list.get(2));
  }

}
