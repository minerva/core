package lcsb.mapviewer.model.user;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ConfigurationElementTypeTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (final ConfigurationElementType type : ConfigurationElementType.values()) {
      assertNotNull(type);

      // for coverage tests
      ConfigurationElementType.valueOf(type.toString());
      assertNotNull(type.getCommonName());
      assertNotNull(type.getDefaultValue());
      assertNotNull(type.getEditType());
    }
  }
}
