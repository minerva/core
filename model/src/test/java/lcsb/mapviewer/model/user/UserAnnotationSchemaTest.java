package lcsb.mapviewer.model.user;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class UserAnnotationSchemaTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testAddClassAnnotator() {
    UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
    UserClassAnnotators ca = new UserClassAnnotators(String.class, new ArrayList<>());
    UserClassAnnotators ca2 = new UserClassAnnotators(String.class, new ArrayList<>());
    annotationSchema.addClassAnnotator(ca);
    annotationSchema.addClassAnnotator(ca2);
    assertEquals(1, annotationSchema.getClassAnnotators().size());
    assertEquals(ca2.getAnnotators(), annotationSchema.getAnnotatorsForClass(String.class));
  }

  @Test(expected = InvalidArgumentException.class)
  public void testInvalidAddClassAnnotator() {
    UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
    UserClassAnnotators ca = new UserClassAnnotators(String.class, new ArrayList<>());
    ca.setClassName((String) null);
    annotationSchema.addClassAnnotator(ca);
  }

  @Test
  public void testAddGuiPreference() {
    UserAnnotationSchema annotationSchema = new UserAnnotationSchema();

    annotationSchema.addGuiPreference(new UserGuiPreference());
    assertEquals(1, annotationSchema.getGuiPreferences().size());
  }

  @Test
  public void testAddGuiPreferenceWithDuplicateKey() {
    UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
    UserGuiPreference preference1 = new UserGuiPreference();
    preference1.setKey("test");
    preference1.setValue("value");
    annotationSchema.addGuiPreference(preference1);
    UserGuiPreference preference2 = new UserGuiPreference();
    preference2.setKey("test");
    preference2.setValue("new value");
    annotationSchema.addGuiPreference(preference2);
    assertEquals(1, annotationSchema.getGuiPreferences().size());
    assertEquals("new value", annotationSchema.getGuiPreferences().iterator().next().getValue());
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new UserAnnotationSchema());
  }

  @Test
  public void testGetters() {
    UserAnnotationSchema uas = new UserAnnotationSchema();
    Boolean validateMiriamTypes = true;
    List<UserClassAnnotators> classAnnotators = new ArrayList<>();
    User user = new User();

    Boolean sbgnFormat = true;
    Boolean networkBackgroundAsDefault = true;

    uas.setValidateMiriamTypes(validateMiriamTypes);
    uas.setClassAnnotators(classAnnotators);
    uas.setSbgnFormat(sbgnFormat);
    uas.setNetworkBackgroundAsDefault(networkBackgroundAsDefault);
    uas.setUser(user);

    assertEquals(validateMiriamTypes, uas.getValidateMiriamTypes());
    assertEquals(classAnnotators, uas.getClassAnnotators());
    assertEquals(sbgnFormat, uas.getSbgnFormat());
    assertEquals(networkBackgroundAsDefault, uas.getNetworkBackgroundAsDefault());
    assertEquals(user, uas.getUser());
  }

}
