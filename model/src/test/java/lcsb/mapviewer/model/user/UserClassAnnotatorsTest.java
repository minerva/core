package lcsb.mapviewer.model.user;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;

public class UserClassAnnotatorsTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testSerialization() {
    SerializationUtils.serialize(new UserClassAnnotators());
  }

  @Test
  public void testConstructor() {
    List<AnnotatorData> list = new ArrayList<>();
    list.add(new AnnotatorData(Object.class));
    UserClassAnnotators uca = new UserClassAnnotators(String.class, list);

    assertEquals(1, uca.getAnnotators().size());
  }

  @Test
  public void testGetters() {
    UserClassAnnotators uca = new UserClassAnnotators();
    Integer id = 26;
    UserAnnotationSchema annotationSchema = new UserAnnotationSchema();
    List<AnnotatorData> annotators = new ArrayList<>();
    AnnotatorData annotator = new AnnotatorData(Object.class);

    uca.setId(id);
    uca.setAnnotationSchema(annotationSchema);
    uca.setAnnotators(annotators);
    uca.addAnnotator(annotator);

    assertEquals(id, uca.getId());
    assertEquals(annotationSchema, uca.getAnnotationSchema());
    assertEquals(annotator, uca.getAnnotators().get(0));
  }
}
