package lcsb.mapviewer.model.user;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class ConfigurationElementEditTypeTest {

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValues() {
    for (final ConfigurationElementEditType type : ConfigurationElementEditType.values()) {
      assertNotNull(ConfigurationElementEditType.valueOf(type.toString()));
    }
  }

}
