package lcsb.mapviewer.modelutils.serializer.model.map;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;

public class MiriamDataSerializerTest {

  private ObjectMapper objectMapper;

  public MiriamDataSerializerTest() {

    objectMapper = new ObjectMapper();
    SimpleModule module = new SimpleModule();
    module.addSerializer(MiriamData.class, new MiriamDataSerializer());
    objectMapper.registerModule(module);

  }

  @Test
  public void testCreateAnnotationWithWhitespace() throws Exception {
    String response = objectMapper.writeValueAsString(new MiriamData(MiriamType.PUBMED, "28255955 "));
    assertNotNull(response);
  }

  @Test
  public void testCreateAnnotationWithNonNumericValues() throws Exception {
    String response = objectMapper.writeValueAsString(new MiriamData(MiriamType.PUBMED, "28255955PG"));
    assertNotNull(response);
  }

  @Test
  public void testCreateAnnotationWithEmptyValue() throws Exception {
    String response = objectMapper.writeValueAsString(new MiriamData(MiriamType.PUBMED, ""));
    assertNotNull(response);
  }
}
