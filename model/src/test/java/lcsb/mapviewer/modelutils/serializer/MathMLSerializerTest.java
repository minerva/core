package lcsb.mapviewer.modelutils.serializer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import lcsb.mapviewer.ModelTestFunctions;

public class MathMLSerializerTest extends ModelTestFunctions {

  private ObjectMapper objectMapper = null;

  @Before
  public void setUp() {
    objectMapper = new ObjectMapper();
    SimpleModule s = new SimpleModule();
    s.addSerializer(String.class, new MathMLSerializer());
    objectMapper.registerModule(s);
  }

  @Test
  public void testMathMLToPresentationML() throws Exception {
    String content = super.readFile("src/test/resources/mathml/content.xml");
    String presentationXml = objectMapper.writeValueAsString(content);
    assertNotNull(presentationXml);
    assertTrue("Presentation math ml doesn't contain expected mrow tag", presentationXml.indexOf("mrow") >= 0);
  }

  @Test
  public void testMathMLToPresentationMLCalledTwice() throws Exception {
    String content = super.readFile("src/test/resources/mathml/content.xml");
    String presentationXml = objectMapper.writeValueAsString(content);
    String presentationXml2 = objectMapper.writeValueAsString(content);
    assertEquals(presentationXml, presentationXml2);
  }

  @Test
  public void testMathMLToPresentationMLShouldntHaveXmlNode() throws Exception {
    String content = super.readFile("src/test/resources/mathml/content.xml");
    String presentationXml = objectMapper.writeValueAsString(content);
    assertFalse(presentationXml.startsWith("<?xml"));
  }

}
