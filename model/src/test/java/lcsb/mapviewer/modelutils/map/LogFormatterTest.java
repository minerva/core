package lcsb.mapviewer.modelutils.map;

import static org.junit.Assert.assertNotNull;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.impl.Log4jLogEvent;
import org.apache.logging.log4j.message.SimpleMessage;
import org.junit.Test;

import lcsb.mapviewer.model.LogMarker;
import lcsb.mapviewer.model.map.BioEntity;

public class LogFormatterTest {
  private String formatWithEverything = "%T M S %I %C %i %c";

  @Test
  public void testFormatterWithoutData() {
    LogFormatter formatter = new LogFormatter();
    LogEvent event = new Log4jLogEvent.Builder()
        .setMarker(new LogMarker(null, (BioEntity) null))
        .setMessage(new SimpleMessage("ttestt"))
        .build();

    String result = formatter.createFormattedString(event, formatWithEverything);
    assertNotNull(result);
  }

}
