package lcsb.mapviewer.modelutils.map;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RequireAnnotationMapTest {

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testValidValues() {
    for (final RequireAnnotationMap type : RequireAnnotationMap.values()) {
      assertNotNull(type);

      // for coverage tests
      RequireAnnotationMap.valueOf(type.toString());
    }
  }

}
