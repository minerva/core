package lcsb.mapviewer.modelutils.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import lcsb.mapviewer.ModelTestFunctions;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.StateTransitionReaction;
import lcsb.mapviewer.model.map.reaction.type.UnknownNegativeInfluenceReaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.IonChannelProtein;
import lcsb.mapviewer.model.map.species.Protein;

public class ElementUtilsTest extends ModelTestFunctions {

  @Before
  public void setUp() throws Exception {
    ElementUtils.setElementClasses(null);
    ElementUtils.setReactionClasses(null);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testClassTree() throws Exception {
    ElementUtils elementUtils = new ElementUtils();

    ClassTreeNode top = elementUtils.getAnnotatedElementClassTree();
    elementUtils.getAnnotatedElementClassTree();
    elementUtils.getAnnotatedElementClassTree();
    elementUtils.getAnnotatedElementClassTree();
    elementUtils.getAnnotatedElementClassTree();

    assertNotNull(top);

    assertTrue(top.getChildren().size() > 0);
  }

  @Test
  public void testGetTag() throws Exception {
    ElementUtils elementUtils = new ElementUtils();

    assertNotNull(elementUtils.getElementTag(Mockito.spy(new Reaction("re"))));
  }

  @Test
  public void testGetTag2() throws Exception {
    ElementUtils elementUtils = new ElementUtils();

    GenericProtein protein = new GenericProtein("id");

    assertNotNull(elementUtils.getElementTag(protein, new Object()));
  }

  @Test
  public void testTagForNull() throws Exception {
    ElementUtils elementUtils = new ElementUtils();

    assertNotNull(elementUtils.getElementTag(null, null));
  }

  protected void print(final ClassTreeNode top, final int indent) {
    if (indent > 10) {
      throw new InvalidArgumentException();
    }
    String tmp = "";
    for (int i = 0; i < indent; i++) {
      tmp += "  ";
    }
    logger.debug(tmp + top.getCommonName());
    for (final ClassTreeNode node : top.getChildren()) {
      print(node, indent + 1);
    }

  }

  @Test
  public void tesGetAvailableElementSubclasses() throws Exception {
    ElementUtils elementUtils = new ElementUtils();

    List<Class<? extends Element>> list = elementUtils.getAvailableElementSubclasses();
    assertTrue(list.contains(IonChannelProtein.class));
    assertFalse(list.contains(Protein.class));
  }

  @Test
  public void testClassByName() throws Exception {
    ElementUtils elementUtils = new ElementUtils();
    assertEquals(Ion.class, elementUtils.getClassByName(Ion.class.getSimpleName()));
    assertEquals(Ion.class, elementUtils.getClassByName("Ion"));
    assertNull(elementUtils.getClassByName("unknown class name"));
  }

  @Test
  public void testGetAvailableReactionSubclasses() throws Exception {
    ElementUtils elementUtils = new ElementUtils();

    List<Class<? extends Reaction>> list = elementUtils.getAvailableReactionSubclasses();
    assertTrue(list.contains(UnknownNegativeInfluenceReaction.class));
    assertFalse(list.contains(Reaction.class));
  }

  @Test
  public void testClassByName2() throws Exception {
    ElementUtils elementUtils = new ElementUtils();
    assertEquals(UnknownNegativeInfluenceReaction.class, elementUtils.getClassByName("UnknownNegativeInfluence"));
  }

  @Test
  public void testAnnotatedElementClassTree() throws Exception {
    ElementUtils elementUtils = new ElementUtils();
    ClassTreeNode tree = elementUtils.getAnnotatedElementClassTree();
    Queue<ClassTreeNode> queue = new LinkedList<ClassTreeNode>();
    queue.add(tree);
    while (!queue.isEmpty()) {
      ClassTreeNode node = queue.poll();
      for (final ClassTreeNode child : node.getChildren()) {
        queue.add(child);
      }
      assertNotNull("Data for class " + node.getClazz() + " is null", node.getData());
    }
  }

  @Test
  public void testAnnotatedElementClassTreeSorted() throws Exception {
    ElementUtils elementUtils = new ElementUtils();
    ClassTreeNode tree = elementUtils.getAnnotatedElementClassTree();
    Queue<ClassTreeNode> queue = new LinkedList<ClassTreeNode>();
    queue.add(tree);
    while (!queue.isEmpty()) {
      ClassTreeNode node = queue.poll();
      String lastChild = "";
      for (final ClassTreeNode child : node.getChildren()) {
        queue.add(child);
        assertTrue(lastChild.compareTo(child.getCommonName()) <= 0);
        lastChild = child.getCommonName();
      }
    }
  }

  @Test
  public void testGetClassesByElementStringType() throws Exception {
    ElementUtils elementUtils = new ElementUtils();
    List<Class<? extends BioEntity>> classes = elementUtils.getClassesByStringTypes(new GenericProtein("id").getStringType());
    assertTrue(classes.contains(GenericProtein.class));
  }

  @Test
  public void testGetClassesByReactionStringType() throws Exception {
    ElementUtils elementUtils = new ElementUtils();
    List<Class<? extends BioEntity>> classes = elementUtils.getClassesByStringTypes(new StateTransitionReaction("id").getStringType());
    assertTrue(classes.contains(StateTransitionReaction.class));
  }

  @Test
  public void testGetClassesByElementStringTypes() throws Exception {
    ElementUtils elementUtils = new ElementUtils();
    List<String> types  =  Arrays.asList(new GenericProtein("id").getStringType(), new Ion("id").getStringType());
    List<Class<? extends BioEntity>> classes = elementUtils.getClassesByStringTypes(types);
    assertTrue(classes.contains(GenericProtein.class));
    assertTrue(classes.contains(Ion.class));
    assertFalse(classes.contains(StateTransitionReaction.class));
  }


}
