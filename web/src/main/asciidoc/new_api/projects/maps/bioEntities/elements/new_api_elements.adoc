= Rest New API Documentation
minerva
v{project-version} {build-time}
:toc: left
:sectnums:


== Get element by id
Returns list of bio entities matching the query.

=== Path Parameters
include::{snippets}/new_api/projects/maps/bioEntities/elements/get_by_id/path-parameters.adoc[]

=== Response Fields
include::{snippets}/new_api/projects/maps/bioEntities/elements/get_by_id/response-fields.adoc[]

=== CURL sample
include::{snippets}/new_api/projects/maps/bioEntities/elements/get_by_id/curl-request.adoc[]

=== Sample Response
include::{snippets}/new_api/projects/maps/bioEntities/elements/get_by_id/response-body.adoc[]

== Download element list as csv file
Returns csv file with list of elements.

=== Path Parameters
include::{snippets}/new_api/projects/maps/bioEntities/elements/export_to_csv/path-parameters.adoc[]

=== Request Fields
include::{snippets}/new_api/projects/maps/bioEntities/elements/export_to_csv/request-fields.adoc[]

=== CURL sample
include::{snippets}/new_api/projects/maps/bioEntities/elements/export_to_csv/curl-request.adoc[]

=== Sample Response
include::{snippets}/new_api/projects/maps/bioEntities/elements/export_to_csv/response-body.adoc[]
