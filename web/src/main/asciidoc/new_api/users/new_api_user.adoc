= Rest New API Documentation - User
minerva
v{project-version} {build-time}
:toc: left
:sectnums:


== Get user by id

Returns user data.

=== CURL sample

include::{snippets}/new_api/users/get_by_id/curl-request.adoc[]

=== Path Parameters

include::{snippets}/new_api/users/get_by_id/path-parameters.adoc[]

=== Response Fields

include::{snippets}/new_api/users/get_by_id/response-fields.adoc[]

=== Sample Response

include::{snippets}/new_api/users/get_by_id/response-body.adoc[]

== List all users

=== CURL sample

include::{snippets}/new_api/users/list_users/curl-request.adoc[]

=== Path Parameters

include::{snippets}/new_api/users/list_users/path-parameters.adoc[]

=== Response Fields

include::{snippets}/new_api/users/list_users/response-fields.adoc[]

=== Sample Response

include::{snippets}/new_api/users/list_users/response-body.adoc[]

== Create user

=== CURL sample

include::{snippets}/new_api/users/create_user/curl-request.adoc[]

=== Path Parameters

include::{snippets}/new_api/users/create_user/path-parameters.adoc[]

=== Request Fields

include::{snippets}/new_api/users/create_user/request-fields.adoc[]

=== Response Fields

include::{snippets}/new_api/users/create_user/response-fields.adoc[]

=== Sample Response

include::{snippets}/new_api/users/create_user/response-body.adoc[]

== Update user

=== CURL sample

include::{snippets}/new_api/users/update_user/curl-request.adoc[]

=== Path Parameters

include::{snippets}/new_api/users/update_user/path-parameters.adoc[]

=== Request Fields

include::{snippets}/new_api/users/update_user/request-fields.adoc[]

=== Response Fields

include::{snippets}/new_api/users/update_user/response-fields.adoc[]

=== Sample Response

include::{snippets}/new_api/users/update_user/response-body.adoc[]

== Grant user privilege

=== Path Parameters

include::{snippets}/new_api/users/grant_privilege/path-parameters.adoc[]

=== Request Fields

include::{snippets}/new_api/users/grant_privilege/request-fields.adoc[]

=== Response Fields

include::{snippets}/new_api/users/grant_privilege/response-fields.adoc[]

=== CURL sample

include::{snippets}/new_api/users/grant_privilege/curl-request.adoc[]

=== Sample Response

include::{snippets}/new_api/users/grant_privilege/response-body.adoc[]

== Revoke user privilege

=== Path Parameters

include::{snippets}/new_api/users/revoke_privilege/path-parameters.adoc[]

=== Request Fields

include::{snippets}/new_api/users/revoke_privilege/request-fields.adoc[]

=== Response Fields

include::{snippets}/new_api/users/revoke_privilege/response-fields.adoc[]

=== CURL sample

include::{snippets}/new_api/users/revoke_privilege/curl-request.adoc[]

=== Sample Response

include::{snippets}/new_api/users/revoke_privilege/response-body.adoc[]

== Delete user

=== CURL sample

include::{snippets}/new_api/users/delete_user/curl-request.adoc[]

=== Path Parameters

include::{snippets}/new_api/users/delete_user/path-parameters.adoc[]

== Request password reset over email

=== CURL sample

include::{snippets}/new_api/users/request_reset_password/curl-request.adoc[]

=== Path Parameters

include::{snippets}/new_api/users/request_reset_password/path-parameters.adoc[]

== Reset password using token obtained over email

=== CURL sample

include::{snippets}/new_api/users/reset_password/curl-request.adoc[]

=== Request Fields

include::{snippets}/new_api/users/reset_password/request-fields.adoc[]

== Register new user

=== CURL sample

include::{snippets}/new_api/users/register_user/curl-request.adoc[]

=== Request Fields

include::{snippets}/new_api/users/register_user/request-fields.adoc[]

=== Response Fields

include::{snippets}/new_api/users/register_user/response-fields.adoc[]

=== Sample Response

include::{snippets}/new_api/users/register_user/response-body.adoc[]

== Confirm email

=== CURL sample

include::{snippets}/new_api/users/confirm_email/curl-request.adoc[]

=== Request Parameters

include::{snippets}/new_api/users/confirm_email/request-parameters.adoc[]

=== Path Parameters

include::{snippets}/new_api/users/confirm_email/path-parameters.adoc[]

=== Response Fields

include::{snippets}/new_api/users/confirm_email/response-fields.adoc[]

=== Sample Response

include::{snippets}/new_api/users/confirm_email/response-body.adoc[]
