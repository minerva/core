= Rest API Documentation - Project comments
minerva
v{project-version} {build-time}
:toc: left
:sectnums:

== Get comments
Get list of comments.

=== CURL sample
include::{snippets}/projects/project_comments/get_all/curl-request.adoc[]

=== Path Parameters
include::{snippets}/projects/project_comments/get_all/path-parameters.adoc[]

=== Request Parameters
include::{snippets}/projects/project_comments/get_all/request-parameters.adoc[]

=== Response Fields
include::{snippets}/projects/project_comments/get_all/response-fields.adoc[]

=== Sample Response
include::{snippets}/projects/project_comments/get_all/response-body.adoc[]


== Get comments for specific reaction

=== CURL sample
include::{snippets}/projects/project_comments/get_reaction/curl-request.adoc[]

=== Path Parameters
include::{snippets}/projects/project_comments/get_reaction/path-parameters.adoc[]

=== Request Parameters
include::{snippets}/projects/project_comments/get_reaction/request-parameters.adoc[]

=== Response Fields
include::{snippets}/projects/project_comments/get_reaction/response-fields.adoc[]

=== Sample Response
include::{snippets}/projects/project_comments/get_reaction/response-body.adoc[]


== Get comments for specific element

=== CURL sample
include::{snippets}/projects/project_comments/get_element/curl-request.adoc[]

=== Path Parameters
include::{snippets}/projects/project_comments/get_element/path-parameters.adoc[]

=== Request Parameters
include::{snippets}/projects/project_comments/get_element/request-parameters.adoc[]

=== Response Fields
include::{snippets}/projects/project_comments/get_element/response-fields.adoc[]

=== Sample Response
include::{snippets}/projects/project_comments/get_element/response-body.adoc[]


== Get comments for specific point

=== CURL sample
include::{snippets}/projects/project_comments/get_point/curl-request.adoc[]

=== Path Parameters
include::{snippets}/projects/project_comments/get_point/path-parameters.adoc[]

=== Request Parameters
include::{snippets}/projects/project_comments/get_point/request-parameters.adoc[]

=== Response Fields
include::{snippets}/projects/project_comments/get_point/response-fields.adoc[]

=== Sample Response
include::{snippets}/projects/project_comments/get_point/response-body.adoc[]


== Create element comment

=== CURL sample
include::{snippets}/projects/project_comments/create_element_comment/curl-request.adoc[]

=== Path Parameters
include::{snippets}/projects/project_comments/create_element_comment/path-parameters.adoc[]

=== Request Parameters
include::{snippets}/projects/project_comments/create_element_comment/request-parameters.adoc[]

=== Response Fields
include::{snippets}/projects/project_comments/create_element_comment/response-fields.adoc[]

=== Sample Response
include::{snippets}/projects/project_comments/create_element_comment/response-body.adoc[]


== Create reaction comment

=== CURL sample
include::{snippets}/projects/project_comments/create_reaction_comment/curl-request.adoc[]

=== Path Parameters
include::{snippets}/projects/project_comments/create_reaction_comment/path-parameters.adoc[]

=== Request Parameters
include::{snippets}/projects/project_comments/create_reaction_comment/request-parameters.adoc[]

=== Response Fields
include::{snippets}/projects/project_comments/create_reaction_comment/response-fields.adoc[]

=== Sample Response
include::{snippets}/projects/project_comments/create_reaction_comment/response-body.adoc[]


== Create coordinates comment

=== CURL sample
include::{snippets}/projects/project_comments/create_point_comment/curl-request.adoc[]

=== Path Parameters
include::{snippets}/projects/project_comments/create_point_comment/path-parameters.adoc[]

=== Request Parameters
include::{snippets}/projects/project_comments/create_point_comment/request-parameters.adoc[]

=== Response Fields
include::{snippets}/projects/project_comments/create_point_comment/response-fields.adoc[]

=== Sample Response
include::{snippets}/projects/project_comments/create_point_comment/response-body.adoc[]


== Delete comment

=== CURL sample
include::{snippets}/projects/project_comments/delete_comment/curl-request.adoc[]

=== Path Parameters
include::{snippets}/projects/project_comments/delete_comment/path-parameters.adoc[]
