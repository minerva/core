package lcsb.mapviewer.web.events;

import lcsb.mapviewer.common.exception.InvalidArgumentException;

/**
 * Event called when object was removed.
 * 
 * @author Piotr Gawron
 * 
 */
public class ObjectRemovedEvent extends Event {

  /**
   * Object that was removed.
   */
  private Object object;

  /**
   * Default constructor.
   * 
   * @param object
   *          {@link #object}
   */
  public ObjectRemovedEvent(final Object object) {
    if (object == null) {
      throw new InvalidArgumentException("object cannot be null");
    }

    this.object = object;
  }

  /**
   * @return the object
   * @see #object
   */
  public Object getObject() {
    return object;
  }

}
