package lcsb.mapviewer.web.events;

import lcsb.mapviewer.common.exception.InvalidArgumentException;

/**
 * Event called when object was modified.
 * 
 * @author Piotr Gawron
 * 
 */
public class ObjectModifiedEvent extends Event {

  /**
   * Object that was modified.
   */
  private Object object;

  /**
   * Default constructor.
   * 
   * @param object
   *          {@link #object}
   */
  public ObjectModifiedEvent(final Object object) {
    if (object == null) {
      throw new InvalidArgumentException("object cannot be null");
    }
    this.object = object;
  }

  /**
   * @return the object
   * @see #object
   */
  public Object getObject() {
    return object;
  }

}
