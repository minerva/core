package lcsb.mapviewer.web.events;

import java.io.Serializable;

/**
 * Abstract class that handles {@link Event events}.
 * 
 * @author Piotr Gawron
 */
public abstract class Listener<T extends Event> implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Class of event handled by this listener.
   */
  private Class<T> eventClass;

  /**
   * Default constructor.
   * 
   * @param clazz
   *          class of events handled by this listener
   */
  public Listener(final Class<T> clazz) {
    this.eventClass = clazz;
  }

  /**
   * This method handle the event.
   * 
   * @param event
   *          event to be handled
   */
  protected abstract void handle(final T event);

  /**
   * Handle the event.
   * 
   * @param event
   *          event to be handled
   */
  @SuppressWarnings("unchecked")
  public void handleEvent(final Event event) {
    this.handle((T) event);
  }

  /**
   * @return the eventClass
   * @see #eventClass
   */
  public Class<T> getEventClass() {
    return eventClass;
  }
}
