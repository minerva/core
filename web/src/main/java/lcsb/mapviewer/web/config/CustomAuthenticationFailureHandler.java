package lcsb.mapviewer.web.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

  private Logger logger = LogManager.getLogger();

  public CustomAuthenticationFailureHandler(final String string) {
    super(string);
  }

  @Override
  public void onAuthenticationFailure(final HttpServletRequest request,
      final HttpServletResponse response, final AuthenticationException exception) throws IOException, ServletException {
    logger.error("Problem with oauth2 authentication", exception);
    super.onAuthenticationFailure(request, response, exception);
  }

}
