package lcsb.mapviewer.web.config;

import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistration.Builder;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.oidc.IdTokenClaimNames;

public enum CustomOAuth2Provider {
  ORCID {
    @Override
    public Builder getBuilder(final String registrationId, final String baseUrl) {

      String url = "{baseUrl}" + SpringSecurityConfig.OAUTH_REDIRECT_PATH + "{registrationId}";
      if (baseUrl != null && !baseUrl.trim().isEmpty()) {
        url = url.replace("{baseUrl}", baseUrl);
      }
      url = url.replaceAll("minerva/+minerva", "minerva");

      final ClientRegistration.Builder builder = getBuilder(registrationId, ClientAuthenticationMethod.BASIC, url);
      builder.scope("/authenticate");
      builder.authorizationUri("https://orcid.org/oauth/authorize");
      builder.tokenUri("https://orcid.org/oauth/token");
      builder.jwkSetUri("https://orcid.org/oauth/jwks");
      builder.userInfoUri("https://orcid.org/oauth/userinfo");
      builder.userNameAttributeName(IdTokenClaimNames.SUB);
      builder.clientName("Orcid");
      return builder;
    }
  };

  protected final ClientRegistration.Builder getBuilder(final String registrationId,
                                                        final ClientAuthenticationMethod method, final String redirectUri) {
    final ClientRegistration.Builder builder = ClientRegistration.withRegistrationId(registrationId);
    builder.clientAuthenticationMethod(method);
    builder.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE);
    builder.redirectUriTemplate(redirectUri);
    return builder;
  }

  public abstract ClientRegistration.Builder getBuilder(String registrationId, String baseUrl);

}
