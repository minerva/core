package lcsb.mapviewer.web.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IUserService;

@Order(1)
@Service
public class LocalAuthenticationProvider implements AuthenticationProvider {

  private DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();

  private UserDetailsService userDetailsService;
  private PasswordEncoder passwordEncoder;
  private IUserService userService;

  @Autowired
  public LocalAuthenticationProvider(final UserDetailsService userDetailsService,
      final PasswordEncoder passwordEncoder,
      final IUserService userService) {
    this.userDetailsService = userDetailsService;
    this.passwordEncoder = passwordEncoder;
    this.userService = userService;
  }

  @PostConstruct
  private void init() {
    daoAuthenticationProvider.setPasswordEncoder(passwordEncoder);
    daoAuthenticationProvider.setUserDetailsService(userDetailsService);
  }

  @Override
  public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
    String username = authentication.getName();
    if (username.isEmpty()) {
      throw new BadCredentialsException("Username must not be empty.");
    }
    User user = userService.getUserByLogin(username);
    if (user == null || user.isConnectedToLdap()) {
      throw new UsernameNotFoundException("Provider cannot authenticate user.");
    }
    return daoAuthenticationProvider.authenticate(authentication);
  }

  @Override
  public boolean supports(final Class<?> authentication) {
    return daoAuthenticationProvider.supports(authentication);
  }

}
