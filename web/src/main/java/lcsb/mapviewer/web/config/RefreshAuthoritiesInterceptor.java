package lcsb.mapviewer.web.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.HandlerInterceptor;

/**
 * This class intercepts a request (before the authorization step) to refresh
 * the current authentication object in case the authorities have been updated
 * since the last request.
 * <p>
 * Performance note: This code has an impact on performance due to the repeated
 * DB requests, but is much simpler and therefor safer. If performance becomes
 * an issue, user sessions must be updated manually when authorities change.
 * This also requires being able to directly access and modify an arbitrary user
 * session from the current request session.
 * </p>
 */
@Transactional
@Service
public class RefreshAuthoritiesInterceptor implements HandlerInterceptor {

  private UserDetailsService userDetailsService;

  @Autowired
  public RefreshAuthoritiesInterceptor(final UserDetailsService userDetailsService) {
    this.userDetailsService = userDetailsService;
  }

  @Override
  public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler) throws Exception {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth == null) {
      return true;
    }
    Authentication newAuth = new UsernamePasswordAuthenticationToken(
        auth.getPrincipal(),
        auth.getCredentials(),
        userDetailsService.loadUserByUsername(auth.getName()).getAuthorities());
    SecurityContextHolder.getContext().setAuthentication(newAuth);
    return true;
  }

}
