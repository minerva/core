package lcsb.mapviewer.web.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

@Component
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {

  private static final Logger logger = LogManager.getLogger();

  private final IUserService userService;

  public AuthenticationSuccessHandlerImpl(final IUserService userService) {
    this.userService = userService;
  }

  @Override
  public void onAuthenticationSuccess(
      final HttpServletRequest request,
      final HttpServletResponse response,
      final Authentication authentication) throws IOException {
    response.setStatus(HttpStatus.OK.value());

    User user = userService.getUserByLogin(authentication.getName());
    if (user == null) {
      logger.fatal("Inconsistent state. User doesn't exist");
    } else {
      String method;
      if (user.isConnectedToLdap()) {
        method = "LDAP";
      } else {
        method = "LOCAL";
      }
      logger.debug("User {} successfully logged in using {} authentication method", authentication.getName(), method);

      request.getSession().setMaxInactiveInterval(Configuration.getSessionLength());

      Map<String, Object> result = new TreeMap<>();
      result.put("info", "Login successful.");
      result.put("token", request.getSession().getId());
      result.put("login", user.getLogin());

      String json = new ObjectMapper().writeValueAsString(result);

      response.setContentType("application/json");
      response.getWriter().print(json);

    }
  }
}
