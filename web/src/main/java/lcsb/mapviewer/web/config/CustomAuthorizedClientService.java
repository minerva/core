package lcsb.mapviewer.web.config;

import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.InMemoryOAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;

public class CustomAuthorizedClientService implements OAuth2AuthorizedClientService {

  private InMemoryOAuth2AuthorizedClientService inMemoryService;

  public CustomAuthorizedClientService(final ClientRegistrationRepository clientRegistrationRepository) {
    inMemoryService = new InMemoryOAuth2AuthorizedClientService(clientRegistrationRepository);
  }

  @Override
  public <T extends OAuth2AuthorizedClient> T loadAuthorizedClient(final String clientRegistrationId, final String principalName) {
    return inMemoryService.loadAuthorizedClient(clientRegistrationId, principalName);
  }

  @Override
  public void saveAuthorizedClient(final OAuth2AuthorizedClient authorizedClient, final Authentication principal) {
    inMemoryService.saveAuthorizedClient(authorizedClient, principal);
  }

  @Override
  public void removeAuthorizedClient(final String clientRegistrationId, final String principalName) {
    inMemoryService.removeAuthorizedClient(clientRegistrationId, principalName);
  }

}
