package lcsb.mapviewer.web.config;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class CustomUserDetails extends User {
  private Integer id = null;

  public CustomUserDetails(final String username, final String password, final Collection<? extends GrantedAuthority> authorities) {
    super(username, password, authorities);
  }

  public CustomUserDetails(final UserDetails user, final int id) {
    super(user.getUsername(),
        user.getPassword(),
        user.isEnabled(),
        user.isAccountNonExpired(),
        user.isCredentialsNonExpired(),
        user.isAccountNonLocked(),
        user.getAuthorities());
    this.id = id;
  }

  public Integer getId() {
    return id;
  }

  @Override
  public String toString() {
    return getClass().getName()
        + " ["
        + "Username=" + this.getUsername() + ", "
        + "Password=[PROTECTED], "
        + "Enabled=" + this.isEnabled() + ", "
        + "AccountNonExpired=" + this.isAccountNonExpired() + ", "
        + "credentialsNonExpired=" + this.isCredentialsNonExpired() + ", "
        + "AccountNonLocked=" + this.isAccountNonLocked() + ", "
        + "ID=" + getId() + ", "
        + "Granted Authorities=" + this.getAuthorities()
        + "]";
  }
}
