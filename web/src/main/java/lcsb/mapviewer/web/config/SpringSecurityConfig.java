package lcsb.mapviewer.web.config;

import lcsb.mapviewer.api.SpringRestApiConfig;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.SpringServiceConfig;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.bean.utils.CORSFilter;
import lcsb.mapviewer.web.bean.utils.CacheInterceptor;
import lcsb.mapviewer.web.bean.utils.XFrameFilter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.endpoint.NimbusAuthorizationCodeTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository;
import org.springframework.security.oauth2.client.web.HttpSessionOAuth2AuthorizationRequestRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Configuration
@ComponentScan(basePackages = {"lcsb.mapviewer.web.config"})
@Import({SpringRestApiConfig.class, SpringServiceConfig.class})
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {

  public static final String OAUTH_REDIRECT_PATH = "/minerva/login/oauth2/code/";
  private static final Logger logger = LogManager.getLogger();

  private final AuthenticationSuccessHandler successHandler;
  private final AuthenticationFailureHandler failureHandler;
  private final LogoutSuccessHandler logoutSuccessHandler;
  private final List<AuthenticationProvider> authenticationProviders;

  private final SessionRegistry sessionRegistry;

  private final IConfigurationService configurationService;
  private final IUserService userService;

  @Autowired
  SpringSecurityConfig(final AuthenticationSuccessHandler successHandler,
                       final AuthenticationFailureHandler failureHandler,
                       final LogoutSuccessHandler logoutSuccessHandler,
                       final List<AuthenticationProvider> authenticationProviders,
                       final SessionRegistry sessionRegistry,
                       final IConfigurationService configurationService,
                       final IUserService userService) {
    this.successHandler = successHandler;
    this.failureHandler = failureHandler;
    this.logoutSuccessHandler = logoutSuccessHandler;
    this.authenticationProviders = authenticationProviders;
    this.sessionRegistry = sessionRegistry;
    this.configurationService = configurationService;
    this.userService = userService;
  }

  @Override
  public void addInterceptors(final InterceptorRegistry registry) {
    registry.addInterceptor(new CacheInterceptor());
  }

  @Override
  protected void configure(final AuthenticationManagerBuilder auth) {
    authenticationProviders.forEach(auth::authenticationProvider);
  }


  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    http
        .sessionManagement()
        .maximumSessions(-1).sessionRegistry(sessionRegistry)
        .and()
        .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
        .and()
        .anonymous().principal(lcsb.mapviewer.common.Configuration.ANONYMOUS_LOGIN)
        .and()

        .oauth2Login()
        .redirectionEndpoint()
        .baseUri(OAUTH_REDIRECT_PATH + "*")
        .and()

        .clientRegistrationRepository(clientRegistrationRepository())
        .authorizedClientService(authorizedClientService())

        .authorizationEndpoint()
        .baseUri("/minerva/oauth2/authorize-client")
        .authorizationRequestRepository(authorizationRequestRepository())
        .and()
        .tokenEndpoint()
        .accessTokenResponseClient(accessTokenResponseClient())
        .and()
        .successHandler(new SimpleUrlAuthenticationSuccessHandler("/minerva/?oauthLogin=success"))
        .failureHandler(new CustomAuthenticationFailureHandler("/minerva/?oauthLogin=failure"))
        .userInfoEndpoint()
        .userService(oidcUserService())
        .and()

        .and()
        .exceptionHandling()
        .authenticationEntryPoint(new Http403ForbiddenEntryPoint())
        .and()
        .formLogin()
        .usernameParameter("login")
        .passwordParameter("password")
        .loginProcessingUrl("/minerva/api/doLogin")
        .successHandler(successHandler)
        .failureHandler(failureHandler)
        .and()
        .logout()
        .logoutUrl("/minerva/api/doLogout").permitAll()
        .logoutSuccessHandler(logoutSuccessHandler)
        .addLogoutHandler(new LogoutHandler() {
          @Override
          public void logout(final HttpServletRequest request, final HttpServletResponse response, final Authentication authentication) {
            try {
              // for some reason session registry is not informed about logout
              final SessionInformation sessionInformation = sessionRegistry.getSessionInformation(request.getSession().getId());
              if (sessionInformation != null) {
                sessionInformation.expireNow();
              }
            } catch (final Exception e) {
              logger.error(e, e);
            }
          }
        })
        .deleteCookies(lcsb.mapviewer.common.Configuration.AUTH_TOKEN)
        .invalidateHttpSession(true)
        .and()
        .authorizeHttpRequests()
        .and()
        .headers()
        .frameOptions().disable() // is managed by XFrameFilter
        .and()
        .cors().disable() // is managed by CORSFilter
        .csrf().disable()
        .addFilterAfter(new XFrameFilter(), BasicAuthenticationFilter.class)
        .addFilterBefore(new CORSFilter(), UsernamePasswordAuthenticationFilter.class);
  }

  @Bean
  public OAuth2UserService<OAuth2UserRequest, OAuth2User> oidcUserService() {
    final DefaultOAuth2UserService delegate = new DefaultOAuth2UserService();
    return (userRequest) -> {
      final OAuth2User oidcUser = delegate.loadUser(userRequest);

      final Map<String, Object> attributes = new HashMap<>();
      final String orcidId = (String) oidcUser.getAttributes().get("sub");
      final String firstName = (String) oidcUser.getAttributes().get("given_name");
      final String lastName = (String) oidcUser.getAttributes().get("family_name");
      User user = userService.getUserByOrcidId(orcidId);
      if (user == null) {
        String login = orcidId;
        if (userService.getUserByLogin(orcidId) != null) {
          login = UUID.randomUUID().toString();
        }
        user = new User();
        user.setLogin(login);
        user.setName(firstName);
        user.setCryptedPassword("");
        user.setSurname(lastName);
        user.setOrcidId(orcidId);
        userService.add(user);
        userService.grantDefaultPrivileges(user);
      }
      user = userService.getUserByLogin(user.getLogin(), true);
      attributes.putAll(oidcUser.getAttributes());
      attributes.put("minervaLogin", user.getLogin());
      final Set<GrantedAuthority> authorities = user.getPrivileges().stream()
          .map(privilege -> new SimpleGrantedAuthority(privilege.toString()))
          .collect(Collectors.toSet());
      authorities.add(new SimpleGrantedAuthority("ORCID_USER"));
      final OAuth2User result = new DefaultOAuth2User(authorities, attributes, "minervaLogin");

      return result;
    };
  }

  @Bean
  public OAuth2AccessTokenResponseClient<OAuth2AuthorizationCodeGrantRequest> accessTokenResponseClient() {
    return new NimbusAuthorizationCodeTokenResponseClient();
  }

  @Bean
  public AuthorizationRequestRepository<OAuth2AuthorizationRequest> authorizationRequestRepository() {
    return new HttpSessionOAuth2AuthorizationRequestRepository();
  }

  @Bean
  public OAuth2AuthorizedClientService authorizedClientService() {
    return new CustomAuthorizedClientService(clientRegistrationRepository());
  }

  @Bean
  public ClientRegistrationRepository clientRegistrationRepository() {
    return new ClientRegistrationRepository() {
      @Override
      public ClientRegistration findByRegistrationId(final String registrationId) {
        return getRegistration(registrationId);
      }
    };
  }

  private ClientRegistration getRegistration(final String client) {
    if (Objects.equals("orcid", client)) {
      final String clientId = configurationService.getConfigurationValue(ConfigurationElementType.ORCID_CLIENT_ID);
      final String clientSecret = configurationService.getConfigurationValue(ConfigurationElementType.ORCID_CLIENT_SECRET);
      if (clientId != null && !clientId.isEmpty()
          && clientSecret != null && !clientSecret.isEmpty()) {
        return CustomOAuth2Provider.ORCID.getBuilder(client, configurationService.getConfigurationValue(ConfigurationElementType.MINERVA_ROOT))
            .clientId(clientId).clientSecret(clientSecret).build();
      }
    }
    return null;
  }
}