package lcsb.mapviewer.web.config;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.user.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

  private final UserDao userDao;

  @Autowired
  public UserDetailsServiceImpl(final UserDao userDao) {
    this.userDao = userDao;
  }

  @Override
  public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
    User user = userDao.getUserByLogin(username);
    if (user == null) {
      throw new UsernameNotFoundException(username);
    }
    List<GrantedAuthority> authorities = user.getPrivileges().stream()
        .map(privilege -> new SimpleGrantedAuthority(privilege.toString()))
        .collect(Collectors.toList());
    return new CustomUserDetails(org.springframework.security.core.userdetails.User
        .withUsername(username)
        .password(user.getCryptedPassword())
        .disabled(user.isRemoved() || !user.isActive() || username.equals(Configuration.ANONYMOUS_LOGIN))
        .authorities(authorities)
        .build(),
        user.getId());
  }
}