package lcsb.mapviewer.web.config;

import com.unboundid.ldap.sdk.LDAPException;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.UserDTO;
import lcsb.mapviewer.services.interfaces.ILdapService;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Order(2)
@Service
public class LdapAuthenticationProvider implements AuthenticationProvider {

  private final IUserService userService;
  private final ILdapService ldapService;
  private final UserDetailsService userDetailsService;

  @Autowired
  public LdapAuthenticationProvider(final IUserService userService,
                                    final ILdapService ldapService,
                                    final UserDetailsService userDetailsService) {
    this.userService = userService;
    this.ldapService = ldapService;
    this.userDetailsService = userDetailsService;
  }

  @Override
  public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
    String username = authentication.getName().toLowerCase();
    if (username.isEmpty()) {
      throw new BadCredentialsException("Invalid username.");
    }
    User existingUser = userService.getUserByLogin(username);

    if (existingUser != null) {
      if (!existingUser.isConnectedToLdap()) {
        throw new BadCredentialsException("User cannot authenticate over LDAP");
      }
    }

    boolean ldapLoginSuccess;
    try {
      ldapLoginSuccess = ldapService.login(username, (String) authentication.getCredentials());
    } catch (final LDAPException e) {
      throw new AuthenticationServiceException("Connection to LDAP service failed.", e);
    }

    if (!ldapLoginSuccess) {
      throw new BadCredentialsException("Invalid credentials or username.");
    }

    boolean userExistsLocally = existingUser != null;
    if (!userExistsLocally) {
      createLocalUser(authentication);
    }

    return new UsernamePasswordAuthenticationToken(
        username,
        authentication.getCredentials(),
        userDetailsService.loadUserByUsername(username).getAuthorities());
  }

  @Override
  public boolean supports(final Class<?> authentication) {
    return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
  }

  private void createLocalUser(final Authentication authentication) {
    UserDTO userDTO;
    try {
      userDTO = ldapService.getUserByLogin(authentication.getName());
    } catch (final LDAPException e) {
      throw new UsernameNotFoundException("Could not find username in LDAP.", e);
    }
    User newUser = new User();
    newUser.setLogin(userDTO.getLogin());
    newUser.setConnectedToLdap(true);
    newUser.setName(userDTO.getFirstName());
    newUser.setSurname(userDTO.getLastName());
    newUser.setEmail(userDTO.getEmail());
    // spring requires not null password - the password is hashed and the hash
    // would
    // never be equal to empty string
    newUser.setCryptedPassword("");
    userService.add(newUser);
    userService.grantDefaultPrivileges(newUser);
  }

}
