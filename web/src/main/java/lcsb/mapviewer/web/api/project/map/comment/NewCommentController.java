package lcsb.mapviewer.web.api.project.map.comment;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.map.CommentProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.ICommentService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.HashMap;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/comments",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewCommentController extends BaseController {

  private final IUserService userService;
  private final IModelService modelService;
  private final ICommentService commentService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewCommentController(final IUserService userService,
                              final IModelService modelService,
                              final ICommentService commentService,
                              final NewApiResponseSerializer serializer) {
    this.userService = userService;
    this.modelService = modelService;
    this.commentService = commentService;
    this.serializer = serializer;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping
  public ResponseEntity<?> getComments(
      final Pageable pageable,
      final Authentication authentication,
      final @PathVariable(value = "projectId") String projectId,
      final @Valid @Pattern(regexp = "^[-+]?\\d+$|^\\*$") @PathVariable(value = "mapId") String mapId,
      final @RequestParam(value = "removed", required = false) Boolean removed) throws QueryException, ObjectNotFoundException {

    final boolean isAdminView = checkAdminView(authentication, projectId);

    Map<CommentProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(CommentProperty.PROJECT_ID, projectId);
    filterOptions.put(CommentProperty.MAP_ID, mapId);

    if (removed != null) {
      filterOptions.put(CommentProperty.DELETED, removed);
    }
    if (!isAdminView) {
      filterOptions.put(CommentProperty.VISIBLE, true);
    }

    Page<Comment> page = commentService.getAll(filterOptions, pageable);

    if (isAdminView) {
      return serializer.prepareResponse(page, new AdminCommentResponseDecorator());
    } else {
      return serializer.prepareResponse(page);
    }
  }

  private boolean checkAdminView(final Authentication authentication, final String projectId) {
    final boolean isAdmin = authentication.getAuthorities()
        .contains(new SimpleGrantedAuthority(PrivilegeType.IS_ADMIN.name()));
    final boolean isProjectCurator = authentication.getAuthorities()
        .contains(new SimpleGrantedAuthority(PrivilegeType.IS_CURATOR.name()))
        && authentication.getAuthorities()
        .contains(new SimpleGrantedAuthority(PrivilegeType.READ_PROJECT.name() + ":" + projectId));

    return isAdmin || isProjectCurator;
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId)"
      + " or @commentService.getOwnerByCommentId(#projectId, #commentId)?.login == authentication.name")
  @DeleteMapping(value = "/{commentId}")
  public Object removeComment(
      final Authentication authentication,
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @RequestBody(required = false) NewRemoveCommentDTO body,
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "mapId") String mapId,
      final @PathVariable(value = "commentId") Integer commentId) throws QueryException, ObjectNotFoundException {

    final boolean isAdminView = checkAdminView(authentication, projectId);

    Comment comment = getComment(projectId, mapId, commentId, isAdminView);

    if (comment == null) {
      throw new ObjectNotFoundException("Comment with given id doesn't exist");
    }

    serializer.checkETag(oldETag, comment);

    if (body != null) {
      body.saveToComment(comment);
    }
    commentService.remove(comment);
    if (isAdminView) {
      return serializer.prepareResponse(comment, new AdminCommentResponseDecorator());
    } else {
      return serializer.prepareResponse(comment);
    }
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{commentId}")
  public ResponseEntity<?> getComment(
      final Authentication authentication,
      final @PathVariable(value = "projectId") String projectId,
      final @Valid @Pattern(regexp = "^[-+]?\\d+$|^\\*$") @PathVariable(value = "mapId") String mapId,
      final @PathVariable(value = "commentId") Integer commentId) throws ObjectNotFoundException {

    final boolean isAdminView = checkAdminView(authentication, projectId);

    Comment comment = getComment(projectId, mapId, commentId, isAdminView);

    if (isAdminView) {
      return serializer.prepareResponse(comment, new AdminCommentResponseDecorator());
    } else {
      return serializer.prepareResponse(comment);
    }
  }

  private Comment getComment(
      final String projectId,
      final String mapId,
      final Integer commentId,
      final boolean isAdminView
  ) throws ObjectNotFoundException {
    Map<CommentProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(CommentProperty.PROJECT_ID, projectId);
    filterOptions.put(CommentProperty.MAP_ID, mapId);
    filterOptions.put(CommentProperty.ID, commentId);

    if (!isAdminView) {
      filterOptions.put(CommentProperty.VISIBLE, true);
    }

    Page<Comment> page = commentService.getAll(filterOptions, Pageable.unpaged());

    if (page.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("No comment found");
    }
    return page.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @PostMapping
  public ResponseEntity<?> addCommentForElement(
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "mapId") Integer mapId,
      final Authentication authentication,
      final @Valid @RequestBody NewCommentDTO data
  ) throws QueryException, ObjectNotFoundException {
    final boolean isAdminView = checkAdminView(authentication, projectId);

    User user = userService.getUserByLogin(authentication.getName());
    if (user.getLogin().equals(Configuration.ANONYMOUS_LOGIN)) {
      user = null;
    }
    final ModelData model = modelService.getModelByMapId(projectId, mapId);
    if (model == null) {
      throw new ObjectNotFoundException("Map with given id doesn't exist");
    }

    Comment comment = new Comment();
    comment.setUser(user);
    comment.setModel(model);
    data.saveToComment(comment);


    commentService.add(comment);

    if (isAdminView) {
      return serializer.prepareResponse(comment, new AdminCommentResponseDecorator());
    } else {
      return serializer.prepareResponse(comment);
    }
  }

}