package lcsb.mapviewer.web.api.project.map.publication;

import lcsb.mapviewer.model.Article;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.web.api.AbstractDTO;

import java.util.List;

public class NewPublicationDTO extends AbstractDTO {

  private List<BioEntity> elements;
  private Article article;

  public List<BioEntity> getElements() {
    return elements;
  }

  public void setElements(final List<BioEntity> elements) {
    this.elements = elements;
  }

  public Article getArticle() {
    return article;
  }

  public void setArticle(final Article article) {
    this.article = article;
  }
}
