package lcsb.mapviewer.web.api.project.map.bioentity.reactions;

import java.util.ArrayList;
import java.util.List;

public class DownloadCsvRequest {
  private List<String> elementTypes = new ArrayList<>();
  private List<String> reactionTypes = new ArrayList<>();
  private List<Integer> submaps = new ArrayList<>();
  private List<String> columns = new ArrayList<>();
  private List<String> annotations = new ArrayList<>();
  private List<Integer> includedCompartmentIds = new ArrayList<>();
  private List<Integer> excludedCompartmentIds = new ArrayList<>();

  @Override
  public String toString() {
    return "reactionTypes: " + reactionTypes + "\n"
        + "elementTypes: " + elementTypes + "\n"
        + "submaps: " + submaps + "\n"
        + "columns: " + columns + "\n"
        + "annotations: " + annotations + "\n"
        + "includedCompartments: " + includedCompartmentIds + "\n"
        + "excludedCompoartments: " + excludedCompartmentIds;
  }

  public List<String> getElementTypes() {
    return elementTypes;
  }

  public void setElementTypes(final List<String> types) {
    this.elementTypes = types;
  }

  public List<Integer> getSubmaps() {
    return submaps;
  }

  public void setSubmaps(final List<Integer> submaps) {
    this.submaps = submaps;
  }

  public List<String> getColumns() {
    return columns;
  }

  public void setColumns(final List<String> columns) {
    this.columns = columns;
  }

  public List<Integer> getIncludedCompartmentIds() {
    return includedCompartmentIds;
  }

  public void setIncludedCompartmentIds(final List<Integer> includedCompartmentIds) {
    this.includedCompartmentIds = includedCompartmentIds;
  }

  public List<Integer> getExcludedCompartmentIds() {
    return excludedCompartmentIds;
  }

  public void setExcludedCompartmentIds(final List<Integer> excludedCompartmentIds) {
    this.excludedCompartmentIds = excludedCompartmentIds;
  }

  public List<String> getReactionTypes() {
    return reactionTypes;
  }

  public void setReactionTypes(final List<String> reactionTypes) {
    this.reactionTypes = reactionTypes;
  }

  public List<String> getAnnotations() {
    return annotations;
  }

  public void setAnnotations(final List<String> annotations) {
    this.annotations = annotations;
  }
}
