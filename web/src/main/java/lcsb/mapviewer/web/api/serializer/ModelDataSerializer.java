package lcsb.mapviewer.web.api.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lcsb.mapviewer.model.map.model.ModelData;

import java.io.IOException;

public class ModelDataSerializer extends JsonSerializer<ModelData> {

  @Override
  public void serialize(final ModelData entry, final JsonGenerator gen,
                        final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();
    gen.writeNumberField("id", entry.getId());
    gen.writeNumberField("width", entry.getWidth());
    gen.writeNumberField("height", entry.getHeight());
    gen.writeNumberField("tileSize", entry.getTileSize());
    gen.writeObjectField("defaultCenterX", entry.getDefaultCenterX());
    gen.writeObjectField("defaultCenterY", entry.getDefaultCenterY());
    gen.writeObjectField("defaultZoomLevel", entry.getDefaultZoomLevel());
    gen.writeStringField("description", entry.getNotes());
    gen.writeStringField("name", entry.getName());
    gen.writeObjectField("references", entry.getMiriamData());
    gen.writeObjectField("authors", entry.getAuthors());
    gen.writeObjectField("creationDate", entry.getCreationDate());
    gen.writeObjectField("modificationDates", entry.getModificationDates());
    gen.writeNumberField("minZoom", entry.getMinZoomLevel());
    gen.writeNumberField("maxZoom", entry.getMaxZoomLevel());
    gen.writeEndObject();
  }
}