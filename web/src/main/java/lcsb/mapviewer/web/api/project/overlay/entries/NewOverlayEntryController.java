package lcsb.mapviewer.web.api.project.overlay.entries;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.overlay.GenericDataOverlayEntry;
import lcsb.mapviewer.persist.dao.map.DataOverlayEntryProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IDataOverlayEntryService;
import lcsb.mapviewer.services.interfaces.IDataOverlayService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewOverlayEntryController extends BaseController {

  private final IDataOverlayService dataOverlayService;
  private final NewApiResponseSerializer serializer;
  private final IDataOverlayEntryService dataOverlayEntryService;

  public NewOverlayEntryController(
      final IDataOverlayService dataOverlayService,
      final NewApiResponseSerializer serializer,
      final IDataOverlayEntryService dataOverlayEntryService) {
    this.dataOverlayService = dataOverlayService;
    this.dataOverlayEntryService = dataOverlayEntryService;
    this.serializer = serializer;
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)"
      + " or hasAuthority('READ_PROJECT:' + #projectId) and "
      + "  (@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name "
      + "  or @dataOverlayService.getDataOverlayById(#overlayId)?.public == true)")
  @GetMapping(value = "/")
  public ResponseEntity<?> getOverlayEntryList(
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "overlayId") Integer overlayId,
      final Pageable pageable) {
    final Map<DataOverlayEntryProperty, Object> filter = new HashMap<>();
    filter.put(DataOverlayEntryProperty.DATA_OVERLAY_ID, overlayId);
    filter.put(DataOverlayEntryProperty.PROJECT_ID, projectId);
    final Page<DataOverlayEntry> result = dataOverlayEntryService.getAll(filter, pageable);
    return serializer.prepareResponse(result);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)"
      + " or hasAuthority('READ_PROJECT:' + #projectId) and "
      + "   (@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name "
      + "  or @dataOverlayService.getDataOverlayById(#overlayId)?.public == true)")
  @GetMapping(value = "/{overlayEntryId}/")
  public ResponseEntity<?> getOverlayEntry(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "overlayId") Integer overlayId,
      final @NotNull @PathVariable(value = "overlayEntryId") Integer overlayEntryId)
      throws ObjectNotFoundException {

    final DataOverlayEntry overlayEntry = getDataOverlayEntryFromService(projectId, overlayId, overlayEntryId);
    return serializer.prepareResponse(overlayEntry);
  }

  private DataOverlayEntry getDataOverlayEntryFromService(final String projectId,
                                                          final Integer overlayId,
                                                          final Integer overlayEntryId
  ) throws ObjectNotFoundException {
    final Map<DataOverlayEntryProperty, Object> filter = new HashMap<>();
    filter.put(DataOverlayEntryProperty.PROJECT_ID, projectId);
    filter.put(DataOverlayEntryProperty.DATA_OVERLAY_ID, overlayId);
    filter.put(DataOverlayEntryProperty.ID, overlayEntryId);
    final Page<DataOverlayEntry> overlayEntry = dataOverlayEntryService.getAll(filter, Pageable.unpaged());
    if (overlayEntry.getContent().isEmpty()) {
      throw new ObjectNotFoundException("Entry does not exist");
    }
    return overlayEntry.getContent().get(0);
  }

  @PreAuthorize("(@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name) ")
  @PostMapping(value = "/")
  public ResponseEntity<?> addOverlayEntry(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @RequestBody NewDataOverlayEntryDTO data,
      final @PathVariable(value = "overlayId") Integer overlayId)
      throws QueryException, ObjectNotFoundException {

    final DataOverlay overlay = dataOverlayService.getDataOverlayById(projectId, overlayId);

    final DataOverlayEntry overlayEntry = new GenericDataOverlayEntry();
    data.saveToDataOverlayEntry(overlayEntry, false);
    overlayEntry.setDataOverlay(overlay);
    dataOverlayEntryService.add(overlayEntry);

    return serializer.prepareResponse(overlayEntry, HttpStatus.CREATED);
  }

  @PreAuthorize("(@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name) ")
  @PutMapping(value = "/{overlayEntryId}")
  public ResponseEntity<?> updateOverlayEntry(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewDataOverlayEntryDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "overlayId") Integer overlayId,
      final @NotNull @PathVariable(value = "overlayEntryId") Integer overlayEntryId) throws QueryException, ObjectNotFoundException {
    final DataOverlayEntry overlayEntry = getDataOverlayEntryFromService(projectId, overlayId, overlayEntryId);
    serializer.checkETag(oldETag, overlayEntry);
    data.saveToDataOverlayEntry(overlayEntry, false);
    dataOverlayEntryService.update(overlayEntry);

    return getOverlayEntry(projectId, overlayId, overlayEntryId);
  }

  @PreAuthorize("(@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name) ")
  @DeleteMapping(value = "/{overlayEntryId}")
  public void deleteOverlayEntry(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "overlayId") Integer overlayId,
      final @Valid @NotNull @PathVariable(value = "overlayEntryId") Integer overlayEntryId)
      throws QueryException, ObjectNotFoundException {

    final DataOverlayEntry overlayEntry = getDataOverlayEntryFromService(projectId, overlayId, overlayEntryId);
    serializer.checkETag(oldETag, overlayEntry);
    dataOverlayEntryService.remove(overlayEntry);
  }

}