package lcsb.mapviewer.web.api.project;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.persist.dao.map.OverviewImageProperty;
import lcsb.mapviewer.services.FailedDependencyException;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IOverviewImageService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId:.+}/overviewImage",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewProjectOverviewImageController {

  private IOverviewImageService overviewImageService;
  private NewApiResponseSerializer serializer;

  @Autowired
  public NewProjectOverviewImageController(final NewApiResponseSerializer serializer, final IOverviewImageService overviewImageService) {
    this.overviewImageService = overviewImageService;
    this.serializer = serializer;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping
  public ResponseEntity<?> getOverviewImages(
      final @NotBlank @PathVariable(value = "projectId") String projectId,
      final Pageable pageable)
      throws ObjectNotFoundException, FailedDependencyException, IOException {
    Map<OverviewImageProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(OverviewImageProperty.PROJECT_ID, projectId);
    Page<OverviewImage> entries = overviewImageService.getAll(filterOptions, pageable);

    return serializer.prepareResponse(entries);

  }
}