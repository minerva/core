package lcsb.mapviewer.web.api.project.overlay.entries;

import lcsb.mapviewer.web.api.AbstractDTO;
import org.hibernate.validator.constraints.NotBlank;

public class NewCompartmentDTO extends AbstractDTO {

  @NotBlank
  private String compartment;

  public String getCompartment() {
    return compartment;
  }

  public void setCompartment(final String compartment) {
    this.compartment = compartment;
  }
}
