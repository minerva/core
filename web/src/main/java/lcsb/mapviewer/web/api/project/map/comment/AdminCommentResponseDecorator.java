package lcsb.mapviewer.web.api.project.map.comment;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.web.api.ResponseDecorator;

import java.util.ArrayList;
import java.util.List;

public class AdminCommentResponseDecorator implements ResponseDecorator {
  @Override
  public List<Pair<String, Object>> getDecoratedValues(final MinervaEntity object) {
    List<Pair<String, Object>> properties = new ArrayList<>();
    if (object instanceof Comment) {
      Comment comment = (Comment) object;
      String author = null;
      if (comment.getUser() != null) {
        author = comment.getUser().getLogin();
      }
      properties.add(new Pair<>("author", author));
      properties.add(new Pair<>("email", comment.getEmail()));
      properties.add(new Pair<>("removeReason", comment.getRemoveReason()));
    }
    return properties;
  }

  @Override
  public List<Pair<String, Object>> getDecoratedValuesForRaw(final Object object) {
    return new ArrayList<>();
  }
}
