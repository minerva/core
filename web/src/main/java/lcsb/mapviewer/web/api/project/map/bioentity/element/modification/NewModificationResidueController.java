package lcsb.mapviewer.web.api.project.map.bioentity.element.modification;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationResidue;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.persist.dao.map.species.ModificationResidueProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IModificationResidueService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/modifications/",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewModificationResidueController {

  protected static Logger logger = LogManager.getLogger();

  private final IProjectService projectService;
  private final IElementService elementService;
  private final IModificationResidueService modificationResidueService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewModificationResidueController(
      final NewApiResponseSerializer serializer,
      final IElementService elementService,
      final IModificationResidueService modificationResidueService,
      final IProjectService projectService) {
    this.modificationResidueService = modificationResidueService;
    this.serializer = serializer;
    this.elementService = elementService;
    this.projectService = projectService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{modificationId}")
  public ResponseEntity<?> getModification(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @NotNull @PathVariable(value = "modificationId") Integer modificationId)
      throws ObjectNotFoundException {
    final ModificationResidue modification = getModificationFromService(projectId, mapId, elementId, modificationId);
    return serializer.prepareResponse(modification);
  }

  private ModificationResidue getModificationFromService(final String projectId, final Integer mapId, final Integer elementId,
                                                         final Integer modificationId)
      throws ObjectNotFoundException {
    final Map<ModificationResidueProperty, Object> properties = new HashMap<>();
    properties.put(ModificationResidueProperty.ID, Collections.singletonList(modificationId));
    properties.put(ModificationResidueProperty.ELEMENT_ID, Collections.singletonList(elementId));
    properties.put(ModificationResidueProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(ModificationResidueProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<ModificationResidue> elements = modificationResidueService.getAll(properties, Pageable.unpaged());
    if (elements.isEmpty()) {
      throw new ObjectNotFoundException("Modification Residue does not exist");
    }
    return elements.getContent().get(0);
  }

  private Element getElement(final String projectId, final Integer mapId, final Integer elementId) throws ObjectNotFoundException {
    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.ID, Collections.singletonList(elementId));
    properties.put(ElementProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(ElementProperty.PROJECT_ID, Collections.singletonList(projectId));
    final List<Element> elements = elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent();
    if (elements.isEmpty()) {
      throw new ObjectNotFoundException("Modification Residue does not exist");
    }
    return elements.get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addModificationResidue(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @RequestBody NewModificationResidueDTO data)
      throws QueryException, ObjectNotFoundException {

    final Element element = getElement(projectId, mapId, elementId);
    if (!(element instanceof SpeciesWithModificationResidue)) {
      throw new QueryException("Modification Residue cannot be added to a given element");
    }

    final Species species = (Species) element;

    final Class<? extends ModificationResidue> clazz = data.getClazz();
    final ModificationResidue mr;
    try {
      mr = clazz.getConstructor(String.class).newInstance(data.getModificationResidueId());
    } catch (final Exception e) {
      throw new QueryException("Invalid element class", e);
    }

    data.saveToModificationResidue(mr, projectService.getBackgrounds(projectId, false).isEmpty());
    mr.setSpecies(species);
    modificationResidueService.add(mr);

    return serializer.prepareResponse(mr, HttpStatus.CREATED);
  }


  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{modificationId}")
  public ResponseEntity<?> updateModification(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewModificationResidueDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @NotNull @PathVariable(value = "modificationId") Integer modificationId
  ) throws QueryException, ObjectNotFoundException {
    final ModificationResidue modification = getModificationFromService(projectId, mapId, elementId, modificationId);
    serializer.checkETag(oldETag, modification);
    data.saveToModificationResidue(modification, projectService.getBackgrounds(projectId, false).isEmpty());
    modificationResidueService.update(modification);

    return serializer.prepareResponse(getModificationFromService(projectId, mapId, elementId, modificationId));
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{modificationId}")
  public void deleteModification(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @NotNull @PathVariable(value = "modificationId") Integer modificationId
  ) throws QueryException, ObjectNotFoundException {
    final ModificationResidue modification = getModificationFromService(projectId, mapId, elementId, modificationId);
    if (modification == null) {
      throw new ObjectNotFoundException("Modification does not exist");
    }
    serializer.checkETag(oldETag, modification);

    modificationResidueService.remove(modification);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> getModifications(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId) {
    final Map<ModificationResidueProperty, Object> properties = new HashMap<>();
    properties.put(ModificationResidueProperty.ELEMENT_ID, Collections.singletonList(elementId));
    properties.put(ModificationResidueProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(ModificationResidueProperty.PROJECT_ID, Collections.singletonList(projectId));

    final Page<ModificationResidue> modifications = modificationResidueService.getAll(properties, Pageable.unpaged());

    return serializer.prepareResponse(modifications);
  }

}