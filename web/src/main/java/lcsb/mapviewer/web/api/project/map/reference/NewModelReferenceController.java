package lcsb.mapviewer.web.api.project.map.reference;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IMiriamService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.NewMiriamDataDTO;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/references",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewModelReferenceController {

  private IModelService modelService;
  private IMiriamService referenceService;
  private NewApiResponseSerializer serializer;

  @Autowired
  public NewModelReferenceController(
      final IModelService modelService,
      final NewApiResponseSerializer serializer,
      final IMiriamService referenceService) {
    this.modelService = modelService;
    this.serializer = serializer;
    this.referenceService = referenceService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{referenceId}")
  public ResponseEntity<?> getReference(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "referenceId") Integer referenceId)
      throws QueryException, ObjectNotFoundException {
    MiriamData reference = getReferenceFromService(projectId, mapId, referenceId);
    return serializer.prepareResponse(reference);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addReference(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @RequestBody NewMiriamDataDTO data)
      throws QueryException, ObjectNotFoundException {

    ModelData model = modelService.getModelByMapId(projectId, mapId);
    if (model == null) {
      throw new ObjectNotFoundException("Map does not exist");
    }

    MiriamData reference = new MiriamData();
    data.saveToMiriamData(reference);
    model.addMiriamData(reference);
    modelService.update(model);

    return serializer.prepareResponse(getReferenceFromService(projectId, mapId, reference.getId()), HttpStatus.CREATED);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{referenceId}")
  public ResponseEntity<?> updateReference(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewMiriamDataDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "referenceId") Integer referenceId)
      throws QueryException, ObjectNotFoundException {
    MiriamData reference = getReferenceFromService(projectId, mapId, referenceId);
    serializer.checkETag(oldETag, reference);
    data.saveToMiriamData(reference);
    referenceService.update(reference);

    return serializer.prepareResponse(referenceService.getById(referenceId));
  }

  private MiriamData getReferenceFromService(final String projectId, final Integer mapId, final Integer referenceId)
      throws QueryException, ObjectNotFoundException {
    MiriamData reference = null;
    ModelData model = modelService.getModelByMapId(projectId, mapId);
    if (model != null) {
      for (MiriamData md : model.getMiriamData()) {
        if (md.getId() == referenceId) {
          reference = md;
        }
      }
    }

    if (reference == null) {
      throw new ObjectNotFoundException("Reference does not exist");
    }
    return reference;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{referenceId}")
  public void deleteReference(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "referenceId") Integer referenceId)
      throws QueryException, ObjectNotFoundException {
    MiriamData reference = getReferenceFromService(projectId, mapId, referenceId);
    if (reference == null) {
      throw new ObjectNotFoundException("Reference does not exist");
    }
    serializer.checkETag(oldETag, reference);
    referenceService.delete(reference);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listReferences(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId) throws QueryException, ObjectNotFoundException {
    ModelData model = modelService.getModelByMapId(projectId, mapId);
    if (model == null) {
      throw new ObjectNotFoundException("Map does not exist");
    }
    List<MiriamData> references = new ArrayList<>(model.getMiriamData());

    references.sort(new Comparator<MinervaEntity>() {
      @Override
      public int compare(final MinervaEntity o1, final MinervaEntity o2) {
        return o1.getId() - o2.getId();
      }
    });

    return serializer.prepareResponse(serializer.createPage(references, pageable));
  }

}