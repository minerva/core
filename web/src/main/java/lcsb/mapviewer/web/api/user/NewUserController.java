package lcsb.mapviewer.web.api.user;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.unboundid.ldap.sdk.LDAPException;
import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.OperationNotAllowedException;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.api.UpdateConflictException;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.exception.InvalidArgumentException;
import lcsb.mapviewer.common.exception.InvalidStateException;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.modelutils.serializer.model.security.PrivilegeKeyDeserializer;
import lcsb.mapviewer.persist.dao.user.UserProperty;
import lcsb.mapviewer.services.InvalidTokenException;
import lcsb.mapviewer.services.ObjectExistsException;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IAnnotationService;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.ILdapService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.services.utils.EmailException;
import lcsb.mapviewer.services.utils.EmailSender;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/users",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewUserController extends BaseController {

  private final IUserService userService;
  private final ILdapService ldapService;
  private final IAnnotationService annotationService;
  private final IConfigurationService configurationService;
  private final PasswordEncoder passwordEncoder;
  private final EmailSender emailSender;

  private final NewApiResponseSerializer serializer;
  private final NewUserResponseDecorator userResponseDecorator;
  private final List<String> ldapUsernames = new ArrayList<>();

  @Autowired
  public NewUserController(final IUserService userService,
                           final PasswordEncoder passwordEncoder,
                           final EmailSender emailSender,
                           final IConfigurationService configurationService,
                           final ILdapService ldapService,
                           final IAnnotationService annotationService,
                           final SessionRegistry sessionRegistry,
                           final NewApiResponseSerializer serializer) {
    this.userService = userService;
    this.passwordEncoder = passwordEncoder;
    this.emailSender = emailSender;
    this.configurationService = configurationService;
    this.serializer = serializer;
    this.ldapService = ldapService;
    this.annotationService = annotationService;
    this.userResponseDecorator = new NewUserResponseDecorator(ldapUsernames, sessionRegistry);
    this.refreshLdapUsernames();
  }

  private void refreshLdapUsernames() {
    try {
      ldapUsernames.clear();
      ldapUsernames.addAll(ldapService.getUsernames());
    } catch (LDAPException e) {
      logger.trace("LDAP connection not configured", e);
    }
  }

  /**
   * This API action is protected by spring security and will fail if the user is
   * not authenticated. Therefore, simply calling this action can be used to detect
   * whether the current session is still valid.
   */
  @GetMapping(value = "/isSessionValid")
  @PreAuthorize("isAuthenticated() and authentication.name != '" + Configuration.ANONYMOUS_LOGIN + "'")
  public Object isSessionValid(final Authentication authentication, final HttpServletRequest request) {
    return new NewAuthenticationInfoDTO(authentication.getName(), request.getSession().getId());
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'IS_CURATOR')  or #userId == authentication.principal.id")
  @GetMapping(value = "/{userId}")
  public ResponseEntity<?> getUser(final @PathVariable(value = "userId") Integer userId) throws ObjectNotFoundException {
    final User user = userService.getById(userId);
    if (user == null) {
      throw new ObjectNotFoundException("User doesn't exist");
    }
    if (user.getAnnotationSchema() == null) {
      user.setAnnotationSchema(annotationService.prepareUserAnnotationSchema(user, true));
    }
    return serializer.prepareResponse(user, this.userResponseDecorator);
  }

  public static class UserPrivilegesDTO {
    @JsonDeserialize(keyUsing = PrivilegeKeyDeserializer.class)
    private Map<Privilege, Boolean> privileges = new HashMap<>();

    public Map<Privilege, Boolean> getPrivileges() {
      return privileges;
    }

    public void setPrivileges(final Map<Privilege, Boolean> privileges) {
      this.privileges = privileges;
    }
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @PostMapping(value = "/{userId}:grantPrivilege", consumes = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<?> grantPrivilege(
      final @NotNull @RequestBody NewUserPrivilegeDTO data,
      final @PathVariable(value = "userId") int userId) throws QueryException, ObjectNotFoundException {
    final User user = userService.getById(userId);
    if (user == null) {
      throw new QueryException("User does not exist.");
    }
    userService.grantUserPrivilege(user, data.getPrivilegeType(), data.getObjectId());
    return serializer.prepareResponse(user, this.userResponseDecorator);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @PostMapping(value = "/{userId}:revokePrivilege", consumes = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<?> revokePrivilege(
      final @NotNull @RequestBody NewUserPrivilegeDTO data,
      final @PathVariable(value = "userId") int userId) throws QueryException, ObjectNotFoundException {
    final User user = userService.getById(userId);
    if (user == null) {
      throw new QueryException("User does not exist.");
    }
    userService.revokeUserPrivilege(user, data.getPrivilegeType(), data.getObjectId());
    return serializer.prepareResponse(user, this.userResponseDecorator);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'IS_CURATOR')")
  @GetMapping(value = "/")
  public ResponseEntity<?> getUsers(
      final Pageable pageable
  )
      throws IOException {
    Map<UserProperty, Object> filter = new HashMap<>();
    final Page<User> users = userService.getAll(filter, pageable);
    this.refreshLdapUsernames();
    return serializer.prepareResponse(users, this.userResponseDecorator);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN') or #userId == authentication.principal.id")
  @PutMapping(value = "/{userId}")
  public ResponseEntity<?> updateUser(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewUserDTO data,
      final @PathVariable(value = "userId") Integer userId,
      final Authentication authentication) throws QueryException, IOException, ObjectNotFoundException {

    final boolean isAdmin = authentication.getAuthorities().contains(new SimpleGrantedAuthority(PrivilegeType.IS_ADMIN.toString()));

    final User user = userService.getById(userId);
    if (user == null) {
      throw new ObjectNotFoundException("user doesn't exist");
    }
    final User oldUser = userService.getUserByOrcidId(data.getOrcidId());
    if (oldUser != null && !Objects.equals(oldUser.getLogin(), user.getLogin()) && !data.getOrcidId().trim().isEmpty()) {
      throw new UpdateConflictException("Orcid id is already assigned to another user");
    }

    serializer.checkETag(oldETag, user);

    boolean sendActivation = !user.isActive() && data.getActive();

    data.saveToUser(user, passwordEncoder, isAdmin);

    userService.update(user);
    if (sendActivation) {
      userService.sendUserActivatedEmail(user);
    }

    return serializer.prepareResponse(user, this.userResponseDecorator);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @PostMapping(value = "/")
  public ResponseEntity<?> addUser(
      final @Valid @RequestBody NewUserDTO data) throws QueryException, ObjectExistsException, ObjectNotFoundException {
    User user = userService.getUserByLogin(data.getLogin());
    if (user != null) {
      throw new ObjectExistsException("user exists");
    }

    user = new User();
    data.saveToUser(user, passwordEncoder, true);
    userService.add(user);
    return serializer.prepareResponse(user, this.userResponseDecorator, HttpStatus.CREATED);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')")
  @DeleteMapping(value = "/{userId}")
  public void removeUser(
      final @PathVariable(value = "userId") Integer userId,
      final @RequestHeader(name = "If-Match", required = false) String oldETag
  ) throws Exception {
    final User user = userService.getById(userId);
    if (user == null) {
      throw new ObjectNotFoundException("user doesn't exists");
    } else if (user.getLogin().equals(Configuration.ANONYMOUS_LOGIN)) {
      throw new OperationNotAllowedException("guest account cannot be removed");
    }
    serializer.checkETag(oldETag, user);
    userService.remove(user);
  }

  @PostMapping(value = "/{login}:requestResetPassword")
  public Object requestResetPasswordToken(
      final @PathVariable(value = "login") String login) throws QueryException, ObjectNotFoundException {
    final User user = userService.getUserByLogin(login);
    if (user == null) {
      throw new ObjectNotFoundException("User does not exist");
    }
    if (user.getEmail() == null || user.getEmail().isEmpty()) {
      throw new QueryException("User does not have email address defined");
    }
    if (user.isConnectedToLdap()) {
      throw new QueryException("User authentication is provided over LDAP");
    }

    if (configurationService.getConfigurationValue(ConfigurationElementType.MINERVA_ROOT).trim().isEmpty()) {
      throw new InvalidStateException("Cannot create token - minerva root url is not defined");
    }

    if (!emailSender.canSendEmails()) {
      throw new InvalidStateException("Cannot create token - minerva cannot send emails");
    }

    try {
      userService.createResetPasswordToken(user);
      return okStatus();
    } catch (EmailException e) {
      throw new InvalidStateException("Cannot create token - minerva could not send an email", e);
    }
  }

  @PostMapping(value = ":resetPassword")
  public Object resetPassword(
      final @Valid @RequestBody ResetPasswordDTO data)
      throws IOException, QueryException, ObjectNotFoundException {
    try {
      userService.resetPassword(data.getToken(), passwordEncoder.encode(data.getPassword()));
      return okStatus();
    } catch (final InvalidTokenException e) {
      throw new ObjectNotFoundException("Invalid token", e);
    }

  }

  @PostMapping(value = "/{login}:confirmEmail")
  public Object confirmEmail(
      final @PathVariable(value = "login") String login,
      final @RequestParam(value = "token", required = true) String token) throws QueryException, EmailException, ObjectNotFoundException {
    try {
      final User user = userService.confirmEmail(login, token);
      final String message;
      if (user.isActive()) {
        message = "Your email is confirmed. You can login";
      } else {
        message = "Your email is confirmed. You need to wait for admin approval before you can login";
      }
      final Map<String, Object> result = okStatus();
      result.put("message", message);
      return result;
    } catch (final InvalidTokenException e) {
      throw new ObjectNotFoundException("Invalid token", e);
    }

  }

  @PostMapping(value = ":registerUser")
  public ResponseEntity<?> registerUser(
      @Valid final @RequestBody NewUserDTO data) throws QueryException, MessagingException, ObjectNotFoundException {
    if (!configurationService.getConfigurationValue(ConfigurationElementType.ALLOW_AUTO_REGISTER).equalsIgnoreCase("true")) {
      throw new QueryException("Registration of new users is not enabled.");
    }
    if (!emailSender.canSendEmails()) {
      throw new QueryException("Cannot register new user. Cannot send emails from minerva");
    }
    final String minervaRootUrl = configurationService.getConfigurationValue(ConfigurationElementType.MINERVA_ROOT);
    if (!new UrlValidator().isValid(minervaRootUrl)) {
      throw new QueryException("Cannot send confirmation email.");
    }

    final User user = new User();
    data.saveToUser(user, passwordEncoder, false);

    try {
      userService.registerUser(user);
      userService.grantDefaultPrivileges(user);
      return serializer.prepareResponse(user, this.userResponseDecorator);
    } catch (EmailException e) {
      throw new InvalidStateException("Cannot register user. Minerva could not send an email", e);
    } catch (InvalidArgumentException e) {
      throw new QueryException("Cannot register user. " + e.getMessage(), e);
    }
  }

  @PostMapping(value = "/{login}:resendConfirmEmail")
  public Object resendConfirmationEmail(
      final @PathVariable(value = "login") String login) throws QueryException, MessagingException, ObjectNotFoundException {
    final User user = userService.getUserByLogin(login);
    if (user == null) {
      throw new QueryException("Cannot send confirmation email. User does not exist");
    }
    if (user.isConfirmed()) {
      throw new QueryException("Cannot send confirmation email. Account already confirmed");
    }
    if (!emailSender.canSendEmails()) {
      throw new QueryException("Cannot send confirmation email.");
    }
    final String minervaRootUrl = configurationService.getConfigurationValue(ConfigurationElementType.MINERVA_ROOT);
    if (!new UrlValidator().isValid(minervaRootUrl)) {
      throw new QueryException("Cannot send confirmation email.");
    }

    try {
      userService.createRegisterUserToken(user);
      return okStatus();
    } catch (EmailException e) {
      throw new InvalidStateException("Cannot create token - minerva could not send an email", e);
    }
  }

}