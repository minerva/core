package lcsb.mapviewer.web.api.project.map.bioentity.reaction.operator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.map.reaction.AbstractNode;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.persist.dao.map.ReactionProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.INodeService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.NewIdDTO;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators/{operatorId}/outputs",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewOperatorOutputController {

  private IReactionService reactionService;
  private NewApiResponseSerializer serializer;
  private INodeService nodeService;

  @Autowired
  public NewOperatorOutputController(final IReactionService reactionService,
      final NewApiResponseSerializer serializer,
      final INodeService nodeService) {
    this.reactionService = reactionService;
    this.serializer = serializer;
    this.nodeService = nodeService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping("/{outputId}")
  public ResponseEntity<?> getOutput(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @NotNull @PathVariable(value = "operatorId") Integer operatorId,
      final @NotNull @PathVariable(value = "outputId") Integer outputId)
      throws QueryException, ObjectNotFoundException {
    NodeOperator operator = getOperatorFromService(projectId, mapId, reactionId, operatorId);
    AbstractNode result = null;
    for (AbstractNode output : operator.getOutputs()) {
      if (output.getId() == outputId) {
        result = output;
      }
    }
    if (result == null) {
      throw new ObjectNotFoundException("Object does not exist");
    }

    return serializer.prepareResponse(result);

  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addOutput(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @NotNull @PathVariable(value = "operatorId") Integer operatorId,
      final @Valid @RequestBody NewIdDTO data)
      throws QueryException, ObjectNotFoundException {

    NodeOperator operator = getOperatorFromService(projectId, mapId, reactionId, operatorId);
    serializer.checkETag(oldETag, operator);

    AbstractNode output = null;

    for (AbstractNode node : operator.getReaction().getNodes()) {
      if (node.getId() == data.getId()) {
        output = node;
      }
    }
    if (output == null) {
      throw new ObjectNotFoundException("Output does not exist");
    }

    if (output.getNodeOperatorForOutput() != null) {
      throw new QueryException("Output is already assigned to operator");
    }

    operator.addOutput(output);

    nodeService.update(output);

    return serializer.prepareResponse(output);
  }

  private NodeOperator getOperatorFromService(final String projectId, final Integer mapId, final Integer reactionId, final Integer operatorId)
      throws ObjectNotFoundException {
    Map<ReactionProperty, Object> properties = new HashMap<>();
    properties.put(ReactionProperty.ID, Arrays.asList(reactionId));
    properties.put(ReactionProperty.MAP_ID, Arrays.asList(mapId));
    properties.put(ReactionProperty.PROJECT_ID, Arrays.asList(projectId));
    Page<Reaction> reactions = reactionService.getAll(properties, Pageable.unpaged(), true);
    if (reactions.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Reaction does not exist");
    }
    Reaction reaction = reactions.getContent().get(0);
    NodeOperator operator = null;
    for (NodeOperator md : reaction.getOperators()) {
      if (md.getId() == operatorId) {
        operator = md;
      }
    }

    if (operator == null) {
      throw new ObjectNotFoundException("Operator does not exist");
    }
    return operator;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{outputId}")
  public void deleteOutput(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @NotNull @PathVariable(value = "operatorId") Integer operatorId,
      final @Valid @NotNull @PathVariable(value = "outputId") Integer outputId)
      throws QueryException, ObjectNotFoundException {
    NodeOperator operator = getOperatorFromService(projectId, mapId, reactionId, operatorId);
    AbstractNode output = null;
    for (AbstractNode node : operator.getOutputs()) {
      if (node.getId() == outputId) {
        output = node;
      }
    }
    if (output == null) {
      throw new ObjectNotFoundException("Object does not exist");
    }

    serializer.checkETag(oldETag, output);
    output.setNodeOperatorForOutput(null);
    nodeService.update(output);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listOutputs(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @NotNull @PathVariable(value = "operatorId") Integer operatorId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId) throws QueryException, ObjectNotFoundException {
    NodeOperator operator = getOperatorFromService(projectId, mapId, reactionId, operatorId);
    List<AbstractNode> nodes = new ArrayList<>(operator.getOutputs());

    nodes.sort(new Comparator<MinervaEntity>() {
      @Override
      public int compare(final MinervaEntity o1, final MinervaEntity o2) {
        return o1.getId() - o2.getId();
      }
    });

    return serializer.prepareResponse(serializer.createPage(nodes, pageable));
  }

}