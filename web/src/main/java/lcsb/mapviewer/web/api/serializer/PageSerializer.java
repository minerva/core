package lcsb.mapviewer.web.api.serializer;

import java.io.IOException;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;

@SuppressWarnings("rawtypes")
@Component
public class PageSerializer extends JsonSerializer<Page> {

  @Override
  public void serialize(final Page page, final JsonGenerator jsonGenerator, final SerializerProvider serializerProvider)
      throws IOException {
    jsonGenerator.writeStartObject();
    ((ObjectMapper) jsonGenerator.getCodec()).setConfig(serializerProvider.getConfig());
    jsonGenerator.writeObjectField("content", page.getContent());
    jsonGenerator.writeNumberField("totalPages", page.getTotalPages());
    jsonGenerator.writeNumberField("totalElements", page.getTotalElements());
    jsonGenerator.writeNumberField("numberOfElements", page.getNumberOfElements());
    jsonGenerator.writeNumberField("size", page.getSize());
    jsonGenerator.writeNumberField("number", page.getNumber());
    jsonGenerator.writeEndObject();
  }
}