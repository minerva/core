package lcsb.mapviewer.web.api.project.map.author;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.map.AuthorProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IAuthorService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/authors"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewAuthorController {

  private IModelService modelService;
  private IAuthorService authorService;
  private NewApiResponseSerializer serializer;

  @Autowired
  public NewAuthorController(
      final IModelService modelService,
      final NewApiResponseSerializer serializer,
      final IAuthorService authorService) {
    this.modelService = modelService;
    this.serializer = serializer;
    this.authorService = authorService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{authorId}")
  public ResponseEntity<?> getAuthor(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "authorId") Integer authorId)
      throws QueryException, ObjectNotFoundException {
    Author author = getAuthorFromService(projectId, mapId, authorId);
    return serializer.prepareResponse(author);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addAuthor(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @RequestBody NewAuthorDTO data)
      throws QueryException, ObjectNotFoundException {

    ModelData model = modelService.getModelByMapId(projectId, mapId);
    if (model == null) {
      throw new ObjectNotFoundException("Map does not exist");
    }

    Author author = new Author("", "");
    data.saveToAuthor(author);
    model.addAuthor(author);
    modelService.update(model);

    return serializer.prepareResponse(getAuthorFromService(projectId, mapId, author.getId()), HttpStatus.CREATED);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{authorId}")
  public ResponseEntity<?> updateAuthor(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewAuthorDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "authorId") Integer authorId)
      throws QueryException, ObjectNotFoundException {
    Author author = getAuthorFromService(projectId, mapId, authorId);
    serializer.checkETag(oldETag, author);
    data.saveToAuthor(author);
    authorService.update(author);

    return serializer.prepareResponse(authorService.getById(authorId));
  }

  private Author getAuthorFromService(final String projectId, final Integer mapId, final Integer authorId) throws ObjectNotFoundException {
    Map<AuthorProperty, Object> filter = new HashMap<>();
    filter.put(AuthorProperty.PROJECT_ID, projectId);
    filter.put(AuthorProperty.MAP_ID, mapId);
    filter.put(AuthorProperty.ID, authorId);
    Page<Author> page = authorService.getAll(filter, Pageable.unpaged());
    if (page.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Author does not exist");
    }
    Author author = page.getContent().get(0);
    return author;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{authorId}")
  public void deleteAuthor(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "authorId") Integer authorId)
      throws QueryException, ObjectNotFoundException {
    Author author = getAuthorFromService(projectId, mapId, authorId);
    if (author == null) {
      throw new ObjectNotFoundException("Author does not exist");
    }
    serializer.checkETag(oldETag, author);
    authorService.remove(author);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listAuthors(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId) {
    Map<AuthorProperty, Object> filter = new HashMap<>();
    filter.put(AuthorProperty.PROJECT_ID, projectId);
    filter.put(AuthorProperty.MAP_ID, mapId);

    return serializer.prepareResponse(authorService.getAll(filter, pageable));
  }

}