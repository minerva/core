package lcsb.mapviewer.web.api.project.map.function;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.map.kinetics.SbmlFunctionProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.ISbmlFunctionService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/functions",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewModelFunctionController {

  private IModelService modelService;
  private ISbmlFunctionService functionService;
  private NewApiResponseSerializer serializer;

  @Autowired
  public NewModelFunctionController(
      final IModelService modelService,
      final NewApiResponseSerializer serializer,
      final ISbmlFunctionService functionService) {
    this.modelService = modelService;
    this.serializer = serializer;
    this.functionService = functionService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{functionId}")
  public ResponseEntity<?> getFunction(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "functionId") Integer functionId)
      throws QueryException, ObjectNotFoundException {
    SbmlFunction function = getFunctionFromService(projectId, mapId, functionId);
    return serializer.prepareResponse(function);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addFunction(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @RequestBody NewFunctionDTO data)
      throws QueryException, ObjectNotFoundException {

    ModelData model = modelService.getModelByMapId(projectId, mapId);
    if (model == null) {
      throw new ObjectNotFoundException("Map does not exist");
    }

    SbmlFunction function = new SbmlFunction("");
    data.saveToSbmlFunction(function);
    function.setModel(model);
    functionService.add(function);

    return serializer.prepareResponse(getFunctionFromService(projectId, mapId, function.getId()), HttpStatus.CREATED);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{functionId}")
  public ResponseEntity<?> updateFunction(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewFunctionDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "functionId") Integer functionId)
      throws QueryException, ObjectNotFoundException {
    SbmlFunction function = getFunctionFromService(projectId, mapId, functionId);
    serializer.checkETag(oldETag, function);
    data.saveToSbmlFunction(function);
    functionService.update(function);

    return serializer.prepareResponse(functionService.getById(functionId));
  }

  private SbmlFunction getFunctionFromService(final String projectId, final Integer mapId, final Integer functionId)
      throws QueryException, ObjectNotFoundException {
    Map<SbmlFunctionProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(SbmlFunctionProperty.PROJECT_ID, projectId);
    filterOptions.put(SbmlFunctionProperty.MAP_ID, mapId);
    filterOptions.put(SbmlFunctionProperty.ID, functionId);
    Page<SbmlFunction> page = functionService.getAll(filterOptions, Pageable.unpaged());

    if (page.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Function does not exist");
    }
    return page.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{functionId}")
  public void deleteFunction(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "functionId") Integer functionId)
      throws QueryException, ObjectNotFoundException {
    SbmlFunction function = getFunctionFromService(projectId, mapId, functionId);
    if (function == null) {
      throw new ObjectNotFoundException("Function does not exist");
    }
    serializer.checkETag(oldETag, function);
    functionService.remove(function);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listFunctions(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId) throws QueryException {
    Map<SbmlFunctionProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(SbmlFunctionProperty.PROJECT_ID, projectId);
    filterOptions.put(SbmlFunctionProperty.MAP_ID, mapId);
    Page<SbmlFunction> page = functionService.getAll(filterOptions, pageable);

    return serializer.prepareResponse(page);
  }

}