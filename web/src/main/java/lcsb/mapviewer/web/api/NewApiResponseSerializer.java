package lcsb.mapviewer.web.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import lcsb.mapviewer.api.CustomPage;
import lcsb.mapviewer.api.PreconditionFailedException;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.job.MinervaJob;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.modelutils.serializer.CalendarSerializer;
import lcsb.mapviewer.modelutils.serializer.ColorSerializer;
import lcsb.mapviewer.modelutils.serializer.Line2DSerializer;
import lcsb.mapviewer.modelutils.serializer.model.map.MiriamDataSerializer;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IMiriamService;
import lcsb.mapviewer.web.api.serializer.ColorDeserializer;
import lcsb.mapviewer.web.api.serializer.ModelDataSerializer;
import lcsb.mapviewer.web.api.serializer.NewCustomPageSerializer;
import lcsb.mapviewer.web.api.serializer.OverviewImageSerializer;
import lcsb.mapviewer.web.api.serializer.PageDeserializer;
import lcsb.mapviewer.web.api.serializer.PageSerializer;
import lcsb.mapviewer.web.api.serializer.ProjectSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.awt.Color;
import java.awt.geom.Line2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Old API (<17.0) uses different set of serializers. Therefore, for the new API
 * serialization is done here.
 */
@Component
public class NewApiResponseSerializer {

  @SuppressWarnings("unused")
  private static final Logger logger = LogManager.getLogger();

  private final ObjectMapper objectMapper;

  @Autowired
  private IMiriamService miriamService;

  public NewApiResponseSerializer() {
    objectMapper = new ObjectMapper();
    final SimpleModule module = new SimpleModule();
    module.addSerializer(Calendar.class, new CalendarSerializer());
    module.addSerializer(Color.class, new ColorSerializer());
    module.addSerializer(Line2D.class, new Line2DSerializer());
    module.addSerializer(Page.class, new PageSerializer());
    module.addSerializer(CustomPage.class, new NewCustomPageSerializer());

    module.addSerializer(Project.class, new ProjectSerializer());
    module.addSerializer(OverviewImage.class, new OverviewImageSerializer());
    module.addSerializer(ModelData.class, new ModelDataSerializer());

    module.addDeserializer(Page.class, new PageDeserializer());
    module.addDeserializer(Color.class, new ColorDeserializer());

    module.addSerializer(MiriamData.class, new MiriamDataSerializer());

    objectMapper.registerModule(module);
  }

  public ResponseEntity<?> prepareResponse(final MinervaEntity object) throws ObjectNotFoundException {
    return prepareResponse(object, ResponseDecorator.EMPTY, HttpStatus.OK);
  }

  public ResponseEntity<?> prepareResponse(final MinervaEntity object, final ResponseDecorator responseDecorator) throws ObjectNotFoundException {
    return prepareResponse(object, responseDecorator, HttpStatus.OK);
  }

  public ResponseEntity<?> prepareResponse(final MinervaEntity object, final HttpStatus status) throws ObjectNotFoundException {
    return prepareResponse(object, ResponseDecorator.EMPTY, status);
  }

  public ResponseEntity<?> prepareResponse(final MinervaEntity object, final ResponseDecorator responseDecorator, final HttpStatus httpStatus)
      throws ObjectNotFoundException {
    if (object == null) {
      throw new ObjectNotFoundException("Object not found");
    }
    try {
      final Map<String, Object> result = decorateObject(object, responseDecorator);
      return ResponseEntity.status(httpStatus)
          .eTag(object.getEntityVersion() + "")
          .body(result);

    } catch (final IOException e) {
      throw new InternalServerErrorException("Problem with response serialization", e);
    }
  }

  public ResponseEntity<?> prepareResponse(final Page<?> objects, final ResponseDecorator responseDecorator) {
    try {
      final List<Map<String, Object>> content = new ArrayList<>();
      for (final Object entity : objects.getContent()) {
        content.add(decorateObject(entity, responseDecorator));
      }
      final Page<?> page;
      if (objects instanceof CustomPage) {
        page = new CustomPage<>(content, objects.getPageable(), objects.getTotalElements(), ((CustomPage<?>) objects).getFiltered());
      } else {
        page = new PageImpl<>(content, objects.getPageable(), objects.getTotalElements());
      }


      final String json = objectMapper.writeValueAsString(page);
      final Object result = objectMapper.readValue(json, new TypeReference<Map<String, Object>>() {
      });

      return ResponseEntity.status(HttpStatus.OK)
          .body(result);
    } catch (final IOException e) {
      throw new InternalServerErrorException("Problem with response serialization", e);
    }
  }

  public ResponseEntity<?> prepareResponse(final Page<?> objects) {
    return prepareResponse(objects, ResponseDecorator.EMPTY);
  }

  public ResponseEntity<?> prepareResponse(final HttpStatus status) {
    return ResponseEntity.status(status).build();
  }

  public ResponseEntity<?> prepareResponse(final List<?> data) throws IOException {
    return prepareResponse(data, ResponseDecorator.EMPTY);
  }

  public ResponseEntity<?> prepareResponse(final List<?> data, final ResponseDecorator responseDecorator) throws IOException {

    try {
      final List<Map<String, Object>> content = new ArrayList<>();
      for (final Object entity : data) {
        content.add(decorateObject(entity, responseDecorator));
      }
      final List<?> page = new ArrayList<>(content);

      final String json = objectMapper.writeValueAsString(page);
      final List<Map<String, Object>> result = objectMapper.readValue(json, new TypeReference<List<Map<String, Object>>>() {
      });

      return ResponseEntity.status(HttpStatus.OK)
          .body(result);
    } catch (final IOException e) {
      throw new InternalServerErrorException("Problem with response serialization", e);
    }
  }

  public ObjectMapper getObjectMapper() {
    return objectMapper;
  }

  private Map<String, Object> decorateMinervaObject(final MinervaEntity object, final ResponseDecorator responseDecorator)
      throws IOException {
    final String json = objectMapper.writeValueAsString(object);
    final Map<String, Object> result = objectMapper.readValue(json, new TypeReference<Map<String, Object>>() {
    });
    for (final Pair<String, Object> pair : responseDecorator.getDecoratedValues(object)) {
      addKeyValue(result, pair.getLeft(), pair.getRight());
    }
    return result;
  }

  public ResponseEntity<MinervaJob> prepareMinervaJobResponse(final MinervaJob job) {
    return ResponseEntity.status(HttpStatus.ACCEPTED)
        .body(job);
  }

  private Map<String, Object> decorateObject(final MinervaEntity object, final ResponseDecorator responseDecorator)
      throws IOException {
    final String json = objectMapper.writeValueAsString(object);
    final Map<String, Object> result = objectMapper.readValue(json, new TypeReference<Map<String, Object>>() {
    });
    for (final Pair<String, Object> pair : responseDecorator.getDecoratedValues(object)) {
      addKeyValue(result, pair.getLeft(), pair.getRight());
    }
    return result;
  }

  private Map<String, Object> decorateObject(final Object object, final ResponseDecorator responseDecorator)
      throws IOException {
    if (object instanceof MinervaEntity) {
      return decorateMinervaObject((MinervaEntity) object, responseDecorator);
    } else {
      return decorateRawObject(object, responseDecorator);
    }
  }

  private void addKeyValue(final Map<String, Object> result, final String key, final Object value) {
    if (key.indexOf(".") < 0) {
      result.put(key, value);
    } else {
      final int index = key.indexOf(".");
      final String parentKey = key.substring(0, index);
      final String childKey = key.substring(index + 1);

      Map<String, Object> subTree = (Map<String, Object>) result.get(parentKey);

      if (subTree == null) {
        subTree = new HashMap<>();
        result.put(parentKey, subTree);
      }

      addKeyValue(subTree, childKey, value);
    }

  }

  private Map<String, Object> decorateRawObject(final Object object, final ResponseDecorator responseDecorator)
      throws IOException {
    final String json = objectMapper.writeValueAsString(object);
    final Map<String, Object> result = objectMapper.readValue(json, new TypeReference<Map<String, Object>>() {
    });
    for (final Pair<String, Object> pair : responseDecorator.getDecoratedValuesForRaw(object)) {
      result.put(pair.getLeft(), pair.getRight());
    }
    return result;
  }

  public void checkETag(
      // CHECKSTYLE.OFF: ParameterName
      final String eTag,
      // CHECKSTYLE.ON: ParameterName
      final MinervaEntity project) throws PreconditionFailedException {
    if (eTag != null && !Objects.equals(eTag, project.getEntityVersion() + "")) {
      throw new PreconditionFailedException("ETag does not match. Expected " + project.getEntityVersion() + ", but " + eTag + " found");
    }
  }

  public Page<? extends MinervaEntity> createPage(final List<? extends MinervaEntity> miriamData, final Pageable pageable) {
    if (pageable.isUnpaged()) {
      return new PageImpl<>(miriamData, pageable, miriamData.size());
    }
    final List<? extends MinervaEntity> sublist;
    if (pageable.getOffset() >= miriamData.size()) {
      sublist = new ArrayList<>();
    } else {
      if (pageable.getOffset() + pageable.getPageSize() > miriamData.size()) {
        sublist = miriamData.subList((int) pageable.getOffset(), miriamData.size());
      } else {
        sublist = miriamData.subList((int) pageable.getOffset(), (int) pageable.getOffset() + pageable.getPageSize());
      }
    }
    return new PageImpl<>(sublist, pageable, miriamData.size());
  }

  public Page<?> createRawPage(final List<?> miriamData, final Pageable pageable) {
    if (pageable.isUnpaged()) {
      return new PageImpl<>(miriamData, pageable, miriamData.size());
    }
    final List<?> sublist;
    if (pageable.getOffset() >= miriamData.size()) {
      sublist = new ArrayList<>();
    } else {
      if (pageable.getOffset() + pageable.getPageSize() > miriamData.size()) {
        sublist = miriamData.subList((int) pageable.getOffset(), miriamData.size());
      } else {
        sublist = miriamData.subList((int) pageable.getOffset(), (int) pageable.getOffset() + pageable.getPageSize());
      }
    }
    return new PageImpl<>(sublist, pageable, miriamData.size());
  }

}
