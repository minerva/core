package lcsb.mapviewer.web.api.project;

import lcsb.mapviewer.api.OperationNotAllowedException;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.api.UpdateConflictException;
import lcsb.mapviewer.api.minervanet.MinervaNetController;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.job.MinervaJob;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.ProjectProperty;
import lcsb.mapviewer.services.FailedDependencyException;
import lcsb.mapviewer.services.ObjectExistsException;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IMeshService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.ITaxonomyService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.user.NewUserLoginDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewProjectController {

  private final IConfigurationService configurationService;
  private final Logger logger = LogManager.getLogger();

  private final MinervaNetController minervaNetController;
  private final IMeshService meshService;
  private final IUserService userService;
  private final IProjectService projectService;
  private final ITaxonomyService taxonomyService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewProjectController(
      final IMeshService meshService,
      final MinervaNetController minervaNetController,
      final IProjectService projectService,
      final IUserService userService,
      final ITaxonomyService taxonomyService,
      final NewApiResponseSerializer serializer,
      final IConfigurationService configurationService) {
    this.meshService = meshService;
    this.userService = userService;
    this.projectService = projectService;
    this.configurationService = configurationService;
    this.minervaNetController = minervaNetController;
    this.serializer = serializer;
    this.taxonomyService = taxonomyService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{projectId:.+}")
  public ResponseEntity<?> getProject(final @NotBlank @PathVariable(value = "projectId") String projectId)
      throws ObjectNotFoundException, FailedDependencyException, IOException {
    final Project project = projectService.getProjectByProjectId(projectId, true);
    return serializer.prepareResponse(project, new ProjectResponseDecorator(minervaNetController.getSharedProjects(), meshService, taxonomyService));
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'IS_CURATOR')")
  @PostMapping(value = "/")
  public ResponseEntity<?> addProject(
      final Authentication authentication,
      final @Valid @RequestBody NewProjectDTO data)
      throws QueryException, ObjectNotFoundException, ObjectExistsException, FailedDependencyException, IOException {

    Project project = projectService.getProjectByProjectId(data.getProjectId());
    if (project != null) {
      throw new ObjectExistsException("Project with given projectId already exists");
    }
    project = new Project(data.getProjectId());
    data.saveToProject(project);
    project.setOwner(userService.getUserByLogin(authentication.getName()));
    project.setDirectory(UUID.randomUUID().toString());
    projectService.add(project);

    project = projectService.getProjectByProjectId(data.getProjectId(), true);
    return serializer.prepareResponse(project,
        new ProjectResponseDecorator(minervaNetController.getSharedProjects(), meshService, taxonomyService), HttpStatus.CREATED);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'IS_CURATOR')")
  @PostMapping(value = "/{projectId:.+}:move")
  public ResponseEntity<?> moveProject(
      final @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @RequestBody NewMoveProjectDTO data)
      throws ObjectNotFoundException, ObjectExistsException, UpdateConflictException {

    if (projectService.getProjectByProjectId(data.getProjectId()) != null) {
      throw new ObjectExistsException("Project with given projectId already exists");
    }
    Project project = projectService.getProjectByProjectId(projectId);

    if (project == null) {
      throw new ObjectExistsException("Project with given projectId does not exists");
    }

    if (configurationService.getConfigurationValue(ConfigurationElementType.DEFAULT_MAP).equals(data.getProjectId())) {
      throw new UpdateConflictException("Cannot move project. Project is shared in minerva net");
    }

    boolean sharedProject;
    try {
      sharedProject = minervaNetController.getSharedProjects().contains(data.getProjectId());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }

    if (sharedProject) {
      throw new UpdateConflictException("Cannot move project. Project is shared in minerva net");
    }
    data.saveToProject(project);

    projectService.update(project);
    userService.moveProjectPrivileges(projectId, data.getProjectId());

    project = projectService.getProjectByProjectId(data.getProjectId(), true);
    return serializer.prepareResponse(project, HttpStatus.OK);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{projectId:.+}")
  public ResponseEntity<?> updateProject(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewProjectDTO data,
      final @PathVariable(value = "projectId") String projectId)
      throws IOException, QueryException, ObjectNotFoundException, FailedDependencyException {
    Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given projectId does not exist");
    }
    serializer.checkETag(oldETag, project);
    data.saveToProject(project);
    projectService.update(project);

    project = projectService.getProjectByProjectId(projectId, true);
    return serializer.prepareResponse(project, new ProjectResponseDecorator(minervaNetController.getSharedProjects(), meshService, taxonomyService));
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{projectId:.+}")
  public ResponseEntity<?> deleteProject(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @PathVariable(value = "projectId") String projectId)
      throws QueryException, ObjectNotFoundException {
    if (configurationService.getConfigurationValue(ConfigurationElementType.DEFAULT_MAP).equals(projectId)) {
      throw new OperationNotAllowedException("You cannot remove default map");
    }

    final Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    serializer.checkETag(oldETag, project);

    final MinervaJob job = projectService.submitRemoveProjectJob(project);

    return serializer.prepareMinervaJobResponse(job);
  }

  @GetMapping(value = "/")
  public ResponseEntity<?> listProjects(final Pageable pageable, final Authentication authentication) throws FailedDependencyException, IOException {
    final User user = userService.getUserByLogin(authentication.getName(), true);
    final Map<ProjectProperty, Object> projectFilter = new HashMap<>();
    if (!user.getPrivileges().contains(new Privilege(PrivilegeType.IS_ADMIN))) {
      final List<String> projectIds = new ArrayList<>();
      for (final Privilege privilege : user.getPrivileges()) {
        if (privilege.getType() == PrivilegeType.READ_PROJECT) {
          projectIds.add(privilege.getObjectId());
        }
      }
      projectFilter.put(ProjectProperty.PROJECT_ID, projectIds);
    }
    final Page<Project> projects = projectService.getAll(projectFilter, pageable);
    return serializer.prepareResponse(projects, new ProjectResponseDecorator(minervaNetController.getSharedProjects(), meshService, taxonomyService));
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN')")
  @PutMapping(value = "/{projectId}/owner/")
  public ResponseEntity<?> updateOwner(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewUserLoginDTO data,
      final @PathVariable(value = "projectId") String projectId)
      throws IOException, QueryException, ObjectNotFoundException, FailedDependencyException {
    Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given projectId does not exist");
    }
    serializer.checkETag(oldETag, project);
    final User user = userService.getUserByLogin(data.getLogin());
    if (user == null) {
      throw new ObjectNotFoundException("User with given login does not exist");
    }

    project.setOwner(user);
    projectService.update(project);

    project = projectService.getProjectByProjectId(projectId, true);
    return serializer.prepareResponse(project, new ProjectResponseDecorator(minervaNetController.getSharedProjects(), meshService, taxonomyService));
  }

}