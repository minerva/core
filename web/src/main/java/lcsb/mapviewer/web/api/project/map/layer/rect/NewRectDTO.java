package lcsb.mapviewer.web.api.project.map.layer.rect;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.web.api.AbstractDTO;

import javax.validation.constraints.NotNull;
import java.awt.Color;

public class NewRectDTO extends AbstractDTO {

  @NotNull
  private Color fillColor;

  @NotNull
  private Color borderColor;

  @NotNull
  private Double x;

  @NotNull
  private Double y;

  @NotNull
  private Double width;

  @NotNull
  private Double height;

  @NotNull
  private Integer z;

  public void saveToRectangle(final LayerRect rect) throws QueryException {
    rect.setFillColor(fillColor);
    rect.setBorderColor(borderColor);
    rect.setX(x);
    rect.setY(y);
    rect.setZ(z);
    rect.setWidth(width);
    rect.setHeight(height);
  }

  public Double getX() {
    return x;
  }

  public void setX(final Double x) {
    this.x = x;
  }

  public Double getY() {
    return y;
  }

  public void setY(final Double y) {
    this.y = y;
  }

  public Double getWidth() {
    return width;
  }

  public void setWidth(final Double width) {
    this.width = width;
  }

  public Double getHeight() {
    return height;
  }

  public void setHeight(final Double height) {
    this.height = height;
  }

  public Integer getZ() {
    return z;
  }

  public void setZ(final Integer z) {
    this.z = z;
  }

  public Color getFillColor() {
    return fillColor;
  }

  public void setFillColor(final Color fillColor) {
    this.fillColor = fillColor;
  }

  public Color getBorderColor() {
    return borderColor;
  }

  public void setBorderColor(final Color borderColor) {
    this.borderColor = borderColor;
  }
}
