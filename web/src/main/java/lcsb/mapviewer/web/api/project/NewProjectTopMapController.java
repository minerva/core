package lcsb.mapviewer.web.api.project;

import java.io.IOException;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId:.+}/topMap",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewProjectTopMapController {

  private IModelService modelService;
  private IProjectService projectService;
  private NewApiResponseSerializer serializer;

  @Autowired
  public NewProjectTopMapController(final IModelService modelService, final IProjectService projectService,
      final NewApiResponseSerializer serializer) {
    this.modelService = modelService;
    this.serializer = serializer;
    this.projectService = projectService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping
  public ResponseEntity<?> getTopMap(
      final @NotBlank @PathVariable(value = "projectId") String projectId,
      final Pageable pageable)
      throws IOException, QueryException, ObjectNotFoundException {
    Project project = projectService.getProjectByProjectId(projectId);
    ModelData result = null;
    if (project != null) {
      result = modelService.getModelByMapId(projectId, project.getTopModelData().getId());
    }

    return serializer.prepareResponse(result);

  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping
  public ResponseEntity<?> updateTopMap(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @RequestBody NewIdDTO data,
      final @PathVariable(value = "projectId") String projectId) throws IOException, QueryException, ObjectNotFoundException {
    Project project = projectService.getProjectByProjectId(projectId, true);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given projectId does not exist");
    }
    serializer.checkETag(oldETag, project.getTopModelData());
    if (data.getId() == null) {
      throw new QueryException("Id cannot be null");
    } else {
      ModelData modelData = modelService.getModelByMapId(projectId, data.getId());
      if (modelData != null) {
        for (ModelData m : project.getModels()) {
          if (m.getId() == modelData.getId()) {
            project.setTopModel(m);
          }
        }
        projectService.update(project);

      }
      return serializer.prepareResponse(modelData);
    }

  }

}