package lcsb.mapviewer.web.api.project.map.layer.text;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.web.api.AbstractDTO;

import javax.validation.constraints.NotNull;
import java.awt.Color;

public class NewTextDTO extends AbstractDTO {

  @NotNull
  private Color color;

  @NotNull
  private Color borderColor;

  @NotNull
  private Double x;

  @NotNull
  private Double y;

  @NotNull
  private Double width;

  @NotNull
  private Double height;

  @NotNull
  private Integer z;

  @NotNull
  private Integer fontSize;

  @NotNull
  private String notes;

  @NotNull
  private VerticalAlign verticalAlign;

  @NotNull
  private HorizontalAlign horizontalAlign;


  public void saveToText(final LayerText text) throws QueryException {
    text.setColor(color);
    text.setBorderColor(borderColor);
    text.setNotes(notes);
    text.setFontSize(fontSize);
    text.setX(x);
    text.setY(y);
    text.setZ(z);
    text.setWidth(width);
    text.setHeight(height);
    text.setVerticalAlign(verticalAlign);
    text.setHorizontalAlign(horizontalAlign);
  }

  public Double getX() {
    return x;
  }

  public void setX(final Double x) {
    this.x = x;
  }

  public Double getY() {
    return y;
  }

  public void setY(final Double y) {
    this.y = y;
  }

  public Double getWidth() {
    return width;
  }

  public void setWidth(final Double width) {
    this.width = width;
  }

  public Double getHeight() {
    return height;
  }

  public void setHeight(final Double height) {
    this.height = height;
  }

  public Integer getZ() {
    return z;
  }

  public void setZ(final Integer z) {
    this.z = z;
  }

  public Color getColor() {
    return color;
  }

  public void setColor(final Color color) {
    this.color = color;
  }

  public Color getBorderColor() {
    return borderColor;
  }

  public void setBorderColor(final Color borderColor) {
    this.borderColor = borderColor;
  }

  public Integer getFontSize() {
    return fontSize;
  }

  public void setFontSize(final Integer fontSize) {
    this.fontSize = fontSize;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(final String notes) {
    this.notes = notes;
  }

  public VerticalAlign getVerticalAlign() {
    return verticalAlign;
  }

  public void setVerticalAlign(final VerticalAlign verticalAlign) {
    this.verticalAlign = verticalAlign;
  }

  public HorizontalAlign getHorizontalAlign() {
    return horizontalAlign;
  }

  public void setHorizontalAlign(final HorizontalAlign horizontalAlign) {
    this.horizontalAlign = horizontalAlign;
  }
}
