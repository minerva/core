package lcsb.mapviewer.web.api.project.drugs;

import lcsb.mapviewer.annotation.data.Drug;
import lcsb.mapviewer.annotation.services.DrugSearchException;
import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.modelutils.serializer.model.map.ElementIdentifierType;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.search.DbSearchCriteria;
import lcsb.mapviewer.services.search.drug.IDrugService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewDrugController extends BaseController {

  private final IProjectService projectService;
  private final IDrugService drugService;
  private final IElementService elementService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewDrugController(final NewApiResponseSerializer serializer, final IProjectService projectService, final IDrugService drugService,
                           final IElementService elementService) {
    this.projectService = projectService;
    this.drugService = drugService;
    this.elementService = elementService;
    this.serializer = serializer;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "drugs:search")
  public ResponseEntity<?> getDrugsByQuery(
      final @PathVariable(value = "projectId") String projectId,
      final @RequestParam(value = "query", defaultValue = "") String query,
      final @RequestParam(value = "target", defaultValue = "") String target) throws QueryException, ObjectNotFoundException, IOException {
    List<Drug> data = new ArrayList<>();
    final Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    if (!query.isEmpty()) {

      MiriamData organism = project.getOrganism();
      if (organism == null) {
        organism = TaxonomyBackend.HUMAN_TAXONOMY;
      }
      final Drug drug = drugService.getByName(query, new DbSearchCriteria().project(project).organisms(organism).colorSet(0));
      if (drug != null) {
        data.add(drug);
      }
    } else if (target.contains(":")) {
      final String targetType = target.split(":", -1)[0];
      final String targetId = target.split(":", -1)[1];

      final Integer dbId = Integer.valueOf(targetId);
      final List<Element> targets = getTargets(targetType, project, dbId);
      MiriamData organism = project.getOrganism();
      if (organism == null) {
        organism = TaxonomyBackend.HUMAN_TAXONOMY;
      }

      data = drugService.getForTargets(targets, new DbSearchCriteria().project(project).organisms(organism));
    }

    return serializer.prepareResponse(data);
  }

  private List<Element> getTargets(final String targetType, final Project project, final Integer dbId)
      throws QueryException, ObjectNotFoundException {
    if (targetType.equals(ElementIdentifierType.ALIAS.getJsName())) {
      final Map<ElementProperty, Object> properties = new HashMap<>();
      properties.put(ElementProperty.ID, Collections.singletonList(dbId));
      properties.put(ElementProperty.PROJECT_ID, Collections.singletonList(project.getProjectId()));
      final Page<Element> elements = elementService.getAll(properties, Pageable.unpaged());
      if (elements.getNumberOfElements() == 0) {
        throw new ObjectNotFoundException("Element does not exist");
      }
      return new ArrayList<>(elements.getContent());
    } else {
      throw new QueryException("Targeting for the type not implemented");
    }
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "drugs/suggestedQueryList")
  public List<String> getSuggestedQueryList(final @PathVariable(value = "projectId") String projectId)
      throws DrugSearchException, ObjectNotFoundException {
    final Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    return drugService.getSuggestedQueryList(project, project.getOrganism());
  }

}