package lcsb.mapviewer.web.api.project.map.bioentity.reaction;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;
import lcsb.mapviewer.web.api.AbstractDTO;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.awt.geom.Point2D;

public class NewReactionDTO extends AbstractDTO {

  @NotNull
  private String notes;

  @NotBlank
  private String idReaction;

  @NotNull
  private String name;

  @NotNull
  private Boolean reversible;

  @NotBlank
  private String sboTerm;

  private String symbol;

  private String abbreviation;

  private String formula;

  private Integer mechanicalConfidenceScore;

  private Double lowerBound;

  private Double upperBound;

  private String subsystem;

  private String geneProteinReaction;

  private String visibilityLevel = "";

  @NotNull
  private Integer z;

  @NotNull
  private Point2D processCoordinates;

  public Class<? extends Reaction> getClazz() {
    return SBOTermReactionType.getTypeForSBOTerm(sboTerm);
  }

  public String getReactionId() {
    return idReaction;
  }

  public void saveToReaction(final Reaction reaction, final boolean allowLayoutChanges) throws QueryException {
    checkEquality("SBOterm", getClazz(), reaction.getClass());
    if (reaction.getElementId() == null || reaction.getElementId().equals(idReaction)) {
      reaction.setIdReaction(idReaction);
    } else {
      checkEquality("idReaction", idReaction, reaction.getElementId());
    }

    if (!allowLayoutChanges) {
      checkEquality("z", z, reaction.getZ());
      checkEquality("visibilityLevel", visibilityLevel, reaction.getVisibilityLevel());
      checkEquality("processCoordinates", processCoordinates, reaction.getProcessCoordinates());

    } else {
      reaction.setZ(z);
      reaction.setVisibilityLevel(visibilityLevel);
      reaction.setProcessCoordinates(processCoordinates);
      if (reaction.getLine() == null) {
        reaction.setLine(new PolylineData());
      }
    }
    reaction.setName(name);
    reaction.setNotes(notes);
    reaction.setReversible(reversible);
    reaction.setSymbol(symbol);
    reaction.setAbbreviation(abbreviation);
    reaction.setFormula(formula);
    reaction.setMechanicalConfidenceScore(mechanicalConfidenceScore);
    reaction.setLowerBound(lowerBound);
    reaction.setUpperBound(upperBound);
    reaction.setSubsystem(subsystem);
    reaction.setGeneProteinReaction(geneProteinReaction);
    reaction.setVisibilityLevel(visibilityLevel);

  }

  public void setSboTerm(final String sboTerm) {
    this.sboTerm = sboTerm;
  }

  public void setReactionId(final String idReaction) {
    this.idReaction = idReaction;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(final String notes) {
    this.notes = notes;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public Boolean getReversible() {
    return reversible;
  }

  public void setReversible(final Boolean reversible) {
    this.reversible = reversible;
  }

  public String getSboTerm() {
    return sboTerm;
  }

  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(final String symbol) {
    this.symbol = symbol;
  }

  public String getAbbreviation() {
    return abbreviation;
  }

  public void setAbbreviation(final String abbreviation) {
    this.abbreviation = abbreviation;
  }

  public String getFormula() {
    return formula;
  }

  public void setFormula(final String formula) {
    this.formula = formula;
  }

  public Integer getMechanicalConfidenceScore() {
    return mechanicalConfidenceScore;
  }

  public void setMechanicalConfidenceScore(final Integer mechanicalConfidenceScore) {
    this.mechanicalConfidenceScore = mechanicalConfidenceScore;
  }

  public Double getLowerBound() {
    return lowerBound;
  }

  public void setLowerBound(final Double lowerBound) {
    this.lowerBound = lowerBound;
  }

  public Double getUpperBound() {
    return upperBound;
  }

  public void setUpperBound(final Double upperBound) {
    this.upperBound = upperBound;
  }

  public String getSubsystem() {
    return subsystem;
  }

  public void setSubsystem(final String subsystem) {
    this.subsystem = subsystem;
  }

  public String getGeneProteinReaction() {
    return geneProteinReaction;
  }

  public void setGeneProteinReaction(final String geneProteinReaction) {
    this.geneProteinReaction = geneProteinReaction;
  }

  public String getVisibilityLevel() {
    return visibilityLevel;
  }

  public void setVisibilityLevel(final String visibilityLevel) {
    this.visibilityLevel = visibilityLevel;
  }

  public Integer getZ() {
    return z;
  }

  public void setZ(final Integer z) {
    this.z = z;
  }

  public Point2D getProcessCoordinates() {
    return processCoordinates;
  }

  public void setProcessCoordinates(final Point2D processCoordinates) {
    this.processCoordinates = processCoordinates;
  }

}
