package lcsb.mapviewer.web.api.project.map.function;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import javax.validation.constraints.Size;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.web.api.AbstractDTO;

public class NewFunctionDTO extends AbstractDTO {

  @NotBlank
  @Size(max = 255)
  private String functionId;

  @NotBlank
  @Size(max = 255)
  private String name;

  @NotBlank
  private String definition;

  public void saveToSbmlFunction(final SbmlFunction function) throws QueryException {
    validateDefinition();
    try {
      function.setDefinition(definition);
    } catch (InvalidXmlSchemaException e) {
      throw new QueryException("Invalid definition", e);
    }
    function.setFunctionId(functionId);
    function.setName(name);
  }

  private void validateDefinition() throws QueryException {
    try {
      TransformerFactory factory = TransformerFactory.newInstance();

      Resource resource = new ClassPathResource("mathmlc2p.xsl");
      InputStream styleInputStream;
      styleInputStream = resource.getInputStream();

      StreamSource stylesource = new StreamSource(styleInputStream);
      Transformer transformer = factory.newTransformer(stylesource);

      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      transformer.transform(new StreamSource(new ByteArrayInputStream(definition.getBytes())), new StreamResult(baos));
    } catch (Exception e) {
      throw new QueryException("Invalid definition", e);
    }
  }

  public String getFunctionId() {
    return functionId;
  }

  public void setFunctionId(final String functionId) {
    this.functionId = functionId;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getDefinition() {
    return definition;
  }

  public void setDefinition(final String definition) {
    this.definition = definition;
  }
}
