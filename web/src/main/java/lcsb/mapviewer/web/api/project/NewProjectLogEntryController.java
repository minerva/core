package lcsb.mapviewer.web.api.project;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.model.ProjectLogEntry;
import lcsb.mapviewer.persist.dao.ProjectLogEntryProperty;
import lcsb.mapviewer.services.FailedDependencyException;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId:.+}/logEntry",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewProjectLogEntryController {

  private IProjectService projectService;
  private NewApiResponseSerializer serializer;

  @Autowired
  public NewProjectLogEntryController(final IProjectService projectService, final NewApiResponseSerializer serializer) {
    this.projectService = projectService;
    this.serializer = serializer;
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)")
  @GetMapping
  public ResponseEntity<?> getLogEntries(
      final @NotBlank @PathVariable(value = "projectId") String projectId,
      final @RequestParam(value = "level", required = false) String level,
      final @RequestParam(value = "search", required = false) String searchQuery,
      final Pageable pageable)
      throws ObjectNotFoundException, FailedDependencyException, IOException {
    Map<ProjectLogEntryProperty, Object> searchFilter = new HashMap<>();
    if (level != null) {
      searchFilter.put(ProjectLogEntryProperty.LEVEL, level);
    }
    if (searchQuery != null) {
      searchFilter.put(ProjectLogEntryProperty.CONTENT, searchQuery);
    }
    searchFilter.put(ProjectLogEntryProperty.PROJECT_ID, projectId);

    Page<ProjectLogEntry> entries;
    if (pageable.getSort().isSorted()) {
      entries = projectService.getLogEntries(searchFilter, pageable);
    } else {
      entries = projectService.getLogEntries(searchFilter,
          PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Direction.ASC, ProjectLogEntryProperty.ID.name()));
    }

    return serializer.prepareResponse(entries);

  }
}