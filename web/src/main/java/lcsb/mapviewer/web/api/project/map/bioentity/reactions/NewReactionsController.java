package lcsb.mapviewer.web.api.project.map.bioentity.reactions;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.persist.dao.map.ModelProperty;
import lcsb.mapviewer.persist.dao.map.ReactionProperty;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@RestController
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/reactions/"},
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewReactionsController extends BaseController {

  private final IReactionService reactionService;
  private final IElementService elementService;
  private final IProjectService projectService;
  private final IModelService modelService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewReactionsController(final IElementService elementService, final IProjectService projectService, final IModelService modelService,
                                final IReactionService reactionService,
                                final NewApiResponseSerializer serializer) {
    this.reactionService = reactionService;
    this.projectService = projectService;
    this.modelService = modelService;
    this.serializer = serializer;
    this.elementService = elementService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "{reactionId}")
  public ResponseEntity<?> listReactions(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") String mapId,
      final @Valid @NotBlank @PathVariable(value = "reactionId") String reactionId) throws QueryException, ObjectNotFoundException {

    if (!StringUtils.isNumeric(reactionId)) {
      throw new QueryException("Invalid reactionId: " + reactionId);
    }
    if (!StringUtils.isNumeric(mapId)) {
      throw new QueryException("Invalid mapId: " + mapId);
    }
    final Map<ReactionProperty, Object> properties = new HashMap<>();
    properties.put(ReactionProperty.ID, Collections.singletonList(Integer.valueOf(reactionId)));

    properties.put(ReactionProperty.PROJECT, Collections.singletonList(projectService.getProjectByProjectId(projectId)));
    if (!mapId.equals("*")) {
      try {
        properties.put(ReactionProperty.MAP, Collections.singletonList(modelService.getModelByMapId(projectId, Integer.valueOf(mapId))));
      } catch (final NumberFormatException e) {
        throw new QueryException("mapId must be numeric", e);
      }
    }

    final Page<Reaction> reactions = reactionService.getAll(properties, Pageable.unpaged(), true);
    if (reactions.getContent().size() == 0) {
      throw new ObjectNotFoundException("Reaction does not exists");
    }
    return serializer.prepareResponse(reactions.getContent().get(0));
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @PostMapping(value = ":downloadCsv")
  public ResponseEntity<?> downloadCsv(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") String mapId,
      final @Valid @NotBlank @RequestBody DownloadCsvRequest request)
      throws QueryException, IOException, NumberFormatException, ObjectNotFoundException {

    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.PROJECT, Collections.singletonList(projectService.getProjectByProjectId(projectId)));
    if (request.getElementTypes().size() > 0) {
      properties.put(ElementProperty.CLASS, new ElementUtils().getClassesByStringTypes(request.getElementTypes()));
    }
    if (NumberUtils.isCreatable(mapId)) {
      properties.put(ElementProperty.MAP, Collections.singletonList(modelService.getModelByMapId(projectId, Integer.valueOf(mapId))));
    } else if (request.getSubmaps().size() > 0) {
      final Map<ModelProperty, Object> mapProperties = new HashMap<>();
      mapProperties.put(ModelProperty.PROJECT_ID, Collections.singletonList(projectId));

      final List<ModelData> models = modelService.getAll(mapProperties, Pageable.unpaged()).getContent();
      final List<ModelData> filteredModels = new ArrayList<>();
      for (final ModelData modelData : models) {
        if (request.getSubmaps().contains(modelData.getId())) {
          filteredModels.add(modelData);
        }
      }

      if (filteredModels.size() == 0) {
        throw new QueryException("Submaps don't exist");
      }
      properties.put(ElementProperty.MAP, filteredModels);
    }
    if (request.getIncludedCompartmentIds().size() > 0) {
      properties.put(ElementProperty.INCLUDED_IN_COMPARTMENT, request.getIncludedCompartmentIds());
    }
    if (request.getExcludedCompartmentIds().size() > 0) {
      properties.put(ElementProperty.EXCLUDED_FROM_COMPARTMENT, request.getExcludedCompartmentIds());
    }

    final List<Element> filteredElements = elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent();

    final Set<Integer> elementIds = new HashSet<>();
    for (final Element element : filteredElements) {
      elementIds.add(element.getId());
    }
    final List<Reaction> reactions;
    if (elementIds.size() > 0) {
      reactions = reactionService.getReactionById(projectId, "*", new HashSet<>(), elementIds);
    } else {
      reactions = new ArrayList<>();
    }

    List<Reaction> filteredReactions = new ArrayList<>();
    if (request.getReactionTypes().size() > 0) {
      for (final Reaction reaction : reactions) {
        if (getStriungReactionTypes(request.getReactionTypes()).contains(reaction.getStringType())) {
          filteredReactions.add(reaction);
        }
      }
    } else {
      filteredReactions = reactions;
    }

    final String content = prepareCsv(filteredReactions, request.getColumns(), request.getAnnotations());
    final byte[] out = content.getBytes();

    final HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.add("content-disposition", "attachment; filename=elements.csv");
    responseHeaders.add("Content-Type", "text/csv");

    return new ResponseEntity<>(out, responseHeaders, HttpStatus.OK);
  }

  private Set<String> getStriungReactionTypes(final List<String> reactionTypes) throws QueryException {
    final Set<String> result = new HashSet<>();
    for (final String string : reactionTypes) {
      if (string.equals("Process description")) {
        result.addAll(Arrays.asList("Dissociation",
            "Heterodimer association",
            "Inhibition",
            "Known transition omitted",
            "Modulation",
            "Physical stimulation",
            "State transition",
            "Transcription",
            "Translation",
            "Transport",
            "Trigger",
            "Truncation",
            "Unknown catalysis",
            "Unknown inhibition",
            "Unknown transition"));
      } else if (string.equals("Activity flow")) {
        result.addAll(Arrays.asList("Reduced modulation",
            "Reduced physical stimulation",
            "Reduced trigger",
            "Unknown reduced modulation",
            "Unknown reduced physical stimulation",
            "Unknown reduced trigger",
            "Negative influence",
            "Positive influence",
            "Unknown negative influence",
            "Unknown positive influence"));
      } else {
        throw new QueryException("Unvalid reaction type: " + string);
      }
    }
    return result;
  }

  private String prepareCsv(final List<Reaction> reactions, final List<String> columns, final List<String> annotations)
      throws IOException, QueryException {
    final List<String> header = new ArrayList<>(columns);
    header.addAll(annotations);
    final Writer writer = new StringWriter();
    try (final CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
        .builder().setHeader(header.toArray(new String[header.size()])).build())) {
      for (final Reaction element : reactions) {
        csvPrinter.printRecord(getCsvRow(element, columns, annotations));
      }

      csvPrinter.flush();
      return writer.toString();
    }
  }

  private List<Object> getCsvRow(final Reaction reaction, final List<String> columns, final List<String> annotations) throws QueryException {
    final List<Object> result = new ArrayList<>();
    Object value;
    for (final String string : columns) {
      value = null;
      switch (string) {
        case ("id"):
          value = reaction.getId();
          break;
        case ("description"):
          value = reaction.getNotes();
          break;
        case ("modelId"):
          value = reaction.getModelData().getId();
          break;
        case ("mapName"):
          value = reaction.getModelData().getName();
          break;
        case ("symbol"):
          value = reaction.getSymbol();
          break;
        case ("abbreviation"):
          value = reaction.getAbbreviation();
          break;
        case ("synonyms"):
          value = StringUtils.join(reaction.getSynonyms(), ";");
          break;
        case ("references"):
          final List<String> refs = new ArrayList<>();
          for (final MiriamData md : reaction.getMiriamData()) {
            refs.add(md.getDataType() + ":" + md.getResource());
          }
          value = StringUtils.join(refs, ";");
          break;
        case ("name"):
          value = reaction.getName();
          break;
        case ("type"):
          value = reaction.getStringType();
          break;
        case ("elements"):
          final List<String> participants = new ArrayList<>();
          for (final Reactant reactant : reaction.getReactants()) {
            participants.add("REACTANT:" + reactant.getElement().getId());
          }
          for (final Product product : reaction.getProducts()) {
            participants.add("PRODUCT:" + product.getElement().getId());
          }
          for (final Modifier modifier : reaction.getModifiers()) {
            participants.add("MODIFIER:" + modifier.getElement().getId());
          }
          value = StringUtils.join(participants, ";");
          break;
        case ("reactantIds"):
          final List<String> reactants = new ArrayList<>();
          for (final Reactant reactant : reaction.getReactants()) {
            reactants.add(reactant.getElement().getId() + "");
          }
          value = StringUtils.join(reactants, ";");
          break;
        case ("productIds"):
          final List<String> products = new ArrayList<>();
          for (final Product product : reaction.getProducts()) {
            products.add(product.getElement().getId() + "");
          }
          value = StringUtils.join(products, ";");
          break;
        case ("modifierIds"):
          final List<String> modifiers = new ArrayList<>();
          for (final Modifier modifier : reaction.getModifiers()) {
            modifiers.add(modifier.getElement().getId() + "");
          }
          value = StringUtils.join(modifiers, ";");
          break;
        case ("reactionId"):
          value = reaction.getIdReaction();
          break;
        case ("mechanicalConfidenceScore"):
          value = reaction.getMechanicalConfidenceScore();
          break;
        case ("lowerBound"):
          value = reaction.getLowerBound();
          break;
        case ("upperBound"):
          value = reaction.getUpperBound();
          break;
        case ("geneProteinReaction"):
          value = reaction.getGeneProteinReaction();
          break;
        case ("subsystem"):
          value = reaction.getSubsystem();
          break;
        default:
          throw new QueryException("Unknown column name: " + string);
      }
      result.add(value);
    }
    for (final String annotation : annotations) {
      value = null;
      try {
        final MiriamType type = MiriamType.valueOf(annotation);
        final List<String> refs = new ArrayList<>();
        for (final MiriamData md : reaction.getMiriamData()) {
          if (Objects.equals(md.getDataType(), type)) {
            refs.add(md.getResource());
          }
        }
        value = StringUtils.join(refs, ";");
        result.add(value);
      } catch (final IllegalArgumentException e) {
        throw new QueryException("Invalid annotation name: " + annotation, e);
      }
    }

    return result;
  }

}