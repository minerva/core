package lcsb.mapviewer.web.api.minervanet;

public class NewErrorReport {

  private String url;
  private String login;
  private String email;
  private String browser;
  private String timestamp;
  private String stacktrace;
  private String javaStacktrace;
  private String version;
  private String comment;

  public String getUrl() {
    return url;
  }

  public void setUrl(final String url) {
    this.url = url;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(final String login) {
    this.login = login;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public String getBrowser() {
    return browser;
  }

  public void setBrowser(final String browser) {
    this.browser = browser;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(final String timestamp) {
    this.timestamp = timestamp;
  }

  public String getStacktrace() {
    return stacktrace;
  }

  public void setStacktrace(final String stacktrace) {
    this.stacktrace = stacktrace;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(final String version) {
    this.version = version;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(final String comment) {
    this.comment = comment;
  }

  public String getJavaStacktrace() {
    return javaStacktrace;
  }

  public void setJavaStacktrace(final String javaStacktrace) {
    this.javaStacktrace = javaStacktrace;
  }
}
