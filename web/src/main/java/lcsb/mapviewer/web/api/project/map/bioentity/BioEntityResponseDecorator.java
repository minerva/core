package lcsb.mapviewer.web.api.project.map.bioentity;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.utils.BioEntitySearchResult;
import lcsb.mapviewer.web.api.ResponseDecorator;

public class BioEntityResponseDecorator implements ResponseDecorator {

  private IElementService elementService;

  public BioEntityResponseDecorator(final IElementService elementService) {
    this.elementService = elementService;
  }

  @Override
  public List<Pair<String, Object>> getDecoratedValues(final MinervaEntity object) {
    List<Pair<String, Object>> result = new ArrayList<>();
    if (object instanceof BioEntitySearchResult) {
      List<Pair<String, Object>> bioEntityExtensions = getBioEntityExtensions(((BioEntitySearchResult) object).getBioEntity());
      for (Pair<String, Object> pair : bioEntityExtensions) {
        result.add(new Pair<>("bioEntity." + pair.getLeft(), pair.getRight()));
      }
    } else if (object instanceof BioEntity) {
      List<Pair<String, Object>> bioEntityExtensions = getBioEntityExtensions((BioEntity) object);
      for (Pair<String, Object> pair : bioEntityExtensions) {
        result.add(new Pair<>(pair.getLeft(), pair.getRight()));
      }
    }
    return result;
  }

  List<Pair<String, Object>> getBioEntityExtensions(final BioEntity bioEntity) {
    List<Pair<String, Object>> result = new ArrayList<>();
    if (bioEntity instanceof Element) {
      Element element = (Element) bioEntity;

      String compartmentName = getCompartmentName(element);
      result.add(new Pair<String, Object>("compartmentName", compartmentName));
    }

    if (bioEntity instanceof Species) {
      Species species = (Species) bioEntity;

      String complexName = getComplexName(species);
      result.add(new Pair<String, Object>("complexName", complexName));
    }

    return result;
  }

  private String getCompartmentName(final Element element) {
    String compartmentName = null;
    if (element.getCompartment() != null) {
      Compartment compartment = element.getCompartment();
      if (!Hibernate.isInitialized(compartment)) {
        compartment = (Compartment) elementService.getById(compartment.getId());
      }
      compartmentName = compartment.getName();
    }
    return compartmentName;
  }

  private String getComplexName(final Species species) {
    String complexName = null;
    if (species.getComplex() != null) {
      Complex complex = species.getComplex();
      if (!Hibernate.isInitialized(complex)) {
        complex = (Complex) elementService.getById(complex.getId());
      }
      complexName = complex.getName();
    }
    return complexName;
  }

  @Override
  public List<Pair<String, Object>> getDecoratedValuesForRaw(final Object object) {
    return new ArrayList<>();
  }

}
