package lcsb.mapviewer.web.api.project.map.bioentity.elements;

import java.util.ArrayList;
import java.util.List;

public class DownloadCsvRequest {
  private List<String> types = new ArrayList<>();
  private List<Integer> submaps = new ArrayList<>();
  private List<String> columns = new ArrayList<>();
  private List<String> annotations = new ArrayList<>();
  private List<Integer> includedCompartmentIds = new ArrayList<>();
  private List<Integer> excludedCompartmentIds = new ArrayList<>();

  @Override
  public String toString() {
    return "types: " + types + "\n"
        + "submaps: " + submaps + "\n"
        + "columns: " + columns + "\n"
        + "annotations: " + annotations + "\n"
        + "includedCompartments: " + includedCompartmentIds + "\n"
        + "excludedCompoartments: " + excludedCompartmentIds;
  }

  public List<String> getTypes() {
    return types;
  }

  public void setTypes(final List<String> types) {
    this.types = types;
  }

  public List<Integer> getSubmaps() {
    return submaps;
  }

  public void setSubmaps(final List<Integer> submaps) {
    this.submaps = submaps;
  }

  public List<String> getColumns() {
    return columns;
  }

  public void setColumns(final List<String> columns) {
    this.columns = columns;
  }

  public List<Integer> getIncludedCompartmentIds() {
    return includedCompartmentIds;
  }

  public void setIncludedCompartmentIds(final List<Integer> includedCompartmentIds) {
    this.includedCompartmentIds = includedCompartmentIds;
  }

  public List<Integer> getExcludedCompartmentIds() {
    return excludedCompartmentIds;
  }

  public void setExcludedCompartmentIds(final List<Integer> excludedCompartmentIds) {
    this.excludedCompartmentIds = excludedCompartmentIds;
  }

  public List<String> getAnnotations() {
    return annotations;
  }

  public void setAnnotations(final List<String> annotations) {
    this.annotations = annotations;
  }
}
