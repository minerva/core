package lcsb.mapviewer.web.api.project;

import lcsb.mapviewer.web.api.AbstractDTO;

public class NewIdDTO extends AbstractDTO {
  private Integer id;

  public Integer getId() {
    return id;
  }

  public void setId(final Integer id) {
    this.id = id;
  }
}
