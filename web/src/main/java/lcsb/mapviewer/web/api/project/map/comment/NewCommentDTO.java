package lcsb.mapviewer.web.api.project.map.comment;

import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.web.api.AbstractDTO;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.awt.geom.Point2D;

public class NewCommentDTO extends AbstractDTO {

  @NotBlank
  @Size(max = 255)
  private String content;

  @Size(max = 255)
  private String email;

  @NotNull
  private Point2D coordinates;

  @NotNull
  private Boolean visible;

  public void saveToComment(final Comment comment) {
    comment.setContent(content);
    comment.setEmail(email);
    comment.setCoordinates(coordinates);
    comment.setPinned(visible);
  }

  public Point2D getCoordinates() {
    return coordinates;
  }

  public void setCoordinates(final Point2D coordinates) {
    this.coordinates = coordinates;
  }

  public Boolean getVisible() {
    return visible;
  }

  public void setVisible(final Boolean visible) {
    this.visible = visible;
  }

  public String getContent() {
    return content;
  }

  public void setContent(final String content) {
    this.content = content;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }
}
