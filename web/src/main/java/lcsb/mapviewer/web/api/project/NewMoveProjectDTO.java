package lcsb.mapviewer.web.api.project;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.web.api.AbstractDTO;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class NewMoveProjectDTO extends AbstractDTO {
  @NotBlank
  @Size(max = 255)
  @Pattern(regexp = "^[a-z0-9A-Z\\-_]+$", message = "projectId can contain only alphanumeric characters and -_")
  private String projectId;

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(final String projectId) {
    this.projectId = projectId;
  }

  public void saveToProject(final Project project) {
    project.setProjectId(projectId);
  }
}
