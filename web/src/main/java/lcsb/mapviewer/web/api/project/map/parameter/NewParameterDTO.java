package lcsb.mapviewer.web.api.project.map.parameter;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.web.api.AbstractDTO;

public class NewParameterDTO extends AbstractDTO {

  @NotBlank
  @Size(max = 255)
  private String parameterId;

  @NotBlank
  @Size(max = 255)
  private String name;

  @NotNull
  private Double value;

  @NotNull
  private Integer unitId;

  public void saveToSbmlParameter(final SbmlParameter parameter, final List<SbmlUnit> units) throws QueryException {
    parameter.setName(name);
    parameter.setParameterId(parameterId);
    parameter.setValue(value);
    SbmlUnit unit = null;
    for (SbmlUnit sbmlUnit : units) {
      if (sbmlUnit.getId() == unitId) {
        unit = sbmlUnit;
      }
    }
    if (unit == null) {
      throw new QueryException("Unit with id=" + unitId + " not found");
    }
  }

  public String getParameterId() {
    return parameterId;
  }

  public void setParameterId(final String parameterId) {
    this.parameterId = parameterId;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public Double getValue() {
    return value;
  }

  public void setValue(final Double value) {
    this.value = value;
  }

  public Integer getUnitId() {
    return unitId;
  }

  public void setUnitId(final Integer unitId) {
    this.unitId = unitId;
  }
}
