package lcsb.mapviewer.web.api.project.map.bioentity.element.uniprot;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.species.field.Structure;
import lcsb.mapviewer.model.map.species.field.UniprotRecord;
import lcsb.mapviewer.persist.dao.map.species.UniprotRecordProperty;
import lcsb.mapviewer.persist.dao.map.species.UniprotStructureProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.impl.UniprotRecordService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUniprotStructureService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/{uniprotId}/structures/",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewUniprotStructureController {

  protected static Logger logger = LogManager.getLogger();

  private final IProjectService projectService;
  private final IUniprotStructureService uniprotStructureService;
  private final NewApiResponseSerializer serializer;
  private final UniprotRecordService uniprotRecordService;

  @Autowired
  public NewUniprotStructureController(
      final NewApiResponseSerializer serializer,
      final IUniprotStructureService uniprotStructureService,
      final IProjectService projectService, final UniprotRecordService uniprotRecordService) {
    this.uniprotStructureService = uniprotStructureService;
    this.serializer = serializer;
    this.projectService = projectService;
    this.uniprotRecordService = uniprotRecordService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{structureId}")
  public ResponseEntity<?> getUniprot(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @NotNull @PathVariable(value = "uniprotId") Integer uniprotId,
      final @NotNull @PathVariable(value = "structureId") Integer structureId
  )
      throws ObjectNotFoundException {
    final Structure uniprot = getStructureFromService(projectId, mapId, elementId, uniprotId, structureId);
    return serializer.prepareResponse(uniprot);
  }

  private UniprotRecord getUniprotFromService(final String projectId, final Integer mapId, final Integer elementId,
                                              final Integer uniprotId)
      throws ObjectNotFoundException {
    final Map<UniprotRecordProperty, Object> properties = new HashMap<>();
    properties.put(UniprotRecordProperty.ID, Collections.singletonList(uniprotId));
    properties.put(UniprotRecordProperty.ELEMENT_ID, Collections.singletonList(elementId));
    properties.put(UniprotRecordProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(UniprotRecordProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<UniprotRecord> elements = uniprotRecordService.getAll(properties, Pageable.unpaged());
    if (elements.isEmpty()) {
      throw new ObjectNotFoundException("Uniprot Residue does not exist");
    }
    return elements.getContent().get(0);
  }

  private Structure getStructureFromService(final String projectId, final Integer mapId, final Integer elementId,
                                            final Integer uniprotId, final Integer structureId)
      throws ObjectNotFoundException {
    final Map<UniprotStructureProperty, Object> properties = new HashMap<>();
    properties.put(UniprotStructureProperty.UNIPROT_ID, Collections.singletonList(uniprotId));
    properties.put(UniprotStructureProperty.ID, Collections.singletonList(structureId));
    properties.put(UniprotStructureProperty.ELEMENT_ID, Collections.singletonList(elementId));
    properties.put(UniprotStructureProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(UniprotStructureProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<Structure> elements = uniprotStructureService.getAll(properties, Pageable.unpaged());
    if (elements.isEmpty()) {
      throw new ObjectNotFoundException("Uniprot Structure does not exist");
    }
    return elements.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addStructure(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @NotNull @PathVariable(value = "uniprotId") Integer uniprotId,
      final @Valid @RequestBody NewUniprotStructureDTO data)
      throws QueryException, ObjectNotFoundException {

    final UniprotRecord uniprot = getUniprotFromService(projectId, mapId, elementId, uniprotId);

    final Structure structure = new Structure();

    data.saveToUniprotStructure(structure, projectService.getBackgrounds(projectId, false).isEmpty());
    structure.setUniprot(uniprot);
    uniprotStructureService.add(structure);

    return serializer.prepareResponse(structure, HttpStatus.CREATED);
  }


  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{structureId}")
  public ResponseEntity<?> updateUniprot(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewUniprotStructureDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @NotNull @PathVariable(value = "uniprotId") Integer uniprotId,
      final @Valid @NotNull @PathVariable(value = "structureId") Integer structureId
  ) throws QueryException, ObjectNotFoundException {
    final Structure uniprotStructure = getStructureFromService(projectId, mapId, elementId, uniprotId, structureId);
    serializer.checkETag(oldETag, uniprotStructure);
    data.saveToUniprotStructure(uniprotStructure, projectService.getBackgrounds(projectId, false).isEmpty());
    uniprotStructureService.update(uniprotStructure);

    return serializer.prepareResponse(getStructureFromService(projectId, mapId, elementId, uniprotId, structureId));
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{structureId}")
  public void deleteUniprot(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @NotNull @PathVariable(value = "uniprotId") Integer uniprotId,
      final @Valid @NotNull @PathVariable(value = "structureId") Integer structureId
  ) throws QueryException, ObjectNotFoundException {
    final Structure structure = getStructureFromService(projectId, mapId, elementId, uniprotId, structureId);
    if (structure == null) {
      throw new ObjectNotFoundException("Uniprot Structure does not exist");
    }
    serializer.checkETag(oldETag, structure);

    uniprotStructureService.remove(structure);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> getUniprots(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @NotNull @PathVariable(value = "uniprotId") Integer uniprotId
  ) {
    final Map<UniprotStructureProperty, Object> properties = new HashMap<>();
    properties.put(UniprotStructureProperty.ELEMENT_ID, Collections.singletonList(elementId));
    properties.put(UniprotStructureProperty.UNIPROT_ID, Collections.singletonList(uniprotId));
    properties.put(UniprotStructureProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(UniprotStructureProperty.PROJECT_ID, Collections.singletonList(projectId));

    final Page<Structure> uniprots = uniprotStructureService.getAll(properties, Pageable.unpaged());

    return serializer.prepareResponse(uniprots);
  }

}