package lcsb.mapviewer.web.api.project.map.bioentity.elements;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.ElementSubmodelConnection;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.persist.dao.map.ModelProperty;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.map.bioentity.BioEntityResponseDecorator;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/elements/"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewElementsController extends BaseController {

  private final IElementService elementService;
  private final IProjectService projectService;
  private final IModelService modelService;
  private final NewApiResponseSerializer serializer;
  private final BioEntityResponseDecorator bioEntityResponseDecorator;

  @Autowired
  public NewElementsController(final IProjectService projectService, final IModelService modelService, final IElementService elementService,
                               final NewApiResponseSerializer serializer) {
    this.elementService = elementService;
    this.projectService = projectService;
    this.modelService = modelService;
    this.serializer = serializer;
    this.bioEntityResponseDecorator = new BioEntityResponseDecorator(elementService);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "{elementId}")
  public ResponseEntity<?> listElements(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") String mapId,
      final @Valid @NotBlank @PathVariable(value = "elementId") String elementId) throws QueryException, ObjectNotFoundException {

    if (!StringUtils.isNumeric(elementId)) {
      throw new QueryException("Invalid elementId: " + elementId);
    }
    if (!StringUtils.isNumeric(mapId)) {
      throw new QueryException("Invalid mapId: " + mapId);
    }
    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.ID, Collections.singletonList(Integer.valueOf(elementId)));

    properties.put(ElementProperty.PROJECT, Collections.singletonList(projectService.getProjectByProjectId(projectId)));
    if (!mapId.equals("*")) {
      try {
        properties.put(ElementProperty.MAP, Collections.singletonList(modelService.getModelByMapId(projectId, Integer.valueOf(mapId))));
      } catch (final NumberFormatException e) {
        throw new QueryException("mapId must be numeric", e);
      }
    }

    final List<Element> elements = elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent();
    if (elements.isEmpty()) {
      throw new ObjectNotFoundException("Element does not exists");
    }
    return serializer.prepareResponse(elements.get(0), bioEntityResponseDecorator);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @PostMapping(value = ":downloadCsv")
  public ResponseEntity<?> downloadCsv(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") String mapId,
      final @Valid @NotBlank @RequestBody DownloadCsvRequest request)
      throws QueryException, IOException, NumberFormatException, ObjectNotFoundException {

    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.PROJECT, Collections.singletonList(projectService.getProjectByProjectId(projectId)));
    if (request.getTypes().size() > 0) {
      properties.put(ElementProperty.CLASS, new ElementUtils().getClassesByStringTypes(request.getTypes()));
    }
    if (StringUtils.isNumeric(mapId)) {
      properties.put(ElementProperty.MAP, Collections.singletonList(modelService.getModelByMapId(projectId, Integer.valueOf(mapId))));
    } else if (request.getSubmaps().size() > 0) {
      final Map<ModelProperty, Object> mapProperties = new HashMap<>();
      mapProperties.put(ModelProperty.PROJECT_ID, Collections.singletonList(projectId));

      final List<ModelData> models = modelService.getAll(mapProperties, Pageable.unpaged()).getContent();
      final List<ModelData> filteredModels = new ArrayList<>();
      for (final ModelData modelData : models) {
        if (request.getSubmaps().contains(modelData.getId())) {
          filteredModels.add(modelData);
        }
      }

      if (filteredModels.size() == 0) {
        throw new QueryException("Submaps don't exist");
      }
      properties.put(ElementProperty.MAP, filteredModels);
    }
    if (request.getIncludedCompartmentIds().size() > 0) {
      properties.put(ElementProperty.INCLUDED_IN_COMPARTMENT, request.getIncludedCompartmentIds());
    }
    if (request.getExcludedCompartmentIds().size() > 0) {
      properties.put(ElementProperty.EXCLUDED_FROM_COMPARTMENT, request.getExcludedCompartmentIds());
    }

    final List<Element> allElements = elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent();

    final String content = prepareCsv(allElements, request.getColumns(), request.getAnnotations());
    final byte[] out = content.getBytes();

    final HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.add("content-disposition", "attachment; filename=elements.csv");
    responseHeaders.add("Content-Type", "text/csv");

    return new ResponseEntity<>(out, responseHeaders, HttpStatus.OK);
  }

  private String prepareCsv(final List<Element> allElements, final List<String> columns, final List<String> annotations)
      throws IOException, QueryException {
    final List<String> header = new ArrayList<>(columns);
    header.addAll(annotations);
    final Writer writer = new StringWriter();
    try (final CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
        .builder().setHeader(header.toArray(new String[header.size()])).build())) {
      for (final Element element : allElements) {
        csvPrinter.printRecord(getCsvRow(element, columns, annotations));
      }

      csvPrinter.flush();
      return writer.toString();
    }
  }

  private List<Object> getCsvRow(final Element element, final List<String> columns, final List<String> annotations) throws QueryException {
    final List<Object> result = new ArrayList<>();
    Object value;
    for (final String column : columns) {
      value = null;
      switch (column) {
        case ("id"):
          value = element.getId();
          break;
        case ("description"):
          value = element.getNotes();
          break;
        case ("modelId"):
          value = element.getModelData().getId();
          break;
        case ("mapName"):
          value = element.getModelData().getName();
          break;
        case ("symbol"):
          value = element.getSymbol();
          break;
        case ("abbreviation"):
          value = element.getAbbreviation();
          break;
        case ("synonyms"):
          value = StringUtils.join(element.getSynonyms(), ";");
          break;
        case ("references"):
          final List<String> refs = new ArrayList<>();
          for (final MiriamData md : element.getMiriamData()) {
            refs.add(md.getDataType() + ":" + md.getResource());
          }
          value = StringUtils.join(refs, ";");
          break;
        case ("name"):
          value = element.getName();
          break;
        case ("type"):
          value = element.getStringType();
          break;
        case ("complexId"):
          if (element instanceof Species) {
            final Complex complex = ((Species) element).getComplex();
            if (complex != null) {
              value = complex.getId();
            }
          }
          break;
        case ("complexName"):
          if (element instanceof Species) {
            value = getComplexName((Species) element);
          }
          break;
        case ("compartmentId"): {
          final Compartment compartment = element.getCompartment();
          if (compartment != null) {
            value = compartment.getId();
          }
          break;
        }
        case ("compartmentName"):
          value = getCompartmentName(element);
          break;
        case ("charge"):
          if (element instanceof Species) {
            value = ((Species) element).getCharge();
          }
          break;
        case ("fullName"):
          if (element instanceof Species) {
            value = element.getFullName();
          }
          break;
        case ("formula"):
          if (element instanceof Species) {
            value = element.getFormula();
          }
          break;

        case ("formerSymbols"):
          if (element instanceof Species) {
            value = StringUtils.join(element.getFormerSymbols(), ";");
          }
          break;
        case ("linkedSubmodelId"):
          final ElementSubmodelConnection submodel = element.getSubmodel();
          if (submodel != null) {
            value = submodel.getSubmodel().getId();
          }
          break;
        case ("elementId"):
          value = element.getElementId();
          break;
        default:
          throw new QueryException("Unknown column name: " + column);
      }
      result.add(value);
    }
    for (final String annotation : annotations) {
      value = null;
      try {
        final MiriamType type = MiriamType.valueOf(annotation);
        final List<String> refs = new ArrayList<>();
        for (final MiriamData md : element.getMiriamData()) {
          if (Objects.equals(md.getDataType(), type)) {
            refs.add(md.getResource());
          }
        }
        value = StringUtils.join(refs, ";");
        result.add(value);
      } catch (final IllegalArgumentException e) {
        throw new QueryException("Invalid annotation name: " + annotation, e);
      }
    }
    return result;
  }

  private String getCompartmentName(final Element element) {
    String compartmentName = null;
    if (element.getCompartment() != null) {
      Compartment compartment = element.getCompartment();
      if (!Hibernate.isInitialized(compartment)) {
        compartment = (Compartment) elementService.getById(compartment.getId());
      }
      compartmentName = compartment.getName();
    }
    return compartmentName;
  }

  private String getComplexName(final Species species) {
    String complexName = null;
    if (species.getComplex() != null) {
      Complex complex = species.getComplex();
      if (!Hibernate.isInitialized(complex)) {
        complex = (Complex) elementService.getById(complex.getId());
      }
      complexName = complex.getName();
    }
    return complexName;
  }

}