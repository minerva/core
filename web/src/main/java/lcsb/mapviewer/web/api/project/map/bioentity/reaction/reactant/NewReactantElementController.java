package lcsb.mapviewer.web.api.project.map.bioentity.reaction.reactant;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.persist.dao.map.ReactionProperty;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.NewIdDTO;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/reactants/{reactantId}/element",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewReactantElementController {

  private final IReactionService reactionService;
  private final IElementService elementService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewReactantElementController(final IReactionService reactionService,
                                      final IElementService elementService,
                                      final NewApiResponseSerializer serializer) {
    this.reactionService = reactionService;
    this.elementService = elementService;
    this.serializer = serializer;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping("/")
  public ResponseEntity<?> getElement(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @NotNull @PathVariable(value = "reactantId") Integer reactantId)
      throws QueryException, ObjectNotFoundException {
    final Reactant reactant = getReactantFromService(projectId, mapId, reactionId, reactantId);

    return serializer.prepareResponse(elementService.getById(reactant.getElement().getId()));

  }

  private Element getElementFromService(final String projectId, final Integer mapId, final Integer elementId)
      throws ObjectNotFoundException {
    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.ID, Collections.singletonList(elementId));
    properties.put(ElementProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(ElementProperty.PROJECT_ID, Collections.singletonList(projectId));
    final List<Element> elements = elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent();
    if (elements.size() == 0) {
      return null;
    }
    return elements.get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping("/")
  public ResponseEntity<?> updateElement(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @RequestBody NewIdDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @NotNull @PathVariable(value = "reactantId") Integer reactantId)
      throws QueryException, ObjectNotFoundException {
    final Reactant reactant = getReactantFromService(projectId, mapId, reactionId, reactantId);
    serializer.checkETag(oldETag, reactant);
    Element element = null;
    element = getElementFromService(projectId, mapId, data.getId());
    if (element == null) {
      throw new QueryException("Element does not exist");
    } else if (!(element instanceof Species)) {
      throw new QueryException("Element with given id is not a species");
    }
    reactant.setElement(element);
    reactionService.update(reactant.getReaction());
    return serializer.prepareResponse(element);
  }

  private Reactant getReactantFromService(final String projectId, final Integer mapId, final Integer reactionId, final Integer reactantId)
      throws ObjectNotFoundException {
    final Map<ReactionProperty, Object> properties = new HashMap<>();
    properties.put(ReactionProperty.ID, Collections.singletonList(reactionId));
    properties.put(ReactionProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(ReactionProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<Reaction> reactions = reactionService.getAll(properties, Pageable.unpaged(), true);
    if (reactions.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Reaction does not exist");
    }
    final Reaction reaction = reactions.getContent().get(0);
    Reactant reactant = null;
    for (final Reactant md : reaction.getReactants()) {
      if (md.getId() == reactantId) {
        reactant = md;
      }
    }

    if (reactant == null) {
      throw new ObjectNotFoundException("Reactant does not exist");
    }
    return reactant;
  }

}