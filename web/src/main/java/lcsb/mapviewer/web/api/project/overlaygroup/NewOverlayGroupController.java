package lcsb.mapviewer.web.api.project.overlaygroup;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.overlay.DataOverlayGroup;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.map.DataOverlayGroupProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IDataOverlayGroupService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/overlay_groups"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewOverlayGroupController extends BaseController {

  private final IDataOverlayGroupService dataOverlayGroupService;
  private final NewApiResponseSerializer serializer;
  private final IProjectService projectService;
  private final IUserService userService;

  public NewOverlayGroupController(final IProjectService projectService,
                                   final NewApiResponseSerializer serializer,
                                   final IUserService userService,
                                   final IDataOverlayGroupService dataOverlayGroupService) {
    this.serializer = serializer;
    this.projectService = projectService;
    this.userService = userService;
    this.dataOverlayGroupService = dataOverlayGroupService;
  }


  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{overlayGroupId}")
  public ResponseEntity<?> getOverlayGroup(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "overlayGroupId") Integer overlayGroupId,
      final Authentication authentication)
      throws ObjectNotFoundException {

    DataOverlayGroup group = getDataOverlayGroup(projectId, overlayGroupId, authentication.getName());
    return serializer.prepareResponse(group);
  }

  private DataOverlayGroup getDataOverlayGroup(
      final String projectId,
      final Integer overlayGroupId,
      final String login
  ) throws ObjectNotFoundException {
    Map<DataOverlayGroupProperty, Object> filter = new HashMap<>();
    filter.put(DataOverlayGroupProperty.PROJECT_ID, projectId);
    filter.put(DataOverlayGroupProperty.USER_LOGIN, login);
    filter.put(DataOverlayGroupProperty.ID, overlayGroupId);
    final Page<DataOverlayGroup> page = dataOverlayGroupService.getAll(filter, Pageable.unpaged());
    if (page.getContent().isEmpty()) {
      throw new ObjectNotFoundException("No overlay group found for id " + overlayGroupId);
    }
    return page.getContent().get(0);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('READ_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addOverlayGroup(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @RequestBody NewDataOverlayGroupDTO data,
      final Authentication authentication)
      throws ObjectNotFoundException {

    final User user = userService.getUserByLogin(authentication.getName());
    final Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given projectId does not exist");
    }

    final DataOverlayGroup group = new DataOverlayGroup();
    data.saveToDataOverlayGroup(group);
    group.setProject(project);
    group.setOwner(user);
    dataOverlayGroupService.add(group);

    return serializer.prepareResponse(group, HttpStatus.CREATED);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)"
      + " or hasAuthority('READ_PROJECT:' + #projectId)")
  @PutMapping(value = "/{overlayGroupId}")
  public ResponseEntity<?> updateGroup(
      final Authentication authentication,
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewDataOverlayGroupDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "overlayGroupId") Integer groupId) throws QueryException, ObjectNotFoundException {
    final DataOverlayGroup group = getDataOverlayGroup(projectId, groupId, authentication.getName());
    serializer.checkETag(oldETag, group);

    data.saveToDataOverlayGroup(group);
    dataOverlayGroupService.update(group);

    return serializer.prepareResponse(group);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)"
      + " or hasAuthority('READ_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{overlayGroupId}")
  public void deleteGroup(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final Authentication authentication,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "overlayGroupId") Integer groupId)
      throws QueryException, ObjectNotFoundException {

    final DataOverlayGroup group = getDataOverlayGroup(projectId, groupId, authentication.getName());
    serializer.checkETag(oldETag, group);
    dataOverlayGroupService.remove(group);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listUserGroups(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final Authentication authentication) {
    Map<DataOverlayGroupProperty, Object> filter = new HashMap<>();
    filter.put(DataOverlayGroupProperty.PROJECT_ID, projectId);
    filter.put(DataOverlayGroupProperty.USER_LOGIN, authentication.getName());
    final Page<DataOverlayGroup> page = dataOverlayGroupService.getAll(filter, pageable);
    return serializer.prepareResponse(page);
  }

}