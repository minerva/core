package lcsb.mapviewer.web.api.project.overlay;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IDataOverlayService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.user.NewUserLoginDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/overlays/{overlayId}/owner"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewOverlayOwnerController {

  private static final Logger logger = LogManager.getLogger();

  private final IDataOverlayService dataOverlayService;
  private final IUserService userService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewOverlayOwnerController(final IDataOverlayService dataOverlayService,
                                   final IUserService userService,
                                   final NewApiResponseSerializer serializer) {
    this.dataOverlayService = dataOverlayService;
    this.userService = userService;
    this.serializer = serializer;
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('WRITE_PROJECT:' + #projectId)")
  @PostMapping
  public void updateOwner(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @RequestBody NewUserLoginDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "overlayId") Integer overlayId) throws QueryException, ObjectNotFoundException {
    final DataOverlay overlay = dataOverlayService.getDataOverlayById(projectId, overlayId);
    serializer.checkETag(oldETag, overlay);
    final User user = userService.getUserByLogin(data.getLogin());
    if (user == null) {
      throw new QueryException("User with given login does not exist");
    }

    overlay.setCreator(user);
    dataOverlayService.update(overlay);
  }

}