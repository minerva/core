package lcsb.mapviewer.web.api.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import lcsb.mapviewer.model.user.User;

public class UserLoginSerializer extends JsonSerializer<User> {

  @Override
  public void serialize(final User entry, final JsonGenerator gen, final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeStringField("login", entry.getLogin());
    gen.writeEndObject();
  }
}