package lcsb.mapviewer.web.api.project.shape;

public abstract class NewShapeDTO {

  private final String type;

  public NewShapeDTO(final String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }
}
