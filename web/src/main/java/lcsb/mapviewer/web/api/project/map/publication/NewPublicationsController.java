package lcsb.mapviewer.web.api.project.map.publication;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.CustomPage;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.Article;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.ArticleProperty;
import lcsb.mapviewer.persist.dao.map.ReactionProperty;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IPublicationService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/publications",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewPublicationsController extends BaseController {

  private final IModelService modelService;

  private final IPublicationService publicationService;

  private final IElementService elementService;
  private final IReactionService reactionService;

  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewPublicationsController(final IModelService modelService,
                                   final IPublicationService publicationService,
                                   final IElementService elementService,
                                   final IReactionService reactionService,
                                   final NewApiResponseSerializer serializer) {
    this.modelService = modelService;
    this.publicationService = publicationService;
    this.elementService = elementService;
    this.reactionService = reactionService;
    this.serializer = serializer;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> getPublications(
      final @PathVariable(value = "projectId") String projectId,
      final @Valid @Pattern(regexp = "^[-+]?\\d+$|^\\*$") @PathVariable(value = "mapId") String modelId,
      final @RequestParam(value = "search", defaultValue = "") String search,
      final Pageable pageable) throws QueryException, ObjectNotFoundException {

    final List<ModelData> models = modelService.getModelsByMapId(projectId, modelId);
    if (models.isEmpty()) {
      return serializer.prepareResponse(new CustomPage<>(new ArrayList<>(), pageable, 0, 0));
    }

    final Map<ArticleProperty, Object> filterOptions = new HashMap<>();
    filterOptions.put(ArticleProperty.MODEL, models);

    if (search != null && !search.trim().isEmpty()) {
      filterOptions.put(ArticleProperty.TEXT, search);
    }
    final Page<Article> articles = publicationService.getAll(filterOptions, pageable);

    final List<MiriamData> mds = new ArrayList<>();
    for (final Article article : articles) {
      mds.add(new MiriamData(MiriamType.PUBMED, article.getPubmedId()));
    }

    final Map<ElementProperty, Object> elementSearch = new HashMap<>();
    elementSearch.put(ElementProperty.ANNOTATION, mds);
    elementSearch.put(ElementProperty.MAP, models);

    final List<BioEntity> bioEntities = new ArrayList<>(elementService.getElementsByFilter(elementSearch, Pageable.unpaged(), true).getContent());

    final Map<ReactionProperty, Object> reactionSearch = new HashMap<>();
    reactionSearch.put(ReactionProperty.ANNOTATION, mds);
    reactionSearch.put(ReactionProperty.MAP, models);

    bioEntities.addAll(reactionService.getAll(reactionSearch, Pageable.unpaged(), true).getContent());

    final List<NewPublicationDTO> resultList = new ArrayList<>();

    final Map<MiriamData, List<BioEntity>> publications = new HashMap<>();

    for (final BioEntity bioEntity : bioEntities) {
      for (final MiriamData md : bioEntity.getMiriamData()) {
        if (md.getDataType().equals(MiriamType.PUBMED)) {
          final List<BioEntity> list = publications.computeIfAbsent(md, k -> new ArrayList<>());
          list.add(bioEntity);
        }
      }

    }

    for (final Article article : articles) {
      final List<BioEntity> elements = publications.computeIfAbsent(new MiriamData(MiriamType.PUBMED, article.getPubmedId()),
          k -> new ArrayList<>());
      final NewPublicationDTO entry = new NewPublicationDTO();
      entry.setElements(elements);
      entry.setArticle(article);
      resultList.add(entry);
    }

    final long count = publicationService.getCount(filterOptions);

    Page<NewPublicationDTO> page = new CustomPage<>(resultList, pageable, count, articles.getTotalElements());
    return serializer.prepareResponse(page);

  }
}