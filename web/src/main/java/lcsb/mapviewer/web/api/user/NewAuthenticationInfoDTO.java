package lcsb.mapviewer.web.api.user;

import lcsb.mapviewer.web.api.AbstractDTO;

import javax.validation.constraints.NotNull;

public class NewAuthenticationInfoDTO extends AbstractDTO {

  @NotNull
  private final String login;

  @NotNull
  private final String token;

  public NewAuthenticationInfoDTO(final String login, final String token) {
    this.login = login;
    this.token = token;
  }

  public String getLogin() {
    return login;
  }

  public String getToken() {
    return token;
  }
}
