package lcsb.mapviewer.web.api.project.map.bioentity.element;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/formerSymbols",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewElementFormerSymbolsController {

  private final IElementService elementService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewElementFormerSymbolsController(
      final NewApiResponseSerializer serializer,
      final IElementService elementService) {
    this.serializer = serializer;
    this.elementService = elementService;
  }

  private Element getFormerSymbolFromService(final String projectId, final Integer mapId, final Integer elementId) throws ObjectNotFoundException {
    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.ID, Collections.singletonList(elementId));
    properties.put(ElementProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(ElementProperty.PROJECT_ID, Collections.singletonList(projectId));
    final List<Element> elements = elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent();
    if (elements.size() == 0) {
      throw new ObjectNotFoundException("Element does not exist");
    }
    return elements.get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addFormerSymbol(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @RequestBody NewFormerSymbolDTO data)
      throws QueryException, ObjectNotFoundException {

    final Element element = getFormerSymbolFromService(projectId, mapId, elementId);
    element.addFormerSymbol(data.getSymbol());
    elementService.update(element);

    return serializer.prepareResponse(HttpStatus.CREATED);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{formerSymbol}")
  public void deleteFormerSymbol(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @NotNull @PathVariable(value = "formerSymbol") String formerSymbol)
      throws QueryException, ObjectNotFoundException {
    final Element element = getFormerSymbolFromService(projectId, mapId, elementId);
    if (!element.getFormerSymbols().contains(formerSymbol)) {
      throw new ObjectNotFoundException("FormerSymbol does not exist");
    }
    element.getFormerSymbols().remove(formerSymbol);
    elementService.update(element);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listFormerSymbols(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "elementId") Integer elementId) throws QueryException, ObjectNotFoundException {
    final List<String> formerSymbols = getFormerSymbolFromService(projectId, mapId, elementId).getFormerSymbols();

    return ResponseEntity.ok(serializer.createRawPage(formerSymbols, pageable));
  }

}