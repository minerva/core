package lcsb.mapviewer.web.api.project.map.bioentity.element.uniprot;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.species.field.Structure;

import javax.validation.constraints.NotNull;

public class NewUniprotStructureDTO extends lcsb.mapviewer.web.api.AbstractDTO {

  private String pdbId;

  @NotNull
  private String chainId;

  private Double coverage;

  private Double resolution;

  private Integer structStart;

  private Integer structEnd;

  private Integer unpStart;

  private Integer unpEnd;

  private String experimentalMethod;

  private Integer taxId;


  public void saveToUniprotStructure(
      final Structure uniprotStructure,
      final boolean allowLayoutChanges) throws QueryException {

    uniprotStructure.setUnpStart(this.getUnpStart());
    uniprotStructure.setUnpEnd(this.getUnpEnd());
    uniprotStructure.setPdbId(this.getPdbId());
    uniprotStructure.setChainId(this.getChainId());
    uniprotStructure.setCoverage(this.getCoverage());
    uniprotStructure.setResolution(this.getResolution());
    uniprotStructure.setStructStart(this.getStructStart());
    uniprotStructure.setStructEnd(this.getStructEnd());
    uniprotStructure.setTaxId(this.getTaxId());
    uniprotStructure.setExperimentalMethod(this.getExperimentalMethod());
  }

  public String getPdbId() {
    return pdbId;
  }

  public void setPdbId(final String pdbId) {
    this.pdbId = pdbId;
  }

  public String getChainId() {
    return chainId;
  }

  public void setChainId(final String chainId) {
    this.chainId = chainId;
  }

  public Double getCoverage() {
    return coverage;
  }

  public void setCoverage(final Double coverage) {
    this.coverage = coverage;
  }

  public Double getResolution() {
    return resolution;
  }

  public void setResolution(final Double resolution) {
    this.resolution = resolution;
  }

  public Integer getStructStart() {
    return structStart;
  }

  public void setStructStart(final Integer structStart) {
    this.structStart = structStart;
  }

  public Integer getStructEnd() {
    return structEnd;
  }

  public void setStructEnd(final Integer structEnd) {
    this.structEnd = structEnd;
  }

  public Integer getUnpStart() {
    return unpStart;
  }

  public void setUnpStart(final Integer unpStart) {
    this.unpStart = unpStart;
  }

  public Integer getUnpEnd() {
    return unpEnd;
  }

  public void setUnpEnd(final Integer unpEnd) {
    this.unpEnd = unpEnd;
  }

  public String getExperimentalMethod() {
    return experimentalMethod;
  }

  public void setExperimentalMethod(final String experimentalMethod) {
    this.experimentalMethod = experimentalMethod;
  }

  public Integer getTaxId() {
    return taxId;
  }

  public void setTaxId(final Integer taxId) {
    this.taxId = taxId;
  }
}

