package lcsb.mapviewer.web.api.project.shape;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.sbml.species.render.ShapeFactory;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.sbo.SBOTermModificationResidueType;
import lcsb.mapviewer.model.map.sbo.SBOTermReactionType;
import lcsb.mapviewer.model.map.sbo.SBOTermSpeciesType;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/shapes",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewShapeController {

  private final Logger logger = LogManager.getLogger();

  private final NewApiResponseSerializer serializer;

  private final ShapeFactory shapeFactory = new ShapeFactory();
  private final NewShapeDTOFactory newShapeDTOFactory = new NewShapeDTOFactory();

  private final IProjectService projectService;

  @Autowired
  public NewShapeController(final NewApiResponseSerializer serializer, final IProjectService projectService) {
    this.serializer = serializer;
    this.projectService = projectService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listShapes(
      @SuppressWarnings("unused") final @PathVariable(value = "projectId") String projectId
  ) throws IOException {
    final Project project = projectService.getProjectByProjectId(projectId);
    final List<NewShapeDataDTO> shapes = new ArrayList<>();
    final List<String> sboTerms = getAllSboTerms();
    for (final String sboTerm : sboTerms) {
      final List<GraphicalPrimitive1D> sbmlDefinitions = new ArrayList<>();
      try {
        sbmlDefinitions.addAll(shapeFactory.createShapeForSboTerm(sboTerm, project.isSbgnFormat()));
      } catch (final NotImplementedException e) {
        logger.error("Unknown shape for {}", sboTerm);
      }
      final List<NewShapeDTO> sboTermShapes = newShapeDTOFactory.createNewShapeDTOs(sbmlDefinitions);
      shapes.add(new NewShapeDataDTO(sboTerm, sboTermShapes));
    }
    return serializer.prepareResponse(shapes);
  }

  private List<String> getAllSboTerms() {
    final List<String> result = new ArrayList<>();
    for (final SBOTermSpeciesType type : SBOTermSpeciesType.values()) {
      result.addAll(type.getSboTerms());
    }
    for (final SBOTermModificationResidueType type : SBOTermModificationResidueType.values()) {
      result.addAll(type.getSboTerms());
    }

    for (final SBOTermReactionType type : SBOTermReactionType.values()) {
      String sbo = type.getSBO();
      if (sbo != null) {
        result.add(sbo);
      }
    }

    return result;
  }
}