package lcsb.mapviewer.web.api.project.linetype;

import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.web.api.AbstractDTO;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class LineTypeDTO extends AbstractDTO {
  @NotNull
  private final String name;

  @NotNull
  private final List<Double> pattern = new ArrayList<>();

  public LineTypeDTO(final LineType lineType) {
    name = lineType.name();
    for (float f : lineType.getPattern()) {
      pattern.add((double) f);
    }
  }

  public String getName() {
    return name;
  }

  public List<Double> getPattern() {
    return pattern;
  }

}
