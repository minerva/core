package lcsb.mapviewer.web.api.project.map.layer.line;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.persist.dao.graphics.LayerProperty;
import lcsb.mapviewer.persist.dao.graphics.PolylineDataProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.ILayerLineService;
import lcsb.mapviewer.services.interfaces.ILayerService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.ResponseDecorator;
import lcsb.mapviewer.web.api.project.map.bioentity.reaction.NewLineDTO;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/lines",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewLayerLineController {

  private final ILayerLineService layerLineService;
  private final ILayerService layerService;
  private final NewApiResponseSerializer serializer;

  private final ResponseDecorator polylineDataDecorator = new ResponseDecorator() {
    @Override
    public List<Pair<String, Object>> getDecoratedValues(final MinervaEntity object) {
      List<Pair<String, Object>> result = new ArrayList<>();
      if (object instanceof PolylineData) {
        Layer layer = ((PolylineData) object).getLayer();
        if (layer != null) {
          result.add(new Pair<>("layer", layer.getId()));
        } else {
          result.add(new Pair<>("layer", null));
        }
      }
      return result;
    }

    @Override
    public List<Pair<String, Object>> getDecoratedValuesForRaw(final Object object) {
      return Collections.emptyList();
    }
  };


  @Autowired
  public NewLayerLineController(
      final NewApiResponseSerializer serializer,
      final ILayerLineService layerLineService,
      final ILayerService layerService) {
    this.serializer = serializer;
    this.layerLineService = layerLineService;
    this.layerService = layerService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{lineId}")
  public ResponseEntity<?> getLine(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "layerId") String stringLayerId,
      final @NotNull @PathVariable(value = "lineId") Integer lineId)
      throws QueryException, ObjectNotFoundException {
    Integer layerId = stringLayerId.equals("*") ? null : Integer.parseInt(stringLayerId);
    final PolylineData line = getLineFromService(projectId, mapId, layerId, lineId);
    return serializer.prepareResponse(line, polylineDataDecorator);
  }

  private PolylineData getLineFromService(
      final String projectId,
      final Integer mapId,
      final Integer layerId,
      final Integer lineId) throws ObjectNotFoundException {
    final Map<PolylineDataProperty, Object> properties = new HashMap<>();
    properties.put(PolylineDataProperty.ID, Collections.singletonList(lineId));
    if (layerId != null) {
      properties.put(PolylineDataProperty.LAYER_ID, Collections.singletonList(layerId));
    }
    properties.put(PolylineDataProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(PolylineDataProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<PolylineData> lines = layerLineService.getAll(properties, Pageable.unpaged());
    if (lines.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Line does not exist");
    }
    return lines.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{lineId}")
  public ResponseEntity<?> updateLine(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewLineDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "layerId") Integer layerId,
      final @Valid @NotNull @PathVariable(value = "lineId") Integer lineId)
      throws QueryException, ObjectNotFoundException {
    final PolylineData line = getLineFromService(projectId, mapId, layerId, lineId);
    serializer.checkETag(oldETag, line);
    data.saveToLine(line, true);
    layerLineService.update(line);

    return serializer.prepareResponse(getLineFromService(projectId, mapId, layerId, lineId), polylineDataDecorator);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{lineId}")
  public void deleteLine(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "layerId") Integer layerId,
      final @Valid @NotNull @PathVariable(value = "lineId") Integer lineId)
      throws QueryException, ObjectNotFoundException {
    final PolylineData line = getLineFromService(projectId, mapId, layerId, lineId);
    serializer.checkETag(oldETag, line);

    layerLineService.remove(line);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listLines(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "layerId") Integer layerId
  ) {
    final Map<PolylineDataProperty, Object> properties = new HashMap<>();
    properties.put(PolylineDataProperty.LAYER_ID, Collections.singletonList(layerId));
    properties.put(PolylineDataProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(PolylineDataProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<PolylineData> page = layerLineService.getAll(properties, pageable);

    return serializer.prepareResponse(page, polylineDataDecorator);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addLine(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "layerId") Integer layerId,
      final @Valid @RequestBody NewLineDTO data)
      throws QueryException, ObjectNotFoundException {

    final Layer layer = getLayerFromService(projectId, mapId, layerId);

    final PolylineData line = new PolylineData();

    data.saveToLine(line, true);
    line.setLayer(layer);
    layerLineService.add(line);

    return serializer.prepareResponse(getLineFromService(projectId, mapId, layerId, line.getId()), polylineDataDecorator, HttpStatus.CREATED);
  }

  private Layer getLayerFromService(final String projectId, final Integer mapId, final Integer layerId) throws ObjectNotFoundException {
    final Map<LayerProperty, Object> properties = new HashMap<>();
    properties.put(LayerProperty.ID, Collections.singletonList(layerId));
    properties.put(LayerProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(LayerProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<Layer> elements = layerService.getAll(properties, Pageable.unpaged());
    if (elements.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Layer does not exist");
    }
    return elements.getContent().get(0);
  }

}