package lcsb.mapviewer.web.api.project.map.bioentity.reaction.operator;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.sbo.SBOTermOperatorType;
import lcsb.mapviewer.web.api.AbstractDTO;
import org.hibernate.validator.constraints.NotBlank;

public class NewOperatorNodeDTO extends AbstractDTO {

  @NotBlank
  private String sboTerm;

  public void saveToNode(final NodeOperator node) throws QueryException {
    checkEquality("SBOterm", getClazz(), node.getClass());
  }

  public String getSboTerm() {
    return sboTerm;
  }

  public void setSboTerm(final String sboTerm) {
    this.sboTerm = sboTerm;
  }

  public Class<? extends NodeOperator> getClazz() {
    return SBOTermOperatorType.getClazzFromSboTerm(sboTerm);
  }

}
