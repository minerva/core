package lcsb.mapviewer.web.api.user;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.web.api.ResponseDecorator;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class NewUserResponseDecorator implements ResponseDecorator {

  private final SessionRegistry sessionRegistry;
  private final Collection<String> ldapLogins;

  public NewUserResponseDecorator(final Collection<String> ldapLogins, final SessionRegistry sessionRegistry) {
    this.ldapLogins = ldapLogins;
    this.sessionRegistry = sessionRegistry;
  }

  @Override
  public List<Pair<String, Object>> getDecoratedValues(final MinervaEntity object) {
    List<Pair<String, Object>> result = new ArrayList<>();
    if (object instanceof User) {
      User user = (User) object;

      result.add(new Pair<>("ldapAccountAvailable", this.ldapLogins.contains(user.getLogin())));
      result.add(new Pair<>("lastActive", getLastActive(user)));
    }
    return result;
  }

  @Override
  public List<Pair<String, Object>> getDecoratedValuesForRaw(final Object object) {
    return new ArrayList<>();
  }

  private Calendar getLastActive(final User user) {
    Calendar result = null;
    for (final Object principal : sessionRegistry.getAllPrincipals()) {
      String login = principal.toString();
      if (principal instanceof org.springframework.security.core.userdetails.User) {
        login = ((org.springframework.security.core.userdetails.User) principal).getUsername();
      }
      if (Objects.equals(user.getLogin(), login)) {
        for (final SessionInformation sessionInformation : sessionRegistry.getAllSessions(principal, false)) {
          final Calendar date = Calendar.getInstance();
          date.setTime(sessionInformation.getLastRequest());
          if (result == null) {
            result = date;
          } else if (result.before(date)) {
            result = date;
          }
        }
      }
    }
    return result;
  }


}
