package lcsb.mapviewer.web.api.project.map.bioentity.reaction;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.persist.dao.map.ReactionProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewModelReactionController {

  private IModelService modelService;
  private IProjectService projectService;
  private IReactionService reactionService;
  private NewApiResponseSerializer serializer;

  @Autowired
  public NewModelReactionController(
      final IModelService modelService,
      final NewApiResponseSerializer serializer,
      final IReactionService reactionService,
      final IProjectService projectService) {
    this.modelService = modelService;
    this.serializer = serializer;
    this.reactionService = reactionService;
    this.projectService = projectService;
    this.reactionService = reactionService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{reactionId}")
  public ResponseEntity<?> getReaction(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId)
      throws QueryException, ObjectNotFoundException {
    Reaction reaction = getReactionFromService(projectId, mapId, reactionId);
    return serializer.prepareResponse(reaction);
  }

  private Reaction getReactionFromService(final String projectId, final Integer mapId, final Integer reactionId) throws ObjectNotFoundException {
    Map<ReactionProperty, Object> properties = new HashMap<>();
    properties.put(ReactionProperty.ID, Arrays.asList(reactionId));
    properties.put(ReactionProperty.MAP_ID, Arrays.asList(mapId));
    properties.put(ReactionProperty.PROJECT_ID, Arrays.asList(projectId));
    Page<Reaction> reactions = reactionService.getAll(properties, Pageable.unpaged(), true);
    if (reactions.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Reaction does not exist");
    }
    return reactions.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addReaction(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @RequestBody NewReactionDTO data)
      throws QueryException, ObjectNotFoundException {

    ModelData model = modelService.getModelByMapId(projectId, mapId);

    Class<? extends Reaction> clazz = data.getClazz();
    Reaction reaction;
    try {
      reaction = clazz.getConstructor(String.class).newInstance(data.getReactionId());
    } catch (Exception e) {
      throw new QueryException("Invalid reaction class", e);
    }

    data.saveToReaction(reaction, projectService.getBackgrounds(projectId, false).size() == 0);
    reaction.setModelData(model);
    reactionService.add(reaction);

    return serializer.prepareResponse(getReactionFromService(projectId, mapId, reaction.getId()), HttpStatus.CREATED);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{reactionId}")
  public ResponseEntity<?> updateReaction(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewReactionDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "reactionId") Integer reactionId)
      throws QueryException, ObjectNotFoundException {
    Reaction reaction = reactionService.getReactionById(projectId, mapId, reactionId);
    serializer.checkETag(oldETag, reaction);
    data.saveToReaction(reaction, projectService.getBackgrounds(projectId, false).size() == 0);
    reactionService.update(reaction);

    return serializer.prepareResponse(getReactionFromService(projectId, mapId, reactionId));
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{reactionId}")
  public void deleteReaction(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "reactionId") Integer reactionId)
      throws QueryException, ObjectNotFoundException {
    Reaction reaction = reactionService.getReactionById(projectId, mapId, reactionId);
    if (reaction == null) {
      throw new ObjectNotFoundException("Reaction does not exist");
    }
    serializer.checkETag(oldETag, reaction);
    reactionService.remove(reaction);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listReactions(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId) throws QueryException {
    Map<ReactionProperty, Object> properties = new HashMap<>();

    properties.put(ReactionProperty.PROJECT_ID, Arrays.asList(projectId));
    properties.put(ReactionProperty.MAP_ID, Arrays.asList(mapId));

    Page<Reaction> page = reactionService.getAll(properties, pageable, true);

    return serializer.prepareResponse(page);
  }

}