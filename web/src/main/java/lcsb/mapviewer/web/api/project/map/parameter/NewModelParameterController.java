package lcsb.mapviewer.web.api.project.map.parameter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.map.kinetics.SbmlParameterProperty;
import lcsb.mapviewer.persist.dao.map.kinetics.SbmlUnitProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.ISbmlParameterService;
import lcsb.mapviewer.services.interfaces.ISbmlUnitService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/parameters",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewModelParameterController {

  private IModelService modelService;
  private ISbmlParameterService parameterService;
  private ISbmlUnitService unitService;
  private NewApiResponseSerializer serializer;

  @Autowired
  public NewModelParameterController(
      final IModelService modelService,
      final NewApiResponseSerializer serializer,
      final ISbmlUnitService unitService,
      final ISbmlParameterService parameterService) {
    this.modelService = modelService;
    this.serializer = serializer;
    this.parameterService = parameterService;
    this.unitService = unitService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{parameterId}")
  public ResponseEntity<?> getParameter(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "parameterId") Integer parameterId)
      throws QueryException, ObjectNotFoundException {
    SbmlParameter parameter = getParameterFromService(projectId, mapId, parameterId);
    return serializer.prepareResponse(parameter);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addParameter(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @RequestBody NewParameterDTO data)
      throws QueryException, ObjectNotFoundException {

    ModelData model = modelService.getModelByMapId(projectId, mapId);
    if (model == null) {
      throw new ObjectNotFoundException("Map does not exist");
    }

    SbmlParameter parameter = new SbmlParameter("");
    data.saveToSbmlParameter(parameter, getUnitsForMap(projectId, mapId));
    model.addParameter(parameter);
    modelService.update(model);

    return serializer.prepareResponse(getParameterFromService(projectId, mapId, parameter.getId()), HttpStatus.CREATED);
  }

  private List<SbmlUnit> getUnitsForMap(final String projectId, final Integer mapId) {
    Map<SbmlUnitProperty, Object> filter = new HashMap<>();
    filter.put(SbmlUnitProperty.PROJECT_ID, projectId);
    filter.put(SbmlUnitProperty.MAP_ID, mapId);
    return unitService.getAll(filter, Pageable.unpaged()).getContent();
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{parameterId}")
  public ResponseEntity<?> updateParameter(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewParameterDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "parameterId") Integer parameterId)
      throws QueryException, ObjectNotFoundException {
    SbmlParameter parameter = getParameterFromService(projectId, mapId, parameterId);
    serializer.checkETag(oldETag, parameter);
    data.saveToSbmlParameter(parameter, getUnitsForMap(projectId, mapId));
    parameterService.update(parameter);

    return serializer.prepareResponse(parameterService.getById(parameterId));
  }

  private SbmlParameter getParameterFromService(final String projectId, final Integer mapId, final Integer parameterId)
      throws QueryException, ObjectNotFoundException {
    Map<SbmlParameterProperty, Object> filter = new HashMap<>();
    filter.put(SbmlParameterProperty.PROJECT_ID, projectId);
    filter.put(SbmlParameterProperty.MAP_ID, mapId);
    filter.put(SbmlParameterProperty.ID, parameterId);
    Page<SbmlParameter> page = parameterService.getAll(filter, Pageable.unpaged());

    if (page.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Parameter does not exist");
    }
    return page.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{parameterId}")
  public void deleteParameter(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "parameterId") Integer parameterId)
      throws QueryException, ObjectNotFoundException {
    SbmlParameter parameter = getParameterFromService(projectId, mapId, parameterId);
    if (parameter == null) {
      throw new ObjectNotFoundException("Parameter does not exist");
    }
    serializer.checkETag(oldETag, parameter);
    parameterService.remove(parameter);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listParameters(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId) throws QueryException {
    Map<SbmlParameterProperty, Object> filter = new HashMap<>();
    filter.put(SbmlParameterProperty.PROJECT_ID, projectId);
    filter.put(SbmlParameterProperty.MAP_ID, mapId);
    Page<SbmlParameter> page = parameterService.getAll(filter, pageable);

    return serializer.prepareResponse(page);
  }

}