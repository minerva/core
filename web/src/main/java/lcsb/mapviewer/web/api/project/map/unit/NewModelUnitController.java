package lcsb.mapviewer.web.api.project.map.unit;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.map.kinetics.SbmlUnitProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.ISbmlUnitService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/units",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewModelUnitController {

  private IModelService modelService;
  private ISbmlUnitService unitService;
  private NewApiResponseSerializer serializer;

  @Autowired
  public NewModelUnitController(
      final IModelService modelService,
      final NewApiResponseSerializer serializer,
      final ISbmlUnitService unitService) {
    this.modelService = modelService;
    this.serializer = serializer;
    this.unitService = unitService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{unitId}")
  public ResponseEntity<?> getUnit(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "unitId") Integer unitId)
      throws QueryException, ObjectNotFoundException {
    SbmlUnit unit = getUnitFromService(projectId, mapId, unitId);
    return serializer.prepareResponse(unit);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addUnit(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @RequestBody NewUnitDTO data)
      throws QueryException, ObjectNotFoundException {

    ModelData model = modelService.getModelByMapId(projectId, mapId);
    if (model == null) {
      throw new ObjectNotFoundException("Map does not exist");
    }

    SbmlUnit unit = new SbmlUnit("");
    data.saveToSbmlUnit(unit);
    unit.setModel(model);
    unitService.add(unit);

    return serializer.prepareResponse(getUnitFromService(projectId, mapId, unit.getId()), HttpStatus.CREATED);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{unitId}")
  public ResponseEntity<?> updateUnit(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewUnitDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "unitId") Integer unitId)
      throws QueryException, ObjectNotFoundException {
    SbmlUnit unit = getUnitFromService(projectId, mapId, unitId);
    serializer.checkETag(oldETag, unit);
    data.saveToSbmlUnit(unit);
    unitService.update(unit);

    return serializer.prepareResponse(unitService.getById(unitId));
  }

  private SbmlUnit getUnitFromService(final String projectId, final Integer mapId, final Integer unitId)
      throws QueryException, ObjectNotFoundException {
    Map<SbmlUnitProperty, Object> filter = new HashMap<>();
    filter.put(SbmlUnitProperty.PROJECT_ID, projectId);
    filter.put(SbmlUnitProperty.MAP_ID, mapId);
    filter.put(SbmlUnitProperty.ID, unitId);
    Page<SbmlUnit> page = unitService.getAll(filter, Pageable.unpaged());
    if (page.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Unit does not exist");
    }
    return page.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{unitId}")
  public void deleteUnit(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "unitId") Integer unitId)
      throws QueryException, ObjectNotFoundException {
    SbmlUnit unit = getUnitFromService(projectId, mapId, unitId);
    if (unit == null) {
      throw new ObjectNotFoundException("Unit does not exist");
    }
    serializer.checkETag(oldETag, unit);
    Map<SbmlUnitProperty, Object> filter = new HashMap<>();
    filter.put(SbmlUnitProperty.ID, unitId);
    filter.put(SbmlUnitProperty.IN_USE, true);
    if (unitService.getCount(filter) > 0) {
      throw new QueryException("Cannot delete unit. It's used by elements on the map");
    }
    unitService.remove(unit);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listUnits(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId) throws QueryException {
    Map<SbmlUnitProperty, Object> filter = new HashMap<>();
    filter.put(SbmlUnitProperty.PROJECT_ID, projectId);
    filter.put(SbmlUnitProperty.MAP_ID, mapId);
    Page<SbmlUnit> page = unitService.getAll(filter, pageable);

    return serializer.prepareResponse(page);
  }

}