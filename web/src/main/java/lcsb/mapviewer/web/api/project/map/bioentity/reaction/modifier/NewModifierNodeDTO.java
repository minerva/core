package lcsb.mapviewer.web.api.project.map.bioentity.reaction.modifier;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.sbo.SBOTermModifierType;
import lcsb.mapviewer.web.api.project.map.bioentity.reaction.NewReactionNodeDTO;
import org.hibernate.validator.constraints.NotBlank;

public class NewModifierNodeDTO extends NewReactionNodeDTO {

  @NotBlank
  private String sboTerm;

  @Override
  public void saveToNode(final ReactionNode node) throws QueryException {
    checkEquality("SBOterm", getClazz(), node.getClass());
    super.saveToNode(node);
  }

  public String getSboTerm() {
    return sboTerm;
  }

  public void setSboTerm(final String sboTerm) {
    this.sboTerm = sboTerm;
  }

  public Class<? extends Modifier> getClazz() {
    return SBOTermModifierType.getClazzFromSboTerm(sboTerm);
  }

}
