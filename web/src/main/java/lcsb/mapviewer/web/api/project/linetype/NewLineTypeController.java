package lcsb.mapviewer.web.api.project.linetype;

import lcsb.mapviewer.converter.model.sbml.species.render.ShapeFactory;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.shape.NewShapeDTOFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/lineTypes",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewLineTypeController {

  private final Logger logger = LogManager.getLogger();

  private final NewApiResponseSerializer serializer;

  private final ShapeFactory shapeFactory = new ShapeFactory();
  private final NewShapeDTOFactory newShapeDTOFactory = new NewShapeDTOFactory();

  @Autowired
  public NewLineTypeController(
      final NewApiResponseSerializer serializer) {
    this.serializer = serializer;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listTypes(
      @SuppressWarnings("unused") final @PathVariable(value = "projectId") String projectId
  ) throws IOException {
    final List<LineTypeDTO> shapes = new ArrayList<>();
    for (final LineType lineType : LineType.values()) {
      shapes.add(new LineTypeDTO(lineType));
    }
    return serializer.prepareResponse(shapes);
  }

}