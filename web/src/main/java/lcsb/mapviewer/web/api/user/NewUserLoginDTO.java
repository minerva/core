package lcsb.mapviewer.web.api.user;

import lcsb.mapviewer.web.api.AbstractDTO;

import javax.validation.constraints.NotNull;

public class NewUserLoginDTO extends AbstractDTO {

  @NotNull
  private String login;

  public String getLogin() {
    return login;
  }

  public void setLogin(final String login) {
    this.login = login;
  }
}
