package lcsb.mapviewer.web.api.project.map;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import lcsb.mapviewer.api.LayoutChangeNotSupportedException;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.model.map.model.ModelData;

public class NewModelDataDTO {

  private static DoubleComparator doubleComparator = new DoubleComparator();

  @NotNull
  @Min(1)
  private Double width;

  @NotNull
  @Min(1)
  private Double height;

  private Double defaultCenterX;

  private Double defaultCenterY;

  private Integer defaultZoomLevel;

  @NotNull
  private String notes;

  @NotBlank
  private String name;

  public void saveToModelData(final ModelData modelData, final boolean allowLayoutChanges) throws QueryException {
    if (!allowLayoutChanges) {
      checkEquality("width", width, modelData.getWidth());
      checkEquality("height", height, modelData.getHeight());
    } else {
      modelData.setWidth(width);
      modelData.setHeight(width);
    }
    modelData.setDefaultCenterX(defaultCenterX);
    modelData.setDefaultCenterY(defaultCenterY);
    modelData.setDefaultZoomLevel(defaultZoomLevel);
    modelData.setNotes(notes);
    modelData.setName(name);
  }

  public Double getWidth() {
    return width;
  }

  public void setWidth(final Double width) {
    this.width = width;
  }

  public Double getHeight() {
    return height;
  }

  public void setHeight(final Double height) {
    this.height = height;
  }

  public Double getDefaultCenterX() {
    return defaultCenterX;
  }

  public void setDefaultCenterX(final Double defaultCenterX) {
    this.defaultCenterX = defaultCenterX;
  }

  public Double getDefaultCenterY() {
    return defaultCenterY;
  }

  public void setDefaultCenterY(final Double defaultCenterY) {
    this.defaultCenterY = defaultCenterY;
  }

  public Integer getDefaultZoomLevel() {
    return defaultZoomLevel;
  }

  public void setDefaultZoomLevel(final Integer defaultZoomLevel) {
    this.defaultZoomLevel = defaultZoomLevel;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(final String notes) {
    this.notes = notes;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  private void checkEquality(final String fieldName, final Double newValue, final Double origianlValue) throws LayoutChangeNotSupportedException {
    if (doubleComparator.compare(newValue, origianlValue) != 0) {
      throw new LayoutChangeNotSupportedException(fieldName, newValue, origianlValue);
    }
  }

}
