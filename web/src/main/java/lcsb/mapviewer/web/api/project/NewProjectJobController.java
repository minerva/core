package lcsb.mapviewer.web.api.project;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.job.MinervaJob;
import lcsb.mapviewer.persist.dao.MinervaJobProperty;
import lcsb.mapviewer.services.FailedDependencyException;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId:.+}/job",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewProjectJobController {

  private final IProjectService projectService;
  private final IMinervaJobService minervaJobService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewProjectJobController(
      final IProjectService projectService,
      final NewApiResponseSerializer serializer,
      final IMinervaJobService minervaJobService
  ) {
    this.projectService = projectService;
    this.serializer = serializer;
    this.minervaJobService = minervaJobService;
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)")
  @GetMapping
  public ResponseEntity<?> getLogEntries(
      final @NotBlank @PathVariable(value = "projectId") String projectId,
      final Pageable pageable)
      throws ObjectNotFoundException, FailedDependencyException, IOException {
    Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException();
    }
    Map<MinervaJobProperty, Object> searchFilter = new HashMap<>();
    searchFilter.put(MinervaJobProperty.EXTERNAL_OBJECT_CLASS, Collections.singleton(Project.class));
    searchFilter.put(MinervaJobProperty.EXTERNAL_OBJECT_ID, Collections.singleton(project.getId()));

    Page<MinervaJob> page = minervaJobService.getAll(searchFilter, pageable);

    return serializer.prepareResponse(page);

  }
}