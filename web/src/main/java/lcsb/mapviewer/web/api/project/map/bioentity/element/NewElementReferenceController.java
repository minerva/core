package lcsb.mapviewer.web.api.project.map.bioentity.element;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IMiriamService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.NewMiriamDataDTO;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/references",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewElementReferenceController {

  private final IMiriamService referenceService;
  private final IElementService elementService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewElementReferenceController(
      final NewApiResponseSerializer serializer,
      final IMiriamService referenceService,
      final IElementService elementService) {
    this.serializer = serializer;
    this.referenceService = referenceService;
    this.elementService = elementService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{referenceId}")
  public ResponseEntity<?> getReference(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @NotNull @PathVariable(value = "referenceId") Integer referenceId)
      throws QueryException, ObjectNotFoundException {
    final MiriamData reference = getReferenceFromService(projectId, mapId, elementId, referenceId);
    return serializer.prepareResponse(reference);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addReference(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @RequestBody NewMiriamDataDTO data)
      throws QueryException, ObjectNotFoundException {

    final Element element = getElementFromService(projectId, mapId, elementId);

    final MiriamData reference = new MiriamData();
    data.saveToMiriamData(reference);
    element.addMiriamData(reference);
    elementService.update(element);

    return serializer.prepareResponse(getReferenceFromService(projectId, mapId, elementId, reference.getId()), HttpStatus.CREATED);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{referenceId}")
  public ResponseEntity<?> updateReference(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewMiriamDataDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @NotNull @PathVariable(value = "referenceId") Integer referenceId)
      throws QueryException, ObjectNotFoundException {
    final MiriamData reference = getReferenceFromService(projectId, mapId, elementId, referenceId);
    serializer.checkETag(oldETag, reference);
    data.saveToMiriamData(reference);
    referenceService.update(reference);

    return serializer.prepareResponse(referenceService.getById(referenceId));
  }

  private MiriamData getReferenceFromService(final String projectId, final Integer mapId, final Integer elementId, final Integer referenceId)
      throws QueryException, ObjectNotFoundException {
    MiriamData reference = null;
    final Element element = getElementFromService(projectId, mapId, elementId);
    for (final MiriamData md : element.getMiriamData()) {
      if (md.getId() == referenceId) {
        reference = md;
      }
    }

    if (reference == null) {
      throw new ObjectNotFoundException("Reference does not exist");
    }
    return reference;
  }

  private Element getElementFromService(final String projectId, final Integer mapId, final Integer elementId) throws ObjectNotFoundException {
    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.ID, Collections.singletonList(elementId));
    properties.put(ElementProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(ElementProperty.PROJECT_ID, Collections.singletonList(projectId));
    final List<Element> elements = elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent();
    if (elements.size() == 0) {
      throw new ObjectNotFoundException("Element does not exist");
    }
    return elements.get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{referenceId}")
  public void deleteReference(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @NotNull @PathVariable(value = "referenceId") Integer referenceId)
      throws QueryException, ObjectNotFoundException {
    final MiriamData reference = getReferenceFromService(projectId, mapId, elementId, referenceId);
    serializer.checkETag(oldETag, reference);
    referenceService.delete(reference);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listReferences(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId) throws QueryException, ObjectNotFoundException {
    final Element element = getElementFromService(projectId, mapId, elementId);
    final List<MiriamData> references = new ArrayList<>(element.getMiriamData());

    references.sort(new Comparator<MinervaEntity>() {
      @Override
      public int compare(final MinervaEntity o1, final MinervaEntity o2) {
        return o1.getId() - o2.getId();
      }
    });

    return serializer.prepareResponse(serializer.createPage(references, pageable));
  }

}