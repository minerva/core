package lcsb.mapviewer.web.api.project.map.bioentity.element;

import org.hibernate.validator.constraints.NotBlank;

import lcsb.mapviewer.web.api.AbstractDTO;

public class NewFormerSymbolDTO extends AbstractDTO {

  @NotBlank
  private String symbol;

  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(final String symbol) {
    this.symbol = symbol;
  }
}
