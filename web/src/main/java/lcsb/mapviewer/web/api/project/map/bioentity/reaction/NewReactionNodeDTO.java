package lcsb.mapviewer.web.api.project.map.bioentity.reaction;

import javax.validation.constraints.NotNull;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.web.api.AbstractDTO;
import lcsb.mapviewer.web.api.project.NewIdDTO;

public class NewReactionNodeDTO extends AbstractDTO {

  private Double stoichiometry;

  @NotNull
  private NewIdDTO element;

  public void saveToNode(final ReactionNode node) throws QueryException {
    checkEquality("element.id", element.getId(), node.getElement().getId());
    node.setStoichiometry(stoichiometry);
  }

  public Double getStoichiometry() {
    return stoichiometry;
  }

  public void setStoichiometry(final Double stoichiometry) {
    this.stoichiometry = stoichiometry;
  }

  public NewIdDTO getElement() {
    return element;
  }

  public void setElement(final NewIdDTO element) {
    this.element = element;
  }
}
