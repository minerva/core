package lcsb.mapviewer.web.api.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.model.map.OverviewImageLink;
import lcsb.mapviewer.model.map.OverviewLink;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

public class ProjectSerializer extends JsonSerializer<Project> {

  private static final Logger logger = LogManager.getLogger();

  @Override
  public void serialize(final Project entry, final JsonGenerator gen,
                        final SerializerProvider serializers)
      throws IOException {
    gen.writeStartObject();

    gen.writeStringField("version", entry.getVersion());
    gen.writeObjectField("disease", entry.getDisease());
    gen.writeObjectField("organism", entry.getOrganism());
    gen.writeNumberField("id", entry.getId());

    gen.writeStringField("status", entry.getStatus().toString());

    gen.writeStringField("directory", entry.getDirectory());
    gen.writeNumberField("progress", entry.getProgress());
    gen.writeStringField("notifyEmail", entry.getNotifyEmail());
    gen.writeBooleanField("logEntries", !entry.getLogEntries().isEmpty());

    gen.writeStringField("name", entry.getName());
    gen.writeBooleanField("sbgnFormat", entry.isSbgnFormat());

    gen.writeObjectFieldStart("owner");
    gen.writeStringField("login", entry.getOwner().getLogin());
    gen.writeEndObject();

    gen.writeStringField("projectId", entry.getProjectId());
    gen.writeObjectField("creationDate", entry.getCreationDate());
    gen.writeObjectField("overviewImageViews", entry.getOverviewImages());

    Set<OverviewImage> set = new LinkedHashSet<>(entry.getOverviewImages());
    for (final OverviewImage image : entry.getOverviewImages()) {
      for (final OverviewLink ol : image.getLinks()) {
        if (ol instanceof OverviewImageLink) {
          set.remove(((OverviewImageLink) ol).getLinkedOverviewImage());
        }
      }
    }
    if (!set.isEmpty()) {
      gen.writeObjectField("topOverviewImage", set.iterator().next());
    } else if (!entry.getOverviewImages().isEmpty()) {
      logger.warn("Cannot determine top level image. Taking first one. {}", entry.getOverviewImages().get(0).getFilename());
      gen.writeObjectField("topOverviewImage", entry.getOverviewImages().get(0));
    } else {
      gen.writeObjectField("topOverviewImage", null);
    }

    gen.writeObjectFieldStart("topMap");
    Integer id = null;
    if (entry.getTopModelData() != null) {
      id = entry.getTopModelData().getId();
    }
    gen.writeObjectField("id", id);
    gen.writeEndObject();

    gen.writeObjectField("license", entry.getLicense());
    gen.writeStringField("customLicenseName", entry.getCustomLicenseName());
    gen.writeStringField("customLicenseUrl", entry.getCustomLicenseUrl());

    gen.writeEndObject();
  }
}