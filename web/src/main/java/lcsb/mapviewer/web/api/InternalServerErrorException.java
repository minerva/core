package lcsb.mapviewer.web.api;

import java.io.IOException;

public class InternalServerErrorException extends RuntimeException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public InternalServerErrorException(final String message) {
    super(message);
  }

  public InternalServerErrorException(final String message, final IOException exception) {
    super(message, exception);
  }

}
