package lcsb.mapviewer.web.api.project.map.bioentity.reaction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.persist.dao.map.ReactionProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IMiriamService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.NewMiriamDataDTO;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/references",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewReactionReferenceController {

  private IMiriamService referenceService;
  private IReactionService reactionService;
  private NewApiResponseSerializer serializer;

  @Autowired
  public NewReactionReferenceController(
      final NewApiResponseSerializer serializer,
      final IMiriamService referenceService,
      final IReactionService reactionService) {
    this.serializer = serializer;
    this.referenceService = referenceService;
    this.reactionService = reactionService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{referenceId}")
  public ResponseEntity<?> getReference(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @NotNull @PathVariable(value = "referenceId") Integer referenceId)
      throws QueryException, ObjectNotFoundException {
    MiriamData reference = getReferenceFromService(projectId, mapId, reactionId, referenceId);
    return serializer.prepareResponse(reference);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addReference(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @RequestBody NewMiriamDataDTO data)
      throws QueryException, ObjectNotFoundException {

    Reaction reaction = getReactionFromService(projectId, mapId, reactionId);

    MiriamData reference = new MiriamData();
    data.saveToMiriamData(reference);
    reaction.addMiriamData(reference);
    reactionService.update(reaction);

    return serializer.prepareResponse(getReferenceFromService(projectId, mapId, reactionId, reference.getId()), HttpStatus.CREATED);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{referenceId}")
  public ResponseEntity<?> updateReference(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewMiriamDataDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @NotNull @PathVariable(value = "referenceId") Integer referenceId)
      throws QueryException, ObjectNotFoundException {
    MiriamData reference = getReferenceFromService(projectId, mapId, reactionId, referenceId);
    serializer.checkETag(oldETag, reference);
    data.saveToMiriamData(reference);
    referenceService.update(reference);

    return serializer.prepareResponse(referenceService.getById(referenceId));
  }

  private MiriamData getReferenceFromService(final String projectId, final Integer mapId, final Integer reactionId, final Integer referenceId)
      throws QueryException, ObjectNotFoundException {
    MiriamData reference = null;
    Reaction reaction = getReactionFromService(projectId, mapId, reactionId);
    for (MiriamData md : reaction.getMiriamData()) {
      if (md.getId() == referenceId) {
        reference = md;
      }
    }

    if (reference == null) {
      throw new ObjectNotFoundException("Reference does not exist");
    }
    return reference;
  }

  private Reaction getReactionFromService(final String projectId, final Integer mapId, final Integer reactionId) throws ObjectNotFoundException {
    Map<ReactionProperty, Object> properties = new HashMap<>();
    properties.put(ReactionProperty.ID, Arrays.asList(reactionId));
    properties.put(ReactionProperty.MAP_ID, Arrays.asList(mapId));
    properties.put(ReactionProperty.PROJECT_ID, Arrays.asList(projectId));
    Page<Reaction> reactions = reactionService.getAll(properties, Pageable.unpaged());
    if (reactions.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Reaction does not exist");
    }
    return reactions.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{referenceId}")
  public void deleteReference(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @NotNull @PathVariable(value = "referenceId") Integer referenceId)
      throws QueryException, ObjectNotFoundException {
    MiriamData reference = getReferenceFromService(projectId, mapId, reactionId, referenceId);
    serializer.checkETag(oldETag, reference);
    referenceService.delete(reference);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listReferences(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId) throws QueryException, ObjectNotFoundException {
    Reaction reaction = getReactionFromService(projectId, mapId, reactionId);
    List<MiriamData> references = new ArrayList<>(reaction.getMiriamData());

    references.sort(new Comparator<MinervaEntity>() {
      @Override
      public int compare(final MinervaEntity o1, final MinervaEntity o2) {
        return o1.getId() - o2.getId();
      }
    });

    return serializer.prepareResponse(serializer.createPage(references, pageable));
  }

}