package lcsb.mapviewer.web.api.project.map.bioentity.element;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.persist.dao.map.kinetics.SbmlUnitProperty;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.ISbmlUnitService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.NewIdDTO;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/unit",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewElementUnitController {

  private final IElementService elementService;
  private final ISbmlUnitService unitService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewElementUnitController(final IElementService elementService,
                                  final NewApiResponseSerializer serializer,
                                  final ISbmlUnitService unitService) {
    this.elementService = elementService;
    this.serializer = serializer;
    this.unitService = unitService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping
  public ResponseEntity<?> getUnit(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId) throws QueryException, ObjectNotFoundException {
    final Element element = getElementFromService(projectId, mapId, elementId);
    SbmlUnit unit = null;
    if (element instanceof Species) {
      unit = ((Species) element).getSubstanceUnits();
    }

    return serializer.prepareResponse(unit);

  }

  private Element getElementFromService(final String projectId, final Integer mapId, final Integer elementId)
      throws ObjectNotFoundException {
    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.ID, Collections.singletonList(elementId));
    properties.put(ElementProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(ElementProperty.PROJECT_ID, Collections.singletonList(projectId));
    final List<Element> elements = elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent();
    if (elements.size() == 0) {
      throw new ObjectNotFoundException("Element does not exist");
    }
    return elements.get(0);
  }

  private SbmlUnit getUnitFromService(final String projectId, final Integer mapId, final Integer unitId)
      throws QueryException, ObjectNotFoundException {
    final Map<SbmlUnitProperty, Object> filter = new HashMap<>();
    filter.put(SbmlUnitProperty.PROJECT_ID, projectId);
    filter.put(SbmlUnitProperty.MAP_ID, mapId);
    filter.put(SbmlUnitProperty.ID, unitId);
    final Page<SbmlUnit> page = unitService.getAll(filter, Pageable.unpaged());
    if (page.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Unit does not exist");
    }
    return page.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping
  public ResponseEntity<?> updateUnit(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @RequestBody NewIdDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId) throws QueryException, ObjectNotFoundException {
    final Element element = getElementFromService(projectId, mapId, elementId);
    serializer.checkETag(oldETag, element);
    if (!(element instanceof Species)) {
      throw new QueryException("Only species can have unit defined");
    }
    SbmlUnit unit = null;
    if (data.getId() != null) {
      unit = getUnitFromService(projectId, mapId, data.getId());
    }
    ((Species) element).setSubstanceUnits(unit);
    elementService.update(element);
    if (unit == null) {
      return serializer.prepareResponse(HttpStatus.OK);
    }
    return serializer.prepareResponse(unit);
  }

}