package lcsb.mapviewer.web.api;

import java.util.ArrayList;
import java.util.List;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.MinervaEntity;

public interface ResponseDecorator {

  List<Pair<String, Object>> getDecoratedValues(MinervaEntity object);

  List<Pair<String, Object>> getDecoratedValuesForRaw(Object object);

  public static ResponseDecorator EMPTY = new ResponseDecorator() {
    @Override
    public List<Pair<String, Object>> getDecoratedValues(final MinervaEntity object) {
      return new ArrayList<>();
    }

    @Override
    public List<Pair<String, Object>> getDecoratedValuesForRaw(final Object object) {
      return new ArrayList<>();
    }
  };
}
