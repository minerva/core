package lcsb.mapviewer.web.api.project.overlay;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayGroup;
import lcsb.mapviewer.model.overlay.DataOverlayType;
import lcsb.mapviewer.web.api.AbstractDTO;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.function.Function;

public class NewDataOverlayDTO extends AbstractDTO {

  @NotNull
  @NotBlank
  private String name;

  private Integer group;

  @NotNull
  private Boolean isPublic;

  @NotNull
  private DataOverlayType colorSchemaType;

  @NotNull
  @Min(0)
  private Integer orderIndex;

  @NotNull
  private String description;

  private String genomeVersion;

  private ReferenceGenomeType genomeType;


  public void saveToDataOverlay(final DataOverlay overlay, final Function<Integer, DataOverlayGroup> groupIdToGroup) throws QueryException {
    overlay.setName(name);
    overlay.setGroup(groupIdToGroup.apply(group));
    overlay.setPublic(isPublic);
    overlay.setColorSchemaType(colorSchemaType);
    overlay.setOrderIndex(orderIndex);
    overlay.setDescription(description);
    overlay.setGenomeVersion(genomeVersion);
    overlay.setGenomeType(genomeType);
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public Boolean getPublic() {
    return isPublic;
  }

  public void setPublic(final Boolean isPublic) {
    this.isPublic = isPublic;
  }

  public DataOverlayType getColorSchemaType() {
    return colorSchemaType;
  }

  public void setColorSchemaType(final DataOverlayType colorSchemaType) {
    this.colorSchemaType = colorSchemaType;
  }

  public Integer getOrderIndex() {
    return orderIndex;
  }

  public void setOrderIndex(final Integer orderIndex) {
    this.orderIndex = orderIndex;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public String getGenomeVersion() {
    return genomeVersion;
  }

  public void setGenomeVersion(final String genomeVersion) {
    this.genomeVersion = genomeVersion;
  }

  public ReferenceGenomeType getGenomeType() {
    return genomeType;
  }

  public void setGenomeType(final ReferenceGenomeType genomeType) {
    this.genomeType = genomeType;
  }

  public Integer getGroup() {
    return group;
  }

  public void setGroup(final Integer group) {
    this.group = group;
  }
}
