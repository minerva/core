package lcsb.mapviewer.web.api.project.map.layer;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.graphics.LayerProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.ILayerService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/layers",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewLayerController {

  private final IModelService modelService;
  private final ILayerService layerService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewLayerController(
      final IModelService modelService,
      final NewApiResponseSerializer serializer,
      final ILayerService layerService) {
    this.modelService = modelService;
    this.serializer = serializer;
    this.layerService = layerService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{layerId}")
  public ResponseEntity<?> getLayer(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "layerId") Integer elementId)
      throws ObjectNotFoundException {
    final Layer layer = getLayerFromService(projectId, mapId, elementId);
    return serializer.prepareResponse(layer);
  }

  private Layer getLayerFromService(final String projectId, final Integer mapId, final Integer elementId) throws ObjectNotFoundException {
    final Map<LayerProperty, Object> properties = new HashMap<>();
    properties.put(LayerProperty.ID, Collections.singletonList(elementId));
    properties.put(LayerProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(LayerProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<Layer> elements = layerService.getAll(properties, Pageable.unpaged());
    if (elements.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Layer does not exist");
    }
    return elements.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addLayer(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @RequestBody NewLayerDTO data)
      throws QueryException, ObjectNotFoundException {

    final ModelData model = modelService.getModelByMapId(projectId, mapId);

    final Layer layer = new Layer();

    data.saveToLayer(layer);
    layer.setModel(model);
    layerService.add(layer);

    return serializer.prepareResponse(getLayerFromService(projectId, mapId, layer.getId()), HttpStatus.CREATED);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{layerId}")
  public ResponseEntity<?> updateLayer(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewLayerDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "layerId") Integer layerId)
      throws QueryException, ObjectNotFoundException {
    final Layer layer = getLayerFromService(projectId, mapId, layerId);
    serializer.checkETag(oldETag, layer);
    data.saveToLayer(layer);
    layerService.update(layer);

    return serializer.prepareResponse(getLayerFromService(projectId, mapId, layerId));
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{layerId}")
  public void deleteLayer(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "layerId") Integer layerId)
      throws QueryException, ObjectNotFoundException {
    final Layer layer = getLayerFromService(projectId, mapId, layerId);
    if (layer == null) {
      throw new ObjectNotFoundException("Layer does not exist");
    }
    serializer.checkETag(oldETag, layer);

    layerService.remove(layer);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listLayers(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId) throws QueryException {
    final Map<LayerProperty, Object> properties = new HashMap<>();

    properties.put(LayerProperty.PROJECT_ID, Collections.singletonList(projectId));
    properties.put(LayerProperty.MAP_ID, Collections.singletonList(mapId));

    final Page<Layer> page = layerService.getAll(properties, pageable);

    return serializer.prepareResponse(page);
  }

}