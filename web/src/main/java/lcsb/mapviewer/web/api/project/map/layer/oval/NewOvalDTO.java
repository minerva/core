package lcsb.mapviewer.web.api.project.map.layer.oval;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.web.api.AbstractDTO;

import javax.validation.constraints.NotNull;
import java.awt.Color;

public class NewOvalDTO extends AbstractDTO {

  @NotNull
  private Color color;

  @NotNull
  private Double x;

  @NotNull
  private Double y;

  @NotNull
  private Double width;

  @NotNull
  private Double height;

  @NotNull
  private Integer z;

  public void saveToOval(final LayerOval oval) throws QueryException {
    oval.setColor(color);
    oval.setX(x);
    oval.setY(y);
    oval.setZ(z);
    oval.setWidth(width);
    oval.setHeight(height);
  }

  public Color getColor() {
    return color;
  }

  public void setColor(final Color color) {
    this.color = color;
  }

  public Double getX() {
    return x;
  }

  public void setX(final Double x) {
    this.x = x;
  }

  public Double getY() {
    return y;
  }

  public void setY(final Double y) {
    this.y = y;
  }

  public Double getWidth() {
    return width;
  }

  public void setWidth(final Double width) {
    this.width = width;
  }

  public Double getHeight() {
    return height;
  }

  public void setHeight(final Double height) {
    this.height = height;
  }

  public Integer getZ() {
    return z;
  }

  public void setZ(final Integer z) {
    this.z = z;
  }
}
