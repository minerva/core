package lcsb.mapviewer.web.api.project.map.layer.image;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerImage;
import lcsb.mapviewer.persist.dao.graphics.GlyphProperty;
import lcsb.mapviewer.persist.dao.graphics.LayerImageProperty;
import lcsb.mapviewer.persist.dao.graphics.LayerProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IGlyphService;
import lcsb.mapviewer.services.interfaces.ILayerImageService;
import lcsb.mapviewer.services.interfaces.ILayerService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/images",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewImageController {

  private final ILayerService layerService;
  private final NewApiResponseSerializer serializer;
  private final ILayerImageService layerImageService;
  private final IGlyphService glyphService;

  @Autowired
  public NewImageController(
      final NewApiResponseSerializer serializer,
      final ILayerService layerService,
      final ILayerImageService layerImageService,
      final IGlyphService glyphService) {
    this.serializer = serializer;
    this.layerService = layerService;
    this.layerImageService = layerImageService;
    this.glyphService = glyphService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{imageId}")
  public ResponseEntity<?> getImage(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "layerId") String stringLayerId,
      final @NotNull @PathVariable(value = "imageId") Integer imageId
  )
      throws ObjectNotFoundException {
    Integer layerId = stringLayerId.equals("*") ? null : Integer.parseInt(stringLayerId);
    final LayerImage image = getImageFromService(projectId, mapId, layerId, imageId);
    return serializer.prepareResponse(image);
  }

  private LayerImage getImageFromService(
      final String projectId,
      final Integer mapId,
      final Integer layerId,
      final Integer imageId
  ) throws ObjectNotFoundException {
    final Map<LayerImageProperty, Object> properties = new HashMap<>();
    properties.put(LayerImageProperty.ID, Collections.singletonList(imageId));
    if (layerId != null) {
      properties.put(LayerImageProperty.LAYER_ID, Collections.singletonList(layerId));
    }
    properties.put(LayerImageProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(LayerImageProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<LayerImage> elements = layerImageService.getAll(properties, Pageable.unpaged());
    if (elements.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Image does not exist");
    }
    return elements.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addImage(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "layerId") Integer layerId,
      final @Valid @RequestBody NewImageDTO data)
      throws QueryException, ObjectNotFoundException {

    final Layer layer = getLayerFromService(projectId, mapId, layerId);

    final LayerImage image = new LayerImage();

    data.saveToLayer(image, (glyphId) -> getGlyphFromService(projectId, glyphId));
    image.setLayer(layer);

    layerImageService.add(image);

    return serializer.prepareResponse(getImageFromService(projectId, mapId, layer.getId(), image.getId()), HttpStatus.CREATED);
  }

  private Layer getLayerFromService(final String projectId, final Integer mapId, final Integer elementId) throws ObjectNotFoundException {
    final Map<LayerProperty, Object> properties = new HashMap<>();
    properties.put(LayerProperty.ID, Collections.singletonList(elementId));
    properties.put(LayerProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(LayerProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<Layer> elements = layerService.getAll(properties, Pageable.unpaged());
    if (elements.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Layer does not exist");
    }
    return elements.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{imageId}")
  public ResponseEntity<?> updateImage(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewImageDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "layerId") Integer layerId,
      final @Valid @NotNull @PathVariable(value = "imageId") Integer imageId
  )
      throws QueryException, ObjectNotFoundException {
    final LayerImage image = getImageFromService(projectId, mapId, layerId, imageId);
    serializer.checkETag(oldETag, image);
    data.saveToLayer(image, (glyphId) -> getGlyphFromService(projectId, glyphId));
    layerImageService.update(image);

    return serializer.prepareResponse(getImageFromService(projectId, mapId, layerId, imageId));
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{imageId}")
  public void deleteText(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "layerId") Integer layerId,
      final @Valid @NotNull @PathVariable(value = "imageId") Integer imageId
  )
      throws QueryException, ObjectNotFoundException {
    final LayerImage text = getImageFromService(projectId, mapId, layerId, imageId);

    serializer.checkETag(oldETag, text);

    layerImageService.remove(text);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listImages(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "layerId") Integer layerId
  ) {
    final Map<LayerImageProperty, Object> properties = new HashMap<>();

    properties.put(LayerImageProperty.PROJECT_ID, Collections.singletonList(projectId));
    properties.put(LayerImageProperty.LAYER_ID, Collections.singletonList(layerId));
    properties.put(LayerImageProperty.MAP_ID, Collections.singletonList(mapId));

    final Page<LayerImage> page = layerImageService.getAll(properties, pageable);

    return serializer.prepareResponse(page);
  }

  private Glyph getGlyphFromService(final String projectId, final Integer glyphId)
      throws ObjectNotFoundException {
    final Map<GlyphProperty, Object> properties = new HashMap<>();
    properties.put(GlyphProperty.ID, glyphId);
    properties.put(GlyphProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<Glyph> glyphs = glyphService.getAll(properties, Pageable.unpaged());
    if (glyphs.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Glyph does not exist");
    }
    return glyphs.getContent().get(0);
  }

}