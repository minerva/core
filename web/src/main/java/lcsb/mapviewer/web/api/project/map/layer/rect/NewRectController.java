package lcsb.mapviewer.web.api.project.map.layer.rect;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.persist.dao.graphics.LayerProperty;
import lcsb.mapviewer.persist.dao.graphics.LayerRectProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.ILayerRectService;
import lcsb.mapviewer.services.interfaces.ILayerService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/rects",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewRectController {

  private final ILayerService layerService;
  private final NewApiResponseSerializer serializer;
  private final ILayerRectService layerRectService;

  @Autowired
  public NewRectController(
      final NewApiResponseSerializer serializer,
      final ILayerService layerService,
      final ILayerRectService layerRectService) {
    this.serializer = serializer;
    this.layerService = layerService;
    this.layerRectService = layerRectService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{rectId}")
  public ResponseEntity<?> getRect(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "layerId") String stringLayerId,
      final @NotNull @PathVariable(value = "rectId") Integer rectId
  )
      throws ObjectNotFoundException {
    Integer layerId = stringLayerId.equals("*") ? null : Integer.parseInt(stringLayerId);
    final LayerRect rect = getRectFromService(projectId, mapId, layerId, rectId);
    return serializer.prepareResponse(rect);
  }

  private LayerRect getRectFromService(
      final String projectId,
      final Integer mapId,
      final Integer layerId,
      final Integer rectId
  ) throws ObjectNotFoundException {
    final Map<LayerRectProperty, Object> properties = new HashMap<>();
    properties.put(LayerRectProperty.ID, Collections.singletonList(rectId));
    if (layerId != null) {
      properties.put(LayerRectProperty.LAYER_ID, Collections.singletonList(layerId));
    }
    properties.put(LayerRectProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(LayerRectProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<LayerRect> elements = layerRectService.getAll(properties, Pageable.unpaged());
    if (elements.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Rect does not exist");
    }
    return elements.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addRect(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "layerId") Integer layerId,
      final @Valid @RequestBody NewRectDTO data)
      throws QueryException, ObjectNotFoundException {

    final Layer layer = getLayerFromService(projectId, mapId, layerId);

    final LayerRect rect = new LayerRect();

    data.saveToRectangle(rect);
    rect.setLayer(layer);

    layerRectService.add(rect);

    return serializer.prepareResponse(getRectFromService(projectId, mapId, layer.getId(), rect.getId()), HttpStatus.CREATED);
  }

  private Layer getLayerFromService(final String projectId, final Integer mapId, final Integer elementId) throws ObjectNotFoundException {
    final Map<LayerProperty, Object> properties = new HashMap<>();
    properties.put(LayerProperty.ID, Collections.singletonList(elementId));
    properties.put(LayerProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(LayerProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<Layer> elements = layerService.getAll(properties, Pageable.unpaged());
    if (elements.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Layer does not exist");
    }
    return elements.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{rectId}")
  public ResponseEntity<?> updateRect(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewRectDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "layerId") Integer layerId,
      final @Valid @NotNull @PathVariable(value = "rectId") Integer rectId
  )
      throws QueryException, ObjectNotFoundException {
    final LayerRect rect = getRectFromService(projectId, mapId, layerId, rectId);
    serializer.checkETag(oldETag, rect);
    data.saveToRectangle(rect);
    layerRectService.update(rect);

    return serializer.prepareResponse(getRectFromService(projectId, mapId, layerId, rectId));
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{rectId}")
  public void deleteRect(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "layerId") Integer layerId,
      final @Valid @NotNull @PathVariable(value = "rectId") Integer rectId
  )
      throws QueryException, ObjectNotFoundException {
    final LayerRect rect = getRectFromService(projectId, mapId, layerId, rectId);

    serializer.checkETag(oldETag, rect);

    layerRectService.remove(rect);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listRects(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "layerId") Integer layerId
  ) {
    final Map<LayerRectProperty, Object> properties = new HashMap<>();

    properties.put(LayerRectProperty.PROJECT_ID, Collections.singletonList(projectId));
    properties.put(LayerRectProperty.LAYER_ID, Collections.singletonList(layerId));
    properties.put(LayerRectProperty.MAP_ID, Collections.singletonList(mapId));

    final Page<LayerRect> page = layerRectService.getAll(properties, pageable);

    return serializer.prepareResponse(page);
  }

}