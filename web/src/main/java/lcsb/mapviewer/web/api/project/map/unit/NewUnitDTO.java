package lcsb.mapviewer.web.api.project.map.unit;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitTypeFactor;
import lcsb.mapviewer.web.api.AbstractDTO;

public class NewUnitDTO extends AbstractDTO {

  @NotBlank
  @Size(max = 255)
  private String name;

  @NotBlank
  @Size(max = 255)
  private String unitId;

  private Set<NewUnitTypeFactorDTO> unitTypeFactors = new HashSet<>();

  public void saveToSbmlUnit(final SbmlUnit unit) {
    unit.setName(name);
    unit.setUnitId(unitId);
    unit.getUnitTypeFactors().clear();
    for (NewUnitTypeFactorDTO factorDTO : unitTypeFactors) {
      SbmlUnitTypeFactor factor = new SbmlUnitTypeFactor(null, 0, 0, 0);
      factorDTO.saveToSbmlTypeFactor(factor);
      unit.addUnitTypeFactor(factor);
    }
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getUnitId() {
    return unitId;
  }

  public void setUnitId(final String unitId) {
    this.unitId = unitId;
  }

  public Set<NewUnitTypeFactorDTO> getUnitTypeFactors() {
    return unitTypeFactors;
  }

  public void setUnitTypeFactors(final Set<NewUnitTypeFactorDTO> unitTypeFactors) {
    this.unitTypeFactors = unitTypeFactors;
  }

  public void addUnitTypeFactor(final NewUnitTypeFactorDTO factor) {
    this.unitTypeFactors.add(factor);
  }
}
