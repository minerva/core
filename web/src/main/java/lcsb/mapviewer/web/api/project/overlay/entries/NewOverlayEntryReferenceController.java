package lcsb.mapviewer.web.api.project.overlay.entries;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.persist.dao.map.DataOverlayEntryProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IDataOverlayEntryService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.NewMiriamDataDTO;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}/references/"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewOverlayEntryReferenceController {

  private final NewApiResponseSerializer serializer;
  private final IDataOverlayEntryService dataOverlayEntryService;


  @Autowired
  public NewOverlayEntryReferenceController(
      final NewApiResponseSerializer serializer,
      final IDataOverlayEntryService dataOverlayEntryService) {
    this.serializer = serializer;
    this.dataOverlayEntryService = dataOverlayEntryService;
  }

  @PreAuthorize("(@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name) ")
  @PostMapping(value = "/")
  public ResponseEntity<?> addReference(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "overlayId") Integer overlayId,
      final @NotNull @PathVariable(value = "overlayEntryId") Integer overlayEntryId,
      final @Valid @RequestBody NewMiriamDataDTO data)
      throws ObjectNotFoundException {

    final DataOverlayEntry overlayEntry = getDataOverlayEntryFromService(projectId, overlayId, overlayEntryId);
    final MiriamData reference = new MiriamData();
    data.saveToMiriamData(reference);
    overlayEntry.addMiriamData(reference);
    dataOverlayEntryService.update(overlayEntry);

    return serializer.prepareResponse(HttpStatus.CREATED);
  }

  @PreAuthorize("(@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name) ")
  @DeleteMapping(value = "/{referenceId}")
  public void deleteReference(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "overlayId") Integer overlayId,
      final @NotNull @PathVariable(value = "overlayEntryId") Integer overlayEntryId,
      final @Valid @NotNull @PathVariable(value = "referenceId") Integer referenceId)
      throws QueryException, ObjectNotFoundException {

    MiriamData reference = null;
    final DataOverlayEntry entry = getDataOverlayEntryFromService(projectId, overlayId, overlayEntryId);
    for (final MiriamData md : entry.getMiriamData()) {
      if (md.getId() == referenceId) {
        reference = md;
      }
    }

    if (reference == null) {
      throw new ObjectNotFoundException("Reference does not exist");
    }

    serializer.checkETag(oldETag, reference);
    entry.getMiriamData().remove(reference);
    dataOverlayEntryService.update(entry);
  }

  @PreAuthorize("(@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name) ")
  @GetMapping(value = "/")
  public ResponseEntity<?> listReferences(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "overlayId") Integer overlayId,
      final @NotNull @PathVariable(value = "overlayEntryId") Integer overlayEntryId)
      throws ObjectNotFoundException {
    final DataOverlayEntry overlayEntry = getDataOverlayEntryFromService(projectId, overlayId, overlayEntryId);

    final List<MiriamData> references = new ArrayList<>(overlayEntry.getMiriamData());
    Collections.sort(references);

    return ResponseEntity.ok(serializer.createRawPage(references, pageable));
  }

  private DataOverlayEntry getDataOverlayEntryFromService(final String projectId,
                                                          final Integer overlayId,
                                                          final Integer overlayEntryId
  ) throws ObjectNotFoundException {
    final Map<DataOverlayEntryProperty, Object> filter = new HashMap<>();
    filter.put(DataOverlayEntryProperty.PROJECT_ID, projectId);
    filter.put(DataOverlayEntryProperty.DATA_OVERLAY_ID, overlayId);
    filter.put(DataOverlayEntryProperty.ID, overlayEntryId);
    final Page<DataOverlayEntry> overlayEntry = dataOverlayEntryService.getAll(filter, Pageable.unpaged(), true);
    if (overlayEntry.getContent().isEmpty()) {
      throw new ObjectNotFoundException("Entry does not exist");
    }
    return overlayEntry.getContent().get(0);
  }

}