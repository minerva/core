package lcsb.mapviewer.web.api.project.shape;

import lcsb.mapviewer.common.exception.NotImplementedException;
import org.sbml.jsbml.ext.render.Ellipse;
import org.sbml.jsbml.ext.render.GraphicalPrimitive1D;
import org.sbml.jsbml.ext.render.Polygon;

import java.util.ArrayList;
import java.util.List;

public class NewShapeDTOFactory {
  public NewShapeDTO createNewShapeDTO(final GraphicalPrimitive1D sbmlDefinition) {
    if (sbmlDefinition instanceof Ellipse) {
      return new NewEllipseShapeDTO((Ellipse) sbmlDefinition);
    }
    if (sbmlDefinition instanceof Polygon) {
      return new NewPolygonShapeDTO((Polygon) sbmlDefinition);
    }
    throw new NotImplementedException(sbmlDefinition.getClass().getName() + " is not implemented yet");
  }

  public List<NewShapeDTO> createNewShapeDTOs(final List<GraphicalPrimitive1D> sbmlDefinitions) {
    final List<NewShapeDTO> result = new ArrayList<>();
    for (final GraphicalPrimitive1D sbmlDefinition : sbmlDefinitions) {
      result.add(createNewShapeDTO(sbmlDefinition));
    }
    return result;
  }
}
