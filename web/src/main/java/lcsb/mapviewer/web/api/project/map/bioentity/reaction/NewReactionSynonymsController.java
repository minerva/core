package lcsb.mapviewer.web.api.project.map.bioentity.reaction;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.persist.dao.map.ReactionProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.map.bioentity.NewSynonymDTO;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/synonyms",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewReactionSynonymsController {

  private IReactionService reactionService;
  private NewApiResponseSerializer serializer;

  @Autowired
  public NewReactionSynonymsController(
      final NewApiResponseSerializer serializer,
      final IReactionService reactionService) {
    this.serializer = serializer;
    this.reactionService = reactionService;
  }

  private Reaction getReactionFromService(final String projectId, final Integer mapId, final Integer reactionId) throws ObjectNotFoundException {
    Map<ReactionProperty, Object> properties = new HashMap<>();
    properties.put(ReactionProperty.ID, Arrays.asList(reactionId));
    properties.put(ReactionProperty.MAP_ID, Arrays.asList(mapId));
    properties.put(ReactionProperty.PROJECT_ID, Arrays.asList(projectId));
    Page<Reaction> reactions = reactionService.getAll(properties, Pageable.unpaged());
    if (reactions.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Reaction does not exist");
    }
    return reactions.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addSynonym(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @RequestBody NewSynonymDTO data)
      throws QueryException, ObjectNotFoundException {

    Reaction reaction = getReactionFromService(projectId, mapId, reactionId);
    reaction.addSynonym(data.getSynonym());
    reactionService.update(reaction);

    return serializer.prepareResponse(HttpStatus.CREATED);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{synonym}")
  public void deleteSynonym(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @NotNull @PathVariable(value = "synonym") String synonym)
      throws QueryException, ObjectNotFoundException {
    Reaction reaction = getReactionFromService(projectId, mapId, reactionId);
    if (!reaction.getSynonyms().contains(synonym)) {
      throw new ObjectNotFoundException("Synonym does not exist");
    }
    reaction.getSynonyms().remove(synonym);
    reactionService.update(reaction);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listSynonyms(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "reactionId") Integer reactionId) throws QueryException, ObjectNotFoundException {
    List<String> synonyms = getReactionFromService(projectId, mapId, reactionId).getSynonyms();

    return ResponseEntity.ok(serializer.createRawPage(synonyms, pageable));
  }

}