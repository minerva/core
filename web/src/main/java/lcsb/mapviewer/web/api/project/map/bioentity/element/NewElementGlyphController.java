package lcsb.mapviewer.web.api.project.map.bioentity.element;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.graphics.GlyphProperty;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IGlyphService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.NewIdDTO;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/glyph",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewElementGlyphController {

  private final IElementService elementService;
  private final IGlyphService glyphService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewElementGlyphController(final IElementService elementService,
                                   final IGlyphService glyphService,
                                   final NewApiResponseSerializer serializer) {
    this.elementService = elementService;
    this.glyphService = glyphService;
    this.serializer = serializer;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping
  public ResponseEntity<?> getGlyph(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId) throws QueryException, ObjectNotFoundException {
    final Element element = getElementFromService(projectId, mapId, elementId);
    Glyph glyph = null;
    if (element.getGlyph() != null) {
      glyph = getGlyphFromService(projectId, element.getGlyph().getId());
    }

    return serializer.prepareResponse(glyph);

  }

  private Element getElementFromService(final String projectId, final Integer mapId, final Integer elementId)
      throws ObjectNotFoundException {
    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.ID, Collections.singletonList(elementId));
    properties.put(ElementProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(ElementProperty.PROJECT_ID, Collections.singletonList(projectId));
    final List<Element> elements = elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent();
    if (elements.size() == 0) {
      throw new ObjectNotFoundException("Element does not exist");
    }
    return elements.get(0);
  }

  private Glyph getGlyphFromService(final String projectId, final Integer glyphId)
      throws ObjectNotFoundException {
    final Map<GlyphProperty, Object> properties = new HashMap<>();
    properties.put(GlyphProperty.ID, glyphId);
    properties.put(GlyphProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<Glyph> glyphs = glyphService.getAll(properties, Pageable.unpaged());
    if (glyphs.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Glyph does not exist");
    }
    return glyphs.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping
  public ResponseEntity<?> updateGlyph(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @RequestBody NewIdDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId) throws QueryException, ObjectNotFoundException {
    final Element element = getElementFromService(projectId, mapId, elementId);
    serializer.checkETag(oldETag, element);
    Glyph glyph = null;
    if (data.getId() != null) {
      glyph = getGlyphFromService(projectId, data.getId());
    }
    element.setGlyph(glyph);
    elementService.update(element);
    if (glyph == null) {
      return serializer.prepareResponse(HttpStatus.OK);
    }
    return serializer.prepareResponse(glyph);
  }

}