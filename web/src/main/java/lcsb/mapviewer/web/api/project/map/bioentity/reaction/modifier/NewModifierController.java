package lcsb.mapviewer.web.api.project.map.bioentity.reaction.modifier;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.map.ReactionProperty;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.map.bioentity.reaction.NewReactionNodeDTO;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/modifiers",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewModifierController {

  private final IReactionService reactionService;
  private final IElementService elementService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewModifierController(
      final NewApiResponseSerializer serializer,
      final IElementService elementService,
      final IReactionService reactionService) {
    this.serializer = serializer;
    this.reactionService = reactionService;
    this.elementService = elementService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{modifierId}")
  public ResponseEntity<?> getModifier(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @NotNull @PathVariable(value = "modifierId") Integer modifierId)
      throws QueryException, ObjectNotFoundException {
    final Modifier modifier = getModifierFromService(projectId, mapId, reactionId, modifierId);
    return serializer.prepareResponse(modifier);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addModifier(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @RequestBody NewModifierNodeDTO data)
      throws QueryException, ObjectNotFoundException {

    final Reaction reaction = getReactionFromService(projectId, mapId, reactionId);

    final Element element = getElementFromService(projectId, mapId, data.getElement().getId());
    final Modifier modifier;
    try {
      final Class<? extends Modifier> clazz = data.getClazz();
      modifier = clazz.getConstructor(Element.class).newInstance(element);
    } catch (final Exception e) {
      throw new QueryException("Invalid modifier class", e);
    }
    modifier.setLine(new PolylineData());
    data.saveToNode(modifier);
    reaction.addModifier(modifier);
    reactionService.update(reaction);

    return serializer.prepareResponse(getModifierFromService(projectId, mapId, reactionId, modifier.getId()), HttpStatus.CREATED);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{modifierId}")
  public ResponseEntity<?> updateModifier(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewReactionNodeDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @NotNull @PathVariable(value = "modifierId") Integer modifierId)
      throws QueryException, ObjectNotFoundException {
    final Modifier modifier = getModifierFromService(projectId, mapId, reactionId, modifierId);
    serializer.checkETag(oldETag, modifier);
    data.saveToNode(modifier);
    reactionService.update(modifier.getReaction());

    return serializer.prepareResponse(getModifierFromService(projectId, mapId, reactionId, modifierId));
  }

  private Modifier getModifierFromService(final String projectId, final Integer mapId, final Integer reactionId, final Integer modifierId)
      throws QueryException, ObjectNotFoundException {
    Modifier modifier = null;
    final Reaction reaction = getReactionFromService(projectId, mapId, reactionId);
    for (final Modifier md : reaction.getModifiers()) {
      if (md.getId() == modifierId) {
        modifier = md;
      }
    }

    if (modifier == null) {
      throw new ObjectNotFoundException("Modifier does not exist");
    }
    return modifier;
  }

  private Reaction getReactionFromService(final String projectId, final Integer mapId, final Integer reactionId) throws ObjectNotFoundException {
    final Map<ReactionProperty, Object> properties = new HashMap<>();
    properties.put(ReactionProperty.ID, Collections.singletonList(reactionId));
    properties.put(ReactionProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(ReactionProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<Reaction> reactions = reactionService.getAll(properties, Pageable.unpaged());
    if (reactions.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Reaction does not exist");
    }
    return reactions.getContent().get(0);
  }

  private Element getElementFromService(final String projectId, final Integer mapId, final Integer elementId) throws ObjectNotFoundException {
    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.ID, Collections.singletonList(elementId));
    properties.put(ElementProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(ElementProperty.PROJECT_ID, Collections.singletonList(projectId));
    final List<Element> elements = elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent();
    if (elements.size() == 0) {
      throw new ObjectNotFoundException("Reaction does not exist");
    }
    return elements.get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{modifierId}")
  public void deleteModifier(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @NotNull @PathVariable(value = "modifierId") Integer modifierId)
      throws QueryException, ObjectNotFoundException {
    final Modifier modifier = getModifierFromService(projectId, mapId, reactionId, modifierId);
    final Reaction reaction = modifier.getReaction();
    serializer.checkETag(oldETag, modifier);
    reaction.removeNode(modifier);
    reactionService.update(reaction);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listModifiers(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId) throws QueryException, ObjectNotFoundException {
    final Reaction reaction = getReactionFromService(projectId, mapId, reactionId);
    final List<Modifier> modifiers = new ArrayList<>(reaction.getModifiers());

    modifiers.sort(new Comparator<MinervaEntity>() {
      @Override
      public int compare(final MinervaEntity o1, final MinervaEntity o2) {
        return o1.getId() - o2.getId();
      }
    });

    return serializer.prepareResponse(serializer.createPage(modifiers, pageable));
  }

}