package lcsb.mapviewer.web.api.project;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.web.api.AbstractDTO;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Objects;

public class NewProjectDTO extends AbstractDTO {
  @NotBlank
  @Size(max = 255)
  @Pattern(regexp = "^[a-z0-9A-Z\\-_]+$", message = "projectId can contain only alphanumeric characters and -_")
  private String projectId;

  @NotNull
  private String name;

  @NotNull
  private String version;

  @Email
  private String notifyEmail;

  @NotNull
  private Boolean sbgnFormat;

  public void saveToProject(final Project project) throws QueryException {
    if (project.getProjectId() != null && !Objects.equals(projectId, project.getProjectId())) {
      throw new QueryException("ProjectId does not match: " + projectId + ", " + project.getProjectId());
    }
    project.setName(name);
    project.setVersion(version);
    project.setNotifyEmail(notifyEmail);
    project.setSbgnFormat(sbgnFormat);
  }

  public String getProjectId() {
    return projectId;
  }

  public String getName() {
    return name;
  }

  public String getVersion() {
    return version;
  }

  public String getNotifyEmail() {
    return notifyEmail;
  }

  public Boolean getSbgnFormat() {
    return sbgnFormat;
  }

  public void setProjectId(final String projectId) {
    this.projectId = projectId;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public void setVersion(final String version) {
    this.version = version;
  }

  public void setNotifyEmail(final String notifyEmail) {
    this.notifyEmail = notifyEmail;
  }

  public void setSbgnFormat(final Boolean sbgnFormat) {
    this.sbgnFormat = sbgnFormat;
  }

}
