package lcsb.mapviewer.web.api.user;

import lcsb.mapviewer.web.api.AbstractDTO;

import javax.validation.constraints.NotNull;

public class NewOAuthDTO extends AbstractDTO {

  @NotNull
  private String name;

  @NotNull
  private String uri;

  public NewOAuthDTO(final String name, final String uri) {
    this.name = name;
    this.uri = uri;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getUri() {
    return uri;
  }

  public void setUri(final String uri) {
    this.uri = uri;
  }
}
