package lcsb.mapviewer.web.api.serializer;

import java.io.IOException;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class PageDeserializer extends StdDeserializer<Page<?>> {

  public PageDeserializer() {
    super(Page.class);
  }

  /**
  * 
  */
  private static final long serialVersionUID = 1L;

  @Override
  public Page<?> deserialize(final JsonParser parser, final DeserializationContext ctxt) throws IOException, JsonProcessingException {
    ObjectMapper mapper = (ObjectMapper) parser.getCodec();
    ObjectNode rootNode = mapper.readTree(parser);
    List<Object> data = mapper.readValue(mapper.treeAsTokens(rootNode.get("content")), new TypeReference<List<Object>>() {
    });
    int total = mapper.readValue(mapper.treeAsTokens(rootNode.get("totalElements")), Integer.class);
    int number = mapper.readValue(mapper.treeAsTokens(rootNode.get("number")), Integer.class);
    int size = mapper.readValue(mapper.treeAsTokens(rootNode.get("size")), Integer.class);

    Pageable pageable = PageRequest.of(number, size);
    return new PageImpl<>(data, pageable, total);
  }
}
