package lcsb.mapviewer.web.api.user;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.web.api.AbstractDTO;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.plugins.validation.constraints.NotBlank;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class NewUserDTO extends AbstractDTO {

  private static final Logger logger = LogManager.getLogger();

  @NotNull
  @NotBlank
  private String login;

  @NotNull
  private String name;

  private String orcidId;

  @NotNull
  private String surname;

  @NotNull
  @NotBlank
  private String password;

  @NotNull
  private String email;

  @NotNull
  private Boolean connectedToLdap;

  @NotNull
  private Boolean active;

  @NotNull
  private Boolean termsOfUseConsent;

  public String getLogin() {
    return login;
  }

  public void setLogin(final String login) {
    this.login = login;
  }

  public void saveToUser(final User user, final PasswordEncoder passwordEncoder, final boolean isAdmin) throws QueryException {
    if (user.getLogin() != null && stringComparator.compare(user.getLogin(), login) != 0) {
      throw new QueryException("login cannot be changed");
    }
    if (!isAdmin && !Objects.equals(user.isConnectedToLdap(), getConnectedToLdap())) {
      throw new AccessDeniedException("connectedToLdap can be updated by admin");
    }
    if (!isAdmin && !Objects.equals(user.isActive(), getActive())) {
      throw new AccessDeniedException("active can be updated by admin");
    }
    if (email != null && !email.isEmpty() && !EmailValidator.getInstance().isValid(email)) {
      throw new QueryException("Invalid email address: " + email);
    }

    user.setLogin(login);
    user.setName(name);
    user.setSurname(surname);
    user.setEmail(email);
    user.setOrcidId(orcidId);
    user.setConnectedToLdap(connectedToLdap);
    user.setActive(active);
    user.setTermsOfUseConsent(termsOfUseConsent);
    user.setCryptedPassword(passwordEncoder.encode(password));
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(final String surname) {
    this.surname = surname;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(final String password) {
    this.password = password;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public String getOrcidId() {
    return orcidId;
  }

  public void setOrcidId(final String orcidId) {
    this.orcidId = orcidId;
  }

  public Boolean getConnectedToLdap() {
    return connectedToLdap;
  }

  public void setConnectedToLdap(final Boolean connectedToLdap) {
    this.connectedToLdap = connectedToLdap;
  }

  public Boolean getTermsOfUseConsent() {
    return termsOfUseConsent;
  }

  public void setTermsOfUseConsent(final Boolean termsOfUseConsent) {
    this.termsOfUseConsent = termsOfUseConsent;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(final Boolean active) {
    this.active = active;
  }
}
