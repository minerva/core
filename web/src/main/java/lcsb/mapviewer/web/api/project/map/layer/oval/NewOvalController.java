package lcsb.mapviewer.web.api.project.map.layer.oval;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.persist.dao.graphics.LayerOvalProperty;
import lcsb.mapviewer.persist.dao.graphics.LayerProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.ILayerOvalService;
import lcsb.mapviewer.services.interfaces.ILayerService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/ovals",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewOvalController {

  private final ILayerService layerService;
  private final NewApiResponseSerializer serializer;
  private final ILayerOvalService layerOvalService;

  @Autowired
  public NewOvalController(
      final NewApiResponseSerializer serializer,
      final ILayerService layerService,
      final ILayerOvalService layerOvalService) {
    this.serializer = serializer;
    this.layerService = layerService;
    this.layerOvalService = layerOvalService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{ovalId}")
  public ResponseEntity<?> getOval(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "layerId") String stringLayerId,
      final @NotNull @PathVariable(value = "ovalId") Integer ovalId
  )
      throws ObjectNotFoundException {
    Integer layerId = stringLayerId.equals("*") ? null : Integer.parseInt(stringLayerId);
    final LayerOval oval = getOvalFromService(projectId, mapId, layerId, ovalId);
    return serializer.prepareResponse(oval);
  }

  private LayerOval getOvalFromService(
      final String projectId,
      final Integer mapId,
      final Integer layerId,
      final Integer ovalId
  ) throws ObjectNotFoundException {
    final Map<LayerOvalProperty, Object> properties = new HashMap<>();
    properties.put(LayerOvalProperty.ID, Collections.singletonList(ovalId));
    if (layerId != null) {
      properties.put(LayerOvalProperty.LAYER_ID, Collections.singletonList(layerId));
    }
    properties.put(LayerOvalProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(LayerOvalProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<LayerOval> elements = layerOvalService.getAll(properties, Pageable.unpaged());
    if (elements.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Oval does not exist");
    }
    return elements.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addOval(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "layerId") Integer layerId,
      final @Valid @RequestBody NewOvalDTO data)
      throws QueryException, ObjectNotFoundException {

    final Layer layer = getLayerFromService(projectId, mapId, layerId);

    final LayerOval oval = new LayerOval();

    data.saveToOval(oval);
    oval.setLayer(layer);

    layerOvalService.add(oval);

    return serializer.prepareResponse(getOvalFromService(projectId, mapId, layer.getId(), oval.getId()), HttpStatus.CREATED);
  }

  private Layer getLayerFromService(final String projectId, final Integer mapId, final Integer elementId) throws ObjectNotFoundException {
    final Map<LayerProperty, Object> properties = new HashMap<>();
    properties.put(LayerProperty.ID, Collections.singletonList(elementId));
    properties.put(LayerProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(LayerProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<Layer> elements = layerService.getAll(properties, Pageable.unpaged());
    if (elements.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Layer does not exist");
    }
    return elements.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{ovalId}")
  public ResponseEntity<?> updateOval(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewOvalDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "layerId") Integer layerId,
      final @Valid @NotNull @PathVariable(value = "ovalId") Integer ovalId
  )
      throws QueryException, ObjectNotFoundException {
    final LayerOval oval = getOvalFromService(projectId, mapId, layerId, ovalId);
    serializer.checkETag(oldETag, oval);
    data.saveToOval(oval);
    layerOvalService.update(oval);

    return serializer.prepareResponse(getOvalFromService(projectId, mapId, layerId, ovalId));
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{ovalId}")
  public void deleteOval(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "layerId") Integer layerId,
      final @Valid @NotNull @PathVariable(value = "ovalId") Integer ovalId
  )
      throws QueryException, ObjectNotFoundException {
    final LayerOval oval = getOvalFromService(projectId, mapId, layerId, ovalId);

    serializer.checkETag(oldETag, oval);

    layerOvalService.remove(oval);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listOvals(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "layerId") Integer layerId
  ) {
    final Map<LayerOvalProperty, Object> properties = new HashMap<>();

    properties.put(LayerOvalProperty.PROJECT_ID, Collections.singletonList(projectId));
    properties.put(LayerOvalProperty.LAYER_ID, Collections.singletonList(layerId));
    properties.put(LayerOvalProperty.MAP_ID, Collections.singletonList(mapId));

    final Page<LayerOval> page = layerOvalService.getAll(properties, pageable);

    return serializer.prepareResponse(page);
  }

}