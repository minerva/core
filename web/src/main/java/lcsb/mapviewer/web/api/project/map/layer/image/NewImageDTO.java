package lcsb.mapviewer.web.api.project.map.layer.image;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.function.CheckedFunction;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.model.map.layout.graphics.LayerImage;
import lcsb.mapviewer.web.api.AbstractDTO;

import javax.validation.constraints.NotNull;

public class NewImageDTO extends AbstractDTO {

  @NotNull
  private Double x;

  @NotNull
  private Double y;

  @NotNull
  private Double width;

  @NotNull
  private Double height;

  @NotNull
  private Integer z;

  private Integer glyph;

  public void saveToLayer(final LayerImage image, final CheckedFunction<Integer, Glyph> getGlyphById)
      throws QueryException {
    image.setX(x);
    image.setY(y);
    image.setZ(z);
    image.setWidth(width);
    image.setHeight(height);
    if (glyph != null) {
      try {
        image.setGlyph(getGlyphById.apply(glyph));
      } catch (Exception e) {
        throw new QueryException("Invalid glyph id: " + glyph);
      }
    } else {
      image.setGlyph(null);
    }
  }

  public Double getX() {
    return x;
  }

  public void setX(final Double x) {
    this.x = x;
  }

  public Double getY() {
    return y;
  }

  public void setY(final Double y) {
    this.y = y;
  }

  public Double getWidth() {
    return width;
  }

  public void setWidth(final Double width) {
    this.width = width;
  }

  public Double getHeight() {
    return height;
  }

  public void setHeight(final Double height) {
    this.height = height;
  }

  public Integer getZ() {
    return z;
  }

  public void setZ(final Integer z) {
    this.z = z;
  }

  public Integer getGlyph() {
    return glyph;
  }

  public void setGlyph(final Integer glyph) {
    this.glyph = glyph;
  }
}
