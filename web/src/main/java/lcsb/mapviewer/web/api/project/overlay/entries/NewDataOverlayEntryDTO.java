package lcsb.mapviewer.web.api.project.overlay.entries;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.awt.Color;

public class NewDataOverlayEntryDTO {

  @NotNull
  private String name;

  private String modelName;

  private String elementId;

  private Boolean reverseReaction;

  private Double lineWidth;

  @Min(-1)
  @Max(1)
  private Double value;

  private Color color;

  private String description;


  public void saveToDataOverlayEntry(final DataOverlayEntry overlay, final boolean allowLayoutChanges) throws QueryException {
    overlay.setName(name);
    overlay.setModelName(modelName);
    overlay.setElementId(elementId);
    overlay.setReverseReaction(reverseReaction);
    overlay.setLineWidth(lineWidth);
    overlay.setValue(value);
    overlay.setColor(color);
    overlay.setDescription(description);
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getModelName() {
    return modelName;
  }

  public void setModelName(final String modelName) {
    this.modelName = modelName;
  }

  public String getElementId() {
    return elementId;
  }

  public void setElementId(final String elementId) {
    this.elementId = elementId;
  }

  public Boolean getReverseReaction() {
    return reverseReaction;
  }

  public void setReverseReaction(final Boolean reverseReaction) {
    this.reverseReaction = reverseReaction;
  }

  public Double getLineWidth() {
    return lineWidth;
  }

  public void setLineWidth(final Double lineWidth) {
    this.lineWidth = lineWidth;
  }

  public Double getValue() {
    return value;
  }

  public void setValue(final Double value) {
    this.value = value;
  }

  public Color getColor() {
    return color;
  }

  public void setColor(final Color color) {
    this.color = color;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }
}
