package lcsb.mapviewer.web.api.project.map.bioentity.reaction;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.graphics.ArrowTypeData;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.web.api.AbstractDTO;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.awt.Color;
import java.awt.geom.Line2D;
import java.util.List;

public class NewLineDTO extends AbstractDTO {

  @NotNull
  private ArrowTypeData startArrow;

  @NotNull
  private ArrowTypeData endArrow;

  @NotNull
  private List<Line2D> segments;

  @NotNull
  @Min(1)
  private Double width;

  @NotNull
  private Integer z;

  @NotNull
  private Color color = Color.BLACK;

  @NotNull
  private LineType lineType = LineType.SOLID;

  public void saveToLine(final PolylineData line, final boolean allowLayoutChanges) throws QueryException {
    if (!allowLayoutChanges) {
      checkEquality("startArrow", startArrow, line.getBeginAtd());
      checkEquality("endArrow", endArrow, line.getEndAtd());
      checkEquality("color", color, line.getColor());
      checkEquality("segments", segments, line.getLines());
      checkEquality("width", width, line.getWidth());
      checkEquality("lineType", lineType, line.getType());
      checkEquality("z", z, line.getZ());
    } else {
      line.setBeginAtd(startArrow);
      line.setEndAtd(endArrow);
      line.setBorderColor(color);
      line.removeLines();
      line.addLines(segments);
      line.setWidth(width);
      line.setType(lineType);
      line.setZ(z);
    }
  }

  public ArrowTypeData getStartArrow() {
    return startArrow;
  }

  public void setStartArrow(final ArrowTypeData startArrow) {
    this.startArrow = startArrow;
  }

  public ArrowTypeData getEndArrow() {
    return endArrow;
  }

  public void setEndArrow(final ArrowTypeData endArrow) {
    this.endArrow = endArrow;
  }

  public List<Line2D> getSegments() {
    return segments;
  }

  public void setSegments(final List<Line2D> segments) {
    this.segments = segments;
  }

  public Double getWidth() {
    return width;
  }

  public void setWidth(final Double width) {
    this.width = width;
  }

  public Color getColor() {
    return color;
  }

  public void setColor(final Color color) {
    this.color = color;
  }

  public LineType getLineType() {
    return lineType;
  }

  public void setLineType(final LineType lineType) {
    this.lineType = lineType;
  }

  public Integer getZ() {
    return z;
  }

  public void setZ(final Integer z) {
    this.z = z;
  }
}
