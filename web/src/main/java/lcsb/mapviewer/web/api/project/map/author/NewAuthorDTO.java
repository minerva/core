package lcsb.mapviewer.web.api.project.map.author;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.web.api.AbstractDTO;

public class NewAuthorDTO extends AbstractDTO {

  @Size(max = 255)
  private String firstName;

  @Size(max = 255)
  private String lastName;

  @Size(max = 255)
  @Email
  private String email;

  @Size(max = 255)
  private String organisation;

  public void saveToAuthor(final Author author) throws QueryException {
    author.setEmail(email);
    author.setFirstName(firstName);
    author.setLastName(lastName);
    author.setOrganisation(organisation);
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(final String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public String getOrganisation() {
    return organisation;
  }

  public void setOrganisation(final String organisation) {
    this.organisation = organisation;
  }

}
