package lcsb.mapviewer.web.api.project.map.bioentity.reaction.reactant;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.persist.dao.map.ReactionProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.map.bioentity.reaction.NewLineDTO;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/reactants/{reactantId}/line",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewReactantLineController {

  private final IProjectService projectService;
  private final IReactionService reactionService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewReactantLineController(
      final NewApiResponseSerializer serializer,
      final IReactionService reactionService,
      final IProjectService projectService) {
    this.serializer = serializer;
    this.projectService = projectService;
    this.reactionService = reactionService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> getLine(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @NotNull @PathVariable(value = "reactantId") Integer reactantId)
      throws QueryException, ObjectNotFoundException {
    final Reactant reactant = getReactantFromService(projectId, mapId, reactionId, reactantId);
    return serializer.prepareResponse(reactant.getLine());
  }

  private Reactant getReactantFromService(final String projectId, final Integer mapId, final Integer reactionId, final Integer reactantId)
      throws ObjectNotFoundException {
    final Map<ReactionProperty, Object> properties = new HashMap<>();
    properties.put(ReactionProperty.ID, Collections.singletonList(reactionId));
    properties.put(ReactionProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(ReactionProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<Reaction> reactions = reactionService.getAll(properties, Pageable.unpaged(), true);
    if (reactions.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Reaction does not exist");
    }
    final Reaction reaction = reactions.getContent().get(0);
    Reactant reactant = null;
    for (final Reactant md : reaction.getReactants()) {
      if (md.getId() == reactantId) {
        reactant = md;
      }
    }

    if (reactant == null) {
      throw new ObjectNotFoundException("Reactant does not exist");
    }
    return reactant;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/")
  public ResponseEntity<?> updateLine(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewLineDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @NotNull @PathVariable(value = "reactantId") Integer reactantId)
      throws QueryException, ObjectNotFoundException {
    final Reactant reactant = getReactantFromService(projectId, mapId, reactionId, reactantId);
    serializer.checkETag(oldETag, reactant.getLine());
    data.saveToLine(reactant.getLine(), projectService.getBackgrounds(projectId, false).isEmpty());
    reactionService.update(reactant.getReaction());

    return serializer.prepareResponse(getReactantFromService(projectId, mapId, reactionId, reactantId).getLine());
  }

}