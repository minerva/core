package lcsb.mapviewer.web.api.project.shape;

import java.util.ArrayList;
import java.util.List;

public class NewShapeDataDTO {
  private final String sboTerm;

  private final List<NewShapeDTO> shapes = new ArrayList<>();

  public NewShapeDataDTO(final String sboTerm, final List<NewShapeDTO> shapes) {
    this.sboTerm = sboTerm;
    this.shapes.addAll(shapes);
  }

  public String getSboTerm() {
    return sboTerm;
  }

  public List<NewShapeDTO> getShapes() {
    return shapes;
  }
}
