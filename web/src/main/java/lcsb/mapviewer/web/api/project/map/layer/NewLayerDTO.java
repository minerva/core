package lcsb.mapviewer.web.api.project.map.layer;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.web.api.AbstractDTO;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

public class NewLayerDTO extends AbstractDTO {

  @Size(max = 255)
  @NotNull
  private String name;

  private boolean visible;

  private boolean locked;

  @NotNull
  private Integer z;

  public void saveToLayer(final Layer layer) throws QueryException {
    if (layer.getLayerId() == null) {
      layer.setLayerId(UUID.randomUUID().toString());
    }

    layer.setVisible(visible);
    layer.setName(name);
    layer.setLocked(locked);
    layer.setZ(z);
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public boolean isVisible() {
    return visible;
  }

  public void setVisible(final boolean visible) {
    this.visible = visible;
  }

  public boolean isLocked() {
    return locked;
  }

  public void setLocked(final boolean locked) {
    this.locked = locked;
  }

  public Integer getZ() {
    return z;
  }

  public void setZ(final Integer z) {
    this.z = z;
  }
}
