package lcsb.mapviewer.web.api.plugin;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.plugin.Plugin;
import lcsb.mapviewer.web.api.AbstractDTO;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.function.Function;

public class NewPluginDTO extends AbstractDTO {

  @NotBlank
  @Size(max = 255)
  private String hash;

  @NotNull
  private String name;

  @NotNull
  private String version;

  private boolean isPublic;

  private Boolean isDefault;

  @NotNull
  private String url;

  public void saveToPlugin(final Plugin plugin, final boolean isAdmin, final Function<String, String> urlToHash) throws QueryException {
    plugin.setHash(hash);
    plugin.setPublic(isPublic);
    if (isDefault != null && isAdmin) {
      plugin.setDefault(isDefault);
    }
    plugin.setName(name);
    plugin.setVersion(version);
    if (!plugin.getUrls().contains(url)) {
      if (!url.startsWith("http://localhost")) {
        String md5;
        try {
          md5 = urlToHash.apply(url);
        } catch (Exception e) {
          throw new QueryException("Problem with fetching source file: " + url, e);
        }
        if (!Objects.equals(md5, hash)) {
          throw new QueryException(String.format("Invalid hash. Expected %s, but %s found", md5, hash));
        }
      }
      plugin.addUrl(url);
    }
  }

  public String getHash() {
    return hash;
  }

  public void setHash(final String hash) {
    this.hash = hash;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(final String version) {
    this.version = version;
  }

  public boolean isPublic() {
    return isPublic;
  }

  public void setPublic(final boolean isPublic) {
    this.isPublic = isPublic;
  }

  public Boolean getDefault() {
    return isDefault;
  }

  public void setDefault(final Boolean isDefault) {
    this.isDefault = isDefault;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(final String url) {
    this.url = url;
  }
}
