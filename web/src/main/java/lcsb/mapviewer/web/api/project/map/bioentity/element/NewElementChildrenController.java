package lcsb.mapviewer.web.api.project.map.bioentity.element;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/children"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewElementChildrenController {

  private final IElementService elementService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewElementChildrenController(
      final NewApiResponseSerializer serializer,
      final IElementService elementService) {
    this.serializer = serializer;
    this.elementService = elementService;
  }

  private Element getElementFromService(final String projectId, final Integer mapId, final Integer elementId) throws ObjectNotFoundException {
    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.ID, Collections.singletonList(elementId));
    properties.put(ElementProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(ElementProperty.PROJECT_ID, Collections.singletonList(projectId));
    final List<Element> elements = elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent();
    if (elements.size() == 0) {
      throw new ObjectNotFoundException("Element does not exist");
    }
    return elements.get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listChildren(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId) throws QueryException, ObjectNotFoundException {
    final Element element = getElementFromService(projectId, mapId, elementId);
    final List<Element> elements = new ArrayList<>();
    if (element instanceof Compartment) {
      final Map<ElementProperty, Object> properties = new HashMap<>();
      properties.put(ElementProperty.COMPARTMENT_ID, Collections.singletonList(element.getId()));
      elements.addAll(elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent());
    } else if (element instanceof Complex) {
      final Map<ElementProperty, Object> properties = new HashMap<>();
      properties.put(ElementProperty.COMPLEX_ID, Collections.singletonList(element.getId()));
      elements.addAll(elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent());
    }

    elements.sort(new Comparator<MinervaEntity>() {
      @Override
      public int compare(final MinervaEntity o1, final MinervaEntity o2) {
        return o1.getId() - o2.getId();
      }
    });

    return serializer.prepareResponse(serializer.createPage(elements, pageable));
  }

}