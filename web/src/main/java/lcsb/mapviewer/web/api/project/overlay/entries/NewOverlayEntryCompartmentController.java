package lcsb.mapviewer.web.api.project.overlay.entries;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.persist.dao.map.DataOverlayEntryProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IDataOverlayEntryService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}/compartments/"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewOverlayEntryCompartmentController {

  private final NewApiResponseSerializer serializer;
  private final IDataOverlayEntryService dataOverlayEntryService;

  @Autowired
  public NewOverlayEntryCompartmentController(
      final NewApiResponseSerializer serializer,
      final IDataOverlayEntryService dataOverlayEntryService) {
    this.serializer = serializer;
    this.dataOverlayEntryService = dataOverlayEntryService;
  }


  @PreAuthorize("(@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name) ")
  @PostMapping(value = "/")
  public ResponseEntity<?> addCompartment(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "overlayId") Integer overlayId,
      final @NotNull @PathVariable(value = "overlayEntryId") Integer overlayEntryId,
      final @Valid @RequestBody NewCompartmentDTO data)
      throws ObjectNotFoundException {

    final DataOverlayEntry overlayEntry = getDataOverlayEntryFromService(projectId, overlayId, overlayEntryId);
    overlayEntry.addCompartment(data.getCompartment());
    dataOverlayEntryService.update(overlayEntry);

    return serializer.prepareResponse(HttpStatus.CREATED);
  }

  @PreAuthorize("(@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name) ")
  @DeleteMapping(value = "/{compartment}")
  public void deleteCompartment(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "overlayId") Integer overlayId,
      final @NotNull @PathVariable(value = "overlayEntryId") Integer overlayEntryId,
      final @Valid @NotNull @PathVariable(value = "compartment") String compartment)
      throws QueryException, ObjectNotFoundException {
    final DataOverlayEntry overlayEntry = getDataOverlayEntryFromService(projectId, overlayId, overlayEntryId);
    if (!overlayEntry.getCompartments().contains(compartment)) {
      throw new ObjectNotFoundException("Compartment does not exist");
    }
    overlayEntry.getCompartments().remove(compartment);
    dataOverlayEntryService.update(overlayEntry);
  }

  @PreAuthorize("(@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name) ")
  @GetMapping(value = "/")
  public ResponseEntity<?> listCompartments(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "overlayId") Integer overlayId,
      final @NotNull @PathVariable(value = "overlayEntryId") Integer overlayEntryId)
      throws ObjectNotFoundException {
    final DataOverlayEntry overlayEntry = getDataOverlayEntryFromService(projectId, overlayId, overlayEntryId);

    final List<String> compartments = new ArrayList<>(overlayEntry.getCompartments());
    Collections.sort(compartments);

    return ResponseEntity.ok(serializer.createRawPage(compartments, pageable));
  }

  private DataOverlayEntry getDataOverlayEntryFromService(final String projectId,
                                                          final Integer overlayId,
                                                          final Integer overlayEntryId
  ) throws ObjectNotFoundException {
    final Map<DataOverlayEntryProperty, Object> filter = new HashMap<>();
    filter.put(DataOverlayEntryProperty.PROJECT_ID, projectId);
    filter.put(DataOverlayEntryProperty.DATA_OVERLAY_ID, overlayId);
    filter.put(DataOverlayEntryProperty.ID, overlayEntryId);
    final Page<DataOverlayEntry> overlayEntry = dataOverlayEntryService.getAll(filter, Pageable.unpaged(), true);
    if (overlayEntry.getContent().isEmpty()) {
      throw new ObjectNotFoundException("Entry does not exist");
    }
    return overlayEntry.getContent().get(0);
  }

}