package lcsb.mapviewer.web.api.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import lcsb.mapviewer.api.CustomPage;

import java.io.IOException;

public class NewCustomPageSerializer extends JsonSerializer<CustomPage> {

  @Override
  public void serialize(final CustomPage page, final JsonGenerator jsonGenerator, final SerializerProvider serializerProvider) throws IOException {
    jsonGenerator.writeStartObject();
    ((ObjectMapper) jsonGenerator.getCodec()).setConfig(serializerProvider.getConfig());
    jsonGenerator.writeObjectField("content", page.getContent());
    jsonGenerator.writeNumberField("totalPages", page.getTotalPages());
    jsonGenerator.writeNumberField("totalElements", page.getTotalElements());
    jsonGenerator.writeNumberField("numberOfElements", page.getNumberOfElements());
    jsonGenerator.writeNumberField("filteredSize", page.getFiltered());
    jsonGenerator.writeNumberField("size", page.getSize());
    jsonGenerator.writeNumberField("number", page.getNumber());
    jsonGenerator.writeEndObject();
  }

}
