package lcsb.mapviewer.web.api.user;

import org.apache.logging.log4j.core.config.plugins.validation.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class ResetPasswordDTO {
  @NotNull
  @NotBlank
  private String token;

  @NotNull
  private String password;

  public String getToken() {
    return token;
  }

  public void setToken(final String token) {
    this.token = token;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(final String password) {
    this.password = password;
  }
}
