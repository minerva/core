package lcsb.mapviewer.web.api.project.overlay;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.overlay.DataOverlayGroup;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.map.DataOverlayGroupProperty;
import lcsb.mapviewer.persist.dao.map.DataOverlayProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IDataOverlayGroupService;
import lcsb.mapviewer.services.interfaces.IDataOverlayService;
import lcsb.mapviewer.services.interfaces.IFileService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/overlays"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewOverlayController extends BaseController {

  private final IModelService modelService;
  private final IDataOverlayService dataOverlayService;
  private final IDataOverlayGroupService dataOverlayGroupService;
  private final NewApiResponseSerializer serializer;
  private final IProjectService projectService;
  private final IUserService userService;
  private final IFileService fileService;

  public NewOverlayController(final IProjectService projectService,
                              final IModelService modelService,
                              final IDataOverlayService dataOverlayService,
                              final NewApiResponseSerializer serializer,
                              final IUserService userService,
                              final IFileService fileService,
                              final IDataOverlayGroupService dataOverlayGroupService) {
    this.dataOverlayService = dataOverlayService;
    this.serializer = serializer;
    this.modelService = modelService;
    this.projectService = projectService;
    this.userService = userService;
    this.fileService = fileService;
    this.dataOverlayGroupService = dataOverlayGroupService;
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)"
      + " or hasAuthority('READ_PROJECT:' + #projectId) and "
      + "   (@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name "
      + "    or @dataOverlayService.getDataOverlayById(#overlayId)?.public == true)")
  @GetMapping(value = "/{overlayId}/models/{modelId}/bioEntities/")
  public ResponseEntity<?> getOverlayElements(
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "modelId") String modelId,
      final @PathVariable(value = "overlayId") Integer overlayId,
      final @RequestParam(value = "includeIndirect", defaultValue = "false") boolean includeIndirect)
      throws QueryException, IOException, ObjectNotFoundException {
    if (overlayId == null) {
      throw new QueryException("Invalid overlayId");
    }
    dataOverlayService.getDataOverlayById(projectId, overlayId);

    final List<ModelData> models = modelService.getModelsByMapId(projectId, modelId);

    final List<Pair<? extends BioEntity, DataOverlayEntry>> result = new ArrayList<>();
    for (final ModelData model : models) {
      result.addAll(dataOverlayService.getBioEntitiesForDataOverlay(projectId, model.getId(), overlayId, true, includeIndirect));
    }

    return serializer.prepareResponse(result);
  }


  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)"
      + " or hasAuthority('READ_PROJECT:' + #projectId) and "
      + "   (@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name "
      + "    or @dataOverlayService.getDataOverlayById(#overlayId)?.public == true)")
  @GetMapping(value = "/{overlayId}/")
  public ResponseEntity<?> getOverlay(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "overlayId") Integer overlayId)
      throws ObjectNotFoundException {
    final DataOverlay overlay = dataOverlayService.getDataOverlayById(projectId, overlayId);
    return serializer.prepareResponse(overlay);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('READ_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addOverlay(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @RequestBody NewDataOverlayDTO data,
      final Authentication authentication)
      throws QueryException, ObjectNotFoundException {

    final User user = userService.getUserByLogin(authentication.getName());
    final Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given projectId does not exist");
    }

    final UploadedFileEntry entry = createEmptyFile(user);

    final DataOverlay overlay = new DataOverlay();
    data.saveToDataOverlay(overlay, getGroupIdToGroup(projectId, user.getLogin()));
    overlay.setProject(project);
    overlay.setCreator(user);
    overlay.setInputData(entry);
    dataOverlayService.add(overlay);

    return serializer.prepareResponse(overlay, HttpStatus.CREATED);
  }

  private Function<Integer, DataOverlayGroup> getGroupIdToGroup(final String projectId, final String login) {
    return groupId -> {
      Map<DataOverlayGroupProperty, Object> properties = new HashMap<>();
      properties.put(DataOverlayGroupProperty.PROJECT_ID, projectId);
      properties.put(DataOverlayGroupProperty.USER_LOGIN, login);
      properties.put(DataOverlayGroupProperty.ID, groupId);
      Page<DataOverlayGroup> page = dataOverlayGroupService.getAll(properties, Pageable.unpaged());
      if (page.getSize() > 0) {
        return page.getContent().get(0);
      }
      return null;
    };
  }

  private UploadedFileEntry createEmptyFile(final User user) {
    final UploadedFileEntry entry = new UploadedFileEntry();
    entry.setOriginalFileName("empty.txt");
    entry.setFileContent(new byte[]{});
    entry.setLength(0);
    entry.setOwner(user);
    fileService.add(entry);
    return entry;
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)"
      + " or hasAuthority('READ_PROJECT:' + #projectId) and "
      + "   (@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name "
      + "    or @dataOverlayService.getDataOverlayById(#overlayId)?.public == true)")
  @PutMapping(value = "/{overlayId}")
  public ResponseEntity<?> updateOverlay(
      final Authentication authentication,
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewDataOverlayDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "overlayId") Integer overlayId) throws QueryException, ObjectNotFoundException {
    final DataOverlay overlay = dataOverlayService.getDataOverlayById(projectId, overlayId);
    if (overlay == null) {
      throw new ObjectNotFoundException("Data overlay with given id does not exist");
    }
    serializer.checkETag(oldETag, overlay);

    data.saveToDataOverlay(overlay, getGroupIdToGroup(projectId, authentication.getName()));
    dataOverlayService.updateDataOverlay(overlay);

    return getOverlay(projectId, overlayId);
  }

  @PreAuthorize("hasAuthority('IS_ADMIN')"
      + " or hasAuthority('IS_CURATOR') and hasAuthority('READ_PROJECT:' + #projectId)"
      + " or hasAuthority('READ_PROJECT:' + #projectId) and "
      + "   (@dataOverlayService.getOverlayCreator(#overlayId)?.login == authentication.name "
      + "    or @dataOverlayService.getDataOverlayById(#overlayId)?.public == true)")
  @DeleteMapping(value = "/{overlayId}")
  public void deleteOverlay(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "overlayId") Integer overlayId)
      throws QueryException, ObjectNotFoundException {

    final DataOverlay overlay = dataOverlayService.getDataOverlayById(projectId, overlayId);
    if (overlay == null) {
      throw new ObjectNotFoundException("Data overlay with given id does not exist");
    }
    serializer.checkETag(oldETag, overlay);
    dataOverlayService.removeDataOverlay(overlay);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listOverlays(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @RequestParam(value = "isPublic", required = false) Boolean isPublic,
      final @RequestParam(value = "creator", required = false) String creator,
      final Authentication authentication) {
    final Map<DataOverlayProperty, Object> overlayFilter = new HashMap<>();
    overlayFilter.put(DataOverlayProperty.PROJECT_ID, projectId);
    final boolean isAdmin = authentication.getAuthorities()
        .contains(new SimpleGrantedAuthority(PrivilegeType.IS_ADMIN.toString()));

    if (!isAdmin && (isPublic == null || !isPublic)) {
      overlayFilter.put(DataOverlayProperty.USER_LOGIN, authentication.getName());
    }
    if (creator != null) {
      overlayFilter.put(DataOverlayProperty.USER_LOGIN, creator);
    }
    if (isPublic != null) {
      overlayFilter.put(DataOverlayProperty.IS_PUBLIC, isPublic);
    }

    final Page<DataOverlay> overlays = dataOverlayService.getAll(overlayFilter, pageable);
    return serializer.prepareResponse(overlays);
  }

}