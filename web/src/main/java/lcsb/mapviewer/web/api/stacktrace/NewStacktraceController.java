package lcsb.mapviewer.web.api.stacktrace;

import lcsb.mapviewer.model.Stacktrace;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IStacktraceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/stacktraces",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewStacktraceController {

  private final IStacktraceService stacktraceService;

  @Autowired
  public NewStacktraceController(final IStacktraceService stacktraceService) {
    this.stacktraceService = stacktraceService;
  }

  @GetMapping(value = "/{stacktraceId}")
  public Stacktrace getProject(final @PathVariable(value = "stacktraceId") String stacktraceId)
      throws ObjectNotFoundException {
    Stacktrace st = stacktraceService.getById(stacktraceId);
    if (st == null) {
      throw new ObjectNotFoundException("Object does not exist");
    }
    return st;
  }

}