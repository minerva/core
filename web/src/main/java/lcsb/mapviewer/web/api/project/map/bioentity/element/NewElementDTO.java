package lcsb.mapviewer.web.api.project.map.bioentity.element;

import java.awt.Color;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.sbo.SBOTermSpeciesType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.web.api.AbstractDTO;

public class NewElementDTO extends AbstractDTO {

  @NotBlank
  private String sboTerm;

  @NotBlank
  private String elementId;

  @NotNull
  private Double x;

  @NotNull
  private Double y;

  @NotNull
  private Integer z;

  @NotNull
  private Double width;

  @NotNull
  private Double height;

  @NotNull
  private Double fontSize;

  @NotNull
  private Color fontColor;

  @NotNull
  private Color fillColor;

  @NotNull
  private Color borderColor;

  @NotNull
  private String visibilityLevel;

  @NotNull
  private String transparencyLevel;

  @NotNull
  private String notes;

  @Size(max = 255)
  private String symbol;

  @Size(max = 255)
  private String fullName;

  @Size(max = 255)
  private String abbreviation;

  @Size(max = 255)
  private String formula;

  @NotNull
  @Size(max = 255)
  private String name;

  @NotNull
  private Double nameX;

  @NotNull
  private Double nameY;

  @NotNull
  private Double nameWidth;

  @NotNull
  private Double nameHeight;

  @NotNull
  private VerticalAlign nameVerticalAlign;

  @NotNull
  private HorizontalAlign nameHorizontalAlign;

  // ------------------------------------------
  // Compartment specific
  private Double thickness;

  private Double outerWidth;

  private Double innerWidth;

  // ------------------------------------------
  // Species
  private Boolean activity;

  private Double lineWidth;

  private Double initialAmount;

  private Integer charge;

  private Double initialConcentration;

  private Boolean onlySubstanceUnits;

  private Integer homodimer;

  private Boolean hypothetical;

  private Boolean boundaryCondition;

  private Boolean constant;
  // --------------------------

  public void saveToElement(
      final Element element,
      final boolean allowLayoutChanges) throws QueryException {
    checkEquality("SBOterm", getClazz(), element.getClass());
    if (element.getElementId() == null || element.getElementId().equals(elementId)) {
      element.setElementId(elementId);
    } else {
      checkEquality("elementId", elementId, element.getElementId());
    }

    if (!allowLayoutChanges) {
      checkEquality("width", width, element.getWidth());
      checkEquality("height", height, element.getHeight());
      checkEquality("x", x, element.getX());
      checkEquality("y", y, element.getY());
      checkEquality("z", z, element.getZ());
      checkEquality("fontSize", fontSize, element.getFontSize());
      checkEquality("fontColor", fontColor, element.getFontColor());
      checkEquality("fillColor", fillColor, element.getFillColor());
      checkEquality("borderColor", borderColor, element.getBorderColor());
      checkEquality("visibilityLevel", visibilityLevel, element.getVisibilityLevel());
      checkEquality("transparencyLevel", transparencyLevel, element.getTransparencyLevel());
      checkEquality("name", name, element.getName());
      checkEquality("nameX", nameX, element.getNameX());
      checkEquality("nameY", nameY, element.getNameY());
      checkEquality("nameWidth", nameWidth, element.getNameWidth());
      checkEquality("nameHeight", nameHeight, element.getNameHeight());
      checkEquality("nameVerticalAlign", nameVerticalAlign, element.getNameVerticalAlign());
      checkEquality("nameHorizontalAlign", nameHorizontalAlign, element.getNameHorizontalAlign());

    } else {
      element.setWidth(width);
      element.setHeight(height);
      element.setX(x);
      element.setY(y);
      element.setZ(z);
      element.setFontSize(fontSize);
      element.setFontColor(fontColor);
      element.setFillColor(fillColor);
      element.setBorderColor(borderColor);
      element.setVisibilityLevel(visibilityLevel);
      element.setTransparencyLevel(transparencyLevel);
      element.setName(name);
      element.setNameX(nameX);
      element.setNameY(nameY);
      element.setNameWidth(nameWidth);
      element.setNameHeight(nameHeight);
      element.setNameVerticalAlign(nameVerticalAlign);
      element.setNameHorizontalAlign(nameHorizontalAlign);
    }

    element.setNotes(notes);
    element.setSymbol(symbol);
    element.setFullName(fullName);
    element.setAbbreviation(abbreviation);
    element.setFormula(formula);

    if (element instanceof Compartment) {
      Compartment compartment = (Compartment) element;
      if (!allowLayoutChanges) {
        checkEquality("thickness", thickness, compartment.getThickness());
        checkEquality("outerWidth", outerWidth, compartment.getOuterWidth());
        checkEquality("innerWidth", innerWidth, compartment.getInnerWidth());
      } else {
        if (thickness == null) {
          throw new QueryException("thickness cannot be null");
        }
        compartment.setThickness(thickness);
        if (outerWidth == null) {
          throw new QueryException("outerWidth cannot be null");
        }
        compartment.setOuterWidth(outerWidth);
        if (innerWidth == null) {
          throw new QueryException("innerWidth cannot be null");
        }
        compartment.setInnerWidth(innerWidth);
      }
    }

    if (element instanceof Species) {
      Species species = (Species) element;
      if (!allowLayoutChanges) {
        checkEquality("lineWidth", lineWidth, species.getLineWidth());
        checkEquality("homodimer", homodimer, species.getHomodimer());
        checkEquality("hypothetical", hypothetical, species.getHypothetical());
        checkEquality("activity", activity, species.getActivity());

      } else {
        if (lineWidth == null) {
          throw new QueryException("lineWidth cannot be null");
        }
        species.setLineWidth(lineWidth);
        if (homodimer == null) {
          throw new QueryException("homodimer cannot be null");
        }
        species.setHomodimer(homodimer);
        species.setHypothetical(hypothetical);
        species.setActivity(activity);
      }
      species.setInitialAmount(initialAmount);
      species.setCharge(charge);
      species.setInitialConcentration(initialConcentration);
      species.setOnlySubstanceUnits(onlySubstanceUnits);
      species.setBoundaryCondition(boundaryCondition);
      species.setConstant(constant);
    }
  }

  public Class<? extends Element> getClazz() {
    if (sboTerm.equalsIgnoreCase("SBO:0000290")) {
      return SquareCompartment.class;
    }
    return SBOTermSpeciesType.createElementForSBOTerm(sboTerm, "s1", null).getClass();
  }

  public String getSboTerm() {
    return sboTerm;
  }

  public void setSboTerm(final String sboTerm) {
    this.sboTerm = sboTerm;
  }

  public String getElementId() {
    return elementId;
  }

  public void setElementId(final String elementId) {
    this.elementId = elementId;
  }

  public Double getX() {
    return x;
  }

  public void setX(final Double x) {
    this.x = x;
  }

  public Double getY() {
    return y;
  }

  public void setY(final Double y) {
    this.y = y;
  }

  public Integer getZ() {
    return z;
  }

  public void setZ(final Integer z) {
    this.z = z;
  }

  public Double getWidth() {
    return width;
  }

  public void setWidth(final Double width) {
    this.width = width;
  }

  public Double getHeight() {
    return height;
  }

  public void setHeight(final Double height) {
    this.height = height;
  }

  public Double getFontSize() {
    return fontSize;
  }

  public void setFontSize(final Double fontSize) {
    this.fontSize = fontSize;
  }

  public Color getFontColor() {
    return fontColor;
  }

  public void setFontColor(final Color fontColor) {
    this.fontColor = fontColor;
  }

  public Color getFillColor() {
    return fillColor;
  }

  public void setFillColor(final Color fillColor) {
    this.fillColor = fillColor;
  }

  public Color getBorderColor() {
    return borderColor;
  }

  public void setBorderColor(final Color borderColor) {
    this.borderColor = borderColor;
  }

  public String getVisibilityLevel() {
    return visibilityLevel;
  }

  public void setVisibilityLevel(final String visibilityLevel) {
    this.visibilityLevel = visibilityLevel;
  }

  public String getTransparencyLevel() {
    return transparencyLevel;
  }

  public void setTransparencyLevel(final String transparencyLevel) {
    this.transparencyLevel = transparencyLevel;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(final String notes) {
    this.notes = notes;
  }

  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(final String symbol) {
    this.symbol = symbol;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(final String fullName) {
    this.fullName = fullName;
  }

  public String getAbbreviation() {
    return abbreviation;
  }

  public void setAbbreviation(final String abbreviation) {
    this.abbreviation = abbreviation;
  }

  public String getFormula() {
    return formula;
  }

  public void setFormula(final String formula) {
    this.formula = formula;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public Double getNameX() {
    return nameX;
  }

  public void setNameX(final Double nameX) {
    this.nameX = nameX;
  }

  public Double getNameY() {
    return nameY;
  }

  public void setNameY(final Double nameY) {
    this.nameY = nameY;
  }

  public Double getNameWidth() {
    return nameWidth;
  }

  public void setNameWidth(final Double nameWidth) {
    this.nameWidth = nameWidth;
  }

  public Double getNameHeight() {
    return nameHeight;
  }

  public void setNameHeight(final Double nameHeight) {
    this.nameHeight = nameHeight;
  }

  public VerticalAlign getNameVerticalAlign() {
    return nameVerticalAlign;
  }

  public void setNameVerticalAlign(final VerticalAlign nameVerticalAlign) {
    this.nameVerticalAlign = nameVerticalAlign;
  }

  public HorizontalAlign getNameHorizontalAlign() {
    return nameHorizontalAlign;
  }

  public void setNameHorizontalAlign(final HorizontalAlign nameHorizontalAlign) {
    this.nameHorizontalAlign = nameHorizontalAlign;
  }

  public Double getThickness() {
    return thickness;
  }

  public void setThickness(final Double thickness) {
    this.thickness = thickness;
  }

  public Double getOuterWidth() {
    return outerWidth;
  }

  public void setOuterWidth(final Double outerWidth) {
    this.outerWidth = outerWidth;
  }

  public Double getInnerWidth() {
    return innerWidth;
  }

  public void setInnerWidth(final Double innerWidth) {
    this.innerWidth = innerWidth;
  }

  public Boolean getActivity() {
    return activity;
  }

  public void setActivity(final Boolean activity) {
    this.activity = activity;
  }

  public Double getLineWidth() {
    return lineWidth;
  }

  public void setLineWidth(final Double lineWidth) {
    this.lineWidth = lineWidth;
  }

  public Double getInitialAmount() {
    return initialAmount;
  }

  public void setInitialAmount(final Double initialAmount) {
    this.initialAmount = initialAmount;
  }

  public Integer getCharge() {
    return charge;
  }

  public void setCharge(final Integer charge) {
    this.charge = charge;
  }

  public Double getInitialConcentration() {
    return initialConcentration;
  }

  public void setInitialConcentration(final Double initialConcentration) {
    this.initialConcentration = initialConcentration;
  }

  public Boolean getOnlySubstanceUnits() {
    return onlySubstanceUnits;
  }

  public void setOnlySubstanceUnits(final Boolean onlySubstanceUnits) {
    this.onlySubstanceUnits = onlySubstanceUnits;
  }

  public Integer getHomodimer() {
    return homodimer;
  }

  public void setHomodimer(final Integer homodimer) {
    this.homodimer = homodimer;
  }

  public Boolean getHypothetical() {
    return hypothetical;
  }

  public void setHypothetical(final Boolean hypothetical) {
    this.hypothetical = hypothetical;
  }

  public Boolean getBoundaryCondition() {
    return boundaryCondition;
  }

  public void setBoundaryCondition(final Boolean boundaryCondition) {
    this.boundaryCondition = boundaryCondition;
  }

  public Boolean getConstant() {
    return constant;
  }

  public void setConstant(final Boolean constant) {
    this.constant = constant;
  }

}
