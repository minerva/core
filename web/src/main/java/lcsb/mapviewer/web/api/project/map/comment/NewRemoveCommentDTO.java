package lcsb.mapviewer.web.api.project.map.comment;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.web.api.AbstractDTO;

import javax.validation.constraints.Size;

public class NewRemoveCommentDTO extends AbstractDTO {

  @Size(max = 255)
  private String reason;

  public void saveToComment(final Comment comment) throws QueryException {
    comment.setRemoveReason(reason);
  }

  public String getReason() {
    return reason;
  }

  public void setReason(final String reason) {
    this.reason = reason;
  }
}
