package lcsb.mapviewer.web.api;

import lcsb.mapviewer.api.LayoutChangeNotSupportedException;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.comparator.BooleanComparator;
import lcsb.mapviewer.common.comparator.ColorComparator;
import lcsb.mapviewer.common.comparator.DoubleComparator;
import lcsb.mapviewer.common.comparator.EnumComparator;
import lcsb.mapviewer.common.comparator.IntegerComparator;
import lcsb.mapviewer.common.comparator.LineComparator;
import lcsb.mapviewer.common.comparator.ListComparator;
import lcsb.mapviewer.common.comparator.PointComparator;
import lcsb.mapviewer.common.comparator.StringComparator;
import lcsb.mapviewer.model.graphics.ArrowTypeData;
import lcsb.mapviewer.model.graphics.ArrowTypeDataComparator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.Color;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.List;
import java.util.Objects;

public class AbstractDTO {

  protected final Logger logger = LogManager.getLogger();

  private static final DoubleComparator doubleComparator = new DoubleComparator();
  private static final IntegerComparator integerComparator = new IntegerComparator();
  protected static final StringComparator stringComparator = new StringComparator();
  private static final PointComparator pointComparator = new PointComparator();
  private static final ColorComparator colorComparator = new ColorComparator();
  private static final BooleanComparator booleanComparator = new BooleanComparator();
  private static final ArrowTypeDataComparator atdComparator = new ArrowTypeDataComparator();
  private static final ListComparator<Line2D> linesComparator = new ListComparator<>(new LineComparator());
  @SuppressWarnings("rawtypes")
  private static final EnumComparator enumComparator = new EnumComparator<>();

  protected void checkEquality(final String fieldName, final Double newValue, final Double originalValue) throws LayoutChangeNotSupportedException {
    if (doubleComparator.compare(newValue, originalValue) != 0) {
      throw new LayoutChangeNotSupportedException(fieldName, newValue, originalValue);
    }
  }

  protected void checkEquality(final String fieldName, final Integer newValue, final Integer originalValue) throws LayoutChangeNotSupportedException {
    if (integerComparator.compare(newValue, originalValue) != 0) {
      throw new LayoutChangeNotSupportedException(fieldName, newValue, originalValue);
    }
  }

  protected void checkEquality(final String fieldName, final Color newValue, final Color originalValue) throws LayoutChangeNotSupportedException {
    if (colorComparator.compare(newValue, originalValue) != 0) {
      throw new LayoutChangeNotSupportedException(fieldName, newValue, originalValue);
    }
  }

  protected void checkEquality(final String fieldName, final String newValue, final String originalValue) throws LayoutChangeNotSupportedException {
    if (stringComparator.compare(newValue, originalValue) != 0) {
      throw new LayoutChangeNotSupportedException(fieldName, newValue, originalValue);
    }
  }

  protected void checkEquality(final String fieldName, final Point2D newValue, final Point2D originalValue) throws LayoutChangeNotSupportedException {
    if (pointComparator.compare(newValue, originalValue) != 0) {
      throw new LayoutChangeNotSupportedException(fieldName, newValue, originalValue);
    }
  }

  protected void checkEquality(final String fieldName, final Boolean newValue, final Boolean originalValue) throws LayoutChangeNotSupportedException {
    if (booleanComparator.compare(newValue, originalValue) != 0) {
      throw new LayoutChangeNotSupportedException(fieldName, newValue, originalValue);
    }
  }

  protected void checkEquality(final String fieldName, final Class<?> newValue, final Class<?> originalValue)
      throws QueryException {
    if (!Objects.equals(newValue, originalValue)) {
      throw new QueryException("Cannot change: " + fieldName);
    }
  }

  @SuppressWarnings("unchecked")
  protected void checkEquality(final String fieldName, final Enum<?> newValue, final Enum<?> originalValue) throws LayoutChangeNotSupportedException {
    if (enumComparator.compare(newValue, originalValue) != 0) {
      throw new LayoutChangeNotSupportedException(fieldName, newValue, originalValue);
    }
  }

  protected void checkEquality(final String fieldName, final ArrowTypeData newValue, final ArrowTypeData oldValue)
      throws LayoutChangeNotSupportedException {
    if (atdComparator.compare(newValue, oldValue) != 0) {
      throw new LayoutChangeNotSupportedException(fieldName, newValue, oldValue);
    }
  }

  protected void checkEquality(final String fieldName, final List<Line2D> newValue, final List<Line2D> oldValue)
      throws LayoutChangeNotSupportedException {
    if (linesComparator.compare(newValue, oldValue) != 0) {
      throw new LayoutChangeNotSupportedException(fieldName, newValue, oldValue);
    }
  }

  protected void checkEqualityIgnoreCase(final String fieldName, final String newValue, final String originalValue)
      throws LayoutChangeNotSupportedException {
    if (stringComparator.compareIgnoreCase(newValue, originalValue) != 0) {
      throw new LayoutChangeNotSupportedException(fieldName, newValue, originalValue);
    }
  }

}
