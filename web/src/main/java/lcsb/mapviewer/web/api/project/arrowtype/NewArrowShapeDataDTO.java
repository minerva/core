package lcsb.mapviewer.web.api.project.arrowtype;

import lcsb.mapviewer.web.api.project.shape.NewShapeDTO;

import java.util.ArrayList;
import java.util.List;

public class NewArrowShapeDataDTO {
  private final String arrowType;

  private final List<NewShapeDTO> shapes = new ArrayList<>();

  public NewArrowShapeDataDTO(final String arrowType, final List<NewShapeDTO> shapes) {
    this.arrowType = arrowType;
    this.shapes.addAll(shapes);
  }

  public String getArrowType() {
    return arrowType;
  }

  public List<NewShapeDTO> getShapes() {
    return shapes;
  }
}
