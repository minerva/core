package lcsb.mapviewer.web.api.project.map.bioentity.element.uniprot;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.species.field.UniprotRecord;
import org.hibernate.validator.constraints.NotBlank;

public class NewUniprotRecordDTO extends lcsb.mapviewer.web.api.AbstractDTO {

  @NotBlank
  private String uniprotId;

  public void saveToUniprotRecord(
      final UniprotRecord uniprotRecord,
      final boolean allowLayoutChanges) throws QueryException {

    uniprotRecord.setUniprotId(getUniprotId());
  }

  public String getUniprotId() {
    return uniprotId;
  }

  public void setUniprotId(final String uniprotId) {
    this.uniprotId = uniprotId;
  }
}

