package lcsb.mapviewer.web.api.project.map.unit;

import javax.validation.constraints.NotNull;

import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitTypeFactor;
import lcsb.mapviewer.web.api.AbstractDTO;

public class NewUnitTypeFactorDTO extends AbstractDTO {

  @NotNull
  private SbmlUnitType unitType;

  private int exponent = 1;

  private int scale = 0;

  private double multiplier = 1.0;

  public void saveToSbmlTypeFactor(final SbmlUnitTypeFactor factor) {
    factor.setExponent(exponent);
    factor.setMultiplier(multiplier);
    factor.setScale(scale);
    factor.setUnitType(unitType);
  }

  public SbmlUnitType getUnitType() {
    return unitType;
  }

  public void setUnitType(final SbmlUnitType unitType) {
    this.unitType = unitType;
  }

  public int getExponent() {
    return exponent;
  }

  public void setExponent(final int exponent) {
    this.exponent = exponent;
  }

  public int getScale() {
    return scale;
  }

  public void setScale(final int scale) {
    this.scale = scale;
  }

  public double getMultiplier() {
    return multiplier;
  }

  public void setMultiplier(final double multiplier) {
    this.multiplier = multiplier;
  }

}
