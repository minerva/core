package lcsb.mapviewer.web.api.project.shape;

import lcsb.mapviewer.converter.model.sbml.extension.render.CustomEllipse;
import org.sbml.jsbml.ext.render.Ellipse;

public class NewEllipseShapeDTO extends NewShapeDTO {
  private final RelAbsPointDTO center;
  private final RelAbsRadiusDTO radius;

  private final boolean fill;

  public NewEllipseShapeDTO(final Ellipse ellipse) {
    super("ELLIPSE");
    Double centerHeightRelative = null;
    Double centerWidthRelative = null;

    Double radiusHeightRelative = null;
    Double radiusWidthRelative = null;

    if (ellipse instanceof CustomEllipse) {
      centerHeightRelative = ((CustomEllipse) ellipse).getCenterHeightRelative();
      centerWidthRelative = ((CustomEllipse) ellipse).getCenterWidthRelative();
      radiusHeightRelative = ((CustomEllipse) ellipse).getRadiusHeightRelative();
      radiusWidthRelative = ((CustomEllipse) ellipse).getRadiusWidthRelative();
      fill = ((CustomEllipse) ellipse).isFill();
    } else {
      fill = true;
    }
    center = new RelAbsPointDTO(ellipse.getCx(), ellipse.getCy(), centerHeightRelative, centerWidthRelative);
    radius = new RelAbsRadiusDTO(ellipse.getRx(), ellipse.getRy(), radiusHeightRelative, radiusWidthRelative);
  }

  public RelAbsPointDTO getCenter() {
    return center;
  }

  public RelAbsRadiusDTO getRadius() {
    return radius;
  }

  public boolean isFill() {
    return fill;
  }
}
