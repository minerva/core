package lcsb.mapviewer.web.api.project.overlaygroup;

import lcsb.mapviewer.model.overlay.DataOverlayGroup;
import lcsb.mapviewer.web.api.AbstractDTO;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class NewDataOverlayGroupDTO extends AbstractDTO {

  @NotNull
  @NotBlank
  private String name;

  @NotNull
  @Min(0)
  private Integer order;

  public void saveToDataOverlayGroup(final DataOverlayGroup group) {
    group.setName(name);
    group.setOrderIndex(order);
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public Integer getOrder() {
    return order;
  }

  public void setOrder(final Integer order) {
    this.order = order;
  }

}
