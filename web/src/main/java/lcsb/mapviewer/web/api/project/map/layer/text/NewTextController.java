package lcsb.mapviewer.web.api.project.map.layer.text;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.persist.dao.graphics.LayerProperty;
import lcsb.mapviewer.persist.dao.graphics.LayerTextProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.ILayerService;
import lcsb.mapviewer.services.interfaces.ILayerTextService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/texts",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewTextController {

  private final ILayerService layerService;
  private final NewApiResponseSerializer serializer;
  private final ILayerTextService layerTextService;

  @Autowired
  public NewTextController(
      final NewApiResponseSerializer serializer,
      final ILayerService layerService,
      final ILayerTextService layerTextService) {
    this.serializer = serializer;
    this.layerService = layerService;
    this.layerTextService = layerTextService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{textId}")
  public ResponseEntity<?> getText(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "layerId") String stringLayerId,
      final @NotNull @PathVariable(value = "textId") Integer textId
  )
      throws ObjectNotFoundException {
    Integer layerId = stringLayerId.equals("*") ? null : Integer.parseInt(stringLayerId);
    final LayerText text = getTextFromService(projectId, mapId, layerId, textId);
    return serializer.prepareResponse(text);
  }

  private LayerText getTextFromService(
      final String projectId,
      final Integer mapId,
      final Integer layerId,
      final Integer textId
  ) throws ObjectNotFoundException {
    final Map<LayerTextProperty, Object> properties = new HashMap<>();
    properties.put(LayerTextProperty.ID, Collections.singletonList(textId));
    if (layerId != null) {
      properties.put(LayerTextProperty.LAYER_ID, Collections.singletonList(layerId));
    }
    properties.put(LayerTextProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(LayerTextProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<LayerText> elements = layerTextService.getAll(properties, Pageable.unpaged());
    if (elements.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Text does not exist");
    }
    return elements.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addText(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "layerId") Integer layerId,
      final @Valid @RequestBody NewTextDTO data)
      throws QueryException, ObjectNotFoundException {

    final Layer layer = getLayerFromService(projectId, mapId, layerId);

    final LayerText text = new LayerText();

    data.saveToText(text);
    text.setLayer(layer);

    layerTextService.add(text);

    return serializer.prepareResponse(getTextFromService(projectId, mapId, layer.getId(), text.getId()), HttpStatus.CREATED);
  }

  private Layer getLayerFromService(final String projectId, final Integer mapId, final Integer elementId) throws ObjectNotFoundException {
    final Map<LayerProperty, Object> properties = new HashMap<>();
    properties.put(LayerProperty.ID, Collections.singletonList(elementId));
    properties.put(LayerProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(LayerProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<Layer> elements = layerService.getAll(properties, Pageable.unpaged());
    if (elements.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Layer does not exist");
    }
    return elements.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{textId}")
  public ResponseEntity<?> updateText(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewTextDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "layerId") Integer layerId,
      final @Valid @NotNull @PathVariable(value = "textId") Integer textId
  )
      throws QueryException, ObjectNotFoundException {
    final LayerText text = getTextFromService(projectId, mapId, layerId, textId);
    serializer.checkETag(oldETag, text);
    data.saveToText(text);
    layerTextService.update(text);

    return serializer.prepareResponse(getTextFromService(projectId, mapId, layerId, textId));
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{textId}")
  public void deleteText(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "layerId") Integer layerId,
      final @Valid @NotNull @PathVariable(value = "textId") Integer textId
  )
      throws QueryException, ObjectNotFoundException {
    final LayerText text = getTextFromService(projectId, mapId, layerId, textId);

    serializer.checkETag(oldETag, text);

    layerTextService.remove(text);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listTexts(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "layerId") Integer layerId
  ) {
    final Map<LayerTextProperty, Object> properties = new HashMap<>();

    properties.put(LayerTextProperty.PROJECT_ID, Collections.singletonList(projectId));
    properties.put(LayerTextProperty.LAYER_ID, Collections.singletonList(layerId));
    properties.put(LayerTextProperty.MAP_ID, Collections.singletonList(mapId));

    final Page<LayerText> page = layerTextService.getAll(properties, pageable);

    return serializer.prepareResponse(page);
  }

}