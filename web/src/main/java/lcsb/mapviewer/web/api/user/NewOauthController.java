package lcsb.mapviewer.web.api.user;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.services.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/oauth"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewOauthController extends BaseController {

  private final ClientRegistrationRepository clientRegistrationRepository;

  @Autowired
  @Lazy
  public NewOauthController(final ClientRegistrationRepository clientRegistrationRepository) {
    this.clientRegistrationRepository = clientRegistrationRepository;
  }

  @GetMapping(value = "/providers")
  public List<NewOAuthDTO> oauthProviders()
      throws IOException, QueryException, ObjectNotFoundException {

    String authorizationRequestBaseUri = "/oauth2/authorize-client";

    List<NewOAuthDTO> result = new ArrayList<>();
    for (String client : lcsb.mapviewer.common.Configuration.availableOAuthClients) {
      ClientRegistration clientRegistration = clientRegistrationRepository.findByRegistrationId(client);
      if (clientRegistration != null) {
        result.add(new NewOAuthDTO(clientRegistration.getClientName(), authorizationRequestBaseUri + "/" + clientRegistration.getRegistrationId()));
      }
    }
    return result;
  }
}