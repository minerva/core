package lcsb.mapviewer.web.api.project.map.bioentity.element.modification;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.converter.model.celldesigner.species.ModificationStateMapping;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.sbo.SBOTermModificationResidueType;
import lcsb.mapviewer.model.map.species.field.AbstractSiteModification;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.TranscriptionSite;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class NewModificationResidueDTO extends lcsb.mapviewer.web.api.AbstractDTO {

  @NotBlank
  private String sboTerm;

  @NotBlank
  private String modificationResidueId;

  @NotNull
  private Double x;

  @NotNull
  private Double y;

  @NotNull
  private Integer z;

  @NotNull
  private Double width;

  @NotNull
  private Double height;

  @NotNull
  private Double fontSize;

  @NotNull
  private java.awt.Color borderColor;

  @NotNull
  private java.awt.Color fillColor;

  @NotNull
  private String name;

  @NotNull
  private Double nameX;

  @NotNull
  private Double nameY;

  @NotNull
  private Double nameWidth;

  @NotNull
  private Double nameHeight;

  @NotNull
  private HorizontalAlign nameHorizontalAlign;

  @NotNull
  private VerticalAlign nameVerticalAlign;


  // ---------------------------------- AbstractSiteModification
  private String state;

  // ---------------------------------- TranscriptionSite
  @Pattern(regexp = "left|right", flags = Pattern.Flag.CASE_INSENSITIVE)
  private String direction;

  private Boolean active;


  public void saveToModificationResidue(
      final ModificationResidue mr,
      final boolean allowLayoutChanges) throws QueryException {
    checkEquality("SBOterm", getClazz(), mr.getClass());
    if (mr.getElementId() == null || mr.getElementId().equals(getModificationResidueId())) {
      mr.setIdModificationResidue(getModificationResidueId());
    } else {
      checkEquality("modificationResidueId", getModificationResidueId(), mr.getIdModificationResidue());
    }

    if (!allowLayoutChanges) {
      checkEquality("width", width, mr.getWidth());
      checkEquality("height", height, mr.getHeight());
      checkEquality("x", x, mr.getX());
      checkEquality("y", y, mr.getY());
      checkEquality("z", z, mr.getZ());
      checkEquality("fontSize", fontSize, mr.getFontSize());
      checkEquality("borderColor", getBorderColor(), mr.getBorderColor());
      checkEquality("fillColor", getFillColor(), mr.getFillColor());
      checkEquality("name", getName(), mr.getName());
      checkEquality("nameX", nameX, mr.getNameX());
      checkEquality("nameY", nameY, mr.getNameY());
      checkEquality("nameWidth", nameWidth, mr.getNameWidth());
      checkEquality("nameHeight", nameHeight, mr.getNameHeight());
      checkEquality("nameHorizontalAlign", nameHorizontalAlign, mr.getNameHorizontalAlign());
      checkEquality("nameVerticalAlign", nameVerticalAlign, mr.getNameVerticalAlign());
    } else {
      mr.setWidth(width);
      mr.setHeight(height);
      mr.setX(x);
      mr.setY(y);
      mr.setZ(z);
      mr.setFontSize(fontSize);
      mr.setBorderColor(getBorderColor());
      mr.setFillColor(getFillColor());
      mr.setName(getName());
      mr.setNameX(nameX);
      mr.setNameY(nameY);
      mr.setNameWidth(nameWidth);
      mr.setNameHeight(nameHeight);
      mr.setNameHorizontalAlign(nameHorizontalAlign);
      mr.setNameVerticalAlign(nameVerticalAlign);
    }

    if (mr instanceof AbstractSiteModification) {
      final AbstractSiteModification abstractSiteModification = (AbstractSiteModification) mr;
      if (!allowLayoutChanges) {
        checkEquality("state", getState(), abstractSiteModification.getState().getFullName());
      } else {
        abstractSiteModification.setState(ModificationStateMapping.getStateByName(getState()));
      }
    }

    if (mr instanceof TranscriptionSite) {
      final TranscriptionSite transcriptionSite = (TranscriptionSite) mr;
      if (!allowLayoutChanges) {
        checkEquality("active", getActive(), transcriptionSite.getActive());
        checkEqualityIgnoreCase("direction", getDirection(), transcriptionSite.getDirection());
      } else {
        transcriptionSite.setDirection(getDirection());
        transcriptionSite.setActive(getActive());
      }
    }
  }

  public Class<? extends ModificationResidue> getClazz() {
    return SBOTermModificationResidueType.createElementForSBOTerm(sboTerm, "m1", null).getClass();
  }

  public String getSboTerm() {
    return sboTerm;
  }

  public void setSboTerm(final String sboTerm) {
    this.sboTerm = sboTerm;
  }


  public Double getX() {
    return x;
  }

  public void setX(final Double x) {
    this.x = x;
  }

  public Double getY() {
    return y;
  }

  public void setY(final Double y) {
    this.y = y;
  }

  public Integer getZ() {
    return z;
  }

  public void setZ(final Integer z) {
    this.z = z;
  }

  public Double getWidth() {
    return width;
  }

  public void setWidth(final Double width) {
    this.width = width;
  }

  public Double getHeight() {
    return height;
  }

  public void setHeight(final Double height) {
    this.height = height;
  }

  public Double getFontSize() {
    return fontSize;
  }

  public void setFontSize(final Double fontSize) {
    this.fontSize = fontSize;
  }

  public java.awt.Color getBorderColor() {
    return borderColor;
  }

  public java.awt.Color getFillColor() {
    return fillColor;
  }

  public void setBorderColor(final java.awt.Color borderColor) {
    this.borderColor = borderColor;
  }

  public void setFillColor(final java.awt.Color fillColor) {
    this.fillColor = fillColor;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getState() {
    return state;
  }

  public void setState(final String state) {
    this.state = state;
  }

  public String getModificationResidueId() {
    return modificationResidueId;
  }

  public void setModificationResidueId(final String modificationResidueId) {
    this.modificationResidueId = modificationResidueId;
  }

  public String getDirection() {
    if (direction != null) {
      return direction.toUpperCase();
    } else {
      return null;
    }
  }

  public void setDirection(final String direction) {
    this.direction = direction;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(final Boolean active) {
    this.active = active;
  }

  public Double getNameX() {
    return nameX;
  }

  public void setNameX(final Double nameX) {
    this.nameX = nameX;
  }

  public Double getNameY() {
    return nameY;
  }

  public void setNameY(final Double nameY) {
    this.nameY = nameY;
  }

  public Double getNameWidth() {
    return nameWidth;
  }

  public void setNameWidth(final Double nameWidth) {
    this.nameWidth = nameWidth;
  }

  public Double getNameHeight() {
    return nameHeight;
  }

  public void setNameHeight(final Double nameHeight) {
    this.nameHeight = nameHeight;
  }

  public HorizontalAlign getNameHorizontalAlign() {
    return nameHorizontalAlign;
  }

  public void setNameHorizontalAlign(final HorizontalAlign nameHorizontalAlign) {
    this.nameHorizontalAlign = nameHorizontalAlign;
  }

  public VerticalAlign getNameVerticalAlign() {
    return nameVerticalAlign;
  }

  public void setNameVerticalAlign(final VerticalAlign nameVerticalAlign) {
    this.nameVerticalAlign = nameVerticalAlign;
  }
}
