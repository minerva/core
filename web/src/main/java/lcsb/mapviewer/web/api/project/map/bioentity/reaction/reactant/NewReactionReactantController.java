package lcsb.mapviewer.web.api.project.map.bioentity.reaction.reactant;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.map.ReactionProperty;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.map.bioentity.reaction.NewReactionNodeDTO;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/reactants",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewReactionReactantController {

  private final IReactionService reactionService;
  private final IElementService elementService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewReactionReactantController(
      final NewApiResponseSerializer serializer,
      final IElementService elementService,
      final IReactionService reactionService) {
    this.serializer = serializer;
    this.reactionService = reactionService;
    this.elementService = elementService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{reactantId}")
  public ResponseEntity<?> getReactant(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @NotNull @PathVariable(value = "reactantId") Integer reactantId)
      throws QueryException, ObjectNotFoundException {
    final Reactant reactant = getReactantFromService(projectId, mapId, reactionId, reactantId);
    return serializer.prepareResponse(reactant);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addReactant(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @RequestBody NewReactionNodeDTO data)
      throws QueryException, ObjectNotFoundException {

    final Reaction reaction = getReactionFromService(projectId, mapId, reactionId);

    final Reactant reactant = new Reactant(getElementFromService(projectId, mapId, data.getElement().getId()));
    reactant.setLine(new PolylineData());
    data.saveToNode(reactant);
    reaction.addReactant(reactant);
    reactionService.update(reaction);

    return serializer.prepareResponse(getReactantFromService(projectId, mapId, reactionId, reactant.getId()), HttpStatus.CREATED);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{reactantId}")
  public ResponseEntity<?> updateReactant(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewReactionNodeDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @NotNull @PathVariable(value = "reactantId") Integer reactantId)
      throws QueryException, ObjectNotFoundException {
    final Reactant reactant = getReactantFromService(projectId, mapId, reactionId, reactantId);
    serializer.checkETag(oldETag, reactant);
    data.saveToNode(reactant);
    reactionService.update(reactant.getReaction());

    return serializer.prepareResponse(getReactantFromService(projectId, mapId, reactionId, reactantId));
  }

  private Reactant getReactantFromService(final String projectId, final Integer mapId, final Integer reactionId, final Integer reactantId)
      throws QueryException, ObjectNotFoundException {
    Reactant reactant = null;
    final Reaction reaction = getReactionFromService(projectId, mapId, reactionId);
    for (final Reactant md : reaction.getReactants()) {
      if (md.getId() == reactantId) {
        reactant = md;
      }
    }

    if (reactant == null) {
      throw new ObjectNotFoundException("Reactant does not exist");
    }
    return reactant;
  }

  private Reaction getReactionFromService(final String projectId, final Integer mapId, final Integer reactionId) throws ObjectNotFoundException {
    final Map<ReactionProperty, Object> properties = new HashMap<>();
    properties.put(ReactionProperty.ID, Collections.singletonList(reactionId));
    properties.put(ReactionProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(ReactionProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<Reaction> reactions = reactionService.getAll(properties, Pageable.unpaged());
    if (reactions.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Reaction does not exist");
    }
    return reactions.getContent().get(0);
  }

  private Element getElementFromService(final String projectId, final Integer mapId, final Integer elementId) throws ObjectNotFoundException {
    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.ID, Collections.singletonList(elementId));
    properties.put(ElementProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(ElementProperty.PROJECT_ID, Collections.singletonList(projectId));
    final List<Element> elements = elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent();
    if (elements.size() == 0) {
      throw new ObjectNotFoundException("Reaction does not exist");
    }
    return elements.get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{reactantId}")
  public void deleteReactant(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @NotNull @PathVariable(value = "reactantId") Integer reactantId)
      throws QueryException, ObjectNotFoundException {
    final Reactant reactant = getReactantFromService(projectId, mapId, reactionId, reactantId);
    final Reaction reaction = reactant.getReaction();
    serializer.checkETag(oldETag, reactant);
    reaction.removeNode(reactant);
    reactionService.update(reaction);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listReactants(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId) throws QueryException, ObjectNotFoundException {
    final Reaction reaction = getReactionFromService(projectId, mapId, reactionId);
    final List<Reactant> reactants = new ArrayList<>(reaction.getReactants());

    reactants.sort(new Comparator<MinervaEntity>() {
      @Override
      public int compare(final MinervaEntity o1, final MinervaEntity o2) {
        return o1.getId() - o2.getId();
      }
    });

    return serializer.prepareResponse(serializer.createPage(reactants, pageable));
  }

}