package lcsb.mapviewer.web.api.project.shape;

import org.sbml.jsbml.ext.render.RelAbsVector;

public class RelAbsBezierPointDTO extends NewShapeDTO {
  private final Double absoluteX1;
  private final Double absoluteY1;
  private final Double relativeX1;
  private final Double relativeY1;
  private final Double relativeHeightForX1;
  private final Double relativeWidthForY1;

  private final Double absoluteX2;
  private final Double absoluteY2;
  private final Double relativeX2;
  private final Double relativeY2;
  private final Double relativeHeightForX2;
  private final Double relativeWidthForY2;

  private final Double absoluteX3;
  private final Double absoluteY3;
  private final Double relativeX3;
  private final Double relativeY3;
  private final Double relativeHeightForX3;
  private final Double relativeWidthForY3;

  public RelAbsBezierPointDTO(
      final RelAbsVector cx1, final RelAbsVector cy1, final Double relativeHeightForX1, final Double relativeWidthForY1,
      final RelAbsVector cx2, final RelAbsVector cy2, final Double relativeHeightForX2, final Double relativeWidthForY2,
      final RelAbsVector cx3, final RelAbsVector cy3, final Double relativeHeightForX3, final Double relativeWidthForY3
  ) {
    super("REL_ABS_BEZIER_POINT");
    absoluteX1 = cx1.getAbsoluteValue();
    absoluteY1 = cy1.getAbsoluteValue();
    relativeX1 = cx1.getRelativeValue();
    relativeY1 = cy1.getRelativeValue();
    this.relativeHeightForX1 = relativeHeightForX1;
    this.relativeWidthForY1 = relativeWidthForY1;

    absoluteX2 = cx2.getAbsoluteValue();
    absoluteY2 = cy2.getAbsoluteValue();
    relativeX2 = cx2.getRelativeValue();
    relativeY2 = cy2.getRelativeValue();
    this.relativeHeightForX2 = relativeHeightForX2;
    this.relativeWidthForY2 = relativeWidthForY2;

    absoluteX3 = cx3.getAbsoluteValue();
    absoluteY3 = cy3.getAbsoluteValue();
    relativeX3 = cx3.getRelativeValue();
    relativeY3 = cy3.getRelativeValue();
    this.relativeHeightForX3 = relativeHeightForX3;
    this.relativeWidthForY3 = relativeWidthForY3;
  }

  public Double getAbsoluteX1() {
    return absoluteX1;
  }

  public Double getAbsoluteY1() {
    return absoluteY1;
  }

  public Double getRelativeX1() {
    return relativeX1;
  }

  public Double getRelativeY1() {
    return relativeY1;
  }

  public Double getRelativeHeightForX1() {
    return relativeHeightForX1;
  }

  public Double getRelativeWidthForY1() {
    return relativeWidthForY1;
  }

  public Double getAbsoluteX2() {
    return absoluteX2;
  }

  public Double getAbsoluteY2() {
    return absoluteY2;
  }

  public Double getRelativeX2() {
    return relativeX2;
  }

  public Double getRelativeY2() {
    return relativeY2;
  }

  public Double getRelativeHeightForX2() {
    return relativeHeightForX2;
  }

  public Double getRelativeWidthForY2() {
    return relativeWidthForY2;
  }

  public Double getAbsoluteX3() {
    return absoluteX3;
  }

  public Double getAbsoluteY3() {
    return absoluteY3;
  }

  public Double getRelativeX3() {
    return relativeX3;
  }

  public Double getRelativeY3() {
    return relativeY3;
  }

  public Double getRelativeHeightForX3() {
    return relativeHeightForX3;
  }

  public Double getRelativeWidthForY3() {
    return relativeWidthForY3;
  }
}
