package lcsb.mapviewer.web.api.user;

import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.web.api.AbstractDTO;

import javax.validation.constraints.NotNull;

public class NewUserPrivilegeDTO extends AbstractDTO {

  @NotNull
  private PrivilegeType privilegeType;

  private String objectId;

  public PrivilegeType getPrivilegeType() {
    return privilegeType;
  }

  public void setPrivilegeType(final PrivilegeType privilegeType) {
    this.privilegeType = privilegeType;
  }

  public String getObjectId() {
    return objectId;
  }

  public void setObjectId(final String objectId) {
    this.objectId = objectId;
  }
}
