package lcsb.mapviewer.web.api.minervanet;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.minervanet.ReportSubmissionException;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.services.http.HttpClientFactory;
import lcsb.mapviewer.services.http.HttpClientFactoryImpl;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(
    value = {
        "/minerva/new_api/minervanet"
    })
public class NewMinervaNetController extends BaseController {

  private final Logger logger = LogManager.getLogger();

  private final IConfigurationService configurationService;

  private HttpClientFactory httpClientFactory;

  @Autowired
  public NewMinervaNetController(final IConfigurationService configurationService) {
    this.configurationService = configurationService;
    this.httpClientFactory = new HttpClientFactoryImpl();
  }

  void setHttpClientFactory(final HttpClientFactory httpClientFactory) {
    this.httpClientFactory = httpClientFactory;
  }

  @PostMapping(value = "/submitError")
  public Map<String, Object> submitError(final @RequestBody NewErrorReport report) throws IOException {
    String version = configurationService.getSystemSvnVersion();
    report.setVersion(version);

    ObjectMapper mapper = new ObjectMapper();
    String jsonReport = mapper.writeValueAsString(report);

    String server = configurationService.getValue(ConfigurationElementType.MINERVANET_URL).getValue();

    String url = server + "/api/reports";

    try (final CloseableHttpClient client = httpClientFactory.create()) {
      StringEntity requestEntity = new StringEntity(jsonReport, ContentType.APPLICATION_JSON);
      HttpPost post = new HttpPost(url);
      post.setEntity(requestEntity);
      try (final CloseableHttpResponse response = client.execute(post)) {
        HttpEntity responseEntity = response.getEntity();
        String responseBody = EntityUtils.toString(responseEntity, "UTF-8");
        if (response.getStatusLine().getStatusCode() != 200 || !responseBodyValid(responseBody)) {
          String error = "Could not submit report to MinervaNet. Reason: " + responseBody;
          logger.error(error);
          throw new ReportSubmissionException(error);
        }
      }
    }
    return new HashMap<>();
  }

  private boolean responseBodyValid(final String body) {
    if (body == null) {
      return false;
    }
    ObjectMapper mapper = new ObjectMapper();
    mapper.enable(DeserializationFeature.FAIL_ON_READING_DUP_TREE_KEY);
    try {
      mapper.readTree(body);
    } catch (final IOException e) {
      return false;
    }
    return true;
  }
}
