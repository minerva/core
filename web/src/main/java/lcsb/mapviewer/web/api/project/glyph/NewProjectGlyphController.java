package lcsb.mapviewer.web.api.project.glyph;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.persist.dao.graphics.GlyphProperty;
import lcsb.mapviewer.services.FailedDependencyException;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IFileService;
import lcsb.mapviewer.services.interfaces.IGlyphService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.ResponseDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.CacheControl;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId:.+}/glyphs",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewProjectGlyphController {

  private final Logger logger = LogManager.getLogger();

  private final IGlyphService glyphService;
  private final IFileService fileService;
  private final IProjectService projectService;
  private final NewApiResponseSerializer serializer;
  private final IUserService userService;

  @Autowired
  public NewProjectGlyphController(
      final IGlyphService glyphService,
      final NewApiResponseSerializer serializer,
      final IFileService fileService,
      final IProjectService projectService,
      final IUserService userService
  ) {
    this.glyphService = glyphService;
    this.serializer = serializer;
    this.fileService = fileService;
    this.projectService = projectService;
    this.userService = userService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping
  public ResponseEntity<?> getGlyphs(
      final @NotBlank @PathVariable(value = "projectId") String projectId,
      final Pageable pageable)
      throws ObjectNotFoundException, FailedDependencyException, IOException {
    Map<GlyphProperty, Object> searchFilter = new HashMap<>();
    searchFilter.put(GlyphProperty.PROJECT_ID, projectId);

    Page<Glyph> entries = glyphService.getAll(searchFilter, pageable, true);

    return serializer.prepareResponse(entries, new ResponseDecorator() {
      @Override
      public List<Pair<String, Object>> getDecoratedValues(final MinervaEntity object) {
        List<Pair<String, Object>> properties = new ArrayList<>();
        if (object instanceof Glyph) {
          properties.add(new Pair<>("filename", ((Glyph) object).getFile().getOriginalFileName()));
        }
        return properties;
      }

      @Override
      public List<Pair<String, Object>> getDecoratedValuesForRaw(final Object object) {
        return new ArrayList<>();
      }
    });

  }

  @PostMapping("/")
  public ResponseEntity<?> addGlyph(
      final @NotBlank @PathVariable(value = "projectId") String projectId,
      final Authentication authentication,
      @RequestParam("file") final MultipartFile file
  ) throws IOException, ObjectNotFoundException {


    UploadedFileEntry entry = new UploadedFileEntry();
    entry.setFileContent(file.getBytes());
    entry.setLength(entry.getFileContent().length);
    entry.setOwner(userService.getUserByLogin(authentication.getName()));
    entry.setOriginalFileName(file.getOriginalFilename());

    fileService.add(entry);

    Glyph glyph = new Glyph();
    glyph.setFile(entry);
    glyph.setProject(projectService.getProjectByProjectId(projectId));
    glyphService.add(glyph);

    return serializer.prepareResponse(glyph);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{glyphId}/fileContent")
  public ResponseEntity<byte[]> getProjectSource(
      final @PathVariable(value = "projectId") String projectId,
      final @PathVariable(value = "glyphId") Integer glyphId
  )
      throws QueryException, ObjectNotFoundException {

    Map<GlyphProperty, Object> searchFilter = new HashMap<>();
    searchFilter.put(GlyphProperty.PROJECT_ID, projectId);
    searchFilter.put(GlyphProperty.ID, glyphId);

    Page<Glyph> entries = glyphService.getAll(searchFilter, Pageable.unpaged());

    if (entries.isEmpty()) {
      throw new ObjectNotFoundException();
    }
    Glyph glyph = entries.getContent().get(0);

    UploadedFileEntry file = fileService.getById(glyph.getFile().getId());

    MediaType type = MediaType.APPLICATION_OCTET_STREAM;
    if (file.getOriginalFileName().endsWith("png")) {
      type = MediaType.IMAGE_PNG;
    } else if (file.getOriginalFileName().endsWith("gif")) {
      type = MediaType.IMAGE_GIF;
    } else if (file.getOriginalFileName().endsWith("jpg")) {
      type = MediaType.IMAGE_JPEG;
    }

    CacheControl cacheControl = CacheControl.maxAge(1, TimeUnit.DAYS);
    return ResponseEntity.ok().contentLength(file.getFileContent().length).contentType(type)
        .header("Content-Disposition", "attachment; filename=" + file.getOriginalFileName())
        .cacheControl(cacheControl)
        .body(file.getFileContent());
  }

}