package lcsb.mapviewer.web.api.project.map.bioentity.element;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.apache.commons.compress.utils.Sets;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewModelElementController {

  private final IModelService modelService;
  private final IProjectService projectService;
  private final IElementService elementService;
  private final IReactionService reactionService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewModelElementController(
      final IModelService modelService,
      final NewApiResponseSerializer serializer,
      final IElementService elementService,
      final IProjectService projectService,
      final IReactionService reactionService) {
    this.modelService = modelService;
    this.serializer = serializer;
    this.elementService = elementService;
    this.projectService = projectService;
    this.reactionService = reactionService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{elementId}")
  public ResponseEntity<?> getElement(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId)
      throws QueryException, ObjectNotFoundException {
    final Element element = getElementFromService(projectId, mapId, elementId);
    return serializer.prepareResponse(element);
  }

  private Element getElementFromService(final String projectId, final Integer mapId, final Integer elementId) throws ObjectNotFoundException {
    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.ID, Collections.singletonList(elementId));
    properties.put(ElementProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(ElementProperty.PROJECT_ID, Collections.singletonList(projectId));
    final List<Element> elements = elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent();
    if (elements.isEmpty()) {
      throw new ObjectNotFoundException("Element does not exist");
    }
    return elements.get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addElement(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @RequestBody NewElementDTO data)
      throws QueryException, ObjectNotFoundException {

    final ModelData model = modelService.getModelByMapId(projectId, mapId);

    final Class<? extends Element> clazz = data.getClazz();
    final Element element;
    try {
      element = clazz.getConstructor(String.class).newInstance(data.getElementId());
    } catch (final Exception e) {
      throw new QueryException("Invalid element class", e);
    }

    data.saveToElement(element, projectService.getBackgrounds(projectId, false).isEmpty());
    element.setModelData(model);
    elementService.add(element);

    return serializer.prepareResponse(getElementFromService(projectId, mapId, element.getId()), HttpStatus.CREATED);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{elementId}")
  public ResponseEntity<?> updateElement(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewElementDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "elementId") Integer elementId)
      throws QueryException, ObjectNotFoundException {
    final Element element = getElementFromService(projectId, mapId, elementId);
    serializer.checkETag(oldETag, element);
    data.saveToElement(element, projectService.getBackgrounds(projectId, false).isEmpty());
    elementService.update(element);

    return serializer.prepareResponse(getElementFromService(projectId, mapId, elementId));
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{elementId}")
  public void deleteElement(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "elementId") Integer elementId)
      throws QueryException, ObjectNotFoundException {
    final Element element = getElementFromService(projectId, mapId, elementId);
    if (element == null) {
      throw new ObjectNotFoundException("Element does not exist");
    }
    serializer.checkETag(oldETag, element);

    if (!reactionService.getReactionById(projectId, mapId + "", new HashSet<>(), Sets.newHashSet(elementId)).isEmpty()) {
      throw new QueryException("Cannot remove element with reactions");
    }
    Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.COMPARTMENT_ID, Collections.singletonList(elementId));
    if (elementService.getCount(properties) > 0) {
      throw new QueryException("Cannot remove compartment with children");
    }
    properties = new HashMap<>();
    properties.put(ElementProperty.COMPLEX_ID, Collections.singletonList(elementId));
    if (elementService.getCount(properties) > 0) {
      throw new QueryException("Cannot remove complex with children");
    }
    elementService.remove(element);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listElements(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId) throws QueryException {
    final Map<ElementProperty, Object> properties = new HashMap<>();

    properties.put(ElementProperty.PROJECT_ID, Collections.singletonList(projectId));
    properties.put(ElementProperty.MAP_ID, Collections.singletonList(mapId));

    final Page<Element> page = elementService.getElementsByFilter(properties, pageable, true);

    return serializer.prepareResponse(page);
  }

}