package lcsb.mapviewer.web.api.project.map.bioentity.element.uniprot;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.UniprotRecord;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.persist.dao.map.species.UniprotRecordProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUniprotRecordService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewUniprotRecordController {

  protected static Logger logger = LogManager.getLogger();

  private final IProjectService projectService;
  private final IElementService elementService;
  private final IUniprotRecordService uniprotRecordService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewUniprotRecordController(
      final NewApiResponseSerializer serializer,
      final IElementService elementService,
      final IUniprotRecordService uniprotRecordService,
      final IProjectService projectService) {
    this.uniprotRecordService = uniprotRecordService;
    this.serializer = serializer;
    this.elementService = elementService;
    this.projectService = projectService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{uniprotId}")
  public ResponseEntity<?> getUniprot(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @NotNull @PathVariable(value = "uniprotId") Integer uniprotId)
      throws ObjectNotFoundException {
    final UniprotRecord uniprot = getUniprotFromService(projectId, mapId, elementId, uniprotId);
    return serializer.prepareResponse(uniprot);
  }

  private UniprotRecord getUniprotFromService(final String projectId, final Integer mapId, final Integer elementId,
                                              final Integer uniprotId)
      throws ObjectNotFoundException {
    final Map<UniprotRecordProperty, Object> properties = new HashMap<>();
    properties.put(UniprotRecordProperty.ID, Collections.singletonList(uniprotId));
    properties.put(UniprotRecordProperty.ELEMENT_ID, Collections.singletonList(elementId));
    properties.put(UniprotRecordProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(UniprotRecordProperty.PROJECT_ID, Collections.singletonList(projectId));
    final Page<UniprotRecord> elements = uniprotRecordService.getAll(properties, Pageable.unpaged());
    if (elements.isEmpty()) {
      throw new ObjectNotFoundException("Uniprot Residue does not exist");
    }
    return elements.getContent().get(0);
  }

  private Element getElement(final String projectId, final Integer mapId, final Integer elementId) throws ObjectNotFoundException {
    final Map<ElementProperty, Object> properties = new HashMap<>();
    properties.put(ElementProperty.ID, Collections.singletonList(elementId));
    properties.put(ElementProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(ElementProperty.PROJECT_ID, Collections.singletonList(projectId));
    final List<Element> elements = elementService.getElementsByFilter(properties, Pageable.unpaged(), true).getContent();
    if (elements.isEmpty()) {
      throw new ObjectNotFoundException("Uniprot Residue does not exist");
    }
    return elements.get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addUniprotRecord(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @RequestBody NewUniprotRecordDTO data)
      throws QueryException, ObjectNotFoundException {

    final Element element = getElement(projectId, mapId, elementId);
    if (!(element instanceof Species)) {
      throw new QueryException("Uniprot Record cannot be added to a given element");
    }

    final Species species = (Species) element;

    final UniprotRecord mr = new UniprotRecord();

    data.saveToUniprotRecord(mr, projectService.getBackgrounds(projectId, false).isEmpty());
    mr.setSpecies(species);
    uniprotRecordService.add(mr);

    return serializer.prepareResponse(mr, HttpStatus.CREATED);
  }


  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{uniprotId}")
  public ResponseEntity<?> updateUniprot(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewUniprotRecordDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @NotNull @PathVariable(value = "uniprotId") Integer uniprotId
  ) throws QueryException, ObjectNotFoundException {
    final UniprotRecord uniprot = getUniprotFromService(projectId, mapId, elementId, uniprotId);
    serializer.checkETag(oldETag, uniprot);
    data.saveToUniprotRecord(uniprot, projectService.getBackgrounds(projectId, false).isEmpty());
    uniprotRecordService.update(uniprot);

    return serializer.prepareResponse(getUniprotFromService(projectId, mapId, elementId, uniprotId));
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{uniprotId}")
  public void deleteUniprot(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @Valid @NotNull @PathVariable(value = "elementId") Integer elementId,
      final @Valid @NotNull @PathVariable(value = "uniprotId") Integer uniprotId
  ) throws QueryException, ObjectNotFoundException {
    final UniprotRecord uniprot = getUniprotFromService(projectId, mapId, elementId, uniprotId);
    if (uniprot == null) {
      throw new ObjectNotFoundException("Uniprot does not exist");
    }
    serializer.checkETag(oldETag, uniprot);

    uniprotRecordService.remove(uniprot);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> getUniprots(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "elementId") Integer elementId) {
    final Map<UniprotRecordProperty, Object> properties = new HashMap<>();
    properties.put(UniprotRecordProperty.ELEMENT_ID, Collections.singletonList(elementId));
    properties.put(UniprotRecordProperty.MAP_ID, Collections.singletonList(mapId));
    properties.put(UniprotRecordProperty.PROJECT_ID, Collections.singletonList(projectId));

    final Page<UniprotRecord> uniprots = uniprotRecordService.getAll(properties, Pageable.unpaged());

    return serializer.prepareResponse(uniprots);
  }

}