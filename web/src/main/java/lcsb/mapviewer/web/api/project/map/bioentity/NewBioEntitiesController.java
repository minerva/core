package lcsb.mapviewer.web.api.project.map.bioentity;

import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.persist.dao.map.BioEntityProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.ISearchService;
import lcsb.mapviewer.services.utils.BioEntitySearchResult;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewBioEntitiesController extends BaseController {

  private final ISearchService searchService;
  private final NewApiResponseSerializer serializer;
  private final BioEntityResponseDecorator bioEntityResponseDecorator;

  @Autowired
  public NewBioEntitiesController(final ISearchService searchService, final NewApiResponseSerializer serializer,
                                  final IElementService elementService) {
    this.searchService = searchService;
    this.serializer = serializer;
    this.bioEntityResponseDecorator = new BioEntityResponseDecorator(elementService);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/:search")
  public ResponseEntity<?> listElements(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") String mapId,
      final @RequestParam(value = "query", required = false) String query,
      final @RequestParam(value = "perfectMatch", defaultValue = "false") boolean perfectMatch) throws QueryException, ObjectNotFoundException {
    Map<BioEntityProperty, Object> properties = new HashMap<>();

    properties.put(BioEntityProperty.PROJECT_ID, projectId);
    if (!mapId.equals("*")) {
      try {
        properties.put(BioEntityProperty.MAP_ID, Collections.singletonList(Integer.valueOf(mapId)));
      } catch (NumberFormatException e) {
        throw new QueryException("mapId must be numeric", e);
      }
    }
    if (query != null && !query.isEmpty()) {
      properties.put(BioEntityProperty.TEXT, query);
    }

    Page<BioEntitySearchResult> page = searchService.getBioEntitesByFilter(properties, pageable, perfectMatch);

    return serializer.prepareResponse(page, bioEntityResponseDecorator);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @RequestMapping(value = "suggestedQueryList", method = {RequestMethod.GET, RequestMethod.POST})
  public List<String> getSuggestedQueryList(
      final @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") String mapId
  )
      throws QueryException, ObjectNotFoundException {
    return searchService.getSuggestedQueryList(projectId, mapId);
  }

}