package lcsb.mapviewer.web.api.project.map.bioentity;

import org.hibernate.validator.constraints.NotBlank;

import lcsb.mapviewer.web.api.AbstractDTO;

public class NewSynonymDTO extends AbstractDTO {

  @NotBlank
  private String synonym;

  public String getSynonym() {
    return synonym;
  }

  public void setSynonym(final String synonym) {
    this.synonym = synonym;
  }

}
