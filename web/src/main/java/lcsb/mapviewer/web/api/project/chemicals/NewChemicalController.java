package lcsb.mapviewer.web.api.project.chemicals;

import lcsb.mapviewer.annotation.data.Chemical;
import lcsb.mapviewer.annotation.data.MeSH;
import lcsb.mapviewer.annotation.services.DrugSearchException;
import lcsb.mapviewer.annotation.services.TaxonomyBackend;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.annotation.services.dapi.ChemicalSearchException;
import lcsb.mapviewer.api.BaseController;
import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.modelutils.serializer.model.map.ElementIdentifierType;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IMeshService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.search.DbSearchCriteria;
import lcsb.mapviewer.services.search.chemical.IChemicalService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.ResponseDecorator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/"
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewChemicalController extends BaseController {

  private final IProjectService projectService;
  private final IChemicalService chemicalService;
  private final IElementService elementService;
  private final NewApiResponseSerializer serializer;
  private final IMeshService meshService;

  @Autowired
  public NewChemicalController(final NewApiResponseSerializer serializer, final IProjectService projectService,
                               final IChemicalService chemicalService,
                               final IElementService elementService,
                               final IMeshService meshService) {
    this.projectService = projectService;
    this.chemicalService = chemicalService;
    this.elementService = elementService;
    this.serializer = serializer;
    this.meshService = meshService;

  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "chemicals:search")
  public ResponseEntity<?> getChemicalsByQuery(
      final @PathVariable(value = "projectId") String projectId,
      final @RequestParam(value = "query", defaultValue = "") String query,
      final @RequestParam(value = "target", defaultValue = "") String target) throws QueryException, IOException, ObjectNotFoundException {
    final Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    if (project.getDisease() == null) {
      throw new QueryException("Project doesn't have disease associated to it");
    }

    List<Chemical> data = new ArrayList<>();
    if (!query.isEmpty()) {

      MiriamData organism = project.getOrganism();
      if (organism == null) {
        organism = TaxonomyBackend.HUMAN_TAXONOMY;
      }

      final Chemical chemical = chemicalService.getByName(query,
          new DbSearchCriteria().project(project).organisms(organism).colorSet(0).disease(project.getDisease()));
      if (chemical != null) {
        data.add(chemical);
      }
    } else if (target.contains(":")) {
      final String targetType = target.split(":", -1)[0];
      final String targetId = target.split(":", -1)[1];

      final Integer dbId = Integer.valueOf(targetId);
      final List<Element> targets = new ArrayList<>();
      if (targetType.equals(ElementIdentifierType.ALIAS.getJsName())) {
        final Map<ElementProperty, Object> properties = new HashMap<>();
        properties.put(ElementProperty.ID, Collections.singletonList(dbId));
        properties.put(ElementProperty.PROJECT_ID, Collections.singletonList(project.getProjectId()));
        final Page<Element> elements = elementService.getAll(properties, Pageable.unpaged());
        if (elements.getNumberOfElements() == 0) {
          throw new ObjectNotFoundException("Element does not exist");
        }
        targets.addAll(elements.getContent());
      } else {
        throw new QueryException("Targeting for the type not implemented");
      }
      MiriamData organism = project.getOrganism();
      if (organism == null) {
        organism = TaxonomyBackend.HUMAN_TAXONOMY;
      }

      data = chemicalService.getForTargets(targets,
          new DbSearchCriteria().project(project).organisms(organism).disease(project.getDisease()));
    }

    return serializer.prepareResponse(data, new ResponseDecorator() {

      @Override
      public List<Pair<String, Object>> getDecoratedValuesForRaw(final Object object) {
        final List<Pair<String, Object>> result = new ArrayList<>();
        if (object instanceof Chemical) {
          final Chemical chemical = (Chemical) object;
          String description = "Mesh term not available";

          try {
            final MeSH mesh = meshService.getMesh(chemical.getChemicalId());
            if (mesh != null && mesh.getDescription() != null) {
              description = mesh.getDescription();
            }
          } catch (final AnnotatorException e) {
            logger.error(e, e);
          }

          final List<MiriamData> references = new ArrayList<>();
          references.add(chemical.getChemicalId());
          if (chemical.getCasID() != null) {
            references.add(chemical.getCasID());
          }

          result.add(new Pair<String, Object>("description", description));
          result.add(new Pair<String, Object>("references", references));
        }
        return result;
      }

      @Override
      public List<Pair<String, Object>> getDecoratedValues(final MinervaEntity object) {
        return new ArrayList<>();
      }
    });
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "chemicals/suggestedQueryList")
  public List<String> getSuggestedQueryList(final @PathVariable(value = "projectId") String projectId)
      throws DrugSearchException, ObjectNotFoundException, ChemicalSearchException {
    final Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given id doesn't exist");
    }
    return chemicalService.getSuggestedQueryList(project, project.getDisease());
  }

}