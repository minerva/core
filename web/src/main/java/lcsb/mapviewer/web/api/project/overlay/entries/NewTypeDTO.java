package lcsb.mapviewer.web.api.project.overlay.entries;

import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.web.api.AbstractDTO;

import javax.validation.constraints.NotNull;

public class NewTypeDTO extends AbstractDTO {

  @NotNull
  private Class<? extends BioEntity> type;

  public Class<? extends BioEntity> getType() {
    return type;
  }

  public void setType(final Class<? extends BioEntity> type) {
    this.type = type;
  }
}
