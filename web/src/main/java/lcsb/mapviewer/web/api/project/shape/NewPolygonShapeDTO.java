package lcsb.mapviewer.web.api.project.shape;

import lcsb.mapviewer.common.exception.NotImplementedException;
import lcsb.mapviewer.converter.model.sbml.extension.render.CustomRenderCubicBezier;
import lcsb.mapviewer.converter.model.sbml.extension.render.CustomRenderPoint;
import org.sbml.jsbml.ext.render.Polygon;
import org.sbml.jsbml.ext.render.RenderCubicBezier;
import org.sbml.jsbml.ext.render.RenderPoint;

import java.util.ArrayList;
import java.util.List;

public class NewPolygonShapeDTO extends NewShapeDTO {
  private final List<NewShapeDTO> points = new ArrayList<>();

  private final boolean fill = true;

  public NewPolygonShapeDTO(final Polygon polygon) {
    super("POLYGON");
    for (final RenderPoint point : polygon.getListOfElements()) {
      if (point.getClass() == RenderPoint.class) {
        getPoints().add(new RelAbsPointDTO(point.getX(), point.getY()));
      } else if (point.getClass() == CustomRenderPoint.class) {
        final CustomRenderPoint customRenderPoint = (CustomRenderPoint) point;
        getPoints().add(new RelAbsPointDTO(
            customRenderPoint.getX(),
            customRenderPoint.getY(),
            customRenderPoint.getHeightRelative(),
            customRenderPoint.getWidthRelative()));
      } else if (point.getClass() == CustomRenderCubicBezier.class) {
        final CustomRenderCubicBezier customRenderPoint = (CustomRenderCubicBezier) point;
        getPoints().add(new RelAbsBezierPointDTO(
            customRenderPoint.getX(),
            customRenderPoint.getY(),
            customRenderPoint.getHeightRelative(),
            customRenderPoint.getWidthRelative(),
            customRenderPoint.getX1(),
            customRenderPoint.getY1(),
            customRenderPoint.getHeightRelative1(),
            customRenderPoint.getWidthRelative1(),
            customRenderPoint.getX2(),
            customRenderPoint.getY2(),
            customRenderPoint.getHeightRelative2(),
            customRenderPoint.getWidthRelative2()
        ));
      } else if (point.getClass() == RenderCubicBezier.class) {
        final RenderCubicBezier customRenderPoint = (RenderCubicBezier) point;
        getPoints().add(new RelAbsBezierPointDTO(
            customRenderPoint.getX(),
            customRenderPoint.getY(),
            null,
            null,
            customRenderPoint.getX1(),
            customRenderPoint.getY1(),
            null,
            null,
            customRenderPoint.getX2(),
            customRenderPoint.getY2(),
            null,
            null
        ));
      } else {
        throw new NotImplementedException(point.getClass().getSimpleName() + " not implemented");
      }
    }
  }

  public List<NewShapeDTO> getPoints() {
    return points;
  }

  public boolean isFill() {
    return fill;
  }
}
