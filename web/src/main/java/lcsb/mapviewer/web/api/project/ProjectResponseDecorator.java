package lcsb.mapviewer.web.api.project;

import lcsb.mapviewer.annotation.data.MeSH;
import lcsb.mapviewer.annotation.services.TaxonomySearchException;
import lcsb.mapviewer.annotation.services.annotators.AnnotatorException;
import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.services.interfaces.IMeshService;
import lcsb.mapviewer.services.interfaces.ITaxonomyService;
import lcsb.mapviewer.web.api.ResponseDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ProjectResponseDecorator implements ResponseDecorator {

  private final Logger logger = LogManager.getLogger();

  private final Set<String> projectsInMinervaNet;
  private final IMeshService meshService;
  private final ITaxonomyService taxonomyService;


  public ProjectResponseDecorator(final Set<String> projectsInMinervaNet,
                                  final IMeshService meshService,
                                  final ITaxonomyService taxonomyService) {
    this.projectsInMinervaNet = projectsInMinervaNet;
    this.meshService = meshService;
    this.taxonomyService = taxonomyService;
  }

  @Override
  public List<Pair<String, Object>> getDecoratedValues(final MinervaEntity object) {
    final List<Pair<String, Object>> result = new ArrayList<>();
    if (object instanceof Project) {
      final Project project = (Project) object;
      String diseaseName = null;
      if (project.getDisease() != null) {
        final MeSH mesh;
        try {
          mesh = meshService.getMesh(project.getDisease());
          if (mesh != null) {
            diseaseName = mesh.getName();
          }
        } catch (final AnnotatorException e) {
          logger.error("Problem with mesh", e);
        }
      }
      String organismName = null;
      if (project.getOrganism() != null) {
        try {
          organismName = taxonomyService.getNameForTaxonomy(project.getOrganism());
        } catch (final TaxonomySearchException e) {
          logger.error("Problem with taxonomy", e);
        }
      }

      result.add(new Pair<>("diseaseName", diseaseName));
      result.add(new Pair<>("organismName", organismName));

      final boolean val = projectsInMinervaNet.contains((project).getProjectId());
      result.add(new Pair<>("sharedInMinervaNet", val));
    }
    return result;
  }

  @Override
  public List<Pair<String, Object>> getDecoratedValuesForRaw(final Object object) {
    return new ArrayList<>();
  }

}
