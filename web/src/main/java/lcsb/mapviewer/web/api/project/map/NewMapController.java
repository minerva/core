package lcsb.mapviewer.web.api.project.map;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.map.ModelProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewMapController {

  private IModelService modelService;
  private IProjectService projectService;
  private NewApiResponseSerializer serializer;

  @Autowired
  public NewMapController(
      final IModelService modelService,
      final NewApiResponseSerializer serializer,
      final IProjectService projectService) {
    this.modelService = modelService;
    this.serializer = serializer;
    this.projectService = projectService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{mapId}")
  public ResponseEntity<?> getMap(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId)
      throws QueryException, ObjectNotFoundException {
    ModelData modelData = modelService.getModelByMapId(projectId, mapId);
    return serializer.prepareResponse(modelData);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addMap(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @RequestBody NewModelDataDTO data)
      throws QueryException, ObjectNotFoundException {

    Project project = projectService.getProjectByProjectId(projectId);
    if (project == null) {
      throw new ObjectNotFoundException("Project with given projectId does not exist");
    }

    ModelData modelData = new ModelData();
    modelData.setCreationDate(Calendar.getInstance());
    data.saveToModelData(modelData, true);
    modelData.setProject(project);
    modelService.add(modelData);

    return serializer.prepareResponse(modelData, HttpStatus.CREATED);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{mapId}")
  public ResponseEntity<?> updateMap(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewModelDataDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId) throws QueryException, ObjectNotFoundException {
    ModelData model = modelService.getModelByMapId(projectId, mapId);
    if (model == null) {
      throw new ObjectNotFoundException("Map with given id does not exist");
    }
    serializer.checkETag(oldETag, model);
    data.saveToModelData(model, projectService.getBackgrounds(projectId, false).size() == 0);
    modelService.update(model);

    return getMap(projectId, mapId);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{mapId}")
  public void deleteMap(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId)
      throws QueryException, ObjectNotFoundException {

    ModelData model = modelService.getModelByMapId(projectId, mapId);
    if (model == null) {
      throw new ObjectNotFoundException("Map with given id does not exist");
    }
    serializer.checkETag(oldETag, model);
    modelService.remove(model);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listMaps(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId) {
    Map<ModelProperty, Object> modelFilter = new HashMap<>();
    modelFilter.put(ModelProperty.PROJECT_ID, projectId);

    Page<ModelData> models = modelService.getAll(modelFilter, pageable);
    return serializer.prepareResponse(models);
  }

}