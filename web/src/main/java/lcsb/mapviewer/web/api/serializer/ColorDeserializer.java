package lcsb.mapviewer.web.api.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.awt.Color;
import java.io.IOException;

public class ColorDeserializer extends StdDeserializer<Color> {

  public ColorDeserializer() {
    super(Color.class);
  }

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  @Override
  public Color deserialize(final JsonParser parser, final DeserializationContext context) throws IOException {
    ObjectMapper mapper = (ObjectMapper) parser.getCodec();
    ObjectNode rootNode = mapper.readTree(parser);

    int alpha = rootNode.get("alpha").asInt() & 0xff;
    int rgb = rootNode.get("rgb").asInt() & 0xffffff;

    return new Color(rgb | alpha << 24, true);
  }
}
