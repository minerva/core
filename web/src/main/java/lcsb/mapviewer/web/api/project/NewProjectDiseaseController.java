package lcsb.mapviewer.web.api.project;

import java.io.IOException;
import java.util.Objects;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.services.FailedDependencyException;
import lcsb.mapviewer.services.ObjectExistsException;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IMiriamService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId:.+}/disease",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewProjectDiseaseController {

  private IProjectService projectService;
  private IMiriamService miriamService;
  private NewApiResponseSerializer serializer;

  @Autowired
  public NewProjectDiseaseController(final IProjectService projectService, final NewApiResponseSerializer serializer,
      final IMiriamService miriamService) {
    this.projectService = projectService;
    this.serializer = serializer;
    this.miriamService = miriamService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping
  public ResponseEntity<?> getDisease(final @NotBlank @PathVariable(value = "projectId") String projectId)
      throws ObjectNotFoundException, FailedDependencyException, IOException {
    Project project = projectService.getProjectByProjectId(projectId, true);
    MiriamData md = null;
    if (project != null) {
      md = project.getDisease();
    }
    return serializer.prepareResponse(md);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'IS_CURATOR')")
  @PostMapping
  public ResponseEntity<?> addDisease(
      final @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @RequestBody NewMiriamDataDTO data)
      throws QueryException, ObjectExistsException, ObjectNotFoundException {
    Project project = projectService.getProjectByProjectId(projectId, true);
    MiriamData result = null;
    if (project != null) {
      if (project.getDisease() != null) {
        throw new ObjectExistsException("Disease is already defined");
      }
      result = new MiriamData();
      data.saveToMiriamData(result);
      if (!Objects.equals(MiriamType.MESH_2012, data.getType())) {
        throw new QueryException("Only mesh identifiers can be accepted as disease identifier");
      }
      miriamService.add(result);
      project.setDisease(result);
      projectService.update(project);
    }
    return serializer.prepareResponse(result);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping
  public ResponseEntity<?> updateDisease(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @PathVariable(value = "projectId") String projectId,
      final @Valid @RequestBody NewMiriamDataDTO data)
      throws IOException, QueryException, ObjectNotFoundException {
    Project project = projectService.getProjectByProjectId(projectId, true);
    MiriamData result = null;
    if (project != null && project.getDisease() != null) {
      result = project.getDisease();
      serializer.checkETag(oldETag, result);
      data.saveToMiriamData(result);
      if (!Objects.equals(MiriamType.MESH_2012, data.getType())) {
        throw new QueryException("Only mesh identifiers can be accepted as disease identifier");
      }
      miriamService.update(result);
    }
    return serializer.prepareResponse(result);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @DeleteMapping
  public void deleteDisease(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @PathVariable(value = "projectId") String projectId)
      throws QueryException, ObjectNotFoundException {
    Project project = projectService.getProjectByProjectId(projectId);
    if (project == null || project.getDisease() == null) {
      throw new ObjectNotFoundException("Object not found");
    }
    serializer.checkETag(oldETag, project.getDisease());
    miriamService.delete(project.getDisease());
  }
}