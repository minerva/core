package lcsb.mapviewer.web.api.project;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.web.api.AbstractDTO;

public class NewMiriamDataDTO extends AbstractDTO {
  @NotNull
  private MiriamType type;

  @NotBlank
  @Size(max = 255)
  private String resource;

  public void saveToMiriamData(final MiriamData md) {
    md.setDataType(getType());
    md.setResource(resource);
  }

  public MiriamType getType() {
    return type;
  }

  public String getResource() {
    return resource;
  }

  public void setType(final MiriamType type) {
    this.type = type;
  }

  public void setResource(final String resource) {
    this.resource = resource;
  }
}
