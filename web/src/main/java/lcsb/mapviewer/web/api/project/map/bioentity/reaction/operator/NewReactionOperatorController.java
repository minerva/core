package lcsb.mapviewer.web.api.project.map.bioentity.reaction.operator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.MinervaEntity;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.persist.dao.map.ReactionProperty;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.INodeService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewReactionOperatorController {

  private IReactionService reactionService;
  private NewApiResponseSerializer serializer;
  private INodeService nodeService;

  @Autowired
  public NewReactionOperatorController(
      final NewApiResponseSerializer serializer,
      final IReactionService reactionService,
      final INodeService nodeService) {
    this.serializer = serializer;
    this.reactionService = reactionService;
    this.nodeService = nodeService;
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/{operatorId}")
  public ResponseEntity<?> getOperator(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @NotNull @PathVariable(value = "operatorId") Integer operatorId)
      throws QueryException, ObjectNotFoundException {
    NodeOperator operator = getOperatorFromService(projectId, mapId, reactionId, operatorId);
    return serializer.prepareResponse(operator);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PostMapping(value = "/")
  public ResponseEntity<?> addOperator(
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @RequestBody NewOperatorNodeDTO data)
      throws QueryException, ObjectNotFoundException, lcsb.mapviewer.api.QueryException {

    Reaction reaction = getReactionFromService(projectId, mapId, reactionId);

    NodeOperator operator;
    try {
      Class<? extends NodeOperator> clazz = data.getClazz();
      operator = clazz.getConstructor().newInstance();
    } catch (Exception e) {
      throw new QueryException("Invalid oprator class", e);
    }
    operator.setLine(new PolylineData());
    data.saveToNode(operator);
    reaction.addNode(operator);
    reactionService.update(reaction);

    return serializer.prepareResponse(getOperatorFromService(projectId, mapId, reactionId, operator.getId()), HttpStatus.CREATED);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN', 'WRITE_PROJECT:' + #projectId)")
  @PutMapping(value = "/{operatorId}")
  public ResponseEntity<?> updateOperator(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewOperatorNodeDTO data,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @NotNull @PathVariable(value = "operatorId") Integer operatorId)
      throws QueryException, ObjectNotFoundException, lcsb.mapviewer.api.QueryException {
    NodeOperator operator = getOperatorFromService(projectId, mapId, reactionId, operatorId);
    serializer.checkETag(oldETag, operator);
    data.saveToNode(operator);
    reactionService.update(operator.getReaction());

    return serializer.prepareResponse(getOperatorFromService(projectId, mapId, reactionId, operatorId));
  }

  private NodeOperator getOperatorFromService(final String projectId, final Integer mapId, final Integer reactionId, final Integer operatorId)
      throws QueryException, ObjectNotFoundException {
    NodeOperator operator = null;
    Reaction reaction = getReactionFromService(projectId, mapId, reactionId);
    for (NodeOperator md : reaction.getOperators()) {
      if (md.getId() == operatorId) {
        operator = md;
      }
    }

    if (operator == null) {
      throw new ObjectNotFoundException("Operator does not exist");
    }
    return operator;
  }

  private Reaction getReactionFromService(final String projectId, final Integer mapId, final Integer reactionId) throws ObjectNotFoundException {
    Map<ReactionProperty, Object> properties = new HashMap<>();
    properties.put(ReactionProperty.ID, Arrays.asList(reactionId));
    properties.put(ReactionProperty.MAP_ID, Arrays.asList(mapId));
    properties.put(ReactionProperty.PROJECT_ID, Arrays.asList(projectId));
    Page<Reaction> reactions = reactionService.getAll(properties, Pageable.unpaged());
    if (reactions.getNumberOfElements() == 0) {
      throw new ObjectNotFoundException("Reaction does not exist");
    }
    return reactions.getContent().get(0);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','WRITE_PROJECT:' + #projectId)")
  @DeleteMapping(value = "/{operatorId}")
  public void deleteOperator(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @NotNull @PathVariable(value = "operatorId") Integer operatorId)
      throws QueryException, ObjectNotFoundException {
    NodeOperator operator = getOperatorFromService(projectId, mapId, reactionId, operatorId);
    serializer.checkETag(oldETag, operator);
    nodeService.remove(operator);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN','READ_PROJECT:' + #projectId)")
  @GetMapping(value = "/")
  public ResponseEntity<?> listOperators(
      final Pageable pageable,
      final @Valid @NotBlank @PathVariable(value = "projectId") String projectId,
      final @NotNull @PathVariable(value = "reactionId") Integer reactionId,
      final @Valid @NotNull @PathVariable(value = "mapId") Integer mapId) throws QueryException, ObjectNotFoundException {
    Reaction reaction = getReactionFromService(projectId, mapId, reactionId);
    List<NodeOperator> operators = new ArrayList<>(reaction.getOperators());

    operators.sort(new Comparator<MinervaEntity>() {
      @Override
      public int compare(final MinervaEntity o1, final MinervaEntity o2) {
        return o1.getId() - o2.getId();
      }
    });

    return serializer.prepareResponse(serializer.createPage(operators, pageable));
  }

}