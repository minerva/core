package lcsb.mapviewer.web.api.plugin;

import lcsb.mapviewer.api.QueryException;
import lcsb.mapviewer.model.plugin.Plugin;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.persist.dao.plugin.PluginProperty;
import lcsb.mapviewer.services.FailedDependencyException;
import lcsb.mapviewer.services.ObjectExistsException;
import lcsb.mapviewer.services.ObjectNotFoundException;
import lcsb.mapviewer.services.interfaces.IPluginService;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@Validated
@RequestMapping(
    value = {
        "/minerva/new_api/plugins",
    },
    produces = MediaType.APPLICATION_JSON_VALUE)
public class NewPluginController {

  private final Logger logger = LogManager.getLogger();

  private final IPluginService pluginService;
  private final NewApiResponseSerializer serializer;

  @Autowired
  public NewPluginController(
      final IPluginService pluginService,
      final NewApiResponseSerializer serializer) {
    this.serializer = serializer;
    this.pluginService = pluginService;
  }

  @GetMapping(value = "/{hash}")
  public ResponseEntity<?> getPlugin(final @NotBlank @PathVariable(value = "hash") String hash)
      throws ObjectNotFoundException, FailedDependencyException, IOException {
    final lcsb.mapviewer.model.plugin.Plugin plugin = pluginService.getByHash(hash);
    return serializer.prepareResponse(plugin);
  }

  @PostMapping(value = "/")
  public ResponseEntity<?> addPlugin(
      final Authentication authentication,
      final @Valid @RequestBody NewPluginDTO data)
      throws QueryException, ObjectNotFoundException, ObjectExistsException {

    final boolean isAdmin = authentication.getAuthorities()
        .contains(new SimpleGrantedAuthority(PrivilegeType.IS_ADMIN.name()));

    Plugin plugin = pluginService.getByHash(data.getHash());
    if (plugin != null) {
      throw new ObjectExistsException("Plugin with given hash already exists");
    }
    plugin = new Plugin();
    data.saveToPlugin(plugin, isAdmin, pluginService::getExpectedHash);
    pluginService.add(plugin);

    return serializer.prepareResponse(plugin, HttpStatus.CREATED);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN')")
  @PutMapping(value = "/{hash}")
  public ResponseEntity<?> updatePlugin(
      final Authentication authentication,
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @Valid @RequestBody NewPluginDTO data,
      final @PathVariable(value = "hash") String hash)
      throws IOException, QueryException, ObjectNotFoundException, FailedDependencyException {
    final boolean isAdmin = authentication.getAuthorities()
        .contains(new SimpleGrantedAuthority(PrivilegeType.IS_ADMIN.name()));

    Plugin plugin = pluginService.getByHash(hash);
    if (plugin == null) {
      throw new ObjectNotFoundException("Plugin with given hash does not exist");
    }
    serializer.checkETag(oldETag, plugin);
    data.saveToPlugin(plugin, isAdmin, pluginService::getExpectedHash);
    pluginService.update(plugin);

    return serializer.prepareResponse(plugin);
  }

  @PreAuthorize("hasAnyAuthority('IS_ADMIN')")
  @DeleteMapping(value = "/{hash}")
  public void deletePlugin(
      final @RequestHeader(name = "If-Match", required = false) String oldETag,
      final @PathVariable(value = "hash") String hash)
      throws QueryException, ObjectNotFoundException {

    final Plugin plugin = pluginService.getByHash(hash);
    if (plugin == null) {
      throw new ObjectNotFoundException("Plugin with given id doesn't exist");
    }
    serializer.checkETag(oldETag, plugin);

    pluginService.delete(plugin);
  }

  @GetMapping(value = "/")
  public ResponseEntity<?> listPlugins(final Pageable pageable) {
    final Map<PluginProperty, Object> pluginFilter = new HashMap<>();
    pluginFilter.put(PluginProperty.PUBLIC_VALUE, true);
    final Page<Plugin> plugins = pluginService.getAll(pluginFilter, pageable);
    return serializer.prepareResponse(plugins);
  }
}