package lcsb.mapviewer.web.api.project.shape;

import org.sbml.jsbml.ext.render.RelAbsVector;

public class RelAbsPointDTO extends NewShapeDTO {
  private final Double absoluteX;
  private final Double absoluteY;
  private final Double relativeX;
  private final Double relativeY;
  private final Double relativeHeightForX;
  private final Double relativeWidthForY;

  public RelAbsPointDTO(final RelAbsVector cx, final RelAbsVector cy) {
    super("REL_ABS_POINT");
    absoluteX = cx.getAbsoluteValue();
    absoluteY = cy.getAbsoluteValue();
    relativeX = cx.getRelativeValue();
    relativeY = cy.getRelativeValue();
    this.relativeHeightForX = null;
    this.relativeWidthForY = null;
  }

  public RelAbsPointDTO(final RelAbsVector cx, final RelAbsVector cy, final Double relativeHeightForX, final Double relativeWidthForY) {
    super("REL_ABS_POINT");
    absoluteX = cx.getAbsoluteValue();
    absoluteY = cy.getAbsoluteValue();
    relativeX = cx.getRelativeValue();
    relativeY = cy.getRelativeValue();
    this.relativeHeightForX = relativeHeightForX;
    this.relativeWidthForY = relativeWidthForY;
  }

  public Double getAbsoluteX() {
    return absoluteX;
  }

  public Double getAbsoluteY() {
    return absoluteY;
  }

  public Double getRelativeX() {
    return relativeX;
  }

  public Double getRelativeY() {
    return relativeY;
  }

  public Double getRelativeHeightForX() {
    return relativeHeightForX;
  }

  public Double getRelativeWidthForY() {
    return relativeWidthForY;
  }
}
