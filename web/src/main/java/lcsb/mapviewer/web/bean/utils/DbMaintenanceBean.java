package lcsb.mapviewer.web.bean.utils;

import lcsb.mapviewer.model.job.MinervaJobStatus;
import lcsb.mapviewer.model.job.MinervaJobType;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.persist.dao.cache.UploadedFileEntryDao;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.sql.SQLException;
import java.util.Calendar;

@Component
public class DbMaintenanceBean {

  private final transient Logger logger = LogManager.getLogger();

  private final transient DbUtils dbUtils;
  private final transient UploadedFileEntryDao uploadedFileEntryDao;
  private final transient IMinervaJobService minervaJobService;

  @Autowired
  public DbMaintenanceBean(final DbUtils dbUtils, final UploadedFileEntryDao uploadedFileEntryDao, final IMinervaJobService minervaJobService) {
    this.dbUtils = dbUtils;
    this.uploadedFileEntryDao = uploadedFileEntryDao;
    this.minervaJobService = minervaJobService;
  }

  @Transactional
  @Scheduled(cron = "0 15 2 * * ?")
  public void scheduleTaskUsingCronExpression() {
    try {
      logger.info("VACUUM started");
      dbUtils.vaccuum();
    } catch (SQLException e) {
      logger.error("Problem with vacuum", e);
    } finally {
      logger.info("VACUUM finished");
    }
  }

  @Transactional
  @Scheduled(cron = "0 0 1 * * SAT")
  public void scheduleOrphanFileRemoveTaskUsingCronExpression() {
    try {
      logger.info("Remove orphan files");
      uploadedFileEntryDao.removeOrphans();
    } catch (Exception e) {
      logger.error("Problem with Remove orphan files", e);
    } finally {
      logger.info("Remove orphan finished");
    }
  }

  @Transactional
  @Scheduled(cron = "0 15 1 * * ?")
  public void removeOldJobsCronJobExpression() {
    try {
      logger.info("Remove old jobs from db");

      //remove all older than 6 months
      Calendar beforeDate = Calendar.getInstance();
      beforeDate.add(Calendar.MONTH, -6);
      minervaJobService.removeOldJobs(beforeDate);

      //remove successful older than 1 month
      beforeDate = Calendar.getInstance();
      beforeDate.add(Calendar.MONTH, -1);
      minervaJobService.removeOldJobs(MinervaJobStatus.DONE, beforeDate);

      //remove successful refresh older than 1 week
      beforeDate = Calendar.getInstance();
      beforeDate.add(Calendar.DATE, -7);
      for (MinervaJobType type : MinervaJobType.values()) {
        if (type.name().toLowerCase().startsWith("refresh")) {
          minervaJobService.removeOldJobs(type, MinervaJobStatus.DONE, beforeDate);
        }
      }

    } finally {
      logger.info("Remove old jobs from db finished");
    }
  }

}
