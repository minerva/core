package lcsb.mapviewer.web.bean.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CacheInterceptor implements HandlerInterceptor {
  private final Logger logger = LogManager.getLogger();

  @Override
  public void postHandle(final HttpServletRequest request,
                         final HttpServletResponse response,
                         final Object handler,
                         final ModelAndView modelAndView) {
    boolean disableCache = true;
    String url = request.getRequestURL().toString();
    if (url.contains("minerva/resources") || !response.getHeaders("Cache-Control").isEmpty()) {
      disableCache = false;
    }

    if (disableCache) {
      // caching on Safari
      response.addHeader("Vary", "*");
      // generic cache prevent mechanism
      response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
      response.addHeader("Pragma", "no-cache");
      response.addHeader("Expires", "0");
    }
  }
}
