package lcsb.mapviewer.web.bean.utils;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.routines.UrlValidator;

import lcsb.mapviewer.common.Configuration;

public class XFrameFilter implements Filter {

  @Override
  public void init(final FilterConfig config) throws ServletException {
  }

  @Override
  public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
      throws IOException, ServletException {
    HttpServletResponse response = (HttpServletResponse) res;
    List<String> domains = Configuration.getxFrameDomain();

    StringBuilder value = new StringBuilder("frame-ancestors ");
    for (final String domain : domains) {
      if (new UrlValidator().isValid(domain) || (domain != null && domain.contains("localhost"))) {
        value.append(domain).append(" ");
      }
    }

    if (!value.toString().equals("frame-ancestors ")) {
      response.addHeader("Content-Security-Policy", value.toString());
    } else {
      response.addHeader("X-Frame-Options", "DENY");
    }
    chain.doFilter(req, response);
  }

  @Override
  public void destroy() {
  }

}
