package lcsb.mapviewer.web.bean.utils;

import lcsb.mapviewer.common.Configuration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This filter enables ajax queries from all domains. It should be used for
 * restfull API.
 *
 * @author Piotr Gawron
 */
public class CORSFilter implements Filter {

  @Override
  public void init(final FilterConfig config) throws ServletException {
  }

  @Override
  public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
      throws IOException, ServletException {
    HttpServletResponse response = (HttpServletResponse) res;
    HttpServletRequest request = (HttpServletRequest) req;

    String origin = request.getHeader("ORIGIN");
    if (origin == null || origin.trim().isEmpty() || !Configuration.isDisableCors()) {
      origin = "*";
    }
    response.setHeader("Access-Control-Allow-Origin", origin);
    if (Configuration.isDisableCors()) {
      response.setHeader("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,PATCH,OPTIONS");
      response.setHeader("Access-Control-Allow-Credentials", "true");
      response.setHeader("Access-Control-Allow-Headers",
          "Access-Control-Allow-Headers, "
              + "Origin, "
              + "Accept, "
              + "X-Requested-With, "
              + "Content-Type, "
              + "Access-Control-Request-Method, "
              + "Access-Control-Request-Headers, "
              + Configuration.AUTH_TOKEN);
    }
    chain.doFilter(req, response);
  }

  @Override
  public void destroy() {
  }

}
