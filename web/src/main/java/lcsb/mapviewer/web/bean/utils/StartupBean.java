package lcsb.mapviewer.web.bean.utils;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.IProgressUpdater;
import lcsb.mapviewer.common.MinervaConfigurationHolder;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectLogEntry;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.ProjectStatus;
import lcsb.mapviewer.model.map.layout.ReferenceGenome;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.persist.CustomDatabasePopulator;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IReferenceGenomeService;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.init.ScriptException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Bean where init script of the application is placed. The method in this bean
 * should be called only once, during application start.
 */
@Component
public class StartupBean {

  private final transient Logger logger = LogManager.getLogger();

  private final transient IProjectService projectService;
  private final transient IConfigurationService configurationService;
  private final transient IReferenceGenomeService referenceGenomeService;
  private final CustomDatabasePopulator flyway;
  private final IMinervaJobService minervaJobService;

  private final MinervaConfigurationHolder configurationHolder;

  private final DbMaintenanceBean dbMaintenanceBean;

  @Autowired
  public StartupBean(final IProjectService projectService,
                     final IConfigurationService configurationService,
                     final IReferenceGenomeService referenceGenomeService,
                     final CustomDatabasePopulator flyway,
                     final IMinervaJobService minervaJobService,
                     final MinervaConfigurationHolder configurationHolder,
                     final DbMaintenanceBean dbMaintenanceBean) {
    this.projectService = projectService;
    this.configurationService = configurationService;
    this.referenceGenomeService = referenceGenomeService;
    this.flyway = flyway;
    this.minervaJobService = minervaJobService;
    this.configurationHolder = configurationHolder;
    this.dbMaintenanceBean = dbMaintenanceBean;
  }

  @PostConstruct
  public void init() {
    loadCustomLog4jProperties();
    try {
      flyway.populate(flyway.getDataSource().getConnection());
    } catch (ScriptException | SQLException e) {
      logger.error(e, e);
    }
    logger.debug("Application startup script starts");
    setJarPath();
    setInterruptedProjectsStatuses();
    setInterruptedMinervaJobStatuses();
    modifyXFrameDomain();
    modifyCorsDomain();
    setSessionLength();
    disableAssistiveTechnologies();
    dbMaintenanceBean.removeOldJobsCronJobExpression();
    removeInterruptedReferenceGenomeDownloads();
    provideFrontendUrls();
    logger.debug("Application startup script ends");
  }

  private void setJarPath() {
    try {
      String path = (this.getClass().getProtectionDomain().getCodeSource().getLocation()
          .toURI() + "").replace("jar:", "").replace("file:", "").split("!")[0];
      Configuration.setJarPath(path);
    } catch (Exception e) {
      logger.error("Problem with setting jar path.", e);
    }
  }

  private void setInterruptedMinervaJobStatuses() {
    minervaJobService.setInterruptedOnRunningJobs();
    minervaJobService.enableQueue();
  }

  private void loadCustomLog4jProperties() {
    String filename = "/etc/minerva/log4j2.properties";
    File file = new File(filename);
    if (file.exists()) {
      try {
        LoggerContext context = (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
        context.setConfigLocation(file.toURI());
        logger.info("log4j  configuration loaded from: {}", filename);
      } catch (final Exception e) {
        logger.error("Problem with loading log4j configuration: {}", filename);
      }
    }

  }

  private void setSessionLength() {
    try {
      String sessionLength = configurationService.getConfigurationValue(ConfigurationElementType.SESSION_LENGTH);
      int value = Integer.parseInt(sessionLength);
      Configuration.setSessionLength(value);
    } catch (final Exception e) {
      logger.error("Problem with setting default session length.", e);
    }
  }

  private void modifyXFrameDomain() {
    try {
      for (final String domain : configurationService.getConfigurationValue(ConfigurationElementType.X_FRAME_DOMAIN)
          .split(";")) {
        Configuration.getxFrameDomain().add(domain);
      }
    } catch (final Exception e) {
      logger.error("Problem with modifying x frame domain...", e);
    }
  }

  private void modifyCorsDomain() {
    try {
      Configuration.setDisableCors(
          configurationService.getConfigurationValue(ConfigurationElementType.CORS_DOMAIN).equalsIgnoreCase("true"));
    } catch (final Exception e) {
      logger.error("Problem with modifying cors...", e);
    }
  }

  /**
   * Removes downloads of reference genomes that were interrupted by tomcat
   * restart.
   */
  private void removeInterruptedReferenceGenomeDownloads() {
    try {
      for (final ReferenceGenome genome : referenceGenomeService.getDownloadedGenomes()) {
        if (genome.getDownloadProgress() < IProgressUpdater.MAX_PROGRESS) {
          logger.warn("Removing genome that was interrupted: {}", genome);
          try {
            referenceGenomeService.removeGenome(genome);
          } catch (final IOException e) {
            logger.error("Problem with removing genome: {}", genome);
          }
        }
      }
    } catch (final Exception e) {
      logger.error("Problem with removing interrupted downloads...", e);
    }

  }

  /**
   * Set {@link ProjectStatus#FAIL} statuses to projects that were uploading when
   * application was shutdown.
   */
  private void setInterruptedProjectsStatuses() {
    try {
      for (Project project : projectService.getAllProjects()) {
        if (!ProjectStatus.DONE.equals(project.getStatus())
            && !ProjectStatus.ARCHIVED.equals(project.getStatus())
            && !ProjectStatus.FAIL.equals(project.getStatus())
            && !ProjectStatus.UNKNOWN.equals(project.getStatus())) {
          project = projectService.getProjectByProjectId(project.getProjectId(), true);
          ProjectLogEntry entry = new ProjectLogEntry();
          entry.setSeverity("ERROR");
          entry.setType(ProjectLogEntryType.OTHER);
          entry.setContent(
              "Project uploading was interrupted by application restart (at the stage: " + project.getStatus() + ").");
          project.setStatus(ProjectStatus.FAIL);
          project.addLogEntry(entry);
          projectService.update(project);
          logger.info("Status of project: {} changed to fail.", project.getProjectId());
        }
      }
    } catch (final Exception e) {
      logger.error("Problem with changing project status ...", e);
    }
  }

  private void disableAssistiveTechnologies() {
    Properties props = System.getProperties();
    props.setProperty("javax.accessibility.assistive_technologies", "");
  }

  private void provideFrontendUrls() {
    try {
      String frontZip = new File(Configuration.getJarPath()).getParent() + "/front.zip";
      if (new File(frontZip).exists()) {
        String unzipFolder = configurationHolder.getDataRootPath() + "/";
        if (new File(unzipFolder + "front/").exists()) {
          FileUtils.deleteDirectory(new File(unzipFolder + "front/"));
        }

        Path targetDir = Paths.get(unzipFolder);
        try (ZipInputStream zipIn = new ZipInputStream(new FileInputStream(new File(frontZip)))) {
          for (ZipEntry ze; (ze = zipIn.getNextEntry()) != null; ) {
            Path resolvedPath = targetDir.resolve(ze.getName()).normalize();
            if (ze.isDirectory()) {
              Files.createDirectories(resolvedPath);
            } else {
              Files.createDirectories(resolvedPath.getParent());
              Files.copy(zipIn, resolvedPath);
            }
          }
        }
        new File(unzipFolder + "/config.js").delete();
      } else {
        logger.warn("Cannot find front.zip file: {}", frontZip);
      }
    } catch (Exception e) {
      logger.error("Problem with extracting front.zip", e);
    }
  }

}
