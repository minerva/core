package lcsb.mapviewer.web.bean.utils;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class should be used only for debugging. It's a wrapper to standard JSF
 * {@link HttpServletResponse} which logs all activity in the servlet response.
 * To use it properly one can create a {@link javax.servlet.Filter Filter} that
 * will only encapsulate response with this class and ass it via
 * {@link javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)}
 * method.
 * 
 * @author Piotr Gawron
 *
 */
public class CustomHttpServletResponseWrapper extends HttpServletResponseWrapper {

  /**
   * Default class logger.
   */
  private Logger logger = LogManager.getLogger();

  /**
   * Default constructor.
   * 
   * @param response
   *          {@link HttpServletResponse} that should be encapsluated
   */
  public CustomHttpServletResponseWrapper(final HttpServletResponse response) {
    super(response);
    logger.debug("Constructor: " + response);
  }

  @Override
  public void addCookie(final Cookie cookie) {
    logger.debug("addCookie");
    this.addCookie(cookie);
  }

  @Override
  public PrintWriter getWriter() throws IOException {
    logger.debug("getWriter");
    return this.getWriter();
  }

  @Override
  public boolean containsHeader(final String name) {
    logger.debug("containsHeader");
    return this.containsHeader(name);
  }

  @Override
  public ServletOutputStream getOutputStream() throws IOException {
    logger.debug("getOutputStream");
    logger.debug(new Exception(), new Exception());
    return this.getOutputStream();
  }

  @Override
  public String encodeURL(final String url) {
    logger.debug("encodeURL");
    return this.encodeURL(url);
  }

  @Override
  public String getCharacterEncoding() {
    logger.debug("getCharacterEncoding");
    return this.getCharacterEncoding();
  }

  @Override
  public String encodeRedirectURL(final String url) {
    logger.debug("encodeRedirectURL");
    return this.encodeRedirectURL(url);
  }

  @Override
  public String getContentType() {
    logger.debug("getContentType");
    return this.getContentType();
  }

  @Override
  public String encodeUrl(final String url) {
    logger.debug("encodeUrl");
    return this.encodeUrl(url);
  }

  @Override
  public void setCharacterEncoding(final String charset) {
    logger.debug("setCharacterEncoding");
    this.setCharacterEncoding(charset);
  }

  @Override
  public String encodeRedirectUrl(final String url) {
    logger.debug("encodeRedirectUrl");
    return this.encodeRedirectUrl(url);
  }

  @Override
  public void setContentLength(final int len) {
    logger.debug("setContentLength");
    this.setContentLength(len);
  }

  @Override
  public void sendError(final int sc, final String msg) throws IOException {
    logger.debug("sendError");
    this.sendError(sc);
  }

  @Override
  public void sendError(final int sc) throws IOException {
    logger.debug("sendError");
    this.sendError(sc);
  }

  @Override
  public void setContentType(final String type) {
    logger.debug("setContentType: " + type);
    logger.debug(new Exception(), new Exception());
    this.setContentType(type);
  }

  @Override
  public void setBufferSize(final int size) {
    logger.debug("setBufferSize");
    this.setBufferSize(size);
  }

  @Override
  public void sendRedirect(final String location) throws IOException {
    logger.debug("sendRedirect");
    this.sendRedirect(location);

  }

  @Override
  public int getBufferSize() {
    logger.debug("getBufferSize");
    return this.getBufferSize();
  }

  @Override
  public void setDateHeader(final String name, final long date) {
    logger.debug("setDateHeader");
    this.setDateHeader(name, date);

  }

  @Override
  public void flushBuffer() throws IOException {
    logger.debug("flushBuffer");
    this.flushBuffer();
  }

  @Override
  public void addDateHeader(final String name, final long date) {
    logger.debug("addDateHeader");
    this.addDateHeader(name, date);

  }

  @Override
  public void resetBuffer() {
    logger.debug("resetBuffer");
    this.resetBuffer();
  }

  @Override
  public void setHeader(final String name, final String value) {
    logger.debug("setHeader");
    this.setHeader(name, value);

  }

  @Override
  public boolean isCommitted() {
    logger.debug("isCommitted");
    return this.isCommitted();
  }

  @Override
  public void addHeader(final String name, final String value) {
    logger.debug("addHeader: " + name + " - " + value);
    this.addHeader(name, value);

  }

  @Override
  public void reset() {
    logger.debug("reset");
    this.reset();
  }

  @Override
  public void setIntHeader(final String name, final int value) {
    logger.debug("setIntHeader");
    this.setIntHeader(name, value);

  }

  @Override
  public void setLocale(final Locale loc) {
    logger.debug("setLocale");
    this.setLocale(loc);
  }

  @Override
  public void addIntHeader(final String name, final int value) {
    logger.debug("addIntHeader");
    this.addIntHeader(name, value);

  }

  @Override
  public Locale getLocale() {
    logger.debug("getLocale");
    return this.getLocale();
  }

  @Override
  public void setStatus(final int sc) {
    logger.debug("setStatus: " + sc);
    this.setStatus(sc);

  }

  @Override
  public void setStatus(final int sc, final String sm) {
    logger.debug("setStatus");
    this.setStatus(sc, sm);

  }

}
