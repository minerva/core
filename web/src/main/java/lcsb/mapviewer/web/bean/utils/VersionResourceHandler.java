package lcsb.mapviewer.web.bean.utils;

import java.io.File;
import java.io.IOException;

import javax.faces.application.Resource;
import javax.faces.application.ResourceHandler;
import javax.faces.application.ResourceHandlerWrapper;
import javax.faces.application.ResourceWrapper;
import javax.faces.context.FacesContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lcsb.mapviewer.services.impl.ConfigurationService;

/**
 * This class is used to provide url for jsf resource files with "?v=version"
 * addition that will make sure that when we update files from git repository
 * browser will reload them.
 * 
 * @author Piotr Gawron
 *
 */
public class VersionResourceHandler extends ResourceHandlerWrapper {
  /**
   * GIT version of the system.
   */
  private static String version = null;
  /**
   * Default class logger.
   */
  private final Logger logger = LogManager.getLogger();
  /**
   * Resource for which we generate modified url.
   */
  private ResourceHandler wrapped;

  /**
   * Default constructor.
   * 
   * @param wrapped
   *          {@link #wrapped}
   */
  public VersionResourceHandler(final ResourceHandler wrapped) {
    this.wrapped = wrapped;
  }

  @Override
  public ResourceHandler getWrapped() {
    return wrapped;
  }

  @Override
  public Resource createResource(final String resourceName) {
    return createResource(resourceName, null, null);
  }

  @Override
  public Resource createResource(final String resourceName, final String libraryName) {
    return createResource(resourceName, libraryName, null);
  }

  @Override
  public Resource createResource(final String resourceName, final String libraryName, final String contentType) {
    final Resource resource = super.createResource(resourceName, libraryName, contentType);

    if (resource == null) {
      return null;
    }

    return new ResourceWrapper() {

      @Override
      public Resource getWrapped() {
        return resource;
      }

      @Override
      public String getRequestPath() {
        return super.getRequestPath() + "&m_version=" + getVersion();
      }

      @Override
      public String getContentType() {
        return getWrapped().getContentType();
      }

      @Override
      public String getLibraryName() {
        return getWrapped().getLibraryName();
      }

      @Override
      public String getResourceName() {
        return getWrapped().getResourceName();
      }
    };
  }

  /**
   * This method returns git version of the framework.
   * 
   * @return git version of the framework
   */
  private String getVersion() {
    if (version == null) {
      try {
        version = new ConfigurationService(null, null).getSystemGitVersion();
      } catch (final Exception e) {
        logger.error(e, e);
        version = "UNKNOWN";
      }
    }
    return version;
  }

  private String getPath() {
    FacesContext context = FacesContext.getCurrentInstance();
    if (context != null) {
      return context.getExternalContext().getRealPath("/");
    } else {
      String path;
      try {
        path = new File(".").getCanonicalPath() + "/";
      } catch (final IOException e) {
        logger.warn("Problem with detecting current path", e);
        path = "";
      }
      return path;
    }
  }

}