package lcsb.mapviewer.web.app;

import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class FrontendController {

  @SuppressWarnings("unused")
  private final Logger logger = LogManager.getLogger();

  @Autowired
  private IConfigurationService configurationService;

  @GetMapping({
      "/minerva",
      "/minerva/",
      "/minerva/index.xhtml",
  })
  public RedirectView index(final @RequestParam(value = "id", required = false) String id,
                            final @RequestParam(value = "x", required = false) String x,
                            final @RequestParam(value = "y", required = false) String y,
                            final @RequestParam(value = "zoom", required = false) String z,
                            final @RequestParam(value = "submap", required = false) String modelId,
                            final @RequestParam(value = "oauthLogin", required = false) String oauthLogin

  ) {
    boolean enforceOldInterface = "true".equalsIgnoreCase(configurationService.getConfigurationValue(ConfigurationElementType.ENFORCE_OLD_INTERFACE));

    String url = "/minerva/index.html?";
    if (enforceOldInterface) {
      url = "/minerva/old_index.xhtml?";
    }
    if (x != null) {
      url += "x=" + x + "&";
    }
    if (y != null) {
      url += "y=" + y + "&";
    }
    if (z != null) {
      if (enforceOldInterface) {
        url += "zoom=" + z + "&";
      } else {
        url += "z=" + z + "&";
      }
    }
    if (modelId != null) {
      if (enforceOldInterface) {
        url += "submap=" + modelId + "&";
      } else {
        url += "modelId=" + modelId + "&";
      }
    }
    if (id != null && !id.isEmpty()) {
      url += "id=" + id + "&";
    }
    if (oauthLogin != null && !oauthLogin.isEmpty()) {
      url += "oauthLogin=" + oauthLogin + "&";
    }
    return new RedirectView(url);
  }

  @GetMapping({
      "/minerva/old_index.xhtml",
  })
  public String old_index(final Model model) {
    model.addAttribute("version", configurationService.getSystemGitVersion());
    return "old_index";
  }

  @GetMapping({
      "/minerva/admin",
      "/minerva/admin.xhtml",
  })
  public String admin(final Model model) {
    model.addAttribute("version", configurationService.getSystemGitVersion());
    return "admin";
  }

  @GetMapping({
      "/minerva/login",
      "/minerva/login.xhtml",
  })
  public String login(final Model model) {
    model.addAttribute("version", configurationService.getSystemGitVersion());
    return "login";
  }

  @GetMapping({
      "/minerva/export",
      "/minerva/export.xhtml",
  })
  public String export(final Model model) {
    model.addAttribute("version", configurationService.getSystemGitVersion());
    return "export";
  }

  @GetMapping({
      "/minerva/default-cookie-policy",
      "/minerva/default-cookie-policy.xhtml",
      "/minerva/index.html/minerva/default-cookie-policy.xhtml",
      "/minerva/index.html/default-cookie-policy.xhtml",
  })
  public String defaultCookiePolicy(final Model model) {
    model.addAttribute("version", configurationService.getSystemGitVersion());
    return "default-cookie-policy";
  }

  @RequestMapping("/")
  public void mainIndex(final HttpServletResponse response) throws IOException {
    response.sendRedirect("/minerva/");
  }

  @RequestMapping("/minerva/docs/")
  public void docs(final HttpServletResponse response) throws IOException {
    response.sendRedirect("/minerva/docs/index.html");
  }

  @GetMapping(
      value = {
          "/minerva/config.js",
      },
      produces = "application/javascript")
  @ResponseBody
  public String updateFrontendConfig() throws IOException {
    return "window.config = {\n"
        + "  ADMIN_PANEL_URL: '/minerva/admin.xhtml',\n"
        + "  BASE_API_URL: '/minerva/api',\n"
        + "  BASE_NEW_API_URL: '/minerva/new_api/',\n"
        + "  BASE_MAP_IMAGES_URL: '',\n"
        + "  DEFAULT_PROJECT_ID: '" + configurationService.getConfigurationValue(ConfigurationElementType.DEFAULT_MAP) + "',\n"
        + "};\n";
  }

}
