package lcsb.mapviewer.web.app;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import lcsb.mapviewer.api.CustomPage;
import lcsb.mapviewer.api.CustomPageDeserialize;
import lcsb.mapviewer.api.CustomPageSerializer;
import lcsb.mapviewer.common.MinervaConfigurationHolder;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.model.map.OverviewImageLink;
import lcsb.mapviewer.model.map.OverviewModelLink;
import lcsb.mapviewer.model.map.OverviewSearchLink;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.modelutils.map.ClassTreeNode;
import lcsb.mapviewer.modelutils.serializer.CalendarSerializer;
import lcsb.mapviewer.modelutils.serializer.ClassTreeNodeSerializer;
import lcsb.mapviewer.modelutils.serializer.ColorSerializer;
import lcsb.mapviewer.modelutils.serializer.Line2DDeserializer;
import lcsb.mapviewer.modelutils.serializer.Line2DSerializer;
import lcsb.mapviewer.modelutils.serializer.Point2DDeserializer;
import lcsb.mapviewer.modelutils.serializer.model.map.CommentSerializer;
import lcsb.mapviewer.modelutils.serializer.model.map.MinifiedBioEntitySerializer;
import lcsb.mapviewer.modelutils.serializer.model.map.OverviewImageLinkSerializer;
import lcsb.mapviewer.modelutils.serializer.model.map.OverviewImageSerializer;
import lcsb.mapviewer.modelutils.serializer.model.map.OverviewModelLinkSerializer;
import lcsb.mapviewer.modelutils.serializer.model.map.OverviewSearchLinkSerializer;
import lcsb.mapviewer.modelutils.serializer.model.map.reaction.ReactionNodeSerializer;
import lcsb.mapviewer.services.websockets.IWebSocketMessenger;
import lcsb.mapviewer.web.api.serializer.ColorDeserializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.data.web.config.PageableHandlerMethodArgumentResolverCustomizer;
import org.springframework.http.CacheControl;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.reactive.socket.server.WebSocketService;
import org.springframework.web.reactive.socket.server.support.HandshakeWebSocketService;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;
import org.springframework.web.reactive.socket.server.upgrade.TomcatRequestUpgradeStrategy;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import java.awt.Color;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableSpringDataWebSupport
@EnableScheduling
@EnableWebSocket
@ComponentScan(basePackages = {"lcsb.mapviewer.api"})
public class WebConfig implements WebMvcConfigurer, WebSocketConfigurer {

  private final Logger logger = LogManager.getLogger();
  private final List<HandlerInterceptor> interceptors;

  @Autowired
  private MinervaConfigurationHolder configurationHolder;

  @Autowired
  private IWebSocketMessenger webSocketMessenger;

  @Autowired
  public WebConfig(final List<HandlerInterceptor> interceptors) {
    this.interceptors = interceptors;
  }

  @Bean
  public ViewResolver viewResolver() {
    final InternalResourceViewResolver resolver = new InternalResourceViewResolver();
    resolver.setPrefix("/");
    resolver.setSuffix(".html");
    return resolver;
  }

  @Override
  public void addInterceptors(final InterceptorRegistry registry) {
    interceptors.forEach(registry::addInterceptor);
  }

  @Override
  public void addResourceHandlers(final ResourceHandlerRegistry registry) {
    final String path = configurationHolder.getDataRootPath();
    final CacheControl cacheControl = CacheControl.maxAge(31, TimeUnit.DAYS);
    registry.addResourceHandler("/**")
        .addResourceLocations("/", "classpath:/")
        .setCacheControl(cacheControl);

    registry.addResourceHandler("/minerva/**")
        .addResourceLocations("/", "classpath:/", "file:" + path + "/front/")
        .setCacheControl(cacheControl);

    registry.addResourceHandler("/minerva/*.html")
        .addResourceLocations("file:" + path + "/front/")
        .setCacheControl(CacheControl.noCache());

    registry.addResourceHandler("/map_images/**")
        .addResourceLocations("file:" + path + "/map_images/")
        .setCacheControl(cacheControl);

    registry.addResourceHandler("/minerva-big/**")
        .addResourceLocations("file:" + path + "/minerva-big/")
        .setCacheControl(cacheControl);
  }

  @Override
  public void addCorsMappings(final CorsRegistry registry) {
    registry.addMapping("/**").allowedMethods("*");
  }

  @Override
  public void extendMessageConverters(final List<HttpMessageConverter<?>> converters) {
    for (final HttpMessageConverter<?> converter : converters) {
      if (converter instanceof MappingJackson2HttpMessageConverter) {
        final MappingJackson2HttpMessageConverter m = (MappingJackson2HttpMessageConverter) converter;
        final SimpleModule module = new SimpleModule();
        module.addSerializer(Calendar.class, new CalendarSerializer());
        module.addSerializer(Color.class, new ColorSerializer());
        module.addSerializer(Line2D.class, new Line2DSerializer());
        module.addSerializer(ClassTreeNode.class, new ClassTreeNodeSerializer());
        module.addSerializer(ReactionNode.class, new ReactionNodeSerializer());
        module.addSerializer(BioEntity.class, new MinifiedBioEntitySerializer());
        module.addSerializer(CustomPage.class, new CustomPageSerializer());
        module.addSerializer(Comment.class, new CommentSerializer());

        module.addDeserializer(Point2D.class, new Point2DDeserializer());

        module.addDeserializer(Line2D.class, new Line2DDeserializer());
        module.addDeserializer(Color.class, new ColorDeserializer());
        module.addDeserializer(CustomPage.class, new CustomPageDeserialize<>());

        module.addSerializer(OverviewImage.class, new OverviewImageSerializer());
        module.addSerializer(OverviewImageLink.class, new OverviewImageLinkSerializer());
        module.addSerializer(OverviewModelLink.class, new OverviewModelLinkSerializer());
        module.addSerializer(OverviewSearchLink.class, new OverviewSearchLinkSerializer());

        m.getObjectMapper().registerModule(module);
      }
    }
  }

  @Bean
  public ObjectMapper objectMapper() {
    final ObjectMapper mapper = new ObjectMapper();

    final SimpleModule module = new SimpleModule();
    module.addDeserializer(CustomPage.class, new CustomPageDeserialize<>());
    mapper.registerModule(module);

    return mapper;
  }

  @Bean
  PageableHandlerMethodArgumentResolverCustomizer pageableResolverCustomizer() {
    return pageableResolver -> pageableResolver.setMaxPageSize(10000);
  }

  @Override
  public void registerWebSocketHandlers(final WebSocketHandlerRegistry registry) {
    registry.addHandler(webSocketMessenger, "/minerva/api/websocket/entity-updates");
  }

  @Bean
  public WebSocketHandlerAdapter handlerAdapter() {
    return new WebSocketHandlerAdapter(webSocketService());
  }

  @Bean
  public WebSocketService webSocketService() {
    TomcatRequestUpgradeStrategy tomcatRequestUpgradeStrategy = new TomcatRequestUpgradeStrategy();
    tomcatRequestUpgradeStrategy.setMaxSessionIdleTimeout(10000L);
    tomcatRequestUpgradeStrategy.setAsyncSendTimeout(10000L);
    return new HandshakeWebSocketService(tomcatRequestUpgradeStrategy);
  }

}
