package lcsb.mapviewer.web.app;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.services.websockets.IWebSocketMessenger;
import lcsb.mapviewer.services.websockets.messages.incoming.WebSocketIncomingMessage;
import lcsb.mapviewer.services.websockets.messages.incoming.WebSocketIncomingMessageLogin;
import lcsb.mapviewer.services.websockets.messages.incoming.WebSocketIncomingMessageRegisterProject;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessage;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageAccessDeniedInput;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageAck;
import lcsb.mapviewer.services.websockets.messages.outgoing.WebSocketMessageInvalidInput;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public class WebSocketMessenger extends TextWebSocketHandler implements IWebSocketMessenger {

  private final Logger logger = LogManager.getLogger();

  private final ObjectMapper objectMapper = new ObjectMapper();

  private final List<WebSocketSession> sessions = new ArrayList<>();
  private final Map<WebSocketSession, String> usernameForSession = new HashMap<>();

  private final Map<String, Set<WebSocketSession>> sessionsForProjects = new HashMap<>();

  @Autowired
  private SessionRegistry sessionRegistry;

  @Autowired
  private UserDetailsService userDetailsService;

  @Override
  public void handleTextMessage(final WebSocketSession session, final TextMessage message) {
    String payload = message.getPayload();
    logger.trace("Received message via websockets: {}", payload);
    try {
      WebSocketIncomingMessage incomingMessage = objectMapper.readValue(payload, WebSocketIncomingMessage.class);
      List<String> errors = validate(incomingMessage);
      if (!errors.isEmpty()) {
        logger.debug("Problem with parsing json message: {}. Payload: {}", errors, payload);
        sendMessage(session, new WebSocketMessageInvalidInput("Problem with the data. " + errors));
      } else if (incomingMessage instanceof WebSocketIncomingMessageLogin) {
        String token = ((WebSocketIncomingMessageLogin) incomingMessage).getToken();
        SessionInformation information = sessionRegistry.getSessionInformation(token);
        if (information != null) {
          Object principal = information.getPrincipal();
          if (principal instanceof UserDetails) {
            usernameForSession.put(session, ((UserDetails) principal).getUsername());
            sendMessage(session, new WebSocketMessageAck("ok", incomingMessage));
          } else {
            sendMessage(session, new WebSocketMessageInvalidInput("Internal server error."));
          }
        } else {
          sendMessage(session, new WebSocketMessageAccessDeniedInput("Invalid credentials."));
        }

      } else if (incomingMessage instanceof WebSocketIncomingMessageRegisterProject) {
        String projectId = ((WebSocketIncomingMessageRegisterProject) incomingMessage).getProjectId();
        if (hasAnyAuthority(session, new String[]{"IS_ADMIN", "READ_PROJECT:" + projectId})) {
          addSessionForProject(session, ((WebSocketIncomingMessageRegisterProject) incomingMessage).getProjectId());
          sendMessage(session, new WebSocketMessageAck("ok", incomingMessage));
        } else {
          sendMessage(session, new WebSocketMessageAccessDeniedInput("Access denied."));
        }
      } else {
        logger.debug("Problem with parsing json message. Payload: {}", payload);
        sendMessage(session, new WebSocketMessageInvalidInput("Unknown message type."));
      }
    } catch (JsonProcessingException e) {
      logger.debug("Problem with parsing json message. Payload: {}", payload, e);
      sendMessage(session, new WebSocketMessageInvalidInput("Problem with parsing json message. " + e.getMessage()));
    }
  }

  private boolean hasAnyAuthority(final WebSocketSession session, final String[] authorities) {
    String username = usernameForSession.get(session);
    if (username == null) {
      logger.trace("No username found for session: {}", session.getId());
      return false;
    }
    try {
      UserDetails userDetails = userDetailsService.loadUserByUsername(usernameForSession.get(session));
      if (userDetails == null) {
        logger.trace("No user details found for session: {}", session.getId());
        return false;
      }
      for (String string : authorities) {
        if (userDetails.getAuthorities().contains(new SimpleGrantedAuthority(string))) {
          return true;
        }
      }
      return false;
    } catch (Exception e) {
      logger.error(e);
      return false;
    }
  }

  private List<String> validate(final WebSocketIncomingMessage object) {
    List<String> errors = new ArrayList<>();

    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator();
    for (final ConstraintViolation<WebSocketIncomingMessage> violation : validator.validate(object)) {
      errors.add(violation.getPropertyPath() + ": " + violation.getMessage());
    }
    return errors;
  }

  private void addSessionForProject(final WebSocketSession session, final String projectId) {
    sessionsForProjects.computeIfAbsent(projectId, k -> new HashSet<>());
    sessionsForProjects.get(projectId).add(session);
  }


  synchronized void addSession(final WebSocketSession sess) {
    this.sessions.add(sess);
  }

  @Override
  public void afterConnectionEstablished(final WebSocketSession session) {
    logger.trace("Websocket Session established: {}", session.getId());
    addSession(session);
  }

  public void afterConnectionClosed(final WebSocketSession session, final CloseStatus status) {
    logger.trace("Websocket Session closed: {}", session.getId());
    logger.trace(status.getReason());
    removeSession(session);
  }

  private void removeSession(final WebSocketSession session) {
    sessions.remove(session);
    usernameForSession.remove(session);
    for (Set<WebSocketSession> sessionsForProject : sessionsForProjects.values()) {
      sessionsForProject.remove(session);
    }
  }

  public void broadcast(final WebSocketMessage message) {
    for (WebSocketSession session : sessions) {
      sendMessage(session, message);
    }
  }

  @Override
  public void broadcast(final String projectId, final WebSocketMessage message) {
    logger.trace("Websocket broadcasting message about project {}: {}", projectId, message);
    Set<WebSocketSession> sessionsForProject = sessionsForProjects.get(projectId);
    if (sessionsForProject != null) {
      for (WebSocketSession session : sessionsForProject) {
        sendMessage(session, message);
      }
    }
  }

  private void sendMessage(final WebSocketSession session, final WebSocketMessage myMessage) {
    try {
      String json = objectMapper.writeValueAsString(myMessage);
      TextMessage msg = new TextMessage(json);
      session.sendMessage(msg);
      logger.trace("Message to client {} sent: {}", session.getId(), json);
    } catch (IOException e) {
      logger.error("Problem sending message: {}", session.getId(), e);
    } catch (Throwable e) {
      logger.fatal("Problem sending message: {}", session.getId(), e);
    }
  }
}