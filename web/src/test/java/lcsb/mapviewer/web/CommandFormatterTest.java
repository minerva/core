package lcsb.mapviewer.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import lcsb.mapviewer.web.utils.CommandFormatterWithReplacingPostFilenameHeader;

@RunWith(SpringJUnit4ClassRunner.class)
public class CommandFormatterTest extends ControllerIntegrationTest {

  @Test
  public void testExtractDefaultContentTypeContentType() throws Exception {
    CommandFormatterWithReplacingPostFilenameHeader commandFormatter = new CommandFormatterWithReplacingPostFilenameHeader(" ");
    String contentType = commandFormatter.getContentType(Arrays.asList(" "));
    assertEquals(MediaType.APPLICATION_OCTET_STREAM_VALUE, contentType);
  }

  @Test
  public void testExtractCustomContentType() throws Exception {
    CommandFormatterWithReplacingPostFilenameHeader commandFormatter = new CommandFormatterWithReplacingPostFilenameHeader(" ");
    String contentType = commandFormatter.getContentType(Arrays.asList(" ", "-H \"Content-Type: text/plain\""));
    assertEquals("text/plain", contentType);
  }

  @Test
  public void testExtractCustomContentTypeSingleQuote() throws Exception {
    CommandFormatterWithReplacingPostFilenameHeader commandFormatter = new CommandFormatterWithReplacingPostFilenameHeader(" ");
    String contentType = commandFormatter.getContentType(Arrays.asList(" ", "-H 'Content-Type: text/plain'"));
    assertEquals("text/plain", contentType);
  }

  @Test
  public void testExtractCustomContentTypeWithCharset() throws Exception {
    CommandFormatterWithReplacingPostFilenameHeader commandFormatter = new CommandFormatterWithReplacingPostFilenameHeader(" ");
    String contentType = commandFormatter.getContentType(Arrays.asList(" ", "-H 'Content-Type: text/html; charset=utf-8'"));
    assertTrue(contentType.contains("charset=utf-8"));
  }

}
