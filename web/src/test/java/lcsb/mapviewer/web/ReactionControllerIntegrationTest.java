package lcsb.mapviewer.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import com.fasterxml.jackson.core.type.TypeReference;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IUserService;

@RunWith(SpringJUnit4ClassRunner.class)
public class ReactionControllerIntegrationTest extends ControllerIntegrationTest {

  @Autowired
  private IUserService userService;

  private User anonymous;

  private Project project;

  @Before
  public void setup() {
    project = createAndPersistProject(TEST_PROJECT);
    anonymous = userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN);
  }

  @After
  public void tearDown() throws Exception {
    removeProject(project);
  }

  @Test
  public void testGetAllReactions() throws Exception {
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/*/bioEntities/reactions/", TEST_PROJECT);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertTrue("user should be able to access elements", result.size() > 0);
  }

  @Test
  public void testGetReactionsWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/*/models/*/bioEntities/reactions/")
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals(0, result.size());
  }
}
