package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("webCtdTestProfile")
public class ChemicalControllerIntegrationTest extends ControllerIntegrationTest {

  private static final String TEST_PROJECT = "TEST_PROJECT";
  private static final String TEST_PROJECT2 = "TEST_PROJECT2";

  @Autowired
  private IProjectService projectService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private ObjectMapper objectMapper;

  private Project project;

  @Before
  public void setup() {
    assumeTrue("DAPI credentials are not provided",
        isDapiConfigurationAvailable());
    project = createAndPersistProject(TEST_PROJECT);
  }

  @After
  public void tearDown() throws Exception {
    removeProject(TEST_PROJECT);
    removeProject(TEST_PROJECT2);
  }

  @Test
  public void testDocsSearchChemicalsInProjectUrl() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/chemicals:search?query=stilbene oxide", TEST_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_chemicals/search_by_query",
            getProjectPathParameters(),
            getChemicalFilter(),
            listOfChemicalsFields()))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testSearchChemicalsWithColumns() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/chemicals:search?query=stilbene oxide&columns=name", TEST_PROJECT)
        .session(session);

    final String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    final List<Map<String, Object>> list = objectMapper.readValue(content, new TypeReference<List<Map<String, Object>>>() {
    });
    assertTrue(list.get(0).containsKey("name"));
    assertFalse(list.get(0).containsKey("id"));
  }

  @Test
  public void testDocsGetSuggestedList() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final MiriamData bloodLossDisease = new MiriamData(MiriamType.MESH_2012, "D016063");

    final Project project = createEmptyProject(TEST_PROJECT2);
    project.setDisease(bloodLossDisease);
    projectService.update(project);

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/chemicals/suggestedQueryList", TEST_PROJECT2)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_chemicals/suggested_query_list",
            getProjectPathParameters(),
            responseFields(
                fieldWithPath("[]")
                    .description("list of suggested chemical queries")
                    .type("array<string>"))))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
  }

  private ResponseFieldsSnippet listOfChemicalsFields() {
    return responseFields(
        subsectionWithPath("[].id")
            .description("identifier of the chemical")
            .type(JsonFieldType.OBJECT),
        fieldWithPath("[].name")
            .description("name")
            .type(JsonFieldType.STRING),
        fieldWithPath("[].description")
            .description("description")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath("[].synonyms")
            .description("list of synonyms")
            .type("array<string>"),
        subsectionWithPath("[].references")
            .description("list of references")
            .type("array<Reference>"),
        fieldWithPath("[].targets")
            .description("list of targets")
            .type("array<Target>"),
        fieldWithPath("[].targets[].name")
            .description("target name")
            .type(JsonFieldType.STRING),
        subsectionWithPath("[].targets[].references")
            .description("list of target references")
            .type("array<Reference>"),
        subsectionWithPath("[].targets[].targetElements")
            .description("list of elements on the map associated with this target")
            .type("array<object>"),
        subsectionWithPath("[].targets[].targetParticipants")
            .description("list of identifiers associated with this target")
            .type("array<object>"),
        fieldWithPath("[].directEvidence")
            .description("direct evidence")
            .optional()
            .type(JsonFieldType.STRING),
        subsectionWithPath("[].directEvidenceReferences")
            .description("list of references")
            .type("array<Reference>"));
  }

  @Test
  public void testSearchChemicalsInAsteriskProjectId() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/*/chemicals:search?query=xyz")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testSearchChemicalsByTargetInAsteriskProjectId() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/*/chemicals:search?target=ALIAS:xyz")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDocsSearchChemicalsByTarget() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Element element = null;
    for (final Element e : project.getTopModel().getElements()) {
      if (e instanceof SimpleMolecule) {
        element = e;
      }
    }

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/chemicals:search?target=ALIAS:{aliasId}",
        TEST_PROJECT,
        element.getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_chemicals/search_by_target",
            getProjectPathParameters(),
            getChemicalFilter(),
            responseFields()))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testGetSuggestedListAsAnonymous() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/chemicals/suggestedQueryList", BUILT_IN_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testGetSuggestedListForUndefinedProject() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/*/chemicals/suggestedQueryList")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testSearchChemicalsWithUnknownChemical() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/chemicals:search?query=blablablablah&columns=name", TEST_PROJECT)
        .session(session);

    final String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    final List<Object> list = objectMapper.readValue(content, new TypeReference<List<Object>>() {
    });
    assertEquals(0, list.size());
  }

}
