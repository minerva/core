package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.modelutils.serializer.model.map.CommentSerializer;
import lcsb.mapviewer.modelutils.serializer.model.map.ElementIdentifierType;
import lcsb.mapviewer.services.interfaces.ICommentService;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.restdocs.request.PathParametersSnippet;
import org.springframework.restdocs.request.RequestParametersSnippet;
import org.springframework.restdocs.snippet.Snippet;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class CommentControllerIntegrationTest extends ControllerIntegrationTest {

  private static final String TEST_CURATOR_PASSWORD = "test_curator_pass";
  private static final String TEST_CURATOR_LOGIN = "test_curator";

  private static final String NO_ACCESS_USER_PASSWORD = "no_access_pass";
  private static final String NO_ACCESS_USER_LOGIN = "no_access_user";

  private ModelData map;
  private Reaction reaction;
  private Element element;
  private Project project;

  @Autowired
  private ICommentService commentService;

  @Autowired
  private IUserService userService;

  @Autowired
  private ProjectSnippets snippets;

  private User user;
  private User curator;
  private User noAccessUser;

  @Before
  public void setup() throws Exception {
    project = createAndPersistProject(TEST_PROJECT);
    map = project.getTopModel().getModelData();
    reaction = map.getReactions().iterator().next();
    element = map.getElements().iterator().next();
    user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);
    noAccessUser = createUser(NO_ACCESS_USER_LOGIN, NO_ACCESS_USER_PASSWORD);
    curator = createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD, project);
  }

  @After
  public void tearDown() throws Exception {
    removeProject(project);
    removeUser(user);
    removeUser(noAccessUser);
    removeUser(curator);
  }

  @Test
  public void testDocsAdminSeesAllComments() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createComments();

    RequestBuilder request = get("/minerva/api/projects/{projectId}/comments/models/{mapId}/?columns=author,elementId,content",
        TEST_PROJECT, map.getId())
        .session(session);

    String response = mockMvc.perform(request)
        .andDo(document("projects/project_comments/get_all",
            getMapPathParameters(),
            getCommentsRequestParameters(),
            getCommentsResponseFields()))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals(3, result.size());
  }

  @Test
  public void testListCommentsWithUser() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Comment comment = createComment(map, user);
    commentService.add(comment);

    RequestBuilder request = get("/minerva/api/projects/{projectId}/comments/models/{mapId}/?columns=author,elementId,content",
        TEST_PROJECT, map.getId())
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    List<Object> data = objectMapper.readValue(response, new TypeReference<List<Object>>() {
    });
    assertEquals(1, data.size());
  }

  private ResponseFieldsSnippet getCommentsResponseFields() {
    return responseFields(
        subsectionWithPath("[]")
            .description("list of comments")
            .type("array<Comment>")).andWithPrefix("[].", getCommentResponseFields());
  }

  private List<FieldDescriptor> getCommentResponseFields() {
    return Arrays.asList(
        fieldWithPath("owner")
            .description("login of the user that created a comment")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath("email")
            .description("author email address")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath("content")
            .description("content")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath("title")
            .description("formatted name of the commented element")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath("pinned")
            .description("should the comment be visible to everybody on the map")
            .optional()
            .type("boolean"),
        fieldWithPath("isPinned").ignored().optional(),
        fieldWithPath("removed")
            .description("is the comment removed")
            .optional()
            .type("boolean"),
        fieldWithPath("removeReason")
            .description("reason why comment was removed")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath("id")
            .description("comment identifier")
            .optional()
            .type("integer"),
        fieldWithPath("modelId")
            .description("map identifier")
            .optional()
            .type("integer"),
        subsectionWithPath("coord")
            .description("coordinates where comment should be pinned")
            .optional()
            .type("point"),
        fieldWithPath("icon")
            .description("icon that should be used to visualize this comment")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath("elementId")
            .description("element identifier")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath("type")
            .description("type of the element that was commented on. Available options "
                + snippets.getOptionsAsString(ElementIdentifierType.class))
            .optional()
            .type(JsonFieldType.STRING));
  }

  private RequestParametersSnippet getCommentsRequestParameters() {
    return requestParameters(
        parameterWithName("columns")
            .description("set of columns (all by default). Available options: "
                + StringUtils.join(CommentSerializer.availableColumns(), ", "))
            .optional(),
        parameterWithName("removed")
            .description("true/false/undefined - when defined comment property must match this parameter")
            .optional());
  }

  @Test
  public void testListOfCommentsWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createComments();

    RequestBuilder request = get("/minerva/api/projects/*/comments/models/" + map.getId() + "/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testCuratorSeesAllComments() throws Exception {
    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    createComments();

    RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/")
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals(3, result.size());
  }

  private void createComments() throws Exception {
    Comment comment = createComment(map);
    commentService.add(comment);

    comment = createComment(map);
    comment.setPinned(true);
    commentService.add(comment);

    comment = createComment(map);
    comment.setDeleted(true);
    commentService.add(comment);
  }

  @Test
  public void testUserSeesOnlyPinnedComments() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    createComments();

    RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/")
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals(1, result.size());
  }

  @Test
  public void testUserCannotSeeAuthorDataInComment() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Comment comment = createComment(map);
    comment.setPinned(true);
    commentService.add(comment);

    RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/")
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertFalse("Normal user shouldn't see author email", response.contains("email"));
    assertFalse("Normal user shouldn't see remove reason", response.contains("removeReason"));

  }

  @Test
  public void testAdminCanSeeAuthorDataInComment() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Comment comment = createComment(map);
    comment.setPinned(true);
    commentService.add(comment);

    RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/")
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertTrue("Admin should see author email", response.contains("email"));
    assertTrue("Admin should see remove reason", response.contains("removeReason"));

  }

  @Test
  public void testCuratorCanSeeAuthorDataInComment() throws Exception {
    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    Comment comment = createComment(map);
    comment.setPinned(true);
    commentService.add(comment);

    RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/")
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    assertTrue("Curator should see author email", response.contains("email"));
    assertTrue("Curator should see remove reason", response.contains("removeReason"));

  }

  @Test
  public void testUserWithoutProjectAccessGetsForbidden() throws Exception {
    MockHttpSession session = createSession(NO_ACCESS_USER_LOGIN, NO_ACCESS_USER_PASSWORD);

    createComments();

    RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testDocsRemoveAsAdmin() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Comment comment = createAndPersistComment(map, null);

    RequestBuilder request = delete("/minerva/api/projects/{projectId}/comments/{commentId}/",
        TEST_PROJECT, comment.getId(), comment.getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_comments/delete_comment",
            getProjectPathParameters().and(parameterWithName("commentId").description("comment identifier"))))
        .andExpect(status().is2xxSuccessful());

    comment = commentService.getCommentById(TEST_PROJECT, comment.getId() + "");
    assertTrue(comment.isDeleted());
  }

  @Test
  public void testRemoveAsCurator() throws Exception {
    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    Comment comment = createAndPersistComment(map, null);

    RequestBuilder request = delete("/minerva/api/projects/" + TEST_PROJECT + "/comments/" + comment.getId() + "/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    comment = commentService.getCommentById(TEST_PROJECT, comment.getId() + "");

    assertTrue(comment.isDeleted());
  }

  @Test
  public void testRemoveAsCuratorWithoutAccess() throws Exception {
    userService.revokeUserPrivilege(curator, PrivilegeType.READ_PROJECT, project.getProjectId());
    userService.revokeUserPrivilege(curator, PrivilegeType.WRITE_PROJECT, project.getProjectId());

    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    Comment comment = createAndPersistComment(map, null);

    RequestBuilder request = delete("/minerva/api/projects/" + TEST_PROJECT + "/comments/" + comment.getId() + "/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testRemoveAsOwner() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Comment comment = createAndPersistComment(map, user);

    RequestBuilder request = delete("/minerva/api/projects/" + TEST_PROJECT + "/comments/" + comment.getId() + "/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    comment = commentService.getCommentById(TEST_PROJECT, comment.getId() + "");
    assertTrue(comment.isDeleted());
  }

  @Test
  public void testRemoveAsGuestAccount() throws Exception {
    Comment comment = createAndPersistComment(map, null);

    RequestBuilder request = delete("/minerva/api/projects/" + TEST_PROJECT + "/comments/" + comment.getId() + "/");

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testDocsGetReactionCommentsAsAdmin() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createReactionComment();

    RequestBuilder request = get("/minerva/api/projects/{projectId}/comments/models/{mapId}/bioEntities/reactions/{reactionId}",
        TEST_PROJECT, map.getId(), reaction.getId())
        .session(session);

    String response = mockMvc.perform(request)
        .andDo(document("projects/project_comments/get_reaction",
            getReactionPathParameters(),
            getCommentsRequestParameters(),
            getCommentsResponseFields()))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals(1, result.size());
  }

  @Test
  public void testGetReactionCommentsWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createReactionComment();

    RequestBuilder request = get("/minerva/api/projects/*/comments/models/{mapId}/bioEntities/reactions/{reactionId}",
        map.getId(), reaction.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetReactionCommentsAsCurator() throws Exception {
    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    createReactionComment();

    RequestBuilder request = get("/minerva/api/projects/{projectId}/comments/models/{mapId}/bioEntities/reactions/{reactionId}",
        TEST_PROJECT, map.getId(), reaction.getId())
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals(1, result.size());
  }

  @Test
  public void testGetReactionCommentsWithoutAccess() throws Exception {
    MockHttpSession session = createSession(NO_ACCESS_USER_LOGIN, NO_ACCESS_USER_PASSWORD);

    createReactionComment();

    RequestBuilder request = get("/minerva/api/projects/{projectId}/comments/models/{mapId}/bioEntities/reactions/{reactionId}",
        TEST_PROJECT, map.getId(), reaction.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testDocsGetElementCommentsAsAdmin() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createElementComment();

    RequestBuilder request = get("/minerva/api/projects/{projectId}/comments/models/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, map.getId(), element.getId())
        .session(session);

    String response = mockMvc.perform(request)
        .andDo(document("projects/project_comments/get_element",
            getElementPathParameters(),
            getCommentsRequestParameters(),
            getCommentsResponseFields()))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals(1, result.size());
  }

  @Test
  public void testGetElementCommentsWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createElementComment();

    RequestBuilder request = get("/minerva/api/projects/*/comments/models/{mapId}/bioEntities/elements/{elementId}",
        map.getId(),
        element.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetElementCommentsAsCurator() throws Exception {
    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    createElementComment();

    RequestBuilder request = get("/minerva/api/projects/{projectId}/comments/models/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT,
        map.getId(),
        element.getId())
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals(1, result.size());
  }

  @Test
  public void testGetElementCommentsWithoutAccess() throws Exception {
    MockHttpSession session = createSession(NO_ACCESS_USER_LOGIN, NO_ACCESS_USER_PASSWORD);

    createElementComment();

    RequestBuilder request = get("/minerva/api/projects/{projectId}/comments/models/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT,
        map.getId(),
        element.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testDocsGetPointCommentsAsAdmin() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createAndPersistComment(map, null);

    RequestBuilder request = get("/minerva/api/projects/{projectId}/comments/models/{mapId}/points/{point}",
        TEST_PROJECT, map.getId(), "10.00,20.00")
        .session(session);

    String response = mockMvc.perform(request)
        .andDo(document("projects/project_comments/get_point",
            getPointPathParameters(),
            getCommentsRequestParameters(),
            getCommentsResponseFields()))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals(1, result.size());
  }

  private PathParametersSnippet getPointPathParameters() {
    return getMapPathParameters().and(parameterWithName("point").description("point coordinates"));
  }

  @Test
  public void testGetPointCommentsWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createAndPersistComment(map, null);

    RequestBuilder request = get("/minerva/api/projects/*/comments/models/" + map.getId() + "/points/10.00,20.00")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetPointCommentsAsCurator() throws Exception {
    MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    createAndPersistComment(map, null);

    RequestBuilder request = get("/minerva/api/projects/{projectId}/comments/models/{mapId}/points/{coords}",
        TEST_PROJECT,
        map.getId(),
        "10.00,20.00")
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals(1, result.size());
  }

  @Test
  public void testGetPointCommentsWithoutAccess() throws Exception {
    MockHttpSession session = createSession(NO_ACCESS_USER_LOGIN, NO_ACCESS_USER_PASSWORD);

    createAndPersistComment(map, null);

    RequestBuilder request = get("/minerva/api/projects/{projectId}/comments/models/{mapId}/points/{coords}",
        TEST_PROJECT,
        map.getId(),
        "10.00,20.00")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testDocsAddElementCommentAsUserWithAccess() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "test content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("coordinates", "10,2"),
        new BasicNameValuePair("modelId", map.getId() + ""))));

    RequestBuilder request = post("/minerva/api/projects/{projectId}/comments/models/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, map.getId(), element.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_comments/create_element_comment",
            getElementPathParameters(),
            getCreateCommentRequestParameters(),
            responseFields(getCommentResponseFields())))
        .andExpect(status().is2xxSuccessful());

    List<Comment> comments = commentService.getCommentsByModel(project, map.getId() + "");
    assertEquals(1, comments.size());

    Comment comment = comments.get(0);
    assertEquals("Owner of the comment wasn't set properly", user.getLogin(),
        commentService.getOwnerByCommentId(TEST_PROJECT, comment.getId() + "").getLogin());
  }

  private Snippet getCreateCommentRequestParameters() {
    return requestParameters(
        parameterWithName("modelId")
            .description("map identifier"),
        parameterWithName("content")
            .description("content"),
        parameterWithName("email")
            .description("user email address"),
        parameterWithName("coordinates")
            .description("coordinates where comment should be pinned")
            .optional(),
        parameterWithName("pinned")
            .description("should the comment be visible to everybody"));
  }

  @Test
  public void testAddElementCommentAsAnonymous() throws Exception {
    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "test content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("coordinates", "10,2"),
        new BasicNameValuePair("modelId", map.getId() + ""))));

    userService.grantUserPrivilege(userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN),
        PrivilegeType.READ_PROJECT, TEST_PROJECT);

    RequestBuilder request = post("/minerva/api/projects/{projectId}/comments/models/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT,
        map.getId(),
        element.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    List<Comment> comments = commentService.getCommentsByModel(project, map.getId() + "");
    assertEquals(1, comments.size());

    Comment comment = comments.get(0);
    assertNull("Owner of the comment wasn't set properly", comment.getUser());
  }

  @Test
  public void testAddElementCommentWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "test content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("coordinates", "10,2"),
        new BasicNameValuePair("modelId", map.getId() + ""))));

    RequestBuilder request = post("/minerva/api/projects/*/comments/models/{mapId}/bioEntities/elements/{elementId}",
        map.getId(),
        element.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testAddElementCommentWithoutAllNecessaryData() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Collections.singletonList(
        new BasicNameValuePair("modelId", map.getId() + ""))));

    RequestBuilder request = post("/minerva/api/projects/{projectId}/comments/models/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT,
        map.getId(),
        element.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testAddElementCommentAsAdmin() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "test content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("coordinates", "10,2"),
        new BasicNameValuePair("modelId", map.getId() + ""))));

    RequestBuilder request = post("/minerva/api/projects/{projectId}/comments/models/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT,
        map.getId(),
        element.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    List<Comment> comments = commentService.getCommentsByModel(project, map.getId() + "");
    assertEquals(1, comments.size());
  }

  @Test
  public void testDocsAddReactionCommentAsUserWithAccess() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "test content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("coordinates", "10,2"),
        new BasicNameValuePair("modelId", map.getId() + ""))));

    RequestBuilder request = post(
        "/minerva/api/projects/{projectId}/comments/models/{mapId}/bioEntities/reactions/{reactionId}",
        TEST_PROJECT, map.getId(), reaction.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_comments/create_reaction_comment",
            getReactionPathParameters(),
            getCreateCommentRequestParameters(),
            responseFields(getCommentResponseFields())))
        .andExpect(status().is2xxSuccessful());

    List<Comment> comments = commentService.getCommentsByModel(project, map.getId() + "");
    assertEquals(1, comments.size());
  }

  @Test
  public void testAddReactionCommentAsAdmin() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "test content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("coordinates", "10,2"),
        new BasicNameValuePair("modelId", map.getId() + ""))));

    RequestBuilder request = post(
        "/minerva/api/projects/{projectId}/comments/models/{mapId}/bioEntities/reactions/{reactionId}",
        TEST_PROJECT,
        map.getId(),
        reaction.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    List<Comment> comments = commentService.getCommentsByModel(project, map.getId() + "");
    assertEquals(1, comments.size());
  }

  @Test
  public void testAddReactionCommentWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "test content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("coordinates", "10,2"),
        new BasicNameValuePair("modelId", map.getId() + ""))));

    RequestBuilder request = post("/minerva/api/projects/*/comments/models/{mapId}/bioEntities/reactions/{reactionId}",
        map.getId(), reaction.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDocsAddPointCommentAsUserWithAccess() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "test content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("modelId", map.getId() + ""))));

    RequestBuilder request = post("/minerva/api/projects/{projectId}/comments/models/{mapId}/points/{point}",
        TEST_PROJECT, map.getId(), "10,2")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_comments/create_point_comment",
            getPointPathParameters(),
            getCreateCommentRequestParameters(),
            responseFields(getCommentResponseFields())))
        .andExpect(status().is2xxSuccessful());

    List<Comment> comments = commentService.getCommentsByModel(project, map.getId() + "");
    assertEquals(1, comments.size());
  }

  @Test
  public void testAddPointCommentAsAdmin() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "test content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("modelId", map.getId() + ""))));

    RequestBuilder request = post("/minerva/api/projects/" + TEST_PROJECT + "/comments/models/" + map.getId() + "/points/10,2")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    List<Comment> comments = commentService.getCommentsByModel(project, map.getId() + "");
    assertEquals(1, comments.size());
  }

  @Test
  public void testAddPointCommentWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "test content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("modelId", map.getId() + ""))));

    RequestBuilder request = post("/minerva/api/projects/*/comments/models/" + map.getId() + "/points/10,2")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testCommentsOnNonExistingModel() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createComments();

    RequestBuilder request = get("/minerva/api/projects/{projectId}/comments/models/{modelId}/", TEST_PROJECT, 0)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals(0, result.size());

  }

  @Test
  public void testRemoveFromSubmap() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    ModelData submap = map.getSubmodels().iterator().next().getSubmodel();

    Comment comment = createAndPersistComment(submap, null);

    RequestBuilder request = delete("/minerva/api/projects/" + TEST_PROJECT + "/comments/" + comment.getId() + "/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    comment = commentService.getCommentById(TEST_PROJECT, comment.getId() + "");
    assertTrue(comment.isDeleted());
  }

  private Comment createReactionComment() throws Exception {
    Comment comment = createComment(map);
    comment.setReaction(reaction);
    commentService.add(comment);
    return comment;
  }

  private Comment createElementComment() throws Exception {
    Comment comment = createComment(map);
    comment.setElement(element);
    commentService.add(comment);
    return comment;
  }

  @Test
  public void testInvalidInputWhenCreateComment() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String invalidString = new String(new char[1024]).replace('\0', 'x');

    for (final String type : new String[]{"email"}) {
      String body = createContentBody(type, invalidString);

      RequestBuilder request = post("/minerva/api/projects/" + TEST_PROJECT + "/comments/models/" + map.getId()
          + "/bioEntities/elements/" + element.getId())
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(session);

      mockMvc.perform(request)
          .andExpect(status().isBadRequest());
    }
  }

  private String createContentBody(final String type, final String value) throws IOException {
    List<BasicNameValuePair> params = new ArrayList<>(Arrays.asList(
        new BasicNameValuePair("email", "a@a.lu"),
        new BasicNameValuePair("content", "test content"),
        new BasicNameValuePair("pinned", "true"),
        new BasicNameValuePair("coordinates", "10,2"),
        new BasicNameValuePair("modelId", map.getId() + "")));

    BasicNameValuePair toRemove = null;
    for (final BasicNameValuePair basicNameValuePair : params) {
      if (basicNameValuePair.getName().equals(type)) {
        toRemove = basicNameValuePair;
      }
    }
    params.remove(toRemove);
    params.add(new BasicNameValuePair(type, value));

    return EntityUtils.toString(new UrlEncodedFormEntity(params));
  }

  @Test
  public void testRemoveNonExistingCommentOnNonExistingMap() throws Exception {
    RequestBuilder request = delete("/minerva/api/projects/*/comments/-1/");

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testRemoveCommentAsUserWithAccess() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Comment comment = createAndPersistComment(getBuildInModel(), user);

    RequestBuilder request = delete("/minerva/api/projects/" + BUILT_IN_PROJECT + "/comments/" + comment.getId() + "/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

}
