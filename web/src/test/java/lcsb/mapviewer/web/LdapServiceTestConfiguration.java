package lcsb.mapviewer.web;

import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;

import lcsb.mapviewer.annotation.services.dapi.DapiConnector;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.persist.dao.ConfigurationDao;
import lcsb.mapviewer.services.impl.ConfigurationService;
import lcsb.mapviewer.services.impl.LdapService;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.ILdapService;

@Profile("ldapTest")
@Configuration
public class LdapServiceTestConfiguration {
  protected static final String TEST_LOGIN = "john.doe.test";
  protected static final String TEST_PASSWD = "test_passwd";
  protected static final String LDAP_FILE_CONTENT = "./src/test/resources/ldap/john-doe-test-example.ldif";
  protected static final String TEST_INVALID_PASSWD = "incorrect password";

  @Autowired
  private ConfigurationDao configurationDao;

  @Autowired
  private DapiConnector dapiConnector;

  @Bean
  @Primary
  public ILdapService createMockLdapService() throws LDAPException {
    IConfigurationService configurationServiceMock = Mockito
        .spy(new ConfigurationService(configurationDao, dapiConnector));

    Mockito.doReturn("dc=uni,dc=lu").when(configurationServiceMock)
        .getConfigurationValue(ConfigurationElementType.LDAP_BASE_DN);
    Mockito.doReturn("person").when(configurationServiceMock)
        .getConfigurationValue(ConfigurationElementType.LDAP_OBJECT_CLASS);
    Mockito.doReturn("memberof=cn=gitlab,cn=groups,cn=accounts,dc=uni,dc=lu").when(configurationServiceMock)
        .getConfigurationValue(ConfigurationElementType.LDAP_FILTER);

    LdapService ldapService = Mockito.spy(new LdapService(null));
    ldapService.setConfigurationService(configurationServiceMock);
    Mockito.doAnswer(new Answer<LDAPConnection>() {

      @Override
      public LDAPConnection answer(final InvocationOnMock invocation) throws Throwable {
        // Create the configuration to use for the server.
        InMemoryDirectoryServerConfig config = new InMemoryDirectoryServerConfig("dc=uni,dc=lu");
        config.addAdditionalBindCredentials("uid=" + TEST_LOGIN + ",cn=users,cn=accounts,dc=uni,dc=lu", TEST_PASSWD);
        config.setSchema(null);

        // Create the directory server instance, populate it with data from the
        // "test-data.ldif" file, and start listening for client connections.
        InMemoryDirectoryServer ds = new InMemoryDirectoryServer(config);
        ds.importFromLDIF(true, LDAP_FILE_CONTENT);
        ds.startListening();
        return ds.getConnection();
      }
    }).when(ldapService).getConnection();
    return ldapService;
  }

}
