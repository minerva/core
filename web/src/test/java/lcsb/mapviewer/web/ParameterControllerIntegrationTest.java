package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.ModelData;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class ParameterControllerIntegrationTest extends ControllerIntegrationTest {

  private Project project;
  private ModelData map;

  @Before
  public void setup() {
    project = createAndPersistProject(TEST_PROJECT);
    map = project.getTopModel().getModelData();
  }

  @After
  public void tearDown() throws Exception {
    removeProject(project);
  }

  @Test
  public void testGetParameterListWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/*/models/*/parameters/")
        .session(session);

    String content = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse().getContentAsString();
    List<Map<String, Object>> result = objectMapper.readValue(content, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals(0, result.size());
  }

  @Test
  public void testGetParameterWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/*/models/*/parameters/1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDocsGetParameters() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/parameters/", TEST_PROJECT, "*")
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_maps/get_parameters",
            pathParameters(parameterWithName("projectId").description("project identifier"),
                parameterWithName("mapId").description("map identifier")),
            listOfParameterFields()))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDocsGetParameter() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/parameters/{parameterId}", TEST_PROJECT, "*",
        map.getParameters().iterator().next().getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_maps/get_parameter",
            pathParameters(parameterWithName("projectId").description("project identifier"),
                parameterWithName("mapId").description("map identifier"),
                parameterWithName("parameterId").description("parameter identifier")),
            responseFields(parameterFields())))
        .andExpect(status().is2xxSuccessful());
  }

  private List<FieldDescriptor> parameterFields() {
    return Arrays.asList(
        fieldWithPath("id")
            .description("unique parameter identifier")
            .type("number"),
        fieldWithPath("name")
            .description("name")
            .type(JsonFieldType.STRING),
        fieldWithPath("value")
            .description("value")
            .type("number"),
        fieldWithPath("unitsId")
            .description("unit identifier")
            .type("number"),
        fieldWithPath("global")
            .description("is parameter global")
            .type("boolean"),
        fieldWithPath("parameterId")
            .description("parameter identifier taken from source file")
            .type(JsonFieldType.STRING));
  }

  private ResponseFieldsSnippet listOfParameterFields() {
    return responseFields(
        fieldWithPath("[]").description("list of parameters"))
        .andWithPrefix("[].", parameterFields());
  }

}
