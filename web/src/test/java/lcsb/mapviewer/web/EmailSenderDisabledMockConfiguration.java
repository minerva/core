package lcsb.mapviewer.web;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import lcsb.mapviewer.services.utils.EmailSender;

@Profile("EmailSenderDisabledEmailProfile")
@Configuration
public class EmailSenderDisabledMockConfiguration {

  @Bean
  @Primary
  public EmailSender emailSender() throws Exception {
    EmailSender mock = Mockito.mock(EmailSender.class);

    Mockito.doReturn(false).when(mock).canSendEmails();

    return mock;
  }

}
