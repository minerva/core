package lcsb.mapviewer.web;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import lcsb.mapviewer.services.utils.EmailSender;

@Profile("EmailSenderEnabledEmailProfile")
@Configuration
public class EmailSenderEnabledMockConfiguration {

  @Bean
  @Primary
  public EmailSender emailSender() throws Exception {
    EmailSender mock = Mockito.mock(EmailSender.class);

    Mockito.doReturn(true).when(mock).canSendEmails();

    return mock;
  }

}
