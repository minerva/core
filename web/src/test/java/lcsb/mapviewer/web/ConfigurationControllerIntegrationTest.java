package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.patch;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class ConfigurationControllerIntegrationTest extends ControllerIntegrationTest {
  @Autowired
  private IUserService userService;

  @Autowired
  private IConfigurationService configurationService;

  private User user;

  @Autowired
  private ProjectSnippets snippets;

  @Autowired
  private ObjectMapper objectMapper;

  @Before
  public void setup() {
    user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
  }

  @After
  public void tearDown() throws Exception {
    removeUser(user);
  }

  @Test
  public void testSetSmtpPortToInvalid() throws Exception {
    userService.grantUserPrivilege(user, PrivilegeType.IS_ADMIN);
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Map<String, Object> option = new HashMap<>();
    option.put("value", "not a number");
    Map<String, Object> data = new HashMap<>();
    data.put("option", option);

    RequestBuilder request = patch("/minerva/api/configuration/options/" + ConfigurationElementType.EMAIL_SMTP_PORT.name())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testDocsAccessConfigurationAsAnonymous() throws Exception {
    RequestBuilder request = get("/minerva/api/configuration/");

    mockMvc.perform(request)
        .andDo(document("configuration/details",
            configurationFields()))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
  }

  @Test
  public void testDocsAccessConfigurationOptionsAsAnonymous() throws Exception {
    RequestBuilder request = get("/minerva/api/configuration/options/");

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("configuration/list_options",
            responseFields(subsectionWithPath("[]")
                .description("list of options")
                .type("array<Option>")).andWithPrefix("[].", configurationOptionFields())))

        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> options = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });
    for (final Map<String, Object> jsonElement : options) {
      assertFalse((Boolean) jsonElement.get("isServerSide"));
    }

  }

  @Test
  public void testDocsSetSmtpPort() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Map<String, Object> option = new HashMap<>();
    option.put("value", ConfigurationElementType.EMAIL_SMTP_PORT.getDefaultValue());
    Map<String, Object> data = new HashMap<>();
    data.put("option", option);

    RequestBuilder request = patch("/minerva/api/configuration/options/{option}", ConfigurationElementType.EMAIL_SMTP_PORT.name())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andDo(document("configuration/modify_option",
            pathParameters(parameterWithName("option").description("option type")),
            requestFields(
                fieldWithPath("option.value").description("new option value")
                    .type(JsonFieldType.STRING)),
            responseFields(configurationOptionFields())))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testSetOptionEmptyBody() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = patch("/minerva/api/configuration/options/{option}", ConfigurationElementType.EMAIL_SMTP_PORT.name())
        .contentType(MediaType.APPLICATION_JSON)
        .content("{}")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testSetOptionInvalidType() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Map<String, Object> option = new HashMap<>();
    option.put("value", "25");
    Map<String, Object> data = new HashMap<>();
    data.put("option", option);

    RequestBuilder request = patch("/minerva/api/configuration/options/{option}", "blabla")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void accessConfigurationOptionsAsAdmin() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/configuration/options/").session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> options = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    boolean includeServerSide = false;
    for (final Map<String, Object> jsonElement : options) {
      includeServerSide |= (Boolean) jsonElement.get("isServerSide");
    }
    assertTrue(includeServerSide);
  }

  @Test
  public void testSetSmtpPortWithoutAdminPrivileges() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Map<String, Object> option = new HashMap<>();
    option.put("value", "123");
    Map<String, Object> data = new HashMap<>();
    data.put("option", option);

    RequestBuilder request = patch("/minerva/api/configuration/options/" + ConfigurationElementType.EMAIL_SMTP_PORT.name())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  private ResponseFieldsSnippet configurationFields() {
    return responseFields(
        fieldWithPath("annotators")
            .description("list of annotators"),
        fieldWithPath("bioEntityFields")
            .description("list of bieEntity fields that are used by annotators"),
        fieldWithPath("buildDate")
            .description("when minerva platform was built")
            .type("timestamp"),
        fieldWithPath("gitHash")
            .description("git hash used to build minerva platform")
            .type(JsonFieldType.STRING),
        fieldWithPath("version")
            .description("version of the minerva platform")
            .type(JsonFieldType.STRING),
        fieldWithPath("imageFormats")
            .description("list of available export image formats"),
        fieldWithPath("mapTypes")
            .description("list of submap types"),
        subsectionWithPath("miriamTypes")
            .description("map of miriam types supported by minerva. Types are defined on https://identifiers.org/"),
        fieldWithPath("modelFormats")
            .description("list of map formats"),
        subsectionWithPath("modificationStateTypes")
            .description("map with modification states"),
        fieldWithPath("options")
            .description("list of configuration options"),
        fieldWithPath("overlayTypes")
            .description("list of overlay types"),
        subsectionWithPath("privilegeTypes")
            .description("map with privilege type available in the system"),
        fieldWithPath("reactionTypes")
            .description("list of reaction types"),
        fieldWithPath("unitTypes")
            .description("list of basic unit types"),
        fieldWithPath("elementTypes")
            .description("list of element types"))
        .andWithPrefix("annotators[].", annotatorFields())
        .andWithPrefix("bioEntityFields[].", bioEntityFields())
        .andWithPrefix("elementTypes[].", elementTypeFields())
        .andWithPrefix("imageFormats[].", imageFormatFields())
        .andWithPrefix("mapTypes[].", mapTypeFields())
        .andWithPrefix("miriamTypes[].", miriamTypeFields())
        .andWithPrefix("modelFormats[].", mapFormatFields())
        .andWithPrefix("modificationStateTypes[].", modificationStateFields())
        .andWithPrefix("options[].", configurationOptionFields())
        .andWithPrefix("overlayTypes[].", overlayTypeFields())
        .andWithPrefix("privilegeTypes[].", privilegeTypeFields())
        .andWithPrefix("reactionTypes[].", elementTypeFields())
        .andWithPrefix("unitTypes[].", unitTypeFields());
  }

  private List<FieldDescriptor> annotatorFields() {
    List<FieldDescriptor> result = new ArrayList<>(Arrays.asList(
        fieldWithPath("name")
            .description("annotator name")
            .type(JsonFieldType.STRING),
        fieldWithPath("url")
            .description("url address of assoiated database")
            .type(JsonFieldType.STRING),
        fieldWithPath("className")
            .description("Java class responsible for annotating process")
            .type(JsonFieldType.STRING),
        fieldWithPath("elementClassNames")
            .description("Java classes that identify bioEntity types that can be annotated by the annotator")
            .type("array<string>"),
        fieldWithPath("description")
            .description("short description of the annotator")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("parameters")
            .description(
                "list of supported input fields, generated outputs that can be filled by annotator and configuration options")
            .type("array<object>")));
    result.addAll(annotatorParameterFields("parameters[]."));

    return result;
  }

  private List<FieldDescriptor> bioEntityFields() {
    return new ArrayList<>(Arrays.asList(
        fieldWithPath("commonName")
            .description("human readable name of the field")
            .type(JsonFieldType.STRING),
        fieldWithPath("name")
            .description("unique name of the field")
            .type(JsonFieldType.STRING)));
  }

  private List<FieldDescriptor> elementTypeFields() {
    return new ArrayList<>(Arrays.asList(
        fieldWithPath("className")
            .description("Java class that identify type")
            .type(JsonFieldType.STRING),
        fieldWithPath("name")
            .description("human readable name of the type")
            .type(JsonFieldType.STRING),
        fieldWithPath("parentClass")
            .description("Java class that identify parent type")
            .type(JsonFieldType.STRING)));
  }

  private List<FieldDescriptor> unitTypeFields() {
    return new ArrayList<>(Arrays.asList(
        fieldWithPath("name")
            .description("human readable name of the type")
            .type(JsonFieldType.STRING),
        fieldWithPath("id")
            .description("type identifier")
            .type(JsonFieldType.STRING)));
  }

  private List<FieldDescriptor> imageFormatFields() {
    return new ArrayList<>(Arrays.asList(
        fieldWithPath("extension")
            .description("file extension")
            .type(JsonFieldType.STRING),
        fieldWithPath("name")
            .description("human readable name of the format")
            .type(JsonFieldType.STRING),
        fieldWithPath("handler")
            .description("Java class that is responsible for export")
            .type(JsonFieldType.STRING)));
  }

  private List<FieldDescriptor> mapTypeFields() {
    return new ArrayList<>(Arrays.asList(
        fieldWithPath("id")
            .description("identifier of the type")
            .type(JsonFieldType.STRING),
        fieldWithPath("name")
            .description("human readable name of the submap type")
            .type(JsonFieldType.STRING)));
  }

  private List<FieldDescriptor> miriamTypeFields() {
    return new ArrayList<>(Arrays.asList(
        fieldWithPath("commonName")
            .description("human readable name of the identifier type")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("homepage")
            .description("url to database homepage")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("registryIdentifier")
            .description("unique identifier used by identifiers.org to reference this type")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("registryIdentifier")
            .description("list of uris used by this identifier type")
            .type("array<string>")
            .optional()));
  }

  private List<FieldDescriptor> mapFormatFields() {
    return new ArrayList<>(Arrays.asList(
        fieldWithPath("name")
            .description("human readable name of map format")
            .type(JsonFieldType.STRING),
        fieldWithPath("extension")
            .description("default file extension")
            .type(JsonFieldType.STRING),
        fieldWithPath("extensions")
            .description("file extensions supported by parser")
            .type(JsonFieldType.ARRAY),
        fieldWithPath("handler")
            .description("Java class that is used for import/export from the map format")
            .type(JsonFieldType.STRING)));
  }

  private List<FieldDescriptor> modificationStateFields() {
    return new ArrayList<>(Arrays.asList(
        fieldWithPath("commonName")
            .description("human readable name of modification state type")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("abbreviation")
            .description("short name of the type")
            .type(JsonFieldType.STRING)
            .optional()));
  }

  private List<FieldDescriptor> configurationOptionFields() {
    return new ArrayList<>(Arrays.asList(
        fieldWithPath("commonName")
            .description("human readable name of configuration option")
            .type(JsonFieldType.STRING),
        fieldWithPath("group")
            .description("group of options where this configuration option belongs to")
            .type(JsonFieldType.STRING),
        fieldWithPath("idObject")
            .description("option identifier")
            .type("number")
            .ignored(),
        fieldWithPath("isServerSide")
            .description("TRUE/FALSE - is the option responsible for server side processing or frontend")
            .type("boolean"),
        fieldWithPath("type")
            .description("option type")
            .type(JsonFieldType.STRING),
        fieldWithPath("value")
            .description("value of the configuration option")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("valueType")
            .description("type of the configuration option. Available options: "
                + snippets.getOptionsAsString(ConfigurationElementType.class))
            .type(JsonFieldType.STRING)

    ));
  }

  private List<FieldDescriptor> overlayTypeFields() {
    return new ArrayList<>(Collections.singletonList(
        fieldWithPath("name")
            .description("name of the type")
            .type(JsonFieldType.STRING)

    ));
  }

  private List<FieldDescriptor> privilegeTypeFields() {
    return new ArrayList<>(Arrays.asList(
        fieldWithPath("commonName")
            .description("human readable description of the privilege")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("objectType")
            .description("class of objects to which privilege applies")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("valueType")
            .description("type of the privilege value (ie. boolean)")
            .type(JsonFieldType.STRING)
            .optional()

    ));
  }

  private List<FieldDescriptor> annotatorParameterFields(final String originalPrefix) {
    String prefix = originalPrefix;
    if (prefix == null) {
      prefix = "";
    }
    List<FieldDescriptor> result = Arrays.asList(
        fieldWithPath(prefix + "annotation_type")
            .description("identifier type. Either this or 'field' is required for INPUT/OUTPUT parameter")
            .type("MiriamType")
            .optional(),
        fieldWithPath(prefix + "field")
            .description("bioEntity field. Either this or 'annotation_type' is required for INPUT/OUTPUT parameter")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath(prefix + "commonName")
            .description("name of the CONFIG parameter")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath(prefix + "description")
            .description("description of the parameter")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath(prefix + "inputType")
            .description("type of the CONFIG parameter")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath(prefix + "name")
            .description("name of the CONFIG parameter")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath(prefix + "value")
            .description("default value of the CONFIG parameter")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath(prefix + "order")
            .description("order position when listing parameters")
            .type("number"),
        fieldWithPath(prefix + "type")
            .description("type of parameter (INPUT/OUTPUT/CONFIG)")
            .type(JsonFieldType.STRING));
    return result;
  }

  @Test
  public void testSetValidRootUrl() throws Exception {
    String url = "https://minerva-dev.lcsb.uni.lu/minerva/";

    userService.grantUserPrivilege(user, PrivilegeType.IS_ADMIN);
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Map<String, Object> option = new HashMap<>();
    option.put("value", url);
    Map<String, Object> data = new HashMap<>();
    data.put("option", option);

    RequestBuilder request = patch("/minerva/api/configuration/options/" + ConfigurationElementType.MINERVA_ROOT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(url, configurationService.getConfigurationValue(ConfigurationElementType.MINERVA_ROOT));
  }

  @Test
  public void testSetInvalidRootUrl() throws Exception {
    String url = "https://minerva-dev.lcsb.uni.lu/";

    userService.grantUserPrivilege(user, PrivilegeType.IS_ADMIN);
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Map<String, Object> option = new HashMap<>();
    option.put("value", url);
    Map<String, Object> data = new HashMap<>();
    data.put("option", option);

    RequestBuilder request = patch("/minerva/api/configuration/options/" + ConfigurationElementType.MINERVA_ROOT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());

    assertNotEquals(url, configurationService.getConfigurationValue(ConfigurationElementType.MINERVA_ROOT));
  }

}
