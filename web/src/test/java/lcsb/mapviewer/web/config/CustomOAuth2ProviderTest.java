package lcsb.mapviewer.web.config;

import lcsb.mapviewer.web.ControllerIntegrationTest;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.security.oauth2.client.registration.ClientRegistration;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;

public class CustomOAuth2ProviderTest extends ControllerIntegrationTest {
  @Test
  public void testOrcidRedirect() throws Exception {
    final ClientRegistration.Builder builder = CustomOAuth2Provider.ORCID.getBuilder("orcid", "https://lux1.atcomp.pl/minerva/");
    final Field field = builder.getClass().getDeclaredField("redirectUri"); //NoSuchFieldException
    field.setAccessible(true);

    final String redirectUrl = (String) field.get(builder);

    final int count = StringUtils.countMatches(redirectUrl, "minerva");

    assertEquals(1, count);
  }
}
