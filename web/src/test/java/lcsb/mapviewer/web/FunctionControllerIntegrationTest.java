package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.ModelData;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class FunctionControllerIntegrationTest extends ControllerIntegrationTest {

  private Project project;
  private ModelData map;

  @Before
  public void setup() throws Exception {
    project = createAndPersistProject(TEST_PROJECT);
    map = project.getTopModel().getModelData();
  }

  @After
  public void tearDown() throws Exception {
    removeProject(project);
  }

  @Test
  public void testGetFunctionListWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/*/models/*/functions/")
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals(0, result.size());
  }

  @Test
  public void testGetFunctionWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{modelId}/functions/{functionId}", "*", 1, 1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDocsGetFunctions() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/functions/", TEST_PROJECT, map.getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_maps/get_functions",
            pathParameters(parameterWithName("projectId").description("project identifier"),
                parameterWithName("mapId").description("map identifier")),
            listOfFunctionFields()))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDocsGetFunction() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/functions/{functionId}", TEST_PROJECT,
        map.getId(),
        map.getFunctions().iterator().next().getId())
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("projects/project_maps/get_function",
            pathParameters(parameterWithName("projectId").description("project identifier"),
                parameterWithName("mapId").description("map identifier"),
                parameterWithName("functionId").description("function identifier")),
            responseFields(functionFields())))
        .andReturn().getResponse().getContentAsString();

    assertFalse(response.contains("\\\\"));
  }

  private List<FieldDescriptor> functionFields() {
    return Arrays.asList(
        fieldWithPath("id")
            .description("unique function identifier")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("name")
            .description("name")
            .type(JsonFieldType.STRING),
        fieldWithPath("definition")
            .description("definition")
            .type(JsonFieldType.STRING),
        fieldWithPath("mathMlPresentation")
            .description("mathMl representation of the definition")
            .type(JsonFieldType.STRING),
        fieldWithPath("functionId")
            .description("function identifier taken from source file")
            .type(JsonFieldType.STRING),
        fieldWithPath("arguments")
            .description("list of function parameters")
            .type(JsonFieldType.ARRAY));
  }

  private ResponseFieldsSnippet listOfFunctionFields() {
    return responseFields(
        fieldWithPath("[]").description("list of functions"))
        .andWithPrefix("[].", functionFields());
  }

}
