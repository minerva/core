package lcsb.mapviewer.web.utils;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.restdocs.cli.CommandFormatter;
import org.springframework.restdocs.cli.CurlRequestSnippet;
import org.springframework.restdocs.operation.Operation;

/**
 * Custom modification of {@link CurlRequestSnippet} that documents the curl
 * command for a request. The modification adds to the formatter information if
 * session object was present in the query. This allows a formatter to add
 * information about authentication token to the generated curl commands.
 * 
 * @author Piotr Gawron
 *
 */
public class CustomCurlRequestSnippet extends CurlRequestSnippet {

  protected static Logger logger = LogManager.getLogger();

  private final CommandFormatter commandFormatter;

  public CustomCurlRequestSnippet(final CommandFormatter commandFormatter) {
    super(commandFormatter);
    this.commandFormatter = commandFormatter;
  }

  public CustomCurlRequestSnippet(final Map<String, Object> attributes, final CommandFormatter commandFormatter) {
    super(attributes, commandFormatter);
    this.commandFormatter = commandFormatter;
  }

  @Override
  protected Map<String, Object> createModel(final Operation operation) {
    Object obj = operation.getAttributes().get("org.springframework.mock.web.MockHttpServletRequest");
    boolean session = false;
    if (obj instanceof org.springframework.mock.web.MockHttpServletRequest) {
      session = (((org.springframework.mock.web.MockHttpServletRequest) obj).getSession() != null);
      if (session) {
        session = !((org.springframework.mock.web.MockHttpServletRequest) obj).getSession().isNew();
      }
    }
    if (commandFormatter instanceof CommandFormatterWithReplacingPostFilenameHeader) {
      ((CommandFormatterWithReplacingPostFilenameHeader) commandFormatter).setSessionAvailable(session);
    }
    Map<String, Object> result = super.createModel(operation);

    String options = (String) result.get("options");
    // get rid of writeIncludeHeadersInOutputOption
    result.put("options", options.replace("-i ", ""));
    return result;
  }

}
