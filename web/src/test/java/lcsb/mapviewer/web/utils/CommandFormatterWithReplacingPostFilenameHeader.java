package lcsb.mapviewer.web.utils;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.restdocs.cli.CommandFormatter;
import org.springframework.util.CollectionUtils;

import lcsb.mapviewer.common.Configuration;

/**
 * This is a class for workaround the issue that when we put content of file
 * inside the request we would like to provide human readable curl request.In
 * such cases the curl example should not contain content of the file but simple
 * reference @filename. The filename is taken from header "post-filename" which
 * will be removed from the curl sample.
 * 
 * <p>
 * It is also a workaround the issue that restdocs ignores fact that curl by
 * default uses application/x-www-form-urlencoded content-type if no content
 * type is provided.
 * </p>
 * 
 * 
 * @author Piotr Gawron
 *
 */
public class CommandFormatterWithReplacingPostFilenameHeader implements CommandFormatter {

  protected static Logger logger = LogManager.getLogger();

  private String separator;

  /**
   * Information if in the formatted list of string there should also be
   * information about session.
   */
  private boolean session = false;

  public CommandFormatterWithReplacingPostFilenameHeader(final String separator) {
    this.separator = separator;
  }

  /**
   * Concatenates a list of {@code String}s with a specified separator.
   * 
   * @param elements
   *          a list of {@code String}s to be concatenated
   * @return concatenated list of {@code String}s as one {@code String}
   */
  @Override
  public String format(final List<String> elements) {
    if (CollectionUtils.isEmpty(elements)) {
      if (session) {
        return String.format(this.separator) + "--cookie \"" + Configuration.AUTH_TOKEN + "=xxxxxxxx\"";
      }
      return "";
    }
    String contentFileName = getContentFileName(elements);
    String contentType = getContentType(elements);

    if (session) {
      elements.add("--cookie \"" + Configuration.AUTH_TOKEN + "=xxxxxxxx\"");
    }

    StringBuilder result = new StringBuilder();
    for (String element : elements) {
      if (element.startsWith("-d") && contentFileName != null) {
        element = "--data-binary @" + contentFileName;
      }
      if (!element.startsWith("-H 'post-filename:")
          && !element.toLowerCase().startsWith("-h 'content-type:")) {
        result.append(String.format(this.separator));
        result.append(element);
      }
    }
    if (contentType != null && !contentType.isEmpty()) {
      result.append(String.format(this.separator));
      result.append("-H 'Content-Type: " + contentType + "'");
    }
    return result.toString();
  }

  public String getContentType(final List<String> elements) {
    String contentType = "application/octet-stream";
    for (String orgString : elements) {
      String string = orgString.replaceAll("\\s+", "")
          .replaceAll("'", "")
          .replaceAll("\"", "");
      if (string.toLowerCase().startsWith("-hcontent-type:")) {
        return string.split(":")[1];
      }
    }
    return contentType;
  }

  private String getContentFileName(final List<String> elements) {
    String contentFileName = null;
    for (final String element : elements) {
      if (element.startsWith("-H 'post-filename:")) {
        contentFileName = element.replace("-H 'post-filename: ", "").replace("'", "");
      }
    }
    return contentFileName;
  }

  public void setSessionAvailable(final boolean session) {
    this.session = session;
  }

}