package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpSession;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class SpringSecurityGeneralIntegrationTest extends ControllerIntegrationTest {

  @Autowired
  private IConfigurationService configurationService;

  private User user;

  @Before
  public void setup() throws Exception {
    user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
  }

  @After
  public void tearDown() throws Exception {
    removeUser(user);
  }

  @Test
  public void login2xxWithValidCredentials() throws Exception {
    final RequestBuilder request = post("/minerva/api/doLogin")
        .param("login", TEST_USER_LOGIN)
        .param("password", TEST_USER_PASSWORD);
    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void loginReturnsJsonWithAuthToken() throws Exception {
    final RequestBuilder request = post("/minerva/api/doLogin")
        .param("login", TEST_USER_LOGIN)
        .param("password", TEST_USER_PASSWORD);

    mockMvc.perform(request).andExpect((mvcResult) -> {
      assertTrue(mvcResult.getResponse().getContentAsString().contains("token"));
      assertTrue(mvcResult.getResponse().getContentAsString().contains("Login successful"));
    });
  }

  @Test
  @Ignore
  public void loginCreateLogEntries() throws Exception {
    final RequestBuilder request = post("/minerva/api/doLogin")
        .param("login", TEST_USER_LOGIN)
        .param("password", TEST_USER_PASSWORD);

    mockMvc.perform(request);
    assertEquals("User test_user successfully logged in using LOCAL authentication method",
        getDebugs().get(0).getMessage().getFormattedMessage());
  }

  @Test
  public void login4xxWithInvalidCredentials() throws Exception {
    final RequestBuilder request = post("/minerva/api/doLogin")
        .param("login", TEST_USER_LOGIN)
        .param("password", "test_foo");
    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void login4xxWithNonexistingPassword() throws Exception {
    final RequestBuilder request = post("/minerva/api/doLogin")
        .param("login", TEST_USER_LOGIN);
    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void login4xxWithNonexistingLogin() throws Exception {
    final RequestBuilder request = post("/minerva/api/doLogin")
        .param("password", "test_foo");
    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void logout2xxWithValidSession() throws Exception {
    final RequestBuilder request = post("/minerva/api/doLogout")
        .session(createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD));
    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void logoutReturnsJson() throws Exception {
    final RequestBuilder request = post("/minerva/api/doLogout")
        .session(createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD));
    mockMvc.perform(request).andExpect((mvcResult) -> {
      assertTrue(mvcResult.getResponse().getContentAsString().contains("OK"));
    });
  }

  @Test
  public void logout4xxWithInvalidSession() throws Exception {
    final RequestBuilder request = post("/minerva/api/doLogout");
    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDisallowCacheForIndexRequest() throws Exception {
    final RequestBuilder request = get("/minerva/old_index.xhtml");
    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse();
    assertTrue(response.getHeaderNames().contains("Pragma"));
    assertTrue(response.getHeaderNames().contains("Vary"));
    assertTrue(response.getHeaderNames().contains("Cache-Control"));

  }

  @Test
  public void testAllowCacheForNonApiRequest() throws Exception {
    final RequestBuilder request = get("/minerva/resources/minerva.js");
    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse();
    assertFalse(response.getHeaderNames().contains("Pragma"));
  }

  @Test
  public void testDisallowCacheForRootReuqest() throws Exception {
    final RequestBuilder request = get("/");
    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().is3xxRedirection())
        .andReturn().getResponse();
    assertTrue(response.getHeaderNames().contains("Pragma"));
    assertTrue(response.getHeaderNames().contains("Vary"));
    assertTrue(response.getHeaderNames().contains("Cache-Control"));
  }

  @Test
  public void testXFrameFilter() throws Exception {
    configurationService.setConfigurationValue(ConfigurationElementType.X_FRAME_DOMAIN, "https://minerva.uni.lu");
    final RequestBuilder request = get("/asd");
    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().is4xxClientError())
        .andReturn().getResponse();
    assertTrue(response.getHeaderNames().contains("Content-Security-Policy"));
  }

  @Test
  public void testXFrameFilterDisabled() throws Exception {
    configurationService.setConfigurationValue(ConfigurationElementType.X_FRAME_DOMAIN, "");
    final RequestBuilder request = get("/asd");
    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().is4xxClientError())
        .andReturn().getResponse();
    assertFalse(response.getHeaderNames().contains("Content-Security-Policy"));
  }

  @Test
  public void testDisableCacheForApiRequest() throws Exception {
    final RequestBuilder request = get("/minerva/api/configuration/");
    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse();
    assertTrue(response.getHeaderNames().contains("Pragma"));
    assertTrue(response.getHeaderNames().contains("Vary"));

  }

  @Test
  public void checkSessionLength() throws Exception {
    final HttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    assertTrue("Max session length  is not defined", session.getMaxInactiveInterval() > 0);
  }

  @Test
  public void loginValidJsonResponse() throws Exception {
    final RequestBuilder request = post("/minerva/api/doLogin")
        .param("login", TEST_USER_LOGIN)
        .param("password", TEST_USER_PASSWORD);
    final String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    final Map<String, Object> result = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });

    assertEquals(TEST_USER_LOGIN, result.get("login"));
  }

}
