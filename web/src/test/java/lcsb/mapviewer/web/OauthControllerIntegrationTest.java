package lcsb.mapviewer.web;

import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("EmailSenderEnabledEmailProfile")
public class OauthControllerIntegrationTest extends ControllerIntegrationTest {

  private static final String TEST_LOGIN = "test_login";

  private User user;

  @Autowired
  private IUserService userService;

  @Before
  public void setup() throws Exception {
    user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
  }

  @After
  public void tearDown() throws Exception {
    removeUser(user);
    removeUser(userService.getUserByLogin(TEST_LOGIN));
    removeProject(TEST_PROJECT);
  }

  @Test
  public void getProviders() throws Exception {

    RequestBuilder request = get("/minerva/api/oauth/providers");

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }
}
