package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.api.projects.models.bioentities.elements.ElementsController;
import lcsb.mapviewer.api.projects.models.bioentities.reactions.ReactionsController;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.converter.graphics.PdfImageGenerator;
import lcsb.mapviewer.converter.graphics.PngImageGenerator;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.converter.model.sbgnml.SbgnmlXmlConverter;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.modelutils.serializer.model.map.ElementIdentifierType;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.restdocs.request.PathParametersSnippet;
import org.springframework.restdocs.request.RequestParametersSnippet;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.patch;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class MapControllerIntegrationTest extends ControllerIntegrationTest {

  @Autowired
  private IUserService userService;

  @Autowired
  private IModelService modelService;

  @Autowired
  private ProjectSnippets snippets;

  @Autowired
  private ObjectMapper objectMapper;

  private User anonymous;

  private Project project;

  private ModelData map;

  @Before
  public void setup() throws Exception {
    project = createAndPersistProject(TEST_PROJECT);
    map = project.getTopModel().getModelData();
    anonymous = userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN);
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());
  }

  @After

  public void tearDown() throws Exception {
    removeProject(project);
  }

  ResponseFieldsSnippet listOfElementsResponseFields() {
    return responseFields(
        fieldWithPath("[].id")
            .description("unique element identifier")
            .type("number")
            .optional(),
        fieldWithPath("[].elementId")
            .description("element identifier taken from source file")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("[].name")
            .description("name")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("[].immediateLink")
            .description("link that should be opened immediately when element is clicked on the map")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("[].modelId")
            .description("map identifier")
            .type("number")
            .optional(),
        fieldWithPath("[].complexId")
            .description("identifier of a complex in which element is located")
            .type("number")
            .optional(),
        fieldWithPath("[].compartmentId")
            .description("identifier of a compartment in which element is located")
            .type("number")
            .optional(),
        fieldWithPath("[].linkedSubmodel")
            .description("identifier of a submap to which this element is an entry point (anchor)")
            .type("number")
            .optional(),
        fieldWithPath("[].type")
            .description("element type")
            .type(JsonFieldType.STRING)
            .optional(),
        subsectionWithPath("[].bounds")
            .description("coordinates and the dimension of the element on the map")
            .type("object")
            .optional(),
        fieldWithPath("[].hierarchyVisibilityLevel")
            .description("at what zoom level this element becomes visible in hierarchical view")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("[].transparencyLevel")
            .description("at what zoom level this element becomes transparent in hierarchical view")
            .type(JsonFieldType.STRING)
            .optional(),
        subsectionWithPath("[].synonyms")
            .description("list of synonyms")
            .type("array<string>")
            .optional(),
        subsectionWithPath("[].formerSymbols")
            .description("list of former symbols")
            .type("array<string>")
            .optional(),
        subsectionWithPath("[].references")
            .description("list of references")
            .type("array<Reference>")
            .optional(),
        fieldWithPath("[].notes")
            .description("notes and description")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("[].symbol")
            .description("symbol")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("[].fullName")
            .description("full name")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("[].abbreviation")
            .description("abbreviation")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("[].formula")
            .description("formula")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("[].boundaryCondition")
            .description("SBML kinetics is boundary condition")
            .type("boolean")
            .optional(),
        fieldWithPath("[].constant")
            .description("SBML kinetics is constant")
            .type("boolean")
            .optional(),
        fieldWithPath("[].initialAmount")
            .description("SBML kinetics initial amount")
            .type("number")
            .optional(),
        fieldWithPath("[].initialConcentration")
            .description("SBML kinetics initial concentration")
            .type("boolean")
            .optional(),
        fieldWithPath("[].glyph")
            .description("image glyph associated with the element")
            .type("Glyph")
            .optional(),
        fieldWithPath("[].glyph.fileId")
            .description("id of file associated with the glyph")
            .type("Glyph")
            .optional(),
        fieldWithPath("[].activity")
            .description("is the element active")
            .type("boolean")
            .optional(),
        fieldWithPath("[].hypothetical")
            .description("is the element hypothetical")
            .type("boolean")
            .optional(),
        fieldWithPath("[].homomultimer")
            .description("multimer value")
            .type("int")
            .optional(),
        subsectionWithPath("[].other")
            .description("list of other properties")
            .type("object")
            .optional());
  }

  private ResponseFieldsSnippet listOfReactionResponseFields() {
    List<FieldDescriptor> fields = new ArrayList<>(Arrays.asList(
        fieldWithPath("[].id")
            .description("unique element identifier")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath("[].reactionId")
            .description("reaction identifier taken from source file")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("[].name")
            .description("reaction name")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("[].modelId")
            .description("map identifier")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath("[].type")
            .description("reaction type")
            .type(JsonFieldType.STRING)
            .optional(),
        subsectionWithPath("[].centerPoint")
            .description("center point")
            .type(JsonFieldType.OBJECT)
            .optional(),
        subsectionWithPath("[].lines")
            .description("list of lines used to draw reaction")
            .type(JsonFieldType.ARRAY)
            .optional(),
        fieldWithPath("[].hierarchyVisibilityLevel")
            .description("at what zoom level this element becomes visible in hierarchical view")
            .type(JsonFieldType.STRING)
            .optional(),
        subsectionWithPath("[].references")
            .description("list of references")
            .type(JsonFieldType.ARRAY)
            .optional(),
        fieldWithPath("[].modifiers")
            .description("list of modifiers")
            .type(JsonFieldType.ARRAY)
            .optional(),
        fieldWithPath("[].reactants")
            .description("list of reactants")
            .type(JsonFieldType.ARRAY)
            .optional(),
        fieldWithPath("[].products")
            .description("list of products")
            .type(JsonFieldType.ARRAY)
            .optional(),
        fieldWithPath("[].notes")
            .description("notes and description")
            .type(JsonFieldType.STRING)
            .optional(),
        subsectionWithPath("[].kineticLaw")
            .description("SBML kinetics law")
            .type(JsonFieldType.OBJECT)
            .optional(),
        subsectionWithPath("[].other")
            .description("list of other properties")
            .type(JsonFieldType.OBJECT)
            .optional()));
    fields.addAll(reactionNodeFields("[].reactants[]"));
    fields.addAll(reactionNodeFields("[].products[]"));
    fields.addAll(reactionNodeFields("[].modifiers[]"));
    return responseFields(fields);
  }

  private List<FieldDescriptor> reactionNodeFields(final String originalPrefix) {
    String prefix = originalPrefix;
    if (!prefix.endsWith(".") && !prefix.isEmpty()) {
      prefix = prefix + ".";
    }
    return Arrays.asList(
        fieldWithPath(prefix + "aliasId")
            .description("element identifier")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath(prefix + "type")
            .description("type")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath(prefix + "stoichiometry")
            .description("element stoichiometry")
            .type(JsonFieldType.STRING)
            .optional());
  }

  private ResponseFieldsSnippet listSearchResults() {
    return responseFields(
        fieldWithPath("[].id")
            .description("unique element identifier")
            .type("number"),
        fieldWithPath("[].modelId")
            .description("map identifier")
            .type("number"),
        fieldWithPath("[].type")
            .description("type of the bioEntity (available options: " + ElementIdentifierType.ALIAS + ", "
                + ElementIdentifierType.REACTION + ")")
            .type(JsonFieldType.STRING));
  }

  RequestParametersSnippet getAllElementsFilter() {
    return requestParameters(
        parameterWithName("id")
            .description("set of database ids (all by default)")
            .optional(),
        parameterWithName("columns")
            .description("set of columns (all by default). Available options: "
                + StringUtils.join(new ElementsController(null).getAvailableElementColumns(), ", "))
            .optional(),
        parameterWithName("includedCompartmentIds")
            .description("set of database compartment ids (all by default)")
            .optional(),
        parameterWithName("excludedCompartmentIds")
            .description("set of database compartment ids (none by default)")
            .optional(),
        parameterWithName("type")
            .description("element type (all by default). Available options: "
                + StringUtils.join(new ProjectSnippets().getElementStringTypes(), ", "))
            .optional());
  }

  private RequestParametersSnippet getAllReactionsFilter() {
    return requestParameters(
        parameterWithName("id")
            .description("set of database ids (all by default)")
            .optional(),
        parameterWithName("columns")
            .description("set of columns (all by default). Available options: "
                + StringUtils.join(new ReactionsController(null).getAvailableReactionColumns(), ", "))
            .optional(),
        parameterWithName("participantId")
            .description("set of element identifiers for which reaction we are looking for "
                + "(only reactions with at least one participant will be returned)")
            .optional());
  }

  private RequestParametersSnippet getSearchParameters() {
    List<String> types = new ElementUtils().getBioEntityStringTypes();
    return requestParameters(
        parameterWithName("query")
            .description("search term identifying bioEntity")
            .optional(),
        parameterWithName("coordinates")
            .description("coordinates where bioEntity should be located")
            .optional(),
        parameterWithName("count")
            .description("max number of bioEntities to return")
            .optional(),
        parameterWithName("type")
            .description("type of bioEntity, available values are: " + StringUtils.join(types, ", "))
            .optional(),
        parameterWithName("perfectMatch")
            .description("true when true query must be matched exactly, if false similar results are also acceptable")
            .optional());
  }

  @Test
  public void testDocsGetAllElementsFilteredByType() throws Exception {
    RequestBuilder request = get(
        "/minerva/api/projects/{projectId}/models/{mapId}/bioEntities/elements/?columns=name,complexId&type=Protein",
        TEST_PROJECT, "*");

    String response = mockMvc.perform(request)
        .andDo(document("projects/project_maps/get_elements_by_type",
            mapPathParameters(),
            getAllElementsFilter(),
            listOfElementsResponseFields()))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertNotNull(result.get(0).get("name").toString());

    assertNull(result.get(0).get("id"));

  }

  @Test
  public void testGetElementsInsideCompartment() throws Exception {
    int compartmentId = -1;
    for (Element e : map.getElements()) {
      if (e instanceof Compartment) {
        compartmentId = e.getId();
      }
    }
    RequestBuilder request = get(
        "/minerva/api/projects/{projectId}/models/{mapId}/bioEntities/elements/?includedCompartmentIds={compIds}",
        TEST_PROJECT, "*", compartmentId);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertTrue(result.size() > 0);

  }

  @Test
  public void testGetAllElementsForUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/*/models/*/bioEntities/elements/").session(session);

    String content = mockMvc.perform(request).andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(content, new TypeReference<List<Map<String, Object>>>() {
    });

    printPrettyJson(content);
    assertEquals(0, result.size());
  }

  @Test
  public void testDocsGetAllReactions() throws Exception {
    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/bioEntities/reactions/", TEST_PROJECT, "*");

    String response = mockMvc.perform(request)
        .andDo(document("projects/project_maps/get_reactions",
            mapPathParameters(),
            getAllReactionsFilter(),
            listOfReactionResponseFields()))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertTrue("user should be able to access elements", result.size() > 0);
    assertFalse(response.contains("\\\\"));
  }

  @Test
  public void testDocsGetAllReactionsWithFilteredColumns() throws Exception {
    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/bioEntities/reactions/?columns=id,notes",
        TEST_PROJECT, "*");

    String response = mockMvc.perform(request)
        .andDo(document("projects/project_maps/get_reactions_limited_columns",
            mapPathParameters(),
            getAllReactionsFilter(),
            listOfReactionResponseFields()))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    String id = result.get(0).get("id").toString();

    assertNotNull(id);

    assertNull(result.get(0).get("modifiers"));
  }

  @Test
  public void testDocsSearchBioEntitiesAsAnonymous() throws Exception {
    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/bioEntities:search?coordinates={coords}",
        TEST_PROJECT, map.getId(),
        "104.36,182.81");

    mockMvc.perform(request)
        .andDo(document("projects/project_maps/search_by_coordinates",
            mapPathParameters(),
            getSearchParameters(),
            listSearchResults()))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testSearchProteinsByCoordinates() throws Exception {
    RequestBuilder request = get(
        "/minerva/api/projects/{projectId}/models/{mapId}/bioEntities:search?coordinates=104.36,182.81&type=Protein",
        TEST_PROJECT,
        map.getId());

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    List<Map<String, Object>> result = objectMapper.readValue(content, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals(1, result.size());
  }

  @Test
  public void testSearchBioEntitiesByCoordinatesWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/*/models/{mapId}/bioEntities:search?coordinates=104.36,182.81",
        map.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testSearchBioEntitiesByQueryWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/*/models/" + map.getId() + "/bioEntities:search?query=s1")
        .session(session);

    String content = mockMvc.perform(request).andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(content, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals(0, result.size());
  }

  @Test
  public void testSearchBioEntitiesByCoordinatesWithInvalidModelId() throws Exception {
    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/-1/bioEntities:search?coordinates=104.36,182.81",
        TEST_PROJECT);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testSearchBioEntitiesWithInvalidModelId() throws Exception {
    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{modelId}/bioEntities:search?query=s1",
        TEST_PROJECT, "0");

    String content = mockMvc.perform(request)
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(content, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals(0, result.size());

  }

  @Test
  public void testDocsSearchBioEntitiesOnEverySubmap() throws Exception {
    Element element = map.getElements().iterator().next();

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/bioEntities:search?query={query}",
        TEST_PROJECT, "*", element.getElementId());

    mockMvc.perform(request)
        .andDo(document("projects/project_maps/search_by_query",
            mapPathParameters(),
            getSearchParameters(),
            listSearchResults()))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testSearchReactionByReactionId() throws Exception {
    Reaction reaction = map.getReactions().iterator().next();

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/bioEntities:search?count=150&perfectMatch=false&query={query}",
        TEST_PROJECT, "*", "reaction:" + reaction.getElementId());

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    List<?> result = objectMapper.readValue(response, new TypeReference<List<?>>() {
    });
    assertTrue(result.size() > 0);
  }

  @Test
  public void testSearchWithCount() throws Exception {
    RequestBuilder request = get(
        "/minerva/api/projects/{projectId}/models/{mapId}/bioEntities:search?query=p&count=1", TEST_PROJECT, "*");

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    List<?> result = objectMapper.readValue(response, new TypeReference<List<?>>() {
    });
    assertEquals(1, result.size());

  }

  @Test
  public void testSearchReaction() throws Exception {
    Reaction reaction = map.getReactions().iterator().next();

    RequestBuilder request = get(
        "/minerva/api/projects/{projectId}/models/{mapId}/bioEntities:search?query=reaction:" + reaction.getElementId(), TEST_PROJECT, "*");

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    List<?> result = objectMapper.readValue(response, new TypeReference<List<?>>() {
    });
    assertTrue(result.size() > 0);
  }

  @Test
  public void testSuggestedQueryListWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/*/models/*/bioEntities/suggestedQueryList").session(session);

    String content = mockMvc.perform(request).andReturn().getResponse().getContentAsString();

    List<Object> result = objectMapper.readValue(content, new TypeReference<List<Object>>() {
    });

    assertEquals(0, result.size());
  }

  @Test
  public void testDocsSuggestedQueryList() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/bioEntities/suggestedQueryList",
        TEST_PROJECT,
        "*").session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_maps/suggested_query_list",
            mapPathParameters(),
            responseFields(
                fieldWithPath("[]")
                    .description("list of all full search queries that could be used for querying the map")
                    .type("array<string>"))))
        .andExpect(status().is2xxSuccessful());
  }

  ResponseFieldsSnippet getMapDescription() {
    return responseFields(fieldWithPath("name").description("name of the map").type(JsonFieldType.STRING),
        fieldWithPath("description").description("description").type(JsonFieldType.STRING),
        fieldWithPath("idObject").description("map id").type(JsonFieldType.NUMBER),
        fieldWithPath("width").description("map width").type(JsonFieldType.NUMBER),
        fieldWithPath("height").description("map height").type(JsonFieldType.NUMBER),
        fieldWithPath("tileSize").description("size of the png tile used to visualize in frontend")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("defaultCenterX")
            .description("default x center used in frontend visualization")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath("defaultCenterY")
            .description("default y center used in frontend visualization")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath("defaultZoomLevel")
            .description("default zoom level used in frontend visualization")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath("minZoom").description("minimum zoom level available for the map").type(JsonFieldType.NUMBER),
        fieldWithPath("maxZoom").description("maximum zoom level available for the map").type(JsonFieldType.NUMBER),
        subsectionWithPath("authors").description("list of authors").type(JsonFieldType.ARRAY),
        subsectionWithPath("references").description("list of references").type(JsonFieldType.ARRAY),
        fieldWithPath("creationDate").description("creation date").type("timestamp"),
        fieldWithPath("modificationDates").description("modification dates").type("array<timestamp>"));
  }

  @Test
  public void testGetMapByIdWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/*/models/" + map.getId()).session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetMapsWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/*/models/").session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testUpdateMapWithMissingContent() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String content = "{}";

    RequestBuilder request = patch("/minerva/api/projects/{projectId}/models/{mapId}", TEST_PROJECT, map.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .session(session).content(content);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testDocsUpdateMap() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Double centerX = 10.0;
    Double centerY = 20.0;
    Integer zoom = 3;

    Map<String, Map<String, Object>> data = new HashMap<>();
    Map<String, Object> modelData = new HashMap<>();
    modelData.put("defaultCenterX", centerX);
    modelData.put("defaultCenterY", centerY);
    modelData.put("defaultZoomLevel", zoom);
    data.put("model", modelData);

    RequestBuilder request = patch("/minerva/api/projects/{projectId}/models/{mapId}", TEST_PROJECT, map.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .session(session)
        .content(objectMapper.writeValueAsString(data));

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("projects/project_maps/update",
            mapPathParameters(),
            requestFields(
                fieldWithPath("model.defaultCenterX").description("default x center used in frontend visualization")
                    .type(JsonFieldType.NUMBER)
                    .optional(),
                fieldWithPath("model.defaultCenterY").description("default y center used in frontend visualization")
                    .type(JsonFieldType.NUMBER)
                    .optional(),
                fieldWithPath("model.defaultZoomLevel").description("default zoom level used in frontend visualization")
                    .type(JsonFieldType.NUMBER)
                    .optional()),
            getMapDescription()));
    ModelData updatedModel = modelService.getModelByMapId(TEST_PROJECT, map.getId());
    assertEquals(centerX, updatedModel.getDefaultCenterX(), EPSILON);
    assertEquals(centerY, updatedModel.getDefaultCenterY(), EPSILON);
    assertEquals(zoom, updatedModel.getDefaultZoomLevel());
  }

  @Test
  public void testUpdateMapWithNull() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    map.setDefaultCenterX(1.0);
    map.setDefaultCenterY(2.0);
    map.setDefaultZoomLevel(3);
    modelService.update(map);

    Map<String, Map<String, Object>> data = new HashMap<>();
    Map<String, Object> modelData = new HashMap<>();
    modelData.put("defaultCenterX", null);
    modelData.put("defaultCenterY", null);
    modelData.put("defaultZoomLevel", null);
    data.put("model", modelData);

    RequestBuilder request = patch("/minerva/api/projects/{projectId}/models/{mapId}", TEST_PROJECT, map.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .session(session)
        .content(objectMapper.writeValueAsString(data));

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
    ModelData updatedModel = modelService.getModelByMapId(TEST_PROJECT, map.getId());
    assertNull(updatedModel.getDefaultCenterX());
    assertNull(updatedModel.getDefaultCenterY());
    assertNull(updatedModel.getDefaultZoomLevel());
  }

  @Test
  public void testUpdateMapWithPartial() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    map.setDefaultCenterX(1.0);
    map.setDefaultCenterY(2.0);
    map.setDefaultZoomLevel(3);
    modelService.update(map);

    Map<String, Map<String, Object>> data = new HashMap<>();
    Map<String, Object> modelData = new HashMap<>();
    modelData.put("defaultCenterX", null);
    data.put("model", modelData);

    RequestBuilder request = patch("/minerva/api/projects/{projectId}/models/{mapId}", TEST_PROJECT, map.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .session(session)
        .content(objectMapper.writeValueAsString(data));

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
    ModelData updatedModel = modelService.getModelByMapId(TEST_PROJECT, map.getId());
    assertNull(updatedModel.getDefaultCenterX());
    assertNotNull(updatedModel.getDefaultCenterY());
    assertNotNull(updatedModel.getDefaultZoomLevel());
  }

  @Test
  public void testUpdateMapWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String content = "{\"model\":{}}";

    RequestBuilder request = patch("/minerva/api/projects/*/models/" + map.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .session(session)
        .content(content);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDocsDownloadImage() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}:downloadImage?"
        + "handlerClass=" + PngImageGenerator.class.getCanonicalName(), TEST_PROJECT, map.getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_maps/download_image_simple",
            downloadImageRequestParameters(),
            mapPathParameters()))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDownloadSubmapImage() throws Exception {
    ModelData submap = getSubmap();

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}:downloadImage?"
        + "handlerClass=" + PngImageGenerator.class.getCanonicalName(), TEST_PROJECT, submap.getId());

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  private ModelData getSubmap() {
    ModelData submap = null;
    for (ModelData modelData : project.getModels()) {
      if (modelData != map) {
        submap = modelData;
      }
    }
    return submap;
  }

  @Test
  public void testDownloadSubmapModel() throws Exception {
    ModelData submap = getSubmap();

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}:downloadModel?"
        + "handlerClass=" + CellDesignerXmlParser.class.getCanonicalName(), TEST_PROJECT, submap.getId());

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDownloadModelWithOverlay() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    ModelData submap = getSubmap();
    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    DataOverlay overlay = createOverlay(project, admin, "name\tvalue\nGSTA4\t1");

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}:downloadModel?"
            + "handlerClass={handlerClass}"
            + "&overlayIds={overlayId}", TEST_PROJECT, submap.getId(),
        CellDesignerXmlParser.class.getCanonicalName(),
        overlay.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDownloadSubmapModelWithPost() throws Exception {
    ModelData submap = getSubmap();

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Collections.singletonList(
        new BasicNameValuePair("handlerClass", CellDesignerXmlParser.class.getCanonicalName()))));

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}:downloadModel", TEST_PROJECT, submap.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDocsDownloadImageDataOverlay() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    DataOverlay overlay = createOverlay(TEST_PROJECT,
        userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}:downloadImage?"
            + "handlerClass={handlerClass}&overlayIds={ids}",
        TEST_PROJECT,
        map.getId(),
        PngImageGenerator.class.getCanonicalName(),
        overlay.getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_maps/download_image_data_overlay",
            downloadImageRequestParameters(),
            mapPathParameters()))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDownloadInfluenceMap() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder elementRequest = get("/minerva/api/projects/{projectId}/models/*/bioEntities/elements/",
        TEST_PROJECT)
        .session(session);
    String content = mockMvc.perform(elementRequest).andReturn().getResponse().getContentAsString();

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}:downloadImage?"
            + "handlerClass=" + PdfImageGenerator.class.getCanonicalName()
            + "&polygonString=10000,10000;10001,10000;10000,10001",
        TEST_PROJECT, map.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    String afterImageDownloadContent = mockMvc.perform(elementRequest).andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> originalElements = objectMapper.readValue(content,
        new TypeReference<List<Map<String, Object>>>() {
        });

    List<Map<String, Object>> afterImageDownloadElements = objectMapper.readValue(afterImageDownloadContent,
        new TypeReference<List<Map<String, Object>>>() {
        });

    assertEquals("Some elements were removed during image generation",
        originalElements.size(), afterImageDownloadElements.size());
  }

  @Test
  public void testDocsDownloadImagePolygon() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}:downloadImage?"
        + "handlerClass=" + PdfImageGenerator.class.getCanonicalName()
        + "&polygonString=0,0;100,0;100,100;0,100", TEST_PROJECT, map.getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_maps/download_image_polygon",
            downloadImageRequestParameters(),
            mapPathParameters()))
        .andExpect(status().is2xxSuccessful());
  }

  private RequestParametersSnippet downloadImageRequestParameters() {
    return requestParameters(
        parameterWithName("handlerClass").description("class preparing image. Available options: "
            + snippets.getImageConverters()),
        parameterWithName("polygonString").description("polygon defining part of the map to be downloaded")
            .optional(),
        parameterWithName("backgroundOverlayId")
            .description("identifier of the overlay used as a background when creating image")
            .optional(),
        parameterWithName("zoomLevel")
            .description("zoom level at which image should be generated (min value used by default)")
            .optional(),
        parameterWithName("overlayIds")
            .description("comma separated list of overlay identifiers that should be included in the image")
            .optional());
  }

  @Test
  public void testDownloadImageWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/*/models/" + map.getId() + ":downloadImage?"
        + "handlerClass=" + PngImageGenerator.class.getCanonicalName())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDownloadModelWarnings() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/minerva/api/projects/" + TEST_PROJECT + "/models/" + map.getId() + ":downloadModelWarnings?"
            + "handlerClass=" + SbgnmlXmlConverter.class.getCanonicalName())
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    Map<String, Object> result = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });

    int elements = ((List<?>) result.get("data")).size();
    assertEquals(1, elements);
  }

  @Test
  public void testDownloadModelAsZip() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/models/" + map.getId() + ":downloadModel?"
        + "handlerClass=" + CellDesignerXmlParser.class.getCanonicalName())
        .accept(MimeType.ZIP.getTextRepresentation() + ";q=0.8")
        .session(session);

    MvcResult result = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn();
    assertEquals(MimeType.ZIP.getTextRepresentation(), result.getResponse().getContentType());
    assertTrue(result.getResponse().getHeader("Content-Disposition").endsWith(".zip"));
  }

  @Test
  public void testDownloadModelWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/*/models/" + map.getId() + ":downloadModel?"
        + "handlerClass=" + CellDesignerXmlParser.class.getCanonicalName())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  private PathParametersSnippet mapPathParameters() {
    return pathParameters(parameterWithName("projectId").description("project identifier"),
        parameterWithName("mapId").description("map identifier"));
  }

}
