package lcsb.mapviewer.web;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.converter.model.sbgnml.SbgnmlXmlConverter;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.DbUtils;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.apache.commons.io.FileUtils;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.patch;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class BuildTestDatabase extends ControllerIntegrationTest {

  private static String TEST_SIMPLE_PROJECT = "single-map";
  private static String TEST_SIMPLE_GOOGLE_MAP_PROJECT = "singleGM-map";
  private static String TEST_ADVANCED_MAP_PROJECT = "advanced-map";
  private static String TEST_SBGNML_MAP_PROJECT = "SBGN-ML-map";

  private static String TEST_ADMIN_1_USER = "test-admin";
  private static String TEST_ADMIN_2_USER = "test-general";
  private static String TEST_CURATOR_USER = "test-curator";
  private static String TEST_USER_CAN_CREATE_OVERLAYS_USER = "test-user-overlays";
  private static String TEST_USER = "test-user";

  @Autowired
  private IUserService userService;

  @Autowired
  private DbUtils dbUtils;

  private MockHttpSession session;

  private User admin;

  @Before
  public void setup() throws Exception {
    session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
  }

  @Test
  @Ignore
  public void generateDataSet() throws Exception {
    String imageDirectory = "src/main/map_images";
    FileUtils.deleteDirectory(new File(imageDirectory));

    createAdmin1();
    createAdmin2();
    createCurator();
    createUser();
    createUserOverlays();

    createTestSimpleProject();
    grantPrivilege(Configuration.ANONYMOUS_LOGIN, PrivilegeType.READ_PROJECT, TEST_SIMPLE_PROJECT);
    grantPrivilege(TEST_ADMIN_1_USER, PrivilegeType.READ_PROJECT, TEST_SIMPLE_PROJECT);
    grantPrivilege(TEST_ADMIN_2_USER, PrivilegeType.READ_PROJECT, TEST_SIMPLE_PROJECT);
    grantPrivilege(TEST_CURATOR_USER, PrivilegeType.READ_PROJECT, TEST_SIMPLE_PROJECT);
    grantPrivilege(TEST_USER, PrivilegeType.READ_PROJECT, TEST_SIMPLE_PROJECT);
    grantPrivilege(TEST_USER_CAN_CREATE_OVERLAYS_USER, PrivilegeType.READ_PROJECT, TEST_SIMPLE_PROJECT);

    grantPrivilege(TEST_ADMIN_1_USER, PrivilegeType.WRITE_PROJECT, TEST_SIMPLE_PROJECT);
    grantPrivilege(TEST_ADMIN_2_USER, PrivilegeType.WRITE_PROJECT, TEST_SIMPLE_PROJECT);
    grantPrivilege(TEST_CURATOR_USER, PrivilegeType.WRITE_PROJECT, TEST_SIMPLE_PROJECT);

    createTestSingleGoogleMapsProject();
    grantPrivilege(Configuration.ANONYMOUS_LOGIN, PrivilegeType.READ_PROJECT, TEST_SIMPLE_GOOGLE_MAP_PROJECT);
    grantPrivilege(TEST_ADMIN_1_USER, PrivilegeType.READ_PROJECT, TEST_SIMPLE_GOOGLE_MAP_PROJECT);
    grantPrivilege(TEST_ADMIN_2_USER, PrivilegeType.READ_PROJECT, TEST_SIMPLE_GOOGLE_MAP_PROJECT);
    grantPrivilege(TEST_CURATOR_USER, PrivilegeType.READ_PROJECT, TEST_SIMPLE_GOOGLE_MAP_PROJECT);
    grantPrivilege(TEST_USER, PrivilegeType.READ_PROJECT, TEST_SIMPLE_GOOGLE_MAP_PROJECT);
    grantPrivilege(TEST_USER_CAN_CREATE_OVERLAYS_USER, PrivilegeType.READ_PROJECT, TEST_SIMPLE_GOOGLE_MAP_PROJECT);

    grantPrivilege(TEST_ADMIN_1_USER, PrivilegeType.WRITE_PROJECT, TEST_SIMPLE_GOOGLE_MAP_PROJECT);
    grantPrivilege(TEST_ADMIN_2_USER, PrivilegeType.WRITE_PROJECT, TEST_SIMPLE_GOOGLE_MAP_PROJECT);
    grantPrivilege(TEST_CURATOR_USER, PrivilegeType.WRITE_PROJECT, TEST_SIMPLE_GOOGLE_MAP_PROJECT);

    createAdvancedMapProject();
    grantPrivilege(Configuration.ANONYMOUS_LOGIN, PrivilegeType.READ_PROJECT, TEST_ADVANCED_MAP_PROJECT);
    grantPrivilege(TEST_ADMIN_1_USER, PrivilegeType.READ_PROJECT, TEST_ADVANCED_MAP_PROJECT);
    grantPrivilege(TEST_ADMIN_2_USER, PrivilegeType.READ_PROJECT, TEST_ADVANCED_MAP_PROJECT);
    grantPrivilege(TEST_CURATOR_USER, PrivilegeType.READ_PROJECT, TEST_ADVANCED_MAP_PROJECT);
    grantPrivilege(TEST_USER, PrivilegeType.READ_PROJECT, TEST_ADVANCED_MAP_PROJECT);
    grantPrivilege(TEST_USER_CAN_CREATE_OVERLAYS_USER, PrivilegeType.READ_PROJECT, TEST_ADVANCED_MAP_PROJECT);

    createSbgnmlMapProject();
    grantPrivilege(Configuration.ANONYMOUS_LOGIN, PrivilegeType.READ_PROJECT, TEST_SBGNML_MAP_PROJECT);
    grantPrivilege(TEST_ADMIN_1_USER, PrivilegeType.READ_PROJECT, TEST_SBGNML_MAP_PROJECT);
    grantPrivilege(TEST_ADMIN_2_USER, PrivilegeType.READ_PROJECT, TEST_SBGNML_MAP_PROJECT);
    grantPrivilege(TEST_CURATOR_USER, PrivilegeType.READ_PROJECT, TEST_SBGNML_MAP_PROJECT);
    grantPrivilege(TEST_USER, PrivilegeType.READ_PROJECT, TEST_SBGNML_MAP_PROJECT);
    grantPrivilege(TEST_USER_CAN_CREATE_OVERLAYS_USER, PrivilegeType.READ_PROJECT, TEST_SBGNML_MAP_PROJECT);

    String tmpFileName = "tmp.sql";
    if (new File(tmpFileName).exists()) {
      new File(tmpFileName).delete();
    }
    dbUtils.dumpDbToFile(tmpFileName);
    String content = filterImportantStatemants(tmpFileName);
    String sqlFile = "V15.0.0.20200206__test_db_dump.sql";
    BufferedWriter writer = new BufferedWriter(new FileWriter(sqlFile, true));
    writer.append(content);
    writer.close();

    String testZip = "generated_test_data.zip";
    createZip(sqlFile, imageDirectory, testZip);

    new File(sqlFile).delete();
    new File(tmpFileName).delete();
  }

  private void createAdmin1() throws Exception {
    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("login", TEST_ADMIN_1_USER),
        new BasicNameValuePair("password", "test"),
        new BasicNameValuePair("name", "TEST-admin"),
        new BasicNameValuePair("surname", ""),
        new BasicNameValuePair("email", "ewa.smula@uni.lu"))));
    RequestBuilder request = post("/minerva/api/users/" + TEST_ADMIN_1_USER)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    grantPrivilege(TEST_ADMIN_1_USER, PrivilegeType.IS_ADMIN);
  }

  private void createAdmin2() throws Exception {
    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("login", TEST_ADMIN_2_USER),
        new BasicNameValuePair("password", "test"),
        new BasicNameValuePair("name", "TEST-general"),
        new BasicNameValuePair("surname", ""),
        new BasicNameValuePair("email", "ewa.smula@uni.lu"))));
    RequestBuilder request = post("/minerva/api/users/" + TEST_ADMIN_2_USER)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    grantPrivilege(TEST_ADMIN_2_USER, PrivilegeType.IS_ADMIN);
  }

  private void createCurator() throws Exception {
    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("login", TEST_CURATOR_USER),
        new BasicNameValuePair("password", "test"),
        new BasicNameValuePair("name", "TEST-curator"),
        new BasicNameValuePair("surname", ""),
        new BasicNameValuePair("email", "ewa.smula@uni.lu"))));
    RequestBuilder request = post("/minerva/api/users/" + TEST_CURATOR_USER)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    grantPrivilege(TEST_CURATOR_USER, PrivilegeType.IS_CURATOR);
  }

  private void createUserOverlays() throws Exception {
    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("login", TEST_USER_CAN_CREATE_OVERLAYS_USER),
        new BasicNameValuePair("password", "test"),
        new BasicNameValuePair("name", "TEST-curator"),
        new BasicNameValuePair("surname", ""),
        new BasicNameValuePair("email", "ewa.smula@uni.lu"))));
    RequestBuilder request = post("/minerva/api/users/" + TEST_USER_CAN_CREATE_OVERLAYS_USER)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  private void createUser() throws Exception {
    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("login", TEST_USER),
        new BasicNameValuePair("password", "test"),
        new BasicNameValuePair("name", "TEST-user"),
        new BasicNameValuePair("surname", ""),
        new BasicNameValuePair("email", "ewa.smula@uni.lu"))));
    RequestBuilder request = post("/minerva/api/users/" + TEST_USER)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  private void grantPrivilege(final String userLogin, final PrivilegeType privilege) throws Exception {
    String body = "{\"privileges\":{\"" + privilege + "\":true}}";

    RequestBuilder request = patch("/minerva/api/users/" + userLogin + ":updatePrivileges")
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  private void grantPrivilege(final String userLogin, final PrivilegeType privilege, final String projectId) throws Exception {
    RequestBuilder request;
    String body = "[{"
        + "\"privilegeType\":\"" + privilege + "\", "
        + "\"login\":\"" + userLogin + "\""
        + "}]";

    request = patch("/minerva/api/projects/{projectId}:grantPrivileges", projectId)
        .content(body)
        .session(session);
    mockMvc.perform(request).andExpect(status().is2xxSuccessful());
  }

  private void createTestSimpleProject() throws Exception, IOException, UnsupportedEncodingException {
    RequestBuilder request = createTestSimpleProjectRequest(TEST_SIMPLE_PROJECT);

    mockMvc.perform(request).andExpect(status().is2xxSuccessful());
    waitForProjectToFinishLoading(TEST_SIMPLE_PROJECT);
  }

  private void createSbgnmlMapProject() throws Exception, IOException, UnsupportedEncodingException {
    RequestBuilder request = createSbgnProjectRequest();

    mockMvc.perform(request).andExpect(status().is2xxSuccessful());
    waitForProjectToFinishLoading(TEST_SBGNML_MAP_PROJECT);
  }

  private void createTestSingleGoogleMapsProject() throws Exception, IOException, UnsupportedEncodingException {
    RequestBuilder request = createTestSimpleProjectRequest(TEST_SIMPLE_GOOGLE_MAP_PROJECT);

    mockMvc.perform(request).andExpect(status().is2xxSuccessful());
    waitForProjectToFinishLoading(TEST_SIMPLE_GOOGLE_MAP_PROJECT);
  }

  private void createAdvancedMapProject() throws Exception, IOException, UnsupportedEncodingException {
    RequestBuilder request = createAdvancedProjectRequest();

    mockMvc.perform(request).andExpect(status().is2xxSuccessful());
    waitForProjectToFinishLoading(TEST_ADVANCED_MAP_PROJECT);
  }

  private RequestBuilder createTestSimpleProjectRequest(final String projectId)
      throws Exception, IOException, UnsupportedEncodingException {
    UploadedFileEntry fileEntry = createFile(
        Files.readAllBytes(Paths.get("./src/test/resources/test-base/singleTestMap.zip")),
        admin);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("projectId", projectId),
        new BasicNameValuePair("name", "CellD in OpenLayers"),
        new BasicNameValuePair("parser", CellDesignerXmlParser.class.getName()),
        new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
        new BasicNameValuePair("cache", "false"),
        new BasicNameValuePair("auto-resize", "true"),
        new BasicNameValuePair("disease", "D010300"),
        new BasicNameValuePair("organism", "9606"),
        new BasicNameValuePair("notify-email", "notify.me@uni.lu"),
        new BasicNameValuePair("sbgn", "false"),
        new BasicNameValuePair("semantic-zoom-contains-multiple-layouts", "false"),
        new BasicNameValuePair("annotate", "false"),
        new BasicNameValuePair("semantic-zoom", "false"),
        new BasicNameValuePair("zip-entries[0][_type]", "OVERLAY"),
        new BasicNameValuePair("zip-entries[0][_filename]", "layouts/basic-no-header.txt"),
        new BasicNameValuePair("zip-entries[0][_data][name]", "basic"),
        new BasicNameValuePair("zip-entries[0][_data][description]", ""),
        new BasicNameValuePair("zip-entries[1][_type]", "OVERLAY"),
        new BasicNameValuePair("zip-entries[1][_filename]", "layouts/general-advanced.txt"),
        new BasicNameValuePair("zip-entries[1][_data][name]", "Sorted-by-species"),
        new BasicNameValuePair("zip-entries[1][_data][description]", ""),
        new BasicNameValuePair("zip-entries[2][_type]", "OVERLAY"),
        new BasicNameValuePair("zip-entries[2][_filename]", "layouts/hg38-genetic.txt"),
        new BasicNameValuePair("zip-entries[2][_data][name]", "genetic"),
        new BasicNameValuePair("zip-entries[2][_data][description]", ""),
        new BasicNameValuePair("zip-entries[3][_type]", "MAP"),
        new BasicNameValuePair("zip-entries[3][_filename]", "single.xml"),
        new BasicNameValuePair("zip-entries[3][_data][root]", "true"),
        new BasicNameValuePair("zip-entries[3][_data][name]", "single"))));

    RequestBuilder request = post("/minerva/api/projects/{projectId}/", projectId)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);
    return request;
  }

  private RequestBuilder createSbgnProjectRequest()
      throws Exception, IOException, UnsupportedEncodingException {
    UploadedFileEntry fileEntry = createFile(
        Files.readAllBytes(Paths.get("./src/test/resources/test-base/sourceMaps_SBGN-MLmap.sgbn")),
        admin);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("projectId", TEST_SBGNML_MAP_PROJECT),
        new BasicNameValuePair("name", "From NEWT map"),
        new BasicNameValuePair("parser", SbgnmlXmlConverter.class.getName()),
        new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
        new BasicNameValuePair("cache", "false"),
        new BasicNameValuePair("auto-resize", "true"),
        new BasicNameValuePair("disease", "D010300"),
        new BasicNameValuePair("organism", "9606"),
        new BasicNameValuePair("notify-email", "notify.me@uni.lu"),
        new BasicNameValuePair("sbgn", "true"),
        new BasicNameValuePair("semantic-zoom-contains-multiple-layouts", "false"),
        new BasicNameValuePair("annotate", "false"),
        new BasicNameValuePair("semantic-zoom", "false"))));

    RequestBuilder request = post("/minerva/api/projects/{projectId}/", TEST_SBGNML_MAP_PROJECT)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);
    return request;
  }

  private RequestBuilder createAdvancedProjectRequest()
      throws Exception, IOException, UnsupportedEncodingException {
    UploadedFileEntry fileEntry = createFile(
        Files.readAllBytes(Paths.get("./src/test/resources/test-base/advancedMap.zip")),
        admin);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("projectId", TEST_ADVANCED_MAP_PROJECT),
        new BasicNameValuePair("name", "Advanced map"),
        new BasicNameValuePair("parser", CellDesignerXmlParser.class.getName()),
        new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
        new BasicNameValuePair("cache", "false"),
        new BasicNameValuePair("auto-resize", "true"),
        new BasicNameValuePair("disease", "D010300"),
        new BasicNameValuePair("organism", "9606"),
        new BasicNameValuePair("notify-email", "notify.me@uni.lu"),
        new BasicNameValuePair("sbgn", "false"),
        new BasicNameValuePair("semantic-zoom-contains-multiple-layouts", "false"),
        new BasicNameValuePair("annotate", "false"),
        new BasicNameValuePair("semantic-zoom", "false"),
        new BasicNameValuePair("zip-entries[0][_type]", "IMAGE"),
        new BasicNameValuePair("zip-entries[0][_filename]", "images/coords.txt"),
        new BasicNameValuePair("zip-entries[1][_type]", "IMAGE"),
        new BasicNameValuePair("zip-entries[1][_filename]", "images/overview_sub.png"),
        new BasicNameValuePair("zip-entries[2][_type]", "IMAGE"),
        new BasicNameValuePair("zip-entries[2][_filename]", "images/overview_main.png"),
        new BasicNameValuePair("zip-entries[3][_type]", "MAP"),
        new BasicNameValuePair("zip-entries[3][_filename]", "submaps/mapping.xml"),
        new BasicNameValuePair("zip-entries[3][_data][mapping]", "true"),
        new BasicNameValuePair("zip-entries[3][_data][name]", "mapping"),
        new BasicNameValuePair("zip-entries[4][_type]", "MAP"),
        new BasicNameValuePair("zip-entries[4][_filename]", "submaps/Mitochondrial and ROS metabolism.xml"),
        new BasicNameValuePair("zip-entries[4][_data][name]", "Mitochondrial and ROS metabolism"),
        new BasicNameValuePair("zip-entries[4][_data][type][id]", "UNKNOWN"),
        new BasicNameValuePair("zip-entries[4][_data][type][name]", "Unknown"),
        new BasicNameValuePair("zip-entries[5][_type]", "OVERLAY"),
        new BasicNameValuePair("zip-entries[5][_filename]", "layouts/genetic_overlay_POLG.txt"),
        new BasicNameValuePair("zip-entries[5][_data][name]", "genetic_overlay_POLG"),
        new BasicNameValuePair("zip-entries[5][_data][description]", ""),
        new BasicNameValuePair("zip-entries[6][_type]", "OVERLAY"),
        new BasicNameValuePair("zip-entries[6][_filename]", "layouts/layout-2.txt"),
        new BasicNameValuePair("zip-entries[6][_data][name]", "Random layout"),
        new BasicNameValuePair("zip-entries[6][_data][description]", "This is test."),
        new BasicNameValuePair("zip-entries[7][_type]", "OVERLAY"),
        new BasicNameValuePair("zip-entries[7][_filename]", "layouts/layout-1-pd-SN.txt"),
        new BasicNameValuePair("zip-entries[7][_data][name]", "PD substantia nigra"),
        new BasicNameValuePair("zip-entries[7][_data][description]",
            "Differential transcriptome expression from post mortem tissue. "
                + "Meta-analysis from 8 published datasets, final FDR = 0.05, see PMIDs 23832570 and 25447234."),
        new BasicNameValuePair("zip-entries[8][_type]", "MAP"),
        new BasicNameValuePair("zip-entries[8][_filename]", "MainMap.xml"),
        new BasicNameValuePair("zip-entries[8][_data][root]", "true"),
        new BasicNameValuePair("zip-entries[8][_data][name]", "MainMap"))));

    RequestBuilder request = post("/minerva/api/projects/{projectId}/", TEST_ADVANCED_MAP_PROJECT)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);
    return request;
  }

  private void createZip(final String sqlFile, final String imageDirectory, final String testZip) throws IOException {
    FileOutputStream fos = new FileOutputStream(testZip);
    ZipOutputStream zipOut = new ZipOutputStream(fos);

    zipFile(new File(sqlFile), sqlFile, zipOut);
    zipFile(new File(imageDirectory), new File(imageDirectory).getName(), zipOut);

    zipOut.close();
    fos.close();
  }

  private static void zipFile(final File fileToZip, final String fileName, final ZipOutputStream zipOut) throws IOException {
    if (fileToZip.isHidden()) {
      return;
    }
    if (fileToZip.isDirectory()) {
      if (fileName.endsWith("/")) {
        zipOut.putNextEntry(new ZipEntry(fileName));
        zipOut.closeEntry();
      } else {
        zipOut.putNextEntry(new ZipEntry(fileName + "/"));
        zipOut.closeEntry();
      }
      File[] children = fileToZip.listFiles();
      for (final File childFile : children) {
        zipFile(childFile, fileName + "/" + childFile.getName(), zipOut);
      }
      return;
    }
    FileInputStream fis = new FileInputStream(fileToZip);
    ZipEntry zipEntry = new ZipEntry(fileName);
    zipOut.putNextEntry(zipEntry);
    byte[] bytes = new byte[1024];
    int length;
    while ((length = fis.read(bytes)) >= 0) {
      zipOut.write(bytes, 0, length);
    }
    fis.close();
  }

  private String filterImportantStatemants(final String string) throws FileNotFoundException, IOException {
    List<String> dropStatementsBeginning = Arrays.asList(
        "SET DATABASE",
        "SET FILES",
        "CREATE USER",
        "ALTER USER",
        "CREATE SCHEMA",
        "SET SCHEMA",
        "CREATE SEQUENCE",
        "CREATE MEMORY TABLE",
        "ALTER TABLE",
        "GRANT USAGE",
        "GRANT DBA",
        "CREATE INDEX",
        "INSERT INTO BLOCKS",
        "INSERT INTO LOBS",
        "INSERT INTO LOB_IDS",
        "INSERT INTO \"flyway_schema_history",
        "INSERT INTO CACHE_TYPE_TABLE",
        "INSERT INTO CONFIGURATION_OPTION_TABLE",
        "INSERT INTO DATA_OVERLAY_IMAGE_LAYER_TABLE VALUES(1,5,3,'normal')",
        "INSERT INTO LAYOUT_TABLE VALUES(5,'Normal',NULL,TRUE,0.0E0,NULL,FALSE,NULL,NULL,FALSE,FALSE,1,'UNKNOWN',4,'GENERIC')",
        "INSERT INTO MODEL_DATA_TABLE VALUES(3,14285.0E0,NULL,256,25195.0E0,7,4,NULL,NULL,NULL,NULL,NULL,NULL)",
        "INSERT INTO PRIVILEGE_TABLE VALUES(169,NULL,'IS_ADMIN')",
        "INSERT INTO PRIVILEGE_TABLE VALUES(170,NULL,'IS_CURATOR')",
        "INSERT INTO PRIVILEGE_TABLE VALUES(171,NULL,'CAN_CREATE_OVERLAYS')",
        "INSERT INTO PRIVILEGE_TABLE VALUES(172,'empty','READ_PROJECT')",
        "INSERT INTO PRIVILEGE_TABLE VALUES(173,'empty','WRITE_PROJECT')",
        "INSERT INTO PRIVILEGE_TABLE VALUES(174,'*','WRITE_PROJECT')",
        "INSERT INTO PRIVILEGE_TABLE VALUES(175,'*','READ_PROJECT')",
        "INSERT INTO PROJECT_TABLE VALUES(4,'empty',100.0E0,'a2e4822a98337283e39f7b60acf85ec9',"
            + "NULL,NULL,NULL,FALSE,'empty DISEASE MAP',NULL,NULL,'DONE',NULL,'OPEN_LAYERS','2014-03-27 15:59:31.321000',1)",
        "INSERT INTO USER_PRIVILEGE_MAP_TABLE VALUES(1,169)",
        "INSERT INTO USER_PRIVILEGE_MAP_TABLE VALUES(1,170)",
        "INSERT INTO USER_PRIVILEGE_MAP_TABLE VALUES(1,172)",
        "INSERT INTO USER_PRIVILEGE_MAP_TABLE VALUES(1,173)",
        "INSERT INTO USER_PRIVILEGE_MAP_TABLE VALUES(3,172)",
        "INSERT INTO USER_TABLE VALUES(1,'$2a$10$AGem45sdWGs2xJ1sU.J3eOcaLwvLIIm3.D9DbRY2T0vUpyouNw8iW',"
            + "NULL,'admin',NULL,NULL,FALSE,NULL,NULL,NULL,NULL,FALSE,NULL,FALSE)",
        "INSERT INTO USER_TABLE VALUES(3,'$2a$10$gyuDgRZ1wwuFe67DHnkMruhOCKJp1KBzQLeQSJ57WAehJXmhkreRi',"
            + "NULL,'anonymous',NULL,NULL,FALSE,NULL,NULL,NULL,NULL,FALSE,NULL,FALSE)"

    );
    StringBuilder result = new StringBuilder("SET DATABASE REFERENTIAL INTEGRITY FALSE;\n");
    try (final BufferedReader br = new BufferedReader(new FileReader(string))) {
      String line;
      while ((line = br.readLine()) != null) {
        boolean discard = false;
        for (final String string2 : dropStatementsBeginning) {
          if (line.startsWith(string2)) {
            discard = true;
          }
        }
        if (!discard) {
          result.append(line + ";\n");
        }
      }
    }
    result.append("SET DATABASE REFERENTIAL INTEGRITY TRUE;\n");
    return result.toString();
  }
}
