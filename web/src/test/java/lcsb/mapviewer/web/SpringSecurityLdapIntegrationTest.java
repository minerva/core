package lcsb.mapviewer.web;

import com.unboundid.ldap.sdk.LDAPException;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("ldapTest")
@RunWith(SpringJUnit4ClassRunner.class)
public class SpringSecurityLdapIntegrationTest extends ControllerIntegrationTest {

  private static final String LOCAL_PASSWORD = "xxx";

  @Autowired
  private IUserService userService;

  @Before
  public void setUp() throws LDAPException {
  }

  @After
  public void tearDown() throws Exception {
    removeUser(userService.getUserByLogin(LdapServiceTestConfiguration.TEST_LOGIN));
  }

  @Test
  public void testInvalidLoginFromLdap() throws Exception {
    int count = userService.getUsers(false).size();

    RequestBuilder request = post("/minerva/api/doLogin")
        .param("login", LdapServiceTestConfiguration.TEST_LOGIN)
        .param("password", LdapServiceTestConfiguration.TEST_INVALID_PASSWD);
    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());

    assertEquals("No users should be added to the system after failed login attempt",
        count, userService.getUsers(false).size());
  }

  @Test
  public void testLoginFromLdap() throws Exception {
    RequestBuilder request = post("/minerva/api/doLogin")
        .param("login", LdapServiceTestConfiguration.TEST_LOGIN)
        .param("password", LdapServiceTestConfiguration.TEST_PASSWD);
    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    User user = userService.getUserByLogin(LdapServiceTestConfiguration.TEST_LOGIN);
    assertNotNull("After authentication from LDAP user is not present in the system", user);
    assertTrue("LDAP user password should be empty",
        user.getCryptedPassword() == null || user.getCryptedPassword().isEmpty());
    assertTrue(user.isConnectedToLdap());
  }

  @Test
  public void testLoginCaseInsensitiveFromLdap() throws Exception {
    int count = userService.getUsers(false).size();

    RequestBuilder request = post("/minerva/api/doLogin")
        .param("login", LdapServiceTestConfiguration.TEST_LOGIN)
        .param("password", LdapServiceTestConfiguration.TEST_PASSWD);
    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    request = post("/minerva/api/doLogin")
        .param("login", LdapServiceTestConfiguration.TEST_LOGIN.toLowerCase())
        .param("password", LdapServiceTestConfiguration.TEST_PASSWD);
    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    request = post("/minerva/api/doLogin")
        .param("login", LdapServiceTestConfiguration.TEST_LOGIN.toUpperCase())
        .param("password", LdapServiceTestConfiguration.TEST_PASSWD);
    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals("LDAP login is case insensitive and no new user should be added for different cases",
        count + 1, userService.getUsers(false).size());
  }

  @Test
  public void testLocalAccountShouldntAuthenticateFromLdap() throws Exception {
    createUser(LdapServiceTestConfiguration.TEST_LOGIN, LOCAL_PASSWORD);

    RequestBuilder request = post("/minerva/api/doLogin")
        .param("login", LdapServiceTestConfiguration.TEST_LOGIN)
        .param("password", LdapServiceTestConfiguration.TEST_PASSWD);
    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testLdapAccountShouldntAuthenticateFromLocal() throws Exception {
    User user = createUser(LdapServiceTestConfiguration.TEST_LOGIN, LOCAL_PASSWORD);
    user.setConnectedToLdap(true);
    userService.update(user);

    try {
      RequestBuilder request = post("/minerva/api/doLogin")
          .param("login", LdapServiceTestConfiguration.TEST_LOGIN)
          .param("password", LdapServiceTestConfiguration.TEST_PASSWD);
      mockMvc.perform(request)
          .andExpect(status().is2xxSuccessful());

      request = post("/minerva/api/doLogin")
          .param("login", LdapServiceTestConfiguration.TEST_LOGIN)
          .param("password", LOCAL_PASSWORD);
      mockMvc.perform(request)
          .andExpect(status().is4xxClientError());
    } finally {
      removeUser(user);
    }

  }

}
