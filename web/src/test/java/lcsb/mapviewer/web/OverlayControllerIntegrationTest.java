package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IDataOverlayService;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.restdocs.request.PathParametersSnippet;
import org.springframework.restdocs.request.RequestParametersSnippet;
import org.springframework.restdocs.snippet.Snippet;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.patch;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class OverlayControllerIntegrationTest extends ControllerIntegrationTest {

  private static final String TEST_CURATOR_PASSWORD = "test_curator_pass";
  private static final String TEST_CURATOR_LOGIN = "test_curator";

  private static final String NO_ACCESS_USER_PASSWORD = "no_access_pass";
  private static final String NO_ACCESS_USER_LOGIN = "no_access_user";

  @Autowired
  private IDataOverlayService dataOverlayService;

  @Autowired
  private IUserService userService;

  private Project project;
  private ModelData map;
  private Reaction reaction;
  private Element element;
  private User curator;
  private User user;
  private User noAccessUser;

  @Autowired
  private ProjectSnippets snippets;

  @Before
  public void setup() throws Exception {
    project = createAndPersistProject(TEST_PROJECT);
    map = project.getTopModel().getModelData();
    reaction = map.getReactions().iterator().next();
    element = map.getElements().iterator().next();
    curator = createCurator(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD, project);
    user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD, project);
    noAccessUser = createUser(NO_ACCESS_USER_LOGIN, NO_ACCESS_USER_PASSWORD);
    assertNotNull(project);
  }

  @After
  public void tearDown() throws Exception {
    removeProject(project);
    removeUser(curator);
    removeUser(user);
    removeUser(noAccessUser);
  }

  @Test
  public void testDocsListOverlaysByCreator() throws Exception {

    createOverlay(TEST_PROJECT, user);

    createOverlay(TEST_PROJECT, curator);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/overlays/?creator=" + TEST_CURATOR_LOGIN, TEST_PROJECT)
        .session(session);

    final String response = mockMvc.perform(request)
        .andDo(document("projects/project_overlays/get_all_by_creator",
            getProjectPathParameters(),
            getOverlayFilter(),
            listOfOverlayFields()))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    final List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals("Curator has only one overlay", 1, result.size());
  }

  @Test
  public void testListOverlaysByPublicFlag() throws Exception {
    final DataOverlay layout = createOverlay(TEST_PROJECT, curator);
    layout.setPublic(true);

    final DataOverlay curatorLayout = createOverlay(TEST_PROJECT, curator);
    curatorLayout.setPublic(false);
    dataOverlayService.updateDataOverlay(layout);
    dataOverlayService.updateDataOverlay(curatorLayout);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/overlays/?publicOverlay=true")
        .session(session);

    final String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    final List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals("There is only one public overlay", 1, result.size());
  }

  @Test
  public void testDocsCuratorShouldSeeAllOverlays() throws Exception {
    final DataOverlay layout = createOverlay(TEST_PROJECT, project.getOwner());
    layout.setPublic(true);

    final DataOverlay userLayout = createOverlay(TEST_PROJECT, user);
    userLayout.setPublic(false);
    dataOverlayService.updateDataOverlay(layout);
    dataOverlayService.updateDataOverlay(userLayout);

    final MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/overlays/", TEST_PROJECT)
        .session(session);

    final String response = mockMvc.perform(request)
        .andDo(document("projects/project_overlays/get_all",
            getProjectPathParameters(),
            getOverlayFilter(),
            listOfOverlayFields()))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    final List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals("Curator should see all overlays", 2, result.size());
  }

  @Test
  public void testUserShouldSeeAllPublicOverlaysAndOverlays() throws Exception {
    final DataOverlay layout = createOverlay(TEST_PROJECT,
        userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    layout.setPublic(true);

    final DataOverlay hiddenLayout = createOverlay(TEST_PROJECT,
        userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    hiddenLayout.setPublic(false);

    final DataOverlay userLayout = createOverlay(TEST_PROJECT, user);
    userLayout.setPublic(false);

    dataOverlayService.updateDataOverlay(layout);
    dataOverlayService.updateDataOverlay(hiddenLayout);
    dataOverlayService.updateDataOverlay(userLayout);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/overlays/")
        .session(session);

    final String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    final List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals("User should see his own overlays and public ones", 2, result.size());
  }

  @Test
  public void testNoPrivilegesShouldResultInForbidden() throws Exception {
    final MockHttpSession session = createSession(NO_ACCESS_USER_LOGIN, NO_ACCESS_USER_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/overlays/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDocsAdminCanAccessUserOverlay() throws Exception {
    final DataOverlay overlay = createOverlay(TEST_PROJECT, user);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/overlays/{overlayId}/", TEST_PROJECT, overlay.getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_overlays/get_by_id",
            getOverlayPathParameters(),
            responseFields(overlayFields())))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testCuratorCanAccessUserOverlay() throws Exception {
    final DataOverlay overlay = createOverlay(TEST_PROJECT, user);

    final MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testUserCanAccessHisOwnOverlay() throws Exception {
    final DataOverlay overlay = createOverlay(TEST_PROJECT, user);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testUserCannotAccessAnotherUserOverlay() throws Exception {
    final DataOverlay overlay = createOverlay(TEST_PROJECT, curator);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDocsGetBioEntitiesForOverlay() throws Exception {
    final ModelData submap = map.getSubmodels().iterator().next().getSubmodel();
    final String content = createOverlayContentForAllEntities(map);
    final DataOverlay overlay = createOverlay(project, user, content);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/",
        TEST_PROJECT, overlay.getId(), "*")
        .session(session);

    final String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("projects/project_overlays/get_bio_entities",
            getOverlayMapPathParameters(),
            listOfOverlayBioEntitiesFields()))
        .andReturn().getResponse().getContentAsString();

    final int count = map.getElements().size() + map.getReactions().size()
        + submap.getElements().size() + submap.getReactions().size();

    final List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals("All bioEntities should be returned", count, result.size());
  }

  @Test
  public void testGetBioEntitiesForSpecificSubmap() throws Exception {
    final List<ModelData> models = new ArrayList<>(project.getModels());
    final ModelData map1 = models.get(0);
    final ModelData map2 = models.get(1);
    final DataOverlay overlay = createOverlay(user);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder requestAll = get(
        "/minerva/api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/",
        TEST_PROJECT, overlay.getId(), "*")
        .session(session);

    final RequestBuilder request1 = get(
        "/minerva/api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/",
        TEST_PROJECT, overlay.getId(), map1.getId())
        .session(session);

    final RequestBuilder request2 = get(
        "/minerva/api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/",
        TEST_PROJECT, overlay.getId(), map2.getId())
        .session(session);

    final String responseAll = mockMvc.perform(requestAll)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    final String response1 = mockMvc.perform(request1)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    final String response2 = mockMvc.perform(request2)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    final List<Map<String, Object>> resultAll = objectMapper.readValue(responseAll, new TypeReference<List<Map<String, Object>>>() {
    });

    final List<Map<String, Object>> result1 = objectMapper.readValue(response1, new TypeReference<List<Map<String, Object>>>() {
    });

    final List<Map<String, Object>> result2 = objectMapper.readValue(response2, new TypeReference<List<Map<String, Object>>>() {
    });

    final int countAll = resultAll.size();
    final int count1 = result1.size();
    final int count2 = result2.size();

    assertTrue(String.format("Too many results %d < %d + %d", countAll, count1, count2), countAll >= count1 + count2);
  }

  @Test
  public void testGetBioEntitiesForOverlayAsUser() throws Exception {
    final ModelData submap = map.getSubmodels().iterator().next().getSubmodel();
    final String content = createOverlayContentForAllEntities(map);
    final DataOverlay overlay = createOverlay(project, user, content);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/",
        TEST_PROJECT,
        overlay.getId(),
        "*")
        .session(session);

    final String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    final int count = map.getElements().size() + map.getReactions().size()
        + submap.getElements().size() + submap.getReactions().size();

    final List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals("All bioEntities should be returned", count, result.size());
  }

  private String createOverlayContentForAllEntities(final ModelData map) {
    String content = "element_identifier\tvalue\n";
    for (final ModelSubmodelConnection submapConnection : map.getSubmodels()) {
      final ModelData submap = submapConnection.getSubmodel();
      for (final Reaction entity : submap.getReactions()) {
        content += entity.getElementId() + "\t" + "-1\n";
      }
      for (final Element entity : submap.getElements()) {
        content += entity.getElementId() + "\t" + "1\n";
      }
    }
    for (final Reaction entity : map.getReactions()) {
      content += entity.getElementId() + "\t" + "-1\n";
    }
    for (final Element entity : map.getElements()) {
      content += entity.getElementId() + "\t" + "1\n";
    }
    return content;
  }

  @Test
  public void testGetBioEntitiesForInvalidOverlayId() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/overlays/ /models/ /bioEntities/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testGetBioEntitiesAsAnonymousForInvalidOverlayId() throws Exception {
    final RequestBuilder request = get("/minerva/api/projects/" + BUILT_IN_PROJECT + "/overlays/ /models/ /bioEntities/");

    // it should be either 404 or 403 - depending how permissions are checked on
    // invalid overlayId
    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testGetBioEntitiesForOverlayAsCurator() throws Exception {
    final ModelData submap = map.getSubmodels().iterator().next().getSubmodel();
    final String content = createOverlayContentForAllEntities(map);
    final DataOverlay overlay = createOverlay(project, user, content);

    final MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/",
        TEST_PROJECT,
        overlay.getId(),
        "*")
        .session(session);

    final String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    final int count = map.getElements().size() + map.getReactions().size()
        + submap.getElements().size() + submap.getReactions().size();

    final List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals("All bioEntities should be returned", count, result.size());
  }

  @Test
  public void testDocsGetReactionForOverlay() throws Exception {
    final DataOverlay overlay = createOverlay(user);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/reactions/{reactionId}/",
        TEST_PROJECT, overlay.getId(), map.getId(), reaction.getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_overlays/get_reaction",
            getOverlayReactionPathParameters(),
            listOfOverlayBioEntitiesFields()))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
  }

  @Test
  public void testGetReactionForOverlayWithWidth() throws Exception {
    final String reactionId = reaction.getElementId();
    final DataOverlay overlay = createOverlay(project, user, "element_identifier\tvalue\tline_width\n" + reactionId + "\t1\t6");

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/reactions/{reactionId}/",
        TEST_PROJECT, overlay.getId(), map.getId(), reaction.getId())
        .session(session);

    final String json = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    final List<Map<String, Object>> objects = objectMapper.readValue(json, new TypeReference<List<Map<String, Object>>>() {
    });
    final Map<?, ?> content = (Map<?, ?>) objects.get(0).get("overlayContent");
    assertTrue(content.containsKey("width"));

  }

  private PathParametersSnippet getOverlayReactionPathParameters() {
    return getOverlayMapPathParameters().and(parameterWithName("reactionId").description("reaction identifier"));
  }

  @Test
  public void testDocsGetReactionForOverlayWithColor() throws Exception {
    final DataOverlay overlay = createOverlayWithColor(project, user);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/reactions/{reactionId}/",
        TEST_PROJECT, overlay.getId(), map.getId(), reaction.getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("{method-name}",
            getOverlayReactionPathParameters(),
            listOfOverlayBioEntitiesFields()))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
  }

  @Test
  public void testGetReactionsForOverlayAsUser() throws Exception {
    final DataOverlay overlay = createOverlay(user);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/reactions/{reactionId}/",
        TEST_PROJECT, overlay.getId(), map.getId(), reaction.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testGetReactionsForOverlayAsCurator() throws Exception {
    final DataOverlay overlay = createOverlay(user);

    final MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/reactions/{reactionId}/",
        TEST_PROJECT, overlay.getId(), map.getId(), reaction.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDocsGetElementsForOverlay() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    final DataOverlay overlay = createOverlay(admin);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/elements/{elementId}/",
        TEST_PROJECT, overlay.getId(), map.getId(), element.getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_overlays/get_element",
            getOverlayMapPathParameters().and(parameterWithName("elementId").description("element identifier")),
            listOfOverlayBioEntitiesFields()))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testGetElementsForOverlayAsUser() throws Exception {
    final DataOverlay overlay = createOverlay(user);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/elements/{elementId}/",
        TEST_PROJECT, overlay.getId(), map.getId(), element.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testGetElementsForOverlayAsCurator() throws Exception {
    final DataOverlay overlay = createOverlay(user);

    final MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/elements/{elementId}/",
        TEST_PROJECT, overlay.getId(), map.getId(), element.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDocsAdminCreateOverlay() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    final UploadedFileEntry file = createFile("element_identifier\tvalue\n\t-1", admin);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("fileId", String.valueOf(file.getId())),
        new BasicNameValuePair("name", "overlay name"),
        new BasicNameValuePair("description", "overlay name"),
        new BasicNameValuePair("filename", "overlay name"),
        new BasicNameValuePair("type", "GENERIC"))));

    final RequestBuilder request = post("/minerva/api/projects/{projectId}/overlays/", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_overlays/create_from_file",
            getProjectPathParameters(),
            getCreateOverlayRequestParameters(),
            responseFields(overlayFields())))
        .andExpect(status().is2xxSuccessful());

    assertEquals(1, dataOverlayService.getDataOverlaysByProject(project).size());
  }

  @Test
  public void testDocsAdminCreateOverlayFromContent() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("content", "element_identifier\tvalue\n\t-1"),
        new BasicNameValuePair("name", "overlay name"),
        new BasicNameValuePair("description", "overlay description"),
        new BasicNameValuePair("filename", "source.txt"),
        new BasicNameValuePair("type", "GENERIC"))));

    final RequestBuilder request = post("/minerva/api/projects/{projectId}/overlays/", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_overlays/create_from_content",
            getProjectPathParameters(),
            getCreateOverlayRequestParameters(),
            responseFields(overlayFields())))
        .andExpect(status().is2xxSuccessful());

    assertEquals(1, dataOverlayService.getDataOverlaysByProject(project).size());
  }

  @Test
  public void testCuratorCreateOverlay() throws Exception {
    final UploadedFileEntry file = createFile("element_identifier\tvalue\n\t-1", curator);

    try {
      final MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

      final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("fileId", String.valueOf(file.getId())),
          new BasicNameValuePair("name", "overlay name"),
          new BasicNameValuePair("description", "overlay name"),
          new BasicNameValuePair("filename", "overlay name"),
          new BasicNameValuePair("type", "GENERIC"))));

      final RequestBuilder request = post("/minerva/api/projects/" + TEST_PROJECT + "/overlays/")
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(session);

      mockMvc.perform(request)
          .andExpect(status().is2xxSuccessful());

      assertEquals(1, dataOverlayService.getDataOverlaysByProject(project).size());
    } finally {
      removeFile(file);
    }
  }

  @Test
  public void testUserCreateOverlay() throws Exception {

    final UploadedFileEntry file = createFile("element_identifier\tvalue\n\t-1", user);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("fileId", String.valueOf(file.getId())),
        new BasicNameValuePair("name", "overlay name"),
        new BasicNameValuePair("description", "overlay name"),
        new BasicNameValuePair("filename", "overlay name"),
        new BasicNameValuePair("type", "GENERIC"))));

    final RequestBuilder request = post("/minerva/api/projects/" + TEST_PROJECT + "/overlays/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(1, dataOverlayService.getDataOverlaysByProject(project).size());
  }

  @Test
  public void testUserWithoutAccessToProjectCannotCreateOverlay() throws Exception {
    final UploadedFileEntry file = createFile("element_identifier\tvalue\n\t-1", user);

    final MockHttpSession session = createSession(NO_ACCESS_USER_LOGIN, NO_ACCESS_USER_PASSWORD);

    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("fileId", String.valueOf(file.getId())),
        new BasicNameValuePair("name", "overlay name"),
        new BasicNameValuePair("description", "overlay name"),
        new BasicNameValuePair("filename", "overlay name"),
        new BasicNameValuePair("type", "GENERIC"))));

    final RequestBuilder request = post("/minerva/api/projects/" + TEST_PROJECT + "/overlays/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDocsAdminCanRemoveOverlay() throws Exception {
    final DataOverlay overlay = createOverlay(user);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/api/projects/{projectId}/overlays/{overlayId}", TEST_PROJECT, overlay.getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_overlays/delete_overlay",
            getOverlayPathParameters()))
        .andExpect(status().is2xxSuccessful());
    assertEquals(0, dataOverlayService.getDataOverlaysByProject(project).size());
  }

  @Test
  public void testCuratorCanRemoveOverlay() throws Exception {
    final DataOverlay overlay = createOverlay(user);

    final MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    final RequestBuilder request = delete("/minerva/api/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
    assertEquals(0, dataOverlayService.getDataOverlaysByProject(project).size());
  }

  @Test
  public void testUserCanRemoveOwnOverlay() throws Exception {
    final DataOverlay overlay = createOverlay(user);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = delete("/minerva/api/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
    assertEquals(0, dataOverlayService.getDataOverlaysByProject(project).size());
  }

  @Test
  public void testUserCannotRemoveOtherUserOverlays() throws Exception {
    final DataOverlay overlay = createOverlay(curator);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = delete("/minerva/api/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDocsAdminCanUpdateOverlay() throws Exception {
    final DataOverlay overlay = createOverlay(user);

    final Map<String, Object> updateOverlay =
        objectMapper.readValue(objectMapper.writeValueAsString(overlay), new TypeReference<Map<String, Object>>() {
        });
    updateOverlay.remove("idObject");
    updateOverlay.remove("defaultOverlay");
    updateOverlay.remove("images");
    updateOverlay.remove("genomeType");
    updateOverlay.remove("genomeVersion");
    updateOverlay.remove("inputDataAvailable");

    final String body = "{\"overlay\":" + objectMapper.writeValueAsString(updateOverlay) + "}";

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = patch("/minerva/api/projects/{projectId}/overlays/{overlayId}", TEST_PROJECT, overlay.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_overlays/update_overlay",
            getOverlayPathParameters(),
            getUpdateOverlayRequestParameters(),
            responseFields(overlayFields())))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testCuratorCanUpdateOverlay() throws Exception {
    final DataOverlay overlay = createOverlay(user);
    final String body = "{\"overlay\":{}}";

    final MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    final RequestBuilder request = patch("/minerva/api/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testUserCanUpdateOwnOverlay() throws Exception {
    final DataOverlay overlay = createOverlay(user);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    final String body = "{\"overlay\":{}}";

    final RequestBuilder request = patch("/minerva/api/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testUpdateInvalidName() throws Exception {
    final DataOverlay overlay = createOverlay(user);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    final String body = "{\"overlay\":{\"name\":\"\"}}";

    final RequestBuilder request = patch("/minerva/api/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testUserCannotUpdateOtherUserOverlays() throws Exception {
    final DataOverlay overlay = createOverlay(curator);
    final String body = "{\"overlay\":{}}";

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = patch("/minerva/api/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDocsAdminCanAccessOverlaySource() throws Exception {
    final DataOverlay overlay = createOverlay(user);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/overlays/{overlayId}:downloadSource", TEST_PROJECT,
        overlay.getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_overlays/download_source",
            getOverlayPathParameters()))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testCuratorCanAccessOverlaySource() throws Exception {
    final DataOverlay overlay = createOverlay(user);

    final MockHttpSession session = createSession(TEST_CURATOR_LOGIN, TEST_CURATOR_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + ":downloadSource")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testUserCanAccessOwnOverlaySource() throws Exception {
    final DataOverlay overlay = createOverlay(user);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + ":downloadSource")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testUserCannotAccessOtherUserOverlaysSource() throws Exception {
    final DataOverlay overlay = createOverlay(curator);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + ":downloadSource")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testOverlayCreatedAsFirstShouldHaveProperOrderIndex() throws Exception {
    createOverlay(user);

    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final UploadedFileEntry file = createFile("element_identifier\tvalue\n\t-1", admin);
    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("fileId", String.valueOf(file.getId())),
        new BasicNameValuePair("name", "overlay name"),
        new BasicNameValuePair("description", "overlay name"),
        new BasicNameValuePair("filename", "overlay name"),
        new BasicNameValuePair("type", "GENERIC"))));

    final RequestBuilder request = post("/minerva/api/projects/" + TEST_PROJECT + "/overlays/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    final String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    final Map<String, Object> result = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });

    final int orderNumber = (int) result.get("order");

    assertEquals("First user data overlay should be ordered with 1", 1, orderNumber);
  }

  @Test
  public void testUserCanAccessPublicOverlay() throws Exception {
    final DataOverlay overlay = createOverlay(TEST_PROJECT, project.getOwner());
    overlay.setPublic(true);
    dataOverlayService.updateDataOverlay(overlay);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId() + "/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testUserCanAccessDataInPublicOverlay() throws Exception {
    final DataOverlay overlay = createOverlay(project.getOwner());
    overlay.setPublic(true);
    dataOverlayService.updateDataOverlay(overlay);
    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/overlays/{overlayId}/models/*/bioEntities/",
        TEST_PROJECT,
        overlay.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testUserCanAccessReactionDataInPublicOverlay() throws Exception {
    final DataOverlay overlay = createOverlay(project.getOwner());
    overlay.setPublic(true);
    dataOverlayService.updateDataOverlay(overlay);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/reactions/-1/",
        TEST_PROJECT,
        overlay.getId(),
        map.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testUserCanAccessElementDataInPublicOverlay() throws Exception {
    final DataOverlay overlay = createOverlay(project.getOwner());
    overlay.setPublic(true);
    dataOverlayService.updateDataOverlay(overlay);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/elements/-1/",
        TEST_PROJECT,
        overlay.getId(),
        map.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testUserCanDownloadSourceOfPublicOverlay() throws Exception {
    final DataOverlay overlay = createOverlay(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    overlay.setPublic(true);
    dataOverlayService.updateDataOverlay(overlay);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/overlays/{overlayId}:downloadSource/",
        TEST_PROJECT,
        overlay.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  private DataOverlay createOverlay(final User user) throws Exception {
    return createOverlay(TEST_PROJECT, user);
  }

  @Test
  public void testListOverlaysWithUnknownProject() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/*/overlays/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetOverlayByIdWithUnknownProject() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    final DataOverlay overlay = createOverlay(admin);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/*/overlays/" + overlay.getId() + "/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetBioEntitiesForOverlayWithUndefinedProject() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    final DataOverlay overlay = createOverlay(admin);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/*/overlays/{overlayId}/models/{mapId}/bioEntities/",
        overlay.getId(),
        map.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetReactionsForOverlayWithUndefinedProject() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    final DataOverlay overlay = createOverlay(admin);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/api/projects/*/overlays/{overlayId}/models/{mapId}/bioEntities/reactions/{reactionId}/",
        overlay.getId(),
        map.getId(),
        reaction.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetElementsForOverlayWithUndefinedProject() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    final DataOverlay overlay = createOverlay(admin);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/*/overlays/" + overlay.getId() + "/models/" + map.getId()
        + "/bioEntities/elements/" + element.getId() + "/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testCreateOverlayWithUndefinedProjectId() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    final UploadedFileEntry file = createFile("element_identifier\tvalue\n\t-1", admin);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("fileId", String.valueOf(file.getId())),
        new BasicNameValuePair("name", "overlay name"),
        new BasicNameValuePair("description", "overlay name"),
        new BasicNameValuePair("filename", "overlay name"),
        new BasicNameValuePair("type", "GENERIC"))));

    final RequestBuilder request = post("/minerva/api/projects/*/overlays/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testRemoveOverlayWithUndefinedProject() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    final DataOverlay overlay = createOverlay(admin);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/api/projects/*/overlays/" + overlay.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testUpdateOverlayWithUndefinedProject() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    final DataOverlay overlay = createOverlay(admin);

    final String body = "{\"overlay\":{}}";

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = patch("/minerva/api/projects/*/overlays/" + overlay.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testAccessOverlaySourceWithUndefinedProject() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    final DataOverlay overlay = createOverlay(admin);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/*/overlays/" + overlay.getId() + ":downloadSource")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testOrderAfterRemoveOverlay() throws Exception {
    final DataOverlay overlay1 = createOverlay(user);
    overlay1.setOrderIndex(1);
    final DataOverlay overlay2 = createOverlay(user);
    overlay2.setOrderIndex(2);
    final DataOverlay overlay3 = createOverlay(user);
    overlay3.setOrderIndex(3);

    dataOverlayService.updateDataOverlay(overlay1);
    dataOverlayService.updateDataOverlay(overlay2);
    dataOverlayService.updateDataOverlay(overlay3);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/api/projects/" + TEST_PROJECT + "/overlays/" + overlay2.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
    assertEquals(1, dataOverlayService.getDataOverlayById(overlay1.getId()).getOrderIndex());
    assertEquals(2, dataOverlayService.getDataOverlayById(overlay3.getId()).getOrderIndex());
  }

  @Test
  public void testOrderAfterRemoveOverlayForPublicOverlay() throws Exception {
    final DataOverlay overlay1 = createOverlay(project.getOwner());
    overlay1.setPublic(true);
    overlay1.setOrderIndex(1);
    final DataOverlay overlay2 = createOverlay(project.getOwner());
    overlay2.setPublic(true);
    overlay2.setOrderIndex(2);
    final DataOverlay overlay3 = createOverlay(project.getOwner());
    overlay3.setPublic(true);
    overlay3.setOrderIndex(3);

    dataOverlayService.updateDataOverlay(overlay1);
    dataOverlayService.updateDataOverlay(overlay2);
    dataOverlayService.updateDataOverlay(overlay3);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/api/projects/" + TEST_PROJECT + "/overlays/" + overlay2.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
    assertEquals(1, dataOverlayService.getDataOverlayById(overlay1.getId()).getOrderIndex());
    assertEquals(2, dataOverlayService.getDataOverlayById(overlay3.getId()).getOrderIndex());
  }

  @Test
  public void testChangeOrderAfterChaningOwner() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    final DataOverlay overlay1 = createOverlay(user);
    overlay1.setOrderIndex(1);
    final DataOverlay overlay2 = createOverlay(user);
    overlay2.setOrderIndex(2);

    final DataOverlay overlay3 = createOverlay(admin);
    overlay3.setOrderIndex(1);
    final DataOverlay overlay4 = createOverlay(admin);
    overlay4.setOrderIndex(2);

    dataOverlayService.updateDataOverlay(overlay1);
    dataOverlayService.updateDataOverlay(overlay2);
    dataOverlayService.updateDataOverlay(overlay3);
    dataOverlayService.updateDataOverlay(overlay4);

    final String body = "{\"overlay\":{\"creator\":\"" + admin.getLogin() + "\"}}";

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = patch("/minerva/api/projects/" + TEST_PROJECT + "/overlays/" + overlay1.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals("Order of overlays wasn't updated for original owner of overlay", 1,
        dataOverlayService.getDataOverlayById(overlay2.getId()).getOrderIndex());

    assertTrue(dataOverlayService.getDataOverlayById(overlay1.getId()).getOrderIndex() == 1
        || dataOverlayService.getDataOverlayById(overlay3.getId()).getOrderIndex() == 1);
    assertTrue(dataOverlayService.getDataOverlayById(overlay1.getId()).getOrderIndex() == 2
        || dataOverlayService.getDataOverlayById(overlay3.getId()).getOrderIndex() == 2);
    assertEquals(3, dataOverlayService.getDataOverlayById(overlay4.getId()).getOrderIndex());
  }

  @Test
  public void testCreateOverlayAsGuest() throws Exception {
    final UploadedFileEntry file = createFile("element_identifier\tvalue\n\t-1", userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN));

    try {
      final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("fileId", String.valueOf(file.getId())),
          new BasicNameValuePair("name", "overlay name"),
          new BasicNameValuePair("description", "overlay name"),
          new BasicNameValuePair("filename", "overlay name"),
          new BasicNameValuePair("type", "GENERIC"))));

      final RequestBuilder request = post("/minerva/api/projects/" + BUILT_IN_PROJECT + "/overlays/")
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body);

      mockMvc.perform(request)
          .andExpect(status().isForbidden());
    } finally {
      removeFile(file);
    }
  }

  protected RequestParametersSnippet getOverlayFilter() {
    return requestParameters(
        parameterWithName("creator")
            .description("login of the user who created the overlay")
            .optional(),
        parameterWithName("publicOverlay")
            .description(
                "true/false - are we interested in public overlays or private custom data sets (default: false)")
            .optional());
  }

  private ResponseFieldsSnippet listOfOverlayFields() {
    return responseFields().andWithPrefix("[].", overlayFields());
  }

  private List<FieldDescriptor> overlayFields() {
    return Arrays.asList(
        fieldWithPath("id")
            .description("id")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("name")
            .description("name")
            .type(JsonFieldType.STRING),
        fieldWithPath("description")
            .description("description")
            .type(JsonFieldType.STRING),
        fieldWithPath("idObject")
            .description("identifier")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("group")
            .description("overlay group identifier")
            .optional()
            .type(JsonFieldType.NUMBER),
        fieldWithPath("publicOverlay")
            .description("is the data overlay publicly available to all users")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath("order")
            .description("sort order")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("type")
            .description("type; available options: " + new ProjectSnippets().getOptionsAsString(DataOverlayType.class))
            .type(JsonFieldType.STRING),
        fieldWithPath("genomeType")
            .description("reference genome type; available options: "
                + new ProjectSnippets().getOptionsAsString(ReferenceGenomeType.class))
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath("genomeVersion")
            .description("reference genome version")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath("creator")
            .description("login of the user who uploaded data overlay")
            .type(JsonFieldType.STRING)
            .optional());
  }

  private Snippet getCreateOverlayRequestParameters() {
    return requestParameters(
        parameterWithName("name")
            .description("name of the data overlay"),
        parameterWithName("content")
            .description(
                "content of the file that is uploaded with definition what should be visible where (alternative to fileId)")
            .optional(),
        parameterWithName("fileId")
            .description(
                "uploaded file id that should be used as data overlay content content (alternative to content)")
            .optional(),
        parameterWithName("description")
            .description("short description of the data overlay"),
        parameterWithName("filename")
            .description("name of the file that should be used when downloading the source"),
        parameterWithName("type")
            .description("type of the data overlay (default: " + DataOverlayType.GENERIC + "). Available options: "
                + snippets.getOptionsAsString(DataOverlayType.class))
            .optional());
  }

  private Snippet getUpdateOverlayRequestParameters() {
    final List<FieldDescriptor> fields = new ArrayList<>(overlayFields());
    final List<FieldDescriptor> toRemove = new ArrayList<>();
    for (final FieldDescriptor fieldDescriptor : fields) {
      if (fieldDescriptor.getPath().equals("idObject")
          || fieldDescriptor.getPath().equals("inputDataAvailable")
          || fieldDescriptor.getPath().equals("images")
          || fieldDescriptor.getPath().equals("genomeType")
          || fieldDescriptor.getPath().equals("genomeVersion")
          || fieldDescriptor.getPath().equals("defaultOverlay")) {
        toRemove.add(fieldDescriptor);
      }
    }
    fields.removeAll(toRemove);
    return requestFields().andWithPrefix("overlay.", fields);
  }

  private ResponseFieldsSnippet listOfOverlayBioEntitiesFields() {
    return responseFields().andWithPrefix("[].", overlayBioEntityFields());
  }

  private List<FieldDescriptor> overlayBioEntityFields() {
    final List<FieldDescriptor> result = new ArrayList<>(Arrays.asList(
        fieldWithPath("type")
            .description("type of bioEntity (ALIAS/REACTION)")
            .type(JsonFieldType.STRING),
        fieldWithPath("overlayContent.idObject")
            .description("identifier of the bioEntity")
            .type(JsonFieldType.STRING),
        fieldWithPath("overlayContent.uniqueId")
            .description("identifier of the bioEntity")
            .type(JsonFieldType.NUMBER)
            .ignored(),
        fieldWithPath("overlayContent.modelId")
            .description("map identifier where bioEntity is located")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("overlayContent.value")
            .description("normalized value assigned to the bioEntity from overlay")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath("overlayContent.color")
            .description("color assigned to the bioEntity from overlay")
            .optional()
            .type(JsonFieldType.OBJECT),
        fieldWithPath("overlayContent.description")
            .description("description assigned to the bioEntity from overlay")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath("overlayContent.geneVariations")
            .description("list of gene variants assigned to the bioEntity from overlay")
            .type(JsonFieldType.ARRAY)
            .optional(),
        fieldWithPath("overlayContent.type")
            .description("type of data overlay")
            .type(JsonFieldType.STRING)
            .optional()
            .ignored(),
        fieldWithPath("overlayContent.width")
            .description("line width of reaction bioEntity assigned to the bioEntity from overlay")
            .type(JsonFieldType.NUMBER)
            .optional()));
    result.addAll(colorField("overlayContent.color", true, false));
    return result;
  }

  private Collection<FieldDescriptor> colorField(final String originalPrefix, final boolean ignore, final boolean required) {
    String prefix = originalPrefix;
    if (!prefix.isEmpty() && !prefix.endsWith(".")) {
      prefix = prefix + ".";
    }
    final List<FieldDescriptor> result = Arrays.asList(
        fieldWithPath(prefix + "alpha")
            .description("alpha value (0-255)")
            .type(JsonFieldType.NUMBER),
        fieldWithPath(prefix + "rgb")
            .description("RED-GREEN-BLUE value as integer #RRGGBB")
            .type(JsonFieldType.NUMBER));
    if (ignore) {
      for (final FieldDescriptor fieldDescriptor : result) {
        fieldDescriptor.ignored();
      }
    }
    if (!required) {
      for (final FieldDescriptor fieldDescriptor : result) {
        fieldDescriptor.optional();
      }
    }
    return result;
  }

  @Test
  public void testGetBioEntitiesForNonExistingOverlay() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/overlays/-1/models/*/bioEntities/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetReactionForNonExistingOverlay() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/overlays/-1/models/-1/bioEntities/reactions/123/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetReactionForInvalidModelId() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/overlays/-1/models/abc/bioEntities/reactions/123/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testGetElementForInvalidModelId() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/overlays/-1/models/abc/bioEntities/elements/123/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testGetElementForInvalidOverlayId() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/overlays/abc/models/-1/bioEntities/elements/123/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testGetElementForNonExistingOverlay() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/overlays/-1/models/-1/bioEntities/elements/123/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testRemoveNonExistingOverlay() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/api/projects/" + TEST_PROJECT + "/overlays/-1/").session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testUpdateNonExistingOverlay() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final String body = "{\"overlay\":{\"overlayId\":-1}}";
    final RequestBuilder request = patch("/minerva/api/projects/" + TEST_PROJECT + "/overlays/-1/")
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testUpdateIvalidOverlayId() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final String body = "{\"overlay\":{\"overlayId\":-1}}";
    final RequestBuilder request = patch("/minerva/api/projects/" + TEST_PROJECT + "/overlays/bla/")
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testFetchSourceForExistingOverlay() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/overlays/-1:downloadSource").session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testRemoveUserOverlayAsAnonymous() throws Exception {
    final DataOverlay overlay = createOverlay(TEST_PROJECT, user);
    final RequestBuilder request = delete("/minerva/api/projects/" + TEST_PROJECT + "/overlays/" + overlay.getId());

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testFetchGeneticDataOverlayData() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final DataOverlay overlay = createOverlay(project, user, "#TYPE=GENETIC_VARIANT\n"
        + "#GENOME_TYPE=UCSC\n"
        + "#GENOME_VERSION=xenLae2\n"
        + "position\toriginal_dna\talternative_dna\tgene_name\tdescription\tcolor\tcontig\n"
        + "10146\tAC\tA\tGSTA4\tupstream\t#ff0000\tchr1", DataOverlayType.GENETIC_VARIANT);

    assertNotNull(overlay);
    for (final Element elementIterator : map.getElements()) {
      if (elementIterator.getName().equals("GSTA4")) {
        element = elementIterator;
      }
    }
    assertEquals(element.getName(), "GSTA4");

    final RequestBuilder request = get(
        "/minerva/api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/elements/{elementId}/",
        TEST_PROJECT, overlay.getId(), map.getId(), element.getId())
        .session(session);

    final String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    objectMapper.readValue(response, Object.class);
  }

  @Test
  public void testFetchGeneticDataOverlay() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final DataOverlay overlay = createOverlay(project, user, "#TYPE=GENETIC_VARIANT\n"
        + "#GENOME_TYPE=UCSC\n"
        + "#GENOME_VERSION=xenLae2\n"
        + "position\toriginal_dna\talternative_dna\tgene_name\tdescription\tcolor\tcontig\n"
        + "10146\tAC\tA\tGSTA4\tupstream\t#ff0000\tchr1", DataOverlayType.GENETIC_VARIANT);

    assertNotNull(overlay);
    for (final Element elementIterator : map.getElements()) {
      if (elementIterator.getName().equals("GSTA4")) {
        element = elementIterator;
      }
    }
    assertEquals(element.getName(), "GSTA4");

    final RequestBuilder request = get(
        "/minerva/api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/",
        TEST_PROJECT, overlay.getId(), map.getId())
        .session(session);

    final String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    objectMapper.readValue(response, Object.class);
  }

  @Test
  public void testDocsDownloadOverlayLegend() throws Exception {
    final DataOverlay overlay = createOverlay(user);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/overlays/{overlayId}:downloadLegend", TEST_PROJECT,
        overlay.getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_overlays/download_legend",
            getOverlayPathParameters()))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testUpdateOverlayNameTooLong() throws Exception {
    final DataOverlay overlay = createOverlay(user);

    final Map<String, Object> updateOverlay = new HashMap<>();
    updateOverlay.put("name", INVALID_LONG_NAME);

    final String body = "{\"overlay\":" + objectMapper.writeValueAsString(updateOverlay) + "}";

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = patch("/minerva/api/projects/{projectId}/overlays/{overlayId}", TEST_PROJECT, overlay.getId())
        .content(body)
        .contentType(MediaType.APPLICATION_JSON)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testInvalidName() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    final UploadedFileEntry file = createFile("element_identifier\tvalue\n\t-1", admin);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("fileId", String.valueOf(file.getId())),
        new BasicNameValuePair("name", " "),
        new BasicNameValuePair("description", "overlay name"),
        new BasicNameValuePair("filename", "overlay name"),
        new BasicNameValuePair("type", "GENERIC"))));

    final RequestBuilder request = post("/minerva/api/projects/{projectId}/overlays/", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testUpdateWithEmptyBody() throws Exception {
    final DataOverlay overlay = createOverlay(user);

    final String body = "{}";

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = patch("/minerva/api/projects/{projectId}/overlays/{overlayId}", TEST_PROJECT, overlay.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testListOverlaysForNotExitingProject() throws Exception {

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/overlays/", "unknown");

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

}
