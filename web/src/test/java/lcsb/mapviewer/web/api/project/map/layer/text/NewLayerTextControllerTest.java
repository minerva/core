package lcsb.mapviewer.web.api.project.map.layer.text;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.persist.dao.graphics.LayerTextProperty;
import lcsb.mapviewer.services.interfaces.ILayerTextService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiDocs;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.awt.Color;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewLayerTextControllerTest extends ControllerIntegrationTest {

  @Autowired
  private ILayerTextService layerTextService;

  @Autowired
  private IProjectService projectService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private LayerText layerText;

  private int layerId;
  private int textId;
  private int mapId;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    Project project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    final Layer layer = project.getTopModel().getLayers().iterator().next();
    layerId = layer.getId();
    layerText = layer.getTexts().iterator().next();
    textId = layerText.getId();
    projectService.removeBackground(project.getProjectBackgrounds().get(0));
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
    removeProject(TEST_PROJECT_ID);
  }

  @Test
  public void testDocsGetLayerText() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/texts/{textId}",
        TEST_PROJECT, mapId, layerId, textId)
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/projects/maps/layers/texts/get_text",
            getLayerTextPathParameters(),
            responseFields(NewApiDocs.getLayerTextResponse("")))).andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    assertNotNull(result.get("verticalAlign"));
    assertNotNull(result.get("horizontalAlign"));
  }

  @Test
  public void testGetLayerTextWildcardLayer() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/texts/{textId}",
        TEST_PROJECT, mapId, "*", textId)
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    assertNotNull(result.get("verticalAlign"));
    assertNotNull(result.get("horizontalAlign"));
  }

  @Test
  public void testGetNonExisting() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/texts/{textId}",
        TEST_PROJECT, mapId, layerId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetLayerTextWithoutPermission() throws Exception {
    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/texts/{textId}",
        TEST_PROJECT, mapId, layerId, textId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDocsCreateLayerText() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewTextDTO data = createLayerTextDTO();

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/texts/",
        TEST_PROJECT,
        mapId, layerId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andDo(document("new_api/projects/maps/layers/texts/add_text",
            getLayerPathParameters(),
            NewApiDocs.getAddLayerTextRequest(),
            responseFields(NewApiDocs.getLayerTextResponse(""))))
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    final Map<LayerTextProperty, Object> filter = new HashMap<>();
    filter.put(LayerTextProperty.PROJECT_ID, TEST_PROJECT);
    filter.put(LayerTextProperty.MAP_ID, Collections.singletonList(mapId));
    filter.put(LayerTextProperty.LAYER_ID, Collections.singletonList(layerId));
    filter.put(LayerTextProperty.ID, Collections.singletonList(result.get("id")));
    final Page<LayerText> layers = layerTextService.getAll(filter, Pageable.unpaged());

    assertEquals(1, layers.getNumberOfElements());
  }

  private NewTextDTO createLayerTextDTO() {
    final NewTextDTO result = new NewTextDTO();
    result.setNotes("not a note");
    result.setFontSize(4);
    result.setColor(Color.BLUE);
    result.setBorderColor(Color.LIGHT_GRAY);
    result.setHeight(11.1);
    result.setWidth(12.1);
    result.setX(2.2);
    result.setY(3.3);
    result.setZ(5);
    result.setHorizontalAlign(HorizontalAlign.CENTER);
    result.setVerticalAlign(VerticalAlign.MIDDLE);
    return result;
  }

  @Test
  public void testDocsUpdateLayerText() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewTextDTO data = createLayerTextDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/texts/{textId}",
        TEST_PROJECT, mapId, layerId, textId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/projects/maps/layers/texts/update_text",
            getLayerTextPathParameters(),
            NewApiDocs.getAddLayerTextRequest(),
            responseFields(NewApiDocs.getLayerTextResponse(""))))
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final LayerText layerText = layerTextService.getById(textId);
    assertEquals(data.getColor(), layerText.getColor());
  }

  @Test
  public void testUpdateLayerTextWithOldVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewTextDTO data = createLayerTextDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/texts/{textId}",
        TEST_PROJECT, mapId, layerId, textId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdateLayerTextWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String originalVersion = layerText.getEntityVersion() + "";

    final NewTextDTO data = createLayerTextDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/texts/{textId}",
        TEST_PROJECT, mapId, layerId, textId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final LayerText layerText = layerTextService.getById(textId);
    assertNotEquals(originalVersion, layerText.getEntityVersion() + "");
  }

  @Test
  public void testDocsDeleteLayerText() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/texts/{textId}",
        TEST_PROJECT, mapId, layerId, textId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/projects/maps/layers/texts/delete_text",
            getLayerTextPathParameters()));

    assertNull(layerTextService.getById(textId));
  }

  @Test
  public void testDeleteNotExistingLayerText() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/texts/{textId}",
        TEST_PROJECT, mapId, layerId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessLayerText() throws Exception {
    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/texts/{textId}",
        TEST_PROJECT, mapId, layerId, textId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeleteLayerTextWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String originalVersion = layerText.getEntityVersion() + "";

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/texts/{textId}",
        TEST_PROJECT, mapId, layerId, textId)
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
    assertNull(layerTextService.getById(textId));
  }

  @Test
  public void testDeleteLayerTextWithWrongVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/texts/{textId}",
        TEST_PROJECT, mapId, layerId, textId)
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testDocsListLayerTexts() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/texts/",
        TEST_PROJECT, mapId, layerId)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("new_api/projects/maps/layers/texts/list_texts",
            getLayerPathParameters(),
            requestParameters(getPageableFilter()),
            NewApiDocs.getLayerTextsSearchResult()))
        .andExpect(status().isOk());
  }

  @Test
  public void testListLayerTextsWithoutAccess() throws Exception {

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/texts/",
        TEST_PROJECT, mapId, layerId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testGetLayerTextsWithGlyph() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    Project projectWithGlyph = createProjectWithGlyph(TEST_PROJECT_ID);

    Model model = projectWithGlyph.getTopModel();
    Layer layer = model.getLayers().iterator().next();

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/texts/",
        TEST_PROJECT_ID, model.getId(), layer.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }


}
