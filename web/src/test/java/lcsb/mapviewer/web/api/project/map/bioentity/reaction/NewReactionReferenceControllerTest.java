package lcsb.mapviewer.web.api.project.map.bioentity.reaction;

import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.NewMiriamDataDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewReactionReferenceControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IReactionService reactionService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private Project project;

  private int reactionId;
  private int mapId;
  private MiriamData reference;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    Set<Integer> ids = new HashSet<>();
    for (Reaction reaction : project.getTopModelData().getReactions()) {
      ids.add(reaction.getId());
    }
    reactionId = ids.iterator().next();
    reference = project.getTopModel().getReactionByDbId(reactionId).getMiriamData().iterator().next();
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testCreateReference() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewMiriamDataDTO data = createReferenceDTO();

    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/references/",
        TEST_PROJECT,
        mapId,
        reactionId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isCreated());

    Reaction reaction = reactionService.getReactionById(TEST_PROJECT, mapId, reactionId);

    assertEquals(project.getTopModel().getReactionByDbId(reactionId).getMiriamData().size() + 1, reaction.getMiriamData().size());
  }

  private NewMiriamDataDTO createReferenceDTO() {
    NewMiriamDataDTO result = new NewMiriamDataDTO();
    result.setType(MiriamType.PUBMED);
    result.setResource("" + counter++);
    return result;
  }

  @Test
  public void testDeleteReference() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/references/{reference}",
        TEST_PROJECT, mapId, reactionId, reference.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    Reaction reaction = reactionService.getReactionById(TEST_PROJECT, mapId, reactionId);

    assertEquals(project.getTopModel().getReactionByDbId(reactionId).getMiriamData().size() - 1, reaction.getMiriamData().size());
  }

  @Test
  public void testDeleteNotExistingReference() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/references/{reference}",
        TEST_PROJECT, mapId, reactionId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessReference() throws Exception {
    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/references/{reference}",
        TEST_PROJECT, mapId, reactionId, reference.getId());

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testListReferences() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/references/", TEST_PROJECT,
        mapId,
        reactionId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testListReferencesWithoutAccess() throws Exception {

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/references/", TEST_PROJECT,
        mapId,
        reactionId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }
}
