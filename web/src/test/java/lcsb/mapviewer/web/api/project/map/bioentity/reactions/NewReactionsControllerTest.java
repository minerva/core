package lcsb.mapviewer.web.api.project.map.bioentity.reactions;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiDocs;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewReactionsControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IUserService userService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private Project project;

  private ModelData map;

  @Before
  public void setup() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    map = project.getTopModel().getModelData();
    User anonymous = userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN);
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());
  }

  @After
  public void tearDown() throws Exception {
    removeProject(project);
  }

  @Test
  public void testDocsGetReactionById() throws Exception {
    RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/reactions/{reactionId}",
        TEST_PROJECT, map.getId(), map.getReactions().iterator().next().getId());

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("new_api/projects/maps/bioEntities/reactions/get_by_id",
            getReactionPathParameters(),
            responseFields(NewApiDocs.getReactionResponse(""))))
        .andReturn().getResponse().getContentAsString();

  }

  @Test
  public void testGetNotExistingReactionById() throws Exception {
    RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/reactions/{reactionId}",
        TEST_PROJECT, map.getId(), 0);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetReactionByInvalidId() throws Exception {
    RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/reactions/{reactionId}",
        TEST_PROJECT, map.getId(), "blah");

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testExportAllToCsv() throws Exception {
    DownloadCsvRequest requestBody = new DownloadCsvRequest();
    requestBody.setColumns(Arrays.asList(
        "id",
        "description",
        "modelId",
        "mapName",
        "symbol",
        "abbreviation",
        "synonyms",
        "references",
        "name",
        "type",
        "elements",
        "reactionId",
        "mechanicalConfidenceScore",
        "lowerBound",
        "upperBound",
        "geneProteinReaction",
        "subsystem"));
    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/reactions/:downloadCsv",
        TEST_PROJECT, "*")
        .content(objectMapper.writeValueAsBytes(requestBody))
        .contentType(MediaType.APPLICATION_JSON);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    int lines = content.split("\n").length;

    int elements = 0;
    for (ModelData model : project.getModels()) {
      elements += model.getReactions().size();
    }
    assertEquals(elements + 1, lines);
  }

  @Test
  public void testExportWithLimitedColumnsToCsv() throws Exception {
    DownloadCsvRequest requestBody = new DownloadCsvRequest();
    requestBody.setColumns(Arrays.asList(
        "id",
        "modelId"));
    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/reactions/:downloadCsv",
        TEST_PROJECT, "*")
        .content(objectMapper.writeValueAsBytes(requestBody))
        .contentType(MediaType.APPLICATION_JSON);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    String[] lines = content.split("\n");

    for (String string : lines) {
      assertEquals(requestBody.getColumns().size(), string.split(",").length);
    }
  }

  @Test
  public void testExportWithAnnotationColumnsToCsv() throws Exception {
    DownloadCsvRequest requestBody = new DownloadCsvRequest();
    requestBody.setColumns(Arrays.asList(
        "id",
        "modelId"));

    requestBody.setAnnotations(Collections.singletonList(
        "PUBMED"));

    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/reactions/:downloadCsv",
        TEST_PROJECT, "*")
        .content(objectMapper.writeValueAsBytes(requestBody))
        .contentType(MediaType.APPLICATION_JSON);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    String[] lines = content.split("\n");

    for (String string : lines) {
      assertEquals(requestBody.getColumns().size() + requestBody.getAnnotations().size(), string.split(",").length);
    }
  }

  @Test
  public void testExportWithElementTypeToCsv() throws Exception {
    DownloadCsvRequest requestBody = new DownloadCsvRequest();
    requestBody.setElementTypes(Collections.singletonList(new Drug("").getStringType()));
    requestBody.setColumns(Arrays.asList(
        "id",
        "modelId"));
    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/reactions/:downloadCsv",
        TEST_PROJECT, "*")
        .content(objectMapper.writeValueAsBytes(requestBody))
        .contentType(MediaType.APPLICATION_JSON);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    int lines = content.split("\n").length;

    assertEquals("There should be no reaction with Drug", 1, lines);
  }

  @Test
  public void testExportWithElementTypeToCsv2() throws Exception {
    DownloadCsvRequest requestBody = new DownloadCsvRequest();
    requestBody.setElementTypes(Collections.singletonList(new SimpleMolecule("").getStringType()));
    requestBody.setColumns(Arrays.asList(
        "id",
        "modelId"));
    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/reactions/:downloadCsv",
        TEST_PROJECT, "*")
        .content(objectMapper.writeValueAsBytes(requestBody))
        .contentType(MediaType.APPLICATION_JSON);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    int lines = content.split("\n").length;

    assertEquals("There should be one reaction with Protein", 2, lines);
  }

  @Test
  public void testExportWithReactionTypeAFToCsv() throws Exception {
    DownloadCsvRequest requestBody = new DownloadCsvRequest();
    requestBody.setReactionTypes(Collections.singletonList("Activity flow"));
    requestBody.setColumns(Arrays.asList(
        "id",
        "modelId"));
    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/reactions/:downloadCsv",
        TEST_PROJECT, "*")
        .content(objectMapper.writeValueAsBytes(requestBody))
        .contentType(MediaType.APPLICATION_JSON);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    int lines = content.split("\n").length;

    assertEquals("There should be no AF reaction", 1, lines);
  }

  @Test
  public void testExportWithReactionTypePDToCsv() throws Exception {
    DownloadCsvRequest requestBody = new DownloadCsvRequest();
    requestBody.setReactionTypes(Collections.singletonList("Process description"));
    requestBody.setColumns(Arrays.asList(
        "id",
        "modelId"));
    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/reactions/:downloadCsv",
        TEST_PROJECT, "*")
        .content(objectMapper.writeValueAsBytes(requestBody))
        .contentType(MediaType.APPLICATION_JSON);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    int lines = content.split("\n").length;

    assertEquals("There should be one PD reaction", 2, lines);
  }

  @Test
  public void testDocsExportToCsvAndDocument() throws Exception {
    Compartment compartment = null;
    for (Element element : map.getElements()) {
      if (element instanceof Compartment) {
        compartment = (Compartment) element;
      }
    }
    assertNotNull(compartment);

    DownloadCsvRequest requestBody = createDownloadCsvRequest(compartment);

    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/reactions/:downloadCsv",
        TEST_PROJECT, "*")
        .content(objectMapper.writeValueAsBytes(requestBody))
        .contentType(MediaType.APPLICATION_JSON);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("new_api/projects/maps/bioEntities/reactions/export_to_csv",
            getMapPathParameters(),
            NewApiDocs.getReactionCsvRequest()))
        .andReturn().getResponse().getContentAsString();
    int lines = content.split("\n").length;

    assertEquals("There should be one PD reaction", 2, lines);
  }

  private DownloadCsvRequest createDownloadCsvRequest(final Compartment compartment) {
    DownloadCsvRequest requestBody = new DownloadCsvRequest();
    requestBody.setSubmaps(Collections.singletonList(map.getId()));
    requestBody.setElementTypes(Collections.singletonList(new SimpleMolecule("").getStringType()));
    requestBody.setIncludedCompartmentIds(Collections.singletonList(compartment.getId()));
    requestBody.setReactionTypes(Collections.singletonList("Process description"));
    requestBody.setColumns(Arrays.asList(
        "id",
        "modelId"));
    requestBody.setAnnotations(Collections.singletonList(
        "PUBMED"));
    return requestBody;
  }

  @Test
  public void testExportWithParticipantsToCsv() throws Exception {
    DownloadCsvRequest requestBody = new DownloadCsvRequest();
    requestBody.setColumns(Arrays.asList(
        "id",
        "modelId",
        "reactantIds",
        "productIds",
        "modifierIds"));
    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/reactions/:downloadCsv",
        TEST_PROJECT, "*")
        .content(objectMapper.writeValueAsBytes(requestBody))
        .contentType(MediaType.APPLICATION_JSON);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    String[] lines = content.split("\n");

    for (String string : lines) {
      assertEquals(requestBody.getColumns().size(), string.split(",").length);
    }
  }

}
