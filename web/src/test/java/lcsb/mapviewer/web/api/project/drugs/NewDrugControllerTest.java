package lcsb.mapviewer.web.api.project.drugs;

import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiDocs;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewDrugControllerTest extends ControllerIntegrationTest {

  @Before
  public void setup() {
    createAndPersistProject(TEST_PROJECT);
  }

  @After
  public void tearDown() throws Exception {
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testDocsSearchDrugsInProjectUrl() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/drugs:search?query={query}", TEST_PROJECT, "aspirin")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("new_api/projects/project_drugs/search_by_query",
            getProjectPathParameters(),
            NewApiDocs.getDrugFilter(),
            responseFields(NewApiDocs.getDrugListResponse(""))));
  }

  @Test
  public void testDocsSuggestedQueryList() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/drugs/suggestedQueryList", TEST_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("new_api/projects/project_drugs/suggestedQueryList",
            getProjectPathParameters(),
            responseFields(NewApiDocs.getSuggestedQueryListFields())))
        .andReturn().getResponse().getContentAsString();
  }

}
