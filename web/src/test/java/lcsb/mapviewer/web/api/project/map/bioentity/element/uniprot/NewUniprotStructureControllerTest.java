package lcsb.mapviewer.web.api.project.map.bioentity.element.uniprot;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.Structure;
import lcsb.mapviewer.model.map.species.field.UniprotRecord;
import lcsb.mapviewer.persist.dao.map.species.UniprotStructureProperty;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUniprotStructureService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewUniprotStructureControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IUniprotStructureService uniprotStructureService;

  @Autowired
  private IMinervaJobService minervaJobService;
  @Autowired
  private IProjectService projectService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private Project project;

  private int elementId;
  private int uniprotId;
  private int structureId;
  private UniprotRecord uniprot;
  private Structure uniprotStructure;
  private int compartmentId;
  private int complexId;
  private int mapId;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    for (final Element element : project.getTopModelData().getElements()) {
      if (element instanceof Compartment) {
        compartmentId = element.getId();
      } else if (element instanceof Complex) {
        complexId = element.getId();
      } else if (element instanceof Species) {
        for (final UniprotRecord uniprot : ((Species) element).getUniprots()) {
          uniprotId = uniprot.getId();
          this.uniprot = uniprot;

          uniprotStructure = uniprot.getStructures().iterator().next();
          structureId = uniprotStructure.getId();

          elementId = uniprot.getSpecies().getId();
        }
      }
    }
    projectService.removeBackground(project.getProjectBackgrounds().get(0));
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testGetStructure() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/{uniprotId}/structures/{structureId}",
        TEST_PROJECT, mapId, elementId, uniprotId, structureId)
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });
    assertNotNull(result.get("chainId"));
  }

  @Test
  public void testGetNonExisting() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/{uniprotId}/structures/{structureId}",
        TEST_PROJECT, mapId, elementId, uniprotId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetStructureWithoutPermission() throws Exception {
    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/{uniprotId}/structures/{structureId}",
        TEST_PROJECT, mapId, elementId, uniprotId, structureId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testCreateStructure() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewUniprotStructureDTO dto = createUniprotStructureDTO();

    final RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/{uniprotId}/structures/",
        TEST_PROJECT,
        mapId,
        elementId,
        uniprotId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(dto))
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    final Map<UniprotStructureProperty, Object> filter = new HashMap<>();
    filter.put(UniprotStructureProperty.PROJECT_ID, Collections.singletonList(TEST_PROJECT));
    filter.put(UniprotStructureProperty.MAP_ID, Collections.singletonList(mapId));
    filter.put(UniprotStructureProperty.ELEMENT_ID, Collections.singletonList(elementId));
    filter.put(UniprotStructureProperty.UNIPROT_ID, Collections.singletonList(uniprotId));
    filter.put(UniprotStructureProperty.ID, Collections.singletonList((int) result.get("id")));
    final Page<Structure> elements = uniprotStructureService.getAll(filter, Pageable.unpaged());

    assertEquals(1, elements.getNumberOfElements());

  }

  @Test
  public void testCreateUniprotForNotExisting() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/{uniprotId}/structures/",
        TEST_PROJECT,
        mapId,
        complexId,
        uniprotId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(createUniprotStructureDTO()))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testUpdateStructure() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewUniprotStructureDTO data = createUniprotStructureDTO(uniprotStructure);
    data.setChainId("123-123");

    final RequestBuilder request = put(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/{uniprotId}/structures/{structureId}",
        TEST_PROJECT, mapId, elementId, uniprotId, structureId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final Structure uniprotStructure = uniprotStructureService.getById(structureId);
    assertEquals(data.getChainId(), uniprotStructure.getChainId());
    assertNotEquals(uniprotStructure.getEntityVersion(), uniprot.getEntityVersion());
  }


  private NewUniprotStructureDTO createUniprotStructureDTO() {
    return createUniprotStructureDTO(createUniprotStructure());
  }

  private NewUniprotStructureDTO createUniprotStructureDTO(final Structure structure) {
    final NewUniprotStructureDTO result = new NewUniprotStructureDTO();
    result.setCoverage(structure.getCoverage());
    result.setChainId(structure.getChainId());
    result.setResolution(structure.getResolution());
    result.setPdbId(structure.getPdbId());
    result.setStructEnd(structure.getStructEnd());
    result.setStructStart(structure.getStructStart());
    result.setExperimentalMethod(structure.getExperimentalMethod());
    result.setTaxId(structure.getTaxId());
    result.setUnpEnd(structure.getUnpEnd());
    result.setUnpStart(structure.getUnpStart());
    return result;
  }

  @Test
  public void testDeleteStructure() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/{uniprotId}/structures/{structureId}",
        TEST_PROJECT, mapId, elementId, uniprotId, structureId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    final Structure structure = uniprotStructureService.getById(structureId);
    assertNull(structure);
  }

  @Test
  public void testDeleteNoAccessUniprot() throws Exception {
    final RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/{uniprotId}/structures/{structureId}",
        TEST_PROJECT, mapId, elementId, uniprotId, structureId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());

    final Structure mr = uniprotStructureService.getById(structureId);
    assertNotNull(mr);
  }

  @Test
  public void testDeleteNonExistingStructure() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/{uniprotId}/structures/{structureId}",
        TEST_PROJECT, mapId, elementId, uniprotId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());

  }

  @Test
  public void testGetStructures() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/{uniprotId}/structures/",
        TEST_PROJECT, mapId, elementId, uniprotId)
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();

    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });
    assertTrue((int) result.get("size") > 0);
  }

}
