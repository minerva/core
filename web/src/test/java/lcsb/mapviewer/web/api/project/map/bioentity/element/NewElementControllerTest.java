package lcsb.mapviewer.web.api.project.map.bioentity.element;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.ReactionNode;
import lcsb.mapviewer.model.map.sbo.SBOTermSpeciesType;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.persist.dao.map.species.ElementProperty;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.awt.Color;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewElementControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IElementService elementService;

  @Autowired
  private IMinervaJobService minervaJobService;
  @Autowired
  private IProjectService projectService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private Project project;

  private int elementId;
  private int compartmentId;
  private int complexId;
  private int elementWithReactionId;
  private int mapId;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    Set<Integer> ids = new HashSet<>();
    for (Element element : project.getTopModelData().getElements()) {
      if (element instanceof Compartment) {
        compartmentId = element.getId();
      } else if (element instanceof Complex) {
        complexId = element.getId();
      } else {
        ids.add(element.getId());
      }
    }
    for (Reaction reaction : project.getTopModelData().getReactions()) {
      for (ReactionNode node : reaction.getReactionNodes()) {
        ids.remove(node.getElement().getId());
        elementWithReactionId = node.getElement().getId();
      }
    }
    elementId = ids.iterator().next();
    projectService.removeBackground(project.getProjectBackgrounds().get(0));
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testGetElement() throws Exception {

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, mapId, elementId)
        .session(session);

    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });
    assertNotNull(result.get("sboTerm"));
    assertNotNull(result.get("borderLineType"));
  }

  @Test
  public void testGetNonExisting() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, mapId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetElementWithoutPermission() throws Exception {
    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, mapId, elementId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testCreateElement() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewElementDTO data = createElementDTO();

    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/",
        TEST_PROJECT,
        mapId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    Map<ElementProperty, Object> filter = new HashMap<>();
    filter.put(ElementProperty.PROJECT_ID, Collections.singletonList(TEST_PROJECT));
    filter.put(ElementProperty.MAP_ID, Collections.singletonList(mapId));
    filter.put(ElementProperty.ID, Collections.singletonList((int) result.get("id")));
    Page<Element> elements = elementService.getElementsByFilter(filter, Pageable.unpaged(), false);

    assertEquals(1, elements.getNumberOfElements());
  }

  private NewElementDTO createElementDTO(final Element element) {
    NewElementDTO result = createElementDTO();
    result.setSboTerm(element.getSboTerm());
    result.setElementId(element.getElementId());
    return result;
  }

  private NewElementDTO createElementDTO() {
    NewElementDTO result = new NewElementDTO();
    result.setSboTerm(SBOTermSpeciesType.getTermByType(new Gene("")));
    result.setElementId("id" + counter++);
    result.setX(10.0);
    result.setY(20.0);
    result.setZ(30);
    result.setWidth(40.0);
    result.setHeight(50.0);
    result.setFontSize(12.0);
    result.setFontColor(Color.BLUE);
    result.setFillColor(Color.YELLOW);
    result.setBorderColor(Color.DARK_GRAY);
    result.setVisibilityLevel("");
    result.setTransparencyLevel("");
    result.setNotes("hello gene");
    result.setSymbol("SNCA");
    result.setFullName("SNCA gene");
    result.setAbbreviation("GWI");
    result.setFormula("");
    result.setName("SNCA");
    result.setNameX(result.getX());
    result.setNameY(result.getY());
    result.setNameWidth(result.getWidth());
    result.setNameHeight(result.getHeight());
    result.setNameVerticalAlign(VerticalAlign.MIDDLE);
    result.setNameHorizontalAlign(HorizontalAlign.CENTER);
    result.setLineWidth(1.0);
    result.setHomodimer(1);

    result.setThickness(3.0);
    result.setInnerWidth(4.0);
    result.setOuterWidth(5.0);
    return result;
  }

  @Test
  public void testUpdateElement() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewElementDTO data = createElementDTO(project.getTopModel().getElementByDbId(elementId));
    data.setFillColor(Color.YELLOW);

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, mapId, elementId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    Element element = elementService.getById(elementId);
    assertEquals(data.getElementId(), element.getElementId());
    assertEquals(Color.YELLOW, element.getFillColor());
  }

  @Test
  public void updateWithError() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewElementDTO data = createElementDTO(project.getTopModel().getElementByDbId(elementId));
    data.setX(null);

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, mapId, elementId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    MockHttpServletResponse response = mockMvc.perform(request)
        .andReturn().getResponse();

    String content = response.getContentAsString();
    Map<String, Object> errorMessage = objectMapper.readValue(content, new TypeReference<Map<String, Object>>() {
    });
    assertNotNull(errorMessage.get("error"));
  }

  @Test
  public void testUpdateElementWithOldVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewElementDTO data = createElementDTO();

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, mapId, elementId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdateElementWithGoodVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String originalVersion = project.getTopModel().getElementByDbId(elementId).getEntityVersion() + "";

    NewElementDTO data = createElementDTO(project.getTopModel().getElementByDbId(elementId));

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, mapId, elementId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    Element element = elementService.getById(elementId);
    assertNotEquals(originalVersion, element.getEntityVersion() + "");
  }

  @Test
  public void testDeleteElement() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, mapId, elementId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    assertNull(elementService.getById(elementId));
  }

  @Test
  public void testDeleteElementWithReaction() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, mapId, elementWithReactionId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());

    assertNotNull(elementService.getById(elementWithReactionId));
  }

  @Test
  public void testDeleteCompartmentWithElement() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, mapId, compartmentId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());

    assertNotNull(elementService.getById(compartmentId));
  }

  @Test
  public void testDeleteComplexWithElement() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, mapId, complexId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());

    assertNotNull(elementService.getById(complexId));
  }

  @Test
  public void testDeleteNotExistingElement() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, mapId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessElement() throws Exception {
    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, mapId, elementId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeleteElementWithGoodVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String originalVersion = project.getTopModel().getElementByDbId(elementId).getEntityVersion() + "";

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, mapId, elementId)
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
    assertNull(elementService.getById(elementId));
  }

  @Test
  public void testDeleteElementWithWrongVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, mapId, elementId)
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testListElements() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/", TEST_PROJECT, mapId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testListElementsWithoutAccess() throws Exception {

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/", TEST_PROJECT, mapId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testCreateGenericElement() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    for (Class<? extends Element> clazz : new ElementUtils().getAvailableElementSubclasses()) {
      NewElementDTO data = createElementDTO(clazz.getConstructor(String.class).newInstance("id" + (counter++)));

      RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/",
          TEST_PROJECT,
          mapId)
          .contentType(MediaType.APPLICATION_JSON)
          .content(objectMapper.writeValueAsString(data))
          .session(session);

      MockHttpServletResponse response = mockMvc.perform(request)
          .andExpect(status().isCreated())
          .andReturn().getResponse();
      assertNotNull(response.getHeader("ETag"));
    }
  }

  @Test
  public void testGetCompartment() throws Exception {

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, mapId, compartmentId)
        .session(session);

    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });
    assertNotNull(result.get("sboTerm"));
    assertNotNull(result.get("shape"));

  }


}
