package lcsb.mapviewer.web.api;

import lcsb.mapviewer.model.ProjectStatus;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.RequestFieldsSnippet;
import org.springframework.restdocs.request.ParameterDescriptor;
import org.springframework.restdocs.request.RequestParametersSnippet;
import org.springframework.restdocs.snippet.Snippet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;

public class NewApiDocs {

  @SuppressWarnings("unused")
  private static final Logger logger = LogManager.getLogger();

  public static List<FieldDescriptor> getBioEntityResponse(final String prefix) {
    return new NewApiDocs().getBioEntityFields(prefix);
  }

  public static List<FieldDescriptor> getElementResponse(final String prefix) {
    return new NewApiDocs().getElementFields(prefix);
  }

  public static List<FieldDescriptor> getReactionResponse(final String prefix) {
    return new NewApiDocs().getReactionFields(prefix);
  }

  public static List<FieldDescriptor> getChemicalListResponse(final String prefix) {
    return new NewApiDocs().getChemicalsFields(prefix + "[].");
  }

  public static List<FieldDescriptor> getDrugListResponse(final String prefix) {
    return new NewApiDocs().getDrugsFields(prefix + "[].");
  }

  public static List<FieldDescriptor> getProjectResponse(final String prefix) {
    return new NewApiDocs().getProjectFields(prefix);
  }

  public static List<FieldDescriptor> getJobResponse(final String prefix) {
    return new NewApiDocs().getJobFields(prefix);
  }

  public static List<FieldDescriptor> getUserResponse(final String prefix) {
    return new NewApiDocs().getUserFields(prefix);
  }

  public static List<FieldDescriptor> getPluginResponse(final String prefix) {
    return new NewApiDocs().getPluginFields(prefix);
  }

  public static List<FieldDescriptor> getLayerResponse(final String prefix) {
    return new NewApiDocs().getLayerFields(prefix);
  }

  public static List<FieldDescriptor> getCommentResponse(final String prefix) {
    return new NewApiDocs().getCommentFields(prefix);
  }

  public static List<FieldDescriptor> getGlyphResponse(final String prefix) {
    return new NewApiDocs().getGlyphFields(prefix);
  }

  public static List<FieldDescriptor> getLayerImageResponse(final String prefix) {
    return new NewApiDocs().getLayerImageFields(prefix);
  }

  public static List<FieldDescriptor> getLayerTextResponse(final String prefix) {
    return new NewApiDocs().getLayerTextFields(prefix);
  }

  public static List<FieldDescriptor> getDataOverlayGroupResponse(final String prefix) {
    return new NewApiDocs().getDataOverlayGroupFields(prefix);
  }

  public static List<FieldDescriptor> getStacktraceResponse(final String prefix) {
    return new NewApiDocs().getStacktraceFields(prefix);
  }

  public static List<FieldDescriptor> getOAuthProvidersResponse(final String prefix) {
    return new NewApiDocs().getOauthProviderFields(prefix + "[].");
  }

  public static List<FieldDescriptor> getErrorReportResponse(final String prefix) {
    return new NewApiDocs().getErrorReportFields(prefix);
  }

  public static RequestParametersSnippet getChemicalFilter() {
    return requestParameters(
        parameterWithName("query")
            .description("name of chemical that we are searching for")
            .optional(),
        parameterWithName("target")
            .description("target element that we are searching for in format TYPE:ID")
            .optional());
  }

  public static RequestParametersSnippet getDrugFilter() {
    return requestParameters(
        parameterWithName("query")
            .description("name of drug that we are searching for")
            .optional(),
        parameterWithName("target")
            .description("target element that we are searching for in format TYPE:ID")
            .optional());
  }

  public static RequestFieldsSnippet getCreateProjectFields() {
    return requestFields(
        fieldWithPath("projectId")
            .type(JsonFieldType.STRING)
            .description("project identifier"),
        fieldWithPath("name")
            .type(JsonFieldType.STRING)
            .description("name"),
        fieldWithPath("version")
            .type(JsonFieldType.STRING)
            .description("version"),
        fieldWithPath("notifyEmail")
            .type(JsonFieldType.STRING)
            .description("email used for notifications about this project"),
        fieldWithPath("sbgnFormat")
            .type(JsonFieldType.BOOLEAN)
            .description("should the project be visualized using sbgn standard")
    );
  }

  public static RequestFieldsSnippet getUpdateProjectFields() {
    return requestFields(
        fieldWithPath("projectId")
            .type(JsonFieldType.STRING)
            .description("project identifier"),
        fieldWithPath("name")
            .type(JsonFieldType.STRING)
            .description("name"),
        fieldWithPath("version")
            .type(JsonFieldType.STRING)
            .description("version"),
        fieldWithPath("notifyEmail")
            .type(JsonFieldType.STRING)
            .description("email used for notifications about this project"),
        fieldWithPath("sbgnFormat")
            .type(JsonFieldType.BOOLEAN)
            .description("should the project be visualized using sbgn standard")
    );
  }

  public static RequestFieldsSnippet getReactionCsvRequest() {
    return requestFields(new NewApiDocs().getReactionCsvFields());
  }

  public static RequestFieldsSnippet getElementCsvRequest() {
    return requestFields(new NewApiDocs().getElementCsvFields());
  }

  public static List<FieldDescriptor> getSuggestedQueryListFields() {
    return Collections.singletonList(
        fieldWithPath("[]")
            .description("list of suggested queries")
            .type(JsonFieldType.ARRAY));
  }

  public static List<FieldDescriptor> getOverlayBioEntityList(final String prefix) {
    return new NewApiDocs().getOverlayBioEntityPairFields(prefix + "[].");
  }

  public static List<FieldDescriptor> getOverlayResponse(final String prefix) {
    return new NewApiDocs().getOverlayFields(prefix);
  }

  public static List<FieldDescriptor> getPageableFields() {
    return Arrays.asList(
        fieldWithPath("totalPages")
            .description("total number of pages"),
        fieldWithPath("totalElements")
            .description("total number of elements"),
        fieldWithPath("numberOfElements")
            .description("number of elements on this page"),
        fieldWithPath("size")
            .description("page size"),
        fieldWithPath("number")
            .description("page number"));
  }

  public static RequestFieldsSnippet getAddLayerRequest() {
    return requestFields(
        fieldWithPath("name")
            .description("name")
            .type(JsonFieldType.STRING),
        fieldWithPath("z")
            .description("z index (order of layers)")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("visible")
            .description("is layer visible")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath("locked")
            .description("can layer be edited")
            .type(JsonFieldType.BOOLEAN)
    );
  }

  public static RequestFieldsSnippet getAddUserRequest() {
    return requestFields(
        fieldWithPath("name")
            .description("first name")
            .type(JsonFieldType.STRING),
        fieldWithPath("surname")
            .description("family name")
            .type(JsonFieldType.STRING),
        fieldWithPath("login")
            .description("login")
            .type(JsonFieldType.STRING),
        fieldWithPath("orcidId")
            .description("orcid identifier (https://orcid.org/)")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath("password")
            .description("password")
            .type(JsonFieldType.STRING),
        fieldWithPath("email")
            .description("email")
            .type(JsonFieldType.STRING),
        fieldWithPath("connectedToLdap")
            .description("is the account connected to ldap")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath("active")
            .description("is active")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath("termsOfUseConsent")
            .description("did user consent to the terms of use")
            .type(JsonFieldType.BOOLEAN)
    );
  }

  public static RequestFieldsSnippet getAddPluginRequest() {
    return requestFields(
        fieldWithPath("name")
            .description("name")
            .type(JsonFieldType.STRING),
        fieldWithPath("version")
            .description("version of the plugin")
            .type(JsonFieldType.STRING),
        fieldWithPath("url")
            .description("location of the javascript file")
            .type(JsonFieldType.STRING),
        fieldWithPath("public")
            .description("should the plugin be listed in the browser")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath("default")
            .description("should the plugin be loaded by default when opening diagram browser")
            .optional()
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath("hash")
            .description("hash of plugin javascript file")
            .type(JsonFieldType.STRING)
    );
  }

  public static RequestFieldsSnippet getGrantUserPrivilegeRequest() {
    return requestFields(
        fieldWithPath("privilegeType")
            .description("typo of privilege")
            .type(JsonFieldType.STRING),
        fieldWithPath("objectId")
            .description("referenced object id (if necessary)")
            .optional()
            .type(JsonFieldType.STRING)
    );
  }

  public static RequestFieldsSnippet getRegisterUserRequest() {
    return requestFields(
        fieldWithPath("email")
            .description("user email (and login)")
            .type(JsonFieldType.STRING),
        fieldWithPath("login")
            .ignored(),
        fieldWithPath("orcidId")
            .ignored(),
        fieldWithPath("connectedToLdap")
            .ignored(),
        fieldWithPath("active")
            .ignored(),
        fieldWithPath("termsOfUseConsent")
            .ignored(),
        fieldWithPath("password")
            .description("password")
            .type(JsonFieldType.STRING),
        fieldWithPath("name")
            .description("given name)")
            .type(JsonFieldType.STRING),
        fieldWithPath("surname")
            .description("family name")
            .type(JsonFieldType.STRING));
  }

  public static RequestFieldsSnippet getPasswordResetRequest() {
    return requestFields(
        fieldWithPath("password")
            .description("new password")
            .type(JsonFieldType.STRING),
        fieldWithPath("token")
            .description("reset password token obtained using email")
            .type(JsonFieldType.STRING)
    );
  }

  public static RequestFieldsSnippet getAddDataOverlayGroupRequest() {
    return requestFields(
        fieldWithPath("name")
            .description("name")
            .type(JsonFieldType.STRING),
        fieldWithPath("order")
            .description("order")
            .type(JsonFieldType.NUMBER)
    );
  }

  public static RequestFieldsSnippet getAddCommentRequest() {
    return requestFields(
        fieldWithPath("content")
            .description("content")
            .type(JsonFieldType.STRING),
        fieldWithPath("coordinates.x")
            .description("x coordinate on the map")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("coordinates.y")
            .description("y coordinate on the map")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("visible")
            .description("is content visible")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath("email")
            .description("reported email address")
            .type(JsonFieldType.STRING)
    );
  }

  public static RequestFieldsSnippet getAddLayerImageRequest() {
    return requestFields(
        fieldWithPath("x")
            .description("x")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("y")
            .description("y")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("z")
            .description("z")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("width")
            .description("width")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("height")
            .description("height")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("glyph")
            .description("glyph id")
            .optional()
            .type(JsonFieldType.NUMBER)
    );
  }

  public static RequestFieldsSnippet getAddGlyphRequest() {
    return requestFields(
        fieldWithPath("file")
            .description("image file (PNG,JPG,GIF)")
    );
  }

  public static RequestFieldsSnippet getAddLayerTextRequest() {
    return requestFields(
        fieldWithPath("x")
            .description("x")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("y")
            .description("y")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("z")
            .description("z")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("width")
            .description("width")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("height")
            .description("height")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("fontSize")
            .description("font size")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("notes")
            .description("text")
            .type(JsonFieldType.STRING),
        fieldWithPath("verticalAlign")
            .description("enum describing vertical alignment, available values: " + getOptionsAsString(VerticalAlign.class))
            .type(JsonFieldType.STRING),
        fieldWithPath("horizontalAlign")
            .description("enum describing horizontal alignment, available values: " + getOptionsAsString(HorizontalAlign.class))
            .type(JsonFieldType.STRING),
        subsectionWithPath("color")
            .description("text color")
            .type(JsonFieldType.OBJECT),
        subsectionWithPath("borderColor")
            .description("border color of the text area")
            .type(JsonFieldType.OBJECT)
    );
  }

  public static List<ParameterDescriptor> getLogsSearchFilter() {
    return Arrays.asList(
        parameterWithName("level")
            .description("warning level")
            .optional(),
        parameterWithName("sort")
            .description("sort order")
            .optional(),
        parameterWithName("LEVEL.dir")
            .ignored()
            .description("sort order direction"),
        parameterWithName("<SORT>.dir")
            .optional()
            .description("sort order direction"),
        parameterWithName("search")
            .optional()
            .description("search query")
    );
  }

  private List<FieldDescriptor> getOverlayBioEntityPairFields(final String prefix) {
    final List<FieldDescriptor> result = new ArrayList<>();
    result.addAll(Arrays.asList(
        fieldWithPath(prefix + "left")
            .description("bioEntity")
            .type(JsonFieldType.OBJECT),
        fieldWithPath(prefix + "right")
            .description("overlay entry matching bioEntity")
            .type(JsonFieldType.OBJECT)));
    result.addAll(getBioEntityFields(prefix + "left."));
    result.addAll(getOverlayEntryFields(prefix + "right."));
    return result;
  }

  private List<FieldDescriptor> getOverlayFields(final String prefix) {
    return new ArrayList<>(Arrays.asList(
        fieldWithPath(prefix + "name")
            .description("name")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "description")
            .description("description")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "genomeType")
            .description("genome type")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "type")
            .description("type")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "publicOverlay")
            .description("is the overlay public")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath(prefix + "genomeVersion")
            .description("genome version")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "creator")
            .description("overlay owner")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "group")
            .description("group identifier")
            .optional()
            .type(JsonFieldType.NUMBER),
        fieldWithPath(prefix + "id")
            .description("id")
            .type(JsonFieldType.NUMBER),
        fieldWithPath(prefix + "idObject")
            .description("idObject")
            .ignored()
            .optional()
            .type(JsonFieldType.NUMBER),
        fieldWithPath(prefix + "order")
            .description("order")
            .type(JsonFieldType.NUMBER)));
  }

  private List<FieldDescriptor> getOverlayEntryFields(final String prefix) {
    final List<FieldDescriptor> result = new ArrayList<>();
    result.addAll(Arrays.asList(
        fieldWithPath(prefix + "id")
            .description("identifier")
            .type(JsonFieldType.NUMBER),
        fieldWithPath(prefix + "name")
            .description("bioEntity name that should match")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "modelName")
            .description("bioEntity model name that should match")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "elementId")
            .description("bioEntity identifier from external resource (like CellDesigner file that was used for generating the map)")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "reverseReaction")
            .description("should the reaction be reversed in the overlay")
            .optional()
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath(prefix + "lineWidth")
            .description("line width used when drawing overlay entry")
            .optional()
            .type(JsonFieldType.NUMBER),
        fieldWithPath(prefix + "value")
            .description("normalized value <-1,1> assigned to this entry")
            .optional()
            .type(JsonFieldType.NUMBER),
        fieldWithPath(prefix + "color")
            .description("color that should be used when drawing overlay entry")
            .optional()
            .type(JsonFieldType.OBJECT),
        fieldWithPath(prefix + "description")
            .description("description")
            .type(JsonFieldType.STRING)
            .optional()

    ));
    result.addAll(getColorFields(prefix + "color."));
    return result;
  }

  private List<FieldDescriptor> getReactionCsvFields() {
    return new ArrayList<>(Arrays.asList(
        fieldWithPath("elementTypes")
            .description("list of element types")
            .type(JsonFieldType.ARRAY),
        fieldWithPath("reactionTypes")
            .description("list of reaction types")
            .type(JsonFieldType.ARRAY),
        fieldWithPath("submaps")
            .description("list of map identifiers")
            .type(JsonFieldType.ARRAY),
        fieldWithPath("includedCompartmentIds")
            .description("list of compartment identifiers where at least one participant should be present")
            .type(JsonFieldType.ARRAY),
        fieldWithPath("columns")
            .description("list of columns that should be returned in csv")
            .type(JsonFieldType.ARRAY),
        fieldWithPath("annotations")
            .description("list of annotations that should be returned in csv")
            .type(JsonFieldType.ARRAY),
        fieldWithPath("excludedCompartmentIds")
            .description("list of compartment identifiers where all participant should not be present")
            .type(JsonFieldType.ARRAY)));
  }

  private List<FieldDescriptor> getElementCsvFields() {
    return new ArrayList<>(Arrays.asList(
        fieldWithPath("types")
            .description("list of element types")
            .type(JsonFieldType.ARRAY),
        fieldWithPath("submaps")
            .description("list of map identifiers")
            .type(JsonFieldType.ARRAY),
        fieldWithPath("includedCompartmentIds")
            .description("list of compartment identifiers where elements should be included")
            .type(JsonFieldType.ARRAY),
        fieldWithPath("columns")
            .description("list of columns that should be returned in csv")
            .type(JsonFieldType.ARRAY),
        fieldWithPath("annotations")
            .description("list of annotations that should be returned in csv")
            .type(JsonFieldType.ARRAY),
        fieldWithPath("excludedCompartmentIds")
            .description("list of compartment identifiers from which elements should be excluded")
            .type(JsonFieldType.ARRAY)));
  }

  private List<FieldDescriptor> getBioEntityFields(final String prefix) {
    final List<FieldDescriptor> result = getElementFields(prefix);
    result.addAll(getReactionSpecificFields(prefix));
    return result;
  }

  private List<FieldDescriptor> getElementFields(final String prefix) {
    final List<FieldDescriptor> result = getBioEntitySpecificFields(prefix);
    result.addAll(getSpeciesSpecificFields(prefix));
    result.addAll(getChemicalSpecificFields(prefix));
    result.addAll(getCompartmentSpecificFields(prefix));
    return result;
  }

  private List<FieldDescriptor> getReactionFields(final String prefix) {
    final List<FieldDescriptor> result = getBioEntitySpecificFields(prefix);
    result.addAll(getReactionSpecificFields(prefix));
    return result;
  }

  private List<FieldDescriptor> getBioEntitySpecificFields(final String prefix) {
    final List<FieldDescriptor> result = new ArrayList<>();
    result.add(
        fieldWithPath(prefix + "immediateLink")
            .description("link that should be opened immediately when element is clicked on the map")
            .optional()
            .type(JsonFieldType.STRING));
    result.add(
        fieldWithPath(prefix + "id")
            .description("unique bioEntity identifier")
            .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "name")
        .description("name")
        .type(JsonFieldType.STRING)
        .optional());

    result.add(fieldWithPath(prefix + "elementId")
        .description("element identifier taken from source file")
        .type(JsonFieldType.STRING));

    result.add(fieldWithPath(prefix + "model")
        .description("map identifier")
        .type(JsonFieldType.NUMBER)

    );

    result.add(subsectionWithPath(prefix + "references")
        .description("list of references")
        .type(JsonFieldType.ARRAY)
    );

    result.add(fieldWithPath(prefix + "z")
        .description("z index (coordinate) of the element on the map")
        .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "notes")
        .description("notes and description")
        .type(JsonFieldType.STRING));

    result.add(fieldWithPath(prefix + "symbol")
        .description("symbol")
        .type(JsonFieldType.STRING)
        .optional());

    result.add(fieldWithPath(prefix + "abbreviation")
        .description("abbreviation")
        .type(JsonFieldType.STRING)
        .optional());

    result.add(fieldWithPath(prefix + "formula")
        .description("formula")
        .type(JsonFieldType.STRING)
        .optional());

    result.add(subsectionWithPath(prefix + "synonyms")
        .description("list of synonyms")
        .type(JsonFieldType.ARRAY)
        .optional());

    result.add(fieldWithPath(prefix + "visibilityLevel")
        .description("at what zoom level this element becomes visible in hierarchical view")
        .type(JsonFieldType.STRING)
        .optional());

    result.add(fieldWithPath(prefix + "homodimer")
        .description("multimer value")
        .type(JsonFieldType.NUMBER)
        .optional());

    result.add(fieldWithPath(prefix + "sboTerm")
        .description("SBO term")
        .type(JsonFieldType.STRING)
        .optional());
    return result;
  }

  private List<FieldDescriptor> getSpeciesSpecificFields(final String prefix) {
    final List<FieldDescriptor> result = new ArrayList<>(Arrays.asList(
        fieldWithPath(prefix + "nameX")
            .description("x coordinate of the element name on the map")
            .type(JsonFieldType.NUMBER)
            .optional(),

        fieldWithPath(prefix + "nameY")
            .description("y coordinate of the element name on the map")
            .type(JsonFieldType.NUMBER)
            .optional(),

        fieldWithPath(prefix + "borderLineType")
            .description("line type")
            .type(JsonFieldType.STRING)
            .optional(),

        fieldWithPath(prefix + "nameWidth")
            .description("width of the box with element name")
            .type(JsonFieldType.NUMBER)
            .optional(),

        fieldWithPath(prefix + "nameHeight")
            .description("height of the box with element name")
            .type(JsonFieldType.NUMBER)
            .optional(),

        fieldWithPath(prefix + "nameVerticalAlign")
            .description("vertical align of the text inside the box with element name")
            .type(JsonFieldType.STRING)
            .optional(),

        fieldWithPath(prefix + "nameHorizontalAlign")
            .description("horizontal align of the text inside the box with element name")
            .type(JsonFieldType.STRING)
            .optional(),

        fieldWithPath(prefix + "width")
            .description("width of the element")
            .type(JsonFieldType.NUMBER)
            .optional(),

        fieldWithPath(prefix + "height")
            .description("height of the element")
            .type(JsonFieldType.NUMBER)
            .optional(),

        fieldWithPath(prefix + "transparencyLevel")
            .description("at what zoom level this element becomes transparent in hierarchical view")
            .type(JsonFieldType.STRING)
            .optional(),

        subsectionWithPath(prefix + "formerSymbols")
            .description("list of former symbols")
            .type(JsonFieldType.ARRAY)
            .optional(),

        fieldWithPath(prefix + "fullName")
            .description("full name")
            .type(JsonFieldType.STRING)
            .optional(),

        fieldWithPath(prefix + "glyph")
            .description("image glyph associated with the element")
            .type(JsonFieldType.OBJECT)
            .optional(),

        fieldWithPath(prefix + "glyph.id")
            .description("glyph id")
            .type(JsonFieldType.NUMBER)
            .optional(),

        fieldWithPath(prefix + "glyph.file")
            .description("id of file associated with the glyph")
            .type(JsonFieldType.NUMBER)
            .optional(),

        fieldWithPath(prefix + "activity")
            .description("is the element active")
            .type(JsonFieldType.BOOLEAN)
            .optional(),

        fieldWithPath(prefix + "hypothetical")
            .description("is the element hypothetical")
            .type(JsonFieldType.BOOLEAN)
            .optional(),

        fieldWithPath(prefix + "boundaryCondition")
            .description("SBML kinetics is boundary condition")
            .type(JsonFieldType.BOOLEAN)
            .optional(),

        fieldWithPath(prefix + "constant")
            .description("SBML kinetics is constant")
            .type(JsonFieldType.BOOLEAN)
            .optional(),

        fieldWithPath(prefix + "initialAmount")
            .description("SBML kinetics initial amount")
            .type(JsonFieldType.NUMBER)
            .optional(),

        fieldWithPath(prefix + "initialConcentration")
            .description("SBML kinetics initial concentration")
            .type(JsonFieldType.NUMBER)
            .optional(),

        fieldWithPath(prefix + "charge")
            .description("SBML charge")
            .type(JsonFieldType.NUMBER)
            .optional(),

        fieldWithPath(prefix + "substanceUnits")
            .description("SBML substance units")
            .type(JsonFieldType.OBJECT)
            .optional(),

        fieldWithPath(prefix + "onlySubstanceUnits")
            .description("SBML only substance units")
            .type(JsonFieldType.BOOLEAN)
            .optional(),

        subsectionWithPath(prefix + "modificationResidues")
            .description("List of species modification residues")
            .type(JsonFieldType.ARRAY)
            .optional(),

        subsectionWithPath(prefix + "structuralState")
            .description("Species structural state")
            .type(JsonFieldType.OBJECT)
            .optional(),

        fieldWithPath(prefix + "complex")
            .description("identifier of a complex in which element is located")
            .type(JsonFieldType.NUMBER)
            .optional(),

        fieldWithPath(prefix + "complexName")
            .description("name of a complex in which element is located")
            .type(JsonFieldType.STRING)
            .optional(),

        fieldWithPath(prefix + "compartment")
            .description("identifier of a compartment in which element is located")
            .type(JsonFieldType.NUMBER)
            .optional(),

        fieldWithPath(prefix + "pathway")
            .description("identifier of a pathway in which element is located")
            .type(JsonFieldType.NUMBER)
            .optional(),

        fieldWithPath(prefix + "compartmentName")
            .description("name of a compartment in which element is located")
            .type(JsonFieldType.STRING)
            .optional(),

        fieldWithPath(prefix + "submodel")
            .description("submap to which this bioEntity is an entry point (anchor)")
            .type(JsonFieldType.OBJECT)
            .optional(),

        fieldWithPath(prefix + "submodel.mapId")
            .description("identifier of a submap to which this bioEntity is an entry point (anchor)")
            .type(JsonFieldType.NUMBER)
            .optional(),

        fieldWithPath(prefix + "submodel.type")
            .description("type of submap connection")
            .type(JsonFieldType.STRING)
            .optional(),

        fieldWithPath(prefix + "x")
            .description("x coordinate of the element on the map")
            .type(JsonFieldType.NUMBER)
            .optional(),

        fieldWithPath(prefix + "y")
            .description("y coordinate of the element on the map")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath(prefix + "lineWidth")
            .description("border line width")
            .type(JsonFieldType.NUMBER)
            .optional(),

        subsectionWithPath(prefix + "substanceUnits")
            .description("Species units")
            .type(JsonFieldType.OBJECT)
            .optional()
    ));
    result.add(fieldWithPath(prefix + "fontColor")
        .description("font color")
        .type(JsonFieldType.OBJECT)
        .optional());

    result.addAll(getColorFields(prefix + "fontColor."));
    result.add(fieldWithPath(prefix + "fontSize")
        .description("font size")
        .type(JsonFieldType.NUMBER)
        .optional());
    result.add(fieldWithPath(prefix + "fillColor")
        .description("fill color")
        .type(JsonFieldType.OBJECT)
        .optional());
    result.addAll(getColorFields(prefix + "fillColor."));
    result.add(fieldWithPath(prefix + "borderColor")
        .description("border color")
        .optional()
        .type(JsonFieldType.OBJECT));
    result.addAll(getColorFields(prefix + "borderColor."));

    return result;
  }

  private List<FieldDescriptor> getChemicalSpecificFields(final String prefix) {
    return Arrays.asList(
        fieldWithPath(prefix + "smiles")
            .description("smiles identifier (for chemicals)")
            .type(JsonFieldType.STRING)
            .optional(),

        fieldWithPath(prefix + "inChI")
            .description("inChI identifier (for chemicals)")
            .type(JsonFieldType.STRING)
            .optional(),

        fieldWithPath(prefix + "inChIKey")
            .description("inChI key identifier (for chemicals)")
            .type(JsonFieldType.STRING)
            .optional());
  }

  private List<FieldDescriptor> getCompartmentSpecificFields(final String prefix) {
    return Arrays.asList(
        fieldWithPath(prefix + "thickness")
            .description("compartment membrane thickness")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath(prefix + "outerWidth")
            .description("compartment outer line width")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath(prefix + "shape")
            .description("shape of a compartment")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath(prefix + "innerWidth")
            .description("compartment inner line width")
            .type(JsonFieldType.NUMBER)
            .optional());
  }

  private List<FieldDescriptor> getReactionSpecificFields(final String prefix) {
    final List<FieldDescriptor> result = new ArrayList<>();
    result.addAll(Arrays.asList(
        fieldWithPath(prefix + "idReaction")
            .description("reaction identifier taken from source file")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath(prefix + "reversible")
            .description("is reaction reversible")
            .type(JsonFieldType.BOOLEAN)
            .optional(),
        fieldWithPath(prefix + "mechanicalConfidenceScore")
            .description("reaction mechanical confidence score (from RECON)")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath(prefix + "lowerBound")
            .description("reaction lower bound (from RECON)")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath(prefix + "upperBound")
            .description("reaction upper bound (from RECON)")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath(prefix + "subsystem")
            .description("reaction subsystem (from RECON)")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath(prefix + "geneProteinReaction")
            .description("reaction gene protein reaction (from RECON)")
            .type(JsonFieldType.STRING)
            .optional(),
        subsectionWithPath(prefix + "kinetics")
            .description("reaction SBML kinetics law")
            .type(JsonFieldType.OBJECT)
            .optional(),

        fieldWithPath(prefix + "products")
            .description("list of reaction products")
            .type(JsonFieldType.ARRAY)
            .optional(),
        fieldWithPath(prefix + "products[].id")
            .description("identifier of the product")
            .type(JsonFieldType.NUMBER)
            .ignored(),
        fieldWithPath(prefix + "products[].element")
            .description("identifier of the element for this product")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath(prefix + "products[].stoichiometry")
            .description("SBML stoichiometry")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath(prefix + "products[].line")
            .description("line used to draw product connection")
            .type(JsonFieldType.OBJECT)
            .optional(),

        fieldWithPath(prefix + "reactants")
            .description("list of reaction reactants")
            .type(JsonFieldType.ARRAY)
            .optional(),
        fieldWithPath(prefix + "reactants[].id")
            .description("identifier of the reactant")
            .type(JsonFieldType.NUMBER)
            .ignored(),
        fieldWithPath(prefix + "reactants[].element")
            .description("identifier of the element for this reactant")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath(prefix + "reactants[].stoichiometry")
            .description("SBML stoichiometry")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath(prefix + "reactants[].line")
            .description("line used to draw reactant connection")
            .type(JsonFieldType.OBJECT)
            .optional(),

        fieldWithPath(prefix + "modifiers")
            .description("list of reaction modifiers")
            .type(JsonFieldType.ARRAY)
            .optional(),
        fieldWithPath(prefix + "modifiers[].id")
            .description("identifier of the modifier")
            .type(JsonFieldType.NUMBER)
            .ignored(),
        fieldWithPath(prefix + "modifiers[].element")
            .description("identifier of the element for this modifier")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath(prefix + "modifiers[].stoichiometry")
            .description("SBML stoichiometry")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath(prefix + "modifiers[].line")
            .description("line used to draw modifier connection")
            .type(JsonFieldType.OBJECT)
            .optional(),

        subsectionWithPath(prefix + "processCoordinates")
            .description(
                "coordinates of process in the reaction if defined manually. If this value is not present then center of line should be taken.")
            .type(JsonFieldType.OBJECT)
            .optional(),
        fieldWithPath(prefix + prefix + "line")
            .description("reaction central line")
            .type(JsonFieldType.OBJECT)
            .optional()));
    result.addAll(getLineFields(prefix + "line."));
    result.addAll(getLineFields(prefix + "products[].line."));
    result.addAll(getLineFields(prefix + "reactants[].line."));
    result.addAll(getLineFields(prefix + "modifiers[].line."));
    result.add(fieldWithPath(prefix + "operators")
        .description("reaction operators")
        .type(JsonFieldType.ARRAY)
        .optional());
    result.addAll(getOperatorFields(prefix + "operators[]."));
    return result;
  }

  private List<FieldDescriptor> getLineFields(final String prefix) {
    return ListUtils.union(Arrays.asList(
            fieldWithPath(prefix + "id")
                .description("line identifier")
                .type(JsonFieldType.NUMBER)
                .optional(),
            fieldWithPath(prefix + "width")
                .description("line width")
                .type(JsonFieldType.NUMBER)
                .optional(),
            fieldWithPath(prefix + "z")
                .description("line z coordinate (z-index)")
                .type(JsonFieldType.NUMBER)
                .optional(),
            subsectionWithPath(prefix + "segments")
                .description("line segments")
                .type(JsonFieldType.ARRAY)
                .optional(),
            subsectionWithPath(prefix + "startArrow")
                .description("arrow definition of the line beginning")
                .type(JsonFieldType.OBJECT)
                .optional(),
            subsectionWithPath(prefix + "endArrow")
                .description("arrow definition of the line ending")
                .type(JsonFieldType.OBJECT)
                .optional(),
            fieldWithPath(prefix + "lineType")
                .description("line pattern")
                .type(JsonFieldType.STRING)
                .optional(),
            fieldWithPath(prefix + "color")
                .description("line color")
                .type(JsonFieldType.OBJECT)
                .optional()),
        getColorFields(prefix + "color."));
  }

  private List<FieldDescriptor> getOperatorFields(final String prefix) {
    return Arrays.asList(
        fieldWithPath(prefix + "id")
            .description("operator identifier")
            .type(JsonFieldType.NUMBER)
            .optional(),
        subsectionWithPath(prefix + "line")
            .description("line for the operator")
            .type(JsonFieldType.OBJECT)
            .optional(),
        subsectionWithPath(prefix + "inputs").optional().ignored(),
        subsectionWithPath(prefix + "outputs").optional().ignored(),
        fieldWithPath(prefix + "operatorText").optional().ignored(),
        fieldWithPath(prefix + "reactantOperator").optional().ignored(),
        fieldWithPath(prefix + "productOperator").optional().ignored(),
        fieldWithPath(prefix + "modifierOperator").optional().ignored());
  }

  private List<FieldDescriptor> getColorFields(final String prefix) {
    return Arrays.asList(
        fieldWithPath(prefix + "alpha")
            .description("alpha value (0-255)")
            .type(JsonFieldType.NUMBER)
            .optional(),
        fieldWithPath(prefix + "rgb")
            .description("RED-GREEN-BLUE value as integer #RRGGBB")
            .type(JsonFieldType.NUMBER)
            .optional());
  }

  private List<FieldDescriptor> getChemicalsFields(final String prefix) {
    return Arrays.asList(
        subsectionWithPath(prefix + "id")
            .description("identifier of the chemical")
            .type(JsonFieldType.OBJECT),
        fieldWithPath(prefix + "name")
            .description("name")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "description")
            .description("description")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "synonyms")
            .description("list of synonyms")
            .type(JsonFieldType.ARRAY),
        subsectionWithPath(prefix + "references")
            .description("list of references that identify sources")
            .type(JsonFieldType.ARRAY),
        fieldWithPath(prefix + "targets")
            .description("list of targets")
            .type(JsonFieldType.ARRAY),
        fieldWithPath(prefix + "targets[].name")
            .description("target name")
            .type(JsonFieldType.STRING),
        subsectionWithPath(prefix + "targets[].references")
            .description("list of target references")
            .type(JsonFieldType.ARRAY),
        subsectionWithPath(prefix + "targets[].targetElements")
            .description("list of elements on the map associated with this target")
            .type(JsonFieldType.ARRAY),
        subsectionWithPath(prefix + "targets[].targetParticipants")
            .description("list of identifiers associated with this target")
            .type(JsonFieldType.ARRAY),
        fieldWithPath(prefix + "directEvidence")
            .description("direct evidence")
            .optional()
            .ignored()
            .type(JsonFieldType.STRING),
        subsectionWithPath(prefix + "directEvidenceReferences")
            .description("list of references")
            .ignored()
            .type(JsonFieldType.ARRAY));
  }

  private List<FieldDescriptor> getDrugsFields(final String prefix) {
    return Arrays.asList(
        subsectionWithPath(prefix + "id")
            .description("identifier of the drug")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "name")
            .description("name")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "description")
            .description("description")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "synonyms")
            .description("list of synonyms")
            .type(JsonFieldType.ARRAY),
        fieldWithPath(prefix + "brandNames")
            .description("list of brand names")
            .type(JsonFieldType.ARRAY),
        fieldWithPath(prefix + "bloodBrainBarrier")
            .description("does drug cross blood brain barrier")
            .type(JsonFieldType.STRING),
        subsectionWithPath(prefix + "references")
            .description("list of references that identify sources")
            .type(JsonFieldType.ARRAY),
        fieldWithPath(prefix + "targets")
            .description("list of targets")
            .type(JsonFieldType.ARRAY),
        fieldWithPath(prefix + "targets[].name")
            .description("target name")
            .type(JsonFieldType.STRING),
        subsectionWithPath(prefix + "targets[].references")
            .description("list of target references")
            .type(JsonFieldType.ARRAY),
        subsectionWithPath(prefix + "targets[].targetElements")
            .description("list of elements on the map associated with this target")
            .type(JsonFieldType.ARRAY),
        subsectionWithPath(prefix + "targets[].targetParticipants")
            .description("list of identifiers associated with this target")
            .type(JsonFieldType.ARRAY));
  }

  public List<FieldDescriptor> getProjectFields(final String prefix) {
    return Arrays.asList(
        fieldWithPath(prefix + "projectId")
            .description("project identifier")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "name")
            .description("name")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "sharedInMinervaNet")
            .description("is the project shared in minerva net")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath(prefix + "version")
            .description("version")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "owner.login")
            .description("login of the project creator")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "creationDate")
            .description("when project was uploaded")
            .type(JsonFieldType.STRING),
        subsectionWithPath(prefix + "disease")
            .description("identifier of the disease")
            .type("Reference")
            .optional(),
        fieldWithPath(prefix + "diseaseName")
            .description("disease name")
            .type(JsonFieldType.STRING)
            .optional(),
        subsectionWithPath(prefix + "organism")
            .description("identifier of the organism")
            .type("Reference")
            .optional(),
        fieldWithPath(prefix + "organismName")
            .description("organism name")
            .type(JsonFieldType.STRING)
            .optional(),
        fieldWithPath(prefix + "id")
            .type(JsonFieldType.NUMBER)
            .ignored(),
        fieldWithPath(prefix + "sbgnFormat")
            .description("is the map visualized using SBGN standard")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath(prefix + "directory")
            .description(
                "directory where files related to the project are located. "
                    + "Whole path looks like: /minerva/map_images/{directory}")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "status")
            .description(
                "status of the uploaded project; possible values: " + getOptionsAsString(ProjectStatus.class))
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "progress")
            .description("how much current stage progressed in percent")
            .type(JsonFieldType.NUMBER),
        fieldWithPath(prefix + "notifyEmail")
            .description("email address connected to the project")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "license")
            .description("project license")
            .optional()
            .type(JsonFieldType.OBJECT),
        fieldWithPath(prefix + "license.id")
            .description("project license id")
            .optional()
            .type(JsonFieldType.NUMBER),
        fieldWithPath(prefix + "license.name")
            .description("project license name")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "license.url")
            .description("project license url")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "license.content")
            .description("project license content")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "customLicenseName")
            .description("if custom license is defined here is the license name")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "customLicenseUrl")
            .description("if custom license is defined here is the license url")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "topMap.id")
            .description("id of the top map")
            .optional()
            .type(JsonFieldType.NUMBER),
        fieldWithPath(prefix + "logEntries")
            .description("flag indicating that there are log entries attached to the project")
            .type(JsonFieldType.BOOLEAN),
        subsectionWithPath(prefix + "overviewImageViews")
            .description("list of overview images")
            .type("array<OverviewImage>")
            .optional(),
        subsectionWithPath(prefix + "topOverviewImage")
            .description("overview image that should be used as top level image")
            .type("OverviewImage")
            .optional());
  }

  public List<FieldDescriptor> getJobFields(final String prefix) {
    return Arrays.asList(
        fieldWithPath(prefix + "id")
            .description("job id")
            .type(JsonFieldType.NUMBER),
        fieldWithPath(prefix + "jobType")
            .description("job type")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "jobStatus")
            .description("job status")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "jobStarted")
            .description("when the job execution started")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "jobFinished")
            .description("when the job execution finished")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "priority")
            .description("job priority (1 is the highest priority)")
            .type(JsonFieldType.NUMBER)
    );
  }

  public List<FieldDescriptor> getUserFields(final String prefix) {
    return Arrays.asList(
        fieldWithPath(prefix + "id")
            .description("identifier")
            .type(JsonFieldType.NUMBER),
        fieldWithPath(prefix + "login")
            .description("user login")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "orcidId")
            .description("orcid identifier")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "name")
            .description("first name")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "surname")
            .description("last name")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "email")
            .description("email address")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "minColor")
            .description("user min color value used for overlay coloring")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "maxColor")
            .description("user max color value used for overlay coloring")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "neutralColor")
            .description("user neutral color value used for overlay coloring")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "simpleColor")
            .description("user color value used for overlay coloring (without value and color defined)")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "removed")
            .description("is user removed")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath(prefix + "active")
            .description("is the account active")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath(prefix + "confirmed")
            .description("is the account email confirmed")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath(prefix + "connectedToLdap")
            .description("is user connected to ldap")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath(prefix + "ldapAccountAvailable")
            .description("is the ldap account available")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath(prefix + "termsOfUseConsent")
            .description("did user accept terms of consent")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath(prefix + "lastActive")
            .optional()
            .description("last activity timestamp")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "privileges")
            .description("list of privileges")
            .type(JsonFieldType.ARRAY),
        fieldWithPath(prefix + "privileges[].privilegeType")
            .description("privilege name")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "privileges[].objectId")
            .description("associated object")
            .optional()
            .type(JsonFieldType.STRING),
        subsectionWithPath(prefix + "preferences")
            .ignored()
            .optional()
            .type(JsonFieldType.OBJECT)
    );
  }

  public List<FieldDescriptor> getProjectLogFields(final String prefix) {
    return Arrays.asList(
        subsectionWithPath(prefix + "id")
            .description("log entry id")
            .type(JsonFieldType.NUMBER),
        subsectionWithPath(prefix + "content")
            .description("log entry content")
            .type(JsonFieldType.STRING),
        subsectionWithPath(prefix + "severity")
            .description("log entry level")
            .type(JsonFieldType.STRING),
        subsectionWithPath(prefix + "type")
            .description("type of the entry")
            .type(JsonFieldType.STRING),
        subsectionWithPath(prefix + "objectIdentifier")
            .description("element identifier related to the log entry")
            .optional()
            .type(JsonFieldType.STRING),
        subsectionWithPath(prefix + "objectClass")
            .description("element identifier type related to the log entry")
            .optional()
            .type(JsonFieldType.STRING),
        subsectionWithPath(prefix + "mapName")
            .description("map name where element is located")
            .type(JsonFieldType.STRING),
        subsectionWithPath(prefix + "source")
            .description("which module reported the error")
            .optional()
            .type(JsonFieldType.STRING)
    );
  }

  public List<FieldDescriptor> getPluginFields(final String prefix) {
    return Arrays.asList(
        fieldWithPath(prefix + "hash")
            .description("plugin hash (identifier)")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "name")
            .description("name")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "version")
            .description("version")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "isPublic")
            .description("is the plugin visible in the list of plugins")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath(prefix + "isDefault")
            .description("is the plugin automatically loaded when opening map")
            .type(JsonFieldType.BOOLEAN),
        subsectionWithPath(prefix + "urls")
            .description("list of urls where plugin is accessible")
            .type(JsonFieldType.ARRAY)
            .optional()
    );
  }

  private static <T extends Enum<T>> String getOptionsAsString(final Class<T> enumType) {
    final List<String> options = new ArrayList<>();
    for (final T c : enumType.getEnumConstants()) {
      options.add(c.name());
    }
    Collections.sort(options);
    return StringUtils.join(options, ", ");
  }


  private List<FieldDescriptor> getLayerFields(final String prefix) {
    final List<FieldDescriptor> result = new ArrayList<>();
    result.add(
        fieldWithPath(prefix + "id")
            .description("unique layer identifier")
            .type(JsonFieldType.NUMBER));
    result.add(
        fieldWithPath(prefix + "z")
            .description("z index (order of layers)")
            .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "name")
        .description("name")
        .type(JsonFieldType.STRING));

    result.add(fieldWithPath(prefix + "visible")
        .description("is layer visible")
        .type(JsonFieldType.BOOLEAN));

    result.add(fieldWithPath(prefix + "locked")
        .description("can layer be edited")
        .type(JsonFieldType.BOOLEAN));

    return result;
  }

  private List<FieldDescriptor> getCommentFields(final String prefix) {
    return Arrays.asList(
        fieldWithPath(prefix + "id")
            .description("unique comment identifier")
            .type(JsonFieldType.NUMBER),
        fieldWithPath(prefix + "map")
            .description("map identifier")
            .type(JsonFieldType.NUMBER),
        fieldWithPath(prefix + "content")
            .description("comment content")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "author")
            .description("login of the author")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "email")
            .description("email of the author")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "removeReason")
            .description("why the comment has been removed ")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "coordinates.x")
            .description("x coordinate where the comment is located on map")
            .type(JsonFieldType.NUMBER),
        fieldWithPath(prefix + "coordinates.y")
            .description("y coordinate where the comment is located on map")
            .type(JsonFieldType.NUMBER),
        fieldWithPath(prefix + "deleted")
            .description("is comment deleted")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath(prefix + "visible")
            .description("is comment visible")
            .type(JsonFieldType.BOOLEAN)
    );
  }

  private List<FieldDescriptor> getGlyphFields(final String prefix) {
    final List<FieldDescriptor> result = new ArrayList<>();
    result.add(
        fieldWithPath(prefix + "id")
            .description("unique glyph identifier")
            .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "file")
        .description("attached file identifier")
        .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "filename")
        .description("attached file name")
        .optional()
        .type(JsonFieldType.STRING));
    return result;
  }

  private List<FieldDescriptor> getLayerImageFields(final String prefix) {
    final List<FieldDescriptor> result = new ArrayList<>();
    result.add(
        fieldWithPath(prefix + "id")
            .description("unique layer identifier")
            .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "layer")
        .description("layer identifier")
        .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "x")
        .description("x")
        .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "y")
        .description("y")
        .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "z")
        .description("z")
        .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "width")
        .description("width")
        .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "height")
        .description("height")
        .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "glyph")
        .description("glyph identifier")
        .optional()
        .type(JsonFieldType.NUMBER));

    return result;
  }

  private List<FieldDescriptor> getLayerTextFields(final String prefix) {
    final List<FieldDescriptor> result = new ArrayList<>();
    result.add(
        fieldWithPath(prefix + "id")
            .description("unique layer identifier")
            .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "layer")
        .description("layer identifier")
        .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "x")
        .description("x")
        .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "y")
        .description("y")
        .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "z")
        .description("z")
        .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "width")
        .description("width")
        .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "height")
        .description("height")
        .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "notes")
        .description("text")
        .type(JsonFieldType.STRING));

    result.add(fieldWithPath(prefix + "fontSize")
        .description("font size")
        .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "verticalAlign")
        .description("enum describing vertical alignment, available values: " + getOptionsAsString(VerticalAlign.class))
        .type(JsonFieldType.STRING));

    result.add(fieldWithPath(prefix + "horizontalAlign")
        .description("enum describing horizontal alignment, available values: " + getOptionsAsString(HorizontalAlign.class))
        .type(JsonFieldType.STRING));

    result.add(fieldWithPath(prefix + "color")
        .description("text color")
        .type(JsonFieldType.OBJECT));
    result.addAll(getColorFields(prefix + "color."));

    result.add(fieldWithPath(prefix + "backgroundColor")
        .description("background color")
        .type(JsonFieldType.OBJECT));
    result.addAll(getColorFields(prefix + "backgroundColor."));

    result.add(fieldWithPath(prefix + "borderColor")
        .description("border color of the text area")
        .type(JsonFieldType.OBJECT));
    result.addAll(getColorFields(prefix + "borderColor."));

    return result;
  }

  private List<FieldDescriptor> getDataOverlayGroupFields(final String prefix) {
    final List<FieldDescriptor> result = new ArrayList<>();
    result.add(
        fieldWithPath(prefix + "id")
            .description("unique group identifier")
            .type(JsonFieldType.NUMBER));


    result.add(fieldWithPath(prefix + "name")
        .description("name")
        .type(JsonFieldType.STRING));

    result.add(fieldWithPath(prefix + "order")
        .description("order")
        .type(JsonFieldType.NUMBER));

    result.add(fieldWithPath(prefix + "owner")
        .ignored()
        .optional()
        .description("owner login")
        .type(JsonFieldType.STRING));

    result.add(fieldWithPath(prefix + "projectId")
        .ignored()
        .optional()
        .type(JsonFieldType.NUMBER));

    return result;
  }

  private List<FieldDescriptor> getStacktraceFields(final String prefix) {
    return new ArrayList<>(Arrays.asList(
        fieldWithPath(prefix + "id")
            .description("error id")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "content")
            .description("stacktrace")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "createdAt")
            .description("timestamp")
            .type(JsonFieldType.STRING)
    ));
  }

  private List<FieldDescriptor> getOauthProviderFields(final String prefix) {
    return new ArrayList<>(Arrays.asList(
        fieldWithPath(prefix + "name")
            .description("name")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "uri")
            .description("oauth authentication uri")
            .type(JsonFieldType.STRING)
    ));
  }

  private List<FieldDescriptor> getErrorReportFields(final String prefix) {
    return new ArrayList<>();
  }


  public static Snippet getLayersSearchResult() {
    final String prefix = "content[].";
    final List<FieldDescriptor> fields = new ArrayList<>();
    fields.add(fieldWithPath("content")
        .description("list of layers on the page")
        .type(JsonFieldType.ARRAY));
    fields.addAll(getLayerResponse(prefix));
    fields.addAll(getPageableFields());
    return responseFields(fields);
  }

  public static Snippet getPluginsSearchResult() {
    final String prefix = "content[].";
    final List<FieldDescriptor> fields = new ArrayList<>();
    fields.add(fieldWithPath("content")
        .description("list of plugins on the page")
        .type(JsonFieldType.ARRAY));
    fields.addAll(getPluginResponse(prefix));
    fields.addAll(getPageableFields());
    return responseFields(fields);
  }

  public static Snippet getLayerImagesSearchResult() {
    final String prefix = "content[].";
    final List<FieldDescriptor> fields = new ArrayList<>();
    fields.add(fieldWithPath("content")
        .description("list of layer images on the page")
        .type(JsonFieldType.ARRAY));
    fields.addAll(getLayerImageResponse(prefix));
    fields.addAll(getPageableFields());
    return responseFields(fields);
  }

  public static Snippet getLayerTextsSearchResult() {
    final String prefix = "content[].";
    final List<FieldDescriptor> fields = new ArrayList<>();
    fields.add(fieldWithPath("content")
        .description("list of layer texts on the page")
        .type(JsonFieldType.ARRAY));
    fields.addAll(getLayerTextResponse(prefix));
    fields.addAll(getPageableFields());
    return responseFields(fields);
  }

  public static Snippet getProjectsSearchResult() {
    final String prefix = "content[].";
    final List<FieldDescriptor> fields = new ArrayList<>();
    fields.add(fieldWithPath("content")
        .description("list of projects on the page")
        .type(JsonFieldType.ARRAY));
    fields.addAll(getProjectResponse(prefix));
    fields.addAll(getPageableFields());
    return responseFields(fields);
  }

  public static Snippet getProjectLogsSearchResult() {
    final String prefix = "content[].";
    final List<FieldDescriptor> fields = new ArrayList<>();
    fields.add(fieldWithPath("content")
        .description("list of projects on the page")
        .type(JsonFieldType.ARRAY));
    fields.addAll(getProjectLogResponse(prefix));
    fields.addAll(getPageableFields());
    return responseFields(fields);
  }

  public static List<FieldDescriptor> getProjectLogResponse(final String prefix) {
    return new NewApiDocs().getProjectLogFields(prefix);
  }

  public static Snippet getUserSearchResult() {
    final String prefix = "content[].";
    final List<FieldDescriptor> fields = new ArrayList<>();
    fields.add(fieldWithPath("content")
        .description("list of users on the page")
        .type(JsonFieldType.ARRAY));
    fields.addAll(getUserResponse(prefix));
    fields.addAll(getPageableFields());
    return responseFields(fields);
  }

  public static Snippet getCommentsSearchResult() {
    final String prefix = "content[].";
    final List<FieldDescriptor> fields = new ArrayList<>();
    fields.add(fieldWithPath("content")
        .description("list of comments on the page")
        .type(JsonFieldType.ARRAY));
    fields.addAll(getCommentResponse(prefix));
    fields.addAll(getPageableFields());
    return responseFields(fields);
  }

  public static Snippet getGlyphsSearchResult() {
    final String prefix = "content[].";
    final List<FieldDescriptor> fields = new ArrayList<>();
    fields.add(fieldWithPath("content")
        .description("list of glyphs on the page")
        .type(JsonFieldType.ARRAY));
    fields.addAll(getGlyphResponse(prefix));
    fields.addAll(getPageableFields());
    return responseFields(fields);
  }

}
