package lcsb.mapviewer.web.api.user;

import lcsb.mapviewer.services.interfaces.IStacktraceService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiDocs;
import lcsb.mapviewer.web.api.WebApiTestConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebApiTestConfig.class)
public class NewOauthControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IStacktraceService stacktraceService;

  @Test
  public void testGetProviders() throws Exception {
    RequestBuilder request = get("/minerva/new_api/oauth/providers");

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("new_api/oauth/get_providers",
            responseFields(NewApiDocs.getOAuthProvidersResponse(""))))
        .andReturn().getResponse().getContentAsString();

    logger.info(response);
  }

}
