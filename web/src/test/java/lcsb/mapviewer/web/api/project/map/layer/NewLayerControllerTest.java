package lcsb.mapviewer.web.api.project.map.layer;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.persist.dao.graphics.LayerProperty;
import lcsb.mapviewer.services.interfaces.ILayerService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiDocs;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewLayerControllerTest extends ControllerIntegrationTest {

  @Autowired
  private ILayerService layerService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private Project project;

  private int layerId;
  private int mapId;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createEmptyProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    Layer layer = createLayer();
    layer.setModel(project.getTopModel().getModelData());
    project.getTopModel().addLayer(layer);
    layerService.add(layer);
    layerId = layer.getId();
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testDocsGetLayer() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}",
        TEST_PROJECT, mapId, layerId)
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/projects/maps/layers/get_layer",
            getLayerPathParameters(),
            responseFields(NewApiDocs.getLayerResponse(""))))
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
  }

  @Test
  public void testGetNonExisting() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}",
        TEST_PROJECT, mapId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetLayerWithoutPermission() throws Exception {
    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}",
        TEST_PROJECT, mapId, layerId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDocsCreateLayer() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewLayerDTO data = createLayerDTO();

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/",
        TEST_PROJECT,
        mapId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andDo(document("new_api/projects/maps/layers/add_layer",
            getMapPathParameters(),
            NewApiDocs.getAddLayerRequest(),
            responseFields(NewApiDocs.getLayerResponse(""))))
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    final Map<LayerProperty, Object> filter = new HashMap<>();
    filter.put(LayerProperty.PROJECT_ID, TEST_PROJECT);
    filter.put(LayerProperty.MAP_ID, Collections.singletonList(mapId));
    filter.put(LayerProperty.ID, Collections.singletonList(result.get("id")));
    final Page<Layer> layers = layerService.getAll(filter, Pageable.unpaged());

    assertEquals(1, layers.getNumberOfElements());
  }

  private NewLayerDTO createLayerDTO() {
    final NewLayerDTO result = new NewLayerDTO();
    result.setName(faker.name().name());
    result.setZ(faker.number().numberBetween(1, 100));
    return result;
  }

  @Test
  public void testDocsUpdateLayer() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewLayerDTO data = createLayerDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}",
        TEST_PROJECT, mapId, layerId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/projects/maps/layers/update_layer",
            getLayerPathParameters(),
            NewApiDocs.getAddLayerRequest(),
            responseFields(NewApiDocs.getLayerResponse(""))))
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final Layer layer = layerService.getById(layerId);
    assertEquals(data.getName(), layer.getName());
  }

  @Test
  public void testUpdateLayerWithOldVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewLayerDTO data = createLayerDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}",
        TEST_PROJECT, mapId, layerId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdateLayerWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String originalVersion = project.getTopModelData().getLayers().iterator().next().getEntityVersion() + "";

    final NewLayerDTO data = createLayerDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}",
        TEST_PROJECT, mapId, layerId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final Layer layer = layerService.getById(layerId);
    assertNotEquals(originalVersion, layer.getEntityVersion() + "");
  }

  @Test
  public void testDocsDeleteLayer() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}",
        TEST_PROJECT, mapId, layerId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/projects/maps/layers/delete_layer",
            getLayerPathParameters()));

    assertNull(layerService.getById(layerId));
  }

  @Test
  public void testDeleteNotExistingLayer() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}",
        TEST_PROJECT, mapId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessLayer() throws Exception {
    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}",
        TEST_PROJECT, mapId, layerId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeleteLayerWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String originalVersion = project.getTopModelData().getLayers().iterator().next().getEntityVersion() + "";

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}",
        TEST_PROJECT, mapId, layerId)
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
    assertNull(layerService.getById(layerId));
  }

  @Test
  public void testDeleteLayerWithWrongVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}",
        TEST_PROJECT, mapId, layerId)
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testDocsListLayers() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/", TEST_PROJECT, mapId)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("new_api/projects/maps/layers/list_layers",
            getMapPathParameters(),
            requestParameters(getPageableFilter()),
            NewApiDocs.getLayersSearchResult()))
        .andExpect(status().isOk());
  }

  @Test
  public void testListLayersWithoutAccess() throws Exception {

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/", TEST_PROJECT, mapId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

}
