package lcsb.mapviewer.web.api.project.overlay.entries;

import com.fasterxml.jackson.core.type.TypeReference;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.geometry.ColorParser;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IDataOverlayEntryService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewOverlayEntryControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IUserService userService;

  @Autowired
  private IDataOverlayEntryService dataOverlayEntryService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private User anonymous;

  private Project project;

  private ModelData map;

  private DataOverlay dataOverlay;

  private DataOverlayEntry dataOverlayEntry;

  private User user;

  @Before
  public void setup() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    map = project.getTopModel().getModelData();
    anonymous = userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN);
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());

    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    dataOverlay = super.createOverlay(TEST_PROJECT, admin);
    dataOverlayEntry = dataOverlay.getEntries().iterator().next();
    user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
  }

  @After
  public void tearDown() throws Exception {
    removeProject(project);
    removeUser(user);
  }

  @Test
  public void testGetOverlayEntry() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}/",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId())
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
  }

  @Test
  public void testGetNonExisting() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}/",
        BUILT_IN_PROJECT, dataOverlay.getId(), -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetOverlayEntryWithoutPermission() throws Exception {
    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}/",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId());

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testCreateOverlayEntry() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewDataOverlayEntryDTO data = createDataOverlayEntryDTO();

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/",
        TEST_PROJECT,
        dataOverlay.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    assertNotNull(dataOverlayEntryService.getById((int) result.get("id")));
  }

  private NewDataOverlayEntryDTO createDataOverlayEntryDTO() {
    final ColorParser parser = new ColorParser();
    final NewDataOverlayEntryDTO data = new NewDataOverlayEntryDTO();
    data.setColor(parser.parse(this.faker.color().hex()));
    data.setDescription(this.faker.text().text());
    data.setName(this.faker.name().firstName());
    data.setModelName(this.faker.name().name());
    data.setValue(this.faker.number().randomDouble(10, -1, 1));
    data.setLineWidth(this.faker.number().randomDouble(10, 1, 100));
    return data;
  }

  private NewDataOverlayEntryDTO createDataOverlayEntryDTO(final DataOverlayEntry overlayEntry) {
    final NewDataOverlayEntryDTO data = createDataOverlayEntryDTO();
    return data;
  }

  @Test
  public void testUpdateOverlayEntry() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewDataOverlayEntryDTO data = createDataOverlayEntryDTO(dataOverlayEntryService.getById(dataOverlayEntry.getId()));

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    dataOverlayEntry = dataOverlayEntryService.getById(dataOverlayEntry.getId());
    assertEquals(data.getName(), dataOverlayEntry.getName());
  }

  @Test
  public void testUpdateDataOverlayEntryWithOldVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewDataOverlayEntryDTO data = createDataOverlayEntryDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdateOverlayEntryWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String originalVersion = dataOverlayEntry.getEntityVersion() + "";

    final NewDataOverlayEntryDTO data = createDataOverlayEntryDTO(dataOverlayEntry);

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final DataOverlayEntry overlayEntry = dataOverlayEntryService.getById(dataOverlayEntry.getId());
    assertNotEquals(originalVersion, overlayEntry.getEntityVersion() + "");
  }

  @Test
  public void testDeleteOverlayEntry() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    assertNull(dataOverlayEntryService.getById(dataOverlayEntry.getId()));
  }

  @Test
  public void testDeleteNotExistingOverlayEntry() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}",
        TEST_PROJECT, dataOverlay.getId(), -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessOverlayEntry() throws Exception {
    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}",
        BUILT_IN_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId());

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeleteOverlayEntryWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String originalVersion = dataOverlayEntry.getEntityVersion() + "";

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId())
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testDeleteOverlayEntryWithWrongVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId())
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testListOverlayEntries() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/",
        TEST_PROJECT, dataOverlay.getId())
        .session(session);

    final String response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse().getContentAsString();

    final Map<String, Object> result = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });

    assertEquals(1, result.get("numberOfElements"));
  }

  @Test
  public void testListOwnOverlayEntries() throws Exception {
    userService.grantUserPrivilege(user, PrivilegeType.READ_PROJECT, project.getProjectId());
    final DataOverlay overlay = super.createOverlay(TEST_PROJECT, user);

    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/",
        TEST_PROJECT,
        overlay.getId())
        .session(session);

    final String response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse().getContentAsString();

    final Map<String, Object> result = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });

    assertEquals(1, result.get("numberOfElements"));
  }
}
