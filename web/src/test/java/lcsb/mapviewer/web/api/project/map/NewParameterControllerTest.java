package lcsb.mapviewer.web.api.project.map;

import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.persist.dao.map.kinetics.SbmlParameterProperty;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.ISbmlParameterService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.map.parameter.NewParameterDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewParameterControllerTest extends ControllerIntegrationTest {

  @Autowired
  private ISbmlParameterService parameterService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private Project project;

  private int parameterId;
  private int mapId;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    parameterId = project.getTopModel().getParameters().iterator().next().getId();
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testGetParameter() throws Exception {

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/parameters/{parameterId}",
        TEST_PROJECT, mapId, parameterId)
        .session(session);

    HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
  }

  @Test
  public void testGetNonExisting() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/parameters/{parameterId}",
        TEST_PROJECT, mapId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetParameterWithoutPermission() throws Exception {
    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/parameters/{parameterId}",
        TEST_PROJECT, mapId, parameterId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testCreateParameter() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewParameterDTO data = createParameterDTO();

    int count = project.getTopModelData().getParameters().size();

    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/parameters/",
        TEST_PROJECT,
        mapId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    Map<SbmlParameterProperty, Object> filter = new HashMap<>();
    filter.put(SbmlParameterProperty.PROJECT_ID, TEST_PROJECT);
    filter.put(SbmlParameterProperty.MAP_ID, mapId);
    assertEquals(count + 1, parameterService.getCount(filter));
  }

  @Test
  public void testCreateParameterNoAccess() throws Exception {
    NewParameterDTO data = createParameterDTO();

    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/parameters/",
        TEST_PROJECT,
        mapId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data));

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  private NewParameterDTO createParameterDTO() {
    NewParameterDTO result = new NewParameterDTO();
    result.setName("parameter_name_" + (counter++));
    result.setParameterId("parameter_id_" + (counter++));
    result.setUnitId(project.getTopModelData().getUnits().iterator().next().getId());
    result.setValue((double) counter++);
    return result;
  }

  @Test
  public void testUpdateParameter() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewParameterDTO data = createParameterDTO();

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/parameters/{parameterId}",
        TEST_PROJECT, mapId, parameterId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    SbmlParameter parameter = parameterService.getById(parameterId);
    assertEquals(data.getName(), parameter.getName());
  }

  @Test
  public void testUpdateParameterWithOldVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewParameterDTO data = createParameterDTO();

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/parameters/{parameterId}",
        TEST_PROJECT, mapId, parameterId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdateParameterWithGoodVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String originalVersion = project.getTopModelData().getParameters().iterator().next().getEntityVersion() + "";

    NewParameterDTO data = createParameterDTO();

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/parameters/{parameterId}",
        TEST_PROJECT, mapId, parameterId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    SbmlParameter parameter = parameterService.getById(parameterId);
    assertNotEquals(originalVersion, parameter.getEntityVersion() + "");
  }

  @Test
  public void testDeleteParameter() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/parameters/{parameterId}",
        TEST_PROJECT, mapId, parameterId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    assertNull(parameterService.getById(parameterId));
  }

  @Test
  public void testDeleteNotExistingParameter() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/parameters/{parameterId}",
        TEST_PROJECT, mapId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessParameter() throws Exception {
    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/parameters/{parameterId}",
        TEST_PROJECT, mapId, parameterId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeleteParameterWithGoodVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String originalVersion = project.getTopModelData().getParameters().iterator().next().getEntityVersion() + "";

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/parameters/{parameterId}",
        TEST_PROJECT, mapId, parameterId)
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
    assertNull(parameterService.getById(parameterId));
  }

  @Test
  public void testDeleteParameterWithWrongVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/parameters/{parameterId}",
        TEST_PROJECT, mapId, parameterId)
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testListParameters() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/parameters/", TEST_PROJECT, mapId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testListParametersWithoutAccess() throws Exception {

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/parameters/", TEST_PROJECT, mapId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

}
