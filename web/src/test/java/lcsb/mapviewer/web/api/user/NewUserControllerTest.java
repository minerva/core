package lcsb.mapviewer.web.api.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.EmailConfirmationToken;
import lcsb.mapviewer.model.user.ResetPasswordToken;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiDocs;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.Collections;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("EmailSenderEnabledEmailProfile")
public class NewUserControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IUserService userService;

  @Autowired
  private IConfigurationService configurationService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeUser(userService.getUserByLogin(TEST_USER_LOGIN));
  }

  @Test
  public void testDocsGetUserAsAdmin() throws Exception {
    User user = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/users/{userId}/", user.getId())
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/users/get_by_id",
            getUserPathParameters(),
            responseFields(NewApiDocs.getUserResponse(""))))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse();

    assertNotNull(response.getHeader("ETag"));
  }

  @Test
  public void testGetUserAsUser() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/users/{userId}/", user.getId())
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();

    assertNotNull(response.getHeader("ETag"));
  }

  @Test
  public void testDocsGrantPrivilege() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewUserPrivilegeDTO data = createUserPrivilegeDTO();

    final RequestBuilder request = post("/minerva/new_api/users/{userId}:grantPrivilege", user.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/users/grant_privilege",
            getUserPathParameters(),
            NewApiDocs.getGrantUserPrivilegeRequest(),
            responseFields(NewApiDocs.getUserResponse(""))))
        .andExpect(status().is2xxSuccessful());

    user = userService.getById(user.getId());
    assertEquals(1, user.getPrivileges().size());
  }

  @Test
  public void testDocsRevokePrivilege() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    userService.grantUserPrivilege(user, PrivilegeType.IS_CURATOR);
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewUserPrivilegeDTO data = createUserPrivilegeDTO();

    final RequestBuilder request = post("/minerva/new_api/users/{userId}:revokePrivilege", user.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/users/revoke_privilege",
            getUserPathParameters(),
            NewApiDocs.getGrantUserPrivilegeRequest(),
            responseFields(NewApiDocs.getUserResponse(""))))
        .andExpect(status().is2xxSuccessful());

    user = userService.getById(user.getId());
    assertEquals(0, user.getPrivileges().size());
  }

  @Test
  public void testGrantPrivilegeNoAccess() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    NewUserPrivilegeDTO data = createUserPrivilegeDTO();

    final RequestBuilder request = post("/minerva/new_api/users/{userId}:grantPrivilege", user.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
    user = userService.getById(user.getId());
    assertEquals(0, user.getPrivileges().size());
  }

  @Test
  public void testRevokePrivilegeNoAccess() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    userService.grantUserPrivilege(user, PrivilegeType.IS_CURATOR);
    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    NewUserPrivilegeDTO data = createUserPrivilegeDTO();

    final RequestBuilder request = post("/minerva/new_api/users/{userId}:revokePrivilege", user.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
    user = userService.getById(user.getId());
    assertEquals(1, user.getPrivileges().size());
  }

  private NewUserPrivilegeDTO createUserPrivilegeDTO() {
    NewUserPrivilegeDTO privilegeDTO = new NewUserPrivilegeDTO();
    privilegeDTO.setPrivilegeType(PrivilegeType.IS_CURATOR);
    return privilegeDTO;
  }


  @Test
  public void testGetUserNoAccess() throws Exception {
    createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    User user = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/users/{userId}/", user.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }


  @Test
  public void testGetNonExisting() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/users/{userId}/", -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDocsCreateUser() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewUserDTO data = createUserDTO(TEST_USER_LOGIN);

    final RequestBuilder request = post("/minerva/new_api/users/")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andDo(document("new_api/users/create_user",
            NewApiDocs.getAddUserRequest(),
            pathParameters(),
            responseFields(NewApiDocs.getUserResponse(""))))
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    assertNotNull(userService.getUserByLogin(TEST_USER_LOGIN));
  }

  private NewUserDTO createUserDTO(final String login) {
    NewUserDTO data = new NewUserDTO();
    data.setLogin(login);
    data.setPassword(TEST_USER_PASSWORD);
    data.setActive(true);
    data.setName(faker.name().firstName());
    data.setEmail(faker.internet().emailAddress());
    data.setSurname(faker.name().lastName());
    data.setConnectedToLdap(false);
    data.setTermsOfUseConsent(false);
    return data;
  }

  @Test
  public void testCreateUserExisting() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewUserDTO data = createUserDTO(BUILT_IN_TEST_ADMIN_LOGIN);

    final RequestBuilder request = post("/minerva/new_api/users/")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isConflict());
  }

  @Test
  public void testDocsUpdateUser() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final NewUserDTO data = createUserDTO(TEST_USER_LOGIN);

    final RequestBuilder request = put("/minerva/new_api/users/{userId}/", user.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/users/update_user",
            getUserPathParameters(),
            NewApiDocs.getAddUserRequest(),
            responseFields(NewApiDocs.getUserResponse(""))))
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    user = userService.getById(user.getId());
    assertEquals(data.getName(), user.getName());
  }

  @Test
  public void testUpdateUserWithMissingData() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final NewUserDTO data = createUserDTO(TEST_PROJECT);
    data.setName(null);

    final RequestBuilder request = put("/minerva/new_api/users/{userId}/", user.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testUpdateUserWithOldVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final NewUserDTO data = createUserDTO(TEST_USER_LOGIN);

    final RequestBuilder request = put("/minerva/new_api/users/{userId}/", user.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdateUserWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final String originalVersion = user.getEntityVersion() + "";

    final NewUserDTO data = createUserDTO(TEST_USER_LOGIN);

    final RequestBuilder request = put("/minerva/new_api/users/{userId}/", user.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    user = userService.getById(user.getId());
    assertNotEquals(originalVersion, user.getEntityVersion() + "");
  }

  @Test
  public void testDocsDeleteUser() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/users/{userId}/", user.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/users/delete_user",
            getUserPathParameters()));


    assertNull(userService.getById(user.getId()));
  }

  @Test
  public void testDeleteNotExistingUser() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/users/{userId}/", -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessUser() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/users/{userId}/", user.getId());

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeleteUserWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final String originalVersion = user.getEntityVersion() + "";

    final RequestBuilder request = delete("/minerva/new_api/users/{userId}/", user.getId())
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testDeleteUserWithWrongVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/users/{userId}/", user.getId())
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testListUsers() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/users/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testListUsersWithoutAccess() throws Exception {
    final RequestBuilder request = get("/minerva/new_api/users/");

    mockMvc.perform(request)
        .andExpect(status().isForbidden());

  }

  @Test
  public void testDocsGetUsers() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/users/")
        .session(session);

    mockMvc.perform(request)
        .andDo(document("new_api/users/list_users",
            pathParameters(),
            NewApiDocs.getUserSearchResult()))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
  }

  @Test
  public void resendConfirmationEmail() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    user.setEmail("test@test.xyz");
    user.setConfirmed(false);
    userService.update(user);

    configureServerForResetPasswordRequest();

    final RequestBuilder resetRequest = post("/minerva/new_api/users/{login}:resendConfirmEmail", user.getLogin());

    mockMvc.perform(resetRequest)
        .andExpect(status().is2xxSuccessful());

    user = userService.getUserByLogin(TEST_USER_LOGIN);
    assertFalse(user.isConfirmed());
  }

  @Test
  public void requestResetPasswordForLdapUser() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    user.setEmail("test@test.xyz");
    user.setConnectedToLdap(true);
    userService.update(user);

    configureServerForResetPasswordRequest();

    final RequestBuilder grantRequest = post("/minerva/new_api/users/" + TEST_USER_LOGIN + ":requestResetPassword");

    mockMvc.perform(grantRequest)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void requestResetPasswordWhenMinervaRootNotConfigured() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    user.setEmail("test@test.xyz");
    userService.update(user);

    configureServerForResetPasswordRequest();
    configurationService.setConfigurationValue(ConfigurationElementType.MINERVA_ROOT, "");

    final RequestBuilder grantRequest = post("/minerva/new_api/users/" + TEST_USER_LOGIN + ":requestResetPassword");

    mockMvc.perform(grantRequest)
        .andExpect(status().is5xxServerError());

  }

  @Test
  public void requestResetPasswordWhenUserWithoutEmail() throws Exception {
    configureServerForResetPasswordRequest();

    final User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    user.setEmail("");
    userService.update(user);

    final RequestBuilder grantRequest = post("/minerva/new_api/users/" + TEST_USER_LOGIN + ":requestResetPassword");

    mockMvc.perform(grantRequest)
        .andExpect(status().is4xxClientError());

  }

  @Test
  public void requestResetPasswordForUnknownUser() throws Exception {
    configureServerForResetPasswordRequest();

    final RequestBuilder grantRequest = post("/minerva/new_api/users/blah:requestResetPassword");

    mockMvc.perform(grantRequest)
        .andExpect(status().is4xxClientError());

  }

  @Test
  public void testDocsResetPassword() throws Exception {
    configureServerForResetPasswordRequest();
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    user.setEmail("test@test.xyz");
    userService.update(user);

    final String newPassword = "pass2";

    final Calendar expires = Calendar.getInstance();
    expires.add(Calendar.DAY_OF_MONTH, 1);
    final ResetPasswordToken token = new ResetPasswordToken(user, UUID.randomUUID().toString().replaceAll("[a-z]", "0"),
        expires);
    userService.add(token);

    ResetPasswordDTO data = new ResetPasswordDTO();
    data.setPassword(newPassword);
    data.setToken(token.getToken());

    final RequestBuilder resetRequest = post("/minerva/new_api/users/:resetPassword")
        .content(objectMapper.writeValueAsString(data))
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(resetRequest)
        .andDo(document("new_api/users/reset_password",
            NewApiDocs.getPasswordResetRequest()))
        .andExpect(status().is2xxSuccessful());

    createSession(TEST_USER_LOGIN, newPassword);
  }

  @Test
  public void resetPasswordUsingTheSameTokenTwice() throws Exception {
    configureServerForResetPasswordRequest();

    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    user.setEmail("test@test.xyz");
    userService.update(user);

    final RequestBuilder request = post("/minerva/new_api/users/" + TEST_USER_LOGIN + ":requestResetPassword");

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    final String newPassword = "pass2";

    final ResetPasswordToken token = userService.getPasswordTokens(TEST_USER_LOGIN).get(0);

    ResetPasswordDTO data = new ResetPasswordDTO();
    data.setPassword(newPassword);
    data.setToken(token.getToken());

    RequestBuilder resetRequest = post("/minerva/new_api/users/:resetPassword")
        .content(objectMapper.writeValueAsString(data))
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(resetRequest)
        .andExpect(status().is2xxSuccessful());

    data = new ResetPasswordDTO();
    data.setPassword(newPassword + "XX");
    data.setToken(token.getToken());

    resetRequest = post("/minerva/new_api/users/:resetPassword")
        .content(objectMapper.writeValueAsString(data))
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(resetRequest)
        .andExpect(status().is4xxClientError());

    createSession(TEST_USER_LOGIN, newPassword);

  }

  @Test
  public void testDocsConfirmRegistrationOfNewUser() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    user.setEmail("test@test.xyz");
    user.setActive(false);
    user.setConfirmed(false);
    userService.update(user);

    configureServerForResetPasswordRequest();

    final Calendar expires = Calendar.getInstance();
    expires.add(Calendar.DAY_OF_MONTH, 1);
    final EmailConfirmationToken token = new EmailConfirmationToken(user, UUID.randomUUID().toString(), expires);
    userService.add(token);

    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Collections.singletonList(
        new BasicNameValuePair("token", token.getToken()))));

    final RequestBuilder resetRequest = post("/minerva/new_api/users/{login}:confirmEmail", user.getLogin())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body);

    mockMvc.perform(resetRequest)
        .andDo(document("new_api/users/confirm_email",
            userPathParameters(),
            requestParameters(parameterWithName("token")
                .description("token obtained in the registration email")),
            responseFields(
                fieldWithPath("message")
                    .description("detailed information about the status")
                    .type(JsonFieldType.STRING),
                fieldWithPath("status")
                    .description("status")
                    .type(JsonFieldType.STRING))))
        .andExpect(status().is2xxSuccessful());

    user = userService.getUserByLogin(TEST_USER_LOGIN);
    assertTrue(user.isConfirmed());
    assertFalse(user.isActive());
    assertFalse(user.isRemoved());
  }

  @Test
  public void resendConfirmationEmailForConfirmedUser() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    user.setEmail("test@test.xyz");
    user.setConfirmed(true);
    userService.update(user);

    configureServerForResetPasswordRequest();

    final RequestBuilder resetRequest = post("/minerva/new_api/users/{login}:resendConfirmEmail", user.getLogin());

    mockMvc.perform(resetRequest)
        .andExpect(status().isBadRequest());

    user = userService.getUserByLogin(TEST_USER_LOGIN);
    assertTrue(user.isConfirmed());
  }

  @Test
  public void confirmRegistrationOfNewUserWithNotRequiredApproval() throws Exception {
    configureServerForResetPasswordRequest();
    configurationService.setConfigurationValue(ConfigurationElementType.REQUIRE_APPROVAL_FOR_AUTO_REGISTERED_USERS,
        "false");

    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    user.setActive(false);
    user.setConfirmed(false);
    userService.update(user);
    final Calendar expires = Calendar.getInstance();
    expires.add(Calendar.DAY_OF_MONTH, 1);
    final EmailConfirmationToken token = new EmailConfirmationToken(user, UUID.randomUUID().toString(), expires);
    userService.add(token);

    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Collections.singletonList(
        new BasicNameValuePair("token", token.getToken()))));

    final RequestBuilder resetRequest = post("/minerva/new_api/users/{login}:confirmEmail", user.getLogin())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body);

    mockMvc.perform(resetRequest)
        .andExpect(status().is2xxSuccessful());

    user = userService.getUserByLogin(TEST_USER_LOGIN);
    assertTrue(user.isConfirmed());
    assertTrue(user.isActive());
    assertFalse(user.isRemoved());
  }

  @Test
  public void registerNewUserWithInvalidEmail() throws Exception {
    configureServerForResetPasswordRequest();

    configurationService.setConfigurationValue(ConfigurationElementType.ALLOW_AUTO_REGISTER, "true");
    final NewUserDTO data = new NewUserDTO();
    data.setEmail("blah");
    data.setPassword("123qwe--9");
    data.setName("Piotr");
    data.setSurname("Gawron");
    data.setLogin("blah");
    data.setConnectedToLdap(false);
    data.setActive(true);
    data.setTermsOfUseConsent(false);

    final RequestBuilder request = post("/minerva/new_api/users/:registerUser")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data));

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testDocsRegisterNewUser() throws Exception {
    configureServerForResetPasswordRequest();

    configurationService.setConfigurationValue(ConfigurationElementType.ALLOW_AUTO_REGISTER, "true");
    final NewUserDTO data = createUserDTO(TEST_USER_LOGIN);
    data.setConnectedToLdap(false);
    data.setActive(true);
    data.setTermsOfUseConsent(false);

    final RequestBuilder request = post("/minerva/new_api/users/:registerUser")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data));

    mockMvc.perform(request)
        .andDo(document("new_api/users/register_user",
            NewApiDocs.getRegisterUserRequest(),
            responseFields(NewApiDocs.getUserResponse(""))))
        .andExpect(status().isOk());
    User user = userService.getUserByLogin(TEST_USER_LOGIN);
    assertFalse(user.isConfirmed());
    assertFalse(user.isActive());
  }

  @Test
  public void confirmRegistrationOfNewUserWithInvalidToken() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    user.setEmail("test@test.xyz");
    user.setActive(false);
    user.setConfirmed(false);
    userService.update(user);

    configureServerForResetPasswordRequest();

    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Collections.singletonList(
        new BasicNameValuePair("token", "xxx"))));

    final RequestBuilder resetRequest = post("/minerva/new_api/users/{login}:confirmEmail", user.getLogin())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body);

    mockMvc.perform(resetRequest)
        .andExpect(status().is4xxClientError());

    user = userService.getUserByLogin(TEST_USER_LOGIN);
    assertFalse(user.isConfirmed());
    assertFalse(user.isActive());
    assertFalse(user.isRemoved());
  }

  @Test
  public void resetPasswordWithExpiredToken() throws Exception {
    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    user.setEmail("test@test.xyz");
    userService.update(user);

    configureServerForResetPasswordRequest();

    final Calendar expires = Calendar.getInstance();
    expires.add(Calendar.DATE, -1);
    final ResetPasswordToken token = new ResetPasswordToken(user, "bla", expires);
    userService.add(token);

    ResetPasswordDTO data = new ResetPasswordDTO();
    data.setPassword("blah");
    data.setToken(token.getToken());

    RequestBuilder resetRequest = post("/minerva/new_api/users/:resetPassword")
        .content(objectMapper.writeValueAsString(data))
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(resetRequest)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDocsRequestResetPassword() throws Exception {
    final long count = userService.getPasswordTokenCount();
    configureServerForResetPasswordRequest();

    User user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    user.setEmail("test@test.xyz");
    userService.update(user);

    final RequestBuilder grantRequest = post("/minerva/new_api/users/{login}:requestResetPassword", TEST_USER_LOGIN);

    mockMvc.perform(grantRequest)
        .andDo(document("new_api/users/request_reset_password",
            userPathParameters()))
        .andExpect(status().is2xxSuccessful());
    assertEquals(count + 1, userService.getPasswordTokenCount());

  }

  @Test
  public void resetPasswordWithInvalidToken() throws Exception {
    configureServerForResetPasswordRequest();

    ResetPasswordDTO data = new ResetPasswordDTO();
    data.setPassword("blah");
    data.setToken("???");

    RequestBuilder resetRequest = post("/minerva/new_api/users/:resetPassword")
        .content(objectMapper.writeValueAsString(data))
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(resetRequest)
        .andExpect(status().isNotFound());
  }

  private void configureServerForResetPasswordRequest() {

    configurationService.setConfigurationValue(ConfigurationElementType.MINERVA_ROOT,
        "https://minerva-dev.lcsb.uni.lu/minerva/");
  }


}
