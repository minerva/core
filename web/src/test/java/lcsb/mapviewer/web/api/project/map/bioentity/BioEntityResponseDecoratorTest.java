package lcsb.mapviewer.web.api.project.map.bioentity;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.web.ControllerIntegrationTest;

@RunWith(SpringJUnit4ClassRunner.class)
public class BioEntityResponseDecoratorTest extends ControllerIntegrationTest {

  @Autowired
  private IElementService elementService;

  private Project project;

  @Before
  public void setup() throws Exception {
    project = createAndPersistProject(TEST_PROJECT);
  }

  @After
  public void tearDown() throws Exception {
    removeProject(project);
  }

  @Test
  public void testElementWithCompartment() {
    BioEntityResponseDecorator decorator = new BioEntityResponseDecorator(elementService);

    Compartment comp = project.getTopModel().getCompartments().get(0);

    Element element = comp.getElements().iterator().next();

    List<Pair<String, Object>> list = decorator.getBioEntityExtensions(elementService.getById(element.getId()));

    assertTrue(list.size() > 0);

  }

}
