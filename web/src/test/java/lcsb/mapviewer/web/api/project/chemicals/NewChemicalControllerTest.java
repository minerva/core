package lcsb.mapviewer.web.api.project.chemicals;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiDocs;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.junit.Assume.assumeTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("webCtdTestProfile")
public class NewChemicalControllerTest extends ControllerIntegrationTest {

  private static final String TEST_PROJECT = "TEST_PROJECT";

  private Project project;

  @Autowired
  private IProjectService projectService;

  @Before
  public void setup() {
    assumeTrue("DAPI credentials are not provided", isDapiConfigurationAvailable());
    project = createAndPersistProject(TEST_PROJECT);
  }

  @After
  public void tearDown() throws Exception {
    if (project != null) {
      removeProject(project);
    }
    removeProject(TEST_PROJECT_2);
  }

  @Test
  public void testDocsSearchChemicalsInProjectUrl() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/chemicals:search?query=stilbene oxide", TEST_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("new_api/projects/project_chemicals/search_by_query",
            getProjectPathParameters(),
            NewApiDocs.getChemicalFilter(),
            responseFields(NewApiDocs.getChemicalListResponse(""))))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
  }

  @Test
  public void testDocsSuggestedQueryList() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final MiriamData bloodLossDisease = new MiriamData(MiriamType.MESH_2012, "D016063");

    final Project project = createEmptyProject(TEST_PROJECT_2);
    project.setDisease(bloodLossDisease);
    projectService.update(project);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/chemicals/suggestedQueryList", TEST_PROJECT_2)
        .session(session);


    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("new_api/projects/project_chemicals/suggestedQueryList",
            getProjectPathParameters(),
            responseFields(NewApiDocs.getSuggestedQueryListFields())))
        .andReturn().getResponse().getContentAsString();
  }


}
