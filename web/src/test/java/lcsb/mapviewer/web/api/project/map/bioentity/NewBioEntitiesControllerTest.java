package lcsb.mapviewer.web.api.project.map.bioentity;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiDocs;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.apache.commons.collections4.ListUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.request.ParameterDescriptor;
import org.springframework.restdocs.snippet.Snippet;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewBioEntitiesControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IUserService userService;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private Project project;

  @Before
  public void setup() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    User anonymous = userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN);
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());
  }

  @After
  public void tearDown() throws Exception {
    removeProject(project);
  }

  @Test
  public void testSearchWithCount() throws Exception {
    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/:search?size=4&page=1", TEST_PROJECT, "*");

    final String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    final Page<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<Page<Map<String, Object>>>() {
    });

    assertEquals(3, result.getNumberOfElements());

  }

  @Test
  public void testSearchNonExistingMap() throws Exception {
    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/:search?size=4", TEST_PROJECT, "-1");

    mockMvc.perform(request)
        .andExpect(status().isNotFound());

  }

  @Test
  public void testDocsSearchProteinAndDocument() throws Exception {
    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/:search?query=GSTA&size=20&page=0&perfectMatch=false", TEST_PROJECT, "*");

    final String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("new_api/projects/maps/bioEntities/search",
            getMapPathParameters(),
            requestParameters(ListUtils.union(getSearchFilter(), getPageableFilter())),
            getBioEntitiesSearchResult()))
        .andReturn().getResponse().getContentAsString();

    final Page<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<Page<Map<String, Object>>>() {
    });
    assertEquals(2, result.getNumberOfElements());

  }

  @Test
  public void testDocsSearchAnyType() throws Exception {
    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/:search?", TEST_PROJECT, "*");

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("new_api/projects/maps/bioEntities/xxx",
            getMapPathParameters(),
            requestParameters(ListUtils.union(getSearchFilter(), getPageableFilter())),
            getBioEntitiesSearchResult()))
        .andReturn().getResponse().getContentAsString();

  }

  private Snippet getBioEntitiesSearchResult() {
    final String prefix = "content[].bioEntity.";
    final List<FieldDescriptor> fields = new ArrayList<>();
    fields.add(
        fieldWithPath("content")
            .description("list of elements on the page")
            .type(JsonFieldType.ARRAY));
    fields.add(
        fieldWithPath("content[].bioEntity")
            .description("bioEntity")
            .type(JsonFieldType.OBJECT));
    fields.add(
        fieldWithPath("content[].perfect")
            .description("is the bioEntity a perfect match to the search query")
            .type(JsonFieldType.BOOLEAN));
    fields.addAll(NewApiDocs.getBioEntityResponse(prefix));
    fields.addAll(NewApiDocs.getPageableFields());
    return responseFields(fields);
  }

  private List<ParameterDescriptor> getSearchFilter() {
    return Arrays.asList(
        parameterWithName("query")
            .description("search term identifying bioEntity")
            .optional(),
        parameterWithName("perfectMatch")
            .description("true when true query must be matched exactly, if false similar results are also acceptable")
            .optional());
  }

  @Test
  public void testDocsSuggestedQueryList() throws Exception {
    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/suggestedQueryList", TEST_PROJECT, "*");

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("new_api/projects/maps/bioEntities/suggestedQueryList",
            getMapPathParameters(),
            responseFields(NewApiDocs.getSuggestedQueryListFields())))
        .andReturn().getResponse().getContentAsString();
  }

}
