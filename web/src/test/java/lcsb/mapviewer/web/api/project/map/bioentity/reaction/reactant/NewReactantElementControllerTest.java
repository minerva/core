package lcsb.mapviewer.web.api.project.map.bioentity.reaction.reactant;

import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.NewIdDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewReactantElementControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IReactionService reactionService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private Project project;

  private int reactionId;
  private int reactantId;
  private int elementId;
  private int mapId;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    for (Element element : project.getTopModelData().getElements()) {
      if (element instanceof Protein) {
        elementId = element.getId();
      }
    }
    Reaction reaction = project.getTopModelData().getReactions().iterator().next();
    reactionId = reaction.getId();
    reactantId = reaction.getReactants().get(0).getId();
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testSetElement() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewIdDTO data = createIdDTO(elementId);

    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/reactants/{reactantId}/element/",
        TEST_PROJECT, mapId, reactionId, reactantId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    Reactant reactant = getReactantFromDatabase();

    assertEquals(elementId, reactant.getElement().getId());
  }

  private Reactant getReactantFromDatabase() {
    for (Reactant reactant : reactionService.getById(reactionId).getReactants()) {
      if (reactant.getId() == reactantId) {
        return reactant;
      }
    }
    return null;
  }

  @Test
  public void testSetEmptyElement() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewIdDTO data = createIdDTO(null);

    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/reactants/{reactantId}/element/",
        TEST_PROJECT, mapId, reactionId, reactantId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  private NewIdDTO createIdDTO(final Integer id) {
    NewIdDTO result = new NewIdDTO();
    result.setId(id);
    return result;
  }

  @Test
  public void testGetElementWithoutAccess() throws Exception {

    RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/reactants/{reactantId}/element/",
        TEST_PROJECT, mapId, reactionId, reactantId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testGetElement() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/reactants/{reactantId}/element/",
        TEST_PROJECT, mapId, reactionId, reactantId)
        .session(session);

    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
  }
}
