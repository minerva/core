package lcsb.mapviewer.web.api.project;

import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IMiriamService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewProjectDiseaseControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private IProjectService projectService;

  @Autowired
  private IMiriamService miriamService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testGetNonExistingDisease() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/disease", BUILT_IN_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetDisease() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createAndPersistProject(TEST_PROJECT);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/disease", TEST_PROJECT)
        .session(session);

    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
  }

  @Test
  public void testCreateDisease() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Project project = createAndPersistProject(TEST_PROJECT);
    project.setDisease(null);
    projectService.update(project);

    minervaJobService.waitForTasksToFinish();

    NewMiriamDataDTO data = createMiriamDataDTO(MiriamType.MESH_2012, "D010300");

    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/disease", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    minervaJobService.waitForTasksToFinish();
    project = projectService.getProjectByProjectId(TEST_PROJECT);
    assertNotNull(project.getDisease().getLink());
  }

  @Test
  public void testCreateDiseaseWhenDiseaseExists() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createAndPersistProject(TEST_PROJECT);

    NewMiriamDataDTO data = createMiriamDataDTO(MiriamType.MESH_2012, "D010300");

    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/disease", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isConflict());
  }

  @Test
  public void testCreateInvalidDisease() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Project project = createAndPersistProject(TEST_PROJECT);
    project.setDisease(null);
    projectService.update(project);

    NewMiriamDataDTO data = createMiriamDataDTO();

    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/disease", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testUpdateDisease() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createAndPersistProject(TEST_PROJECT);

    NewMiriamDataDTO data = createMiriamDataDTO(MiriamType.MESH_2012, "D010301");

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/disease", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    Project project = projectService.getProjectByProjectId(TEST_PROJECT);
    assertEquals(data.getResource(), project.getDisease().getResource());
  }

  @Test
  public void testUpdateDiseaseWithMissingData() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createAndPersistProject(TEST_PROJECT);

    NewMiriamDataDTO data = createMiriamDataDTO(MiriamType.MESH_2012, null);

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/disease", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testUpdateDiseaseWithOldVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createAndPersistProject(TEST_PROJECT);

    NewMiriamDataDTO data = createMiriamDataDTO(MiriamType.MESH_2012, "D010301");

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/disease", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdateDiseaseWithGoodVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Project project = createAndPersistProject(TEST_PROJECT);

    String originalVersion = project.getDisease().getEntityVersion() + "";

    NewMiriamDataDTO data = createMiriamDataDTO(MiriamType.MESH_2012, "D010301");

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/disease", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    project = projectService.getProjectByProjectId(TEST_PROJECT);
    assertNotEquals(originalVersion, project.getDisease().getEntityVersion() + "");
  }

  @Test
  public void testDeleteDisease() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    Project project = createAndPersistProject(TEST_PROJECT);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/disease", TEST_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    assertNull(projectService.getProjectByProjectId(TEST_PROJECT).getDisease());
    assertNull(miriamService.getById(project.getDisease().getId()));
  }

  @Test
  public void testDeleteNotExistingProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/disease", TEST_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNotExistingDisease() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/disease", BUILT_IN_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteDiseaseWithGoodVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Project project = createAndPersistProject(TEST_PROJECT);

    String originalVersion = project.getDisease().getEntityVersion() + "";

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/disease", TEST_PROJECT)
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    project = projectService.getProjectByProjectId(TEST_PROJECT);

    assertNull(project.getDisease());
  }

  @Test
  public void testDeleteDiseaseWithWrongVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createAndPersistProject(TEST_PROJECT);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/disease", TEST_PROJECT)
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  private NewMiriamDataDTO createMiriamDataDTO() {
    return createMiriamDataDTO(MiriamType.PUBMED, "12345");
  }

  private NewMiriamDataDTO createMiriamDataDTO(final MiriamType type, final String resource) {
    NewMiriamDataDTO data = new NewMiriamDataDTO();
    data.setResource(resource);
    data.setType(type);
    return data;
  }

}
