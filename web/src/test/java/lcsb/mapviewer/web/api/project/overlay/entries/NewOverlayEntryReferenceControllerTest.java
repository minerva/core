package lcsb.mapviewer.web.api.project.overlay.entries;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IDataOverlayEntryService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.NewMiriamDataDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.junit.Assert.assertEquals;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewOverlayEntryReferenceControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IUserService userService;

  @Autowired
  private IDataOverlayEntryService dataOverlayEntryService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private User anonymous;

  private Project project;

  private DataOverlay dataOverlay;

  private DataOverlayEntry dataOverlayEntry;

  private MiriamData reference;

  @Before
  public void setup() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    anonymous = userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN);
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());

    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    dataOverlay = super.createOverlay(project, admin, "element_identifier\tvalue\tidentifier\n\t-1\tHGNC Symbol:SNCA");
    dataOverlayEntry = dataOverlay.getEntries().iterator().next();
    reference = dataOverlayEntry.getMiriamData().iterator().next();
  }

  @After
  public void tearDown() throws Exception {
    removeProject(project);
  }

  @Test
  public void testCreateReference() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewMiriamDataDTO data = createReferenceDTO();

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}/references/",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isCreated());

    final DataOverlayEntry entry = dataOverlayEntryService.getById(dataOverlayEntry.getId(), true);

    assertEquals(dataOverlayEntry.getMiriamData().size() + 1, entry.getMiriamData().size());
  }

  private NewMiriamDataDTO createReferenceDTO() {
    final NewMiriamDataDTO result = new NewMiriamDataDTO();
    result.setType(MiriamType.PUBMED);
    result.setResource("" + counter++);
    return result;
  }

  @Test
  public void testDeleteReference() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}/references/{reference}",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId(),
        reference.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    final DataOverlayEntry entry = dataOverlayEntryService.getById(dataOverlayEntry.getId(), true);

    assertEquals(dataOverlayEntry.getMiriamData().size() - 1, entry.getMiriamData().size());
  }

  @Test
  public void testDeleteNotExistingReference() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}/references/{reference}",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId(),
        -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessReference() throws Exception {
    final RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}/references/{reference}",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId(),
        reference.getId());

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testListReferences() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}/references/",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testListReferencesWithoutAccess() throws Exception {

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}/references/",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId());

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }
}
