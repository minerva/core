package lcsb.mapviewer.web.api.project;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.job.MinervaJob;
import lcsb.mapviewer.model.job.MinervaJobType;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IStacktraceService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiDocs;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.user.NewUserLoginDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.snippet.Snippet;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("webCtdTestProfile")
public class NewProjectControllerTest extends ControllerIntegrationTest {

  private static final String CURATOR_PASSWORD = "curator";
  private static final String CURATOR_LOGIN = "curator";

  @Autowired
  private IProjectService projectService;

  @Autowired
  private IStacktraceService stacktraceService;

  @Autowired
  private IUserService userService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
    removeProject(TEST_PROJECT_2);
    removeUser(userService.getUserByLogin(CURATOR_LOGIN));
  }

  @Test
  public void testDocsGetProject() throws Exception {
    createEmptyProject(TEST_PROJECT);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/", TEST_PROJECT)
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/projects/get_by_project_id",
            getProjectPathParameters(),
            responseFields(NewApiDocs.getProjectResponse(""))))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse();
    Map<String, Object> data = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    assertTrue(data.containsKey("diseaseName"));

    assertNotNull(response.getHeader("ETag"));
  }

  @Test
  public void testGetProjectContainsInfoAboutMinervaNet() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/", BUILT_IN_PROJECT)
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();

    final Map<String, Object> object = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    assertTrue(object.containsKey("sharedInMinervaNet"));

  }

  @Test
  public void testGetNonExisting() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/", "unknown")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetProjectWithoutPermission() throws Exception {
    createEmptyProject(TEST_PROJECT);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/", TEST_PROJECT);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDocsCreateProject() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewProjectDTO data = createProjectDTO(TEST_PROJECT);

    final RequestBuilder request = post("/minerva/new_api/projects/")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andDo(document("new_api/projects/create_project_from_json",
            pathParameters(),
            NewApiDocs.getCreateProjectFields(),
            responseFields(NewApiDocs.getProjectResponse(""))))
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    assertNotNull(projectService.getProjectByProjectId(TEST_PROJECT));
  }

  @Test
  public void testCreateProjectExisting() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewProjectDTO data = createProjectDTO(BUILT_IN_PROJECT);

    final RequestBuilder request = post("/minerva/new_api/projects/")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isConflict());
  }

  @Test
  public void testDocsUpdateProject() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createEmptyProject(TEST_PROJECT);

    final NewProjectDTO data = createProjectDTO(TEST_PROJECT);

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/projects/update_project",
            projectPathParameters(),
            NewApiDocs.getUpdateProjectFields(),
            responseFields(NewApiDocs.getProjectResponse(""))))
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final Project project = projectService.getProjectByProjectId(TEST_PROJECT);
    assertEquals(data.getVersion(), project.getVersion());
  }

  @Test
  public void testUpdateProjectWithMissingData() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createEmptyProject(TEST_PROJECT);

    final NewProjectDTO data = createProjectDTO(TEST_PROJECT);
    data.setName(null);

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testUpdateProjectWithOldVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createEmptyProject(TEST_PROJECT);

    final NewProjectDTO data = createProjectDTO(TEST_PROJECT);

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdateProjectWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Project project = createEmptyProject(TEST_PROJECT);

    final String originalVersion = project.getEntityVersion() + "";

    final NewProjectDTO data = createProjectDTO(TEST_PROJECT);

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    project = projectService.getProjectByProjectId(TEST_PROJECT);
    assertNotEquals(originalVersion, project.getEntityVersion() + "");
  }

  private NewProjectDTO createProjectDTO(final String projectId) {
    final NewProjectDTO data = new NewProjectDTO();
    data.setName(faker.name().name());
    data.setNotifyEmail(faker.internet().emailAddress());
    data.setProjectId(projectId);
    data.setSbgnFormat(faker.bool().bool());
    data.setVersion(faker.number().randomDigit() + "." + faker.number().randomDigit() + "." + faker.number().randomDigit());
    return data;
  }

  @Test
  public void testDocsDeleteProject() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    createEmptyProject(TEST_PROJECT);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/", TEST_PROJECT)
        .session(session);

    final String response = mockMvc.perform(request)
        .andExpect(status().isAccepted())
        .andDo(document("new_api/projects/delete_project",
            projectPathParameters(),
            responseFields(NewApiDocs.getJobResponse(""))))
        .andReturn().getResponse()
        .getContentAsString();

    final MinervaJob job = objectMapper.readValue(response, MinervaJob.class);
    assertEquals(MinervaJobType.DELETE_PROJECT, job.getJobType());

    minervaJobService.waitForTasksToFinish();

    assertNull(projectService.getProjectByProjectId(TEST_PROJECT));
  }

  @Test
  public void testDeleteNotExistingProject() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/", TEST_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessProject() throws Exception {
    createEmptyProject(TEST_PROJECT);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/", TEST_PROJECT);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeleteDefaultProject() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/", BUILT_IN_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeleteProjectDropsPrivileges() throws Exception {
    createEmptyProject(TEST_PROJECT);
    User curator = createCurator(CURATOR_LOGIN, CURATOR_PASSWORD);

    userService.grantUserPrivilege(curator, PrivilegeType.WRITE_PROJECT, TEST_PROJECT);

    final RequestBuilder request = delete("/minerva/api/projects/" + TEST_PROJECT)
        .session(createSession(CURATOR_LOGIN, CURATOR_PASSWORD));

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    minervaJobService.waitForTasksToFinish();

    curator = userService.getUserByLogin(CURATOR_LOGIN, true);

    assertFalse("Curator privileges weren't updated after project was removed",
        curator.getPrivileges().contains(new Privilege(PrivilegeType.WRITE_PROJECT, TEST_PROJECT)));
  }

  @Test
  public void testDeleteProjectWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final Project project = createEmptyProject(TEST_PROJECT);

    final String originalVersion = project.getEntityVersion() + "";

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/", TEST_PROJECT)
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isAccepted());
  }

  @Test
  public void testDeleteProjectWithWrongVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createEmptyProject(TEST_PROJECT);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/", TEST_PROJECT)
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testDocsListProjects() throws Exception {
    final RequestBuilder request = get("/minerva/new_api/projects/?size=10&page=0");

    String response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/projects/list_projects",
            requestParameters(getPageableFilter()),
            NewApiDocs.getProjectsSearchResult()))
        .andReturn().getResponse().getContentAsString();

    final Page<Object> page = objectMapper.readValue(response, new TypeReference<Page<Object>>() {
    });
    assertEquals(1, page.getTotalElements());
    assertEquals(10, page.getSize());
    assertEquals(0, page.getNumber());
    assertEquals(1, page.getNumberOfElements());

  }

  @Test
  public void testListProjectsWithoutAccess() throws Exception {
    final RequestBuilder request = get("/minerva/new_api/projects/");

    createEmptyProject(TEST_PROJECT);

    final String response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse()
        .getContentAsString();

    final Page<Object> page = objectMapper.readValue(response, new TypeReference<Page<Object>>() {
    });
    assertEquals(1, page.getTotalElements());
    assertEquals(1, page.getNumberOfElements());
  }

  @Test
  public void testListProjectsWithoutAdminAccess() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final RequestBuilder request = get("/minerva/new_api/projects/")
        .session(session);

    createEmptyProject(TEST_PROJECT);

    final String response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse()
        .getContentAsString();

    final Page<Object> page = objectMapper.readValue(response, new TypeReference<Page<Object>>() {
    });
    assertEquals(2, page.getTotalElements());
    assertEquals(2, page.getNumberOfElements());
  }

  @Test
  public void testUpdateOwnerInProject() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createEmptyProject(TEST_PROJECT);

    final NewUserLoginDTO data = new NewUserLoginDTO();
    data.setLogin(Configuration.ANONYMOUS_LOGIN);

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/owner/", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final Project project = projectService.getProjectByProjectId(TEST_PROJECT);
    assertEquals(Configuration.ANONYMOUS_LOGIN, project.getOwner().getLogin());
  }

  @Test
  public void testUpdateInvalidOwnerInProject() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createEmptyProject(TEST_PROJECT);

    final NewUserLoginDTO data = new NewUserLoginDTO();
    data.setLogin("blah");

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/owner/", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDocsGetProjects() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/")
        .session(session);

    mockMvc.perform(request)
        .andDo(document("new_api/projects/get_projects",
            getProjectsSearchResult()))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
  }

  private Snippet getProjectsSearchResult() {
    final String prefix = "content[].";
    final List<FieldDescriptor> fields = new ArrayList<>();
    fields.add(
        fieldWithPath("content")
            .description("list of projects on the page")
            .type(JsonFieldType.ARRAY));
    fields.addAll(NewApiDocs.getProjectResponse(prefix));
    fields.addAll(NewApiDocs.getPageableFields());
    return responseFields(fields);
  }

  @Test
  public void testGetProjectWithError() throws Exception {
    Project project = new Project(TEST_PROJECT);
    projectService.add(project);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/", TEST_PROJECT)
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isInternalServerError())
        .andReturn().getResponse();

    Map<String, String> data = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, String>>() {
    });

    assertNotNull(data.get("error-id"));
    assertNotNull(stacktraceService.getById(data.get("error-id")));
  }

  @Test
  public void testMoveProject() throws Exception {
    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    Project project = new Project(TEST_PROJECT);
    project.setOwner(admin);
    projectService.add(project);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewMoveProjectDTO dto = new NewMoveProjectDTO();
    dto.setProjectId(TEST_PROJECT_2);

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}:move", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(dto))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    assertNull(projectService.getProjectByProjectId(TEST_PROJECT));
    assertNotNull(projectService.getProjectByProjectId(TEST_PROJECT_2));
  }

  @Test
  public void testMoveProjectMovePrivileges() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    userService.revokeUserPrivilege(admin, PrivilegeType.READ_PROJECT, TEST_PROJECT_2);

    createEmptyProject(TEST_PROJECT);
    userService.grantUserPrivilege(admin, PrivilegeType.READ_PROJECT, TEST_PROJECT);

    NewMoveProjectDTO dto = new NewMoveProjectDTO();
    dto.setProjectId(TEST_PROJECT_2);

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}:move", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(dto))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    User user = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN, true);

    List<GrantedAuthority> authorities = user.getPrivileges().stream()
        .map(privilege -> new SimpleGrantedAuthority(privilege.toString()))
        .collect(Collectors.toList());

    assertTrue(authorities.contains(new SimpleGrantedAuthority(PrivilegeType.READ_PROJECT + ":" + TEST_PROJECT_2)));
    assertFalse(authorities.contains(new SimpleGrantedAuthority(PrivilegeType.READ_PROJECT + ":" + TEST_PROJECT)));

  }

}
