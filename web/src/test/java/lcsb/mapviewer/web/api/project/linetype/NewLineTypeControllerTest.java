package lcsb.mapviewer.web.api.project.linetype;

import com.fasterxml.jackson.core.type.TypeReference;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewLineTypeControllerTest extends ControllerIntegrationTest {

  @Test
  public void testGetTypes() throws Exception {
    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/lineTypes/", BUILT_IN_PROJECT);

    final String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    final List<?> result = objectMapper.readValue(response, new TypeReference<List<?>>() {
    });

    assertFalse(result.isEmpty());
    assertEquals(0, super.getWarnings().size());
  }
}
