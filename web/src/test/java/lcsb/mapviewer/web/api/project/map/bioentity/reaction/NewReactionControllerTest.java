package lcsb.mapviewer.web.api.project.map.bioentity.reaction;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import lcsb.mapviewer.persist.dao.map.ReactionProperty;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.awt.geom.Point2D;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewReactionControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IReactionService reactionService;

  @Autowired
  private IElementService elementService;

  @Autowired
  private IMinervaJobService minervaJobService;
  @Autowired
  private IProjectService projectService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private Project project;

  private int reactionId;
  private int mapId;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    reactionId = project.getTopModelData().getReactions().iterator().next().getId();
    projectService.removeBackground(project.getProjectBackgrounds().get(0));
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testGetReaction() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}",
        TEST_PROJECT, mapId, reactionId)
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
  }

  @Test
  public void testGetNonExisting() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}",
        TEST_PROJECT, mapId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetReactionWithoutPermission() throws Exception {
    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}",
        TEST_PROJECT, mapId, reactionId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testCreateReaction() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewReactionDTO data = createReactionDTO();

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/",
        TEST_PROJECT,
        mapId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    final Map<ReactionProperty, Object> filter = new HashMap<>();
    filter.put(ReactionProperty.PROJECT_ID, Collections.singletonList(TEST_PROJECT));
    filter.put(ReactionProperty.MAP_ID, Collections.singletonList(mapId));
    filter.put(ReactionProperty.ID, Collections.singletonList((int) result.get("id")));

    assertEquals(1, reactionService.getCount(filter));
  }

  private NewReactionDTO createReactionDTO(final Reaction reaction) {
    final NewReactionDTO result = createReactionDTO();
    result.setSboTerm(reaction.getSboTerm());
    result.setReactionId(reaction.getIdReaction());
    return result;
  }

  private NewReactionDTO createReactionDTO() {
    final NewReactionDTO result = new NewReactionDTO();
    result.setSboTerm(new TransportReaction("").getSboTerm());
    result.setReactionId("re" + counter++);
    result.setAbbreviation("A");
    result.setFormula("FORM");
    result.setGeneProteinReaction("GPR");
    result.setLowerBound(-1.0);
    result.setMechanicalConfidenceScore(1);
    result.setName("re_nam" + counter++);
    result.setNotes("note");
    result.setProcessCoordinates(new Point2D.Double(10, 10));
    result.setReversible(false);
    result.setSubsystem("SUB");
    result.setSymbol("SYM");
    result.setUpperBound(2.0);
    result.setZ(1);
    return result;
  }

  @Test
  public void testUpdateReaction() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewReactionDTO data = createReactionDTO(project.getTopModel().getReactionByDbId(reactionId));

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}",
        TEST_PROJECT, mapId, reactionId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final Reaction reaction = reactionService.getById(reactionId);
    assertEquals(data.getReactionId(), reaction.getIdReaction());
  }

  @Test
  public void testUpdateReactionWithOldVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewReactionDTO data = createReactionDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}",
        TEST_PROJECT, mapId, reactionId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdateReactionWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String originalVersion = project.getTopModel().getReactionByDbId(reactionId).getEntityVersion() + "";

    final NewReactionDTO data = createReactionDTO(project.getTopModel().getReactionByDbId(reactionId));

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}",
        TEST_PROJECT, mapId, reactionId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final Reaction reaction = reactionService.getById(reactionId);
    assertNotEquals(originalVersion, reaction.getEntityVersion() + "");
  }

  @Test
  public void testDeleteReaction() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}",
        TEST_PROJECT, mapId, reactionId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    assertNull(reactionService.getById(reactionId));
  }

  @Test
  public void testDeleteNotExistingReaction() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}",
        TEST_PROJECT, mapId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessReaction() throws Exception {
    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}",
        TEST_PROJECT, mapId, reactionId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeleteReactionWithGoodVersion() throws Exception {
    final long elements = elementService.getCount(new HashMap<>());
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String originalVersion = project.getTopModel().getReactionByDbId(reactionId).getEntityVersion() + "";

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}",
        TEST_PROJECT, mapId, reactionId)
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
    assertNull(reactionService.getById(reactionId));
    assertEquals(elements, elementService.getCount(new HashMap<>()));
  }

  @Test
  public void testDeleteReactionWithWrongVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}",
        TEST_PROJECT, mapId, reactionId)
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testListReactions() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/", TEST_PROJECT, mapId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testListReactionsWithoutAccess() throws Exception {

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/", TEST_PROJECT, mapId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testCreateGenericReaction() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    for (final Class<? extends Reaction> clazz : new ElementUtils().getAvailableReactionSubclasses()) {
      final NewReactionDTO data = createReactionDTO(clazz.getConstructor(String.class).newInstance("id" + (counter++)));

      final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/",
          TEST_PROJECT,
          mapId)
          .contentType(MediaType.APPLICATION_JSON)
          .content(objectMapper.writeValueAsString(data))
          .session(session);

      final MockHttpServletResponse response = mockMvc.perform(request)
          .andExpect(status().isCreated())
          .andReturn().getResponse();
      assertNotNull(response.getHeader("ETag"));
    }
  }

}
