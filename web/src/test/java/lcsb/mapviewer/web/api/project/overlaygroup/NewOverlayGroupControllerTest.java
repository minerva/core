package lcsb.mapviewer.web.api.project.overlaygroup;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.overlay.DataOverlayGroup;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IDataOverlayGroupService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiDocs;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewOverlayGroupControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IDataOverlayGroupService dataOverlayGroupService;

  @Autowired
  private IUserService userService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private User admin;

  private DataOverlayGroup group;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    Project project = createAndPersistProject(TEST_PROJECT);
    admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    group = createAndPersistDataOverlayGroup(project, admin);
  }

  @After
  public void tearDown() throws Exception {
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testGetDataOverlayGroup() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlay_groups/{overlayGroupId}",
        TEST_PROJECT,
        group.getId())
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/projects/data_overlay_groups/get_group",
            getDataOverlayGroupPathParameters(),
            responseFields(NewApiDocs.getDataOverlayGroupResponse("")))).andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    assertNotNull(result.get("name"));
    assertNotNull(result.get("order"));
  }

  @Test
  public void testGetNonExisting() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlay_groups/{overlayGroupId}",
        TEST_PROJECT, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetDataOverlayGroupWithoutPermission() throws Exception {
    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlay_groups/{overlayGroupId}",
        TEST_PROJECT, group.getId());

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testCreateDataOverlayGroup() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewDataOverlayGroupDTO data = createDataOverlayGroupDTO();

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/overlay_groups/",
        TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andDo(document("new_api/projects/data_overlay_groups/add_group",
            getProjectPathParameters(),
            NewApiDocs.getAddDataOverlayGroupRequest(),
            responseFields(NewApiDocs.getDataOverlayGroupResponse(""))))
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });
    int id = (int) result.get("id");

    DataOverlayGroup group = dataOverlayGroupService.getById(id);

    assertNotNull(group);
  }

  private NewDataOverlayGroupDTO createDataOverlayGroupDTO() {
    final NewDataOverlayGroupDTO result = new NewDataOverlayGroupDTO();
    result.setName(faker.name().name());
    result.setOrder(faker.number().numberBetween(1, 100));
    return result;
  }

  @Test
  public void testUpdateDataOverlayGroup() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewDataOverlayGroupDTO data = createDataOverlayGroupDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/overlay_groups/{overlayGroupId}",
        TEST_PROJECT, group.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/projects/data_overlay_groups/update_group",
            getDataOverlayGroupPathParameters(),
            NewApiDocs.getAddDataOverlayGroupRequest(),
            responseFields(NewApiDocs.getDataOverlayGroupResponse(""))))
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final DataOverlayGroup dataOverlayGroup = dataOverlayGroupService.getById(group.getId());
    assertEquals(dataOverlayGroup.getName(), data.getName());
  }

  @Test
  public void testUpdateDataOverlayGroupWithOldVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewDataOverlayGroupDTO data = createDataOverlayGroupDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/overlay_groups/{overlayGroupId}",
        TEST_PROJECT, group.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdateDataOverlayGroupWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String originalVersion = group.getEntityVersion() + "";

    final NewDataOverlayGroupDTO data = createDataOverlayGroupDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/overlay_groups/{overlayGroupId}",
        TEST_PROJECT, group.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final DataOverlayGroup dataOverlayGroup = dataOverlayGroupService.getById(group.getId());
    assertNotEquals(originalVersion, dataOverlayGroup.getEntityVersion() + "");
  }

  @Test
  public void testDeleteDataOverlayGroup() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/overlay_groups/{overlayGroupId}",
        TEST_PROJECT, group.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/projects/data_overlay_groups/delete_group",
            getDataOverlayGroupPathParameters()));

    assertNull(dataOverlayGroupService.getById(group.getId()));
  }

  @Test
  public void testDeleteNotExistingDataOverlayGroup() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/overlay_groups/{overlayGroupId}",
        TEST_PROJECT, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessDataOverlayGroup() throws Exception {
    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/overlay_groups/{overlayGroupId}",
        TEST_PROJECT, group.getId());

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeleteDataOverlayGroupWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String originalVersion = group.getEntityVersion() + "";

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/overlay_groups/{overlayGroupId}",
        TEST_PROJECT, group.getId())
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
    assertNull(dataOverlayGroupService.getById(group.getId()));
  }

  @Test
  public void testDeleteDataOverlayGroupWithWrongVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/overlay_groups/{overlayGroupId}",
        TEST_PROJECT, group.getId())
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testListDataOverlayGroups() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlay_groups/",
        TEST_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testListDataOverlayGroupsWithoutAccess() throws Exception {

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlay_groups/",
        TEST_PROJECT);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

}
