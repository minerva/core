package lcsb.mapviewer.web.api.project.map.layer.line;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.persist.dao.graphics.PolylineDataProperty;
import lcsb.mapviewer.services.interfaces.ILayerLineService;
import lcsb.mapviewer.services.interfaces.ILayerService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.map.bioentity.reaction.NewLineDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewLayerLineControllerTest extends ControllerIntegrationTest {

  @Autowired
  private ILayerLineService layerLineService;

  @Autowired
  private ILayerService layerService;

  @Autowired
  private IProjectService projectService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private Project project;

  private PolylineData line;

  private int layerId;
  private int lineId;
  private int mapId;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    final Layer layer = project.getTopModel().getLayers().iterator().next();
    layerId = layer.getId();
    line = layer.getLines().iterator().next();
    lineId = line.getId();
    projectService.removeBackground(project.getProjectBackgrounds().get(0));
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testGetPolylineData() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/lines/{lineId}",
        TEST_PROJECT, mapId, layerId, lineId)
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    String content = response.getContentAsString();
    Map<String, Object> data = objectMapper.readValue(content, new TypeReference<Map<String, Object>>() {
    });
    assertNotNull(data.get("layer"));
  }

  @Test
  public void testGetPolylineDataWildcardLayer() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/lines/{lineId}",
        TEST_PROJECT, mapId, "*", lineId)
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
  }

  @Test
  public void testGetNonExisting() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/lines/{lineId}",
        TEST_PROJECT, mapId, layerId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetPolylineDataWithoutPermission() throws Exception {
    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/lines/{lineId}",
        TEST_PROJECT, mapId, layerId, lineId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testCreatePolylineData() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewLineDTO data = createLineDTO();

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/lines/",
        TEST_PROJECT, mapId, layerId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    final Map<PolylineDataProperty, Object> filter = new HashMap<>();
    filter.put(PolylineDataProperty.PROJECT_ID, TEST_PROJECT);
    filter.put(PolylineDataProperty.MAP_ID, Collections.singletonList(mapId));
    filter.put(PolylineDataProperty.LAYER_ID, Collections.singletonList(layerId));
    filter.put(PolylineDataProperty.ID, Collections.singletonList(result.get("id")));
    final Page<PolylineData> layers = layerLineService.getAll(filter, Pageable.unpaged());

    assertEquals(1, layers.getNumberOfElements());
  }

  @Test
  public void testUpdatePolylineData() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewLineDTO data = createLineDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/lines/{lineId}",
        TEST_PROJECT, mapId, layerId, lineId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final PolylineData layerLine = layerLineService.getById(lineId);
    assertEquals(data.getColor(), layerLine.getColor());
  }

  @Test
  public void testUpdatePolylineDataWithOldVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewLineDTO data = createLineDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/lines/{lineId}",
        TEST_PROJECT, mapId, layerId, lineId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdatePolylineDataWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String originalVersion = line.getEntityVersion() + "";

    final NewLineDTO data = createLineDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/lines/{lineId}",
        TEST_PROJECT, mapId, layerId, lineId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final PolylineData layerLine = layerLineService.getById(lineId);
    assertNotEquals(originalVersion, layerLine.getEntityVersion() + "");
  }

  @Test
  public void testDeletePolylineData() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/lines/{lineId}",
        TEST_PROJECT, mapId, layerId, lineId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    assertNull(layerLineService.getById(lineId));
    assertNotNull(layerService.getById(layerId));
  }

  @Test
  public void testDeleteNotExistingPolylineData() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/lines/{lineId}",
        TEST_PROJECT, mapId, layerId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessPolylineData() throws Exception {
    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/lines/{lineId}",
        TEST_PROJECT, mapId, layerId, lineId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeletePolylineDataWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String originalVersion = line.getEntityVersion() + "";

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/lines/{lineId}",
        TEST_PROJECT, mapId, layerId, lineId)
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
    assertNull(layerLineService.getById(lineId));
  }

  @Test
  public void testDeletePolylineDataWithWrongVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/lines/{lineId}",
        TEST_PROJECT, mapId, layerId, lineId)
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testListPolylineDatas() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/lines/",
        TEST_PROJECT, mapId, layerId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testListPolylineDataWithoutAccess() throws Exception {

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/lines/",
        TEST_PROJECT, mapId, layerId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

}
