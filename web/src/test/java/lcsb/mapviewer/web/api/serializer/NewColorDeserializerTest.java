package lcsb.mapviewer.web.api.serializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import lcsb.mapviewer.modelutils.serializer.ColorSerializer;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.awt.Color;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewColorDeserializerTest extends ControllerIntegrationTest {


  private ObjectMapper objectMapper;

  @Before
  public void setup() {
    objectMapper = new ObjectMapper();

    final SimpleModule module = new SimpleModule();
    module.addSerializer(Color.class, new ColorSerializer());
    module.addDeserializer(Color.class, new ColorDeserializer());
    objectMapper.registerModule(module);

    objectMapper.registerModule(new SimpleModule());

  }

  @Test
  public void testGetDeserializeColor() throws Exception {
    Color color = Color.BLUE;
    String json = objectMapper.writeValueAsString(color);
    Color color2 = objectMapper.readValue(json, Color.class);
    assertEquals(color, color2);
  }

  @Test
  public void testGetDeserializeAlpha() throws Exception {
    Color color = new Color(11, 22, 33, 126);
    String json = objectMapper.writeValueAsString(color);
    Color color2 = objectMapper.readValue(json, Color.class);

    assertEquals(color, color2);
  }
}
