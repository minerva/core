package lcsb.mapviewer.web.api.project.map.layer.image;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerImage;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.persist.dao.graphics.LayerImageProperty;
import lcsb.mapviewer.services.interfaces.ILayerImageService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiDocs;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewLayerImageControllerTest extends ControllerIntegrationTest {

  @Autowired
  private ILayerImageService layerImageService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private LayerImage layerImage;

  private int layerId;
  private int glyphId;
  private int imageId;
  private int mapId;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    Project project = createProjectWithGlyph(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    final Layer layer = project.getTopModel().getLayers().iterator().next();
    layerId = layer.getId();
    layerImage = layer.getImages().iterator().next();
    imageId = layerImage.getId();
    glyphId = project.getGlyphs().iterator().next().getId();
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
    removeProject(TEST_PROJECT_ID);
  }

  @Test
  public void testDocsGetLayerImage() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/images/{imageId}",
        TEST_PROJECT, mapId, layerId, imageId)
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/projects/maps/layers/images/get_image",
            getLayerImagePathParameters(),
            responseFields(NewApiDocs.getLayerImageResponse(""))))
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

  }

  @Test
  public void testGetLayerImageWildcardLayer() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/images/{imageId}",
        TEST_PROJECT, mapId, "*", imageId)
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

  }

  @Test
  public void testGetNonExisting() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/images/{imageId}",
        TEST_PROJECT, mapId, layerId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetLayerImageWithoutPermission() throws Exception {
    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/images/{imageId}",
        TEST_PROJECT, mapId, layerId, imageId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDocsCreateLayerImage() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewImageDTO data = createLayerImageDTO();

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/images/",
        TEST_PROJECT,
        mapId, layerId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andDo(document("new_api/projects/maps/layers/images/add_image",
            getLayerPathParameters(),
            NewApiDocs.getAddLayerImageRequest(),
            responseFields(NewApiDocs.getLayerImageResponse(""))))
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    final Map<LayerImageProperty, Object> filter = new HashMap<>();
    filter.put(LayerImageProperty.PROJECT_ID, TEST_PROJECT);
    filter.put(LayerImageProperty.MAP_ID, Collections.singletonList(mapId));
    filter.put(LayerImageProperty.LAYER_ID, Collections.singletonList(layerId));
    filter.put(LayerImageProperty.ID, Collections.singletonList(result.get("id")));
    final Page<LayerImage> layers = layerImageService.getAll(filter, Pageable.unpaged());

    assertEquals(1, layers.getNumberOfElements());
  }

  private NewImageDTO createLayerImageDTO() {
    final NewImageDTO result = new NewImageDTO();
    result.setHeight(11.1);
    result.setWidth(12.1);
    result.setX(2.2);
    result.setY(3.3);
    result.setZ(5);
    result.setGlyph(glyphId);
    return result;
  }

  @Test
  public void testDocsUpdateLayerImage() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewImageDTO data = createLayerImageDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/images/{imageId}",
        TEST_PROJECT, mapId, layerId, imageId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/projects/maps/layers/images/update_image",
            getLayerImagePathParameters(),
            NewApiDocs.getAddLayerImageRequest(),
            responseFields(NewApiDocs.getLayerImageResponse(""))))
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final LayerImage layerImage = layerImageService.getById(imageId);
    assertEquals(data.getZ(), layerImage.getZ());
  }

  @Test
  public void testUpdateLayerImageWithOldVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewImageDTO data = createLayerImageDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/images/{imageId}",
        TEST_PROJECT, mapId, layerId, imageId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdateLayerImageWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String originalVersion = layerImage.getEntityVersion() + "";

    final NewImageDTO data = createLayerImageDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/images/{imageId}",
        TEST_PROJECT, mapId, layerId, imageId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final LayerImage layerImage = layerImageService.getById(imageId);
    assertNotEquals(originalVersion, layerImage.getEntityVersion() + "");
  }

  @Test
  public void testDocsDeleteLayerImage() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/images/{imageId}",
        TEST_PROJECT, mapId, layerId, imageId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/projects/maps/layers/images/delete_image",
            getLayerImagePathParameters()));

    assertNull(layerImageService.getById(imageId));
  }

  @Test
  public void testDeleteNotExistingLayerImage() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/images/{imageId}",
        TEST_PROJECT, mapId, layerId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessLayerImage() throws Exception {
    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/images/{imageId}",
        TEST_PROJECT, mapId, layerId, imageId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeleteLayerImageWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String originalVersion = layerImage.getEntityVersion() + "";

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/images/{imageId}",
        TEST_PROJECT, mapId, layerId, imageId)
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
    assertNull(layerImageService.getById(imageId));
  }

  @Test
  public void testDeleteLayerImageWithWrongVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/images/{imageId}",
        TEST_PROJECT, mapId, layerId, imageId)
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testDocsListLayerImages() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/images/",
        TEST_PROJECT, mapId, layerId)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("new_api/projects/maps/layers/images/list_images",
            getLayerPathParameters(),
            requestParameters(getPageableFilter()),
            NewApiDocs.getLayerImagesSearchResult()))
        .andExpect(status().isOk());
  }

  @Test
  public void testListLayerImagesWithoutAccess() throws Exception {

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/images/",
        TEST_PROJECT, mapId, layerId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testGetLayerImagesWithGlyph() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    Project projectWithGlyph = createProjectWithGlyph(TEST_PROJECT_ID);

    Model model = projectWithGlyph.getTopModel();
    Layer layer = model.getLayers().iterator().next();

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/images/",
        TEST_PROJECT_ID, model.getId(), layer.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }


}
