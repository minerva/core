package lcsb.mapviewer.web.api.project;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.junit.Assert.assertEquals;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewProjectTopMapControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private IProjectService projectService;

  @Before
  public void setUp() throws Exception {
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testGetTopMapForProject() throws Exception {
    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/topMap", BUILT_IN_PROJECT);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testChangeTopMapForProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    Project project = createAndPersistProject(TEST_PROJECT);
    int mapId = project.getTopModelData().getId();
    for (ModelData map : project.getModels()) {
      if (map.getId() != mapId) {
        mapId = map.getId();
        break;
      }
    }
    String content = "{\"id\":" + mapId + "}";
    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/topMap", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(content)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    project = projectService.getProjectByProjectId(TEST_PROJECT);
    assertEquals(mapId, project.getTopModelData().getId());
  }

  @Test
  public void testChangeTopMapWithoutAccess() throws Exception {
    Project project = createAndPersistProject(TEST_PROJECT);
    int mapId = project.getTopModelData().getId();
    for (ModelData map : project.getModels()) {
      if (map.getId() != mapId) {
        mapId = map.getId();
        break;
      }
    }
    String content = "{\"id\":" + mapId + "}";
    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/topMap", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(content);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testChangeTopMapForInvalid() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    createAndPersistProject(TEST_PROJECT);
    String content = "{\"id\":0}";
    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/topMap", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(content)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testChangeTopMapForInvalidData() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    String content = "{}";
    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/topMap", BUILT_IN_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(content)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

}
