package lcsb.mapviewer.web.api;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;

@Configuration
public class WebApiTestConfig {

  @Bean
  public ClientRegistrationRepository clientRegistrationRepository() {
    ClientRegistrationRepository mock = Mockito.mock(ClientRegistrationRepository.class);
    ClientRegistration clientRegistration = Mockito.mock(ClientRegistration.class);
    Mockito.when(clientRegistration.getClientName()).thenReturn("Orcid auth ");
    Mockito.when(mock.findByRegistrationId("orcid")).thenReturn(clientRegistration);
    return mock;
  }
}
