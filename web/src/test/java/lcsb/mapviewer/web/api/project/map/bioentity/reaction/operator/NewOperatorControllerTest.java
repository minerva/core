package lcsb.mapviewer.web.api.project.map.bioentity.reaction.operator;

import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.sbo.SBOTermOperatorType;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewOperatorControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IReactionService reactionService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private Project project;

  private int reactionId;
  private int mapId;
  private NodeOperator oprator;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    Set<Integer> ids = new HashSet<>();
    for (Reaction reaction : project.getTopModelData().getReactions()) {
      ids.add(reaction.getId());
    }
    reactionId = ids.iterator().next();
    oprator = project.getTopModel().getReactionByDbId(reactionId).getOperators().get(0);
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testCreateNodeOperator() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewOperatorNodeDTO data = createNodeOperatorDTO();

    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators/",
        TEST_PROJECT,
        mapId,
        reactionId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isCreated());

    Reaction reaction = reactionService.getReactionById(TEST_PROJECT, mapId, reactionId);

    assertEquals(project.getTopModel().getReactionByDbId(reactionId).getOperators().size() + 1, reaction.getOperators().size());
  }

  private NewOperatorNodeDTO createNodeOperatorDTO() {
    NewOperatorNodeDTO result = new NewOperatorNodeDTO();
    result.setSboTerm(SBOTermOperatorType.AND.getSBO());
    return result;
  }

  @Test
  public void testDeleteNodeOperator() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators/{operatorId}",
        TEST_PROJECT, mapId, reactionId, oprator.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    Reaction reaction = reactionService.getReactionById(TEST_PROJECT, mapId, reactionId);

    assertEquals(project.getTopModel().getReactionByDbId(reactionId).getOperators().size() - 1, reaction.getOperators().size());
  }

  @Test
  public void testDeleteNotExistingNodeOperator() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators/{operatorId}",
        TEST_PROJECT, mapId, reactionId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessNodeOperator() throws Exception {
    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators/{operatorId}",
        TEST_PROJECT, mapId, reactionId, oprator.getId());

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testListNodeOperators() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators/", TEST_PROJECT,
        mapId,
        reactionId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testListNodeOperatorsWithoutAccess() throws Exception {

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators/", TEST_PROJECT,
        mapId,
        reactionId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }
}
