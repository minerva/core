package lcsb.mapviewer.web.api.project.map.bioentity.reaction.operator;

import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.NewIdDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewOperatorInputControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IReactionService reactionService;

  @Autowired
  private IMinervaJobService minervaJobService;
  @Autowired
  private IProjectService projectService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private Project project;

  private int reactionId;
  private int productId;
  private int operatorId;
  private int mapId;
  private int inputId;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    Reaction reaction = project.getTopModelData().getReactions().iterator().next();
    reactionId = reaction.getId();
    productId = reaction.getProducts().get(0).getId();
    for (NodeOperator operator : reaction.getOperators()) {
      if (operator.isReactantOperator()) {
        operatorId = operator.getId();
        inputId = operator.getInputs().get(0).getId();
      }
    }
    projectService.removeBackground(project.getProjectBackgrounds().get(0));
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testGetInput() throws Exception {

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators/{operatorId}/inputs/{inputId}",
        TEST_PROJECT, mapId, reactionId, operatorId, inputId)
        .session(session);

    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
  }

  @Test
  public void testGetNonExisting() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators/{operatorId}/inputs/{inputId}",
        TEST_PROJECT, mapId, reactionId, operatorId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetInputWithoutPermission() throws Exception {
    RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators/{operatorId}/inputs/{inputId}",
        TEST_PROJECT, mapId, reactionId, operatorId, inputId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  private NewIdDTO createInputDTO(final int id) {
    NewIdDTO result = new NewIdDTO();
    result.setId(id);
    return result;
  }

  @Test
  public void testAddInput() throws Exception {
    NodeOperator operator = getOperatorFromDb();
    long elements = operator.getInputs().size();

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewIdDTO data = createInputDTO(productId);

    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators/{operatorId}/inputs/",
        TEST_PROJECT, mapId, reactionId, operatorId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    operator = getOperatorFromDb();
    assertEquals(elements + 1, operator.getInputs().size());
  }

  @Test
  public void testAddInputWithOldVersion() throws Exception {

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewIdDTO data = createInputDTO(productId);

    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators/{operatorId}/inputs/",
        TEST_PROJECT, mapId, reactionId, operatorId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());

  }

  @Test
  public void testAddInputWithGoodVersion() throws Exception {
    NodeOperator operator = getOperatorFromDb();
    long elements = operator.getInputs().size();
    String originalVersion = operator.getEntityVersion() + "";

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewIdDTO data = createInputDTO(productId);

    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators/{operatorId}/inputs/",
        TEST_PROJECT, mapId, reactionId, operatorId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    operator = getOperatorFromDb();
    assertEquals(elements + 1, operator.getInputs().size());

  }

  @Test
  public void testListInputs() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators/{operatorId}/inputs/",
        TEST_PROJECT, mapId, reactionId, operatorId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testDeleteInput() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    int size = getOperatorFromDb().getInputs().size();

    RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators/{operatorId}/inputs/{inputId}",
        TEST_PROJECT, mapId, reactionId, operatorId, inputId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    NodeOperator operator = getOperatorFromDb();
    assertEquals(size - 1, operator.getInputs().size());
  }

  private NodeOperator getOperatorFromDb() {
    NodeOperator operator = null;
    Reaction reaction = reactionService.getById(reactionId);
    for (NodeOperator node : reaction.getOperators()) {
      if (node.isReactantOperator()) {
        operator = node;
      }
    }
    return operator;
  }

  @Test
  public void testDeleteNotExistingInput() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators/{operatorId}/inputs/{inputId}",
        TEST_PROJECT, mapId, reactionId, operatorId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessInput() throws Exception {
    RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators/{operatorId}/inputs/{inputId}",
        TEST_PROJECT, mapId, reactionId, operatorId, inputId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeleteInputWithGoodVersion() throws Exception {
    NodeOperator operator = getOperatorFromDb();
    long elements = operator.getInputs().size();
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String originalVersion = project.getTopModel().getReactionByDbId(reactionId).getEntityVersion() + "";

    RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators/{operatorId}/inputs/{inputId}",
        TEST_PROJECT, mapId, reactionId, operatorId, inputId)
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    operator = getOperatorFromDb();

    assertEquals(elements - 1, operator.getInputs().size());
  }

  @Test
  public void testDeleteReactionWithWrongVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/operators/{operatorId}/inputs/{inputId}",
        TEST_PROJECT, mapId, reactionId, operatorId, inputId)
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }
}
