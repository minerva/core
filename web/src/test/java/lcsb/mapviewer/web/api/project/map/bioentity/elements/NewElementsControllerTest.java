package lcsb.mapviewer.web.api.project.map.bioentity.elements;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiDocs;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringJUnit4ClassRunner.class)
public class NewElementsControllerTest extends ControllerIntegrationTest {

  private final Logger logger = LogManager.getLogger();

  @Autowired
  private IUserService userService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private User anonymous;

  private Project project;

  private ModelData map;

  @Before
  public void setup() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    map = project.getTopModel().getModelData();
    anonymous = userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN);
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());
  }

  @After
  public void tearDown() throws Exception {
    removeProject(project);
  }

  @Test
  public void testDocsGetElementById() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    Project projectWithGlyph = createProjectWithGlyph(TEST_PROJECT_ID);
    try {
      Element element = null;
      for (Element e : projectWithGlyph.getTopModel().getElements()) {
        if (e.getGlyph() != null) {
          element = e;
        }
      }
      assertNotNull(element);
      RequestBuilder request = get(
          "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/elements/{elementId}",
          TEST_PROJECT_ID,
          element.getModelData().getId(),
          element.getId())
          .session(session);

      mockMvc.perform(request)
          .andExpect(status().is2xxSuccessful())
          .andDo(document("new_api/projects/maps/bioEntities/elements/get_by_id",
              getElementPathParameters(),
              responseFields(NewApiDocs.getElementResponse(""))))
          .andReturn().getResponse().getContentAsString();
    } finally {
      removeProject(projectWithGlyph);
    }

  }

  @Test
  public void testGetNotExistingElementById() throws Exception {
    RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, map.getId(), 0);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetElementByInvalidId() throws Exception {
    RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/elements/{elementId}",
        TEST_PROJECT, map.getId(), "blah");

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testExportAllToCsv() throws Exception {
    DownloadCsvRequest requestBody = new DownloadCsvRequest();
    requestBody.setColumns(Arrays.asList(
        "id",
        "description",
        "modelId",
        "mapName",
        "symbol",
        "abbreviation",
        "synonyms",
        "references",
        "name",
        "type",
        "complexId",
        "complexName",
        "compartmentId",
        "compartmentName",
        "charge",
        "fullName",
        "formula",
        "formerSymbols",
        "linkedSubmodelId",
        "elementId"));
    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/elements/:downloadCsv",
        TEST_PROJECT, "*")
        .content(objectMapper.writeValueAsBytes(requestBody))
        .contentType(MediaType.APPLICATION_JSON);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    int lines = content.split("\n").length;

    int elements = 0;
    for (ModelData model : project.getModels()) {
      elements += model.getElements().size();
    }
    assertEquals(elements + 1, lines);
  }

  @Test
  public void testExportAnnotationsToCsv() throws Exception {
    DownloadCsvRequest requestBody = new DownloadCsvRequest();
    requestBody.setColumns(Arrays.asList(
        "id",
        "modelId"));
    requestBody.setAnnotations(Collections.singletonList(
        "HGNC_SYMBOL"));
    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/elements/:downloadCsv",
        TEST_PROJECT, "*")
        .content(objectMapper.writeValueAsBytes(requestBody))
        .contentType(MediaType.APPLICATION_JSON);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    String[] lines = content.split("\n");
    assertEquals(requestBody.getColumns().size() + requestBody.getAnnotations().size(), lines[0].split(",").length);

  }

  @Test
  public void testExportInvalidAnnotationsToCsv() throws Exception {
    DownloadCsvRequest requestBody = new DownloadCsvRequest();
    requestBody.setColumns(Arrays.asList(
        "id",
        "modelId"));
    requestBody.setAnnotations(Collections.singletonList(
        "xx"));
    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/elements/:downloadCsv",
        TEST_PROJECT, "*")
        .content(objectMapper.writeValueAsBytes(requestBody))
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());

  }

  @Test
  public void testExportWithSelectedColumnsToCsv() throws Exception {
    DownloadCsvRequest requestBody = new DownloadCsvRequest();
    requestBody.setColumns(Arrays.asList(
        "id",
        "name"));
    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/elements/:downloadCsv",
        TEST_PROJECT, "*")
        .content(objectMapper.writeValueAsBytes(requestBody))
        .contentType(MediaType.APPLICATION_JSON);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    String[] lines = content.split("\n");

    for (String string : lines) {
      assertEquals(requestBody.getColumns().size(), string.split(",").length);
    }
  }

  @Test
  public void testExportWithTypeFilter() throws Exception {
    DownloadCsvRequest requestBody = new DownloadCsvRequest();
    requestBody.setColumns(Arrays.asList(
        "id",
        "elementId",
        "compartmentName"));
    requestBody.setTypes(Collections.singletonList(new GenericProtein("").getStringType()));
    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/elements/:downloadCsv",
        TEST_PROJECT, "*")
        .content(objectMapper.writeValueAsBytes(requestBody))
        .contentType(MediaType.APPLICATION_JSON);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    int lines = content.split("\n").length;

    int elements = 0;
    for (ModelData model : project.getModels()) {
      for (Element element : model.getElements()) {
        if (element instanceof Protein) {
          elements++;
        }

      }
    }
    assertEquals(elements + 1, lines);
  }

  @Test
  public void testExportWithSubmapFilter() throws Exception {
    DownloadCsvRequest requestBody = new DownloadCsvRequest();
    requestBody.setColumns(Arrays.asList(
        "id",
        "elementId"));
    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/elements/:downloadCsv",
        TEST_PROJECT, map.getId())
        .content(objectMapper.writeValueAsBytes(requestBody))
        .contentType(MediaType.APPLICATION_JSON);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    int lines = content.split("\n").length;

    int elements = map.getElements().size();
    assertEquals(elements + 1, lines);
  }

  @Test
  public void testExportWithSubmapFilter2() throws Exception {
    DownloadCsvRequest requestBody = new DownloadCsvRequest();
    requestBody.setColumns(Arrays.asList(
        "id",
        "elementId"));
    requestBody.setSubmaps(Collections.singletonList(map.getId()));
    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/elements/:downloadCsv",
        TEST_PROJECT, "*")
        .content(objectMapper.writeValueAsBytes(requestBody))
        .contentType(MediaType.APPLICATION_JSON);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    int lines = content.split("\n").length;

    int elements = map.getElements().size();
    assertEquals(elements + 1, lines);
  }

  @Test
  public void testExportWithIncludedCompartmentSpecies() throws Exception {
    DownloadCsvRequest requestBody = new DownloadCsvRequest();
    requestBody.setColumns(Arrays.asList(
        "id",
        "elementId"));
    Compartment compartment = null;
    for (Element element : map.getElements()) {
      if (element instanceof Compartment) {
        compartment = (Compartment) element;
      }

    }
    assertNotNull(compartment);
    requestBody.setIncludedCompartmentIds(Collections.singletonList(compartment.getId()));
    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/elements/:downloadCsv",
        TEST_PROJECT, "*")
        .content(objectMapper.writeValueAsBytes(requestBody))
        .contentType(MediaType.APPLICATION_JSON);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    int lines = content.split("\n").length;

    int elements = compartment.getElements().size();
    assertEquals(elements + 1, lines);
  }

  @Test
  public void testExportWithExcludedCompartmentSpecies() throws Exception {
    DownloadCsvRequest requestBody = new DownloadCsvRequest();
    requestBody.setColumns(Arrays.asList(
        "id",
        "elementId"));
    Compartment compartment = null;
    for (Element element : map.getElements()) {
      if (element instanceof Compartment) {
        compartment = (Compartment) element;
      }

    }
    assertNotNull(compartment);
    requestBody.setExcludedCompartmentIds(Collections.singletonList(compartment.getId()));
    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/elements/:downloadCsv",
        TEST_PROJECT, "*")
        .content(objectMapper.writeValueAsBytes(requestBody))
        .contentType(MediaType.APPLICATION_JSON);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    int lines = content.split("\n").length;

    int elements = 0;
    for (ModelData model : project.getModels()) {
      elements += model.getElements().size();
    }

    elements -= compartment.getElements().size();
    assertEquals(elements + 1, lines);
  }

  @Test
  public void testDocsExportToCsvAndDocument() throws Exception {
    Compartment compartment = null;
    for (Element element : map.getElements()) {
      if (element instanceof Compartment) {
        compartment = (Compartment) element;
      }

    }
    assertNotNull(compartment);

    DownloadCsvRequest requestBody = new DownloadCsvRequest();
    requestBody.setSubmaps(Collections.singletonList(map.getId()));
    requestBody.setTypes(Collections.singletonList(new SimpleMolecule("").getStringType()));
    requestBody.setIncludedCompartmentIds(Collections.singletonList(compartment.getId()));
    requestBody.setColumns(Arrays.asList(
        "id",
        "modelId"));
    requestBody.setAnnotations(Collections.singletonList(
        "CHEBI"));

    RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/models/{mapId}/bioEntities/elements/:downloadCsv",
        TEST_PROJECT, "*")
        .content(objectMapper.writeValueAsBytes(requestBody))
        .contentType(MediaType.APPLICATION_JSON);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("new_api/projects/maps/bioEntities/elements/export_to_csv",
            getMapPathParameters(),
            NewApiDocs.getElementCsvRequest()))
        .andReturn().getResponse().getContentAsString();
  }

}
