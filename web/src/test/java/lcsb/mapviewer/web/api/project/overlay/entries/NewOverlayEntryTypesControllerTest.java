package lcsb.mapviewer.web.api.project.overlay.entries;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayEntry;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IDataOverlayEntryService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.junit.Assert.assertEquals;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewOverlayEntryTypesControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IUserService userService;

  @Autowired
  private IDataOverlayEntryService dataOverlayEntryService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private User anonymous;

  private Project project;

  private DataOverlay dataOverlay;

  private DataOverlayEntry dataOverlayEntry;

  private String type;

  @Before
  public void setup() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    anonymous = userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN);
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());

    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    dataOverlay = super.createOverlay(project, admin, "element_identifier\tvalue\ttype\n\t-1\tProtein");
    dataOverlayEntry = dataOverlay.getEntries().iterator().next();
    type = Protein.class.getName();
  }

  @After
  public void tearDown() throws Exception {
    removeProject(project);
  }

  @Test
  public void testCreateType() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewTypeDTO data = createTypeDTO();

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}/types/",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isCreated());

    final DataOverlayEntry entry = dataOverlayEntryService.getById(dataOverlayEntry.getId(), true);

    assertEquals(dataOverlayEntry.getTypes().size() + 1, entry.getTypes().size());
  }

  private NewTypeDTO createTypeDTO() {
    final NewTypeDTO result = new NewTypeDTO();
    result.setType(GenericProtein.class);
    return result;
  }

  @Test
  public void testDeleteType() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}/types/{type}",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId(),
        type)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    final DataOverlayEntry entry = dataOverlayEntryService.getById(dataOverlayEntry.getId(), true);

    assertEquals(dataOverlayEntry.getTypes().size() - 1, entry.getTypes().size());
  }

  @Test
  public void testDeleteInvalidType() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}/types/{type}",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId(),
        "blah")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());

    final DataOverlayEntry entry = dataOverlayEntryService.getById(dataOverlayEntry.getId(), true);

    assertEquals(dataOverlayEntry.getTypes().size(), entry.getTypes().size());
  }

  @Test
  public void testDeleteNotExistingType() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}/types/{type}",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId(),
        Gene.class.getName())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessType() throws Exception {
    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}/types/{type}",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId(),
        type);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testListTypes() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}/types/",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse().getContentAsString();
  }

  @Test
  public void testListSynonymsWithoutAccess() throws Exception {

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/entries/{overlayEntryId}/types/",
        TEST_PROJECT,
        dataOverlay.getId(),
        dataOverlayEntry.getId());

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }
}
