package lcsb.mapviewer.web.api.project.map.layer.oval;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.persist.dao.graphics.LayerOvalProperty;
import lcsb.mapviewer.services.interfaces.ILayerOvalService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.awt.Color;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewLayerOvalControllerTest extends ControllerIntegrationTest {

  @Autowired
  private ILayerOvalService layerOvalService;

  @Autowired
  private IProjectService projectService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private LayerOval layerOval;

  private int layerId;
  private int layerOvalId;
  private int mapId;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    Project project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    final Layer layer = project.getTopModel().getLayers().iterator().next();
    layerId = layer.getId();
    layerOval = layer.getOvals().iterator().next();
    layerOvalId = layerOval.getId();
    projectService.removeBackground(project.getProjectBackgrounds().get(0));
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testGetLayerOval() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/ovals/{layerOvalId}",
        TEST_PROJECT, mapId, layerId, layerOvalId)
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });
    assertTrue(result.containsKey("lineWidth"));
  }

  @Test
  public void testGetLayerOvalWildcardLayer() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/ovals/{layerOvalId}",
        TEST_PROJECT, mapId, "*", layerOvalId)
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });
    assertTrue(result.containsKey("lineWidth"));
  }

  @Test
  public void testGetNonExisting() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/ovals/{layerOvalId}",
        TEST_PROJECT, mapId, layerId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetLayerOvalWithoutPermission() throws Exception {
    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/ovals/{layerOvalId}",
        TEST_PROJECT, mapId, layerId, layerOvalId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testCreateLayerOval() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewOvalDTO data = createLayerOvalDTO();

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/ovals/",
        TEST_PROJECT,
        mapId, layerId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    final Map<LayerOvalProperty, Object> filter = new HashMap<>();
    filter.put(LayerOvalProperty.PROJECT_ID, TEST_PROJECT);
    filter.put(LayerOvalProperty.MAP_ID, Collections.singletonList(mapId));
    filter.put(LayerOvalProperty.LAYER_ID, Collections.singletonList(layerId));
    filter.put(LayerOvalProperty.ID, Collections.singletonList(result.get("id")));
    final Page<LayerOval> layers = layerOvalService.getAll(filter, Pageable.unpaged());

    assertEquals(1, layers.getNumberOfElements());
  }

  private NewOvalDTO createLayerOvalDTO() {
    final NewOvalDTO result = new NewOvalDTO();
    result.setColor(Color.BLUE);
    result.setHeight(11.1);
    result.setWidth(12.1);
    result.setX(2.2);
    result.setY(3.3);
    result.setZ(5);
    return result;
  }

  @Test
  public void testUpdateLayerOval() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewOvalDTO data = createLayerOvalDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/ovals/{ovalId}",
        TEST_PROJECT, mapId, layerId, layerOvalId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final LayerOval layerOval = layerOvalService.getById(layerOvalId);
    assertEquals(data.getColor(), layerOval.getColor());
  }

  @Test
  public void testUpdateLayerOvalWithOldVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewOvalDTO data = createLayerOvalDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/ovals/{ovalId}",
        TEST_PROJECT, mapId, layerId, layerOvalId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdateLayerOvalWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String originalVersion = layerOval.getEntityVersion() + "";

    final NewOvalDTO data = createLayerOvalDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/ovals/{ovalId}",
        TEST_PROJECT, mapId, layerId, layerOvalId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final LayerOval layerOval = layerOvalService.getById(layerOvalId);
    assertNotEquals(originalVersion, layerOval.getEntityVersion() + "");
  }

  @Test
  public void testDeleteLayerOval() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/ovals/{ovalId}",
        TEST_PROJECT, mapId, layerId, layerOvalId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    assertNull(layerOvalService.getById(layerOvalId));
  }

  @Test
  public void testDeleteNotExistingLayerOval() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/ovals/{ovalId}",
        TEST_PROJECT, mapId, layerId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessLayerOval() throws Exception {
    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/ovals/{ovalId}",
        TEST_PROJECT, mapId, layerId, layerOvalId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeleteLayerOvalWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String originalVersion = layerOval.getEntityVersion() + "";

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/ovals/{ovalId}",
        TEST_PROJECT, mapId, layerId, layerOvalId)
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
    assertNull(layerOvalService.getById(layerOvalId));
  }

  @Test
  public void testDeleteLayerOvalWithWrongVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/ovals/{ovalId}",
        TEST_PROJECT, mapId, layerId, layerOvalId)
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testListLayerOvals() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/ovals/",
        TEST_PROJECT, mapId, layerId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testListLayerOvalsWithoutAccess() throws Exception {

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/ovals/",
        TEST_PROJECT, mapId, layerId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

}
