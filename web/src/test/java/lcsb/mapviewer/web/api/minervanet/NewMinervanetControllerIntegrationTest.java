package lcsb.mapviewer.web.api.minervanet;

import lcsb.mapviewer.services.http.HttpClientFactory;
import lcsb.mapviewer.services.http.HttpClientFactoryImpl;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiDocs;
import org.apache.http.HttpEntity;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicStatusLine;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.io.ByteArrayInputStream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewMinervanetControllerIntegrationTest extends ControllerIntegrationTest {

  @Autowired
  private NewMinervaNetController newMinervaNetController;


  @After
  public final void tearDown() {
    newMinervaNetController.setHttpClientFactory(new HttpClientFactoryImpl());
  }

  @Test
  public void testCreateReportError() throws Exception {
    HttpEntity httpEntity = mock(HttpEntity.class);
    when(httpEntity.getContent()).thenReturn(new ByteArrayInputStream("{}".getBytes()));

    CloseableHttpResponse expected = mock(CloseableHttpResponse.class);
    when(expected.getEntity()).thenReturn(httpEntity);
    when(expected.getStatusLine()).thenReturn(new BasicStatusLine(HttpVersion.HTTP_1_0, 200, ""));
    CloseableHttpClient httpClient = mock(CloseableHttpClient.class);
    when(httpClient.execute(any())).thenReturn(expected);

    HttpClientFactory httpClientFactory = mock(HttpClientFactory.class);
    when(httpClientFactory.create()).thenReturn(httpClient);

    newMinervaNetController.setHttpClientFactory(httpClientFactory);

    NewErrorReport report = createErrorReport();

    RequestBuilder request = post("/minerva/new_api/minervanet/submitError")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(report));

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("new_api/minervanet/add_error_report",
            responseFields(NewApiDocs.getErrorReportResponse(""))))
        .andReturn().getResponse().getContentAsString();
  }

  private NewErrorReport createErrorReport() {
    return new NewErrorReport();
  }

  @Test
  public void testCreateReportErrorProblemWithMinervaNet() throws Exception {
    HttpEntity httpEntity = mock(HttpEntity.class);
    when(httpEntity.getContent()).thenReturn(new ByteArrayInputStream("{}".getBytes()));

    CloseableHttpResponse expected = mock(CloseableHttpResponse.class);
    when(expected.getEntity()).thenReturn(httpEntity);
    when(expected.getStatusLine()).thenReturn(new BasicStatusLine(HttpVersion.HTTP_1_0, 400, ""));
    CloseableHttpClient httpClient = mock(CloseableHttpClient.class);
    when(httpClient.execute(any())).thenReturn(expected);

    HttpClientFactory httpClientFactory = mock(HttpClientFactory.class);
    when(httpClientFactory.create()).thenReturn(httpClient);

    newMinervaNetController.setHttpClientFactory(httpClientFactory);

    NewErrorReport report = createErrorReport();

    RequestBuilder request = post("/minerva/new_api/minervanet/submitError")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(report));

    mockMvc.perform(request)
        .andExpect(status().isFailedDependency())
        .andReturn().getResponse().getContentAsString();
  }
}
