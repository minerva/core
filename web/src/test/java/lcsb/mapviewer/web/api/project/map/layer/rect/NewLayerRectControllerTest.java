package lcsb.mapviewer.web.api.project.map.layer.rect;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.persist.dao.graphics.LayerRectProperty;
import lcsb.mapviewer.services.interfaces.ILayerRectService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.awt.Color;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewLayerRectControllerTest extends ControllerIntegrationTest {

  @Autowired
  private ILayerRectService layerRectService;

  @Autowired
  private IProjectService projectService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private LayerRect layerRect;

  private int layerId;
  private int layerRectId;
  private int mapId;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    Project project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    final Layer layer = project.getTopModel().getLayers().iterator().next();
    layerId = layer.getId();
    layerRect = layer.getRectangles().iterator().next();
    layerRectId = layerRect.getId();
    projectService.removeBackground(project.getProjectBackgrounds().get(0));
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testGetLayerRect() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/rects/{layerRectId}",
        TEST_PROJECT, mapId, layerId, layerRectId)
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });
    assertTrue(result.containsKey("lineWidth"));
  }

  @Test
  public void testGetLayerRectWildcardLayer() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/rects/{layerRectId}",
        TEST_PROJECT, mapId, "*", layerRectId)
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });
    assertTrue(result.containsKey("lineWidth"));
  }

  @Test
  public void testGetNonExisting() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/rects/{layerRectId}",
        TEST_PROJECT, mapId, layerId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetLayerRectWithoutPermission() throws Exception {
    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/rects/{layerRectId}",
        TEST_PROJECT, mapId, layerId, layerRectId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testCreateLayerRect() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewRectDTO data = createLayerRectDTO();

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/rects/",
        TEST_PROJECT,
        mapId, layerId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    final Map<LayerRectProperty, Object> filter = new HashMap<>();
    filter.put(LayerRectProperty.PROJECT_ID, TEST_PROJECT);
    filter.put(LayerRectProperty.MAP_ID, Collections.singletonList(mapId));
    filter.put(LayerRectProperty.LAYER_ID, Collections.singletonList(layerId));
    filter.put(LayerRectProperty.ID, Collections.singletonList(result.get("id")));
    final Page<LayerRect> layers = layerRectService.getAll(filter, Pageable.unpaged());

    assertEquals(1, layers.getNumberOfElements());
  }

  private NewRectDTO createLayerRectDTO() {
    final NewRectDTO result = new NewRectDTO();
    result.setFillColor(Color.BLUE);
    result.setBorderColor(Color.LIGHT_GRAY);
    result.setHeight(11.1);
    result.setWidth(12.1);
    result.setX(2.2);
    result.setY(3.3);
    result.setZ(5);
    return result;
  }

  @Test
  public void testUpdateLayerRect() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewRectDTO data = createLayerRectDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/rects/{rectId}",
        TEST_PROJECT, mapId, layerId, layerRectId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final LayerRect layerRect = layerRectService.getById(layerRectId);
    assertEquals(data.getFillColor(), layerRect.getFillColor());
  }

  @Test
  public void testUpdateLayerRectWithOldVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewRectDTO data = createLayerRectDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/rects/{rectId}",
        TEST_PROJECT, mapId, layerId, layerRectId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdateLayerRectWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String originalVersion = layerRect.getEntityVersion() + "";

    final NewRectDTO data = createLayerRectDTO();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/rects/{rectId}",
        TEST_PROJECT, mapId, layerId, layerRectId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final LayerRect layerRect = layerRectService.getById(layerRectId);
    assertNotEquals(originalVersion, layerRect.getEntityVersion() + "");
  }

  @Test
  public void testDeleteLayerRect() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/rects/{rectId}",
        TEST_PROJECT, mapId, layerId, layerRectId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    assertNull(layerRectService.getById(layerRectId));
  }

  @Test
  public void testDeleteNotExistingLayerRect() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/rects/{rectId}",
        TEST_PROJECT, mapId, layerId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessLayerRect() throws Exception {
    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/rects/{rectId}",
        TEST_PROJECT, mapId, layerId, layerRectId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeleteLayerRectWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String originalVersion = layerRect.getEntityVersion() + "";

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/rects/{rectId}",
        TEST_PROJECT, mapId, layerId, layerRectId)
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
    assertNull(layerRectService.getById(layerRectId));
  }

  @Test
  public void testDeleteLayerRectWithWrongVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/rects/{rectId}",
        TEST_PROJECT, mapId, layerId, layerRectId)
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testListLayerRects() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/rects/",
        TEST_PROJECT, mapId, layerId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testListLayerRectsWithoutAccess() throws Exception {

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/layers/{layerId}/rects/",
        TEST_PROJECT, mapId, layerId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

}
