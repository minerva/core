package lcsb.mapviewer.web.api.project.map.bioentity.element.uniprot;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.model.map.species.field.UniprotRecord;
import lcsb.mapviewer.persist.dao.map.species.UniprotRecordProperty;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUniprotRecordService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewUniprotRecordControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IUniprotRecordService uniprotRecordService;

  @Autowired
  private IMinervaJobService minervaJobService;
  @Autowired
  private IProjectService projectService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private Project project;

  private int elementId;
  private int uniprotId;
  private UniprotRecord uniprot;
  private int compartmentId;
  private int complexId;
  private int mapId;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    for (final Element element : project.getTopModelData().getElements()) {
      if (element instanceof Compartment) {
        compartmentId = element.getId();
      } else if (element instanceof Complex) {
        complexId = element.getId();
      } else if (element instanceof Species) {
        for (final UniprotRecord uniprot : ((Species) element).getUniprots()) {
          uniprotId = uniprot.getId();
          this.uniprot = uniprot;
          elementId = uniprot.getSpecies().getId();
        }
      }
    }
    projectService.removeBackground(project.getProjectBackgrounds().get(0));
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testGetUniprot() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/{uniprotId}",
        TEST_PROJECT, mapId, elementId, uniprotId)
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });
    assertNotNull(result.get("uniprotId"));
  }

  @Test
  public void testGetNonExisting() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/{uniprotId}",
        TEST_PROJECT, mapId, elementId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetUniprotWithoutPermission() throws Exception {
    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/{uniprotId}",
        TEST_PROJECT, mapId, elementId, uniprotId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testCreateUniprot() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewUniprotRecordDTO dto = createUniprotRecordDTO();

    final RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/",
        TEST_PROJECT,
        mapId,
        elementId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(dto))
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    final Map<UniprotRecordProperty, Object> filter = new HashMap<>();
    filter.put(UniprotRecordProperty.PROJECT_ID, Collections.singletonList(TEST_PROJECT));
    filter.put(UniprotRecordProperty.MAP_ID, Collections.singletonList(mapId));
    filter.put(UniprotRecordProperty.ELEMENT_ID, Collections.singletonList(elementId));
    filter.put(UniprotRecordProperty.ID, Collections.singletonList((int) result.get("id")));
    final Page<UniprotRecord> elements = uniprotRecordService.getAll(filter, Pageable.unpaged());

    assertEquals(1, elements.getNumberOfElements());

  }

  @Test
  public void testCreateUniprotForCompartment() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/",
        TEST_PROJECT,
        mapId,
        compartmentId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(createUniprotRecordDTO()))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testUpdateUniprot() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewUniprotRecordDTO data = createUniprotRecordDTO(uniprot);
    data.setUniprotId("xxx");

    final RequestBuilder request = put(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/{uniprotId}",
        TEST_PROJECT, mapId, elementId, uniprotId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final UniprotRecord updatedUniprot = uniprotRecordService.getById(uniprotId);
    assertEquals(data.getUniprotId(), updatedUniprot.getUniprotId());
    assertNotEquals(updatedUniprot.getEntityVersion(), uniprot.getEntityVersion());
  }


  private NewUniprotRecordDTO createUniprotRecordDTO() {
    final NewUniprotRecordDTO result = new NewUniprotRecordDTO();
    result.setUniprotId("x");
    return result;
  }

  private NewUniprotRecordDTO createUniprotRecordDTO(final UniprotRecord uniprot) {
    final NewUniprotRecordDTO result = new NewUniprotRecordDTO();
    result.setUniprotId(uniprot.getUniprotId());
    return result;
  }

  @Test
  public void testDeleteUniprot() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/{uniprotId}",
        TEST_PROJECT, mapId, elementId, uniprotId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    final UniprotRecord mr = uniprotRecordService.getById(uniprotId);
    assertNull(mr);
  }

  @Test
  public void testDeleteNoAccessUniprot() throws Exception {
    final RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/{uniprotId}",
        TEST_PROJECT, mapId, elementId, uniprotId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());

    final UniprotRecord mr = uniprotRecordService.getById(uniprotId);
    assertNotNull(mr);
  }

  @Test
  public void testDeleteNonExistingUniprot() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/{uniprotId}",
        TEST_PROJECT, mapId, elementId, 1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());

  }

  @Test
  public void testGetUniprots() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/uniprotRecords/",
        TEST_PROJECT, mapId, elementId)
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();

    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });
    assertTrue((int) result.get("size") > 0);
  }

}
