package lcsb.mapviewer.web.api.project;

import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IMiriamService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewProjectOrganismControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private IProjectService projectService;

  @Autowired
  private IMiriamService miriamService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testGetNonExistingOrganism() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/organism", BUILT_IN_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetOrganism() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createAndPersistProject(TEST_PROJECT);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/organism", TEST_PROJECT)
        .session(session);

    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
  }

  @Test
  public void testCreateOrganism() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Project project = createAndPersistProject(TEST_PROJECT);
    project.setOrganism(null);
    projectService.update(project);

    NewMiriamDataDTO data = createMiriamDataDTO(MiriamType.TAXONOMY, "9605");

    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/organism", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    minervaJobService.waitForTasksToFinish();
    project = projectService.getProjectByProjectId(TEST_PROJECT);
    assertNotNull(project.getOrganism().getLink());
  }

  @Test
  public void testCreateOrganismWhenOrganismExists() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createAndPersistProject(TEST_PROJECT);

    NewMiriamDataDTO data = createMiriamDataDTO(MiriamType.TAXONOMY, "9605");

    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/organism", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isConflict());
  }

  @Test
  public void testCreateInvalidOrganism() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Project project = createAndPersistProject(TEST_PROJECT);
    project.setOrganism(null);
    projectService.update(project);

    NewMiriamDataDTO data = createMiriamDataDTO();

    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/organism", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testUpdateOrganism() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createAndPersistProject(TEST_PROJECT);

    NewMiriamDataDTO data = createMiriamDataDTO(MiriamType.TAXONOMY, "9604");

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/organism", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    Project project = projectService.getProjectByProjectId(TEST_PROJECT);
    assertEquals(data.getResource(), project.getOrganism().getResource());
  }

  @Test
  public void testUpdateOrganismWithMissingData() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createAndPersistProject(TEST_PROJECT);

    NewMiriamDataDTO data = createMiriamDataDTO(MiriamType.TAXONOMY, null);

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/organism", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testUpdateOrganismWithOldVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createAndPersistProject(TEST_PROJECT);

    NewMiriamDataDTO data = createMiriamDataDTO(MiriamType.TAXONOMY, "D010301");

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/organism", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdateOrganismWithGoodVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Project project = createAndPersistProject(TEST_PROJECT);

    String originalVersion = project.getOrganism().getEntityVersion() + "";

    NewMiriamDataDTO data = createMiriamDataDTO(MiriamType.TAXONOMY, "D010301");

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/organism", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    project = projectService.getProjectByProjectId(TEST_PROJECT);
    assertNotEquals(originalVersion, project.getOrganism().getEntityVersion() + "");
  }

  @Test
  public void testDeleteOrganism() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    Project project = createAndPersistProject(TEST_PROJECT);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/organism", TEST_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    assertNull(projectService.getProjectByProjectId(TEST_PROJECT).getOrganism());
    assertNull(miriamService.getById(project.getOrganism().getId()));
  }

  @Test
  public void testDeleteNotExistingProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/organism", TEST_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNotExistingOrganism() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/organism", BUILT_IN_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteOrganismWithGoodVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Project project = createAndPersistProject(TEST_PROJECT);

    String originalVersion = project.getOrganism().getEntityVersion() + "";

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/organism", TEST_PROJECT)
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    project = projectService.getProjectByProjectId(TEST_PROJECT);

    assertNull(project.getOrganism());
  }

  @Test
  public void testDeleteOrganismWithWrongVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createAndPersistProject(TEST_PROJECT);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/organism", TEST_PROJECT)
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  private NewMiriamDataDTO createMiriamDataDTO() {
    return createMiriamDataDTO(MiriamType.PUBMED, "12345");
  }

  private NewMiriamDataDTO createMiriamDataDTO(final MiriamType type, final String resource) {
    NewMiriamDataDTO data = new NewMiriamDataDTO();
    data.setResource(resource);
    data.setType(type);
    return data;
  }

}
