package lcsb.mapviewer.web.api.project.map.bioentity.reaction;

import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.map.bioentity.NewSynonymDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewReactionSynonymControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IReactionService reactionService;

  @Autowired
  private IMinervaJobService minervaJobService;
  @Autowired
  private IProjectService projectService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private Project project;

  private int reactionId;
  private int mapId;
  private String synonym;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    Set<Integer> ids = new HashSet<>();
    for (Reaction reaction : project.getTopModelData().getReactions()) {
      ids.add(reaction.getId());
    }
    reactionId = ids.iterator().next();
    projectService.removeBackground(project.getProjectBackgrounds().get(0));
    synonym = project.getTopModel().getReactionByDbId(reactionId).getSynonyms().get(0);
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testCreateSynonym() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewSynonymDTO data = createSynonymDTO();

    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/synonyms/",
        TEST_PROJECT,
        mapId,
        reactionId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isCreated());

    Reaction reaction = reactionService.getReactionById(TEST_PROJECT, mapId, reactionId);

    assertEquals(project.getTopModel().getReactionByDbId(reactionId).getSynonyms().size() + 1, reaction.getSynonyms().size());
  }

  private NewSynonymDTO createSynonymDTO() {
    NewSynonymDTO result = new NewSynonymDTO();
    result.setSynonym("Synonym " + counter++);
    return result;
  }

  @Test
  public void testDeleteSynonym() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/synonyms/{synonym}",
        TEST_PROJECT, mapId, reactionId, synonym)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    Reaction reaction = reactionService.getReactionById(TEST_PROJECT, mapId, reactionId);

    assertEquals(project.getTopModel().getReactionByDbId(reactionId).getSynonyms().size() - 1, reaction.getSynonyms().size());
  }

  @Test
  public void testDeleteNotExistingSynonym() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/synonyms/{synonym}",
        TEST_PROJECT, mapId, reactionId, "blah")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessSynonym() throws Exception {
    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/synonyms/{synonym}",
        TEST_PROJECT, mapId, reactionId, synonym);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testListSynonyms() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/synonyms/", TEST_PROJECT,
        mapId,
        reactionId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testListSynonymsWithoutAccess() throws Exception {

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/synonyms/", TEST_PROJECT,
        mapId,
        reactionId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }
}
