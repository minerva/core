package lcsb.mapviewer.web.api.project.map.bioentity.element;

import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewElementFormerSymbolControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IElementService elementService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private Project project;

  private int elementId;
  private int mapId;
  private String formerSymbol;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    final Set<Integer> ids = new HashSet<>();
    for (final Element element : project.getTopModelData().getElements()) {
      if (element instanceof Compartment) {
        ids.add(element.getId());
      }
    }
    elementId = ids.iterator().next();
    formerSymbol = project.getTopModel().getElementByDbId(elementId).getFormerSymbols().get(0);
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testCreateFormerSymbol() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewFormerSymbolDTO data = createFormerSymbolDTO();

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/formerSymbols/",
        TEST_PROJECT,
        mapId,
        elementId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isCreated());

    final Element element = elementService.getById(elementId);

    assertEquals(project.getTopModel().getElementByDbId(elementId).getFormerSymbols().size() + 1, element.getFormerSymbols().size());
  }

  private NewFormerSymbolDTO createFormerSymbolDTO() {
    final NewFormerSymbolDTO result = new NewFormerSymbolDTO();
    result.setSymbol("FormerSymbol " + counter++);
    return result;
  }

  @Test
  public void testDeleteFormerSymbol() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/formerSymbols/{formerSymbol}",
        TEST_PROJECT, mapId, elementId, formerSymbol)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    final Element element = elementService.getById(elementId);

    assertEquals(project.getTopModel().getElementByDbId(elementId).getFormerSymbols().size() - 1, element.getFormerSymbols().size());
  }

  @Test
  public void testDeleteNotExistingFormerSymbol() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/formerSymbols/{formerSymbol}",
        TEST_PROJECT, mapId, elementId, "blah")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessFormerSymbol() throws Exception {
    final RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/formerSymbols/{formerSymbol}",
        TEST_PROJECT, mapId, elementId, formerSymbol);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testListFormerSymbols() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/formerSymbols/", TEST_PROJECT,
        mapId,
        elementId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testListFormerSymbolsWithoutAccess() throws Exception {

    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/formerSymbols/", TEST_PROJECT,
        mapId,
        elementId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }
}
