package lcsb.mapviewer.web.api.project.overlay;

import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IDataOverlayService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.user.NewUserLoginDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewOverlayOwnerControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IUserService userService;

  @Autowired
  private IDataOverlayService dataOverlayService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private User anonymous;

  private Project project;

  private DataOverlay dataOverlay;

  private String ownerLogin;

  @Before
  public void setup() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    anonymous = userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN);
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());

    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    dataOverlay = super.createOverlay(TEST_PROJECT, admin);

    ownerLogin = anonymous.getLogin();
  }

  @After
  public void tearDown() throws Exception {
    removeProject(project);
  }


  @Test
  public void testSetOwner() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewUserLoginDTO data = createUserDTO(ownerLogin);

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/owner/",
        TEST_PROJECT,
        dataOverlay.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    final DataOverlay overlay = dataOverlayService.getById(dataOverlay.getId());

    assertNotEquals(overlay.getCreator().getId(), dataOverlay.getCreator().getId());
  }

  @Test
  public void testSetEmptyOwner() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewUserLoginDTO data = createUserDTO(null);

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/owner/",
        TEST_PROJECT,
        dataOverlay.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());

    final DataOverlay overlay = dataOverlayService.getById(dataOverlay.getId());

    assertNotNull(overlay.getCreator());
  }

  private NewUserLoginDTO createUserDTO(final String login) {
    final NewUserLoginDTO result = new NewUserLoginDTO();
    result.setLogin(login);
    return result;
  }

}
