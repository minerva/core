package lcsb.mapviewer.web.api.plugin;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.plugin.Plugin;
import lcsb.mapviewer.services.interfaces.IPluginService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiDocs;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("webCtdTestProfile")
public class NewPluginControllerTest extends ControllerIntegrationTest {

  private static final String PLUGIN_URL = "https://minerva-dev.lcsb.uni.lu/plugins/starter-kit/plugin.js";

  private static final String PLUGIN_HASH = "b7a875a0536949d4d8a2f834dc54489e";

  @Autowired
  private IPluginService pluginService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private List<String> pluginHashes = new ArrayList<>();

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    pluginHashes = new ArrayList<>();
  }

  @After
  public void tearDown() throws Exception {
    for (String hash : pluginHashes) {
      Plugin plugin = pluginService.getByHash(hash);
      if (plugin != null) {
        pluginService.delete(plugin);
      }
    }
  }

  @Test
  public void testDocsGetPlugin() throws Exception {
    Plugin plugin = createAndPersistPlugin(PLUGIN_HASH);

    final RequestBuilder request = get("/minerva/new_api/plugins/{hash}", plugin.getHash());

    mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/plugins/get_by_hash",
            getPluginPathParameters(),
            responseFields(NewApiDocs.getPluginResponse(""))))
        .andExpect(status().is2xxSuccessful());
  }

  private Plugin createAndPersistPlugin(final String hash) {
    Plugin plugin = createPlugin(hash);
    persistPlugin(plugin);
    return plugin;
  }

  private static Plugin createPlugin(final String hash) {
    Plugin plugin = new Plugin();
    plugin.setHash(hash);
    plugin.setName(faker.name().name());
    plugin.setVersion(faker.number().digit() + "." + faker.number().digit() + "." + faker.number().digit());
    return plugin;
  }

  @Test
  public void testGetNonExisting() throws Exception {
    final RequestBuilder request = get("/minerva/new_api/plugins/{pluginId}/", "unknown");

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDocsCreatePlugin() throws Exception {
    final NewPluginDTO data = createPluginDTO(PLUGIN_HASH);

    final RequestBuilder request = post("/minerva/new_api/plugins/")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data));

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andDo(document("new_api/plugins/create_plugin",
            pathParameters(),
            NewApiDocs.getAddPluginRequest(),
            responseFields(NewApiDocs.getPluginResponse(""))))
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    assertNotNull(pluginService.getByHash(PLUGIN_HASH));
  }

  @Test
  public void testCreatePluginWithHashMismatch() throws Exception {
    final NewPluginDTO data = createPluginDTO(TEST_PROJECT);

    final RequestBuilder request = post("/minerva/new_api/plugins/")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data));

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testCreatePluginExisting() throws Exception {
    createAndPersistPlugin(PLUGIN_HASH);

    final NewPluginDTO data = createPluginDTO(PLUGIN_HASH);

    final RequestBuilder request = post("/minerva/new_api/plugins/")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data));

    mockMvc.perform(request)
        .andExpect(status().isConflict());
  }

  @Test
  public void testUpdatePlugin() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);


    createAndPersistPlugin(PLUGIN_HASH);

    final NewPluginDTO data = createPluginDTO(PLUGIN_HASH);

    final RequestBuilder request = put("/minerva/new_api/plugins/{pluginId}/", PLUGIN_HASH)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    Plugin plugin = pluginService.getByHash(PLUGIN_HASH);
    assertEquals(data.getVersion(), plugin.getVersion());
  }


  @Test
  public void testUpdatePluginWithMissingData() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createAndPersistPlugin(PLUGIN_HASH);

    final NewPluginDTO data = createPluginDTO(PLUGIN_HASH);
    data.setName(null);

    final RequestBuilder request = put("/minerva/new_api/plugins/{pluginId}/", PLUGIN_HASH)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testUpdatePluginWithOldVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createAndPersistPlugin(PLUGIN_HASH);

    final NewPluginDTO data = createPluginDTO(PLUGIN_HASH);

    final RequestBuilder request = put("/minerva/new_api/plugins/{pluginId}/", PLUGIN_HASH)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdatePluginWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Plugin plugin = createAndPersistPlugin(PLUGIN_HASH);

    String originalVersion = plugin.getEntityVersion() + "";

    final NewPluginDTO data = createPluginDTO(PLUGIN_HASH);

    final RequestBuilder request = put("/minerva/new_api/plugins/{pluginId}/", PLUGIN_HASH)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    plugin = pluginService.getByHash(PLUGIN_HASH);
    assertEquals(data.getVersion(), plugin.getVersion());

    assertNotEquals(originalVersion, plugin.getEntityVersion() + "");
  }

  private NewPluginDTO createPluginDTO(final String hash) {
    final NewPluginDTO data = new NewPluginDTO();
    data.setName(faker.name().name());
    data.setHash(hash);
    data.setVersion(faker.number().digit() + "." + faker.number().digit() + "." + faker.number().digit());
    data.setUrl(PLUGIN_URL);
    return data;
  }


  @Test
  public void testDeletePlugin() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createAndPersistPlugin(PLUGIN_HASH);

    final RequestBuilder request = delete("/minerva/new_api/plugins/{pluginId}/", PLUGIN_HASH)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    assertNull(pluginService.getByHash(PLUGIN_HASH));
  }


  @Test
  public void testDeleteNotExistingPlugin() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/plugins/{pluginId}/", PLUGIN_HASH)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessPlugin() throws Exception {
    createAndPersistPlugin(PLUGIN_HASH);

    final RequestBuilder request = delete("/minerva/new_api/plugins/{pluginId}/", PLUGIN_HASH);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeletePluginWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Plugin plugin = createAndPersistPlugin(PLUGIN_HASH);

    final String originalVersion = plugin.getEntityVersion() + "";

    final RequestBuilder request = delete("/minerva/new_api/plugins/{pluginId}/", PLUGIN_HASH)
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testDeletePluginWithWrongVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createAndPersistPlugin(PLUGIN_HASH);

    final RequestBuilder request = delete("/minerva/new_api/plugins/{pluginId}/", PLUGIN_HASH)
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testDocsListPlugins() throws Exception {
    Plugin plugin = createPlugin(PLUGIN_HASH);
    plugin.setPublic(true);
    persistPlugin(plugin);


    plugin = createPlugin(faker.hashing().md5());
    plugin.addUrl(PLUGIN_URL);
    persistPlugin(plugin);

    plugin = createPlugin(faker.hashing().md5());
    plugin.addUrl(PLUGIN_URL);
    persistPlugin(plugin);

    final RequestBuilder request = get("/minerva/new_api/plugins/");

    String content = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/plugins/list_plugins",
            pathParameters(),
            NewApiDocs.getPluginsSearchResult()))
        .andReturn().getResponse().getContentAsString();

    final Page<Object> page = objectMapper.readValue(content, new TypeReference<Page<Object>>() {
    });
    assertEquals(1, page.getTotalElements());
    assertEquals(0, page.getNumber());
    assertEquals(1, page.getNumberOfElements());

  }

  private void persistPlugin(final Plugin plugin) {
    pluginService.add(plugin);
    pluginHashes.add(plugin.getHash());
  }
}
