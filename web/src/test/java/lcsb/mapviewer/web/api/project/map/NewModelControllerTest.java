package lcsb.mapviewer.web.api.project.map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.persist.dao.map.ModelProperty;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewModelControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IProjectService projectService;

  @Autowired
  private IModelService modelService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testGetMap() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}", BUILT_IN_PROJECT, BUILT_IN_MAP_ID)
        .session(session);

    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    assertTrue(result.containsKey("minZoom"));

  }

  @Test
  public void testGetNonExisting() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}", BUILT_IN_PROJECT, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetMapWithoutPermission() throws Exception {
    Project project = createAndPersistProject(TEST_PROJECT);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}", TEST_PROJECT, project.getTopModel().getId());

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testCreateMap() throws Exception {
    createAndPersistProject(TEST_PROJECT);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewModelDataDTO data = createModelDTO();

    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    assertNotNull(modelService.getModelByMapId(TEST_PROJECT, (int) result.get("id")));
    assertNotNull(modelService.getModelByMapId(TEST_PROJECT, (int) result.get("id")).getCreationDate());
  }

  private NewModelDataDTO createModelDTO() {
    NewModelDataDTO data = new NewModelDataDTO();
    data.setDefaultCenterX(10.0);
    data.setDefaultCenterY(20.0);
    data.setDefaultZoomLevel(3);
    data.setHeight(100.0);
    data.setWidth(200.0);
    data.setNotes("hello kitty");
    data.setName("test map");
    return data;
  }

  private NewModelDataDTO createModelDTO(final ModelData map) {
    NewModelDataDTO data = createModelDTO();
    data.setHeight(map.getHeight());
    data.setWidth(map.getWidth());
    return data;
  }

  @Test
  public void testUpdateMap() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Project project = createAndPersistProject(TEST_PROJECT);
    int mapId = project.getTopModel().getId();

    NewModelDataDTO data = createModelDTO(modelService.getModelByMapId(TEST_PROJECT, mapId));

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}",
        TEST_PROJECT, mapId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    ModelData map = modelService.getModelByMapId(TEST_PROJECT, mapId);
    assertEquals(data.getName(), map.getName());
  }

  @Test
  public void testUpdateMapWithForbiddenLayoutChange() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Project project = createAndPersistProject(TEST_PROJECT);
    int mapId = project.getTopModel().getId();

    NewModelDataDTO data = createModelDTO();

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}",
        TEST_PROJECT, mapId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testUpdateProjectWithOldVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Project project = createAndPersistProject(TEST_PROJECT);

    NewModelDataDTO data = createModelDTO();

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}", TEST_PROJECT, project.getTopModel().getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdateMapWithGoodVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Project project = createAndPersistProject(TEST_PROJECT);

    int mapId = project.getTopModel().getId();

    String originalVersion = project.getTopModelData().getEntityVersion() + "";

    NewModelDataDTO data = createModelDTO(modelService.getModelByMapId(TEST_PROJECT, mapId));

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}", TEST_PROJECT, mapId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    project = projectService.getProjectByProjectId(TEST_PROJECT);
    assertNotEquals(originalVersion, project.getEntityVersion() + "");
  }

  @Test
  public void testDeleteMap() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    Project project = createAndPersistProject(TEST_PROJECT);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}", TEST_PROJECT, project.getTopModel().getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    Map<ModelProperty, Object> filter = new HashMap<>();
    filter.put(ModelProperty.PROJECT_ID, TEST_PROJECT);

    assertEquals(project.getModels().size() - 1, modelService.getCount(filter));
    assertNotNull(projectService.getProjectByProjectId(TEST_PROJECT));
  }

  @Test
  public void testDeleteNotExistingMap() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}", TEST_PROJECT, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessMap() throws Exception {
    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/", BUILT_IN_PROJECT, BUILT_IN_MAP_ID);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeleteMapWithGoodVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    Project project = createAndPersistProject(TEST_PROJECT);

    String originalVersion = project.getTopModelData().getEntityVersion() + "";

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}", TEST_PROJECT, project.getTopModel().getId())
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testDeleteMapWithWrongVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Project project = createAndPersistProject(TEST_PROJECT);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}", TEST_PROJECT, project.getTopModel().getId())
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testListMaps() throws Exception {
    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/", BUILT_IN_PROJECT);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testListMapWithoutAccess() throws Exception {

    createAndPersistProject(TEST_PROJECT);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/", TEST_PROJECT);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

}
