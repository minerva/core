package lcsb.mapviewer.web.api.project.map.bioentity.element;

import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Species;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.NewIdDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewElementGlyphControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IElementService elementService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private int elementId;
  private int glyphId;
  private int mapId;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    final Project project = createProjectWithGlyph(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    for (final Element element : project.getTopModelData().getElements()) {
      if (element instanceof Protein) {
        elementId = element.getId();
      }
    }
    glyphId = project.getGlyphs().get(0).getId();
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testSetGlyph() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewIdDTO data = createReferenceDTO(glyphId);

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/glyph/",
        TEST_PROJECT,
        mapId,
        elementId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    final Species element = (Species) elementService.getById(elementId);

    assertNotNull(element.getGlyph());
  }

  @Test
  public void testSetEmptyGlyph() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewIdDTO data = createReferenceDTO(null);

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/glyph/",
        TEST_PROJECT,
        mapId,
        elementId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    final Species element = (Species) elementService.getById(elementId);

    assertNull(element.getGlyph());
  }

  private NewIdDTO createReferenceDTO(final Integer id) {
    final NewIdDTO result = new NewIdDTO();
    result.setId(id);
    return result;
  }

  @Test
  public void testGetGlyphWithoutAccess() throws Exception {

    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/glyph/",
        TEST_PROJECT,
        mapId,
        elementId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testGetGlyph() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/glyph/",
        TEST_PROJECT,
        mapId,
        elementId).session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
  }
}
