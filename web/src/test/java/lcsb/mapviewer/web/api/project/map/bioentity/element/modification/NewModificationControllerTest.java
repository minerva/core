package lcsb.mapviewer.web.api.project.map.bioentity.element.modification;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.sbo.SBOTermModificationResidueType;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.ModificationState;
import lcsb.mapviewer.model.map.species.field.SpeciesWithModificationResidue;
import lcsb.mapviewer.persist.dao.map.species.ModificationResidueProperty;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IModificationResidueService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewModificationControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IModificationResidueService modificationResidueService;

  @Autowired
  private IMinervaJobService minervaJobService;
  @Autowired
  private IProjectService projectService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private int elementId;
  private int modificationId;
  private ModificationResidue mr;
  private int compartmentId;
  private int mapId;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    Project project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    for (final Element element : project.getTopModelData().getElements()) {
      if (element instanceof Compartment) {
        compartmentId = element.getId();
      } else if (element instanceof SpeciesWithModificationResidue) {
        for (final ModificationResidue mr : ((SpeciesWithModificationResidue) element).getModificationResidues()) {
          modificationId = mr.getId();
          this.mr = mr;
          elementId = mr.getSpecies().getId();
        }
      }
    }
    projectService.removeBackground(project.getProjectBackgrounds().get(0));
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testGetModification() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/modifications/{modificationId}",
        TEST_PROJECT, mapId, elementId, modificationId)
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });
    assertNotNull(result.get("sboTerm"));
  }

  @Test
  public void testGetNonExisting() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/modifications/{modificationId}",
        TEST_PROJECT, mapId, elementId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetModificationWithoutPermission() throws Exception {
    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/modifications/{modificationId}",
        TEST_PROJECT, mapId, elementId, modificationId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testCreateModification() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final List<NewModificationResidueDTO> testResidues = new ArrayList<>();

    testResidues.add(createResidueDTO());
    testResidues.add(createModificationSiteDTO());
    testResidues.add(createRegulatoryRegionDTO());
    testResidues.add(createProteinBindingDomainDTO());
    testResidues.add(createBindingRegionDTO());
    testResidues.add(createStructuralStateDTO());
    testResidues.add(createCodingRegionDTO());
    testResidues.add(createTranscriptionSiteDTO());

    for (final NewModificationResidueDTO dto : testResidues) {
      final RequestBuilder request = post(
          "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/modifications/",
          TEST_PROJECT,
          mapId,
          elementId)
          .contentType(MediaType.APPLICATION_JSON)
          .content(objectMapper.writeValueAsString(dto))
          .session(session);

      final MockHttpServletResponse response = mockMvc.perform(request)
          .andExpect(status().isCreated())
          .andReturn().getResponse();
      assertNotNull(response.getHeader("ETag"));
      final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
      });

      final Map<ModificationResidueProperty, Object> filter = new HashMap<>();
      filter.put(ModificationResidueProperty.PROJECT_ID, Collections.singletonList(TEST_PROJECT));
      filter.put(ModificationResidueProperty.MAP_ID, Collections.singletonList(mapId));
      filter.put(ModificationResidueProperty.ELEMENT_ID, Collections.singletonList(elementId));
      filter.put(ModificationResidueProperty.ID, Collections.singletonList((int) result.get("id")));
      final Page<ModificationResidue> elements = modificationResidueService.getAll(filter, Pageable.unpaged());

      assertEquals(1, elements.getNumberOfElements());
    }
  }

  @Test
  public void testCreateModificationForCompartment() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/modifications/",
        TEST_PROJECT,
        mapId,
        compartmentId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(createResidueDTO()))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testUpdateModification() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewModificationResidueDTO data = createModificationResidueDTO(mr);
    data.setBorderColor(Color.DARK_GRAY);

    final RequestBuilder request = put(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/modifications/{modificationId}",
        TEST_PROJECT, mapId, elementId, modificationId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    final ModificationResidue mr = modificationResidueService.getById(modificationId);
    assertEquals(data.getModificationResidueId(), mr.getElementId());
    assertEquals(Color.DARK_GRAY, mr.getBorderColor());
  }

  private NewModificationResidueDTO createResidueDTO() {
    final NewModificationResidueDTO result = createModificationResidueDTO();
    result.setSboTerm(SBOTermModificationResidueType.RESIDUE.getSBO());
    result.setState(ModificationState.PHOSPHORYLATED.getFullName());
    return result;
  }

  private NewModificationResidueDTO createProteinBindingDomainDTO() {
    final NewModificationResidueDTO result = createModificationResidueDTO();
    result.setSboTerm(SBOTermModificationResidueType.PROTEIN_BINDING_DOMAIN.getSBO());
    return result;
  }

  private NewModificationResidueDTO createRegulatoryRegionDTO() {
    final NewModificationResidueDTO result = createModificationResidueDTO();
    result.setSboTerm(SBOTermModificationResidueType.REGULATORY_REGION.getSBO());
    return result;
  }

  private NewModificationResidueDTO createStructuralStateDTO() {
    final NewModificationResidueDTO result = createModificationResidueDTO();
    result.setSboTerm(SBOTermModificationResidueType.STRUCTURAL_STATE.getSBO());
    return result;
  }

  private NewModificationResidueDTO createTranscriptionSiteDTO() {
    final NewModificationResidueDTO result = createModificationResidueDTO();
    result.setSboTerm(SBOTermModificationResidueType.TRANSCRIPTION_SITE.getSBO());
    result.setDirection("LEfT");
    result.setActive(true);
    return result;
  }

  private NewModificationResidueDTO createModificationSiteDTO() {
    final NewModificationResidueDTO result = createModificationResidueDTO();
    result.setSboTerm(SBOTermModificationResidueType.MODIFICATION_SITE.getSBO());
    return result;
  }

  private NewModificationResidueDTO createCodingRegionDTO() {
    final NewModificationResidueDTO result = createModificationResidueDTO();
    result.setSboTerm(SBOTermModificationResidueType.CODING_REGION.getSBO());
    return result;
  }

  private NewModificationResidueDTO createBindingRegionDTO() {
    final NewModificationResidueDTO result = createModificationResidueDTO();
    result.setSboTerm(SBOTermModificationResidueType.BINDING_REGION.getSBO());
    return result;
  }


  private NewModificationResidueDTO createModificationResidueDTO(final ModificationResidue mr) {
    final NewModificationResidueDTO result = new NewModificationResidueDTO();
    result.setSboTerm(mr.getSboTerm());
    result.setHeight(mr.getHeight());
    result.setWidth(mr.getWidth());
    result.setName(mr.getName());
    result.setBorderColor(mr.getBorderColor());
    result.setFillColor(mr.getFillColor());
    result.setModificationResidueId(mr.getIdModificationResidue());
    result.setFontSize(mr.getFontSize());
    result.setX(mr.getX());
    result.setY(mr.getY());
    result.setZ(mr.getZ());
    result.setNameVerticalAlign(mr.getNameVerticalAlign());
    result.setNameHorizontalAlign(mr.getNameHorizontalAlign());
    result.setNameX(mr.getNameX());
    result.setNameY(mr.getNameY());
    result.setNameWidth(mr.getNameWidth());
    result.setNameHeight(mr.getNameHeight());
    return result;
  }

  private NewModificationResidueDTO createModificationResidueDTO() {
    final NewModificationResidueDTO result = new NewModificationResidueDTO();
    result.setHeight(1.0);
    result.setWidth(1.0);
    result.setName("xy");
    result.setBorderColor(Color.CYAN);
    result.setFillColor(Color.MAGENTA);
    result.setModificationResidueId("zz");
    result.setFontSize(10.0);
    result.setX(12.0);
    result.setY(13.0);
    result.setZ(2);
    result.setNameX(result.getX());
    result.setNameY(result.getY());
    result.setNameHeight(result.getHeight());
    result.setNameWidth(result.getWidth());
    result.setNameHorizontalAlign(HorizontalAlign.CENTER);
    result.setNameVerticalAlign(VerticalAlign.MIDDLE);
    return result;
  }

  @Test
  public void testDeleteModification() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/modifications/{modificationId}",
        TEST_PROJECT, mapId, elementId, modificationId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    final ModificationResidue mr = modificationResidueService.getById(modificationId);
    assertNull(mr);
  }

  @Test
  public void testDeleteNoAccessModification() throws Exception {
    final RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/modifications/{modificationId}",
        TEST_PROJECT, mapId, elementId, modificationId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());

    final ModificationResidue mr = modificationResidueService.getById(modificationId);
    assertNotNull(mr);
  }

  @Test
  public void testDeleteNonExistingModification() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/modifications/{modificationId}",
        TEST_PROJECT, mapId, elementId, 1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());

  }

  @Test
  public void testGetModifications() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/modifications/",
        TEST_PROJECT, mapId, elementId)
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();

    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });
    assertTrue((int) result.get("size") > 0);
  }

}
