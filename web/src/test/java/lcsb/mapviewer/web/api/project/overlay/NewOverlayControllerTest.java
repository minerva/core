package lcsb.mapviewer.web.api.project.overlay;

import com.fasterxml.jackson.core.type.TypeReference;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayGroup;
import lcsb.mapviewer.model.overlay.DataOverlayType;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IDataOverlayService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiDocs;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewOverlayControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IUserService userService;

  @Autowired
  private IDataOverlayService dataOverlayService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private User anonymous;

  private Project project;

  private ModelData map;

  private DataOverlayGroup group;
  private User admin;

  @Before
  public void setup() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    map = project.getTopModel().getModelData();
    anonymous = userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN);
    admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());

    group = super.createAndPersistDataOverlayGroup(project, admin);
  }

  @After
  public void tearDown() throws Exception {
    removeProject(project);
  }

  @Test
  public void testDocsGetBioEntitiesForOverlay() throws Exception {
    final String content = createOverlayContentForAllEntities(map);
    final DataOverlay overlay = createOverlay(project, userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN), content);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/",
        TEST_PROJECT, overlay.getId(), "*")
        .session(session);

    final String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("new_api/projects/overlays/get_bioEntities",
            getOverlayMapPathParameters(),
            responseFields(NewApiDocs.getOverlayBioEntityList(""))))
        .andReturn().getResponse().getContentAsString();

    int elements = 0;
    for (final ModelData model : project.getModels()) {
      elements += model.getElements().size() + model.getReactions().size();
    }

    final List<?> result = objectMapper.readValue(response, new TypeReference<List<?>>() {
    });

    assertEquals(elements, result.size());
  }

  private String createOverlayContentForAllEntities(final ModelData map) {
    StringBuilder content = new StringBuilder("element_identifier\tvalue\tcolor\n");
    for (ModelSubmodelConnection submapConnection : map.getSubmodels()) {
      ModelData submap = submapConnection.getSubmodel();
      for (Reaction entity : submap.getReactions()) {
        content.append(entity.getElementId()).append("\t").append("-1\t\n");
      }
      for (Element entity : submap.getElements()) {
        content.append(entity.getElementId()).append("\t").append("\t").append("#FF0000").append("\n");
      }
    }
    for (Reaction entity : map.getReactions()) {
      content.append(entity.getElementId()).append("\t").append("-1\t\n");
    }
    for (Element entity : map.getElements()) {
      content.append(entity.getElementId()).append("\t").append("1\t\n");
    }
    return content.toString();
  }

  @Test
  public void testGetBioEntitiesAndIncludeIndirect() throws Exception {
    final String content = createOverlayContentForAllEntities(map);
    final DataOverlay overlay = createOverlay(project, userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN), content);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/",
        TEST_PROJECT, overlay.getId(), map.getId())
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<?> result = objectMapper.readValue(response, new TypeReference<List<?>>() {
    });

    final int countWithoutSubmaps = result.size();

    request = get("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/models/{mapId}/bioEntities/?includeIndirect=true",
        TEST_PROJECT, overlay.getId(), map.getId())
        .session(session);

    response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    result = objectMapper.readValue(response, new TypeReference<List<?>>() {
    });

    final int countWithSubmaps = result.size();
    assertTrue(countWithoutSubmaps < countWithSubmaps);
  }


  @Test
  public void testDocsGetOverlay() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final DataOverlay overlay = super.createOverlay(TEST_PROJECT, admin);
    overlay.setGroup(super.createAndPersistDataOverlayGroup(project, admin));
    dataOverlayService.update(overlay);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/", TEST_PROJECT, overlay.getId())
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/projects/overlays/get_overlay",
            getOverlayPathParameters(),
            responseFields(NewApiDocs.getOverlayResponse(""))))
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
  }

  @Test
  public void testGetNonExisting() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/", BUILT_IN_PROJECT, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetOverlayWithoutPermission() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    final DataOverlay overlay = super.createOverlay(TEST_PROJECT, admin);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlays/{overlayId}/", TEST_PROJECT, overlay.getId());

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testCreateOverlay() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewDataOverlayDTO data = createDataOverlayDTO();

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/overlays/", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    assertNotNull(dataOverlayService.getDataOverlayById((int) result.get("id")));
  }

  @Test
  public void testCreateOverlayWithoutGroup() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewDataOverlayDTO data = createDataOverlayDTO();
    data.setGroup(null);

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/overlays/", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    DataOverlay overlay = dataOverlayService.getDataOverlayById((int) result.get("id"));
    assertNotNull(overlay);
    assertNull(overlay.getGroup());
  }

  private NewDataOverlayDTO createDataOverlayDTO() {
    final NewDataOverlayDTO data = new NewDataOverlayDTO();
    data.setName(faker.name().name());
    data.setDescription(faker.text().text());
    data.setColorSchemaType(DataOverlayType.GENERIC);
    data.setPublic(true);
    data.setGenomeType(ReferenceGenomeType.UCSC);
    data.setGenomeVersion(faker.name().prefix());
    data.setOrderIndex(faker.number().positive());
    data.setGroup(group.getId());
    return data;
  }

  private NewDataOverlayDTO createDataOverlayDTO(final DataOverlay overlay) {
    final NewDataOverlayDTO data = createDataOverlayDTO();
    return data;
  }

  @Test
  public void testUpdateOverlay() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    DataOverlay overlay = super.createOverlay(TEST_PROJECT, admin);
    final int overlayId = overlay.getId();

    final NewDataOverlayDTO data = createDataOverlayDTO(dataOverlayService.getDataOverlayById(overlayId));
    data.setName("xx");

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/overlays/{overlayId}",
        TEST_PROJECT, overlayId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    overlay = dataOverlayService.getDataOverlayById(overlayId);
    assertEquals(data.getName(), overlay.getName());
    assertEquals(data.getGroup(), (Integer) overlay.getGroup().getId());

    data.setGroup(null);

    request = put("/minerva/new_api/projects/{projectId}/overlays/{overlayId}",
        TEST_PROJECT, overlayId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    overlay = dataOverlayService.getDataOverlayById(overlayId);
    assertNull(overlay.getGroup());

  }

  @Test
  public void testUpdateDataOverlayWithOldVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewDataOverlayDTO data = createDataOverlayDTO();

    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    final DataOverlay overlay = super.createOverlay(TEST_PROJECT, admin);
    final int overlayId = overlay.getId();

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/overlays/{overlayId}", TEST_PROJECT, overlayId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdateOverlayWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    DataOverlay overlay = super.createOverlay(TEST_PROJECT, admin);
    final int overlayId = overlay.getId();

    final String originalVersion = overlay.getEntityVersion() + "";

    final NewDataOverlayDTO data = createDataOverlayDTO(overlay);

    final RequestBuilder request = put("/minerva/new_api/projects/{projectId}/overlays/{overlayId}", TEST_PROJECT, overlayId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    overlay = dataOverlayService.getDataOverlayById(overlayId);
    assertNotEquals(originalVersion, overlay.getEntityVersion() + "");
  }

  @Test
  public void testDeleteOverlay() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    final DataOverlay overlay = super.createOverlay(TEST_PROJECT, admin);
    final int overlayId = overlay.getId();

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/overlays/{overlayId}", TEST_PROJECT, overlayId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    assertNull(dataOverlayService.getDataOverlayById(overlayId));
  }

  @Test
  public void testDeleteNotExistingOverlay() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/overlays/{overlayId}", TEST_PROJECT, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessOverlay() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    final DataOverlay overlay = super.createOverlay(TEST_PROJECT, admin);
    final int overlayId = overlay.getId();

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/overlays/{overlayId}", BUILT_IN_PROJECT, overlayId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeleteOverlayWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    final DataOverlay overlay = super.createOverlay(TEST_PROJECT, admin);
    final int overlayId = overlay.getId();

    final String originalVersion = overlay.getEntityVersion() + "";

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/overlays/{overlayId}", TEST_PROJECT, overlayId)
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testDeleteOverlayWithWrongVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    final DataOverlay overlay = super.createOverlay(TEST_PROJECT, admin);
    final int overlayId = overlay.getId();

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/overlays/{overlayId}", TEST_PROJECT, overlayId)
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testListOverlays() throws Exception {

    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    super.createOverlay(TEST_PROJECT, admin);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlays/", TEST_PROJECT);

    final String response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse().getContentAsString();

    final Map<String, Object> result = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });

    assertEquals(0, result.get("numberOfElements"));
  }

  @Test
  public void testListOwnOverlays() throws Exception {
    final User guest = userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN);
    super.createOverlay(TEST_PROJECT, guest);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlays/", TEST_PROJECT);

    final String response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse().getContentAsString();

    final Map<String, Object> result = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });

    assertEquals(1, result.get("numberOfElements"));
  }

  @Test
  public void testListOverlaysFilterByCreator() throws Exception {
    final User guest = userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN);
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    super.createOverlay(TEST_PROJECT, guest);
    super.createOverlay(TEST_PROJECT, admin);
    super.createOverlay(TEST_PROJECT, admin);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlays/?creator={creator}",
        TEST_PROJECT,
        guest.getLogin())
        .session(session);

    final String response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse().getContentAsString();

    final Map<String, Object> result = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });

    assertEquals(1, result.get("numberOfElements"));
  }

  @Test
  public void testListPublicOverlays() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    final DataOverlay overlay = super.createOverlay(TEST_PROJECT, admin);
    overlay.setPublic(true);
    dataOverlayService.update(overlay);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlays/?isPublic={isPublic}",
        TEST_PROJECT,
        true);

    final String response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse().getContentAsString();

    final Map<String, Object> result = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });

    assertEquals(1, result.get("numberOfElements"));
  }

  @Test
  public void testListOverlayWithoutAccess() throws Exception {
    userService.revokeUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, TEST_PROJECT);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/overlays/", TEST_PROJECT);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }
}
