package lcsb.mapviewer.web.api.project.glyph;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiDocs;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.Assert.assertEquals;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.multipart;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewProjectGlyphControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testDocsGetGlyphsForProject() throws Exception {
    createProjectWithGlyph(TEST_PROJECT);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/glyphs/?page=0&size=5",
        TEST_PROJECT).session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("new_api/projects/glyphs/list_glyphs",
            getProjectPathParameters(),
            requestParameters(getPageableFilter()),
            NewApiDocs.getGlyphsSearchResult()))
        .andReturn().getResponse().getContentAsString();
    Page<Object> result = objectMapper.readValue(response, new TypeReference<Page<Object>>() {
    });
    assertEquals(1, result.getTotalElements());
  }

  @Test
  public void testDocsAddGlyph() throws Exception {
    createProjectWithGlyph(TEST_PROJECT);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    MockMultipartFile file =
        new MockMultipartFile("file", "filename.png", null, new byte[0]);

    RequestBuilder request = multipart("/minerva/new_api/projects/{projectId}/glyphs/",
        TEST_PROJECT)
        .file(file)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("new_api/projects/glyphs/add_glyph",
            getProjectPathParameters(),
            responseFields(NewApiDocs.getGlyphResponse(""))));
  }

  @Test
  public void testGetGlyphFileContentForProject() throws Exception {
    Project project = createProjectWithGlyph(TEST_PROJECT);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get(
        "/minerva/new_api/projects/{projectId}/glyphs/{glyphId}/fileContent",
        TEST_PROJECT,
        project.getGlyphs().get(0).getId()
    ).session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andExpect(MockMvcResultMatchers.header().string("Cache-Control", "max-age=86400"));
  }

}
