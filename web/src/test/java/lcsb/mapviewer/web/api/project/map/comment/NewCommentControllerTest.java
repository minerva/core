package lcsb.mapviewer.web.api.project.map.comment;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.persist.dao.map.CommentProperty;
import lcsb.mapviewer.services.interfaces.ICommentService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IModelService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiDocs;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewCommentControllerTest extends ControllerIntegrationTest {

  @Autowired
  private ICommentService commentService;

  @Autowired
  private IModelService modelService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private ModelData modelData;
  private User user;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();

    modelData = modelService.getById(BUILT_IN_MAP_ID);

    user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
  }

  @After
  public void tearDown() throws Exception {
    removeUser(user);
    minervaJobService.waitForTasksToFinish();
  }

  @Test
  public void testGetComment() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Comment comment = createComment(modelData);
    commentService.add(comment);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/comments/{commentId}",
        BUILT_IN_PROJECT,
        BUILT_IN_MAP_ID,
        comment.getId())
        .session(session);

    final HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/projects/maps/comments/get_comment",
            getCommentPathParameters(),
            responseFields(NewApiDocs.getCommentResponse(""))))
        .andReturn().getResponse();

    assertNotNull(response.getHeader("ETag"));
  }

  @Test
  public void testGetNonExisting() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/comments/{commentId}",
        BUILT_IN_PROJECT,
        BUILT_IN_MAP_ID,
        -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetCommentWithoutPermission() throws Exception {
    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    Comment comment = createComment(modelData);
    commentService.add(comment);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/comments/{commentId}",
        BUILT_IN_PROJECT,
        BUILT_IN_MAP_ID,
        comment.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testCreateComment() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewCommentDTO data = createCommentDTO();

    final RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/comments/",
        BUILT_IN_PROJECT,
        BUILT_IN_MAP_ID)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    final MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/projects/maps/comments/add_comment",
            getMapPathParameters(),
            NewApiDocs.getAddCommentRequest(),
            responseFields(NewApiDocs.getCommentResponse(""))))
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    final Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    final Map<CommentProperty, Object> filter = new HashMap<>();
    filter.put(CommentProperty.PROJECT_ID, BUILT_IN_PROJECT);
    filter.put(CommentProperty.MAP_ID, BUILT_IN_MAP_ID);
    filter.put(CommentProperty.ID, result.get("id"));
    final Page<Comment> comments = commentService.getAll(filter, Pageable.unpaged());

    assertEquals(1, comments.getNumberOfElements());
  }

  private NewCommentDTO createCommentDTO() {
    final NewCommentDTO data = new NewCommentDTO();
    data.setContent("blah blah blah");
    data.setCoordinates(new Point2D.Double(10, 12));
    data.setEmail("test@test.com");
    data.setVisible(true);
    return data;
  }

  private NewRemoveCommentDTO createRemoveCommentDTO() {
    final NewRemoveCommentDTO data = new NewRemoveCommentDTO();
    data.setReason("xyz");
    return data;
  }


  @Test
  public void testDeleteComment() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Comment comment = createComment(modelData);
    commentService.add(comment);

    NewRemoveCommentDTO removeCommentDTO = createRemoveCommentDTO();

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/comments/{commentId}",
        BUILT_IN_PROJECT,
        BUILT_IN_MAP_ID,
        comment.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(removeCommentDTO))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk())
        .andDo(document("new_api/projects/maps/comments/delete_comment",
            getCommentPathParameters()));

    comment = commentService.getById(comment.getId());
    assertTrue(comment.isDeleted());
  }

  @Test
  public void testDeleteNotExistingComment() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/comments/{commentId}",
        BUILT_IN_PROJECT,
        BUILT_IN_MAP_ID,
        -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessComment() throws Exception {
    Comment comment = createComment(modelData);
    commentService.add(comment);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/comments/{commentId}",
        BUILT_IN_PROJECT,
        BUILT_IN_MAP_ID,
        comment.getId());

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeleteCommentWithGoodVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Comment comment = createComment(modelData);
    commentService.add(comment);

    final String originalVersion = comment.getEntityVersion() + "";

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/comments/{commentId}",
        BUILT_IN_PROJECT,
        BUILT_IN_MAP_ID,
        comment.getId())
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
    comment = commentService.getById(comment.getId());
    assertTrue(comment.isDeleted());
  }

  @Test
  public void testDeleteCommentWithWrongVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Comment comment = createComment(modelData);
    commentService.add(comment);

    final RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/comments/{commentId}",
        BUILT_IN_PROJECT,
        BUILT_IN_MAP_ID,
        comment.getId())
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
    comment = commentService.getById(comment.getId());
    assertFalse(comment.isDeleted());
  }

  @Test
  public void testListComments() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Comment comment = createComment(modelData);
    commentService.add(comment);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/comments/",
        BUILT_IN_PROJECT,
        BUILT_IN_MAP_ID)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("new_api/projects/maps/comments/list_comments",
            getMapPathParameters(),
            NewApiDocs.getCommentsSearchResult()))
        .andExpect(status().isOk());
  }

  @Test
  public void testListCommentsWithoutAccess() throws Exception {
    final MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);
    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/comments/",
        BUILT_IN_PROJECT,
        BUILT_IN_MAP_ID)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

}
