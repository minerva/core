package lcsb.mapviewer.web.api.stacktrace;

import com.fasterxml.jackson.core.type.TypeReference;
import lcsb.mapviewer.model.Stacktrace;
import lcsb.mapviewer.services.interfaces.IStacktraceService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiDocs;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewStacktraceControllerIntegrationTest extends ControllerIntegrationTest {

  @Autowired
  private IStacktraceService stacktraceService;

  @Test
  public void testGetStacktrace() throws Exception {
    Stacktrace st = stacktraceService.createFromStackTrace(new Exception());
    RequestBuilder request = get("/minerva/new_api/stacktraces/{id}", st.getId());

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("new_api/stacktraces/get",
            getStacktracePathParameters(),
            responseFields(NewApiDocs.getStacktraceResponse(""))))
        .andReturn().getResponse().getContentAsString();

    Map<String, Object> result = objectMapper.readValue(response, new TypeReference<HashMap<String, Object>>() {
    });

    assertEquals(st.getId(), result.get("id"));
  }

  @Test
  public void testGetNotExistingStacktrace() throws Exception {
    RequestBuilder request = get("/minerva/new_api/stacktraces/{id}", "blah");

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

}
