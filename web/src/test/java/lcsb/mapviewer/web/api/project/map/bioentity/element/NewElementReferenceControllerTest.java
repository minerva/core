package lcsb.mapviewer.web.api.project.map.bioentity.element;

import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.services.interfaces.IElementService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.NewMiriamDataDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewElementReferenceControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IElementService elementService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private Project project;

  private int elementId;
  private int mapId;
  private MiriamData reference;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    final Set<Integer> ids = new HashSet<>();
    for (final Element element : project.getTopModelData().getElements()) {
      if (element instanceof Compartment) {
        ids.add(element.getId());
      }
    }
    elementId = ids.iterator().next();
    reference = project.getTopModel().getElementByDbId(elementId).getMiriamData().iterator().next();
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testCreateReference() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final NewMiriamDataDTO data = createReferenceDTO();

    final RequestBuilder request = post(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/references/",
        TEST_PROJECT,
        mapId,
        elementId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isCreated());

    final Element element = elementService.getById(elementId);

    assertEquals(project.getTopModel().getElementByDbId(elementId).getMiriamData().size() + 1, element.getMiriamData().size());
  }

  private NewMiriamDataDTO createReferenceDTO() {
    final NewMiriamDataDTO result = new NewMiriamDataDTO();
    result.setType(MiriamType.PUBMED);
    result.setResource("" + counter++);
    return result;
  }

  @Test
  public void testDeleteReference() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/references/{reference}",
        TEST_PROJECT,
        mapId,
        elementId,
        reference.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    final Element element = elementService.getById(elementId);

    assertEquals(project.getTopModel().getElementByDbId(elementId).getMiriamData().size() - 1, element.getMiriamData().size());
  }

  @Test
  public void testDeleteNotExistingReference() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/references/{reference}",
        TEST_PROJECT,
        mapId,
        elementId,
        -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessReference() throws Exception {
    final RequestBuilder request = delete(
        "/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/references/{reference}",
        TEST_PROJECT, mapId, elementId, reference.getId());

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testListReferences() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/references/",
        TEST_PROJECT,
        mapId,
        elementId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testListReferencesWithoutAccess() throws Exception {

    final RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/elements/{elementId}/references/",
        TEST_PROJECT,
        mapId,
        elementId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }
}
