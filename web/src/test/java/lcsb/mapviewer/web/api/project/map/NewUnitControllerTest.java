package lcsb.mapviewer.web.api.project.map;

import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.kinetics.SbmlUnitType;
import lcsb.mapviewer.persist.dao.map.kinetics.SbmlUnitProperty;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.ISbmlUnitService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.map.unit.NewUnitDTO;
import lcsb.mapviewer.web.api.project.map.unit.NewUnitTypeFactorDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewUnitControllerTest extends ControllerIntegrationTest {

  @Autowired
  private ISbmlUnitService unitService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private Project project;

  private int usedUnitId;
  private int unusedUnitId;
  private int mapId;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    for (SbmlUnit unit : project.getTopModel().getUnits()) {
      if (unit.getUnitId().equals("unused")) {
        unusedUnitId = unit.getId();
      } else {
        usedUnitId = unit.getId();
      }
    }
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testGetUnit() throws Exception {

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/units/{unitId}",
        TEST_PROJECT, mapId, usedUnitId)
        .session(session);

    HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
  }

  @Test
  public void testGetNonExisting() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/units/{unitId}",
        TEST_PROJECT, mapId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetUnitWithoutPermission() throws Exception {
    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/units/{unitId}",
        TEST_PROJECT, mapId, usedUnitId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testCreateUnit() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewUnitDTO data = createUnitDTO();

    int count = project.getTopModelData().getUnits().size();

    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/units/",
        TEST_PROJECT,
        mapId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    Map<SbmlUnitProperty, Object> filter = new HashMap<>();
    filter.put(SbmlUnitProperty.PROJECT_ID, TEST_PROJECT);
    filter.put(SbmlUnitProperty.MAP_ID, mapId);
    assertEquals(count + 1, unitService.getCount(filter));
  }

  @Test
  public void testCreateUnitNoAccess() throws Exception {
    NewUnitDTO data = createUnitDTO();

    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/units/",
        TEST_PROJECT,
        mapId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data));

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  private NewUnitDTO createUnitDTO() {
    NewUnitDTO result = new NewUnitDTO();
    result.setName("unit_name_" + (counter++));
    result.setUnitId("unit_id_" + (counter++));
    NewUnitTypeFactorDTO factor = new NewUnitTypeFactorDTO();
    factor.setExponent(counter++);
    factor.setMultiplier(counter++);
    factor.setScale(counter++);
    factor.setUnitType(SbmlUnitType.AMPERE);
    result.addUnitTypeFactor(factor);
    return result;
  }

  @Test
  public void testUpdateUnit() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewUnitDTO data = createUnitDTO();

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/units/{unitId}",
        TEST_PROJECT, mapId, usedUnitId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    SbmlUnit unit = unitService.getById(usedUnitId);
    assertEquals(data.getName(), unit.getName());
  }

  @Test
  public void testUpdateUnitWithOldVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewUnitDTO data = createUnitDTO();

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/units/{unitId}",
        TEST_PROJECT, mapId, usedUnitId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdateUnitWithGoodVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String originalVersion = project.getTopModelData().getUnits().iterator().next().getEntityVersion() + "";

    NewUnitDTO data = createUnitDTO();

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/units/{unitId}",
        TEST_PROJECT, mapId, usedUnitId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    SbmlUnit unit = unitService.getById(usedUnitId);
    assertNotEquals(originalVersion, unit.getEntityVersion() + "");
  }

  @Test
  public void testDeleteUnit() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/units/{unitId}",
        TEST_PROJECT, mapId, unusedUnitId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    assertNull(unitService.getById(unusedUnitId));
  }

  @Test
  public void testDeleteUsedUnit() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/units/{unitId}",
        TEST_PROJECT, mapId, usedUnitId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());

    assertNotNull(unitService.getById(usedUnitId));
  }

  @Test
  public void testDeleteNotExistingUnit() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/units/{unitId}",
        TEST_PROJECT, mapId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessUnit() throws Exception {
    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/units/{unitId}",
        TEST_PROJECT, mapId, unusedUnitId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeleteUnitWithGoodVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String originalVersion = project.getTopModelData().getUnits().iterator().next().getEntityVersion() + "";

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/units/{unitId}",
        TEST_PROJECT, mapId, unusedUnitId)
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
    assertNull(unitService.getById(unusedUnitId));
  }

  @Test
  public void testDeleteUnitWithWrongVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/units/{unitId}",
        TEST_PROJECT, mapId, unusedUnitId)
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testListUnits() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/units/", TEST_PROJECT, mapId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testListUnitsWithoutAccess() throws Exception {

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/units/", TEST_PROJECT, mapId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

}
