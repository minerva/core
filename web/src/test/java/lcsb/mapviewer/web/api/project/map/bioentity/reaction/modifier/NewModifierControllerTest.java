package lcsb.mapviewer.web.api.project.map.bioentity.reaction.modifier;

import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.sbo.SBOTermModifierType;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IReactionService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.NewIdDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewModifierControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IReactionService reactionService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private Project project;

  private int reactionId;
  private int mapId;
  private Modifier modifier;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    Set<Integer> ids = new HashSet<>();
    for (Reaction reaction : project.getTopModelData().getReactions()) {
      ids.add(reaction.getId());
    }
    reactionId = ids.iterator().next();
    modifier = project.getTopModel().getReactionByDbId(reactionId).getModifiers().get(0);
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testCreateModifier() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewModifierNodeDTO data = createModifierDTO();

    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/modifiers/",
        TEST_PROJECT,
        mapId,
        reactionId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isCreated());

    Reaction reaction = reactionService.getReactionById(TEST_PROJECT, mapId, reactionId);

    assertEquals(project.getTopModel().getReactionByDbId(reactionId).getModifiers().size() + 1, reaction.getModifiers().size());
  }

  private NewModifierNodeDTO createModifierDTO() {
    NewModifierNodeDTO result = new NewModifierNodeDTO();
    result.setStoichiometry(3.0);
    result.setSboTerm(SBOTermModifierType.CATALYSIS.getSBO());
    NewIdDTO id = new NewIdDTO();
    id.setId(modifier.getElement().getId());
    result.setElement(id);
    return result;
  }

  @Test
  public void testDeleteModifier() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/modifiers/{modifierId}",
        TEST_PROJECT, mapId, reactionId, modifier.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    Reaction reaction = reactionService.getReactionById(TEST_PROJECT, mapId, reactionId);

    assertEquals(project.getTopModel().getReactionByDbId(reactionId).getModifiers().size() - 1, reaction.getModifiers().size());
  }

  @Test
  public void testDeleteNotExistingModifier() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/modifiers/{modifierId}",
        TEST_PROJECT, mapId, reactionId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessModifier() throws Exception {
    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/modifiers/{modifierId}",
        TEST_PROJECT, mapId, reactionId, modifier.getId());

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testListModifiers() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/modifiers/", TEST_PROJECT,
        mapId,
        reactionId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testListModifiersWithoutAccess() throws Exception {

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/bioEntities/reactions/{reactionId}/modifiers/", TEST_PROJECT,
        mapId,
        reactionId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }
}
