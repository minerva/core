package lcsb.mapviewer.web.api.project.map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.persist.dao.map.AuthorProperty;
import lcsb.mapviewer.services.interfaces.IAuthorService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.web.ControllerIntegrationTest;
import lcsb.mapviewer.web.api.NewApiResponseSerializer;
import lcsb.mapviewer.web.api.project.map.author.NewAuthorDTO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class NewAuthorControllerTest extends ControllerIntegrationTest {

  @Autowired
  private IAuthorService authorService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private NewApiResponseSerializer newApiResponseSerializer;

  private ObjectMapper objectMapper;

  private Project project;

  private int authorId;
  private int mapId;

  @Before
  public void setUp() throws Exception {
    objectMapper = newApiResponseSerializer.getObjectMapper();
    project = createAndPersistProject(TEST_PROJECT);
    mapId = project.getTopModel().getId();
    authorId = project.getTopModel().getAuthors().get(0).getId();
  }

  @After
  public void tearDown() throws Exception {
    minervaJobService.waitForTasksToFinish();
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testGetAuthor() throws Exception {

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/authors/{authorId}",
        TEST_PROJECT, mapId, authorId)
        .session(session);

    HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
  }

  @Test
  public void testGetNonExisting() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/authors/{authorId}",
        TEST_PROJECT, mapId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetAuthorWithoutPermission() throws Exception {
    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/authors/{authorId}",
        TEST_PROJECT, mapId, authorId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testCreateAuthor() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewAuthorDTO data = createAuthorDTO();

    RequestBuilder request = post("/minerva/new_api/projects/{projectId}/maps/{mapId}/authors/",
        TEST_PROJECT,
        mapId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    MockHttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isCreated())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));
    Map<String, Object> result = objectMapper.readValue(response.getContentAsString(), new TypeReference<Map<String, Object>>() {
    });

    Map<AuthorProperty, Object> filter = new HashMap<>();
    filter.put(AuthorProperty.PROJECT_ID, TEST_PROJECT);
    filter.put(AuthorProperty.MAP_ID, mapId);
    filter.put(AuthorProperty.ID, result.get("id"));
    Page<Author> authors = authorService.getAll(filter, Pageable.unpaged());

    assertEquals(1, authors.getNumberOfElements());
  }

  private NewAuthorDTO createAuthorDTO() {
    NewAuthorDTO result = new NewAuthorDTO();
    result.setEmail("john.doe@uni.lu");
    result.setFirstName("John");
    result.setLastName("Doe");
    result.setOrganisation("University of Luxembourg");
    return result;
  }

  @Test
  public void testUpdateAuthor() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewAuthorDTO data = createAuthorDTO();

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/authors/{authorId}",
        TEST_PROJECT, mapId, authorId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    Author author = authorService.getById(authorId);
    assertEquals(data.getFirstName(), author.getFirstName());
  }

  @Test
  public void testUpdateAuthorWithOldVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    NewAuthorDTO data = createAuthorDTO();

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/authors/{authorId}",
        TEST_PROJECT, mapId, authorId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testUpdateAuthorWithGoodVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String originalVersion = project.getTopModelData().getAuthors().get(0).getEntityVersion() + "";

    NewAuthorDTO data = createAuthorDTO();

    RequestBuilder request = put("/minerva/new_api/projects/{projectId}/maps/{mapId}/authors/{authorId}",
        TEST_PROJECT, mapId, authorId)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .header("If-Match", originalVersion)
        .session(session);

    HttpServletResponse response = mockMvc.perform(request)
        .andExpect(status().isOk())
        .andReturn().getResponse();
    assertNotNull(response.getHeader("ETag"));

    Author author = authorService.getById(authorId);
    assertNotEquals(originalVersion, author.getEntityVersion() + "");
  }

  @Test
  public void testDeleteAuthor() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/authors/{authorId}",
        TEST_PROJECT, mapId, authorId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());

    assertNull(authorService.getById(authorId));
  }

  @Test
  public void testDeleteNotExistingAuthor() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/authors/{authorId}",
        TEST_PROJECT, mapId, -1)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDeleteNoAccessAuthor() throws Exception {
    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/authors/{authorId}",
        TEST_PROJECT, mapId, authorId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDeleteAuthorWithGoodVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String originalVersion = project.getTopModelData().getAuthors().get(0).getEntityVersion() + "";

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/authors/{authorId}",
        TEST_PROJECT, mapId, authorId)
        .header("If-Match", originalVersion)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
    assertNull(authorService.getById(authorId));
  }

  @Test
  public void testDeleteAuthorWithWrongVersion() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/new_api/projects/{projectId}/maps/{mapId}/authors/{authorId}",
        TEST_PROJECT, mapId, authorId)
        .header("If-Match", "-1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isPreconditionFailed());
  }

  @Test
  public void testListAuthors() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/authors/", TEST_PROJECT, mapId)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isOk());
  }

  @Test
  public void testListAuthorsWithoutAccess() throws Exception {

    RequestBuilder request = get("/minerva/new_api/projects/{projectId}/maps/{mapId}/authors/", TEST_PROJECT, mapId);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

}
