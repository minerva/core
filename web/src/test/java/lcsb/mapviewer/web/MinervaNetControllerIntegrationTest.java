package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class MinervaNetControllerIntegrationTest extends ControllerIntegrationTest {

  @Autowired
  private IConfigurationService configurationService;

  @Autowired
  private IUserService userService;

  private final ObjectMapper mapper = new ObjectMapper();

  @Before
  public void setup() {
    configurationService.setConfigurationValue(ConfigurationElementType.MINERVA_ROOT, "https://minerva-dev.lcsb.uni.lu/minerva/");
    User user = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    user.setEmail("minerva@uni.lu");
    userService.update(user);
  }

  @After
  public void tearDown() {
    configurationService.setConfigurationValue(ConfigurationElementType.MINERVA_ROOT, "");
    configurationService.setConfigurationValue(ConfigurationElementType.MINERVANET_AUTH_TOKEN, "");
    User user = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    user.setEmail("");
    userService.update(user);
  }

  @Test
  public void testRegisterMachineNoPermission() throws Exception {
    RequestBuilder request = post("/minerva/api/minervanet/registerMachine");

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testRegisterMachineNoRootUrl() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    configurationService.setConfigurationValue(ConfigurationElementType.MINERVA_ROOT, "");

    RequestBuilder request = post("/minerva/api/minervanet/registerMachine")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testRegisterMachineNoEmailUrl() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    User user = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    user.setEmail("");
    userService.update(user);

    RequestBuilder request = post("/minerva/api/minervanet/registerMachine")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testUnregisterMachineNoToken() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = post("/minerva/api/minervanet/unregisterMachine")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  @Ignore("Don't register in production")
  public void testProductionRegister() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = post("/minerva/api/minervanet/registerMachine")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertFalse(configurationService.getConfigurationValue(ConfigurationElementType.MINERVANET_AUTH_TOKEN).isEmpty());

    request = post("/minerva/api/minervanet/projects/{projectId}:registerProject", "sample")
        .contentType(MediaType.APPLICATION_JSON)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    request = post("/minerva/api/minervanet/projects/{projectId}:unregisterProject", "sample")
        .contentType(MediaType.APPLICATION_JSON)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    request = post("/minerva/api/minervanet/unregisterMachine")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertTrue(configurationService.getConfigurationValue(ConfigurationElementType.MINERVANET_AUTH_TOKEN).isEmpty());
  }

  @Test
  public void testConnectedToMinervaNet() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/minervanet/isMachineRegistered")
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    Map<String, Object> result = mapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });
    assertFalse((Boolean) result.get("status"));

    configurationService.setConfigurationValue(ConfigurationElementType.MINERVANET_AUTH_TOKEN, "blah");
    response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    result = mapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });
    assertTrue((Boolean) result.get("status"));
  }

  @Test
  public void testRegisterProjectWithNoConnection() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = post("/minerva/api/minervanet/projects/{projectId}:registerProject", BUILT_IN_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testUnregisterProjectWithNoConnection() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = post("/minerva/api/minervanet/projects/{projectId}:unregisterProject", BUILT_IN_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }
}
