package lcsb.mapviewer.web;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import lcsb.mapviewer.api.projects.IDirectoryNameGenerator;
import lcsb.mapviewer.common.Md5;
import lcsb.mapviewer.services.interfaces.IProjectService;

@Profile("NumericDirectoryNameGeneratorProfile")
@Configuration
public class NumericDirectoryNameGeneratorConfiguration {

  @Autowired
  private IProjectService projectService;

  @Bean
  @Primary
  public IDirectoryNameGenerator directoryNameGenerator() throws Exception {
    IDirectoryNameGenerator mock = Mockito.mock(IDirectoryNameGenerator.class);

    Mockito.when(mock.projectIdToDirectoryName(Mockito.anyString())).thenAnswer(projectId -> {
      long id = projectService.getNextId();
      return Md5.compute(projectId + "-" + id).replaceAll("[a-z]", "0");
    });

    return mock;
  }

}
