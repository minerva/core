package lcsb.mapviewer.web;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class LicenseControllerIntegrationTest extends ControllerIntegrationTest {

  @Before
  public void setup() {
  }

  @Test
  public void testDocsListLicenses() throws Exception {

    final RequestBuilder request = get("/minerva/api/licenses/");

    mockMvc.perform(request)
        .andDo(document("licenses/list",
            requestParameters(
                parameterWithName("start").description("number of first entry in this response").optional(),
                parameterWithName("sortColumn")
                    .description("entry column that should be used for sorting (default: pubmedId)").optional(),
                parameterWithName("sortOrder").description("entry sort order (asc, desc)").optional(),
                parameterWithName("length").description("number of entries we want to obtain").optional(),
                parameterWithName("search").description("search query used for filtering").optional()),
            responseFields(
                fieldWithPath("data")
                    .description("list of licenses")
                    .type(JsonFieldType.ARRAY),
                subsectionWithPath("data[].id")
                    .description("license name")
                    .ignored()
                    .type(JsonFieldType.NUMBER),
                subsectionWithPath("data[].name")
                    .description("license name")
                    .type(JsonFieldType.STRING),
                subsectionWithPath("data[].content")
                    .description("license text")
                    .type(JsonFieldType.STRING),
                subsectionWithPath("data[].url")
                    .description("official license url")
                    .type(JsonFieldType.STRING),
                fieldWithPath("filteredSize")
                    .description("number of entries that match filter criteria")
                    .type(JsonFieldType.NUMBER),
                fieldWithPath("length")
                    .description("number of entries in this response")
                    .type(JsonFieldType.NUMBER),
                fieldWithPath("page")
                    .description("page number of result list")
                    .type(JsonFieldType.NUMBER),
                fieldWithPath("totalSize")
                    .description("number of all entries")
                    .type(JsonFieldType.NUMBER))))
        .andExpect(status().is2xxSuccessful());
  }
}
