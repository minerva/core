package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import lcsb.mapviewer.annotation.services.annotators.UniprotAnnotator;
import lcsb.mapviewer.api.users.UserController;
import lcsb.mapviewer.api.users.UserController.UserPrivilegesDTO;
import lcsb.mapviewer.api.users.UserPreferencesDTO;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.ProjectBackground;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.AnnotatorParamDefinition;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.model.user.UserAnnotationSchema;
import lcsb.mapviewer.model.user.UserClassAnnotators;
import lcsb.mapviewer.model.user.UserGuiPreference;
import lcsb.mapviewer.model.user.annotator.AnnotatorConfigParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorData;
import lcsb.mapviewer.model.user.annotator.AnnotatorInputParameter;
import lcsb.mapviewer.model.user.annotator.AnnotatorOutputParameter;
import lcsb.mapviewer.model.user.annotator.BioEntityField;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IDataOverlayService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.RequestFieldsSnippet;
import org.springframework.restdocs.request.ParameterDescriptor;
import org.springframework.restdocs.request.RequestParametersSnippet;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.patch;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("EmailSenderEnabledEmailProfile")
public class UserControllerIntegrationTest extends ControllerIntegrationTest {

  private static final String TEST_LOGIN = "test_login";

  private User user;

  @Autowired
  private IUserService userService;

  @Autowired
  private IConfigurationService configurationService;

  @Autowired
  private IProjectService projectService;

  @Autowired
  private IDataOverlayService dataOverlayService;

  @Autowired
  private ProjectSnippets projectSnippets;

  @Autowired
  private UserController userController;

  @Before
  public void setup() throws Exception {
    user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
  }

  @After
  public void tearDown() throws Exception {
    removeUser(user);
    removeUser(userService.getUserByLogin(TEST_LOGIN));
    removeProject(TEST_PROJECT);
    configurationService.setConfigurationValue(ConfigurationElementType.REQUIRE_APPROVAL_FOR_AUTO_REGISTERED_USERS,
        ConfigurationElementType.REQUIRE_APPROVAL_FOR_AUTO_REGISTERED_USERS.getDefaultValue());

  }

  @Test
  public void testDocsGrantPrivilege() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    UserPrivilegesDTO data = new UserPrivilegesDTO();
    data.getPrivileges().put(new Privilege(PrivilegeType.IS_ADMIN), true);

    RequestBuilder request = patch("/minerva/api/users/{login}:updatePrivileges", TEST_USER_LOGIN)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    String response = mockMvc.perform(request)
        .andDo(document("user/update_privilege_1",
            userPathParameters(),
            updatePrivilegeRequest(),
            responseFields(getUserResponseFields())))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    Map<String, Object> responseObject = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });

    assertTrue(responseObject.get("privileges") instanceof List);
    List<?> privileges = (List<?>) responseObject.get("privileges");
    assertEquals(1, privileges.size());
    assertFalse(privileges.get(0) instanceof String);
    assertEquals(1, userService.getUserByLogin(TEST_USER_LOGIN, true).getPrivileges().size());
  }

  @Test
  public void grantPrivilegeForNonExistingUser() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    UserPrivilegesDTO data = new UserPrivilegesDTO();
    data.getPrivileges().put(new Privilege(PrivilegeType.IS_ADMIN), true);

    RequestBuilder request = patch("/minerva/api/users/unkown_login:updatePrivileges")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsBytes(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void revokePrivilege() throws Exception {
    userService.grantUserPrivilege(user, PrivilegeType.IS_ADMIN);

    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    assertEquals(1, userService.getUserByLogin(TEST_USER_LOGIN, true).getPrivileges().size());

    UserPrivilegesDTO data = new UserPrivilegesDTO();
    data.getPrivileges().put(new Privilege(PrivilegeType.IS_ADMIN), false);

    RequestBuilder request = patch("/minerva/api/users/" + TEST_USER_LOGIN + ":updatePrivileges")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsBytes(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals("Privilege wasn't revoked", 0,
        userService.getUserByLogin(TEST_USER_LOGIN, true).getPrivileges().size());
  }

  @Test
  public void testDocsGrantProjectPrivilege() throws Exception {
    Project project = new Project(TEST_PROJECT);
    project.setOwner(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    projectService.add(project);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    UserPrivilegesDTO data = new UserPrivilegesDTO();
    data.getPrivileges().put(new Privilege(PrivilegeType.READ_PROJECT, project.getProjectId()), true);

    RequestBuilder request = patch("/minerva/api/users/{login}:updatePrivileges", TEST_USER_LOGIN)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andDo(document("user/update_privilege_2",
            userPathParameters(),
            updatePrivilegeRequest(),
            responseFields(getUserResponseFields())))
        .andExpect(status().is2xxSuccessful());

    User user = userService.getUserByLogin(TEST_USER_LOGIN, true);
    assertEquals(1, user.getPrivileges().size());

  }

  private RequestFieldsSnippet updatePrivilegeRequest() {
    return requestFields(
        fieldWithPath("privileges.*")
            .description("should the privilege defined as PRIVILEGE_TYPE:ID_OBJECT be granted or not")
            .type(JsonFieldType.BOOLEAN));
  }

  @Test
  public void grantInvalidPrivilege() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = "{\"privileges\":{\"XYZ\":true}}";

    RequestBuilder request = patch("/minerva/api/users/" + TEST_USER_LOGIN + ":updatePrivileges")
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void grantInvalidProjectPrivilege() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    UserPrivilegesDTO data = new UserPrivilegesDTO();
    data.getPrivileges().put(new Privilege(PrivilegeType.READ_PROJECT, "-5:-1"), true);

    RequestBuilder request = patch("/minerva/api/users/" + TEST_USER_LOGIN + ":updatePrivileges")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void updateInvalidPrivilege() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = "{\"privileges\":{\"IS_ADMIN\":\"surprise\"}}";

    RequestBuilder request = patch("/minerva/api/users/" + TEST_USER_LOGIN + ":updatePrivileges")
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void revokeProjectPrivilege() throws Exception {
    Project project = new Project(TEST_PROJECT);
    project.setOwner(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    projectService.add(project);

    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    UserPrivilegesDTO grantData = new UserPrivilegesDTO();
    grantData.getPrivileges().put(new Privilege(PrivilegeType.READ_PROJECT, project.getProjectId()), true);

    UserPrivilegesDTO revokeData = new UserPrivilegesDTO();
    revokeData.getPrivileges().put(new Privilege(PrivilegeType.READ_PROJECT, project.getProjectId()), false);

    RequestBuilder grantRequest = patch("/minerva/api/users/" + TEST_USER_LOGIN + ":updatePrivileges")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsBytes(grantData))
        .session(session);

    RequestBuilder revokeRequest = patch("/minerva/api/users/" + TEST_USER_LOGIN + ":updatePrivileges")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsBytes(revokeData))
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().is2xxSuccessful());
    mockMvc.perform(revokeRequest)
        .andExpect(status().is2xxSuccessful());
    assertEquals(0, user.getPrivileges().size());
  }

  @Test
  public void testDocsFetchUserPrivilege() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/users/{login}", TEST_USER_LOGIN)
        .session(session);

    String response = mockMvc.perform(request)
        .andDo(document("user/get_user",
            userPathParameters(),
            userColumListRequestParam(),
            responseFields(getUserResponseFields())))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    Map<String, Object> responseObject = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });

    List<?> privileges = (List<?>) responseObject.get("privileges");
    assertEquals(0, privileges.size());
  }

  @Test
  public void fetchNotExistingUser() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/users/{login}", "not_existing")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  private RequestParametersSnippet userColumListRequestParam() {
    List<String> columns = new ArrayList<>(userController.createUserColumnSet(""));
    Collections.sort(columns);
    return requestParameters(parameterWithName("columns")
        .description("comma separated list of columns to be returned (accepted values: "
            + StringUtils.join(columns, ",") + ")")
        .optional());
  }

  @Test
  public void testUsersShouldBeFetchedInAlphabeticOrder() throws Exception {
    List<User> users = new ArrayList<>();
    try {
      for (int i = 10; i > 0; i--) {
        users.add(createUser("random_user_" + i, "passwd" + i));
      }

      MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

      RequestBuilder grantRequest = get("/minerva/api/users/?columns=login")
          .session(session);

      String response = mockMvc.perform(grantRequest)
          .andExpect(status().is2xxSuccessful())
          .andReturn().getResponse().getContentAsString();

      List<Map<String, Object>> resultUsers = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
      });

      for (int i = 1; i < resultUsers.size(); i++) {
        String login1 = resultUsers.get(i - 1).get("login").toString();
        String login2 = resultUsers.get(i).get("login").toString();
        assertTrue("[sort issue]: " + login1 + " should be before " + login2, login1.compareTo(login2) > 0);
      }
    } finally {
      for (final User u : users) {
        userService.remove(u);
      }
    }
  }

  @Test
  public void testInformationAboutLdapForAllUsers() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder grantRequest = get("/minerva/api/users/?columns=login,ldapAccountAvailable,connectedToLdap")
        .session(session);

    String response = mockMvc.perform(grantRequest)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> resultUsers = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });
    for (int i = 0; i < resultUsers.size(); i++) {
      assertNotNull(resultUsers.get(i).get("ldapAccountAvailable"));
      assertNotNull(resultUsers.get(i).get("connectedToLdap"));
    }
  }

  @Test
  public void testInformationAboutLdapForSingleUser() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder grantRequest = get("/minerva/api/users/admin/?columns=login,ldapAccountAvailable,connectedToLdap,lastActive")
        .session(session);

    String response = mockMvc.perform(grantRequest)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    Map<String, Object> result = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });
    assertNotNull(result.get("ldapAccountAvailable"));
    assertNotNull(result.get("connectedToLdap"));
    assertNotNull(result.get("lastActive"));
  }

  @Test
  public void testDocsAddUser() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("name", "Name"),
        new BasicNameValuePair("password", "Password"))));

    RequestBuilder grantRequest = post("/minerva/api/users/{login}", TEST_LOGIN)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(grantRequest)
        .andDo(document("user/create_user",
            userPathParameters(),
            requestParameters(addUserRequestFields()),
            responseFields(getUserResponseFields())))
        .andExpect(status().is2xxSuccessful());

    assertNotNull(userService.getUserByLogin(TEST_LOGIN));
  }

  @Test
  public void testAddUserWithoutPassword() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("name", "FirstName"),
        new BasicNameValuePair("password", " "))));

    RequestBuilder grantRequest = post("/minerva/api/users/" + TEST_LOGIN)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testGrantDefaultPrivilegesFromAnonymousWhenAddingUser() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createAndPersistProject(TEST_PROJECT);

    User anonymous = userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN);
    userService.grantUserPrivilege(anonymous, PrivilegeType.WRITE_PROJECT, TEST_PROJECT);
    userService.revokeUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, TEST_PROJECT);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("name", "FirstName"),
        new BasicNameValuePair("password", "UserPassword"),
        new BasicNameValuePair("defaultPrivileges", "true"))));

    RequestBuilder grantRequest = post("/minerva/api/users/" + TEST_LOGIN)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().is2xxSuccessful());

    User user = userService.getUserByLogin(TEST_LOGIN, true);

    assertTrue("Default privilege wasn't added after user was created",
        user.getPrivileges().contains(new Privilege(PrivilegeType.WRITE_PROJECT, TEST_PROJECT)));
    assertFalse("Default privilege was added after user was created",
        user.getPrivileges().contains(new Privilege(PrivilegeType.READ_PROJECT, TEST_PROJECT)));
  }

  @Test
  public void addUserWithoutBodyShouldThrowBadRequest() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder grantRequest = post("/minerva/api/users/" + TEST_LOGIN)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testDocsUserUpdateOwnPassword() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String newPassword = "new pass";
    String body = "{\"user\":{\"password\":\"" + newPassword + "\"}}";

    RequestBuilder grantRequest = patch("/minerva/api/users/{login}", TEST_USER_LOGIN)
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(grantRequest)
        .andDo(document("user/update_user",
            userPathParameters(),
            requestParameters(getUserRequestFieldsWithoutLogin()),
            responseFields(getUserResponseFields())))
        .andExpect(status().is2xxSuccessful());

    MockHttpSession sessionWithNewPass = createSession(TEST_USER_LOGIN, newPassword);
    assertNotNull(sessionWithNewPass);
  }

  @Test
  public void userUpdateOrcid() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = "{\"user\":{\"orcidId\":\"0000-1111-2222-3333\"}}";

    RequestBuilder grantRequest = patch("/minerva/api/users/{login}", TEST_USER_LOGIN)
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void userUpdateOrcidWithX() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = "{\"user\":{\"orcidId\":\"0000-1111-2222-333X\"}}";

    RequestBuilder grantRequest = patch("/minerva/api/users/{login}", TEST_USER_LOGIN)
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void userUpdateEmptyOrcid() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = "{\"user\":{\"orcidId\":\"\"}}";

    RequestBuilder grantRequest = patch("/minerva/api/users/{login}", TEST_USER_LOGIN)
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void userUpdateInvalidOrcid() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = "{\"user\":{\"orcidId\":\"xxxx-1111-2222-3333\"}}";

    RequestBuilder grantRequest = patch("/minerva/api/users/{login}", TEST_USER_LOGIN)
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void userCannotUpdateOwnLdapConnection() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = "{\"user\":{\"connectedToLdap\":" + (!user.isConnectedToLdap()) + "}}";

    RequestBuilder grantRequest = patch("/minerva/api/users/" + TEST_USER_LOGIN)
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().isForbidden());
  }

  @Test
  public void userCannotUpdateOwnActive() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = "{\"user\":{\"active\":" + (!user.isActive()) + "}}";

    RequestBuilder grantRequest = patch("/minerva/api/users/" + TEST_USER_LOGIN)
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().isForbidden());
  }

  @Test
  public void userCannotUpdateOwnLdapConnectionButNoChangeShouldPass() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = "{\"user\":{\"connectedToLdap\":" + user.isConnectedToLdap() + "}}";

    RequestBuilder grantRequest = patch("/minerva/api/users/" + TEST_USER_LOGIN)
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void userCannotUpdateOwnActiveButNoChangeShouldPass() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = "{\"user\":{\"active\":" + user.isActive() + "}}";

    RequestBuilder grantRequest = patch("/minerva/api/users/" + TEST_USER_LOGIN)
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(grantRequest)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDocsRemoveUserWithLayouts() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Project project = new Project(TEST_PROJECT);
    project.setOwner(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));

    projectService.add(project);

    createOverlay(TEST_PROJECT, user);

    RequestBuilder request = delete("/minerva/api/users/{login}", user.getLogin())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("user/delete_user",
            userPathParameters()))
        .andExpect(status().is2xxSuccessful());

    assertEquals(0, dataOverlayService.getDataOverlaysByProject(project).size());
  }

  @Test
  public void removeUserWithBackgrounds() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Project project = new Project(TEST_PROJECT);
    project.setOwner(user);
    ProjectBackground background = new ProjectBackground();
    background.setCreator(user);
    project.addProjectBackground(background);
    projectService.add(project);

    RequestBuilder request = delete("/minerva/api/users/{login}", user.getLogin())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertNull(userService.getUserByLogin(user.getLogin()));
  }

  @Test
  public void testDocsGetUsers() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder grantRequest = get("/minerva/api/users/")
        .session(session);

    mockMvc.perform(grantRequest)
        .andDo(document("user/list_users",
            userColumListRequestParam(),
            responseFields(fieldWithPath("[]")
                .description("list of users")
                .type(JsonFieldType.ARRAY)).andWithPrefix("[].", getUserResponseFields())))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testGetUsersWithLdapInfo() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder grantRequest = get("/minerva/api/users/?columns=connectedToLdap")
        .session(session);

    String content = mockMvc.perform(grantRequest)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    List<Map<String, Object>> list = objectMapper.readValue(content, new TypeReference<List<Map<String, Object>>>() {
    });
    assertTrue(list.get(0).containsKey("connectedToLdap"));
    assertFalse(list.get(0).containsKey("login"));
  }

  private List<FieldDescriptor> getUserResponseFields() {
    List<FieldDescriptor> result = new ArrayList<>(Arrays.asList(
        fieldWithPath("login")
            .description("user login")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath("connectedToLdap")
            .description("is user account connected to LDAP")
            .optional()
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath("ldapAccountAvailable")
            .description("does the account exist in LDAP")
            .optional()
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath("active")
            .description("ist the use account active (can user login)")
            .optional()
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath("confirmed")
            .description("is the user email confirmed")
            .optional()
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath("email")
            .description("email address")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath("orcidId")
            .description("orcid identifier")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath("id")
            .description("user unique id")
            .optional()
            .type(JsonFieldType.NUMBER),
        fieldWithPath("maxColor")
            .description("color used for drawing data overlays with max value")
            .optional()
            .type("color"),
        fieldWithPath("minColor")
            .description("color used for drawing data overlays with min value")
            .optional()
            .type("color"),
        fieldWithPath("neutralColor")
            .description("color used for drawing data overlays with 0 value")
            .optional()
            .type("color"),
        fieldWithPath("simpleColor")
            .description("color used for drawing data overlays without value")
            .optional()
            .type("color"),
        fieldWithPath("name")
            .description("first name")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath("surname")
            .description("last name")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath("removed")
            .description("is the account removed")
            .optional()
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath("termsOfUseConsent")
            .description("did user agree to terms of use")
            .optional()
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath("privileges")
            .description("list of user privileges")
            .optional()
            .type(JsonFieldType.ARRAY),
        fieldWithPath("privileges[].objectId")
            .description("object id to which project has privilege")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath("lastActive")
            .description("datetime of the last activity in the active session")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath("privileges[].privilegeType")
            .description(
                "type of privilege, available values: " + projectSnippets.getOptionsAsString(PrivilegeType.class))
            .optional()
            .type(JsonFieldType.STRING)));
    result.addAll(getUserPreferencesResponseFields("", true));
    return result;
  }

  private List<ParameterDescriptor> addUserRequestFields() {
    return Arrays.asList(
        parameterWithName("name")
            .optional()
            .description("first name"),
        parameterWithName("surname")
            .optional()
            .description("last name"),
        parameterWithName("password")
            .description("user password"),
        parameterWithName("email")
            .optional()
            .description("email address"),
        parameterWithName("defaultPrivileges")
            .optional()
            .description("should the default privileges be added to the user after user creation"));
  }

  private List<ParameterDescriptor> getUserRequestFieldsWithoutLogin() {
    return Arrays.asList(
        parameterWithName("password")
            .optional()
            .description("user password"),
        parameterWithName("connectedToLdap")
            .optional()
            .description("is user account connected to LDAP"),
        parameterWithName("ldapAccountAvailable")
            .optional()
            .description("does is account exist in LDAP"),
        parameterWithName("email")
            .optional()
            .description("email address"),
        parameterWithName("maxColor")
            .optional()
            .description("color used for drawing data overlays with max value"),
        parameterWithName("minColor")
            .optional()
            .description("color used for drawing data overlays with min value"),
        parameterWithName("neutralColor")
            .optional()
            .description("color used for drawing data overlays with 0 value"),
        parameterWithName("simpleColor")
            .optional()
            .description("color used for drawing data overlays without value"),
        parameterWithName("name")
            .optional()
            .description("first name"),
        parameterWithName("surname")
            .optional()
            .description("last name"));
  }

  @Test
  public void testDocsUpdateProjectUploadPreferences() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    UserAnnotationSchema preferences = new UserAnnotationSchema();
    UserPreferencesDTO data = new UserPreferencesDTO(preferences);
    preferences.setValidateMiriamTypes(true);
    preferences.setAnnotateModel(true);
    preferences.setAutoResizeMap(true);
    preferences.setSemanticZoomContainsMultipleOverlays(true);
    preferences.setSbgnFormat(true);

    RequestBuilder request = patch("/minerva/api/users/{login}:updatePreferences", TEST_USER_LOGIN)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andDo(document("user/update_preferences_1",
            userPathParameters(),
            updatePreferencesRequest(),
            responseFields(getUserPreferencesResponseFields("", false))))
        .andExpect(status().is2xxSuccessful());

    User user = userService.getUserByLogin(TEST_USER_LOGIN, true);
    assertTrue(user.getAnnotationSchema().getValidateMiriamTypes());
  }

  @Test
  public void testDocsUpdateElementAnnotatorsPreferences() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    UserAnnotationSchema preferences = new UserAnnotationSchema();
    UserPreferencesDTO data = new UserPreferencesDTO(preferences);
    AnnotatorData uniprotAnnotator = new AnnotatorData(UniprotAnnotator.class);
    uniprotAnnotator.addAnnotatorParameter(new AnnotatorInputParameter(null, MiriamType.UNIPROT));
    uniprotAnnotator.addAnnotatorParameter(new AnnotatorInputParameter(BioEntityField.NAME, MiriamType.UNIPROT));
    uniprotAnnotator.addAnnotatorParameter(new AnnotatorOutputParameter(MiriamType.HGNC_SYMBOL));
    uniprotAnnotator.addAnnotatorParameter(new AnnotatorOutputParameter(MiriamType.UNIPROT));
    uniprotAnnotator.addAnnotatorParameter(new AnnotatorOutputParameter(MiriamType.EC));
    uniprotAnnotator.addAnnotatorParameter(new AnnotatorOutputParameter(MiriamType.ENTREZ));
    uniprotAnnotator
        .addAnnotatorParameter(new AnnotatorConfigParameter(AnnotatorParamDefinition.KEGG_ORGANISM_IDENTIFIER, "XXX"));
    preferences.addClassAnnotator(new UserClassAnnotators(Gene.class, Collections.singletonList(uniprotAnnotator)));

    RequestBuilder request = patch("/minerva/api/users/{login}:updatePreferences", TEST_USER_LOGIN)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data))
        .session(session);

    mockMvc.perform(request)
        .andDo(document("user/update_preferences_2",
            userPathParameters(),
            updatePreferencesRequest(),
            responseFields(getUserPreferencesResponseFields("", false))))
        .andExpect(status().is2xxSuccessful());

    User user = userService.getUserByLogin(TEST_USER_LOGIN, true);
    assertEquals(1, user.getAnnotationSchema().getAnnotatorsForClass(Gene.class).size());
    AnnotatorData annotatorData = user.getAnnotationSchema().getAnnotatorsForClass(Gene.class).get(0);
    assertEquals(2, annotatorData.getInputParameters().size());
    assertEquals(4, annotatorData.getOutputParameters().size());
    assertEquals(7, annotatorData.getAnnotatorParams().size());

  }

  @Test
  public void testDocsUpdateGuiPreferences() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    UserAnnotationSchema preferences = new UserAnnotationSchema();
    UserPreferencesDTO data = new UserPreferencesDTO(preferences);
    data.getPreferences().addGuiPreference(new UserGuiPreference("admin-projects-datatable-order", "1-asc"));

    RequestBuilder request = patch("/minerva/api/users/{login}:updatePreferences", TEST_USER_LOGIN)
        .content(objectMapper.writeValueAsString(data))
        .contentType(MediaType.APPLICATION_JSON)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("user/update_preferences_5",
            userPathParameters(),
            updatePreferencesRequest(),
            responseFields(getUserPreferencesResponseFields("", false))))
        .andExpect(status().is2xxSuccessful());

    User user = userService.getUserByLogin(TEST_USER_LOGIN, true);
    assertEquals("1-asc", user.getAnnotationSchema().getGuiPreference("admin-projects-datatable-order").getValue());
  }

  private RequestFieldsSnippet updatePreferencesRequest() {
    RequestFieldsSnippet result = requestFields(
        fieldWithPath("preferences")
            .description("information about default preferences")
            .type(JsonFieldType.OBJECT));
    for (final FieldDescriptor fd : getProjectUploadResponseFields("")) {
      if (fd.getDescription() != null) {
        result = result.andWithPrefix("preferences.",
            fieldWithPath(fd.getPath()).description(fd.getDescription()).type(fd.getType()).optional());
      }
    }
    for (final FieldDescriptor fd : getElementAnnotatorsResponseFields("")) {
      if (fd.getDescription() != null) {
        result = result.andWithPrefix("preferences.",
            fieldWithPath(fd.getPath()).description(fd.getDescription()).type(fd.getType()).optional());
      }
    }
    for (final FieldDescriptor fd : getGuiPreferencesResponseFields("")) {
      if (fd.getDescription() != null) {
        result = result.andWithPrefix("preferences.",
            fieldWithPath(fd.getPath()).description(fd.getDescription()).type(fd.getType()).optional());
      }
    }
    return result;
  }

  private List<FieldDescriptor> getUserPreferencesResponseFields(final String originalPrefix, final boolean ignore) {
    String prefix = originalPrefix;
    if (!prefix.isEmpty() && !prefix.endsWith(".")) {
      prefix = prefix + ".preferences";
    } else {
      prefix = "preferences";
    }
    List<FieldDescriptor> fields = new ArrayList<>();
    fields.add(fieldWithPath(prefix).description("set of user preferences").type(JsonFieldType.OBJECT));
    fields.addAll(getElementAnnotatorsResponseFields(prefix + "."));
    fields.addAll(getGuiPreferencesResponseFields(prefix + "."));
    fields.addAll(getProjectUploadResponseFields(prefix + "."));
    if (ignore) {
      for (final FieldDescriptor fieldDescriptor : fields) {
        fieldDescriptor.ignored().optional();
      }
    }
    return fields;
  }

  private List<FieldDescriptor> getElementAnnotatorsResponseFields(final String prefix) {
    return Arrays.asList(
        fieldWithPath(prefix + "element-annotators")
            .description("definition of element annotators used by user")
            .type(JsonFieldType.OBJECT),
        fieldWithPath(prefix + "element-annotators['*']")
            .description(
                "list of annotators used for specific BioEntity type. "
                    + "Type of BioEntity is a class name that represents specific type in the system, for example: "
                    + BioEntity.class.getName() + ", " + Complex.class.getName())
            .type(JsonFieldType.ARRAY),
        fieldWithPath(prefix + "element-annotators['*'][].id")
            .description("id")
            .ignored()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "element-annotators['*'][].annotatorClass")
            .description("type of annotator")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "element-annotators['*'][].order")
            .description("defines order in which annotators should be used")
            .type(JsonFieldType.NUMBER),
        fieldWithPath(prefix + "element-annotators['*'][].parameters")
            .description("list of parameters for the annotator")
            .type(JsonFieldType.ARRAY),
        fieldWithPath(prefix + "element-annotators['*'][].parameters[].type")
            .description(
                "is this parameter INPUT/OUTPUT/CONFIG type. "
                    + "INPUT types define what should be used to identify the element. "
                    + "OUTPUT parameter defines what should be written to the element.")
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "element-annotators['*'][].parameters[].annotation_type")
            .description("specifies annotation type to be used (if empty then field will be used)")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "element-annotators['*'][].parameters[].field")
            .description("specifies field to be used")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "element-annotators['*'][].parameters[].name")
            .description("name of the CONFIG parameter type")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "element-annotators['*'][].parameters[].commonName")
            .description("readable name of the CONFIG parameter type")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "element-annotators['*'][].parameters[].inputType")
            .description("class type of the CONFIG parameter type")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "element-annotators['*'][].parameters[].description")
            .description("description of the CONFIG parameter type")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "element-annotators['*'][].parameters[].value")
            .description("value of the CONFIG parameter type")
            .optional()
            .type(JsonFieldType.STRING),
        fieldWithPath(prefix + "element-annotators['*'][].parameters[].order")
            .description(
                "input parameters are checked in order. First match identifies the element. "
                    + "This field defines what is the order of INPUT parameters.")
            .optional()
            .type(JsonFieldType.NUMBER)

    );
  }

  private List<FieldDescriptor> getGuiPreferencesResponseFields(final String prefix) {
    return Arrays.asList(
        fieldWithPath(prefix + "gui-preferences")
            .description("definition of user gui preferences.")
            .type(JsonFieldType.OBJECT),
        fieldWithPath(prefix + "gui-preferences.*")
            .optional()
            .description(
                "key-value definition of gui preference used for remembering default sort order, default pagination, etc")
            .type(JsonFieldType.STRING));
  }

  private List<FieldDescriptor> getProjectUploadResponseFields(final String prefix) {
    return Arrays.asList(
        fieldWithPath(prefix + "project-upload")
            .description("definition of preferences for project-upload.")
            .type(JsonFieldType.OBJECT),
        fieldWithPath(prefix + "project-upload.annotate-model")
            .description("annotate model after it is uploaded.")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath(prefix + "project-upload.auto-resize")
            .description("auto resize map after parsing")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath(prefix + "project-upload.sbgn")
            .description("visualize project in sbgn-like view")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath(prefix + "project-upload.semantic-zooming-contains-multiple-overlays")
            .description("display each semantic zoom level in separate overlay.")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath(prefix + "project-upload.validate-miriam")
            .description("validate all miriam annotations")
            .type(JsonFieldType.BOOLEAN));
  }

  @Test
  public void grantPrivilegeWithNoData() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = patch("/minerva/api/users/{login}:updatePrivileges", TEST_USER_LOGIN)
        .contentType(MediaType.APPLICATION_JSON)
        .content("{}")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void updatePreferencesWithInvalidData() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = patch("/minerva/api/users/{login}:updatePreferences", TEST_USER_LOGIN)
        .contentType(MediaType.APPLICATION_JSON)
        .content("{}")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void registerNewUserWhenRegistrationDisabled() throws Exception {
    configurationService.setConfigurationValue(ConfigurationElementType.ALLOW_AUTO_REGISTER, "false");
    Map<String, Object> data = new HashMap<>();
    data.put("email", "piotr.gawron@uni.lu");
    data.put("password", "123qweasdzxc");
    data.put("name", "Piotr");
    data.put("surname", "Gawron");

    RequestBuilder request = post("/minerva/api/users:registerUser")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data));

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());

  }

  @Test
  public void testDocsRegisterNewUser() throws Exception {
    try {
      configurationService.setConfigurationValue(ConfigurationElementType.MINERVA_ROOT,
          "https://minerva-dev.lcsb.uni.lu/minerva/");
      configurationService.setConfigurationValue(ConfigurationElementType.ALLOW_AUTO_REGISTER, "true");

      Map<String, Object> data = new HashMap<>();
      String email = "piotr.gawron@uni.lu";
      data.put("email", email);
      data.put("password", "123qweasdzxc");
      data.put("name", "Piotr");
      data.put("surname", "Gawron");

      RequestBuilder request = post("/minerva/api/users:registerUser")
          .contentType(MediaType.APPLICATION_JSON)
          .content(objectMapper.writeValueAsString(data));

      mockMvc.perform(request)
          .andDo(document("user/register_user",
              requestFields(
                  fieldWithPath("email")
                      .description("user email (and login)")
                      .type(JsonFieldType.STRING),
                  fieldWithPath("password")
                      .description("password")
                      .type(JsonFieldType.STRING),
                  fieldWithPath("name")
                      .description("given name)")
                      .type(JsonFieldType.STRING),
                  fieldWithPath("surname")
                      .description("family name")
                      .type(JsonFieldType.STRING)),
              responseFields(getUserResponseFields())))
          .andExpect(status().is2xxSuccessful());

      User user = userService.getUserByLogin(email);
      assertNotNull(user);
      userService.remove(user);
    } finally {
      configurationService.setConfigurationValue(ConfigurationElementType.ALLOW_AUTO_REGISTER, "false");
      configurationService.setConfigurationValue(ConfigurationElementType.MINERVA_ROOT, "");

    }
  }

  @Test
  public void registerNewUserThatAlreadyExists() throws Exception {
    configurationService.setConfigurationValue(ConfigurationElementType.MINERVA_ROOT,
        "https://minerva-dev.lcsb.uni.lu/minerva/");

    String email = "piotr.gawron@uni.lu";

    User user = new User();
    user.setLogin(email);
    user.setCryptedPassword("");
    userService.add(user);

    configurationService.setConfigurationValue(ConfigurationElementType.ALLOW_AUTO_REGISTER, "true");
    try {
      Map<String, Object> data = new HashMap<>();
      data.put("email", email);
      data.put("password", "123qweasdzxc");
      data.put("name", "Piotr");
      data.put("surname", "Gawron");

      RequestBuilder request = post("/minerva/api/users:registerUser")
          .contentType(MediaType.APPLICATION_JSON)
          .content(objectMapper.writeValueAsString(data));

      mockMvc.perform(request)
          .andExpect(status().isBadRequest());

    } finally {
      configurationService.setConfigurationValue(ConfigurationElementType.ALLOW_AUTO_REGISTER, "false");
      configurationService.setConfigurationValue(ConfigurationElementType.MINERVA_ROOT, "");

      userService.remove(user);
    }

  }

  @Test
  public void registerNewUserWithUsedEmail() throws Exception {
    try {
      configurationService.setConfigurationValue(ConfigurationElementType.MINERVA_ROOT,
          "https://minerva-dev.lcsb.uni.lu/minerva/");

      String email = "piotr.gawron@uni.lu";

      User user = new User();
      user.setLogin(TEST_LOGIN);
      user.setEmail(email);
      user.setCryptedPassword("");
      userService.add(user);

      configurationService.setConfigurationValue(ConfigurationElementType.ALLOW_AUTO_REGISTER, "true");
      Map<String, Object> data = new HashMap<>();
      data.put("email", email);
      data.put("password", "123qweasdzxc");
      data.put("name", "Piotr");
      data.put("surname", "Gawron");

      RequestBuilder request = post("/minerva/api/users:registerUser")
          .contentType(MediaType.APPLICATION_JSON)
          .content(objectMapper.writeValueAsString(data));

      mockMvc.perform(request)
          .andExpect(status().isBadRequest());
    } finally {
      userService.remove(user);
      configurationService.setConfigurationValue(ConfigurationElementType.MINERVA_ROOT, "");
    }
  }

}
