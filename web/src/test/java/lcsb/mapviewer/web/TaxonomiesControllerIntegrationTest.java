package lcsb.mapviewer.web;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class TaxonomiesControllerIntegrationTest extends ControllerIntegrationTest {

  @Before
  public void setup() {
  }

  @After
  public void tearDown() {
  }

  @Test
  public void testDocsGetTaxonomy() throws Exception {

    RequestBuilder request = get("/minerva/api/taxonomy/{taxonomyId}/", "9606");

    mockMvc.perform(request)
        .andDo(document("taxonomy/get_taxonomy",
            pathParameters(parameterWithName("taxonomyId").description("taxonomy id")),
            responseFields(
                fieldWithPath("name")
                    .description("name")
                    .type(JsonFieldType.STRING),
                fieldWithPath("id")
                    .description("taxonomy id")
                    .type(JsonFieldType.STRING))))
        .andExpect(status().is2xxSuccessful());
  }

}
