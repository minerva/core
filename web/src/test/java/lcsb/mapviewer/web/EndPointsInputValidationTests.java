package lcsb.mapviewer.web;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.common.comparator.StringComparator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.servlet.http.HttpServletResponse;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

@RunWith(SpringJUnit4ClassRunner.class)
public class EndPointsInputValidationTests extends ControllerIntegrationTest {

  private static final String[] testValues = {" ", "-1", "0", "empty", "admin", "1.00,2.00", "17.00", "1"};
  private static final List<HttpStatus> validResponses = Arrays.asList(HttpStatus.OK, HttpStatus.BAD_REQUEST,
      HttpStatus.NOT_FOUND, HttpStatus.FORBIDDEN, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS, HttpStatus.UNSUPPORTED_MEDIA_TYPE,
      HttpStatus.FOUND);
  @Autowired
  private RequestMappingHandlerMapping requestMappingHandlerMapping;

  @Before
  public void setUp() throws Exception {
    Configurator.setAllLevels(LogManager.getRootLogger().getName(), Level.ERROR);
  }

  @After
  public void tearDown() throws Exception {
    final String file = "src/test/resources/log4j2.properties";
    final LoggerContext context = (LoggerContext) LogManager.getContext(false);
    context.setConfigLocation(new URI(file));
  }

  @Test
  @Ignore
  public void testResponseStatusCodeFromEndpoints() throws Exception {
    final List<Pair<String, RequestMethod>> urls = new ArrayList<>();
    for (final RequestMappingInfo t : requestMappingHandlerMapping.getHandlerMethods().keySet()) {

      for (final String url : t.getPatternsCondition().getPatterns()) {
        for (final RequestMethod method : t.getMethodsCondition().getMethods()) {
          urls.add(new Pair<>(url, method));
        }
      }
    }
    urls.sort(new Comparator<Pair<String, RequestMethod>>() {
      @Override
      public int compare(final Pair<String, RequestMethod> arg0, final Pair<String, RequestMethod> arg1) {
        return new StringComparator().compare(arg0.getLeft() + " - " + arg0.getRight(),
            arg1.getLeft() + " - " + arg1.getRight());
      }
    });
    for (final Pair<String, RequestMethod> pair : urls) {
      testUrl(pair.getLeft(), pair.getRight());
    }
  }

  private void testUrl(final String url, final RequestMethod method) throws Exception {
    for (final String urlWithData : getAllPossibleUrls(url)) {
      final List<MockHttpServletRequestBuilder> requests = new ArrayList<>();
      switch (method) {
        case GET:
          requests.add(get(urlWithData).session(new MockHttpSession()));
          break;
        case PATCH:
          requests.add(patch(urlWithData).content("XX=YY").session(new MockHttpSession()).contentType(MediaType.APPLICATION_FORM_URLENCODED));
          requests.add(patch(urlWithData).content("{\"XX\":\"YY\"}").session(new MockHttpSession()).contentType(MediaType.APPLICATION_JSON));
          break;
        case POST:
          requests.add(post(urlWithData).content("XX=YY").session(new MockHttpSession()).contentType(MediaType.APPLICATION_FORM_URLENCODED));
          requests.add(post(urlWithData).content("{\"XX\":\"YY\"}").session(new MockHttpSession()).contentType(MediaType.APPLICATION_JSON));
          break;
        case DELETE:
          requests.add(delete(urlWithData).session(new MockHttpSession()));
          break;
        case PUT:
          requests.add(put(urlWithData).content("{\"XX\":\"YY\"}").session(new MockHttpSession()).contentType(MediaType.APPLICATION_JSON));
          break;
        default:
          fail(method.toString());
      }
      boolean validResponse = false;
      HttpStatus status = HttpStatus.I_AM_A_TEAPOT;
      for (final MockHttpServletRequestBuilder request : requests) {
        final HttpServletResponse response = mockMvc.perform(request).andReturn().getResponse();
        status = HttpStatus.valueOf(response.getStatus());
        validResponse |= validResponses.contains(status);
      }

      assertTrue("[" + method + " \"" + urlWithData + "\"]\tInvalid response: " + status.name(), validResponse);
    }

  }

  private List<String> getAllPossibleUrls(final String url) {
    final List<String> result = new ArrayList<>();
    final Set<String> parameters = getParameters(url);
    if (parameters.size() == 0) {
      result.add(url);
    } else {
      final String parameter = "{" + parameters.iterator().next() + "}";
      for (final String s : testValues) {
        result.addAll(getAllPossibleUrls(url.replace(parameter, s)));
      }
    }
    return result;
  }

  private Set<String> getParameters(final String url) {
    final Set<String> result = new HashSet<>();
    int pos = url.indexOf("{");
    while (pos > 0) {
      final int endPos = url.indexOf("}", pos);
      result.add(url.substring(pos + 1, endPos));
      pos = url.indexOf("{", pos + 1);
    }
    return result;
  }

}
