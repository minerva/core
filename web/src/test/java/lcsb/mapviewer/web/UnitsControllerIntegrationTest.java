package lcsb.mapviewer.web;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.ModelData;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.Arrays;
import java.util.List;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class UnitsControllerIntegrationTest extends ControllerIntegrationTest {

  private Project project;
  private ModelData map;

  @Before
  public void setup() {
    project = createAndPersistProject(TEST_PROJECT);
    map = project.getTopModel().getModelData();
  }

  @After
  public void tearDown() throws Exception {
    removeProject(project);
  }

  @Test
  public void testGetUnitListWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/*/models/*/units/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetUnitWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/*/models/*/units/1")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDocsGetUnits() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/units/", TEST_PROJECT, "*")
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_maps/get_units",
            pathParameters(parameterWithName("projectId").description("project identifier"),
                parameterWithName("mapId").description("map identifier")),
            listOfUnitsFields()))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDocsGetUnit() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/units/{unitId}", TEST_PROJECT, "*",
        map.getUnits().iterator().next().getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_maps/get_unit",
            pathParameters(parameterWithName("projectId").description("project identifier"),
                parameterWithName("mapId").description("map identifier"),
                parameterWithName("unitId").description("unit identifier")),
            responseFields(unitFields())))
        .andExpect(status().is2xxSuccessful());
  }

  private List<FieldDescriptor> unitFields() {
    return Arrays.asList(
        fieldWithPath("id")
            .description("unique unit identifier")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("name")
            .description("name")
            .optional()
            .type(JsonFieldType.STRING),
        subsectionWithPath("unitTypeFactors")
            .description("in complex unit this define how specific unit types are associated")
            .type(JsonFieldType.ARRAY),
        fieldWithPath("unitId")
            .description("unit identifier taken from source file")
            .type(JsonFieldType.STRING));
  }

  private ResponseFieldsSnippet listOfUnitsFields() {
    return responseFields(
        fieldWithPath("[]").description("list of units"))
        .andWithPrefix("[].", unitFields());
  }

}
