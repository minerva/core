package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.annotation.cache.BigFileCache;
import lcsb.mapviewer.annotation.services.genome.ReferenceGenomeConnector;
import lcsb.mapviewer.common.MinervaConfigurationHolder;
import lcsb.mapviewer.model.cache.BigFileEntry;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.ReferenceGenome;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeGeneMapping;
import lcsb.mapviewer.model.map.layout.ReferenceGenomeType;
import lcsb.mapviewer.services.interfaces.IReferenceGenomeService;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.io.File;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class GenomicsControllerIntegrationTest extends ControllerIntegrationTest {

  private final MiriamData ebolaId = new MiriamData(MiriamType.TAXONOMY, "1570291");
  private final String ebolaUrl = "ftp://hgdownload.cse.ucsc.edu/goldenPath/eboVir3/bigZips/eboVir3.2bit";

  @Autowired
  private ProjectSnippets snippets;

  @Autowired
  private ReferenceGenomeConnector referenceGenomeConnector;

  @Autowired
  private IReferenceGenomeService referenceGenomeService;

  @Autowired
  private BigFileCache bigFileCache;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private MinervaConfigurationHolder configurationHolder;

  @Before
  public void setup() {
  }

  @After
  public void tearDown() throws Exception {
    referenceGenomeConnector.waitForDownloadsToFinish();
    ReferenceGenome referenceGenome = referenceGenomeService.getReferenceGenomeViewByParams(ebolaId,
        ReferenceGenomeType.UCSC, "eboVir3");
    if (referenceGenome != null) {
      referenceGenomeService.removeGenome(referenceGenome);
    }
  }

  @Test
  public void testDocsGetTaxonomies() throws Exception {

    RequestBuilder request = get("/minerva/api/genomics/taxonomies/");

    String response = mockMvc.perform(request)
        .andDo(document("genomics/get_organisms",
            responseFields(fieldWithPath("[]")
                .description("list of organisms")
                .type("array")).andWithPrefix("[].", organismResponseFields())))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Object> data = objectMapper.readValue(response, new TypeReference<List<Object>>() {
    });

    assertFalse("list of taxonomies shouldn't be empty", data.isEmpty());
  }

  private List<FieldDescriptor> organismResponseFields() {
    return Arrays.asList(
        fieldWithPath("resource")
            .description("taxonomy id of the organism")
            .type(JsonFieldType.STRING),
        fieldWithPath("annotatorClassName").ignored(),
        fieldWithPath("id").ignored(),
        fieldWithPath("link").ignored(),
        fieldWithPath("type").ignored());
  }

  @Test
  public void testDocsGetGenomeTypes() throws Exception {

    RequestBuilder request = get("/minerva/api/genomics/taxonomies/{taxonomyId}/genomeTypes/", "9606");

    String response = mockMvc.perform(request)
        .andDo(document("genomics/get_genome_types",
            pathParameters(parameterWithName("taxonomyId").description("organism taxonomy id")),
            responseFields(
                fieldWithPath("[]")
                    .description("list of genome types")
                    .type("array"),
                fieldWithPath("[].type")
                    .description("type")
                    .type(JsonFieldType.STRING))))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Object> data = objectMapper.readValue(response, new TypeReference<List<Object>>() {
    });
    assertEquals(1, data.size());

  }

  @Test
  public void testDocsGetAllGenomeVersions() throws Exception {

    RequestBuilder request = get("/minerva/api/genomics/taxonomies/{taxonomyId}/genomeTypes/{genomeType}/versions/", "9606",
        ReferenceGenomeType.UCSC.name());

    String response = mockMvc.perform(request)
        .andDo(document("genomics/get_genome_versions",
            pathParameters(
                parameterWithName("taxonomyId").description("organism taxonomy id"),
                parameterWithName("genomeType").description("genome type, acceptable values: "
                    + snippets.getOptionsAsString(ReferenceGenomeType.class))),
            responseFields(
                fieldWithPath("[]")
                    .description("list of genome versions")
                    .type("array"),
                fieldWithPath("[].version")
                    .description("version")
                    .type(JsonFieldType.STRING))))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Object> data = objectMapper.readValue(response, new TypeReference<List<Object>>() {
    });
    assertFalse(data.isEmpty());
  }

  @Test
  public void testDocsGetRemoteUrl() throws Exception {

    RequestBuilder request = get(
        "/minerva/api/genomics/taxonomies/{taxonomyId}/genomeTypes/{genomeType}/versions/{version}:getAvailableRemoteUrls",
        "9606",
        ReferenceGenomeType.UCSC.name(), "hg38");

    String response = mockMvc.perform(request)
        .andDo(document("genomics/get_genome_remote_url",
            pathParameters(
                parameterWithName("taxonomyId").description("organism taxonomy id"),
                parameterWithName("genomeType").description("genome type, acceptable values: "
                    + snippets.getOptionsAsString(ReferenceGenomeType.class)),
                parameterWithName("version").description("genome version")),
            responseFields(
                fieldWithPath("[]")
                    .description("list of urls")
                    .type("array"),
                fieldWithPath("[].url")
                    .description("url")
                    .type(JsonFieldType.STRING))))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Object> data = objectMapper.readValue(response, new TypeReference<List<Object>>() {
    });
    assertFalse(data.isEmpty());
  }

  @Test
  public void testDocsDownloadGenomeDataFromRemote() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(
        Arrays.asList(
            new BasicNameValuePair("organismId", ebolaId.getResource()),
            new BasicNameValuePair("type", ReferenceGenomeType.UCSC.name()),
            new BasicNameValuePair("version", "eboVir3"),
            new BasicNameValuePair("url", "https://minerva-dev.lcsb.uni.lu/eboVir3.2bit"))));

    RequestBuilder request = post("/minerva/api/genomics/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("genomics/download_genome",
            requestParameters(
                parameterWithName("organismId").description("organism taxonomy id"),
                parameterWithName("type").description(
                    "genome type, acceptable values: " + snippets.getOptionsAsString(ReferenceGenomeType.class)),
                parameterWithName("version").description("genome version"),
                parameterWithName("url").description("url address from where the file should be downloaded")),
            responseFields()))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
  }

  @Test
  public void testDocsGetDownloadedGenomes() throws Exception {
    createGenome();

    RequestBuilder request = get("/minerva/api/genomics/");

    mockMvc.perform(request)
        .andDo(document("genomics/get_genomes",
            responseFields(fieldWithPath("[]")
                .description("list of downloaded genomes")
                .type("array")).andWithPrefix("[].", genomeInformationResponseFields())))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
  }

  private ReferenceGenome createGenome() {
    ReferenceGenome genome = new ReferenceGenome();
    genome.setOrganism(ebolaId);
    genome.setSourceUrl(ebolaUrl);
    genome.setDownloadProgress(100.0);
    genome.setType(ReferenceGenomeType.UCSC);
    genome.setVersion("eboVir3");
    ReferenceGenomeGeneMapping mapping = new ReferenceGenomeGeneMapping();
    mapping.setName("custom-gene-mapping");
    mapping.setSourceUrl("https://minerva-dev.lcsb.uni.lu/tmp/refGene.bb");
    genome.addReferenceGenomeGeneMapping(mapping);

    referenceGenomeConnector.add(genome);
    return genome;

  }

  @Test
  public void testDocsGetGenomeInformation() throws Exception {
    createGenome();

    BigFileEntry entry = new BigFileEntry();
    entry.setDownloadProgress(100.0);
    entry.setDownloadDate(Calendar.getInstance());
    entry.setLocalPath("../");
    entry.setUrl(ebolaUrl);
    bigFileCache.add(entry);

    entry.setLocalPath(entry.getLocalPath() + "/" + entry.getId() + "/eboVir3.2bit");
    bigFileCache.update(entry);

    String path = configurationHolder.getDataPath() + entry.getLocalPath();

    File targetFile = new File(path);
    targetFile.getParentFile().mkdirs();
    targetFile.createNewFile();

    RequestBuilder request = get(
        "/minerva/api/genomics/taxonomies/{taxonomyId}/genomeTypes/{genomeType}/versions/{version}/", ebolaId.getResource(),
        ReferenceGenomeType.UCSC.name(), "eboVir3");

    mockMvc.perform(request)
        .andDo(document("genomics/get_genome_information",
            pathParameters(
                parameterWithName("taxonomyId").description("organism taxonomy id"),
                parameterWithName("genomeType").description("genome type, acceptable values: "
                    + snippets.getOptionsAsString(ReferenceGenomeType.class)),
                parameterWithName("version").description("genome version")),
            responseFields(genomeInformationResponseFields())))
        .andExpect(status().is2xxSuccessful());
  }

  private List<FieldDescriptor> genomeInformationResponseFields() {
    return Arrays.asList(fieldWithPath("downloadProgress")
            .description("download progress (0-100%)")
            .type("double"),
        fieldWithPath("geneMapping")
            .description("list of available gene mappings for given genome")
            .type(JsonFieldType.ARRAY),
        fieldWithPath("geneMapping[].name")
            .description("name of the gene mapping")
            .type(JsonFieldType.STRING),
        fieldWithPath("geneMapping[].sourceUrl")
            .description("source url of the gene mapping")
            .type(JsonFieldType.STRING),
        fieldWithPath("geneMapping[].localUrl")
            .description("url with local copy of the mapping")
            .type(JsonFieldType.STRING),
        fieldWithPath("geneMapping[].downloadProgress")
            .description("download progress")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("geneMapping[].idObject")
            .description("id of the gene mapping")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("idObject")
            .description("unique id of genome in minerva")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("localUrl")
            .description("url on minerva where local copy of genome can be accessed")
            .type(JsonFieldType.STRING),
        fieldWithPath("organism")
            .description("organism identifier")
            .type(JsonFieldType.OBJECT),
        fieldWithPath("organism.resource")
            .description("organism identifier")
            .type(JsonFieldType.STRING),
        fieldWithPath("organism.type")
            .description("organism identifier type (usually " + MiriamType.TAXONOMY + ")")
            .type(JsonFieldType.STRING),
        fieldWithPath("organism.annotatorClassName")
            .ignored(),
        fieldWithPath("organism.id")
            .ignored(),
        fieldWithPath("organism.link")
            .ignored(),
        fieldWithPath("sourceUrl")
            .description("genome source url")
            .type(JsonFieldType.STRING),
        fieldWithPath("type")
            .description("type of genome (database from which it was downloaded)")
            .type(JsonFieldType.STRING),
        fieldWithPath("version")
            .description("genome version")
            .type(JsonFieldType.STRING));
  }

  @Test
  public void testDocsDeleteGenomeInformation() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    ReferenceGenome genome = createGenome();

    RequestBuilder request = delete("/minerva/api/genomics/{genomeId}/", genome.getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("genomics/delete_genome_information",
            pathParameters(
                parameterWithName("genomeId")
                    .description("genome id (genomeId is retrieved as idObject described above in 6.3)")),
            responseFields()))
        .andExpect(status().is2xxSuccessful());

  }

  @Test
  public void deleteWithInvalidGenomeId() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/api/genomics/ /").session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());

  }

  @Test
  public void deleteWithNonExistingGenomeId() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/api/genomics/-1/").session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());

  }

  @Test
  public void testDocsAddGeneMappingInformation() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    ReferenceGenome genome = createGenome();
    String body = EntityUtils.toString(new UrlEncodedFormEntity(
        Arrays.asList(
            new BasicNameValuePair("name", "mappingName"),
            new BasicNameValuePair("url", "https://minerva-dev.lcsb.uni.lu/tmp/refGene.bb"))));

    RequestBuilder request = post("/minerva/api/genomics/{genomeId}/geneMapping/", genome.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("genomics/add_gene_mapping",
            pathParameters(
                parameterWithName("genomeId").description("genome id (genomeId is retrieved as idObject described above in 6.3)")),
            requestParameters(parameterWithName("name").description("name of the gene-genome mapping"),
                parameterWithName("url").description("url where file with mapping is located")),
            responseFields()))
        .andExpect(status().is2xxSuccessful());

  }

  @Test
  public void testDocsDeleteGeneMappingInformation() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    ReferenceGenome genome = createGenome();

    ReferenceGenomeGeneMapping mapping = new ReferenceGenomeGeneMapping();
    mapping.setName("x");
    mapping.setSourceUrl("https://minerva-dev.lcsb.uni.lu/tmp/refGene.bb");
    genome.addReferenceGenomeGeneMapping(mapping);
    referenceGenomeConnector.update(genome);

    RequestBuilder request = delete("/minerva/api/genomics/{genomeId}/geneMapping/{geneMappingId}/", genome.getId(),
        genome.getGeneMapping().get(0).getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("genomics/delete_gene_mapping",
            pathParameters(
                parameterWithName("genomeId")
                    .description("genome id (genomeId is retrieved as idObject described above in 6.3)"),
                parameterWithName("geneMappingId")
                    .description("gene genome mapping id (geneMapping[].objectId described in 6.3)"))))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void deleteInvalidGeneMappingInformation() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/api/genomics/ /geneMapping/ /").session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testGetNonExistingGenomeType() throws Exception {
    RequestBuilder request = get(
        "/minerva/api/genomics/taxonomies/{taxonomyId}/genomeTypes/{genomeType}/versions/{version}/",
        "9606", "type", "ver");

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testGetNonExistingGenome2() throws Exception {
    RequestBuilder request = get(
        "/minerva/api/genomics/taxonomies/{taxonomyId}/genomeTypes/{genomeType}/versions/{version}/",
        "960000", "UCSC", "ver");

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDownloadGenomeWithInvalidData() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(
        Arrays.asList(
            new BasicNameValuePair("organismId", ""),
            new BasicNameValuePair("type", ReferenceGenomeType.UCSC.name()),
            new BasicNameValuePair("version", "eboVir3"),
            new BasicNameValuePair("url", "https://minerva-dev.lcsb.uni.lu/eboVir3.2bit"))));

    RequestBuilder request = post("/minerva/api/genomics/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest())
        .andReturn().getResponse().getContentAsString();
  }

  @Test
  public void addNotExistingUrlGeneMapping() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    ReferenceGenome genome = createGenome();
    String body = EntityUtils.toString(new UrlEncodedFormEntity(
        Arrays.asList(
            new BasicNameValuePair("name", "mappingName"),
            new BasicNameValuePair("url", "https://minerva-dev.lcsb.uni.lu/not.existing.bb"))));

    RequestBuilder request = post("/minerva/api/genomics/{genomeId}/geneMapping/", genome.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());

  }

  @Test
  public void addInvalidUrlGeneMapping() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    ReferenceGenome genome = createGenome();
    String body = EntityUtils.toString(new UrlEncodedFormEntity(
        Arrays.asList(
            new BasicNameValuePair("name", "mappingName"),
            new BasicNameValuePair("url", "https://asdasdsa.bb"))));

    RequestBuilder request = post("/minerva/api/genomics/{genomeId}/geneMapping/", genome.getId())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());

  }

}
