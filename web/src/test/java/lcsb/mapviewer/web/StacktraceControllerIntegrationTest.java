package lcsb.mapviewer.web;

import static org.junit.Assert.assertEquals;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import com.fasterxml.jackson.core.type.TypeReference;

import lcsb.mapviewer.model.Stacktrace;
import lcsb.mapviewer.services.interfaces.IStacktraceService;

@RunWith(SpringJUnit4ClassRunner.class)
public class StacktraceControllerIntegrationTest extends ControllerIntegrationTest {

  @Autowired
  private IStacktraceService stacktraceService;

  @Test
  public void testGetStacktrace() throws Exception {
    Stacktrace st = stacktraceService.createFromStackTrace(new Exception());
    RequestBuilder request = get("/minerva/api/stacktrace/{id}", st.getId());

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("stacktraces/get",
            responseFields(
                fieldWithPath("id")
                    .description("error id")
                    .type(JsonFieldType.STRING),
                fieldWithPath("content")
                    .description("stacktrace")
                    .type(JsonFieldType.STRING),
                fieldWithPath("createdAt")
                    .description("timestamp")
                    .type(JsonFieldType.STRING))))
        .andReturn().getResponse().getContentAsString();

    Map<String, Object> result = objectMapper.readValue(response, new TypeReference<HashMap<String, Object>>() {
    });

    assertEquals("admin should see all projects", st.getId(), result.get("id"));
  }

  @Test
  public void testGetNotExistingStacktrace() throws Exception {
    RequestBuilder request = get("/minerva/api/stacktrace/{id}", "blah");

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

}
