package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.services.interfaces.IFileService;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class FileControllerIntegrationTest extends ControllerIntegrationTest {

  @Autowired
  private IUserService userService;

  @Autowired
  private IFileService fileService;

  @Autowired
  private ObjectMapper objectMapper;

  @Before
  public void setup() {
  }

  @Test
  public void testDocsCanCreateAndUploadFile() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("filename", "test_file"),
        new BasicNameValuePair("length", "999999"))));

    RequestBuilder request = post("/minerva/api/files/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    String response = mockMvc.perform(request)
        .andDo(document("file/create_file",
            requestParameters(
                parameterWithName("filename")
                    .description("original name of the file"),
                parameterWithName("length")
                    .description("length of the file (in bytes)")),
            responseFields(fileResponseFields())))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    Map<String, Object> result = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });

    String fileId = result.get("id").toString();

    body = "test_content\nhello world";

    request = post("/minerva/api/files/{fileId}:uploadContent", fileId)
        .contentType(MediaType.APPLICATION_OCTET_STREAM_VALUE)
        .header("post-filename", "input_file.txt")
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("file/upload_content",
            pathParameters(parameterWithName("fileId")
                .description("file id")),
            requestParameters(),
            responseFields(fileResponseFields())))
        .andExpect(status().is2xxSuccessful());

    UploadedFileEntry file = fileService.getById(Integer.valueOf(fileId));
    assertNotNull(file);
    assertEquals(body, new String(file.getFileContent()));
  }

  @Test
  public void tryToAppendToFileWithoutOwner() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    UploadedFileEntry file = new UploadedFileEntry();
    file.setFileContent(new byte[]{});
    file.setLength(99999);
    fileService.add(file);

    try {
      String fileId = String.valueOf(file.getId());

      String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("id", fileId),
          new BasicNameValuePair("data", "test_content"))));

      RequestBuilder request = post("/minerva/api/files/" + fileId + ":uploadContent")
          .content(body)
          .session(session);

      mockMvc.perform(request)
          .andExpect(status().isForbidden());
    } finally {
      fileService.delete(file);
    }
  }

  @Test
  public void testDocsGetInfoAboutFile() throws Exception {
    UploadedFileEntry file = createFile("Hello world",
        userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    try {
      MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

      RequestBuilder request = get("/minerva/api/files/{fileId}", file.getId() + "")
          .session(session);

      mockMvc.perform(request)
          .andDo(document("file/get_file",
              pathParameters(parameterWithName("fileId")
                  .description("file id")),
              responseFields(fileResponseFields())))
          .andExpect(status().is2xxSuccessful());
    } finally {
      fileService.delete(file);
    }
  }

  private List<FieldDescriptor> fileResponseFields() {
    return Arrays.asList(
        fieldWithPath("id")
            .description("unique id in the system")
            .type("number"),
        fieldWithPath("filename")
            .description("original name of the file")
            .type("number"),
        fieldWithPath("length")
            .description("file length")
            .type("number"),
        fieldWithPath("owner")
            .description("login of the file owner")
            .type("number"),
        fieldWithPath("uploadedDataLength")
            .description("total number of uploaded bytes")
            .type("number"));
  }

  @Test
  public void appendToNonExistingFile() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("id", "-1"),
        new BasicNameValuePair("data", "test_content"))));

    RequestBuilder request = post("/minerva/api/files/-1:uploadContent")
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void createAndAppendToExistingFile() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("filename", "unknown.txt"),
        new BasicNameValuePair("length", "29"))));

    RequestBuilder request = post("/minerva/api/files/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful()).andReturn().getResponse().getContentAsString();

    Map<String, Object> data = objectMapper.readValue(response,
        new TypeReference<Map<String, Object>>() {
        });

    assertEquals("unknown.txt", data.get("filename"));
    assertEquals(29, data.get("length"));
    assertEquals(BUILT_IN_TEST_ADMIN_LOGIN, data.get("owner"));
    assertEquals(0, data.get("uploadedDataLength"));

    int id = (int) data.get("id");

    String fileContent = "elementIdentifier\tvalue\nxx\t-1";

    RequestBuilder postContentRequest = post("/minerva/api/files/" + id + ":uploadContent")
        .content(fileContent)
        .session(session);

    mockMvc.perform(postContentRequest)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void createFileWithoutPrivileges() throws Exception {
    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("filename", "unknown.txt"),
        new BasicNameValuePair("length", "29"))));

    RequestBuilder request = post("/minerva/api/files/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void createFileWitInvalidLength() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("filename", "test_file"),
        new BasicNameValuePair("length", "-1"))));

    RequestBuilder request = post("/minerva/api/files/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

}
