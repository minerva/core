package lcsb.mapviewer.web.serialization;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.layout.ProjectBackgroundImageLayer;
import lcsb.mapviewer.model.map.layout.ProjectBackgroundStatus;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.modelutils.serializer.model.user.UserAsLoginSerializer;

public abstract class ProjectBackgroundUpdateMixIn {
  @JsonIgnore
  private Project project;

  @JsonIgnore
  private ProjectBackgroundStatus status;

  @JsonIgnore
  private double progress;

  @JsonIgnore
  public abstract Project getProject();

  @JsonIgnore
  public abstract ProjectBackgroundStatus getStatus();

  @JsonIgnore
  public abstract double getProgress();

  @JsonIgnore
  public abstract List<ProjectBackgroundImageLayer> getProjectBackgroundImageLayer();

  @JsonSerialize(using = UserAsLoginSerializer.class)
  public abstract User getCreator();
}
