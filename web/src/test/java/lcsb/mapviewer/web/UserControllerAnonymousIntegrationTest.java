package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import javax.servlet.http.Cookie;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserControllerAnonymousIntegrationTest extends ControllerIntegrationTest {

  @Autowired
  private IUserService userService;

  @Before
  public void setup() {
  }

  @Test
  public void grantPrivilegeForAnonymousUser() throws Exception {
    try {
      MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

      String body = "{\"privileges\":{\"IS_ADMIN\":true}}";

      RequestBuilder request = patch("/minerva/api/users/" + Configuration.ANONYMOUS_LOGIN + ":updatePrivileges")
          .contentType(MediaType.APPLICATION_JSON)
          .content(body)
          .session(session);

      mockMvc.perform(request)
          .andExpect(status().is2xxSuccessful());

      RequestBuilder anonymousRequest = get("/minerva/api/users/");
      mockMvc.perform(anonymousRequest)
          .andExpect(status().is2xxSuccessful());
    } finally {
      userService.revokeUserPrivilege(userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN),
          PrivilegeType.IS_ADMIN);
    }
  }

  @Test
  public void loginAsAnonymousUser() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = "{\"user\":{\"password\":\"test\"}}";

    RequestBuilder request = patch("/minerva/api/users/{login}", Configuration.ANONYMOUS_LOGIN)
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    RequestBuilder loginRequest = post("/minerva/api/doLogin")
        .param("login", Configuration.ANONYMOUS_LOGIN)
        .param("password", "test");
    mockMvc.perform(loginRequest)
        .andExpect(status().is4xxClientError());

  }

  @Test
  public void removeAnonymousUser() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = delete("/minerva/api/users/" + Configuration.ANONYMOUS_LOGIN)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testDocsCheckIsAuthenticatedAdmin() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/users/isSessionValid")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("authentication/isSessionValid",
            responseFields(
                fieldWithPath("login")
                    .description("user login of the session owner"),
                fieldWithPath("token")
                    .description("authentication token")
            )))
        .andReturn().getResponse().getContentAsString();

    Map<String, Object> result = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });

    assertEquals(BUILT_IN_TEST_ADMIN_LOGIN, result.get("login"));
  }

  @Test
  public void checkIsAuthenticatedAnonymous() throws Exception {
    RequestBuilder request = get("/minerva/api/users/isSessionValid")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    mockMvc.perform(request)
        .andExpect(status().is4xxClientError());
  }

  @Test
  public void testDocsLogin() throws Exception {
    RequestBuilder request = post("/minerva/api/doLogin")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content("login=" + BUILT_IN_TEST_ADMIN_LOGIN + "&password=" + BUILT_IN_TEST_ADMIN_PASSWORD);

    mockMvc.perform(request)
        .andDo(document("authentication/login", requestParameters(parameterWithName("login").description("login"),
                parameterWithName("password").description("password")),
            responseFields(
                fieldWithPath("info")
                    .description("status message"),
                fieldWithPath("login")
                    .description("user login"),
                fieldWithPath("token")
                    .description("session token"))))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDocsLogout() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = post("/minerva/api/doLogout")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .cookie(new Cookie(Configuration.AUTH_TOKEN, "xxxxxxxx"))
        .session(session);

    mockMvc.perform(request)
        .andDo(document("authentication/logout",
            responseFields(fieldWithPath("status").description("status message"))))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void getAnonymousPreferences() throws Exception {
    RequestBuilder request = get("/minerva/api/users/{login}?columns=preferences", Configuration.ANONYMOUS_LOGIN);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    Map<String, Object> result = objectMapper.readValue(content, new TypeReference<Map<String, Object>>() {
    });

    assertNotNull(result.get("preferences"));
  }

}
