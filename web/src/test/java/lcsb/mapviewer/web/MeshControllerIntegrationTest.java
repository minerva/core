package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class MeshControllerIntegrationTest extends ControllerIntegrationTest {

  @Autowired
  private ObjectMapper objectMapper;

  @Before
  public void setup() {
  }

  @After
  public void tearDown() {
  }

  @Test
  public void testDocsGetMesh() throws Exception {

    RequestBuilder request = get("/minerva/api/mesh/{meshId}", "D010300");

    String response = mockMvc.perform(request)
        .andDo(document("mesh/get_mesh",
            pathParameters(parameterWithName("meshId").description("mesh id")),
            responseFields(
                fieldWithPath("name")
                    .description("name")
                    .type(JsonFieldType.STRING),
                fieldWithPath("description")
                    .description("description")
                    .type(JsonFieldType.STRING),
                fieldWithPath("synonyms")
                    .description("list of synonyms")
                    .type("array"),
                fieldWithPath("id")
                    .description("mesh id")
                    .type(JsonFieldType.STRING))))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    Map<String, Object> result = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });
    String name = (String) result.get("name");
    assertTrue(name.toLowerCase().contains("parkinson"));

  }

}
