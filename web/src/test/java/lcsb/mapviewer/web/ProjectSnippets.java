package lcsb.mapviewer.web;

import lcsb.mapviewer.common.Pair;
import lcsb.mapviewer.converter.Converter;
import lcsb.mapviewer.converter.graphics.AbstractImageGenerator;
import lcsb.mapviewer.converter.graphics.ImageGenerators;
import lcsb.mapviewer.model.ProjectStatus;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.modelutils.map.ElementUtils;
import org.apache.commons.lang3.StringUtils;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;

@Component
public class ProjectSnippets {

  @Autowired
  private List<Converter> converters;

  public <T extends Enum<T>> String getOptionsAsString(final Class<T> enumType) {
    List<String> options = new ArrayList<>();
    for (final T c : enumType.getEnumConstants()) {
      options.add(c.name());
    }
    Collections.sort(options);
    return StringUtils.join(options, ", ");
  }

  public ResponseFieldsSnippet getProjectSnippet() {
    return responseFields(
        fieldWithPath("projectId")
            .description("project identifier")
            .type(JsonFieldType.STRING),
        fieldWithPath("name")
            .description("name")
            .type(JsonFieldType.STRING),
        fieldWithPath("sharedInMinervaNet")
            .description("is the project shared in minerva net")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath("defaultProject")
            .description("true if the project is defined as default in configuration")
            .type(JsonFieldType.BOOLEAN),
        fieldWithPath("version")
            .description("version")
            .type(JsonFieldType.STRING),
        fieldWithPath("owner")
            .description("login of the project creator")
            .type(JsonFieldType.STRING),
        fieldWithPath("creationDate")
            .description("when project was uploaded")
            .type(JsonFieldType.STRING),
        subsectionWithPath("disease")
            .description("identifier of the disease")
            .type("Reference")
            .optional(),
        subsectionWithPath("organism")
            .description("identifier of the organism")
            .type("Reference")
            .optional(),
        fieldWithPath("idObject")
            .type(JsonFieldType.NUMBER)
            .ignored(),
        fieldWithPath("directory")
            .description(
                "directory where files related to the project are located. "
                    + "Whole path looks like: /minerva/map_images/{directory}")
            .type(JsonFieldType.STRING),
        fieldWithPath("status")
            .description(
                "status of the uploaded project; possible values: " + getOptionsAsString(ProjectStatus.class))
            .type(JsonFieldType.STRING),
        fieldWithPath("progress")
            .description("how much current stage progressed in percent")
            .type(JsonFieldType.NUMBER),
        fieldWithPath("notifyEmail")
            .description("email address connected to the project")
            .type(JsonFieldType.STRING),
        fieldWithPath("logEntries")
            .description("flag indicating that there are log entries attached to the project")
            .type(JsonFieldType.BOOLEAN),
        subsectionWithPath("license")
            .description("license of this project")
            .type("License")
            .optional(),
        fieldWithPath("customLicenseName")
            .description(
                "if license is not provided this is the custom license name provided by user")
            .type(JsonFieldType.STRING),
        fieldWithPath("customLicenseUrl")
            .description(
                "if license is not provided this is the custom license url provided by user")
            .type(JsonFieldType.STRING),
        subsectionWithPath("overviewImageViews")
            .description("list of overview images")
            .type("array<OverviewImage>")
            .optional(),
        subsectionWithPath("topOverviewImage")
            .description("overview image that should be used as top level image")
            .type("OverviewImage")
            .optional());
  }

  public String getParsers() {
    List<String> options = new ArrayList<>();
    for (final Converter c : converters) {
      options.add(c.getClass().getCanonicalName());
    }
    Collections.sort(options);
    return StringUtils.join(options, ", ");
  }

  public String getImageConverters() {
    List<String> options = new ArrayList<>();
    ImageGenerators imageGenerators = new ImageGenerators();
    List<Pair<String, Class<? extends AbstractImageGenerator>>> imageGeneratorList = imageGenerators
        .getAvailableImageGenerators();

    for (Pair<String, Class<? extends AbstractImageGenerator>> element : imageGeneratorList) {
      options.add(element.getRight().getCanonicalName());
    }
    return StringUtils.join(options, ", ");
  }

  public List<String> getElementStringTypes() {
    Set<String> types = new HashSet<>();
    for (final Class<? extends Element> clazz : new ElementUtils().getAvailableElementSubclasses()) {
      types.add(Mockito.mock(clazz, CALLS_REAL_METHODS).getStringType());
    }
    List<String> result = new ArrayList<>(types);
    Collections.sort(result);
    return result;
  }

}
