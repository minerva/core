package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.converter.graphics.PngImageGenerator;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.request.RequestParametersSnippet;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class MapWithGlyphControllerIntegrationTest extends ControllerIntegrationTest {

  private User anonymous;

  private Project project;

  private ModelData map;

  @Autowired
  private IUserService userService;

  @Autowired
  private ProjectSnippets projectSnippets;

  @Before
  public void setup() throws Exception {
    anonymous = userService.getUserByLogin(Configuration.ANONYMOUS_LOGIN);
    project = createProjectWithGlyph(TEST_PROJECT);
    map = project.getTopModel().getModelData();
  }

  @After
  public void tearDown() throws Exception {
    removeProject(project);
  }

  @Test
  public void testDownloadImageWithGlyphs() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    // fetch the map (but don't do anything with glyphs)
    RequestBuilder request2 = get("/minerva/api/projects/" + TEST_PROJECT + "/models/")
        .session(session);

    mockMvc.perform(request2)
        .andExpect(status().is2xxSuccessful());

    RequestBuilder request = get("/minerva/api/projects/" + TEST_PROJECT + "/models/" + map.getId() + ":downloadImage?"
        + "handlerClass=" + PngImageGenerator.class.getCanonicalName())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDocsGetMapsWithForProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/", BUILT_IN_PROJECT).session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("projects/project_maps/list",
            pathParameters(parameterWithName("projectId").description("project identifier")),
            responseFields(subsectionWithPath("[]").description("list of maps").type("array<Map>"))));
  }

  @Test
  public void testDocsGetAllElements() throws Exception {
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/bioEntities/elements/", TEST_PROJECT, "*");

    String response = mockMvc.perform(request)
        .andDo(document("projects/project_maps/get_elements",
            pathParameters(parameterWithName("projectId").description("project identifier"),
                parameterWithName("mapId").description("map identifier")),
            new MapControllerIntegrationTest().getAllElementsFilter(),
            new MapControllerIntegrationTest().listOfElementsResponseFields()))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertTrue("user should be able to access elements", result.size() > 0);
    assertFalse(response.contains("Unknown column"));
  }

  @Test
  public void testDocsGetMapById() throws Exception {
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}", TEST_PROJECT, map.getId());

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("projects/project_maps/get_by_id",
            pathParameters(parameterWithName("projectId").description("project identifier"),
                parameterWithName("mapId").description("map identifier")),
            new MapControllerIntegrationTest().getMapDescription()))
        .andReturn().getResponse().getContentAsString();

    Map<String, Object> result = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });

    int mapId = (int) result.get("idObject");

    assertEquals(map.getId(), mapId);
  }

  @Test
  public void testGetMapsByAsterisk() throws Exception {
    userService.grantUserPrivilege(anonymous, PrivilegeType.READ_PROJECT, project.getProjectId());

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/*/", TEST_PROJECT);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
  }

  @Test
  public void testDocsDownloadModelByElementList() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}:downloadModel?"
        + "handlerClass=" + CellDesignerXmlParser.class.getCanonicalName()
        + "&elementIds=" + map.getElements().iterator().next().getId(), TEST_PROJECT, map.getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_maps/download_with_element_list",
            downloadModelRequestParameters(),
            pathParameters(parameterWithName("projectId").description("project identifier"),
                parameterWithName("mapId").description("map identifier"))))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDocsDownloadModel() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}:downloadModel?"
        + "handlerClass=" + CellDesignerXmlParser.class.getCanonicalName(), TEST_PROJECT, map.getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_maps/download_model_simple",
            downloadModelRequestParameters(),
            pathParameters(parameterWithName("projectId").description("project identifier"),
                parameterWithName("mapId").description("map identifier"))))
        .andExpect(status().is2xxSuccessful());
  }

  private RequestParametersSnippet downloadModelRequestParameters() {
    return requestParameters(
        parameterWithName("handlerClass").description("class preparing model file. Available options: "
            + projectSnippets.getParsers()),
        parameterWithName("polygonString").description("polygon defining part of the model to be downloaded")
            .optional(),
        parameterWithName("strictCutoff").description("must the reactions be inside polygon (default: true)")
            .optional(),
        parameterWithName("elementIds").description("list of element ids that should be included in the output")
            .optional(),
        parameterWithName("reactionIds").description("list of reaction ids that should be included in the output")
            .optional());
  }

  @Test
  public void testDocsDownloadModelByPolygon() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}:downloadModel?"
        + "handlerClass=" + CellDesignerXmlParser.class.getCanonicalName()
        + "&polygonString=0,0;100,0;0,100&strictCutoff=false", TEST_PROJECT, map.getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_maps/download_from_polygon",
            downloadModelRequestParameters(),
            getMapPathParameters()))
        .andExpect(status().is2xxSuccessful());
  }
}
