package lcsb.mapviewer.web.bean.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectLogEntry;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.ControllerIntegrationTest;

@RunWith(SpringJUnit4ClassRunner.class)
public class StartupBeanTest extends ControllerIntegrationTest {

  @Autowired
  private StartupBean startupBean;

  @Autowired
  private IProjectService projectService;

  @Autowired
  private IUserService userService;

  private Project project;

  @Before
  public void setup() {
    project = new Project(TEST_PROJECT_ID);
    project.setOwner(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    projectService.add(project);
  }

  @After
  public void tearDown() throws Exception {
    removeProject(project);
  }

  @Test
  public void testInit() {
    ProjectLogEntry entry = new ProjectLogEntry();
    entry.setSeverity("WARN");
    entry.setType(ProjectLogEntryType.OTHER);
    entry.setProject(project);
    projectService.addLogEntry(project, entry);
    startupBean.init();
    assertEquals(0, super.getErrors().size());
  }

  @Test
  public void testAssistiveTechnologies() {
    Properties props = System.getProperties();
    assertNotEquals(props.getProperty("javax.accessibility.assistive_technologies"), "org.GNOME.Accessibility.AtkWrapper");
  }

}
