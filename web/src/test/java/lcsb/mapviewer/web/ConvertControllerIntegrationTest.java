package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.common.MimeType;
import lcsb.mapviewer.converter.ConverterParams;
import lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser;
import lcsb.mapviewer.model.map.BioEntity;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Model;
import lcsb.mapviewer.model.map.model.ModelFullIndexed;
import lcsb.mapviewer.model.map.species.Element;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;

import java.awt.geom.Rectangle2D;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class ConvertControllerIntegrationTest extends ControllerIntegrationTest {

  @Autowired
  private ObjectMapper objectMapper;

  @Test
  public void testConvertInvalidFile() throws Exception {
    String body = "invalid content";
    RequestBuilder request = post("/minerva/api/convert/image/SBGN-ML:svg")
        .contentType(MediaType.TEXT_PLAIN_VALUE)
        .content(body);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void convertWithBOM() throws Exception {
    FileReader fr = new FileReader(new File("src/test/resources/SBGN_with_BOM.sbgn"));
    BufferedReader br = new BufferedReader(fr);
    StringBuilder tmp = new StringBuilder();
    String line;
    while ((line = br.readLine()) != null) {
      tmp.append(line + System.getProperty("line.separator"));
    }
    RequestBuilder request = post("/minerva/api/convert/image/SBGN-ML:svg")
        .content(tmp.toString().getBytes(StandardCharsets.UTF_8)).characterEncoding("UTF-8");

    br.close();
    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testConvertInvalidMapFormat() throws Exception {
    String body = "invalid content";
    RequestBuilder request = post("/minerva/api/convert/image/unknown:svg")
        .contentType(MediaType.TEXT_PLAIN_VALUE)
        .content(body);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testDocsMergeCellDesignerMaps() throws Exception {
    byte[] body = Files.readAllBytes(new File("src/test/resources/convert/cd-maps.zip").toPath());
    RequestBuilder request = post("/minerva/api/convert/merge/{inputFormat}:{outputFormat}", "CellDesigner_SBML",
        "CellDesigner_SBML")
        .header("post-filename", "maps.zip")
        .content(body);

    byte[] content = mockMvc.perform(request)
        .andDo(document("converter/merge_maps",
            pathParameters(
                parameterWithName("inputFormat")
                    .description("input format, available options: " + getModelInputConverters()),
                parameterWithName("outputFormat")
                    .description("output format, available options: " + getModelOutputConverters()))))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsByteArray();

    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().inputStream(new ByteArrayInputStream(content)));

    int s1 = 0;
    int s2 = 0;
    int s3 = 0;

    for (final Element element : model.getElements()) {
      if (element.getName().equals("s1")) {
        s1++;
      }
      if (element.getName().equals("s2")) {
        s2++;
      }
      if (element.getName().equals("s3")) {
        s3++;
      }
    }
    assertEquals(1, s1);
    assertEquals(2, s2);
    assertEquals(1, s3);
  }

  @Test
  public void testMergeMapsWithZipResult() throws Exception {
    byte[] body = Files.readAllBytes(new File("src/test/resources/convert/cd-maps.zip").toPath());
    RequestBuilder request = post("/minerva/api/convert/merge/{inputFormat}:{outputFormat}", "CellDesigner_SBML", "CellDesigner_SBML")
        .header("post-filename", "maps.zip")
        .accept(MimeType.ZIP.getTextRepresentation())
        .content(body);

    MvcResult result = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn();

    assertTrue(result.getResponse().getHeader("Content-Disposition").endsWith(".zip"));

    byte[] content = result.getResponse().getContentAsByteArray();

    ZipInputStream zipStream = new ZipInputStream(new ByteArrayInputStream(content));
    ZipEntry entry = null;
    int entryCount = 0;
    while ((entry = zipStream.getNextEntry()) != null) {
      logger.debug("entry: " + entry);
      entryCount++;
    }
    zipStream.close();

    assertEquals(1, entryCount);
  }

  @Test
  public void testDocsListAvailableFormats() throws Exception {
    RequestBuilder request = get("/minerva/api/convert/");

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("converter/list_formats",
            responseFields(
                fieldWithPath("inputs")
                    .description("list of available input formats")
                    .type("array"),
                fieldWithPath("inputs[].available_names")
                    .description("list of names that could be used for specific format")
                    .type("array<string>"),
                fieldWithPath("outputs")
                    .description("list of available input formats")
                    .type("array"),
                fieldWithPath("outputs[].available_names")
                    .description("list of names that could be used for specific format")
                    .type("array<string>"))));
  }

  @Test
  public void testDocsConvertCellDesignerToGpml() throws Exception {
    File file = new File("src/test/resources/convert/cd-sample.xml");
    String content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
    RequestBuilder request = post("/minerva/api/convert/{inputFormat}:{outputFormat}", "CellDesigner_SBML", "GPML")
        .header("post-filename", "input_file.xml")
        .contentType(MediaType.TEXT_PLAIN_VALUE)
        .content(content);

    mockMvc.perform(request)
        .andDo(document("converter/format_conversion",
            pathParameters(
                parameterWithName("inputFormat")
                    .description("input format, available options: " + getModelInputConverters()),
                parameterWithName("outputFormat")
                    .description("output format, available options: " + getModelOutputConverters()))))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDocsListAvailableImageFormats() throws Exception {
    RequestBuilder request = get("/minerva/api/convert/image/");

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("converter/list_image_formats",
            responseFields(
                fieldWithPath("inputs")
                    .description("list of available input formats")
                    .type("array"),
                fieldWithPath("inputs[].available_names")
                    .description("list of names that could be used for specific format")
                    .type("array<string>"),
                fieldWithPath("outputs")
                    .description("list of available input formats")
                    .type("array"),
                fieldWithPath("outputs[].available_names")
                    .description("list of names that could be used for specific format")
                    .type("array<string>"))));
  }

  @Test
  public void testDocsConvertCellDesignerToSvg() throws Exception {
    File file = new File("src/test/resources/convert/cd-sample.xml");
    String content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
    RequestBuilder request = post("/minerva/api/convert/image/{inputFormat}:{outputFormat}", "CellDesigner_SBML", "png")
        .header("post-filename", "input_file.xml")
        .content(content.getBytes(StandardCharsets.UTF_8))
        .characterEncoding("UTF-8");

    mockMvc.perform(request)
        .andDo(document("converter/image_conversion",
            pathParameters(
                parameterWithName("inputFormat")
                    .description("input format, available options: " + getModelInputConverters()),
                parameterWithName("outputFormat")
                    .description("output format, available options: " + getImageOutputConverters()))))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testCelDesigner2Sbml1() throws Exception {
    String content = readFile("src/test/resources/convert/sample-cd.xml");
    String result = convertToModel(content, "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser",
        "lcsb.mapviewer.converter.model.sbml.SbmlParser");
    assertTrue(result.length() > 0);
    assertTrue(result.contains("</layout:speciesReferenceGlyph>"));
  }

  @Test
  public void testCellDesignerToSbmlToSvg() throws Exception {
    String content = readFile("src/test/resources/convert/neuron_cd.xml");
    String resultSbml = convertToModel(content, "CellDesigner_SBML", "SBML");
    String resultSvg = convertToImage(resultSbml, "SBML", "svg");

    assertTrue(resultSvg.contains("<rect"));
  }

  @Test
  public void testCellDesignerToSbmlToSvg2() throws Exception {
    String content = readFile("src/test/resources/convert/glycolysis2.xml");
    String resultSbml = convertToModel(content, "CellDesigner_SBML", "SBML");
    String resultSvg = convertToImage(resultSbml, "SBML", "svg");

    assertTrue(resultSvg.contains("<rect"));
  }

  @Test
  public void convertCaseInsensitive() throws Exception {
    String content = readFile("src/test/resources/convert/glycolysis2.xml");
    convertToImage(content, "CellDesigner_SBML", "PNG");
  }

  @Test
  public void testCellDesignerToSbmlToCellDesigner() throws Exception {
    String content = readFile("src/test/resources/convert/glycolysis2.xml");
    String resultSbml = convertToModel(content, "CellDesigner_SBML", "SBML");
    String resultCellDesigner = convertToModel(resultSbml, "SBML", "CellDesigner_SBML");

    assertTrue(resultCellDesigner.contains("rect"));
  }

  @Test
  public void testCellDesigner2Sbml2CellDesignerSvg() throws Exception {
    String content = readFile("src/test/resources/convert/neuron_cd.xml");
    String resultSbml = convertToModel(content, "CellDesigner_SBML", "SBML");
    String resultCellDesignerSbml = convertToModel(resultSbml, "SBML", "CellDesigner_SBML");
    String resultSvg = convertToImage(resultCellDesignerSbml, "CellDesigner_SBML", "svg");

    assertTrue(resultSvg.contains("<rect"));
  }

  @Test
  public void testCelDesigner2Sbgn() throws Exception {
    String content = readFile("src/test/resources/convert/sample-cd.xml");
    String result = convertToModel(content, "CellDesigner_SBML", "SBGN-ML");
    assertTrue(result.contains("glyph class=\"complex\""));
  }

  private void test2All(final String content, final String converter) throws Exception {

    List<String> modelConverters = getModelOutputConverters();

    for (final String target : modelConverters) {
      String result = convertToModel(content, converter, target);
      assertTrue(result.length() > 0);

      result = convertToModel(content, converter, target.toLowerCase());
      assertTrue(result.length() > 0);
    }

    List<String> imageConverters = getImageOutputConverters();
    for (final String target : imageConverters) {
      String result = convertToImage(content, converter, target);
      assertTrue(result.length() > 0);

      result = convertToImage(content, converter, target.toLowerCase());
      assertTrue(result.length() > 0);
    }
  }

  private String convertToImage(final String content, final String converter, final String target)
      throws Exception {
    RequestBuilder request = post("/minerva/api/convert/image/{inputFormat}:{outputFormat}", converter, target)
        .header("post-filename", "input_file.xml")
        .content(content.getBytes(StandardCharsets.UTF_8))
        .characterEncoding("UTF-8");
    String result = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    return result;
  }

  private String convertToModel(final String content, final String converter, final String target)
      throws Exception {
    RequestBuilder request = post("/minerva/api/convert/{inputFormat}:{outputFormat}", converter, target)
        .header("post-filename", "input_file.xml")
        .content(content.getBytes(StandardCharsets.UTF_8))
        .characterEncoding("UTF-8");
    String result = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
    return result;
  }

  private List<String> getModelOutputConverters() throws Exception {

    Map<String, List<Map<String, List<String>>>> data = getModelConverters();

    List<String> result = new ArrayList<>();
    for (Map<String, List<String>> entry : data.get("outputs")) {
      result.add(entry.get("available_names").get(0));
    }

    return result;
  }

  private List<String> getModelInputConverters() throws Exception {

    Map<String, List<Map<String, List<String>>>> data = getModelConverters();

    List<String> result = new ArrayList<>();
    for (Map<String, List<String>> entry : data.get("inputs")) {
      result.add(entry.get("available_names").get(0));
    }

    return result;
  }

  private Map<String, List<Map<String, List<String>>>> getModelConverters()
      throws Exception {
    RequestBuilder request = get("/minerva/api/convert/");

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    Map<String, List<Map<String, List<String>>>> data = objectMapper.readValue(response,
        new TypeReference<Map<String, List<Map<String, List<String>>>>>() {
        });
    return data;
  }

  private List<String> getImageOutputConverters() throws Exception {
    List<String> result = new ArrayList<>();

    Map<String, List<Map<String, List<String>>>> data = getImageConverters();

    for (Map<String, List<String>> entry : data.get("outputs")) {
      result.add(entry.get("available_names").get(0));
    }

    return result;
  }

  private Map<String, List<Map<String, List<String>>>> getImageConverters()
      throws Exception {
    RequestBuilder request = get("/minerva/api/convert/image/");

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    Map<String, List<Map<String, List<String>>>> data = objectMapper.readValue(response,
        new TypeReference<Map<String, List<Map<String, List<String>>>>>() {
        });
    return data;
  }

  @Test
  public void testCelDesigner2All() throws Exception {
    String content = readFile("src/test/resources/convert/sample-cd.xml");
    test2All(content, "CellDesigner_SBML");
  }

  @Test
  public void testSbml2All() throws Exception {
    String content = readFile("src/test/resources/convert/sample-sbml.xml");
    test2All(content, "SBML");
  }

  @Test
  public void testSbgn2All() throws Exception {
    String content = readFile("src/test/resources/convert/sample.sbgn");
    test2All(content, "SBGN-ML");
  }

  @Test
  public void testNewtSbgn2All() throws Exception {
    String content = readFile("src/test/resources/convert/newt.sbgn");
    test2All(content, "SBGN-ML");
  }

  @Test
  public void testCelDesigner2Svg() throws Exception {
    String content = readFile("src/test/resources/convert/sample-cd.xml");
    String result = convertToImage(content, "CellDesigner_SBML", "svg");

    assertTrue(result.contains("<rect"));
  }

  @Test
  public void testGetImageInformation() throws Exception {
    List<String> outputs = getImageOutputConverters();
    assertTrue(outputs.size() > 0);

    List<String> inputs = getModelInputConverters();
    assertTrue(inputs.size() > 0);
  }

  @Test
  public void testGetInformation() throws Exception {
    List<String> outputs = getModelOutputConverters();
    assertTrue(outputs.size() > 0);

    List<String> inputs = getModelInputConverters();
    assertTrue(inputs.size() > 0);
  }

  @Test
  public void testInvalidGpml2Sbml() throws Exception {
    String content = "blah";

    RequestBuilder request = post("/minerva/api/convert/{inputFormat}:{outputFormat}", "GPML", "SBML")
        .header("post-filename", "input_file.xml")
        .content(content.getBytes(StandardCharsets.UTF_8))
        .characterEncoding("UTF-8");
    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testInvalidGpml2Svg() throws Exception {
    String content = "blah";

    RequestBuilder request = post("/minerva/api/convert/image/{inputFormat}:{outputFormat}", "GPML", "svg")
        .header("post-filename", "input_file.xml")
        .content(content.getBytes(StandardCharsets.UTF_8))
        .characterEncoding("UTF-8");
    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void convertCellDesignerToGpmlUsingUrlEncoded() throws Exception {
    File file = new File("src/test/resources/convert/cd-sample.xml");
    String content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
    content = URLEncoder.encode(content, "UTF-8");
    RequestBuilder request = post("/minerva/api/convert/{inputFormat}:{outputFormat}", "CellDesigner_SBML", "GPML")
        .header("post-filename", "input_file.xml")
        .content(content.getBytes(StandardCharsets.UTF_8))
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .characterEncoding("UTF-8");

    mockMvc.perform(request)
        .andExpect(status().isUnsupportedMediaType());
  }

  @Test
  public void testDocsMergeCellDesignerWithTextArea() throws Exception {
    Model model = new ModelFullIndexed(null);
    Layer layer = createLayer();
    LayerText text = new LayerText(new Rectangle2D.Double(10, 20, 30, 40), "test text");
    model.addLayer(layer);
    layer.addLayerText(text);
    model.setWidth(300);
    model.setHeight(400);
    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    String xml = parser.model2String(model);

    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    try (ZipOutputStream zos = new ZipOutputStream(baos)) {
      ZipEntry entry = new ZipEntry("maps/map.xml");

      zos.putNextEntry(entry);
      zos.write(xml.getBytes());
      zos.closeEntry();
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }

    byte[] body = baos.toByteArray();
    RequestBuilder request = post("/minerva/api/convert/merge/{inputFormat}:{outputFormat}", "CellDesigner_SBML",
        "CellDesigner_SBML")
        .header("post-filename", "maps.zip")
        .content(body);

    byte[] content = mockMvc.perform(request)
        .andDo(document("converter/merge_maps",
            pathParameters(
                parameterWithName("inputFormat")
                    .description("input format, available options: " + getModelInputConverters()),
                parameterWithName("outputFormat")
                    .description("output format, available options: " + getModelOutputConverters()))))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsByteArray();

    Model outputMap = parser.createModel(new ConverterParams().inputStream(new ByteArrayInputStream(content)));

    assertEquals(1, outputMap.getLayers().size());
    assertEquals(1, outputMap.getLayers().iterator().next().getTexts().size());

  }

  @Test
  public void testPrunNotesByDefault() throws Exception {
    byte[] body = Files.readAllBytes(new File("src/test/resources/convert/cd-maps-with-notes.zip").toPath());
    RequestBuilder request = post("/minerva/api/convert/merge/{inputFormat}:{outputFormat}", "CellDesigner_SBML",
        "CellDesigner_SBML")
        .header("post-filename", "maps.zip")
        .content(body);

    byte[] content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsByteArray();

    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().inputStream(new ByteArrayInputStream(content)));
    for (BioEntity bioEntity : model.getBioEntities()) {
      assertEquals("Notes should be empty by default. Found: " + bioEntity.getNotes(), "", bioEntity.getNotes());
    }
  }

  @Test
  public void testDocsPreserveNotesWithCustomHeader() throws Exception {
    byte[] body = Files.readAllBytes(new File("src/test/resources/convert/cd-maps-with-notes.zip").toPath());
    RequestBuilder request = post("/minerva/api/convert/merge/{inputFormat}:{outputFormat}", "CellDesigner_SBML",
        "CellDesigner_SBML")
        .header("post-filename", "maps.zip")
        .header("X-MINERVA-KEEP-NOTES", "true")
        .content(body);

    byte[] content = mockMvc.perform(request)
        .andDo(document("converter/merge_maps_keep_notes",
            pathParameters(
                parameterWithName("inputFormat")
                    .description("input format, available options: " + getModelInputConverters()),
                parameterWithName("outputFormat")
                    .description("output format, available options: " + getModelOutputConverters()))))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsByteArray();

    CellDesignerXmlParser parser = new CellDesignerXmlParser();
    Model model = parser.createModel(new ConverterParams().inputStream(new ByteArrayInputStream(content)));
    boolean notesFound = false;
    for (BioEntity bioEntity : model.getBioEntities()) {
      notesFound |= !bioEntity.getNotes().isEmpty();
    }
    assertTrue("All notes were cleared, but should not", notesFound);
  }

}
