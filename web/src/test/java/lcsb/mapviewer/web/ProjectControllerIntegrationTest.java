package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.common.Configuration;
import lcsb.mapviewer.common.MinervaConfigurationHolder;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectStatus;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.layout.ProjectBackground;
import lcsb.mapviewer.model.map.layout.ProjectBackgroundStatus;
import lcsb.mapviewer.model.security.Privilege;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IProjectBackgroundService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.serialization.ProjectBackgroundUpdateMixIn;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.request.PathParametersSnippet;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.patch;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class ProjectControllerIntegrationTest extends ControllerIntegrationTest {

  private static final String CURATOR_PASSWORD = "curator_pass";
  private static final String CURATOR_LOGIN = "curator_user";

  private static final String USER_PASSWORD = "user_pass";
  private static final String USER_LOGIN = "test_user";

  private static final String TEST_PROJECT_2 = "test_project2";

  @Autowired
  private IUserService userService;

  @Autowired
  private IProjectService projectService;

  @Autowired
  private IProjectBackgroundService projectBackgroundService;

  @Autowired
  private ProjectSnippets snippets;

  @Autowired
  private ObjectMapper objectMapper;

  private User curator;

  @Autowired
  private IConfigurationService configurationService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private MinervaConfigurationHolder minervaConfigurationHolder;

  @Before
  public void setup() {
    curator = createCurator(CURATOR_LOGIN, CURATOR_PASSWORD);
    configurationService.setConfigurationValue(ConfigurationElementType.MINERVA_ROOT, "https://pdmap.uni.lu/minerva/");
  }

  @After
  public void tearDown() throws Exception {
    removeProject(TEST_PROJECT);
    removeProject(TEST_PROJECT_2);
    removeUser(curator);
    removeUser(userService.getUserByLogin(USER_LOGIN));
    configurationService.setConfigurationValue(ConfigurationElementType.MINERVA_ROOT, "");
  }

  @Test
  public void testDocsGetAllProjectsAsAdmin() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final RequestBuilder request = get("/minerva/api/projects/")
        .session(session);

    final Project project = projectService.getProjectByProjectId(BUILT_IN_PROJECT);
    project.setDirectory(project.getDirectory().replaceAll("[a-z]", "0"));
    projectService.update(project);

    final String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("projects/project_data/list",
            responseFields(
                subsectionWithPath("[]")
                    .description("list of projects").type(JsonFieldType.ARRAY))))
        .andReturn().getResponse().getContentAsString();

    final List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals("admin should see all projects", projectService.getAllProjects().size(), result.size());
  }

  @Test
  public void testDocsGetSourceFile() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    createAndPersistProject(TEST_PROJECT);
    final RequestBuilder request = get("/minerva/api/projects/{projectId}:downloadSource", TEST_PROJECT)
        .session(session);
    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("projects/project_data/get_source_file",
            projectPathParameters()));

  }

  @Test
  public void testGetLimitedProjectsAsCurator() throws Exception {
    final Project project = createAndPersistProject(TEST_PROJECT);

    userService.grantUserPrivilege(curator, PrivilegeType.READ_PROJECT, project.getProjectId());
    final Project project2 = new Project(TEST_PROJECT_2);
    project2.setOwner(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    projectService.add(project2);

    final MockHttpSession session = createSession(CURATOR_LOGIN, CURATOR_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/")
        .session(session);

    final String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    final List<Map<String, Object>> result = objectMapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {
    });

    assertEquals("curator should see only limited list of projects", 1, result.size());
  }

  @Test
  public void testDocsGetLimitedProjectsAsGuest() throws Exception {
    final Project project2 = new Project(TEST_PROJECT_2);
    project2.setOwner(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    projectService.add(project2);

    final RequestBuilder request = get("/minerva/api/projects/");

    final String response = mockMvc.perform(request)
        .andDo(document("projects/project_data/list_guest",
            responseFields(
                subsectionWithPath("[]")
                    .description("list of projects").type(JsonFieldType.ARRAY))))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    final List<Object> result = objectMapper.readValue(response, new TypeReference<List<Object>>() {
    });

    assertEquals("guest should see only limited list of projects", 1, result.size());
  }

  @Test
  public void testDocsUserPrivilegesChangeDuringActiveSession() throws Exception {
    final MockHttpSession session = createSession(CURATOR_LOGIN, CURATOR_PASSWORD);

    final Project project = createAndPersistProject(TEST_PROJECT);

    userService.grantUserPrivilege(curator, PrivilegeType.READ_PROJECT, project.getProjectId());

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/", TEST_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_data/get_project",
            projectPathParameters(),
            snippets.getProjectSnippet()))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDocsGetLogsForProject() throws Exception {
    final Project project = createAndPersistProject(TEST_PROJECT);

    userService.grantUserPrivilege(curator, PrivilegeType.READ_PROJECT, project.getProjectId());

    final MockHttpSession session = createSession(CURATOR_LOGIN, CURATOR_PASSWORD);

    final RequestBuilder request = get(
        "/minerva/api/projects/{projectId}/logs/?start=0&length=5&level=warning&sortColumn=id&sortOrder=asc&search=age",
        TEST_PROJECT).session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_data/get_project_logs",
            requestParameters(
                parameterWithName("start").description("number of first entry in this response").optional(),
                parameterWithName("sortColumn")
                    .description("log entry column that should be used for sorting (default: id)").optional(),
                parameterWithName("sortOrder").description("log entry sort order (asc, desc)").optional(),
                parameterWithName("level").description("level of log entry (warning, error)").optional(),
                parameterWithName("length").description("number of log entries we want to obtain").optional(),
                parameterWithName("search").description("search query used for filtering").optional()),
            projectPathParameters(),
            responseFields(
                subsectionWithPath("data")
                    .description("list of log entries")
                    .type(JsonFieldType.ARRAY),
                fieldWithPath("filteredSize")
                    .description("number of entries that match filter criteria")
                    .type(JsonFieldType.NUMBER),
                fieldWithPath("length")
                    .description("number of entries in this response")
                    .type(JsonFieldType.NUMBER),
                fieldWithPath("start")
                    .description("number of first entry in this response")
                    .type(JsonFieldType.NUMBER),
                fieldWithPath("totalSize")
                    .description("number of all log entries")
                    .type(JsonFieldType.NUMBER))))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDocsGetStatisticsForProject() throws Exception {
    createAndPersistProject(TEST_PROJECT);
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/statistics", TEST_PROJECT).session(session);

    mockMvc.perform(request).andDo(document("projects/project_data/get_project_statistics",
            projectPathParameters(),
            responseFields(
                subsectionWithPath("reactionAnnotations")
                    .description("list of reaction annotation types")
                    .type(JsonFieldType.OBJECT),
                subsectionWithPath("elementAnnotations")
                    .description("list of element annotation types")
                    .type(JsonFieldType.OBJECT),
                fieldWithPath("publications")
                    .description("number of publications included in the project")
                    .type(JsonFieldType.NUMBER))))
        .andExpect(status().is2xxSuccessful());
    assertEquals(0, getErrors().size());
  }

  @Test
  public void testDocsGrantPrivilege() throws Exception {
    createAndPersistProject(TEST_PROJECT);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String body = "[{"
        + "\"privilegeType\":\"" + PrivilegeType.READ_PROJECT + "\", "
        + "\"login\":\"" + CURATOR_LOGIN + "\""
        + "}]";

    final RequestBuilder request = patch("/minerva/api/projects/{projectId}:grantPrivileges", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_privileges/grant_privilege",
            projectPathParameters(),
            requestFields(
                fieldWithPath("[]")
                    .description("list of privileges to grant")
                    .type(JsonFieldType.ARRAY),
                fieldWithPath("[].privilegeType")
                    .description("type of privilege (READ_PROJECT/WRITE_PROJECT)")
                    .type(JsonFieldType.STRING),
                fieldWithPath("[].login")
                    .description("user login who should gain access")
                    .type(JsonFieldType.STRING))))
        .andExpect(status().is2xxSuccessful());

    assertTrue(userService.getUserByLogin(CURATOR_LOGIN, true).getPrivileges()
        .contains(new Privilege(PrivilegeType.READ_PROJECT, TEST_PROJECT)));
  }

  @Test
  public void testDocsRevokePrivilege() throws Exception {
    final Project project = createAndPersistProject(TEST_PROJECT);
    userService.grantUserPrivilege(curator, PrivilegeType.READ_PROJECT, project.getProjectId());

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String body = "[{"
        + "\"privilegeType\":\"" + PrivilegeType.READ_PROJECT + "\", "
        + "\"login\":\"" + CURATOR_LOGIN + "\""
        + "}]";

    final RequestBuilder request = patch("/minerva/api/projects/{projectId}:revokePrivileges", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_privileges/revoke_privilege",
            projectPathParameters(),
            requestFields(
                fieldWithPath("[]")
                    .description("list of privileges to revoke")
                    .type(JsonFieldType.ARRAY),
                fieldWithPath("[].privilegeType")
                    .description("type of privilege (READ_PROJECT/WRITE_PROJECT)")
                    .type(JsonFieldType.STRING),
                fieldWithPath("[].login")
                    .description("user login who should lose access")
                    .type(JsonFieldType.STRING))))
        .andExpect(status().is2xxSuccessful());

    assertFalse(curator.getPrivileges().contains(new Privilege(PrivilegeType.READ_PROJECT, TEST_PROJECT)));
  }

  @Test
  public void testGetNonExistingProject() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/*/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDocsUpdateProject() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    createAndPersistProject(TEST_PROJECT);

    final String content = "{\"project\":{"
        + "\"version\":\"xxx\", "
        + "\"name\": \"new name\", "
        + "\"notifyEmail\": \"notify@uni.lu\", "
        + "\"organism\": {\"type\": \"TAXONOMY\", \"resource\": \"9605\"}, "
        + "\"disease\": {\"type\": \"MESH_2012\", \"resource\": \"D010300\"}, "
        + "\"license\": {\"id\": 1} "
        + "}}";

    final RequestBuilder request = patch("/minerva/api/projects/{projectId}/", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(content)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_data/update",
            projectPathParameters(),
            requestFields(
                fieldWithPath("project.version")
                    .description("version of the project")
                    .type(JsonFieldType.STRING)
                    .optional(),
                fieldWithPath("project.name")
                    .description("name of the project")
                    .type(JsonFieldType.STRING)
                    .optional(),
                fieldWithPath("project.license.id")
                    .description("license id")
                    .type(JsonFieldType.NUMBER)
                    .optional(),
                fieldWithPath("project.notifyEmail")
                    .description("email address that should be when something change in the project")
                    .type(JsonFieldType.STRING)
                    .optional(),
                subsectionWithPath("project.organism")
                    .description("organism associated with the project")
                    .type(JsonFieldType.OBJECT)
                    .optional(),
                subsectionWithPath("project.disease")
                    .description("disease associated with the project")
                    .type(JsonFieldType.OBJECT)
                    .optional()),
            snippets.getProjectSnippet()))

        .andExpect(status().is2xxSuccessful());

    minervaJobService.waitForTasksToFinish();
    assertEquals("xxx", projectService.getProjectByProjectId(TEST_PROJECT).getVersion());
    assertEquals("9605", projectService.getProjectByProjectId(TEST_PROJECT).getOrganism().getResource());
    assertEquals(MiriamType.TAXONOMY, projectService.getProjectByProjectId(TEST_PROJECT).getOrganism().getDataType());
    minervaJobService.waitForTasksToFinish();
    assertNotNull(projectService.getProjectByProjectId(TEST_PROJECT).getOrganism().getLink());
    assertNotNull(projectService.getProjectByProjectId(TEST_PROJECT).getDisease().getLink());
    assertEquals(1, projectService.getProjectByProjectId(TEST_PROJECT).getLicense().getId());
  }

  @Test
  public void testUpdateEmptyLicenseProject() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    createAndPersistProject(TEST_PROJECT);

    final String content = "{\"project\":{"
        + "\"version\":\"xxx\", "
        + "\"name\": \"new name\", "
        + "\"notifyEmail\": \"notify@uni.lu\", "
        + "\"organism\": {\"type\": \"TAXONOMY\", \"resource\": \"9605\"}, "
        + "\"disease\": {\"type\": \"MESH_2012\", \"resource\": \"D010300\"}, "
        + "\"license\": {} "
        + "}}";

    final RequestBuilder request = patch("/minerva/api/projects/{projectId}/", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(content)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertNull(projectService.getProjectByProjectId(TEST_PROJECT).getLicense());
  }

  @Test
  public void testUpdateWithEmptyDiseaseProject() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    createAndPersistProject(TEST_PROJECT);

    final String content = "{\"project\":{"
        + "\"version\":\"xxx\", "
        + "\"name\": \"new name\", "
        + "\"notifyEmail\": \"notify@uni.lu\", "
        + "\"organism\": {}, "
        + "\"disease\": {} "
        + "}}";

    final RequestBuilder request = patch("/minerva/api/projects/{projectId}/", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(content)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertNull(projectService.getProjectByProjectId(TEST_PROJECT).getOrganism());
    assertNull(projectService.getProjectByProjectId(TEST_PROJECT).getDisease());

  }

  @Test
  public void testUpdateProjectWithTooLongVersion() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    createAndPersistProject(TEST_PROJECT);

    final String content = "{\"project\":{\"version\":\"12345678901234567890123456\"}}";

    final RequestBuilder request = patch("/minerva/api/projects/" + TEST_PROJECT + "/")
        .contentType(MediaType.APPLICATION_JSON)
        .content(content)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testUpdateProjectWithUndefinedProjectId() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String content = "{\"project\":{\"version\":\"xxx\"}}";

    final RequestBuilder request = patch("/minerva/api/projects/*/")
        .contentType(MediaType.APPLICATION_JSON)
        .content(content)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGrantPrivilegeForUndefinedProject() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String body = "[{"
        + "\"privilegeType\":\"" + PrivilegeType.READ_PROJECT + "\", "
        + "\"login\":\"" + CURATOR_LOGIN + "\""
        + "}]";

    final RequestBuilder request = patch("/minerva/api/projects/*:grantPrivileges")
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testRevokePrivilegeForUndefinedProject() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String body = "[{"
        + "\"privilegeType\":\"" + PrivilegeType.READ_PROJECT + "\", "
        + "\"login\":\"" + CURATOR_LOGIN + "\""
        + "}]";

    final RequestBuilder request = patch("/minerva/api/projects/*:revokePrivileges")
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testRemoveProjectForUndefinedProjectId() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = delete("/minerva/api/projects/*/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  @SuppressWarnings("unchecked")
  public void testGetStatisticsForUndefinedProjectId() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/*/statistics")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDownloadSourceForUndefinedProjectId() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/*:downloadSource")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetLogsForUndefinedProjectId() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/*/logs/")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testGetSubmapConnectionsForUndefinedProjectId() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/*/submapConnections/").session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDocsGetSubmapConnectionsForProject() throws Exception {
    createAndPersistProject(TEST_PROJECT);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/submapConnections/", TEST_PROJECT).session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("projects/project_maps/get_connections",
            projectPathParameters(),
            responseFields(
                fieldWithPath("[]").description("list of connections").type(JsonFieldType.ARRAY),
                fieldWithPath("[].from.id").description("anchor bioEntity id").type(JsonFieldType.NUMBER),
                fieldWithPath("[].from.modelId").description("anchor map id").type(JsonFieldType.NUMBER),
                fieldWithPath("[].from.type").description("anchor type").type(JsonFieldType.STRING),
                fieldWithPath("[].to.modelId").description("destination map id").type(JsonFieldType.NUMBER))));
  }

  @Test
  public void addProjectWithInvalidFileId() throws Exception {
    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("file-id", "-1"),
        new BasicNameValuePair("parser",
            "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

    final RequestBuilder request = post("/minerva/api/projects/" + TEST_PROJECT)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD));

    mockMvc.perform(request).andExpect(status().isBadRequest());
  }

  @Test
  public void addInvalidProject() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    final UploadedFileEntry fileEntry = createFile(
        new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), StandardCharsets.UTF_8),
        admin);
    try {

      final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
          new BasicNameValuePair("parser",
              "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

      final RequestBuilder request = post("/minerva/api/projects/{projectId}", INVALID_LONG_NAME)
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD));

      mockMvc.perform(request).andExpect(status().isBadRequest());

    } finally {
      removeFile(fileEntry);
    }
  }

  @Test
  public void addProjectWithTooLongName() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    final UploadedFileEntry fileEntry = createFile(
        new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), StandardCharsets.UTF_8),
        admin);
    try {
      final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
          new BasicNameValuePair("name", INVALID_LONG_NAME),
          new BasicNameValuePair("parser", "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

      final RequestBuilder request = post("/minerva/api/projects/" + TEST_PROJECT)
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD));

      mockMvc.perform(request).andExpect(status().isBadRequest());

    } finally {
      removeFile(fileEntry);
    }
  }

  @Test
  public void addProjectWithTooLongVersion() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    final UploadedFileEntry fileEntry = createFile(
        new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), StandardCharsets.UTF_8),
        admin);
    try {
      final String invalidVersion = "12345678901234567890123456";

      final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
          new BasicNameValuePair("version", invalidVersion),
          new BasicNameValuePair("parser",
              "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

      final RequestBuilder request = post("/minerva/api/projects/" + TEST_PROJECT)
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD));

      mockMvc.perform(request).andExpect(status().isBadRequest());

    } finally {
      removeFile(fileEntry);
    }
  }

  @Test
  public void modifyProjectWithTooLongName() throws Exception {
    createAndPersistProject(TEST_PROJECT);

    final String content = "{\"project\":{\"name\":\"" + INVALID_LONG_NAME + "\"}}";

    final RequestBuilder request = patch("/minerva/api/projects/{projectId}/", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(content)
        .session(createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD));

    mockMvc.perform(request).andExpect(status().isBadRequest());
  }

  @Test(timeout = 10000)
  public void testDocsRemoveProjectWithUsedFile() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    try {
      createFullProject(admin);

      final RequestBuilder request = delete("/minerva/api/projects/{projectId}/", TEST_PROJECT_2)
          .session(createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD));

      mockMvc.perform(request).andExpect(status().is2xxSuccessful())
          .andDo(document("projects/project_data/delete",
              projectPathParameters(),
              snippets.getProjectSnippet()));

    } finally {
      waitForProjectToFinishLoading(TEST_PROJECT);
    }
    final Project p = projectService.getProjectByProjectId(TEST_PROJECT);
    assertEquals(ProjectStatus.DONE, p.getStatus());
  }

  private void createFullProject(final User admin)
      throws Exception {
    final UploadedFileEntry fileEntry = createFile(
        new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), StandardCharsets.UTF_8),
        admin);

    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
        new BasicNameValuePair("name", "Project name"),
        new BasicNameValuePair("version", "0.0.1"),
        new BasicNameValuePair("notify-email", "minerva@uni.lu"),
        new BasicNameValuePair("custom-license-name", "my awesome license"),
        new BasicNameValuePair("custom-license-url", "https://my.awesome.lincese.org/"),
        new BasicNameValuePair("parser",
            "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

    RequestBuilder request = post("/minerva/api/projects/" + TEST_PROJECT)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD));
    mockMvc.perform(request).andExpect(status().is2xxSuccessful());

    request = post("/minerva/api/projects/" + TEST_PROJECT_2)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD));
    mockMvc.perform(request).andExpect(status().is2xxSuccessful());

    waitForProjectToFinishLoading(TEST_PROJECT_2);

    final Project project = projectService.getProjectByProjectId(TEST_PROJECT_2);
    project.setDirectory(project.getDirectory().replaceAll("[a-z]", "0"));
    projectService.update(project);
  }

  @Test
  public void addComplexProjectWithInvalidOverlayName() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    final UploadedFileEntry fileEntry = createFile(
        Files.readAllBytes(Paths.get("./src/test/resources/complex_model_with_layouts.zip")), admin);
    try {
      final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
          new BasicNameValuePair("parser", "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"),

          new BasicNameValuePair("zip-entries[0][_type]", "MAP"),
          new BasicNameValuePair("zip-entries[0][_filename]", "main.xml"),
          new BasicNameValuePair("zip-entries[0][_data][root]", "true"),
          new BasicNameValuePair("zip-entries[0][_data][name]", "s1"),
          new BasicNameValuePair("zip-entries[0][_data][type][id]", "UNKNOWN"),
          new BasicNameValuePair("zip-entries[0][_data][type][name]", "Unknown"),

          new BasicNameValuePair("zip-entries[1][_type]", "OVERLAY"),
          new BasicNameValuePair("zip-entries[1][_filename]", "layouts/goodSchema.txt"),
          new BasicNameValuePair("zip-entries[1][_data][name]", "")

      )));

      final RequestBuilder request = post("/minerva/api/projects/" + TEST_PROJECT)
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD));

      mockMvc.perform(request).andExpect(status().isBadRequest());
    } finally {
      removeFile(fileEntry);
    }
  }

  @Test
  public void addComplexProject() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    final UploadedFileEntry fileEntry = createFile(
        Files.readAllBytes(Paths.get("./src/test/resources/complex_model_with_layouts.zip")), admin);
    try {
      final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
          new BasicNameValuePair("parser", "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"),

          new BasicNameValuePair("zip-entries[0][_type]", "MAP"),
          new BasicNameValuePair("zip-entries[0][_filename]", "main.xml"),
          new BasicNameValuePair("zip-entries[0][_data][root]", "true"),
          new BasicNameValuePair("zip-entries[0][_data][name]", "s1"),
          new BasicNameValuePair("zip-entries[0][_data][type][id]", "UNKNOWN"),
          new BasicNameValuePair("zip-entries[0][_data][type][name]", "Unknown"),

          new BasicNameValuePair("zip-entries[1][_type]", "OVERLAY"),
          new BasicNameValuePair("zip-entries[1][_filename]", "layouts/goodSchema.txt"),
          new BasicNameValuePair("zip-entries[1][_data][name]", "test-o")

      )));

      final RequestBuilder request = post("/minerva/api/projects/" + TEST_PROJECT)
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD));

      mockMvc.perform(request).andExpect(status().is2xxSuccessful());
    } finally {
      waitForProjectToFinishLoading(TEST_PROJECT);
    }
  }

  @Test
  public void addComplexProjectWithInvalidOverlay() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    final UploadedFileEntry fileEntry = createFile(
        Files.readAllBytes(Paths.get("./src/test/resources/complex_model_with_invalid_layouts.zip")), admin);
    try {
      final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
          new BasicNameValuePair("parser", "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"),

          new BasicNameValuePair("zip-entries[0][_type]", "MAP"),
          new BasicNameValuePair("zip-entries[0][_filename]", "main.xml"),
          new BasicNameValuePair("zip-entries[0][_data][root]", "true"),
          new BasicNameValuePair("zip-entries[0][_data][name]", "s1"),
          new BasicNameValuePair("zip-entries[0][_data][type][id]", "UNKNOWN"),
          new BasicNameValuePair("zip-entries[0][_data][type][name]", "Unknown"),

          new BasicNameValuePair("zip-entries[1][_type]", "OVERLAY"),
          new BasicNameValuePair("zip-entries[1][_filename]", "layouts/badSchema.txt"),
          new BasicNameValuePair("zip-entries[1][_data][name]", "test-o")

      )));

      final RequestBuilder request = post("/minerva/api/projects/" + TEST_PROJECT)
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD));

      mockMvc.perform(request).andExpect(status().is2xxSuccessful());
      waitForProjectToFinishLoading(TEST_PROJECT);

      final Project project = projectService.getProjectByProjectId(TEST_PROJECT);
      assertEquals(ProjectStatus.FAIL, project.getStatus());

    } finally {
      waitForProjectToFinishLoading(TEST_PROJECT);
    }
  }

  @Test
  public void testDocsGetBackgrounds() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    createAndPersistProject(TEST_PROJECT);
    final RequestBuilder request = get("/minerva/api/projects/{projectId}/backgrounds/", TEST_PROJECT)
        .session(session);
    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("projects/project_backgrounds/get_all",
            projectPathParameters(),
            responseFields().andWithPrefix("[].", getBackgroundFields())));
  }

  private List<FieldDescriptor> getUpdateBackgroundFields() {
    return Arrays.asList(
        fieldWithPath("id")
            .type(JsonFieldType.NUMBER)
            .description("identifier"),
        fieldWithPath("name")
            .type(JsonFieldType.STRING)
            .description("name"),
        fieldWithPath("description")
            .type(JsonFieldType.STRING)
            .description("description"),
        fieldWithPath("creator")
            .type(JsonFieldType.STRING)
            .description("who created background"),
        fieldWithPath("order")
            .type(JsonFieldType.NUMBER)
            .description("order used when listing all backgrounds"),
        fieldWithPath("defaultOverlay")
            .type(JsonFieldType.BOOLEAN)
            .description(
                "should the background be used as default (at most one per project should be marked with true)"));
  }

  private List<FieldDescriptor> getBackgroundFields() {
    return Arrays.asList(
        fieldWithPath("id")
            .type(JsonFieldType.NUMBER)
            .description("identifier"),
        fieldWithPath("name")
            .type(JsonFieldType.STRING)
            .description("name"),
        fieldWithPath("description")
            .type(JsonFieldType.STRING)
            .optional()
            .description("description"),
        fieldWithPath("project.projectId")
            .type(JsonFieldType.STRING)
            .description("project id where this background belongs to"),
        fieldWithPath("creator.login")
            .type(JsonFieldType.STRING)
            .description("who created background"),
        fieldWithPath("status")
            .type(JsonFieldType.STRING)
            .description("is the background ready. Available statuses are: "
                + snippets.getOptionsAsString(ProjectBackgroundStatus.class)),
        fieldWithPath("progress")
            .type(JsonFieldType.NUMBER)
            .description("generating images progress information (in %)"),
        fieldWithPath("order")
            .type(JsonFieldType.NUMBER)
            .description("order used when listing all backgrounds"),
        fieldWithPath("images[].id")
            .type(JsonFieldType.NUMBER)
            .ignored(),
        fieldWithPath("images[].projectBackground.id")
            .type(JsonFieldType.NUMBER)
            .ignored(),
        fieldWithPath("images[].path")
            .type(JsonFieldType.STRING)
            .description("directory where background tiles are located"),
        fieldWithPath("images[].model.id")
            .type(JsonFieldType.NUMBER)
            .description("(sub)map for which images are described"),
        fieldWithPath("defaultOverlay")
            .type(JsonFieldType.BOOLEAN)
            .description(
                "should the background be used as default (at most one per project should be marked with true)"));
  }

  @Test
  public void testDocsGetBackgroundById() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final Project project = createAndPersistProject(TEST_PROJECT);
    final RequestBuilder request = get("/minerva/api/projects/{projectId}/backgrounds/{backgroundId}", TEST_PROJECT,
        project.getProjectBackgrounds().get(0).getId())
        .session(session);
    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("projects/project_backgrounds/get_by_id",
            backgroundPathParameters(),
            responseFields(getBackgroundFields())));
  }

  @Test
  public void testDocsUpdateBackgroundById() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final Project project = createAndPersistProject(TEST_PROJECT);
    final ProjectBackground background = new ProjectBackground("weird_title");
    background.setId(project.getProjectBackgrounds().get(0).getId());
    background.setCreator(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    background.setDescription("new description");

    final ObjectMapper mapper = new ObjectMapper();
    mapper.addMixIn(ProjectBackground.class, ProjectBackgroundUpdateMixIn.class);

    final RequestBuilder request = patch("/minerva/api/projects/{projectId}/backgrounds/{backgroundId}", TEST_PROJECT,
        project.getProjectBackgrounds().get(0).getId())
        .contentType(MediaType.APPLICATION_JSON)
        .session(session)
        .content(mapper.writeValueAsString(background));

    final String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("projects/project_backgrounds/update_background",
            backgroundPathParameters(),
            requestFields(getUpdateBackgroundFields()),
            responseFields(getBackgroundFields())))
        .andReturn().getResponse().getContentAsString();

    final ProjectBackground newBackground = objectMapper.readValue(response, new TypeReference<ProjectBackground>() {
    });
    assertEquals(background.getName(), newBackground.getName());
  }

  @Test
  public void testDocsRemoveBackgroundById() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    final UploadedFileEntry fileEntry = createFile(
        new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), StandardCharsets.UTF_8),
        admin);

    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
        new BasicNameValuePair("parser",
            "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

    RequestBuilder request = post("/minerva/api/projects/" + TEST_PROJECT)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);
    mockMvc.perform(request).andExpect(status().is2xxSuccessful());

    waitForProjectToFinishLoading(TEST_PROJECT);

    final Project project = projectService.getProjectByProjectId(TEST_PROJECT);
    final List<ProjectBackground> backgrounds = projectBackgroundService.getProjectBackgroundsByProject(project);
    final ProjectBackground background = backgrounds.get(0);
    final String path = minervaConfigurationHolder.getDataPath() + "/map_images/" + project.getDirectory() + "/"
        + background.getProjectBackgroundImageLayer().iterator().next().getDirectory();
    assertTrue(new File(path).exists());

    request = delete("/minerva/api/projects/{projectId}/backgrounds/{backgroundId}", TEST_PROJECT,
        backgrounds.get(0).getId())
        .session(session);
    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("projects/project_backgrounds/delete_background",
            backgroundPathParameters(),
            responseFields()));
    assertFalse(new File(path).exists());

    final int backgroundCount = projectBackgroundService.getProjectBackgroundsByProject(project).size();
    assertEquals(backgrounds.size() - 1, backgroundCount);
  }

  private PathParametersSnippet backgroundPathParameters() {
    return projectPathParameters().and(parameterWithName("backgroundId").description("background identifier"));
  }

  private void waitForProjectToFinishRemoving(final String projectId) throws InterruptedException {
    while (projectService.projectExists(projectId)) {
      Thread.sleep(1);
    }
  }

  @Test(timeout = 10000)
  public void checkDefaultPrivilegesAfterAddingProject() throws Exception {
    final User user = createCurator(USER_LOGIN, USER_PASSWORD);
    grantPrivilegeInSeparateThread(user, PrivilegeType.READ_PROJECT.name() + ":*");
    final UploadedFileEntry fileEntry = createFile(
        new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), StandardCharsets.UTF_8),
        curator);

    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
        new BasicNameValuePair("parser",
            "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

    final RequestBuilder request = post("/minerva/api/projects/" + TEST_PROJECT)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(createSession(CURATOR_LOGIN, CURATOR_PASSWORD));

    mockMvc.perform(request).andExpect(status().is2xxSuccessful());

    waitForProjectToFinishLoading(TEST_PROJECT);

    final User u = userService.getUserByLogin(USER_LOGIN, true);
    final User c = userService.getUserByLogin(CURATOR_LOGIN, true);
    assertTrue("User privileges weren't updated after project was created",
        u.getPrivileges().contains(new Privilege(PrivilegeType.READ_PROJECT, TEST_PROJECT)));
    assertTrue("Curator privileges weren't updated after project was created",
        c.getPrivileges().contains(new Privilege(PrivilegeType.WRITE_PROJECT, TEST_PROJECT)));
    assertTrue("Curator privileges weren't updated after project was created",
        c.getPrivileges().contains(new Privilege(PrivilegeType.READ_PROJECT, TEST_PROJECT)));
  }

  @Test(timeout = 10000)
  public void checkDropPrivilegesAfterRemovingProject() throws Exception {
    createAndPersistProject(TEST_PROJECT);
    grantPrivilegeInSeparateThread(curator, PrivilegeType.WRITE_PROJECT.name() + ":" + TEST_PROJECT);
    final RequestBuilder request = delete("/minerva/api/projects/" + TEST_PROJECT)
        .session(createSession(CURATOR_LOGIN, CURATOR_PASSWORD));

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    waitForProjectToFinishRemoving(TEST_PROJECT);

    final User c = userService.getUserByLogin(CURATOR_LOGIN, true);

    assertFalse("Curator privileges weren't updated after project was removed",
        c.getPrivileges().contains(new Privilege(PrivilegeType.WRITE_PROJECT, TEST_PROJECT)));
  }

  private void grantPrivilegeInSeparateThread(final User user, final String string) throws Exception {
    userService.grantUserPrivilege(user, PrivilegeType.valueOf(string.split(":")[0]), string.split(":")[1]);
  }

  @Test(timeout = 10000)
  public void addProjectWithInvalidId() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    final UploadedFileEntry fileEntry = createFile(
        new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), StandardCharsets.UTF_8),
        admin);

    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
        new BasicNameValuePair("parser",
            "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

    final RequestBuilder request = post("/minerva/api/projects/*")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD));

    mockMvc.perform(request).andExpect(status().is4xxClientError());
    assertNull(projectService.getProjectByProjectId("*"));
  }

  @Test(timeout = 10000)
  public void addProjectWithoutPrivileges() throws Exception {
    final User user = createUser(USER_LOGIN, USER_PASSWORD);
    final UploadedFileEntry fileEntry = createFile(
        new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), StandardCharsets.UTF_8),
        user);

    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
        new BasicNameValuePair("parser",
            "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

    final RequestBuilder request = post("/minerva/api/projects/" + TEST_PROJECT)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(createSession(USER_LOGIN, USER_PASSWORD));

    mockMvc.perform(request).andExpect(status().isForbidden());
    assertNull(projectService.getProjectByProjectId(TEST_PROJECT));
  }

  @Test
  public void testUserPrivilegeChangeDuringUpload() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);

    final Project defaultProject = createAndPersistProject(TEST_PROJECT_2);
    grantPrivilegeInSeparateThread(admin, PrivilegeType.READ_PROJECT.name() + ":" + defaultProject.getProjectId());
    grantPrivilegeInSeparateThread(admin, PrivilegeType.WRITE_PROJECT.name() + ":" + defaultProject.getProjectId());

    final UploadedFileEntry fileEntry = createFile(
        new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), StandardCharsets.UTF_8),
        curator);

    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
        new BasicNameValuePair("parser",
            "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

    final RequestBuilder request = post("/minerva/api/projects/" + TEST_PROJECT)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(createSession(CURATOR_LOGIN, CURATOR_PASSWORD));

    mockMvc.perform(request).andExpect(status().is2xxSuccessful());
    waitForProjectToFinishLoading(TEST_PROJECT);

    userService.revokeObjectDomainPrivilegesForAllUsers(PrivilegeType.READ_PROJECT,
        defaultProject.getProjectId());

    final Project project = projectService.getProjectByProjectId(TEST_PROJECT);
    assertEquals(ProjectStatus.DONE, project.getStatus());
  }

  @Test
  public void createWithoutFileId() throws Exception {
    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("projectId", TEST_PROJECT),
        new BasicNameValuePair("name", "New Disease Map"),
        new BasicNameValuePair("parser", "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

    final RequestBuilder request = post("/minerva/api/projects/{projectId}/", TEST_PROJECT)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD));

    mockMvc.perform(request).andExpect(status().isBadRequest());
  }

  @Test
  public void createWithoNotOwnFileFileId() throws Exception {
    final UploadedFileEntry fileEntry = createFile(
        new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), StandardCharsets.UTF_8),
        curator);

    try {
      final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("projectId", TEST_PROJECT),
          new BasicNameValuePair("name", "New Disease Map"),
          new BasicNameValuePair("file-id", fileEntry.getId() + ""),
          new BasicNameValuePair("parser", "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

      final RequestBuilder request = post("/minerva/api/projects/{projectId}/", TEST_PROJECT)
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD));

      mockMvc.perform(request).andExpect(status().isForbidden());
    } finally {
      removeFile(fileEntry);
    }
  }

  @Test
  public void createWithoNoParser() throws Exception {
    final UploadedFileEntry fileEntry = createFile(
        new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), StandardCharsets.UTF_8),
        curator);

    try {
      final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("projectId", TEST_PROJECT),
          new BasicNameValuePair("name", "New Disease Map"),
          new BasicNameValuePair("file-id", fileEntry.getId() + ""))));

      final RequestBuilder request = post("/minerva/api/projects/{projectId}/", TEST_PROJECT)
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(createSession(CURATOR_LOGIN, CURATOR_PASSWORD));

      mockMvc.perform(request).andExpect(status().isBadRequest());
    } finally {
      removeFile(fileEntry);
    }
  }

  @Test
  public void createWithInvalidProjectId() throws Exception {
    final UploadedFileEntry fileEntry = createFile(
        new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), StandardCharsets.UTF_8),
        curator);

    try {
      final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("projectId", "bla bla"),
          new BasicNameValuePair("name", "New Disease Map"),
          new BasicNameValuePair("file-id", fileEntry.getId() + ""),
          new BasicNameValuePair("parser", "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

      final RequestBuilder request = post("/minerva/api/projects/{projectId}/", "bla bla")
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(createSession(CURATOR_LOGIN, CURATOR_PASSWORD));

      mockMvc.perform(request).andExpect(status().isBadRequest());
    } finally {
      removeFile(fileEntry);
    }
  }

  @Test
  public void testGetBackgroundByIdWithOtherUser() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final Project project = createAndPersistProject(TEST_PROJECT);
    final ProjectBackground background = project.getProjectBackgrounds().get(0);
    background.setCreator(curator);
    projectBackgroundService.updateProjectBackground(background);

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/backgrounds/{backgroundId}",
        TEST_PROJECT,
        background.getId())
        .session(session);
    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void addComplexProjectWithGenomicLayout() throws Exception {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final UploadedFileEntry fileEntry = createFile(
        Files.readAllBytes(Paths.get("./src/test/resources/complex_model_with_genomic_layout.zip")), admin);
    try {
      final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
          new BasicNameValuePair("parser", "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"),

          new BasicNameValuePair("zip-entries[0][_type]", "MAP"),
          new BasicNameValuePair("zip-entries[0][_filename]", "main.xml"),
          new BasicNameValuePair("zip-entries[0][_data][root]", "true"),
          new BasicNameValuePair("zip-entries[0][_data][name]", "s1"),
          new BasicNameValuePair("zip-entries[0][_data][type][id]", "UNKNOWN"),
          new BasicNameValuePair("zip-entries[0][_data][type][name]", "Unknown"),

          new BasicNameValuePair("zip-entries[1][_type]", "OVERLAY"),
          new BasicNameValuePair("zip-entries[1][_filename]", "layouts/goodSchema.txt"),
          new BasicNameValuePair("zip-entries[1][_data][name]", "test-o")

      )));

      final RequestBuilder request = post("/minerva/api/projects/" + TEST_PROJECT)
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(session);

      mockMvc.perform(request).andExpect(status().is2xxSuccessful());
      waitForProjectToFinishLoading(TEST_PROJECT);

      final RequestBuilder overlayRequest = get("/minerva/api/projects/{projectId}/overlays/", TEST_PROJECT)
          .session(session);

      final String json = mockMvc.perform(overlayRequest)
          .andExpect(status().is2xxSuccessful())
          .andReturn().getResponse().getContentAsString();
      final List<Map<String, Object>> overlays = objectMapper.readValue(json, new TypeReference<List<Map<String, Object>>>() {
      });

      assertNotNull(overlays.get(0).get("genomeType"));
      assertNotNull(overlays.get(0).get("genomeVersion"));
    } finally {
      waitForProjectToFinishLoading(TEST_PROJECT);
    }
  }

  @Test
  public void testDocsArchiveProject() throws Exception {
    final Project project = createProjectWithBackground();
    final String projectHomeDir = projectService.getProjectHomeDir(project);

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final RequestBuilder request = post("/minerva/api/projects/{projectId}:archive", TEST_PROJECT)
        .session(session);
    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("projects/project_data/archive",
            projectPathParameters()));

    minervaJobService.waitForTasksToFinish();

    assertEquals(ProjectStatus.ARCHIVED, projectService.getProjectByProjectId(TEST_PROJECT).getStatus());
    assertEquals(0, projectService.getBackgrounds(TEST_PROJECT, true).size());
    assertEquals(0, Files.list(Paths.get(projectHomeDir)).count());
  }

  private Project createProjectWithBackground() throws Exception {
    final UploadedFileEntry fileEntry = createFile(
        new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), StandardCharsets.UTF_8),
        userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));

    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
        new BasicNameValuePair("name", "Project name"),
        new BasicNameValuePair("version", "0.0.1"),
        new BasicNameValuePair("notify-email", "minerva@uni.lu"),
        new BasicNameValuePair("parser",
            "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

    final RequestBuilder request = post("/minerva/api/projects/" + TEST_PROJECT)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD));
    mockMvc.perform(request).andExpect(status().is2xxSuccessful());

    waitForProjectToFinishLoading(TEST_PROJECT);
    final Project project = projectService.getProjectByProjectId(TEST_PROJECT);
    return project;
  }

  @Test
  public void testReviveGoodProjectProject() throws Exception {

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final RequestBuilder request = post("/minerva/api/projects/{projectId}:revive", BUILT_IN_PROJECT)
        .session(session);
    mockMvc.perform(request)
        .andExpect(status().isConflict());
  }

  @Test
  public void testDocsReviveProject() throws Exception {
    final Project project = createAndPersistProject(TEST_PROJECT);
    project.setStatus(ProjectStatus.ARCHIVED);
    project.removeProjectBackground(project.getProjectBackgrounds().get(0));
    projectService.update(project);

    minervaJobService.waitForTasksToFinish();

    final String projectHomeDir = projectService.getProjectHomeDir(project);

    new File(projectHomeDir).mkdirs();

    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    final RequestBuilder request = post("/minerva/api/projects/{projectId}:revive", TEST_PROJECT)
        .session(session);
    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("projects/project_data/revive",
            projectPathParameters()));

    minervaJobService.waitForTasksToFinish();

    assertEquals(ProjectStatus.DONE, projectService.getProjectByProjectId(TEST_PROJECT).getStatus());
    assertTrue(projectService.getBackgrounds(TEST_PROJECT, true).size() > 1);
    assertEquals(6, Files.list(Paths.get(projectHomeDir)).count());
  }

  @Test
  public void testGetBackgroundsNoAccess() throws Exception {
    createAndPersistProject(TEST_PROJECT);
    final RequestBuilder request = get("/minerva/api/projects/{projectId}/backgrounds/", TEST_PROJECT);
    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testGetBackgroundsNotExistingNoAccess() throws Exception {
    final RequestBuilder request = get("/minerva/api/projects/{projectId}/backgrounds/", TEST_PROJECT);
    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testUpdateProjectOwner() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final String content = "{\"project\":{"
        + "\"organism\": null, "
        + "\"disease\": null, "
        + "\"owner\":\"" + Configuration.ANONYMOUS_LOGIN + "\""
        + "}}";

    final RequestBuilder request = patch("/minerva/api/projects/{projectId}/", BUILT_IN_PROJECT)
        .contentType(MediaType.APPLICATION_JSON)
        .content(content)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertEquals(Configuration.ANONYMOUS_LOGIN, projectService.getProjectByProjectId(BUILT_IN_PROJECT).getOwner().getLogin());
    for (final ProjectBackground background : projectService.getBackgrounds(BUILT_IN_PROJECT, true)) {
      assertEquals(Configuration.ANONYMOUS_LOGIN, background.getCreator().getLogin());
    }
  }
}
