package lcsb.mapviewer.web;

import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class DrugControllerIntegrationTest extends ControllerIntegrationTest {

  @Autowired
  private IMinervaJobService minervaJobService;

  @Before
  public void setup() {
    createAndPersistProject(TEST_PROJECT);
  }

  @After
  public void tearDown() throws Exception {
    removeProject(TEST_PROJECT);
    removeProject(TEST_PROJECT_ID);
  }

  @Test
  public void testDocsSearchDrugsInProjectUrl() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/drugs:search?query=aspirin", TEST_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andDo(document("projects/project_drugs/search_by_query",
            getProjectPathParameters(),
            getChemicalFilter(),
            listOfDrugFields()));
  }

  @Test
  public void testSearchDrugsInUndefinedProject() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/*/drugs:search?query=xyz")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testSearchDrugsByTargetInUndefinedProject() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/*/drugs:search?target=ALIAS:123")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDocsSearchDrugsByTargetInSubmap() throws Exception {
    Project project = createAndPersistProject(TEST_PROJECT_ID);
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final int submapId = project.getTopModel().getSubmodels().iterator().next().getElements()
        .iterator().next().getId();
    final RequestBuilder request = get("/minerva/api/projects/{projectId}/drugs:search?target=ALIAS:" + submapId, project.getProjectId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_drugs/search_by_target",
            getProjectPathParameters(),
            getChemicalFilter(),
            listOfDrugFields()))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
  }

  @Test
  public void testDocsGetSuggestedList() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/{projectId}/drugs/suggestedQueryList", TEST_PROJECT)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_drugs/suggested_query_list",
            getProjectPathParameters(),
            responseFields(
                fieldWithPath("[]")
                    .description("list of suggested drug queries")
                    .type("array<string>"))))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();
  }

  @Test
  public void testGetSuggestedListWithUndefinedProject() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    final RequestBuilder request = get("/minerva/api/projects/*/drugs/suggestedQueryList")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isNotFound());
  }

  private ResponseFieldsSnippet listOfDrugFields() {
    return responseFields(
        subsectionWithPath("[].id")
            .description("identifier of the chemical")
            .type(JsonFieldType.STRING),
        fieldWithPath("[].name")
            .description("name")
            .type(JsonFieldType.STRING),
        fieldWithPath("[].description")
            .description("description")
            .type(JsonFieldType.STRING),
        fieldWithPath("[].synonyms")
            .description("list of synonyms")
            .type("array<string>"),
        fieldWithPath("[].brandNames")
            .description("list of brand names")
            .type("array<string>"),
        fieldWithPath("[].bloodBrainBarrier")
            .description("does drug cross blood brain barrier")
            .type(JsonFieldType.STRING),
        subsectionWithPath("[].references")
            .description("list of references")
            .type("array<Reference>"),
        fieldWithPath("[].targets")
            .description("list of targets")
            .type("array<Target>"),
        fieldWithPath("[].targets[].name")
            .description("target name")
            .type(JsonFieldType.STRING),
        subsectionWithPath("[].targets[].references")
            .description("list of target references")
            .type("array<Reference>"),
        subsectionWithPath("[].targets[].targetElements")
            .description("list of elements on the map associated with this target")
            .type("array<object>"),
        subsectionWithPath("[].targets[].targetParticipants")
            .description("list of identifiers associated with this target")
            .type("array<object>"));
  }

}
