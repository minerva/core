package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import lcsb.mapviewer.api.CustomPage;
import lcsb.mapviewer.api.projects.models.publications.PublicationsController.ArticleDTO;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.map.model.ModelData;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.junit.Assert.assertEquals;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class PublicationsControllerIntegrationTest extends ControllerIntegrationTest {

  private Project project;
  private ModelData map;

  @Before
  public void setup() throws Exception {
    project = createAndPersistProject(TEST_PROJECT);
    map = project.getTopModel().getModelData();
  }

  @After
  public void tearDown() throws Exception {
    removeProject(TEST_PROJECT);
  }

  @Test
  public void testDocsGetPublicationsForMap() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/publications/", TEST_PROJECT, map.getId())
        .session(session);

    mockMvc.perform(request)
        .andDo(document("projects/project_data/get_publications",
            requestParameters(
                parameterWithName("start").description("number of first entry in this response").optional(),
                parameterWithName("sortColumn")
                    .description("entry column that should be used for sorting (default: pubmedId)").optional(),
                parameterWithName("sortOrder").description("entry sort order (asc, desc)").optional(),
                parameterWithName("length").description("number of entres we want to obtain").optional(),
                parameterWithName("search").description("search query used for filtering").optional()),
            pathParameters(parameterWithName("projectId").description("project identifier"),
                parameterWithName("mapId").description("submap identifier (use '*' to indicate all maps)")),
            responseFields(
                fieldWithPath("data")
                    .description("list of publication data")
                    .type(JsonFieldType.ARRAY),
                subsectionWithPath("data[].elements")
                    .description("list of bioEntities annotated with publication")
                    .type(JsonFieldType.ARRAY),
                fieldWithPath("data[].publication")
                    .description("publication")
                    .type(JsonFieldType.OBJECT),
                fieldWithPath("data[].publication.article.title")
                    .description("title")
                    .type(JsonFieldType.STRING),
                fieldWithPath("data[].publication.article.authors")
                    .description("authors")
                    .type(JsonFieldType.ARRAY),
                fieldWithPath("data[].publication.article.journal")
                    .description("journal")
                    .type(JsonFieldType.STRING),
                fieldWithPath("data[].publication.article.year")
                    .description("year")
                    .type(JsonFieldType.NUMBER),
                fieldWithPath("data[].publication.article.link")
                    .description("link")
                    .type(JsonFieldType.STRING),
                fieldWithPath("data[].publication.article.pubmedId")
                    .description("pubmed id")
                    .type(JsonFieldType.STRING),
                fieldWithPath("data[].publication.article.citationCount")
                    .description("citation count")
                    .type(JsonFieldType.NUMBER),
                fieldWithPath("filteredSize")
                    .description("number of entries that match filter criteria")
                    .type(JsonFieldType.NUMBER),
                fieldWithPath("length")
                    .description("number of entries in this response")
                    .type(JsonFieldType.NUMBER),
                fieldWithPath("page")
                    .description("page number of result list")
                    .type(JsonFieldType.NUMBER),
                fieldWithPath("totalSize")
                    .description("number of all log entries")
                    .type(JsonFieldType.NUMBER))))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testGetPublicationsWithUndefinedProject() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = get("/minerva/api/projects/*/models/" + map.getId() + "/publications/")
        .session(session);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    CustomPage<ArticleDTO> page = objectMapper.readValue(content, new TypeReference<CustomPage<ArticleDTO>>() {
    });

    assertEquals(0, page.getTotalElements());
    assertEquals(0, page.getNumber());
    assertEquals(0, page.getContent().size());
  }

  @Test
  public void testPublications() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/publications/?page=1&length=1", TEST_PROJECT, map.getId())
        .session(session);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    CustomPage<ArticleDTO> page = objectMapper.readValue(content, new TypeReference<CustomPage<ArticleDTO>>() {
    });

    assertEquals(1, page.getTotalElements());
    assertEquals(1, page.getNumber());
    assertEquals(0, page.getContent().size());
  }

  @Test
  public void testPublicationsInvalidPageSize() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/publications/?length=0", TEST_PROJECT, map.getId())
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void testPublicationsFiltering() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/publications/?search=287", TEST_PROJECT, map.getId())
        .session(session);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    CustomPage<ArticleDTO> page = objectMapper.readValue(content, new TypeReference<CustomPage<ArticleDTO>>() {
    });

    assertEquals(1, page.getContent().size());
  }

  @Test
  public void testPublicationsFilteringNonExistingPubmed() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/publications/?search=2877", TEST_PROJECT, map.getId())
        .session(session);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    CustomPage<ArticleDTO> page = objectMapper.readValue(content, new TypeReference<CustomPage<ArticleDTO>>() {
    });

    assertEquals(0, page.getContent().size());
  }

  @Test
  public void testPublicationsFilteringByModelName() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/publications/?search=map_name", TEST_PROJECT, map.getId())
        .session(session);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    CustomPage<ArticleDTO> page = objectMapper.readValue(content, new TypeReference<CustomPage<ArticleDTO>>() {
    });

    assertEquals(1, page.getContent().size());
  }

  @Test
  public void testPublicationsFilteringByNonExistingModelName() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    RequestBuilder request = get("/minerva/api/projects/{projectId}/models/{mapId}/publications/?search=map_nameX", TEST_PROJECT, map.getId())
        .session(session);

    String content = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    CustomPage<ArticleDTO> page = objectMapper.readValue(content, new TypeReference<CustomPage<ArticleDTO>>() {
    });

    assertEquals(0, page.getContent().size());
  }

}
