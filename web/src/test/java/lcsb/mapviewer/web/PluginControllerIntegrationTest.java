package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.model.plugin.Plugin;
import lcsb.mapviewer.model.plugin.PluginDataEntry;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IPluginService;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.request.PathParametersSnippet;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.patch;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class PluginControllerIntegrationTest extends ControllerIntegrationTest {

  private static final String PLUGIN_URL = "https://minerva-dev.lcsb.uni.lu/plugins/starter-kit/plugin.js";

  private static final String PLUGIN_HASH = "b7a875a0536949d4d8a2f834dc54489e";

  @Autowired
  private IPluginService pluginService;

  private User user;

  private final ObjectMapper objectMapper = new ObjectMapper();

  @Before
  public void setup() throws Exception {
    user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
  }

  @After
  public void tearDown() throws Exception {
    removeUser(user);
    Plugin plugin = pluginService.getByHash(PLUGIN_HASH);
    if (plugin != null) {
      pluginService.delete(plugin);
    }
  }

  @Test
  public void testDocsListPlugins() throws Exception {
    createPlugin();

    RequestBuilder request = get("/minerva/api/plugins/");

    mockMvc.perform(request)
        .andDo(document("plugin/list_plugins",
            responseFields(fieldWithPath("[]")
                .description("list of plugins")
                .type(JsonFieldType.ARRAY)).andWithPrefix("[].", pluginResponseFields())))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void createPublicPluginWithoutPrivileges() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("hash", PLUGIN_HASH),
        new BasicNameValuePair("name", "x"),
        new BasicNameValuePair("version", "x"),
        new BasicNameValuePair("isPublic", "true"),
        new BasicNameValuePair("url", PLUGIN_URL))));

    RequestBuilder request = post("/minerva/api/plugins/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDocsCreatePublicPluginWithPrivileges() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("hash", PLUGIN_HASH),
        new BasicNameValuePair("name", "x"),
        new BasicNameValuePair("version", "x"),
        new BasicNameValuePair("isPublic", "true"),
        new BasicNameValuePair("url", PLUGIN_URL))));

    RequestBuilder request = post("/minerva/api/plugins/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("plugin/create_plugin",
            requestParameters(
                parameterWithName("hash")
                    .description("md5 sum of javascript file"),
                parameterWithName("name")
                    .description("name"),
                parameterWithName("version")
                    .description("version"),
                parameterWithName("isDefault")
                    .optional()
                    .description("should be opened automatically when map is browsed"),
                parameterWithName("isPublic")
                    .description("should the plugin be visible by all users"),
                parameterWithName("url")
                    .description("url to the javascript file with plugin source code")),
            responseFields(pluginResponseFields())))

        .andExpect(status().is2xxSuccessful());
    assertEquals(1, getPlugins().size());
  }

  private List<Plugin> getPlugins() {
    return pluginService.getAll();
  }

  @Test
  public void testDocsGetPluginInfo() throws Exception {
    Plugin plugin = createPlugin();

    RequestBuilder request = get("/minerva/api/plugins/{hash}", plugin.getHash());

    mockMvc.perform(request)
        .andDo(document("plugin/get_plugin",
            pluginPathParameters(),
            responseFields(pluginResponseFields())))
        .andExpect(status().is2xxSuccessful());
  }

  private PathParametersSnippet pluginPathParameters() {
    return pathParameters(
        parameterWithName("hash")
            .description("plugin md5 checksum"));
  }

  private List<FieldDescriptor> pluginResponseFields() {
    return Arrays.asList(fieldWithPath("hash")
            .description("md5 checksum of the source file")
            .type(JsonFieldType.STRING),
        fieldWithPath("isDefault")
            .description("should be opened automatically when map is browsed")
            .type("boolean"),
        fieldWithPath("isPublic")
            .description("should be visible on plugin list to all users")
            .type("boolean"),
        fieldWithPath("name")
            .description("name")
            .type(JsonFieldType.STRING),
        fieldWithPath("urls")
            .description("list of urls where source can be found")
            .type(JsonFieldType.ARRAY),
        fieldWithPath("version")
            .description("plugin version")
            .type(JsonFieldType.STRING));
  }

  @Test
  public void createPublicPluginWithInvalidUrl() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("hash", PLUGIN_HASH),
        new BasicNameValuePair("name", "x"),
        new BasicNameValuePair("version", "x"),
        new BasicNameValuePair("isPublic", "true"),
        new BasicNameValuePair("url", "x"))));

    RequestBuilder request = post("/minerva/api/plugins/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
    assertEquals(0, getPlugins().size());
  }

  @Test
  public void createPrivatePlugin() throws Exception {
    MockHttpSession session = createSession(TEST_USER_LOGIN, TEST_USER_PASSWORD);

    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("hash", PLUGIN_HASH),
        new BasicNameValuePair("name", "x"),
        new BasicNameValuePair("version", "x"),
        new BasicNameValuePair("isPublic", "false"),
        new BasicNameValuePair("url", PLUGIN_URL))));

    RequestBuilder request = post("/minerva/api/plugins/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
    assertEquals(1, getPlugins().size());
  }

  @Test
  public void createPluginWithLocalUrl() throws Exception {
    String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("hash", PLUGIN_HASH),
        new BasicNameValuePair("name", "x"),
        new BasicNameValuePair("version", "x"),
        new BasicNameValuePair("isPublic", "false"),
        new BasicNameValuePair("url", "http://localhost:8080/test/highlight-something.js"))));

    RequestBuilder request = post("/minerva/api/plugins/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());
    assertEquals(1, getPlugins().size());
  }

  @Test
  public void testDocsTestDocsSetAndGetGlobalPluginData() throws Exception {
    String body = "value=xxx";
    Plugin plugin = createPlugin();

    RequestBuilder request = post("/minerva/api/plugins/{hash}/data/global/{key}/", plugin.getHash(), "my_key")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body);

    mockMvc.perform(request)
        .andDo(document("plugin/set_plugin_data",
            pluginPathParameters().and(parameterWithName("key").description("id of the entry")),
            requestParameters(parameterWithName("value").description("value of the entry")),
            responseFields(fieldWithPath("key").description("id"),
                fieldWithPath("value").description("value of the entry"))))
        .andExpect(status().is2xxSuccessful());

    request = get("/minerva/api/plugins/{hash}/data/global/{key}/", plugin.getHash(), "my_key");

    String response = mockMvc.perform(request)
        .andDo(document("plugin/get_plugin_data",
            pluginPathParameters().and(parameterWithName("key").description("id of the entry")),
            responseFields(fieldWithPath("key").description("id"),
                fieldWithPath("value").description("value of the entry"))))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    Map<String, Object> result = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });

    assertEquals("xxx", result.get("value"));
  }

  @Test
  public void testDocsSetAndGetUserPluginData() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_LOGIN);

    String body = "value=xxx";
    Plugin plugin = createPlugin();

    RequestBuilder request = post("/minerva/api/plugins/{hash}/data/users/{key}/", plugin.getHash(), "my_key")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session)
        .content(body);

    mockMvc.perform(request)
        .andDo(document("plugin/set_user_plugin_data",
            pluginPathParameters().and(parameterWithName("key").description("id of the entry")),
            requestParameters(parameterWithName("value").description("value of the entry")),
            responseFields(fieldWithPath("key").description("id"),
                fieldWithPath("value").description("value of the entry"),
                fieldWithPath("user").description("owner of the entry"))))
        .andExpect(status().is2xxSuccessful());

    request = get("/minerva/api/plugins/{hash}/data/users/{key}/", plugin.getHash(), "my_key")
        .session(session);

    String response = mockMvc.perform(request)
        .andDo(document("plugin/get_user_plugin_data",
            pluginPathParameters().and(parameterWithName("key").description("id of the entry")),
            responseFields(fieldWithPath("key").description("id"),
                fieldWithPath("value").description("value of the entry"),
                fieldWithPath("user").description("owner of the entry"))))
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    Map<String, Object> result = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });

    assertEquals("xxx", result.get("value"));
  }

  @Test
  public void updateUserPluginData() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_LOGIN);

    String body = "value=xxx";
    Plugin plugin = createPlugin();

    RequestBuilder request = post("/minerva/api/plugins/{hash}/data/users/{key}/", plugin.getHash(), "my_key")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session)
        .content(body);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    body = "value=xxx2";
    request = post("/minerva/api/plugins/{hash}/data/users/{key}/", plugin.getHash(), "my_key")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .session(session)
        .content(body);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    request = get("/minerva/api/plugins/{hash}/data/users/{key}/", plugin.getHash(), "my_key")
        .session(session);

    String response = mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn().getResponse().getContentAsString();

    Map<String, Object> result = objectMapper.readValue(response, new TypeReference<Map<String, Object>>() {
    });

    assertEquals("xxx2", result.get("value"));
  }

  @Test
  public void removeGlobalPluginData() throws Exception {
    Plugin plugin = createPlugin();

    PluginDataEntry entry = new PluginDataEntry();
    entry.setKey("key");
    entry.setValue("val");
    entry.setPlugin(plugin);
    pluginService.add(entry);

    RequestBuilder request = delete("/minerva/api/plugins/" + plugin.getHash() + "/data/global/" + entry.getKey() + "/");

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertNull(pluginService.getEntryByKey(plugin, entry.getKey(), null));

  }

  @Test
  public void testSetInvalidGlobalPluginDataKey() throws Exception {
    StringBuilder body = new StringBuilder("value=xxx");
    for (int i = 0; i < 2000000; i++) {
      body.append("y");
    }
    Plugin plugin = createPlugin();

    RequestBuilder request = post("/minerva/api/plugins/" + plugin.getHash() + "/data/global/key/")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body.toString());

    mockMvc.perform(request)
        .andExpect(status().isBadRequest())
        .andReturn().getResponse().getContentAsString();

  }

  @Test
  public void testRemovePlugin() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);
    Plugin plugin = createPlugin();

    RequestBuilder request = delete("/minerva/api/plugins/" + plugin.getHash() + "/").session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    assertNull(pluginService.getByHash(plugin.getHash()));
  }

  private Plugin createPlugin() {
    return createPlugin(PLUGIN_HASH);
  }

  private Plugin createPlugin(final String hash) {
    Plugin plugin = new Plugin();
    plugin.setHash(hash);
    plugin.setName("starter-kit");
    plugin.setVersion("0.0.1");
    plugin.addUrl(PLUGIN_URL);
    pluginService.add(plugin);
    return plugin;
  }

  @Test
  public void updatePluginWithPrivileges() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Plugin plugin = createPlugin();
    Plugin updatedData = new Plugin();
    updatedData.setHash(plugin.getHash());
    updatedData.setName("new name");
    updatedData.setVersion("v0.0.13");
    updatedData.setDefault(true);

    Map<String, Object> bodyMap = new HashedMap<>();
    bodyMap.put("plugin", updatedData);

    String body = objectMapper.writeValueAsString(bodyMap);

    RequestBuilder request = patch("/minerva/api/plugins/" + plugin.getHash())
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    Plugin p = pluginService.getByHash(plugin.getHash());
    assertEquals(updatedData.getName(), p.getName());
    assertEquals(updatedData.getVersion(), p.getVersion());
    assertEquals(updatedData.isDefault(), p.isDefault());
  }

  @Test
  public void updatePluginWithInvalidData() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Plugin plugin = createPlugin();

    RequestBuilder request = patch("/minerva/api/plugins/" + plugin.getHash())
        .contentType(MediaType.APPLICATION_JSON)
        .content("{}")
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void updatePluginWithHashChange() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    Plugin plugin = createPlugin("old-hash");
    Plugin updatedData = new Plugin();
    updatedData.setHash(PLUGIN_HASH);
    updatedData.setName("new name");
    updatedData.setVersion("v0.0.13");
    updatedData.setDefault(true);

    Map<String, Object> bodyMap = new HashedMap<>();
    bodyMap.put("plugin", updatedData);

    String body = objectMapper.writeValueAsString(bodyMap);

    RequestBuilder request = patch("/minerva/api/plugins/" + plugin.getHash())
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    Plugin p = pluginService.getByHash(PLUGIN_HASH);
    assertEquals(updatedData.getName(), p.getName());
    assertEquals(updatedData.getVersion(), p.getVersion());
    assertEquals(updatedData.isDefault(), p.isDefault());

    assertNull(pluginService.getByHash("old-hash"));
  }

  @Test
  public void updatePluginWithHashChangeAndExistingDestinationHash() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    createPlugin(PLUGIN_HASH);

    Plugin plugin = createPlugin("old-hash");
    Plugin updatedData = new Plugin();
    updatedData.setHash(PLUGIN_HASH);
    updatedData.setName("new name");
    updatedData.setVersion("v0.0.13");
    updatedData.setDefault(true);

    Map<String, Object> bodyMap = new HashedMap<>();
    bodyMap.put("plugin", updatedData);

    String body = objectMapper.writeValueAsString(bodyMap);

    RequestBuilder request = patch("/minerva/api/plugins/" + plugin.getHash())
        .contentType(MediaType.APPLICATION_JSON)
        .content(body)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    Plugin p = pluginService.getByHash(PLUGIN_HASH);
    assertEquals(updatedData.getName(), p.getName());
    assertEquals(updatedData.getVersion(), p.getVersion());
    assertEquals(updatedData.isDefault(), p.isDefault());

    assertNull(pluginService.getByHash("old-hash"));
  }

}
