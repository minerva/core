package lcsb.mapviewer.web;

import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("EmailSenderDisabledEmailProfile")
public class UserControllerDisabledEmailIntegrationTest extends ControllerIntegrationTest {

  private User user;

  @Autowired
  private IUserService userService;

  @Autowired
  private IConfigurationService configurationService;

  @Before
  public void setup() throws Exception {
    user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
  }

  @After
  public void tearDown() throws Exception {
    removeUser(user);
  }

  @Test
  public void requestResetPasswordWhenMinervaCannotSendMails() throws Exception {
    configureServerForResetPasswordRequest();

    RequestBuilder grantRequest = post("/minerva/api/users/" + TEST_USER_LOGIN + ":requestResetPassword");

    mockMvc.perform(grantRequest)
        .andExpect(status().is5xxServerError());
  }

  private void configureServerForResetPasswordRequest() {
    User user = userService.getUserByLogin(TEST_USER_LOGIN);
    user.setEmail("test@test.xyz");
    userService.update(user);

    configurationService.setConfigurationValue(ConfigurationElementType.MINERVA_ROOT, "https://minerva-dev.lcsb.uni.lu/minerva/");
  }

}
