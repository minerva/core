package lcsb.mapviewer.web;

import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.EmailConfirmationToken;
import lcsb.mapviewer.model.user.ResetPasswordToken;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@Rollback
@ActiveProfiles("EmailSenderEnabledEmailProfile")
public class UserControllerIntegrationWithoutTransactionTest extends ControllerIntegrationTest {

  private static final String TEST_USER_LOGIN_2 = "test_user2";

  @Autowired
  private IUserService userService;

  @Autowired
  private IConfigurationService configurationService;

  private User user;

  @Before
  public void setup() throws Exception {
    user = createUser(TEST_USER_LOGIN, TEST_USER_PASSWORD);
  }

  @After
  public void tearDown() throws Exception {
    removeUser(user);
  }

  @Test
  public void createUser() throws Exception {
    try {
      final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

      final String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("login", TEST_USER_LOGIN_2),
          new BasicNameValuePair("password", TEST_USER_PASSWORD),
          new BasicNameValuePair("name", ""),
          new BasicNameValuePair("surname", ""),
          new BasicNameValuePair("email", ""))));
      final RequestBuilder request = post("/minerva/api/users/" + TEST_USER_LOGIN_2)
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(session);

      mockMvc.perform(request)
          .andExpect(status().is2xxSuccessful());
    } finally {
      removeUser(userService.getUserByLogin(TEST_USER_LOGIN_2));
    }
  }

  @Test
  public void resendConfirmationEmail() throws Exception {
    configureServerForResetPasswordRequest();

    User user = userService.getUserByLogin(TEST_USER_LOGIN);
    user.setConfirmed(false);
    userService.update(user);

    final RequestBuilder resetRequest = post("/minerva/api/users/{login}:resendConfirmEmail", user.getLogin());

    mockMvc.perform(resetRequest)
        .andExpect(status().is2xxSuccessful());

    user = userService.getUserByLogin(TEST_USER_LOGIN);
    assertFalse(user.isConfirmed());
  }

  @Test
  public void createGuiPreferencesTwice() throws Exception {
    final MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    try {
      final RequestBuilder request = patch("/minerva/api/users/" + TEST_USER_LOGIN + ":updatePreferences")
          .contentType(MediaType.APPLICATION_JSON)
          .content("{\"preferences\":{}}")
          .session(session);

      mockMvc.perform(request)
          .andExpect(status().is2xxSuccessful());

      final MutableBoolean exceptionHappened = new MutableBoolean(false);

      final List<Thread> threads = new ArrayList<>();
      for (int i = 0; i < 10; i++) {
        final String body = "{\"preferences\":{\"gui-preferences\":{\"test\":\"val" + i + "\"}}}";
        final Thread thread = new Thread(new Runnable() {

          @Override
          public void run() {
            try {
              final RequestBuilder request = patch("/minerva/api/users/" + TEST_USER_LOGIN + ":updatePreferences")
                  .contentType(MediaType.APPLICATION_JSON)
                  .content(body)
                  .session(session);

              final int status = mockMvc.perform(request).andReturn().getResponse().getStatus();
              assertNotEquals(500, status);
            } catch (final Throwable e) {
              logger.error(e, e);
              exceptionHappened.setTrue();
            }
          }
        });
        threads.add(thread);
        thread.start();
      }
      for (final Thread thread : threads) {
        thread.join();
      }
      assertFalse(exceptionHappened.getValue());
    } finally {
      removeUser(userService.getUserByLogin(TEST_USER_LOGIN));
    }
  }

  @Test
  public void requestResetPasswordForLdapUser() throws Exception {
    configureServerForResetPasswordRequest();

    final User user = userService.getUserByLogin(TEST_USER_LOGIN);
    user.setConnectedToLdap(true);
    userService.update(user);

    final RequestBuilder grantRequest = post("/minerva/api/users/" + TEST_USER_LOGIN + ":requestResetPassword");

    mockMvc.perform(grantRequest)
        .andExpect(status().isBadRequest());
  }

  @Test
  public void requestResetPasswordWhenMinervaRootNotConfigured() throws Exception {
    configureServerForResetPasswordRequest();
    configurationService.setConfigurationValue(ConfigurationElementType.MINERVA_ROOT, "");

    final RequestBuilder grantRequest = post("/minerva/api/users/" + TEST_USER_LOGIN + ":requestResetPassword");

    mockMvc.perform(grantRequest)
        .andExpect(status().is5xxServerError());

  }

  @Test
  public void requestResetPasswordWhenUserWithoutEmail() throws Exception {
    configureServerForResetPasswordRequest();

    final User user = userService.getUserByLogin(TEST_USER_LOGIN);
    user.setEmail("");
    userService.update(user);

    final RequestBuilder grantRequest = post("/minerva/api/users/" + TEST_USER_LOGIN + ":requestResetPassword");

    mockMvc.perform(grantRequest)
        .andExpect(status().is4xxClientError());

  }

  @Test
  public void requestResetPasswordForUnknownUser() throws Exception {
    configureServerForResetPasswordRequest();

    final RequestBuilder grantRequest = post("/minerva/api/users/blah:requestResetPassword");

    mockMvc.perform(grantRequest)
        .andExpect(status().is4xxClientError());

  }

  @Test
  public void testDocsResetPassword() throws Exception {
    configureServerForResetPasswordRequest();
    final String newPassword = "pass2";

    final Calendar expires = Calendar.getInstance();
    expires.add(Calendar.DAY_OF_MONTH, 1);
    final ResetPasswordToken token = new ResetPasswordToken(user, UUID.randomUUID().toString().replaceAll("[a-z]", "0"),
        expires);
    userService.add(token);

    final String content = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("token", token.getToken()),
        new BasicNameValuePair("password", newPassword))));

    final RequestBuilder resetRequest = post("/minerva/api/users:resetPassword")
        .content(content)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    mockMvc.perform(resetRequest)
        .andDo(document("user/reset_password",
            requestParameters(
                parameterWithName("password")
                    .optional()
                    .description("new password"),
                parameterWithName("token")
                    .optional()
                    .description("reset password token obtained using email"))))
        .andExpect(status().is2xxSuccessful());

    createSession(TEST_USER_LOGIN, newPassword);

  }

  @Test
  public void resetPasswordUsingTheSameTokenTwice() throws Exception {
    configureServerForResetPasswordRequest();

    final RequestBuilder request = post("/minerva/api/users/" + TEST_USER_LOGIN + ":requestResetPassword");

    mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful());

    final String newPassword = "pass2";

    final ResetPasswordToken token = userService.getPasswordTokens(TEST_USER_LOGIN).get(0);

    String content = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("token", token.getToken()),
        new BasicNameValuePair("password", newPassword))));

    RequestBuilder resetRequest = post("/minerva/api/users:resetPassword")
        .content(content)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    mockMvc.perform(resetRequest)
        .andExpect(status().is2xxSuccessful());

    content = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("token", token.getToken()),
        new BasicNameValuePair("password", newPassword + "xx"))));

    resetRequest = post("/minerva/api/users:resetPassword")
        .content(content)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    mockMvc.perform(resetRequest)
        .andExpect(status().is4xxClientError());

    createSession(TEST_USER_LOGIN, newPassword);

  }

  @Test
  public void testDocsConfirmRegistrationOfNewUser() throws Exception {
    configureServerForResetPasswordRequest();

    User user = userService.getUserByLogin(TEST_USER_LOGIN);

    user.setActive(false);
    user.setConfirmed(false);
    userService.update(user);
    final Calendar expires = Calendar.getInstance();
    expires.add(Calendar.DAY_OF_MONTH, 1);
    final EmailConfirmationToken token = new EmailConfirmationToken(user, UUID.randomUUID().toString(), expires);
    userService.add(token);

    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Collections.singletonList(
        new BasicNameValuePair("token", token.getToken()))));

    final RequestBuilder resetRequest = post("/minerva/api/users/{login}:confirmEmail", user.getLogin())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body);

    mockMvc.perform(resetRequest)
        .andDo(document("user/confirm_email",
            userPathParameters(),
            requestParameters(parameterWithName("token")
                .description("token obtained in the registration email")),
            responseFields(
                fieldWithPath("message")
                    .description("detailed information about the status")
                    .type(JsonFieldType.STRING),
                fieldWithPath("status")
                    .description("status")
                    .type(JsonFieldType.STRING))))
        .andExpect(status().is2xxSuccessful());

    user = userService.getUserByLogin(TEST_USER_LOGIN);
    assertTrue(user.isConfirmed());
    assertFalse(user.isActive());
    assertFalse(user.isRemoved());
  }

  @Test
  public void resendConfirmationEmailForConfirmedUser() throws Exception {
    configureServerForResetPasswordRequest();

    User user = userService.getUserByLogin(TEST_USER_LOGIN);
    user.setConfirmed(true);
    userService.update(user);

    final RequestBuilder resetRequest = post("/minerva/api/users/{login}:resendConfirmEmail", user.getLogin());

    mockMvc.perform(resetRequest)
        .andExpect(status().isBadRequest());

    user = userService.getUserByLogin(TEST_USER_LOGIN);
    assertTrue(user.isConfirmed());
  }

  @Test
  public void confirmRegistrationOfNewUserWithNotRequiredApproval() throws Exception {
    configureServerForResetPasswordRequest();
    configurationService.setConfigurationValue(ConfigurationElementType.REQUIRE_APPROVAL_FOR_AUTO_REGISTERED_USERS,
        "false");

    User user = userService.getUserByLogin(TEST_USER_LOGIN);
    user.setActive(false);
    user.setConfirmed(false);
    userService.update(user);
    final Calendar expires = Calendar.getInstance();
    expires.add(Calendar.DAY_OF_MONTH, 1);
    final EmailConfirmationToken token = new EmailConfirmationToken(user, UUID.randomUUID().toString(), expires);
    userService.add(token);

    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Collections.singletonList(
        new BasicNameValuePair("token", token.getToken()))));

    final RequestBuilder resetRequest = post("/minerva/api/users/{login}:confirmEmail", user.getLogin())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body);

    mockMvc.perform(resetRequest)
        .andExpect(status().is2xxSuccessful());

    user = userService.getUserByLogin(TEST_USER_LOGIN);
    assertTrue(user.isConfirmed());
    assertTrue(user.isActive());
    assertFalse(user.isRemoved());
  }

  @Test
  public void registerNewUserWithInvalidEmail() throws Exception {
    configureServerForResetPasswordRequest();

    configurationService.setConfigurationValue(ConfigurationElementType.ALLOW_AUTO_REGISTER, "true");
    final Map<String, Object> data = new HashMap<>();
    data.put("email", "blah");
    data.put("password", "123blah");
    data.put("name", "Piotr");
    data.put("surname", "Gawron");

    final RequestBuilder request = post("/minerva/api/users:registerUser")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(data));

    mockMvc.perform(request)
        .andExpect(status().isBadRequest());

  }

  @Test
  public void confirmRegistrationOfNewUserWithInvalidToken() throws Exception {
    configureServerForResetPasswordRequest();

    User user = userService.getUserByLogin(TEST_USER_LOGIN);

    user.setActive(false);
    user.setConfirmed(false);
    userService.update(user);

    final String body = EntityUtils.toString(new UrlEncodedFormEntity(Collections.singletonList(
        new BasicNameValuePair("token", "xxx"))));

    final RequestBuilder resetRequest = post("/minerva/api/users/{login}:confirmEmail", user.getLogin())
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .content(body);

    mockMvc.perform(resetRequest)
        .andExpect(status().is4xxClientError());

    user = userService.getUserByLogin(TEST_USER_LOGIN);
    assertFalse(user.isConfirmed());
    assertFalse(user.isActive());
    assertFalse(user.isRemoved());
  }

  @Test
  public void resetPasswordWithExpiredToken() throws Exception {
    configureServerForResetPasswordRequest();

    final Calendar expires = Calendar.getInstance();
    expires.add(Calendar.DATE, -1);
    final ResetPasswordToken token = new ResetPasswordToken(userService.getUserByLogin(TEST_USER_LOGIN),
        "bla", expires);
    userService.add(token);

    final String content = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("token", token.getToken()),
        new BasicNameValuePair("password", "blah"))));

    final RequestBuilder resetRequest = post("/minerva/api/users:resetPassword")
        .content(content)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    mockMvc.perform(resetRequest)
        .andExpect(status().isNotFound());
  }

  @Test
  public void testDocsRequestResetPassword() throws Exception {
    final long count = userService.getPasswordTokenCount();
    configureServerForResetPasswordRequest();

    final RequestBuilder grantRequest = post("/minerva/api/users/{login}:requestResetPassword", TEST_USER_LOGIN);

    mockMvc.perform(grantRequest)
        .andDo(document("user/request_reset_password",
            userPathParameters()))
        .andExpect(status().is2xxSuccessful());
    assertEquals(count + 1, userService.getPasswordTokenCount());

  }

  @Test
  public void resetPasswordWithInvalidToken() throws Exception {
    configureServerForResetPasswordRequest();

    final String content = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
        new BasicNameValuePair("token", "blah"),
        new BasicNameValuePair("password", "???"))));

    final RequestBuilder resetRequest = post("/minerva/api/users:resetPassword")
        .content(content)
        .contentType(MediaType.APPLICATION_FORM_URLENCODED);

    mockMvc.perform(resetRequest)
        .andExpect(status().isNotFound());
  }

  private void configureServerForResetPasswordRequest() {
    final User user = userService.getUserByLogin(TEST_USER_LOGIN);
    user.setEmail("test@test.xyz");
    userService.update(user);

    configurationService.setConfigurationValue(ConfigurationElementType.MINERVA_ROOT,
        "https://minerva-dev.lcsb.uni.lu/minerva/");
  }

}
