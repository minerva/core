package lcsb.mapviewer.web;

import lcsb.mapviewer.converter.zip.ZipEntryFileDeserializer;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectStatus;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUserService;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.restdocs.request.ParameterDescriptor;
import org.springframework.restdocs.request.RequestParametersSnippet;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("NumericDirectoryNameGeneratorProfile")
public class ProjectControllerIntegrationForDocsTest extends ControllerIntegrationTest {

  private static final String CURATOR_PASSWORD = "curator_pass";
  private static final String CURATOR_LOGIN = "curator_user";

  @Autowired
  private IUserService userService;

  @Autowired
  private IProjectService projectService;

  @Autowired
  private ProjectSnippets snippets;

  private User curator;

  @Autowired
  private IConfigurationService configurationService;

  @Before
  public void setup() {
    curator = createCurator(CURATOR_LOGIN, CURATOR_PASSWORD);
    configurationService.setConfigurationValue(ConfigurationElementType.MINERVA_ROOT, "https://pdmap.uni.lu/minerva/");
  }

  @After
  public void tearDown() throws Exception {
    removeProject(TEST_PROJECT);
    removeUser(curator);
    configurationService.setConfigurationValue(ConfigurationElementType.MINERVA_ROOT, "");
  }

  @Test
  public void testDocsAddComplexProjectAsCurator() throws Exception {
    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    try {
      UploadedFileEntry fileEntry = createFile(
          Files.readAllBytes(Paths.get("./src/test/resources/complex_model_with_submaps.zip")), admin);

      String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("projectId", TEST_PROJECT),
          new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
          new BasicNameValuePair("name", "Project name"),
          new BasicNameValuePair("version", "0.0.1"),
          new BasicNameValuePair("notify-email", "minerva@uni.lu"),
          new BasicNameValuePair("custom-license-name", "my awesome license"),
          new BasicNameValuePair("custom-license-url", "https://my.awesome.lincese.org/"),
          new BasicNameValuePair("parser",
              "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"),

          new BasicNameValuePair("zip-entries[0][_type]", "MAP"),
          new BasicNameValuePair("zip-entries[0][_filename]", "main.xml"),
          new BasicNameValuePair("zip-entries[0][_data][root]", "true"),
          new BasicNameValuePair("zip-entries[0][_data][name]", "main"),
          new BasicNameValuePair("zip-entries[0][_data][type][id]", "UNKNOWN"),

          new BasicNameValuePair("zip-entries[1][_type]", "MAP"),
          new BasicNameValuePair("zip-entries[1][_filename]", "submaps/mapping.xml"),
          new BasicNameValuePair("zip-entries[1][_data][root]", "false"),
          new BasicNameValuePair("zip-entries[1][_data][name]", "mapping"),
          new BasicNameValuePair("zip-entries[1][_data][type][id]", "UNKNOWN"),

          new BasicNameValuePair("zip-entries[2][_type]", "MAP"),
          new BasicNameValuePair("zip-entries[2][_filename]", "submaps/s1.xml"),
          new BasicNameValuePair("zip-entries[2][_data][root]", "false"),
          new BasicNameValuePair("zip-entries[2][_data][name]", "s1"),
          new BasicNameValuePair("zip-entries[2][_data][type][id]", "UNKNOWN"),

          new BasicNameValuePair("zip-entries[3][_type]", "MAP"),
          new BasicNameValuePair("zip-entries[3][_filename]", "submaps/s2.xml"),
          new BasicNameValuePair("zip-entries[3][_data][root]", "false"),
          new BasicNameValuePair("zip-entries[3][_data][name]", "s2"),
          new BasicNameValuePair("zip-entries[3][_data][type][id]", "UNKNOWN"),

          new BasicNameValuePair("zip-entries[4][_type]", "MAP"),
          new BasicNameValuePair("zip-entries[4][_filename]", "submaps/s3.xml"),
          new BasicNameValuePair("zip-entries[4][_data][root]", "false"),
          new BasicNameValuePair("zip-entries[4][_data][name]", "s3"),
          new BasicNameValuePair("zip-entries[4][_data][type][id]", "UNKNOWN")

      )));

      RequestBuilder request = post("/minerva/api/projects/{projectId}/", TEST_PROJECT)
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD));

      mockMvc.perform(request).andExpect(status().is2xxSuccessful())
          .andDo(document("projects/project_data/create_zip",
              projectPathParameters(),
              createProjectRequestSnippet(),

              snippets.getProjectSnippet()));

    } finally {
      waitForProjectToFinishLoading(TEST_PROJECT);
      Project project = projectService.getProjectByProjectId(TEST_PROJECT);
      assertEquals(ProjectStatus.DONE, project.getStatus());
    }
  }

  @Test
  public void testDocsAddProjectAsCurator() throws Exception {
    User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    try {
      UploadedFileEntry fileEntry = createFile(
          new String(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")), StandardCharsets.UTF_8),
          admin);

      String body = EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
          new BasicNameValuePair("projectId", TEST_PROJECT),
          new BasicNameValuePair("name", "New Disease Map"),
          new BasicNameValuePair("file-id", String.valueOf(fileEntry.getId())),
          new BasicNameValuePair("license-id", "1"),
          new BasicNameValuePair("description", "this is my awesome project"),
          new BasicNameValuePair("notify-email", "notify.me@uni.lu"),
          new BasicNameValuePair("disease", "D010300"),
          new BasicNameValuePair("organism", "9606"),
          new BasicNameValuePair("sbgn", "false"),
          new BasicNameValuePair("version", "0.0.1"),
          new BasicNameValuePair("annotate", "false"),
          new BasicNameValuePair("custom-license-name", "my awesome license"),
          new BasicNameValuePair("custom-license-url", "https://my.awesome.lincese.org/"),
          new BasicNameValuePair("parser", "lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser"))));

      RequestBuilder request = post("/minerva/api/projects/{projectId}/", TEST_PROJECT)
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .content(body)
          .session(createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD));

      mockMvc.perform(request).andExpect(status().is2xxSuccessful())
          .andDo(document("projects/project_data/create_simple",
              projectPathParameters(),
              createProjectRequestSnippet(),
              snippets.getProjectSnippet()));

      assertNotNull(projectService.getProjectByProjectId(TEST_PROJECT).getLicense());
    } finally {
      waitForProjectToFinishLoading(TEST_PROJECT);
    }
  }

  private RequestParametersSnippet createProjectRequestSnippet() {
    return requestParameters(
        parameterWithName("projectId")
            .description("project identifier"),
        parameterWithName("file-id")
            .description("identifier of the file that should be used to create project"),
        parameterWithName("license-id")
            .description("identifier of the license for project")
            .optional(),
        parameterWithName("custom-license-name")
            .description("if custom licensing is used, it's a name of the license")
            .optional(),
        parameterWithName("custom-license-url")
            .description("if custom licensing is used, it's an url of the license")
            .optional(),
        parameterWithName("parser")
            .description("class of the parser that should be used for parsing the file. Available options: "
                + snippets.getParsers()),
        parameterWithName("cache")
            .description("should the data from external sources be cached after project was created")
            .optional(),
        parameterWithName("description")
            .description("description of the project")
            .optional(),
        parameterWithName("notify-email")
            .description("email address that should be when something change in the project")
            .optional(),
        parameterWithName("disease")
            .description("disease associated with the project (final MESH id)")
            .optional(),
        parameterWithName("name")
            .description("name of the project")
            .optional(),
        parameterWithName("organism")
            .description("organism associated with the project (final TAXONOMY id)")
            .optional(),
        parameterWithName("sbgn")
            .description("should the map be visualized in sbgn-like way")
            .optional(),
        parameterWithName("semantic-zooming-contains-multiple-overlays")
            .description("display each semantic zoom level in separate overlay")
            .optional(),
        parameterWithName("version")
            .description("version of the project")
            .optional(),
        parameterWithName("annotate")
            .description("should the project be automatically annotated")
            .optional(),
        parameterWithName("zip-entries")
            .description(
                "array of parameters describing each file in the zipped input file, file-index is number starting from 0")
            .optional()

    )
        .and(createArrayParameters("zip-entries[{number}][_filename]", "name of the file"))
        .and(createArrayParameters("zip-entries[{number}][_type]",
            "type of the file. Possible values: "
                + snippets.getOptionsAsString(ZipEntryFileDeserializer.ZipEntryFileType.class)))
        .and(createArrayParameters("zip-entries[{number}][_data][name]", "name of the map/name of the overlay"))
        .and(createArrayParameters("zip-entries[{number}][_data][mapping]",
            "for submaps - is this map a mapping file (true/false)"))
        .and(createArrayParameters("zip-entries[{number}][_data][root]",
            "for submaps - is this map a root map (true/false)"))
        .and(createArrayParameters("zip-entries[{number}][_data][type][id]",
            "for submaps defines type of the connection. Possible values: "
                + snippets.getOptionsAsString(SubmodelType.class)))
        .and(createArrayParameters("zip-entries[{number}][_data][description]",
            "for overlays - description of the overlay"));
  }

  private List<ParameterDescriptor> createArrayParameters(final String name, final String description) {
    List<ParameterDescriptor> result = new ArrayList<>();
    result.add(parameterWithName(name).description(description).optional());
    for (int i = 0; i < 10; i++) {
      result.add(parameterWithName(name.replace("{number}", i + "")).description(description).optional().ignored());
    }
    return result;
  }

}
