package lcsb.mapviewer.web;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.annotation.cache.CacheQueryMinervaJob;
import lcsb.mapviewer.annotation.data.serializer.ChemicalSerializer;
import lcsb.mapviewer.annotation.services.PubmedSearchException;
import lcsb.mapviewer.common.TextFileUtils;
import lcsb.mapviewer.common.exception.InvalidXmlSchemaException;
import lcsb.mapviewer.common.tests.TestUtils;
import lcsb.mapviewer.common.tests.UnitTestFailedWatcher;
import lcsb.mapviewer.converter.ColorSchemaReader;
import lcsb.mapviewer.converter.zip.ZipEntryFileFactory;
import lcsb.mapviewer.model.Project;
import lcsb.mapviewer.model.ProjectLogEntry;
import lcsb.mapviewer.model.ProjectLogEntryType;
import lcsb.mapviewer.model.ProjectStatus;
import lcsb.mapviewer.model.cache.UploadedFileEntry;
import lcsb.mapviewer.model.graphics.ArrowTypeData;
import lcsb.mapviewer.model.graphics.HorizontalAlign;
import lcsb.mapviewer.model.graphics.LineType;
import lcsb.mapviewer.model.graphics.PolylineData;
import lcsb.mapviewer.model.graphics.VerticalAlign;
import lcsb.mapviewer.model.job.MinervaJob;
import lcsb.mapviewer.model.job.MinervaJobPriority;
import lcsb.mapviewer.model.job.MinervaJobType;
import lcsb.mapviewer.model.map.Comment;
import lcsb.mapviewer.model.map.MiriamData;
import lcsb.mapviewer.model.map.MiriamType;
import lcsb.mapviewer.model.map.OverviewImage;
import lcsb.mapviewer.model.map.OverviewImageLink;
import lcsb.mapviewer.model.map.OverviewModelLink;
import lcsb.mapviewer.model.map.OverviewSearchLink;
import lcsb.mapviewer.model.map.compartment.Compartment;
import lcsb.mapviewer.model.map.compartment.SquareCompartment;
import lcsb.mapviewer.model.map.kinetics.SbmlFunction;
import lcsb.mapviewer.model.map.kinetics.SbmlKinetics;
import lcsb.mapviewer.model.map.kinetics.SbmlParameter;
import lcsb.mapviewer.model.map.kinetics.SbmlUnit;
import lcsb.mapviewer.model.map.layout.ProjectBackground;
import lcsb.mapviewer.model.map.layout.ProjectBackgroundImageLayer;
import lcsb.mapviewer.model.map.layout.ProjectBackgroundStatus;
import lcsb.mapviewer.model.map.layout.graphics.Glyph;
import lcsb.mapviewer.model.map.layout.graphics.Layer;
import lcsb.mapviewer.model.map.layout.graphics.LayerImage;
import lcsb.mapviewer.model.map.layout.graphics.LayerOval;
import lcsb.mapviewer.model.map.layout.graphics.LayerRect;
import lcsb.mapviewer.model.map.layout.graphics.LayerText;
import lcsb.mapviewer.model.map.model.Author;
import lcsb.mapviewer.model.map.model.ElementSubmodelConnection;
import lcsb.mapviewer.model.map.model.ModelData;
import lcsb.mapviewer.model.map.model.ModelSubmodelConnection;
import lcsb.mapviewer.model.map.model.SubmodelType;
import lcsb.mapviewer.model.map.modifier.Catalysis;
import lcsb.mapviewer.model.map.reaction.AndOperator;
import lcsb.mapviewer.model.map.reaction.Modifier;
import lcsb.mapviewer.model.map.reaction.NodeOperator;
import lcsb.mapviewer.model.map.reaction.Product;
import lcsb.mapviewer.model.map.reaction.Reactant;
import lcsb.mapviewer.model.map.reaction.Reaction;
import lcsb.mapviewer.model.map.reaction.type.TransportReaction;
import lcsb.mapviewer.model.map.species.AntisenseRna;
import lcsb.mapviewer.model.map.species.Complex;
import lcsb.mapviewer.model.map.species.Degraded;
import lcsb.mapviewer.model.map.species.Drug;
import lcsb.mapviewer.model.map.species.Element;
import lcsb.mapviewer.model.map.species.Gene;
import lcsb.mapviewer.model.map.species.GenericProtein;
import lcsb.mapviewer.model.map.species.Ion;
import lcsb.mapviewer.model.map.species.Phenotype;
import lcsb.mapviewer.model.map.species.Protein;
import lcsb.mapviewer.model.map.species.Rna;
import lcsb.mapviewer.model.map.species.SimpleMolecule;
import lcsb.mapviewer.model.map.species.Unknown;
import lcsb.mapviewer.model.map.species.field.BindingRegion;
import lcsb.mapviewer.model.map.species.field.ModificationResidue;
import lcsb.mapviewer.model.map.species.field.Residue;
import lcsb.mapviewer.model.map.species.field.StructuralState;
import lcsb.mapviewer.model.map.species.field.Structure;
import lcsb.mapviewer.model.map.species.field.UniprotRecord;
import lcsb.mapviewer.model.overlay.DataOverlay;
import lcsb.mapviewer.model.overlay.DataOverlayGroup;
import lcsb.mapviewer.model.overlay.DataOverlayType;
import lcsb.mapviewer.model.security.PrivilegeType;
import lcsb.mapviewer.model.user.ConfigurationElementType;
import lcsb.mapviewer.model.user.User;
import lcsb.mapviewer.services.interfaces.ICommentService;
import lcsb.mapviewer.services.interfaces.IConfigurationService;
import lcsb.mapviewer.services.interfaces.IDataOverlayGroupService;
import lcsb.mapviewer.services.interfaces.IDataOverlayService;
import lcsb.mapviewer.services.interfaces.IFileService;
import lcsb.mapviewer.services.interfaces.ILicenseService;
import lcsb.mapviewer.services.interfaces.IMinervaJobService;
import lcsb.mapviewer.services.interfaces.IMiriamService;
import lcsb.mapviewer.services.interfaces.IProjectService;
import lcsb.mapviewer.services.interfaces.IUserService;
import lcsb.mapviewer.web.api.project.map.bioentity.reaction.NewLineDTO;
import lcsb.mapviewer.web.app.WebConfig;
import lcsb.mapviewer.web.utils.CommandFormatterWithReplacingPostFilenameHeader;
import lcsb.mapviewer.web.utils.CustomCurlRequestSnippet;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Rule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.cli.CliDocumentation;
import org.springframework.restdocs.http.HttpDocumentation;
import org.springframework.restdocs.operation.preprocess.Preprocessors;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.restdocs.request.ParameterDescriptor;
import org.springframework.restdocs.request.PathParametersSnippet;
import org.springframework.restdocs.request.RequestParametersSnippet;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import javax.servlet.Filter;
import java.awt.Color;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@ContextConfiguration(classes = WebConfig.class)
public abstract class ControllerIntegrationTest extends TestUtils {

  protected static String INVALID_LONG_NAME = "aaaaaaaaxvncnbvscbnmcnbmccbnsbnsdsnbmdsvb"
      + "nmsdvnbmsdbmnbndvmsbnmsvdnbmnmbdsvnbmdsvxncbmbnmscbnzdnbnabnsbnamsdbmnsadbmnasd"
      + "bnmnbmsadbnmasdnbasdbnmsadnbnbmsadbnmadsnbmadsnbnbsadnbmadsbnndsabnbmdasbnmdsaj"
      + "qwrhgjrwhjghgjwerghjwreghwewnjnnbbbnbnbmbnbnzcmnnbmzcnmbcsbnmcsnbcnbzmnbczxnbmc"
      + "zxnbmcxznbcnxbmznbmxzcnbzcxnnbcxznbmzcnbczxnbmnbzcxnbmcznnczbnbzcnbmzcbnmbncznb"
      + "zxnbmcxznbcnxbmznbmxzcnbzcxnnbcxznbmzcnbczxnbmnbzcxnbmcznnczbnbzcnbmzcbnmbncznb";

  protected static Logger logger = LogManager.getLogger();

  @Rule
  public JUnitRestDocumentation junitRestDocumentation = new JUnitRestDocumentation();

  @Rule
  public UnitTestFailedWatcher unitTestFailedWatcher = new UnitTestFailedWatcher();
  protected MockMvc mockMvc;
  @Autowired
  private WebApplicationContext context;
  @Autowired
  private Filter springSecurityFilterChain;

  @Autowired
  private IUserService userService;

  @Autowired
  private IConfigurationService configurationService;

  @Autowired
  private ILicenseService licenseService;

  @Autowired
  private IProjectService projectService;

  @Autowired
  private IMinervaJobService minervaJobService;

  @Autowired
  private ICommentService commentService;

  @Autowired
  private IMiriamService miriamService;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  private IFileService fileService;

  @Autowired
  private IDataOverlayService dataOverlayService;

  @Autowired
  private IDataOverlayGroupService dataOverlayGroupService;

  @Autowired
  protected ObjectMapper objectMapper;

  @PostConstruct
  public void construct() {
    minervaJobService.enableQueue(false);
    mockMvc = MockMvcBuilders.webAppContextSetup(context)
        .addFilter(springSecurityFilterChain)
        .build();

    mockMvc = MockMvcBuilders.webAppContextSetup(context)
        .addFilter(springSecurityFilterChain)
        .apply(documentationConfiguration(this.junitRestDocumentation)
            .operationPreprocessors()
            .withResponseDefaults(Preprocessors.prettyPrint())
            .and()
            .uris().withHost("minerva-service.lcsb.uni.lu").withPort(443).withScheme("https")
            .and().snippets().withDefaults(
                new CustomCurlRequestSnippet(null, new CommandFormatterWithReplacingPostFilenameHeader(" \\%n    ")),
                CliDocumentation.httpieRequest(),
                HttpDocumentation.httpRequest(),
                HttpDocumentation.httpResponse(),
                PayloadDocumentation.requestBody(),
                PayloadDocumentation.responseBody()))
        .build();
  }

  @Before
  public final void __setUp() {
    if (dapiLogin != null) {
      if (configurationService == null) {
        logger.error("Configuration service not set");
      } else {
        configurationService.setConfigurationValue(ConfigurationElementType.DAPI_LOGIN, dapiLogin);
      }
    }

    if (dapiPassword != null) {
      if (configurationService == null) {
        logger.error("Configuration service not set");
      } else {
        configurationService.setConfigurationValue(ConfigurationElementType.DAPI_PASSWORD, dapiPassword);
      }
    }
  }


  /**
   * This method can be used to work around the fact that MockMvc cannot retrieve
   * cookies from Spring Security. The Reason for that is that MockMvc calls the
   * controller directly and (partially?) bypasses Spring.
   * <p>
   * This method creates a mocked session that can be used in a request as opposed
   * to the token.
   * </p>
   * <p>
   * FIXME: Find a better solution, that does not violate the spirit of
   * integration tests.
   * </p>
   */
  protected MockHttpSession createSession(final String login, final String password) throws Exception {
    final RequestBuilder request = post("/minerva/api/doLogin")
        .param("login", login)
        .param("password", password);
    return (MockHttpSession) mockMvc.perform(request)
        .andExpect(status().is2xxSuccessful())
        .andReturn()
        .getRequest()
        .getSession();
  }

  protected User createUser(final String login, final String password) {
    final User user = new User();
    user.setLogin(login);
    user.setCryptedPassword(passwordEncoder.encode(password));
    userService.add(user);
    return user;
  }

  protected User createUser(final String login, final String password, final Project project) {
    final User user = createUser(login, password);
    userService.grantUserPrivilege(user, PrivilegeType.READ_PROJECT, project.getProjectId());
    return user;
  }

  protected User createAdmin(final String login, final String password) {
    final User user = createUser(login, password);
    userService.grantUserPrivilege(user, PrivilegeType.IS_ADMIN);
    return user;
  }

  protected User createCurator(final String login, final String password) {
    final User user = createUser(login, password);
    userService.grantUserPrivilege(user, PrivilegeType.IS_CURATOR);
    return user;
  }

  protected User createCurator(final String login, final String password, final Project project) {
    final User user = createCurator(login, password);
    userService.grantUserPrivilege(user, PrivilegeType.READ_PROJECT, project.getProjectId());
    userService.grantUserPrivilege(user, PrivilegeType.WRITE_PROJECT, project.getProjectId());
    return user;
  }

  protected Project createProjectWithGlyph(final String projectId) throws IOException {
    final User admin = userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN);
    final Project project = new Project(projectId);
    project.setOwner(admin);
    final UploadedFileEntry file = createFile(Files.readAllBytes(Paths.get("./src/test/resources/empty.png")), admin);
    final ModelData map = new ModelData();
    map.setName("My map");
    map.setTileSize(256);
    map.setWidth(100);
    map.setHeight(100);

    final Protein element = createProtein();
    element.addStructuralState(createStructuralState(element));
    final Glyph glyph = new Glyph();
    glyph.setFile(file);
    element.setGlyph(glyph);
    map.addElement(element);

    final Layer layer = createLayer();
    final LayerText text = new LayerText();
    text.setNotes("Hello world!");
    text.setFontSize(10);
    text.setFillColor(Color.WHITE);
    text.setBorderColor(Color.BLACK);
    text.setX(1.0);
    text.setY(2.0);
    text.setWidth(3.0);
    text.setHeight(4.0);
    text.setZ(12);
    text.setGlyph(glyph);
    layer.addLayerText(text);

    layer.addLayerImage(new LayerImage());
    map.addLayer(layer);

    project.addModel(map);
    project.addGlyph(glyph);
    projectService.add(project);
    return project;
  }

  protected Project createAndPersistProject(final String projectId) {
    final Project project = new Project(projectId);
    project.setOwner(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    project.setVersion("1.0");
    project.setDisease(new MiriamData(MiriamType.MESH_2012, "D010300"));
    project.setOrganism(new MiriamData(MiriamType.TAXONOMY, "9606"));
    project.setDirectory("289b78b436176091ad900020c933c544");
    project.setNotifyEmail("minerva@uni.lu");
    final OverviewImage image = new OverviewImage();
    image.setFilename("image.png");
    image.setHeight(100);
    image.setWidth(200);
    project.addOverviewImage(image);
    project.setName("Test Disease");
    final ProjectLogEntry entry = new ProjectLogEntry();
    entry.setMapName("map_name");
    entry.setSeverity("WARNING");
    entry.setType(ProjectLogEntryType.OTHER);
    entry.setContent("This is warning message");
    project.addLogEntry(entry);
    try {
      project.setInputData(createFile(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")),
          userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN)));
    } catch (final IOException e) {
      logger.warn(e, e);
    }

    final ModelData map = new ModelData();
    map.addAuthor(new Author("John", "Doe"));
    map.addMiriamData(new MiriamData(MiriamType.MESH_2012, "D010300"));
    final OverviewModelLink link1 = new OverviewModelLink();
    link1.setLinkedModel(map);
    link1.setPolygon("0,0 10,10 10,0");
    link1.setxCoord(10);
    link1.setyCoord(20);
    link1.setZoomLevel(2);
    image.addLink(link1);

    final OverviewImageLink link2 = new OverviewImageLink();
    link2.setLinkedOverviewImage(image);
    link2.setPolygon("0,0 10,10 10,0");
    image.addLink(link2);

    final OverviewSearchLink link3 = new OverviewSearchLink();
    link3.setQuery("a1");
    link3.setPolygon("0,0 10,10 10,0");
    image.addLink(link3);

    map.setName("map_name");
    map.setTileSize(256);
    map.setWidth(400);
    map.setHeight(400);
    final SbmlFunction sbmlFunction = new SbmlFunction("f1");
    sbmlFunction.setName("name");
    try {
      sbmlFunction.setDefinition("<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><lambda>\n"
          + "<bvar>\n"
          + "<ci> x </ci>\n"
          + "</bvar>\n"
          + "<bvar>\n"
          + "<ci> y </ci>\n"
          + "</bvar>\n"
          + "<apply>\n"
          + "<plus/>\n"
          + "<ci> x </ci>\n"
          + "<ci> y </ci>\n"
          + "</apply>\n"
          + "</lambda></math>");
    } catch (final InvalidXmlSchemaException e) {
      throw new RuntimeException();
    }
    map.addFunction(sbmlFunction);

    final SbmlParameter sbmlParameter = new SbmlParameter("parameter1");
    sbmlParameter.setName("D coefficient");
    sbmlParameter.setValue(1.2);
    map.addParameter(sbmlParameter);

    final SbmlUnit sbmlUnit = new SbmlUnit("u1");
    sbmlUnit.setName("Weight");
    map.addUnit(sbmlUnit);

    map.addUnit(new SbmlUnit("unused"));

    final Compartment comp = createCompartment();
    comp.addFormerSymbol("FFH");
    comp.addMiriamData(new MiriamData(MiriamType.HGNC, "SNCA"));
    map.addElement(comp);

    final Reaction reaction = new TransportReaction("path_0_re5933");
    reaction.setZ(2);
    reaction.setLine(new PolylineData(new Point2D.Double(0, 0), new Point2D.Double(10, 0)));
    reaction.addMiriamData(new MiriamData(MiriamType.PUBMED, "28725475"));
    reaction.addMiriamData(new MiriamData(MiriamType.PUBMED, "invalid123"));
    reaction.addSynonym("re synonym");
    final SbmlKinetics kinetics = new SbmlKinetics();
    kinetics.setDefinition("<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><lambda>\n"
        + "<bvar>\n"
        + "<ci> x </ci>\n"
        + "</bvar>\n"
        + "<bvar>\n"
        + "<ci> y </ci>\n"
        + "</bvar>\n"
        + "<apply>\n"
        + "<plus/>\n"
        + "<ci> x </ci>\n"
        + "<ci> y </ci>\n"
        + "</apply>\n"
        + "</lambda></math>");
    reaction.setKinetics(kinetics);
    map.addReaction(reaction);

    final Complex complex = createComplex();
    map.addElement(complex);
    comp.addElement(complex);

    final Protein element = createProtein();
    element.addMiriamData(new MiriamData(MiriamType.HGNC_SYMBOL, "GSTA4"));
    element.addBindingRegion(createBindingRegion(element));
    element.addUniprot(createUniprot());
    map.addElement(element);
    element.setComplex(complex);
    element.setSubstanceUnits(sbmlUnit);
    comp.addElement(element);

    final Element element2 = createSimpleMolecule();
    element2.addMiriamData(new MiriamData(MiriamType.CHEBI, "CHEBI:1234"));
    map.addElement(element2);
    comp.addElement(element2);

    final Element element3 = createSimpleMolecule();
    map.addElement(element3);

    final Reactant reactant = new Reactant(element2);
    reactant.setLine(new PolylineData(new Point2D.Double(10, 0), new Point2D.Double(0, 0)));
    reaction.addReactant(reactant);

    final Reactant reactant2 = new Reactant(element3);
    reactant2.setLine(new PolylineData(new Point2D.Double(15, 0), new Point2D.Double(0, 0)));
    reaction.addReactant(reactant2);

    final Product product = new Product(element2);
    product.setLine(new PolylineData(new Point2D.Double(15, 0), new Point2D.Double(20, 0)));
    reaction.addProduct(product);

    final Product product2 = new Product(element3);
    product2.setLine(new PolylineData(new Point2D.Double(15, 0), new Point2D.Double(30, 0)));
    reaction.addProduct(product2);

    final Modifier modifier = new Catalysis(element2);
    modifier.setLine(new PolylineData(new Point2D.Double(10, 0), new Point2D.Double(0, 0)));
    reaction.addModifier(modifier);

    final NodeOperator operator = new AndOperator();
    operator.addOutput(product);
    operator.addOutput(product2);
    operator.setLine(new PolylineData(new Point2D.Double(10, 0), new Point2D.Double(15, 0)));
    reaction.addNode(operator);

    final NodeOperator operator2 = new AndOperator();
    operator2.addInput(reactant);
    operator2.addInput(reactant2);
    operator2.setLine(new PolylineData(new Point2D.Double(10, 0), new Point2D.Double(15, 0)));
    reaction.addNode(operator2);

    final ModelData submap = new ModelData();
    submap.setTileSize(256);
    submap.setWidth(300);
    submap.setHeight(350);
    submap.addElement(createProtein());

    final ModelSubmodelConnection submodelConnection = new ModelSubmodelConnection(submap, SubmodelType.UNKNOWN);
    map.addSubmodel(submodelConnection);
    element.setSubmodel(new ElementSubmodelConnection(submap, SubmodelType.UNKNOWN));

    project.addModel(map);
    project.addModel(submap);

    final ProjectBackground background = new ProjectBackground("Normal");
    int id = 0;
    for (final ModelData model : project.getModels()) {
      final ProjectBackgroundImageLayer layer = new ProjectBackgroundImageLayer(model, "directory_" + (id++));
      background.addProjectBackgroundImageLayer(layer);
    }
    background.setDefaultOverlay(true);
    background.setOrderIndex(0);
    background.setProgress(100);
    background.setStatus(ProjectBackgroundStatus.OK);
    background.setCreator(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    project.addProjectBackground(background);

    final Layer layer = createLayer();
    final LayerOval oval = new LayerOval();
    oval.setColor(Color.PINK);
    oval.setX(1.0);
    oval.setY(2.0);
    oval.setWidth(3.0);
    oval.setHeight(4.0);
    oval.setZ(12);
    layer.addLayerOval(oval);
    final LayerRect rect = new LayerRect();
    rect.setFillColor(Color.WHITE);
    rect.setBorderColor(Color.BLACK);
    rect.setX(1.0);
    rect.setY(2.0);
    rect.setWidth(3.0);
    rect.setHeight(4.0);
    rect.setZ(12);
    layer.addLayerRect(rect);
    final LayerText text = new LayerText();
    text.setNotes("Hello world!");
    text.setFontSize(10);
    text.setFillColor(Color.WHITE);
    text.setBorderColor(Color.BLACK);
    text.setX(1.0);
    text.setY(2.0);
    text.setWidth(3.0);
    text.setHeight(4.0);
    text.setZ(12);
    layer.addLayerText(text);

    layer.addLayerLine(new PolylineData(new Point2D.Double(102030, 421), new Point2D.Double(3210, 99)));

    layer.setName("Layer");
    layer.setLayerId("1");
    map.addLayer(layer);

    project.setLicense(licenseService.getById(2));

    projectService.add(project);
    try {
      for (final MiriamData md : reaction.getMiriamData()) {
        miriamService.getArticle(md);
      }
    } catch (final PubmedSearchException e) {
      logger.warn(e, e);
    }
    return project;
  }

  private UniprotRecord createUniprot() {
    final UniprotRecord result = new UniprotRecord();
    result.setUniprotId(faker.idNumber().valid());
    result.addStructure(createUniprotStructure());

    return result;
  }

  protected Structure createUniprotStructure() {
    final Structure result = new Structure();

    result.setChainId(faker.idNumber().valid());
    result.setCoverage(faker.random().nextDouble());
    result.setResolution(faker.random().nextDouble());
    result.setExperimentalMethod(faker.name().name());
    result.setPdbId(faker.idNumber().valid());
    result.setStructEnd(faker.random().nextInt());
    result.setStructStart(faker.random().nextInt());
    result.setTaxId(faker.random().nextInt());
    result.setUnpEnd(faker.random().nextInt());
    result.setUnpStart(faker.random().nextInt());

    return result;
  }

  private BindingRegion createBindingRegion(final Element element) {
    final BindingRegion region = new BindingRegion("b" + (counter++));
    region.setX(element.getCenterX());
    region.setY(element.getY());
    region.setHeight(10);
    region.setWidth(10);
    region.setZ(z++);
    region.setBorderColor(Color.BLACK);

    assignCoordinates(region);
    return region;
  }

  private void assignCoordinates(final ModificationResidue residue) {
    residue.setNameWidth(residue.getWidth());
    residue.setNameHeight(residue.getHeight());
    residue.setNameX(residue.getX());
    residue.setNameY(residue.getY());
  }

  private void assignCoordinates(final double x, final double y, final double width, final double height,
                                 final Element protein) {
    protein.setX(x);
    protein.setY(y);
    protein.setZ(z++);
    protein.setWidth(width);
    protein.setHeight(height);
    protein.setNameX(x);
    protein.setNameY(y);
    protein.setNameWidth(width);
    protein.setNameHeight(height);
    protein.setNameVerticalAlign(VerticalAlign.MIDDLE);
    protein.setNameHorizontalAlign(HorizontalAlign.CENTER);
  }

  private Element createSimpleMolecule() {
    final Element molecule = new SimpleMolecule("s" + (counter++));
    molecule.setName("water");
    assignCoordinates(10, 20, 100, 20, molecule);
    return molecule;
  }

  protected int counter = 1;

  private int z = 10;

  private GenericProtein createProtein() {
    final GenericProtein element = new GenericProtein("p" + (counter++));
    element.setName("GSTA4");
    assignCoordinates(10, 20, 100, 20, element);
    element.addResidue(createResidue(new Point2D.Double(element.getX(), element.getY())));
    element.addStructuralState(createStructuralState());
    element.addSynonym("GSTA_XX");
    element.addSynonym("GSTA_YY");
    return element;
  }

  private StructuralState createStructuralState() {
    final StructuralState result = new StructuralState();
    result.setName("X");
    result.setZ(z++);
    result.setPosition(new Point2D.Double(10, 20));
    result.setWidth(10);
    result.setHeight(10);
    result.setFontSize(12);
    result.setBorderColor(Color.black);

    assignCoordinates(result);
    return result;
  }

  protected StructuralState createStructuralState(final Element element) {
    final StructuralState result = new StructuralState();
    result.setX(element.getX());
    result.setY(element.getY() - 10);
    result.setWidth(30);
    result.setHeight(20);
    result.setZ(element.getZ() + 1);
    result.setBorderColor(Color.BLACK);
    assignCoordinates(result);
    return result;
  }

  private Residue createResidue(final Point2D position) {
    final Residue residue = new Residue("r" + (counter++));
    residue.setZ(z++);
    residue.setBorderColor(Color.BLACK);
    residue.setPosition(position);
    assignCoordinates(residue);
    return residue;
  }

  protected AntisenseRna createAntisenseRna() {
    final AntisenseRna element = new AntisenseRna("p" + (counter++));
    element.setName("aRNA");
    assignCoordinates(10, 20, 100, 20, element);
    return element;
  }

  protected Degraded createDegraded() {
    final Degraded element = new Degraded("p" + (counter++));
    assignCoordinates(10, 20, 100, 20, element);
    return element;
  }

  protected Drug createDrug() {
    final Drug element = new Drug("p" + (counter++));
    assignCoordinates(10, 20, 100, 20, element);
    return element;
  }

  protected Gene createGene() {
    final Gene element = new Gene("p" + (counter++));
    assignCoordinates(10, 20, 100, 20, element);
    return element;
  }

  protected Ion createIon() {
    final Ion element = new Ion("p" + (counter++));
    assignCoordinates(10, 20, 100, 20, element);
    return element;
  }

  protected Phenotype createPhenotype() {
    final Phenotype element = new Phenotype("p" + (counter++));
    assignCoordinates(10, 20, 100, 20, element);
    return element;
  }

  protected Rna createRna() {
    final Rna element = new Rna("p" + (counter++));
    assignCoordinates(10, 20, 100, 20, element);
    return element;
  }

  protected Unknown createUnknown() {
    final Unknown element = new Unknown("p" + (counter++));
    assignCoordinates(10, 20, 100, 20, element);
    return element;
  }

  private Complex createComplex() {
    final Complex element = new Complex("p" + (counter++));
    element.setName("CP");
    assignCoordinates(10, 20, 100, 20, element);
    return element;
  }

  private Compartment createCompartment() {
    final SquareCompartment element = new SquareCompartment("p" + (counter++));
    element.setName("Comp");
    element.addSynonym("my synonym");
    assignCoordinates(5, 5, 1000, 1000, element);
    return element;
  }


  protected Project createEmptyProject(final String projectId) {
    final Project project = new Project(projectId);
    project.setVersion("1.0.0");
    project.setOwner(userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN));
    project.setDirectory("289b78b436176091ad900020c933c544");
    project.setName("Test Disease");
    try {
      project.setInputData(createFile(Files.readAllBytes(Paths.get("./src/test/resources/generic.xml")),
          userService.getUserByLogin(BUILT_IN_TEST_ADMIN_LOGIN)));
    } catch (final IOException e) {
      logger.warn(e, e);
    }

    final ModelData map = new ModelData();
    map.setName("map_name");
    map.setTileSize(256);
    map.setWidth(100);
    map.setHeight(100);
    final Element element = createProtein();
    element.setName("PTI");
    map.addElement(element);

    project.addModel(map);
    projectService.add(project);
    return project;
  }

  protected UploadedFileEntry createFile(final String content, final User user) {
    return createFile(content.getBytes(), user);
  }

  protected UploadedFileEntry createFile(final byte[] content, final User user) {
    final UploadedFileEntry file = new UploadedFileEntry();
    file.setFileContent(content);
    file.setOriginalFileName("test_file");
    file.setLength(content.length);
    file.setOwner(user);
    fileService.add(file);
    return file;
  }

  protected void removeUser(final User user) throws Exception {
    if (user != null && userService.getUserByLogin(user.getLogin()) != null) {
      userService.remove(user);
    }
  }

  protected void removeFile(final UploadedFileEntry fileEntry) {
    fileService.delete(fileEntry);
  }

  protected void waitForProjectToFinishLoading(final String projectId) throws InterruptedException {
    Project project;
    do {
      project = projectService.getProjectByProjectId(projectId);
      if (project == null) {
        logger.warn("Project doesn't exist: {}", projectId);
        break;
      }
      Thread.sleep(100);
    } while (project.getStatus() != ProjectStatus.DONE && project.getStatus() != ProjectStatus.FAIL);
  }

  public void removeProject(final Project project) throws Exception {
    if (project != null) {
      removeProject(project.getProjectId());
    }
  }

  public void removeProject(final String projectId) throws Exception {
    minervaJobService.waitForTasksToFinish();
    final Project project = projectService.getProjectByProjectId(projectId);
    if (project != null) {
      final RequestBuilder request = delete("/minerva/api/projects/{projectId}/", projectId)
          .session(createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD));

      mockMvc.perform(request).andExpect(status().is2xxSuccessful());
    }
    minervaJobService.waitForTasksToFinish();
  }

  protected DataOverlay createOverlay(final Project project, final User admin) throws Exception {
    return createOverlay(project, admin, "element_identifier\tvalue\n\t-1");
  }

  protected DataOverlay createOverlay(final Project project, final User admin, final String content) throws Exception {
    return createOverlay(project, admin, content, DataOverlayType.GENERIC);
  }

  protected DataOverlay createOverlay(final Project project, final User admin, final String content,
                                      final DataOverlayType type)
      throws Exception {
    final UploadedFileEntry file = new UploadedFileEntry();
    file.setFileContent(content.getBytes());
    file.setLength(content.getBytes().length);
    file.setOriginalFileName("test.txt");

    final ColorSchemaReader reader = new ColorSchemaReader();

    final DataOverlay overlay = new DataOverlay();
    overlay.setColorSchemaType(type);
    overlay.setInputData(file);
    overlay.setProject(project);
    overlay.setName("test title");
    overlay.setDescription("test description");
    overlay.setCreator(admin);
    final Map<String, String> headers = new HashMap<>();
    headers.put(TextFileUtils.COLUMN_COUNT_PARAM, "2");
    headers.put(ZipEntryFileFactory.LAYOUT_HEADER_PARAM_TYPE, type.toString());
    overlay.addEntries(reader.readColorSchema(new ByteArrayInputStream(content.getBytes()), headers));
    dataOverlayService.add(overlay);
    return overlay;
  }

  protected DataOverlay createOverlay(final String projectId, final User user) throws Exception {
    final Project project = projectService.getProjectByProjectId(projectId);
    return createOverlay(project, user);
  }

  protected DataOverlay createOverlayWithColor(final Project project, final User admin) throws Exception {
    return createOverlay(project, admin, "element_identifier\tcolor\n\t#aa00cc");
  }

  protected Comment createAndPersistComment(final ModelData model, final User owner) {
    final Comment comment = createComment(model, owner);
    commentService.add(comment);
    return comment;
  }

  protected ModelData getBuildInModel() {
    return projectService.getProjectByProjectId(BUILT_IN_PROJECT, true).getTopModelData();
  }

  protected Comment createComment(final ModelData map) {
    return createComment(map, null);
  }

  protected Comment createComment(final ModelData map, final User owner) {
    final Comment comment = new Comment();
    comment.setContent("it's cool");
    comment.setModel(map);
    comment.setUser(owner);
    comment.setCoordinates(new Point2D.Double(10, 20));
    return comment;
  }

  protected PathParametersSnippet getProjectPathParameters() {
    return pathParameters(parameterWithName("projectId").description("project identifier"));
  }

  protected PathParametersSnippet getUserPathParameters() {
    return pathParameters(parameterWithName("userId").description("user id"));
  }

  protected PathParametersSnippet getPluginPathParameters() {
    return pathParameters(parameterWithName("hash").description("plugin hash"));
  }

  protected PathParametersSnippet getMapPathParameters() {
    return getProjectPathParameters().and(parameterWithName("mapId").description("map identifier"));
  }

  protected PathParametersSnippet getLayerPathParameters() {
    return getMapPathParameters().and(parameterWithName("layerId").description("layer identifier"));
  }

  protected PathParametersSnippet getCommentPathParameters() {
    return getMapPathParameters().and(parameterWithName("commentId").description("comment identifier"));
  }

  protected PathParametersSnippet getLayerImagePathParameters() {
    return getLayerPathParameters().and(parameterWithName("imageId").description("image identifier"));
  }

  protected PathParametersSnippet getLayerTextPathParameters() {
    return getLayerPathParameters().and(parameterWithName("textId").description("text identifier"));
  }

  protected PathParametersSnippet getDataOverlayGroupPathParameters() {
    return getProjectPathParameters().and(parameterWithName("overlayGroupId").description("group identifier"));
  }

  protected PathParametersSnippet getStacktracePathParameters() {
    return pathParameters(parameterWithName("id").description("stacktrace identifier"));
  }

  protected PathParametersSnippet getOverlayPathParameters() {
    return getProjectPathParameters().and(parameterWithName("overlayId").description("overlay identifier"));
  }

  protected PathParametersSnippet getOverlayMapPathParameters() {
    return getMapPathParameters().and(parameterWithName("overlayId").description("overlay identifier"));
  }

  protected PathParametersSnippet getElementPathParameters() {
    return getMapPathParameters().and(parameterWithName("elementId").description("element identifier"));
  }

  protected PathParametersSnippet getReactionPathParameters() {
    return getMapPathParameters().and(parameterWithName("reactionId").description("reaction identifier"));
  }

  protected RequestParametersSnippet getChemicalFilter() {
    return requestParameters(
        parameterWithName("columns")
            .description("set of columns (all by default). Available options: "
                + StringUtils.join(ChemicalSerializer.availableColumns(), ", "))
            .optional(),
        parameterWithName("query")
            .description("name of chemical that we are searching for")
            .optional(),
        parameterWithName("target")
            .description("target element that we are searching for in format TYPE:ID")
            .optional());
  }

  protected PathParametersSnippet projectPathParameters() {
    return pathParameters(parameterWithName("projectId").description("project identifier"));
  }

  protected PathParametersSnippet userPathParameters() {
    return pathParameters(parameterWithName("login").description("user login"));
  }

  protected List<ParameterDescriptor> getPageableFilter() {
    return Arrays.asList(
        parameterWithName("page").description("page number of results to fetch").optional(),
        parameterWithName("size").description("page size").optional());
  }


  protected void printPrettyJson(final String contentAsString) {
    try {
      logger.debug(objectMapper.writerWithDefaultPrettyPrinter()
          .writeValueAsString(objectMapper.readValue(contentAsString, new TypeReference<Object>() {
          })));
    } catch (final IOException e) {
      logger.warn(e, e);
    }
  }

  protected NewLineDTO createLineDTO() {
    final NewLineDTO result = new NewLineDTO();
    result.setColor(Color.YELLOW);
    result.setEndArrow(new ArrowTypeData());
    result.setStartArrow(new ArrowTypeData());
    result.setLineType(LineType.DOTTED);
    result.setSegments(Collections.singletonList(new Line2D.Double(1, 2, 12, 23)));
    result.setWidth(2.0);
    result.setZ(faker.number().numberBetween(1, 100));
    return result;
  }

  protected DataOverlayGroup createAndPersistDataOverlayGroup(final Project project, final User user) {
    DataOverlayGroup group = new DataOverlayGroup();
    group.setName(faker.name().name());
    group.setOrderIndex(faker.number().numberBetween(1, 10));
    group.setOwner(user);
    group.setProject(project);
    dataOverlayGroupService.add(group);
    return group;
  }

  protected Layer createLayer() {
    final Layer result = new Layer();
    result.setLayerId("l" + counter++);
    result.setName(faker.name().name());
    result.setZ(faker.number().numberBetween(1, 100));
    return result;
  }

  protected MinervaJob createMinervaJob() {
    return new MinervaJob(
        MinervaJobType.REFRESH_CACHE,
        MinervaJobPriority.LOWEST,
        new CacheQueryMinervaJob("https://google.lu", -1));
  }

}
