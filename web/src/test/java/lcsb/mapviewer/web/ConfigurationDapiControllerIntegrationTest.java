package lcsb.mapviewer.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import lcsb.mapviewer.annotation.services.dapi.dto.UserDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.RequestBuilder;

import static org.junit.Assume.assumeFalse;
import static org.junit.Assume.assumeTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("webCtdTestProfile")
public class ConfigurationDapiControllerIntegrationTest extends ControllerIntegrationTest {

  @Autowired
  private ObjectMapper objectMapper;

  @Test
  public void testDocsGetDapiConfiguration() throws Exception {
    RequestBuilder request = get("/minerva/api/configuration/dapi/");

    mockMvc.perform(request)
        .andDo(document("configuration/dapi/details",
            responseFields(subsectionWithPath("validConnection")
                .description("is the DAPI connection configured properly")
                .type("boolean"))))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDocsGetDapiDatabasesConfiguration() throws Exception {
    RequestBuilder request = get("/minerva/api/configuration/dapi/database/");

    mockMvc.perform(request)
        .andDo(document("configuration/dapi/list_database",
            responseFields(
                fieldWithPath("[]")
                    .description("list of available databases")
                    .type("array"),
                fieldWithPath("[].name")
                    .description("name of the database")
                    .type(JsonFieldType.STRING))))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void testDocsGetDapiDrugBankReleaseConfiguration() throws Exception {
    assumeTrue("DAPI credentials are not provided", isDapiConfigurationAvailable());
    RequestBuilder request = get("/minerva/api/configuration/dapi/database/{database}/release/", "DrugBank");

    mockMvc.perform(request)
        .andDo(document("configuration/dapi/list_release",
            pathParameters(parameterWithName("database").description("database name")),
            responseFields(
                fieldWithPath("[]")
                    .description("list of available releases")
                    .type("array"),
                fieldWithPath("[].licenseUrl")
                    .description("url with the content of license used for this release")
                    .type(JsonFieldType.STRING),
                fieldWithPath("[].licenseContent")
                    .description("text content of license used for this release")
                    .type(JsonFieldType.STRING),
                fieldWithPath("[].name")
                    .description("version of the database release")
                    .type(JsonFieldType.STRING),
                fieldWithPath("[].accepted")
                    .description("was this release accepted by currently logged in DAPI user")
                    .type("boolean"))))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void acceptLicenseReleaseWithoutAccess() throws Exception {
    RequestBuilder request = post("/minerva/api/configuration/dapi/database/{database}/release/{release}:acceptLicense", "DrugBank",
        "5.1");

    mockMvc.perform(request)
        .andExpect(status().isForbidden());
  }

  @Test
  public void testDocsAcceptLicenseReleaseWithAccess() throws Exception {
    assumeTrue("DAPI credentials are not provided", isDapiConfigurationAvailable());
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = post("/minerva/api/configuration/dapi/database/{database}/release/{release}:acceptLicense", "DrugBank",
        "5.1").session(session);

    mockMvc.perform(request)
        .andDo(document("configuration/dapi/accept_release_license",
            pathParameters(
                parameterWithName("database").description("database name"),
                parameterWithName("release").description("version of the database"))))
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  public void acceptLicenseWithoutDapiCredentials() throws Exception {
    assumeFalse("DAPI credentials are provided", isDapiConfigurationAvailable());
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    RequestBuilder request = post("/minerva/api/configuration/dapi/database/{database}/release/{release}:acceptLicense", "DrugBank",
        "5.1").session(session);

    mockMvc.perform(request)
        .andExpect(status().isUnavailableForLegalReasons());
  }

  @Test
  public void testDocsRegisterExistingDapiUser() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    UserDto user = new UserDto();
    user.setLogin("admin");
    user.setPassword("admin");
    user.setEmail("piotr.gawron@uni.lu");

    RequestBuilder request = post("/minerva/api/configuration/dapi:registerUser")
        .content(objectMapper.writeValueAsString(user))
        .contentType(MediaType.APPLICATION_JSON)
        .session(session);

    mockMvc.perform(request)
        .andDo(document("configuration/dapi/register_user",
            requestFields(
                fieldWithPath("login").description("user login").type(JsonFieldType.STRING),
                fieldWithPath("password").description("user password").type(JsonFieldType.STRING),
                fieldWithPath("email").description("email address").type(JsonFieldType.STRING))))
        .andExpect(status().isConflict());
  }

  @Test
  public void testRegisterUserInvalidData() throws Exception {
    MockHttpSession session = createSession(BUILT_IN_TEST_ADMIN_LOGIN, BUILT_IN_TEST_ADMIN_PASSWORD);

    UserDto user = new UserDto();
    user.setLogin(null);
    user.setPassword("");
    user.setEmail("piotr.gawronuni.lu");

    RequestBuilder request = post("/minerva/api/configuration/dapi:registerUser")
        .content(objectMapper.writeValueAsString(user))
        .contentType(MediaType.APPLICATION_JSON)
        .session(session);

    mockMvc.perform(request)
        .andExpect(status().isBadRequest())
        .andReturn().getResponse().getContentAsString();
  }

}
